
MESSAGE=$(git log -1 HEAD --pretty=format:%s)
if [[ "$MESSAGE" == *\[Sandbox\]\ Bump\ version* ]]; then
echo "Build SandBox"
cd ./fastlane
fastlane ios sandbox
else if [[ "$MESSAGE" == *\[Staging\]\ Bump\ version* ]]; then
echo "Build Staging"
cd ./fastlane
fastlane ios staging
else if [[ "$MESSAGE" == *\[Production\]\ Bump\ version* ]]; then
echo "Build Production"
cd ./fastlane
fastlane ios production
fi
fi
fi

