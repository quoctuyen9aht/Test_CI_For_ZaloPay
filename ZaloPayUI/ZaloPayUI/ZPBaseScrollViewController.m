//
//  ZPBaseScrollViewController.m
//  ZaloPayCommon
//
//  Created by PhucPv on 6/21/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPBaseScrollViewController.h"
#import <ZaloPayCommon/NSArray+ConstrainsLayout.h>
#import <ZaloPayCommon/UIButton+Extension.h>
#import <ZaloPayCommon/UIImage+Extention.h>
#import <ZaloPayCommon/UIView+Extention.h>
#import <Masonry/Masonry.h>
#import <ZaloPayCommon/UIColor+Extension.h>
#import <ZaloPayCommon/ZaloPayCommonLog.h>
#import <ZaloPayCommon/ZPConstants.h>
#import <ZaloPayCommon/R.h>

@interface ZPBaseScrollViewController()
@end

@implementation ZPBaseScrollViewController
- (id)initWithSuccessBlock:(ActionBlock)successBlock
                 failBlock:(ActionBlock)failBlock
               cancelBlock:(ActionBlock)cancelBlock {
    self = [super init];
    if (self) {
        self.successBlock = successBlock;
        self.failBlock = failBlock;
        self.cancelBlock = cancelBlock;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.scrollView = [[UIScrollView alloc] init];
    [self.view addSubview:self.scrollView];
    self.contentView = [[UIView alloc] init];
    [self.scrollView addSubview:self.contentView];
    self.scrollView.alwaysBounceVertical = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.contentView.backgroundColor = [UIColor defaultBackground];
    self.view.backgroundColor = [UIColor defaultBackground];
    self.scrollView.backgroundColor = [UIColor defaultBackground];
    
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(0);
        make.right.equalTo(0);
        make.top.equalTo(0);
        make.height.equalTo([self scrollHeight]);
    }];
 
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.scrollView);
        make.edges.equalTo(self.scrollView);
        make.left.equalTo(@0);
        make.right.equalTo(@0);
    }];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(hideKeyboard)];
    [self.navigationController.navigationBar addGestureRecognizer:singleTap];
    singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                        action:@selector(hideKeyboard)];
    [self.scrollView addGestureRecognizer:singleTap];
    
    [self registerHandleKeyboardNotification];
}

- (void)hideKeyboard {
    [self.view endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    return YES;
}

#pragma mark - BSKeyboardControls

- (NSString *)buttonText {
    return [R string_ButtonLabel_Next];
}

- (UIButton *)bottomButton {
    if (!_bottomButton) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.view addSubview:button];
        [button setTitle:[self buttonText] forState:UIControlStateNormal];
        [button setupZaloPayButton];
        [button addTarget:self
                   action:@selector(continueButtonClicked:)
         forControlEvents:UIControlEventTouchUpInside];
        
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(10);
            make.right.equalTo(-10);
            make.height.equalTo(kZaloButtonHeight);
            make.top.equalTo(self.scrollView.mas_bottom).with.offset([self buttonToScrollOffset]);
        }];
        [button roundRect:3];
        _bottomButton = button;
    }
    return _bottomButton;
}

#pragma mark - override

- (UIScrollView *)internalScrollView {
    return self.scrollView;
}

- (void)continueButtonClicked:(id)sender {
    
}

- (UIButton *)internalBottomButton {
    return self.bottomButton;
}

@end
