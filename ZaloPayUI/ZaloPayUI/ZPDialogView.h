//
//  ZPAlertView.h
//  ZaloPay
//
//  Created by PhucPv on 9/5/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

//#import >
#import <MMPopupView/MMPopupView.h>

typedef enum {
    DialogTypeNone          = 0,
    DialogTypeError         = 1,
    DialogTypeSuccess       = 2,
    DialogTypeWarning       = 3,
    DialogTypeNotification  = 4,
    DialogTypeNoInternet    = 5,
    DialogTypeInfo,
    DialogTypeUpgrade,
    DialogTypeConfirm,
} DialogType;

typedef  void (^ZPDialogCompleteHandle)(int buttonIndex, int cancelButtonIndex);
extern NSString *ZPDialogViewHideNotification;

@class ZPIconFontImageView;
@class MASViewAttribute;

extern NSString *CloseAuthenScreenNotification;
@interface ZPDialogView : MMPopupView
@property (nonatomic, strong) UILabel     *describeLabel;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) ZPIconFontImageView *imageView;
@property (nonatomic, strong) UILabel     *titleLabel;
@property (nonatomic, strong) UIView      *buttonView;
@property (nonatomic, strong) MASViewAttribute *lastAttribute;
@property (nonatomic) DialogType mDialogType;

- (id)initUpgradeVersion:(NSString *)version
                 message:(NSString *)message
                  handle:(void (^)(void)) handle;

- (id)initWithTitle:(NSString *)title
            message:(NSString *)message
              items:(NSArray *)items
              type:(DialogType)dialogType;

- (id)initWithType:(DialogType)dialogType
           message:(NSString *)message
             items:(NSArray *)items;

+ (instancetype)showDialogWithType:(DialogType)type
                           message:(NSString *)message
                 cancelButtonTitle:(NSString *)cancelButton
                  otherButtonTitle:(NSArray *)otherButton
                    completeHandle:(ZPDialogCompleteHandle)completeHandle;

+ (id)showDialogWithType:(DialogType)dialogType
                   title:(NSString *)title
                 message:(NSString *)message
            buttonTitles:(NSArray *)otherButtonTitles
                 handler:(MMPopupItemHandler)handler;

+ (id)showDialogWithType:(DialogType)dialogType
                   title:(NSString *)title
        attributeMessage:(NSAttributedString *)message
            buttonTitles:(NSArray *)otherButtonTitles
                 handler:(MMPopupItemHandler)handler;

+ (id)showDialogBoldFinalWithType:(DialogType)dialogType
                            title:(NSString *)title
                 attributeMessage:(NSAttributedString *)message
                     buttonTitles:(NSArray *)otherButtonTitles
                          handler:(MMPopupItemHandler)handler;


+ (id)showDialogWithError:(NSError *)error handle:(void (^)(void)) handle;

+ (void)dismissAllDialog;

- (void)showAutoDismissAfter:(NSTimeInterval)time;

+ (NSString *)titleWithType:(DialogType)type;

+ (NSMutableAttributedString *)attributeStringWithMessage:(NSString *)message;

+ (BOOL)isDialogShowing;
@end
@interface MMButtonItem : MMPopupItem
@property (nonatomic, strong) UIFont *font;
@end

MMButtonItem * MMButtonItemMake(NSString* title, MMPopupItemHandler handler, UIColor *color, UIFont *font);

MMButtonItem * MMBoldItemMake(NSString* title, MMPopupItemHandler handler);

MMButtonItem * MMNormalItemMake(NSString* title, MMPopupItemHandler handler);
