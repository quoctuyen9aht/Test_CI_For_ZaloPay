//
//  ZPScrollViewController.h
//  ZaloPayCommon
//
//  Created by bonnpv on 11/19/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController (ZPScrollViewController)

- (void)registerHandleKeyboardNotification;

#pragma mark - to override 
- (UIScrollView *)internalScrollView;
- (UIButton *)internalBottomButton;
- (float)scrollHeight;
- (float)buttonToScrollOffset;
@end
