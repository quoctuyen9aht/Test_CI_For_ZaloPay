//
//  BasePageViewController.m
//  ZaloPayUI
//
//  Created by vuongvv on 5/16/18.
//  Copyright © 2018 VNG. All rights reserved.
//

#import "BasePageViewController.h"
#import <ReactiveObjC/ReactiveObjC.h>
#import <ZaloPayCommon/UIFont+Extension.h>
#import <ZaloPayCommon/UIColor+Extension.h>
#import <ZaloPayCommon/UIImage+Extention.h>
#import <ZaloPayCommon/MasonryConfig.h>
#import "BaseViewController.h"
#import <ZaloPayCommon/ZaloPayCommonLog.h>
#import "ZPAppFactory.h"

@interface BasePageViewController ()

@end

@implementation BasePageViewController
- (void)dealloc {
    DDLogInfo(@"-[%@ dealloc]", NSStringFromClass([self class]));
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self baseViewControllerViewDidLoad];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)] &&
        gestureRecognizer == self.navigationController.interactivePopGestureRecognizer) {
        return NO;
    }
    
    return [self isAllowSwipeBack];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    return [gestureRecognizer isKindOfClass:[UIScreenEdgePanGestureRecognizer class]];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if ([[self.navigationController viewControllers] count] <= 1) {
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        return;
    }
    
    self.navigationController.interactivePopGestureRecognizer.enabled = [self isAllowSwipeBack];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
    
    //Fix When Pan Edge back cancel don't hide Bottom Bar
    UITabBar *tabBar = self.tabBarController.tabBar;
    tabBar.hidden = true;
}

- (BOOL)isAllowSwipeBack {
    return YES;
}

- (NSString*)screenName {
    return @"";
}
- (void)backButtonClicked:(id)sender {
    [super backButtonClicked:sender];
}
- (void) setViewControllers:(NSArray*)viewControllers direction:(UIPageViewControllerNavigationDirection)direction animated:(BOOL)animated completion:(void (^)(BOOL))completion {
    
    if (!animated) {
        [super setViewControllers:viewControllers direction:direction animated:NO completion:completion];
        return;
    }
    
    [super setViewControllers:viewControllers direction:direction animated:YES completion:^(BOOL finished){
        
        if (finished) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [super setViewControllers:viewControllers direction:direction animated:NO completion:completion];
            });
        } else {
            if (completion != NULL) {
                completion(finished);
            }
        }
    }];
}
#pragma mark - Rotate

- (BOOL)shouldAutorotate {
    return false;
}

- (UIInterfaceOrientationMask) supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
@end
