//
//  ZPBaseScrollViewController.h
//  ZaloPayCommon
//
//  Created by PhucPv on 6/21/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

typedef void (^ActionBlock)(void);

#import "BaseViewController.h"
#import "BaseViewController+ZPScrollViewController.h"

@interface ZPBaseScrollViewController : BaseViewController
//@property (nonatomic, strong) BSKeyboardControls *keyboardControls;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIView *contentView;

@property (nonatomic, copy) ActionBlock successBlock;
@property (nonatomic, copy) ActionBlock failBlock;
@property (nonatomic, copy) ActionBlock cancelBlock;

- (id)initWithSuccessBlock:(ActionBlock)successBlock
                 failBlock:(ActionBlock)failBlock
               cancelBlock:(ActionBlock)cancelBlock;
@property (nonatomic, strong) UIButton *bottomButton;

#pragma mark - To Override
- (void)continueButtonClicked:(id)sender;
- (NSString *)buttonText;
@end
