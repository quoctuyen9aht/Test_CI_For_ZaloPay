//
//  BaseViewController.h
//  Shoppie
//
//  Created by Le Quang Vinh on 11/13/13.
//
//

#import <UIKit/UIKit.h>
#import "UIViewController+BaseViewController.h"


@interface BaseViewController : UIViewController
@property (assign, nonatomic) BOOL isAppear;
- (BOOL)isAllowSwipeBack;
- (NSString*)screenName;
@end
