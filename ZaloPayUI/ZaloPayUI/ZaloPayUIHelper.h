//
//  ZaloPayUIHelper.h
//  ZaloPayUI
//
//  Created by thi la on 6/19/18.
//  Copyright © 2018 VNG. All rights reserved.
//

#ifndef ZaloPayUIHelper_h
#define ZaloPayUIHelper_h

#import "ZPAppFactory.h"
#import <ZaloPayCommon/UIColor+Extension.h>
#import <ZaloPayCommon/UIButton+IconFont.h>
#import <ZaloPayCommon/UIFont+IconFont.h>
#endif /* ZaloPayUIHelper_h */
