//
//  ZPAppFactory.h
//  ZaloPayCommon
//
//  Created by Nguyễn Hữu Hoà on 7/2/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class ZPTracker;
@class ZPAOrderTracking;
@class RACSignal;
@class ZPRPManager;
@class ReactModuleViewController;
@class ZPTrackerManager;
@class ZPBill;

typedef enum {
    ReactNativeChannelType_Profile            = 1,
    ReactNativeChannelType_Bank               = 2,
    ReactNativeChannelType_Transaction        = 3,
    ReactNativeChannelType_ClearingAccount    = 4,
} ReactNativeChannelType;

@interface ZPAppFactory : NSObject
+ (instancetype)sharedInstance;
@property (nonatomic, strong, readonly) ZPRPManager *rpManager;
@property (nonatomic, strong, readwrite) ZPAOrderTracking *orderTracking;

- (void)registerRedPacketManager:(ZPRPManager *)rpManager;
- (void)registerOrderTracking:(ZPAOrderTracking *)orderTracking;

+ (UIViewController*) topMostController;
//
//- (void)trackEvent:(ZPAnalyticEventAction)eventId;
//- (void)trackEvent:(NSInteger)eventId value:(NSNumber *)eventValue;
//- (void)trackEvent:(ZPAnalyticEventAction)eventId label:(NSString *)eventLabel;
//- (void)trackEvent:(ZPAnalyticEventAction)eventId value:(NSNumber *)eventValue label:(NSString *)eventLabel;
//- (void)trackEvent:(ZPAnalyticEventAction)eventId valueString:(NSString *)eventValue label:(NSString *)eventLabel;
//- (void)trackTiming:(ZPAnalyticEventAction)eventId value:(NSNumber *)eventValue;
//- (void)trackScreen:(NSString *)screenName;
//- (void)trackCustomEvent:(NSString *)eventCategory
//                  action:(NSString *)eventAction
//                   label:(NSString *)eventLabel
//                   value:(NSNumber *)eventValue;
@end

@interface ZPAppFactory (Extension)
#pragma mark - save password
- (void)saveLastTimeInputPin;

#pragma mark - Feedback

- (void)showFeedbackViewController;

- (void)showVoucherView;

#pragma mark - Dialog View

- (void)promptPIN:(ReactNativeChannelType)channel callback:(void (^)(id result))callback error:(void (^)(NSError *))errorBlock;

- (void)showPinDialogMessage:(NSString *)message
                isForceInput:(BOOL)force
                     success:(void (^)(void))handle
                       error:(void (^)(NSError *))errorBlock;
- (RACSignal *)checkPinNoCacheWithMessage:(NSString *)message;

- (void)showZaloPayGateway:(UIViewController *)fromcontroller
             completeBlock:(void (^)(NSDictionary *data))completeBlock
               cancelBlock:(void (^)(NSDictionary *data))cancelBlock
                errorBlock:(void (^)(NSDictionary *data))errorBlock
                     appId:(NSInteger)appId
                  billData:(NSDictionary *)billData;

- (void)showZaloPayGateway:(UIViewController *)fromVC
                     appId:(NSInteger)appId
                      with:(ZPBill *)bill
             completeBlock:(void (^)(NSDictionary *data))completeBlock
               cancelBlock:(void (^)(NSDictionary *data))cancelBlock
                errorBlock:(void (^)(NSDictionary *data))errorBlock;

- (BOOL)isPayingBill;

- (void)showNoInternetConnectionView;

- (void)logout;
#pragma mark - Loading View
- (void)setHUDWithTarget:(id) target sel:(SEL) sel;
- (void)removeHUDWithTarget:(id) target sel:(SEL) sel;
- (void)showHUDAddedTo:(UIView *)view;
- (void)hideAllHUDsForView:(UIView *)view;

#pragma mark - Pay Mechant Bill

- (void)payMerchantBillFromNotification:(NSString *)notificationid;

- (void)showBankView;
#pragma mark - Check Config Network


- (UINavigationController *)rootNavigation;


- (void)sendRemoveMessageWithMtaid:(uint64_t)mtaid mtuid:(uint64_t)mtuid;
- (void)sendReadMessageWithMtaid:(uint64_t)mtaid mtuid:(uint64_t)mtuid;

- (void)trackApiFail:(NSDictionary *)log;
- (void)trackNotification:(NSDictionary *)params;
- (void)trackBackButtonClickEventId:(NSInteger)eventId;
- (void)navigateProtectAccount;

- (void)launchContactList:(NSString *)currentPhone viewMode:(NSString *)viewMode navigationTitle:(NSString *)viewMode completeHandle:(void (^)(NSDictionary *data))completeBlock;
- (void)launchContactList:(NSString *)navigationTitle
          backgroundColor:(NSString *)backgroundColor
            selectedArray:(NSArray *)selectedArray
         maxUsersSelected:(int)maxUsersSelected
                     mode:(int)mode
           completeHandle:(void (^)(NSDictionary *data))completeBlock;

- (void)transferMoney:(NSString *)userId
               amount:(NSInteger)amount
              message:(NSString *)message
         transferMode:(NSString *)transferMode
             completeBlock:(void (^)(NSDictionary *data))completeBlock
               cancelBlock:(void (^)(NSDictionary *data))cancelBlock
                errorBlock:(void (^)(NSDictionary *data))errorBlock;

- (void)setHiddenTabbar:(BOOL)hide;

- (void)openApp:(NSInteger)appId moduleName:(NSString *)moduleName properties:(NSDictionary *)properties;

- (void)launchApp:(NSInteger)appId
       moduleName:(NSString *)moduleName
       properties:(NSDictionary *)properties
          success:(void (^)(NSDictionary *data))successBlock
            error:(void (^)(NSDictionary *data))errorBlock
       unvailable:(void (^)(NSDictionary *data))unvailableBlock;

- (void)launchWithRouterId:(int)routerId from:(UIViewController*)controller animated:(BOOL)animated title:(NSString*)title;

- (NSDictionary *)getUserInfosWith:(NSInteger)appId;

- (ReactModuleViewController *)createReactViewController:(NSDictionary *)properties;
- (void)beginSharing:(UIViewController *)parent image:(UIImage *)image withContent:(NSString *)content andLink:(NSString *)link;
- (void)beginSharing:(UIViewController *)parent withMessage:(NSString *)message andCaption:(NSString *)caption;
- (void)shareFanpageOnFacebook:(UIViewController *)parent callback:(void(^)(NSInteger))callback;
#pragma mark  - Red Packet
- (NSArray*)getListNotificationWithLixi:(NSArray*)arrMtaid arrMtuid:(NSArray*)arrMtuid;

- (void)showUserProfileViewController;
#pragma mark - Load Web View
- (void)launchWebApp:(NSString*)url;

#pragma mark - get app permission from config
- (NSArray*)getAppPermission:(NSInteger)appId;
@end
