//
//  ZPScrollViewController.m
//  ZaloPayCommon
//
//  Created by bonnpv on 11/19/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "BaseViewController+ZPScrollViewController.h"
#import <ZaloPayCommon/UIButton+Extension.h>
#import <ZaloPayCommon/UIImage+Extention.h>
#import <ZaloPayCommon/UIView+Extention.h>
#import <Masonry/Masonry.h>
#import <ZaloPayCommon/UIColor+Extension.h>
#import <ZaloPayCommon/ZaloPayCommonLog.h>
#import <ZaloPayCommon/NSArray+ConstrainsLayout.h>
#import <ReactiveObjC/ReactiveObjC.h>

@implementation BaseViewController (ZPScrollViewController)

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.isAppear = YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.isAppear = NO;
}

- (UIScrollView *)internalScrollView {
    return nil;
}

- (UIButton *)internalBottomButton {
    return nil;
}

- (float)scrollHeight {
    return  150;
}

- (float)buttonToScrollOffset {
    return  kZaloButtonToScrollOffset;
}

- (void)registerHandleKeyboardNotification {
    /* fix issue#1512: observable method of Swift and ObjC same in class ==> obserable of Swift will ignored
    @weakify(self);
    [[self rac_signalForSelector:@selector(viewWillAppear:)] subscribeNext:^(id x) {
        @strongify(self);
        self.isAppear = YES;
    }];
    */
    
    [self addNotification];
}

#pragma mark - Keyboard

- (float)keyboardAnimationDuration:(NSNotification *)notification {
    NSDictionary *info = [notification userInfo];
    NSNumber *number = [info objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    return [number doubleValue];
}

- (void)addNotification {
    /* fix issue#1512: observable method of Swift and ObjC same in class ==> obserable of Swift will ignored
    [[self rac_signalForSelector:@selector(viewWillDisappear:)] subscribeNext:^(id x) {
        @strongify(self);
        self.isAppear = NO;
    }];
     */
    
    @weakify(self);
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillShowNotification object:nil] filter:^BOOL(id value) {
        @strongify(self);
        return self.isAppear;
    }] subscribeNext:^(NSNotification *notification) {
        @strongify(self);
        [self keyboardWillShow:notification];
    }];
    
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillHideNotification object:nil] filter:^BOOL(id value) {
        @strongify(self);
        return self.isAppear;
    }] subscribeNext:^(NSNotification *notification) {
        @strongify(self);
        [self keyboardWillHide:notification];
    }];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    CGSize endFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    float keyboardHeight = endFrame.height;
    
    float remainHeight = self.view.frame.size.height - keyboardHeight;
    float requireHeight = [self scrollHeight] + kZaloButtonHeight + kZaloButtonToScrollOffset;
    
    if (remainHeight >= requireHeight) {
        return;
    }
    
    float scrollHeight = remainHeight - kMinZaloButtonToScrollOffset - kZaloButtonHeight - kMinZaloButtonToScrollOffset;
    
    if (scrollHeight > [self scrollHeight]) {
        scrollHeight = [self scrollHeight];
    }
    
    float buttonTopOffSet = (remainHeight - scrollHeight - kZaloButtonHeight)/2;
    
    [self.internalScrollView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(scrollHeight));
    }];
    
    [self.internalBottomButton mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.internalScrollView.mas_bottom).with.offset(buttonTopOffSet);
    }];
    
    [UIView animateWithDuration:[self keyboardAnimationDuration:notification]
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         [self.view layoutIfNeeded];
                     } completion:nil];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    [self.internalScrollView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@([self scrollHeight]));
    }];
    
    [self.internalBottomButton mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.internalScrollView.mas_bottom).with.offset([self buttonToScrollOffset]);
    }];
    
    [UIView animateWithDuration:[self keyboardAnimationDuration:notification]
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         [self.view layoutIfNeeded];
                     } completion:nil];
}

@end
