//
//  UIViewController+BaseViewController.m
//  ZaloPayCommon
//
//  Created by bonnpv on 9/14/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "UIViewController+BaseViewController.h"
#import "ZPAppFactory.h"
#import <ZaloPayCommon/UIColor+Extension.h>
#import <ZaloPayCommon/UIImage+Extention.h>
#import <ZaloPayCommon/UIFont+IconFont.h>
#import <ZaloPayCommon/UIButton+IconFont.h>
#import <ReactiveObjC/ReactiveObjC.h>

@implementation UIViewController (BaseViewController)

- (void)baseViewControllerViewDidLoad {
    self.view.backgroundColor = [UIColor defaultBackground];
    [self setUpUiBar];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.edgesForExtendedLayout = UIRectEdgeAll;
    self.navigationController.navigationBar.translucent = NO;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    UIImageView *imageView = [self findHairlineImageViewUnder:self.navigationController.navigationBar];
    imageView.hidden = YES;
    [[UINavigationBar appearance] setShadowImage:[UIImage new]];
    
    /* fix issue#1512: observable method of Swift and ObjC same in class ==> obserable of Swift will ignored
    @weakify(self);
    [[[self rac_signalForSelector:@selector(backButtonClicked:)] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(RACTuple * _Nullable x) {
        @strongify(self);
        ZPAnalyticEventAction backEventId = [self backEventId];
        if (backEventId == 0) {
            return;
        }
        [[ZPAppFactory sharedInstance] trackEvent:backEventId];
    }];
     */
}

- (void)trackEventBack{
    NSInteger backEventId = [self backEventId];
    if (backEventId == 0) {
        return;
    }
    [[ZPAppFactory sharedInstance] trackBackButtonClickEventId:backEventId];
}

- (NSString *)screenName {
    return NSStringFromClass([self class]);
}

- (BOOL)isEnableAuthenticationView {
    return TRUE;
}

- (void)setUpUiBar {
    self.navigationController.navigationBarHidden = NO;
    if (self.navigationController.viewControllers.count > 1) {
        self.navigationItem.leftBarButtonItems = [self leftBarButtonItems];
    }
}

- (NSArray *)leftBarButtonItems {    
    UIButton * backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setIconFont:[self backButtonIcon] forState:UIControlStateNormal];
    [backButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    backButton.titleLabel.font = [UIFont iconFontWithSize:20];
    backButton.frame = CGRectMake(0, 0, 44, 44);
    [backButton addTarget:self action:@selector(backButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [backButton addTarget:self action:@selector(trackEventBack) forControlEvents:UIControlEventTouchUpInside];
   
    UIBarButtonItem * backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    if (@available(iOS 11, *)) {
       return  @[backBarButtonItem];
    }
    UIBarButtonItem *negativeSeparator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSeparator.width = -12;
    return  @[negativeSeparator, backBarButtonItem];
}

- (NSString *)backButtonIcon {
    return @"general_backios";
}

- (void)backButtonClicked:(id)sender {
    [self viewWillSwipeBack];
    if (self.navigationController.viewControllers.count == 1) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (UIImageView *)findHairlineImageViewUnder:(UIView *)view {
    if ([view isKindOfClass:UIImageView.class] && view.bounds.size.height <= 1.0) {
        return (UIImageView *)view;
    }
    for (UIView *subview in view.subviews) {
        UIImageView *imageView = [self findHairlineImageViewUnder:subview];
        if (imageView) {
            return imageView;
        }
    }
    return nil;
}

- (void)viewWillSwipeBack {
    
}

- (NSInteger)backEventId {
    return 0;
}

@end
