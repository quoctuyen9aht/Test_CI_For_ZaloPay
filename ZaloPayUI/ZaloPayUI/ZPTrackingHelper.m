//
//  ZPScreenTrace.m
//  ZaloPayUI
//
//  Created by Bon Bon on 5/29/18.
//  Copyright © 2018 VNG. All rights reserved.
//

#import "ZPTrackingHelper.h"
#import "UIViewController+BaseViewController.h"
#import "ZPAppFactory.h"
#import <ZaloPayAnalyticsSwift/ZaloPayAnalyticsSwift-Swift.h>

@interface ZPTrackingHelper ()
@property (nonatomic, strong) NSString *currentScreen;
@property (nonatomic, strong) NSString *previousScreen;
@end

@implementation ZPTrackingHelper

+ (ZPTrackingHelper *)shared {
    static ZPTrackingHelper *_return;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _return = [[ZPTrackingHelper alloc] init];
    });
    return _return;
}

- (void)trackScreenWithController:(UIViewController *)controller {
    NSString *name = [controller screenName];
    [self trackScreenWithName:name];
}

- (void)trackScreenWithName:(NSString *)name {
    if (name.length > 0) {
        [self.eventTracker trackScreen:name];
        if ([name isEqualToString:self.currentScreen]) {
            return;
        }
        self.previousScreen = self.currentScreen;
        self.currentScreen  = name;
    }
}

- (NSString *)currentScreenName {
    return self.currentScreen;
}

- (NSString *)previousScreenName {
    return self.previousScreen;
}

- (void)trackEvent:(ZPAnalyticEventAction) eventId {
    [self.eventTracker trackEvent:eventId value:nil label:self.previousScreen];
}

- (void)trackEvent:(ZPAnalyticEventAction) eventId value:(NSNumber*)value {
    [self.eventTracker trackEvent:eventId value:value label:self.previousScreen];
}

- (void)trackEvent:(ZPAnalyticEventAction) eventId valueString:(NSString*)value {
    [self.eventTracker trackEvent:eventId valueString:value label:self.previousScreen];
}
@end
