//
//  UIViewController+BaseViewController.h
//  ZaloPayCommon
//
//  Created by bonnpv on 9/14/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (BaseViewController)
- (void)baseViewControllerViewDidLoad;

- (NSArray *)leftBarButtonItems;

- (void)backButtonClicked:(id)sender;

//! Should be overrided in derived class to provide customized screenName
- (NSString *)screenName;

- (void)viewWillSwipeBack;

- (NSString *)backButtonIcon;

- (NSInteger)backEventId;

- (void)trackEventBack;

- (BOOL)isEnableAuthenticationView;
@end
