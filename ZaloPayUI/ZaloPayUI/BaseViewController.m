//
//  BaseViewController.m
//  ZaloPay
//
//
#import <ReactiveObjC/ReactiveObjC.h>
#import <ZaloPayCommon/UIFont+Extension.h>
#import <ZaloPayCommon/UIColor+Extension.h>
#import <ZaloPayCommon/UIImage+Extention.h>
#import <ZaloPayCommon/MasonryConfig.h>
#import "BaseViewController.h"
#import <ZaloPayCommon/ZaloPayCommonLog.h>
#import "ZPAppFactory.h"
#import "UIViewController+BaseViewController.h"


@interface BaseViewController ()<UIGestureRecognizerDelegate, UINavigationControllerDelegate>

@end

@implementation BaseViewController

- (void)dealloc {
    DDLogInfo(@"-[%@ dealloc]", NSStringFromClass([self class]));
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self baseViewControllerViewDidLoad];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
   
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)] &&
        gestureRecognizer == self.navigationController.interactivePopGestureRecognizer) {
        return NO;
    }
    
    return [self isAllowSwipeBack];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    return [gestureRecognizer isKindOfClass:[UIScreenEdgePanGestureRecognizer class]];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if ([[self.navigationController viewControllers] count] <= 1) {
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        return;
    }
    
    self.navigationController.interactivePopGestureRecognizer.enabled = [self isAllowSwipeBack];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
}

- (BOOL)isAllowSwipeBack {
    return YES;
}

- (NSString*)screenName {
    return @"";
}
- (void)backButtonClicked:(id)sender {
    [super backButtonClicked:sender];
}

#pragma mark - Rotate

- (BOOL)shouldAutorotate {
    return false;
}

- (UIInterfaceOrientationMask) supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
