//
//  ZPScreenTrace.h
//  ZaloPayUI
//
//  Created by Bon Bon on 5/29/18.
//  Copyright © 2018 VNG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class ZPTrackerManager;
typedef NS_ENUM(NSInteger, ZPAnalyticEventAction);

@interface ZPTrackingHelper : NSObject
@property (nonatomic, strong) ZPTrackerManager *eventTracker;

+ (ZPTrackingHelper *)shared;
- (void)trackScreenWithController:(UIViewController *)controller;
- (void)trackScreenWithName:(NSString *)name;
- (NSString *)currentScreenName;
- (NSString *)previousScreenName;

// Note: Khi gọi 3 hàm track này bên trong code tự động gán eventlabel = previousscreen do yêu cầu bên product muốn tracking như vậy.
- (void)trackEvent:(ZPAnalyticEventAction) eventId;
- (void)trackEvent:(ZPAnalyticEventAction) eventId value:(NSNumber*)value;
- (void)trackEvent:(ZPAnalyticEventAction) eventId valueString:(NSString*)value;
@end
