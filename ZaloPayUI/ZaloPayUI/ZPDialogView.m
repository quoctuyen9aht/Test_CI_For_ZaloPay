//
//  ZPdialogView.m
//  ZaloPay
//
//  Created by PhucPv on 9/5/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPDialogView.h"
#import <ZaloPayCommon/UIFont+Extension.h>
#import <ZaloPayCommon/UIColor+Extension.h>
#import <ZaloPayCommon/MasonryConfig.h>
#import <ZaloPayCommon/ZPIconFontImageView.h>

#import <ZaloPayCommon/ZPConstants.h>
#import <ZaloPayCommon/NSCollection+JSON.h>
#import <Masonry/Masonry.h>
#import <MMPopupView/MMAlertView.h>
#import <MMPopupView/MMPopupDefine.h>
#import <ZaloPayCommon/UIImage+Extention.h>
#import <ZaloPayCommon/UIView+Extention.h>
#import <ZaloPayNetwork/ZaloPayErrorCode.h>
#import <ZaloPayNetwork/NSError+ZaloPayAPI.h>
#import <ReactiveObjC/ReactiveObjC.h>
#import <ZaloPayCommon/R.h>
#import <ZaloPayCommon/UIFont+IconFont.h>
#import <ZaloPayCommon/UIView+Config.h>

MMButtonItem * MMButtonItemMake(NSString* title, MMPopupItemHandler handler, UIColor *color, UIFont *font) {
    MMButtonItem *item = [MMButtonItem new];
    item.title = title;
    item.handler = handler;
    item.font = font;
    item.color = color;
    return item;
}

MMButtonItem * MMBoldItemMake(NSString* title, MMPopupItemHandler handler) {
    return MMButtonItemMake(title, handler, [UIColor zaloBaseColor], [UIFont SFUITextSemiboldWithSize:16]);
}

MMButtonItem * MMNormalItemMake(NSString* title, MMPopupItemHandler handler) {
    return MMButtonItemMake(title, handler, [UIColor zaloBaseColor], [UIFont SFUITextRegularWithSize:16]);
}


NSString *ZPDialogViewHideNotification = @"ZPDialogViewHideNotification";
NSString *CloseAuthenScreenNotification = @"CloseAuthenScreenNotification";
@interface NSError (Dialog)
- (DialogType)dialogType;
@end
@interface MMPopupWindow (Keyboard)
- (void)actionTap:(UITapGestureRecognizer*)gesture;
@end
@implementation NSError (Dialog)
- (DialogType)dialogType {
    if ([self isNetworkConnectionError]) {
        return DialogTypeNoInternet;
    }
    if ([self isRequestTimeout]) {
        return DialogTypeWarning;
    }
    if (self.code == ZALOPAY_ERRORCODE_MAINTAIN) {
        return DialogTypeNotification;
    }
    return DialogTypeError;
}

@end

@interface ZPDialogView()
@property (nonatomic, strong) UILabel     *detailLabel;
@property (nonatomic, strong) UITextField *inputView;
@property (nonatomic, strong) NSArray     *actionItems;
@property (nonatomic, assign) DialogType dialogType;
@end
@implementation ZPDialogView
static ZPDialogView *currentDialogView;
+ (id)showDialogWithError:(NSError *)error handle:(void (^)(void)) handle {
    NSArray *items;
    if (handle) {
        items = @[MMBoldItemMake([R string_ButtonLabel_Close], ^(NSInteger index) {
            if (handle) {
                handle();
            }
        })];
    }
    ZPDialogView *dialog = [[ZPDialogView alloc] initWithType:[error dialogType]
                                                      message:[error apiErrorMessage]
                                                        items:items];
    [dialog show];
    return dialog;
    
}

+ (instancetype)showDialogWithType:(DialogType)type
                           message:(NSString *)message
                 cancelButtonTitle:(NSString *)cancelButton
                  otherButtonTitle:(NSArray *)otherButton
                    completeHandle:(ZPDialogCompleteHandle)completeHandle {
    
    int cancelButtonIndex = (int)otherButton.count;
    MMPopupItemHandler handler = ^(NSInteger index) {
        if (completeHandle) {
            completeHandle((int)index, cancelButtonIndex);
        }
    };
    
    NSMutableArray *items = [NSMutableArray array];
    for (NSString *title in otherButton) {
        MMButtonItem *oneButton = MMBoldItemMake(title, handler);
        [items addObject:oneButton];
    }
    
    
    MMButtonItem *cancel = otherButton.count == 0 ? MMBoldItemMake(cancelButton, handler):
                                                     MMNormalItemMake(cancelButton, handler);
    [items addObject:cancel];
    
    ZPDialogView *dialog = [[ZPDialogView alloc] initWithType:type
                                                      message:message
                                                        items:items];
    [dialog show];
    return dialog;
}

+ (id)showDialogWithType:(DialogType)dialogType
                   title:(NSString *)title
                 message:(NSString *)message
            buttonTitles:(NSArray *)otherButtonTitles
                 handler:(MMPopupItemHandler)handler{
    
    NSMutableAttributedString *attributeMessage = [self attributeStringWithMessage:message];
    return [self showDialogWithType:dialogType
                              title:title
                   attributeMessage:attributeMessage
                       buttonTitles:otherButtonTitles
                            handler:handler];
}

+ (id)showDialogWithType:(DialogType)dialogType
                   title:(NSString *)title
        attributeMessage:(NSAttributedString *)message
            buttonTitles:(NSArray *)otherButtonTitles
                 handler:(MMPopupItemHandler)handler {
    
    NSMutableArray *items = [NSMutableArray array];
    
    for (NSString *title in otherButtonTitles) {
        if (![title isKindOfClass:[NSString class]]) {
            break;
        }
        if ([otherButtonTitles lastObject] == title) {
            MMButtonItem *oneButton = MMNormalItemMake(title, handler);
            [items addObject:oneButton];
        } else {
            MMButtonItem *oneButton = MMBoldItemMake(title, handler);
            [items addObject:oneButton];
        }
    }
    if (dialogType == DialogTypeNoInternet) {
        title = [ZPDialogView titleWithType:dialogType];
        message = [[NSAttributedString alloc] initWithString:[R string_NetworkError_NoConnectionMessage]];
    }
    ZPDialogView *dialogView = [[self alloc] initWithTitle:title
                                                 attributedMessage:message
                                                             items:items
                                                             type :dialogType];
    [dialogView show];    
    return dialogView;
}

+ (id)showDialogBoldFinalWithType:(DialogType)dialogType
                   title:(NSString *)title
        attributeMessage:(NSAttributedString *)message
            buttonTitles:(NSArray *)otherButtonTitles
                 handler:(MMPopupItemHandler)handler {
    
    NSMutableArray *items = [NSMutableArray array];
    
    for (NSString *title in otherButtonTitles) {
        if (![title isKindOfClass:[NSString class]]) {
            break;
        }
        if ([otherButtonTitles lastObject] == title) {
            MMButtonItem *oneButton = MMBoldItemMake(title, handler);
            [items addObject:oneButton];
        } else {
            MMButtonItem *oneButton = MMNormalItemMake(title, handler);
            [items addObject:oneButton];
        }
    }
    if (dialogType == DialogTypeNoInternet) {
        title = [ZPDialogView titleWithType:dialogType];
        message = [[NSAttributedString alloc] initWithString:[R string_NetworkError_NoConnectionMessage]];
    }
    ZPDialogView *dialogView = [[self alloc] initWithTitle:title
                                         attributedMessage:message
                                                     items:items
                                                     type :dialogType];
    [dialogView show];
    return dialogView;
}



- (void)hide {
    currentDialogView = nil;
    [super hide];
    [[NSNotificationCenter defaultCenter] postNotificationName:ZPDialogViewHideNotification object:nil];
}

+ (void)dismissAllDialog {
    if (currentDialogView) {
        [currentDialogView hide];
    }
}

+ (BOOL)isDialogShowing {
    return currentDialogView != nil;
}

- (void)showWithBlock:(MMPopupCompletionBlock)block {
    if (!currentDialogView) {
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(sendAction:
                                                                            to:
                                                                            from:
                                                                            forEvent:)]) {
            [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder)
                                                       to:nil
                                                     from:nil
                                                 forEvent:nil];
        }
        
        currentDialogView = self;
        [super showWithBlock:block];
    }
}
- (void)showAutoDismissAfter:(NSTimeInterval)time {
    [self showWithBlock:^(MMPopupView *view, BOOL finished) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(time * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [view hide];
        });
    }];
}


- (id)initWithType:(DialogType)dialogType
           message:(NSString *)message
             items:(NSArray *)items {
    if (!items || items.count == 0) {
        items = @[MMBoldItemMake([R string_ButtonLabel_Close], nil)];
    }
    return [self initWithTitle:[ZPDialogView titleWithType:dialogType]
                       message:message
                         items:items
                         type:dialogType];
}

- (id)initWithTitle:(NSString *)title
            message:(NSString *)message
              items:(NSArray *)items
              type:(DialogType)dialogType {
    NSMutableAttributedString *attributeString = [ZPDialogView attributeStringWithMessage:message];
    return [self initWithTitle:title attributedMessage:attributeString items:items type:dialogType];
}


- (id)initWithTitle:(NSString *)title
  attributedMessage:(NSAttributedString *)attributeString
              items:(NSArray *)items
              type:(DialogType)dialogType {
    self = [super init];
    if (self) {
        [self setupUI];
        self.contentView = [[UIView alloc] init];
        [self addSubview:self.contentView];
        [self.contentView setBackgroundColor:[UIColor clearColor]];
        [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(0);
            make.right.equalTo(0);
            make.top.equalTo(0);
            make.height.greaterThanOrEqualTo(30);
        }];
        self.actionItems = items;
        [self createBaseUIWithImage:dialogType
                              title:title
                   attributeMessage:attributeString];
        [self createButtonsView:self.contentView.mas_bottom items:items];
        
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.greaterThanOrEqualTo(260);
            make.width.lessThanOrEqualTo(CGRectGetWidth([UIScreen mainScreen].bounds) - 60);
            make.bottom.equalTo(self.buttonView.mas_bottom);
        }];
        
        [[[[MMPopupWindow sharedWindow] rac_signalForSelector:@selector(actionTap:)] takeUntil:self.rac_willDeallocSignal] subscribeNext:^(RACTuple * _Nullable x) {
            NSArray *windows = [UIApplication sharedApplication].windows;
            for (UIWindow *w in windows) {
                [w endEditing:YES];
            }
        }];
    }
    return self;
}

- (id)initUpgradeVersion:(NSString *)version
                 message:(NSString *)message
                  handle:(void (^)(void)) handle{
    
    self = [self initWithType:DialogTypeUpgrade
                      message:message
                        items:nil];
    
    if (self) {
        [self.imageView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(30);
        }];
        UILabel *label = [[UILabel alloc] init];
        [self addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.titleLabel);
            make.width.greaterThanOrEqualTo(50);
            make.height.equalTo(self.titleLabel);
            make.top.equalTo(self.titleLabel);
        }];
        label.text = [NSString stringWithFormat:@"V%@",version];
        [label setFont:[UIFont SFUITextMediumWithSize:16]];
        [label setTextColor:[UIColor zaloBaseColor]];
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.describeLabel.textAlignment = NSTextAlignmentLeft;
        self.actionItems = @[MMNormalItemMake(nil, ^(NSInteger index) {
            if (handle) {
                handle();
            }
        })];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont SFUITextRegularWithSize:16]];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageFromColor:[UIColor zaloBaseColor]]
                          forState:UIControlStateNormal];
        [button setTitle:[R string_ButtonLabel_OK] forState:UIControlStateNormal];
        [self.buttonView addSubview:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(30);
            make.center.equalTo(0);
            make.width.equalTo(self.buttonView.mas_width).multipliedBy(1.0/3.0);
        }];
        [button roundRect:15];
        [button addTarget:self action:@selector(actionButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

+ (NSMutableAttributedString *)attributeStringWithMessage:(NSString *)message {
    NSMutableAttributedString *attributedString = nil;
    if (message.length > 0) {
        attributedString = [[NSMutableAttributedString alloc] initWithString:message];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 3;
        paragraphStyle.alignment = NSTextAlignmentCenter;
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, message.length)];
    }
    return attributedString;
}

- (void)createBaseUIWithImage:(DialogType)type
                        title:(NSString *)title
             attributeMessage:(NSAttributedString *)attributedString{
    MASViewAttribute *lastAttribute = self.mas_top;
    if (type && type != DialogTypeNone) {
        self.imageView = [[ZPIconFontImageView alloc] init];
        [self addSubview:self.imageView];
        [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@20);
            make.centerX.equalTo(0);
            make.width.equalTo(@(60));
            make.height.equalTo(@(60));
        }];
        [self setIconFontForImage:type andImg:self.imageView];
        //self.imageView.image = image;
        
        lastAttribute = self.imageView.mas_bottom;
    }
    if (title) {
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.font = [UIFont SFUITextMediumWithSize:18];
        self.titleLabel.textColor = [UIColor defaultText];
        self.titleLabel.text = title;
        [self addSubview:self.titleLabel];
        self.titleLabel.numberOfLines = 0;
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            //make.left.equalTo(24);
            //make.right.equalTo(-24);
            make.top.equalTo(lastAttribute).offset(0);
            //make.height.equalTo(self.titleLabel.font.lineHeight);
            make.height.equalTo(5);
            make.width.equalTo(5);
            
        }];
        lastAttribute = self.titleLabel.mas_bottom;
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    if (attributedString.length > 0) {
        self.describeLabel = [self createMessageLabel:attributedString];
        [self addSubview:self.describeLabel];
        [self.describeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(24);
            make.right.equalTo(-24);
            make.top.equalTo(lastAttribute).offset(10);
            make.height.greaterThanOrEqualTo(self.describeLabel.font.lineHeight);
        }];
        lastAttribute = self.describeLabel.mas_bottom;
        
    }
    [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(lastAttribute);
    }];
    self.lastAttribute = lastAttribute;
}

- (UILabel *)createMessageLabel:(NSAttributedString *)att {
    UILabel *newLabel = [[UILabel alloc] init];
    newLabel.font = [UIFont SFUITextRegularWithSize:14];
    newLabel.textColor = [UIColor defaultText];
    newLabel.numberOfLines = 0;
    newLabel.attributedText = att;
    newLabel.textAlignment = NSTextAlignmentCenter;
    return newLabel;
}

- (void)setupUI {
    MMAlertViewConfig *config = [MMAlertViewConfig globalConfig];
    self.type = MMPopupTypeAlert;
    self.layer.cornerRadius = config.cornerRadius;
    self.clipsToBounds = YES;
    self.backgroundColor = config.backgroundColor;
    self.layer.borderWidth = MM_SPLIT_WIDTH;
    self.layer.borderColor = config.splitColor.CGColor;
}


+ (NSString *)titleWithType:(DialogType)type {
    NSString *title;
    switch (type) {
        case DialogTypeSuccess:
            title = [R string_Dialog_SuccessTitle];
            break;
        case DialogTypeInfo:
            title = nil;
            break;
        case DialogTypeError:
            title = [R string_Dialog_FailTitle];
            break;
        case DialogTypeUpgrade:
            title = [R string_Dialog_NewVersion];
            break;
        case DialogTypeNoInternet:
        case DialogTypeWarning:
            title = [R string_Dialog_Warning];
            break;
        case DialogTypeNotification:
            title = [R string_Dialog_Notification];
            break;
        case DialogTypeConfirm:
            title = [R string_Dialog_Confirm];
            break;
        default:
            title = nil;
            break;
    }
    return title;
}

- (void)setIconFontForImage:(DialogType)type andImg:(ZPIconFontImageView*)imgView{
    UIColor *color = [UIColor colorWithHexValue:0x008fe5];
    imgView.defaultView.font = [UIFont iconFontWithSize:60];
    [imgView setIconColor:color];
    
    NSString *nameIconFont = @"dialoge_notify";
    switch (type) {
        case DialogTypeSuccess:
            break;
        case DialogTypeInfo:
            nameIconFont = @"dialoge_infor";
            break;
        case DialogTypeError:
            nameIconFont = @"dialoge_warning";
            break;
        case DialogTypeUpgrade:
            break;
        case DialogTypeWarning:
            nameIconFont = @"dialoge_warning";
            break;
        case DialogTypeConfirm:
        case DialogTypeNotification:
            break;
        case DialogTypeNoInternet:
            nameIconFont = @"dialoge_connect";
            [self.imageView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(@26);
                make.centerX.equalTo(0);
                make.width.equalTo(@(80));
                make.height.equalTo(@(58));
            }];
            [imgView setIconColor:[UIColor colorWithHexValue:0xff7c00]];
            break;
        default:
            break;
    }
    [imgView setIconFont:nameIconFont];
}
- (void)createButtonsView:(MASViewAttribute *)lastAttribute
                    items:(NSArray *)items{
    MMAlertViewConfig *config = [MMAlertViewConfig globalConfig];
    self.buttonView = [UIView new];
    [self addSubview:self.buttonView];
    [self.buttonView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lastAttribute).offset(25);
        make.left.right.equalTo(self);
        make.height.greaterThanOrEqualTo(config.buttonHeight);
    }];
    self.buttonView.layer.borderWidth = MM_SPLIT_WIDTH;
    self.buttonView.layer.borderColor = config.splitColor.CGColor;
    if (items.count == 0) {
        return;
    }
    
    __block UIButton *firstButton = nil;
    __block UIButton *lastButton = nil;
    
    for ( NSInteger i = 0 ; i < items.count; ++i ) {
        MMButtonItem *item = items[i];
        
        UIButton *btn = [UIButton mm_buttonWithTarget:self action:@selector(actionButton:)];
        [self.buttonView addSubview:btn];
        btn.tag = i;
        
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            
            if ( items.count <= 2 ) {
                make.top.bottom.equalTo(self.buttonView);
                make.height.mas_equalTo(config.buttonHeight);
                
                if ( !firstButton ) {
                    firstButton = btn;
                    make.left.equalTo(self.buttonView.mas_left).offset(-MM_SPLIT_WIDTH);
                } else {
                    make.left.equalTo(lastButton.mas_right).offset(-MM_SPLIT_WIDTH);
                    make.width.equalTo(firstButton);
                }
            } else {
                make.left.right.equalTo(self.buttonView);
                make.height.mas_equalTo(config.buttonHeight);
                
                if ( !firstButton ) {
                    firstButton = btn;
                    make.top.equalTo(self.buttonView.mas_top).offset(-MM_SPLIT_WIDTH);
                } else {
                    make.top.equalTo(lastButton.mas_bottom).offset(-MM_SPLIT_WIDTH);
                    make.width.equalTo(firstButton);
                }
            }
            
            lastButton = btn;
        }];
        
        [btn setTitle:item.title forState:UIControlStateNormal];
        [btn setTitleColor:item.color forState:UIControlStateNormal];
        [btn.titleLabel setFont:item.font];
        [btn setBackgroundImage:[UIImage mm_imageWithColor:self.backgroundColor] forState:UIControlStateNormal];
        [btn setBackgroundImage:[UIImage mm_imageWithColor:config.itemPressedColor] forState:UIControlStateHighlighted];
    }
    
    [lastButton mas_updateConstraints:^(MASConstraintMaker *make) {
        
        if ( items.count <= 2 ) {
            make.right.equalTo(self.buttonView.mas_right).offset(MM_SPLIT_WIDTH);
        } else {
            make.bottom.equalTo(self.buttonView.mas_bottom).offset(MM_SPLIT_WIDTH);
        }
        
    }];
}

- (void)actionButton:(UIButton*)btn {
    MMPopupItem *item = self.actionItems[btn.tag];
    
    if ( item.disabled ) {
        return;
    }
    
    if ( self.withKeyboard && (btn.tag == 1) ) {
        if ( self.inputView.text.length > 0 ) {
            [self hide];
        }
    } else {
        [self hide];
    }
    
    if ( item.handler ) {
        item.handler(btn.tag);
    }
}

@end
@implementation MMButtonItem
@end
