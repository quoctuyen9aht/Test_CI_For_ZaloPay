//
//  BasePageViewController.h
//  ZaloPayUI
//
//  Created by vuongvv on 5/16/18.
//  Copyright © 2018 VNG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+BaseViewController.h"

@interface BasePageViewController : UIPageViewController<UIGestureRecognizerDelegate, UINavigationControllerDelegate>
@property (assign, nonatomic) BOOL isAppear;
- (BOOL)isAllowSwipeBack;
- (NSString*)screenName;
@end
