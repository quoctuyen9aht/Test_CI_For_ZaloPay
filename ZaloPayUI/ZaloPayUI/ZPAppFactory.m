//
//  ZPAppFactory.m
//  ZaloPayCommon
//
//  Created by Nguyễn Hữu Hoà on 7/2/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPAppFactory.h"
#import <ZaloPayAnalyticsSwift/ZaloPayAnalyticsSwift-Swift.h>

@interface ZPAppFactory()
@property (nonatomic, strong, readwrite) ZPRPManager *rpManager;
@end

@implementation ZPAppFactory

+ (instancetype)sharedInstance {
    static ZPAppFactory *_sharedInstance = nil;
    static dispatch_once_t once_token = 0;
    dispatch_once(&once_token, ^{
        if (_sharedInstance == nil) {
            _sharedInstance = [[ZPAppFactory alloc] init];
        }
    });
    return _sharedInstance;
}

- (void)registerRedPacketManager:(ZPRPManager *)rpManager {
    self.rpManager = rpManager;
}

- (void)registerOrderTracking:(ZPAOrderTracking *)orderTracking {
    self.orderTracking = orderTracking;
}

+ (UIViewController*) topMostController {
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    return topController;
}
@end
