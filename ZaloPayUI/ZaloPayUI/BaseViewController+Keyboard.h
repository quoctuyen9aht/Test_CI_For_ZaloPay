//
//  BaseViewController+Keyboard.h
//  ZaloPayCommon
//
//  Created by bonnpv on 6/9/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "BaseViewController.h"

typedef void (^ShowHandle)(CGSize keyboardSize, float animationduration);
typedef void (^HideHandle)(float animationduration);
@interface BaseViewController (Keyboard)
- (void)handleKeyboardShow:(ShowHandle) showHandle
                   andHide:(HideHandle)hideHandle;
@end
