//
//  BaseViewcontrollerSwift.swift
//  ZaloPayUI
//
//  Created by thi la on 6/19/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import Foundation
import CocoaLumberjackSwift
import ZaloPayUIObjc

open class BaseViewcontrollerSwift: UIViewController, UIGestureRecognizerDelegate {
    open var isAppear: Bool?
    
    open func isAllowSwipeBack() -> Bool {
        return true
    }
    
    open func screenName() -> String {
        return ""
    }
    
    // MARK: Rotate
    override open var shouldAutorotate: Bool {
        return false
    }
    
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override open var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc open func backButtonClicked(_ sender: Any) {
        self.viewWillSwipeBack()
        guard let navi = self.navigationController else {
            return
        }
        if navi.viewControllers.count == 1 {
            navi.dismiss(animated: true, completion: nil)
        } else {
            navi.popViewController(animated: true)
        }
    }
    
    deinit {
        DDLogInfo("-[\(String(describing: type(of: self))) dealloc]")
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        // [self baseViewControllerViewDidLoad];
        self.setupUI()
    }
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        let selector = #selector(getter: UINavigationController.interactivePopGestureRecognizer)
        guard let navi = self.navigationController else {
            return false
        }
        if navi.responds(to: selector) && gestureRecognizer == self.navigationController?.interactivePopGestureRecognizer {
            return false
        }
        return self.isAllowSwipeBack()
    }
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return (gestureRecognizer as? UIScreenEdgePanGestureRecognizer) != nil
    }
    
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard let navi = self.navigationController else {
            return
        }
        if navi.viewControllers.count <= 1 {
            navi.interactivePopGestureRecognizer?.delegate = nil
            navi.interactivePopGestureRecognizer?.isEnabled = false
            return
        }
        // setup delegate swipe back
        navi.interactivePopGestureRecognizer?.isEnabled = self.isAllowSwipeBack()
        let selector = #selector(getter: UINavigationController.interactivePopGestureRecognizer)
        if navi.responds(to: selector) {
            navi.interactivePopGestureRecognizer?.delegate = self
        }
        
    }
    
    
    public func viewWillSwipeBack() {
        
    }
    public func backEventId() -> Int {
        return 0
    }
    
    public func backButtonIcon() -> String {
        return "general_backios"
    }
    
    public func isEnableAuthenticationView() -> Bool {
        return true
    }
    
    private func setupUI() {
        self.view.backgroundColor = UIColor.defaultBackground()
        self.setupUIBar()
        self.automaticallyAdjustsScrollViewInsets = false
        self.edgesForExtendedLayout = .all
        self.navigationController?.navigationBar.isTranslucent = false
        UIApplication.shared.setStatusBarStyle(.lightContent, animated: true)
        if let view = self.navigationController?.navigationBar {
            let imageView: UIImageView? = self.findHairlineImageViewUnder(view: view)
            imageView?.isHidden = true
        }
        UINavigationBar.appearance().shadowImage = UIImage()
        
    }

    func setupUIBar() {
        guard let navi = self.navigationController, navi.viewControllers.count > 1 else {
            return
        }
        navi.isNavigationBarHidden = false
        self.navigationItem.leftBarButtonItems = self.leftBarButtonItems()
       
    }
    
    func findHairlineImageViewUnder(view: UIView) -> UIImageView? {
        if let imageView = view as? UIImageView, view.bounds.size.height <= 1.0 {
            return imageView
        }
        for subview in view.subviews {
            if let imgView = self.findHairlineImageViewUnder(view: subview) {
                return imgView
            }
        }
        return nil
    }
    
    public func leftBarButtonItems() -> Array<UIBarButtonItem> {
        let backButton = UIButton(type: .custom)
        backButton.setIconFont(self.backButtonIcon(), for: .normal)
        backButton.setTitleColor(.white, for: .normal)
        backButton.titleLabel?.font = UIFont.iconFont(withSize: 20)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.addTarget(self, action: #selector(backButtonClicked(_:)), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(trackEventBack), for: .touchUpInside)
        let backBarButtonItem = UIBarButtonItem(customView: backButton)
        if #available(iOS 11.0, *) {
            return  [backBarButtonItem]
        } else {
            let negativeSeparator = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
            negativeSeparator.width = -12
            return [negativeSeparator, backBarButtonItem]
        }
    }
    
    public func trackEventBack() {
        let backEventId = self.backEventId()
        if backEventId == 0 {
            return
        }
        ZPAppFactory.sharedInstance().trackBackButtonClickEventId(backEventId)
    }
    
    
}




