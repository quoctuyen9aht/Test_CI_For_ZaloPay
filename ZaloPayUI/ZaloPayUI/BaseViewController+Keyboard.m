//
//  BaseViewController+Keyboard.m
//  ZaloPayCommon
//
//  Created by bonnpv on 6/9/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "BaseViewController+Keyboard.h"
#import <ReactiveObjC/ReactiveObjC.h>

@implementation BaseViewController (Keyboard)

- (void)handleKeyboardShow:(ShowHandle) showHandle
                   andHide:(HideHandle)hideHandle {
    
    RACSignal *untilSignal = [self rac_willDeallocSignal];
    
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillShowNotification object:nil] takeUntil:untilSignal]  subscribeNext:^(NSNotification *notification) {
        if (showHandle) {
            CGSize beginFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
            CGSize endFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
            CGSize keyboardSize = endFrame.height > beginFrame.height ? endFrame  : beginFrame;
            
            NSDictionary *info = [notification userInfo];
            NSNumber *number = [info objectForKey:UIKeyboardAnimationDurationUserInfoKey];
            double duration = [number doubleValue];
            showHandle(keyboardSize,duration);
        }
    }];
    
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillHideNotification object:nil] takeUntil:untilSignal]  subscribeNext:^(NSNotification *notification) {
        if (hideHandle) {
            NSDictionary *info = [notification userInfo];
            NSNumber *number = [info objectForKey:UIKeyboardAnimationDurationUserInfoKey];
            double duration = [number doubleValue];
            hideHandle(duration);
        }
    }];
}
@end
