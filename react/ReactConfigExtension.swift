//
//  ReactConfigExtension.swift
//  ZaloPay
//
//  Created by bonnpv on 9/27/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation

@objc public enum ReactConfigType : Int {
    case DevInternal = 1
    case DevExternal = 2
    case Release = 3
}

@objc public  enum AppEvironment : Int {
    case Sandbox = 1
    case Staging = 2
    case Production = 3
}

public extension ReactConfig {
    public class func setConfigType(_ type: ReactConfigType) {
        typealias ReactType = ReactConfig
        var config: ReactType.Type
        switch type {
        case .DevInternal:
            config = ReactDevInternalConfig.self
        case .DevExternal:
            config = ReactDevExternalConfig.self
        default:
            config = ReactReleaseConfig.self
        }
        setConfig(config.init())
    }
}

class ReactDevInternalConfig : ReactConfig {
    override func internalBundle() -> URL? {
        return URL(string: "http://localhost:8081/index.ios.bundle?pl!atform=ios")
    }    
    override func externalBundle(_ appId: Int) -> URL? {
        return ReactNativeAppManager.sharedInstance().jsBundle(withAppId: appId)
    }
}

class ReactDevExternalConfig : ReactConfig {
    override func internalBundle() -> URL? {
        return ReactNativeAppManager.sharedInstance().jsBundle(withAppId: reactInternalAppId)
    }
    override func externalBundle(_ appId: Int) -> URL? {
        return URL(string: "http://localhost:8081/index.ios.bundle?pl!atform=ios")
    }    
}

class ReactReleaseConfig : ReactConfig {
    override func internalBundle() -> URL? {
        return externalBundle(reactInternalAppId)
    }
    override func externalBundle(_ appId: Int) -> URL? {
        return ReactNativeAppManager.sharedInstance().jsBundle(withAppId: appId)
    }    
}

