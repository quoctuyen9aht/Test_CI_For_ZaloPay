//
//  ReactDevExternalConfig.m
//  ZaloPay
//
//  Created by bonnpv on 5/19/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ReactDevExternalConfig.h"
#import <ZaloPayAppManager/PaymentReactNativeApp/ReactNativeAppManager+DownloadData.h>


@implementation ReactDevExternalConfig
- (NSURL *)internalBundle {
    //return  [NSURL URLWithString:@"http://localhost:8081/index.ios.bundle?platform=ios"];
   return [[ReactNativeAppManager sharedInstance] jsBundleWithAppId:reactInternalAppId];
}

- (NSURL *)externalBundle:(NSInteger)appId {
    return  [NSURL URLWithString:@"http://localhost:8081/index.ios.bundle?platform=ios"];
//return [[ReactNativeAppManager sharedInstance] jsBundleWithAppId:appId];
//    return  [NSURL URLWithString:@"http://10.42.0.1:8081/index.ios.bundle?platform=ios"];
//      return  [NSURL URLWithString:@"http://192.168.23.102:8081/index.ios.bundle?platform=ios"];
}

@end
