import urllib2
import os
import sys
import time

TIMEOUT = 5

def get_build_console_for_sha1(url, sha1):
    return '{}/scm/bySHA1/{}/consoleText'.format(url, sha1)


def parse_console(build_con):
    retcode = 0

    if build_con:
        total_read_lines = 0
        finished_line = None

        while not finished_line:
            current_read_lines = 0
            try:
                for line in urllib2.urlopen(build_con, 
                                             None, TIMEOUT).readlines():
                    current_read_lines += 1
                    if current_read_lines >= total_read_lines:
                        print line,
                        total_read_lines += 1
                        sys.stdout.flush()
                        if line.startswith('Finished: '):
                            finished_line = line
            except urllib2.HTTPError as e:
                if e.code == 404:
                    time.sleep(2)
                    pass

        if 'FAILURE' in finished_line or 'UNSTABLE' in finished_line:
            retcode = 1

    print "retcode = ", retcode
    return retcode


def main():
    URL = 'http://localhost:8080/project/ZaloPay_1_sandboxdebug'
    sha1 = os.getenv('CI_BUILD_REF')
    bc = get_build_console_for_sha1(URL, sha1)
    return parse_console(bc)

import subprocess, threading

class Command(object):
    def __init__(self, cmd):
        self.cmd = cmd
        self.process = None

    def run(self, timeout):
        def target():
            self.process = subprocess.Popen(self.cmd, shell=True)
            self.process.communicate()

        thread = threading.Thread(target=target)
        thread.start()

        thread.join(timeout)
        if thread.is_alive():
            self.process.terminate()
            thread.join()
        return self.process.returncode

# command = Command("echo 'Process started'; sleep 2; echo 'Process finished'")
# command.run(timeout=3)
# command.run(timeout=1)
def execute(cmd, timeout):
    p = Command(cmd)
    code = p.run(timeout = timeout)
    if code != 0:
        sys.exit(code)


if __name__ == '__main__' :
    sha1 = os.getenv('CI_BUILD_REF')
    print("Starting build\n")
    print("CI_BUILD_REF: %s\n" % sha1)
    sys.stdout.flush()

    execute("git submodule update --init --recursive", 20*60)
    print("Update pod\n")
    execute("pod install > /dev/null", 10*60)
    xcodebuild_flags =  '-scheme ZaloPay -sdk iphoneos9.3 -workspace ZaloPay.xcworkspace -configuration Release build "CODE_SIGN_IDENTITY=iPhone Developer: HUU HOA NGUYEN (EA55ZC9YEE)"'
    xcodebuild = "/usr/bin/xcodebuild %s | /usr/local/bin/xcpretty && exit ${PIPESTATUS[0]}" % xcodebuild_flags
    execute(xcodebuild, None)
    sys.exit(0)
    # sys.exit(main())
