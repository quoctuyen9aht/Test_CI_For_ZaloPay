//
//  ZaloPayDatabaseLog.h
//  ZaloPayDatabase
//
//  Created by bonnpv on 6/8/16.
//  Copyright © 2016 bonnpv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CocoaLumberjack/CocoaLumberjack.h>

extern const DDLogLevel zaloPayDatabaseLogLevel;
#define ddLogLevel  zaloPayDatabaseLogLevel
