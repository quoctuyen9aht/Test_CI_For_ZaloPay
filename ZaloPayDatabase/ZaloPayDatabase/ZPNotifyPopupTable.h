//
//  ZPNotifyPopupTable.h
//  ZaloPayCommon
//
//  Created by bonnpv on 6/16/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPBaseTable.h"

typedef enum {
    NotificationPopupStateUnread = 1,
    NotificationPopupStateView = 2,
    NotificationPopupStateRead = 3,
} NotificationPopupState;


@interface ZPNotifyPopupTable : ZPBaseTable
- (BOOL)addNotifyPopupWithTranId:(u_int64_t)tranid
                       timestamp:(u_int64_t)timestamp
                           appid:(int)appid
                         message:(NSString *)message
                       transtype:(int)transtype
                          userid:(NSString *)userid
                      destuserid:(NSString *)destuserid
                        isUnread:(BOOL)unread
                      notifyType:(int)notifyType
                       embeddata:(NSDictionary *)data
                           mtaid:(uint64_t)mtaid
                           mtuid:(uint64_t)mtuid
                            area:(int)area;

- (NSArray *)getListNotifyPopupMessageFrom:(long)index count:(long)count;

- (NSArray *)getListNotifyPopupMessageUnRead;

- (NSDictionary *)getNotificationPopupWithMtaid:(uint64_t)mtaid
                                          mtuid:(uint64_t)mtuid;

- (NSArray *)getListNotificationPopupWithLixi:(NSArray *)arrMtaid arrMtuid:(NSArray *)arrMtuid;

- (void)updateStateReadWithNotificationPopupId:(NSString *)notificationid;

- (void)updateStateReadNotifyPopUpWithMtaid:(uint64_t)mtaid mtuid:(uint64_t)mtuid;

- (BOOL)addLixiMessagePopupWithUserId:(NSString *)userid
                           destuserid:(NSString *)destuserid
                              message:(NSString *)message
                             bundleid:(uint64_t)bundleid
                              package:(uint64_t)packageid
                          liximessage:(NSString *)liximessage
                            avatarUrl:(NSString *)avatarUrl
                                 name:(NSString *)name
                            timestamp:(uint64_t)timestamp
                     notificationtype:(int)noticationtype
                               unread:(BOOL)unread
                                mtaid:(uint64_t)mtaid
                                mtuid:(uint64_t)mtuid
                                 area:(int)area;

- (int)countUnreadNotifyPopup;

- (BOOL)deleteNotificationPopupWithId:(NSString *)notificationId;

- (BOOL)deleteAlllNotificationPopup;

- (void)clearNotificationPopupCount;

- (uint64_t)NotificationPopupMinTimestamp;

- (NSDictionary *)merchantBillNotificaionPopup:(NSString *)notificationId;

- (NSString *)notificaionPopupIdFromMtaid:(uint64_t)mtaid mtuid:(uint64_t)mtuid;
@end

