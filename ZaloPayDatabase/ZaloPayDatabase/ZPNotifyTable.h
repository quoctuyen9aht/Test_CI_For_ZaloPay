//
//  ZPNotifyTable.h
//  ZaloPayCommon
//
//  Created by bonnpv on 6/16/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPBaseTable.h"

typedef enum {
    NotificationStateUnread = 1,
    NotificationStateView = 2,
    NotificationStateRead = 3,
} NotificationState;

@interface ZPNotifyTable : ZPBaseTable
- (BOOL)addNotifyWithTranId:(u_int64_t)tranid
                  timestamp:(u_int64_t)timestamp
                      appid:(int)appid
                    message:(NSString *)message
                  transtype:(int)transtype
                     userid:(NSString *)userid
                 destuserid:(NSString *)destuserid
                   isUnread:(BOOL)unread
                 notifyType:(int)notifyType
                  embeddata:(NSDictionary *)data
                      mtaid:(uint64_t)mtaid
                      mtuid:(uint64_t)mtuid
                       area:(int)area;

- (NSArray *)getListNotifyMessageFrom:(long)index count:(long)count;

- (NSDictionary *)getNotificationWithMtaid:(uint64_t)mtaid
                                     mtuid:(uint64_t)mtuid;

- (NSArray *)getListNotificationWithLixi:(NSArray *)arrMtaid arrMtuid:(NSArray *)arrMtuid;

- (void)updateStateReadWithNotificationId:(NSString *)notificationid;

- (void)updateStateReadWithMtaid:(uint64_t)mtaid mtuid:(uint64_t)mtuid;

- (BOOL)addLixiMessageWithUserId:(NSString *)userid
                      destuserid:(NSString *)destuserid
                         message:(NSString *)message
                        bundleid:(uint64_t)bundleid
                         package:(uint64_t)packageid
                     liximessage:(NSString *)liximessage
                       avatarUrl:(NSString *)avatarUrl
                            name:(NSString *)name
                       timestamp:(uint64_t)timestamp
                notificationtype:(int)noticationtype
                          unread:(BOOL)unread
                           mtaid:(uint64_t)mtaid
                           mtuid:(uint64_t)mtuid
                            area:(int)area;

- (int)countUnreadNotify;

- (BOOL)deleteNotificationWithId:(NSString *)notificationId;

- (BOOL)deleteAlllNotification;

- (void)clearNotificationCount;

- (uint64_t)notificationMinTimestamp;

- (NSDictionary *)merchantBill:(NSString *)notificationId;

- (NSString *)notificaionIdFromMtaid:(uint64_t)mtaid mtuid:(uint64_t)mtuid;
@end
