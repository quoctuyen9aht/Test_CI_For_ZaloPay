//
//  PerUserDataBase.m
//  ZaloPayDatabase
//
//  Created by bonnpv on 6/3/16.
//  Copyright © 2016 bonnpv. All rights reserved.
//

#import "PerUserDataBase.h"
#import "PerUserDataBase+External.h"
#import <fmdb/FMDatabase.h>
#import <fmdb/FMDatabaseQueue.h>
#import "ZaloPayDatabaseLog.h"
#import <ReactiveObjC/ReactiveObjC.h>
#import "ZPTableProtocol.h"

extern NSString *const kPerUserDataBaseScheme;

@interface PerUserDataBase ()
@property(nonatomic, strong) NSString *userId;
@end

@implementation PerUserDataBase

+ (instancetype)sharedInstance {
    static PerUserDataBase *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[PerUserDataBase alloc] init];
        instance.currentScheme = kPerUserDataBaseScheme;
    });
    return instance;
}

- (BOOL)createDBWithUser:(NSString *)userId oldUser:(NSString *)oldUid {
    if ([userId isEqualToString:self.userId]) {
        return FALSE;
    }
    if (oldUid && ![userId isEqualToString:oldUid]) {
        [self closeDataBase];
        NSString *oldPath = [self databasePathWithUid:oldUid];
        [[NSFileManager defaultManager] removeItemAtPath:oldPath error:nil];
    }

    self.userId = userId;
    return [self createDBWithConfigVersion];
}

- (NSString *)databasePathWithUid:(NSString *)uid {
    NSString *name = [self dataBaseNameWithUid:uid];
    NSString *path = [self buildDatabasePathWithName:name];
    return path;
}

- (BOOL)createDBWithConfigVersion {

    NSString *filePath = [self databasePath];
    if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        [self createDataBaseAtPath:filePath];
        return TRUE;
    }

    [self loadDatabaseWithPath:filePath];
    NSString *dbScheme = [self dbScheme];
    NSString *currentCheme = [self currentScheme];

    if ([dbScheme isEqualToString:currentCheme]) {
        return FALSE;
    }

//    [self dropAllTableWithoutTableNames:[NSSet setWithArray:@[@"Scheme", @"RecentTransferFriend", @"CacheData", @"ZPC"]]];
//    [self removeAllCacheDataButKeepDataWithKey:kUsingTouchID];
    [self dropAllTablesCanBeDropped];
    [self createAllTable];
    [self updateScheme:[self currentScheme]];
    return TRUE;
}

- (NSMutableSet <NSString *> *)allTables {
    NSMutableSet <NSString *> *result = [NSMutableSet new];
    [self.dbQueue inDatabase:^(FMDatabase *_Nonnull db) {
        FMResultSet *rs = [db executeQuery:@"SELECT name FROM sqlite_master WHERE type='table'", nil];
        while ([rs next]) {
            NSString *table = [rs stringForColumnIndex:0];
            if (table.length > 0) {
                [result addObject:table];
            }
        }
        [rs close];
    }];
    return result;
}

- (void)dropAllTables {
    NSSet* tableNames = [self allTables];
    if ([tableNames count] == 0) {
        return;
    }
    
    [self.dbQueue inDatabase:^(FMDatabase *_Nonnull db) {
        for (NSString *tableName in tableNames) {
            NSString *sql = [NSString stringWithFormat:@"DROP TABLE IF EXISTS %@", tableName];
            if (![db executeUpdate:sql]) {
                DDLogInfo(@"No delete table : %@!!!!!!!!!!!!!", tableName);
            }
        }
    }];
}

- (void)dropAllTablesCanBeDropped {
    NSSet* tables = [self getTables];
    if ([tables count] == 0) {
        return;
    }
    
    [self.dbQueue inDatabase:^(FMDatabase *_Nonnull db) {
        for (Class<ZPTableProtocol> table in tables) {
            [table dropTable:db];
        }
    }];
}

- (NSString *)dataBaseName {
    return [self dataBaseNameWithUid:self.userId];
}

- (NSString *)dataBaseNameWithUid:(NSString *)uid {
    return [NSString stringWithFormat:@"ZaloPayPrivate_%@", uid];
}

- (NSString *)currentScheme {
    return _currentScheme;
}

- (void)createAllTable:(FMDatabase *)db {
    NSSet* tables = [self getTables];
    if ([tables count] == 0) {
        return;
    }
    
    for (Class<ZPTableProtocol> table in tables) {
        [table createTable:db];
    }
    
//    [self createTableManifest:db];
//    [self createTableTransactionLog:db];
//    [self createTableProfileInfomation:db];
//    [self createTableNotify:db];
//    [self createTableCacheData:db];
//    [self createTableZaloFriend:db];
//    [self createTableRecentTransfer:db];
//    [self createLixiNotificationTable:db];
//    [self createTableMerchantUserInfor:db];
//    [self createTableUserContact:db];
//    [self createTableZaloPayInfo:db];
//    [self createTableTrackingAppTransid:db];
//    [self createTableAppTranIdStep:db];
//    [self createTableAppTranIdAPI:db];
//    [self createTableUnSavePhoneContactInfo:db];
//    [self createTableSaveModeKeyboard:db];
//    [self createTableNotifyPopup:db];
}

- (void)closeDataBase {
    [super closeDataBase];
    self.userId = nil;
}

@end
