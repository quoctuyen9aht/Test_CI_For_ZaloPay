//
//  ZPTransactionLogTable.h
//  ZaloPayAppManager
//
//  Created by PhucPv on 5/7/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

@class FMDatabase;

#import <UIKit/UIKit.h>
#import "ZPBaseTable.h"

@interface ZPTransactionLogTable : ZPBaseTable

- (void)updateTransactionLogs:(NSArray *)transactionLogs
                   statusType:(int)statustype
                 keyTimestamp:(NSTimeInterval)keyTimestamp
                        order:(int)order;

- (NSArray *)loadTransactionLogWithId:(NSString *)transId;

- (NSTimeInterval)minTimestampFragmentWithKey:(NSTimeInterval)key
                                   statusType:(int)statusType;

- (NSTimeInterval)maxTimestampFragmentWithKey:(NSTimeInterval)keyTimestamp
                                   statusType:(int)statusType;

- (void)updateRetrySuccessTransaction:(uint64_t)tranid;

- (NSArray *)transactionWithStatusType:(int)statusType
                            transTypes:(NSArray *)transTypes
                          keyTimestamp:(NSTimeInterval)keyTimestamp
                                  sign:(int)sign
                                offset:(int)offset
                                 count:(int)count;

- (void)updateFragmentWithKey:(NSTimeInterval)key
                          max:(NSTimeInterval)maxTimestamp
                          min:(NSTimeInterval)minTimestamp
                   statusType:(int)statusType
                        order:(int)order;

- (void)cleanDB;

- (void)setOutOfDataWithTimestamp:(NSTimeInterval)timestamp
                       statusType:(int)statusType;

- (BOOL)outOfDataWithTimestamp:(NSTimeInterval)timestamp
                    statusType:(int)statusType;

- (BOOL)removeTransactionWithId:(NSString *)transid;

- (BOOL)updateThankMessageWithTransId:(NSString *)transid
                        encodeMessage:(NSString *)encodeMessage;
@end
