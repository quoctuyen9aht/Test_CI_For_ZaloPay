//
//  FMResultSet+ZaloPay.h
//  ZaloPayDatabase
//
//  Created by bonnpv on 4/2/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <fmdb/FMDB.h>

@interface FMResultSet (ZaloPay)
- (NSString *)stringForColumn:(NSString *)columnName defaultValue:(NSString *)defaultValue;

- (int)intForColumn:(NSString *)columnName defaultValue:(int)defaultValue;
@end
