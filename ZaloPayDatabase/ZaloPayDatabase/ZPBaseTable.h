//
//  ZPBaseTable.h
//  ZaloPayDatabase
//
//  Created by tridm2 on 3/21/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import "ZPPaymentDatabase.h"
#import "ZPTableProtocol.h"


@interface ZPBaseTable : NSObject<ZPTableProtocol>
@property (nonatomic, weak) ZPPaymentDatabase* db;

- (id) initWithDB:(ZPPaymentDatabase*) db;

// Override by subclass
+ (NSArray<NSString*>*) creatingTableQuery;
+ (NSArray<NSString*>*) tableNamesCanBeDropped;
@end
