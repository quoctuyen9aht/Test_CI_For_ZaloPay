//
//  PaymentDatabase.h
//  ZaloPayCommon
//
//  Created by bonnpv on 6/3/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>


@class FMDatabaseQueue;
@class FMDatabase;

@interface PaymentDatabase : NSObject
@property(nonatomic, strong) FMDatabaseQueue *dbQueue;

- (NSString *)dbScheme;

- (NSString *)databasePath;

- (void)createDataBaseAtPath:(NSString *)filePath;

- (void)loadDatabaseWithPath:(NSString *)filePath;

- (BOOL)createDBWithConfigVersion;

- (void)closeDataBase;

- (NSString *)buildDatabasePathWithName:(NSString *)name;

- (void)createAllTable;

- (void)updateScheme:(NSString *)scheme;

- (void)encryptionDB:(NSString *)DBPath;
@end
