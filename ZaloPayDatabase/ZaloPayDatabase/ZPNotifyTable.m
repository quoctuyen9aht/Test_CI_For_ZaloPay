//
//  ZPNotifyTable.m
//  ZaloPayCommon
//
//  Created by bonnpv on 6/16/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPNotifyTable.h"
#import <ZaloPayCommon/NSCollection+JSON.h>
#import <fmdb/FMDatabase.h>
#import <fmdb/FMDatabaseQueue.h>

#define kNotifyMessage  @"NotifyMessage"
#define kTransid        @"transid"
#define kTimestamp      @"timestamp"
#define kAppid          @"appid"
#define kMessage        @"message"
#define kTranstype      @"transtype"
#define kUserid         @"userid"
#define kDestuserid     @"destuserid"
#define kNotificationState  @"notificationstate"
#define kNotificationType   @"notificationtype"
#define kEmbedData          @"embeddata"
#define kMtaid          @"mtaid"
#define kMtuid          @"mtuid"
#define kArea           @"area"


@implementation ZPNotifyTable

+ (NSArray<NSString*>*) creatingTableQuery {
    NSString* tableNotifyMessage = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@(%@ long, %@ long, %@ int, %@ text, %@ int, %@ text, %@ text, %@ int, %@ int, %@ text, %@ long, %@ long , %@ long,PRIMARY KEY ( %@, %@ ));", kNotifyMessage, kTransid, kTimestamp, kAppid, kMessage, kTranstype, kUserid, kDestuserid, kNotificationState, kNotificationType, kEmbedData, kMtaid, kMtuid, kArea, kMtaid, kMtuid];
    return @[tableNotifyMessage];
}

+ (NSArray<NSString*>*) tableNamesCanBeDropped {
    return @[kNotifyMessage];
}

- (BOOL)addLixiMessageWithUserId:(NSString *)userid
                      destuserid:(NSString *)destuserid
                         message:(NSString *)message
                        bundleid:(uint64_t)bundleid
                         package:(uint64_t)packageid
                     liximessage:(NSString *)liximessage
                       avatarUrl:(NSString *)avatarUrl
                            name:(NSString *)name
                       timestamp:(uint64_t)timestamp
                notificationtype:(int)noticationtype
                          unread:(BOOL)unread
                           mtaid:(uint64_t)mtaid
                           mtuid:(uint64_t)mtuid
                            area:(int)area {

    NSMutableDictionary *extra = [NSMutableDictionary dictionary];
    if (liximessage == nil) {
        liximessage = @"";
    }
    if (avatarUrl == nil) {
        avatarUrl = @"";
    }
    if (name == nil) {
        name = @"";
    }

//    lixi.packageId = [embedDic uint64ForKey:@"packageid"];
//    lixi.bundleId = [embedDic uint64ForKey:@"bundleid"];
//    lixi.avatarUrl = [embedDic stringForKey:@"avatar"];
//    lixi.name = [embedDic stringForKey:@"name"];
//    lixi.lixiMessage = [embedDic stringForKey:@"liximessage"];

    [extra setObject:liximessage forKey:@"liximessage"];
    [extra setObject:[NSString stringWithFormat:@"%@", @(bundleid)] forKey:@"bundleid"];
    [extra setObject:[NSString stringWithFormat:@"%@", @(packageid)] forKey:@"packageid"];
    [extra setObject:avatarUrl forKey:@"avatarurl"];
    [extra setObject:name forKey:@"name"];

    return [self addNotifyWithTranId:0
                           timestamp:timestamp
                               appid:0
                             message:message
                           transtype:0
                              userid:userid
                          destuserid:destuserid
                            isUnread:unread
                          notifyType:noticationtype
                           embeddata:extra
                               mtaid:mtaid
                               mtuid:mtuid
                                area:area];
}

- (BOOL)addNotifyWithTranId:(u_int64_t)tranid
                  timestamp:(u_int64_t)timestamp
                      appid:(int)appid
                    message:(NSString *)message
                  transtype:(int)transtype
                     userid:(NSString *)userid
                 destuserid:(NSString *)destuserid
                   isUnread:(BOOL)unread
                 notifyType:(int)notifyType
                  embeddata:(NSDictionary *)data
                      mtaid:(uint64_t)mtaid
                      mtuid:(uint64_t)mtuid
                       area:(int)area {
    NotificationState state = unread == TRUE ? NotificationStateUnread : NotificationStateRead;
    NSString *embeddata = [data JSONRepresentation];

    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@(tranid) forKey:kTransid];
    [params setObject:@(timestamp) forKey:kTimestamp];
    [params setObject:@(appid) forKey:kAppid];
    [params setObjectCheckNil:message forKey:kMessage];
    [params setObject:@(transtype) forKey:kTranstype];
    [params setObjectCheckNil:userid forKey:kUserid];
    [params setObjectCheckNil:destuserid forKey:kDestuserid];
    [params setObjectCheckNil:@(state) forKey:kNotificationState];
    [params setObject:@(notifyType) forKey:kNotificationType];
    [params setObjectCheckNil:embeddata forKey:kEmbedData];
    [params setObject:@(mtaid) forKey:kMtaid];
    [params setObject:@(mtuid) forKey:kMtuid];
    [params setObject:@(area) forKey:kArea];

    if (unread) { // fix recovery message override new notification message.
        return [self.db replaceToTable:kNotifyMessage params:params];
    }
    return [self.db insertToTable:kNotifyMessage params:params];
}

- (NSArray *)getListNotifyMessageFrom:(long)index count:(long)count {
    NSMutableArray *_return = [NSMutableArray array];
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ ORDER BY %@ COLLATE NOCASE DESC  LIMIT %ld OFFSET %ld", kNotifyMessage, kTimestamp, count, index];
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:query];
        while ([rs next]) {
            NSDictionary *oneDic = [self notificationDataFromRS:rs];
            [_return addObject:oneDic];
        }
        [rs close];
    }];
    return _return;
}

- (NSDictionary *)notificationDataFromRS:(FMResultSet *)rs {
    NSString *tranid = [@([rs longLongIntForColumn:kTransid]) stringValue];
    u_int64_t timestamp = [rs longLongIntForColumn:kTimestamp];
    int appid = [rs intForColumn:kAppid];
    NSString *message = [rs stringForColumn:kMessage];
    int transtype = [rs intForColumn:kTranstype];
    NSString *userid = [rs stringForColumn:kUserid];
    NSString *destuserid = [rs stringForColumn:kDestuserid];
    NotificationState notificationstate = [rs intForColumn:kNotificationState];
    int notifytype = [rs intForColumn:kNotificationType];

    NSString *embeddata = [rs stringForColumn:kEmbedData];
    NSString *mtaid = [rs stringForColumn:@"mtaid"];
    NSString *mtuid = [rs stringForColumn:@"mtuid"];
    int area = [rs intForColumn:kArea];

    NSString *notificationid = [NSString stringWithFormat:@"%@_%@", mtaid, mtuid];
    BOOL unread = notificationstate != NotificationStateRead;

    NSMutableDictionary *oneDic = [NSMutableDictionary dictionary];
    [oneDic setObjectCheckNil:notificationid forKey:@"notificationid"];
    [oneDic setObjectCheckNil:tranid forKey:@"transid"];
    [oneDic setObjectCheckNil:@(timestamp) forKey:@"timestamp"];
    [oneDic setObjectCheckNil:@(appid) forKey:@"appid"];
    [oneDic setObjectCheckNil:message forKey:@"message"];
    [oneDic setObjectCheckNil:@(transtype) forKey:@"transtype"];
    [oneDic setObjectCheckNil:userid forKey:@"userid"];
    [oneDic setObjectCheckNil:destuserid forKey:@"destuserid"];
    [oneDic setObjectCheckNil:@(unread) forKey:@"unread"];
    [oneDic setObjectCheckNil:@(notifytype) forKey:@"notificationtype"];
    [oneDic setObjectCheckNil:@(area) forKey:@"area"];
    [oneDic setObjectCheckNil:embeddata forKey:@"embeddata"];
    return oneDic;
}

- (NSDictionary *)getNotificationWithMtaid:(uint64_t)mtaid mtuid:(uint64_t)mtuid {
    __block NSDictionary *_return = nil;
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE %@=? AND %@=?", kNotifyMessage, kMtaid, kMtuid];
        FMResultSet *rs = [db executeQuery:query, @(mtaid), @(mtuid)];
        while ([rs next]) {
            _return = [self notificationDataFromRS:rs];
            break;
        }
        [rs close];
    }];
    return _return;
}

- (NSArray *)getListNotificationWithLixi:(NSArray *)arrMtaid arrMtuid:(NSArray *)arrMtuid {
    if ([arrMtaid count] == 0 || [arrMtuid count] == 0) {return @[];}
    NSMutableArray *_return = [NSMutableArray array];
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ TB1 INNER JOIN %@ TB2 ON TB1.%@ = TB2.%@ AND TB1.%@ = TB2.%@ where TB1.%@ in (%@) and TB1.%@ in (%@)", kNotifyMessage, kNotifyMessage, kMtaid, kMtaid, kMtuid, kMtuid, kMtaid, [arrMtaid componentsJoinedByString:@","], kMtuid, [arrMtuid componentsJoinedByString:@","]];
        FMResultSet *rs = [db executeQuery:query];
        while ([rs next]) {
            [_return addObject:[self notificationDataFromRS:rs]];
        }
        [rs close];
    }];
    return _return;
}

- (NSString *)notificaionIdFromMtaid:(uint64_t)mtaid mtuid:(uint64_t)mtuid {
    return [NSString stringWithFormat:@"%llu_%llu", mtaid, mtuid];
}

- (void)updateStateReadWithNotificationId:(NSString *)notificationid {
    NSArray *arrayIds = [notificationid componentsSeparatedByString:@"_"];
    NSString *mtaid = arrayIds.firstObject;
    NSString *mtuid = arrayIds.lastObject;
    [self updateStateReadWithMtaid:[mtaid longLongValue] mtuid:[mtuid longLongValue]];
}

- (void)updateStateReadWithMtaid:(uint64_t)mtaid mtuid:(uint64_t)mtuid {
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = @"UPDATE NotifyMessage SET notificationstate=? WHERE mtaid=? AND mtuid=? ";
        [db executeUpdate:query, @(NotificationStateRead), @(mtaid), @(mtuid)];
    }];
}

- (int)countUnreadNotify {
    __block int _return;
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = @"select count(mtaid) as totalUnread from NotifyMessage where notificationstate=?";
        FMResultSet *rs = [db executeQuery:query, @(NotificationStateUnread)];
        if ([rs next]) {
            _return = [rs intForColumn:@"totalUnread"];
        }
        [rs close];
    }];
    return _return;
}

- (NSDictionary *)merchantBill:(NSString *)notificationId {
    __block NSDictionary *params = nil;
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSArray *arrayIds = [notificationId componentsSeparatedByString:@"_"];
        NSString *mtaid = arrayIds.firstObject;
        NSString *mtuid = arrayIds.lastObject;
        NSString *query = [NSString stringWithFormat:@"SELECT %@ FROM %@ WHERE mtaid=? AND mtuid=?", kEmbedData, kNotifyMessage];
        FMResultSet *rs = [db executeQuery:query, mtaid, mtuid];
        while ([rs next]) {
            NSString *jsonString = [rs stringForColumn:kEmbedData];
            params = [jsonString JSONValue];
            break;
        }
        [rs close];
    }];
    return params;
}

- (BOOL)deleteNotificationWithId:(NSString *)notificationid {
    __block BOOL _return = FALSE;
    if (!notificationid) {
        return _return;
    }

    NSArray *arrayIds = [notificationid componentsSeparatedByString:@"_"];
    NSString *mtaid = arrayIds.firstObject;
    NSString *mtuid = arrayIds.lastObject;

    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        _return = [db executeUpdate:@"DELETE FROM NotifyMessage WHERE mtaid=? AND mtuid=? ", mtaid, mtuid];
    }];
    return _return;
}

- (BOOL)deleteAlllNotification {
    return [self.db clearDataInTable:kNotifyMessage];
}

- (void)clearNotificationCount {
    NSString *query = [NSString stringWithFormat:@"UPDATE %@ SET %@=? WHERE %@=? ", kNotifyMessage, kNotificationState, kNotificationState];
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        [db executeUpdate:query, @(NotificationStateView), @(NotificationStateUnread)];
    }];
}

- (uint64_t)notificationMinTimestamp {
    __block uint64_t _return = 0;
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = [NSString stringWithFormat:@"SELECT MIN(%@) AS minimun FROM %@ ", kTimestamp, kNotifyMessage];
        FMResultSet *rs = [db executeQuery:query];
        while ([rs next]) {
            _return = [rs longLongIntForColumn:@"minimun"];
        }
        [rs close];
    }];
    return _return;
}

@end
