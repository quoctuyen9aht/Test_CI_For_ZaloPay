//
//  ZPNotifyPopupTable.m
//  ZaloPayCommon
//
//  Created by bonnpv on 6/16/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPNotifyPopupTable.h"
#import <ZaloPayCommon/NSCollection+JSON.h>
#import <fmdb/FMDatabase.h>
#import <fmdb/FMDatabaseQueue.h>

#define kNotifyPopupMessage  @"NotifyPopupMessage"
#define kTransid        @"transid"
#define kTimestamp      @"timestamp"
#define kAppid          @"appid"
#define kMessage        @"message"
#define kTranstype      @"transtype"
#define kUserid         @"userid"
#define kDestuserid     @"destuserid"
#define kNotificationState  @"notificationstate"
#define kNotificationType   @"notificationtype"
#define kEmbedData          @"embeddata"
#define kMtaid          @"mtaid"
#define kMtuid          @"mtuid"
#define kArea           @"area"


@implementation ZPNotifyPopupTable

+ (NSArray<NSString*>*) creatingTableQuery {
    NSString* tableNotifyPopupMessage = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@(%@ long, %@ long, %@ int, %@ text, %@ int, %@ text, %@ text, %@ int, %@ int, %@ text, %@ long, %@ long , %@ long,PRIMARY KEY ( %@, %@ ));", kNotifyPopupMessage, kTransid, kTimestamp, kAppid, kMessage, kTranstype, kUserid, kDestuserid, kNotificationState, kNotificationType, kEmbedData, kMtaid, kMtuid, kArea, kMtaid, kMtuid];
    return @[tableNotifyPopupMessage];
}

+ (NSArray<NSString*>*) tableNamesCanBeDropped {
    return @[kNotifyPopupMessage];
}

- (BOOL)addLixiMessagePopupWithUserId:(NSString *)userid
                           destuserid:(NSString *)destuserid
                              message:(NSString *)message
                             bundleid:(uint64_t)bundleid
                              package:(uint64_t)packageid
                          liximessage:(NSString *)liximessage
                            avatarUrl:(NSString *)avatarUrl
                                 name:(NSString *)name
                            timestamp:(uint64_t)timestamp
                     notificationtype:(int)noticationtype
                               unread:(BOOL)unread
                                mtaid:(uint64_t)mtaid
                                mtuid:(uint64_t)mtuid
                                 area:(int)area {

    NSMutableDictionary *extra = [NSMutableDictionary dictionary];
    if (liximessage == nil) {
        liximessage = @"";
    }
    if (avatarUrl == nil) {
        avatarUrl = @"";
    }
    if (name == nil) {
        name = @"";
    }

    //    lixi.packageId = [embedDic uint64ForKey:@"packageid"];
    //    lixi.bundleId = [embedDic uint64ForKey:@"bundleid"];
    //    lixi.avatarUrl = [embedDic stringForKey:@"avatar"];
    //    lixi.name = [embedDic stringForKey:@"name"];
    //    lixi.lixiMessage = [embedDic stringForKey:@"liximessage"];

    [extra setObject:liximessage forKey:@"liximessage"];
    [extra setObject:[NSString stringWithFormat:@"%@", @(bundleid)] forKey:@"bundleid"];
    [extra setObject:[NSString stringWithFormat:@"%@", @(packageid)] forKey:@"packageid"];
    [extra setObject:avatarUrl forKey:@"avatarurl"];
    [extra setObject:name forKey:@"name"];

    return [self addNotifyPopupWithTranId:0
                                timestamp:timestamp
                                    appid:0
                                  message:message
                                transtype:0
                                   userid:userid
                               destuserid:destuserid
                                 isUnread:unread
                               notifyType:noticationtype
                                embeddata:extra
                                    mtaid:mtaid
                                    mtuid:mtuid
                                     area:area];
}

- (BOOL)addNotifyPopupWithTranId:(u_int64_t)tranid
                       timestamp:(u_int64_t)timestamp
                           appid:(int)appid
                         message:(NSString *)message
                       transtype:(int)transtype
                          userid:(NSString *)userid
                      destuserid:(NSString *)destuserid
                        isUnread:(BOOL)unread
                      notifyType:(int)notifyType
                       embeddata:(NSDictionary *)data
                           mtaid:(uint64_t)mtaid
                           mtuid:(uint64_t)mtuid
                            area:(int)area {
    NotificationPopupState state = unread == TRUE ? NotificationPopupStateUnread : NotificationPopupStateRead;
    NSString *embeddata = [data JSONRepresentation];

    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@(tranid) forKey:kTransid];
    [params setObject:@(timestamp) forKey:kTimestamp];
    [params setObject:@(appid) forKey:kAppid];
    [params setObjectCheckNil:message forKey:kMessage];
    [params setObject:@(transtype) forKey:kTranstype];
    [params setObjectCheckNil:userid forKey:kUserid];
    [params setObjectCheckNil:destuserid forKey:kDestuserid];
    [params setObjectCheckNil:@(state) forKey:kNotificationState];
    [params setObject:@(notifyType) forKey:kNotificationType];
    [params setObjectCheckNil:embeddata forKey:kEmbedData];
    [params setObject:@(mtaid) forKey:kMtaid];
    [params setObject:@(mtuid) forKey:kMtuid];
    [params setObject:@(area) forKey:kArea];
    
    if (unread) { // fix recovery message override new notification message.
        return [self.db replaceToTable:kNotifyPopupMessage params:params];
    }
    return [self.db insertToTable:kNotifyPopupMessage params:params];
}

- (NSArray *)getListNotifyPopupMessageFrom:(long)index count:(long)count {
    NSMutableArray *_return = [NSMutableArray array];
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ ORDER BY %@ COLLATE NOCASE DESC  LIMIT %ld OFFSET %ld", kNotifyPopupMessage, kTimestamp, count, index];
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:query];
        while ([rs next]) {
            NSDictionary *oneDic = [self NotificationPopupDataFromRS:rs];
            [_return addObject:oneDic];
        }
        [rs close];
    }];
    return _return;
}

- (NSArray *)getListNotifyPopupMessageUnRead {
    NSMutableArray *_return = [NSMutableArray array];
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ where notificationstate=%@", kNotifyPopupMessage, @(NotificationPopupStateUnread)];
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:query];
        while ([rs next]) {
            NSDictionary *oneDic = [self NotificationPopupDataFromRS:rs];
            [_return addObject:oneDic];
        }
        [rs close];
    }];
    return _return;
}

- (NSDictionary *)NotificationPopupDataFromRS:(FMResultSet *)rs {
    NSString *tranid = [@([rs longLongIntForColumn:kTransid]) stringValue];
    u_int64_t timestamp = [rs longLongIntForColumn:kTimestamp];
    int appid = [rs intForColumn:kAppid];
    NSString *message = [rs stringForColumn:kMessage];
    int transtype = [rs intForColumn:kTranstype];
    NSString *userid = [rs stringForColumn:kUserid];
    NSString *destuserid = [rs stringForColumn:kDestuserid];
    NotificationPopupState notificationstate = [rs intForColumn:kNotificationState];
    int notifytype = [rs intForColumn:kNotificationType];

    NSString *embeddata = [rs stringForColumn:kEmbedData];
    u_int64_t mtaid = [rs longForColumn:kMtaid];
    u_int64_t mtuid = [rs longForColumn:kMtuid];
    int area = [rs intForColumn:kArea];

    NSString *notificationid = [NSString stringWithFormat:@"%llu_%llu", mtaid, mtuid];
    BOOL unread = notificationstate != NotificationPopupStateRead;

    NSMutableDictionary *oneDic = [NSMutableDictionary dictionary];
    [oneDic setObjectCheckNil:notificationid forKey:@"notificationid"];
    [oneDic setObjectCheckNil:tranid forKey:@"transid"];
    [oneDic setObjectCheckNil:@(timestamp) forKey:@"timestamp"];
    [oneDic setObjectCheckNil:@(appid) forKey:@"appid"];
    [oneDic setObjectCheckNil:message forKey:@"message"];
    [oneDic setObjectCheckNil:@(transtype) forKey:@"transtype"];
    [oneDic setObjectCheckNil:userid forKey:@"userid"];
    [oneDic setObjectCheckNil:destuserid forKey:@"destuserid"];
    [oneDic setObjectCheckNil:@(unread) forKey:@"unread"];
    [oneDic setObjectCheckNil:@(notifytype) forKey:@"notificationtype"];
    [oneDic setObjectCheckNil:@(area) forKey:@"area"];
    [oneDic setObjectCheckNil:embeddata forKey:@"embeddata"];
    [oneDic setObjectCheckNil:@(mtaid) forKey:@"mtaid"];
    [oneDic setObjectCheckNil:@(mtuid) forKey:@"mtuid"];
    return oneDic;
}

- (NSDictionary *)getNotificationPopupWithMtaid:(uint64_t)mtaid mtuid:(uint64_t)mtuid {
    __block NSDictionary *_return = nil;
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE %@=? AND %@=?", kNotifyPopupMessage, kMtaid, kMtuid];
        FMResultSet *rs = [db executeQuery:query, @(mtaid), @(mtuid)];
        while ([rs next]) {
            _return = [self NotificationPopupDataFromRS:rs];
            break;
        }
        [rs close];
    }];
    return _return;
}

- (NSArray *)getListNotificationPopupWithLixi:(NSArray *)arrMtaid arrMtuid:(NSArray *)arrMtuid {
    if ([arrMtaid count] == 0 || [arrMtuid count] == 0) {return @[];}
    NSMutableArray *_return = [NSMutableArray array];
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ TB1 INNER JOIN %@ TB2 ON TB1.%@ = TB2.%@ AND TB1.%@ = TB2.%@ where TB1.%@ in (%@) and TB1.%@ in (%@)", kNotifyPopupMessage, kNotifyPopupMessage, kMtaid, kMtaid, kMtuid, kMtuid, kMtaid, [arrMtaid componentsJoinedByString:@","], kMtuid, [arrMtuid componentsJoinedByString:@","]];
        FMResultSet *rs = [db executeQuery:query];
        while ([rs next]) {
            [_return addObject:[self NotificationPopupDataFromRS:rs]];
        }
        [rs close];
    }];
    return _return;
}

- (NSString *)notificaionPopupIdFromMtaid:(uint64_t)mtaid mtuid:(uint64_t)mtuid {
    return [NSString stringWithFormat:@"%llu_%llu", mtaid, mtuid];
}

- (void)updateStateReadWithNotificationPopupId:(NSString *)notificationid {
    NSArray *arrayIds = [notificationid componentsSeparatedByString:@"_"];
    NSString *mtaid = arrayIds.firstObject;
    NSString *mtuid = arrayIds.lastObject;
    [self updateStateReadNotifyPopUpWithMtaid:[mtaid longLongValue] mtuid:[mtuid longLongValue]];
}

- (void)updateStateReadNotifyPopUpWithMtaid:(uint64_t)mtaid mtuid:(uint64_t)mtuid {
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = @"UPDATE NotifyPopupMessage SET notificationstate=? WHERE mtaid=? AND mtuid=? ";
        [db executeUpdate:query, @(NotificationPopupStateRead), @(mtaid), @(mtuid)];
    }];
}

- (int)countUnreadNotifyPopup {
    __block int _return;
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = @"select count(mtaid) as totalUnread from NotifyPopupMessage where notificationstate=?";
        FMResultSet *rs = [db executeQuery:query, @(NotificationPopupStateUnread)];
        if ([rs next]) {
            _return = [rs intForColumn:@"totalUnread"];
        }
        [rs close];
    }];
    return _return;
}

- (NSDictionary *)merchantBillNotificaionPopup:(NSString *)notificationId {
    __block NSDictionary *params = nil;
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSArray *arrayIds = [notificationId componentsSeparatedByString:@"_"];
        NSString *mtaid = arrayIds.firstObject;
        NSString *mtuid = arrayIds.lastObject;
        NSString *query = [NSString stringWithFormat:@"SELECT %@ FROM %@ WHERE mtaid=? AND mtuid=?", kEmbedData, kNotifyPopupMessage];
        FMResultSet *rs = [db executeQuery:query, mtaid, mtuid];
        while ([rs next]) {
            NSString *jsonString = [rs stringForColumn:kEmbedData];
            params = [jsonString JSONValue];
            break;
        }
        [rs close];
    }];
    return params;
}

- (BOOL)deleteNotificationPopupWithId:(NSString *)notificationid {
    __block BOOL _return = FALSE;
    if (!notificationid) {
        return _return;
    }

    NSArray *arrayIds = [notificationid componentsSeparatedByString:@"_"];
    NSString *mtaid = arrayIds.firstObject;
    NSString *mtuid = arrayIds.lastObject;

    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        _return = [db executeUpdate:@"DELETE FROM NotifyPopupMessage WHERE mtaid=? AND mtuid=? ", mtaid, mtuid];
    }];
    return _return;
}

- (BOOL)deleteAlllNotificationPopup {
    return [self.db clearDataInTable:kNotifyPopupMessage];
}

- (void)clearNotificationPopupCount {
    NSString *query = [NSString stringWithFormat:@"UPDATE %@ SET %@=? WHERE %@=? ", kNotifyPopupMessage, kNotificationState, kNotificationState];
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        [db executeUpdate:query, @(NotificationPopupStateView), @(NotificationPopupStateUnread)];
    }];
}

- (uint64_t)NotificationPopupMinTimestamp {
    __block uint64_t _return = 0;
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = [NSString stringWithFormat:@"SELECT MIN(%@) AS minimun FROM %@ ", kTimestamp, kNotifyPopupMessage];
        FMResultSet *rs = [db executeQuery:query];
        while ([rs next]) {
            _return = [rs longLongIntForColumn:@"minimun"];
        }
        [rs close];
    }];
    return _return;
}

@end

