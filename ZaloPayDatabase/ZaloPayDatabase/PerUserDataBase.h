//
//  PerUserDataBase.h
//  ZaloPayDatabase
//
//  Created by bonnpv on 6/3/16.
//  Copyright © 2016 bonnpv. All rights reserved.
//

#import "ZPPaymentDatabase.h"

#define kUsingTouchID                   @"UsingTouchID"

@interface PerUserDataBase : ZPPaymentDatabase
@property(copy, nonatomic) NSString *currentScheme;

+ (instancetype)sharedInstance;

- (BOOL)createDBWithUser:(NSString *)userId oldUser:(NSString *)oldUid;

- (NSMutableSet <NSString *> *)allTables;

- (void)dropAllTables;
- (void)dropAllTablesCanBeDropped;
- (NSString *)databasePathWithUid:(NSString *)uid;
@end
