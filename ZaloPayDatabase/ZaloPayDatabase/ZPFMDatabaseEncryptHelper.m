//
//  FMEncryptHelper.m
//  ZaloPay
//
//  Created by vuongvv on 12/5/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import "ZPFMDatabaseEncryptHelper.h"


@implementation ZPFMDatabaseEncryptHelper
+ (NSString *)newKey {
    return @"E4697B0E1F08507293D761A05CE4D1B62866";;
}

+ (NSString *)oldKey {
    return @"E4697B0120850123141761A05CE4D1B62866";
}

+ (void)encrypted:(NSString *)databasePath {
    NSString *key = [self newKey];
    BOOL encrypted = [ZPFMDatabaseEncryptHelper databaseWhetherEncrypted:databasePath key:key];
    if (!encrypted) {
        encrypted = [ZPFMDatabaseEncryptHelper encryptDatabase:databasePath key:key];
    }
}

+ (BOOL)databaseWhetherEncrypted:(NSString *)databasePath key:(NSString *)key {
    sqlite3 *db;
    int ret = sqlite3_open([databasePath UTF8String], &db);
    if (ret != SQLITE_OK) {
        sqlite3_close(db);
        NSAssert(NO, @"Failed to open database with message '%s'.", sqlite3_errmsg(db));
        // not reach here
        return YES;
    }

    ret = sqlite3_exec(db, (const char *) "SELECT count(*) FROM sqlite_master;", NULL, NULL, NULL);
    if (ret == SQLITE_OK) {
        sqlite3_close(db);
        return NO;
    }

    NSString *encryptedKey = key;
    BOOL success = [self openDB:db path:databasePath encryptedKey:encryptedKey];
    if (success) {
        sqlite3_close(db);
        return YES;
    }

    encryptedKey = self.oldKey;
    success = [self openDB:db path:databasePath encryptedKey:encryptedKey];
    if (!success) {
        sqlite3_close(db);
        [self deleteDatabase:databasePath];
        return NO;
    }

    sqlite3_close(db);
    NSString *originKey = encryptedKey;
    NSString *newKey = key;
    BOOL resetKey = [self changeKey:databasePath originKey:originKey newKey:newKey];
    if (!resetKey) {
        [self deleteDatabase:databasePath];
        return NO;
    }

    return YES;
}

+ (BOOL)openDB:(sqlite3 *)db path:(NSString *)path encryptedKey:(NSString *)encryptedKey {
    const char *key = [encryptedKey UTF8String];
    sqlite3_key(db, key, (int) strlen(key));
    int ret = sqlite3_exec(db, (const char *) "SELECT count(*) FROM sqlite_master;", NULL, NULL, NULL);
    if (ret == SQLITE_OK) {
        return YES;
    } else {
        return NO;
    }
}

+ (void)deleteDatabase:(NSString *)databasePath {
    [[NSFileManager defaultManager] removeItemAtPath:databasePath error:NULL];
    [[NSFileManager defaultManager] removeItemAtPath:[databasePath stringByAppendingString:@"-shm"] error:NULL];
    [[NSFileManager defaultManager] removeItemAtPath:[databasePath stringByAppendingString:@"-wal"] error:NULL];
}

+ (BOOL)encryptDatabase:(NSString *)path key:(NSString *)key {
    NSString *sourcePath = path;
    NSString *targetPath = [NSString stringWithFormat:@"%@.tmp.db", path];
    NSString *backupPath = [NSTemporaryDirectory() stringByAppendingPathComponent:[path lastPathComponent]];

    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *error = nil;
    [fm copyItemAtPath:sourcePath toPath:backupPath error:&error];

    if ([self encryptDatabase:backupPath targetPath:targetPath encryptedKey:key]) {
        [fm removeItemAtPath:sourcePath error:nil];
        [fm moveItemAtPath:targetPath toPath:sourcePath error:nil];
        [fm removeItemAtPath:targetPath error:nil];
        [fm removeItemAtPath:backupPath error:nil];
        return YES;
    } else {
        [fm removeItemAtPath:targetPath error:nil];
        [fm removeItemAtPath:backupPath error:nil];
        return NO;
    }
}

+ (BOOL)encryptDatabase:(NSString *)dbPath targetPath:(NSString *)enDBPath encryptedKey:(NSString *)encryptKey {
    sqlite3 *db;
    int ret = sqlite3_open([dbPath UTF8String], &db);
    if (ret != SQLITE_OK) {
        sqlite3_close(db);
        return NO;
    }

    const char *sqlQ = [[NSString stringWithFormat:@"ATTACH DATABASE '%@' AS encrypted KEY '%@';", enDBPath, encryptKey] UTF8String];
    ret = sqlite3_exec(db, sqlQ, NULL, NULL, NULL);
    if (ret != SQLITE_OK) {
        sqlite3_close(db);
        return NO;
    }

    ret = sqlite3_exec(db, "SELECT sqlcipher_export('encrypted');", NULL, NULL, NULL);
    if (ret != SQLITE_OK) {
        sqlite3_close(db);
        return NO;
    }

    ret = sqlite3_exec(db, "DETACH DATABASE encrypted;", NULL, NULL, NULL);
    if (ret != SQLITE_OK) {
        sqlite3_close(db);
        return NO;
    }

    sqlite3_close(db);

    return YES;
}

+ (BOOL)changeKey:(NSString *)dbPath originKey:(NSString *)originKey newKey:(NSString *)newKey {
    sqlite3 *db;
    int ret = sqlite3_open([dbPath UTF8String], &db);
    if (ret != SQLITE_OK) {
        sqlite3_close(db);
        return NO;
    }

    ret = sqlite3_exec(db, [[NSString stringWithFormat:@"PRAGMA key = '%@';", originKey] UTF8String], NULL, NULL, NULL);
    if (ret != SQLITE_OK) {
        sqlite3_close(db);
        return NO;
    }

    ret = sqlite3_exec(db, [[NSString stringWithFormat:@"PRAGMA rekey = '%@';", newKey] UTF8String], NULL, NULL, NULL);
    if (ret != SQLITE_OK) {
        sqlite3_close(db);
        return NO;
    }

    sqlite3_close(db);

    return YES;
}

@end
