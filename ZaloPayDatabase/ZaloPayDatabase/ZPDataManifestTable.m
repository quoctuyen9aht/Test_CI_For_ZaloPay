//
//  ZPDataManifestTable.m
//  ZaloPay
//
//  Created by bonnpv on 5/4/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//
#import <fmdb/FMDatabase.h>
#import <fmdb/FMDatabaseQueue.h>
#import "ZPDataManifestTable.h"

#define kUserBalance                            @"userBalanceKey"


@implementation ZPDataManifestTable

+ (NSArray<NSString*>*) creatingTableQuery {
    NSString* tableDataManifest = @"CREATE TABLE IF NOT EXISTS DataManifest(key text primary key, value text);";
    return @[tableDataManifest];
}

+ (NSArray<NSString*>*) tableNamesCanBeDropped {
    return @[@"DataManifest"];
}

- (void)updateBalance:(long)balance {
    [self setValue:[NSString stringWithFormat:@"%ld", balance] forManifestKey:kUserBalance];
}

- (void)setValue:(NSString *)value forManifestKey:(NSString *)key {
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = @"INSERT OR REPLACE INTO DataManifest (key, value) VALUES (?,?)";
        [db executeUpdate:query, key, value];
    }];
}

- (NSString *)valueForMainfestKey:(NSString *)key {
    __block NSString *version;
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSError *error = nil;
        FMResultSet *rs = [db executeQuery:@"SELECT value FROM DataManifest WHERE key=?" values:@[key] error:&error];
        if (error) {
            return;
        }
        if ([rs next]) {
            version = [rs stringForColumn:@"value"];
        }

        [rs close];
    }];

    return version;
}

- (long)getBalance {
    __block NSString *version;
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSError *error = nil;
        FMResultSet *rs = [db executeQuery:@"SELECT value FROM DataManifest WHERE key=?" values:@[kUserBalance] error:&error];
        if (error) {
            return;
        }
        if ([rs next]) {
            version = [rs stringForColumn:@"value"];
        }
        [rs close];
    }];

    return (long) [version longLongValue];
}


@end
