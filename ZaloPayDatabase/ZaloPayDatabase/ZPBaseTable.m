//
//  ZPBaseTable.m
//  ZaloPayDatabase
//
//  Created by tridm2 on 3/21/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import "ZPBaseTable.h"
#import <Foundation/Foundation.h>
#import "ZaloPayDatabaseLog.h"


@implementation ZPBaseTable

- (id) initWithDB:(ZPPaymentDatabase*) db {
    if (self = [super init]) {
        self.db = db;
    }
    return self;
}

+ (void) createTable:(FMDatabase *)db {
    NSArray<NSString*>* queries = [self creatingTableQuery];
    if ([queries count] == 0) {
        return;
    }
    
    for (NSString* query in queries) {
        [db executeUpdate:query];
    }
}

+ (void) dropTable:(FMDatabase *)db {
    NSArray<NSString*>* tableNames = [self tableNamesCanBeDropped];
    if ([tableNames count] == 0) {
        return;
    }
    
    for (NSString *tableName in tableNames) {
        NSString *sql = [NSString stringWithFormat:@"DROP TABLE IF EXISTS %@", tableName];
        if (![db executeUpdate:sql]) {
            DDLogInfo(@"No delete table : %@!!!!!!!!!!!!!", tableName);
        }
    }
}

// Override by subclass
+ (NSArray<NSString*>*) creatingTableQuery {
    return nil;
}

+ (NSArray<NSString*>*) tableNamesCanBeDropped {
    return nil;
}
@end
