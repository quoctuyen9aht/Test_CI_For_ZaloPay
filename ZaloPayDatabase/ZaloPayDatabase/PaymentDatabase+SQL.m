//
//  PaymentDatabase+SQL.m
//  ZaloPayDatabase
//
//  Created by bonnpv on 7/21/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "PaymentDatabase+SQL.h"
#import <fmdb/FMDatabase.h>
#import <fmdb/FMDatabaseQueue.h>

@implementation PaymentDatabase (SQL)

- (BOOL)replaceToTable:(NSString *)table params:(NSDictionary *)params {
    __block BOOL result = FALSE;
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = [self buildReplaceSQLQuery:table params:params];
        result = [db executeUpdate:query withParameterDictionary:params];
    }];
    return result;
}


- (BOOL)replaceArrayToTable:(NSString *)table params:(NSMutableArray *)params {
    __block BOOL noErrors = YES;
    [self.dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        for (id data in params) {
            NSString *query = [self buildInsertOrIgnoreSQLQuery:table params:data];
            if (![db executeUpdate:query withParameterDictionary:data]) noErrors = NO;
        }
        *rollback = !noErrors;
    }];
    return noErrors;
}

- (BOOL)insertToTable:(NSString *)table params:(NSDictionary *)params {
    __block BOOL result = FALSE;
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = [self buildInsertSQLQuery:table params:params];
        result = [db executeUpdate:query withParameterDictionary:params];
    }];
    return result;
}

- (BOOL)insertOrIgnoreToTable:(NSString *)table params:(NSDictionary *)params {
    __block BOOL result = FALSE;
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = [self buildInsertOrIgnoreSQLQuery:table params:params];
        result = [db executeUpdate:query withParameterDictionary:params];
    }];
    return result;
}

- (BOOL)updateToTable:(NSString *)table params:(NSDictionary *)params whereKey:(NSString *)key isValue:(NSString *)value {
    __block BOOL result = FALSE;
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        NSDictionary *querys = [self buildUpdateQueryTable:table params:params whereKey:key isValue:value];
        NSMutableString *queryString = [querys objectForKey:kUpdateQuery];
        NSArray *values = [querys objectForKey:kUpdateValues];
        result = [db executeUpdate:queryString withArgumentsInArray:values];
    }];
    return result;
}

- (NSDictionary *)buildUpdateQueryTable:(NSString *)table params:(NSDictionary *)params whereKey:(NSString *)key isValue:(NSString *)value {
    NSMutableString *query = [NSMutableString string];
    [query appendFormat:@"UPDATE %@ SET ", table];
    NSMutableArray *values = [NSMutableArray array];
    NSArray *allKey = params.allKeys;
    for (NSString *key in allKey) {
        [values addObject:params[key]];
        NSString *format = key == allKey.lastObject ? @"%@ = ?" : @"%@ = ?, ";
        [query appendFormat:format, key];
    }
    if (key && value) {
        [query appendFormat:@" WHERE %@ = ?", key];
        [values addObject:value];
    }
    return @{kUpdateQuery: query,
            kUpdateValues: values};

}

- (BOOL)clearDataInTable:(NSString *)tableName {
    __block BOOL result = FALSE;
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        result = [db executeUpdate:[NSString stringWithFormat:@"DELETE FROM %@", tableName]];
    }];
    return result;
}

- (NSString *)buildReplaceSQLQuery:(NSString *)tableName params:(NSDictionary *)params {
    return [self buildQuery:@"REPLACE INTO" table:tableName params:params];
}

- (NSString *)buildInsertSQLQuery:(NSString *)tableName params:(NSDictionary *)params {
    return [self buildQuery:@"INSERT INTO" table:tableName params:params];
}

- (NSString *)buildInsertOrIgnoreSQLQuery:(NSString *)tableName params:(NSDictionary *)params {
    return [self buildQuery:@"INSERT OR IGNORE INTO" table:tableName params:params];
}

- (NSString *)buildQuery:(NSString *)updateQuery table:(NSString *)tableName params:(NSDictionary *)params {
    NSArray *keys = params.allKeys;
    NSMutableArray *values = [NSMutableArray arrayWithCapacity:params.count];
    for (NSString *key in keys) {
        NSString *value = [NSString stringWithFormat:@":%@", key];
        [values addObject:value];
    }
    NSMutableString *query = [NSMutableString string];
    [query appendString:updateQuery];
    [query appendFormat:@" %@ ", tableName];
    [query appendFormat:@"( %@ )", [keys componentsJoinedByString:@","]];
    [query appendString:@" VALUES "];
    [query appendFormat:@"( %@ )", [values componentsJoinedByString:@","]];
    return query;
}
@end
