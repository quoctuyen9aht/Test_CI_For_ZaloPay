//
//  ZPCacheDataTable.m
//  ZaloPay
//
//  Created by bonnpv on 6/16/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <fmdb/FMDatabase.h>
#import <fmdb/FMDatabaseQueue.h>
#import "ZPCacheDataTable.h"


@implementation ZPCacheDataTable

+ (NSArray<NSString*>*) creatingTableQuery {
    NSString* tableCacheData = @"CREATE TABLE IF NOT EXISTS CacheData(key text primary key, value text);";
    return @[tableCacheData];
}

- (NSString *)cacheDataValueForKey:(NSString *)key {
    __block NSString *version;
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSError *error = nil;
        FMResultSet *rs = [db executeQuery:@"SELECT value FROM CacheData WHERE key=?" values:@[key] error:&error];
        if (error) {
            return;
        }
        if ([rs next]) {
            version = [rs stringForColumn:@"value"];
        }

        [rs close];
    }];

    return version;
}

- (void)cacheDataUpdateValue:(NSString *)value forKey:(NSString *)key {
    if (!value) {
        [self cacheDataRemoveDataForKey:key];
        return;
    }

    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = @"INSERT OR REPLACE INTO CacheData (key, value) VALUES (?, ?)";
        [db executeUpdate:query, key, value];
    }];
}

- (void)cacheDataRemoveDataForKey:(NSString *)key {
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = @"DELETE FROM CacheData WHERE key=?";
        [db executeUpdate:query, key];
    }];
}

- (void)removeAllCacheDataButKeepDataWithKey:(NSString *)key {
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = @"DELETE FROM CacheData WHERE key!=?";
        [db executeUpdate:query, key];
    }];
}

@end
