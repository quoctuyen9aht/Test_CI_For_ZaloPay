//
//  PaymentDatabase+SQL.h
//  ZaloPayDatabase
//
//  Created by bonnpv on 7/21/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "PaymentDatabase.h"

#define kUpdateQuery    @"UpdateQuery"
#define kUpdateValues   @"UpdateValues"

@interface PaymentDatabase (SQL)
- (BOOL)insertToTable:(NSString *)table params:(NSDictionary *)params;

- (BOOL)insertOrIgnoreToTable:(NSString *)table params:(NSDictionary *)params;

- (BOOL)updateToTable:(NSString *)table params:(NSDictionary *)params whereKey:(NSString *)key isValue:(NSString *)value;

- (BOOL)replaceToTable:(NSString *)table params:(NSDictionary *)params;

- (BOOL)replaceArrayToTable:(NSString *)table params:(NSMutableArray *)params;

- (BOOL)clearDataInTable:(NSString *)tableName;

- (NSString *)buildReplaceSQLQuery:(NSString *)tableName params:(NSDictionary *)params;

- (NSString *)buildInsertOrIgnoreSQLQuery:(NSString *)tableName params:(NSDictionary *)params;

- (NSString *)buildQuery:(NSString *)updateQuery table:(NSString *)tableName params:(NSDictionary *)params;

- (NSDictionary *)buildUpdateQueryTable:(NSString *)table params:(NSDictionary *)params whereKey:(NSString *)key isValue:(NSString *)value;
@end
