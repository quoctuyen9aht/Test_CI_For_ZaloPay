//
//  ZaloPayDatabaseLog.m
//  ZaloPayDatabase
//
//  Created by bonnpv on 6/8/16.
//  Copyright © 2016 bonnpv. All rights reserved.
//

#import "ZaloPayDatabaseLog.h"

const DDLogLevel zaloPayDatabaseLogLevel = DDLogLevelInfo;
