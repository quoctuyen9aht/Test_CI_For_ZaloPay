//
//  ZPTransactionLogTable.m
//  ZaloPayAppManager
//
//  Created by PhucPv on 5/7/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPTransactionLogTable.h"
//#import "NSCollection+JSON.h"
#import <ZaloPayCommon/NSCollection+JSON.h>
#import <fmdb/FMDatabase.h>
#import <fmdb/FMDatabaseQueue.h>
#import <ZaloPayCommon/Collection+P2PNotification.h>


typedef enum {
    StatusTypeSuccess = 1,
    StatusTypeFail = 2
} StatusType;
typedef enum {
    TransOrderLastest = 1,
    TransOrderOldest = -1,
} TransOrder;

@implementation ZPTransactionLogTable

+ (NSArray<NSString*>*) creatingTableQuery {
    NSString* tableTransactionLogs = @"CREATE TABLE IF NOT EXISTS TransactionLogs(transid INT PRIMARY KEY NOT NULL, appid int, platform text, description text, pmcid int, reqdate int, userchargeamt int, amount int, userfeeamt int, type int, userid text, appuser text, sign int, username text, appusername text, statustype int, thankmessage text, item text, discountamount int);";
    NSString* tableFragmentTransaction = @"CREATE TABLE IF NOT EXISTS FragmentTransaction(maxreqdate INT, minreqdate INT PRIMARY KEY , statustype INT, outofdata BOOL);";
    
    return @[tableTransactionLogs, tableFragmentTransaction];
}

+ (NSArray<NSString*>*) tableNamesCanBeDropped {
    return @[@"TransactionLogs", @"FragmentTransaction"];
}

- (BOOL)removeTransactionWithId:(NSString *)transid {
    __block BOOL result;
    [self.db.dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        result = [db executeUpdate:@"DELETE FROM TransactionLogs WHERE transid=?", transid];
    }];
    return result;
}

- (BOOL)updateThankMessageWithTransId:(NSString *)transid
                        encodeMessage:(NSString *)encodeMessage {
    __block BOOL result;
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = @"UPDATE TransactionLogs SET thankmessage = ? WHERE transid = ?";
        result = [db executeUpdate:query, encodeMessage, transid];
    }];
    return result;
}

- (void)updateTransactionLogs:(NSArray *)transactionLogs
                   statusType:(int)statustype
                 keyTimestamp:(NSTimeInterval)keyTimestamp
                        order:(int)order {
    if ([transactionLogs count] == 0) {
        return;
    }
    __block NSTimeInterval maxTimestamp = 0.0;
    __block NSTimeInterval minTimestamp = 0.0;
    [self.db.dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        for (NSDictionary *transactionLog in transactionLogs) {
            int64_t transid = [transactionLog uint64ForKey:@"transid"];
            int appid = [transactionLog intForKey:@"appid"];
            NSString *platform = [transactionLog stringForKey:@"platform"];
            NSString *description = [transactionLog stringForKey:@"description"];
            int pmcid = [transactionLog intForKey:@"pmcid"];
            NSTimeInterval reqdate = [transactionLog uint64ForKey:@"reqdate"];
            int64_t userchargeamt = [transactionLog uint64ForKey:@"userchargeamt"];
            int64_t amount = [transactionLog uint64ForKey:@"amount"];
            int64_t userfeeamt = [transactionLog uint64ForKey:@"userfeeamt"];
            int type = [transactionLog intForKey:@"type"];
            NSString *userid = [transactionLog stringForKey:@"userid"];
            NSString *appuser = [transactionLog stringForKey:@"appuser"];
            int sign = [transactionLog intForKey:@"sign"];
            NSString *username = [transactionLog stringForKey:@"username"];

            NSString *appusername = [transactionLog stringForKey:@"appusername"];

            NSString *item = [transactionLog stringForKey:@"item"];

            int64_t discountamount = [transactionLog uint64ForKey:@"discountamount"];

            [db executeUpdate:@"INSERT OR REPLACE INTO TransactionLogs (transid, appid, platform, description, pmcid, reqdate, userchargeamt, amount, userfeeamt, type, userid, appuser, sign, username, appusername, statustype, item, discountamount) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                              @(transid), @(appid), platform, description, @(pmcid), @(reqdate), @(userchargeamt), @(amount), @(userfeeamt), @(type), userid, appuser, @(sign), username, appusername, @(statustype), item, @(discountamount)];
            if (minTimestamp == 0) {
                minTimestamp = reqdate;
            }
            if (reqdate > maxTimestamp) {
                maxTimestamp = reqdate;
            }
            if (reqdate < minTimestamp) {
                minTimestamp = reqdate;
            }
        }
    }];
    [self updateFragmentWithKey:keyTimestamp
                            max:maxTimestamp
                            min:minTimestamp
                     statusType:statustype
                          order:order];
}

- (void)cleanDB {
    [self.db.dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        [db executeUpdate:@"DELETE FROM FragmentTransaction"];
        [db executeUpdate:@"DELETE FROM TransactionLogs"];
    }];
}

- (void)updateFragmentWithKey:(NSTimeInterval)key
                          max:(NSTimeInterval)maxTimestamp
                          min:(NSTimeInterval)minTimestamp
                   statusType:(int)statusType
                        order:(int)order {
    if (key != 0 && key > maxTimestamp) {
        maxTimestamp = key;
    }
    NSDictionary *fragment = [self fragmentWithTimestamp:key statusType:statusType];
    NSNumber *dbMinTimestamp = [fragment objectForKey:@"minreqdate"];

    if (!dbMinTimestamp) {
        [self.db.dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
            [db executeUpdate:@"INSERT OR REPLACE INTO FragmentTransaction (maxreqdate, minreqdate, statustype, outofdata) VALUES (?, ?, ?, ?)",
                              @(maxTimestamp),
                              @(minTimestamp),
                              @(statusType),
                              @(false)];
        }];
    } else if (order == TransOrderOldest) {
        __block NSTimeInterval minimum;
        __block BOOL outOfData;
        [self.db.dbQueue inDatabase:^(FMDatabase *db) {
            FMResultSet *rs = [db executeQuery:@"SELECT MIN(minreqdate) as minimum, MAX(outofdata) as outofdata FROM FragmentTransaction WHERE (maxreqdate >= ? AND minreqdate <= ?) AND statustype = ?",
                                               @(minTimestamp),
                                               @(minTimestamp),
                                               @(statusType)];
            while ([rs next]) {
                minimum = [rs doubleForColumn:@"minimum"];
                outOfData = [rs boolForColumn:@"outofdata"];
            }
            [rs close];
        }];
        if (minimum != 0 && minimum < minTimestamp) {
            minTimestamp = minimum;
        }

        [self.db.dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
            [db executeUpdate:@"DELETE FROM FragmentTransaction WHERE (maxreqdate >= ? AND minreqdate <= ?) AND minreqdate != ? AND statustype = ?",
                              @(minTimestamp),
                              @(minTimestamp),
                              dbMinTimestamp,
                              @(statusType)];
            [db executeUpdate:@"UPDATE FragmentTransaction SET minreqdate = ?, outofdata = ? WHERE minreqdate = ? AND statustype = ?",
                              @(minTimestamp),
                              @(outOfData),
                              dbMinTimestamp,
                              @(statusType)];
        }];
    } else {
        [self.db.dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
            [db executeUpdate:@"UPDATE FragmentTransaction SET maxreqdate = ? WHERE maxreqdate = (SELECT MAX(maxreqdate) FROM FragmentTransaction WHERE statustype = ?) AND statustype = ?",
                              @(maxTimestamp),
                              @(statusType),
                              @(statusType)];
        }];
    }
}

- (void)setOutOfDataWithTimestamp:(NSTimeInterval)timestamp
                       statusType:(int)statusType {
    NSNumber *minreqdate;
    NSNumber *maxreqdate;

    NSDictionary *fragment = [self fragmentWithTimestamp:timestamp statusType:statusType];
    if ([fragment.allKeys count] == 0) {
        minreqdate = [NSNumber numberWithDouble:timestamp];
        maxreqdate = [NSNumber numberWithDouble:timestamp];
    } else {
        minreqdate = [fragment objectForKey:@"minreqdate"];
        maxreqdate = [fragment objectForKey:@"maxreqdate"];
    }
    [self.db.dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        [db executeUpdate:@"INSERT OR REPLACE INTO FragmentTransaction (maxreqdate, minreqdate, statustype, outofdata) VALUES (?, ?, ?, ?)",
                          maxreqdate,
                          minreqdate,
                          @(statusType),
                          @(true)];
    }];
}

- (BOOL)outOfDataWithTimestamp:(NSTimeInterval)timestamp
                    statusType:(int)statusType {
    NSDictionary *fragment = [self fragmentWithTimestamp:timestamp statusType:statusType];
    __block BOOL outOfData = [[fragment objectForKey:@"outofdata"] boolValue];

    if (outOfData || timestamp == 0) {
        return outOfData;
    }
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:@"SELECT MAX(outofdata) as outofdata FROM FragmentTransaction WHERE minreqdate > ? AND statustype = ?",
                                           @(timestamp),
                                           @(statusType)];
        while ([rs next]) {
            outOfData = [rs boolForColumn:@"outofdata"];
        }
        [rs close];
    }];
    return outOfData;
}

- (NSDictionary *)fragmentWithTimestamp:(NSTimeInterval)timestamp statusType:(int)statusType {
    __block NSMutableDictionary *result = [NSMutableDictionary dictionary];

    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs;
        if (timestamp == 0) {
            rs = [db executeQuery:@"SELECT * FROM FragmentTransaction WHERE maxreqdate = (SELECT MAX(maxreqdate) FROM FragmentTransaction WHERE statustype = ?) AND statustype = ?",
                                  @(statusType),
                                  @(statusType)];
        } else {
            rs = [db executeQuery:@"SELECT * FROM FragmentTransaction WHERE (maxreqdate >= ? AND minreqdate <= ?) AND statustype = ?",
                                  @(timestamp),
                                  @(timestamp),
                                  @(statusType)];
        }

        while ([rs next]) {
            [result setObjectCheckNil:@([rs doubleForColumn:@"maxreqdate"]) forKey:@"maxreqdate"];
            [result setObjectCheckNil:@([rs doubleForColumn:@"minreqdate"]) forKey:@"minreqdate"];
            [result setObjectCheckNil:@([rs intForColumn:@"statustype"]) forKey:@"statustype"];
            [result setObjectCheckNil:@([rs boolForColumn:@"outofdata"]) forKey:@"outofdata"];
        }
        [rs close];
    }];
    return result;
}

- (NSTimeInterval)maxTimestampFragmentWithKey:(NSTimeInterval)key statusType:(int)statusType {
    return [[[self fragmentWithTimestamp:key statusType:statusType] objectForKey:@"maxreqdate"] doubleValue];
}

- (NSTimeInterval)minTimestampFragmentWithKey:(NSTimeInterval)key statusType:(int)statusType {
    __block NSTimeInterval _return = 0;
    _return = [[[self fragmentWithTimestamp:key statusType:statusType] objectForKey:@"minreqdate"] doubleValue];
    if (_return == 0) {
        _return = key;
    }
    return _return;
}

- (NSArray *)transactionWithStatusType:(int)statusType
                            transTypes:(NSArray *)transTypes
                          keyTimestamp:(NSTimeInterval)keyTimestamp
                                  sign:(int)sign
                                offset:(int)offset
                                 count:(int)count {
    NSMutableArray *result = [NSMutableArray new];


    NSDictionary *fragment = [self fragmentWithTimestamp:keyTimestamp statusType:statusType];

    NSNumber *dbMaxTimestamp = [fragment objectForKey:@"maxreqdate"];
    NSNumber *dbMinTimestamp = [fragment objectForKey:@"minreqdate"];
    if (keyTimestamp != 0) {
        dbMaxTimestamp = [NSNumber numberWithDouble:keyTimestamp];
    }
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSMutableString *condition = [self conditionWithTransType:transTypes sign:sign];
        NSString *query = @"SELECT * from TransactionLogs WHERE reqdate <= ? AND reqdate >= ? AND statustype = ? %@ ORDER BY reqdate COLLATE NOCASE DESC  LIMIT ? OFFSET ?";
        FMResultSet *rs = [db executeQuery:[NSString stringWithFormat:query, condition],
                                           dbMaxTimestamp,
                                           dbMinTimestamp,
                                           @(statusType),
                                           @(count),
                                           @(offset)];
        while ([rs next]) {
            NSMutableDictionary *dict = [self transactionFromResultSet:rs];
            [result addObject:dict];
        }
        [rs close];
    }];
    return result;
}

- (NSMutableString *)conditionWithTransType:(NSArray *)transTypes
                                       sign:(int)sign {
    NSMutableString *condition = [NSMutableString stringWithFormat:@""];
    NSMutableArray *_transTypes = [NSMutableArray arrayWithArray:transTypes];

    if ([transTypes count] > 0) {
        // 4: transfer money
        // 11: cashInMerchant
        NSString *transferCondition;
        NSString *typeCondition;
        if (([_transTypes containsObject:@(4)] || [_transTypes containsObject:@(11)]) && sign != 0) {
            [_transTypes removeObject:@(4)];
            [_transTypes removeObject:@(11)];
            // sign = 1: Receive
            if (sign == 1) {
                transferCondition = [NSString stringWithFormat:@"( (type = 4 OR type = 11) AND sign = %@ )", @(1)];
            } else {
                transferCondition = [NSString stringWithFormat:@"( (type = 4 OR type = 11) AND sign != %@ )", @(1)];
            }
        }
        if ([_transTypes count] > 0) {;
            typeCondition = [NSString stringWithFormat:@"type IN ( %@ )", [_transTypes componentsJoinedByString:@", "]];
        }

        if (transferCondition && typeCondition) {
            [condition appendString:[NSString stringWithFormat:@"( %@ OR %@ )", transferCondition, typeCondition]];
        } else if (transferCondition) {
            [condition appendString:transferCondition];
        } else if (typeCondition) {
            [condition appendString:typeCondition];
        }
    }
    if (condition.length > 0) {
        condition = [NSMutableString stringWithFormat:@" AND ( %@ )", condition];
    }
    return condition;
}

- (NSArray *)loadTransactionLogWithId:(NSString *)transId {
    NSMutableArray *result = [NSMutableArray new];
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:@"SELECT * FROM TransactionLogs WHERE transid = ?", transId];
        while ([rs next]) {
            NSMutableDictionary *dict = [self transactionFromResultSet:rs];
            [result addObject:dict];
        }
        [rs close];
    }];
    return result;
}

- (NSMutableDictionary *)transactionFromResultSet:(FMResultSet *)rs {
    int64_t transid = [rs longLongIntForColumn:@"transid"];
    int appid = [rs intForColumn:@"appid"];
    NSString *platform = [rs stringForColumn:@"platform"];
    NSString *description = [rs stringForColumn:@"description"];
    int64_t pmcid = [rs intForColumn:@"pmcid"];
    int64_t reqdate = [rs longLongIntForColumn:@"reqdate"];
    int64_t userchargeamt = [rs longLongIntForColumn:@"userchargeamt"];
    int64_t amount = [rs longLongIntForColumn:@"amount"];
    int64_t userfeeamt = [rs longLongIntForColumn:@"userfeeamt"];
    int type = [rs intForColumn:@"type"];
    NSString *userid = [rs stringForColumn:@"userid"];
    NSString *appuser = [rs stringForColumn:@"appuser"];
    int sign = [rs intForColumn:@"sign"];
    NSString *username = [rs stringForColumn:@"username"];
    NSString *appusername = [rs stringForColumn:@"appusername"];
    int statustype = [rs intForColumn:@"statustype"];
    NSString *encodeThankMessage = [rs stringForColumn:@"thankmessage"];
    NSString *thankMessage = [NSString decodeP2PMessage:encodeThankMessage];
    NSString *item = [rs stringForColumn:@"item"];
    int64_t discountamount = [rs longLongIntForColumn:@"discountamount"];


    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObjectCheckNil:@(transid) forKey:@"transid"];
    [dict setObjectCheckNil:@(appid) forKey:@"appid"];
    [dict setObjectCheckNil:platform forKey:@"platform"];
    [dict setObjectCheckNil:description forKey:@"description"];
    [dict setObjectCheckNil:@(pmcid) forKey:@"pmcid"];
    [dict setObjectCheckNil:@(reqdate) forKey:@"reqdate"];
    [dict setObjectCheckNil:@(userchargeamt) forKey:@"userchargeamt"];
    [dict setObjectCheckNil:@(amount) forKey:@"amount"];
    [dict setObjectCheckNil:@(userfeeamt) forKey:@"userfeeamt"];
    [dict setObjectCheckNil:@(type) forKey:@"type"];
    [dict setObjectCheckNil:userid forKey:@"userid"];
    [dict setObjectCheckNil:appuser forKey:@"userid"];
    [dict setObjectCheckNil:@(sign) forKey:@"sign"];
    [dict setObjectCheckNil:username forKey:@"username"];
    [dict setObjectCheckNil:appusername forKey:@"appusername"];
    [dict setObjectCheckNil:@(statustype) forKey:@"statustype"];
    [dict setObjectCheckNil:thankMessage forKey:@"thankmessage"];
    [dict setObjectCheckNil:item forKey:@"item"];
    [dict setObjectCheckNil:@(discountamount) forKey:@"discountamount"];
    return dict;
}

- (void)updateRetrySuccessTransaction:(uint64_t)tranid {
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = @"UPDATE TransactionLogs SET statustype = ? WHERE transid = ?";
        [db executeUpdate:query, @(1), @(tranid)];
    }];
}

@end
