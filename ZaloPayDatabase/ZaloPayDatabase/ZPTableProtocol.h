//
//  ZPTableProtocol.h
//  ZaloPayDatabase
//
//  Created by tridm2 on 3/21/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FMDatabase;

@protocol ZPTableProtocol<NSObject>
@optional
+ (void) createTable:(FMDatabase *)db;
+ (void) dropTable:(FMDatabase *)db;
@end
