//
//  ZPPaymentDatabase.m
//  ZaloPayDatabase
//
//  Created by tridm2 on 3/20/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import "ZPPaymentDatabase.h"
#import "ZPTableProtocol.h"
#import "ZPSchemeTable.h"
#import <Foundation/Foundation.h>
#import <ReactiveObjC/ReactiveObjC.h>

@interface ZPPaymentDatabase()
@property(nonatomic, strong) ZPSchemeTable *schemeTable;
@property (nonatomic, strong) NSMutableSet<Class<ZPTableProtocol>>* tables;
@end

@implementation ZPPaymentDatabase
- (id) init {
    if (self = [super init]) {
        self.tables = [NSMutableSet new];
        self.schemeTable = [[ZPSchemeTable alloc] initWithDB:self];
        
        // Register scheme table
        [self registerTable:[ZPSchemeTable self]];
    }
    return self;
}

- (void) registerTable:(Class<ZPTableProtocol>) table {
    [self.tables addObject:table];
}

- (void) registerTables:(NSArray<Class<ZPTableProtocol>>*) tables {
    [self.tables addObjectsFromArray:tables];
}

- (NSSet<Class<ZPTableProtocol>>*) getTables {
    return [self.tables copy];
}

//- (void) createDictionaryTable:(FMDatabase *)db {
//    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS Dictionary(key text primary key, value text);"];
//}
//
//- (void) addTableToDictionary:(ZPTableInfo*)tableInfo withDB:(FMDatabase *)db {
//    NSString *query = @"INSERT OR REPLACE INTO Dictionary (key, value) VALUES (?, ?)";
//    [db executeUpdate:query, tableInfo.tableName, tableInfo.canDropWhenUpgrade ? @"TRUE" : @"FALSE"];
//}
//
//- (void) addTableToDictionary:(NSString*)tableName canDropWhenUpgrade:(BOOL)canDropWhenUpgrade withDB:(FMDatabase *)db {
//    NSString *query = @"INSERT OR REPLACE INTO Dictionary (key, value) VALUES (?, ?)";
//    [db executeUpdate:query, tableName, canDropWhenUpgrade ? @"TRUE" : @"FALSE"];
//}

#pragma mark - TableConfig

- (void)updateScheme:(NSString *)scheme {
    [self.schemeTable updateScheme:scheme];
}

- (NSString *)dbScheme {
    return [self.schemeTable dbScheme];
}

@end
