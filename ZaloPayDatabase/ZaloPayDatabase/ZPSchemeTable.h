//
//  ZPSchemeTable.h
//  ZaloPayDatabase
//
//  Created by tridm2 on 4/10/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import "ZPBaseTable.h"

@interface ZPSchemeTable : ZPBaseTable
- (void)updateScheme:(NSString *)scheme;
- (NSString *)dbScheme;
@end
