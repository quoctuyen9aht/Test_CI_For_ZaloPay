//
//  GlobalDatabase.h
//  ZaloPayDatabase
//
//  Created by bonnpv on 6/3/16.
//  Copyright © 2016 bonnpv. All rights reserved.
//

#import "ZPPaymentDatabase.h"
#import "PaymentDatabase+SQL.h"

@interface GlobalDatabase : ZPPaymentDatabase
+ (instancetype)sharedInstance;
@end
