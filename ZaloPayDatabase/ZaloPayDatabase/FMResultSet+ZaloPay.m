//
//  FMResultSet+ZaloPay.m
//  ZaloPayDatabase
//
//  Created by bonnpv on 4/2/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "FMResultSet+ZaloPay.h"

@implementation FMResultSet (ZaloPay)
- (NSString *)stringForColumn:(NSString *)columnName defaultValue:(NSString *)defaultValue {
    NSString *value = [self stringForColumn:columnName];
    if (value) {
        return value;
    }
    return defaultValue;
}

- (int)intForColumn:(NSString *)columnName defaultValue:(int)defaultValue {
    NSString *value = [self stringForColumn:columnName];
    if (value) {
        return [value intValue];
    }
    return defaultValue;
}
@end
