//
//  FMEncryptHelper.h
//  ZaloPay
//
//  Created by vuongvv on 12/5/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <SQLCipher/sqlite3.h>

@interface ZPFMDatabaseEncryptHelper : NSObject
+ (void)encrypted:(NSString *)databasePath;

+ (BOOL)databaseWhetherEncrypted:(NSString *)databasePath key:(NSString *)key;

+ (BOOL)encryptDatabase:(NSString *)path key:(NSString *)key;

+ (BOOL)openDB:(sqlite3 *)db path:(NSString *)path encryptedKey:(NSString *)encryptedKey;

+ (void)deleteDatabase:(NSString *)databasePath;

+ (NSString *)newKey;

+ (NSString *)oldKey;
@end
