//
//  ZPPaymentDatabase.h
//  ZaloPayDatabase
//
//  Created by tridm2 on 3/20/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import "ZPTableProtocol.h"
#import "PaymentDatabase.h"
#import "PaymentDatabase+SQL.h"
#import <fmdb/FMDatabase.h>
#import <fmdb/FMDatabaseQueue.h>

@class RACSignal;

@interface ZPPaymentDatabase : PaymentDatabase
- (void) registerTable:(Class<ZPTableProtocol>) table;
- (void) registerTables:(NSArray<Class<ZPTableProtocol>>*) tables;
- (NSSet<Class<ZPTableProtocol>>*) getTables;

//- (void) createDictionaryTable:(FMDatabase *)db;
//- (void) addTableToDictionary:(ZPTableInfo*)tableInfo withDB:(FMDatabase *)db;
//- (void) addTableToDictionary:(NSString*)tableName canDropWhenUpgrade:(BOOL)canDropWhenUpgrade withDB:(FMDatabase *)db;

- (NSString *)dbScheme;
- (void)updateScheme:(NSString *)scheme;
@end
