//
//  PerUserDataBase+External.h
//  ZaloPayDatabase
//
//  Created by bonnpv on 6/3/16.
//  Copyright © 2016 bonnpv. All rights reserved.
//

#import "PerUserDataBase.h"

@interface PerUserDataBase (External)
//- (void)createTableTransactionLog:(FMDatabase *)db;

//- (void)createTableManifest:(FMDatabase *)db;

//- (void)createTableProfileInfomation:(FMDatabase *)db;

//- (void)createTableNotify:(FMDatabase *)db;

//- (void)createTableCacheData:(FMDatabase *)db;

/**
 will keep data with key in table CacheData after update new version
 */

//- (void)removeAllCacheDataButKeepDataWithKey:(NSString *)key;

//- (void)createTableZaloFriend:(FMDatabase *)db;

//- (void)createTableRecentTransfer:(FMDatabase *)db;

//- (void)createLixiNotificationTable:(FMDatabase *)db;

//- (void)createTableMerchantUserInfor:(FMDatabase *)db;

//- (void)createTableUserContact:(FMDatabase *)db;

//- (void)createTableZaloPayInfo:(FMDatabase *)db;

//- (void)createTableTrackingAppTransid:(FMDatabase *)db;

//- (void)createTableAppTranIdStep:(FMDatabase *)db;

//- (void)createTableAppTranIdAPI:(FMDatabase *)db;

//- (void)createTableUnSavePhoneContactInfo:(FMDatabase *)db;

//- (void)createTableSaveModeKeyboard:(FMDatabase *)db;
@end
