//
//  GlobalDatabase.m
//  ZaloPayDatabase
//
//  Created by bonnpv on 6/3/16.
//  Copyright © 2016 bonnpv. All rights reserved.
//

#import "GlobalDatabase.h"
#import "GlobalDatabase+External.h"

extern NSString *const kGlobalDataBaseScheme;

@implementation GlobalDatabase

+ (instancetype)sharedInstance {
    static GlobalDatabase *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[GlobalDatabase alloc] init];
    });
    return instance;
}

- (NSString *)dataBaseName {
    return @"ZaloPayGlobal";
}

- (NSString *)currentScheme {
    return kGlobalDataBaseScheme;
}

- (void)createAllTable:(FMDatabase *)db {
    NSSet* tables = [self getTables];
    if ([tables count] == 0) {
        return;
    }
    
    for (Class<ZPTableProtocol> table in tables) {
        [table createTable:db];
    }
    
//    [self tbReactCreateTable:db];
//    [self createTableCacheData:db];
//    [self createTableMarkDownloadApp:db];
}
@end
