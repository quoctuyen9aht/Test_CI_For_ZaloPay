//
//  ZPDataManifestTable.h
//  ZaloPay
//
//  Created by bonnpv on 5/4/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPBaseTable.h"

@interface ZPDataManifestTable : ZPBaseTable

- (void)updateBalance:(long)balance;

- (long)getBalance;

@end
