//
//  PaymentDatabase.m
//  ZaloPayCommon
//
//  Created by bonnpv on 6/3/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "PaymentDatabase.h"
#import <fmdb/FMDatabase.h>
#import <fmdb/FMDatabaseQueue.h>
#import "ZaloPayDatabaseLog.h"
#import "ZPFMDatabaseEncryptHelper.h"


@implementation PaymentDatabase

- (BOOL)createDBWithConfigVersion {
    NSString *filePath = [self databasePath];

    if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        [self createDataBaseAtPath:filePath];
        return TRUE;
    }

    [self loadDatabaseWithPath:filePath];
    NSString *dbScheme = [self dbScheme];
    NSString *currentCheme = [self currentScheme];

    if (![dbScheme isEqualToString:currentCheme]) {
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
        [self createDataBaseAtPath:filePath];
        return TRUE;
    }
    return FALSE;
}

- (void)createDataBaseAtPath:(NSString *)filePath {
    [self loadDatabaseWithPath:filePath];
    [self createAllTable];
    [self updateScheme:[self currentScheme]];
}

- (void)loadDatabaseWithPath:(NSString *)filePath {
    [self closeDataBase];

    //Check encrypt
    [self encryptionDB:filePath];
    self.dbQueue = [FMDatabaseQueue databaseQueueWithPath:filePath];
    //Open with key
    [self.dbQueue inDatabase:^(FMDatabase *_Nonnull db) {
        [db setKey:[ZPFMDatabaseEncryptHelper newKey]];
    }];
}

- (void)closeDataBase {
    [self.dbQueue close];
    self.dbQueue = nil;
}

- (NSString *)databasePath {
    NSString *name = [self dataBaseName];
    return [self buildDatabasePathWithName:name];
}

- (NSString *)buildDatabasePathWithName:(NSString *)name {
    NSString *cachesDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *folderPath = [cachesDirectory stringByAppendingPathComponent:@"Databases"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:folderPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:folderPath
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:nil];
    }

    NSString *filePath = [folderPath stringByAppendingString:[NSString stringWithFormat:@"/%@.db", name]];
    DDLogInfo(@"DB path: %@", filePath);
    return filePath;

}

- (void)createAllTable {
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        [self createAllTable:db];
    }];
}

- (void)encryptionDB:(NSString *)DBPath {
    [ZPFMDatabaseEncryptHelper encrypted:DBPath];
}

#pragma mark - To Override

- (NSString *)dataBaseName {
    return @"DefaultName";
}

- (NSString *)currentScheme {
    return @"1";
}

- (void)createAllTable:(FMDatabase *)db {

}

- (NSString *)dbScheme {
    return @"1";
}

- (void)updateScheme:(NSString *)scheme {
    
}

@end
