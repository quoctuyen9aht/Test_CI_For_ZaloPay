//
//  ZPSchemeTable.m
//  ZaloPayDatabase
//
//  Created by tridm2 on 4/10/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import "ZPSchemeTable.h"
#import <fmdb/FMDatabase.h>
#import <fmdb/FMDatabaseQueue.h>

#define kSchemeKey      @"scheme"

@implementation ZPSchemeTable

+ (NSArray<NSString*>*) creatingTableQuery {
    NSString* tableScheme = @"CREATE TABLE IF NOT EXISTS Scheme(key text primary key, value text);";
    return @[tableScheme];
}

- (void)updateScheme:(NSString *)scheme {
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        [db executeUpdate:@"INSERT OR REPLACE INTO Scheme (key, value) VALUES (?, ?)", kSchemeKey, scheme];
    }];
}

- (NSString *)dbScheme {
    __block NSString *scheme = @"";
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSError *error = nil;
        FMResultSet *rs = [db executeQuery:@"SELECT value FROM Scheme WHERE key=?" values:@[kSchemeKey] error:&error];
        if ([rs next]) {
            scheme = [rs stringForColumn:@"value"];
        }
        [rs close];
    }];
    return scheme;
}

@end
