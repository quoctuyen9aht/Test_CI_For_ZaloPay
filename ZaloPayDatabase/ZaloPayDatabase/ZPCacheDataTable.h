//
//  ZPCacheDataTable.h
//  ZaloPay
//
//  Created by bonnpv on 6/16/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <fmdb/FMDatabase.h>
#import <fmdb/FMDatabaseQueue.h>
#import "ZPBaseTable.h"

#define kZaloPayUserInfo                @"ZaloPayUserInfo"
#define kDisableUpdateProfileLevel3     @"DisableUpdateProfileLevel3"
#define kLastTimeSyncContact            @"LastTimeSyncContact"
#define kAskForPassword                 @"AskForPassword"
#define kTurnOffTouchIdManual           @"TurnOffTouchIdManual"

#define kApplePushNotifyToken           @"ApplePushNotifyToken"
#define kLastLoginUser                  @"LastLoginUser"
#define kUserLocation                   @"UserLocation"
#define kZPUserLoginDataKey             @"ZPUserLoginDataKey"

#define kBankViewLastActiveTab          @"BankViewLastActiveTab"
#define kLastTimeShowSuggestTouchIdKey  @"LastTimeShowSuggestTouchIdKey"
#define kDoneShowSuggestTouchId         @"DoneShowSuggestTouchId"
#define kLastTimeLoadZaloPayInfoFromPhome           @"LastTimeLoadZaloPayInfoFromPhome"
#define kPaymentCodeShowSecurityIntro   @"PaymentCodeShowSecurityIntro"
#define kPaymentCodeChannelSelect       @"PaymentCodeChannelSelect"
#define kAddSplashScreen                @"AddSplashScreen"
#define kAuthenticationOpen             @"AuthenticationOpen"
#define kAutoLockTime                   @"AutoLockTime"

@interface ZPCacheDataTable : ZPBaseTable

- (void)cacheDataUpdateValue:(NSString *)value forKey:(NSString *)key;

- (NSString *)cacheDataValueForKey:(NSString *)key;

- (void)cacheDataRemoveDataForKey:(NSString *)key;

- (void)removeAllCacheDataButKeepDataWithKey:(NSString *)key;

@end
