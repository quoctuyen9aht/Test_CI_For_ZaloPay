//
//  ZPWebViewJSBase + Extension.h
//  ZaloPay
//
//  Created by Dung Vu on 2/27/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <zwebapp/ZPWebViewJSBase.h>
@class RACSignal;
@interface ZPWebViewJSBase(Extension)
- (RACSignal *)evaluteStringJS: (NSString*) jsString;
@end
