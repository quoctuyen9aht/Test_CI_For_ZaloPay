//
//  ZPWebViewJSBase + Extension.m
//  ZaloPay
//
//  Created by Dung Vu on 2/27/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPWebViewJSBase + Extension.h"
#import <ReactiveObjC/ReactiveObjC.h>
#import <objc/runtime.h>

@implementation ZPWebViewJSBase(Extension)
- (RACSignal *)evaluteStringJS:(NSString *)jsString {
    @weakify(self);
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self);
        [self evaluteStringJS:jsString completionHandler:^(id objectResponse, NSError *error) {
            if (error) {
                [subscriber sendError:error];
            }else {
                [subscriber sendNext:objectResponse];
                [subscriber sendCompleted];
            }
        }];
        
        return nil;
    }];
}

@end
