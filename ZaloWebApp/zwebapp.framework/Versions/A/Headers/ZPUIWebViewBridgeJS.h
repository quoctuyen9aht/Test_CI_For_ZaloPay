//
//  ZPUIWebViewBridgeJS.h
//  jsExample
//
//  Created by Dung Vu on 2/23/17.
//  Copyright © 2017 VNG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ZPWebViewJSBase.h"

/**
 Class for handler JS in UIWebView
 */
@interface ZPUIWebViewBridgeJS : ZPWebViewJSBase

/**
 Create handler js in UIWebview

 @param webView UIWebView
 @param callback handler value response
 @return a instance bridge
 */
+ (instancetype)bridge: (UIWebView *)webView using:(ZPWebJSCallback) callback;
@end
