//
//  ZPUIWebViewDelegateProxy.h
//  jsExample
//
//  Created by Dung Vu on 2/23/17.
//  Copyright © 2017 VNG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 Class Keep Delegate and move action handler for other
 */
@interface ZPUIWebViewDelegateProxy : NSObject<UIWebViewDelegate>
@property (weak, nonatomic) id<UIWebViewDelegate> original;
@property (weak, nonatomic) id<UIWebViewDelegate> proxy;

/**
 Init Proxy

 @param original original delegate from UIWebview
 @param proxy delegate that want to handler first
 @return a type of proxy
 */
- (instancetype)init: (id<UIWebViewDelegate>) original proxy: (id<UIWebViewDelegate>) proxy;
@end
