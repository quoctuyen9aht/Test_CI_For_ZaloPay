//
//  ZPWebViewJSBase.h
//  jsExample
//
//  Created by Dung Vu on 2/23/17.
//  Copyright © 2017 VNG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Config.h"

@interface ZPWebViewJSBase : NSObject
@property (copy, nonatomic) ZPWebJSCallback callback;

/**
 Create js string from file ios_jsbridge.js
 
 @return string encoding utf8
 */
- (NSString *)makeDefaultScript;
/**
 Run js script

 @param js string
 @param handler handle result
 */
- (void)evaluteStringJS:(NSString *)js completionHandler: (ZPWebJSCallback) handler;
@end
