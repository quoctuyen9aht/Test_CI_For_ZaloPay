//
//  ZPWKWebViewBridgeJS.h
//  jsExample
//
//  Created by Dung Vu on 2/23/17.
//  Copyright © 2017 VNG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZPWebViewJSBase.h"
#import <WebKit/WebKit.h>

@interface ZPWKWebViewBridgeJS : ZPWebViewJSBase
/**
 Create handler js in WKWebView
 
 @param webView WKWebView
 @param callback handler value response
 @return a instance bridge
 */
+ (instancetype)bridge:(WKWebView *)webView using:(ZPWebJSCallback) callback;
@end
