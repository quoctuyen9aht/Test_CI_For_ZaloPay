//
//  TransactionLogsConst.h
//  ZaloPayAppManager
//
//  Created by Mr Bu on 4/10/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#ifndef TransactionLogsConst_h
#define TransactionLogsConst_h

typedef enum {
    TransOrderLastest = 1,
    TransOrderOldest = -1,
} TransOrder;
typedef enum {
    StatusTypeSuccess = 1,
    StatusTypeFail = 2
} StatusType;

//NSTimeInterval const  MainKeyTimestamp = 0.0;
#define MainKeyTimestamp 0.0

#endif /* TransactionLogsConst_h */
