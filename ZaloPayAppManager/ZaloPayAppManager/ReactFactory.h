//
//  ReactFactory.h
//  ZaloPay
//
//  Created by Nguyễn Hữu Hoà on 3/26/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class RCTBridge;

@interface ReactFactory : NSObject
@property(nonatomic, strong, readonly) RCTBridge *bridge;

+ (instancetype)sharedInstance;

//! Register react native module
//! @return: module instance id. Need when close the module
- (NSInteger)registerModuleInstance:(UIViewController *)viewController;

//! Close viewcontroler of registered module
- (void)closeModuleInstance:(NSInteger)instanceId;

- (void)removeModuleInstnce:(NSInteger)instanceId;

//! Register and pre-load the js for module
- (RCTBridge *)registerPaymentApp:(NSInteger)appId;

////! Get bridge for module
//- (RCTBridge *)bridgeForApp:(NSInteger)appId;

- (void)releaseBridge:(NSInteger)appId;

- (void)reloadAllViewWithAppId:(NSInteger)appId;

- (void)replaceModuleInstance:(NSInteger)instanceId
           withViewController:(UIViewController *)viewController
           fromViewController:(UIViewController *)fromViewController;
- (void)clearAllData;
@end
