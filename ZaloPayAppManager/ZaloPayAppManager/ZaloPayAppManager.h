//
//  ZaloPayAppManager.h
//  ZaloPayAppManager
//
//  Created by Phuoc Huynh on 3/23/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ZaloPayAppManager.
FOUNDATION_EXPORT double ZaloPayAppManagerVersionNumber;

//! Project version string for ZaloPayAppManager.
FOUNDATION_EXPORT const unsigned char ZaloPayAppManagerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ZaloPayAppManager/PublicHeader.h>


