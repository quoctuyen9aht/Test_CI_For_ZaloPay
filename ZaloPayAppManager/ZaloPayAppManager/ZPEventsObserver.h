//
//  ZPEventsObserver.h
//  ZaloPayAppManager
//
//  Created by Nguyễn Hữu Hoà on 7/21/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTEventEmitter.h>

@interface ZPEventsObserver : RCTEventEmitter

@end
