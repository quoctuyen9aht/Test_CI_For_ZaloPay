//
//  ZaloPayApi.m
//  ZaloPay
//
//  Created by Nguyễn Hữu Hoà on 3/31/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZaloPayApi.h"
#import <AFNetworking/AFNetworking.h>
#import <ZaloPayCommon/NSCollection+JSON.h>
#import <ZaloPayUI/ZPAppFactory.h>
#import <ZaloPayUI/ZPTrackingHelper.h>
#import <ZaloPayCommon/UIFont+IconFont.h>
#import <ZaloPayUI/ZPDialogView.h>
#import <ZaloPayCommon/UIFont+Extension.h>
#import <ZaloPayNetwork/ZPReactNetwork.h>
#import <ZaloPayNetwork/NetworkManager.h>
#import <ZaloPayAnalyticsSwift/ZaloPayAnalyticsSwift-Swift.h>
#import "PaymentModulLog.h"
#import "ReactFactory.h"
#import "ZaloPayApiHelper.h"
#import "ReactModuleViewController.h"
//#import <ApplicationState.h>

extern NSString *termsOfUseUrl;
extern NSString *faqUrl;
extern NSString *supportCenterUrl;
extern NSString *kLixiApiUrl;

@interface ZaloPayApi ()
@end


@implementation ZaloPayApi
RCT_EXPORT_MODULE()

- (instancetype)init {
    self = [super init];
    return self;
}

RCT_EXPORT_METHOD(setHiddenTabbar:
    (BOOL) hiddenTabbar) {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[ZPAppFactory sharedInstance] setHiddenTabbar:hiddenTabbar];
    });
}

RCT_EXPORT_METHOD(transferMoney:
    (NSString *) userid
        :(NSString *)amount
        :(NSString *)description
        :(NSString *)transferMode
        resolver:(RCTPromiseResolveBlock)resolve
        rejecter:(RCTPromiseRejectBlock)reject) {

    dispatch_async(dispatch_get_main_queue(), ^{
        NSInteger money = [amount integerValue];
        [[ZPAppFactory sharedInstance] transferMoney:userid amount:money message:description transferMode:[NSString stringWithFormat:@"%@", transferMode] completeBlock:^(NSDictionary *data) {
            if (resolve) {
                NSMutableDictionary *_return = [NSMutableDictionary dictionary];
                [_return setObject:@"1" forKey:@"code"];
                [_return setObjectCheckNil:data forKey:@"data"];
                resolve(_return);
            }
        }                                cancelBlock:^(NSDictionary *data) {
            if (reject) {
                reject(@"-1", @"cancel", nil);
            }
        }                                 errorBlock:^(NSDictionary *data) {
            if (reject) {
                reject(@"-1", @"error", nil);
            }
        }];
    });
}

RCT_EXPORT_METHOD(request:
    (NSString *) url
        :(NSDictionary *)requestOption
        resolver:(RCTPromiseResolveBlock)resolve
        rejecter:(RCTPromiseRejectBlock)reject) {

    DDLogDebug(@"url = %@", url);
    DDLogDebug(@"request option = %@", requestOption);

    NSString *method;
    NSDictionary *headers;
    NSString *body;
    NSDictionary *preQuery;

    if ([requestOption isKindOfClass:[NSDictionary class]]) {
        method = [requestOption stringForKey:@"method" defaultValue:@"GET"];
        headers = [requestOption dictionaryForKey:@"headers" defaultValue:@{}];
        body = [requestOption stringForKey:@"body" defaultValue:@""];
        preQuery = [requestOption dictionaryForKey:@"query" defaultValue:@{}];
    }
    NSMutableDictionary *query = [NSMutableDictionary dictionaryWithDictionary:preQuery];
    [query setObjectCheckNil:[[NetworkManager sharedInstance] accesstoken] forKey:@"accesstoken"];
    [query setObjectCheckNil:[[NetworkManager sharedInstance] paymentUserId] forKey:@"zalopayid"];


    ZPReactNetwork *client = [[ZPReactNetwork alloc] init];

    [[client requestWithMethod:method
                        header:headers
                     urlString:url
                        params:query
                          body:body] subscribeNext:^(id x) {
        if (resolve) {
            resolve(x);
        }
    }                                        error:^(NSError *error) {
        if (reject) {
            NSDictionary *data = error.userInfo;
            NSString *code = [[data numericForKey:@"error_code"] stringValue];
            NSString *message = [data stringForKey:@"error_message"];
            reject(code, message, error);
        }
    }];
}


RCT_EXPORT_METHOD(navigateBankAccount) {
    DDLogInfo(@"mapBankAccout");
    dispatch_async(dispatch_get_main_queue(), ^{
        [[ZPAppFactory sharedInstance] showBankView];
    });
}

RCT_EXPORT_METHOD(navigateCardList) {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[ZPAppFactory sharedInstance] showBankView];
    });
}

RCT_REMAP_METHOD(loadFontAsync,
            loadAsyncWithFontFamilyName:
            (NSString *) fontFamilyName
            withLocalUri:
            (NSURL *) uri
            resolver:
            (RCTPromiseResolveBlock) resolve
            rejecter:
            (RCTPromiseRejectBlock) reject) {

    NSString *path = [uri path];
    NSData *inData = [NSData dataWithContentsOfURL:uri];
    if (!inData) {
        reject(@"E_FONT_FILE_NOT_FOUND",
                [NSString stringWithFormat:@"File '%@' for font '%@' doesn't exist", path, fontFamilyName],
                nil);
        return;
    }
    RACSignal *signal = [RACSignal createSignal:^RACDisposable *(id <RACSubscriber> subscriber) {
        [UIFont registerFontWithData:inData
                          subscriber:subscriber];
        return nil;
    }];

    [signal subscribeError:^(NSError *error) {
        reject(@"E_FONT_CREATION_FAILED",
                error.localizedDescription,
                nil);
    }            completed:^{
        resolve(nil);
    }];
}

RCT_EXPORT_METHOD(payOrder:
    (NSString *) notificationId) {
    [[ZPAppFactory sharedInstance] payMerchantBillFromNotification:notificationId];
}

RCT_EXPORT_METHOD(payOrderWithParams:
    (NSDictionary *) param
            resolver:
            (RCTPromiseResolveBlock) resolve
            rejecter:
            (RCTPromiseRejectBlock) reject) {
    [ZaloPayApiHelper payOrder:param resolver:resolve rejecter:reject];
}

RCT_EXPORT_METHOD(popLastViewController) {
    DDLogInfo(@"Request to popLastViewController");
    dispatch_async(dispatch_get_main_queue(), ^{
        UINavigationController *navigationController = [[ZPAppFactory sharedInstance] rootNavigation];
        [navigationController popViewControllerAnimated:YES];
    });
}


RCT_EXPORT_METHOD(navigateIntro:
    (NSString *) title) {
}

RCT_EXPORT_METHOD(closeModule:
    (NSInteger) moduleId) {
    DDLogInfo(@"Request to close module %@", @(moduleId));
    dispatch_async(dispatch_get_main_queue(), ^{
        [[ReactFactory sharedInstance] closeModuleInstance:moduleId];
    });
}

RCT_EXPORT_METHOD(logError:
    (NSString *) message) {
    DDLogError(@"%@", message);
}

RCT_EXPORT_METHOD(navigateProfile) {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[ZPAppFactory sharedInstance] showUserProfileViewController];
//        [[ZPAppFactory sharedInstance] launchWithRouterId:RouterIdUserProfile from:nil animated:YES title:nil];
//        [[ZPAppFactory sharedInstance] promptPIN:ReactNativeChannelType_Profile callback:^(id result) {
//            UINavigationController *navigationController = [[ZPAppFactory sharedInstance] rootNavigation];
//            ZPMainProfileViewController *viewController = [[ZPMainProfileViewController alloc] init];
//            [navigationController pushViewController:viewController animated:YES];
//        } error:^(NSError *error) {
//
//        }];
    });
}

RCT_EXPORT_METHOD(navigateProtectAccount) {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[ZPAppFactory sharedInstance] navigateProtectAccount];
    });
}

RCT_EXPORT_METHOD(launchContactList:
    (NSString *) currentPhone
            viewMode:
            (NSString *) viewMode
            navigationTitle:
            (NSString *) navigationTitle
            resolver:
            (RCTPromiseResolveBlock) resolve
            rejecter:
            (RCTPromiseRejectBlock) reject) {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[ZPAppFactory sharedInstance] launchContactList:currentPhone viewMode:viewMode navigationTitle:navigationTitle completeHandle:^(NSDictionary *data) {
            if (resolve) {
                resolve(data);
            }
        }];
    });
}
    
RCT_EXPORT_METHOD(trackEvent:
                  (NSInteger) eventId) {
    [[ZPTrackingHelper shared].eventTracker trackEvent:eventId value:nil label:nil];
}


RCT_EXPORT_METHOD(trackEventApi:
    (NSInteger) eventId :(NSString *)eventLabel)  {
    [[ZPTrackingHelper shared].eventTracker trackEvent:eventId value:nil label:eventLabel];
}

RCT_EXPORT_METHOD(trackEventWithIdAndValue:
    (NSInteger) eventId :(NSNumber *)eventValue) {
    [[ZPTrackingHelper shared].eventTracker trackEvent:eventId value:eventValue label:nil];
}

RCT_EXPORT_METHOD(trackScreen:
    (NSString*) screenName) {
    [[ZPTrackingHelper shared].eventTracker trackScreen:screenName];
}

RCT_EXPORT_METHOD(showDialog:
    (int) dialogType
            title:
            (NSString *) title
            message:
            (NSString *) message
            buttonTitles:
            (NSArray *) buttonTitles
            resolver:
            (RCTPromiseResolveBlock) resolve
            rejecter:
            (RCTPromiseRejectBlock) reject) {

    dispatch_async(dispatch_get_main_queue(), ^{
        
//        if([self isReactViewActive] == false) {
//            return;
//        }
//
        NSString *msg = message;
        if (msg.length > 0) {
            NSString *fontStyle = [NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%dpx;}</style>",
                                   @"SFUIText-Regular",
                                   16];
            msg = [msg stringByAppendingString:fontStyle];
        }

        MMPopupItemHandler handler = ^(NSInteger index) {
            if (resolve) {
                NSDictionary *data = @{@"code": @(index)};
                resolve(data);
            }
        };

        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[msg dataUsingEncoding:NSUTF8StringEncoding]
                                                                                options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                        NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                                     documentAttributes:nil error:nil];
        [ZPDialogView showDialogWithType:dialogType
                                   title:title
                        attributeMessage:attributedString
                            buttonTitles:buttonTitles
                                 handler:handler];
    });
}

//- (BOOL)isReactViewActive {
//    UINavigationController *nav = [[ZPAppFactory sharedInstance] rootNavigation];
//    if (![nav isKindOfClass:[UINavigationController class]]) {
//        return false;
//    }
//    return [nav.topViewController isKindOfClass:[ReactModuleViewController class]];    
//}

RCT_EXPORT_METHOD(showLoading) {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[ZPAppFactory sharedInstance] showHUDAddedTo:[[[UIApplication sharedApplication] delegate] window]];
    });
}

RCT_EXPORT_METHOD(hideLoading) {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[ZPAppFactory sharedInstance] hideAllHUDsForView:[[[UIApplication sharedApplication] delegate] window]];
    });
}

RCT_EXPORT_METHOD(promptPIN:
    (int) channel
            resolve:
            (RCTPromiseResolveBlock) resolve
            rejecter:
            (RCTPromiseRejectBlock) reject) {
    dispatch_async(dispatch_get_main_queue(), ^{

        [[ZPAppFactory sharedInstance] promptPIN:channel callback:^(id result) {
            NSDictionary *back = @{@"code": result};
            resolve(back);
        }                                  error:^(NSError *error) {

        }];
    });
}

- (NSDictionary *)constantsToExport {
    return @{@"termsOfUseUrl": termsOfUseUrl,
            @"faqUrl": faqUrl,
            @"storeUrl": @"https://zalopay.com.vn/gioi-thieu/",
            @"supportCenterUrl": supportCenterUrl,
            @"redPacketApiUrl": kLixiApiUrl};
}

RCT_EXPORT_METHOD(navigateFeedBack) {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[ZPAppFactory sharedInstance] showFeedbackViewController];
    });
}

#pragma mark - voucher list

RCT_EXPORT_METHOD(navigateVoucherList) {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[ZPAppFactory sharedInstance] showVoucherView];

    });
}

RCT_EXPORT_METHOD(shareViaSocial:
    (NSString *) imageUrl
            caption:
            (NSString *) caption) {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (imageUrl != nil && imageUrl.length > 0) {
            UIImage *image = [UIImage imageWithContentsOfFile:imageUrl];
            UIViewController *topViewController = [ZPAppFactory topMostController];
            [[ZPAppFactory sharedInstance] beginSharing:topViewController image:image withContent:caption andLink:nil];

//            NSArray *objectsToShare = @[caption,image];
//            UIActivityViewController * lixiShare = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
//            lixiShare.excludedActivityTypes = @[UIActivityTypeSaveToCameraRoll];
//            lixiShare.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionRight;
//            [[ZPAppFactory topMostController] presentViewController:lixiShare animated: YES completion:nil];
        }
    });
}

RCT_EXPORT_METHOD(shareMessageViaSocial:
    (NSString *) message
            caption:
            (NSString *) caption) {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIViewController *topViewController = [ZPAppFactory topMostController];
        [[ZPAppFactory sharedInstance] beginSharing:topViewController withMessage:message andCaption:caption];
    });
}


RCT_EXPORT_METHOD(shareFanPagePost:
    (RCTPromiseResolveBlock) resolve
            rejecter:
            (RCTPromiseRejectBlock) reject) {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIViewController *topViewController = [ZPAppFactory topMostController];
        [[ZPAppFactory sharedInstance] shareFanpageOnFacebook:topViewController callback:^(NSInteger result) {
            if (resolve) {
                NSDictionary *data = @{@"code": @(result)};
                resolve(data);
            }
        }];
    });
}
RCT_EXPORT_METHOD(launchWebView:(NSString *) url) {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[ZPAppFactory sharedInstance] launchWebApp:url];
    });
}
@end

