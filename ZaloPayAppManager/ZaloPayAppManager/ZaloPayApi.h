//
//  ZaloPayApi.h
//  ZaloPay
//
//  Created by Nguyễn Hữu Hoà on 3/31/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface ZaloPayApi : NSObject <RCTBridgeModule>

@end
