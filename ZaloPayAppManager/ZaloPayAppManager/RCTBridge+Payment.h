//
//  RCTBridge+Payment.h
//  ZaloPayAppManager
//
//  Created by bonnpv on 5/19/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <React/RCTBridge.h>

@interface RCTBridge (Payment)
+ (instancetype)internalBridge;

+ (instancetype)externalBridge:(NSInteger)appId;
@end
