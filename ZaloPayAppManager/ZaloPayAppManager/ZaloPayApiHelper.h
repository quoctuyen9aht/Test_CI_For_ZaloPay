//
//  ZaloPayHelper.h
//  ZaloPayGateway
//
//  Created by Bon Bon on 12/18/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface ZaloPayApiHelper : NSObject
+ (void)payOrder:(NSDictionary *)param resolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject;
@end
