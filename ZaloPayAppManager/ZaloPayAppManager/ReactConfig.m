//
//  ReactConfig.m
//  ZaloPayAppManager
//
//  Created by bonnpv on 5/19/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ReactConfig.h"

static ReactConfig *_config;

@implementation ReactConfig

- (NSURL *)internalBundle {
    return nil;
}

- (NSURL *)externalBundle:(NSInteger)appId {
    return nil;
}

+ (ReactConfig *)config {
    return _config;
}

+ (void)setConfig:(ReactConfig *)config {
    _config = config;
}
@end
