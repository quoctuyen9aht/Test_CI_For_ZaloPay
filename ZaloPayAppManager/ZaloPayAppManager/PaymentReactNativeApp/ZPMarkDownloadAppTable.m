//
//  ZPMarkDownloadAppTable.m
//  PaymentReactNativeApp
//
//  Created by bonnpv on 7/12/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPMarkDownloadAppTable.h"
#import <fmdb/FMDatabase.h>
#import <fmdb/FMDatabaseQueue.h>

@implementation ZPMarkDownloadAppTable

+ (NSArray<NSString*>*) creatingTableQuery {
    NSString* tableCacheData = @"CREATE TABLE IF NOT EXISTS MarkDownloadApp(key int primary key, value int);";
    return @[tableCacheData];
}

+ (NSArray<NSString*>*) tableNamesCanBeDropped {
    return @[@"MarkDownloadApp"];
}

- (BOOL)isDownloaded:(NSInteger)appId {
    __block BOOL isDownload;
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:@"SELECT value FROM MarkDownloadApp WHERE key=?" values:@[@(appId)] error:nil];
        if ([rs next]) {
            isDownload = [rs intForColumn:@"value"] > 0;
        }
        [rs close];
    }];
    return isDownload;
}

- (void)setDoneDownloadApp:(NSInteger)appId state:(BOOL)isDownLoad {
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSNumber *value = isDownLoad == true ? @(1) : @(0);
        [db executeUpdate:@"INSERT OR REPLACE INTO MarkDownloadApp (key, value) VALUES (?, ?)", @(appId), value];
    }];
}
@end
