//
//  PerUserDataBase+Merchant.m
//  ZaloPayAppManager
//
//  Created by bonnpv on 9/19/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPMerchantTable.h"
#import <fmdb/FMDatabase.h>
#import <fmdb/FMDatabaseQueue.h>
#import <ZaloPayCommon/NSCollection+JSON.h>

#define kMerchantUserInfo           @"MerchantUserInfo"
#define kMuid                       @"muid"
#define kMDisplayName               @"displayname"
#define kBirthdate                  @"birthdate"
#define kUserGender                 @"usergender"
#define kMAccesstoken               @"maccesstoken"
#define kAppid                      @"appid"
#define kAvatar                     @"avatar"

@implementation ZPMerchantTable

+ (NSArray<NSString*>*) creatingTableQuery {
    NSString* tableMerchantUserInfo = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@(%@ int primary key,%@ text, %@ text,%@ text,%@ text,%@ text, %@ text);", kMerchantUserInfo, kAppid, kMuid, kMDisplayName, kBirthdate, kUserGender, kMAccesstoken, kAvatar];
    return @[tableMerchantUserInfo];
}

+ (NSArray<NSString*>*) tableNamesCanBeDropped {
    return @[kMerchantUserInfo];
}

- (ZPMerchantUserInfo *)getMerchantUserInfo:(NSInteger)appId {
    __block ZPMerchantUserInfo *_return = nil;
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE %@=?", kMerchantUserInfo, kAppid];
        FMResultSet *rs = [db executeQuery:query, @(appId)];
        while ([rs next]) {
            _return = [self merchantUserInfoFromResultSet:rs];
        }
        [rs close];
    }];
    return _return;
}

- (ZPMerchantUserInfo *)merchantUserInfoFromResultSet:(FMResultSet *)rs {
    NSString *muid = [rs stringForColumn:kMuid];
    NSString *displayname = [rs stringForColumn:kMDisplayName];
    NSString *birthday = [rs stringForColumn:kBirthdate];
    NSString *gender = [rs stringForColumn:kUserGender];
    NSString *accesstoken = [rs stringForColumn:kMAccesstoken];
    NSInteger appId = [rs intForColumn:kAppid];

    ZPMerchantUserInfo *_return = [[ZPMerchantUserInfo alloc] init];
    _return.appId = appId;
    _return.muid = muid;
    _return.displayName = displayname;
    _return.maccesstoken = accesstoken;
    _return.gender = gender;
    _return.birthday = birthday;
    return _return;
}

- (void)saverMerchanUserInfoWithAppId:(NSInteger)appId
                                 muid:(NSString *)muid
                          displayName:(NSString *)displayName
                         maccesstoken:(NSString *)maccesstoken
                               gender:(NSString *)gender
                             birthday:(NSString *)birthday
                               avatar:(NSString *)avatar {

    NSMutableDictionary *toSave = [NSMutableDictionary dictionary];
    [toSave setObjectCheckNil:@(appId) forKey:kAppid];
    [toSave setObjectCheckNil:muid forKey:kMuid];
    [toSave setObjectCheckNil:displayName forKey:kMDisplayName];
    [toSave setObjectCheckNil:maccesstoken forKey:kMAccesstoken];
    [toSave setObjectCheckNil:gender forKey:kUserGender];
    [toSave setObjectCheckNil:birthday forKey:kBirthdate];
    [toSave setObjectCheckNil:avatar forKey:kAvatar];
    [self.db replaceToTable:kMerchantUserInfo params:toSave];
}

- (void)clearMerchantUserInfo {
    [self.db clearDataInTable:kMerchantUserInfo];
}

- (NSDictionary *)allMerchantInfo {
    __block NSMutableDictionary *_return = [NSMutableDictionary dictionary];
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@", kMerchantUserInfo];
        FMResultSet *rs = [db executeQuery:query];
        while ([rs next]) {
            ZPMerchantUserInfo *info = [self merchantUserInfoFromResultSet:rs];
            [_return setObjectCheckNil:info forKey:@(info.appId)];
        }
    }];
    return _return;
}

- (NSDictionary *)checkMerchantUserInfoWithAppIds:(NSArray *)appIds {
    __block NSMutableDictionary *_return = [NSMutableDictionary dictionary];
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE %@ IN (%@)", kMerchantUserInfo, kAppid, [appIds componentsJoinedByString:@","]];
        FMResultSet *rs = [db executeQuery:query];
        while ([rs next]) {
            NSInteger oneAppId = [rs intForColumn:kAppid];
            [_return setObject:@(TRUE) forKey:@(oneAppId)];
        }
    }];
    return _return;
}

@end
