//
//  ZPReactNativeTable.h
//  PaymentReactNativeApp
//
//  Created by bonnpv on 5/16/16.
//  Copyright © 2016 bonnpv. All rights reserved.
//

//#import "DBManager.h"

@class FMDatabase;

#import "ReactAppModel.h"
#import <ZaloPayDatabase/ZPBaseTable.h>

#import <ZaloPayDatabase/GlobalDatabase.h>

@interface ZPReactNativeTable : ZPBaseTable

- (void)tbReactSaveApps:(NSArray *)apps;

- (void)saveOrderReactNativeApp:(NSArray *)activeApps;

- (ReactAppModel *)tbReactGetApp:(NSInteger)appId;

- (NSArray *)tbReactGetAppIdList;

- (NSArray *)orderReactNativeApp;

- (void)tbReactSaveAppIdList:(NSArray *)appIdList;

@end
