//
//  ReacNativeAppManager+DownloadData.m
//  PaymentReactNativeApp
//
//  Created by bonnpv on 5/18/16.
//  Copyright © 2016 bonnpv. All rights reserved.
//

#import "ReactNativeAppManager+DownloadData.h"
#import "ReactAppModel.h"
#import "PaymentModulLog.h"
#import "ZPMarkDownloadAppTable.h"
#import "ZPReactNativeTable.h"
#import <ZaloPayCommon/ZPConstants.h>
#import <ZaloPayCommon/NSArray+Safe.h>
#import <ZaloPayCommon/FileUtils.h>
#import <ReactiveObjC/ReactiveObjC.h>
#define kReactNativeAppFolder       @"ReactNativeApp"
#define kDownloadFolder             @"Download"
#define kRunFolder                  @"Run"

typedef void (^DowloadResult)(NSError *error);

@implementation ReactNativeAppManager (DownloadData)

// hàm này gọi sau khi request server trả về app update -> chỉ clear download folder + download lại,
// folder run giữ nguyên, tải xong update lại.
- (void)clearListDownloadedApp:(NSArray *)listApps {
    for (ReactAppModel *app  in listApps) {
        NSString *downloadFolder = [self getDownloadFolder:app];
        [FileUtils removeFolderAtPath:downloadFolder];
        [self.markDownloadAppTable setDoneDownloadApp:app.appid state:FALSE];
    }
}

// hàm này gọi khi nâng cấp version mới -> xoá toàn bộ dữ liệu cũ đi.
- (void)clearAllCacheApp {
    NSArray *listAppId = [self.reactNativeTable tbReactGetAppIdList];
    for (NSNumber *appId in listAppId) {
        ReactAppModel *app = [self getApp:[appId integerValue]];
        [self clearAppData:app];
    }
}

- (void)clearAppData:(ReactAppModel *)app {
    NSString *downloadFolder = [self getDownloadFolder:app];
    NSString *runFolder = [self getRunFolder:app];
    [FileUtils removeFolderAtPath:downloadFolder];
    [FileUtils removeFolderAtPath:runFolder];
    app.appState = ReactNativeAppStateNeedDownLoad;
    [self.markDownloadAppTable setDoneDownloadApp:app.appid state:FALSE];
}

- (BOOL)isDownLoadedApp:(ReactAppModel *)app {
    NSString *appFolder = [self getRunFolder:app];
    BOOL fileExist = [[NSFileManager defaultManager] fileExistsAtPath:appFolder];
    BOOL isDoneDownload = [self.markDownloadAppTable isDownloaded:app.appid];
    return fileExist && isDoneDownload;
}

- (RACSignal *)downloadAllActiveAppIgnoreApp:(NSArray *)ignoredAppIds {
    
    NSMutableArray *allAppIds = [self.activeAppIds mutableCopy];
    [allAppIds removeObjectsInArray:ignoredAppIds];
    
    NSMutableArray *toDownLoad = [NSMutableArray array];
    for (NSNumber *appId in allAppIds) {
        ReactAppModel *app = [[ReactNativeAppManager sharedInstance] getApp:[appId integerValue]];
        if (app.appType == ReactAppTypeWeb) {
            continue;
        }
        if ([ignoredAppIds containsObject:appId]) {
            continue;
        }
        if (app.appState == ReactNativeAppStateWillDownload ||
            app.appState == ReactNativeAppStateDownloading) {
            continue;
        }
        if ([self isDownLoadedApp:app]) {
            continue;
        }
        app.appState = ReactNativeAppStateWillDownload;
        [toDownLoad addObject:app];
    }
    return [self downloadListApp:toDownLoad];
}


- (RACSignal *)downloadListApp:(NSMutableArray *)listApp {
    return [RACSignal createSignal:^RACDisposable *(id <RACSubscriber> subscriber) {
        [self downloadListApp:listApp subscriber:subscriber];
        return nil;
    }];
}

- (void)downloadListApp:(NSMutableArray *)listApp subscriber:(id <RACSubscriber>)subscriber {
    if (listApp.count == 0) {
        [subscriber sendCompleted];
        return;
    }
    ReactAppModel *app = listApp.firstObject;
    [listApp safeRemoveObjectAtIndex:0];

    DDLogInfo(@"start download app with id : %@", @(app.appid));
    [[self downloadApp:app progress:^(float progress) {
//        DDLogInfo(@"downloading app with id : %@ percent :%f ",@(app.appid), progress);
        app.percentDownloaded = progress;
    }] subscribeError:^(NSError *error) {
        DDLogInfo(@"done download app :%d with error : %@ ", (int) app.appid, error);
        [self downloadListApp:listApp subscriber:subscriber];
    }       completed:^{
        DDLogInfo(@"done download app :%d", (int) app.appid);
        [self downloadListApp:listApp subscriber:subscriber];
    }];
}

- (RACSignal *)downloadApp:(ReactAppModel *)appModel progress:(DowloadProgressBlock)progress {
    return [[RACSignal createSignal:^RACDisposable *(id <RACSubscriber> subscriber) {
        [self downloadApp:appModel withSubscriber:subscriber progress:progress];
        return nil;
    }] replayLazily];
}


- (void)downloadApp:(ReactAppModel *)appModel
     withSubscriber:(id <RACSubscriber>)subcriber
           progress:(DowloadProgressBlock)progressHandle {

    appModel.appState = ReactNativeAppStateDownloading;

    NSString *downloadFolder = [self getDownloadFolder:appModel];

    [FileUtils removeFolderAtPath:downloadFolder];
    [FileUtils createFolderAtPath:downloadFolder];

    NSString *jsZipFile = [NSString stringWithFormat:@"%@/js.zip", downloadFolder];
    NSString *imageZipFile = [NSString stringWithFormat:@"%@/img.zip", downloadFolder];


    id downloadImageProgress = nil;
    id downloadJsProgress = nil;

    if (progressHandle) {
        __block float totalImageProgress = 0;
        __block float totalJsProgress = 0;

        downloadImageProgress = ^(float progress) {
            totalImageProgress = progress;
            progressHandle((totalImageProgress + totalJsProgress) / 2);
        };
        downloadJsProgress = ^(float progress) {
            totalJsProgress = progress;
            progressHandle((totalImageProgress + totalJsProgress) / 2);
        };
    }

    [[[[NetworkManager sharedInstance] downloadZipFileWithUrl:appModel.imageurl
                                                       toPath:imageZipFile
                                                  attractPath:downloadFolder
                                                     progress:downloadImageProgress] then:^RACSignal * {

        return [[NetworkManager sharedInstance] downloadZipFileWithUrl:appModel.jsurl
                                                                toPath:jsZipFile
                                                           attractPath:downloadFolder
                                                              progress:downloadJsProgress];

    }] subscribeError:^(NSError *error) {
        [FileUtils removeFolderAtPath:downloadFolder];
        appModel.appState = ReactNativeAppStateDownloadError;
        [subcriber sendError:error];
    }       completed:^{
        NSString *runPath = [self getRunFolder:appModel];
        [FileUtils copyFolderAtPath:downloadFolder toPath:runPath overrideOldFile:YES];
        [self.markDownloadAppTable setDoneDownloadApp:appModel.appid state:YES];
        appModel.appState = ReactNativeAppStateReadyToUse;
        [subcriber sendCompleted];
    }];
}

#pragma mark - Function

- (NSURL *)jsBundleWithAppId:(NSInteger)appId {
    ReactAppModel *app = [self getApp:appId];
    return [self jsBundleWithApp:app];
}

- (NSURL *)jsBundleWithApp:(ReactAppModel *)app {
    if (app) {
        NSString *appFolder = [self getRunFolder:app];
        NSString *jsPath = [NSString stringWithFormat:@"%@/main.jsbundle", appFolder];
        if ([[NSFileManager defaultManager] fileExistsAtPath:jsPath]) {
            return [NSURL fileURLWithPath:jsPath];
        }
        DDLogInfo(@"no file at path :%@", jsPath);
    }

    return nil;
}

- (NSURL *)fontWithAppId:(NSInteger)appId fileName:(NSString *)fileName {
    ReactAppModel *app = [self getApp:appId];
    if (app) {
        NSString *appFolder = [self getRunFolder:app];
        NSString *fontPath = [NSString stringWithFormat:@"%@/%@", appFolder, fileName];
        if ([[NSFileManager defaultManager] fileExistsAtPath:fontPath]) {
            return [NSURL fileURLWithPath:fontPath];
        }
        DDLogInfo(@"no file at path :%@", fontPath);
    }

    return nil;
}

#pragma mark - Folder

- (NSString *)getDownloadFolder:(ReactAppModel *)app {
    NSString *appFolder = [self downloadFolder];
    return [NSString stringWithFormat:@"%@/%@", appFolder, @(app.appid)];
}

- (NSString *)getRunFolder:(ReactAppModel *)app {
    NSString *appFolder = [self runAppFolder];
    return [NSString stringWithFormat:@"%@/%@", appFolder, @(app.appid)];
}

- (NSString *)downloadFolder {
    NSString *appFoldder = [self allAppFolder];
    return [appFoldder stringByAppendingPathComponent:kDownloadFolder];
}

- (NSString *)runAppFolder {
    NSString *appFoldder = [self allAppFolder];
    return [appFoldder stringByAppendingPathComponent:kRunFolder];
}

- (NSString *)allAppFolder {
    NSString *cachesDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *folderPath = [cachesDirectory stringByAppendingPathComponent:kReactNativeAppFolder];
    if (![[NSFileManager defaultManager] fileExistsAtPath:folderPath]) {
        [FileUtils createFolderAtPath:folderPath];
        NSString *downloadFolder = [folderPath stringByAppendingPathComponent:kDownloadFolder];
        [FileUtils createFolderAtPath:downloadFolder];
        NSString *runFolder = [folderPath stringByAppendingPathComponent:kRunFolder];
        [FileUtils createFolderAtPath:runFolder];
    }

    return folderPath;
}

- (void)observe:(NSObject *)observerObject fromAppId:(NSInteger)appId complete:(void (^)(void))handle {
    ReactAppModel *appModel = [self getApp:appId];
    if (appModel.appState == ReactNativeAppStateReadyToUse) {
        return;
    }
    [[[[[RACObserve(appModel, appState) takeUntil:observerObject.rac_willDeallocSignal] filter:^BOOL(NSNumber *value) {
        return [value integerValue] == ReactNativeAppStateReadyToUse;
    }] take:1] deliverOnMainThread] subscribeNext:^(id x) {
        if (handle) {
            handle();
        }
    }];
}

@end
