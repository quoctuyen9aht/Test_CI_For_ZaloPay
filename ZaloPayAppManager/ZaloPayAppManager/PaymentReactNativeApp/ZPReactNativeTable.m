//
//  ZPReactNativeTable.m
//  PaymentReactNativeApp
//
//  Created by bonnpv on 5/16/16.
//  Copyright © 2016 bonnpv. All rights reserved.
//

#import "ZPReactNativeTable.h"
#import <fmdb/FMDatabase.h>
#import <fmdb/FMDatabaseQueue.h>
#import <ZaloPayCommon/NSCollection+JSON.h>

NSString *listAppIdKey = @"listAppIdKey";
NSString *checkSumListKey = @"checkSumListKey";
NSString *activeReactApp = @"activeReactApp";

@implementation ZPReactNativeTable

+ (NSArray<NSString*>*) creatingTableQuery {
    NSString* tableCacheData = @"CREATE TABLE IF NOT EXISTS ReactNativeApp(key text primary key, value text);";
    return @[tableCacheData];
}

+ (NSArray<NSString*>*) tableNamesCanBeDropped {
    return @[@"ReactNativeApp"];
}

- (void)tbReactSaveApps:(NSArray *)apps {
    for (ReactAppModel *oneApp in apps) {
        NSString *key = [@(oneApp.appid) stringValue];
        NSString *value = [oneApp toJsonString];
        [self tbReactUpdateValue:value forKey:key];
    }
}

- (void)saveOrderReactNativeApp:(NSArray *)activeApps {
    if ([activeApps isKindOfClass:[NSArray class]]) {
        NSString *jsonString = [activeApps JSONRepresentation];
        [self tbReactUpdateValue:jsonString forKey:activeReactApp];
    }
}

- (NSArray *)orderReactNativeApp {
    NSString *value = [self tbReactValueForKey:activeReactApp];
    return [value JSONValue];
}

- (ReactAppModel *)tbReactGetApp:(NSInteger)appId {
    NSString *key = [@(appId) stringValue];
    NSString *info = [self tbReactValueForKey:key];
    NSDictionary *dic = [info JSONValue];
    return [ReactAppModel fromJSON:dic];
}

- (NSArray *)tbReactGetAppIdList {
    NSString *value = [self tbReactValueForKey:listAppIdKey];
    return [value JSONValue];
}

- (void)tbReactSaveAppIdList:(NSArray *)appIdList {
    if ([appIdList isKindOfClass:[NSArray class]]) {
        NSString *value = [appIdList JSONRepresentation];
        [self tbReactUpdateValue:value forKey:listAppIdKey];
    }
}

- (NSString *)tbReactValueForKey:(NSString *)key {
    __block NSString *version;
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSError *error = nil;
        FMResultSet *rs = [db executeQuery:@"SELECT value FROM ReactNativeApp WHERE key=?" values:@[key] error:&error];
        if (error) {
            return;
        }
        if ([rs next]) {
            version = [rs stringForColumn:@"value"];
        }

        [rs close];
    }];

    return version;
}

- (void)tbReactUpdateValue:(NSString *)value forKey:(NSString *)key {
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        [db executeUpdate:@"INSERT OR REPLACE INTO ReactNativeApp (key, value) VALUES (?, ?)", key, value];
    }];
}

@end
