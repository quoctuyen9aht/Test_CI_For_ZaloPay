//
//  ReacNativeAppManager+DownloadData.h
//  PaymentReactNativeApp
//
//  Created by bonnpv on 5/18/16.
//  Copyright © 2016 bonnpv. All rights reserved.
//

#import "ReactNativeAppManager.h"
#import "NetworkManager+ReactNativeApp.h"

@interface ReactNativeAppManager (DownloadData)

- (RACSignal *)downloadApp:(ReactAppModel *)appModel progress:(DowloadProgressBlock)progress;

- (RACSignal *)downloadAllActiveAppIgnoreApp:(NSArray *)ignoredAppIds;

- (NSURL *)jsBundleWithAppId:(NSInteger)appId;

- (void)clearListDownloadedApp:(NSArray *)listApps;

- (void)clearAllCacheApp;

- (void)clearAppData:(ReactAppModel *)app;

- (BOOL)isDownLoadedApp:(ReactAppModel *)app;

- (NSString *)getRunFolder:(ReactAppModel *)app;

- (NSString *)getDownloadFolder:(ReactAppModel *)app;

- (NSString *)runAppFolder;

- (NSString *)downloadFolder;

- (NSURL *)fontWithAppId:(NSInteger)appId fileName:(NSString *)fileName;

- (void)observe:(NSObject *)observerObject fromAppId:(NSInteger)appId complete:(void (^)(void))handle;
@end
