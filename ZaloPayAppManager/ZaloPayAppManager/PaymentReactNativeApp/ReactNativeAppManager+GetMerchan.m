//
//  ReacNativeAppManager+GetMerchan.m
//  PaymentReactNativeApp
//
//  Created by bonnpv on 5/24/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ReactNativeAppManager+GetMerchan.h"
#import "ReactNativeNetwork.h"
#import <ZaloPayCommon/NSCollection+JSON.h>
#import "PaymentModulLog.h"
#import "ZPMerchantTable.h"
#import <ReactiveObjC/ReactiveObjC.h>

@implementation ReactNativeAppManager (GetMerchan)

- (ZPMerchantUserInfo *)merchantUserInfoWithAppId:(NSInteger)appId {
    return [self.merchantTable getMerchantUserInfo:appId];
}

- (RACSignal *)getMerchantUserInfo {
    NSInteger appId = self.runingAppId;
    return [self getMerchantUserInfoWithAppId:appId];
}

- (RACSignal *)getMerchantUserInfoWithAppId:(NSInteger)appId {
    ZPMerchantUserInfo *info = [self.merchantTable getMerchantUserInfo:appId];
    if (info.maccesstoken > 0) {
        return [RACSignal return:info];
    }

    return [[[NetworkManager sharedInstance] getMerchanUserInfo:appId] map:^id(NSDictionary *dic) {
        DDLogInfo(@"getMerchanUserInfo :%@", dic);
        ZPMerchantUserInfo *userInfo = nil;
        NSString *mUid = [dic stringForKey:@"muid"];
        NSString *displayName = [dic stringForKey:@"displayname"];
        NSString *dateOfBirth = [dic stringForKey:@"birthdate"];
        NSString *gender = [NSString stringWithFormat:@"%@", [dic objectForKey:@"usergender"]];
        NSString *maccesstoken = [dic stringForKey:@"maccesstoken"];

        userInfo = [[ZPMerchantUserInfo alloc] init];
        userInfo.muid = mUid;
        userInfo.maccesstoken = maccesstoken;
        userInfo.displayName = displayName;
        userInfo.birthday = dateOfBirth;
        userInfo.gender = gender;
        
        [self.merchantTable saverMerchanUserInfoWithAppId:appId
                                                                   muid:mUid
                                                            displayName:displayName
                                                           maccesstoken:maccesstoken
                                                                 gender:gender
                                                               birthday:dateOfBirth
                                                                 avatar:@""];

        return userInfo;
    }];
}

- (void)preloadAllMerchantUserInfo:(NSArray *)listAppIds {
    if (listAppIds.count <= 0 || [NetworkManager sharedInstance].accesstoken.length == 0) {
        return;
    }
    
    NSDictionary *allInfo = [self.merchantTable checkMerchantUserInfoWithAppIds:listAppIds];
    NSMutableArray *toGet = [NSMutableArray array];
    for (NSNumber *appId in listAppIds) {
        if (![allInfo objectForKey:appId]) {
            [toGet addObject:appId];
        }
    }
    if (toGet.count == 0) {
        return;
    }

    [[[NetworkManager sharedInstance] getListMerchantUserInfo:toGet] subscribeNext:^(NSDictionary *dic) {
        DDLogInfo(@"appInfos = %@", dic);

        NSString *gender = [NSString stringWithFormat:@"%@", [dic objectForKey:@"usergender"]];
        NSString *displayName = [dic stringForKey:@"displayname"];
        NSString *birthdate = [dic stringForKey:@"birthdate"];
        NSArray *listmerchantuserinfo = [dic arrayForKey:@"listmerchantuserinfo"];

        for (NSDictionary *oneDic in listmerchantuserinfo) {
            NSInteger appId = [oneDic intForKey:@"appid"];
            NSString *maccesstoken = [oneDic stringForKey:@"maccesstoken"];
            NSString *muid = [oneDic stringForKey:@"muid"];
            
            [self.merchantTable saverMerchanUserInfoWithAppId:appId
                                                                       muid:muid
                                                                displayName:displayName
                                                               maccesstoken:maccesstoken
                                                                     gender:gender
                                                                   birthday:birthdate
                                                                     avatar:@""];
        }
    }];
}

- (void)clearMerchantUserInfo {
    [self.merchantTable clearMerchantUserInfo];
}
@end
