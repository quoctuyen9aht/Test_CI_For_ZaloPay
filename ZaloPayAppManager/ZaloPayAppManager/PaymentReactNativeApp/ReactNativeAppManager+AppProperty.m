//
//  ReactNativeAppManager+AppProperty.m
//  ZaloPayAppManager
//
//  Created by bonnpv on 2/13/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ReactNativeAppManager+AppProperty.h"
#import <ZaloPayCommon/NSCollection+JSON.h>
#import <ReactiveObjC/ReactiveObjC.h>
#import <ZaloPayProfile/ZaloPayProfile-Swift.h>
#import <ZaloPayNetwork/NetworkManager.h>
#import <ZaloPayCommon/ZPConstants.h>
#import <ZaloPayUI/ZPAppFactory.h>

NSString * const kAvatar         = @"avatar";
NSString * const kPhonenumber    = @"phonenumber";
NSString * const kUserid         = @"userid";

@implementation ReactNativeAppManager (AppProperty)

- (NSDictionary *)reactProperties:(NSInteger)appId {
    NSArray *permission = [[ZPAppFactory sharedInstance] getAppPermission:appId];
    NSMutableDictionary *properties = [NSMutableDictionary dictionary];
    
    for (NSString *key in permission) {
        if ([key isEqualToString:kAvatar]) {
            NSString *avatar = [ZPProfileManager shareInstance].currentZaloUser.avatar;
            [properties setObjectCheckNil:avatar forKey:@"user_avatar"];
            continue;
        }
        if ([key isEqualToString:kPhonenumber]) {
            NSString *phone = [ZPProfileManager shareInstance].userLoginData.phoneNumber;
            [properties setObjectCheckNil:phone forKey:@"user_phonenumber"];
            continue;
        }
        if ([key isEqualToString:kUserid]) {
            NSString *zaloPayId = [NetworkManager sharedInstance].paymentUserId;
            [properties setObjectCheckNil:zaloPayId forKey:@"zalopay_userid"];
            continue;
        }
    }
    return properties;
}


- (NSString *)defaultModuleName:(NSInteger)appId {
    if (appId == lixiAppId) {
        return ReactModule_RedPacket;
    }
    return ReactModule_PaymentMain;
}

@end
