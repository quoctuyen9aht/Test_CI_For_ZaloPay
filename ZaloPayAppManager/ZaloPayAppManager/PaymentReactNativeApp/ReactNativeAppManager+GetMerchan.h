//
//  ReacNativeAppManager+GetMerchan.h
//  PaymentReactNativeApp
//
//  Created by bonnpv on 5/24/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ReactNativeAppManager.h"

@class ZPMerchantUserInfo;

@interface ReactNativeAppManager (GetMerchan)
- (RACSignal *)getMerchantUserInfo;

- (RACSignal *)getMerchantUserInfoWithAppId:(NSInteger)appId;

- (void)preloadAllMerchantUserInfo:(NSArray *)listAppIds;

- (void)clearMerchantUserInfo;

- (ZPMerchantUserInfo *)merchantUserInfoWithAppId:(NSInteger)appId;
@end
