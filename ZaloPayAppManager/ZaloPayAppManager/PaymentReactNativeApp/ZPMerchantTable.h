//
//  ZPMerchantTable.h
//  ZaloPayAppManager
//
//  Created by bonnpv on 9/19/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <ZaloPayDatabase/ZPBaseTable.h>
#import "ZPMerchantUserInfo.h"

@interface ZPMerchantTable : ZPBaseTable

- (ZPMerchantUserInfo *)getMerchantUserInfo:(NSInteger)appId;

- (void)clearMerchantUserInfo;

- (void)saverMerchanUserInfoWithAppId:(NSInteger)appId
                                 muid:(NSString *)muid
                          displayName:(NSString *)displayName
                         maccesstoken:(NSString *)maccesstoken
                               gender:(NSString *)gender
                             birthday:(NSString *)birthday
                               avatar:(NSString *)avatar;

- (NSDictionary *)allMerchantInfo;

- (NSDictionary *)checkMerchantUserInfoWithAppIds:(NSArray *)appIds;
@end
