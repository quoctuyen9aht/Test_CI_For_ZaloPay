//
//  ReactNativeAppManager+LocalApp.m
//  PaymentReactNativeApp
//
//  Created by bonnpv on 5/26/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ReactNativeAppManager+LocalApp.h"
#import <ZaloPayCommon/NSCollection+JSON.h>
#import "ReactAppModel.h"
#import <ZaloPayCommon/FileUtils.h>
#import "ReactNativeAppManager+DownloadData.h"
#import "PaymentModulLog.h"

@implementation ReactNativeAppManager (LocalApp)

- (NSString *)extractLocalAppKey {
    NSString *build = [NSString stringWithFormat:@"%@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]];
    NSString *version = [NSString stringWithFormat:@"%@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
    return [NSString stringWithFormat:@"app_external_%@_%@", version, build];
}

- (BOOL)isDoneExtractLocalApp {
    NSString *key = [self extractLocalAppKey];
    return [[[NSUserDefaults standardUserDefaults] objectForKey:key] boolValue];
}

- (void)markDoneExtractLocalApp {
    DDLogInfo(@"mark done extract local app");
    NSString *key = [self extractLocalAppKey];
    [[NSUserDefaults standardUserDefaults] setObject:@(TRUE) forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)loadAppFromJson:(NSArray *)appInfos {
    if ([self isDoneExtractLocalApp] == TRUE) {
        DDLogInfo(@"done extract local app");
        return;
    }
    DDLogInfo(@"start extract local app");
    [self uzipAppFromConfig:appInfos];
    [self markDoneExtractLocalApp];
}
//
//- (NSArray *)allZipAppConfigFromFile:(NSString *)fileName {
//    NSString *filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"json"];
//    if (filePath.length == 0) {
//        return nil;
//    }
//    NSString *string = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
//    NSArray *arrApp = [string JSONValue];
//    return [arrApp isKindOfClass:[NSArray class]] ? arrApp : nil;
//}

- (void)uzipAppFromConfig:(NSArray *)arrApp {
//    NSArray *arrApp = [self allZipAppConfigFromFile:fileName];
//    if (arrApp.count == 0) {
//        return;
//    }
    for (NSDictionary *oneDic in arrApp) {
        int appId = [oneDic intForKey:@"appId"];
        ReactAppModel *app = [self getApp:appId];
        [self clearAppData:app];
        app.localZipFile = [oneDic stringForKey:@"zipFile"];
        [self unzipApp:app];
    }
}

- (void)unzipApp:(ReactAppModel *)appModel {
    NSString *runAppPath = [self getRunFolder:appModel];
    [FileUtils removeFolderAtPath:runAppPath];
    [FileUtils createFolderAtPath:runAppPath];
    NSString *zipPath = [[NSBundle mainBundle] pathForResource:appModel.localZipFile ofType:@"zip"];
    [FileUtils unzipFileAtPath:zipPath toFolderPath:runAppPath removeZipFile:NO];
}

@end
