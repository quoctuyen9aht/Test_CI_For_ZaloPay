//
//  ReacNativeAppManager.m
//  PaymentReactNativeApp
//
//  Created by bonnpv on 5/16/16.
//  Copyright © 2016 bonnpv. All rights reserved.
//

#import "ReactNativeAppManager.h"
#import <ZaloPayCommon/NSCollection+JSON.h>
#import <UIKit/UIKit.h>
#import <ReactiveObjC/ReactiveObjC.h>
#import "NetworkManager+ReactNativeApp.h"
#import "PaymentModulLog.h"
#import "ReactAppModel.h"
#import "ZPMarkDownloadAppTable.h"
#import "ZPReactNativeTable.h"
#import "ReactNativeAppManager+DownloadData.h"
#import "ZPMerchantTable.h"
#import <ZaloPayDatabase/PerUserDataBase.h>
#import <ZaloPayDatabase/GlobalDatabase.h>

@interface ReactNativeAppManager ()
@property(nonatomic, strong) NSArray *sortApps;
@property(nonatomic, strong) NSMutableDictionary *allApps;
@end

@implementation ReactNativeAppManager

+ (instancetype)sharedInstance {
    static ReactNativeAppManager *_return;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _return = [[ReactNativeAppManager alloc] init];
    });
    return _return;
}

- (id)init {
    self = [super init];
    if (self) {
        self.reactNativeTable = [[ZPReactNativeTable alloc] initWithDB:[GlobalDatabase sharedInstance]];
        _allApps = [NSMutableDictionary dictionary];
        _sortApps = [self.reactNativeTable orderReactNativeApp];
        self.merchantTable = [[ZPMerchantTable alloc] initWithDB:[PerUserDataBase sharedInstance]];
        self.reactNativeTable = [[ZPReactNativeTable alloc] initWithDB:[GlobalDatabase sharedInstance]];
        self.markDownloadAppTable = [[ZPMarkDownloadAppTable alloc] initWithDB:[GlobalDatabase sharedInstance]];
    }
    return self;
}

- (NSArray *)activeAppIds {
    return _sortApps;
}

- (void)clearAllData {
    @synchronized(self) {
        self.sortApps = nil;
        [self.allApps removeAllObjects];
    }    
}

- (ReactAppModel *)getApp:(NSInteger)appId {
    @synchronized (self.allApps) {
        ReactAppModel *app = [self.allApps objectForKey:@(appId)];
        if (app) {
            return app;
        }
        app = [self.reactNativeTable tbReactGetApp:appId];
        if (!app) {
            app = [[ReactAppModel alloc] init];
            app.appid = appId;
        }
        
        if ([self isDownLoadedApp:app]) {
            app.appState = ReactNativeAppStateReadyToUse;
        }
        [self.allApps setObject:app forKey:@(appId)];
        return app;
    }
}

- (RACSignal *)loadAppFromServer {
    return [RACSignal createSignal:^RACDisposable *(id <RACSubscriber> subscriber) {
        NSArray *listAppId = [self.reactNativeTable tbReactGetAppIdList];
        NSMutableArray *listCheckSum = [NSMutableArray array];
        for (NSNumber *appId in listAppId) {
            ReactAppModel *appModel = [self getApp:[appId integerValue]];
            NSString *checksum = appModel.checksum.length == 0 ? @"" : appModel.checksum;
            [listCheckSum addObject:checksum];
        }


        [[[NetworkManager sharedInstance] requestAppResourceWithApp:listAppId andCheckSum:listCheckSum] subscribeNext:^(NSDictionary *dic) {
            [self handleJsonData:dic];
            [subscriber sendCompleted];
        }                                                                                                       error:^(NSError *error) {
            DDLogInfo(@"request app error : %@", error);
            [self activeAppFromCache];
            [subscriber sendCompleted];
        }];
        return nil;
    }];
}

- (NSMutableArray *)handleJsonData:(NSDictionary *)dic {
    NSMutableArray *_return = [NSMutableArray array];
    NSString *baseUrl = [dic stringForKey:@"baseurl"];
    NSArray *resources = [dic arrayForKey:@"resourcelist"];

    NSArray *updateResourceApps = [self updateResource:resources baseUrl:baseUrl];
    [self clearListDownloadedApp:updateResourceApps];

    NSArray *applist = [dic arrayForKey:@"appidlist"];
    [self.reactNativeTable tbReactSaveAppIdList:applist];

    self.sortApps = [dic arrayForKey:@"orderedInsideApps"];
    [self.reactNativeTable saveOrderReactNativeApp:self.sortApps];

    for (NSNumber *oneAppId in self.sortApps) {
        NSInteger appId = [oneAppId integerValue];
        ReactAppModel *app = [self getApp:appId];
        [_return addObjectNotNil:app];
    }
    return _return;
}

- (NSMutableArray *)activeAppFromCache {
    NSMutableArray *_return = [NSMutableArray array];
    NSArray *activeApp = [self.reactNativeTable orderReactNativeApp];
    for (NSNumber *oneAppId in activeApp) {
        NSInteger appId = [oneAppId integerValue];
        ReactAppModel *app = [self getApp:appId];
        [_return addObjectNotNil:app];
    }
    return _return;
}

- (NSMutableArray *)updateResource:(NSArray *)resources baseUrl:(NSString *)baseUrl {
    NSMutableArray *toSave = [NSMutableArray array];
    for (NSDictionary *oneDic in resources) {
        int appId = [oneDic intForKey:@"appid"];
        ReactAppModel *oneApp = [self getApp:appId];
        [oneApp updateDataFromDic:oneDic withBaseUrl:baseUrl];
        [toSave addObject:oneApp];
    }
    [self.reactNativeTable tbReactSaveApps:toSave];
    return toSave;
}

@end
