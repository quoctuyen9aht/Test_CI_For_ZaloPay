//
//  ReactNativeNetwork.m
//  PaymentReactNativeApp
//
//  Created by bonnpv on 5/24/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ReactNativeNetwork.h"
#import <ZaloPayCommon/NSCollection+JSON.h>
#import <ZaloPayNetwork/ZPNetWorkApi.h>

@implementation NetworkManager (ReactNativeNetwork)
- (RACSignal *)getMerchanUserInfo:(NSInteger)appId {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@(appId) forKey:@"appid"];
    return [self requestWithPath:api_ummerchant_getmerchantuserinfo parameters:params];
}

- (RACSignal *)getListMerchantUserInfo:(NSArray *)listAppId {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObjectCheckNil:[listAppId componentsJoinedByString:@","] forKey:@"appidlist"];
    return [self requestWithPath:api_ummerchant_getlistmerchantuserinfo parameters:param];
}
@end
