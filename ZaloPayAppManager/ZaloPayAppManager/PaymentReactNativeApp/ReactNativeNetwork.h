//
//  ReactNativeNetwork.h
//  PaymentReactNativeApp
//
//  Created by bonnpv on 5/24/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <ZaloPayNetwork/NetworkManager.h>

@interface NetworkManager (ReactNativeNetwork)
- (RACSignal *)getMerchanUserInfo:(NSInteger)appId;

- (RACSignal *)getListMerchantUserInfo:(NSArray *)listAppId;
@end
