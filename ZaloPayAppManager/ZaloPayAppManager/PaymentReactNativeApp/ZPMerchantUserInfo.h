//
//  ZPMerchantUserInfo.h
//  ZaloPayAppManager
//
//  Created by bonnpv on 9/19/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZPMerchantUserInfo : NSObject
@property(nonatomic) NSInteger appId;
@property(nonatomic, strong) NSString *muid;
@property(nonatomic, strong) NSString *maccesstoken;
@property(nonatomic, strong) NSString *gender;
@property(nonatomic, strong) NSString *birthday;
@property(nonatomic, strong) NSString *displayName;
@end
