//
//  NetworkManager+ReactNativeApp.h
//  PaymentReactNativeApp
//
//  Created by bonnpv on 5/16/16.
//  Copyright © 2016 bonnpv. All rights reserved.
//

#import <ZaloPayNetwork/NetworkManager.h>

@interface NetworkManager (ReactNativeApp)

- (RACSignal *)requestAppResourceWithApp:(NSArray *)listApp andCheckSum:(NSArray *)checkSum;

@end
