//
//  NetworkManager+ReactNativeApp.m
//  PaymentReactNativeApp
//
//  Created by bonnpv on 5/16/16.
//  Copyright © 2016 bonnpv. All rights reserved.
//

#import "NetworkManager+ReactNativeApp.h"
#import <ZaloPayCommon/UIDevice+Payment.h>
#import <ZaloPayCommon/NSCollection+JSON.h>
#import <ZaloPayNetwork/ZPNetWorkApi.h>

@implementation NetworkManager (ReactNativeApp)

- (RACSignal *)requestAppResourceWithApp:(NSArray *)listApp andCheckSum:(NSArray *)checkSum {
    NSString *strListApp = @"";
    NSString *strCheckSum = @"";
    if (listApp) {
        strListApp = [listApp componentsJoinedByString:@","];
    }
    if (checkSum) {
        strCheckSum = [checkSum componentsJoinedByString:@","];
    }
    NSString *appListString = [NSString stringWithFormat:@"[%@]", strListApp];
    NSString *checkSumString = [NSString stringWithFormat:@"[%@]", strCheckSum];

    NSString *appversion = [NSString stringWithFormat:@"%@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
    NSString *screenType = [[UIDevice currentDevice] screenTypeString];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@"ios" forKey:@"platformcode"];
    [params setObject:appListString forKey:@"appidlist"];
    [params setObject:checkSumString forKey:@"checksumlist"];
    [params setObjectCheckNil:screenType forKey:@"dscreentype"];
    [params setObjectCheckNil:appversion forKey:@"appversion"];

    return [self requestWithPath:api_v001_tpe_getinsideappresource parameters:params];
}
@end
