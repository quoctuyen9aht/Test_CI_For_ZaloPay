//
//  ReactNativeAppManager+AppProperty.h
//  ZaloPayAppManager
//
//  Created by bonnpv on 2/13/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ReactNativeAppManager.h"

@interface ReactNativeAppManager (AppProperty)
- (NSMutableDictionary *)reactProperties:(NSInteger)appId;

- (NSString *)defaultModuleName:(NSInteger)appId;
@end
