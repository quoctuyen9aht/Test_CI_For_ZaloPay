//
//  ReactNativeAppModel.m
//  PaymentReactNativeApp
//
//  Created by bonnpv on 5/16/16.
//  Copyright © 2016 bonnpv. All rights reserved.
//

#import "ReactAppModel.h"
#import <UIKit/UIKit.h>
#import <ZaloPayCommon/NSCollection+JSON.h>

#define kAppid      @"appid"
#define kAppName    @"appname"
#define kImageUrl   @"imageurl"
#define kJsUrl      @"jsurl"
#define kCheckSum   @"checksum"
#define kStatus     @"status"
#define kAppType    @"apptype"
#define kWebUrl     @"weburl"
#define kIconUrl    @"iconurl"
#define kIconColor      @"iconcolor"
#define kIconName       @"iconname"

@implementation ReactAppModel

+ (instancetype)fromJSON:(NSDictionary *)json {
    return [self fromJSON:json withBaseUrl:@""];
}

+ (instancetype)fromJSON:(NSDictionary *)json withBaseUrl:(NSString *)baseUrl {
    if (![json isKindOfClass:[NSDictionary class]] || json.count == 0) {
        return nil;
    }

    ReactAppModel *_return = [[ReactAppModel alloc] init];
    _return.appid = [json intForKey:kAppid];
    _return.appname = [json stringForKey:kAppName];
    _return.imageurl = [NSString stringWithFormat:@"%@%@", baseUrl, [json stringForKey:kImageUrl]];
    _return.jsurl = [NSString stringWithFormat:@"%@%@", baseUrl, [json stringForKey:kJsUrl]];
    _return.checksum = [json stringForKey:kCheckSum];
    _return.status = [json intForKey:kStatus];
    _return.appIconUrl = [NSString stringWithFormat:@"%@%@", baseUrl, [json stringForKey:kIconUrl]];
    _return.appType = [json intForKey:kAppType];
    _return.webUrl = [json stringForKey:kWebUrl];
    _return.color = [json stringForKey:kIconColor];
    _return.iconName = [json stringForKey:kIconName];
    return _return;
}

- (void)updateDataFromDic:(NSDictionary *)json withBaseUrl:(NSString *)baseUrl {
    @synchronized (self) {
        self.appid = [json intForKey:kAppid];
        self.appname = [json stringForKey:kAppName];
        self.imageurl = [NSString stringWithFormat:@"%@%@", baseUrl, [json stringForKey:kImageUrl]];
        self.jsurl = [NSString stringWithFormat:@"%@%@", baseUrl, [json stringForKey:kJsUrl]];
        self.checksum = [json stringForKey:kCheckSum];
        self.status = [json intForKey:kStatus];
        self.appIconUrl = [NSString stringWithFormat:@"%@%@", baseUrl, [json stringForKey:kIconUrl]];
        self.appType = [json intForKey:kAppType];
        self.webUrl = [json stringForKey:kWebUrl];
        self.color = [json stringForKey:kIconColor];
        self.iconName = [json stringForKey:kIconName];
    }
}

- (NSString *)toJsonString {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:@(self.appid) forKey:kAppid];
    [dic setObjectCheckNil:self.appname forKey:kAppName];
    [dic setObjectCheckNil:self.imageurl forKey:kImageUrl];
    [dic setObjectCheckNil:self.jsurl forKey:kJsUrl];
    [dic setObjectCheckNil:self.checksum forKey:kCheckSum];
    [dic setObject:@(self.status) forKey:kStatus];
    [dic setObjectCheckNil:self.appIconUrl forKey:kIconUrl];
    [dic setObject:@(self.appType) forKey:kAppType];
    [dic setObjectCheckNil:self.webUrl forKey:kWebUrl];
    [dic setObjectCheckNil:self.iconName forKey:kIconName];
    [dic setObjectCheckNil:self.color forKey:kIconColor];
    return [dic JSONRepresentation];
}

@end
