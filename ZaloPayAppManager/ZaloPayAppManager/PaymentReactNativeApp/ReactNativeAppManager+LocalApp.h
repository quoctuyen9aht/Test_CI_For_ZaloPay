//
//  ReactNativeAppManager+LocalApp.h
//  PaymentReactNativeApp
//
//  Created by bonnpv on 5/26/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ReactNativeAppManager.h"

@interface ReactNativeAppManager (LocalApp)
- (BOOL)isDoneExtractLocalApp;

- (void)loadAppFromJson:(NSArray *)fileName;
@end
