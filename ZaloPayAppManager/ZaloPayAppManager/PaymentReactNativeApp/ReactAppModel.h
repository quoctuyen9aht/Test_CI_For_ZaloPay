//
//  ReactNativeAppModel.h
//  PaymentReactNativeApp
//
//  Created by bonnpv on 5/16/16.
//  Copyright © 2016 bonnpv. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    ReactNativeAppStateNeedDownLoad = 0,
    ReactNativeAppStateWillDownload = 1,
    ReactNativeAppStateDownloading = 2,
    ReactNativeAppStateDownloadError = 3,
    ReactNativeAppStateReadyToUse = 4,
} ReactNativeAppState;

typedef enum {
    ReactAppTypeNative = 1,
    ReactAppTypeWeb = 2,
} ReactAppType;

@interface ReactAppModel : NSObject
@property(nonatomic) NSInteger appid;
@property(nonatomic, strong) NSString *appname;
@property(nonatomic, strong) NSString *imageurl;
@property(nonatomic, strong) NSString *jsurl;
@property(nonatomic, strong) NSString *checksum;
@property(nonatomic, strong) NSString *localZipFile;
@property(nonatomic) int status;
@property(nonatomic) ReactNativeAppState appState;
@property(nonatomic, strong) NSString *appIconUrl;
@property(nonatomic) ReactAppType appType;
@property(nonatomic, strong) NSString *webUrl;
@property(nonatomic, strong) NSString *color;
@property(nonatomic, strong) NSString *iconName;
@property(nonatomic, assign) float percentDownloaded;

+ (instancetype)fromJSON:(NSDictionary *)json;

- (void)updateDataFromDic:(NSDictionary *)json withBaseUrl:(NSString *)baseUrl;

- (NSString *)toJsonString;
@end
