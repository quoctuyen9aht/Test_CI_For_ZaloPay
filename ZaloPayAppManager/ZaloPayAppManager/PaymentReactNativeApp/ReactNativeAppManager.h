//
//  ReacNativeAppManager.h
//  PaymentReactNativeApp
//
//  Created by bonnpv on 5/16/16.
//  Copyright © 2016 bonnpv. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ReactAppModel;
@class RACSignal;

@class ZPMerchantTable;
@class ZPReactNativeTable;
@class ZPMarkDownloadAppTable;

@interface ReactNativeAppManager : NSObject
@property(nonatomic, readonly) NSArray *_Nullable activeAppIds;
@property(nonatomic) NSInteger runingAppId;
@property (nonatomic, strong) ZPMerchantTable* _Nonnull merchantTable;
@property (nonatomic, strong) ZPReactNativeTable* _Nonnull reactNativeTable;
@property (nonatomic, strong) ZPMarkDownloadAppTable* _Nonnull markDownloadAppTable;

+ (instancetype _Nonnull)sharedInstance;

- (RACSignal *_Nonnull)loadAppFromServer;

- (ReactAppModel *_Nonnull)getApp:(NSInteger)appId;

- (void)clearAllData;
@end
