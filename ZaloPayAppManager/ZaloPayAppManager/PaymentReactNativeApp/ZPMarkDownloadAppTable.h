//
//  ZPMarkDownloadAppTable.h
//  PaymentReactNativeApp
//
//  Created by bonnpv on 7/12/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

@class FMDatabase;

#import "ReactAppModel.h"
#import <ZaloPayDatabase/GlobalDatabase.h>
#import <ZaloPayDatabase/ZPBaseTable.h>

@interface ZPMarkDownloadAppTable : ZPBaseTable
- (BOOL)isDownloaded:(NSInteger)appId;

- (void)setDoneDownloadApp:(NSInteger)appId state:(BOOL)isDownLoad;
@end
