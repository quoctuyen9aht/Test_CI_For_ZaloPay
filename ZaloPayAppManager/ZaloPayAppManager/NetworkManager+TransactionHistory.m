//
//  NetworkManager+TransactionHistory.m
//  ZaloPayAppManager
//
//  Created by PhucPv on 5/7/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "NetworkManager+TransactionHistory.h"
#import <ZaloPayCommon/NSCollection+JSON.h>
#import <ZaloPayNetwork/ZPNetWorkApi.h>

@implementation NetworkManager (TransactionLog)
- (RACSignal *)loadTransactionWithTimestamp:(NSTimeInterval)timestamp
                                      count:(int)count
                                      order:(TransOrder)order
                                 statusType:(StatusType)statusType {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObjectCheckNil:@(timestamp) forKey:@"timestamp"];
    [param setObjectCheckNil:@(count) forKey:@"count"];
    [param setObjectCheckNil:@(order) forKey:@"order"];
    [param setObjectCheckNil:@(statusType) forKey:@"statustype"];
    return [self requestWithPath:api_v001_tpe_transhistory parameters:param];
}

@end
