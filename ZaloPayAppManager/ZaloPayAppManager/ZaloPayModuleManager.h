//
//  ZaloPayModuleManager.h
//  ZaloPayAppManager
//
//  Created by Nguyễn Hữu Hoà on 7/21/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#ifndef __ZALOPAY_MODULE_MANAGER_H__
#define __ZALOPAY_MODULE_MANAGER_H__

#import <Foundation/Foundation.h>

extern NSString *const ZPNotificationsAddedNotification;
extern NSString *const ZPTransactionsUpdatedNotification;
extern NSString *const ZPTransactionsUpdated;
extern NSString *const ZPTransactionDetailUpdated;


#endif // __ZALOPAY_MODULE_MANAGER_H__
