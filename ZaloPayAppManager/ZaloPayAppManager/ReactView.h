//
//  ReactView.h
//  ZaloPay
//
//  Created by Nguyễn Hữu Hoà on 3/23/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RCTBridge;

@interface ReactView : UIView
- (instancetype)initWithModuleName:(NSString *)moduleName instanceId:(NSInteger)instanceId bridge:(RCTBridge *)bridge;

- (instancetype)init:(NSInteger)instanceId bridge:(RCTBridge *)bridge;

- (instancetype)initWithModuleName:(NSString *)moduleName instanceId:(NSInteger)instanceId bridge:(RCTBridge *)bridge properties:(NSDictionary *)properties;

@end
