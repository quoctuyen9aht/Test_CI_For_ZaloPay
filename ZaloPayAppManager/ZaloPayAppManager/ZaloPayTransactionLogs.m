//
//  ZaloPayTransactionLogs.m
//  ZaloPayAppManager
//
//  Created by Nguyễn Hữu Hoà on 5/4/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZaloPayTransactionLogs.h"
#import "TransactionHistoryManager.h"
#import "ReactNativeAppManager+AppProperty.h"
#import "ZPReactLaunchAppModel.h"

#import <ZaloPayCommon/NSCollection+JSON.h>
#import <ZaloPayCommon/Collection+P2PNotification.h>
#import <ZaloPayNetwork/NetworkState.h>
#import <ZaloPayCommon/ZPConstants.h>

@interface NetworkManager (P2PNotification)
- (RACSignal *)sendNotificationWithReceiverId:(NSString *)receiverId
                                  embededData:(NSString *)embededData;
@end

extern NSInteger reactInternalAppId;

@implementation ZaloPayTransactionLogs

- (id)init {
    if (self = [super init]) {
        self.transactionHistoryManager = [[TransactionHistoryManager alloc] init];
    }
    return self;
}

RCT_EXPORT_MODULE()


RCT_EXPORT_METHOD(removeTransactionWithId:
    (NSString *) transid
            resolver:
            (RCTPromiseResolveBlock) resolve
            rejecter:
            (RCTPromiseRejectBlock) reject) {
    [[self.transactionHistoryManager removeTransactionWithId:transid] subscribeNext:^(id x) {
        if (resolve) {
            resolve(x);
        }
    }];
}

RCT_EXPORT_METHOD(successfulTransactionWithTransTypes:
    (NSArray *) transTypes
            timestamp:
            (NSString *) timestamp
            offset:
            (int) offset
            count:
            (int) count
            sign:
            (int) sign
            resolver:
            (RCTPromiseResolveBlock) resolve
            rejecter:
            (RCTPromiseRejectBlock) reject) {
    [[self.transactionHistoryManager successfulTransactionWithTransTypes:transTypes
                                                                       keyTimestamp:[timestamp doubleValue]
                                                                               sign:sign
                                                                             offset:offset
                                                                              count:count] subscribeNext:^(id x) {
        if (resolve) {
            resolve(x);
        }
    }];
}

RCT_EXPORT_METHOD(failedTransactionWithTransTypes:
    (NSArray *) transTypes
            timestamp:
            (NSString *) timestamp
            offset:
            (int) offset
            count:
            (int) count
            sign:
            (int) sign
            resolver:
            (RCTPromiseResolveBlock) resolve
            rejecter:
            (RCTPromiseRejectBlock) reject) {
    [[self.transactionHistoryManager failedTransactionWithTransTypes:transTypes
                                                                   keyTimestamp:[timestamp doubleValue]
                                                                           sign:sign
                                                                         offset:offset
                                                                          count:count] subscribeNext:^(id x) {
        if (resolve) {
            resolve(x);
        }
    }];
}


RCT_EXPORT_METHOD(getTransactionsFail:
    (NSInteger) pageIndex
            count:
            (NSInteger) count
            resolver:
            (RCTPromiseResolveBlock) resolve
            rejecter:
            (RCTPromiseRejectBlock) reject) {

}

RCT_EXPORT_METHOD(loadTransactionWithId:
    (NSString *) transId
            resolver:
            (RCTPromiseResolveBlock) resolve
            rejecter:
            (RCTPromiseRejectBlock) reject) {
    [[[self.transactionHistoryManager loadTransactionHistoryWithId:transId] deliverOnMainThread] subscribeNext:^(id data) {
        if (resolve) {
            resolve(data);
        }
    }                                                                                                               error:^(NSError *error) {
        if (resolve) {
            resolve(@{@"code": @(-1)});
        }
    }];
}

RCT_EXPORT_METHOD(reloadTransactionWithId:
    (NSString *) transId
        :(NSString *)notificationid
        resolver:(RCTPromiseResolveBlock)resolve
        rejecter:(RCTPromiseRejectBlock)reject) {
    TransactionHistoryManager *manager = self.transactionHistoryManager;
    [[RACSignal merge:@[[manager loadTransaction:StatusTypeSuccess], [manager loadTransaction:StatusTypeFail]]] subscribeCompleted:^{
        [self loadTransactionWithId:transId resolver:resolve rejecter:reject];
    }];
}

RCT_EXPORT_METHOD(reloadTransaction:
    (NSString *) transid
            timestamp:
            (NSString *) timestamp
            resolver:
            (RCTPromiseResolveBlock) resolve
            rejecter:
            (RCTPromiseRejectBlock) reject) {
    TransactionHistoryManager *manager = self.transactionHistoryManager;
    RACSignal *merge = [RACSignal merge:@[[manager reloadOldestTransaction:StatusTypeSuccess timeStamp:[timestamp longLongValue]],
            [manager reloadOldestTransaction:StatusTypeFail timeStamp:[timestamp longLongValue]]]];
    [merge subscribeError:^(NSError *error) {
        [self loadTransactionWithId:transid resolver:resolve rejecter:reject];
    }           completed:^{
        [self loadTransactionWithId:transid resolver:resolve rejecter:reject];
    }];
}

RCT_EXPORT_METHOD(showTransactionDetail:
    (int) appid
            transid:
            (NSString *) transid
            resolver:
            (RCTPromiseResolveBlock) resolve
            rejecter:
            (RCTPromiseRejectBlock) reject) {
    NSMutableDictionary *properties = [[ReactNativeAppManager sharedInstance] reactProperties:(int) appid];
    [properties setObjectCheckNil:transid forKey:@"transid"];
    [properties setObjectCheckNil:@"history" forKey:@"view"];
    [ZPReactLaunchAppModel launchReactNativeApp:appid properties:properties resolver:resolve rejecter:reject];
}

RCT_EXPORT_METHOD(launchTransactionDetail:
    (int) appid
            properties:
            (NSDictionary *) params
            resolver:
            (RCTPromiseResolveBlock) resolve
            rejecter:
            (RCTPromiseRejectBlock) reject) {
    NSMutableDictionary *properties = [[ReactNativeAppManager sharedInstance] reactProperties:(int) appid];
    [properties addEntriesFromDictionary:params];
    [properties setObjectCheckNil:@"history" forKey:@"view"];
    [ZPReactLaunchAppModel launchReactNativeApp:appid properties:properties resolver:resolve rejecter:reject];
}


RCT_EXPORT_METHOD(sendThankMessage:
    (NSString *) transid
            message:
            (NSString *) message
            receiverid:
            (NSString *) receiverid
            displayname:
            (NSString *) displayname
            zalopayid:
            (NSString *) zalopayid
            timestamp:
            (NSString *) timestamp
            resolver:
            (RCTPromiseResolveBlock) resolve
            rejecter:
            (RCTPromiseRejectBlock) reject) {

    if ([NetworkState sharedInstance].isReachable == false) {
        if (resolve) {
            resolve(@{@"code": @(NSURLErrorNotConnectedToInternet)});
        }
        return;
    }

    NSString *embededData = [NSString embededDataThankMessageWithTransId:transid
                                                             displayName:displayname
                                                                 message:message
                                                               zalopayId:zalopayid
                                                               timestamp:timestamp];

    [[[NetworkManager sharedInstance] sendNotificationWithReceiverId:receiverid
                                                         embededData:embededData] subscribeError:^(NSError *error) {
        if (resolve) {
            resolve(@{@"code": @(-1)});
        }
    }                                                                                  completed:^{

        [[self.transactionHistoryManager updateThankMessageWithTransId:transid
                                                                    encodeMessage:[NSString encodeP2PMessage:message]] subscribeNext:^(id response) {
            if (resolve) {
                resolve(response);
            }
        }];
    }];
}

RCT_EXPORT_METHOD(updateThankMessage:
    (NSString *) transid
            message:
            (NSString *) message               // Message đã encode UTF16
            resolver:
            (RCTPromiseResolveBlock) resolve
            rejecter:
            (RCTPromiseRejectBlock) reject) {

    [[self.transactionHistoryManager updateThankMessageWithTransId:transid
                                                                encodeMessage:message] subscribeNext:^(id response) {
        if (resolve) {
            NSString *decodeMessage = [NSString decodeP2PMessage:message];
            if (decodeMessage) {
                resolve(@{@"code": @(1), @"data": @{@"message": decodeMessage}});
            } else {
                resolve(@{@"code": @(-1)});
            }
        }
    }];
}

@end
