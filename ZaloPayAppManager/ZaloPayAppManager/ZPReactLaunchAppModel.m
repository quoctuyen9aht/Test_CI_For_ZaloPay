//
//  ZPReactLaunchAppModel.m
//  ZaloPayAppManager
//
//  Created by bonnpv on 11/21/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import "ZPReactLaunchAppModel.h"
#import "ReactNativeAppManager+AppProperty.h"
#import <ZaloPayUI/ZPAppFactory.h>
#import <ZaloPayCommon/ZPConstants.h>

@implementation ZPReactLaunchAppModel
+ (void)launchReactNativeApp:(int)appid
                  properties:(NSMutableDictionary *)properties
                    resolver:(RCTPromiseResolveBlock)resolve
                    rejecter:(RCTPromiseRejectBlock)reject {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSArray *activeAppIds = [[ReactNativeAppManager sharedInstance] activeAppIds];
        NSString *moduleName;
        int appId = appid;
        if ([activeAppIds containsObject:@(appid)] && appId != reactInternalAppId) {
            moduleName = ReactModule_PaymentMain;
        } else {
            appId = (int) reactInternalAppId;
            moduleName = ReactModule_TransactionLogs;
        }
        [[ZPAppFactory sharedInstance] openApp:appId moduleName:moduleName properties:properties];
        if (resolve) {
            resolve(@{@"code": @(1)});
        }
    });
}
@end
