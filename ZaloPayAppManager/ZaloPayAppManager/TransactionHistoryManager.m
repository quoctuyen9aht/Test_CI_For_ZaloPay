//
//  TransactionHistoryManager.m
//  ZaloPayAppManager
//
//  Created by PhucPv on 5/7/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "TransactionHistoryManager.h"
#import <ZaloPayDatabase/PerUserDataBase.h>
#import <ZaloPayDatabase/ZPTransactionLogTable.h>
#import "ZaloPayModuleManager.h"
#import "PaymentModulLog.h"
#import <ZaloPayNetwork/ZaloPayErrorCode.h>
#import <ZaloPayNetwork/NSError+ZaloPayAPI.h>
#import <ZaloPayCommon/NSCollection+JSON.h>
#import <ReactiveObjC/ReactiveObjC.h>
#define TRANS_PAGE_SIZE 20

#define kLoadAllSuccessTransaction  @"LoadAllSuccessTransaction"
#define kLoadAllFailTransaction     @"LoadAllFailTransaction"

#define kSuccessfulMaxTimeStamp     @"SuccessfulMaxTimeStamp"
#define kFailedMaxTimeStamp         @"FailedMaxTimeStamp"

#define kSuccessfulMinTimeStamp     @"SuccessfulMinTimeStamp"
#define kFailedMinTimeStamp         @"FailedMinTimeStamp"

@interface ZPCacheDataTable
- (void)cacheDataUpdateValue:(NSString *)value forKey:(NSString *)key;

- (NSString *)cacheDataValueForKey:(NSString *)key;
@end

@interface TransactionHistoryManager()
@property (nonatomic, strong) ZPTransactionLogTable* transactionLogTable;
@end

@implementation TransactionHistoryManager

- (id) init {
    if (self = [super init]) {
        self.transactionLogTable = [[ZPTransactionLogTable alloc] initWithDB:[PerUserDataBase sharedInstance]];
    }
    return self;
}

- (void)updateTransactionLogFromServer {
    if ([NetworkManager sharedInstance].accesstoken.length == 0) {
        return;
    }
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self loadSuccessTransaction];
        [self loadFailTransaction];
    });
}

#pragma mark - Success Transaction

- (void)loadSuccessTransaction {
    [[self loadTransaction:StatusTypeSuccess] subscribeCompleted:^{
    }];
}

#pragma mark - FailTransaction

- (void)loadFailTransaction {
    [[self loadTransaction:StatusTypeFail] subscribeCompleted:^{
    }];
}

- (RACSignal *)successfulTransactionWithTransTypes:(NSArray *)transTypes
                                      keyTimestamp:(NSTimeInterval)keyTimestamp
                                              sign:(int)sign
                                            offset:(int)offset
                                             count:(int)count {
    return [self transactionLogsWithStatusType:StatusTypeSuccess
                                     transType:transTypes
                                          sign:sign
                                  keyTimeStamp:keyTimestamp
                                        offset:offset
                                         count:count];
}

- (RACSignal *)failedTransactionWithTransTypes:(NSArray *)transTypes
                                  keyTimestamp:(NSTimeInterval)keyTimestamp
                                          sign:(int)sign
                                        offset:(int)offset
                                         count:(int)count {

    return [self transactionLogsWithStatusType:StatusTypeFail
                                     transType:transTypes
                                          sign:sign
                                  keyTimeStamp:keyTimestamp
                                        offset:offset
                                         count:count];
}

#pragma mark - Load Transaction

- (RACSignal *)loadTransaction:(StatusType)type {
    NSTimeInterval maxTimestamp = [self.transactionLogTable maxTimestampFragmentWithKey:MainKeyTimestamp
                                                                                     statusType:type];
    if (maxTimestamp == 0) {
        return [self loadMoreTransaction:type count:TRANS_PAGE_SIZE keyTimestamp:maxTimestamp];
    }
    return [RACSignal createSignal:^RACDisposable *(id <RACSubscriber> subscriber) {
        [self updateMainTransactionLogWithStatusType:type subcriber:subscriber];
        return nil;
    }];
}

- (void)updateMainTransactionLogWithStatusType:(StatusType)statusType
                                     subcriber:(id <RACSubscriber>)subscriber {
    NSTimeInterval maxTimestamp = [self.transactionLogTable maxTimestampFragmentWithKey:MainKeyTimestamp
                                                                                     statusType:statusType];
    [[[NetworkManager sharedInstance] loadTransactionWithTimestamp:maxTimestamp
                                                             count:TRANS_PAGE_SIZE
                                                             order:TransOrderLastest
                                                        statusType:statusType] subscribeNext:^(id x) {
        NSArray *data = [x arrayForKey:@"data"];
        [self.transactionLogTable updateTransactionLogs:data
                                                     statusType:statusType
                                                   keyTimestamp:MainKeyTimestamp
                                                          order:TransOrderLastest];
        NSTimeInterval lastMaxTimestamp = [self.transactionLogTable maxTimestampFragmentWithKey:MainKeyTimestamp
                                                                                             statusType:statusType];


        if (data.count > 0 && lastMaxTimestamp == maxTimestamp) {
            DDLogInfo(@"lấy page mới có time > timestamp server trả về dữ liệu không có time nào lơn hơn timestamp -> stop");
            [self notifyDoneLoadTransaction:statusType];
            return;
        }

        if (data.count == TRANS_PAGE_SIZE) {
            [self updateMainTransactionLogWithStatusType:statusType subcriber:subscriber];
            return;
        }
        [self notifyDoneLoadTransaction:statusType];
        [subscriber sendCompleted];
    }                                                                                  error:^(NSError *error) {
        [self notifyDoneLoadTransaction:statusType];
        [subscriber sendCompleted];
    }];
}

- (void)notifyDoneLoadTransaction:(StatusType)type {
    [[NSNotificationCenter defaultCenter] postNotificationName:ZPTransactionsUpdated
                                                        object:@{@"statusType": @(type)}
                                                      userInfo:@{}];
}

#pragma mark - Load Notification transaction


- (void)updateTransctionLogWithNotificationTimestamp:(uint64_t)timestamp {
    if (timestamp == 0) {
        return;
    }
    NSTimeInterval minTime = [self.transactionLogTable minTimestampFragmentWithKey:timestamp statusType:StatusTypeSuccess];
    if (timestamp >= minTime) {
        return;
    }
    [[self loadMoreTransaction:StatusTypeSuccess count:TRANS_PAGE_SIZE keyTimestamp:MainKeyTimestamp] subscribeNext:^(NSArray *data) {
        if (data.count < TRANS_PAGE_SIZE) {
            return;
        }
        [self updateTransctionLogWithNotificationTimestamp:timestamp];
    }];
}


#pragma mark - React Native

- (NSArray *)getTransactionDetail:(NSString *)transId {
    return [self.transactionLogTable loadTransactionLogWithId:transId];
}

- (RACSignal *)loadTransactionHistoryWithId:(NSString *)transId {
    NSArray *result = [self.transactionLogTable loadTransactionLogWithId:transId];
    return [RACSignal return:@{@"code": @(1),
            @"data": result}];
}

- (RACSignal *)requestTransaction:(uint64_t)transId time:(uint64_t)reqTime {
    return [RACSignal createSignal:^RACDisposable *(id <RACSubscriber> subscriber) {
        NSArray *result = [self.transactionLogTable loadTransactionLogWithId:[@(transId) stringValue]];
        if (result.count > 0) {
            [subscriber sendCompleted];
            return nil;
        }

        NSTimeInterval timeInterval = reqTime + 5;
        [[[[NetworkManager sharedInstance] loadTransactionWithTimestamp:timeInterval
                                                                  count:5
                                                                  order:TransOrderOldest
                                                             statusType:StatusTypeSuccess] flattenMap:^__kindof RACSignal * _Nullable(NSDictionary *
        _Nullable value) {

        NSArray *data = [value arrayForKey:@"data"];
        [self.transactionLogTable updateTransactionLogs:data
                                                     statusType:StatusTypeSuccess
                                                   keyTimestamp:reqTime
                                                          order:TransOrderOldest];

        for (NSDictionary *oneDic in data) {
            uint64_t oneTransId = [[oneDic objectForKey:@"transid"] longLongValue];
            if (oneTransId == transId) {
                return [RACSignal empty];
            }
        }
        return [RACSignal error:nil];
    }] subscribe:
        subscriber];
        return nil;
    }];
}

- (RACSignal *)transactionLogsWithStatusType:(StatusType)statusType
                                   transType:(NSArray *)transTypes
                                        sign:(int)sign
                                keyTimeStamp:(NSTimeInterval)keyTimestamp
                                      offset:(int)offset
                                       count:(int)count {
    return [[RACSignal createSignal:^RACDisposable *(id <RACSubscriber> subscriber) {
        [self tryLoadMoreTransactionLogsWithStatusType:statusType
                                             transType:transTypes
                                                  sign:sign
                                          keyTimeStamp:keyTimestamp
                                                offset:offset
                                                 count:count
                                             loopIndex:0
                                            subscriber:subscriber];
        return nil;
    }] replayLazily];
}

- (void)tryLoadMoreTransactionLogsWithStatusType:(StatusType)statusType
                                       transType:(NSArray *)transTypes
                                            sign:(int)sign
                                    keyTimeStamp:(NSTimeInterval)keyTimestamp
                                          offset:(int)offset
                                           count:(int)count
                                       loopIndex:(int)loopIndex
                                      subscriber:(id <RACSubscriber>)subscriber {
    NSArray *data = [self.transactionLogTable transactionWithStatusType:statusType
                                                                     transTypes:transTypes
                                                                   keyTimestamp:keyTimestamp
                                                                           sign:(int) sign
                                                                         offset:offset
                                                                          count:count];
    BOOL outOfData = [self.transactionLogTable outOfDataWithTimestamp:keyTimestamp statusType:statusType];
    if ([data count] == count || loopIndex >= 5) {
        [subscriber sendNext:@{@"data": data,
                @"code": @(ZALOPAY_ERRORCODE_SUCCESSFUL)}];
    } else if (outOfData) {
        [subscriber sendNext:@{@"data": data,
                @"code": @(ZALOPAY_ERRORCODE_OUT_OF_DATA)}];
    } else {
        [[self loadMoreTransaction:statusType count:count keyTimestamp:keyTimestamp] subscribeNext:^(id x) {
            [self tryLoadMoreTransactionLogsWithStatusType:statusType
                                                 transType:transTypes
                                                      sign:sign
                                              keyTimeStamp:keyTimestamp
                                                    offset:offset
                                                     count:count
                                                 loopIndex:loopIndex + 1
                                                subscriber:subscriber];
        }                                                                                    error:^(NSError *error) {
            NSArray *data = [self.transactionLogTable transactionWithStatusType:statusType
                                                                             transTypes:transTypes
                                                                           keyTimestamp:keyTimestamp
                                                                                   sign:(int) sign
                                                                                 offset:offset
                                                                                  count:count];
            [subscriber sendNext:@{@"data": data,
                    @"code": @(error.code),
                    @"message": [error apiErrorMessage]}];
        }];
    }
}


- (NSDictionary *)fetchTransactionLogsWithStatusType:(StatusType)statusType
                                           transType:(NSArray *)transTypes
                                                sign:(int)sign
                                        keyTimeStamp:(NSTimeInterval)keyTimestamp
                                              offset:(int)offset
                                               count:(int)count {
    NSArray *data = [self.transactionLogTable transactionWithStatusType:statusType
                                                                     transTypes:transTypes
                                                                   keyTimestamp:keyTimestamp
                                                                           sign:(int) sign
                                                                         offset:offset
                                                                          count:count];
    BOOL outOfData = [self.transactionLogTable outOfDataWithTimestamp:keyTimestamp statusType:statusType];
    int code = ZALOPAY_ERRORCODE_SUCCESSFUL;
    if ([data count] != count && outOfData) {
        code = ZALOPAY_ERRORCODE_OUT_OF_DATA;
    }

    return @{@"data": data,
            @"code": @(code)};
}


- (void)loadMoreTransaction:(StatusType)type
                      count:(int)count
                  loopCount:(int)loopCount
               keyTimestamp:(NSTimeInterval)keyTimestamp
                 subscriber:(id <RACSubscriber>)subscriber {
    __block int _loopCount = loopCount;
    [[self loadMoreTransaction:type count:count keyTimestamp:keyTimestamp] subscribeNext:^(id data) {
        _loopCount--;
        if ([data count] == count && _loopCount > 0) {
            [self loadMoreTransaction:type
                                count:count
                            loopCount:_loopCount
                         keyTimestamp:keyTimestamp
                           subscriber:subscriber];
        } else {
            [subscriber sendCompleted];
        }
    }];
}

- (RACSignal *)reloadOldestTransaction:(StatusType)statusType
                             timeStamp:(NSTimeInterval)timeStamp {
    return [self requestOldestTransaction:timeStamp count:TRANS_PAGE_SIZE statusType:statusType];
}

- (RACSignal *)loadMoreTransaction:(StatusType)statusType
                             count:(int)count
                      keyTimestamp:(NSTimeInterval)keyTimestamp {
    NSTimeInterval minTimeStamp = [self.transactionLogTable minTimestampFragmentWithKey:keyTimestamp
                                                                                     statusType:statusType];
    if ([self.transactionLogTable outOfDataWithTimestamp:keyTimestamp statusType:statusType]) {
        return [RACSignal return:@[]];
    }
    return [self requestOldestTransaction:minTimeStamp count:count statusType:statusType];
}

- (RACSignal *)requestOldestTransaction:(NSTimeInterval)timeStamp
                                  count:(int)count
                             statusType:(StatusType)statusType {
    return [[[NetworkManager sharedInstance] loadTransactionWithTimestamp:timeStamp
                                                                    count:count
                                                                    order:TransOrderOldest
                                                               statusType:statusType] map:^id(id value) {
        NSArray *data = [value arrayForKey:@"data"];
        [self.transactionLogTable updateTransactionLogs:data
                                         statusType:statusType
                                       keyTimestamp:timeStamp
                                              order:TransOrderOldest];
        if ([data count] != count) {
            [self.transactionLogTable setOutOfDataWithTimestamp:timeStamp statusType:statusType];
        }

        return data;
    }];
}


- (RACSignal *)removeTransactionWithId:(NSString *)transid {
    return [RACSignal createSignal:^RACDisposable *(id <RACSubscriber> subscriber) {
        BOOL result = [self.transactionLogTable removeTransactionWithId:transid];
        if (result) {
            [subscriber sendNext:@{@"code": @(1)}];
        } else {
            [subscriber sendNext:@{@"code": @(-1)}];
        }
        return nil;
    }];
}

- (RACSignal *)updateThankMessageWithTransId:(NSString *)transid
                               encodeMessage:(NSString *)encodeMessage {
    return [RACSignal createSignal:^RACDisposable *(id <RACSubscriber> subscriber) {
        BOOL result = [self.transactionLogTable updateThankMessageWithTransId:transid
                                                                        encodeMessage:encodeMessage];
        if (result) {
            [subscriber sendNext:@{@"code": @(1)}];
        } else {
            [subscriber sendNext:@{@"code": @(-1)}];
        }
        return nil;
    }];
}

@end
