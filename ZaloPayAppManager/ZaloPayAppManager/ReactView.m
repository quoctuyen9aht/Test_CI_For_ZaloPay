//
//  ReactView.m
//  ZaloPay
//
//  Created by Nguyễn Hữu Hoà on 3/23/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ReactView.h"
#import <Masonry/Masonry.h>

#import <React/RCTRootView.h>
#import "PaymentModulLog.h"

@implementation ReactView
- (instancetype)init:(NSInteger)instanceId bridge:(RCTBridge *)bridge {
    return [self initWithModuleName:@"SimpleApp" instanceId:instanceId bridge:bridge properties:nil];
}

- (instancetype)initWithModuleName:(NSString *)moduleName instanceId:(NSInteger)instanceId bridge:(RCTBridge *)bridge {
    return [self initWithModuleName:moduleName instanceId:instanceId bridge:bridge properties:nil];
}

- (instancetype)initWithModuleName:(NSString *)moduleName instanceId:(NSInteger)instanceId bridge:(RCTBridge *)bridge properties:(NSDictionary *)properties {
    self = [super init];

    NSMutableDictionary *props = [NSMutableDictionary dictionaryWithDictionary:properties];
    [props setObject:@(instanceId) forKey:@"moduleId"];

    RCTRootView *rootView = [[RCTRootView alloc] initWithBridge:bridge
                                                     moduleName:moduleName
                                              initialProperties:props];

    [self addSubview:rootView];
    rootView.frame = self.bounds;
    [rootView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@0);
        make.right.equalTo(@0);
        make.top.equalTo(@0);
        make.bottom.equalTo(@0);
    }];
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];

    DDLogDebug(@"ReactView frame: %@", [NSValue valueWithCGRect:self.frame]);
    for (UIView *view in self.subviews) {
        DDLogDebug(@"%@ frame: %@", view, [NSValue valueWithCGRect:view.frame]);
    }
}
@end
