//
//  RCTBridge+Payment.m
//  ZaloPayAppManager
//
//  Created by bonnpv on 5/19/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "RCTBridge+Payment.h"
#import "ReactFactory.h"

//extern NSString *kReactInternalSource;
//extern NSString *kReactExternalSource;

@implementation RCTBridge (Payment)

//+ (instancetype)bridgeWithBundleResouce:(NSString *)bundleResouce {
//    
//    BOOL serverResouce = [bundleResouce hasPrefix:@"http:"];
//    NSURL *jsCodeLocation;
//    
//    if (serverResouce) {
//        jsCodeLocation = [NSURL URLWithString:bundleResouce];
//    }else {
//        jsCodeLocation = [[NSBundle mainBundle] URLForResource:bundleResouce withExtension:@"jsbundle"];
//    }
//    
//    return  [[RCTBridge alloc] initWithBundleURL:jsCodeLocation
//                                  moduleProvider:nil
//                                   launchOptions:nil];
//}

+ (instancetype)internalBridge {
    return [[ReactFactory sharedInstance] bridge];

//    static RCTBridge *internal;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        internal = [[RCTBridge alloc] initWithBundleURL:[ReactConfig config].internalBundle
//                                          moduleProvider:nil
//                                           launchOptions:nil];
//    });
//    return internal;
}

+ (instancetype)externalBridge:(NSInteger)appId {
    return [[ReactFactory sharedInstance] registerPaymentApp:appId];
//    NSURL *url = [[ReactConfig config] externalBundle:appId];
//    if (url) {
//        return [[RCTBridge alloc] initWithBundleURL:url
//                                     moduleProvider:nil
//                                      launchOptions:nil];
//    }
//    DDLogInfo(@"load nil jsbundle url : %d",appId);
//    return nil;
}

@end
