//
//  ZaloPayModuleManager.m
//  ZaloPayAppManager
//
//  Created by Nguyễn Hữu Hoà on 7/21/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZaloPayModuleManager.h"

NSString *const ZPNotificationsAddedNotification = @"ZPNotificationsAddedNotification";
NSString *const ZPTransactionsUpdatedNotification = @"ZPTransactionsUpdatedNotification";
NSString *const ZPTransactionsUpdated = @"zalopayTransactionsUpdated";
NSString *const ZPTransactionDetailUpdated = @"zalopayTransactionDetailUpdated";
