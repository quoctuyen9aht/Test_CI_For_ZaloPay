//
//  ZPReactLaunchAppModel.h
//  ZaloPayAppManager
//
//  Created by bonnpv on 11/21/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface ZPReactLaunchAppModel : NSObject
+ (void)launchReactNativeApp:(int)appid
                  properties:(NSMutableDictionary *)properties
                    resolver:(RCTPromiseResolveBlock)resolve
                    rejecter:(RCTPromiseRejectBlock)reject;
@end
