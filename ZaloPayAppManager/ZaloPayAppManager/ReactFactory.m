//
//  ReactFactory.m
//  ZaloPay
//
//  Created by Nguyễn Hữu Hoà on 3/26/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ReactFactory.h"
#import <React/RCTBridge.h>
#import <ZaloPayCommon/ZPConstants.h>
#import "ReactConfig.h"
#import "ReactModuleViewController.h"
#import "PaymentModulLog.h"
#import "ReacExceptionHandle.h"
#import <React/RCTAssert.h>

@interface ReactModuleViewController (ReactFactory)
@property(nonatomic) NSInteger appId;
@end

@interface ReactFactory ()
@property(nonatomic, strong) NSMutableDictionary *moduleInstances;
@property(nonatomic) NSInteger nextInstanceId;
@property(nonatomic, strong) NSMutableDictionary *bridgeInstances;
@property(nonatomic, strong) RCTBridge *internalAppBridge;
@property(nonatomic, strong) NSMutableDictionary *exeptionHandles;
@end

@implementation ReactFactory
static ReactFactory *sharedInstance;

+ (instancetype)sharedInstance {
    static dispatch_once_t reactFactoryOnceToken;

    dispatch_once(&reactFactoryOnceToken, ^{
        sharedInstance = [[[self class] alloc] init];
    });

    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    self.moduleInstances = [NSMutableDictionary new];
    self.bridgeInstances = [NSMutableDictionary new];
    self.exeptionHandles = [NSMutableDictionary new];
    self.nextInstanceId = 1;
#ifndef DEBUG
    [self catchFatalError];
#endif
    return self;
}

- (RCTBridge *)internalAppBridge {
    if (!_internalAppBridge) {
        NSURL *bundleUrl = [ReactConfig config].internalBundle;
        if (!bundleUrl) {
            DDLogInfo(@"internal bundle = nil.");
            return nil;
        }
        _internalAppBridge = [[RCTBridge alloc] initWithBundleURL:bundleUrl
                                                   moduleProvider:[self reactExceptionHandle:reactInternalAppId]
                                                    launchOptions:nil];
    }
    return _internalAppBridge;
}

- (RCTBridge *)bridge {
    return self.internalAppBridge;
}


- (NSInteger)registerModuleInstance:(UIViewController *)viewController {
    if (viewController == nil) {
        return 0;
    }

    NSInteger instanceId = self.nextInstanceId;
    [self.moduleInstances setObject:viewController forKey:@(instanceId)];
    self.nextInstanceId = self.nextInstanceId + 1;
    return instanceId;
}

- (void)replaceModuleInstance:(NSInteger)instanceId
           withViewController:(UIViewController *)viewController
           fromViewController:(UIViewController *)fromViewController {
    UIViewController *oldVC = [self.moduleInstances objectForKey:@(instanceId)];
    if (oldVC == nil) {
        return;
    }
    if (oldVC.navigationController == nil && oldVC.presentingViewController) {
        [oldVC dismissViewControllerAnimated:YES completion:nil];
        [fromViewController.navigationController pushViewController:viewController animated:YES];
    } else {
        UINavigationController *navi = oldVC.navigationController;
        NSMutableArray *stackViewControllers = [NSMutableArray arrayWithArray:navi.viewControllers];
        [stackViewControllers removeObject:oldVC];
        [stackViewControllers addObject:viewController];
        [navi setViewControllers:stackViewControllers animated:YES];
        [self.moduleInstances removeObjectForKey:@(instanceId)];
    }
}

- (void)closeModuleInstance:(NSInteger)instanceId {

    UIViewController *vc = [self.moduleInstances objectForKey:@(instanceId)];

    if (vc == nil) {
        return;
    }

    if (vc.navigationController.viewControllers.count > 1) {
        [vc.navigationController popViewControllerAnimated:YES];
        [self removeModuleInstnce:instanceId];
        return;
    }
    
    if (vc.presentingViewController) {
        [vc dismissViewControllerAnimated:YES completion:nil];
        [self removeModuleInstnce:instanceId];
        return;
    }
    
    [vc.navigationController popToRootViewControllerAnimated:true];
}

- (void)removeModuleInstnce:(NSInteger)instanceId {
    [self.moduleInstances removeObjectForKey:@(instanceId)];
}


//! Register and pre-load the js for module
- (RCTBridge *)registerPaymentApp:(NSInteger)appId {
    if (appId == reactInternalAppId) {
        return self.internalAppBridge;
    }

    RCTBridge *instance = [self.bridgeInstances objectForKey:@(appId)];
    if (instance != nil) {
        return instance;
    }

    NSURL *url = [[ReactConfig config] externalBundle:(int) appId];
    if (url) {
        instance = [[RCTBridge alloc] initWithBundleURL:url
                                         moduleProvider:[self reactExceptionHandle:appId]
                                          launchOptions:nil];
    }

    if (instance == nil) {
        return nil;
    }

    [self.bridgeInstances setObject:instance forKey:@(appId)];
    return instance;
}

////! Get bridge for module
//- (RCTBridge *)bridgeForApp:(NSInteger)appId {
//    return [self.bridgeInstances objectForKey:@(appId)];
//}

- (void)releaseBridge:(NSInteger)appId {
    if (appId == reactInternalAppId) {
        _internalAppBridge = nil;
        return;
    }
    [self.bridgeInstances removeObjectForKey:@(appId)];
}

- (void)reloadAllViewWithAppId:(NSInteger)appId {
    NSArray *allKey;
    @synchronized(self.moduleInstances) {
        allKey = [self.moduleInstances.allKeys copy];
    }
    NSMutableArray *toReload = [NSMutableArray array];
    for (NSNumber *instanceId in allKey) {
        UIViewController *viewController = [self.moduleInstances objectForKey:instanceId];
        if ([viewController isKindOfClass:[ReactModuleViewController class]]) {
            ReactModuleViewController *reacModule = (ReactModuleViewController *) viewController;
            if (appId > 0 && reacModule.appId == appId) {
                [toReload addObject:reacModule];
            }
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self releaseBridge:appId];
        for (ReactModuleViewController *reactView in toReload) {
            [reactView reloadReactModule];
        }
    });
}

- (void)clearAllData {
    @synchronized(self) {
        [self.moduleInstances removeAllObjects];
        [self.bridgeInstances removeAllObjects];
        self.internalAppBridge = nil;
        [self.exeptionHandles removeAllObjects];
    }    
}

#pragma mark - Exception Handle

#ifdef DEBUG

- (RCTBridgeModuleListProvider)reactExceptionHandle:(NSInteger)appId {
    return nil;
}

#else
- (RCTBridgeModuleListProvider)reactExceptionHandle:(NSInteger)appId {
    return ^{
        ReacExceptionHandle* handle = [self.exeptionHandles objectForKey:@(appId)];
        if (!handle) {
            handle = [[ReacExceptionHandle alloc] initWithAppId:appId];
            [self.exeptionHandles setObject:handle forKey:@(appId)];
        }
        return @[[[RCTExceptionsManager alloc] initWithDelegate:handle]];
    };
}

- (void)catchFatalError {
    RCTSetFatalHandler(^(NSError *error) {
        DDLogInfo(@"RCTSetFatalHandler :%@",error);
    });
}
#endif

@end
