//
//  ZaloPayHelper.m
//  ZaloPayGateway
//
//  Created by Bon Bon on 12/18/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import "ZaloPayApiHelper.h"
#import "PaymentModulLog.h"

#import <ReactiveObjC/ReactiveObjC.h>
#import <ZaloPayCommon/NSCollection+JSON.h>

#import <ZaloPayUI/ZPAppFactory.h>
#import <ZaloPayUI/ZPDialogView.h>

#import <ZaloPayNetwork/NetworkManager.h>

@interface NetworkManager (ZaloPayReactNativeInternal)
- (RACSignal *)getOrderFromAppId:(NSInteger)appid transToken:(NSString *)transToken;
@end

@implementation ZaloPayApiHelper

+ (void)payOrder:(NSDictionary *)param resolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject {
    DDLogInfo(@"about to process payment with payOrder :%@", param);
    dispatch_async(dispatch_get_main_queue(), ^{

        NSInteger appId = [param intForKey:@"appid"];
        if ([[ZPAppFactory sharedInstance] isPayingBill]) {
            DDLogError(@"pay bill twice appid = %ld", (long)appId);
            if (resolve) {
                NSDictionary *response = @{@"code": @(-2), @"data": @""};
                resolve(response);
            }
            return;
        }

        NSString *zptranstoken = [param stringForKey:@"zptranstoken"];
        NSString *apptransid = [param stringForKey:@"apptransid"];

        if (appId > 0 && apptransid.length > 0) {
            [self payOrderWithParams:param appId:appId resolver:resolve rejecter:reject];
            return;
        }

        if (appId > 0 && zptranstoken.length > 0) {
            [self payOrderWithTranstoken:zptranstoken appId:appId resolver:resolve rejecter:reject];

            return;
        }

        NSDictionary *response = @{@"code": @(2), @"data": @""};
        if (resolve) {
            resolve(response);
        }
    });
}

+ (void)payOrderWithTranstoken:(NSString *)zptranstoken
                         appId:(NSInteger)appId
                      resolver:(RCTPromiseResolveBlock)resolve
                      rejecter:(RCTPromiseRejectBlock)reject {
    [[ZPAppFactory sharedInstance] showHUDAddedTo:nil];
    @weakify(self);
    [[[[NetworkManager sharedInstance] getOrderFromAppId:appId transToken:zptranstoken] deliverOnMainThread] subscribeNext:^(NSDictionary *dic) {
        @strongify(self);
        DDLogInfo(@"get order : %@", dic);
        [self payOrderWithParams:dic appId:appId resolver:resolve rejecter:reject];
    }                                                                                                                error:^(NSError *error) {
        [[ZPAppFactory sharedInstance] hideAllHUDsForView:nil];
        [ZPDialogView showDialogWithError:error handle:^{
            NSDictionary *response = @{@"code": @(2), @"data": @""};
            if (resolve) {
                resolve(response);
            }
        }];
    }];
}

+ (void)payOrderWithParams:(NSDictionary *)param
                     appId:(NSInteger)appId
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject {

    UINavigationController *nav = [[ZPAppFactory sharedInstance] rootNavigation];

    if (![nav isKindOfClass:[UINavigationController class]]) {
        DDLogError(@"The rootViewController is not kind of UINavigationController. Something must be wrong");
        if (resolve) {
            NSDictionary *response = @{@"code": @(4), @"data": @""};
            resolve(response);
        }
        return;
    }
    __weak UIViewController *startViewController = nav.topViewController;
    __weak UINavigationController *weakNavi = nav;
    void (^successBlock)(NSDictionary *data) = ^(NSDictionary *data) {
        NSString *zptransid = [data stringForKey:@"zptransid" defaultValue:@""];
        NSString *apptransid = [param stringForKey:@"apptransid" defaultValue:@""];
        int result = [data intForKey:@"errorcode"];
        NSDictionary *response = @{
                @"code": @(1),
                @"data": @{@"zptransid": zptransid, @"apptransid": apptransid, @"result": @(result)}
        };

        DDLogInfo(@"Response to merchant app: %@", response);
        if (resolve) {
            resolve(response);
        }
        [self navigationController:weakNavi popToViewController:startViewController];
    };

    void (^errorBlock)(NSDictionary *data) = ^(NSDictionary *data) {
        if (resolve) {
            NSDictionary *response = @{@"code": @(-1), @"data": @""};
            resolve(response);
        }
        [self navigationController:weakNavi popToViewController:startViewController];
    };

    void (^cancelBlock)(NSDictionary *data) = ^(NSDictionary *data) {
        if (resolve) {
            NSDictionary *response = @{@"code": @(4), @"data": @""};
            resolve(response);
        }
        [self navigationController:weakNavi popToViewController:startViewController];
    };

    [[ZPAppFactory sharedInstance] showZaloPayGateway:startViewController
                                        completeBlock:successBlock
                                          cancelBlock:cancelBlock
                                           errorBlock:errorBlock
                                                appId:appId
                                             billData:param];

}

+ (void)navigationController:(UINavigationController *)navi popToViewController:(UIViewController *)viewController {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (viewController == nil || navi == nil) {
            return;
        }
        if (![navi isKindOfClass:[UINavigationController class]]) {
            return;
        }
        if ([navi.viewControllers containsObject:viewController]) {
            [navi popToViewController:viewController animated:YES];
        }
    });
}

@end
