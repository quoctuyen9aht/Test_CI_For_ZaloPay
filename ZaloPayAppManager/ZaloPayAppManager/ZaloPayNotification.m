//
//  ZaloPayNotification.m
//  ZaloPayAppManager
//
//  Created by PhucPv on 6/9/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//
#import "ZaloPayNotification.h"
#import "ZPReactLaunchAppModel.h"
#import "ReactNativeAppManager+AppProperty.h"
#import "TransactionHistoryManager.h"

#import <ReactiveObjC/ReactiveObjC.h>
#import <ZaloPayDatabase/ZPNotifyTable.h>
#import <ZaloPayDatabase/PerUserDataBase.h>
#import <ZaloPayUI/ZPAppFactory.h>
#import <ZaloPayCommon/NSCollection+JSON.h>

@implementation ZaloPayNotification

- (id) init {
    if (self = [super init]) {
        self.notifyTable = [[ZPNotifyTable alloc] initWithDB:[PerUserDataBase sharedInstance]];
        self.transactionHistoryManager = [[TransactionHistoryManager alloc] init];
    }
    return self;
}

RCT_EXPORT_MODULE()
RCT_EXPORT_METHOD(getNotification:(NSInteger)pageIndex
                  count:(NSInteger)count
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
    long index = pageIndex * count;
    NSArray *listNotify = [self.notifyTable getListNotifyMessageFrom:index count:count];
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:@(1) forKey:@"code"];
    if (listNotify) {
        [param setObject:listNotify forKey:@"data"];
    }
    if(resolve) {
        resolve(param);
    }
}

RCT_EXPORT_METHOD(updateStateReadWithNotificationId:(NSString *) notificationid) {
    if (notificationid) {
        [self.notifyTable updateStateReadWithNotificationId:notificationid];
        
        NSArray *arrayIds = [notificationid componentsSeparatedByString:@"_"];
        uint64_t mtaid = [arrayIds.firstObject longLongValue];
        uint64_t mtuid = [arrayIds.lastObject longLongValue];
        [[ZPAppFactory sharedInstance] sendReadMessageWithMtaid:mtaid mtuid:mtuid];
    }
}

RCT_REMAP_METHOD(removeAllNotification,
                 resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject) {
    BOOL result = [self.notifyTable deleteAlllNotification];
    if (resolve) {
        resolve(@{@"code":result?@1:@2});
    }
}

RCT_EXPORT_METHOD(removeNotification:(NSString *)notificationid
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
    BOOL result = [self.notifyTable deleteNotificationWithId:notificationid];
    if (resolve) {
        resolve(@{@"code":result?@1:@2});
    }
    NSArray *arrayIds = [notificationid componentsSeparatedByString:@"_"];
    uint64_t mtaid = [arrayIds.firstObject longLongValue];
    uint64_t mtuid = [arrayIds.lastObject longLongValue];    
    [[ZPAppFactory sharedInstance] sendRemoveMessageWithMtaid:mtaid mtuid:mtuid];
}

RCT_EXPORT_METHOD(openApp:(NSInteger)appId
                  moduleName:(NSString *)moduleName
                  optionlaunch:(NSDictionary *)options
                  resolve:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
    
    //https://gitlab.zalopay.vn/zalopay-apps/task/issues/1140
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[ZPAppFactory sharedInstance] launchApp:appId
                                      moduleName:moduleName
                                      properties:options
                                         success:^(NSDictionary *data)
         {
             
             if (resolve) {
                 resolve(@{@"code": @(1)});
             }
         } error:^(NSDictionary *data) {
             if (resolve) {
                 resolve(@{@"code": @(1011)});
             }
         } unvailable:^(NSDictionary *data) {
             if (resolve) {
                 resolve(@{@"code": @(1010)});
             }
         }];
    });
}

RCT_EXPORT_METHOD(launchTransactionDetailFromNotification:(int) appid
                  transid:(NSString *)transid
                  properties:(NSDictionary *) params
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
    NSArray *detail = [self.transactionHistoryManager getTransactionDetail:transid];
    NSMutableDictionary *properties = [[ReactNativeAppManager sharedInstance] reactProperties:(int)appid];
    [properties addEntriesFromDictionary:params];
    [properties setObjectCheckNil:@"history" forKey:@"view"];
    if (detail.count > 0) {
        NSDictionary *transactionDetail = [detail firstObject];
        [properties setObjectCheckNil:transactionDetail forKey:@"transactiondetail"];
        [properties setObjectCheckNil:[transactionDetail objectForKey:@"discountamount"] forKey:@"discountamount"];
    }
    [ZPReactLaunchAppModel launchReactNativeApp:appid properties:properties resolver:resolve rejecter:reject];
}

RCT_EXPORT_METHOD(trackOpenNotification:(NSDictionary *) params){
    [[ZPAppFactory sharedInstance] trackNotification:params];
}

@end
