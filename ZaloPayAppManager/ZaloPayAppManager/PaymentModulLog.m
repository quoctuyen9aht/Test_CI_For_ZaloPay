//
//  PaymentModulLog.m
//  ZaloPayAppManager
//
//  Created by bonnpv on 6/8/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "PaymentModulLog.h"

const DDLogLevel paymentModulLogLevel = DDLogLevelInfo;
