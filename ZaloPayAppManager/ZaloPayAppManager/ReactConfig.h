//
//  ReactConfig.h
//  ZaloPayAppManager
//
//  Created by bonnpv on 5/19/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReactConfig : NSObject
- (NSURL *)internalBundle;

- (NSURL *)externalBundle:(NSInteger)appId;

+ (ReactConfig *)config;

+ (void)setConfig:(ReactConfig *)config;
@end
