//
//  ZaloPayNotification.h
//  ZaloPayAppManager
//
//  Created by PhucPv on 6/9/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@class ZPNotifyTable;
@class TransactionHistoryManager;

@interface ZaloPayNotification : NSObject <RCTBridgeModule>
@property (nonatomic, strong, nonnull) ZPNotifyTable* notifyTable;
@property (nonatomic, strong, nonnull) TransactionHistoryManager* transactionHistoryManager;
@end
