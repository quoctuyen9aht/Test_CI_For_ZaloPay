//
//  PaymentModulLog.h
//  ZaloPayAppManager
//
//  Created by bonnpv on 6/8/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CocoaLumberjack/CocoaLumberjack.h>

extern const DDLogLevel paymentModulLogLevel;
#define ddLogLevel  paymentModulLogLevel
