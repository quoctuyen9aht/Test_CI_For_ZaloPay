//
//  ZPEventsObserver.m
//  ZaloPayAppManager
//
//  Created by Nguyễn Hữu Hoà on 7/21/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPEventsObserver.h"
#import "PaymentModulLog.h"

extern NSString *const ZPNotificationsAddedNotification;
extern NSString *const ZPTransactionsUpdated;
extern NSString *const ZPTransactionDetailUpdated;

@implementation ZPEventsObserver
RCT_EXPORT_MODULE(EventsObserver)

/**
 * Override this method to return an array of supported event names. Attempting
 * to observe or send an event that isn't included in this list will result in
 * an error.
 */
- (NSArray<NSString *> *)supportedEvents {
    return @[
            @"zalopayNotificationsAdded",
            ZPTransactionsUpdated,
            ZPTransactionDetailUpdated
    ];
}

/**
 * Send an event that does not relate to a specific view, e.g. a navigation
 * or data update notification.
 */
//- (void)sendEventWithName:(NSString *)name body:(id)body;

/**
 * These methods will be called when the first observer is added and when the
 * last observer is removed (or when dealloc is called), respectively. These
 * should be overridden in your subclass in order to start/stop sending events.
 */
- (void)startObserving {
    DDLogInfo(@"startObserving event");
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];

#define ADD_ZALOPAY_HANDLER(NAME, SELECTOR) \
    [nc addObserver:self selector:@selector(SELECTOR:) name:NAME object:nil]

    ADD_ZALOPAY_HANDLER(ZPNotificationsAddedNotification, zalopayNotificationsAdded);
    ADD_ZALOPAY_HANDLER(ZPTransactionsUpdated, zalopayTransactionsUpdated);
    ADD_ZALOPAY_HANDLER(ZPTransactionDetailUpdated, zalopayTransactionsDetailUpdated);

#undef ADD_ZALOPAY_HANDLER
}

- (void)stopObserving {
    DDLogInfo(@"stopObserving event");
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)zalopayNotificationsAdded:(NSNotification *)notification {
//    NSDictionary *userInfo = notification.userInfo;
//    NSTimeInterval duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
//    UIViewAnimationCurve curve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
//    
//    NSDictionary body = @{
//             @"startCoordinates": RCTRectDictionaryValue(beginFrame),
//             @"endCoordinates": RCTRectDictionaryValue(endFrame),
//             @"duration": @(duration * 1000.0), // ms
//             @"easing": RCTAnimationNameForCurve(curve),
//             };

    [self sendEventWithName:@"zalopayNotificationsAdded" body:@{}];
}

- (void)zalopayTransactionsUpdated:(NSNotification *)notification {
    [self sendEventWithName:ZPTransactionsUpdated body:notification.object];
}

- (void)zalopayTransactionsDetailUpdated:(NSNotification *)notification {
    [self sendEventWithName:ZPTransactionDetailUpdated body:notification.object];
}

- (void)dealloc {
    // Remove observer
    [self stopObserving];
}
@end
