//
//  ZContacts.m
//  ZContacts
//
//  Created by Duy Huynh on 7/27/16.
//  Copyright © 2016 Facebook. All rights reserved.
//

#import "ZContacts.h"
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>
#import <AddressBook/AddressBook.h>

//@import AddressBook;
//@import Contacts;

@interface ZContactsA () <CNContactPickerDelegate, CNContactViewControllerDelegate>

@end

@implementation ZContactsA {
    NSMutableArray<RCTResponseSenderBlock> *_callbacks;

};

RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(requestAccessContact:
    (RCTResponseSenderBlock) accessCallback) {
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied ||
            ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusRestricted) {
        //Denied
        accessCallback(@[@false]);
        return;
    }
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
        accessCallback(@[@true]);
        return;
        //Authorized
    }
    //ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined
    //Not determined
    ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil), ^(bool granted, CFErrorRef error) {
        accessCallback(@[@(granted)]);
    });
}

RCT_REMAP_METHOD(getIOSVersion,
            resolver:
            (RCTPromiseResolveBlock) resolve
            rejecter:
            (RCTPromiseRejectBlock) reject) {
    NSString *version = [[UIDevice currentDevice] systemVersion];
    if (resolve) {
        resolve(version);
    }
}

RCT_EXPORT_METHOD(lookupPhoneNumber:
    (NSString *) thePhoneNumber
            callback:
            (RCTResponseSenderBlock) callback) {
    NSUInteger i;
    NSUInteger k;
    Boolean bFound = false;

    ABAddressBookRef addressBook = ABAddressBookCreate();
    NSArray *people = (__bridge NSArray *) ABAddressBookCopyArrayOfAllPeople(addressBook);

    if (people == nil) {
        //NO ADDRESS BOOK ENTRIES TO SCAN
        if (addressBook != nil) {
            CFRelease(addressBook);
        }

        callback(@[@false]);
        return;
    }

    for (i = 0; i < [people count]; i++) {
        ABRecordRef person = (__bridge ABRecordRef) [people objectAtIndex:i];

        //
        // Phone Numbers
        //
        ABMutableMultiValueRef phoneNumbers = ABRecordCopyValue(person, kABPersonPhoneProperty);
        CFIndex phoneNumberCount = ABMultiValueGetCount(phoneNumbers);

        for (k = 0; k < phoneNumberCount; k++) {
            CFStringRef phoneNumberValue = ABMultiValueCopyValueAtIndex(phoneNumbers, k);

            // Find phone number
            NSString *phoneNumber = (__bridge NSString *) phoneNumberValue;
            if ([phoneNumber isEqualToString:thePhoneNumber]) {
                bFound = true;
            }

            CFRelease(phoneNumberValue);

            if (bFound) {
                break;
            }
        }
    }

    //[people release];
    CFRelease(addressBook);

    if (bFound) {
        callback(@[@true]);
    } else {
        callback(@[@false]);
    }
}

RCT_EXPORT_METHOD(openContactPicker:
    (RCTResponseSenderBlock) callback) {
    _callbacks = [NSMutableArray new];
    [_callbacks addObject:callback];
    
//    UIViewController *presentingController = RCTKeyWindow().rootViewController;
//    if (presentingController == nil) {
//        RCTLogError(@"Tried to display contact picker, but there is no application window.@");
//    }
//
//    // Walk the chain to the topmost model view controller.
//    while (presentingController.presentedViewController) {
//        presentingController = presentingController.presentedViewController;
//    }
//
//    CNContactPickerViewController *contactPickerController = [CNContactPickerViewController new];
//
//    NSArray *displayedItems = @[CNContactPhoneNumbersKey];
//    contactPickerController.delegate = self;
//    contactPickerController.displayedPropertyKeys = displayedItems;
//
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [presentingController presentViewController:contactPickerController animated:YES completion:^{
//            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
//        }];
//    });
}

//#pragma mark - CNContactPickerViewControllerDelegate
//
//- (void)contactPicker:(CNContactPickerViewController *)contactPickerController didSelectContactProperty:(CNContactProperty *)contactProperty {
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
//    if (_callbacks.count == 0) {
//        return;
//    }
//    NSString *phoneNumber = @"";
//    if ([contactProperty.value isKindOfClass:[CNPhoneNumber class]]) {
//        CNPhoneNumber *contactPhone = contactProperty.value;
//        phoneNumber = contactPhone.stringValue;
//    }
//    RCTResponseSenderBlock callback = _callbacks[0];
//    NSString *giveName = contactProperty.contact.givenName.length > 0 ? contactProperty.contact.givenName : @"";
//    NSString *midName = contactProperty.contact.middleName > 0 ? contactProperty.contact.middleName : @"";
//    NSString *familyName = contactProperty.contact.familyName > 0 ? contactProperty.contact.familyName : @"";
//
//    phoneNumber = phoneNumber.length > 0 ? phoneNumber : @"";
//
//    NSMutableDictionary *_return = [NSMutableDictionary dictionary];
//    [_return setObject:giveName forKey:@"giveName"];
//    [_return setObject:midName forKey:@"midName"];
//    [_return setObject:familyName forKey:@"familyName"];
//    [_return setObject:phoneNumber forKey:@"phoneNumber"];
//    callback(@[_return]);
////    callback(@[phoneNumber]);
//    [_callbacks removeObjectAtIndex:0];
//}
//
//- (void)contactPickerDidCancel:(CNContactPickerViewController *)picker {
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
//}

RCT_REMAP_METHOD(getLanguageDevice,
            resolverLang:
            (RCTPromiseResolveBlock) resolverLang
            rejecterLang:
            (RCTPromiseRejectBlock) rejectLang) {

    NSString *language = [[NSLocale preferredLanguages] firstObject];
    NSDictionary *languageDic = [NSLocale componentsFromLocaleIdentifier:language];
    NSString *languageCode = [languageDic objectForKey:@"kCFLocaleLanguageCodeKey"];
    if (resolverLang) {
        resolverLang(languageCode);
    }
}

@end
