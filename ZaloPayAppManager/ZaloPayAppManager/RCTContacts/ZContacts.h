//
//  ZContacts.h
//  ZContacts
//
//  Created by Duy Huynh on 7/27/16.
//  Copyright © 2016 Facebook. All rights reserved.
//

#import "React/RCTViewManager.h"
#import "React/RCTBridgeModule.h"
#import "React/RCTInvalidating.h"

@interface ZContactsA : NSObject <RCTBridgeModule>

@end
