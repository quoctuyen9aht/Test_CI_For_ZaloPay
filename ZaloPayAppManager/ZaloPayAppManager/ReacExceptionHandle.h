//
//  ReacExceptionHandle.h
//  ZaloPayAppManager
//
//  Created by bonnpv on 7/20/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTExceptionsManager.h>

@interface ReacExceptionHandle : NSObject <RCTExceptionsManagerDelegate>
- (id)initWithAppId:(NSInteger)appId;
@end
