//
//  ReactModuleViewController.h
//  ZaloPay
//
//  Created by Nguyễn Hữu Hoà on 3/23/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ZaloPayUI/BaseViewController.h>

@class RCTBridge;

@interface ReactModuleViewController : BaseViewController
@property(nonatomic) BOOL enableSwipeBack;
@property(nonatomic) NSInteger appId;
@property(nonatomic, strong) NSDictionary *properties;
@property(nonatomic, strong) NSString *moduleName;
@property(nonatomic, strong) RCTBridge *bridge;

- (instancetype)initWithBridge:(RCTBridge *)bridge;

//- (instancetype)initWithBridge:(RCTBridge*)bridge
//                    properties:(NSDictionary *)properties;

- (instancetype)initWithAppId:(NSInteger)appId
                   properties:(NSDictionary *)properties;

- (instancetype)initWithProperties:(NSDictionary *)properties;

- (void)setupReactModule;

- (void)reloadReactModule;

@end
