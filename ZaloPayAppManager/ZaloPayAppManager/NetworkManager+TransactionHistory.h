//
//  NetworkManager+TransactionHistory.h
//  ZaloPayAppManager
//
//  Created by PhucPv on 5/7/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <ZaloPayNetwork/NetworkManager.h>
#import "TransactionLogsConst.h"

@interface NetworkManager (TransactionHistory)
/**
 order:	1 : lấy các giao dịch có time > timestamp theo thứ tụ time giảm dần
       -1 : lấy các giao dịch có time < timestamp theo thứ tụ time giảm dần"
 timeStamp: = 0 -> mới nhất
 */
- (RACSignal *)loadTransactionWithTimestamp:(NSTimeInterval)timestamp
                                      count:(int)count
                                      order:(TransOrder)order
                                 statusType:(StatusType)statusType;
@end
