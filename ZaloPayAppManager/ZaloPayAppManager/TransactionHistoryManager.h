//
//  TransactionHistoryManager.h
//  ZaloPayAppManager
//
//  Created by PhucPv on 5/7/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkManager+TransactionHistory.h"

@class RACSignal;

@interface TransactionHistoryManager : NSObject

- (RACSignal *)loadTransaction:(StatusType)type;

- (void)updateTransactionLogFromServer;

- (RACSignal *)loadTransactionHistoryWithId:(NSString *)transId;

- (NSArray *)getTransactionDetail:(NSString *)transId;

- (RACSignal *)requestTransaction:(uint64_t)transId time:(uint64_t)reqTime;

- (void)loadSuccessTransaction;

- (void)loadFailTransaction;

- (void)updateTransctionLogWithNotificationTimestamp:(uint64_t)timestamp;

- (RACSignal *)successfulTransactionWithTransTypes:(NSArray *)transTypes
                                      keyTimestamp:(NSTimeInterval)keyTimestamp
                                              sign:(int)sign
                                            offset:(int)offset
                                             count:(int)count;

- (RACSignal *)failedTransactionWithTransTypes:(NSArray *)transTypes
                                  keyTimestamp:(NSTimeInterval)keyTimestamp
                                          sign:(int)sign
                                        offset:(int)offset
                                         count:(int)count;

- (RACSignal *)removeTransactionWithId:(NSString *)transid;

- (RACSignal *)updateThankMessageWithTransId:(NSString *)transid
                               encodeMessage:(NSString *)encodeMessage;

- (RACSignal *)reloadOldestTransaction:(StatusType)statusType
                             timeStamp:(NSTimeInterval)timeStamp;
@end
