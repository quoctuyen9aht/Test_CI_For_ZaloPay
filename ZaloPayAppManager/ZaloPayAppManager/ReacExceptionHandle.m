//
//  ReacExceptionHandle.m
//  ZaloPayAppManager
//
//  Created by bonnpv on 7/20/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ReacExceptionHandle.h"
#import "PaymentModulLog.h"
#import "ReactFactory.h"
#import <ZaloPayUI/ZPAppFactory.h>
#import <ZaloPayUI/ZPDialogView.h>
#import <ZaloPayCommon/R.h>

#define kMaxRereshCount    5

@interface ReacExceptionHandle ()
@property(nonatomic) NSInteger appId;
@property(nonatomic) NSInteger numberRefresh;
@end

@implementation ReacExceptionHandle

- (id)initWithAppId:(NSInteger)appId {
    self = [super init];
    if (self) {
        self.appId = appId;
    }
    return self;
}

- (void)handleException {
    if (![NSThread isMainThread]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self handleException];
        });
        return;
    }
    [[ZPAppFactory sharedInstance] hideAllHUDsForView:nil];

    if (self.numberRefresh < kMaxRereshCount) {
        self.numberRefresh = self.numberRefresh + 1;
        [[ReactFactory sharedInstance] reloadAllViewWithAppId:self.appId];
        return;
    }

    [ZPDialogView showDialogWithType:DialogTypeNotification
                             message:[R string_ReactNative_ExceptionMessage]
                   cancelButtonTitle:[R string_ButtonLabel_Close]
                    otherButtonTitle:nil
                      completeHandle:nil];
}

#pragma mark - Exception Delegate

- (void)handleSoftJSExceptionWithMessage:(NSString *)message
                                   stack:(NSArray *)stack
                             exceptionId:(NSNumber *)exceptionId {
    DDLogError(@"SoftJSException: %@ stack: %@ exceptionId: %@", message, stack, exceptionId);
    [self handleException];
}

- (void)handleFatalJSExceptionWithMessage:(NSString *)message
                                    stack:(NSArray *)stack
                              exceptionId:(NSNumber *)exceptionId {
    DDLogError(@"FatalJSException: %@ stack: %@ exceptionId: %@", message, stack, exceptionId);
    [self handleException];
}


- (void)updateJSExceptionWithMessage:(NSString *)message
                               stack:(NSArray *)stack
                         exceptionId:(NSNumber *)exceptionId {
    DDLogError(@"JSException: %@ stack: %@ exceptionId: %@", message, stack, exceptionId);
    [self handleException];
}
@end
