//
//  ZaloPayTransactionLogs.h
//  ZaloPayAppManager
//
//  Created by Nguyễn Hữu Hoà on 5/4/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>


@class TransactionHistoryManager;

/**
 * Transaction Logs native module
 */
@interface ZaloPayTransactionLogs : NSObject <RCTBridgeModule>
@property (nonatomic, strong, nonnull) TransactionHistoryManager* transactionHistoryManager;
@end
