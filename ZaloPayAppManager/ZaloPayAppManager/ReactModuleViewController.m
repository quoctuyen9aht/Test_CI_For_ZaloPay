//
//  ReactModuleViewController.m
//  ZaloPay
//
//  Created by Nguyễn Hữu Hoà on 3/23/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ReactModuleViewController.h"

#import "ReactView.h"
#import "ReactFactory.h"
#import <ZaloPayUI/ZPDialogView.h>
#import <ZaloPayCommon/ZPConstants.h>
#import <ZaloPayCommon/R.h>
#import <ZaloPayCommon/UIColor+Extension.h>
#import "ReactNativeAppManager.h"
#import "PaymentModulLog.h"
#import "RCTBridge+Payment.h"

#import <ZaloPayUI/ZPAppFactory.h>

extern int environment;

@interface ReactModuleViewController ()
@property(nonatomic, strong) ReactView *reactView;
@property(nonatomic) NSInteger instanceId;
@end

@implementation ReactModuleViewController

- (instancetype)initWithBridge:(RCTBridge *)bridge {
    self = [super init];
    self.bridge = bridge;
    self.enableSwipeBack = true;
    return self;
}

- (instancetype)init {
    self = [super init];
    self.bridge = [[ReactFactory sharedInstance] bridge];
    return self;
}

- (instancetype)initWithAppId:(NSInteger)appId
                   properties:(NSDictionary *)properties {
    self = [self initWithBridge:[RCTBridge externalBridge:appId]];
    if (self) {
        self.appId = appId;
        self.properties = properties;
    }
    return self;
}

- (instancetype)initWithProperties:(NSDictionary *)properties {
    return [self initWithBridge:[RCTBridge internalBridge] properties:properties];
}

- (instancetype)initWithBridge:(RCTBridge *)bridge properties:(NSDictionary *)properties {
    self = [self initWithBridge:bridge];
    if (self) {
        self.properties = properties;
        self.appId = reactInternalAppId;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupReactModule];
}

- (BOOL)isAllowSwipeBack {
    return self.enableSwipeBack;
}

- (void)setupReactModule {
    if (self.bridge == nil) {
        DDLogError(@"appid = %d, module = %@", (int) self.appId, self.moduleName);
        __weak ReactModuleViewController *wself = self;
        [ZPDialogView showDialogWithType:DialogTypeNotification
                                 message:[R string_ReactNative_ExceptionMessage]
                       cancelButtonTitle:[R string_ButtonLabel_Close]
                        otherButtonTitle:nil
                          completeHandle:^(int buttonIndex, int cancelButtonIndex) {
                              [wself.navigationController popViewControllerAnimated:true];
                          }];
        return;
    }
    [self setupReactNativeView];
}

- (void)setupReactNativeView {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.instanceId <= 0) {
            self.instanceId = [[ReactFactory sharedInstance] registerModuleInstance:self];
        }
        if (self.moduleName) {
            self.reactView = [[ReactView alloc] initWithModuleName:self.moduleName
                                                        instanceId:self.instanceId
                                                            bridge:self.bridge
                                                        properties:self.properties];
        } else {
            self.reactView = [[ReactView alloc] init:self.instanceId bridge:self.bridge];
        }
        [self.view addSubview:self.reactView];
        self.reactView.frame = self.view.bounds;
        self.reactView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

        [self cleanBackground:self.reactView];
    });
}

- (void)cleanBackground:(UIView *)view {
    [view setBackgroundColor:[UIColor clearColor]];
    for (UIView *_view in [view subviews]) {
        if ([[_view subviews] count] > 0) {
            [self cleanBackground:_view];
        }
    }
}

- (void)reloadReactModule {
    RCTBridge *bridge = [RCTBridge externalBridge:self.appId];
    if (bridge == nil) {
        return;
    }
    self.bridge = bridge;
    if (self.reactView) {
        [self.reactView removeFromSuperview];
    }
    [self setupReactNativeView];
}

- (void)setProperties:(NSDictionary *)properties {
    NSMutableDictionary *pros = [NSMutableDictionary dictionary];
    if (properties.count > 0) {
        [pros addEntriesFromDictionary:properties];
    }
    [pros addEntriesFromDictionary:[self defaultProperty]];
    _properties = pros;
}

- (NSDictionary *)defaultProperty {
    return @{@"environment": @(environment)};
}

- (NSString *)screenName {
    if ([self.moduleName isEqualToString:@"PaymentMain"]) {
        NSInteger appId = [[ReactNativeAppManager sharedInstance] runingAppId];
        return [NSString stringWithFormat:@"PaymentApp_%@", @(appId)];
    } else if ([self.moduleName length] == 0) {
        return @"React_Unknown";
    } else {
        return [@"React_" stringByAppendingString:self.moduleName];
    }
}

- (void)viewWillSwipeBack {
    [[ReactFactory sharedInstance] removeModuleInstnce:self.instanceId];
}

- (void)dealloc {
    DDLogInfo(@"dealloc ReactModuleViewController");
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    UINavigationController *navigationController = [[ZPAppFactory sharedInstance] rootNavigation];
    if ([navigationController isKindOfClass:[UINavigationController class]]
            && ![navigationController.topViewController isKindOfClass:[ReactModuleViewController class]]) {
        [navigationController setNavigationBarHidden:NO animated:animated];
        navigationController.navigationBar.backgroundColor = [UIColor zaloBaseColor];
        navigationController.navigationBar.barTintColor = [UIColor zaloBaseColor];
    }
}

@end
