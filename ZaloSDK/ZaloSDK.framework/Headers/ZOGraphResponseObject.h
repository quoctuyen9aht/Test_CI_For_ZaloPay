//
//  ZAGraphResponseObject.h
//  ZaloSDK
//
//  Created by dungttm on 2/13/14.
//  Copyright (c) 2014 VNG Corporation. All rights reserved.
//


#import "ZOResponseObject.h"

///////////////////////////////////////////
/// ZOGraphResponseObject
///////////////////////////////////////////
@interface ZOGraphResponseObject : ZOResponseObject
@end

///////////////////////////////////////////
/// ZOProfileResponseObject
///////////////////////////////////////////
@interface ZOProfileResponseObject : ZOGraphResponseObject
@property (nonatomic, retain) NSDictionary* data;
@end

///////////////////////////////////////////
/// ZOFriendsListResponseObject
///////////////////////////////////////////
@interface ZOFriendsListResponseObject : ZOGraphResponseObject
@property (nonatomic, retain) NSArray* data;
@end


@interface ZOShareResponseObject : ZOResponseObject

@property (assign, nonatomic, getter=isSuccess) BOOL success;
@property (strong, nonatomic) NSString * message;
@property (strong, nonatomic) id result_data;
@property (assign, nonatomic) int send_action; //0 -> cancel, 1 -> send

+ (instancetype)responseWithError:(int)code
                          success:(BOOL)success
                          message:(NSString *)message
                       resultData:(id)data;

@end