//
//  ZPContactSwift.swift
//  ZaloPayProfile
//
//  Created by Huu Hoa Nguyen on 6/3/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import Foundation
import ZaloPayCommonSwift


@objc public enum UserGender : Int {
    case male = 1
    case female = 2
}

enum ParseUserSource {
    case fromServerDictionary
    case fromReactDictionary
}

//@objc public enum ContactMode : Int {
//    case ZPC_All = 1
//    case ZPC_PhoneBook = 2
//}

/*
 Trạng thái user tương ứng số phoneinput
 1: Thành công
 -124: User đang bị lock
 -2005: Số phone không đúng
 -2007: User không tồn tại
 */
@objc public enum UserStatus : Int {
    case Available = 1
    case Lock = -124
    case ReceiverLock = -2013
    case InvalidPhone = -2005
    case NotExist = -2007
}

struct ConstantsZPContactSwift {
    public static let kDisplayName = "displayname"
    public static let kAvatar = "avatar"
    public static let kUserId = "userid"
    public static let kUserGender = "userGender"
    public static let kUsingZaloPay = "usingApp"
    public static let kAscciDisplayName = "ascciDisplayName"
    public static let kZaloPayId = "zalopayid"
    public static let kStatus = "status"
    
    public static let kPhoneNumber = "phonenumber"
    public static let kZaloId = "zaloid"
    public static let kZaloPayName = "zalopayname"
    
    public static let kZaloName = "zaloname"
    public static let kContactDisplayName = "contactdisplayname"
}

@objcMembers
public class ZPContactSwift : NSObject {
    public var zaloPayId: String = ""
    public var zaloId: String = ""
    public var phoneNumber: String = ""
    public var zaloPayName: String = ""
    public var displayNameZLF: String = ""
    public var displayNameUCB: String = ""
    public var avatar: String = ""

    public var status: UserStatus = .Available
    
    public var displayName:String = "" {
        didSet {
            self.asciiDisplayName = (displayName.ascci() ?? "").uppercased()
            if let first = self.asciiDisplayName.first {
                self.firstCharacterInDisplayName = String(first)
            } else {
                self.firstCharacterInDisplayName = ""
            }
        }
    }
    
    public var firstCharacterInDisplayName: String = ""
    public var asciiDisplayName: String = ""

    public var gender: UserGender = .male
    public var birthDay: Date =  Date()
    
    public var usedZaloPay: Bool = false
    public var isFavorite: Bool = false
    public var isRedPacket: Bool = false

    public var zpContactID: String = ""
    public var firstName: String = ""
    public var lastName: String = ""
    
    public var isSavePhoneNumber: Bool = false
    
    // methods
    public func compare(byDisplayValue other: ZPContactSwift) -> ComparisonResult {
        return self.asciiDisplayName.compare(other.asciiDisplayName)
    }

    public class func user(fromServerDictionary dictionary: [String : Any]) -> ZPContactSwift? {
        /*
         Cần gán displayNameUCB = displayNameZLF
         Case: vào chuyển tiền -> yêu thích bạn bè zalo không có trong danh bạ -> khi vào nạp tiền điện thoại không được chọn user này
         Khi Nạp Tiền Điện Thoại -> search ra thông tin -> đây là bạn zalo không có trong danh bạ.
         Search số điện thoại -> cho user đi tiếp.
         */
        let zalopayId = dictionary.string(forKey: ConstantsZPContactSwift.kUserId, defaultValue: "")
        if (zalopayId.isEmpty) {
            return nil
        }
        return self.parseUser(from: .fromServerDictionary, dictionary: dictionary, zalopayId)
    }

    public class func createContact(fromReactDictionary dictionary: [String : Any]) -> ZPContactSwift {
        let zalopayId = dictionary.string(forKey: ConstantsZPContactSwift.kZaloPayId, defaultValue: "")
        return self.parseUser(from: .fromReactDictionary, dictionary: dictionary, zalopayId)
    }

    public class func fromPhone(phone: String, displayName: String) -> ZPContactSwift {
        let item = ZPContactSwift()
        item.phoneNumber = phone
        item.zpContactID = String("zpcontactid_\(phone)")
        item.displayNameUCB = displayName
        item.displayNameZLF = ""
        item.displayName = displayName
        item.avatar = ""
        item.zaloId = ""
        item.zaloPayName = ""
        item.zaloPayId = ""
        item.status = .Available
        item.isSavePhoneNumber = false
        return item
    }

    public func createZaloPayContactId() -> String {
        let phone = self.phoneNumber.isEmpty ? "" : self.phoneNumber
        let zaloId = self.zaloId.isEmpty ? "" : self.zaloId
        let zaloPayId = self.zaloPayId.isEmpty ? "" : self.zaloPayId
        return String("\(phone)\(zaloId)\(zaloPayId)")
    }

    public override init() {
    }
    
    // parse json from server/ or react when search user out ZPC
    class func parseUser(from source: ParseUserSource, dictionary: [String : Any], _ zalopayId: String) -> ZPContactSwift {
        let zpContact = ZPContactSwift()
        zpContact.avatar = dictionary.string(forKey: ConstantsZPContactSwift.kAvatar, defaultValue: "")
        zpContact.phoneNumber = dictionary.string(forKey: ConstantsZPContactSwift.kPhoneNumber, defaultValue: "").normalizePhoneNumber()
        zpContact.displayName = dictionary.string(forKey: ConstantsZPContactSwift.kDisplayName, defaultValue: "")
        
        if source == .fromServerDictionary {
            zpContact.displayNameZLF = dictionary.string(forKey: ConstantsZPContactSwift.kDisplayName, defaultValue: "")
        } else {
            zpContact.displayNameZLF = dictionary.string(forKey: ConstantsZPContactSwift.kZaloName, defaultValue: "")
        }
        
        zpContact.displayNameUCB = zpContact.displayNameZLF
        zpContact.zaloId = dictionary.string(forKey: ConstantsZPContactSwift.kZaloId, defaultValue: "")
        zpContact.zaloPayId = zalopayId
        if let gender = UserGender(rawValue: UserGender.RawValue(dictionary.int(forKey: ConstantsZPContactSwift.kUserGender))) {
            zpContact.gender = gender
        }
        zpContact.usedZaloPay = !zpContact.zaloPayId.isEmpty
        zpContact.zaloPayName = dictionary.string(forKey: ConstantsZPContactSwift.kZaloPayName, defaultValue: "")
        
        let status = dictionary.int(forKey:ConstantsZPContactSwift.kStatus, defaultValue:Int(UserStatus.Available.rawValue))
        if let status = UserStatus(rawValue: UserStatus.RawValue(status)) {
            zpContact.status = status
        }
        zpContact.isSavePhoneNumber = true
        zpContact.zpContactID = zpContact.createZaloPayContactId()
        zpContact.asciiDisplayName = (zpContact.displayName.ascci() ?? "").uppercased()
        if let first = zpContact.asciiDisplayName.first {
            zpContact.firstCharacterInDisplayName = String(first)
        } else {
            zpContact.firstCharacterInDisplayName = ""
        }
        return zpContact
    }

    public override func isEqual(_ object: Any?) -> Bool {
        guard let other = object as? ZPContactSwift else {
            return false
        }
        return zpContactID == other.zpContactID
    }
}
