//
//  ZPZaloFriendTable.m
//  ZaloPay
//
//  Created by bonnpv on 6/20/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <fmdb/FMDatabase.h>
#import <fmdb/FMDatabaseQueue.h>
#import <ZaloPayCommon/NSCollection+JSON.h>
#import <ZaloPayDatabase/FMResultSet+ZaloPay.h>
#import <ZaloPayCommon/NSString+Decimal.h>
#import <ZaloPayCommon/R.h>
#import <ReactiveObjC/ReactiveObjC.h>
#import <ZaloPayProfile/ZaloPayProfile-Swift.h>
#import "ZPZaloFriendTable.h"


#define kUserContact              @"UserContactBook"
#define kPhone                    @"phoneNumber"
#define kContactName              @"displayName"
#define kFirstName                @"firstName"
#define kLastName                 @"lastName"
#define kMiddleName               @"middleName"

#define KZaloPayContactList       @"ZPC"
#define kZalopayid                @"zalopayId"
#define kUserid                   @"zaloId"
#define kZalopayName              @"zalopayName"
#define kDisplayNameZFL           @"displayNameZFL"
#define kDisplayNameUCB           @"displayNameUCB"
#define kAvatar                   @"avatar"
#define kFavorite                 @"favorite"
#define kDateModified             @"dateModified"
#define kStatus                   @"status"

#define kZaloFriend               @"ZaloFriendList"
#define kDisplayName              @"displayName"
#define kUsingZalopay             @"usingzalopay"

#define kUnSavedPhoneContact      @"UnSavedPhoneContact"

#define kAppSource                @"keyAppSource"
#define kValueKeyboard            @"valueKeyboard"
#define kZPCModeKeyboard          @"ZPCModeKeyboard"

@implementation ZPZaloFriendTable

+ (NSArray<NSString*>*) creatingTableQuery {
    NSString* tableZaloFriend = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@(%@ text , %@ text unique, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text, PRIMARY KEY(%@ , %@));", kZaloFriend, kZalopayid, kUserid, kAvatar, kUsingZalopay, kZalopayName, kDisplayName, kPhone, kStatus, kZalopayid, kUserid];
    NSString* tableUserContact = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@( %@ text PRIMARY KEY, %@ text, %@ text, %@ text, %@ text);", kUserContact, kPhone, kDisplayName, kFirstName, kLastName, kMiddleName];
    NSString* tableZPC = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@(%@ text unique, %@ text unique, %@ text unique, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text, PRIMARY KEY(%@ ,%@, %@));", KZaloPayContactList, kZalopayid, kUserid, kPhone, kZalopayName, kDisplayNameZFL, kDisplayNameUCB, kAvatar, kFavorite, kDateModified, kFirstName, kLastName, kMiddleName, kStatus, kZalopayid, kUserid, kPhone];
    NSString* tableUnSavedPhoneContact = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@(%@ text unique  PRIMARY KEY, %@ text unique, %@ text unique, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text);", kUnSavedPhoneContact, kZalopayid, kUserid, kPhone, kZalopayName, kDisplayName, kAvatar, kFavorite, kDateModified, kStatus];
    NSString* tableZPCModeKeyboard = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@(%@ text PRIMARY KEY NOT NULL, %@ text);", kZPCModeKeyboard, kAppSource, kValueKeyboard];
    
    return @[tableZaloFriend, tableUserContact, tableZPC, tableUnSavedPhoneContact, tableZPCModeKeyboard];
}

+ (NSArray<NSString*>*) tableNamesCanBeDropped {
    return @[kZaloFriend, kUserContact, kUnSavedPhoneContact, kZPCModeKeyboard];
}
- (void)saveZaloFriends:(NSArray *)friends {
    [self.db.dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        for (ZaloUserSwift *user in friends) {
            int usingZaloPay = user.usedZaloPay == true ? 1 : 0;
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            [params setObjectCheckNil:user.userId forKey:kUserid];
            [params setObjectCheckNil:user.displayName forKey:kDisplayName];
            [params setObjectCheckNil:user.avatar forKey:kAvatar];
            [params setValue:@(usingZaloPay) forKey:kUsingZalopay];
            NSString *query = [NSString stringWithFormat:@"UPDATE OR IGNORE %@ set %@ = ?, %@ = ?, %@ = ? where %@ = ?", kZaloFriend, kDisplayName, kAvatar, kUsingZalopay, kUserid];
            [db executeUpdate:query, user.displayName, user.avatar, @(usingZaloPay), user.userId];

            query = [self.db buildQuery:@"INSERT OR IGNORE INTO" table:kZaloFriend params:params];
            [db executeUpdate:query withParameterDictionary:params];
        }
    }];
}

- (NSArray *)zaloFriendList {
    NSMutableArray *_return = [NSMutableArray array];
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@", kZaloFriend];
        FMResultSet *rs = [db executeQuery:query];
        while ([rs next]) {
            NSDictionary *user = [self zaloUserFromFMResultSet:rs];
            [_return addObjectNotNil:user];
        }
        [rs close];
    }];
    return _return;
}

- (int)countZaloFriendList {
    __block int _return = 0;
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = [NSString stringWithFormat:@"SELECT COUNT(%@) AS ZALOFRIENDSCOUNT  FROM %@", kUserid, kZaloFriend];
        FMResultSet *rs = [db executeQuery:query];
        while ([rs next]) {
            _return = (int)[rs longLongIntForColumn:@"ZALOFRIENDSCOUNT"];
            break;
        }
        [rs close];
    }];
    return _return;
}

- (NSArray *)zaloPayContactList:(ContactMode)mode {

    return mode == ContactMode_ZPC_All ? [self getAllUserFromZaloPayContact] : [self getAllUserFromContactBook];
}

- (NSDictionary *)getUserFromZaloPayContact:(NSString *)phone {
    NSMutableArray *_return = [NSMutableArray array];
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE %@='%@'", KZaloPayContactList, kPhone, phone];
        FMResultSet *rs = [db executeQuery:query];
        while ([rs next]) {
            NSDictionary *user = [self zaloPayContactFromFMResultSet:rs];
            [_return addObjectNotNil:user];
        }
        [rs close];
    }];

    return _return.count > 0 ? _return[0] : [[NSDictionary alloc] init];
}

- (NSArray *)getListUserFromZaloPayContact:(NSArray *)phones {
    if ([phones count] == 0) {return @[];}
    NSMutableArray *_return = [NSMutableArray array];
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE %@ in (%@)", KZaloPayContactList, kPhone, [NSString stringWithFormat:@"'%@'", [phones componentsJoinedByString:@"','"]]];
        FMResultSet *rs = [db executeQuery:query];
        while ([rs next]) {
            NSDictionary *user = [self zaloPayContactFromFMResultSet:rs];
            [_return addObjectNotNil:user];
        }
        [rs close];
    }];
    return _return;
}

- (NSArray *)getAllUserFromZaloPayContact {
    NSMutableArray *_return = [NSMutableArray array];
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE %@ IS NOT NULL OR %@ IS NOT NULL", KZaloPayContactList, kZalopayid, kPhone];
        FMResultSet *rs = [db executeQuery:query];
        while ([rs next]) {
            NSDictionary *user = [self zaloPayContactFromFMResultSet:rs];
            [_return addObjectNotNil:user];
        }
        [rs close];
    }];
    return _return;
}

- (NSArray *)getAllUserFromContactBook {
    NSMutableArray *_return = [NSMutableArray array];
    [self.db.dbQueue inDatabase:^(FMDatabase *_Nonnull db) {
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@", kUserContact];
        FMResultSet *rs = [db executeQuery:query];
        while ([rs next]) {
            NSDictionary *user = [self userContactFromFMResultSet:rs];
            [_return addObjectNotNil:user];

        }
    }];
    return _return;
}

- (NSDictionary *)getUnsavePhoneContactWith:(NSString *)phone {
    __block NSDictionary *user = nil;
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ where %@ = ? ", kUnSavedPhoneContact, kPhone];
        FMResultSet *rs = [db executeQuery:query, phone];
        if ([rs next]) {
            user = [self unsavePhoneContactFromFMResultSet:rs];
        }
        [rs close];
    }];
    return user;
}

- (NSDictionary *)unsavePhoneContactFromFMResultSet:(FMResultSet *)rs {
    NSString *userId = [rs stringForColumn:kUserid defaultValue:@""];
    NSString *displayName = [rs stringForColumn:kDisplayName defaultValue:@""];
    NSString *avatar = [rs stringForColumn:kAvatar defaultValue:@""];
    NSString *zalopayId = [rs stringForColumn:kZalopayid defaultValue:@""];
    NSString *zalopayName = [rs stringForColumn:kZalopayName defaultValue:@""];
    NSString *phoneNumber = [rs stringForColumn:kPhone defaultValue:@""];
    int status = [rs intForColumn:kStatus defaultValue:1];

    return @{@"zaloId": userId,
            @"displayName": displayName,
            @"avatar": avatar,
            @"phoneNumber": phoneNumber,
            @"zalopayId": zalopayId,
            @"zalopayName": zalopayName,
            @"status": @(status)
    };

}


- (NSArray *)zaloPayContactListFavorite:(ContactMode)kindShowContactList {
    NSMutableArray *_return = [NSMutableArray array];
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = [NSString stringWithFormat:@"select * from %@ where %@ = 1 ORDER BY %@ DESC", KZaloPayContactList, kFavorite, kDateModified];
        if (kindShowContactList == ContactMode_ZPC_PhoneBook) {
            query = [NSString stringWithFormat:@"select * from %@ where %@ = 1 and %@ IS NOT NULL ORDER BY %@ DESC", KZaloPayContactList, kFavorite, kPhone, kDateModified];
        }
        FMResultSet *rs = [db executeQuery:query];
        while ([rs next]) {
            NSDictionary *user = [self zaloPayContactFromFMResultSet:rs];
            [_return addObjectNotNil:user];
        }
        [rs close];
    }];
    return _return;
}

- (NSArray *)lixiZaloFriendInfoWithIds:(NSArray *)ids {
    NSMutableArray *_return = [NSMutableArray array];
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ Where %@ in (%@)", kZaloFriend, kUserid, [ids componentsJoinedByString:@", "]];
        FMResultSet *rs = [db executeQuery:query];

        while ([rs next]) {
            NSString *userId = [rs stringForColumn:kUserid];
            NSString *displayName = [rs stringForColumn:kDisplayName];
            NSString *avatar = [rs stringForColumn:kAvatar];
            NSString *zaloPayId = [rs stringForColumn:kZalopayid];

            NSMutableDictionary *oneDic = [NSMutableDictionary dictionary];
            [oneDic setObjectCheckNil:zaloPayId forKey:@"zaloPayID"];
            [oneDic setObjectCheckNil:userId forKey:@"zaloID"];
            [oneDic setObjectCheckNil:displayName forKey:@"zaloName"];
            [oneDic setObjectCheckNil:avatar forKey:@"avatar"];
            [_return addObject:oneDic];
        }
        [rs close];
    }];
    return _return;
}

- (void)mergeAllContactDataToZPC {

    [self.db.dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        [self updateZFLtoZPC:db];
        [self updateUBCtoZPC:db];
        [self saveMyPhoneNumber:db];
        [self removeZPCNotInUBC:db];
    }];
}

- (void)updateZFLtoZPC:(FMDatabase *)db {

    // case update thông tin info từ zalo
    NSString *queryUpdateZFLtoZPC = @"UPDATE OR IGNORE ZPC SET \
    displayNameZFL = (SELECT ZaloFriendList.displayName  FROM ZaloFriendList WHERE ZPC.zaloId = ZaloFriendList.zaloId),\
    avatar = (SELECT ZaloFriendList.avatar  FROM ZaloFriendList WHERE ZPC.zaloId = ZaloFriendList.zaloId), \
    status = (SELECT ZaloFriendList.status  FROM ZaloFriendList WHERE ZPC.zaloId = ZaloFriendList.zaloId )\
    WHERE ZPC.zaloId is not null AND ZPC.zaloId in (SELECT zaloId FROM ZaloFriendList WHERE ZaloFriendList.zaloId is not null)";
    [db executeUpdate:queryUpdateZFLtoZPC];

    // case bạn bè zalo lúc trước chưa sử dụng zalopay.
    NSString *queryUpdate = @"UPDATE OR IGNORE ZPC SET \
    zalopayId = (SELECT ZaloFriendList.zalopayId FROM ZaloFriendList WHERE ZPC.zaloId = ZaloFriendList.zaloId), \
    zalopayName = (SELECT ZaloFriendList.zalopayName  FROM ZaloFriendList WHERE ZPC.zaloId = ZaloFriendList.zaloId), \
    phoneNumber = (SELECT ZaloFriendList.phoneNumber FROM ZaloFriendList WHERE ZPC.zaloId = ZaloFriendList.zaloId), \
    status = (SELECT ZaloFriendList.status  FROM ZaloFriendList WHERE ZPC.zaloId = ZaloFriendList.zaloId )\
    WHERE ZPC.zaloId is not null AND ZPC.zalopayId is null AND ZPC.zaloId in (SELECT zaloId FROM ZaloFriendList WHERE ZaloFriendList.zalopayId is not null AND ZaloFriendList.zaloId is not null)";
    [db executeUpdate:queryUpdate];

    NSString *queryZFLtoZPC = @"INSERT OR IGNORE INTO ZPC (zalopayId, zaloId, avatar, displayNameZFL, zalopayName, phoneNumber, status) SELECT zalopayId, zaloId, avatar, displayName, zalopayName, phoneNumber, status FROM ZaloFriendList";
    [db executeUpdate:queryZFLtoZPC];
}


- (void)updateUBCtoZPC:(FMDatabase *)db {
    NSString *queryUpateUBCtoZPC = @"UPDATE OR IGNORE ZPC SET \
    displayNameUCB = (SELECT UserContactBook.displayName FROM UserContactBook WHERE ZPC.phoneNumber = UserContactBook.phoneNumber),\
    firstName = (SELECT UserContactBook.firstName FROM UserContactBook WHERE ZPC.phoneNumber = UserContactBook.phoneNumber),\
    lastName = (SELECT UserContactBook.lastName FROM UserContactBook WHERE ZPC.phoneNumber = UserContactBook.phoneNumber),\
    middleName = (SELECT UserContactBook.middleName FROM UserContactBook WHERE ZPC.phoneNumber = UserContactBook.phoneNumber) WHERE ZPC.phoneNumber is not null";

    [db executeUpdate:queryUpateUBCtoZPC];

    NSString *queryUBCtoZPC = @"INSERT OR IGNORE INTO ZPC (phoneNumber, displayNameUCB, firstName, lastName, middleName) SELECT phoneNumber, displayName, firstName, lastName, middleName FROM UserContactBook";
    [db executeUpdate:queryUBCtoZPC];
}

- (void)saveMyPhoneNumber:(FMDatabase *)db {
    NSString *phone = [ZPProfileManager shareInstance].userLoginData.phoneNumber;
    if (phone) {
        ZaloUserSwift *user = [ZPProfileManager shareInstance].currentZaloUser;
        ZPUserLoginDataSwift *loginData = [ZPProfileManager shareInstance].userLoginData;
        
        NSMutableDictionary *param = [NSMutableDictionary dictionary];
        [param setObjectCheckNil:user.displayName forKey:kDisplayNameZFL];
        [param setObjectCheckNil:[R string_ZPC_my_phone_number] forKey:kDisplayNameUCB];
        [param setObjectCheckNil:user.avatar forKey:kAvatar];

        NSDictionary *queryData = [self.db buildUpdateQueryTable:KZaloPayContactList params:param whereKey:kZalopayid isValue:loginData.paymentUserId];
        NSString *query = [queryData stringForKey:kUpdateQuery];
        NSArray *values = [queryData arrayForKey:kUpdateValues];
        [db executeUpdate:query withArgumentsInArray:values];

        [param setObjectCheckNil:phone forKey:kPhone];
        [param setObjectCheckNil:user.userId forKey:kUserid];
        [param setObjectCheckNil:loginData.paymentUserId forKey:kZalopayid];
        NSString *insert = [self.db buildInsertOrIgnoreSQLQuery:KZaloPayContactList params:param];
        [db executeUpdate:insert withParameterDictionary:param];
    }
}

- (NSDictionary *)zaloUserFromFMResultSet:(FMResultSet *)rs {
    NSString *userId = [rs stringForColumn:kUserid defaultValue:@""];
    NSString *displayName = [rs stringForColumn:kDisplayName defaultValue:@""];
    NSString *avatar = [rs stringForColumn:kAvatar defaultValue:@""];
    NSString *phoneNumber = [rs stringForColumn:kPhone defaultValue:@""];
    NSString *zalopayId = [rs stringForColumn:kZalopayid defaultValue:@""];
    NSString *zalopayName = [rs stringForColumn:kZalopayName defaultValue:@""];

    return @{@"zaloId": userId,
            @"displayName": displayName,
            @"avatar": avatar,
            @"phoneNumber": phoneNumber,
            @"zalopayId": zalopayId,
            @"zalopayName": zalopayName,
    };
}

- (NSDictionary *)zaloPayContactFromFMResultSet:(FMResultSet *)rs {

    NSString *userId = [rs stringForColumn:kUserid defaultValue:@""];
    NSString *displayNameZLF = [rs stringForColumn:kDisplayNameZFL defaultValue:@""];
    NSString *displayNameUCB = [rs stringForColumn:kDisplayNameUCB defaultValue:@""];
    NSString *avatar = [rs stringForColumn:kAvatar defaultValue:@""];
    NSString *phoneNumber = [rs stringForColumn:kPhone defaultValue:@""];
    NSString *zalopayId = [rs stringForColumn:kZalopayid defaultValue:@""];
    NSString *zalopayName = [rs stringForColumn:kZalopayName defaultValue:@""];
    NSString *firstName = [rs stringForColumn:kFirstName defaultValue:@""];
    NSString *lastName = [rs stringForColumn:kLastName defaultValue:@""];
    int status = [rs intForColumn:kStatus defaultValue:UserStatusAvailable];

    return @{@"zaloId": userId,
            @"displayNameZLF": displayNameZLF,
            @"displayNameUCB": displayNameUCB,
            @"avatar": avatar,
            @"phoneNumber": phoneNumber,
            @"zalopayId": zalopayId,
            @"zalopayName": zalopayName,
            @"firstName": firstName,
            @"lastName": lastName,
            @"status": @(status)
    };
}

- (NSDictionary *)userContactFromFMResultSet:(FMResultSet *)rs {
    NSString *phoneNumber = [rs stringForColumn:kPhone defaultValue:@""];
    NSString *displayName = [rs stringForColumn:kDisplayName defaultValue:@""];
    NSString *firstName = [rs stringForColumn:kFirstName defaultValue:@""];
    NSString *lastName = [rs stringForColumn:kLastName defaultValue:@""];
    UserStatus status = UserStatusAvailable;

    return @{@"zaloId": @"",
            @"displayNameZLF": @"",
            @"displayNameUCB": displayName,
            @"avatar": @"",
            @"phoneNumber": phoneNumber,
            @"zalopayId": @"",
            @"zalopayName": @"",
            @"firstName": firstName,
            @"lastName": lastName,
            @"status": @(status)
    };
}

- (NSDictionary *)getZaloUserWithId:(NSString *)zaloId {
    __block NSDictionary *_return = nil;
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE %@=?", kZaloFriend, kUserid];
        FMResultSet *rs = [db executeQuery:query, zaloId];
        while ([rs next]) {
            _return = [self zaloUserFromFMResultSet:rs];
            break;
        }
        [rs close];
    }];
    return _return;
}

- (NSInteger)countTotalContact {
    __block NSInteger count = 0;
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = [NSString stringWithFormat:@"SELECT COUNT(%@) AS CONTACTCOUNT FROM %@ ", kZalopayid, KZaloPayContactList];
        FMResultSet *rs = [db executeQuery:query];
        while ([rs next]) {
            count = (int)[rs longLongIntForColumn:@"CONTACTCOUNT"];
            break;
        }
        [rs close];
    }];
    return count;
}

- (NSMutableArray *)getAllFriendIdShouldRequestZaloPayInfo:(BOOL) forSycn {
    NSMutableArray *_return = [NSMutableArray array];
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString * query;
        if (forSycn) {
            query = [NSString stringWithFormat:@"SELECT %@ FROM %@ WHERE %@ = ? ", kUserid, kZaloFriend, kUsingZalopay];
        } else {
            query = [NSString stringWithFormat:@"SELECT %@ FROM %@ WHERE %@ = ? EXCEPT SELECT %@ FROM %@ WHERE %@ !='' AND %@ IS NOT NULL", kUserid, kZaloFriend, kUsingZalopay, kUserid, kZaloFriend, kPhone, kPhone];
        }        
        FMResultSet *rs = [db executeQuery:query, @(1)];
        while ([rs next]) {
            NSString *userId = [rs stringForColumn:kUserid];
            [_return addObjectNotNil:userId];
        }
        [rs close];
    }];
    return _return;
}

- (NSMutableArray *)getAllPhonesShouldRequestInfo {
    NSMutableArray *_return = [NSMutableArray array];
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = [NSString stringWithFormat:@"SELECT %@ FROM %@ WHERE %@ !='' AND %@ IS NOT NULL EXCEPT SELECT %@ FROM %@ WHERE %@ !='' AND %@ IS NOT NULL", kPhone, KZaloPayContactList, kPhone, kPhone, kPhone, KZaloPayContactList, kZalopayid, kZalopayid];
        FMResultSet *rs = [db executeQuery:query];
        while ([rs next]) {
            NSString *phone = [rs stringForColumn:kPhone];
            [_return addObjectNotNil:phone];
        }
        [rs close];
    }];
    return _return;
}

- (NSMutableArray *)getAllPhonesInZPCShouldRequestInfo {
    NSMutableArray *_return = [NSMutableArray array];
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = [NSString stringWithFormat:@"SELECT %@ FROM %@ WHERE %@ IS NOT NULL", kPhone, KZaloPayContactList, kPhone];
        FMResultSet *rs = [db executeQuery:query];
        while ([rs next]) {
            NSString *phone = [rs stringForColumn:kPhone];
            [_return addObjectNotNil:phone];
        }
        [rs close];
    }];
    return _return;
}

#pragma mark - Phone Contact Table

- (void)saveListPhone:(NSMutableArray *)contacts {
    [self.db.dbQueue inTransaction:^(FMDatabase *_Nonnull db, BOOL *_Nonnull rollback) {
        [db executeUpdate:[NSString stringWithFormat:@"delete FROM %@", kUserContact]];
        for (NSDictionary *data in contacts) {
            //note: data đã được format đúng dữ liệu của bảng.
            NSString *replace = [self.db buildReplaceSQLQuery:kUserContact params:data];
            [db executeUpdate:replace withParameterDictionary:data];
        }
    }];
}

- (void)saveUnSavePhoneContact:(ZPContactSwift *)contact {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:contact.phoneNumber forKey:kPhone];
    [params setObjectCheckNil:contact.displayName forKey:kDisplayName];
    [params setObjectCheckNil:contact.zaloId forKey:kUserid];
    [params setObjectCheckNil:contact.zaloPayId forKey:kZalopayid];
    [params setObjectCheckNil:contact.zaloPayName forKey:kZalopayName];
    [params setObjectCheckNil:contact.avatar forKey:kAvatar];
    [params setObject:@(contact.status) forKey:kStatus];
    [self.db replaceToTable:kUnSavedPhoneContact params:params];
}

- (void)updateFriendProfileToZPC:(ZPContactSwift *)contact phone:(NSString *)phone {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:contact.zaloId forKey:kUserid];
    [params setObjectCheckNil:contact.zaloPayId forKey:kZalopayid];
//    [params setObjectCheckNil:contact.phoneNumber forKey:kPhone];
    [params setObjectCheckNil:contact.displayName forKey:kDisplayNameZFL];
    [params setObjectCheckNil:contact.zaloPayName forKey:kZalopayName];
    [params setObjectCheckNil:contact.avatar forKey:kAvatar];
    [params setObject:@(contact.status) forKey:kStatus];
    [self.db updateToTable:KZaloPayContactList params:params whereKey:kPhone isValue:phone];
}

#pragma mark - ZaloPayInfo Table

- (void)createTableZaloPayInfo:(FMDatabase *)db {
    NSString *query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@(%@ text unique, %@ text unique, %@ text unique, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text, PRIMARY KEY(%@ ,%@, %@));", KZaloPayContactList, kZalopayid, kUserid, kPhone, kZalopayName, kDisplayNameZFL, kDisplayNameUCB, kAvatar, kFavorite, kDateModified, kFirstName, kLastName, kMiddleName, kStatus, kZalopayid, kUserid, kPhone];
    [db executeUpdate:query];
}

- (void)createTableUnSavePhoneContactInfo:(FMDatabase *)db {
    NSString *query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@(%@ text unique  PRIMARY KEY, %@ text unique, %@ text unique, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text);", kUnSavedPhoneContact, kZalopayid, kUserid, kPhone, kZalopayName, kDisplayName, kAvatar, kFavorite, kDateModified, kStatus];
    [db executeUpdate:query];
}

- (void)createTableSaveModeKeyboard:(FMDatabase *)db {
    NSString *query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@(%@ text PRIMARY KEY NOT NULL, %@ text);", kZPCModeKeyboard, kAppSource, kValueKeyboard];
    [db executeUpdate:query];
}

- (void)setStateKeyboardWithValue:(NSString *)valueKeyboard
                           forKey:(NSString *)keyAppSource {
    if (!valueKeyboard) {
        [self removeStateKeyboardForKey:keyAppSource];
        return;
    }
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = @"INSERT OR REPLACE INTO ZPCModeKeyboard (keyAppSource, valueKeyboard) VALUES (?, ?)";
        [db executeUpdate:query, keyAppSource, valueKeyboard];
    }];
}

- (NSString *)getStateKeyboardForKey:(NSString *)keyAppSource {
    __block NSString *version;
    version = @"";
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSError *error = nil;
        FMResultSet *rs = [db executeQuery:@"SELECT valueKeyboard FROM ZPCModeKeyboard WHERE keyAppSource=?" values:@[keyAppSource] error:&error];
        if (error) {
            return;
        }
        if ([rs next]) {
            version = [rs stringForColumn:@"valueKeyboard"];
        }
        [rs close];
    }];

    return version;
}

- (void)removeStateKeyboardForKey:(NSString *)keyAppSource {
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = @"DELETE FROM ZPCModeKeyboard WHERE keyAppSource=?";
        [db executeUpdate:query, keyAppSource];
    }];
}

- (void)saveZalopayInfoWithUserId:(NSString *)userid
                        zalopayId:(NSString *)zalopayid
                            phone:(NSString *)phone
                      zalopayname:(NSString *)zalopayname
                           status:(int)status
                      displayName:(NSString *)displayName {

    //note: phone ở đây đã format về dạng chuẩn bắt đầu là số 0
    if (zalopayid.length > 0) {
        NSMutableDictionary *param = [NSMutableDictionary dictionary];
        [param setObjectCheckNil:zalopayid forKey:kZalopayid];
        if (zalopayname.length > 0) {
            [param setObjectCheckNil:zalopayname forKey:kZalopayName];
        }
        if (phone.length > 0) {
            [param setObjectCheckNil:phone forKey:kPhone];
        }
        if (displayName.length > 0) {
            [param setObjectCheckNil:displayName forKey:kDisplayName];
        }
        [param setObject:@(status) forKey:kStatus];
        [self.db updateToTable:kZaloFriend params:param whereKey:kUserid isValue:userid];
    }
}

- (void)saveZaloPayInfoData:(NSArray *)data {
    NSMutableArray *allZaloIds = [NSMutableArray array];
    NSMutableArray *allZaloPayInfo = [NSMutableArray array];
    NSMutableDictionary *phoneToZaloId = [NSMutableDictionary dictionary];

    for (NSDictionary *oneDic in data) {
        NSString *zaloPayId = [oneDic stringForKey:@"userid"];
        if (zaloPayId.length == 0) {
            continue;
        }
        UserStatus status = [oneDic intForKey:@"status"];
        NSString *zaloId = [oneDic stringForKey:@"zaloid"];
        NSString *phone = [[[oneDic numericForKey:@"phonenumber"] stringValue] normalizePhoneNumber];
        NSString *zaloPayName = [oneDic stringForKey:@"zalopayname"];
        NSString *displayName = [oneDic stringForKey:@"displayname"];
        NSString *avatar = [oneDic stringForKey:@"avatar"];

        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObjectCheckNil:zaloPayId forKey:kZalopayid];
        if (zaloId.length > 0) {
            [params setObjectCheckNil:zaloId forKey:kUserid];
        }
        if (zaloPayName.length > 0) {
            [params setObjectCheckNil:zaloPayName forKey:kZalopayName];
        }
        if (displayName.length > 0) {
            [params setObjectCheckNil:displayName forKey:kDisplayNameZFL];
        }
        if (avatar.length > 0) {
            [params setObjectCheckNil:avatar forKey:kAvatar];
        }                
        [params setObject:@(status) forKey:kStatus];

        NSDictionary *updates = [self.db buildUpdateQueryTable:KZaloPayContactList params:params whereKey:kPhone isValue:phone];
        [allZaloIds addObjectNotNil:zaloId];
        [allZaloPayInfo addObject:updates];
        [phoneToZaloId setObjectCheckNil:zaloId forKey:phone];
    }

    [self.db.dbQueue inTransaction:^(FMDatabase *_Nonnull db, BOOL *_Nonnull rollback) {
        [self removeZaloFriendWithouInfoFromZPC:allZaloIds database:db];
        [self removeZaloPayInfoWithInvalidPhone:phoneToZaloId database:db];
        [self updateZaloPayInfoFromArray:allZaloPayInfo database:db];
    }];
}

- (void)removeZaloFriendWithouInfoFromZPC:(NSArray *)zaloIds database:(FMDatabase *)db {
    NSString *query = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ IS NULL AND %@ IS NULL AND %@ IS NULL AND %@ IN (%@)", KZaloPayContactList, kZalopayid, kPhone, kDisplayNameUCB, kUserid, [zaloIds componentsJoinedByString:@","]];
    [db executeUpdate:query];
}

- (void)updateZaloPayInfoFromArray:(NSArray *)datas database:(FMDatabase *)db {
    for (NSDictionary *queryData in datas) {
        NSString *query = [queryData stringForKey:kUpdateQuery];
        NSArray *values = [queryData arrayForKey:kUpdateValues];
        [db executeUpdate:query withArgumentsInArray:values];
    }
}

- (void)removeZaloPayInfoWithInvalidPhone:(NSDictionary *)phoneToZaloId database:(FMDatabase *)db {
    //    @"DELETE FROM ZPC WHERE zaloId = ? AND phoneNumber IS NOT NULL AND zalopayId IS NOT NULL AND phoneNumber != ?"
    NSString *query = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ = ? AND %@ IS NOT NULL AND %@ IS NOT NULL AND %@ != ?", KZaloPayContactList, kUserid, kPhone, kZalopayid, kPhone];
    for (NSString *phone in phoneToZaloId.allKeys) {
        NSString *zaloId = [phoneToZaloId stringForKey:phone];
        [db executeUpdate:query, zaloId, phone];
    }
}

- (void)setFavoriteOrIgnore:(NSDictionary *)data {
    NSString *favorite = [data objectForKey:kFavorite];
    NSString *dateModifile = [data objectForKey:kDateModified];
    NSString *zalopayId = [data objectForKey:kZalopayid];
    NSString *zaloId = [data objectForKey:kUserid];
    NSString *phone = [data objectForKey:kPhone];

    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = [NSString stringWithFormat:@"UPDATE OR IGNORE %@ set %@ = ?,%@ = ?  where %@ = ? or %@ = ? or %@ = ?", KZaloPayContactList, kFavorite, kDateModified, kZalopayid, kUserid, kPhone];
        [db executeUpdate:query, favorite, dateModifile, zalopayId, zaloId, phone];
    }];
}

- (void)removeZPCNotInUBC:(FMDatabase *)db {
    NSString *query = @"DELETE FROM ZPC WHERE ZPC.phoneNumber IN (SELECT phoneNumber FROM ZPC WHERE phoneNumber NOT IN (SELECT phoneNumber FROM UserContactBook) AND ZPC.zalopayId is Null AND ZPC.zaloId is Null)";
    [db executeUpdate:query];
}

@end
