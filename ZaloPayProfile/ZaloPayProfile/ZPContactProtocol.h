//
//  ZPContactProtocol.h
//  ZaloPay
//
//  Created by Phuoc Huynh on 7/17/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
//
//typedef enum {
//    UserGenderMale = 1,
//    UserGenderFemale = 2
//} UserGender;
//
typedef enum {
    ContactMode_ZPC_All,
    ContactMode_ZPC_PhoneBook,
} ContactMode;
//
///*
// Trạng thái user tương ứng số phoneinput
// 1: Thành công
// -124: User đang bị lock
// -2005: Số phone không đúng
// -2007: User không tồn tại
// */
//typedef enum {
//    UserStatus_Available = 1,
//    UserStatus_Lock = -124,
//    UserStatus_ReceiverLock = -2013,
//    UserStatus_InvalidPhone = -2005,
//    UserStatus_NotExist = -2007,
//} UserStatus;
//
//
//@class ZPContact;
//
//@protocol ZPContactProtocol <NSObject>
//
//// properties
//@property(nonatomic, strong) NSString *zaloPayId;
//@property(nonatomic, strong) NSString *zaloId;
//@property(nonatomic, strong) NSString *phoneNumber;
//@property(nonatomic, strong) NSString *zaloPayName;
//@property(nonatomic, strong) NSString *displayNameZLF;
//@property(nonatomic, strong) NSString *displayNameUCB;
//@property(nonatomic, strong) NSString *avatar;
//
//@property(nonatomic, strong) NSString *displayName;
//@property(nonatomic, strong) NSString *firstCharacterInDisplayName;
//@property(nonatomic, strong) NSString *asciiDisplayName;
//@property(nonatomic, assign) UserGender gender;
//@property(nonatomic, strong) NSDate *birthDay;
//@property(nonatomic) UserStatus status;
//
//@property(nonatomic) BOOL isFavorite;
//@property(nonatomic) BOOL isRedPacket;
//@property(nonatomic) BOOL usedZaloPay;
//@property(nonatomic, strong) NSString *zpContactID;
//@property(nonatomic) BOOL isSavePhoneNumber;
//
//// methods
//- (NSComparisonResult)compareByDisplayValue:(ZPContact *)other;
//
////+ (NSDictionary *)userDictionary:(ZPContact *)zpContact;
//
////+ (instancetype)userFromServerDictionary:(NSDictionary *)dictionary;
////
////+ (instancetype)createContactFromReactDictionary:(NSDictionary *)dictionary;
//
//- (NSString *)createZaloPayContactId;
//@end
