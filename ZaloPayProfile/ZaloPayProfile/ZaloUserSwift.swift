//
//  ZaloUserSwift.swift
//  ZaloPayProfile
//
//  Created by vuongvv on 4/10/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import Foundation
import ZaloPayProfilePrivate
import ZaloPayCommonSwift
import Synchronized

struct ConstantsZaloUserSwift {
    static let kDisplayName         = "displayName"
    static let kAvatar              = "avatar"
    static let kUserId              = "userId"
    static let kUserGender          = "userGender"
    static let kUsingZaloPay        = "usingApp"
    static let kAscciDisplayName    = "ascciDisplayName"
    static let kzaloPayId           = "zaloPayId"
}

private struct UMConstant {
    static let zaloid      = "zaloid"
    static let userid      = "userid"
    static let zalopayname = "zalopayname"
    static let avatar      = "avatar"
    static let displayname = "displayname"
    static let phonenumber = "phonenumber"
    static let gender      = "gender"
    static let birthdate   = "birthdate"
}

private struct ZaloConstant {
    static let displayName  = "displayName"
    static let largeAvatar  = "largeAvatar"
    static let userId       = "userId"
    static let userGender   = "userGender"
    static let birthDate    = "birthDate"
}

@objc public enum Gender : Int {
    case male = 1
    case female = 2
}
@objcMembers
public class ZaloUserSwift: NSObject {
    public var userId = ""
    public var displayName:String = "" {
        didSet {
                self.asciiDisplayName = (displayName.ascci() ?? "").uppercased()
                if let first = self.asciiDisplayName.first {
                    self.firstCharacterInDisplayName = String(first)
                } else {
                    self.firstCharacterInDisplayName = ""
                }
        }
    }
    public var avatar:String = ""
    public var gender: Gender = .male
    public var birthDay: Date =  Date()
    public var usedZaloPay = false
    // zaloPayId: userid trong hệ thống của zalopay
    // zaloPayName: zalopayid (accountname) trong hệ thống của zalopay
    public var zaloPayId:String = ""
    public var phone:String = ""
    public var zaloPayName:String = ""
    public var firstCharacterInDisplayName:String = ""
    public var asciiDisplayName:String = ""
    
    public class func userDictionary(_ user: ZaloUserSwift) -> [AnyHashable: Any] {
        var dict = [AnyHashable: Any]()
        dict[ConstantsZaloUserSwift.kDisplayName] = user.displayName
        dict[ConstantsZaloUserSwift.kAvatar] = user.avatar
        dict[ConstantsZaloUserSwift.kUserId] = user.userId
        dict[ConstantsZaloUserSwift.kUserGender] = user.gender.rawValue
        dict[ConstantsZaloUserSwift.kUsingZaloPay] = user.usedZaloPay
        dict[ConstantsZaloUserSwift.kAscciDisplayName] = user.asciiDisplayName
        dict[ConstantsZaloUserSwift.kzaloPayId] = user.zaloPayId
        return dict
    }
    
    public class func userFromDictionary(_ dictionary: [AnyHashable: Any] ) -> ZaloUserSwift {
        return ZaloUserSwift(fromDictionary: dictionary)
    }
    
    convenience init(fromDictionary dictionary: [AnyHashable: Any]) {
        self.init()
        self.userId = dictionary.string(forKey: ConstantsZaloUserSwift.kUserId)
        self.displayName = dictionary.string(forKey: ConstantsZaloUserSwift.kDisplayName)
        self.avatar = dictionary.string(forKey: ConstantsZaloUserSwift.kAvatar)
        self.gender = Gender(rawValue: dictionary.int(forKey: ConstantsZaloUserSwift.kUserGender, defaultValue: 1)) ?? .male
        self.usedZaloPay = dictionary.bool(forKey: ConstantsZaloUserSwift.kUsingZaloPay)
    }
    
    public func compare(byDisplayValue other: ZaloUserSwift) -> ComparisonResult {
        return (asciiDisplayName ).compare((other.asciiDisplayName))
    }
    
    public override func isEqual(_ object: Any?) -> Bool {
        guard let orther = object as? ZaloUserSwift else {
            return false
        }
        return userId == orther.userId
    }
    
    public class func currentUserFromZaloPayUM(_ json: [AnyHashable: Any]?) -> ZaloUserSwift? {
        guard let json = json else {
            return nil
        }
        let user = ZaloUserSwift()
        user.userId = json.string(forKey: UMConstant.zaloid)
        user.zaloPayId = json.string(forKey: UMConstant.userid)
        user.zaloPayName = json.string(forKey: UMConstant.zalopayname)
        user.avatar = json.string(forKey: UMConstant.avatar)
        user.displayName = json.string(forKey: UMConstant.displayname)
        user.phone = json.string(forKey: UMConstant.phonenumber)
        user.gender = Gender.init(rawValue: json.int(forKey: UMConstant.gender)) ?? Gender.female
        user.birthDay = Date.init(timeIntervalSince1970: json.double(forKey: UMConstant.birthdate))
        return user
    }
    
    public class func currentUserFromZaloSDK(_ json: [AnyHashable: Any]?) -> ZaloUserSwift? {
        guard let json = json else {
            return nil
        }
        let currentUser = ZaloUserSwift()
        currentUser.displayName = json.string(forKey: ZaloConstant.displayName)
        currentUser.avatar = json.string(forKey: ZaloConstant.largeAvatar)
        currentUser.userId = json.string(forKey: ZaloConstant.userId)
        currentUser.gender =  Gender.init(rawValue: json.int(forKey: ZaloConstant.userGender)) ?? Gender.female
        currentUser.birthDay = Date.init(timeIntervalSince1970: json.double(forKey: ZaloConstant.birthDate))
        return currentUser        
    }
}

