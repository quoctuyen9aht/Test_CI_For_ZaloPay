//
//  ZaloSDKApiWrapper.swift
//  ZaloPay
//
//  Created by Nguyễn Minh Trí on 4/10/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import CocoaLumberjackSwift
import ZaloSDK
import RxSwift
import ReactiveObjC
import ZaloPayCommonSwift
import ZaloPayProfilePrivate

@objcMembers
public class ZaloSDKApiWrapper: NSObject {
    
    public static let sharedInstance = ZaloSDKApiWrapper()
    
    public func zaloUserId() -> String{
        return ZaloSDK.sharedInstance().zaloUserId()
    }
    
    public func zaloOauthcode() -> String{
        return ZaloSDK.sharedInstance().zaloOauthCode()
    }
    
    public func login(withZalo fromController: UIViewController?, type: ZAZaloSDKAuthenType) -> Observable<[String: Any]> {
        return Observable.create({ (subscriber) -> Disposable in
            ZaloSDK.sharedInstance().authenticateZalo(with: type, parentController: fromController, handler: { (response) in
                if response?.errorCode != Int(kZaloSDKErrorCodeNoneError.rawValue) {
                    let errorCode = response?.errorCode ?? 0
                    let error = NSError.init(domain: "Zalo", code: errorCode, userInfo: nil)
                    subscriber.onError(error)
                    
                    return
                }
                var dic = Dictionary<String, Any>()
                dic["uid"] = response?.userId
                dic["oauthCode"] = response?.oauthCode
                subscriber.onNext(dic)
                subscriber.onCompleted()
            })
            return Disposables.create()
        })
    }
    
    public func register(withZalo fromController: UIViewController?) -> Observable<[String: Any]> {
        return Observable.create({ (subscriber) -> Disposable in
            ZaloSDK.sharedInstance().registZaloAccount(withParentController: fromController, handler: { (response) in
                if response?.errorCode != Int(kZaloSDKErrorCodeNoneError.rawValue) {
                    let errorCode = response?.errorCode ?? 0
                    let error = NSError.init(domain: "Zalo", code: errorCode, userInfo: nil)
                    subscriber.onError(error)
                    return
                }
                var dic = Dictionary<String, Any>()
                dic["uid"] = response?.userId
                dic["oauthCode"] = response?.oauthCode
                subscriber.onNext(dic)
                subscriber.onCompleted()
            })
            return Disposables.create()
        })
    }
    
    public func loadZaloUserProfile() -> RACSignal<AnyObject> {
        return RACSignal.createSignal({ (subscriber) -> RACDisposable? in
            ZaloSDK.sharedInstance().getZaloUserProfile(callback: { (response) in
                /// check responseData is ZOProfileResponseObject: vì có case crash zaloSDK trả ra ZOGraphResponseObject.
                guard let responseData = response,
                          responseData is ZOProfileResponseObject else {
                    DDLogInfo("load zalo profile error")
                    subscriber.sendError(nil)
                    return
                }
                if responseData.errorCode == Int(kZaloSDKErrorCodeNoneError.rawValue){
                    if let data = responseData.data {
                        let user = ZaloUserSwift.currentUserFromZaloSDK(data)
                        subscriber.sendNext(user)
                    }
                }
                subscriber.sendCompleted()
            })
            return nil
        })
    }
    
    
    public func loadZaloFriend(_ offset: Int, count: Int) -> RACSignal<AnyObject> {
        return RACSignal.createSignal({ (subscriber) -> RACDisposable? in
            
            ZaloSDK.sharedInstance().getZaloUserFriendList(atOffset: UInt(offset), count: UInt(count), callback: { (response) in
                if response?.errorCode != Int(kZaloSDKErrorCodeNoneError.rawValue) {
                    DDLogInfo("response.errorCode \(String(describing: response?.errorCode))")
                    DDLogInfo("response.name \(String(describing: response?.errorName))")
                }
                
                let friends: [ZaloUserSwift]
                defer {
                    subscriber.sendNext(friends)
                    subscriber.sendCompleted()
                }
                let selector = #selector(getter: ZOFriendsListResponseObject.data)
                guard response?.responds(to: selector) == true else {
                    friends = []
                    return
                }
                let data = response?.data as? [JSON]
                friends = data?.map({ ZaloUserSwift.userFromDictionary($0) }) ?? []
            })
            return nil
        })
    }
    
    public func shareFeedZalo(_ feed: ZOFeed?, andParentVC parent: UIViewController) -> Observable<Bool> {
        return Observable.create({ (subscriber) -> Disposable in
            ZLShareFeed(feed, parent, { (response) in
                if response?.errorCode != Int(kZaloSDKErrorCodeNoneError.rawValue) {
                    let errorCode = response?.errorCode ?? 0
                    let error = NSError(domain: "Zalo", code: errorCode, userInfo: nil)
                    subscriber.onError(error)
                } else {
                    subscriber.onNext(true)
                    subscriber.onCompleted()
                }
            })
            return Disposables.create()
        })
    }
    
   public func shareLinkZalo(_ url: String, andParentVC parent: UIViewController) -> Observable<Bool> {
        return Observable.create({ (subscriber) -> Disposable in
            let feed: ZOFeed = ZOFeed(link: url, appName: "ZaloPay", message: "", others: nil)
            ZLShareFeed(feed, parent, { (response) in
                if response?.errorCode != Int(kZaloSDKErrorCodeNoneError.rawValue) {
                    let errorCode = response?.errorCode ?? 0
                    let error = NSError(domain: "Zalo", code: errorCode, userInfo: nil)
                    subscriber.onError(error)
                } else {
                    subscriber.onNext(true)
                    subscriber.onCompleted()
                }
            })
            return Disposables.create()
        })
    }
    
    public func sendMessageZalo(_ url: String, andParentVC parent: UIViewController?) -> Observable<Bool> {
        return Observable.create({ (subscriber) -> Disposable in
            let feed: ZOFeed = ZOFeed(link: url, appName: "ZaloPay", message: "", others: nil)
            ZaloSDK.sharedInstance().shareZalo(using: .appThenWeb)
            ZaloSDK.sharedInstance().sendMessage(feed, in: parent) { (response) in
                
                if response?.errorCode != Int(kZaloSDKErrorCodeNoneError.rawValue) {
                    let errorCode = response?.errorCode ?? 0
                    let error = NSError(domain: "Zalo", code: errorCode, userInfo: nil)
                    subscriber.onError(error)
                } else {
                    subscriber.onNext(true)
                    subscriber.onCompleted()
                }
            }
            return Disposables.create()
        })
    }
    
    public func logout(){
        ZaloSDK.sharedInstance().unauthenticate()
    }

}
