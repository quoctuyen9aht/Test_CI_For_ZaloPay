//
//  ZPProfileTable.m
//  ZaloPayCommon
//
//  Created by PhucPv on 6/3/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPProfileTable.h"
#import <fmdb/FMDatabase.h>
#import <fmdb/FMDatabaseQueue.h>
#import <ZaloPayProfile/ZaloPayProfile-Swift.h>

@implementation ZPProfileTable

+ (NSArray<NSString*>*) creatingTableQuery {
    NSString* tableProfileInfomation = @"CREATE TABLE IF NOT EXISTS ProfileInfomation(key text PRIMARY KEY NOT NULL, value text);";
    return @[tableProfileInfomation];
}

+ (NSArray<NSString*>*) tableNamesCanBeDropped {
    return @[@"ProfileInfomation"];
}

- (void)updateProfileData:(NSString *)value forKey:(NSString *)key {
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query = @"INSERT OR REPLACE INTO ProfileInfomation (key, value) VALUES (?, ?)";
        [db executeUpdate:query, key, value];
    }];
}

- (NSString *)getProfileValueForKey:(NSString *)key {
    __block NSString *_return;
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSError *error = nil;
        FMResultSet *rs = [db executeQuery:@"SELECT value FROM ProfileInfomation WHERE key=?" values:@[key] error:&error];
        if (error) {
            return;
        }
        if ([rs next]) {
            _return = [rs stringForColumn:@"value"];
        }
        [rs close];
    }];

    return _return;
}

- (NSString *)queryUpdateWithKey:(NSString *)key value:(NSString *)value {
    NSString *query = [NSString stringWithFormat:@"INSERT OR REPLACE INTO ProfileInfomation (key, value) VALUES ('%@', '%@')", key, value];
    return query;
}

- (void)saveCurrentUserInfo:(NSObject *)userInfo {
    ZaloUserSwift *user = (ZaloUserSwift *)userInfo;
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        if (user.userId) {
            [db executeUpdate:[self queryUpdateWithKey:kUserid value:user.userId]];
        }
        if (user.displayName) {
            [db executeUpdate:[self queryUpdateWithKey:kDisplayName value:user.displayName]];
        }
        if (user.avatar) {
            [db executeUpdate:[self queryUpdateWithKey:kAvatar value:user.avatar]];
        }
        [db executeUpdate:[self queryUpdateWithKey:kUserGender value:[@(user.gender) stringValue]]];

        if (user.birthDay) {
            NSString *timeString = [@([user.birthDay timeIntervalSince1970]) stringValue];
            [db executeUpdate:[self queryUpdateWithKey:kBirthDate value:timeString]];
        }
    }];
}

- (NSObject *)getCurrentUserInfo {
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM ProfileInfomation"];

    __block NSString *userId;
    __block NSString *displayName;
    __block NSString *avatar;
    __block int gender;
    __block NSDate *birhdate;

    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:query];
        while ([rs next]) {
            if ([[rs stringForColumn:@"key"] isEqualToString:kUserid]) {
                userId = [rs stringForColumn:@"value"];
            }

            if ([[rs stringForColumn:@"key"] isEqualToString:kDisplayName]) {
                displayName = [rs stringForColumn:@"value"];
            }

            if ([[rs stringForColumn:@"key"] isEqualToString:kAvatar]) {
                avatar = [rs stringForColumn:@"value"];
            }

            if ([[rs stringForColumn:@"key"] isEqualToString:kUserGender]) {
                gender = [rs intForColumn:@"value"];
            }

            if ([[rs stringForColumn:@"key"] isEqualToString:kBirthDate]) {
                uint64_t time = [rs longLongIntForColumn:@"value"];
                birhdate = [NSDate dateWithTimeIntervalSince1970:time];
            }
        }
        [rs close];
    }];
    if (userId.length == 0) {
        return nil;
    }

    ZaloUserSwift *user = [[ZaloUserSwift alloc] init];
    user.userId = userId;
    user.displayName = displayName;
    user.avatar = avatar;
    user.gender = gender;
    user.birthDay = birhdate;
    return user;
}

@end
