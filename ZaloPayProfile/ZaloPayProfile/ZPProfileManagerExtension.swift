//
//  ZPProfileManagerExtension.swift
//  ZaloPay
//
//  Created by vuongvv on 4/10/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayProfilePrivate
import ZaloPayCommonSwift
import CocoaLumberjackSwift
import CocoaLumberjack
import AddressBook
import ZaloPayConfig

struct ConstantsKeyZPProfileManager {
    public static let kUsersList = "userslist"
    public static let kZaloId = "zaloid"
    public static let kUserId = "userid"
    public static let kStatus = "status"
    public static let kPhoneNumber = "phonenumber"
    public static let kZalopayName = "zalopayname"
    public static let kAvatar = "avatar"
    public static let kDisplayName = "displayname"
    public static let kDBZaloId = "zaloId"
    public static let kDBDisplayName = "displayName"
    public static let kDBAvatar = "avatar"
    public static let kDBPhoneNumber = "phoneNumber"
    public static let kDBZalopayId = "zalopayId"
    public static let kDBZalopayName = "zalopayName"
    public static let kDBstatus = "status"
    public static let kDBContactName = "contactname"
    public static let kDBDisplayNameZLF = "displayNameZLF"
    public static let kDBDisplayNameUCB = "displayNameUCB"
    public static let kDBFirstName = "firstName"
    public static let kDBLastName = "lastName"
}
fileprivate let kUpdateInterval = 3 * 24 * 60 * 60

public extension ZPProfileManager {
    
    public func getZaloFriendTable() -> ZPZaloFriendTable {
        return zaloFriendTable as! ZPZaloFriendTable
    }
    
    public func loadZaloUserProfile() -> RACSignal<AnyObject> {
        return loadZaloUserProfile(usingCache: true)
    }
    public func loadZaloUserProfile(usingCache: Bool) -> RACSignal<AnyObject> {
        
        if currentZaloUser == nil {
            currentZaloUser = dbGetCurrentUserInfo()
        }
        
        if currentZaloUser != nil && usingCache {
            return RACSignal.return(currentZaloUser)
        }
        
        return self.loadZaloPayUserProfile().doNext({ [weak self] user
            in
                guard let userGet = user as? ZaloUserSwift else {
                    return
                }
                self?.currentZaloUser = userGet
                self?.dbSaveCurrentUserInfo(userGet)
        })
    }
    
    func loadZaloPayUserProfile() -> RACSignal<AnyObject> {
        if let level = ZPProfileManager.shareInstance.userLoginData?.profilelevel,level < ZPProfileLevel.level3.rawValue  {
                return ZaloSDKApiWrapper.sharedInstance.loadZaloUserProfile()
        }
        else {
            return self.network.getCurrenUserProfile().map({(data) -> Any? in
                return ZaloUserSwift.currentUserFromZaloPayUM(data as? JSON)
            })
        }
    }
    
    public func loadAllZaloPayId(_ forceSyncContact: Bool) -> RACSignal<AnyObject> {
        return RACSignal.createSignal({ [weak self] (subscriber) -> RACDisposable? in
            DDLogInfo("Starting loadAllZaloPayId")
            let toGet = self?.getZaloFriendTable().getAllFriendIdShouldRequestZaloPayInfo(forceSyncContact) ?? []
            if toGet.count == 0 {
                subscriber.sendCompleted()
                return nil
            }
            self?.getZaloPayId(toGet, pageCount: 50, subscriber: subscriber)
            return nil
        })
    }
    
    public func loadZaloUserFriendList(forceSyncContact: Bool) -> RACSignal<AnyObject> {
        let zFriendsCountOld = getZaloFriendTable().countZaloFriendList()
        let enable = ZPContactService.isEnableReadContact()
        return loadAllZaloFriend().then({
                let zFriendsCountNew = self.getZaloFriendTable().countZaloFriendList()
                let syncContact: Bool = (enable && zFriendsCountNew != zFriendsCountOld) ? true : forceSyncContact
                return RACSignal<AnyObject>.concat([self.loadAllZaloPayId(syncContact),
                                                    ZPContactService.syncAllContact(toDataBase: syncContact),
                                                    self.mergeAllContactDataToZPC(),
                                                    self.loadAllZaloPayInfoFromPhone(forceSync: syncContact)] as NSArray
                )
        })
    }
    
    public func getZaloUserProfile(zaloId: String) -> ZaloUserSwift {
        let userInfoGet = getZaloFriendTable().getZaloUser(withId:zaloId)
        guard let userInfo = userInfoGet else {
            return ZaloUserSwift()
        }
        return self.user(fromDatabaseDic: userInfo)
    }
    
    public func getZaloFriendListFromDB() -> [ZaloUserSwift] {
        let allFriendInfoGet = getZaloFriendTable().zaloFriendList()
        guard let allFriendInfo = allFriendInfoGet as? [[String : Any]] else {
            return [ZaloUserSwift]()
        }
        var _return = [ZaloUserSwift]()
        for userInfo in allFriendInfo {
            let user: ZaloUserSwift = self.user(fromDatabaseDic: userInfo)
            _return.append(user)
        }
        return _return
    }
    
    public func lixiZaloFriendInfo(withIds ids: [String]) -> [[String : Any]] {
        guard let arrDic = getZaloFriendTable().lixiZaloFriendInfo(withIds: ids) as? [[String : Any]] else {
            return [[String : Any]]()
        }
        return arrDic
    }
    
    public func requestUserProfile(userId: String) -> RACSignal<AnyObject> {
        if userId.count == 0 {
            return RACSignal.error(NSError(domain: "UserId Nil", code: -1, userInfo: nil))
        }
        return self.network.getListZaloPayId([userId]).map({ resultGet in
            guard let result =  resultGet as? [AnyHashable: Any] else {
                return nil
            }
            let userslist = result.value(forKey: ConstantsKeyZPProfileManager.kUsersList,defaultValue:[[String : Any]]())
            if userslist.count == 0 {
                return nil
            }
            guard let oneDic = userslist.last else {
                return nil
            }
            let zaloId = oneDic.string(forKey: ConstantsKeyZPProfileManager.kZaloId)
            let zaloPayId = oneDic.string(forKey: ConstantsKeyZPProfileManager.kUserId)
            guard let status: UserStatus = UserStatus(rawValue:oneDic.value(forKey: ConstantsKeyZPProfileManager.kStatus,defaultValue: 1)) else  { return nil }
            if (zaloId != userId) || status.rawValue != 1 || zaloPayId.count  == 0 {
                DDLogInfo("invalid user")
                return nil
            }
            
            let phone = ("\(String(describing: oneDic.value(forKey: ConstantsKeyZPProfileManager.kPhoneNumber,defaultValue: 0)))" as NSString).normalizePhoneNumber()
            let zaloPayName = oneDic.string(forKey: ConstantsKeyZPProfileManager.kZalopayName)
            let avatar = oneDic.string(forKey: ConstantsKeyZPProfileManager.kAvatar)
            let displayName = oneDic.string(forKey: ConstantsKeyZPProfileManager.kDisplayName)
            
            self.getZaloFriendTable().saveZalopayInfo(withUserId: zaloId, zalopayId: zaloPayId, phone: phone, zalopayname: zaloPayName, status: Int32(status.rawValue), displayName: displayName)
            
            let _return = ZaloUserSwift()
            _return.zaloPayId = zaloPayId
            _return.userId = zaloId
            _return.displayName = displayName
            _return.avatar = avatar
            _return.phone = phone ?? ""
            _return.zaloPayName = zaloPayName
            return _return
        })
    }
    
    //public func loadConfig() {
    //    enableMergeName = ZPApplicationConfig.getZPCAppConfig()?.getEnableMergeContactName().toBool(defaultValue: false)
    //    phonePattern = ZPApplicationConfig.getGeneralConfig()?.getPhoneFormat()?.getPatterns() ?? [String]()
    //}
    
    public func getContactInfo(byPhone phone: String) -> ZPContactSwift {
        let userInfo: [AnyHashable: Any] = getZaloFriendTable().getUserFromZaloPayContact(phone)
        return self.user(fromZPCDatabaseDic: userInfo)
    }
    
    public func getListContactInfo(byPhone phones: [String]) -> [ZPContactSwift] {
        let allZPCInfoGet = getZaloFriendTable().getListUser(fromZaloPayContact: phones)
        guard let allZPCInfo = allZPCInfoGet as? [[String : Any]] else {
            return [ZPContactSwift]()
        }
        return self.parseDicContact(allZPCInfo)
    }
    
    public func getZaloPayContactList(fromDB kindShowContactList: UInt32) -> [ZPContactSwift] {
        let contactMode:ContactMode = ContactMode(rawValue:kindShowContactList)
        let allZPCInfoGet = getZaloFriendTable().zaloPayContactList(contactMode)
        guard let allZPCInfo = allZPCInfoGet as? [[String : Any]] else {
            return [ZPContactSwift]()
        }
        return self.parseDicContact(allZPCInfo)
    }
    
    public func getZaloPayContactListFavorite(fromDB contactMode: UInt32) -> [ZPContactSwift] {
        let contactModeGet:ContactMode = ContactMode(rawValue:contactMode)
        let allZPCInfoGet = getZaloFriendTable().zaloPayContactListFavorite(contactModeGet)
        guard let allZPCInfo = allZPCInfoGet as? [[String : Any]] else {
            return [ZPContactSwift]()
        }
        return self.parseDicContact(allZPCInfo)
    }
    
    func parseDicContact(_ arrDic : [[String : Any]]) -> [ZPContactSwift]{
        var _return = [ZPContactSwift]()
        for userInfo: [AnyHashable: Any] in arrDic {
            let user: ZPContactSwift = self.user(fromZPCDatabaseDic: userInfo)
            _return.append(user)
        }
        return _return
    }
    
    public func getUnsavePhoneContactFromDB(withPhoneStringInput phoneStringInput: String) -> ZPContactSwift {
        let dicGet:[AnyHashable: Any]? = getZaloFriendTable().getUnsavePhoneContact(with: phoneStringInput)
        guard let dic = dicGet , dic.count == 0 else {
            return ZPContactSwift()
        }
        let zaloId = dic.string(forKey: ConstantsKeyZPProfileManager.kDBZaloId)
        let userdisplayName = dic.string(forKey: ConstantsKeyZPProfileManager.kDBDisplayName)
        let avatar = dic.string(forKey: ConstantsKeyZPProfileManager.kDBAvatar)
        let phone = dic.string(forKey: ConstantsKeyZPProfileManager.kDBPhoneNumber)
        let zaloPayId = dic.string(forKey: ConstantsKeyZPProfileManager.kDBZalopayId)
        let zaloPayName = dic.string(forKey: ConstantsKeyZPProfileManager.kDBZalopayName)
        
        let user = ZPContactSwift()
        user.zaloId = zaloId
        user.displayName = userdisplayName
        user.displayNameZLF = userdisplayName
        user.displayNameUCB = userdisplayName
        user.avatar = avatar
        user.usedZaloPay = zaloPayId.count > 0
        user.phoneNumber = phone
        user.zaloPayId = zaloPayId
        user.zaloPayName = zaloPayName
        user.zpContactID = user.createZaloPayContactId()
        if let status: UserStatus = UserStatus(rawValue:dic.value(forKey: ConstantsKeyZPProfileManager.kDBstatus,defaultValue: 1)) {
            user.status = status
        }
        
        // No need these lines, asciiDisplayName is set after displayName
//        user.asciiDisplayName = (userdisplayName.ascci() ?? "").uppercased()
//        if user.asciiDisplayName.count > 0 {
//            user.firstCharacterInDisplayName = (user.asciiDisplayName ?? "").getFirstCharacters(numWords: 1)
//        }
        user.isSavePhoneNumber = false
        return user
    }
    
    public func loadAllZaloPayInfoFromPhone(forceSync: Bool) -> RACSignal<AnyObject> {
        let shouldUpdate: Bool = shouldLoadZaloPay(fromPhone: forceSync)
        if shouldUpdate == false {
            return RACSignal.empty()
        }
        return RACSignal.createSignal({ [weak self] (subscriber) -> RACDisposable? in
            DDLogInfo("Starting loadAllZaloPayInfoFromPhone")
            let  toGet = (forceSync ? self?.getZaloFriendTable().getAllPhonesInZPCShouldRequestInfo() : self?.getZaloFriendTable().getAllPhonesShouldRequestInfo()) ?? []
            DDLogInfo("all phone = \(String(describing: toGet))")
            if toGet.count == 0 {
                subscriber.sendCompleted()
                return nil
            }
            self?.getZaloPayInfo(fromPhones: toGet, pageCount: 50, subscriber: subscriber)
            return nil
        })
    }
    
    public func shouldLoadZaloPay(fromPhone forceSync: Bool) -> Bool {
        var forceSyncGet = forceSync
        let cacheTable = ZPCacheDataTable(db: PerUserDataBase.sharedInstance())
        let current = Date().timeIntervalSince1970.toUInt64()
        
        if forceSyncGet == false {
            let last = cacheTable?.cacheDataValue(forKey: kLastTimeLoadZaloPayInfoFromPhome)?.toUInt64() ?? 0
            forceSyncGet = current - last > kUpdateInterval
        }
        if forceSyncGet {
            cacheTable?.cacheDataUpdateValue(current.toString(), forKey: kLastTimeLoadZaloPayInfoFromPhome)
        }
        return forceSyncGet
    }
    
    public func getZaloPayInfo(fromPhones toGet: NSMutableArray, pageCount count: Int, subscriber subcriber: RACSubscriber?) {
        let phones = toGet.subArray(at: 0, withTotalItem: count) ?? []
        toGet.removeObjects(in: phones)
        
        let params = (phones as? Array<String>) ?? []
        self.network.getListZaloPayInfo(fromPhones: params).subscribeNext({ [weak self] (resultGet) in
            guard let result =  resultGet as? [AnyHashable: Any] else {
                subcriber?.sendCompleted()
                return
            }
            let userslist = result.value(forKey: ConstantsKeyZPProfileManager.kUsersList, defaultValue: [[String : Any]]())
            self?.getZaloFriendTable().saveZaloPayInfoData(userslist)
            DDLogInfo("userslist = \(String(describing: userslist))")
            if toGet.count == 0 {
                subcriber?.sendCompleted()
                return
            }
            self?.getZaloPayInfo(fromPhones: toGet, pageCount: count, subscriber: subcriber)
        }, error:({ _ in
                subcriber?.sendCompleted()
        }))
    }
    
    public func mergeAllContactDataToZPC() -> RACSignal<AnyObject> {
        return RACSignal.createSignal({ subscriber -> RACDisposable? in
            DDLogInfo("Starting mergeAllContactDataToZPC")
            self.getZaloFriendTable().mergeAllContactDataToZPC()
            subscriber.sendCompleted()
            return nil
        })
    }
    
    public func getZaloPayId(_ toGet: NSMutableArray, pageCount count: Int, subscriber subcriber: RACSubscriber?) {
        
        let zaloIds = toGet.subArray(at: 0, withTotalItem: count) ?? []
        toGet.removeObjects(in: zaloIds)
        let params = (zaloIds as? Array<String>) ?? []
        self.network.getListZaloPayId(params).subscribeNext({ [weak self] (resultGet) in
                guard let result =  resultGet as? [AnyHashable: Any] else {
                    subcriber?.sendCompleted()
                    return
                }
                let userslist = result.value(forKey: ConstantsKeyZPProfileManager.kUsersList, defaultValue: [[String : Any]]())
                for oneDic in userslist {
                    let zaloPayId = oneDic.string(forKey: ConstantsKeyZPProfileManager.kUserId, defaultValue: "")
                    if zaloPayId.isEmpty {
                        continue
                    }
                    let status: UserStatus =  UserStatus(rawValue:oneDic.int(forKey: ConstantsKeyZPProfileManager.kStatus)) ?? UserStatus.Available
                    let zaloId = oneDic.string(forKey: ConstantsKeyZPProfileManager.kZaloId)
                    let phone = (oneDic.string(forKey: ConstantsKeyZPProfileManager.kPhoneNumber,defaultValue: "0") as NSString).normalizePhoneNumber()
                    let zaloPayName = oneDic.string(forKey: ConstantsKeyZPProfileManager.kZalopayName)
                    let displayName = oneDic.string(forKey: ConstantsKeyZPProfileManager.kDisplayName)
                    
                    self?.getZaloFriendTable().saveZalopayInfo(withUserId: zaloId, zalopayId: zaloPayId, phone: phone, zalopayname: zaloPayName, status: Int32(status.rawValue), displayName: displayName)
                }
                if toGet.count == 0 {
                    subcriber?.sendCompleted()
                    return
                }
                self?.getZaloPayId(toGet, pageCount: count, subscriber: subcriber)
            
            }, error:({ _ in
               subcriber?.sendCompleted()
        }))
    }

    public func loadAllZaloFriend() -> RACSignal<AnyObject> {
        return RACSignal.createSignal({ [weak self] (subscriber) -> RACDisposable? in
            DDLogInfo("Starting loadAllZaloFriend")
            self?.loadZaloFriend(0, count: 50, signal: subscriber)
            return nil
        })
    }
    
    public func loadZaloFriend(_ offset: Int32, count: Int32, signal: RACSubscriber?) {
        ZaloSDKApiWrapper.sharedInstance.loadZaloFriend(Int(offset), count: Int(count)).subscribeNext({ [weak self] friends in
            
            guard let friendsGet = friends as? [ZaloUserSwift] else {
                signal?.sendError(nil)
                return
            }
            self?.getZaloFriendTable().saveZaloFriends(friendsGet)
            if friendsGet.count < count {
                signal?.sendCompleted()
                return
            }
            self?.loadZaloFriend(offset + count, count: count, signal: signal)
        })
    }
    
    public func isDoneLoadContact() -> Bool {
        return getZaloFriendTable().countTotalContact() > 0
    }
    
    public func user(fromDatabaseDic dic: [AnyHashable: Any]) -> ZaloUserSwift {
        let userId = dic.string(forKey: ConstantsKeyZPProfileManager.kDBZaloId)
        let displayName = dic.string(forKey: ConstantsKeyZPProfileManager.kDBDisplayName)
        let avatar = dic.string(forKey: ConstantsKeyZPProfileManager.kDBAvatar)
        let phone = dic.string(forKey: ConstantsKeyZPProfileManager.kDBPhoneNumber)
        let zaloPayId = dic.string(forKey: ConstantsKeyZPProfileManager.kDBZalopayId)
        let zaloPayName = dic.string(forKey: ConstantsKeyZPProfileManager.kZalopayName)
        let contactname = dic.string(forKey: ConstantsKeyZPProfileManager.kDBContactName)
        let userDisplayName = (enableMergeName ?? false) && contactname.count > 0 ? contactname : displayName
        let user = ZaloUserSwift()
        user.userId = userId
        user.displayName = userDisplayName
        user.avatar = avatar
        user.usedZaloPay = zaloPayId.count > 0
        user.phone = phone
        user.zaloPayId = zaloPayId
        user.zaloPayName = zaloPayName
        return user
    }
    
    public func user(fromZPCDatabaseDic dic: [AnyHashable: Any]) -> ZPContactSwift {
        let zaloId = dic.string(forKey: ConstantsKeyZPProfileManager.kDBZaloId)
        let userdisplayNameZLF = dic.string(forKey: ConstantsKeyZPProfileManager.kDBDisplayNameZLF)
        let userdisplayNameUCB = dic.string(forKey: ConstantsKeyZPProfileManager.kDBDisplayNameUCB)
        let avatar = dic.string(forKey: ConstantsKeyZPProfileManager.kDBAvatar)
        let phone = dic.string(forKey: ConstantsKeyZPProfileManager.kDBPhoneNumber)
        let zaloPayId = dic.string(forKey: ConstantsKeyZPProfileManager.kDBZalopayId)
        let zaloPayName = dic.string(forKey: ConstantsKeyZPProfileManager.kDBZalopayName)
        let firstName = dic.string(forKey: ConstantsKeyZPProfileManager.kDBFirstName)
        let lastName = dic.string(forKey: ConstantsKeyZPProfileManager.kDBLastName)
        
        let user = ZPContactSwift()
        user.zaloId = zaloId
        user.displayNameUCB = userdisplayNameUCB
        user.displayNameZLF = userdisplayNameZLF
        user.avatar = avatar
        user.usedZaloPay = zaloPayId.count  > 0
        user.phoneNumber = phone
        user.zaloPayId = zaloPayId
        user.zaloPayName = zaloPayName
        user.zpContactID = user.createZaloPayContactId()
        user.firstName = firstName
        user.lastName = lastName
        
        if let status: UserStatus = UserStatus(rawValue:Int(dic.int32(forKey: ConstantsKeyZPProfileManager.kDBstatus))) {
            user.status = status
        }
        let displayNameFinal = userdisplayNameUCB.count > 0 ? userdisplayNameUCB : userdisplayNameZLF
        user.displayName = displayNameFinal
        user.isSavePhoneNumber = true
        return user
    }    
}
extension String {
    func getFirstCharacters(numWords: Int) -> String {
        guard (numWords > 0) else {
            return ""
        }
        let words = self.trimmingCharacters(in: .whitespacesAndNewlines).split(separator: " ")
        let n = words.compactMap({ $0.first }).map({ String($0) }).joined()
        let startIdx = n.startIndex
        let endIdx = n.endIndex
        guard let end = n.index(startIdx, offsetBy: numWords, limitedBy: endIdx) else {
            return n
        }
        return String(n[startIdx..<end])
    }
}

