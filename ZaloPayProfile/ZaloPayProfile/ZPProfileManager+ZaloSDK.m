////
////  ZPProfileManager+ZaloSDK.m
////  ZaloPayProfile
////
////  Created by bonnpv on 12/19/16.
////  Copyright © 2016 VNG. All rights reserved.
////
//
//#import <ZaloPayProfile/ZPProfileTable.h>
//#import <ZaloPayProfile/ZPZaloFriendTable.h>
////#import <ZaloPayProfile/ZPContactService.h>
//#import <ZaloPayCommon/NSString+Asscci.h>
//#import <ZaloPayDatabase/PerUserDataBase.h>
//
////#import "ApplicationState+GetConfig.h"
//#import "ZaloSDKApiWrapper.h"
//#import "NetworkManager+Order.h"
//#import "ZPContact.h"
//#import ZALOPAY_MODULE_SWIFT
//#import <ZaloPayProfile/ZaloPayProfile-Swift.h>
//
//#define kUpdateInterval  3 * 24 * 60 * 60
//
//@implementation ZPProfileManager (ZaloSDK)
//
//- (RACSignal *)loadZaloUserProfile {
//    return [self loadZaloUserProfileUsingCache:TRUE];
//}
//
//- (RACSignal *)loadZaloUserProfileUsingCache:(BOOL)usingCache {
//
//    if (!self.currentZaloUser) {
//        self.currentZaloUser = [self.profileTable getCurrentUserInfo];
//    }
//
//    if (self.currentZaloUser && usingCache) {
//        return [RACSignal return:self.currentZaloUser];
//    }
//
//    return [[[ZaloSDKApiWrapper sharedInstance] loadZaloUserProfile] doNext:^(ZaloUserSwift *user) {
//        self.currentZaloUser = user;
//        [self.profileTable saveCurrentUserInfo:user];
//    }];
//}
//
//
//- (RACSignal *)loadZaloUserFriendList:(BOOL)forceSyncContact {
//    NSInteger zFriendsCountOld = [self.zaloFriendTable countZaloFriendList];
//    return [[self loadAllZaloFriend] then:^RACSignal* {
//        NSInteger zFriendsCountNew = [self.zaloFriendTable countZaloFriendList];
//        BOOL syncContact = zFriendsCountNew != zFriendsCountOld ? true : forceSyncContact;
//        return [RACSignal concat:@[[self loadAllZaloPayId],
//                                    [ZPContactService syncAllContactToDataBase:syncContact],
//                                    [self mergeAllContactDataToZPC],
//                                    [self loadAllZaloPayInfoFromPhone:syncContact]
//                                    ]] ;
//    }];
//}
//
//- (RACSignal *)mergeAllContactDataToZPC {
//    return [RACSignal createSignal:^RACDisposable *(id <RACSubscriber> subscriber) {
//        DDLogInfo(@"Starting mergeAllContactDataToZPC");
//        [self.zaloFriendTable mergeAllContactDataToZPC];
//        [subscriber sendCompleted];
//        return nil;
//    }];
//}
//
//- (RACSignal *)loadAllZaloPayInfoFromPhone:(BOOL)forceSync {
//    BOOL shouldUpdate = [self shouldLoadZaloPayFromPhone:forceSync];
//    if (shouldUpdate == false) {
//        return [RACSignal empty];
//    }
//    return [RACSignal createSignal:^RACDisposable *(id <RACSubscriber> subscriber) {
//        DDLogInfo(@"Starting loadAllZaloPayInfoFromPhone");
//        NSMutableArray *toGet = forceSync ? [self.zaloFriendTable getAllPhonesInZPCShouldRequestInfo] : [self.zaloFriendTable getAllPhonesShouldRequestInfo];
////        NSMutableArray *toGet = forceSync ? [[PerUserDataBase sharedInstance] getAllPhonesInZPCShouldRequestInfo] : [[PerUserDataBase sharedInstance] getAllPhonesShouldRequestInfo];
//        DDLogInfo(@"all phone = %@", toGet);
//        if (toGet.count == 0) {
//            [subscriber sendCompleted];
//            return nil;
//        }
//        [self getZaloPayInfoFromPhones:toGet pageCount:50 subscriber:subscriber];
//        return nil;
//    }];
//}
//
//- (void)getZaloPayInfoFromPhones:(NSMutableArray *)toGet
//                       pageCount:(int)count
//                      subscriber:(id <RACSubscriber>)subcriber {
//
//    NSArray *phones = [toGet subArrayAtIndex:0 withTotalItem:count];
//    [toGet removeObjectsInArray:phones];
//
//    [[[NetworkManager sharedInstance] getListZaloPayInfoFromPhones:phones] subscribeNext:^(NSDictionary *result) {
//        NSArray *userslist = [result arrayForKey:@"userslist"];
//        DDLogInfo(@"userslist  = %@", userslist);
//        [self.zaloFriendTable saveZaloPayInfoData:userslist];
//        if (toGet.count == 0) {
//            [subcriber sendCompleted];
//            return;
//        }
//        [self getZaloPayInfoFromPhones:toGet pageCount:count subscriber:subcriber];
//    }                                                                              error:^(NSError *error) {
//        [subcriber sendCompleted];
//    }];
//}
//
//- (RACSignal *)loadAllZaloPayId {
//    return [RACSignal createSignal:^RACDisposable *(id <RACSubscriber> subscriber) {
//        DDLogInfo(@"Starting loadAllZaloPayId");
//        NSMutableArray *toGet = [self.zaloFriendTable getAllFriendIdShouldRequestZaloPayInfo];
//        if (toGet.count == 0) {
//            [subscriber sendCompleted];
//            return nil;
//        }
//        [self getZaloPayId:toGet pageCount:50 subscriber:subscriber];
//        return nil;
//    }];
//}
//
//- (void)getZaloPayId:(NSMutableArray *)toGet
//           pageCount:(int)count
//          subscriber:(id <RACSubscriber>)subcriber {
//
//    NSArray *zaloIds = [toGet subArrayAtIndex:0 withTotalItem:count];
//    [toGet removeObjectsInArray:zaloIds];
//
//    [[[NetworkManager sharedInstance] getListZaloPayId:zaloIds] subscribeNext:^(NSDictionary *result) {
//        NSArray *userslist = [result arrayForKey:@"userslist"];
//        for (NSDictionary *oneDic in userslist) {
//            NSString *zaloPayId = [oneDic stringForKey:@"userid"];
//            if (zaloPayId.length == 0) {
//                continue;
//            }
//            UserStatus status = [oneDic intForKey:@"status"];
//            NSString *zaloId = [oneDic stringForKey:@"zaloid"];
//            NSString *phone = [[[oneDic numericForKey:@"phonenumber"] stringValue] normalizePhoneNumber];
//            NSString *zaloPayName = [oneDic stringForKey:@"zalopayname"];
//            [self.zaloFriendTable saveZalopayInfoWithUserId:zaloId
//                                              zalopayId:zaloPayId
//                                                  phone:phone
//                                            zalopayname:zaloPayName
//                                                 status:status];
//        }
//        if (toGet.count == 0) {
//            [subcriber sendCompleted];
//            return;
//        }
//        [self getZaloPayId:toGet pageCount:count subscriber:subcriber];
//    }                                                                   error:^(NSError *error) {
//        [subcriber sendCompleted];
//    }];
//}
//
//- (RACSignal *)loadAllZaloFriend {
//    return [RACSignal createSignal:^RACDisposable *(id <RACSubscriber> subscriber) {
//        DDLogInfo(@"Starting loadAllZaloFriend");
//        [self loadZaloFriend:0 count:50 signal:subscriber];
//        return nil;
//    }];
//}
//
//- (void)loadZaloFriend:(int)offset
//                 count:(int)count
//                signal:(id <RACSubscriber>)signal {
//    [[[ZaloSDKApiWrapper sharedInstance] loadZaloFriend:offset count:count] subscribeNext:^(NSArray *friends) {
//        [self.zaloFriendTable saveZaloFriends:friends];
//        if (friends.count < count) {
//            [signal sendCompleted];
//            return;
//        }
//        [self loadZaloFriend:offset + count count:count signal:signal];
//    }];
//}
//
//- (BOOL)isDoneLoadContact {
//    return [self.zaloFriendTable countTotalContact] > 0;
//}
//
//- (ZaloUserSwift *)getZaloUserProfile:(NSString *)zaloId {
//    NSDictionary *userInfo = [self.zaloFriendTable getZaloUserWithId:zaloId];
//    DDLogInfo(@"zaloid = %@", zaloId);
//    DDLogInfo(@"userInfo = %@", userInfo);
//    return [self userFromDatabaseDic:userInfo];
//}
//
//- (NSArray *)getZaloFriendListFromDB {
//    NSArray *allFriendInfo = [self.zaloFriendTable zaloFriendList];
//
//    NSMutableArray *_return = [NSMutableArray array];
//    for (NSDictionary *userInfo in allFriendInfo) {
//        ZaloUserSwift *user = [self userFromDatabaseDic:userInfo];
//        [_return addObject:user];
//    }
//    return _return;
//}
//
//- (ZPContactSwift *)getContactInfoByPhone:(NSString *)phone {
//    NSDictionary *userInfo = [self.zaloFriendTable getUserFromZaloPayContact:phone];
//    return [self userFromZPCDatabaseDic:userInfo];
//}
//
//- (NSArray<ZPContactSwift *> *)getListContactInfoByPhone:(NSArray *)phones {
//    NSArray *listUserInfo = [self.zaloFriendTable getListUserFromZaloPayContact:phones];
//    NSMutableArray *arrParse = [NSMutableArray new];
//    for (NSDictionary *userInfo in listUserInfo) {
//        [arrParse addObject:[self userFromZPCDatabaseDic:userInfo]];
//    }
//    return arrParse;
//}
//
//- (NSArray *)getZaloPayContactListFromDB:(ContactMode)kindShowContactList {
//    NSArray *allZPCInfo = [self.zaloFriendTable zaloPayContactList:kindShowContactList];
//
//    NSMutableArray *_return = [NSMutableArray array];
//    for (NSDictionary *userInfo in allZPCInfo) {
//        ZPContactSwift * user = [self userFromZPCDatabaseDic:userInfo];
//        [_return addObject:user];
//    }
//    return _return;
//}
//
//- (NSArray *)getZaloPayContactListFavoriteFromDB:(ContactMode)contactMode {
//    NSArray *allZPCInfo = [self.zaloFriendTable zaloPayContactListFavorite:contactMode];
//    NSMutableArray *_return = [NSMutableArray array];
//    for (NSDictionary *userInfo in allZPCInfo) {
//        ZPContactSwift * user = [self userFromZPCDatabaseDic:userInfo];
//        [_return addObject:user];
//    }
//    return _return;
//}
//
//- (ZPContactSwift *)getUnsavePhoneContactFromDBWithPhoneStringInput:(NSString *)phoneStringInput {
//    NSDictionary *dic = [self.zaloFriendTable getUnsavePhoneContactWith:phoneStringInput];
//    if (dic.count == 0) {
//        return nil;
//    }
//
//    NSString *zaloId = [dic stringForKey:@"zaloId"];
//    NSString *userdisplayName = [dic stringForKey:@"displayName"];
//    NSString *avatar = [dic stringForKey:@"avatar"];
//    NSString *phone = [dic stringForKey:@"phoneNumber"];
//    NSString *zaloPayId = [dic stringForKey:@"zalopayId"];
//    NSString *zaloPayName = [dic stringForKey:@"zalopayName"];
//    UserStatus status = [dic intForKey:@"status"];
//
//    ZPContact *user = [ZPContact new];
//    user.zaloId = zaloId;
//    user.displayName = userdisplayName;
//    user.displayNameZLF = userdisplayName;
//    user.displayNameUCB = userdisplayName;
//    user.avatar = avatar;
//    user.usedZaloPay = zaloPayId.length > 0;
//    user.phoneNumber = phone;
//    user.zaloPayId = zaloPayId;
//    user.zaloPayName = zaloPayName;
//    user.zpContactID = [self createZPContactId:user];
//    user.status = status;
//
//    user.asciiDisplayName = [[userdisplayName ascciString] uppercaseString];
//    //[user setFirstCharacterInDisplayName];
//    if (user.asciiDisplayName.length > 0) {
//        user.firstCharacterInDisplayName = [user.asciiDisplayName substringToIndex:1];
//    }
//    user.isSavePhoneNumber = NO;
//    return user;
//}
//
//- (NSArray *)lixiZaloFriendInfoWithIds:(NSArray *)ids {
//    return [self.zaloFriendTable lixiZaloFriendInfoWithIds:ids];
//}
//
//- (NSString *)createZPContactId:(ZPContactSwift *)contact {
//    return [NSString stringWithFormat:@"%@%@%@", contact.phoneNumber, contact.zaloId, contact.zaloPayId];
//}
//
//- (RACSignal *)requestUserProfile:(NSString *)userId {
//    if (userId.length == 0) {
//        return [RACSignal error:[NSError errorWithDomain:@"UserId Nil" code:-1 userInfo:nil]];
//    }
//    return [[[NetworkManager sharedInstance] getListZaloPayId:@[userId]] map:^id(NSDictionary *result) {
//        NSArray *userslist = [result arrayForKey:@"userslist"];
//        if (userslist.count == 0) {
//            return nil;
//        }
//        NSDictionary *oneDic = userslist.lastObject;
//        NSString *zaloId = [oneDic stringForKey:@"zaloid"];
//        NSString *zaloPayId = [oneDic stringForKey:@"userid"];
//        UserStatus status = [oneDic intForKey:@"status"];
//        if ([zaloId isEqualToString:userId] == false || status != 1 || zaloPayId.length == 0) {
//            DDLogInfo(@"invalid user");
//            return nil;
//        }
//
//        NSString *phone = [[[oneDic numericForKey:@"phonenumber"] stringValue] normalizePhoneNumber];
//        NSString *zaloPayName = [oneDic stringForKey:@"zalopayname"];
//        NSString *avatar = [oneDic stringForKey:@"avatar"];
//        NSString *displayName = [oneDic stringForKey:@"displayname"];
//        [self.zaloFriendTable saveZalopayInfoWithUserId:zaloId
//                                          zalopayId:zaloPayId
//                                              phone:phone
//                                        zalopayname:zaloPayName
//                                             status:status];
//        ZaloUserSwift *_return = [[ZaloUserSwift alloc] init];
//        _return.zaloPayId = zaloPayId;
//        _return.userId = zaloId;
//        _return.displayName = displayName;
//        _return.avatar = avatar;
//        _return.phone = phone;
//        _return.zaloPayName = zaloPayName;
//        return _return;
//    }];
//}
//
//- (void)loadConfig {
////    self.enableMergeName = [ApplicationState getBoolConfigFromDic:@"zpc" andKey:@"enable_merge_contact_name" andDefault:false];
//
////    NSDictionary *json_phone_format = [ApplicationState getDictionaryConfigFromDic:@"general" andKey:@"phone_format" andDefault:[NSDictionary new]];
////    self.phonePattern = [json_phone_format arrayForKey:@"patterns" defaultValue:[NSArray new]];
//
//    NSInteger enable = [[ZPApplicationConfig getZPCAppConfig] getEnableMergeContactName];
//    self.enableMergeName = (BOOL) enable;
//
//    NSArray *patterns = [[[ZPApplicationConfig getGeneralConfig] getPhoneFormat] getPatterns];
//    self.phonePattern = (patterns != nil) ? patterns : [NSArray new];
//}
//
//- (ZaloUserSwift *)userFromDatabaseDic:(NSDictionary *)dic {
//    /*
//            @{@"zaloId": userId,
//             @"displayname": displayName,
//             @"avatar": avatar,
//             @"phoneNumber": phoneNumber,
//             @"zalopayId": zalopayId,
//             @"zalopayName": zalopayName,
//             };
//     */
//
//    NSString *userId = [dic stringForKey:@"zaloId"];
//    NSString *displayName = [dic stringForKey:@"displayname"];
//    NSString *avatar = [dic stringForKey:@"avatar"];
//    NSString *phone = [dic stringForKey:@"phoneNumber"];
//    NSString *zaloPayId = [dic stringForKey:@"zalopayId"];
//    NSString *zaloPayName = [dic stringForKey:@"zalopayName"];
//    NSString *contactname = [dic stringForKey:@"contactname"];
//
//    NSString *userDisplayName = self.enableMergeName && contactname.length > 0 ? contactname : displayName;
//
//    ZaloUserSwift *user = [[ZaloUserSwift alloc] init];
//    user.userId = userId;
//    user.displayName = userDisplayName;
//    user.avatar = avatar;
//    user.usedZaloPay = zaloPayId.length > 0;
//    user.phone = phone;
//    user.zaloPayId = zaloPayId;
//    user.zaloPayName = zaloPayName;
//    return user;
//}
//
//- (ZPContactSwift *)userFromZPCDatabaseDic:(NSDictionary *)dic {
//    NSString *zaloId = [dic stringForKey:@"zaloId"];
//    NSString *userdisplayNameZLF = [dic stringForKey:@"displayNameZLF"];
//    NSString *userdisplayNameUCB = [dic stringForKey:@"displayNameUCB"];
//    NSString *avatar = [dic stringForKey:@"avatar"];
//    NSString *phone = [dic stringForKey:@"phoneNumber"];
//    NSString *zaloPayId = [dic stringForKey:@"zalopayId"];
//    NSString *zaloPayName = [dic stringForKey:@"zalopayName"];
//    NSString *firstName = [dic stringForKey:@"firstName"];
//    NSString *lastName = [dic stringForKey:@"lastName"];
//    UserStatus status = [dic intForKey:@"status" defaultValue:UserStatus_Available];
//    //NSString *contactname = [dic stringForKey:@"contactname"];
//
//    //NSString *userDisplayName = self.enableMergeName && contactname.length > 0 ? contactname : userdisplayNameZLF;
//    ZPContact *user = [ZPContact new];
//    user.zaloId = zaloId;
//    user.displayNameUCB = userdisplayNameUCB;
//    user.displayNameZLF = userdisplayNameZLF;
//    user.avatar = avatar;
//    user.usedZaloPay = zaloPayId.length > 0;
//    user.phoneNumber = phone;
//    user.zaloPayId = zaloPayId;
//    user.zaloPayName = zaloPayName;
//    user.zpContactID = [self createZPContactId:user];
//    user.firstName = firstName;
//    user.lastName = lastName;
//    user.status = status;
//
//    NSString *displayNameFinal = userdisplayNameUCB.length > 0 ? userdisplayNameUCB : userdisplayNameZLF;
//    user.displayName = displayNameFinal;
//    user.asciiDisplayName = [[displayNameFinal ascciString] uppercaseString];
//    if (user.asciiDisplayName.length > 0) {
//        user.firstCharacterInDisplayName = [user.asciiDisplayName substringToIndex:1];
//    }
//    //    [user setFirstCharacterInDisplayName];
//    user.isSavePhoneNumber = YES;
//    return user;
//}
//
//- (BOOL)shouldLoadZaloPayFromPhone:(BOOL)forceSync {
//    if (forceSync == false) {
//        uint64_t current = [[NSDate date] timeIntervalSince1970];
//        uint64_t last = [[self.cacheDataTable cacheDataValueForKey:kLastTimeLoadZaloPayInfoFromPhome] longLongValue];
//        forceSync = current - last > kUpdateInterval;
//    }
//    if (forceSync) {
//        uint64_t current = [[NSDate date] timeIntervalSince1970];
//        [self.cacheDataTable cacheDataUpdateValue:[@(current) stringValue] forKey:kLastTimeLoadZaloPayInfoFromPhome];
//    }
//    return forceSync;
//}
//
//@end
