//
//  ZPProfileNetwork.swift
//  ZaloPayProfile
//
//  Created by Bon Bon on 6/14/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import Foundation
import ReactiveObjC

public protocol ZPProfileNetworkProtocol {
    func getCurrenUserProfile() -> RACSignal<AnyObject>
    func getListZaloPayId(_ uids: Array<String>) -> RACSignal<AnyObject>
    func getListZaloPayInfo(fromPhones: Array<String>) -> RACSignal<AnyObject>    
}
