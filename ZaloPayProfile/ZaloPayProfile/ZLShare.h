//
//  ZLShare.h
//  ZaloPay
//
//  Created by Nguyễn Minh Trí on 4/13/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ZaloSDK/ZaloSDK.h>

typedef void(^ResponseSDK)(ZOShareResponseObject * response);
void ZLShareFeed(ZOFeed *feed, UIViewController *controller,ResponseSDK callback);
