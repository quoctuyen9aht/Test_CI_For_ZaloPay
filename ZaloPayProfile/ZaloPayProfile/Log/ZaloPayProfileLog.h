//
//  ZaloPayProfileLog.h
//  ZaloPayProfile
//
//  Created by Duy Dao Pham Hoang on 8/24/17.
//  Copyright © 2017 VNG. All rights reserved.
//

#import <CocoaLumberjack/CocoaLumberjack.h>

extern const DDLogLevel zaloPayProfileLogLevel;
#define ddLogLevel  zaloPayProfileLogLevel
