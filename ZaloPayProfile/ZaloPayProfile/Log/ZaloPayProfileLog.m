//
//  ZaloPayProfileLog.m
//  ZaloPayProfile
//
//  Created by Duy Dao Pham Hoang on 8/24/17.
//  Copyright © 2017 VNG. All rights reserved.
//

#import "ZaloPayProfileLog.h"

const DDLogLevel zaloPayProfileLogLevel = DDLogLevelInfo;
