//
//  ZPContactService.swift
//  ZaloPayProfile
//
//  Created by CPU11680 on 3/26/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import Foundation
import ZaloPayCommonSwift
import RxSwift
import RxCocoa
import ZaloPayProfilePrivate
import AddressBook

// 3 days between syncs
let kSyncContactIntervalTime = 3*24*60*60
let kPhoneNumber = "phoneNumber"
let kDisplayName = "displayName"
let kFirstName = "firstName"
let kLastName = "lastName"
let kMiddleName = "middleName"

@objcMembers
public class ZPContactService: NSObject {
    private static var cacheTable: ZPCacheDataTable = ZPCacheDataTable(db: PerUserDataBase.sharedInstance())
    private static var zaloFriendTable: ZPZaloFriendTable = ZPZaloFriendTable(db: PerUserDataBase.sharedInstance())
    
    @objc public class func syncAllContact(toDataBase forceSync: Bool) -> RACSignal<AnyObject>{
        if self.isEnableReadContact() == false {
            return RACSignal.empty()
        }
        if !forceSync {
            let strTime = cacheTable.cacheDataValue(forKey: kLastTimeSyncContact) ?? ""
            let lastTime: TimeInterval = TimeInterval(strTime) ?? 0
            let currentTime: TimeInterval = Date().timeIntervalSince1970
            if currentTime - lastTime <= Double(kSyncContactIntervalTime) {
                return RACSignal.empty()
            }
        }
        let time = String(Date().timeIntervalSince1970)
        cacheTable.cacheDataUpdateValue(time, forKey: kLastTimeSyncContact)
        return RACSignal.createSignal({ (s) -> RACDisposable? in
            DispatchQueue.global(qos: .background).async {
                self.retrieveContactsFromAddressBook()
                s.sendCompleted()
            }
            return nil
        }).replayLazily()
    }
    
    @objc public func askForContactPermission() -> RACSignal<AnyObject>{
        return RACSignal.createSignal({ (s) -> RACDisposable? in
            let authStatus = ABAddressBookGetAuthorizationStatus()
            let addressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
            if authStatus == .authorized {
                s.sendNext(true)
                s.sendCompleted()
                return nil
            }
            ABAddressBookRequestAccessWithCompletion(addressBook as ABAddressBook, { (granted, error) in
                s.sendNext(granted)
                s.sendCompleted()
            })
            return nil
        }).replayLazily()
    }
    
    public func shouldRequesContacPermission() -> Bool{
        let authStatus = ABAddressBookGetAuthorizationStatus()
        return authStatus == .notDetermined
    }
    
    public class func isEnableReadContact() -> Bool{
        let authStatus = ABAddressBookGetAuthorizationStatus()
        return authStatus == .authorized
    }
    
    public class func retrieveContactsFromAddressBook(){
        var error: Unmanaged<CFError>?
        guard let addressBook: ABAddressBook? = ABAddressBookCreateWithOptions(nil, &error)?.takeRetainedValue() else {
            return
        }
        let allContacts = ABAddressBookCopyArrayOfAllPeopleInSourceWithSortOrdering(addressBook, nil, ABPersonSortOrdering(kABPersonSortByLastName)).takeRetainedValue() as Array
        self.saveListData(listPerson: allContacts)
    }
    
    private class func saveListData(listPerson: Array<Any>){
        let listContact = NSMutableArray()
        for personTemp in listPerson {
            let person = personTemp as ABRecord
            let firstName = ABRecordCopyValue(person, kABPersonFirstNameProperty).takeRetainedValue() as? String ?? ""
            let lastName = ABRecordCopyValue(person, kABPersonLastNameProperty).takeRetainedValue() as? String ?? ""
            let middleName = ABRecordCopyValue(person, kABPersonMiddleNameProperty).takeRetainedValue() as? String ?? ""

            if firstName.count == 0 && lastName.count == 0 && middleName.count == 0 {
                continue
            }

            var fullName: String

            if firstName.count == 0 && middleName.count == 0 {
                fullName = lastName
            }else if lastName.count == 0 && middleName.count == 0 {
                fullName = firstName
            }else if firstName.count == 0 && lastName.count == 0 {
                fullName = middleName
            } else {
                let order = ABPersonGetCompositeNameFormat()
                if order == kABPersonCompositeNameFormatFirstNameFirst {
                    if middleName.count > 0 {
                        fullName = "\(firstName) \(middleName) \(lastName)"
                    }else {
                        fullName = "\(firstName) \(lastName)"
                    }
                }else{
                    if middleName.count > 0 {
                        fullName = "\(lastName) \(middleName) \(firstName)"
                    }else{
                        fullName = "\(lastName) \(firstName)"
                    }
                }
            }

            let multiPhones:ABMultiValue = ABRecordCopyValue(person, kABPersonPhoneProperty).takeRetainedValue()
            for i in 0..<ABMultiValueGetCount(multiPhones) {
                let phoneNumberRef = ABMultiValueCopyValueAtIndex(multiPhones, i)
                let phoneNumber = String(describing: phoneNumberRef)
                let digits = NSCharacterSet.decimalDigits
                let illegalCharacters = digits.inverted
                let components = phoneNumber.components(separatedBy: illegalCharacters)
                var phone = components.joined(separator: "")
                phone = phone.normalizePhoneNumber()
                if phone.count >= 10 && self.isPhone(phone: phone){
                    let param = NSMutableDictionary()
                    param[kPhoneNumber] = phone
                    param[kDisplayName] = fullName
                    if firstName.count > 0 {
                        param[kFirstName] = firstName
                    }
                    if lastName.count > 0{
                        param[kLastName] = lastName
                    }
                    if middleName.count > 0{
                        param[kMiddleName] = middleName
                    }
                    listContact.add(param)
                }
            }
        }
        zaloFriendTable.saveListPhone(listContact)
    }

    public class func isPhone(phone: String) -> Bool{
        return ZPProfileManager.shareInstance.isPhoneNumber(phone)
    }

}





