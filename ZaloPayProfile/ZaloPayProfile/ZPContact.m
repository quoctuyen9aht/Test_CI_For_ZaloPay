//
//  ZPContact.m
//  ZaloPay
//
//  Created by Phuoc Huynh on 7/17/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPContact.h"
//#import <ZaloPayCommon/NSString+Asscci.h>
//#import <ZaloPayCommon/NSString+Decimal.h>
//#import <ZaloPayCommon/NSCollection+JSON.h>
//#import <ZaloPayCommon/R.h>
//#import <ZaloPayProfile/ZaloPayProfile-Swift.h>
//
//#define kDisplayName        @"displayname"
//#define kAvatar             @"avatar"
//#define kUserId             @"userid"
//#define kUserGender         @"userGender"
//#define kZaloPayId          @"zalopayid"
//#define kStatus             @"status"
//
//#define kPhoneNumber        @"phonenumber"
//#define kZaloId             @"zaloid"
//#define kZaloPayName        @"zalopayname"
//
//#define kZaloName           @"zaloname"
//#define kContactDisplayName @"contactdisplayname"
//
//
//@implementation ZPContact
//
//- (NSComparisonResult)compareByDisplayValue:(ZPContact *)other {
//    return [self.asciiDisplayName compare:other.asciiDisplayName];
//}
//
//- (BOOL)isEqual:(ZPContact *)object {
//    if (![object isKindOfClass:[ZPContact class]]) {
//        return NO;
//    }
//
//    return [self.zpContactID isEqualToString:object.zpContactID];
//}
//
//// parse json from server when search user out ZPC
//+ (instancetype)parseUserFromDictionary:(NSDictionary *)dictionary withZalopayId:(NSString *)zalopayId {
//    ZPContact *zpContact = [[ZPContact alloc] init];
//    zpContact.avatar = [dictionary stringForKey:kAvatar defaultValue:@""];
//    zpContact.displayName = [dictionary stringForKey:kDisplayName defaultValue:@""];
//    zpContact.displayNameZLF = [dictionary stringForKey:kDisplayName defaultValue:@""];
//    zpContact.displayNameUCB = zpContact.displayNameZLF;
//    zpContact.phoneNumber = [[dictionary stringForKey:kPhoneNumber defaultValue:@""] normalizePhoneNumber];
//    zpContact.zaloId = [dictionary stringForKey:kZaloId defaultValue:@""];
//    zpContact.zaloPayId = zalopayId;
//    zpContact.gender = [dictionary intForKey:kUserGender];
//    zpContact.usedZaloPay = zpContact.zaloPayId.length > 0;
//    zpContact.zaloPayName = [dictionary stringForKey:kZaloPayName defaultValue:@""];
//    zpContact.status = [dictionary intForKey:kStatus defaultValue:UserStatus_Available];
//    zpContact.isSavePhoneNumber = YES;
//    zpContact.zpContactID = [zpContact createZaloPayContactId];
//    zpContact.asciiDisplayName = [[zpContact.displayName ascciString] uppercaseString];
//    if (zpContact.asciiDisplayName.length > 0) {
//        zpContact.firstCharacterInDisplayName = [zpContact.asciiDisplayName substringToIndex:1];
//    }
//    return zpContact;
//}
//
//+ (instancetype)parseUserFromReactDictionary:(NSDictionary *)dictionary withZalopayId:(NSString *)zalopayId {
//    ZPContact *zpContact = [[ZPContact alloc] init];
//    zpContact.avatar = [dictionary stringForKey:kAvatar defaultValue:@""];
//    zpContact.phoneNumber = [[dictionary stringForKey:kPhoneNumber defaultValue:@""] normalizePhoneNumber];
//    zpContact.zaloPayId = zalopayId;
//    zpContact.displayName = [dictionary stringForKey:kDisplayName defaultValue:@""];
//    zpContact.displayNameZLF = [dictionary stringForKey:kZaloName defaultValue:@""];
//    zpContact.displayNameUCB = [dictionary stringForKey:kContactDisplayName defaultValue:@""];
//    zpContact.zaloId = [dictionary stringForKey:kZaloId defaultValue:@""];
//
//    zpContact.gender = [dictionary intForKey:kUserGender];
//    zpContact.usedZaloPay = zpContact.zaloPayId.length > 0;
//    zpContact.zaloPayName = [dictionary stringForKey:kZaloPayName defaultValue:@""];
//    zpContact.status = [dictionary intForKey:kStatus defaultValue:UserStatus_Available];
//    zpContact.isSavePhoneNumber = YES;
//    zpContact.zpContactID = [zpContact createZaloPayContactId];
//    zpContact.asciiDisplayName = [[zpContact.displayName ascciString] uppercaseString];
//    if (zpContact.asciiDisplayName.length > 0) {
//        zpContact.firstCharacterInDisplayName = [zpContact.asciiDisplayName substringToIndex:1];
//    }
//    return zpContact;
//}
//
//
//+ (instancetype)userFromServerDictionary:(NSDictionary *)dictionary {
//    /*
//    Cần gán displayNameUCB = displayNameZLF
//    Case: vào chuyển tiền -> yêu thích bạn bè zalo không có trong danh bạ -> khi vào nạp tiền điện thoại không được chọn user này
//    Khi Nạp Tiền Điện Thoại -> search ra thông tin -> đây là bạn zalo không có trong danh bạ.
//    Search số điện thoại -> cho user đi tiếp.
//    */
//    NSString *zalopayId = [dictionary stringForKey:kUserId defaultValue:@""];
//    if (zalopayId.length == 0) {
//        return nil;
//    }
//    return [self parseUserFromDictionary:dictionary withZalopayId:zalopayId];
//}
//
//+ (instancetype)createContactFromReactDictionary:(NSDictionary *)dictionary {
//    NSString *zalopayId = [dictionary stringForKey:kZaloPayId defaultValue:@""];
//    return [self parseUserFromReactDictionary:dictionary withZalopayId:zalopayId];
//}
//
//+ (instancetype)fromPhone:(NSString *)phone {
//    ZPContact *item = [[ZPContact alloc] init];
//    item.phoneNumber = phone;
//    item.zpContactID = [NSString stringWithFormat:@"zpcontactid_%@", phone];
//    item.displayNameUCB = [R string_ZPC_Unsave_Phone_Number];
//    item.displayNameZLF = @"";
//    item.displayName = [R string_ZPC_Unsave_Phone_Number];
//    item.avatar = @"";
//    item.zaloId = @"";
//    item.zaloPayName = @"";
//    item.zaloPayId = @"";
//    item.status = UserStatus_Available;
//    item.isSavePhoneNumber = NO;
//    return item;
//}
//
//- (NSString *)createZaloPayContactId {
//    NSString *phone = self.phoneNumber.length > 0 ? self.phoneNumber : @"";
//    NSString *zaloId = self.zaloId.length > 0 ? self.zaloId : @"";
//    NSString *zaloPayId = self.zaloPayId.length > 0 ? self.zaloPayId : @"";
//    return [NSString stringWithFormat:@"%@%@%@", phone, zaloId, zaloPayId];
//}
//@end
