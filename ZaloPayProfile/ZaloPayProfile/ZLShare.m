//
//  ZLShare.m
//  ZaloPay
//
//  Created by Nguyễn Minh Trí on 4/13/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import "ZLShare.h"

void ZLShareFeed(ZOFeed *feed, UIViewController *controller,ResponseSDK callback){
    [[ZaloSDK sharedInstance] shareFeed:feed inController:controller callback:callback];
}
