//
//  ZPProfileManager.swift
//  ZaloPayProfile
//
//  Created by vuongvv on 4/10/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import Foundation
import ZaloPayProfilePrivate
import ZaloPayCommonSwift
import CocoaLumberjackSwift
import CocoaLumberjack
//import ZaloPayModuleProtocols

@objc public protocol ZPProfileManagerSwiftProtocol {
    @objc optional func loadZaloUserProfile() -> RACSignal<AnyObject>
    @objc optional func loadZaloUserProfile(usingCache: Bool) -> RACSignal<AnyObject>
    @objc optional func loadAllZaloPayId() -> RACSignal<AnyObject>
    @objc optional func loadZaloUserFriendList(forceSyncContact: Bool) -> RACSignal<AnyObject>
    @objc optional func isDoneLoadContact() -> Bool
    @objc optional func getZaloUserProfile(zaloId: String) -> ZaloUserSwift
    @objc optional func getZaloFriendListFromDB() -> [ZaloUserSwift]
    @objc optional func lixiZaloFriendInfo(withIds ids: [String]) -> [[String : Any]]
    @objc optional func requestUserProfile(userId: String) -> RACSignal<AnyObject>
    @objc optional func loadConfig()
    @objc optional func getContactInfo(byPhone phone: String) -> ZPContactSwift
    @objc optional func getZaloPayContactList(fromDB kindShowContactList: UInt32) -> [ZPContactSwift]
    @objc optional func getZaloPayContactListFavorite(fromDB contactMode: UInt32) -> [ZPContactSwift]
    @objc optional func getUnsavePhoneContactFromDB(withPhoneStringInput phoneStringInput: String) -> ZPContactSwift
    @objc optional func getListContactInfo(byPhone phones: [String]) -> [ZPContactSwift]
}

public enum ZPProfileLevel : Int32 {
    case level2         = 2
    case level3         = 3
}

@objcMembers
public class ZPProfileManager : NSObject {
    public var network: ZPProfileNetworkProtocol!
    public let zaloFriendTable: NSObject = ZPZaloFriendTable(db: PerUserDataBase.sharedInstance())
    private let profileTable: ZPProfileTable! = ZPProfileTable(db: PerUserDataBase.sharedInstance())
    
    public var email: String? {
        get {
            return self.profileTable.getProfileValue(forKey: kEmail)
        }
        set {
            self.profileTable.updateProfileData(newValue, forKey: kEmail)
        }
    }
    
    // CMND
    public var identifier: String? {
        get {
            return self.profileTable.getProfileValue(forKey: kIdentifier)
        }
        set {
            self.profileTable.updateProfileData(newValue, forKey: kIdentifier)
        }
    }
    
    // 3 type of identity: identity (CMND), passport and CC.
    public var passport: String?
    public var cc: String?
    // KYC infos
    public var fullName: String?
    public var dob: Date?
    public var gender: Gender?
    
    
    public var address: String? {
        get {
            return self.profileTable.getProfileValue(forKey: kAddress)
        }
        set {
            self.profileTable.updateProfileData(newValue, forKey: kAddress)
        }
    }
    
    public var userLoginData: ZPUserLoginDataSwift?
    
    public var currentZaloUser: ZaloUserSwift?
    
    public var enableMergeName: Bool?
    
    public var phonePattern: [String]?
    
    public static let shareInstance = ZPProfileManager()
    
    public override init() {
        super.init()
    }
    
    public func saveUserLoginData() {
        guard let userLoginDataGet = self.userLoginData else {
            _ = KeyChainStore.remove(forKey: kZPUserLoginDataKey, service: nil, accessGroup: nil)
            return
        }
        _ = KeyChainStore.set(string: userLoginDataGet.jsonString(), forKey: kZPUserLoginDataKey, service: nil, accessGroup: nil)
    }
    
    public func loginDataFromCache() -> ZPUserLoginDataSwift? {
        guard let cache = KeyChainStore.string(forKey: kZPUserLoginDataKey, service: nil, accessGroup: nil),
              let json = cache.jsonValue() as? [String: Any] else {
            return nil
        }
        return ZPUserLoginDataSwift.fromDic(json)
    }
    
    public func logout() {
        self.userLoginData = nil
        self.currentZaloUser = nil
        self.saveUserLoginData()
    }
    
    public func isPhoneNumber(_ phone: String) -> Bool {
        guard let phonePatternGet = self.phonePattern else {
            return false
        }
        for pattern in phonePatternGet  {
            let phoneTest:NSPredicate = NSPredicate(format: "SELF MATCHES %@", pattern)
            if phoneTest.evaluate(with: phone)  {
                return true
            }
        }
        return false
    }
    
    public func dbGetCurrentUserInfo() -> ZaloUserSwift? {
        return profileTable.getCurrentUserInfo() as? ZaloUserSwift
    }
    
    public func dbSaveCurrentUserInfo(_ user: ZaloUserSwift) {
        profileTable.saveCurrentUserInfo(user)
    }
}

