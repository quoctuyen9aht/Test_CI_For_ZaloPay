//
//  ZPUserLoginDataSwift.swift
//  ZaloPayProfile
//
//  Created by Phuoc Huynh on 3/14/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import UIKit
import Foundation
import ZaloPayProfilePrivate
import ZaloPayCommonSwift
struct Constants {
    public static let kPhoneNumber = "phonenumber"
    public static let kAccessToken = "accesstoken"
    public static let kExpireIn = "expirein"
    public static let kUserid = "userid"
    public static let kProfileLevel = "profilelevel"
    public static let kProfileLevelPermisssion = "profilelevelpermisssion"
    public static let kZaloPayName = "zalopayname"
    public static let kHourSessionTime = "hoursessiontime"
}
@objcMembers
public class ZPUserLoginDataSwift: NSObject {
    public var accesstoken:String = ""
    public var expirein:Date?
    public var paymentUserId:String = ""
    public var profilelevel:Int = 0
    public var accountName:String = ""
    public var profilelevelpermisssion = Array<Any>()
    public var phoneNumber:String = ""
    public var hourSessionTime:Int = 0
    public var isClearing:Bool = false
    public var isFrom3DTouch:Bool = false
    
    public class func fromDic(_ dict:[AnyHashable:Any]) -> ZPUserLoginDataSwift? {
        if dict.count == 0 {
            return nil
        }
        let phone = dict.string(forKey: Constants.kPhoneNumber).normalizePhoneNumber() ?? ""
        let _return = ZPUserLoginDataSwift()
        _return.accesstoken = dict.string(forKey: Constants.kAccessToken)
        _return.expirein = Date(timeIntervalSince1970: TimeInterval(dict.uInt64(forKey: Constants.kExpireIn)/1000))
        _return.paymentUserId = dict.string(forKey: Constants.kUserid)
        _return.profilelevel = dict.int(forKey: Constants.kProfileLevel)
        _return.profilelevelpermisssion = (dict as NSDictionary).array(forKey: Constants.kProfileLevelPermisssion, defaultValue: Array<Any>())
        _return.phoneNumber = phone
        _return.accountName = dict.string(forKey: Constants.kZaloPayName)
        _return.hourSessionTime = dict.int(forKey: Constants.kHourSessionTime) > 0 ? dict.int(forKey: Constants.kHourSessionTime) : 24
        _return.isClearing = false
        return _return
    }

    public func jsonString() -> String {
        var json = [String:Any]()
        let expirein = self.expirein?.timeIntervalSince1970 ?? 0
        json[Constants.kPhoneNumber] = self.phoneNumber
        json[Constants.kAccessToken] = self.accesstoken
        json[Constants.kExpireIn] = expirein * 1000
        json[Constants.kUserid] = self.paymentUserId
        json[Constants.kProfileLevel] = self.profilelevel
        json[Constants.kProfileLevelPermisssion] = self.profilelevelpermisssion
        json[Constants.kZaloPayName] = self.accountName
        return (json as NSDictionary).jsonRepresentation()
        
    }
    
    public func isExpire() -> Bool {
        if self.expirein != nil {
            return self.expirein!.timeIntervalSinceNow <= 0
        }
        return false
    }
    
}
