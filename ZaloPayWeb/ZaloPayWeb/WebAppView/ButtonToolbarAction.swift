//
//  ButtonToolbarAction.swift
//  ZaloPay
//
//  Created by Duy Dao Pham Hoang on 8/30/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

public class ButtonToolbarAction: UIButton {
    public var iconId:String = ""
    public var api:ZPWWebAppApi?
}
