//
//  ZPWProgressView.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/20/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit

public class ZPWProgressView: UIView {
    
    public var progress: Float = 0
    public var progressBarColor = UIColor(red: 22.0/255, green: 126.0/255, blue: 251.0/255, alpha: 1)
    public var barView:UIView?
    public var barAnimationDuration: TimeInterval = 0.3
    public var fadeAnimationDuration: TimeInterval = 0.3
    public var fadeOutDelay: TimeInterval = 0.3
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.configureViews()
    }
    
    public init(frame: CGRect, color:UIColor) {
        super.init(frame: frame)
        progressBarColor = color
        self.configureViews()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        
        self.configureViews()
    }
    
    public func configureViews() {
        self.isUserInteractionEnabled = false
        self.autoresizingMask = UIViewAutoresizing.flexibleWidth
        addBarView()
    }
    
    public func addBarView()
    {
        if barView == nil
        {
            barView = UIView(frame: CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: 0, height: self.frame.size.height))
            barView?.autoresizingMask = [.flexibleHeight, .flexibleHeight]
            barView?.backgroundColor = progressBarColor
            self.addSubview(barView!)
        }
    }

    public func setProgress(_ progress: Float, animated: Bool = true) {
        self.isHidden = false
        let isGrowing = progress > 0
        let animationDuration = isGrowing && animated ? barAnimationDuration : 0.0
        
        UIView.animate(withDuration: animationDuration, delay: 0, options: .curveEaseInOut, animations: { 
            var frame = self.barView?.frame
            frame?.size.width = CGFloat(progress) * self.bounds.width
            self.barView?.frame = frame!
        }, completion: nil)
        
        let animationDurationComplete = animated ? fadeAnimationDuration : 0
        if progress >= 1 {
            UIView.animate(withDuration: animationDurationComplete, delay: 0.0, options: .curveEaseInOut, animations: {
                
                self.barView?.alpha = 0.8
            }, completion: {
                completed in
                
                self.isHidden = true
                var frame = self.barView?.frame
                frame?.size.width = 0
                self.barView?.frame = frame!
            })
        } else {
            
            UIView.animate(withDuration: animationDurationComplete, delay: 0, options: .curveEaseInOut, animations: { 
                
                self.barView?.alpha = 1.0
            }, completion: nil)
        }
    }
}
