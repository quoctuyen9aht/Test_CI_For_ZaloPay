//
//  ZPWWebView.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/17/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import WebKit
import RxSwift
import RxCocoa
import CocoaLumberjackSwift
import ZaloPayCommonSwift

@objc protocol ZPWWebViewDelegate: class {
    
    @objc optional func closeWebview()
    
    @objc optional func webView(_ webView: ZPWWebView, didFinishNavigation dummy: AnyObject?)
    
    @objc optional func webView(_ webView: ZPWWebView, didFinishNavigationWithError error: Error?)
    
    @objc optional func webView(_ webView: ZPWWebView, didUpdateProgress progress: CGFloat)
    
    @objc optional func handleUrl(_ url: URL, navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void)
    
}

@objc class ZPWWebView: NSObject {
    
    weak var viewController: UIViewController?
    weak var navigationDelegate: ZPWWebViewDelegate?
    weak var webHandle: ZPWebHelperProtocol?
    @objc dynamic var title: String!
    var webView: WKWebView!
    var isUsePushView: Bool = false
    var currentRequest: URLRequest!
    
    var view: UIView {
        get {
            return webView
        }
    }
    
    init(controller: UIViewController, webHandle: ZPWebHelperProtocol?) {
        super.init()
        
        self.viewController = controller
        self.webView = WKWebView()
        self.setupConfigure()
        self.webHandle = webHandle
    }
    
    convenience init(controller: UIViewController, configure: WKWebViewConfiguration, webHandle: ZPWebHelperProtocol?) {
        self.init(controller: controller, webHandle: webHandle)
        self.webView = WKWebView(frame: .zero, configuration: configure)
        self.setupConfigure()
    }
    
    convenience init(controller: UIViewController, configure: WKWebViewConfiguration, usePushView:Bool, webHandle: ZPWebHelperProtocol?) {
        self.init(controller: controller, webHandle: webHandle)
        
        self.webView = WKWebView(frame: .zero, configuration: configure)
        isUsePushView = usePushView
        self.setupConfigure()
    }
    
    deinit {
        self.webView.navigationDelegate = nil
        self.webView = nil
    }
    
    // MARK: Private's method
    private func setupConfigure() {
        
        self.webView.navigationDelegate = self
        self.monitorProgress()
        
        _ = self.webView.rx.observeWeakly(String.self, "title").subscribe(onNext: { [weak self] (wkWebTitle) in
            if let wkWebTitle = wkWebTitle,
                wkWebTitle.count > 0 {
                DispatchQueue.main.async {
                    self?.title = wkWebTitle
                }
            }
        })
    }
    
    private func monitorProgress() {
        
        _ = self.webView.rx.observe(Double.self, "estimatedProgress")
            .takeUntil(self.rx.deallocated)
            .subscribe(onNext: {
                [weak self] (progress) in
                
                if let progress = progress, self != nil {
                    self?.navigationDelegate?.webView?(self!, didUpdateProgress: CGFloat(NSNumber(value: progress).floatValue))
                }
            })
    }
    
    // MARK: Public's method
    
    func loadRequest(_ url: URLRequest) {
        //backup
        currentRequest = url
        self.webView.load(url)
    }
    
    func reload(_ defaultUrlString: String? = nil) {
        self.webView.stopLoading()
        if self.webView.url != nil {
            self.webView.reload()
            return
        }
        if let urlString = defaultUrlString,
            let url = URL(string: urlString) {
            let request = URLRequest(url: url)
            self.loadRequest(request)
            return
        }
        self.loadRequest(currentRequest)
    }
    
    func reload() {
        self.webView.stopLoading()
        guard self.webView.url != nil else {
            self.loadRequest(currentRequest)
            return
        }
        
        self.webView.reload()
    }
    
    func setUserAgent(_ userAgent: String) {
        if #available(iOS 9, *) {
            self.webView.customUserAgent = userAgent
        }
    }
    
    
    func goBack(homePageURLString: String, errorHandler: (() -> Void)) {
        guard self.webView.goBack() == nil else {
            return
        }
        let backList = self.webView.backForwardList.backList
        guard let rootItem = backList.first else {
            errorHandler()
            return
        }
        guard self.webView.go(to: rootItem) != nil else {
            errorHandler()
            return
        }
    }
    
    func evaluateJavaScript(_ javaScriptString: String, completionHandler: ((Any?, Error?) -> Swift.Void)? = nil) {
        
        self.webView.evaluateJavaScript(javaScriptString, completionHandler: completionHandler)
    }
}

extension ZPWWebView: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        let request = navigationAction.request
        
        guard let url = request.url, let schemeURL = url.scheme else {
            decisionHandler(.allow)
            return
        }
        
        if isUsePushView && schemeURL.contains("http") {
            if self.navigationDelegate?.handleUrl?(url, navigationAction: navigationAction, decisionHandler: decisionHandler) == nil {
                decisionHandler(.allow)
            }
            return
        }
        guard let handle = webHandle else {
            return
        }
        
        if handle.handleUrlSchemes(schemesUrl: url, sourceApplication: "") {
            decisionHandler(.cancel)
            return
        } else if url.isNeedHandleScheme() {
            decisionHandler(.cancel)
            UIApplication.shared.openURL(url)
            self.navigationDelegate?.closeWebview?()
            return
        }
        decisionHandler(.allow)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        DDLogInfo("didStartProvisionalNavigation");
    }
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        DDLogInfo("didReceiveServerRedirectForProvisionalNavigation");
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        DDLogInfo("didFailProvisionalNavigation");
        if (error as NSError).code != NSURLErrorCancelled {
            self.navigationDelegate?.webView?(self, didFinishNavigationWithError: error)
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        DDLogInfo("didFinish");
            self.navigationDelegate?.webView?(self, didFinishNavigation: navigation)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        DDLogInfo("didFinish");
        if (error as NSError).code != NSURLErrorCancelled {
            self.navigationDelegate?.webView?(self, didFinishNavigationWithError: error)
        }
    }
    
    func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
           DDLogInfo("webViewWebContentProcessDidTerminate");
    }
}

