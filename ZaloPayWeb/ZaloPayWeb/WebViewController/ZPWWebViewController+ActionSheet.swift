//
//  ZPWWebViewController+ActionSheet.swift
//  ZaloPay
//
//  Created by Bon Bon on 12/5/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayAnalyticsSwift
import ZaloPayWebObjc
import ZaloPayShareControl
import ZaloPayProfile

extension ZPWWebViewController {
    
    func createShareButton(_ color: UIColor = .white, _ shareIcon: String = "share_ios") ->UIBarButtonItem {
        let button = UIButton(type: .custom).build {
            $0.titleLabel?.textColor = color
            $0.setTitleColor(color, for: .normal)
            $0.titleLabel?.font = UIFont.iconFont(withSize: 22)
            $0.setIconFont(shareIcon, for: .normal)
            $0.addTarget(self, action: #selector(showActionSheet), for: .touchUpInside)
            $0.frame = CGRect(x: 0, y: 0, width: 22, height: 22)
        }
        
        if #available(iOS 11, *) {
            button.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -12);
        }
        return UIBarButtonItem(customView: button)
    }
    
    func creatRightBarButtonItems(_ color: UIColor = .white, _ shareIcon: String = "share_ios") -> [UIBarButtonItem] {
        let negativeSeparator = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSeparator.width = -12
        let share = createShareButton(color, shareIcon)
        return [negativeSeparator, share]
    }
    
    @objc func showActionSheet() {
        ZPTrackingHelper.shared().trackEvent(.promotion_detail_touch_share)
        let zpSheetConfigure = ZPShareSheetConfigure.default()
        let domainString = ZPWUtils.getRootDomain(self.urlString)
        zpSheetConfigure.titleHeader = String(format: R.string_WebApp_BottomSheet_SupportFor(), domainString)
        
        let zpActionSheetController = ZPShareSheetController(configure: zpSheetConfigure)
        zpActionSheetController.delegate = self
        
        let items = [
            [
                ZPItemShareModel(title: R.string_WebApp_BottomSheet_ShareTo_Zalo(),
                                 iconImage: UIImage(named: "ico_sharezalo"),
                                 tapAction: {
                                    item in
                                    ZPTrackingHelper.shared().trackEvent(.promotion_detail_share_zalo)
                                    zpActionSheetController.dismissViewController()
                                    _ = ZaloSDKApiWrapper.sharedInstance.shareLinkZalo(self.urlString, andParentVC: self).subscribe(onNext: { (_) in
                                    })
                })
            ],
            [
                ZPItemShareModel(title: R.string_WebApp_BottomSheet_Copy_URL(),
                                 iconString: "webapp_copyurl",
                                 tapAction: {
                                    item in
                                    
                                    ZPTrackingHelper.shared().trackEvent(.promotion_detail_share_urlcopy)
                                    UIPasteboard.general.string = self.urlString
                                    zpActionSheetController.dismissViewController()
                }),
                ZPItemShareModel(title: R.string_WebApp_BottomSheet_Refresh(),
                                 iconString: "webapp_refresh",
                                 tapAction: {
                                    item in
                                    
                                    ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.promotion_detail_share_refresh)
                                    self.refreshButtonClick()
                                    zpActionSheetController.dismissViewController()
                }),
                ZPItemShareModel(title: R.string_WebApp_BottomSheet_Open_Browser(),
                                 iconString: "webapp_browser",
                                 tapAction: {
                                    item in
                                    
                                    ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.promotion_detail_share_tobrowser)
                                    if  let openURL = URL(string: self.urlString),
                                        UIApplication.shared.canOpenURL(openURL)
                                    {
                                        UIApplication.shared.openURL(openURL)
                                    }
                                    
                                    zpActionSheetController.dismissViewController()
                })
            ]
        ]
        
        zpActionSheetController.items = items
        let window = UIApplication.shared.delegate?.window
        window??.rootViewController?.present(zpActionSheetController, animated: true, completion: nil)
    }
    
    func createBackButton(_ color: UIColor = .white) ->UIBarButtonItem {
        let backBtn = UIButton(frame: CGRect(origin: .zero, size: CGSize(width: 40, height: 40)))
        backBtn.titleLabel?.textColor = color
        backBtn.setTitleColor(color, for: .normal)
        backBtn.setIconFont("general_backios", for: .normal)
        backBtn.setTitle("", for: .disabled)
        backBtn.titleLabel?.font = UIFont.iconFont(withSize: 20)
        backBtn.addTarget(self, action: #selector(processBackBtn(sender:)), for: .touchUpInside)
        if #available(iOS 11, *) {
            backBtn.contentEdgeInsets = UIEdgeInsetsMake(0, -12, 0, 0);
        }
        return UIBarButtonItem(customView: backBtn)
    }
    
    func createCloseButton(_ color: UIColor = .white) -> UIBarButtonItem {
        let cancelBtn = UIButton(frame: CGRect(origin: .zero, size: CGSize(width: 35, height: 20)))
        cancelBtn.titleLabel?.textColor = color
        cancelBtn.setTitleColor(color, for: .normal)
        cancelBtn.setIconFont("red_delete", for: .normal)
        cancelBtn.setTitle("", for: .disabled)
        cancelBtn.titleLabel?.font = UIFont.iconFont(withSize: 20)
        cancelBtn.addTarget(self, action: #selector(processCancelButton(sender:)), for: .touchUpInside)
        let cancelBarBtn = UIBarButtonItem(customView: cancelBtn)
        if #available(iOS 11, *) {
            cancelBtn.contentEdgeInsets = UIEdgeInsetsMake(0, -12, 0, 0);
        }
        return cancelBarBtn;
    }
    
    @objc func createLeftBarbuttonItems(_ color: UIColor = .white) -> [UIBarButtonItem] {
        let backBarBtn = createBackButton(color)
        let cancelBarBtn = createCloseButton(color)
        let negativeBarBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeBarBtn.width = -12
        return [negativeBarBtn, backBarBtn, cancelBarBtn]
    }
    
    @objc func processCancelButton(sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func processBackBtn(sender: UIButton) {
        
    }
}
