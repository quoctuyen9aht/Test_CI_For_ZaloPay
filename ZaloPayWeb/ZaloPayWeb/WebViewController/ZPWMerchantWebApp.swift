//
//  ZPWMerchantWebApp.swift
//  ZaloPay
//
//  Created by Bon Bon on 12/7/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

public class ZPWMerchantWebApp: ZPWInternalWebApp {
    override func isValidURLCheckJS() -> Bool {
        return true
    }
    
    override public func createWebView(url: String) -> ZPWInternalWebApp {
        let web =  ZPWMerchantWebApp(url: url, webHandle: webHandle)
        web.urlString = url;
        web.setUpWebView(true, showShareBtn:false)
        web.appId = self.appId;
        return web
    }
    
    override func webviewErrorTittle() -> String {
        return webHandle?.merchantName(appId: self.appId) ?? ""
    }
}
