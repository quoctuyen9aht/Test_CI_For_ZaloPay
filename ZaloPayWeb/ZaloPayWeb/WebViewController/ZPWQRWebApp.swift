//
//  ZPWWebAppViewController.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/20/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import WebKit
import RxSwift
import RxCocoa
import ZaloPayCommonSwift
import ZaloPayAnalyticsSwift
import ZaloPayWebObjc

@objcMembers
public class ZPWQRWebApp: ZPWWebViewController {
    public var webAppModel: ZPWWebAppFunctionWrapper!
    
    override public func initWebView() {
        let configuration = WKWebViewConfiguration()
        let controller = WKUserContentController()
        configuration.userContentController = controller
        
        self.webView = ZPWWebView(controller: self, configure: configuration, usePushView:true, webHandle: webHandle)
        let wkWebView: WKWebView = self.webView.webView
        if isValidURLCheckJS() {
            self.setupHandlerJs(wkWebView)
        }
        
        if #available(iOS 9.0, *) {
            wkWebView.customUserAgent = ZPWUtils.zpCustomUserAgent
        }
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
    }
    
    override public func screenName() -> String {
        return ZPTrackerScreen.Screen_iOS_Pay_ScanQR_WebView
    }
  
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        guard let navi = UINavigationController.currentActiveNavigationController() else  {
            return
        }
        guard let _ = navi.topViewController as? ZPWQRWebApp else {
            navi.defaultNavigationBarStyle()
            UIApplication.shared.statusBarStyle = .lightContent
            self.titleView.textColor = UIColor.white
            return
        }
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        UIApplication.shared.statusBarStyle = .default
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
        ZPTrackingHelper.shared().trackScreen(with: self)
    }
    
    private func setupNavigationBar() {
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        UIApplication.shared.statusBarStyle = .default
        self.navigationItem.leftBarButtonItems = createLeftBarbuttonItems(.black)
        self.navigationItem.rightBarButtonItems = creatRightBarButtonItems(.black,"webapp_3point_ios")
        self.titleView.textColor = UIColor.black
    }

    override func processBackBtn(sender: UIButton) {
        super.backButtonClicked(sender)
    }
            
    public func setupHandlerJs(_ webview: WKWebView) {
        webAppModel = ZPWWebAppFunctionWrapper(webHandle: self.webHandle)
        webAppModel.webAppViewController = self
        webAppModel.setupHandlerJs(webview)
    }
    
    override func doFactoryURL(url: URL, navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        self.urlString = url.absoluteString
        super.doFactoryURL(url: url, navigationAction: navigationAction, decisionHandler: decisionHandler)
    }
    
    override func doFactoryPushViewController(apo: ZPWWebAppApi, url: String) {
        let webApp = ZPWQRWebApp(url: url, webHandle: webHandle)
        webApp.urlString = url
        self.navigationController?.pushViewController(webApp, animated: true)
    }
}



