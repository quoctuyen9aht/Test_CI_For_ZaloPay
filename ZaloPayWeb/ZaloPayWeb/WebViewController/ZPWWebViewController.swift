//
//  ZPWWebViewController.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/17/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import Foundation
import RxSwift
import RxCocoa
import CocoaLumberjackSwift
import WebKit
import ZaloPayCommonSwift
import ZaloPayWebObjc
import ZaloPayShareControl
import ZaloPayUI

//@objcMembers
public class ZPWWebViewController: BaseViewcontrollerSwift {
    var webView: ZPWWebView!
    var urlString: String!
    var webHandle: ZPWebHelperProtocol?

    lazy var progressView: ZPWProgressView = {
        let progress = ZPWProgressView(frame: CGRect.zero, color: UIColor.zp_green())
        self.view.addSubview(progress)
        progress.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(2)
        }
        progress.isHidden = true 
        return progress
    }()
    
    open lazy var titleView: UILabel = {
        let label = UILabel(frame: .zero)
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textAlignment = .center
        return label
    }()
    
    lazy var placeholderView: ZPPlaceHolderView = {
        let holderView = ZPPlaceHolderView(frame: .zero)
        self.view.addSubview(holderView)
        holderView.snp.makeConstraints { (make) in
            make.leading.trailing.bottom.equalTo(0)
            make.top.equalTo(0)
        }
        holderView.isHidden = true
        holderView.buttonRetry.addTarget(self, action: #selector(refreshButtonClick), for: .touchUpInside)
        return holderView
    }()       
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    public convenience init(url: String, webHandle: ZPWebHelperProtocol?) {
        self.init(nibName: nil, bundle: nil)
        self.urlString = url
        self.webHandle = webHandle
    }
    
    public convenience init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, webHandle: ZPWebHelperProtocol?) {
        self.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.webHandle = webHandle
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func refreshButtonClick() {
        if !NetworkState.sharedInstance().isReachable {
            ZPDialogView.showDialog(with:DialogTypeNoInternet,
                                    title: R.string_Dialog_Warning(),
                                    message: R.string_NetworkError_NoConnectionMessage() ,
                                    buttonTitles: [R.string_ButtonLabel_Close()],
                                    handler: nil)
            return
        }
        ZPWUtils.cleanCacheWeb(false)
        self.placeholderView.isHidden = true
        self.webView.reload(self.urlString)
    }
    
    // MARK: Life cycle's
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.titleView = titleView
        self.initWebView()
        self.webView.webView.uiDelegate = self
        self.webView.navigationDelegate = self
        self.view.addSubview(self.webView.view)
        self.webView.view.snp.makeConstraints { (make) in
            make.leading.trailing.bottom.equalTo(0)
            make.top.equalTo(0)
        }
        
        _ = self.titleView.rx.observeWeakly(String.self, "text").subscribe(onNext: { [weak self](_) in
            self?.titleView.sizeToFit()
        })
        
        _ = self.webView.webView.rx.observeWeakly(String.self, "title").observeOn(MainScheduler.instance).map({ $0 ?? "ZaloPay"}).bind(to: self.titleView.rx.text)
        
        self.loadUrlString(self.urlString)
    }
    
    func isValidURLCheckJS() -> Bool{
        let patterns = self.webHandle?.patternURLs()
        return ZPWUtils.isAllowWebAppFunction(self.urlString, patterns: patterns)
    }
    
    // MARK: Private method's
    public func initWebView() {
        self.webView = ZPWWebView(controller: self, webHandle: self.webHandle)
    }
    
    public func loadUrlString(_ url: String) {
        if let urlRequest = URL(string: url) {
            _ = Observable.just("Đang tải...").bind(to: self.titleView.rx.text)
            self.placeholderView.isHidden = true
            let request = URLRequest(url: urlRequest)
//            request.cachePolicy = .reloadIgnoringLocalCacheData
            self.webView.loadRequest(request)
        }
    }
    
    func isWebViewNotAvailable(_ error: Error?) -> Bool {
        guard let error = error as NSError? else {
            return false
        }
        
        let info = error.userInfo as NSDictionary
        let sourceUrl = info.string(forKey: "WKJavaScriptExceptionSourceURL", defaultValue: "")
        return sourceUrl == "about:blank"
    }
    
    func isSameHostWithStartUrl(_ currentURL: String) -> Bool {        
        let currentHost = URL(string: currentURL)?.host
        let homePageHost = URL(string: self.urlString)?.host
        return currentHost == homePageHost
    }
    
    func doFactoryURL(url: URL, navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        decisionHandler(.allow)
        //Need inheritance
    }
    
    func doFactoryPushViewController(apo: ZPWWebAppApi, url: String) {
        //Need inheritance
    }
    
    override public func backButtonClicked(_ sender: Any) {
        if !NetworkState.sharedInstance().isReachable {
            super.backButtonClicked(sender)
            return
        }
        self.webView.goBack(homePageURLString: self.urlString, errorHandler: {[weak self] in
            self?.navigationController?.popViewController(animated: true)
        })
    }
}

extension ZPWWebViewController: ZPShareSheetControllerDelegate {
    public func processCloseShareSheet(){
        if let tabbar = UIApplication.shared.delegate?.window??.rootViewController as? UITabBarController {
            tabbar.tabBar.isHidden = true
        }
    }
}

extension ZPWWebViewController: ZPWWebViewDelegate {
    func closeWebview() {
        backButtonClicked(UIButton())
    }
    
    func handleUrl(_ url: URL, navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        self.doFactoryURL(url: url, navigationAction: navigationAction, decisionHandler: decisionHandler)
    }
    
    func webView(_ webView: ZPWWebView, didUpdateProgress progress: CGFloat) {
        self.progressView.isHidden = !(progress > 0)
        self.progressView.setProgress(Float(progress), animated: true)
    }
    
    func webView(_ webView: ZPWWebView, didFinishNavigation dummy:AnyObject?) {
        self.titleView.text = self.webView.title
        self.placeholderView.isHidden = true
    }
    
    func webView(_ webView: ZPWWebView, didFinishNavigationWithError error: Error?) {
        guard let error = error as NSError? else {
            return
        }
        self.placeholderView.isHidden = false
        self.titleView.text = self.webviewErrorTittle();
        error.isNetworkConnectionError() ? self.placeholderView.setupViewNoNetwork() : self.placeholderView.setupViewErrorLoad()
    }
    
    @objc func webviewErrorTittle() -> String {
        return "ZaloPay"
    }
}

extension ZPWWebViewController: WKUIDelegate {
    public func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        let alert = UIAlertController(title: R.string_Dialog_Title_Notification(), message: message, preferredStyle: .alert)
        let btnAction = UIAlertAction(title: R.string_ButtonLabel_OK(), style: .cancel) { (_) in
            completionHandler()
        }
        alert.addAction(btnAction)
        
        guard let root = UIApplication.shared.delegate?.window??.rootViewController, !(root.presentingViewController is UIAlertController) else {
            completionHandler()
            return
        }
        root.present(alert, animated: true, completion: nil)
    }
}
