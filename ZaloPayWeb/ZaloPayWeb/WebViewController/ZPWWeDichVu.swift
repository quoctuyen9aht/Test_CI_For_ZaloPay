
//
//  ZPWInternalWebController.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/18/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import CocoaLumberjackSwift
import WebKit
import ZaloPayAnalyticsSwift
import SDWebImage
import ZaloPayWebObjc
import ZaloPayCommonSwift

class ZPWWebDichVuWebView: ZPWWebView {
    
    
    // https://bug.zalopay.vn/browse/ZPF-1032 : hard code customUseragent for google sign in.
    
    override func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if #available(iOS 9.0, *) {
            let historyUrl  = webHandle?.webUrl(withAppId: appDichVuId) ?? ""
            let dichVuHost = URL.init(string: historyUrl)?.host ?? "zalopay.com.vn"
            let host = navigationAction.request.url?.host?.lowercased();
            let agent = (host?.contains(dichVuHost) ?? false) ? ZPWUtils.zpCustomUserAgent : ZPWUtils.zpDesktopUserAgent;
            webView.customUserAgent = agent
        }
        super.webView(webView, decidePolicyFor: navigationAction, decisionHandler: decisionHandler)
    }
}

@objcMembers
public class ZPWWeDichVu: ZPWWebViewController {
    
    var appId: Int?
    lazy var historyUrl: String = {
        guard let appId = self.appId else {
            return ""
        }
        //return ZPWebviewHelper.shared.appDependence?.historyUrl(withAppId: appId) ?? ""
        return webHandle?.historyUrl(withAppId: appId) ?? ""
    }()
    
    lazy var navigationItems : [UIBarButtonItem] = {
        let button = UIButton(type: .custom).build {
            $0.titleLabel?.textColor = .white
            $0.titleLabel?.font = UIFont.iconFont(withSize: 20)
            $0.setIconFont("general_history", for: .normal)
            
            $0.addTarget(self, action: #selector(historyButtonClick), for: .touchUpInside)
            $0.frame = CGRect(x: 0, y: -10, width: 30, height: 40)
        }
        
        let barButton = UIBarButtonItem(customView: button)
        let fixedSpace = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        
        fixedSpace.width = -10
        return [fixedSpace,barButton]
    }()
    
    public convenience init(_ url: String, appId: Int, webHandle: ZPWebHelperProtocol) {
        self.init(url: url, webHandle: webHandle)
        self.appId = appId
    }
    
    @objc override public func backButtonClicked(_ sender: Any) {
        if !NetworkState.sharedInstance().isReachable {
            self.navigationController?.popViewController(animated: true)
            return
        }
        self.webView.evaluateJavaScript("utils.back()") { [weak self](obj, error) in
            if error == nil {
                return
            }
            guard let strongSelf = self else {
                return
            }
            if strongSelf.isWebViewNotAvailable(error) {
                strongSelf.navigationController?.popViewController(animated: true)
                return
            }
            strongSelf.webView.goBack(homePageURLString: strongSelf.urlString, errorHandler: {[weak self] in
                self?.navigationController?.popViewController(animated: true)
            })            
        }
    }
    
//    override func backEventId() -> ZPAnalyticEventAction {
//        return ZPAnalyticEventAction.service_touch_back
//    }
    
    override public func initWebView() {
        self.webView = ZPWWebDichVuWebView(controller: self, webHandle: self.webHandle)
        self.webView.setUserAgent(ZPWUtils.zpCustomUserAgent)
        ZPTrackingHelper.shared().trackScreen(with: self)
    }
    
    @objc func historyButtonClick() {
        self.loadUrlString(self.historyUrl)
//        trackScreen(ZPTrackerScreen.Screen_Service_PaymentHistory)
    }
    
    override public func loadUrlString(_ url: String) {
        self.title = "Đang tải..."
        guard let url = URL(string: url) else {
            return
        }
        
        var request = URLRequest(url: url)
        let userAgent = ZPWUtils.zpCustomUserAgent
        request.addValue(userAgent, forHTTPHeaderField:"User-Agent")
        request.cachePolicy = .reloadIgnoringLocalCacheData
        self.webView.loadRequest(request)
        self.placeholderView.isHidden = true
    }
    
    override func webView(_ webView: ZPWWebView, didFinishNavigation dummy: AnyObject?) {
        super.webView(webView, didFinishNavigation: dummy)
        
        if #available(iOS 9, *) {
            self.webView.evaluateJavaScript("window.location.href", completionHandler: {
                [weak self] (webUrlString, error) in
                
                guard let currentUrl = webUrlString as? String, let strongSelf = self else {
                    return
                }
                
                if (strongSelf.isSameHostWithStartUrl(currentUrl)) {
                    ZPWUtils.deleteZaloFacebookOAuthCookies()
                }
            })
        }
        
        let jsString = self.jsString()
        _ = self.webView.evaluateJavaScript(jsString, completionHandler: nil)
        self.webView.evaluateJavaScript("utils.getNav()") {
            [weak self] (obj, error) in
            if error != nil {
            }
            
            if let responseDict = obj as? NSDictionary {
                let title: String = responseDict.string(forKey: "title", defaultValue: "ZaloPay")
                let thumb: String = responseDict.string(forKey: "thumb", defaultValue: "")
                
                if thumb.count > 0 {
                    self?.navigationItem.titleView = self?.titleView(title, imageUrl: thumb)
                } else {
                    self?.navigationItem.titleView = self?.titleView
                    self?.titleView.text = title
                }
            }
        }
        
        self.webView.evaluateJavaScript("window.location.href", completionHandler: {
            [weak self] (urlString, error) in
            guard let currentUrl = urlString as? String else {
                return
            }
            DDLogInfo("url = \(currentUrl)")
            DispatchQueue.main.async {
                guard let wSelf = self else {
                    return
                }
                wSelf.navigationItem.rightBarButtonItems = wSelf.historyUrl == currentUrl ? [UIBarButtonItem]() : wSelf.navigationItems
            }
        })
    }
    
    func jsString() -> String {
        return "var domHeader = document.getElementById(\"zheader\"); if(domHeader != null) {domHeader.style.display = 'none';}"
    }
    
    func titleView(_ title: String, imageUrl: String) -> UIView {
        
        let view = UIView()
        let imageView = UIImageView()
        view.addSubview(imageView)
        
        let label = UILabel()
        view.addSubview(label)
        imageView.sd_setImage(with: URL(string: imageUrl))
        
        label.text = title
        label.font = UIFont.sfuiTextMedium(withSize: 18)
        label.textColor = .white
        imageView.frame = CGRect(x: 0, y: 0, width: 36, height: 36)
        imageView.zp_roundRect(3.0)
        
        let size = label.sizeThatFits(CGSize(width: 0, height: 40))
        let maxWidth = UIScreen.main.bounds.width - 150
        var width = size.width
        if width > maxWidth {
            width = maxWidth
        }
        
        label.frame = CGRect(x: 50, y: 0,
                             width: width, height: 40)
        view.frame = CGRect(x: 0,y: 0,
                            width: imageView.frame.width + size.width + 10, height: 40)
        return view
    }
    
    
    override public func screenName() -> String {
        return ZPTrackerScreen.Screen_iOS_Service
    }
}
