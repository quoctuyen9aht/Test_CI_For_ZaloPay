//
//  ZPPromotionTabWebApp.swift
//  ZaloPay
//
//  Created by Bon Bon on 12/7/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//
import ZaloPayAnalyticsSwift
import ZaloPayWebObjc

public class ZPPromotionTabWebApp: ZPWInternalWebApp {
    
    override func webviewErrorTittle() -> String {
        return R.string_Tabbar_Promotion()
    }
    
    override func createLeftBarbuttonItems(_ color: UIColor = .white) -> [UIBarButtonItem] {
        let backBarBtn = createBackButton(color)
        let negativeBarBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeBarBtn.width = -12
        return [negativeBarBtn, backBarBtn]
    }
    
    override public func createWebView(url: String) -> ZPWInternalWebApp {
        let web =  ZPPromotionTabWebApp(url: url, webHandle: webHandle)
        web.urlString = url;
        web.appId = self.appId
        web.setUpWebView(true, showShareBtn:true)
        
        return web
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ZPTrackingHelper.shared().trackScreen(with: self)
        ZPTrackingHelper.shared().trackEvent(.promotion_launched)
    }
    
    override public func screenName() -> String {
        return ZPTrackerScreen.Screen_iOS_Promotion
    }
    
    public func reloadWebview() {
        if let webview = self.webView {
            webview.reload()
        }
    }
}
