//
//  ZPWPreferenceWebViewController.swift
//  ZaloPay
//
//  Created by Quang Huy on 6/22/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayAnalyticsSwift
import WebKit
import ZaloPayWebObjc

public class ZPWInternalWebApp: ZPWWebViewController {

    public var webAppModel: ZPWWebAppFunctionWrapper!
    public var isShowBtnBack: Bool = false
    public var isShowShareBtn: Bool = false
    public var currentNavigationStyle: [String:Any]?
    public var firstUrl: String?
    public var appId: Int = 0
    public var apiStack = [ZPWWebAppApi]()
    
    public lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(ZPWInternalWebApp.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.lightGray
        return refreshControl
    }()
    
    override public func initWebView() {        
        let configuration = WKWebViewConfiguration()
        let controller = WKUserContentController()
        configuration.userContentController = controller
        
        self.webView = ZPWWebView(controller: self, configure: configuration, usePushView:true, webHandle: webHandle)
        let wkWebView: WKWebView = self.webView.webView
        if isValidURLCheckJS() {
            self.setupHandlerJs(wkWebView)
        }
        if #available(iOS 9.0, *) {
            wkWebView.customUserAgent = ZPWUtils.zpCustomUserAgent
        }        
    }
        
    override public func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        if self.urlString.hasPrefix(kUrlPromotion)  {
            ZPTrackingHelper.shared().trackScreen(withName: self.screenName())
        }
    }

    
    private func setupNavigationBar() {
        if (isShowBtnBack){
            self.navigationItem.leftBarButtonItems = createLeftBarbuttonItems()
        }
        if (isShowShareBtn) {
            self.navigationItem.rightBarButtonItems = creatRightBarButtonItems()
        }
    }
    
    public func setUpWebView(_ showBtnBack:Bool, showShareBtn:Bool) {
        isShowBtnBack = showBtnBack
        isShowShareBtn = showShareBtn
    }
    
    override func processBackBtn(sender: UIButton) {
        if self.urlString.range(of:"promotion/detail") != nil {
            ZPTrackingHelper.shared().trackEvent(.promotion_detail_touch_back)
        }
        if let navController = self.navigationController {
            checkReloadHomePromotion()
            navController.popViewController(animated: true)
        }
    }
    
    public func checkReloadHomePromotion() {
        guard let tabbar = UIApplication.shared.delegate?.window??.rootViewController as? UITabBarController else {
            return
        }
        let tabPromotion = tabbar.viewControllers?[2] as? UINavigationController
        if tabPromotion?.viewControllers.count == 2 {
            if let homeWebPromotion = tabPromotion?.viewControllers[0] as? ZPWInternalWebApp {
                if homeWebPromotion.webView != nil{
                    homeWebPromotion.webView.reload()
                }
            }
        }
    }
    
    
    public func setupHandlerJs(_ webview: WKWebView) {
        webAppModel = ZPWWebAppFunctionWrapper(webHandle: self.webHandle)
        webAppModel.delegate = self
        webAppModel.webAppViewController = self
        webAppModel.setupHandlerJs(webview)
        webAppModel.appId = self.appId
    }
    
    override func doFactoryURL(url: URL, navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if urlString != url.absoluteString  && navigationAction.navigationType == .linkActivated {
            decisionHandler(.cancel)
            if firstUrl != nil && firstUrl == url.absoluteString {
                self.navigationController?.popToRootViewController(animated: true)
                return
            }
            
            let webAppViewController = createWebView(url: url.absoluteString)
            if firstUrl != nil {
                webAppViewController.firstUrl = firstUrl
            }
            if let navController = self.navigationController {
                if url.absoluteString.range(of:"promotion/detail") != nil {
                    ZPTrackingHelper.shared().trackEvent(.promotion_touch_detail)
                    ZPTrackingHelper.shared().trackScreen(withName: ZPTrackerScreen.Screen_iOS_Promotion_Detail)
                }
                navController.pushViewController(webAppViewController, animated: true)
                return
            }
            return
        }
        decisionHandler(.allow)
    }
    
    public func createWebView(url: String) -> ZPWInternalWebApp {
        let web =  ZPWInternalWebApp(url: url, webHandle: webHandle)
        web.urlString = url;
        web.appId = self.appId
        web.setUpWebView(true, showShareBtn:true)
        return web
    }
    
    override func doFactoryPushViewController(apo: ZPWWebAppApi, url: String) {
        let webApp = createWebView(url: url)
        self.navigationController?.pushViewController(webApp, animated: true)
    }
    
    
    override func webviewErrorTittle() -> String {
        return webHandle?.merchantName(appId: self.appId) ?? ""
    }
    
    // Web view back button clicked
    override public func backButtonClicked(_ sender: Any) {
        super.backButtonClicked(sender)
        _ = apiStack.popLast()
        if !apiStack.isEmpty {
            setupBarButtonItems(by: apiStack.last!)
        }
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.barTintColor = UIColor.zaloBase()
        self.navigationController?.navigationBar.tintColor = .white
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if currentNavigationStyle != nil {
            processChangeNavigationStyle(currentNavigationStyle!)
        }
    }
}

extension ZPWInternalWebApp: ZPWWebAppFunctionWrapperDelegate {
    public func processChangeNavigationStyle(_ param:[String:Any]) {
        if let navigationBarColor = param["backgroundColor"] as? String {
            self.navigationController?.navigationBar.barTintColor = UIColor(fromHexString: navigationBarColor)
        }
        
        if let navigationTitleColor = param["titleColor"] as? String {
            self.navigationController?.navigationBar.tintColor = UIColor(fromHexString: navigationTitleColor)
        }
        
        if let pullToRefresh = param["pullToRefresh"] as? Bool {
            self.addPullToRefresh(isAdd : pullToRefresh)
        }        
        currentNavigationStyle = param
    }
    
    public func processSetBarButton(_ api: ZPWWebAppApi) {
        apiStack.append(api)
        setupBarButtonItems(by: api)
    }
    
    public func setupBarButtonItems(by api: ZPWWebAppApi) {
        guard let params = api.param else { return }
        if let data = params["data"] as? [Any]  {
            var listButton = [UIBarButtonItem]()
            
            var xPos = 12
            for item in data.reversed() {
                if let itemTemp = item as? [String:String] {
                    let item = ToolbarActionItem(data: itemTemp)
                    let btnToolbar = ButtonToolbarAction(frame: CGRect(x: xPos, y: 7, width: 28, height: 28))
                    
                    if !(item.iconName ?? "").isEmpty {
                        if (item.iconColor?.isEmpty)! {
                            btnToolbar.titleLabel?.textColor = .white
                        } else {
                            btnToolbar.setTitleColor(UIColor(fromHexString: item.iconColor!), for: .normal)
                        }
                        btnToolbar.setIconFont(item.iconName!, for: .normal)
                        btnToolbar.setTitle("", for: .disabled)
                        btnToolbar.titleLabel?.font = UIFont.iconFont(withSize: 20)
                    } else if !(item.iconLink ?? "").isEmpty {
                        btnToolbar.sd_setImage(with: URL(string:item.iconLink!), for: .normal, completed: nil)
                    }
                    
                    if !(item.iconId ?? "").isEmpty {
                        btnToolbar.iconId = item.iconId!
                    }
                    
                    btnToolbar.addTarget(self, action: #selector(processActionButtonToolbar(_:)), for: .touchUpInside)
                    btnToolbar.api = api
                    
                    let btnView = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
                    btnView.addSubview(btnToolbar)
                    
                    let rightBarButton = UIBarButtonItem(customView: btnView)
                    listButton.append(rightBarButton)
                    
                    xPos -= 14 // draw button item from right to left
                }
            }
            self.navigationItem.rightBarButtonItems = listButton
        }
    }
    
    @objc func processActionButtonToolbar(_ sender:ButtonToolbarAction) {
        if !sender.iconId.isEmpty {
            guard let api = sender.api else {
                return
            }
            let item = ZPWebAppInfor(iconId: sender.iconId, funcAction: "navigatorAction")
            webAppModel.sendCallBack(infor: item, api: api)
        }
    }
    
    public func addPullToRefresh(isAdd : Bool){
        guard let myWebview = self.webView.webView else { return }
        
        let isExist =  myWebview.scrollView.subviews.contains(self.refreshControl)
        if isAdd && !isExist  {
            myWebview.scrollView.addSubview(self.refreshControl)
        }
        else if !isAdd && isExist {
            self.refreshControl.removeFromSuperview()
        }
    }
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        refreshButtonClick()
        delay(0.5) {
            self.refreshControl.endRefreshing()
        }
    }
    public func delay(_ delay:Double, closure:@escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
}


