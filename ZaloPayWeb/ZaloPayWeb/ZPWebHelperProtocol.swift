//
//  ZPWebHelperProtocol.swift
//  ZaloPayWeb
//
//  Created by thi la on 6/12/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation

public protocol ZPWebHelperProtocol: class {
    func patternURLs() -> [String]?
    func historyUrl(withAppId: Int) -> String
    func webUrl(withAppId: Int) -> String
    func handleUrlSchemes(schemesUrl: URL, sourceApplication: String) -> Bool
    func userInfos(appId: Int) -> [String]
    func merchantName(appId: Int) -> String
    func getPayBillReturnCode(params: Dictionary<String,Any>?, completion: @escaping (_ returnCode: Int)->())
    func transferModel(params: Dictionary<String,Any>, completion: @escaping (_ returnCode: Int)->())    
    func getMuid(appId: Int) -> String?
    func launchApp(_ appId:Int, from: UIViewController?)
}
