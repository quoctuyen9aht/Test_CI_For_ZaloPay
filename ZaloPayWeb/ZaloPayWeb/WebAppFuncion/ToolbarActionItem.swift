//
//  ToolbarActionItem.swift
//  ZaloPay
//
//  Created by Duy Dao Pham Hoang on 8/30/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

public class ToolbarActionItem: NSObject {
    public var iconId:String? // unique id
    public var iconLink:String?
    public var iconName:String? // name of icon font
    public var iconColor:String?

    public init(data: [String:String]) {
        super.init()
        iconId = data["iconId"] ?? ""
        iconLink = data["iconLink"] ?? ""
        iconName = data["iconName"] ?? ""
        iconColor = data["iconColor"] ?? ""
    }
}
