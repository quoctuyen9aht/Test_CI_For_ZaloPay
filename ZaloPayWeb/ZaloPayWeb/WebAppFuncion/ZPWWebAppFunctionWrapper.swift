//
//  ZPWWebAppFunctionWrapper.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/20/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import CocoaLumberjackSwift
import ZaloPayProfile
import WebKit
import ZaloPayWebObjc
import ZaloPayCommonSwift

public enum WWebAppActionType: String {
    case showLoading = "showLoading"
    case hideLoading = "hideLoading"
    case showDialog = "showDialog"
    case transferMoney = "transferMoney"
    case payOrder = "payOrder"
    case promotionEvent = "promotionEvent"
    case writeLog = "writeLog"
    case pushViewController = "pushView"
    case setProperty = "setProperty"
    case setToolbarActions = "setToolbarActions"
    case getUserInfo = "getUserInfo"
    case closeWindow = "closeWindow"

}

public enum UserInfo: String {
    case muid = "muid"
    case phone = "phonenumber"
    case email = "email"
    case address = "address"
    case display_name = "display_name"
    case avatar_url = "avatar_url"
}

public struct ZPWebAppInfor {
    var iconId: String
    var funcAction: String
}

public enum reponseCodeLog: Int {
    case success = 1 // Thành công
    case errorNotDefine = 0 // Lỗi không biết
    case appNotExist = -1 // Ứng dụng chưa cài đặt trên máy
    case appInternalNotExist = -2 // Ứng dụng (internal) không tồn tại
}

public enum AppType: Int {
    case native = 1
    case webview = 2
}

public protocol ZPWWebAppFunctionWrapperDelegate: class {
    func processChangeNavigationStyle(_ param:[String:Any])
    func processSetBarButton(_ api:ZPWWebAppApi)
}

public class ZPWWebAppFunctionWrapper: NSObject {
    public var bridge: ZPWKWebViewBridgeJS!
    public weak var webAppViewController: ZPWWebViewController?
    public weak var delegate: ZPWWebAppFunctionWrapperDelegate?
    public weak var webHandle: ZPWebHelperProtocol?
    
    public var appId: Int = 0
    public override init() {
        super.init()
    }
    
    public convenience init(webHandle: ZPWebHelperProtocol?) {
        self.init()
        self.webHandle = webHandle
    }
    
    public func setupHandlerJs(_ webview: WKWebView) {
        
        bridge = ZPWKWebViewBridgeJS.bridge(webview, using: {
            [weak self] (_ objectResponse: Any, _ error: Error?) in
            DDLogInfo("web app : \(objectResponse) ")
            self?.handleWebApi(withParam: objectResponse as? [String: Any])
        })
    }
    
    public func handleWebApi(withParam params: [String: Any]?) {
        guard let params = params else {
            return
        }
        let api = ZPWWebAppApi(with: params)
        handleWebApi(api)
    }
    
    public func handleWebApi(_ api: ZPWWebAppApi) {
        let functionName: String = api.functionName
        guard let action = WWebAppActionType(rawValue: functionName) else {
            return
        }

        switch action {
        case .showLoading:
            showLoading(api)
        case .hideLoading:
            hideLoading(api)
        case .showDialog:
            showDialog(api)
        case .transferMoney:
            transferMoney(api)
        case .payOrder:
            payOrder(api)
        case .promotionEvent:
            promotionEvent(with: api)
        case .writeLog:
            writeLog(api)
        case .pushViewController:
            pushViewController(with: api)
        case .setProperty:
            setProperty(with: api)
        case .setToolbarActions:
            setToolbarActions(with: api)
        case .getUserInfo:
            getUserInfo(with: api)
        case .closeWindow:
            closeWindow()
        }
    }
    
    public func closeWindow() {
        webAppViewController?.navigationController?.popToRootViewController(animated: false)
    }
    
    public func callBackWeb(withMessageData api: ZPWWebAppApi, params: [String: Any]) {
        guard var msgToCallBack = api.messageData else {
            return
        }
        
        var paramDict = (msgToCallBack["param"] as? [String: Any]) ?? [:]
        paramDict = paramDict + params
        
        msgToCallBack["msgType"] = "callback"
        msgToCallBack["param"] = paramDict
        msgToCallBack["keepCallback"] = false
      
        guard let d = try? JSONSerialization.data(withJSONObject: msgToCallBack, options: []) else {
            return
        }
        
        let jsString: String = "ZaloPayJSBridge._invokeJS('\(String(data: d, encoding: .utf8) ?? "")')"
        
        bridge.evaluteStringJS(jsString, completionHandler: {(_ objectResponse: Any, _ error: Error?) in
            self.handleWebApi(withParam: objectResponse as? [String: Any])
        })
    }
    
    public func callWeb(withMessageData api: ZPWWebAppApi, params: [String: Any]) {
        guard var msgToCallBack = api.messageData else {
            return
        }
        
        var paramDict = (msgToCallBack["param"] as? [String: Any]) ?? [:]
        paramDict = paramDict + params
        
        msgToCallBack["msgType"] = "call"
        msgToCallBack["param"] = paramDict
        msgToCallBack["keepCallback"] = true
        
        guard let d = try? JSONSerialization.data(withJSONObject: msgToCallBack, options: []) else {
            return
        }
        
        let jsString: String = "ZaloPayJSBridge._invokeJS('\(String(data: d, encoding: .utf8) ?? "")')"
        bridge.evaluteStringJS(jsString, completionHandler: {(_ objectResponse: Any, _ error: Error?) in
            self.handleWebApi(withParam: objectResponse as? [String: Any])
        })
    }

    // MARK: - Action
    public func payOrder(_ api: ZPWWebAppApi) {
        webHandle?.getPayBillReturnCode(params: api.param, completion: { [weak self] (returnCode) in
            self?.callBackWeb(withMessageData: api, params: ["error": returnCode , "errorMessage": ""])
        })
    }
    
    public func showLoading(_ api: ZPWWebAppApi) {
        ZPAppFactory.sharedInstance().hideAllHUDs(for: nil)
        ZPAppFactory.sharedInstance().showHUDAdded(to: nil)
    }
    
    public func hideLoading(_ api: ZPWWebAppApi) {
        ZPAppFactory.sharedInstance().hideAllHUDs(for: nil)
    }
    
    public func showDialog(_ api: ZPWWebAppApi) {
        guard let p = api.param else { return }
        let title = p.value(forKey: "title", defaultValue: "")
        let message = p.value(forKey: "message", defaultValue: "")
        let button =  p.value(forKey: "button", defaultValue: "")
        
        ZPDialogView.showDialog(with: DialogTypeNotification, title: title , message: message, buttonTitles: [button]) { (idx) in }
    }
    
    public func writeLog(_ api: ZPWWebAppApi) {
        guard let params = api.param else { return }
        let typeLog: String = params.value(forKey: "type", defaultValue: "")
        let data: String = params.value(forKey: "data", defaultValue: "")
        if typeLog == "" || data == "" {
            return
        }
        
        if typeLog == "info" {
//            DDLogInfo("%@", data)
        }
        else if typeLog == "warn" {
//            DDLogWarn("%@", data)
        }
        else if typeLog == "error" {
//            DDLogError("%@", data)
        }
        
    }

    public func transferMoney(_ api: ZPWWebAppApi) {
        guard let params = api.param else {
            return
        }
        webHandle?.transferModel(params: params, completion: { [weak self] returnCode in
            self?.callBackWeb(withMessageData: api, params: ["error": returnCode, "errorMessage": ""])
        })
    }
    
    public func promotionEvent(with api: ZPWWebAppApi) {
        
        guard let params = api.param else {
            return
        }
        
        if (params["url"] as? String) != nil  {
            openAnotherApp(api)
        }
        else if (params["internalApp"] as? Int) != nil
        {
            openZaloPayApp(api)
        }
    }
    
    public func pushViewController(with api: ZPWWebAppApi) {
        
        guard let params = api.param else {
            return
        }
        if let url = params["url"] as? String  {
            self.webAppViewController?.doFactoryPushViewController(apo: api, url: url)
        }
    }
    
    public func setProperty(with api: ZPWWebAppApi) {
        
        guard let params = api.param else {
            return
        }
        if let navigation = params["navigation"] as? [String:Any]  {
            delegate?.processChangeNavigationStyle(navigation)
        }
    }
    
    public func setToolbarActions(with api: ZPWWebAppApi) {
        delegate?.processSetBarButton(api)
    }
    
    public class func userInfos(appId: Int, webHandle: ZPWebHelperProtocol?) -> [String: String] {
        guard let userInfos = webHandle?.userInfos(appId: appId) else {
            return [String: String]()
        }
        return userInfos.map({ self.transform(from: $0, appId: appId, webHandle: webHandle)}).reduce([:], { $0 + $1 })
    }
    
    public func getUserInfo(with api: ZPWWebAppApi) {
        if let params = api.param, let id = params["appid"] as? Int {
            // Get appid param for webapp
            self.appId = id
        }
        
        let outParams: [String: Any] = ["data": ZPWWebAppFunctionWrapper.userInfos(appId: self.appId, webHandle: self.webHandle),
                                     "error": 1 ]
        self.callBackWeb(withMessageData: api, params: outParams)
    }
    
    private class func transform(from valueInfor: String, appId: Int, webHandle: ZPWebHelperProtocol?) -> [String: String] {
        guard let userInfo = UserInfo(rawValue: valueInfor)  else {
            return [:]
        }
        func result() -> String? {
            let profile = ZPProfileManager.shareInstance
            switch userInfo {
            case .muid:
                return webHandle?.getMuid(appId: appId)
            case .phone:
                return profile.userLoginData?.phoneNumber
            case .email:
                return profile.email
            case .address:
                return profile.address
            case .display_name:
                return profile.currentZaloUser?.displayName
            case .avatar_url:
                return profile.currentZaloUser?.avatar
            }
        }
        return [userInfo.rawValue : result() ?? ""]
    }

    func openZaloPayApp(_ api: ZPWWebAppApi)
    {
        guard let params = api.param else {
            return
        }
        
        guard let u = params["internalApp"] as? Int else {
            return
        }
        webHandle?.launchApp(u, from: webAppViewController)
        sendReponseCode(reponseCodeLog.success.rawValue)
    }
    
    func openAnotherApp(_ api: ZPWWebAppApi) {
        guard let params = api.param,
             let u = params["url"] as? String else {
                return
        }
        
        var returnCode = 0
        if let url = URL(string:u) {
            returnCode = UIApplication.shared.openURL(url) == true ? 1: 0;
        }
        if (returnCode == 0) {
            if let alternateUrl = params["alternateUrl"] as? String,
                let altUrl = URL(string:alternateUrl) {
                UIApplication.shared.openURL(altUrl)
                returnCode = reponseCodeLog.appNotExist.rawValue
            } else {
                returnCode = reponseCodeLog.errorNotDefine.rawValue
            }
        }
        sendReponseCode(returnCode)
    }
    
    public func sendReponseCode(_ code:Int) {
        let param = ["code":code]
        guard let d = try? JSONSerialization.data(withJSONObject: param, options: []) else {
            return
        }
        
        let jsString: String = "ZaloPayJSBridge._invokeJS('\(String(data: d, encoding: .utf8) ?? "")')"
        bridge.evaluteStringJS(jsString, completionHandler: {(_ objectResponse: Any, _ error: Error?) in
        })
    }
    
    public func sendCallBack(infor item: ZPWebAppInfor, api: ZPWWebAppApi) {
        let param = ["iconId":item.iconId]
        var nApi = api
        var params = api.messageData
        params?["func"] = item.funcAction
        nApi.messageData = params
        self.callWeb(withMessageData: nApi, params: ["data": param])
    }
    
}


