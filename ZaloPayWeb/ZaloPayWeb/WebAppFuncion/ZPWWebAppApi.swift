//
//  ZPWebAppApi.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/18/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit

public struct ZPWWebAppApi {
    
    public var rawData: [String: Any]
    public var functionName: String
    public var messageKey: String?
    public var messageData: [String: Any]?
    public var param: [String: Any]?
    public var paramArray: [Any]?
    
    public init(with params: [String: Any]) {
        let msgData = params["msgdata"] as? [String: Any]
        self.rawData = params
        self.functionName = msgData?["func"] as? String ?? ""
        self.messageData = msgData
        self.messageKey = msgData?["msgKey"] as? String
        self.param = msgData?["param"] as? [String: Any]
        self.paramArray = msgData?["param"] as? [Any]
    }
    
}
