//
//  ZPWUtils.swift
//  ZaloPay
//
//  Created by Bon Bon on 12/5/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Synchronized
import WebKit
import ZaloPayCommonSwift

public class ZPWUtils {

    public static func configZPUserAgent() {
        _ = self.searchUserAgent().bind { (s) in
            self._eUserAgent = s
        }
    }
    
    private static var _eUserAgent: String? {
        didSet {
            guard let agent = _eUserAgent, !agent.isEmpty else {
                return
            }
            let userDefault = UserDefaults.standard
            synchronized(userDefault) { ()  in
                userDefault.register(defaults: ["UserAgent": agent])
            }
        }
    }
    
    public static var zpCustomUserAgent : String {
        return _eUserAgent ?? ""
    }
    static private func searchUserAgent() -> Observable<String> {
        var secured = UIDevice.current.isJailBroken() ? "false" : "true"
        let osVersion = UIDevice.current.systemVersion
        guard var appVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String else {
            return  Observable.empty()
        }
        
        if appVersion.hasSuffix(".0") {
            let range = appVersion.index(appVersion.endIndex, offsetBy: -2)
            appVersion.removeSubrange(range...)
        }
        
        return Observable.create({ (s) -> Disposable in
            var wkWebView: WKWebView? = WKWebView()
            wkWebView?.evaluateJavaScript("navigator.userAgent", completionHandler: { (r, e) in
                if let e = e {
                    s.onError(e)
                    return
                }
                
                defer {
                    s.onCompleted()
                }
                
                guard let r = r as? String else {
                    return
                }
                
                let result = "\(r) ZaloPayClient/\(appVersion) OS/\(osVersion) Platform/ios Secured/\(secured)"
                s.onNext(result)
            })
    
            return Disposables.create {
                // release
                wkWebView = nil
            }
        }).catchErrorJustReturn("")
    }
    
    public static let zpDesktopUserAgent: String = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/601.5.17 (KHTML, like Gecko) iCab/5.6 Safari/601.2.7"
    
    public static func isAllowWebAppFunction(_ urlString: String?, patterns : [String]? ) -> Bool{
        // Valid first
        guard let path = urlString ,  path.count > 0,
             let patterns = patterns, patterns.count > 0 else {
            return false
        }
        
        guard let baseURL = URL(string: path), baseURL.scheme?.lowercased() == "https" else {
            return false
        }
        
        let url = baseURL.host ?? ""
        
        // Tracking
        func check() -> Bool {
            for regex in patterns {
                let p = NSPredicate(format: "SELF MATCHES %@", regex)
                
                if p.evaluate(with: url) {
                    return true
                }
                continue
            }
            
            return false
        }
        
        return check()
    }
    
    public static func getRootDomain(_ domain: String) -> String {
        var rootDomain: String = ""
        let url = URL(string: domain)
        let host: String = url?.host ?? ""
        
        let hostComponents = host.components(separatedBy: ".")
        if hostComponents.count > 1 {
            let lastPrefix = hostComponents[hostComponents.count-1]
            let preLastPrefix = hostComponents[hostComponents.count-2]
            rootDomain = "\(preLastPrefix).\(lastPrefix)"
            
            if rootDomain == "com.vn" && hostComponents.count > 2 {
                let domainResult = hostComponents[hostComponents.count-3]
                rootDomain = "\(domainResult).\(preLastPrefix).\(lastPrefix)"
            }
        }
        
        return rootDomain
    }
    
    public static func cleanCacheWeb(_ isCleanCookie:Bool) {
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        
        if isCleanCookie{
            if let cookies = HTTPCookieStorage.shared.cookies {
                for cookie in cookies {
                    HTTPCookieStorage.shared.deleteCookie(cookie)
                }
            }
        }
    }
    
    public static func deleteZaloFacebookOAuthCookies() {
        if #available(iOS 9, *) {
            let dataStore: WKWebsiteDataStore = WKWebsiteDataStore.default()
            dataStore.fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes(), completionHandler: {(_ records: [WKWebsiteDataRecord]) -> Void in
                
                for record in records {
                    
                    if  record.displayName.contains("facebook") ||
                        record.displayName.contains("zaloapp") ||
                        record.displayName.contains("google") {
                        
                        WKWebsiteDataStore.default().removeData(ofTypes: record.dataTypes, for: [record], completionHandler: {
                            //                            DDLogDebug(@"Cookies for %@ deleted successfully",record.displayName);
                        })
                    }
                }
            })
        }
    }
}
