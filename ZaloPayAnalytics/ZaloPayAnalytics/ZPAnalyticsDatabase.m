//
//  PerUserDataBase+TrackingAppTransid.m
//  ZaloPayAnalytics
//
//  Created by bonnpv on 4/26/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import "ZPAnalyticsDatabase.h"
#import <fmdb/FMDatabase.h>
#import <fmdb/FMDatabaseQueue.h>
#import <ZaloPayCommon/NSCollection+JSON.h>
#import <ZaloPayDatabase/FMResultSet+ZaloPay.h>
#import "ZPAOrderTrackingAPI.h"

#define kTrackingAppTransid        @"TrackingAppTransid"
#define kApptransId                @"apptransid"
#define kAppID                     @"appid"
#define kStep                      @"step"
#define kStep_Result               @"step_result"
#define kPcmId                     @"pcmid"
#define kTransType                 @"transtype"
#define kTransId                   @"transid"
#define kSdk_Result                @"sdk_result"
#define kServer_Result             @"server_result"
#define kSource                    @"source"
#define kStart_Time                @"start_time"
#define kFinish_Time               @"finish_time"
#define kStatus                    @"status"
#define kBank_Code                 @"bank_code"
#define kError_Code                @"error_code"
#define kPayment_Status            @"payment_status"
#define kMessage                   @"message"

#define kStep                      @"step"
#define kTimeStamp                 @"timestamp"
#define kAppTransIdStep            @"AppTransIdStep"

#define kApiID                      @"api_id"
#define kTimeBegin                  @"time_begin"
#define kTimeEnd                    @"time_end"
#define kReturnCode                 @"return_code"
#define kAppTransIdAPI              @"AppTransIdAPI"

@implementation ZPTrackingAppTransidTable

+ (NSArray<NSString*>*) creatingTableQuery {
    NSString* tableTrackingAppTransid = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@(%@ text primary key, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text, %@ int, %@ text, %@ text, %@ text, %@ text);", kTrackingAppTransid, kApptransId, kAppID, kStep, kStep_Result, kPcmId, kTransType, kTransId, kSdk_Result, kServer_Result, kSource, kStart_Time, kFinish_Time, kStatus,kBank_Code, kError_Code, kPayment_Status, kMessage];
    NSString* tableAppTransIdStep = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@(%@ text, %@ long, %@ long, PRIMARY KEY (%@, %@))", kAppTransIdStep, kApptransId, kStep, kTimeStamp, kApptransId, kStep];
    NSString* tableAppTransIdAPI = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@(%@ long primary key, %@ text, %@ long, %@ long, %@ int)", kAppTransIdAPI, kTimeBegin, kApptransId, kApiID, kTimeEnd, kReturnCode];
    
    return @[tableTrackingAppTransid, tableAppTransIdStep, tableAppTransIdAPI];
}

+ (NSArray<NSString*>*) tableNamesCanBeDropped {
    return @[kTrackingAppTransid, kAppTransIdStep, kAppTransIdAPI];
}

//- (void)markOrderCompleted:(NSString *)apptransid {
//    NSDictionary *params = @{kstatus: @(OrderStatus_Completed)};
//    [self updateToTable:kTrackingAppTransid params:params whereKey:kapptransid isValue:apptransid];
//}

- (void)addOrderLog:(ZPAOrderData *)orderData {
    NSMutableDictionary *params = [self orderLogParamsFromOrderData:orderData];
    [params setObjectCheckNil:orderData.apptransid forKey:kApptransId];
    [self.db replaceToTable:kTrackingAppTransid params:params];
}

- (void)updateOrderLog:(ZPAOrderData *)orderData {
    NSMutableDictionary *params = [self orderLogParamsFromOrderData:orderData];
    [self.db updateToTable:kTrackingAppTransid params:params whereKey:kApptransId isValue:orderData.apptransid];
}

- (NSMutableDictionary *)orderLogParamsFromOrderData:(ZPAOrderData *)orderData {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSNumber *finishTime = @([[NSDate date] timeIntervalSince1970] *1000);
    [params setObjectCheckNil:orderData.appid forKey:kAppID];
    [params setObjectCheckNil:orderData.step forKey:kStep];
    [params setObjectCheckNil:orderData.step_result forKey:kStep_Result];
    [params setObjectCheckNil:orderData.pcmid forKey:kPcmId];
    [params setObjectCheckNil:orderData.transtype forKey:kTransType];
    [params setObjectCheckNil:orderData.transid forKey:kTransId];
    [params setObjectCheckNil:orderData.sdk_result forKey:kSdk_Result];
    [params setObjectCheckNil:orderData.server_result forKey:kServer_Result];
    [params setObjectCheckNil:orderData.source forKey:kSource];
    [params setObjectCheckNil:orderData.start_time forKey:kStart_Time];
    [params setObjectCheckNil:finishTime forKey:kFinish_Time];
    [params setObjectCheckNil:orderData.bank_code forKey:kBank_Code];
    [params setObjectCheckNil:orderData.error_code forKey:kError_Code];
    [params setObjectCheckNil:orderData.payment_status forKey:kPayment_Status];
    [params setObjectCheckNil:orderData.message forKey:kMessage];

    return params;
}

- (NSArray *)getAllOrderLog {
//    NSString * query = [NSString stringWithFormat: @"SELECT * FROM %@ WHERE %@=?",kTrackingAppTransid, kstatus];
    NSString * query = [NSString stringWithFormat: @"SELECT * FROM %@ ",kTrackingAppTransid];
    NSMutableArray *_return = [NSMutableArray array];
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:query];
        while ([rs next]) {
            NSString *apptransid = [rs stringForColumn:kApptransId defaultValue:@""];
            NSInteger appid = [rs intForColumn:kAppID];
            NSInteger step = [rs intForColumn:kStep];
            NSInteger step_result = [rs intForColumn:kStep_Result];
            NSInteger pcmid = [rs intForColumn:kPcmId];
            NSInteger transtype = [rs intForColumn:kTransType];
            uint64_t transid = [rs longLongIntForColumn:kTransId];
            NSInteger sdk_result = [rs intForColumn:kSdk_Result];
            NSInteger server_result = [rs intForColumn:kServer_Result];
            NSInteger source = [rs intForColumn:kSource];
            uint64_t start_time = [rs longLongIntForColumn:kStart_Time];
            uint64_t finish_time = [rs longLongIntForColumn:kFinish_Time];
            NSString *bank_code = [rs stringForColumn:kBank_Code defaultValue:@""];
            NSInteger error_code = [rs intForColumn:kError_Code];
            NSInteger payment_status = [rs intForColumn:kPayment_Status];
            NSString *message = [rs stringForColumn:kMessage defaultValue:@""];

            ZPAOrderData *data = [[ZPAOrderData alloc] init];
            data.apptransid = apptransid;
            data.appid = @(appid);
            data.step = @(step);
            data.step_result = @(step_result);
            data.pcmid = @(pcmid);
            data.transtype = @(transtype);
            data.transid = @(transid);
            data.sdk_result = @(sdk_result);
            data.server_result = @(server_result);
            data.source = @(source);
            data.start_time = @(start_time);
            data.finish_time = @(finish_time);
            data.bank_code = bank_code;
            data.error_code = @(error_code);
            data.payment_status = @(payment_status);
            data.message = message;
            [_return addObject:data];
        }
        [rs close];
    }];
    return _return;
}

- (void)clearTrackingAppTransidTable {
    [self.db clearDataInTable:kTrackingAppTransid];
}

- (void)updateTransIdStepTime:(NSString *)apptransid step:(NSNumber *)step time:(NSNumber *)timestamp {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:apptransid forKey:kApptransId];
    [params setObjectCheckNil:step forKey:kStep];
    [params setObjectCheckNil:timestamp forKey:kTimeStamp];
    [self.db replaceToTable:kAppTransIdStep params:params];
}

- (NSArray *)getStepTimeFromTransaction:(NSString *)apptransid {
    NSMutableArray *_return = [NSMutableArray array];
    NSString * query = [NSString stringWithFormat: @"SELECT * FROM %@ WHERE %@=?",kAppTransIdStep, kApptransId];
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:query, apptransid];
        while ([rs next]) {
            int step = [rs intForColumn:kStep];
            uint64_t timestamp = [rs longLongIntForColumn:kTimeStamp];
            [_return addObject:@{kStep: @(step),
                                 kTimeStamp: @(timestamp)}];
        }
    }];
    return _return;
}

- (void)clearTransactionStepData {
    [self.db clearDataInTable:kAppTransIdStep];
}

- (void)addOrderTrackingAPILog:(ZPAOrderTrackingAPI*)orderTracking {
    NSMutableDictionary *params = [self orderLogParamsFromOrderTrackingAPIData:orderTracking];
    [self.db replaceToTable:kAppTransIdAPI params:params];
}

- (NSMutableDictionary *)orderLogParamsFromOrderTrackingAPIData:(ZPAOrderTrackingAPI*)orderTracking {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:orderTracking.apiId forKey:kApiID];
    [params setObjectCheckNil:orderTracking.timeBegin forKey:kTimeBegin];
    [params setObjectCheckNil:orderTracking.timeEnd forKey:kTimeEnd];
    [params setObjectCheckNil:orderTracking.returnCode forKey:kReturnCode];
    [params setObjectCheckNil:orderTracking.apptransid forKey:kApptransId];
    return params;
}

- (NSArray *)getAPILogFromTransaction:(NSString *)apptransid {
    NSMutableArray *result = [NSMutableArray array];
    NSString * query = [NSString stringWithFormat: @"SELECT * FROM %@ WHERE %@=?",kAppTransIdAPI, kApptransId];
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:query, apptransid];
        while ([rs next]) {
            long long apiID = [rs longLongIntForColumn:kApiID];
            long long timeBegin = [rs longLongIntForColumn:kTimeBegin];
            long long timeEnd = [rs longLongIntForColumn:kTimeEnd];
            long long returnCode = [rs longLongIntForColumn:kReturnCode];
            [result addObject:@{kApiID: @(apiID),
                                kTimeBegin: @(timeBegin),
                                kTimeEnd: @(timeEnd),
                                kReturnCode: @(returnCode)}];
        }
    }];
    return result;
}

- (void)clearTransactionAPIData {
    [self.db clearDataInTable:kAppTransIdAPI];
}

@end
