//
//  ZPAOrderTracking.m
//  ZaloPayAnalytics
//
//  Created by bonnpv on 4/26/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPAOrderTracking.h"
#import "ZPAnalyticsDatabase.h"
#import "ZPAOrderTrackingAPI.h"

@implementation ZPAOrderTracking

- (instancetype)initWithDatabase:(id<ZPAnalyticsDataBase>)dbProtocol {

    self = [super init];
    if (self) {
        self.dbProtocol = dbProtocol;
    }
    return self;
}
- (void)trackSDKInit:(NSString *)apptransid result:(OrderStepResult)result {
    ZPAOrderData *data = [[ZPAOrderData alloc] init];
    data.apptransid = apptransid;
    data.step_result = @(result);
    data.step = @(OrderStep_SDKInit);
    [self updateOrderStep:data];
}

- (void)trackUserCancel:(NSString *)apptransid {
    ZPAOrderData *data = [[ZPAOrderData alloc] init];
    data.step = @(OrderStep_OrderResult);
    data.step_result = @(OrderStepResult_UserCancel);
    data.apptransid = apptransid;
    [self updateOrderStep:data];
}

- (void)trackUserInputCardInfo:(NSString *)apptransid {
    ZPAOrderData *data = [[ZPAOrderData alloc] init];
    data.step = @(OrderStep_InputCardInfo);
    data.step_result = @(OrderStepResult_Success);
    data.apptransid = apptransid;
    [self updateOrderStep:data];
}

- (void)trackChoosePayMethod:(NSString *)apptransid
                       pcmid:(NSInteger)pcmid
                    bankCode:(NSString*)bankCode
                      result:(OrderStepResult)result {
    ZPAOrderData *data = [[ZPAOrderData alloc] init];
    data.step = @(OrderStep_ChoosePayMethod);
    data.step_result = @(result);
    data.apptransid = apptransid;
    data.pcmid = @(pcmid);
    data.bank_code = bankCode;
    [self updateOrderStep:data];
}

- (void)trackSubmitTrans:(NSString *)apptransid
                 transid:(NSString *)transid
                   pcmid:(NSInteger)pcmid
            serverResult:(NSInteger)serverResult {
    ZPAOrderData *data = [[ZPAOrderData alloc] init];
    data.step = @(OrderStep_SubmitTrans);
    data.step_result = @(OrderStepResult_Success);
    data.apptransid = apptransid;
    data.transid = @([transid longLongValue]);
    data.pcmid = @(pcmid);
    data.server_result = @(serverResult);
    [self updateOrderStep:data];
}

- (void)trackVerifyOtp:(NSString *)apptransid
               transid:(NSString *)transid
          serverResult:(NSInteger)serverResult {
    ZPAOrderData *data = [[ZPAOrderData alloc] init];
    data.step = @(OrderStep_VerifyOtp);
    data.step_result = @(OrderStepResult_Success);
    data.apptransid = apptransid;
    data.transid = @([transid longLongValue]);
    data.server_result = @(serverResult);
    [self updateOrderStep:data];
}

- (void)trackWebLogin:(NSString *)apptransid result:(OrderStepResult)result {
    ZPAOrderData *data = [[ZPAOrderData alloc] init];
    data.step = @(OrderStep_WebLogin);
    data.apptransid = apptransid;
    data.step_result = @(result);
    [self updateOrderStep:data];
}

- (void)trackWebInfoConfirm:(NSString *)apptransid result:(OrderStepResult)result {
    ZPAOrderData *data = [[ZPAOrderData alloc] init];
    data.step = @(OrderStep_WebInfoConfirm);
    data.apptransid = apptransid;
    data.step_result = @(result);
    [self updateOrderStep:data];
    
}

- (void)trackWebOtp:(NSString *)apptransid result:(OrderStepResult)result {
    ZPAOrderData *data = [[ZPAOrderData alloc] init];
    data.step = @(OrderStep_WebOtp);
    data.apptransid = apptransid;
    data.step_result = @(result);
    [self updateOrderStep:data];
    
}

- (void)trackOrderResult:(NSString *)apptransid sdkResult:(OrderStepResult)sdkResult serverResult:(NSInteger)serverResult {
    ZPAOrderData *data = [[ZPAOrderData alloc] init];
    data.step = @(OrderStep_OrderResult);
    data.step_result = @(OrderStepResult_Success);
    data.apptransid = apptransid;
    data.sdk_result = @(sdkResult);
    data.server_result = @(serverResult);
    [self updateOrderStep:data];
    
}

- (void)trackOrderDisplay:(NSString *)apptransid errorCode:(NSInteger)errorCode paymentStatus:(OrderResultDisplay)paymentStatus message:(NSString *)message {
    ZPAOrderData *data = [[ZPAOrderData alloc] init];
    data.apptransid = apptransid;
    data.error_code = @(errorCode);
    data.payment_status = @(paymentStatus);
    data.message = message;
    [self updateOrderStep:data];
}

- (void)startTrackOrder:(ZPAOrderData *)orderData {
    if (orderData.apptransid.length == 0) {
        return;
    }
    [self.dbProtocol addOrderLog:orderData];
    [self trackAppTransIdStep:orderData];
}

- (void)updateOrderStep:(ZPAOrderData *)orderData {
    if (orderData.apptransid.length == 0) {
        return;
    }
    [self.dbProtocol updateOrderLog:orderData];
    [self trackAppTransIdStep:orderData];
}

- (void)trackAppTransIdStep:(ZPAOrderData *)data {
    NSNumber *time = @([[NSDate date] timeIntervalSince1970] * 1000);
    NSNumber *step = @([data.step intValue]);
    [self.dbProtocol updateTransIdStepTime:data.apptransid step:step time:time];
}

- (NSArray*)allOrderLog {
    NSMutableArray *_return = [NSMutableArray array];
    NSArray *logs = [self.dbProtocol getAllOrderLog];
    if (logs.count > 0) {
        [self.dbProtocol clearTrackingAppTransidTable];
    }
    for (ZPAOrderData *data in logs) {
        NSArray *timing = [self.dbProtocol getStepTimeFromTransaction:data.apptransid];
        NSArray *apiCalls = [self.dbProtocol getAPILogFromTransaction: data.apptransid];
        
        NSDictionary *displayResult = @{@"error_code":data.error_code,
                                        @"payment_status":data.payment_status,
                                        @"message":data.message
                                        };
        NSDictionary *dic = @{@"apptransid": data.apptransid,
                               @"appid": data.appid,
                               @"step": data.step,
                               @"step_result": data.step_result,
                               @"pcmid": data.pcmid,
                               @"transtype": data.transtype,
                               @"transid": data.transid,
                               @"sdk_result": data.sdk_result,
                               @"server_result": data.server_result,
                               @"source": data.source,
                               @"start_time": data.start_time,
                               @"finish_time": data.finish_time,
                               @"bank_code": data.bank_code,
                               @"timing":timing,
                               @"api_calls": apiCalls,
                               @"display_result": displayResult
                               };

        [_return addObject:dic];
    }
    [self.dbProtocol clearTransactionStepData];
    return _return;
}

//- (void)markOrderCompleted:(NSString *)apptransid {
//    if (apptransid.length > 0) {
//        [self.dbProtocol markOrderCompleted:apptransid];
//    }
//}

- (void)trackingApptransId:(NSString*)apptransId apiId:(NSNumber*)apiId timeBegin: (NSNumber*)timeBegin timeEnd: (NSNumber*)timeEnd returnCode: (NSNumber*)returnCode {
    
    ZPAOrderTrackingAPI *object = [[ZPAOrderTrackingAPI alloc] initWithAppTransId:apptransId apiId:apiId timeBegin:timeBegin timeEnd:timeEnd returnCode:returnCode];
    
    [self.dbProtocol addOrderTrackingAPILog:object];
}

@end
