//
//  ZPAOrderTracking.h
//  ZaloPayAnalytics
//
//  Created by bonnpv on 4/26/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZPAOrderData.h"
#import "ZPAOrderTrackingAPI.h"

@protocol ZPAnalyticsDataBase;


@interface ZPAOrderTracking : NSObject


- (instancetype)initWithDatabase:(id<ZPAnalyticsDataBase>)dbProtocol;
@property (strong, nonatomic) id<ZPAnalyticsDataBase> dbProtocol;


- (void)trackSDKInit:(NSString *)apptransid
              result:(OrderStepResult)result;

- (void)startTrackOrder:(ZPAOrderData *)orderData;

- (void)updateOrderStep:(ZPAOrderData *)orderData;

- (void)trackUserCancel:(NSString *)apptransid;

- (void)trackUserInputCardInfo:(NSString *)apptransid;

- (void)trackChoosePayMethod:(NSString *)apptransid
                       pcmid:(NSInteger)pcmid
                    bankCode:(NSString*)bankCode
                      result:(OrderStepResult)result;

- (void)trackSubmitTrans:(NSString *)apptransid
                 transid:(NSString *)transid
                   pcmid:(NSInteger)pcmid
            serverResult:(NSInteger)serverResult;

- (void)trackVerifyOtp:(NSString *)apptransid
               transid:(NSString *)transid
          serverResult:(NSInteger)serverResult;

- (void)trackWebLogin:(NSString *)apptransid
               result:(OrderStepResult)result;

- (void)trackWebInfoConfirm:(NSString *)apptransid
                     result:(OrderStepResult)result;

- (void)trackWebOtp:(NSString *)apptransid
             result:(OrderStepResult)result;

- (void)trackOrderResult:(NSString *)apptransid
               sdkResult:(OrderStepResult)sdkResult
            serverResult:(NSInteger)serverResult;

- (void)trackOrderDisplay:(NSString *)apptransid errorCode:(NSInteger)errorCode paymentStatus:(OrderResultDisplay)paymentStatus message:(NSString *)message;

- (NSArray*)allOrderLog;

//- (void)markOrderCompleted:(NSString *)apptransid;

- (void)trackingApptransId:(NSString*)apptransId
                     apiId:(NSNumber*)apiId
                 timeBegin: (NSNumber*)timeBegin
                   timeEnd: (NSNumber*)timeEnd
                returnCode: (NSNumber*)returnCode;
@end
