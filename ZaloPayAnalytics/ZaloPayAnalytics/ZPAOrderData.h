//
//  ZPAOrderData.h
//  ZaloPayAnalytics
//
//  Created by bonnpv on 5/2/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef enum {
    OrderStep_GetAppInfo    = 1,
    OrderStep_SDKInit,
    OrderStep_InputCardInfo,
    OrderStep_ChoosePayMethod,
    OrderStep_SubmitTrans,
    OrderStep_VerifyOtp,
    OrderStep_WebLogin,
    OrderStep_WebInfoConfirm,
    OrderStep_WebOtp,
    OrderStep_OrderResult,
} OrderStep;


typedef NS_ENUM(NSInteger, OrderStepResult){
    OrderStepResult_None        = 0,
    OrderStepResult_Success     = 1,
    OrderStepResult_Fail        = 2,
    OrderStepResult_UserCancel  = 3,
};

typedef NS_ENUM(NSInteger, OrderResultDisplay){
    OrderResultDisplay_Success      = 1,
    OrderResultDisplay_Processing   = 2,
    OrderResultDisplay_NetworkError = 3,
    OrderResultDisplay_Fail         = 4
};

typedef enum {
    OrderSource_None          = 0,
    OrderSource_QR          = 1,
    OrderSource_AppToApp,
    OrderSource_WebToApp,
    OrderSource_MerchantApp,
    OrderSource_NotifyInApp,
    OrderSource_Bluetooth,
    OrderSource_NFC,
    OrderSource_Zalo,
} OrderSource;

typedef NS_ENUM(NSInteger, OrderStatus){
    OrderStatus_None            = 0,
    OrderStatus_Completed       = 1,
};

@interface ZPAOrderData : NSObject
@property (nonatomic, strong)  NSString *apptransid;
@property (nonatomic, strong)  NSNumber *appid;
@property (nonatomic, strong)  NSNumber *step;
@property (nonatomic, strong)  NSNumber *step_result;
@property (nonatomic, strong)  NSNumber *pcmid;
@property (nonatomic, strong)  NSNumber *transtype;
@property (nonatomic, strong)  NSNumber *transid;
@property (nonatomic, strong)  NSNumber *sdk_result;
@property (nonatomic, strong)  NSNumber *server_result;
@property (nonatomic, strong)  NSNumber *source;
@property (nonatomic, strong)  NSNumber *start_time;
@property (nonatomic, strong)  NSNumber *finish_time;
@property (nonatomic, strong)  NSNumber *status;
@property (nonatomic, strong)  NSString *bank_code;
@property (nonatomic, strong)  NSNumber *error_code;
@property (nonatomic, strong)  NSNumber *payment_status;
@property (nonatomic, strong)  NSString *message;


+ (ZPAOrderData *)getAppInfo:(NSString *)apptransid
                       appid:(NSString *)appid
                   transtype:(int)transtype
                      source:(OrderSource)source
                      result:(OrderStepResult)result
               server_result:(int)server_result;
@end
