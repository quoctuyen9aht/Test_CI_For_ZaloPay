//
//  ZPAOrderTrackingAPI.m
//  ZaloPayAnalytics
//
//  Created by Quang Huy on 6/26/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPAOrderTrackingAPI.h"

@implementation ZPAOrderTrackingAPI
-(instancetype) initWithAppTransId: (NSString*)apptransId apiId: (NSNumber*)apiId timeBegin: (NSNumber*)timeBegin timeEnd: (NSNumber*)timeEnd returnCode: (NSNumber*)returnCode {
    if (self = [super init]) {
        self.apptransid = apptransId;
        self.apiId = apiId;
        self.timeBegin = timeBegin;
        self.timeEnd = timeEnd;
        self.returnCode = returnCode;
    }
    return self;
}
@end
