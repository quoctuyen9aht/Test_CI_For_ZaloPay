//
//  PerUserDataBase+TrackingAppTransid.h
//  ZaloPayAnalytics
//
//  Created by bonnpv on 4/26/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import <ZaloPayDatabase/ZPBaseTable.h>
#import "ZPAOrderTracking.h"
#import "ZPAnalyticsExternalProtocols.h"

@interface ZPTrackingAppTransidTable : ZPBaseTable<ZPAnalyticsDataBase>
@end
