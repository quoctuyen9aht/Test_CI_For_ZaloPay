//
//  ZPAnalyticsExternalProtocols.h
//  ZaloPayAnalytics
//
//  Created by vuongvv on 8/18/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ZPAOrderTrackingAPI;
@class ZPAOrderTracking;


@protocol ZPAnalyticsDataBase <NSObject>

//=======TrackingAppTransid=======

- (void)addOrderLog:(ZPAOrderData *)orderData;

- (void)updateOrderLog:(ZPAOrderData *)orderData;

//- (void)markOrderCompleted:(NSString *)apptransid;

- (NSArray *)getAllOrderLog;

- (void)clearTrackingAppTransidTable;


//=======AppTransIdStep=======

- (void)updateTransIdStepTime:(NSString *)apptransid step:(NSNumber *)step time:(NSNumber *)timestamp;
- (NSArray *)getStepTimeFromTransaction:(NSString *)apptransid;
- (void)clearTransactionStepData;

//=======AppTransIdAPI=======

- (void)addOrderTrackingAPILog:(ZPAOrderTrackingAPI *)orderTracking;
- (NSArray*)getAPILogFromTransaction:(NSString *)apptransid;
- (void)clearTransactionAPIData;

@end
