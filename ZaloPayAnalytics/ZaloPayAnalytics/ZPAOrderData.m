//
//  ZPAOrderData.m
//  ZaloPayAnalytics
//
//  Created by bonnpv on 5/2/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPAOrderData.h"

@implementation ZPAOrderData
+ (ZPAOrderData *)getAppInfo:(NSString *)apptransid
                       appid:(NSString *)appid
                   transtype:(int)transtype
                      source:(OrderSource)source
                      result:(OrderStepResult)result
               server_result:(int)server_result {
    
    ZPAOrderData *data = [[ZPAOrderData alloc] init];
    data.apptransid = apptransid;
    data.appid = @([appid intValue]);
    data.step = @(OrderStep_GetAppInfo);
    data.step_result = @(result);
    data.transtype = @(transtype);
    data.server_result = @(server_result);
    data.source = @(source);
    data.start_time = @([[NSDate date] timeIntervalSince1970] * 1000);
    return data;
}
@end
