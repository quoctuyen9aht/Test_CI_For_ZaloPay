//
//  ZPAOrderTrackingAPI.h
//  ZaloPayAnalytics
//
//  Created by Quang Huy on 6/26/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZPAOrderTrackingAPI : NSObject
-(instancetype) initWithAppTransId: (NSString*)apptransId apiId: (NSNumber*)apiId timeBegin: (NSNumber*)timeBegin timeEnd: (NSNumber*)timeEnd returnCode: (NSNumber*)returnCode;
@property (nonatomic, strong)  NSString *apptransid;
@property (nonatomic, strong)  NSNumber *apiId;
@property (nonatomic, strong)  NSNumber *timeBegin;
@property (nonatomic, strong)  NSNumber *timeEnd;
@property (nonatomic, strong)  NSNumber *returnCode;
@end
