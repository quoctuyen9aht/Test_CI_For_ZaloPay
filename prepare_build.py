#!/usr/local/bin/python3

import sys
import subprocess
import plistlib
import re
import time


def increase_version(file_path):

    versionCode = ""
    versionName = ""
    with open(file_path, 'rb') as fin:
        s = plistlib.load(fin, fmt=plistlib.FMT_XML)
    print(s['CFBundleVersion'])
    print(s['CFBundleShortVersionString'])

    versionName = s['CFBundleShortVersionString']
    versionCode = s['CFBundleVersion']
    version = int(versionCode)
    version = version + 1
    versionCode = "%d" % version
    s['CFBundleVersion'] = versionCode

    with open(file_path, 'wb') as fout:
        plistlib.dump(s, fout, fmt=plistlib.FMT_XML)

    return (versionCode, versionName)


def extract_gitlog():
    GIT_COMMIT_FIELDS = [
        'shortid',
        'id',
        'author_name',
        'author_email',
        'date',
        'message']
    GIT_LOG_FORMAT = ['%h', '%H', '%an', '%ae', '%ad', '%s']

    GIT_LOG_FORMAT = '%x1f'.join(GIT_LOG_FORMAT) + '%x1e'

    p = subprocess.Popen(
        'git log --format="%s"' %
        GIT_LOG_FORMAT,
        shell=True,
        stdout=subprocess.PIPE)
    (log, _) = p.communicate()
    # print(log)
    logstr = log.decode('utf8')
    # print(type(logstr), type(log))
    logstr = logstr.strip('\n\x1e').split("\x1e")
    logstr = [row.strip().split("\x1f") for row in logstr]
    logstr = [dict(zip(GIT_COMMIT_FIELDS, row)) for row in logstr]

    log_messages = []
    last_bump_env = ''
    for ci in logstr:
        if 'Bump version' in ci['message']:
            m = re.match(r'\[(\w+)\]', ci['message'])
            last_bump_env = m.group(1)
            break
        # add log messages
        log_messages.append(ci)

    return (log_messages, last_bump_env)


def print_usage(valid_env):
    print('Usage: ./prepare_build.py [%s]\n' % '|'.join(valid_env))


def generate_release_notes(env, versionCode, versionName, release_notes_path):
    current_date = time.strftime('%Y-%m-%d %H:%M')
    # ZaloPay (Sandbox v2.12.0 - build 253) - Released on 2017-05-18
    header = 'ZaloPay (%s v%s - build %s) - Released on %s\n' % (env,
                                                                 versionName,
                                                                 versionCode,
                                                                 current_date)
    log_messages, last_bump_env = extract_gitlog()

    log_format = [
        '+ %s by %s (#%s)' %
        (ci['message'],
         ci['author_name'],
         ci['shortid']) for ci in log_messages]
    log_format.append('\n')
    release_notes = '\n'.join(log_format)

    if last_bump_env != env and len(log_format) == 1:
        # bump on different build environment
        # copy previous release note
        with open(release_notes_path) as fin:
            log_format = fin.readlines()
            del log_format[0]
            release_notes = ''.join(log_format)

    with open(release_notes_path, 'wt') as fout:
        fout.write(header)
        fout.write(release_notes)


def git_commit_bump_version(env, versionCode, versionName, files):
    # [Sandbox] Bump version 2.12.0 - build 253
    commit_message = "[%s] Bump version %s - build %s" % (
        env, versionName, versionCode)

    subprocess.run(["git", "add"] + files)
    subprocess.run(["git", "commit", "-m", commit_message])


def main():
    valid_env = ["Sandbox", "Staging", "Production"]

    if len(sys.argv) < 2:
        print_usage(valid_env)
        exit(1)

    env = sys.argv[1]
    if env not in valid_env:
        print_usage(valid_env)
        exit(1)

    version_path = 'ZaloPay/Info.plist'
    release_notes_path = 'fastlane/release_notes.txt'

    (versionCode, versionName) = increase_version(version_path)
    generate_release_notes(env, versionCode, versionName, release_notes_path)
    git_commit_bump_version(
        env, versionCode, versionName, [
            version_path, release_notes_path])


if __name__ == '__main__':
    main()
