//
//  ZaloPayGateway.h
//  ZaloPayGateway
//
//  Created by Phuoc Huynh on 3/23/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ZaloPayGateway.
FOUNDATION_EXPORT double ZaloPayGatewayVersionNumber;

//! Project version string for ZaloPayGateway.
FOUNDATION_EXPORT const unsigned char ZaloPayGatewayVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ZaloPayGateway/PublicHeader.h>


