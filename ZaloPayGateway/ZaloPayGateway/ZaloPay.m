//
//  ZaloPayGateway.m
//  ZaloPayGateway
//
//  Created by PhucPv on 5/19/16.
//  Copyright © 2016 PhucPv. All rights reserved.
//

#import "ZaloPay.h"
#import "ZaloPayGatewayLog.h"
#import <ReactiveObjC/ReactiveObjC.h>
#import <ZaloPayAppManager/PaymentReactNativeApp/ReactNativeAppManager+GetMerchan.h>
#import <ZaloPayCommon/NSCollection+JSON.h>
#import <ZaloPayAppManager/ReactFactory.h>

#import <ZaloPayUI/ZPAppFactory.h>
#import <ZaloPayUI/ZPTrackingHelper.h>
#import <ZaloPayAnalytics/ZaloPayAnalytics.h>
#import <ZaloPayUI/ZPDialogView.h>
#import <ZaloPayAppManager/PaymentReactNativeApp/ZPMerchantUserInfo.h>
#import <ZaloPayNetwork/ZPReactNetwork.h>
#import <ZaloPayCommon/UIDevice+Payment.h>
#import <ZaloPayCommon/ZPConstants.h>
#import <ZaloPayCommon/R.h>
#import <ZaloPayCommon/UIFont+Extension.h>
#import <ZaloPayAppManager/ReactModuleViewController.h>
#import <ZaloPayNetwork/NetworkManager.h>
#import <ZaloPayAppManager/PaymentReactNativeApp/ReactNativeAppManager.h>
#import <ZaloPayAppManager/PaymentReactNativeApp/ReactNativeAppManager+AppProperty.h>
#import <ZaloPayAppManager/ZaloPayApiHelper.h>
#import <ZaloPayProfile/ZaloPayProfile-Swift.h>
#import <ZaloPayProfile/ZPContact.h>
#import <ZaloPayAnalyticsSwift/ZaloPayAnalyticsSwift-Swift.h>
@interface NetworkManager (ZaloPayReactNativeInternal)
- (RACSignal *)getOrderFromAppId:(NSInteger)appid transToken:(NSString *)transToken;
@end

@implementation ZaloPay
RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(launchContactList:(NSString *)currentPhone
                  viewMode:(NSString *)viewMode
                  navigationTitle:(NSString *)navigationTitle
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSString *phone = @"";
        if ([currentPhone isKindOfClass:[NSString class]]) {
            phone = currentPhone;
        }
        if ([currentPhone isKindOfClass:[NSNumber class]]) {
            phone = [(NSNumber *)currentPhone stringValue];
        }
        
        NSString *viewModeTemp = ViewModeKeyboard_ABC;
        if ([viewMode isKindOfClass:[NSString class]]) {
            viewModeTemp = viewMode;
        }
        
        NSString *navigationTitleTemp = @"";
        if ([navigationTitle isKindOfClass:[NSString class]]) {
            navigationTitleTemp = navigationTitle;
        }
        
        [[ZPAppFactory sharedInstance] launchContactList:phone viewMode:viewModeTemp navigationTitle:navigationTitleTemp completeHandle:^(NSDictionary *data) {
            if (resolve)
            {
                resolve(data);
            }
        }];
    });
}

RCT_EXPORT_METHOD(navigateSupportCenter) {
    dispatch_async(dispatch_get_main_queue(), ^{
        ReactModuleViewController *rnViewController =   [[ZPAppFactory sharedInstance] createReactViewController:nil];
        rnViewController.moduleName = @"SupportCenter";
        UINavigationController *navigationController = [[ZPAppFactory sharedInstance] rootNavigation];
        [navigationController pushViewController:rnViewController animated:YES];
    });
}

RCT_REMAP_METHOD(loadFontAsync,
                 loadAsyncWithFontFamilyName:(NSString *)fontFamilyName
                 withLocalUri:(NSURL *)uri
                 resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject)
{
    
    NSString *path = [uri path];
    NSData *inData = [NSData dataWithContentsOfURL:uri];
    if (!inData) {
        reject(@"E_FONT_FILE_NOT_FOUND",
               [NSString stringWithFormat:@"File '%@' for font '%@' doesn't exist", path, fontFamilyName],
               nil);
        return;
    }
    
    RACSubject *subject = [RACSubject subject];
    [subject subscribeError:^(NSError *error) {
        reject(@"E_FONT_CREATION_FAILED",
               error.localizedDescription,
               nil);
    } completed:^{
        resolve(nil);
    }];
    [UIFont registerFontWithData:inData subscriber:subject];
}

RCT_EXPORT_METHOD(payOrder:(NSDictionary *)param
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
    [ZaloPayApiHelper payOrder:param resolver:resolve rejecter:reject];
}

RCT_EXPORT_METHOD(showNoInternetConnection) {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (![self isMerchantAppActive]) {
            return;
        }
        [[ZPAppFactory sharedInstance] showNoInternetConnectionView];
    });    
}


RCT_EXPORT_METHOD(closeModule:(NSInteger)moduleId) {
    DDLogDebug(@"Request to close module %@", @(moduleId));
    dispatch_async(dispatch_get_main_queue(), ^{
        [[ReactFactory sharedInstance] closeModuleInstance:moduleId];
    });
}

RCT_EXPORT_METHOD(logout) {
    DDLogInfo(@"Call Function Logout from app");
    dispatch_async(dispatch_get_main_queue(), ^{
        [[ZPAppFactory sharedInstance] logout];
    });
}

RCT_REMAP_METHOD(getUserInfo,
                 resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject) {
    
    [[[ReactNativeAppManager sharedInstance] getMerchantUserInfo] subscribeNext:^(ZPMerchantUserInfo* info) {
        NSMutableDictionary *data = [NSMutableDictionary dictionary];
        NSInteger appId = [ReactNativeAppManager sharedInstance].runingAppId;
        NSDictionary *userInfos = [[ZPAppFactory sharedInstance] getUserInfosWith:appId];
        [data addEntriesFromDictionary:userInfos];
        [data setObjectCheckNil:info.muid forKey:@"mUid"];
        [data setObjectCheckNil:info.displayName forKey:@"displayName"];
        [data setObjectCheckNil:info.birthday forKey:@"dateOfBirth"];
        [data setObjectCheckNil:info.gender forKey:@"gender"];
        [data setObjectCheckNil:info.maccesstoken forKey:@"mAccessToken"];
        resolve(@{@"code": @(1), @"data": data});
    } error:^(NSError *error) {
        NSString *errorCode = @"-1";
        reject(errorCode,@"Error",error);
    }];
}

RCT_EXPORT_METHOD(showDialog:(int)dialogType
                  title:(NSString *)title
                  message:(NSString *)message
                  buttonTitles:(NSArray *)buttonTitles
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
dispatch_async(dispatch_get_main_queue(), ^{
    if (![self isMerchantAppActive]) {
        return;
    }
    NSString *msg = message;
    if (msg.length > 0) {
        NSString *fontStyle = [NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%dpx;}</style>",
                               @"SFUIText-Regular",
                               16];
        msg = [msg stringByAppendingString:fontStyle];
    }
    MMPopupItemHandler handler = ^(NSInteger index) {
        if (resolve) {
            NSDictionary *data = @{@"code": @(index)};
            resolve(data);
        }
    };
    
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[msg dataUsingEncoding:NSUTF8StringEncoding]
                                                                            options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                      NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                                 documentAttributes:nil error:nil];
    
    
        [ZPDialogView showDialogWithType:dialogType
                                   title:title
                        attributeMessage:attributedString
                            buttonTitles:buttonTitles
                                 handler:handler];
    });        
}

RCT_EXPORT_METHOD(showLoading) {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (![self isMerchantAppActive]) {
            return;
        }
        [[ZPAppFactory sharedInstance] showHUDAddedTo:[[[UIApplication sharedApplication] delegate] window]];
    });
}

RCT_EXPORT_METHOD(hideLoading) {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[ZPAppFactory sharedInstance] hideAllHUDsForView:[[[UIApplication sharedApplication] delegate] window]];
    });
}

#pragma mark - Track Event

RCT_EXPORT_METHOD(trackEvent:
                  (NSInteger) eventId) {
    [[ZPTrackingHelper shared].eventTracker trackEvent:eventId value:nil label:nil];
}

RCT_EXPORT_METHOD(trackEventApi:
                  (NSInteger) eventId :(NSString *)eventLabel)  {
    [[ZPTrackingHelper shared].eventTracker trackEvent:eventId value:nil label:eventLabel];
}

RCT_EXPORT_METHOD(trackEventWithIdAndValue:
                  (NSInteger) eventId :(NSNumber *)eventValue) {
    [[ZPTrackingHelper shared].eventTracker trackEvent:eventId value:eventValue label:nil];
}

RCT_EXPORT_METHOD(trackScreen:
                  (NSString*) screenName) {
    [[ZPTrackingHelper shared].eventTracker trackScreen:screenName];
}

#pragma mark - Request
RCT_EXPORT_METHOD(request:(NSString *)url
                  :(NSDictionary *)requestOption
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
    
    DDLogDebug(@"url = %@",url);
    DDLogDebug(@"request option = %@",requestOption);
    
    NSString *method;
    NSDictionary *headers;
    NSString *body;
    NSDictionary *query;
    
    if ([requestOption isKindOfClass:[NSDictionary class]]) {
        method = [requestOption stringForKey:@"method" defaultValue:@"GET"];
        headers = [requestOption dictionaryForKey:@"headers" defaultValue:@{}];
        body = [requestOption stringForKey:@"body" defaultValue:@""];
        query = [requestOption dictionaryForKey:@"query" defaultValue:@{}];
    }
    NSDictionary *requestParam = [self requestParamFromParams:query];
    ZPReactNetwork *client = [[ZPReactNetwork alloc] init];
    
    [[client requestWithMethod:method
                        header:headers
                     urlString:url
                        params:requestParam
                          body:body] subscribeNext:^(id x) {
        if (resolve) {
            resolve(x);
        }
    } error:^(NSError *error) {
        if (reject) {
            NSDictionary *data = error.userInfo;
            NSString *code = [[data numericForKey:@"error_code"] stringValue];
            NSString *message = [data stringForKey:@"error_message"];
            reject(code, message, error);
        }
    }];
}

- (NSDictionary *)requestParamFromParams:(NSDictionary *)params {
    NSMutableDictionary *_return = [NSMutableDictionary dictionary];
    if (params.count > 0) {
        [_return addEntriesFromDictionary:params];
    }
    NSInteger appId = [ReactNativeAppManager sharedInstance].runingAppId;
    if (appId == appVoucherId) {
        [_return setObjectCheckNil:[[NetworkManager sharedInstance] accesstoken] forKey:@"accesstoken"];
        [_return setObjectCheckNil:[[NetworkManager sharedInstance] paymentUserId] forKey:@"userid"];
    }
    return _return;
}

RCT_EXPORT_METHOD(shareMessage:(NSString *) message) {
    if (!message) {
        return;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        NSArray *objectsToShare = @[message];
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
        
        NSArray *excludeActivities = @[UIActivityTypeAssignToContact];
        
        activityVC.excludedActivityTypes = excludeActivities;
        
        UINavigationController *nav = [[ZPAppFactory sharedInstance] rootNavigation];
        UIViewController *topViewController = nav.topViewController;
        [topViewController presentViewController:activityVC animated:YES completion:nil];
    });
}


#pragma mark - Exported constants
- (NSDictionary *)constantsToExport {
    return @{ @"dscreentype": [[UIDevice currentDevice] screenTypeString],
              @"platform": @"ios",
              };
}

- (BOOL)isMerchantAppActive {
    UINavigationController *nav = [[ZPAppFactory sharedInstance] rootNavigation];
    if (![nav isKindOfClass:[UINavigationController class]]) {
        return false;
    }
    return [nav.topViewController isKindOfClass:[ReactModuleViewController class]];
}

RCT_EXPORT_METHOD(launchMerchantApp:(int)appid
                  properties:(NSDictionary*)params
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSMutableDictionary *properties = [[ReactNativeAppManager sharedInstance] reactProperties:(int)appid];
        [properties addEntriesFromDictionary:params];
        NSArray *activeAppIds = [[ReactNativeAppManager sharedInstance] activeAppIds];
        if([activeAppIds containsObject:@(appid)]) {
            [[ZPAppFactory sharedInstance] openApp:appid
                                        moduleName:ReactModule_PaymentMain
                                        properties:properties];
            resolve(@{@"code:":@(1)});
        } else {
            resolve(@{@"code:":@(-1)});
        }
    });
}

RCT_EXPORT_METHOD(getContactByPhone:(NSString *)phoneNumber
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
    [self resolverGetContactByPhone:phoneNumber resolve:resolve];
}

- (NSDictionary *)userDictionaryShort:(ZPContactSwift *)user {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObjectCheckNil:user.displayName forKey:@"phonename"];
    [dict setObjectCheckNil:user.avatar forKey:@"avatar"];
    return dict;
}

- (void) resolverGetContactByPhone:(NSString*)phoneNumber resolve:(RCTPromiseResolveBlock)resolve{
    ZPContactSwift * info = [[ZPProfileManager shareInstance] getContactInfoByPhone:phoneNumber];
    if (!info.phoneNumber) {
        if (resolve) {
            NSDictionary *response = @{@"code":@(-1)};
            resolve(response);
        }
        return;
    }
    
    NSDictionary *dict = [self userDictionaryShort:info];
    if (resolve) {
        NSDictionary *response = @{@"code":@(1), @"data":dict};
        resolve(response);
    }

}


@end
