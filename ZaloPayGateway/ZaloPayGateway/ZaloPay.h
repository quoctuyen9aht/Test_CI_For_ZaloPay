//
//  ZaloPayGateway.h
//  ZaloPayGateway
//
//  Created by PhucPv on 5/19/16.
//  Copyright © 2016 PhucPv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface ZaloPay : NSObject <RCTBridgeModule>

@end
