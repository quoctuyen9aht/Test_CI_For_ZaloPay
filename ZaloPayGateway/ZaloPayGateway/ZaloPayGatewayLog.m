//
//  ZaloPayGatewayLog.m
//  ZaloPayGateway
//
//  Created by bonnpv on 6/8/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZaloPayGatewayLog.h"

const DDLogLevel ZaloPayGatewayLogLevel = DDLogLevelInfo;
