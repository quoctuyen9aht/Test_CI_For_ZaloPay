//
//  ZaloPayFeedbackCollector.h
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 12/28/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZPBaseCollector.h"
@interface ZPDynamicCollector : ZPBaseCollector
- (void)addFeedbackValue:(NSString *)value withKey:(NSString *)key;
@end
