//
//  ZPFeedbackPhotoViewController.h
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 1/8/17.
//  Copyright © 2017-present VNG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ZaloPayUI/BaseViewController.h>
@class ZPFeedbackPhotoViewController;

@protocol ZPFeedbackPhotoViewControllerDelegate <NSObject>
- (void)removePhoto:(ZPFeedbackPhotoViewController *)controller;
@end

@interface ZPFeedbackPhotoViewController : BaseViewController
@property (nonatomic) NSInteger index;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, weak) id<ZPFeedbackPhotoViewControllerDelegate>delegate;
@end
