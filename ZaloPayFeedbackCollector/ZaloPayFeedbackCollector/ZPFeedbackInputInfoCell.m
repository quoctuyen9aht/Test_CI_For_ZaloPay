//
//  ZPFeedbackInputInfoCell.m
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 1/2/17.
//  Copyright © 2017-present VNG. All rights reserved.
//

#import "ZPFeedbackInputInfoCell.h"
#import <ZaloPayCommon/ZPFloatTextInputView.h>
#import <ZaloPayCommon/MasonryConfig.h>
#import <ZaloPayCommon/UIFont+Extension.h>
#import <ReactiveObjC/ReactiveObjC.h>
#import <ZaloPayCommon/ZPConstants.h>
#import <ZaloPayCommon/R.h>

@interface ZPFeedbackInputInfoCell() <UITextFieldDelegate>
@property (nonatomic, strong) ZPFeedbackCellModel *model;
@property (nonatomic, strong) ZPFloatTextInputView *textField;
@end

@implementation ZPFeedbackInputInfoCell

- (void)setupCell {    
    self.textField = [[ZPFloatTextInputView alloc] init];
    self.textField.textField.font = [UIFont SFUITextRegularWithSize:16];
    self.textField.textField.delegate = self;
    [self.contentView addSubview:self.textField];
    
    float w = [UIScreen mainScreen].applicationFrame.size.width;
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.left.equalTo(0);
        make.width.equalTo(w);
        make.height.equalTo(60);
    }];
    
    @weakify(self);
    [[self.textField.textField rac_signalForControlEvents:UIControlEventEditingDidEndOnExit] subscribeNext:^(id x) {
        @strongify(self);
        [self.textField.textField resignFirstResponder];
    }];
    
    [[self.textField.textField rac_textSignal] subscribeNext:^(id x) {
        @strongify(self);
        self.model.value = x;
        [self.delegate feedbackCell:self valueDidChange:self.model];
    }];
}

- (void)setData:(ZPFeedbackCellModel *)item {
    self.model = item;
    self.textField.textField.placeholder = item.title;
    self.textField.userInteractionEnabled = item.allowEdit;
    if (item.value) {
        self.textField.textField.text = item.value;
    }
}

- (void)setSeparatorLeftInset:(float)leftInset {
    [super setSeparatorLeftInset:leftInset];
    self.textField.lineInsets = UIEdgeInsetsMake(0, leftInset, 0, 0);
}

- (void)showInputErrorMessage:(NSString *)message {
    [self.textField showErrorAuto:message];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (self.model.type  == ZPFeedback_Email) {
        NSInteger length = textField.text.length + string.length;
        return [self validEmailLength:length];
    }
    return true;
}

- (BOOL)validEmailLength:(NSInteger)length {
    if (length > EMAIL_LENGTH) {
        NSString *message = [NSString stringWithFormat:[R string_UpdateProfileLevel3_Invalid_EmailLength], EMAIL_LENGTH];
        [self.textField showErrorAuto:message];
        return false;
    }
    [self.textField clearErrorText];
    return true;
}

@end
