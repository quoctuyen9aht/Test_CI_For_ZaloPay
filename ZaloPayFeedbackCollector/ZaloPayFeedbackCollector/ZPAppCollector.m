//
//  ZPAppCollector.m
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 12/28/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPAppCollector.h"
#import <ZaloPayCommon/NSCollection+JSON.h>

@implementation ZPAppCollector
- (NSString *)collectorName {
    return @"appinfo";
}

- (NSDictionary *)collectorData {
    NSString *version = [NSString stringWithFormat:@"%@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
    NSString *build = [NSString stringWithFormat:@"%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObjectCheckNil:version forKey:@"version"];
    [dic setObjectCheckNil:build forKey:@"build_number"];
    return dic;
}
@end
