//
//  ZPFeedbackInputTextViewCell.m
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 1/5/17.
//  Copyright © 2017-present VNG. All rights reserved.
//

#import "ZPFeedbackInputTextViewCell.h"

#import <ZaloPayCommon/ZPFloatTextViewInputView.h>
#import <ZaloPayCommon/MasonryConfig.h>
#import <ZaloPayCommon/UIFont+Extension.h>
#import <ReactiveObjC/ReactiveObjC.h>
#import <ZaloPayCommon/ZPConstants.h>
#import <ZaloPayCommon/R.h>


@interface ZPFeedbackInputTextViewCell()<UITextViewDelegate>
@property (nonatomic, strong) ZPFeedbackCellModel *model;
@property (nonatomic, strong) ZPFloatTextViewInputView *textField;
@end
@implementation ZPFeedbackInputTextViewCell

- (void)dealloc {
    self.textField.textField.delegate = nil;
}

- (void)setupCell {
    self.textField = [[ZPFloatTextViewInputView alloc] init];
    self.textField.textField.font = [UIFont SFUITextRegularWithSize:16];
    [self.contentView addSubview:self.textField];
    
    float w = [UIScreen mainScreen].applicationFrame.size.width;
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.left.equalTo(0);
        make.width.equalTo(w);
        make.bottom.equalTo(0);
    }];
    self.textField.textField.delegate = self;
}

- (void)setData:(ZPFeedbackCellModel *)item {
    self.model = item;
    self.textField.textField.placeholder = item.title;
    self.textField.userInteractionEnabled = item.allowEdit;
    if (item.value) {
        self.textField.textField.text = item.value;
    }
}

- (void)setSeparatorLeftInset:(float)leftInset {
    [super setSeparatorLeftInset:leftInset];
    self.textField.lineInsets = UIEdgeInsetsMake(0, leftInset, 0, 0);
}

- (void)showInputErrorMessage:(NSString *)message {
    [self.textField showErrorAuto:message];
}

#pragma mark - Text View Delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    if (newString.length >= FEEDBACK_MESSAGE_LENGTH) {
        NSString *message = [NSString stringWithFormat:[R string_Feedback_Message_InvalidLength], FEEDBACK_MESSAGE_LENGTH];
        [self.textField showErrorAuto:message];
        return false;
    }
    return true;
}

- (void)textViewDidChange:(UITextView *)textView {
    self.model.value = textView.text;
    [self.delegate feedbackCell:self valueDidChange:self.model];
}

@end
