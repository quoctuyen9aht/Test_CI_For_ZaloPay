//
//  ZPFeedbackViewController.m
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 12/29/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPFeedbackViewController.h"
#import "ZPFeedbackModel.h"
#import "ZPFeedbackCategoryCell.h"
#import "ZPFeedbackInputInfoCell.h"
#import "ZPFeedbackOptionCell.h"
#import "ZPFeedbackPhotoCell.h"
#import "ZPFeedbackInputTextViewCell.h"
#import "ZPFeedbackViewController.h"
#import "ZPFeedbackViewController+SendEmail.h"

#import <ZaloPayCommon/UIView+Extention.h>
#import <ZaloPayCommon/UIButton+Extension.h>
#import <ZaloPayCommon/MasonryConfig.h>
#import <ZaloPayCommon/ZPConstants.h>
#import <ZaloPayCommon/UITableViewCell+Extension.h>
#import <ZaloPayCommon/UIFont+Extension.h>
#import <ZaloPayCommon/MasonryConfig.h>
#import <ZaloPayCommon/NSArray+Safe.h>
#import <ZaloPayCommon/UIColor+Extension.h>
#import <ZaloPayUI/BaseViewController+ZPScrollViewController.h>
#import <ZaloPayCommon/UIView+Config.h>
#import <ZaloPayCommon/R.h>


@interface ZPFeedbackViewController ()<ZPFeedbackCellDelegate, UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UIView *headerView1;
@property (nonatomic, strong) UIView *headerView3;
@property (nonatomic, strong) UILabel *labelPhotoCount;
@property (nonatomic, strong) UIButton *sendButton;
@property (nonatomic, strong) ZPFeedbackModel *model;
@end

@implementation ZPFeedbackViewController

- (id)initWithModel:(ZPFeedbackModel *)model {
    self = [super init];
    if (self) {
        self.model = model;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.title = [R string_Feedback_Title];
    self.title = [R string_Feedback_Error_Title];
    [self addTableView];
    [self addSendButton];
    [self registerHandleKeyboardNotification];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:false];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    if (self.navigationController.topViewController == self) {
        [self.navigationController setNavigationBarHidden:false];
    }
}

- (ZPFeedbackModel *)createModel {
    ZPFeedbackModel *model = [[ZPFeedbackModel alloc] init];
    [model configDataSource];
    return model;
}

- (void)addSendButton {
    self.sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:self.sendButton];
    [self.sendButton setupZaloPayButton];
    [self.sendButton setTitle:[R string_ButtonLabel_Send] forState:UIControlStateNormal];
    [self.sendButton addTarget:self action:@selector(sendButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.sendButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(10);
        make.right.equalTo(-10);
        make.height.equalTo(kZaloButtonHeight);
        make.top.equalTo(self.tableView.mas_bottom).with.offset([self buttonToScrollOffset]);
    }];
    self.sendButton.enabled = [self.model validData];
}

- (void)addTableView {
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.left.equalTo(0);
        make.right.equalTo(0);
        make.height.equalTo([self scrollHeight]);
    }];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

- (void)tapOnTableView {
    [self.tableView endEditing:true];
}

- (void)sendButtonClick {
    if (![self.model validEmailData]) {
        NSIndexPath *indexPath = [self.model emailIndexPath];
        if (indexPath) {
            [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionNone animated:false];
            ZPFeedbackInputInfoCell *emailCell = [self.tableView cellForRowAtIndexPath:indexPath];
            [emailCell showInputErrorMessage:[R string_UpdateProfileLevel3_Email_Invalid]];
        }
        return;
    }
    [self.tableView endEditing:true];
    NSArray *data = [self.model feedBackData];
    NSArray *images = [self.model screenShootData];
    NSString *bodyMessage = [self.model bodyMessage];
    [self sendEmail:data images:images bodyMessage:bodyMessage];
}

#pragma mark - Cell Delegate

- (void)feedbackCell:(ZPFeedbackCell *)cell valueDidChange:(ZPFeedbackCellModel*)model {
    
    if (model.type == ZPFeedback_ScreenShoot) {
        NSArray *photos = (NSArray *)model.value;
        self.labelPhotoCount.text = [NSString stringWithFormat:[R string_Feedback_ScreenShoot] , photos.count];
        return;
    }
    
    if (model.type == ZPFeedback_Email ||
        model.type == ZPFeedback_Description) {
        self.sendButton.enabled = [self.model validData];
    }
}

#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.model.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *arr = self.model.dataSource[section];
    return  arr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *array = [self.model.dataSource safeObjectAtIndex:indexPath.section];
    ZPFeedbackCellModel *item = [array safeObjectAtIndex:indexPath.row];
    if (item.type == ZPFeedback_ScreenShoot) {
        return [ZPFeedbackPhotoCell height];
    }
    if (item.type == ZPFeedback_Description) {
        return 80;
    }
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *array = [self.model.dataSource safeObjectAtIndex:indexPath.section];
    ZPFeedbackCellModel *item = [array safeObjectAtIndex:indexPath.row];
    Class cellClass = [self cellClassFromType:item.type];
    ZPFeedbackCell *cell = [cellClass defaultCellForTableView:tableView];
    cell.delegate = self;
    [cell setData:item];
    return cell;
}

- (Class)cellClassFromType:(ZPFeedbackType)type {
    switch (type) {
        case ZPFeedback_Category:
            return [ZPFeedbackCategoryCell class];
        case ZPFeedback_Email:
        case ZPFeedback_TransactionID:
            return [ZPFeedbackInputInfoCell class];
        case ZPFeedback_UserInfo:
        case ZPFeedback_AppInfo:
        case ZPFeedback_DeviceInfo:
            return [ZPFeedbackOptionCell class];
        case ZPFeedback_ScreenShoot:
            return [ZPFeedbackPhotoCell class];
        case ZPFeedback_Description:
            return [ZPFeedbackInputTextViewCell class];
        default:
            return [ZPFeedbackCell class];
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    [tableView endEditing:true];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    float left = 0;
    
    if (indexPath.section == 0) {
        NSArray *data = [self.model.dataSource safeObjectAtIndex:indexPath.section];
        if (indexPath.row != data.count - 1) {
            left = 10;
        }
    }
    ZPFeedbackCell *feedbakCell = (ZPFeedbackCell *)cell;
    [feedbakCell setSeparatorLeftInset:left];    
}


#pragma mark - TableView Header

- (UIView *)headerView1 {
    if (!_headerView1) {
        _headerView1 = [[UIView alloc] init];
        UILabel *label = [[UILabel alloc] init];
        label.numberOfLines = 0;
        label.text = [R string_Feedback_Contact_CSKH];
        label.font = [UIFont SFUITextRegularWithSize:14];
        label.textColor = [UIColor subText];
        
        [_headerView1 addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(10);
            make.right.equalTo(-10);
            make.top.equalTo(15);
            make.bottom.equalTo(-5);
        }];
    }
    return _headerView1;
}

- (UIView *)headerView3 {
    if (!_headerView3) {
        _headerView3 = [[UIView alloc] init];
        UILabel *label = [[UILabel alloc] init];
        label.text = [NSString stringWithFormat:[R string_Feedback_ScreenShoot] , 0];
        label.font = [UIFont SFUITextRegularWithSize:14];
        label.textColor = [UIColor subText];
        [_headerView3 addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(10);
            make.right.equalTo(-10);
            make.top.equalTo(15);
            make.bottom.equalTo(-5);
        }];
        self.labelPhotoCount = label;
    }
    return _headerView3;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return self.headerView1;
    }
    if (section == 2) {
        return self.headerView3;
    }
    return [[UIView alloc] init];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        return 10.0;
    }
    if (section == 0 && [UIView isScreen2x]) {
        return 55;
    }    
    return 50;
}

#pragma mark - Footer View

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return  0.1;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

#pragma mark - Keyboard

- (UIScrollView *)internalScrollView {
    return self.tableView;
}

- (UIButton *)internalBottomButton {
    return self.sendButton;
}

- (float)scrollHeight {
    return [UIScreen mainScreen].bounds.size.height - 64 - kZaloButtonHeight - 20;
}

- (float)buttonToScrollOffset {
    return  10;
}


@end
