//
//  ZPFeedbackCellModel.h
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 12/29/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    ZPFeedback_Category = 1,
    ZPFeedback_Description,
    ZPFeedback_TransactionID,
    ZPFeedback_Email,
    ZPFeedback_UserInfo,
    ZPFeedback_DeviceInfo,
    ZPFeedback_AppInfo,
    ZPFeedback_ScreenShoot,
} ZPFeedbackType;

@interface ZPFeedbackCellModel : NSObject
@property (nonatomic) ZPFeedbackType type;
@property (nonatomic) BOOL allowEdit;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) id value;
@property (nonatomic, strong) NSArray *categorys;
+ (instancetype)modelWithType:(ZPFeedbackType)type;
@end
