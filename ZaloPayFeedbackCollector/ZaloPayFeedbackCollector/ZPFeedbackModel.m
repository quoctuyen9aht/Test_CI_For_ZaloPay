//
//  ZPFeedbackModel.m
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 12/29/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPFeedbackModel.h"
#import "ZPDynamicCollector.h"
#import "ZPAppCollector.h"
#import "ZPUserCollector.h"
#import "ZPDeviceCollector.h"

#import <ZaloPayCommon/ZPConstants.h>
#import <ZaloPayCommon/R.h>

#import <ZaloPayAppManager/PaymentReactNativeApp/ReactAppModel.h>
#import <ZaloPayAppManager/PaymentReactNativeApp/ReactNativeAppManager.h>
#import <ZaloPayCommon/NSString+Validation.h>
#import <ZaloPayCommon/NSCollection+JSON.h>
#import "ZPFeedbackCellModel.h"
#import <ZaloPayProfile/ZaloPayProfile-Swift.h>
@interface ZPFeedbackModel()
@end
@implementation ZPFeedbackModel

- (void)configDataSource {
    self.dataSource = @[[self firtDataSoucre], [self secondDataSoucre], [self thirdDataSource:nil]];
}

- (NSArray *)firtDataSoucre {
    ZPFeedbackCellModel *category = [ZPFeedbackCellModel modelWithType:ZPFeedback_Category];
    category.title = [R string_Feedback_Categorys];
    category.categorys = [self allCategorys];
    
    ZPFeedbackCellModel *descriptionData = [ZPFeedbackCellModel modelWithType:ZPFeedback_Description];
    descriptionData.title = [R string_Feedback_Description];
    
    NSString *strEmail = [ZPProfileManager shareInstance].email;
    ZPFeedbackCellModel *emailData = [ZPFeedbackCellModel modelWithType:ZPFeedback_Email];
    emailData.title = strEmail.length > 0 ? [R string_Feedback_Email]: [R string_Feedback_Email_Require];
    emailData.value = strEmail;
    emailData.allowEdit = strEmail.length == 0;
    return @[category,emailData, descriptionData];
}

- (NSArray *)secondDataSoucre {
    ZPFeedbackCellModel *userInfo = [ZPFeedbackCellModel modelWithType:ZPFeedback_UserInfo];
    userInfo.title = [R string_Feedback_UserInfo];
    userInfo.value = @(true);
    
    ZPFeedbackCellModel *deviceInfo = [ZPFeedbackCellModel modelWithType:ZPFeedback_DeviceInfo];
    deviceInfo.title = [R string_Feedback_DeviceInfo];
    deviceInfo.value = @(true);
    
    ZPFeedbackCellModel *appInfo = [ZPFeedbackCellModel modelWithType:ZPFeedback_AppInfo];
    appInfo.title = [R string_Feedback_AppInfo];
    appInfo.value = @(true);    
    return @[userInfo, deviceInfo, appInfo];
}

- (NSArray *)thirdDataSource:(UIImage *)image {
    ZPFeedbackCellModel *screen = [ZPFeedbackCellModel modelWithType:ZPFeedback_ScreenShoot];
    NSMutableArray *value = [NSMutableArray array];
    [value addObjectNotNil:image];
    screen.value = value;
    return @[screen];
}

- (NSArray *)allCategorys {
    NSMutableArray *array = [NSMutableArray array];
    [array addObject:[R string_LeftMenu_LinkCard]];
    [array addObject:[R string_LeftMenu_Transfer]];
    [array addObject:[R string_LeftMenu_PayBill]];
    [array addObject:[R string_Feedback_Withdraw]];
    [array addObject:[R string_Home_ReceiveMoney]];
    
    NSArray *activeApp = [ReactNativeAppManager sharedInstance].activeAppIds;
    for (NSNumber *appId in activeApp) {
        if ([appId integerValue] == reactInternalAppId ||
            [appId integerValue] == showshowAppId) {
            continue;
        }
        ReactAppModel *app = [[ReactNativeAppManager sharedInstance] getApp:[appId integerValue]];
        if (app.appname.length > 0) {
            [array addObject:app.appname];
        }
    }
    return array;
}


- (NSIndexPath *)emailIndexPath {
    for (int i = 0; i < self.dataSource.count; i++) {
        NSArray *section = self.dataSource[i];
        for (int j = 0; j < section.count; j++) {
            ZPFeedbackCellModel *model = section[j];
            if (model.type == ZPFeedback_Email) {
                return [NSIndexPath indexPathForRow:j inSection:i];
            }
        }
    }
    return nil;
}

- (BOOL)validData {
    NSString *email = [self userEmail];
    return email.length > 0;
}

- (BOOL)validEmailData {
    NSString *email = [self userEmail];
    return [email isValidEmail];
}

- (ZPDynamicCollector *)dynamicFeedBack {
    if (!_dynamicFeedBack) {
        _dynamicFeedBack = [[ZPDynamicCollector alloc] init];
    }
    return _dynamicFeedBack;
}

- (NSArray *)feedBackData {
    NSMutableArray *allCollector  = [NSMutableArray array];
    ZPDynamicCollector *dynamic = [self dynamicFeedBack];
    [allCollector addObject:dynamic];
    
    for (NSArray *arrayModel in self.dataSource) {
        for (ZPFeedbackCellModel *model in arrayModel) {
            if (model.type == ZPFeedback_Category) {
                [dynamic addFeedbackValue:model.value withKey:@"category"];
                continue;
            }
            if (model.type == ZPFeedback_Description) {
                [dynamic addFeedbackValue:model.value withKey:@"description"];
                continue;
            }
            if (model.type == ZPFeedback_Email) {
                [dynamic addFeedbackValue:model.value withKey:@"email"];
                continue;
            }
            if (model.type == ZPFeedback_TransactionID) {
                [dynamic addFeedbackValue:model.value withKey:@"transactionID"];
                continue;
            }
            if (model.type == ZPFeedback_UserInfo) {
                if ([model.value boolValue]) {
                    [allCollector addObject:[[ZPUserCollector alloc] init]];
                }
                continue;
            }
            if (model.type == ZPFeedback_DeviceInfo) {
                if ([model.value boolValue]) {
                    [allCollector addObject:[[ZPDeviceCollector alloc] init]];
                }
                continue;
            }
            if (model.type == ZPFeedback_AppInfo) {
                if ([model.value boolValue]) {
                    [allCollector addObject:[[ZPAppCollector alloc] init]];
                }
                continue;
            }
        }
    }
    
    NSMutableArray *jsonData = [NSMutableArray array];
    for (ZPBaseCollector *collector in allCollector) {
        [jsonData addObject:[collector data]];
    }
    return jsonData;
}

- (NSArray *)screenShootData {
    ZPFeedbackCellModel *screenShoot = [self findModelWithType:ZPFeedback_ScreenShoot];
    return screenShoot.value;
}

- (NSString *)bodyMessage {
    ZPFeedbackCellModel *description = [self findModelWithType:ZPFeedback_Description];
    return description.value;
}

- (NSString *)userEmail {
    ZPFeedbackCellModel *email = [self findModelWithType:ZPFeedback_Email];
    return email.value;
}

- (ZPFeedbackCellModel *)findModelWithType:(ZPFeedbackType)type {
    for (NSArray *arrayModel in self.dataSource) {
        for (ZPFeedbackCellModel *model in arrayModel) {
            if (model.type == type) {
                return model;
            }
        }
    }
    return nil;
}

@end
