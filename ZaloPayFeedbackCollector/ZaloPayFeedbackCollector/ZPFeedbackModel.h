//
//  ZPFeedbackModel.h
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 12/29/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@class ZPDynamicCollector;
@interface ZPFeedbackModel : NSObject
@property (nonatomic, strong) ZPDynamicCollector *dynamicFeedBack;
@property (nonatomic, strong) NSArray *dataSource;
- (void)configDataSource;
- (BOOL)validData;
- (BOOL)validEmailData;
- (NSIndexPath *)emailIndexPath;
- (NSArray *)allCategorys;
- (NSArray *)feedBackData;
- (NSArray *)screenShootData;
- (NSString *)bodyMessage;
- (NSArray *)secondDataSoucre;
- (NSArray *)thirdDataSource:(UIImage *)image;
@end
