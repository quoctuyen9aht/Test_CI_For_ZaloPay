//
//  ZPFeedbackSelectCategoryViewController.m
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 1/11/17.
//  Copyright © 2017-present VNG. All rights reserved.
//

#import "ZPFeedbackSelectCategoryViewController.h"
#import <ZaloPayCommon/MasonryConfig.h>
#import <ZaloPayCommon/UITableViewCell+Extension.h>
#import <ZaloPayCommon/ZPConstants.h>
#import <ZaloPayCommon/R.h>
#import <ZaloPayCommon/UIFont+Extension.h>
#import <ZaloPayCommon/UIColor+Extension.h>

@interface ZPFeedbackSelectCategoryViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *dataSource;
@end

@implementation ZPFeedbackSelectCategoryViewController

- (id)initWithData:(NSArray *)data {
    self = [super init];
    if (self) {
        self.dataSource = data;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = [R string_Feedback_Categorys];
    [self addTableView];
}

- (void)addTableView {
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.left.equalTo(0);
        make.right.equalTo(0);
        make.bottom.equalTo(0);
    }];
    self.tableView.backgroundColor = [UIColor defaultBackground];
//    self.tableView.separatorColor = [UIColor lineColor];
    self.tableView.rowHeight = 60;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [UITableViewCell defaultCellForTableView:tableView];
    NSString *text = self.dataSource[indexPath.row];
    cell.textLabel.text = text;
    cell.textLabel.font = [UIFont SFUITextRegularWithSize:16];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    NSString *text = self.dataSource[indexPath.row];
    [self.delegate categoryController:self selectData:text];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *_headerView3 = [[UIView alloc] init];
    UILabel *label = [[UILabel alloc] init];
    label.text = [R string_Feedback_Select_Category];
    label.font = [UIFont SFUITextRegularWithSize:14];
    label.textColor = [UIColor subText];
    [_headerView3 addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(10);
        make.right.equalTo(-10);
        make.top.equalTo(15);
        make.bottom.equalTo(-5);
    }];
    return _headerView3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50;
}

@end
