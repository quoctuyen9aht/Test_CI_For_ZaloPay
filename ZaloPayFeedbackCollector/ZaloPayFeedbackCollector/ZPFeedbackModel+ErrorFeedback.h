//
//  ZPFeatureDataModel+ErrorFeedback.h
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 5/4/17.
//  Copyright © 2017-present VNG. All rights reserved.
//

#import "ZPFeedbackModel.h"

@interface ZPFeedbackModel (ErrorFeedback)
- (void)configDataSourceWithCategory:(NSString *)cagegory
                       transactionId:(NSString *)transactionId
                               image:(UIImage *)image
                             message:(NSString *)message
                           errorCode:(NSNumber *)errorCode;
@end
