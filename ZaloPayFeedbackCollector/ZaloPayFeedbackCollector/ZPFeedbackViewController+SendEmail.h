//
//  ZPFeedbackViewController+SendEmail.h
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 1/5/17.
//  Copyright © 2017-present VNG. All rights reserved.
//

#import "ZPFeedbackViewController.h"
#import <MessageUI/MessageUI.h>

@interface ZPFeedbackViewController (SendEmail)<MFMailComposeViewControllerDelegate>
- (void)sendEmail:(NSArray *)data images:(NSArray *)images bodyMessage:(NSString *)message;
@end
