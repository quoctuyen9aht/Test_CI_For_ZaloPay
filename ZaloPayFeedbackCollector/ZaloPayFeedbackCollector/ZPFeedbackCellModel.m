//
//  ZPFeedbackCellModel.m
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 12/29/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPFeedbackCellModel.h"

@implementation ZPFeedbackCellModel
+ (instancetype)modelWithType:(ZPFeedbackType)type {
    ZPFeedbackCellModel *_return = [[self alloc] init];
    _return.type = type;
    _return.allowEdit = true;
    return _return;
}
@end
