//
//  ZaloPayFeedbackCollector.h
//  ZaloPayFeedbackCollector
//
//  Created by Phuoc Huynh on 3/22/18.
//  Copyright © 2018 VNG. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ZaloPayFeedbackCollector.
FOUNDATION_EXPORT double ZaloPayFeedbackCollectorVersionNumber;

//! Project version string for ZaloPayFeedbackCollector.
FOUNDATION_EXPORT const unsigned char ZaloPayFeedbackCollectorVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ZaloPayFeedbackCollector/PublicHeader.h>


