//
//  ZPFeedbakCategoryCell.h
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 12/29/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZPFeedbackCell.h"

@interface ZPFeedbackCategoryCell : ZPFeedbackCell

@end
