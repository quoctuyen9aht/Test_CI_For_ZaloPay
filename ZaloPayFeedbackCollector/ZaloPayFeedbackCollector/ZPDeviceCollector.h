//
//  ZPDeviceCollector.h
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 12/28/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZPBaseCollector.h"

@interface ZPDeviceCollector : ZPBaseCollector

@end
