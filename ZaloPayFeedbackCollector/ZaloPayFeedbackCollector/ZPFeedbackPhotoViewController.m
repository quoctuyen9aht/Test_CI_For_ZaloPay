//
//  ZPFeedbackPhotoViewController.m
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 1/8/17.
//  Copyright © 2017-present VNG. All rights reserved.
//

#import "ZPFeedbackPhotoViewController.h"
#import <ZaloPayCommon/MasonryConfig.h>
#import <ZaloPayCommon/UIColor+Extension.h>
#import <ZaloPayCommon/UIFont+Extension.h>
#import <ZaloPayCommon/ZPConstants.h>
#import <ZaloPayCommon/R.h>


@interface ZPFeedbackPhotoViewController ()
@property (nonatomic, strong) UIImageView *imageView;
@end

@implementation ZPFeedbackPhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addImageView];
    [self addBottomBar];
    self.view.backgroundColor = [UIColor hex_0x333333];
}

- (IBAction)backButtonClicked:(id)sender {
    [self closeButtonClick];
}

- (void)addImageView {
    self.imageView = [[UIImageView alloc] initWithImage:self.image];
    self.imageView.clipsToBounds = true;
    [self.view addSubview:self.imageView];
    
    float maxh = [UIScreen mainScreen].bounds.size.height - 64 - 45 - 40;
    float maxw = [UIScreen mainScreen].bounds.size.width - 40;
    
    float h = maxh;
    float w = (maxh * self.image.size.width) / self.image.size.height;
    if (w > maxw) {
        w = maxw;
        h = (maxw * self.image.size.height) / self.image.size.width;
    }
    
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.view.mas_centerY).with.offset(-22.5);
        make.centerX.equalTo(self.view.mas_centerX);
        make.width.equalTo(w);
        make.height.equalTo(h);
    }];
}

- (void)addBottomBar {
    UIView *_return = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].applicationFrame.size.width, 44)];
    _return.backgroundColor = [UIColor hex_0x2b2b2b];
    UIButton *btClose = [UIButton buttonWithType:UIButtonTypeCustom];
    [btClose setTitle:[R string_ButtonLabel_Cancel] forState:UIControlStateNormal];
    [btClose setTitleColor:[UIColor hex_0xacb3ba] forState:UIControlStateNormal];
    btClose.titleLabel.font = [UIFont SFUITextMediumWithSize:16];
    [_return addSubview:btClose];
    [btClose mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(0);
        make.width.equalTo(60);
        make.top.equalTo(0);
        make.bottom.equalTo(0);
    }];
    [btClose addTarget:self action:@selector(closeButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton *btDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [btDone setTitle:[R string_ButtonLabel_Remove] forState:UIControlStateNormal];
    [btDone setTitleColor:[UIColor hex_0xacb3ba] forState:UIControlStateNormal];
    btDone.titleLabel.font = [UIFont SFUITextMediumWithSize:16];
    [_return addSubview:btDone];
    [btDone mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(0);
        make.width.equalTo(60);
        make.top.equalTo(0);
        make.bottom.equalTo(0);
    }];
    [btDone addTarget:self action:@selector(removeButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_return];
    [_return mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(0);
        make.right.equalTo(0);
        make.bottom.equalTo(0);
        make.height.equalTo(44);
    }];
}

- (void)closeButtonClick {
    [self.navigationController popViewControllerAnimated:false];
}

- (void)removeButtonClick {
    [self.delegate removePhoto:self];
    [self closeButtonClick];
}

@end
