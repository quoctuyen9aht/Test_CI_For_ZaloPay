//
//  ZPFeatureDataModel+ErrorFeedback.m
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 5/4/17.
//  Copyright © 2017-present VNG. All rights reserved.
//

#import "ZPFeedbackModel+ErrorFeedback.h"
#import "ZPFeedbackCellModel.h"
#import "ZPDynamicCollector.h"
#import <ZaloPayCommon/ZPConstants.h>
#import <ZaloPayCommon/NSCollection+JSON.h>
#import <ZaloPayCommon/R.h>
#import <ZaloPayProfile/ZaloPayProfile-Swift.h>

@implementation ZPFeedbackModel (ErrorFeedback)
- (void)configDataSourceWithCategory:(NSString *)cagegory
                       transactionId:(NSString *)transactionId
                               image:(UIImage *)image
                             message:(NSString *)message
                           errorCode:(NSNumber *)errorCode {
    
    [self.dynamicFeedBack addFeedbackValue:[errorCode stringValue] withKey:@"erorrcode"];
    NSArray *firt = [self firtDataSoucreCategory:cagegory transactionId:transactionId message:message];
    NSArray *second = [self secondDataSoucre];
    NSArray *third = [self thirdDataSource:image];
    self.dataSource = @[firt, second, third];
}


- (NSArray *)firtDataSoucreCategory:(NSString *)categoryTitle
                      transactionId:(NSString *)transactionId
                            message:(NSString *)message{
    
    ZPFeedbackCellModel *category = [ZPFeedbackCellModel modelWithType:ZPFeedback_Category];
    if (categoryTitle.length > 0) {
        category.title = categoryTitle;
        category.value = categoryTitle;
        category.allowEdit = false;
    }else {
        category.title = [R string_Feedback_Categorys];
        category.categorys = [self allCategorys];
    }
    
    ZPFeedbackCellModel *descriptionData = [ZPFeedbackCellModel modelWithType:ZPFeedback_Description];
    descriptionData.title = [R string_Feedback_Error_Description];
    descriptionData.value = message;
    descriptionData.allowEdit = message.length == 0;
    
    NSString *strEmail = [ZPProfileManager shareInstance].email;
    ZPFeedbackCellModel *emailData = [ZPFeedbackCellModel modelWithType:ZPFeedback_Email];
    emailData.title = strEmail.length > 0 ? [R string_Feedback_Email]: [R string_Feedback_Email_Require];
    emailData.value = strEmail;
    emailData.allowEdit = strEmail.length == 0;
    
    ZPFeedbackCellModel *transaction = [ZPFeedbackCellModel modelWithType:ZPFeedback_TransactionID];
    transaction.title = [R string_Feedback_TransactionId];
    transaction.value = transactionId;
    transaction.allowEdit = false;
    return @[category, transaction, emailData, descriptionData];
}

@end
