//
//  ZPFeedbackPhotoCell.h
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 1/2/17.
//  Copyright © 2017-present VNG. All rights reserved.
//

#import "ZPFeedbackCell.h"

@interface ZPFeedbackPhotoCell : ZPFeedbackCell
+ (float)height;
@end
