//
//  ZaloPayFeedbackCollector.m
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 12/28/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPDynamicCollector.h"

@interface ZPDynamicCollector()
@property (nonatomic, strong) NSMutableDictionary *data;
@end

@implementation ZPDynamicCollector

- (id)init {
    self = [super init];
    if (self) {
        self.data = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void)addFeedbackValue:(NSString *)value withKey:(NSString *)key {
    if (value && key) {
        [self.data setObject:value forKey:key];
    }
}

- (NSString *)collectorName {
    return @"dynamicinfo";
}

- (NSDictionary *)collectorData {
    return self.data;
}


@end
