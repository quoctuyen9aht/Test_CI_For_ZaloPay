//
//  ZPFeedbackViewController.h
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 12/29/16.
//  Copyright © 2016-present VNG. All rights reserved.
//


#import <ZaloPayUI/BaseViewController.h>
@class ZPFeedbackModel;
@interface ZPFeedbackViewController : BaseViewController
@property (nonatomic, strong) UITableView *tableView;
- (id)initWithModel:(ZPFeedbackModel *)model;
@end
