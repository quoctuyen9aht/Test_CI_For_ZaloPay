//
//  ZPFeedbakCategoryCell.m
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 12/29/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPFeedbackCategoryCell.h"
#import "ZPFeedbackCellModel.h"
#import "ZPFeedbackSelectCategoryViewController.h"
#import <ZaloPayCommon/MasonryConfig.h>
#import <ZaloPayCommon/UIFont+Extension.h>
#import <ZaloPayCommon/ZPConstants.h>
#import <ZaloPayCommon/R.h>
#import <ZaloPayCommon/UIColor+Extension.h>
#import <ZaloPayCommon/UIView+Extention.h>
#import <ZaloPayCommon/UILabel+IconFont.h>
#import <ZaloPayCommon/UIFont+IconFont.h>
#import <ZaloPayCommon/UIView+Extention.h>

@interface ZPFeedbackCategoryCell()<ZPFeedbackSelectCategoryViewControllerDelegate>
@property (nonatomic, strong) UIButton *buttonSelect;
@property (nonatomic, strong) ZPFeedbackCellModel *model;
@property (nonatomic, strong) UILabel *rightArrow;
@end

@implementation ZPFeedbackCategoryCell


- (void)setupCell {    
    [self addLabel];
    [self rightArrow];
}

- (UILabel *)rightArrow {
    if (!_rightArrow) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 13, 13)];
        label.font = [UIFont iconFontWithSize:label.frame.size.width];
        [label setIconfont:@"general_arrowright"];
        label.textColor = [UIColor lightGrayColor];
        _rightArrow = label;
        [self.contentView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.mas_centerY);
            make.size.equalTo(label.frame.size);
            make.right.equalTo(-10);
        }];
    }
    return _rightArrow;
}

- (void)addLabel {
    self.buttonSelect = [UIButton buttonWithType:UIButtonTypeCustom];
    self.buttonSelect.userInteractionEnabled = false;
    self.buttonSelect.titleLabel.font = [UIFont SFUITextRegularWithSize:16];
    [self.contentView addSubview:self.buttonSelect];
    [self.buttonSelect mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.left.equalTo(10);
        make.bottom.equalTo(0);
        make.right.equalTo(0);
    }];
    [self.buttonSelect setTitleColor:[UIColor defaultText] forState:UIControlStateNormal];
    [self.buttonSelect setTitle:[R string_Feedback_Categorys] forState:UIControlStateNormal];
    self.buttonSelect.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.buttonSelect addTarget:self action:@selector(selectButtonClick) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setData:(ZPFeedbackCellModel *)item {
    self.model = item;
    [self.buttonSelect setTitle:item.title forState:UIControlStateNormal];
    self.buttonSelect.userInteractionEnabled = item.allowEdit;
    self.rightArrow.hidden = !item.allowEdit;
}

- (void)selectButtonClick {
    ZPFeedbackSelectCategoryViewController *category = [[ZPFeedbackSelectCategoryViewController alloc] initWithData:self.model.categorys];
    category.delegate = self;
    [self.viewController.navigationController pushViewController:category animated:true];
}

- (void)categoryController:(ZPFeedbackSelectCategoryViewController *)controller selectData:(NSString *)data {
    self.model.value = data;
    [self.buttonSelect setTitle:data forState:UIControlStateNormal];
    [controller.navigationController popViewControllerAnimated:true];
}
@end
