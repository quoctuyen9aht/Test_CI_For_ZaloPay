//
//  ZPUserCollector.m
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 12/28/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPUserCollector.h"
#import <ZaloPayCommon/NSCollection+JSON.h>
#import <ReactiveObjC/ReactiveObjC.h>
#import <ZaloPayProfile/ZaloPayProfile-Swift.h>

@implementation ZPUserCollector

- (NSString *)collectorName {
    return @"userinfo";
}

- (NSDictionary *)collectorData {
    ZaloUserSwift *user = [ZPProfileManager shareInstance].currentZaloUser;
    NSString *accountName = [ZPProfileManager shareInstance].userLoginData.accountName;
    NSString *zaloPayId = [ZPProfileManager shareInstance].userLoginData.paymentUserId;

    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObjectCheckNil:user.userId forKey:@"zaloid"];
    [dic setObjectCheckNil:zaloPayId forKey:@"zpid"];
    [dic setObjectCheckNil:user.displayName forKey:@"display_name"];
    [dic setObjectCheckNil:user.avatar forKey:@"avatar"];
    [dic setObjectCheckNil:accountName forKey:@"zalopayid"];
    return dic;
}

@end
