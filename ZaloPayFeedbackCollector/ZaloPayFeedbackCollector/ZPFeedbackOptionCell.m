//
//  ZPFeedbackOptionCell.m
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 1/2/17.
//  Copyright © 2017-present VNG. All rights reserved.
//

#import "ZPFeedbackOptionCell.h"
#import <ZaloPayCommon/MasonryConfig.h>
#import <ZaloPayCommon/UIFont+Extension.h>
#import <ZaloPayCommon/UIColor+Extension.h>

@interface ZPFeedbackOptionCell()
@property (nonatomic, strong) ZPFeedbackCellModel *model;
@property (nonatomic, strong) UILabel *labelTitle;
@property (nonatomic, strong) UISwitch *switchControl;
@end
@implementation ZPFeedbackOptionCell

- (void)setupCell {    
    self.switchControl = [[UISwitch alloc] init];
    [self.contentView addSubview:self.switchControl];
    [self.switchControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(-10);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.size.equalTo(self.switchControl.frame.size);
    }];
    [self.switchControl addTarget:self action:@selector(swithchDidChange) forControlEvents:UIControlEventValueChanged];
}

- (UILabel *)labelTitle {
    if (!_labelTitle) {
        _labelTitle = [[UILabel alloc] init];
        _labelTitle.font = [UIFont SFUITextRegularWithSize:16];
        _labelTitle.textColor = [UIColor defaultText];
        [self.contentView addSubview:_labelTitle];
        [_labelTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(10);
            make.right.equalTo(-50);
            make.top.equalTo(0);
            make.bottom.equalTo(0);
        }];
    }
    return _labelTitle;
}

- (void)setData:(ZPFeedbackCellModel *)item {
    self.model = item;
    self.labelTitle.text = item.title;
    NSNumber *value = self.model.value;
    self.switchControl.on = [value boolValue];        
}

- (void) swithchDidChange {
    self.model.value = @(self.switchControl.on);
}


@end
