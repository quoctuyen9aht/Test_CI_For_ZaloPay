//
//  ZPFeedbackPhotoCell.m
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 1/2/17.
//  Copyright © 2017-present VNG. All rights reserved.
//

#import "ZPFeedbackPhotoCell.h"
#import "ZaloPayPhotoPicker.h"
#import "ZPFeedbackPhotoViewController.h"

#import <ZaloPayCommon/MasonryConfig.h>
#import <ZaloPayCommon/UIFont+IconFont.h>
#import <ZaloPayCommon/UILabel+IconFont.h>
#import <ZaloPayCommon/UIButton+IconFont.h>
#import <ZaloPayCommon/UIColor+Extension.h>
#import <ZaloPayUI/ZPAppFactory.h>
#import <ZaloPayCommon/UIView+Extention.h>
#import <ZaloPayCommon/NSArray+Safe.h>

@interface ZPFeedbackScreenShootView : UIControl
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *labelIcon;
@property (nonatomic, strong) UIButton *closeButton;
@end

@implementation ZPFeedbackScreenShootView

+ (float)width {
    return ([UIScreen mainScreen].applicationFrame.size.width - 5 * [self border]) / 4;
}

+ (float)border {
    return 12.0;
}

- (id)init {
    self = [super init];
    if (self) {
        [self setupView];
    }
    return self;
}

- (void)setupView {
    float w = [ZPFeedbackScreenShootView width];
    self.labelIcon = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    self.labelIcon.font = [UIFont iconFontWithSize:self.labelIcon.frame.size.width];
    [self.labelIcon setIconfont:@"profile_camera"];
    [self.labelIcon setTextColor:[UIColor subText]];
    self.labelIcon.userInteractionEnabled = false;
    [self addSubview:self.labelIcon];
    [self.labelIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(self.labelIcon.frame.size);
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY).with.offset(-2);
    }];
    
    self.imageView = [[UIImageView alloc] init];
    self.imageView.userInteractionEnabled = false;
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:self.imageView];
    self.imageView.clipsToBounds = YES;
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.left.equalTo(0);
        make.width.equalTo(w);
        make.height.equalTo(w);
    }];
    
    self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.closeButton.hidden = true;
    self.closeButton.titleLabel.font = [UIFont iconFontWithSize:20];
    [self.closeButton setIconFont:@"general_del" forState:UIControlStateNormal];
    [self.closeButton setTitleColor:[UIColor defaultAppIconColor] forState:UIControlStateNormal];
    self.closeButton.backgroundColor = [UIColor whiteColor];
    [self.closeButton roundRect:10];
    
    self.layer.borderWidth = 0.5;
    self.layer.borderColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.7].CGColor;
}

- (void)setImage:(UIImage *)image {
    if (!image) {
        [self removeImage];
        return;
    }
    self.imageView.image = image;
    self.closeButton.hidden = NO;
    self.labelIcon.hidden = YES;
}

- (void)removeImage {
    self.imageView.image = nil;
    self.closeButton.hidden = YES;
    self.labelIcon.hidden = NO;
}
@end

@interface ZPFeedbackPhotoCell ()<ZPFeedbackPhotoViewControllerDelegate>
@property (nonatomic, strong) ZPFeedbackCellModel *model;
@property (nonatomic, strong) ZaloPayPhotoPicker *picker;
@property (nonatomic, strong) NSMutableArray *allScreenShootView;
@end


@implementation ZPFeedbackPhotoCell

+ (float)height {
    float height = [ZPFeedbackScreenShootView width];
    float border = [ZPFeedbackScreenShootView border];
    return height + 2 * border;
}

- (void)setupCell {
    float width = [ZPFeedbackScreenShootView width];
    
    float border = [ZPFeedbackScreenShootView border];
    self.allScreenShootView = [NSMutableArray array];
    
    for (int i = 0; i < 4;  i++ ) {
        ZPFeedbackScreenShootView *oneView = [[ZPFeedbackScreenShootView alloc] init];
        [self.allScreenShootView addObject:oneView];
        oneView.tag = i;
        oneView.closeButton.tag = i;
        oneView.hidden = i > 0;
        [self.contentView addSubview:oneView];
        float left = (i + 1) * border + i* width;
        [oneView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(left);
            make.top.equalTo(border);
            make.height.equalTo(width);
            make.width.equalTo(width);
        }];
        
        [self.contentView addSubview:oneView.closeButton];
        [oneView.closeButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(20);
            make.height.equalTo(20);
            make.centerX.equalTo(oneView.mas_right).with.offset(-2);
            make.centerY.equalTo(oneView.mas_top).with.offset(2);
        }];
        
        [oneView addTarget:self action:@selector(selectPhotoButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [oneView.closeButton addTarget:self action:@selector(removePhotoButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)setData:(ZPFeedbackCellModel *)item {
    self.model = item;
    [self showAllImage];
}

- (ZaloPayPhotoPicker *)picker {
    if (!_picker) {
        _picker = [[ZaloPayPhotoPicker alloc] init];
        _picker.allowEdit = false;
    }
    return _picker;
}

- (IBAction)selectPhotoButtonClick:(id)sender {
    [[self superTableView] endEditing:YES];
    
    NSInteger index = ((UIView *)sender).tag;
    NSMutableArray *screenShoots = self.model.value;
    UIImage *image = [screenShoots safeObjectAtIndex:index];
    if (image) {
        [self showPhotoDetail:image index:index];
        return;
    }
    UIViewController *controller = [self viewController];    
    [self.picker selectPhotoFromViewController:controller withComplete:^(UIImage *result) {
        if (result) {
            [screenShoots addObject:result];
            [self showAllImage];
        }
    }];
}

- (IBAction)removePhotoButtonClick:(id)sender {
    NSInteger index = ((UIView *)sender).tag;
    [self removePhotoAtIndex:index];
}

- (void)removePhotoAtIndex:(NSInteger)index {
    NSMutableArray *screenShoots = self.model.value;
    [screenShoots safeRemoveObjectAtIndex:index];
    [self showAllImage];
}

- (void)showAllImage {
    NSMutableArray *screenShoots = self.model.value;
    for (int i = 0 ; i < 4; i++) {
        UIImage *image = [screenShoots safeObjectAtIndex:i];
        ZPFeedbackScreenShootView *screenShoot = [self.allScreenShootView safeObjectAtIndex:i];
        [screenShoot setImage:image];
        screenShoot.hidden = [self isHidenScreenShootAtIndex:i];
    }
    [self.delegate feedbackCell:self valueDidChange:self.model];
}

- (BOOL)isHidenScreenShootAtIndex:(int)index {
    if (index == 0) {
        return false;
    }
    NSMutableArray *screenShoots = self.model.value;
    UIImage *image = [screenShoots safeObjectAtIndex:index];
    if (!image) {
        image = [screenShoots safeObjectAtIndex:index - 1];
    }
    return image == nil;
}


- (void)showPhotoDetail:(UIImage *)image index:(NSInteger)index{
    ZPFeedbackPhotoViewController *detail = [[ZPFeedbackPhotoViewController alloc] init];
    UIViewController *controller = self.viewController;
    detail.title = controller.title;
    detail.image = image;
    detail.index = index;
    detail.delegate = self;
    [controller.navigationController pushViewController:detail animated:false];
}

- (void)removePhoto:(ZPFeedbackPhotoViewController *)controller {
    [self removePhotoAtIndex:controller.index];
}


@end
