//
//  ZPBaseCollector.m
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 12/28/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPBaseCollector.h"

@implementation ZPBaseCollector

- (NSDictionary *)data {
    return @{@"name": [self collectorName],
             @"data": [self collectorData]};
}

- (NSString *)collectorName {
    return @"defaultName";
}

- (NSDictionary *)collectorData {
    return @{};
}
@end
