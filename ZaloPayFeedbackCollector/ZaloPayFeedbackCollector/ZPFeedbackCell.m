//
//  ZPFeebackCell.m
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 12/29/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPFeedbackCell.h"

@implementation ZPFeedbackCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle  = UITableViewCellSelectionStyleNone;
        [self setupCell];
    }
    return self;
}

- (void)setupCell {}

- (void)setData:(ZPFeedbackCellModel *)item {}

- (void)didSelectCell {}

- (void)setSeparatorLeftInset:(float)leftInset {
    if ([self respondsToSelector:@selector(setSeparatorInset:)]) {
        [self setSeparatorInset:UIEdgeInsetsMake(0, leftInset, 0, 0)];
    }
    
    if ([self respondsToSelector:@selector(setLayoutMargins:)]) {
        [self setLayoutMargins:UIEdgeInsetsMake(0, leftInset, 0, 0)];
    }
}
@end
