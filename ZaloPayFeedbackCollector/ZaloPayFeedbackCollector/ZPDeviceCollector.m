//
//  ZPDeviceCollector.m
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 12/28/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPDeviceCollector.h"
#import <ZaloPayCommon/UIDevice+Name.h>
#import <ZaloPayCommon/NSCollection+JSON.h>

@implementation ZPDeviceCollector
- (NSString *)collectorName {
    return @"deviceinfo";
}

- (NSDictionary *)collectorData {
    NSString *name = [[UIDevice currentDevice] deviceName];
    NSString *version = [[UIDevice currentDevice] systemVersion];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObjectCheckNil:name forKey:@"model"];
    [dic setObjectCheckNil:version forKey:@"os_version"];
    return dic;
}

@end
