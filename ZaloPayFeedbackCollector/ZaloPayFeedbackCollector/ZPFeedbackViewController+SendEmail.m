//
//  ZPFeedbackViewController+SendEmail.m
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 1/5/17.
//  Copyright © 2017-present VNG. All rights reserved.
//

#import "ZPFeedbackViewController+SendEmail.h"
#import <ZaloPayCommon/NSCollection+JSON.h>
#import <ZaloPayUI/ZPDialogView.h>
#import <ZaloPayCommon/ZPConstants.h>
#import <ZaloPayCommon/R.h>

@implementation ZPFeedbackViewController (SendEmail)

- (void)sendEmail:(NSArray *)data images:(NSArray *)images bodyMessage:(NSString *)message {

    if (![MFMailComposeViewController canSendMail]) {
        [self alertEmailNotAvailable];
        return;
    }
    
    MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
    if ([MFMessageComposeViewController canSendAttachments]) {
        [self attachImages:images forEmail:mail];
        [self attachData:data forEmail:mail];
    }
    
    mail.mailComposeDelegate = self;
    [mail setSubject:[R string_Feedback_Email_Title]];
    [mail setMessageBody:message isHTML:NO];
    [mail setToRecipients:@[[R string_Feedback_Email_Receiver]]];
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    [window.rootViewController presentViewController:mail animated:YES completion:NULL];
    
}

- (void)attachImages:(NSArray *)images forEmail:(MFMailComposeViewController *)mail {
    for (int i = 0; i < images.count; i++) {
        UIImage *img = images[i];
        NSData *data = UIImageJPEGRepresentation(img, 1.0);
        NSString *name = [NSString stringWithFormat:@"screen_%d.jpeg",(i + 1)];
        [mail addAttachmentData:data mimeType:@"jpeg" fileName:name];
    }
}

- (void)attachData:(NSArray *)jsonData forEmail:(MFMailComposeViewController *)mail {
    NSData *data = [[jsonData JSONRepresentation:true] dataUsingEncoding:NSUTF8StringEncoding];
    [mail addAttachmentData:data mimeType:@"json" fileName:@"data.txt"];
}

- (void)alertEmailNotAvailable {
    [ZPDialogView showDialogWithType:DialogTypeNotification
                             message:[R string_Feedback_Email_NotAvailable]
                   cancelButtonTitle:[R string_ButtonLabel_Close]
                    otherButtonTitle:nil
                      completeHandle:nil];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error {
    switch (result) {
        case MFMailComposeResultSent:
            [self sendMailSuccess];
            break;
        case MFMailComposeResultSaved:
            
            break;
        case MFMailComposeResultCancelled:
            
            break;
        case MFMailComposeResultFailed:
            
            break;
        default:
            
            break;
    }
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

- (void)sendMailSuccess {
    [self.navigationController popViewControllerAnimated:true];
}

@end
