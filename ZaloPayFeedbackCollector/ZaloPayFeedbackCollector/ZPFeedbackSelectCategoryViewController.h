//
//  ZPFeedbackSelectCategoryViewController.h
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 1/11/17.
//  Copyright © 2017-present VNG. All rights reserved.
//

#import <ZaloPayUI/BaseViewController.h>
@class ZPFeedbackSelectCategoryViewController;
@protocol ZPFeedbackSelectCategoryViewControllerDelegate<NSObject>
- (void)categoryController:(ZPFeedbackSelectCategoryViewController *)controller selectData:(NSString *)data;
@end

@interface ZPFeedbackSelectCategoryViewController : BaseViewController
- (id)initWithData:(NSArray *)data;
@property (nonatomic, weak) id<ZPFeedbackSelectCategoryViewControllerDelegate>delegate;
@end
