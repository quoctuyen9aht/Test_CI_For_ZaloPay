//
//  ZPCollectorProtocol.h
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 12/28/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#ifndef ZPCollectorProtocol_h
#define ZPCollectorProtocol_h

#import <Foundation/Foundation.h>

@protocol ZPCollectorProtocol <NSObject>
- (NSDictionary *)data;
@end

#endif /* ZPCollectorProtocol_h */
