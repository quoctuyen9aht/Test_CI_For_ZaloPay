//
//  ZPFeebackCell.h
//  ZaloPayFeedbackCollector
//
//  Created by bonnpv on 12/29/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZPFeedbackCellModel.h"

@class ZPFeedbackCell;

@protocol ZPFeedbackCellDelegate <NSObject>
- (void)feedbackCell:(ZPFeedbackCell *)cell valueDidChange:(ZPFeedbackCellModel*)model;
@end

@interface ZPFeedbackCell : UITableViewCell
@property (nonatomic, strong) id<ZPFeedbackCellDelegate>delegate;
- (void)setData:(ZPFeedbackCellModel *)item;
- (void)didSelectCell;
- (void)setupCell;
- (void)setSeparatorLeftInset:(float)leftInset;
@end

