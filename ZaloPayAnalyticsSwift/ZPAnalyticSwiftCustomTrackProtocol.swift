//
//  ZPAnalyticSwiftCustomTrackProtocol.swift
//  ZaloPayAnalyticsSwift
//
//  Created by Dung Vu on 6/7/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import Foundation

@objc public protocol ZPAnalyticSwiftCustomTrackProtocol: AnyObject {
    func uploadLogSession()
    func trackParseOrVerifyConfigError(_ eventValue: Int)
    func writeLog(bin: NSDictionary?)
    func writeLogAll(transactions: Array<NSDictionary>?)
    func writeLog(transaction: NSDictionary?)
    func writeLogErrorApi(errorLog: NSDictionary?)
    func writeLogErrorNotification(error: NSDictionary?)
    func writeLogNotification(log: NSDictionary?)
    func writeLogDeepLink(log: NSDictionary?)
    func writeLogOpenApp(log: NSDictionary?)
    func writeLogWebEvent(log: NSDictionary?)
    
}

