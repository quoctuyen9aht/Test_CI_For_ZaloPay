//
//  ZPLogGRPC.swift
//  ZaloPay
//
//  Created by Dung Vu on 5/17/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//
//
//  MARK: - FLOW TO USE IT
//  Create message appreciate (message: SwiftProtobuf.Message) -> call senderMessage.onNext(message) -> auto send -> complete
//
//  Can see example by log event in <b> MARK Handle event <\b>


import Foundation
import SwiftProtobuf
import RxSwift
import RxCocoa
import ZaloPayAnalyticsPrivate

enum ZPLogGRPCError: String, Error, CustomStringConvertible {
    case makeFolder = "makeFolder"
    case notFoundURL = "notFoundURL"
    case navigatorNotReady = "navigatorNotReady"
    case dataNotExistForPost = "dataNotExistForPost"
    case notHaveInCache = "notHaveInCache"
    case sendDataError = "sendDataError"
    case waitingForAccessFileLogUpload = "waitingForAccessFileLogUpload"
    case errorDataEmbedded = "errorDataEmbedded"
    case errorNoLogsSave = "errorNoLogsSave"
    var description: String {
        return self.rawValue
    }
}

enum ResultSend {
    case fail(Error)
    case success
}

enum ZPLogGRPCRouter: Int {
    case silos
    case postGRPC
    
    var path: String {
        switch self {
        case .silos:
            return "api/v1.0/where"
        case .postGRPC:
            return "api/v1.0/log/write"
        }
    }
    
    var method: String {
        switch self {
        case .silos:
            return "GET"
        case .postGRPC:
            return "POST"
        }
    }
    
    var queries: [URLQueryItem]? {
        switch self {
        case .silos:
            return [URLQueryItem(name: "app", value: "silos"), URLQueryItem(name: "deviceID", value: zpDeviceId)]
        case .postGRPC:
            if zpUserId != nil {
                return [URLQueryItem(name: "userId", value: zpUserId)]
            }
            return nil
        }
    }
    
    
    func request(from url: URL?, data: Data?) throws -> URLRequest {
        guard let url = url else {
            throw ZPLogGRPCError.notFoundURL
        }
        // Create new url
        let nURL = url.appendingPathComponent(self.path)
        printDebug(nURL)
        // Attach query
        if self == .postGRPC && data == nil {
            throw ZPLogGRPCError.dataNotExistForPost
        }
        var components = URLComponents(url: nURL, resolvingAgainstBaseURL: false)
        components?.queryItems = self.queries
        guard let urlPost = components?.url else {
            throw ZPLogGRPCError.notFoundURL
        }
        
        var request = URLRequest(url: urlPost)
        request.httpMethod = self.method
        
        // Case Post
        if self == .postGRPC, let data = data {
            request.setValue("application/octet-stream", forHTTPHeaderField: "Content-Type")
            let lenght = data.count
            request.setValue("\(lenght)", forHTTPHeaderField: "Content-Length")
            if let accessToken = zpAccessToken, zpUserId != nil {
                printDebug("Authorization --------------")
                let authorization = "Bearer \(accessToken)"
                printDebug(authorization)
                request.setValue(authorization, forHTTPHeaderField: "Authorization")
            }
            
            var logRequest = LogRequest()
            logRequest.logs = data
            let dataSend = logRequest --> { try $0?.serializedData() }
            request.httpBody = dataSend
            request.timeoutInterval = 10
            
        }
        printDebug("Request --------------")
        printDebug(request)
        return request
    }
}

// MARK: Main class
@objcMembers
public final class ZPLogGRPC: NSObject, ZPTrackerProtocol, SafeAccessFileProtocol, WriteFileProtocol, ZPAnalyticSwiftCustomTrackProtocol {
    // MARK: - Value Type
    enum ZPLogGRPCSate: Int {
        case unknown
        case ready
        case waiting
        case failed
    }
    
    enum SilosResponseStatus: String, Codable {
        case success
        case failed
    }
    
    struct ZPLogGRPCCache: Codable, Equatable, Hashable {
        let data: Data
        let time: TimeInterval
        
        var hashValue: Int {
            return Int(time)
        }
        
        private enum CodingKeys: String, CodingKey {
            case data
            case time
        }
        
        static func == (lhs: ZPLogGRPCCache, rhs: ZPLogGRPCCache) -> Bool {
            return lhs.time == rhs.time
        }
        
        init(data: Data, time: TimeInterval) {
            self.data = data
            self.time = time
        }
        
        init(from decoder: Swift.Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            data = try container.decode(Data.self, forKey: .data)
            time = try container.decode(TimeInterval.self, forKey: .time)
        }
        
    }
    
    struct ZPSilosResponseLocation: Codable {
        var pubLocations: [URL]?
        var priLocations: [URL]?
        
        private enum CodingKeys: String, CodingKey {
            case pubLocations = "pub"
            case priLocations = "auth"
        }
    }
    
    struct ZPSilosResponse: Codable {
        var status: SilosResponseStatus
        var locations: ZPSilosResponseLocation
        
        private enum CodingKeys: String, CodingKey {
            case status
            case locations
        }
        
        init(from decoder: Swift.Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            status = try container.decode(SilosResponseStatus.self, forKey: .status)
            locations = try container.decode(ZPSilosResponseLocation.self, forKey: .locations)
        }
    }
    
    enum ZPLogGRPCAccessLogFile: String {
        case canAccess
        case waiting
    }
    
    
    enum ZPLogGRPCResultStatus: String, Codable {
        case success
        case error
    }
    
    /// Response write log
    struct ZPLogGRPCResult: Codable, CustomStringConvertible {
        static var `default` = ZPLogGRPCResult.init(status: .error)
        let status: ZPLogGRPCResultStatus
        var description: String {
            return status.rawValue
        }
    }
    
    // MARK: - Variable
    private (set) lazy var lockEvent: NSRecursiveLock = NSRecursiveLock()
    private var session: URLSession
    private var deviceId: String? {
        return zpDeviceId
    }
    
    private static let folderCacheName: String = "LogGRPC"
    private static let fName = "LogFile"
    
    private static var isDebuging: Bool {
        return zpEnvironment == .development || zpEnvironment == .sandbox
    }
    // 15 minute reload
    private static let timeReloadSilos: TimeInterval = 900
    
    private var userid: String? {
        return zpUserId
    }
    
    private var canUseURLPrivate: Bool {
        return zpUserId != nil
    }
    
    // File
    var cacheURL: URL?
    var cacheFileName: String?
    private var urlCacheFile: URL?
    private var cacheLogs: [ZPLogGRPCCache] = []
    
    private let disposeBag = DisposeBag()
    private var silosInfor: ZPSilosResponse?
    private var state: ZPLogGRPCSate = .unknown
    private var stateAccessLog: ZPLogGRPCAccessLogFile = .canAccess
    private lazy var serviceReachability: DefaultReachabilityService? = debug(block: { try DefaultReachabilityService.init() }, default: nil)
    private let eventBus: PublishSubject<Log>
    private lazy var senderMessage: PublishSubject<SwiftProtobuf.Message> = PublishSubject()
    
    // MARK: - Init
    public override init() {
        let session = URLSession.shared
        self.session = session
        self.eventBus = PublishSubject()
        super.init()
        debug(block: { try makeFolderCache() }, default: nil)
        self.setupEvent()
        self.loadSilos()
    }
    
    // MARK: - Make folder
    private func makeFolderCache() throws {
        let folder = FileManager.default.cacheURL?.appendingPathComponent(ZPLogGRPC.folderCacheName, isDirectory: true)
        // Check exist
        var isCreated = false
        FileManager.default.createDirectoy(at: folder, is: &isCreated)
        if !isCreated {
            // Check
            throw ZPLogGRPCError.makeFolder
        }
        
        // Cache
        self.cacheURL = folder
        // Find File Log
        self.createFileLog(with: ZPLogGRPC.fName)
        
        // Load
        let urlCache = self.cacheURL?.appendingPathComponent("\(ZPLogGRPC.fName).txt")
        printDebug(urlCache?.absoluteString ?? "")
        self.urlCacheFile = urlCache
        guard let fCache = self.urlCacheFile else {
            return
        }
        
        let attsFile = try FileManager.default.attributesOfItem(atPath: fCache.path)
        let size = attsFile[FileAttributeKey.size] as? Double ?? 0
        guard size < 2e+6 else {
            // remove
            printDebug("Over size")
            FileManager.default.removeFile(at: fCache)
            self.createFileLog(with: ZPLogGRPC.fName)
            return
        }
        
        let data = try Data.init(contentsOf: fCache)
        guard data.count > 0 else {
            return
        }
        let values = data --> { try [ZPLogGRPCCache].toValue(from: $0) }
        self.cacheLogs = values ?? []
    }
    
    private func loadSilos() {
        self.navigatorResponse().subscribe(onNext: { [weak self] in
            self?.updateSilos(with: $0)
            }, onError: { (e) in
                printDebug(e.localizedDescription)
        }).disposed(by: disposeBag)
    }
    
    private func updateSilos(with response: ZPSilosResponse?) {
        guard let response = response, response.status == .success else {
            // Retry load
            self.state = .failed
            self.loadSilos()
            return
        }
        excuteActionInSafe {
            self.state = .ready
            self.silosInfor = response
        }
    }
    
    private func setupEvent() {
        Observable<Int>.timer(ZPLogGRPC.timeReloadSilos, scheduler: SerialDispatchQueueScheduler(qos: .background)).bind { [weak self](atempt) in
            self?.loadSilos()
        }.disposed(by: disposeBag)

        eventBus
            .buffer(timeSpan: 2, count: 30, scheduler: SerialDispatchQueueScheduler(qos: .background))
            .filter({ $0.count > 0 })
            .debug()
            .bind(onNext: { [weak self] in
                self?.sendLogs($0)
            }).disposed(by: disposeBag)
        
        senderMessage.subscribe(onNext: { [weak self] in
           $0 --> { self?.embeddedDataLog(from: $0) } --> { try self?.sendToGRPC(with: $0) }
        }).disposed(by: disposeBag)
        
    }
    
    private func navigatorResponse() -> Observable<ZPSilosResponse?> {
        let router = ZPLogGRPCRouter.silos
        guard let url = (zpURLNavigatorGRPC ?? "") --> { URL.init(string: $0) },
            self.state != .waiting else {
                return Observable.empty()
        }
        guard let request = url --> { try router.request(from: $0, data: nil) } else {
            return Observable.empty()
        }
        
        excuteActionInSafe {
            self.state = .waiting
        }
        return self.send(using: request)
            .retryOnBecomesReachable(Data(), reachabilityService: self.serviceReachability)
            .filter({ $0.count > 0})
            .map({ data in
            let result = data --> { try ZPSilosResponse.toValue(from: $0) }
            if ZPLogGRPC.isDebuging {
                data --> { (try JSONSerialization.jsonObject(with: $0, options: [])) as? [String : Any] }
                    --> { printDebug($0 ?? [:]) }
            }
            return result
        })
    }
    
    private func send(using request: URLRequest) -> Observable<Data> {
        return self.session.rx.data(request: request)
    }
}

// MARK: - Find File
extension ZPLogGRPC: FindFilesLogProtocol {}

// MARK: - Write File
extension ZPLogGRPC {
    func url(for file: String, type: ZPServerEventLogFileType) -> URL? {
        let nUrl = cacheURL?.appendingPathComponent("\(file).\(type.description)")
        return nUrl
    }
}

// MARK: - Send Event
extension ZPLogGRPC {
    private func sendToGRPC(with message: Log?) throws {
        guard let message = message else {
            throw ZPLogGRPCError.errorDataEmbedded
        }
        debug(block: { try send(message: message) }, default: nil)
    }
    
    private func sendLogs(_ logs:[Log]){
        let router = ZPLogGRPCRouter.postGRPC
        // find url
        let locations = self.silosInfor?.locations
        let url = self.canUseURLPrivate ? locations?.priLocations?.last : locations?.pubLocations?.last
        
        var embeddedData = Logs()
        embeddedData.entries = logs
        let data = embeddedData --> { try $0.serializedData() }
        
        guard let request = (url, data) --> { try router.request(from: $0, data: $1) } else {
            logs --> { try self.saveLogs($0) }
            return
        }
        
        self.send(using: request).flatMap({ data -> Observable<ZPLogGRPCResult> in
            guard let result = data --> { try ZPLogGRPCResult.toValue(from: $0) },
                result.status != .error else {
                    throw ZPLogGRPCError.sendDataError
            }
            return Observable.just(result)
        }).map { (_) in
            ResultSend.success
        }.catchError {
            Observable.just(ResultSend.fail($0))
        }.bind(onNext: { [weak self] in
            switch $0 {
            case .fail(let e):
                // cache
                printDebug(e)
                logs --> { try self?.saveLogs($0) }
            case .success:
                printDebug("Send : success")
            }
        }).disposed(by: disposeBag)
    }
    
    private func send(message: Log) throws {
        guard self.state == .ready else {
            // Not ready append cache
            try message.serializedData() --> { saveCacheMessage(from: $0) }
            return
        }
        
        eventBus.onNext(message)
        debug(block: { try self.uploadCache() } , default: nil)
    }
    
    func uploadCache() throws {
        guard self.cacheLogs.count > 0 else {
            throw ZPLogGRPCError.notHaveInCache
        }
        
        guard self.stateAccessLog == .canAccess else {
            throw ZPLogGRPCError.waitingForAccessFileLogUpload
        }
        
        self.stateAccessLog = .waiting
        let currentItem = self.cacheLogs.count
        let numberItemRemove = min(currentItem, 20)
        let temp = Array(self.cacheLogs[0..<numberItemRemove])
        excuteActionInSafe {
            self.cacheLogs.removeFirst(numberItemRemove)
        }
        save()
        
        guard let logs = temp --> { try $0.map({ try Log(serializedData: $0.data) }) } else {
            throw ZPLogGRPCError.errorDataEmbedded
        }
        logs.forEach({ eventBus.onNext($0) })
    }
    
    
    private func saveLogs(_ logs:[Log]?) throws {
        printDebug(#function)
        guard let logs = logs , logs.count > 0 else {
            throw ZPLogGRPCError.errorNoLogsSave
        }
        excuteActionInSafe {
            let dataLogs = logs
                .compactMap({ $0 --> { try $0.serializedData() } })
                .map({ ZPLogGRPCCache(data: $0, time: Date().timeIntervalSince1970) })
            
            self.cacheLogs += dataLogs
        }
        save()
    }
    
    private func saveCacheMessage(from data: Data) {
        printDebug(#function)
        let cache = ZPLogGRPCCache(data: data, time: Date().timeIntervalSince1970)
        // Save
        self.save(override: false, dataAppend: cache)
    }
    
    private func save(override: Bool = true, dataAppend: ZPLogGRPCCache? = nil) {
        excuteActionInSafe { [unowned self] in
            guard let urlSave = self.urlCacheFile else {
                return
            }
            if let data = dataAppend {
                self.cacheLogs.append(data)
            }
            self.cacheLogs --> { try $0.toData() } --> { try $0?.write(to: urlSave) }
            if override {
                self.stateAccessLog = .canAccess
            }
        }
    }
}
// MARK: - Handle event
extension ZPLogGRPC {
    public func trackScreen(_ screenName: String?) {
        var screenLog = ScreenLog()
        screenLog.screenName = screenName ?? " "
        screenLog.default()
        senderMessage.onNext(screenLog)
    }
    
    public func trackTiming(_ eventId: ZPAnalyticEventAction, value eventValue: NSNumber?) {}
    
    public func trackCustomEvent(_ eventCategory: String?, action eventAction: String?, label eventLabel: String?, value eventValue: NSNumber?) {}
    
    public func trackEvent(_ eventId: ZPAnalyticEventAction, value eventValue: NSNumber?, label eventLabel: String?) {
        var log = EventLog()
        log.type = 1
        log.id = Int32(eventId.rawValue)
        log.previousScreen = eventLabel ?? " "
        log.default()
        senderMessage.onNext(log)
    }
    
    public func trackEvent(_ eventId: ZPAnalyticEventAction, valueString eventValue: String?, label eventLabel: String?) {
        var log = EventLog()
        log.type = 1
        log.id = Int32(eventId.rawValue)
        log.previousScreen = eventLabel ?? " "
        log.default()
        // Send message
        senderMessage.onNext(log)
    }
    
    private func embeddedDataLog(from event: SwiftProtobuf.Message, type: LogType = .eventLog) -> Log? {
        guard let data = event --> { try $0.serializedData() } else {
            return nil
        }
        return data --> {
            var result = Log()
            result.data = $0
            result.type = type
            return result
        }
    }
}

// MARK: - Custom track
extension ZPLogGRPC {
    public func uploadLogSession() {
        
    }
    
    public func trackParseOrVerifyConfigError(_ eventValue: Int) {
        
    }
    
    public func writeLog(bin: NSDictionary?) {
        
    }
    
    public func writeLogAll(transactions: Array<NSDictionary>?) {
        
    }
    
    public func writeLog(transaction: NSDictionary?) {
        
    }
    
    public func writeLogErrorApi(errorLog: NSDictionary?) {
        
    }
    
    public func writeLogErrorNotification(error: NSDictionary?) {
        
    }
    
    public func writeLogNotification(log: NSDictionary?) {
        
    }
    
    public func writeLogDeepLink(log: NSDictionary?) {
        
    }
    
    public func writeLogOpenApp(log: NSDictionary?) {
        
    }
    
    public func writeLogWebEvent(log: NSDictionary?) {
        
    }
}

protocol SilosDefaultValueProtocol {
    var appsflyerID: String { get set }
    var deviceID: String { get set }
    var timestamp: UInt64 { get set }
    var userID: String { get set }
    var osType: Int32 { get set }
    var sessionID: String { get set }
}

extension EventLog: SilosDefaultValueProtocol {}
extension ScreenLog: SilosDefaultValueProtocol {}
extension DeepLinkLog: SilosDefaultValueProtocol {}
extension BinLog: SilosDefaultValueProtocol {}
extension ApiErrorLog: SilosDefaultValueProtocol {}
//extension NotificationErrorLog: SilosDefaultValueProtocol {}
//extension TransactionLog: SilosDefaultValueProtocol {}
//extension OpenAppLog: SilosDefaultValueProtocol {}



extension SilosDefaultValueProtocol {
    mutating func `default`() {
        self.appsflyerID = zpAppflyerId ?? " "
        self.deviceID = zpDeviceId ?? " "
        self.osType = Int32(ZPLogOSType.iOS.rawValue)
        self.timestamp = UInt64(Date().timeIntervalSince1970)
        self.userID = zpUserId ?? " "
        self.sessionID = ZPAnalyticSwiftConfig.sessionId()
    }
}

extension EventLog {
    // format : eventType \t eventId \t value \t timestamp \t ostype \t appsflyerId  \t deviceid  \t userid \t desription \t previousScreen \t sessionID
    var toString: String {
        return "\(self.type)\t\(self.id)\t\(self.value)\t\(self.timestamp)\t\(self.osType)\t\(self.appsflyerID)\t\(self.deviceID)\t\(self.userID)\t\(self.description_p)\t\(self.previousScreen)\t\(self.sessionID)"
    }
}

