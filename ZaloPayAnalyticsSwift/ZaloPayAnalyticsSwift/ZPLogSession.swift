//
//  ZPLogSession.swift
//  ZaloPay
//
//  Created by Dung Vu on 3/6/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

let nameFolderSession = "SessionUser"
fileprivate let cacheFileLogSessionName = "Session-Cache.txt"

protocol UploadLogsSessionProtocol {
    func uploadLogsSession()
}


final class ZPLogSession<Worker: MakeLogSessionWorkerProtocol>: SafeAccessFileProtocol, WriteFileProtocol, UpdateLogContentProtocol, UploadLogsSessionProtocol where Worker: ExportLogProtocol, Worker: Codable, Worker: WriteFileProtocol {
    typealias Dependency = UploadLogsProtocol & WriteFileProtocol & TrackingAppIdInAppProtocol
    lazy var lockEvent: NSRecursiveLock = NSRecursiveLock()
    private lazy var encoder = JSONEncoder()

    /// more workernew
    private var workers: [Worker]

    /// current worker
    private var currentWorker: Worker?

    // Dependency out source
    private weak var dependency: Dependency?
    private (set) var cacheURL: URL?
    private var userId: String? {
        return dependency?.userId
    }
    private var urlCacheFile: URL?

    private let disposeBag = DisposeBag()

    var cacheFileName: String? {
        return cacheFileLogSessionName
    }

    // Track sent log , if it already sent -> wait to next open app
    private var isUploadedLog: Bool = false

    // Track if uploading file
    private var isExcuteUpload: Bool = false

    func url(for file: String, type: ZPServerEventLogFileType) -> URL? {
        let nUrl = cacheURL?.appendingPathComponent("\(file).\(type.description)")
        return nUrl
    }

    // when append?
    // Will load from old file to prepare upload
    init(using dependency: Dependency) {
        self.dependency = dependency
        self.cacheURL = self.dependency?.cacheURL?.appendingPathComponent(nameFolderSession)
        self.urlCacheFile = self.cacheURL?.appendingPathComponent(cacheFileLogSessionName)
        // Make root
        var isCreated = false
        FileManager.default.createDirectoy(at: self.cacheURL, is: &isCreated)
        loadFile:if isCreated {
            // load old file
            guard let fCache = self.urlCacheFile, let data = try? Data.init(contentsOf: fCache), data.count > 0 else {
                self.workers = []
                break loadFile
            }
            let decoder = JSONDecoder()
            self.workers = (try? decoder.decode([Worker].self, from: data)) ?? []
        } else {
            self.workers = []
        }
        self.createNewWorker()
        setupEvent()
    }

    private func setupEvent() {
        // Only excute in background
        NotificationCenter.default.rx
                .notification(Notification.Name.UIApplicationDidBecomeActive)
                .observeOn(SerialDispatchQueueScheduler.init(qos: .background)).bind { [weak self] (_) in
                    self?.createNewWorker()
                }.disposed(by: disposeBag)
    }

    private func createNewWorker() {
        defer {
            saveCurrentAllWorkers()
        }

        // prevent access
        self.excuteActionInSafe {
            let new = Worker(with: self.userId)
            self.currentWorker = new
            self.workers.append(new)
        }
    }

    private func saveCurrentAllWorkers() {
        guard let urlSave = self.urlCacheFile else {
            return
        }
        self.excuteActionInSafe { [unowned self] in
            do {
                let data = try self.encoder.encode(self.workers)
                try data.write(to: urlSave)
            } catch {
#if DEBUG
                assert(false, error.localizedDescription)
#endif
            }
        }
    }

    /// Using upload log
    func uploadLogsSession() {
        // Track if it need upload
        guard !isUploadedLog && !isExcuteUpload else {
            return
        }
        self.isExcuteUpload = true

        // Find all old zip
        let findOldZips = self.findFiles(.zip)
        let export = self.exportFromWorkers().flatMap({ url -> Observable<[URL]> in
            guard let url = url else {
                return Observable.empty()
            }
            return Observable.just([url])
        })

        Observable
                .zip([findOldZips, export])
                .map({ $0.reduce([URL](), { $0 + $1 }) })
                .observeOn(SerialDispatchQueueScheduler.init(qos: .background))
                .flatMap({ urls -> Observable<[URL]> in
                    guard urls.count > 0 else {
                        return Observable.empty()
                    }
                    let r = Observable
                            .zip(urls.map({ self.dependency?.uploadZip(with: $0) ?? Observable.empty() }))
                            .map({ _ -> [URL] in return urls })
                    return r
                }).subscribe(onNext: { (urls) in
#if DEBUG
                    // Check file deleted
                    urls.forEach({
                        if FileManager.default.fileExist(at: $0) {
                            print($0.path)
                        }
                    })
#endif
                }, onError: { (e) in
#if DEBUG
                    print(e.localizedDescription)
#endif
                }, onCompleted: { [weak self] in
                    self?.isUploadedLog = true
                }, onDisposed: { [weak self] in
                    self?.isExcuteUpload = false
                }).disposed(by: disposeBag)
    }

    private func exportFromWorkers() -> Observable<URL?> {

        // will upload exclude current worker
        // create new zip file from workers
        let workersWillExport = self.workers.filter({ self.currentWorker != $0 })
        guard workersWillExport.count > 0 else {
            return Observable.empty()
        }

        let eExportZip = Observable.zip(workersWillExport.map({ $0.exportZipLogFile() })).map({ $0.compactMap({ $0 }) })
        // zip
        return eExportZip.map({ [weak self] urls -> URL? in
            guard urls.count > 0 else {
                return nil
            }
            let time = Date().string(using: "yyyyMMddHHmmss").remove(at: ":")
            let result = self?.zip(with: urls, using: "Session-User-\(time)")
            defer {
                // Remove file
                urls.forEach({ FileManager.default.removeFile(at: $0) })
            }
            return result
        }).do(onNext: { [weak self](_) in
            // Update current workers
            let newCurrentWorkers: [Worker]
            defer {
                self?.excuteActionInSafe {
                    self?.workers = newCurrentWorkers
                }
                self?.saveCurrentAllWorkers()
            }
            guard let current = self?.currentWorker else {
                newCurrentWorkers = []
                return
            }
            newCurrentWorkers = [current]
        })
    }

    // MARK: - Update Log
    func writeLog(using text: String, with type: ZPServerEventWriteLogType) {
        self.currentWorker?.writeLog(using: text, with: type)
    }

    func writeLog(using infor: [String: Any], with type: ZPServerEventWriteLogType) {
        self.currentWorker?.writeLog(using: infor, with: type)
    }

}

extension ZPLogSession: FindFilesLogProtocol {
}

extension ZPLogSession: ZipFilesLogProtocol {
}


