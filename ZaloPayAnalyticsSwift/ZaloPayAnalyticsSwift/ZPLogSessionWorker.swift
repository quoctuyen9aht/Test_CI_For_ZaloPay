//
//  ZPLogSessionWorker.swift
//  ZaloPay
//
//  Created by Dung Vu on 3/6/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol UpdateLogContentProtocol {
    func writeLog(using text: String, with type: ZPServerEventWriteLogType)
    func writeLog(using infor: [String: Any], with type: ZPServerEventWriteLogType)
}

protocol MakeLogSessionWorkerProtocol: Equatable, UpdateLogContentProtocol {

    /// Create worker
    ///
    /// - Parameters:
    ///   - userId: payment userid
    init(with userId: String?)
}

final class ZPLogSessionWorker: Codable, SafeAccessFileProtocol, WriteFileProtocol, MakeLogSessionWorkerProtocol {
    private (set) lazy var lockEvent: NSRecursiveLock = NSRecursiveLock()
    private lazy var cacheRootPath: URL? = {
        return FileManager.default.cacheURL?.appendingPathComponent(nameFolderSession)
    }()

    private (set) lazy var cacheURL: URL? = {
        return self.cacheRootPath?.appendingPathComponent(self.session)
    }()
    private var session: String
    var cacheFileName: String?
    private var identify: TimeInterval

    private enum CodingKeys: String, CodingKey {
        case session
        case cacheFileName
        case identify
    }

    // MARK: init
    init(with userId: String?) {
        let date = Date()
        self.identify = date.timeIntervalSince1970
        self.session = date.string(using: "yyyyMMddHHmmss").remove(at: ":")
        // Cache
        // When write log -> append time
        self.cacheFileName = "\(session)-\(userId ?? "0")"

        // Make children folder from root
        let currentPath = FileManager.default.cacheURL?.appendingPathComponent("\(nameFolderSession)/\(session)")

        var created = false
        FileManager.default.createDirectoy(at: currentPath, is: &created)
#if DEBUG
        assert(created == false, "Duplicate folder!!!")
#endif
    }

    private func fileName(_ type: ZPServerEventWriteLogType) -> String {
        let prefix = type.prefix
        let name = "\(prefix)\(cacheFileName ?? "")"
        return name
    }

    private func write(with data: Data?, at fileName: String) {
        guard let data = data else {
            return
        }
        self.excuteActionInSafe { [unowned self] in
            // Create File before write data
            self.createFileLog(with: fileName)
            // Write
            self.writeLog(using: data, with: fileName)
        }
    }

    private func write(_ text: String, with type: ZPServerEventWriteLogType) {
        let fName = self.fileName(type)
        let data = text.data(using: .utf8)
        write(with: data, at: fName)
    }

    func writeLog(using text: String, with type: ZPServerEventWriteLogType) {
        // Append -> write log
        write("\(text)\n", with: type)
    }

    func writeLog(using infor: [String: Any], with type: ZPServerEventWriteLogType) {
        guard let jsonData = try? JSONSerialization.data(withJSONObject: infor, options: []) else {
            return
        }
        guard var jsonString = String(data: jsonData, encoding: .utf8) else {
            return
        }
        jsonString = "\(jsonString)\n"
        write(jsonString, with: type)
    }

    // MARK: Access File
    /// URL access file
    ///
    /// - Parameters:
    ///   - file: name file
    ///   - type: zip or txt
    /// - Returns: URL
    func url(for file: String, type: ZPServerEventLogFileType) -> URL? {
        let nUrl = cacheURL?.appendingPathComponent("\(file).\(type.description)")
        return nUrl
    }

    // Compare
    static func ==(lhs: ZPLogSessionWorker, rhs: ZPLogSessionWorker) -> Bool {
        return lhs.identify == rhs.identify
    }
}

// Will remove
extension ZPLogSessionWorker: ExportLogProtocol {
    func exportZipLogFile() -> Observable<URL?> {
        // find file -> create zip file
        return self.findFiles(.txt).map({ [weak self] urls -> URL? in
            guard let wSelf = self, urls.count > 0 else {
                return nil
            }

            guard let pathSaveZip = wSelf.zip(with: urls, using: "zalopay-logs-session-\(wSelf.cacheFileName ?? "")") else {
                return nil
            }

            let n = pathSaveZip.lastPathComponent
            // Move item to root
            guard let newPath = wSelf.cacheRootPath?.appendingPathComponent(n) else {
                return nil
            }

            do {
                try FileManager.default.moveItem(at: pathSaveZip, to: newPath)
            } catch {
#if DEBUG
                assert(false, error.localizedDescription)
#endif
            }

            return FileManager.default.fileExist(at: newPath) ? newPath : nil
        }).do(onNext: { [weak self](_) in
            // Clean up
            guard let mFolder = self?.cacheURL else {
                return
            }
            do {
                try FileManager.default.removeItem(at: mFolder)
            } catch {
#if DEBUG
                assert(false, error.localizedDescription)
#endif
            }
        })
    }
}

extension ZPLogSessionWorker: FindFilesLogProtocol {
}

extension ZPLogSessionWorker: ZipFilesLogProtocol {
}
