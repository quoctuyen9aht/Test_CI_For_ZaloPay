//
//  ZPAnalyticSwiftConfig.swift
//  ZaloPayAnalyticsSwift
//
//  Created by Dung Vu on 5/24/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import Foundation
import ZaloPayModuleProtocolsObjc

enum ZPAnalyticSwiftEnvironment: Int {
    case development = 0
    case sandbox
    case staging
    case production
}

// Config
internal fileprivate (set) var zpEnvironment: ZPAnalyticSwiftEnvironment = .development
internal fileprivate (set) var zpUserId: String?
internal fileprivate (set) var zpDeviceId: String?
internal fileprivate (set) var zpAppflyerId: String?
internal fileprivate (set) var zpAdvertisingId: String?
internal fileprivate (set) var zpURLNavigatorGRPC: String?
internal fileprivate (set) var zpAccessToken: String?
internal fileprivate (set) var zpCanUseGRPC: Bool = false
internal fileprivate (set) weak var zpNetworkProtocol: ZaloPayNetworkProtocol?

fileprivate let queueUpdateSession = DispatchQueue(label: "analytic.updateSession")
fileprivate var _sessionId: String?

@objcMembers
public class ZPAnalyticSwiftConfig: NSObject {
    private override init() {
        super.init()
    }
    
    /// Config environment for analytic
    ///
    /// - Parameter type: environment type (0: development, 1: sandbox, 2: staging, 3: production . Orther 'll use development)
    public static func setEnvironment(use type: Int) {
        zpEnvironment = ZPAnalyticSwiftEnvironment(rawValue: type) ?? .development
    }
    
    /// Set userid
    ///
    /// - Parameter id: userid if exist
    public static func setUserId(with id: String?) {
        zpUserId = id
        self.updateSession()
    }
    
    
    /// Set deviceid
    ///
    /// - Parameter id: current deviceid
    public static func setDeviceId(with id: String?) {
        zpDeviceId = id
    }
    
    /// Set appflyerid
    ///
    /// - Parameter id: current appflyerid
    public static func setAppflyerId(with id: String?) {
        zpAppflyerId = id
    }
    
    /// Set advertisingId
    ///
    /// - Parameter id: current advertisingId
    public static func setAdvertisingId(with id: String?) {
        zpAdvertisingId = id
    }
    
    /// Config for log grpc
    ///
    /// - Parameters:
    ///   - use: Config use grpc
    ///   - path: string url (optinal)
    public static func canUseGRPC(_ use: Bool, path: String?) {
        zpCanUseGRPC = use
        zpURLNavigatorGRPC = path
    }
    
    /// Check app using grpc
    ///
    /// - Returns: state true / false
    public static func isUsingGRPC() -> Bool {
        return zpCanUseGRPC
    }
    
    public static func setNetworkDependence(_ network: NSObjectProtocol?) {
        zpNetworkProtocol = network as? ZaloPayNetworkProtocol
    }
    
    public static func setAccessToken(with token: String?) {
        zpAccessToken = token
    }
    
    internal static func updateSession() {
        queueUpdateSession.sync {
            let today = NSNumber(value: Date().timeIntervalSince1970)
            _sessionId = "\(today.intValue)"
        }
    }
    
    internal static func sessionId() -> String{
        return queueUpdateSession.sync {
            return "\(_sessionId ?? "")_\(zpUserId ?? "")"
        }
    }
    
}
