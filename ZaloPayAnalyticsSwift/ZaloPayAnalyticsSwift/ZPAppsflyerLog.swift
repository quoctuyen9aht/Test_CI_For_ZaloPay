//
//  ZPAppsflyerLog.swift
//  ZaloPay
//
//  Created by Dung Vu on 1/25/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift

final class ZPAppsflyerLog: UpdateLogContentProtocol {
    typealias E = TrackingAppIdInAppProtocol & SafeAccessFileProtocol & WriteFileProtocol
    private let trackObject: E

    private struct AppIdTracking {
        let appsFlyerId: String
        let deviceId: String
        let userId: String
    }

    init(with track: E) {
        self.trackObject = track
    }

    private func fileName(_ type: ZPServerEventWriteLogType) -> String {
        var name: String = ""
        self.trackObject.excuteActionInSafe {
            let prefix = type.prefix.replacingOccurrences(of: "zalopay-logs", with: "zpv2-logs")
            let cacheFileName = self.trackObject.cacheFileName ?? ""
            name = "\(prefix)\(cacheFileName)"
        }
        return name
    }

    private func write(with data: Data?, at fileName: String) {
        guard let data = data else {
            return
        }
        self.trackObject.excuteActionInSafe { [unowned self] in
            // Create File before write data
            self.trackObject.createFileLog(with: fileName)
            // Write
            self.trackObject.writeLog(using: data, with: fileName)
        }
    }

    private func appTrackingInfor() -> AppIdTracking {
        let appsFlyerId = self.trackObject.appsFlyerId ?? ""
        let deviceId = zpAdvertisingId ?? ""
        let userId = self.trackObject.userId ?? "0"
        return AppIdTracking(appsFlyerId: appsFlyerId, deviceId: deviceId, userId: userId)
    }

    private func write(_ text: String, with type: ZPServerEventWriteLogType) {
        let fName = self.fileName(type)
        let data = text.data(using: .utf8)
        write(with: data, at: fName)
    }

    func writeLog(using text: String, with type: ZPServerEventWriteLogType) {
        // Append -> write log
        write(text, with: type)
    }

    func writeLog(using infor: [String: Any], with type: ZPServerEventWriteLogType) {
        guard let jsonData = try? JSONSerialization.data(withJSONObject: infor, options: []) else {
            return
        }
        guard var jsonString = String(data: jsonData, encoding: .utf8) else {
            return
        }
        jsonString = "\(jsonString)\n"
        write(jsonString, with: type)
    }

}
