//
//  Helper.swift
//  ZaloPayAnalyticsSwift
//
//  Created by Dung Vu on 1/15/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ZaloPayAnalyticsPrivate
import ReactiveObjC

// MARK: -- Date
extension Date {
    func string(using format: String = "yyyyMMddHHmm") -> String {
        let formater = DateFormatter()
        formater.dateFormat = format
        return formater.string(from: self)
    }
}

// MARK: -- Data
extension Data {
    init?(contentsOfOptional url: URL?) {
        guard let url = url else {
            return nil
        }
        try? self.init(contentsOf: url)
    }
}

// MARK: -- Bool
extension Bool {
    var not: Bool {
        return !self
    }
}

// MARK: -- String
extension String {
    func remove(at str: String?, options: String.CompareOptions = [], range: Range<String.Index>? = nil) -> String {
        guard self.isEmpty.not else {
            return self
        }
        //Validate
        guard let str = str, str.isEmpty.not else {
            return self
        }
        return self.replacingOccurrences(of: str, with: "", options: options, range: range)
    }

    mutating func remove(_ str: String?, options: String.CompareOptions = [], range: Range<String.Index>? = nil) {
        self = self.remove(at: str, options: options, range: range)
    }
}

// MARK: -- Dictionary
extension Dictionary where Value == Any {
    func value<T>(forKey key: Key, defaultValue: @autoclosure () -> T) -> T {
        guard let value = self[key] as? T else {
            return defaultValue()
        }
        return value
    }
}

extension Dictionary where Key == String {
    /// Find value by path (note only use for dict [String: Any])
    ///
    /// - Parameters:
    ///   - path: this is string that has format: "key1.key2.key3"
    ///   - defaultValue: value default if it doesn't find
    /// - Returns: value
    func value<T>(forPath path: String, defaultValue: @autoclosure () -> T) -> T {
        guard self.keys.count > 0 else {
            return defaultValue()
        }
        var allNode = path.components(separatedBy: ".")
        guard allNode.count > 0 else {
            return defaultValue()
        }
        // condition
        var temp: [String: Any] = self
        defer {
            temp.removeAll()
            allNode.removeAll()
        }
        let totalNode = allNode.count - 1
        finding: for node in allNode.enumerated() {
            if node.offset == totalNode {
                // Happy case
                return temp.value(forKey: node.element, defaultValue: defaultValue)
            }
            guard let next = temp[node.element] as? [String: Any] else {
                break finding
            }
            temp = next
        }
        return defaultValue()
    }
}

// MARK: -- RACSignal
fileprivate func asObservable<E>(_ signal: RACSignal<E>) -> Observable<E?> {
    return Observable.create({ (s) -> Disposable in
        let racDisposable = signal.subscribeNext({
            s.onNext($0)
        }, error: { (e) in
            s.onError(e ?? NSError())
        }, completed: {
            s.onCompleted()
        })
        return Disposables.create {
            racDisposable.dispose()
        }
    })
}

prefix operator ~~

internal prefix func ~~<E>(s: RACSignal<E>?) -> Observable<E?> {
    guard let s = s else {
        return Observable.empty()
    }
    return asObservable(s)
}

postfix operator ~~

internal postfix func ~~<E>(o: Observable<E>) -> RACSignal<E> {
    return RACSignal.createSignal({ (s) -> RACDisposable? in
        let disposeAble = o.subscribe(onNext: {
            s.sendNext($0)
        }, onError: {
            s.sendError($0)
        }, onCompleted: {
            s.sendCompleted()
        })
        return RACDisposable(block: {
            disposeAble.dispose()
        })
    })
}

func event<E, F>(from signal: RACSignal<E>?, _ transform: @escaping (E?) throws -> F?) -> Observable<F?> {
    return (~~signal).map({ try transform($0) })
}

// Ignore value
func eventIgnoreValue<E>(from signal: RACSignal<E>?) -> Observable<Void> {
    return (~~signal).map({ _ in })
}

// MARK: - File
public extension FileManager {
    func fileExist(at url: URL?) -> Bool {
        guard let url = url, url.isFileURL else {
            return false
        }
        return self.fileExists(atPath: url.path)
    }

    @discardableResult
    func createFile(at url: URL?, contents: Data? = nil, attributes: [FileAttributeKey: Any]? = nil) -> Bool {
        guard let url = url, url.isFileURL else {
            return false
        }
        return self.createFile(atPath: url.path, contents: contents, attributes: attributes)
    }

    func removeFile(at url: URL?) {
        guard let url = url, url.isFileURL else {
            return
        }
        try? self.removeItem(at: url)
    }

    func createDirectoy(at url: URL?, is created: inout Bool) {
        guard let fWriteFile = url else {
            return
        }
        var isDirectory: ObjCBool = false
        let exist = self.fileExists(atPath: fWriteFile.path, isDirectory: &isDirectory)
        guard !exist || !isDirectory.boolValue else {
            created = true
            return
        }
        do {
            try self.createDirectory(at: fWriteFile, withIntermediateDirectories: true, attributes: nil)
            created = true
        } catch {
            created = false
#if DEBUG
            assert(false, error.localizedDescription)
#endif
        }
    }

    var cacheURL: URL? {
        return self.urls(for: .cachesDirectory, in: .userDomainMask).first
    }
}

// MARK: - Catch Exception from Objc
func catchException(in block: @escaping () -> ()) throws {
    let e = NSException.tryBlockError {
        block()
    }
    guard let exception = e else {
        return
    }
    let error = NSError(domain: exception.name.rawValue, code: NSURLErrorUnknown, userInfo: exception.userInfo as? [String: Any])
    throw error
}

extension Array {
    subscript(from maxItem: Int) -> [Element] {
        let nItems = Swift.min(self.count, maxItem)
        return Array(self[0..<nItems])
    }
}

// MARK: Helper Debug
@discardableResult
func debug<T>(block: () throws -> T?, default: @autoclosure () -> T?) -> T? {
    do {
        let result = try block()
        return result
    } catch {
        printDebug(error)
        return `default`()
    }
}

infix operator -->: TrackOperator
precedencegroup TrackOperator {
    associativity: left
}

func --><E, F>(lhs: E?, rhs: (E?) throws -> F?) -> F? {
    return debug(block: { () -> F? in
        try rhs(lhs)
    }, default: nil)
}

func --><E, F>(lhs: E, rhs: (E) throws -> F?) -> F? {
    return debug(block: { () -> F? in
        try rhs(lhs)
    }, default: nil)
}

func printDebug(_ items: Any...) {
    guard zpEnvironment == .development || zpEnvironment == .sandbox else {
        return
    }
    print(items)
}

extension Swift.Decodable {
    static func toValue(from data: Data) throws -> Self {
        let decoder = JSONDecoder()
        return try decoder.decode(self, from: data)
    }
}

extension Encodable {
    func toData() throws -> Data {
        let encoder = JSONEncoder()
        return try encoder.encode(self)
    }
}



