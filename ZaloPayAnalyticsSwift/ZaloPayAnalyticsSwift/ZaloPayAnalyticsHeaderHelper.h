//
//  ZaloPayAnalyticsHeaderHelper.h
//  ZaloPayAnalyticsSwift
//
//  Created by Dung Vu on 1/15/18.
//  Copyright © 2018 VNG. All rights reserved.
//

#ifndef ZaloPayAnalyticsHeaderHelper_h
#define ZaloPayAnalyticsHeaderHelper_h

//Pods
#import <Objective-Zip/Objective-Zip.h>
#import <Foundation/Foundation.h>
@interface NSException(CatchError)
+ (instancetype _Nullable)tryBlockError: (void(^_Nonnull)(void))block;
@end

#endif /* ZaloPayAnalyticsHeaderHelper_h */
