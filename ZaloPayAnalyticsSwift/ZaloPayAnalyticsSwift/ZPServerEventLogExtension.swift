//
//  ZPServerEventLogExtension.swift
//  ZaloPay
//
//  Created by Dung Vu on 1/25/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ZaloPayModuleProtocols
import ZaloPayAnalyticsPrivate
import AFNetworking
import ZaloPayModuleProtocolsObjc

// MARK: - Access File Protocol
protocol SafeAccessFileProtocol: class {
    var lockEvent: NSRecursiveLock { get }
}

extension SafeAccessFileProtocol {
    internal func excuteActionInSafe(_ block: @escaping () -> ()) {
        lockEvent.lock()
        defer {
            lockEvent.unlock()
        }
        block()
    }
}

extension ZPServerEventLog: SafeAccessFileProtocol {
}

// MARK: - Write File
protocol WriteFileProtocol: class {
    var cacheURL: URL? { get }
    var cacheFileName: String? { get }
    func url(for file: String, type: ZPServerEventLogFileType) -> URL?
}

extension WriteFileProtocol {
    // Using for write at other
    func createFileLog(with name: String) {
        let linkFileTxt = self.url(for: name, type: .txt)
        guard FileManager.default.fileExist(at: linkFileTxt).not else {
            return
        }
        FileManager.default.createFile(at: linkFileTxt)
    }

    func writeLog(using data: Data, with fileName: String) {
        guard let logURL = self.url(for: fileName, type: .txt) else {
            return
        }
        let fileHandler = try? FileHandle(forWritingTo: logURL)
        defer {
            fileHandler?.closeFile()
        }
        fileHandler?.seekToEndOfFile()
        do {
            try catchException(in: {
                fileHandler?.write(data)
            })
        } catch {
#if DEBUG
            print(error.localizedDescription)
#endif
        }

    }
}

extension ZPServerEventLog: WriteFileProtocol {
}

// MARK: - Find Files .zip or .txt
protocol FindFilesLogProtocol {
    func findFiles(_ type: ZPServerEventLogFileType) -> Observable<[URL]>
}

extension FindFilesLogProtocol where Self: SafeAccessFileProtocol, Self: WriteFileProtocol {
    func findFiles(_ type: ZPServerEventLogFileType) -> Observable<[URL]> {
        guard let cacheURL = cacheURL else {
            return Observable.just([])
        }
        return Observable.create { [weak self](s) -> Disposable in
            var result = [URL]()
            // Add prevent write file
            self?.excuteActionInSafe {
                result = (try? FileManager.default.contentsOfDirectory(at: cacheURL, includingPropertiesForKeys: nil, options: .skipsHiddenFiles).filter({
                    ($0.lastPathComponent.hasPrefix("zalopay-logs") || $0.lastPathComponent.hasPrefix("zpv2-logs")) && $0.pathExtension == "\(type.description)"
                })) ?? []
            }
            s.onNext(result)
            s.onCompleted()
            return Disposables.create()
        }
    }
}

extension ZPServerEventLog: FindFilesLogProtocol {}

// MARK: - Zip
protocol ZipFilesLogProtocol: SafeAccessFileProtocol, WriteFileProtocol {}

extension ZipFilesLogProtocol {
    func zip(with urls: [URL], using prefix: String = "zalopay-logs") -> URL? {
        guard let urlZip = self.url(for: "\(prefix)-\(Date().string().remove(at: ":"))", type: .zip) else {
#if DEBUG
            print(self.cacheURL?.absoluteString ?? "")
#endif
            return nil
        }
        self.excuteActionInSafe {
            let zipMaker = OZZipFile(fileName: urlZip.path, mode: .create)
            defer{
                zipMaker.close()
            }

            do {
                try urls.forEach({
                    guard let data = Data(contentsOfOptional: $0), data.isEmpty.not else {
                        return
                    }
                    let fileName = $0.lastPathComponent
                    let zipWriteStream = zipMaker.writeInZip(withName: fileName, compressionLevel: .best)
                    try catchException(in: {
                        zipWriteStream.write(data)
                        zipWriteStream.finishedWriting()
                    })
                })
            } catch {
#if DEBUG
                print(error.localizedDescription)
#endif
            }
        }
        return urlZip
    }
}

extension ZPServerEventLog: ZipFilesLogProtocol {}

// MARK: - Export Log
protocol ExportLogProtocol {

    /// Export Zip all log
    ///
    /// - Returns: event url otional
    func exportZipLogFile() -> Observable<URL?>
}

// MARK: - Upload Log
protocol UploadLogsProtocol: class {
    func uploadZip(with fileURL: URL?) -> Observable<Void>
}

extension UploadLogsProtocol {
    func uploadZip(with fileURL: URL?) -> Observable<Void> {
        guard let fileURL = fileURL else {
            return Observable.empty()
        }
        
        guard let network = zpNetworkProtocol else {
           return Observable.empty()
        }
        
        guard let useId = network.paymentUserId else {
            return Observable.empty()
        }
        
        // Case debug not check
        #if DEBUG
            FileManager.default.removeFile(at: fileURL)
            return Observable.empty()
        #else
            let signal = network.uploadClientLog(withPath: "zp-upload/clientlogs", param: nil) { (form) in
                form?.appendPart(withForm: useId.data(using: .utf8)!, name: "userid")
                try? form?.appendPart(withFileURL: fileURL, name: "fileUpload")
            }
            return eventIgnoreValue(from: signal).retry(3).do(onNext: { (_) in
                FileManager.default.removeFile(at: fileURL)
            })
        #endif
    }
}

extension ZPServerEventLog: UploadLogsProtocol {}

// MARK: - Track userId, deviceId, appsFlyerId
protocol TrackingAppIdInAppProtocol {
    var userId: String? { get }
    var deviceId: String { get }
    var appsFlyerId: String? { get }
}

extension ZPServerEventLog: TrackingAppIdInAppProtocol {
}


