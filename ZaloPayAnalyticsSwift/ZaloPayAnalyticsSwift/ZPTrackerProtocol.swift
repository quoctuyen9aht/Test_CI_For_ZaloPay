//
//  ZPTrackerProtocol.swift
//  ZaloPayAnalyticsSwift
//
//  Created by Dung Vu on 3/12/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import Foundation

public protocol ZPTrackerProtocol {
    func trackEvent(_ eventId: ZPAnalyticEventAction, value eventValue: NSNumber?, label eventLabel: String?)
    func trackEvent(_ eventId: ZPAnalyticEventAction, valueString eventValue: String?, label eventLabel: String?)
    func trackTiming(_ eventId: ZPAnalyticEventAction, value eventValue: NSNumber?)
    func trackScreen(_ screenName: String?)
    func trackCustomEvent(_ eventCategory: String?, action eventAction:String?, label eventLabel:String?, value eventValue: NSNumber?)
}
