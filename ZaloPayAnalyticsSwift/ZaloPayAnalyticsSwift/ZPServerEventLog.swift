//
//  ZPServerEventLog.swift
//  ZaloPay
//
//  Created by Dung Vu on 4/13/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CocoaLumberjackSwift
import ZaloPayAnalyticsPrivate
import ZaloPayModuleProtocolsObjc

/*
 1> File Log Name: zalopay-logs-{yyyyMMddhhmm}.txt, Example: zalopay-logs-201704121911.txt
 2> Duration write log (TimeLog) : 5 minutes
 3> Flow:
    * In TimeLog, write logs to file.
    * End TimeLog, zip file, send to server with api:  v001/zp-upload/clientlogs
 4> Handle Send Log:
    * Success: delete files storage
    * Error or HTTP code <> 2xx: don't work anythings
    * Error , it'll retry 3 times
 5> Active app, after 30s, it'll scan zip file and upload to server
 */

/*
 ZaloPay Data Logging
 Dictionary : {eventType: ZPServerEventLogType,
                eventId: int,
                value: long,
                timestamp: Date}
 */

enum ZPServerEventLogType: Int {
    case event = 1
    case timming
    case transaction
    case errorApi
    case errorNotification
    case bin
    case parseOrVerifyFileConfigError
    case logNotification
    case logDeepLink
    case logScreenName
    case logOpenApp
    case logWebEvent
}

enum ZPServerEventWriteLogType: Int {
    case normal = 0
    case transaction
    case errorApi
    case errorNotification
    case bin
    case logNotification
    case logDeepLink
    case logScreenName
    case logOpenApp
    case logWebEvent
    
    var prefix: String {
        switch self {
        case .normal:
            return "zalopay-logs-events-"
        case .transaction:
            return "zpv2-logs-apptransid-"
        case .errorApi:
            return "zalopay-logs-api-errors-"
        case .errorNotification:
            return "zalopay-logs-pc-errors-"
        case .bin:
            return "zalopay-logs-card-errors-"
        case .logNotification:
            return "zalopay-logs-notification-"
        case .logDeepLink:
            return "zalopay-logs-deepLink-"
        case .logScreenName:
            return "zalopay-logs-screen-"
        case .logOpenApp:
            return "zalopay-logs-openapp-"
        case .logWebEvent:
            return "zalopay-logs-webevent-"
        }
    }
}

enum ZPServerEventLogFileType: CustomStringConvertible {
    case txt
    case zip

    var description: String {
        switch self {
        case .txt:
            return "txt"
        case .zip:
            return "zip"
        }
    }
}

//https://gitlab.zalopay.vn/zalopay-apps/task/issues/1117#note_13337

enum ZPLogOSType: Int {
    case Android = 1
    case iOS = 2
}


fileprivate let durationCheckFiles: TimeInterval = 30
fileprivate let trackingConfigId = 1
fileprivate let maxUploadFileTxt = 20

@objcMembers
public final class ZPServerEventLog: NSObject {
    public static var durationWriteLogs: TimeInterval = 120
    var deviceId: String {
        guard let _deviceId = zpDeviceId else {
            return ""
        }
        return _deviceId
    }
    
    private var currentDateCheck: Date! {
        didSet {
            guard let d = currentDateCheck else {
                _cacheFileName = nil
                return
            }
            _cacheFileName = d.string().remove(at: ":")
        }
    }
    var cacheFileName: String? {
        return _cacheFileName
    }
    private var _cacheFileName: String?
    private (set) lazy var cacheURL: URL? = {
        return FileManager.default.cacheURL
    }()

    var userId: String? {
        return zpUserId
    }
    var appsFlyerId: String? {
        return zpAppflyerId
    }

    // Need to config
    public var isWriteLogSession: Bool = false {
        didSet {
            guard isWriteLogSession else {
                return
            }

            self.excuteActionInSafe { [unowned self] in
                // check exist log session
                guard self.allTrackerLogs.contains(where: { $0 is UploadLogsSessionProtocol }).not else {
                    return
                }
                let sessionLog = ZPLogSession<ZPLogSessionWorker>.init(using: self)
                self.allTrackerLogs.append(sessionLog)
            }
        }
    }

    private (set) var lockEvent: NSRecursiveLock = NSRecursiveLock()
    private let disposeBag = DisposeBag()
    private var diposableCheckFile: Disposable?

    private var allTrackerLogs: [UpdateLogContentProtocol] = []

    public override init() {
        super.init()
        self.setupTrackers()
        self.setupUploadFile()
        self.setupCheckAppActiveEvent()
    }

    private func setupTrackers() {
        let appsFlyerLog = ZPAppsflyerLog.init(with: self)
        allTrackerLogs.append(appsFlyerLog)
    }

    private func setupUploadFile() {
        Observable<Int>
                .interval(ZPServerEventLog.durationWriteLogs, scheduler: SerialDispatchQueueScheduler(qos: .background))
                .takeUntil(self.rx.deallocating)
                .do(onNext: { [weak self](_) in
                    self?.resetDate()
                })
                .flatMap({ [weak self] _ -> Observable<[URL]> in
                    guard let wSelf = self else {
                        return Observable.empty()
                    }
                    return wSelf.findFiles(.txt)
                }).bind(onNext: { [unowned self] in
                    self.prepareUpload(from: $0[from: maxUploadFileTxt])
                }).disposed(by: disposeBag)
    }

    fileprivate func prepareUpload(from textFiles: [URL]) {
        self.uploadTxtFiles(from: textFiles).subscribe(onError: { (e) in
            DDLogDebug(e.localizedDescription)
        }).disposed(by: disposeBag)
    }

    fileprivate func setupCheckAppActiveEvent() {
        NotificationCenter.default.rx
                .notification(NSNotification.Name.UIApplicationDidBecomeActive)
                .observeOn(SerialDispatchQueueScheduler(qos: .background))
                .takeUntil(self.rx.deallocating).bind(onNext: { [weak self](_) in
                    self?.checkErrorFileWhenAppBecomeActive()
                })
                .disposed(by: disposeBag)
    }

    func checkErrorFileWhenAppBecomeActive() {
        resetDisposableCheckFile()
        diposableCheckFile = Observable.just("check")
                .delay(durationCheckFiles, scheduler: SerialDispatchQueueScheduler(qos: .background))
                .flatMap({ [unowned self] _ in self.findFiles(.zip) })
                .flatMap({ [unowned self] in
                    self.uploadFiles(from: $0)
                })
                .subscribe(onError: { [weak self](e) in
                    DDLogDebug(e.localizedDescription)
                    self?.resetDisposableCheckFile()
                }, onCompleted: { [weak self] in
                    self?.resetDisposableCheckFile()
                })
    }

    private func resetDate() {
        excuteActionInSafe { [unowned self] in
            self.currentDateCheck = nil
        }
    }

    private func resetDisposableCheckFile() {
        guard diposableCheckFile != nil else {
            return
        }
        excuteActionInSafe { [unowned self] in
            self.diposableCheckFile?.dispose()
            self.diposableCheckFile = nil
        }
    }

    private func fileName(type: ZPServerEventWriteLogType) -> String {
        let today = Date()
        var name: String = ""
        excuteActionInSafe { [unowned self] in
            defer {
                name = "\(type.prefix)\(self._cacheFileName ?? "")"
                self.createFileLog(with: name)
            }

            // Nil
            guard let dateCheck = self.currentDateCheck else {
                self.currentDateCheck = today
                return
            }

            // Same Date
            guard Calendar.current.isDate(dateCheck, inSameDayAs: today).not else {
                return
            }
            self.currentDateCheck = today

        }
#if DEBUG
        assert(name.count > 0, "Please check this case !!!!")
#endif
        return name
    }
}

// MARK: - Public function 
extension ZPServerEventLog: ZPAnalyticSwiftCustomTrackProtocol {
    public func uploadLogSession() {
        self.allTrackerLogs.compactMap({ $0 as? UploadLogsSessionProtocol }).first?.uploadLogsSession()
    }
    
    public func trackParseOrVerifyConfigError(_ eventValue: Int) {
        let str = makeDataWriteLog(ZPServerEventLogType.parseOrVerifyFileConfigError, eventId: trackingConfigId, value: "\(eventValue)", label: "")
        let data = "\(str)\n".data(using: .utf8)
        writeLog(using: data, mode: .normal)
        self.allTrackerLogs.forEach({ $0.writeLog(using: str, with: .normal) })
    }

    public func writeLog(bin: NSDictionary?) {
        // Validate
        guard var t = bin as? [String: Any] else {
            return
        }
        t.defaultParamsLog()
        t["type"] = ZPServerEventLogType.bin.rawValue
        let data = createData(from: t, .bin)
        writeLog(using: data, mode: .bin)
        self.allTrackerLogs.forEach({ $0.writeLog(using: t, with: .bin) })
    }

    public func writeLogAll(transactions: Array<NSDictionary>?) {
        guard let transactions = transactions, transactions.count > 0 else {
            return
        }
        transactions.forEach({ self.writeLog(transaction: $0) })
        Observable.just("Upload")
                .observeOn(SerialDispatchQueueScheduler(qos: .background))
                .do(onNext:
                { [weak self](_) in
                    self?.resetDate()
                })
                .flatMap({ [weak self] _ -> Observable<[URL]> in
                    guard let wSelf = self else {
                        return Observable.empty()
                    }
                    return wSelf.findFiles(.txt)
                }).bind(onNext: { [unowned self] in
                    self.prepareUpload(from: $0[from: maxUploadFileTxt])
                }).disposed(by: disposeBag)
    }

    public func writeLog(transaction: NSDictionary?) {
        // Validate
        guard var t = transaction as? [String: Any] else {
            return
        }
        t.defaultParamsLog()
        t["type"] = ZPServerEventLogType.transaction.rawValue
        let data = createData(from: t, .transaction)
        writeLog(using: data, mode: .transaction)

        // filter appsflyer log
        self.allTrackerLogs.filter({ ($0 is ZPAppsflyerLog).not }).forEach({ $0.writeLog(using: t, with: .bin) })
    }

    public func writeLogErrorApi(errorLog: NSDictionary?) {
        // Validate
        guard var e = errorLog as? [String: Any] else {
            return
        }
        e.defaultParamsLog()
        e["type"] = ZPServerEventLogType.errorApi.rawValue
        let data = createData(from: e, .errorApi)
        writeLog(using: data, mode: .errorApi)
        self.allTrackerLogs.forEach({ $0.writeLog(using: e, with: .errorApi) })
    }

    public func writeLogErrorNotification(error: NSDictionary?) {
        // Validate
        guard var e = error as? [String: Any] else {
            return
        }
        e.defaultParamsLog()
        
        // Format: eventType \t currentUid \t receivedUid \t mtuid \t sourceid \t timestamp \t ostype \t appsflyerId  \t deviceid \t sessionid
        let f = "%ld\t%@\t%ld\t%ld\t%ld\t%ld\t%ld\t%@\t%@\t%@"
        let jsonString = String(format: "\(f)",
            ZPServerEventLogType.errorNotification.rawValue,
            e.value(forKey: "currentId", defaultValue: ""),
            e.value(forKey: "usrid", defaultValue: 0),
            e.value(forKey: "mtuid", defaultValue: 0),
            e.value(forKey: "sourceid", defaultValue: 0),
            e.value(forKey: "timestamp", defaultValue: 0),
            e.value(forKey: "ostype", defaultValue: 0),
            e.value(forKey: "appsflyerId", defaultValue: ""),
            e.value(forKey: "deviceid", defaultValue: ""),
            e.value(forKey: "sessionid", defaultValue: "")
        )
        let data = ("\(jsonString)\n").data(using: .utf8)
        writeLog(using: data, mode: .errorNotification)
        self.allTrackerLogs.forEach({ $0.writeLog(using: jsonString, with: .errorNotification) })
    }

    public func writeLogNotification(log: NSDictionary?) {
        // Validate
        guard var log = log as? [String: Any] else {
            return
        }
        log.defaultParamsLog()
        log["type"] = ZPServerEventLogType.logNotification.rawValue
        let data = createData(from: log, .logNotification)
        writeLog(using: data, mode: .logNotification)
        self.allTrackerLogs.forEach({ $0.writeLog(using: log, with: .logNotification) })
    }

    public func writeLogDeepLink(log: NSDictionary?) {
        // Validate
        guard var log = log as? [String: Any] else {
            return
        }
        log.defaultParamsLog()
        log["type"] = ZPServerEventLogType.logDeepLink.rawValue
        let data = createData(from: log, .logDeepLink)
        writeLog(using: data, mode: .logDeepLink)
        self.allTrackerLogs.forEach({ $0.writeLog(using: log, with: .logDeepLink) })
    }

    public func writeLogOpenApp(log: NSDictionary?) {
        // Validate
        guard var log = log as? [String: Any] else {
            return
        }
        log.defaultParamsLog()
        log["type"] = ZPServerEventLogType.logOpenApp.rawValue
        let data = createData(from: log, .logOpenApp)
        writeLog(using: data, mode: .logOpenApp)
        self.allTrackerLogs.forEach({ $0.writeLog(using: log, with: .logOpenApp) })
    }
    
    public func writeLogWebEvent(log: NSDictionary?) {
        // Validate
        guard var log = log as? [String: Any] else {
            return
        }
        log.defaultParamsLog()
        log["type"] = ZPServerEventLogType.logWebEvent.rawValue
        let data = createData(from: log, .logWebEvent)
        writeLog(using: data, mode: .logWebEvent)
        self.allTrackerLogs.forEach({ $0.writeLog(using: log, with: .logWebEvent) })
    }
}

// MARK: - Zip File
extension ZPServerEventLog {
    private func zip(fileLog: URL) -> URL? {
        let file = fileLog.deletingPathExtension().lastPathComponent
        guard let zipFile = self.url(for: file, type: .zip) else {
            return nil
        }
        self.excuteActionInSafe {
            guard let data = Data(contentsOfOptional: fileLog) else {
                return
            }
            let zipMaker = OZZipFile(fileName: zipFile.path, mode: .create)
            let zipWriteStream = zipMaker.writeInZip(withName: "\(file).txt", compressionLevel: .best)
            zipWriteStream.write(data)
            zipWriteStream.finishedWriting()
            zipMaker.close()
        }
        return zipFile
    }
}

// MARK: - Write Log
extension ZPServerEventLog {
    fileprivate func writeLog(using data: Data?, mode: ZPServerEventWriteLogType) {
        guard let data = data else {
            return
        }
        self.excuteActionInSafe { [unowned self] in
            let fileName = self.fileName(type: mode)
            self.writeLog(using: data, with: fileName)
        }
    }
}

// MARK: - Upload
extension ZPServerEventLog {
    private func uploadTxtFiles(from urlTxts: [URL]?) -> Observable<Void> {
        guard let urlTxts = urlTxts, urlTxts.count > 0 else {
            return Observable.empty()
        }
        let urlZipFile = self.zip(with: urlTxts)
        defer {
            urlTxts.forEach({ FileManager.default.removeFile(at: $0) })
        }
        return self.upload(with: urlZipFile).catchErrorJustReturn(())
    }

    private func uploadFiles(from urls: [URL?]) -> Observable<Void> {
        let nUrls = urls.compactMap({ $0 })
        guard nUrls.count > 0 else {
            return Observable.empty()
        }
        return Observable.zip(nUrls.map({ self.upload(with: $0) })).map({ _ in }).catchErrorJustReturn(())
    }

    private func upload(with fileURL: URL?) -> Observable<Void> {
        return self.uploadZip(with: fileURL)
    }
}

// MARK: - Helper
extension ZPServerEventLog {
    fileprivate func createData(from params: [String: Any]?, _ type: ZPServerEventWriteLogType) -> Data? {
        guard let params = params else {
            return nil
        }
        guard let jsonData = try? JSONSerialization.data(withJSONObject: params, options: []) else {
            return nil
        }
        guard var jsonString = String(data: jsonData, encoding: .utf8) else {
            return nil
        }
        jsonString = "\(jsonString)\n"
        let result = jsonString.data(using: .utf8)
        return result
    }

    internal func url(for file: String, type: ZPServerEventLogFileType) -> URL? {
        return cacheURL?.appendingPathComponent("\(file).\(type.description)")
    }

    private func makeDataWriteLog(_ eventType: ZPServerEventLogType, timming: Int64 = 0, eventId: Int, value: String, label: String) -> String {
        var eventLog = EventLog()
        eventLog.type = Int32(eventType.rawValue)
        eventLog.id = Int32(eventId)
        eventLog.value = timming
        eventLog.description_p = value.isEmpty ? " " : value
        eventLog.previousScreen = label.isEmpty ? " " : label
        eventLog.default()
        let jsonString = eventLog.toString
        return jsonString
    }
}

// MARK: - Get Event
extension ZPServerEventLog: ZPTrackerProtocol {
    public func trackScreen(_ screenName: String?) {
        guard let screenName = screenName, screenName.isEmpty.not else {
            return
        }
        var json: [String: Any] = [:]
        json.defaultParamsLog()
        json["type"] = ZPServerEventLogType.logScreenName.rawValue
        json["screenName"] = screenName
        let data = createData(from: json, .logScreenName)
        writeLog(using: data, mode: .logScreenName)
        self.allTrackerLogs.forEach({ $0.writeLog(using: json, with: .logScreenName) })
    }

    public func trackEvent(_ eventId: ZPAnalyticEventAction, valueString eventValue: String?, label eventLabel: String?) {
        let str = makeDataWriteLog(ZPServerEventLogType.event, eventId: eventId.rawValue, value: eventValue ?? "", label: (eventLabel ?? ""))
        let data = "\(str)\n".data(using: .utf8)
        zpCanUseGRPC --> { !$0 ? writeLog(using: data, mode: .normal) : {}() }
        self.allTrackerLogs.forEach({ $0.writeLog(using: str, with: .normal) })
    }
    
    public func trackEvent(_ eventId: ZPAnalyticEventAction, value eventValue: NSNumber?, label eventLabel: String?) {
        let str = makeDataWriteLog(ZPServerEventLogType.event, eventId: eventId.rawValue, value: eventValue?.stringValue ?? "", label: (eventLabel ?? ""))
        let data = "\(str)\n".data(using: .utf8)
        zpCanUseGRPC --> { !$0 ? writeLog(using: data, mode: .normal) : {}() }
        self.allTrackerLogs.forEach({ $0.writeLog(using: str, with: .normal) })
    }

    public func trackTiming(_ eventId: ZPAnalyticEventAction, value eventValue: NSNumber?) {
        let str = makeDataWriteLog(ZPServerEventLogType.timming, timming: eventValue?.int64Value ?? 0, eventId: eventId.rawValue, value: "", label: "")
        let data = "\(str)\n".data(using: .utf8)
        writeLog(using: data, mode: .normal)
        self.allTrackerLogs.forEach({ $0.writeLog(using: str, with: .normal) })
    }

    public func trackCustomEvent(_ eventCategory: String?, action eventAction: String?, label eventLabel: String?, value eventValue: NSNumber?) {
        var dictionary: [String: Any] = [:]
        dictionary.defaultParamsLog()
        dictionary["category"] = eventCategory
        dictionary["action"] = eventAction
        dictionary["label"] = eventLabel
        writeLogDeepLink(log: dictionary as NSDictionary)
    }
}

extension Dictionary where Key == String, Value == Any {
    mutating func defaultParamsLog() {
        var temp = self
        temp["ostype"] = ZPLogOSType.iOS.rawValue
        temp["timestamp"] = Int(Date().timeIntervalSince1970)
        temp["deviceid"] = zpDeviceId
        temp["sessionid"] = ZPAnalyticSwiftConfig.sessionId()
        temp["appsflyerId"] = zpAppflyerId
        temp["userid"] = zpUserId
        self = temp
    }
}



