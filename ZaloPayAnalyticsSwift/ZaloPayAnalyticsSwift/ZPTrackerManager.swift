//
//  ZPTrackerManager.swift
//  ZaloPayAnalyticsSwift
//
//  Created by Dung Vu on 3/12/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

@objcMembers
public final class ZPTrackerManager: NSObject {
    private var allTrackers: [ZPTrackerProtocol] = []
    private let disposeBag = DisposeBag()
    private lazy var queue = DispatchQueue.init(label: "zaloPay.trackerManager.syncLog", qos: .background)
    public override init() {
        super.init()
        setupEvent()
    }
    
    private func setupEvent() {
        NotificationCenter.default.rx
            .notification(Notification.Name.UIApplicationDidBecomeActive)
            .observeOn(SerialDispatchQueueScheduler.init(qos: .background))
            .bind { (_) in
                ZPAnalyticSwiftConfig.updateSession()
            }.disposed(by: disposeBag)
    }
    
    /// Add new tracker, in this 'll keep reference your object, please remove manual if you sure that it not use avoid memory leak
    ///
    /// - Parameter eventTracker: object conform ZPTrackerProtocol
    public func registerTracker(_ eventTracker: ZPTrackerProtocol) {
        queue.sync { [unowned self] in
            self.allTrackers.append(eventTracker)
        }
    }


    /// Remove tracker
    ///
    /// - Parameter eventTracker: object conform ZPTrackerProtocol
    /// - Returns: boolean result

//    public func removeTracker(_ eventTracker: ZPTrackerProtocol) -> Bool {
//        var result: Bool = true
//        queue.sync { [unowned self] in
//            guard let idx = self.allTrackers.index(where: { $0.hash == eventTracker.hash }) else {
//                result = false
//                return
//            }
//            self.allTrackers.remove(at: idx)
//        }
//#if DEBUG
//        assert(result, "Check because it can't remove")
//#endif
//        return result
//    }
}

extension ZPTrackerManager: ZPTrackerProtocol {
    public func trackEvent(_ eventId: ZPAnalyticEventAction, valueString eventValue: String?, label eventLabel: String?) {
        queue.async { [unowned self] in
            self.allTrackers.forEach({ $0.trackEvent(eventId, valueString: eventValue, label: eventLabel) })
        }
    }
    
    public func trackEvent(_ eventId: ZPAnalyticEventAction, value eventValue: NSNumber?, label eventLabel: String?) {
        queue.async { [unowned self] in
            self.allTrackers.forEach({ $0.trackEvent(eventId, value: eventValue, label: eventLabel) })
        }
    }

    public func trackTiming(_ eventId: ZPAnalyticEventAction, value eventValue: NSNumber?) {
        queue.async { [unowned self] in
            self.allTrackers.forEach({ $0.trackTiming(eventId, value: eventValue) })
        }
    }

    public func trackScreen(_ screenName: String?) {
        queue.async { [unowned self] in
            self.allTrackers.forEach({ $0.trackScreen(screenName) })
        }
    }

    public func trackCustomEvent(_ eventCategory: String?, action eventAction: String?, label eventLabel: String?, value eventValue: NSNumber?) {
        queue.async { [unowned self] in
            self.allTrackers.forEach({ $0.trackCustomEvent(eventCategory, action: eventAction, label: eventLabel, value: eventValue) })
        }
    }
}

extension ZPTrackerManager: ZPAnalyticSwiftCustomTrackProtocol {
    public func uploadLogSession() {
        queue.async { [unowned self] in
            self.allTrackers
                .compactMap({ $0 as? ZPAnalyticSwiftCustomTrackProtocol })
                .forEach({
               $0.uploadLogSession()
            })
        }
    }
    
    public func trackParseOrVerifyConfigError(_ eventValue: Int) {
        queue.async { [unowned self] in
            self.allTrackers
                .compactMap({ $0 as? ZPAnalyticSwiftCustomTrackProtocol })
                .forEach({
                    $0.trackParseOrVerifyConfigError(eventValue)
                })
        }
    }
    
    public func writeLog(bin: NSDictionary?) {
        queue.async { [unowned self] in
            self.allTrackers
                .compactMap({ $0 as? ZPAnalyticSwiftCustomTrackProtocol })
                .forEach({
                    $0.writeLog(bin: bin)
                })
        }
    }
    
    public func writeLogAll(transactions: Array<NSDictionary>?) {
        queue.async { [unowned self] in
            self.allTrackers
                .compactMap({ $0 as? ZPAnalyticSwiftCustomTrackProtocol })
                .forEach({
                    $0.writeLogAll(transactions: transactions)
                })
        }
    }
    
    public func writeLog(transaction: NSDictionary?) {
        queue.async { [unowned self] in
            self.allTrackers
                .compactMap({ $0 as? ZPAnalyticSwiftCustomTrackProtocol })
                .forEach({
                    $0.writeLog(transaction: transaction)
                })
        }
    }
    
    public func writeLogErrorApi(errorLog: NSDictionary?) {
        queue.async { [unowned self] in
            self.allTrackers
                .compactMap({ $0 as? ZPAnalyticSwiftCustomTrackProtocol })
                .forEach({
                    $0.writeLogErrorApi(errorLog: errorLog)
                })
        }
    }
    
    public func writeLogErrorNotification(error: NSDictionary?) {
        queue.async { [unowned self] in
            self.allTrackers
                .compactMap({ $0 as? ZPAnalyticSwiftCustomTrackProtocol })
                .forEach({
                    $0.writeLogErrorNotification(error: error)
                })
        }
    }
    
    public func writeLogNotification(log: NSDictionary?) {
        queue.async { [unowned self] in
            self.allTrackers
                .compactMap({ $0 as? ZPAnalyticSwiftCustomTrackProtocol })
                .forEach({
                    $0.writeLogNotification(log: log)
                })
        }
    }
    
    public func writeLogDeepLink(log: NSDictionary?) {
        queue.async { [unowned self] in
            self.allTrackers
                .compactMap({ $0 as? ZPAnalyticSwiftCustomTrackProtocol })
                .forEach({
                    $0.writeLogDeepLink(log: log)
                })
        }
    }
    
    public func writeLogOpenApp(log: NSDictionary?) {
        queue.async { [unowned self] in
            self.allTrackers
                .compactMap({ $0 as? ZPAnalyticSwiftCustomTrackProtocol })
                .forEach({
                    $0.writeLogOpenApp(log: log)
                })
        }
    }
    
    public func writeLogWebEvent(log: NSDictionary?) {
        queue.async { [unowned self] in
            self.allTrackers
                .compactMap({ $0 as? ZPAnalyticSwiftCustomTrackProtocol })
                .forEach({
                    $0.writeLogWebEvent(log: log)
                })
        }
    }
}
