//
//  ZPTrackerEvents.swift
//  ZaloPayAnalytics
//
//  Auto-generated.
//  Copyright (c) 2016-present VNG Corporation. All rights reserved.
//

import Foundation


// define enum
@objc public enum ZPAnalyticEventAction: Int {
    case app_launch = 11000
    case onboarding_input_payment_passcode = 11100
    case onboarding_confirm_payment_passcode_success = 11102
    case onboarding_confirm_payment_passcode_fail = 11103
    case onboarding_confirm_payment_passcode_back = 11104
    case onboarding_input_phone_number_invalid = 11105
    case onboarding_input_phone_number_duplicate = 11106
    case onboarding_submit_phone_number = 11107
    case onboarding_input_otp_success = 11108
    case onboarding_input_otp_fail = 11109
    case onboarding_resend_otp = 11110
    case onboarding_submit_otp = 11111
    case onboardingkyc_view_inform = 11150
    case onboardingkyc_input_phone_number_invalid = 11151
    case onboardingkyc_input_phone_number_duplicate = 11152
    case onboardingkyc_input_real_name_invalid = 11153
    case onboardingkyc_input_personal_id_invalid = 11154
    case onboardingkyc_input_citizen_id_invalid = 11155
    case onboardingkyc_input_passport_invalid = 11156
    case onboardingkyc_input_dob_invalid = 11157
    case onboardingkyc_view_term_and_condition = 11158
    case onboardingkyc_submit_user_info = 11159
    case onboardingkyc_input_otp_success = 11160
    case onboardingkyc_input_otp_fail = 11161
    case onboardingkyc_resend_otp = 11162
    case onboardingkyc_submit_otp = 11163
    case onboardingkyc_input_payment_passcode = 11164
    case onboardingkyc_confirm_payment_passcode_success = 11165
    case onboardingkyc_confirm_payment_passcode_fail = 11166
    case onboardingkyc_confirm_payment_passcode_back = 11167
    case onboardingkyc_submit_payment_passcode = 11168
    case login_touch = 11200
    case login_success = 11201
    case login_fail = 11202
    case login_touch_register = 11203
    case home_launched = 11300
    case home_touch_pay = 11301
    case home_touch_balance = 11302
    case home_touch_search = 11303
    case home_touch_notification = 11304
    case home_touch_notification_new = 11305
    case home_touch_home = 11306
    case home_touch_transactionlog = 11307
    case home_touch_promotion = 11308
    case home_touch_promotion_new = 11309
    case home_touch_profile = 11310
    case home_touch_bank = 11311
    case home_touch_moneytransfer = 11312
    case home_touch_moneyreceive = 11313
    case home_touch_service = 11314
    case home_touch_id_app = 11350
    case search_touch_result = 11400
    case search_touch_suggest = 11401
    case search_touch_back = 11402
    case notification_launched = 11500
    case notification_touch_transactionitem = 11501
    case transactionlog_touch_back = 11601
    case transactionlog_alltrans_launched = 11602
    case transactionlog_alltrans_touch_back = 11603
    case transactionlog_sendtransfer_launched = 11604
    case transactionlog_sendtransfer_touch_back = 11605
    case transactionlog_sendtransfer_touch_sendmore = 11606
    case transactionlog_receivetransfer_launched = 11607
    case transactionlog_receivetransfer_touch_back = 11608
    case transactionlog_receivetransfer_touch_sendback = 11609
    case transactionlog_receivetransfer_touch_thanks = 11610
    case transactionlog_touch_filter = 11613
    case transactionlog_filter_touch_time = 11614
    case transactionlog_filter_time_touch_close = 11615
    case transactionlog_filter_time_touch_apply = 11616
    case transactionlog_filter_touch_deposit = 11617
    case transactionlog_filter_touch_receive = 11618
    case transactionlog_filter_touch_send = 11619
    case transactionlog_filter_touch_payment = 11620
    case transactionlog_filter_touch_redpacket = 11621
    case transactionlog_filter_touch_withdraw = 11622
    case transactionlog_filter_touch_success = 11623
    case transactionlog_filter_touch_fail = 11624
    case transactionlog_filter_touch_cancel = 11625
    case transactionlog_filter_touch_apply = 11626
    case promotion_launched = 11700
    case promotion_touch_detail = 11701
    case promotion_detail_touch_back = 11702
    case promotion_detail_touch_share = 11703
    case promotion_detail_share_zalo = 11704
    case promotion_detail_share_urlcopy = 11705
    case promotion_detail_share_refresh = 11706
    case promotion_detail_share_tobrowser = 11707
    case promotion_detail_share_touch_close = 11708
    case voucher_detail_touch_back = 11709
    case voucher_popup_save = 11710
    case voucher_popup_close = 11711
    case cashback_popup_accept = 11712
    case me_touch_info = 11800
    case me_touch_balance = 11830
    case me_touch_bank = 11831
    case me_touch_supportcenter = 11832
    case me_touch_feedback = 11833
    case me_touch_about = 11834
    case me_about_touch_rate = 11835
    case me_about_touch_features = 11836
    case me_about_touch_term = 11837
    case me_touch_security = 11851
    case me_security_touch_back = 11852
    case me_security_touch_touchid = 11853
    case me_security_touch_privacy = 11854
    case me_security_touch_changepassword = 11855
    case me_security_changepassword_back = 11856
    case me_security_changepassword_input = 11857
    case me_security_changepassword_continue = 11858
    case me_security_changepassword_result = 11859
    case me_security_touch_pass_open = 11860
    case me_security_touch_pass_time_set = 11861
    case me_security_touch_pass_time_now = 11862
    case me_security_touch_pass_time_30 = 11863
    case me_security_touch_pass_time_60 = 11864
    case me_security_touch_pass_time_180 = 11865
    case me_security_touch_pass_time_300 = 11866
    case me_security_touch_pass_time_close = 11867
    case me_security_touch_cover = 11868
    case me_security_touch_logout = 11869
    case me_touch_giftlist = 11881
    case me_giftlist_touch_detail = 11882
    case Timing_GetOrder = 12000
    case Timing_ScanQR = 12001
    case api_um_createaccesstoken = 12010
    case api_um_removeaccesstoken = 12011
    case api_um_verifycodetest = 12012
    case api_um_updateprofile = 12013
    case api_um_verifyotpprofile = 12014
    case api_um_recoverypin = 12015
    case api_um_getUserinfo = 12016
    case api_um_getUserprofilelevel = 12017
    case api_um_getUserinfobyzalopayname = 12018
    case api_um_getUserinfobyzalopayid = 12019
    case api_um_checkzalopaynameexist = 12020
    case api_um_updatezalopayname = 12021
    case api_um_validatepin = 12022
    case api_um_sendnotification = 12023
    case api_um_checklistzaloidforclient = 12024
    case api_ummerchant_getmerchantUserinfo = 12025
    case api_ummerchant_getlistmerchantUserinfo = 12026
    case api_umupload_preupdateprofilelevel3 = 12027
    case api_um_listcardinfoforclient = 12028
    case api_um_listbankaccountforclient = 12029
    case api_um_loginviazalo = 12030
    case api_um_registerphonenumber = 12031
    case api_um_authenphonenumber = 12032
    case api_zpc_getuserinfobyphone = 12033
    case api_um_listusersbyphoneforclient = 12034
    case api_um_updateuserinfo4client = 12035
    case api_um_getsessionloginforclient = 12036
    case api_um_removesessionloginforclient = 12037
    case api_um_getuserprofilesimpleinfo = 12038
    case connector_um_createaccesstoken = 12040
    case connector_um_removeaccesstoken = 12041
    case connector_um_verifycodetest = 12042
    case connector_um_updateprofile = 12043
    case connector_um_verifyotpprofile = 12044
    case connector_um_recoverypin = 12045
    case connector_um_getUserinfo = 12046
    case connector_um_getUserprofilelevel = 12047
    case connector_um_getUserinfobyzalopayname = 12048
    case connector_um_getUserinfobyzalopayid = 12049
    case connector_um_checkzalopaynameexist = 12050
    case connector_um_updatezalopayname = 12051
    case connector_um_validatepin = 12052
    case connector_um_sendnotification = 12053
    case connector_um_checklistzaloidforclient = 12054
    case connector_ummerchant_getmerchantUserinfo = 12055
    case connector_ummerchant_getlistmerchantUserinfo = 12056
    case connector_umupload_preupdateprofilelevel3 = 12057
    case connector_um_listcardinfoforclient = 12058
    case connector_um_listbankaccountforclient = 12059
    case connector_um_loginviazalo = 12060
    case connector_um_registerphonenumber = 12061
    case connector_um_authenphonenumber = 12062
    case connector_zpc_getuserinfobyphone = 12063
    case connector_um_listusersbyphoneforclient = 12064
    case connector_um_updateuserinfo4client = 12065
    case connector_um_getsessionloginforclient = 12066
    case connector_um_removesessionloginforclient = 12067
    case connector_um_getuserprofilesimpleinfo = 12068
    case api_v001_tpe_getbalance = 12070
    case api_v001_tpe_createwalletorder = 12071
    case api_v001_tpe_getinsideappresource = 12072
    case api_v001_tpe_getorderinfo = 12073
    case api_v001_tpe_gettransstatus = 12074
    case api_v001_tpe_transhistory = 12075
    case api_v001_tpe_v001getplatforminfo = 12076
    case api_v001_tpe_getappinfo = 12077
    case api_v001_tpe_getbanklist = 12078
    case api_v001_tpe_submittrans = 12079
    case api_v001_tpe_atmauthenpayer = 12080
    case api_v001_tpe_authcardholderformapping = 12081
    case api_v001_tpe_getstatusmapcard = 12082
    case api_v001_tpe_verifycardformapping = 12083
    case api_v001_tpe_getstatusbyapptransidforclient = 12084
    case api_v001_tpe_sdkwriteatmtime = 12085
    case api_v001_tpe_removemapcard = 12086
    case api_v001_tpe_sdkerrorreport = 12087
    case api_v001_tpe_submitmapaccount = 12088
    case api_v001_zp_upload_clientlogs = 12089
    case api_v001_tpe_registeropandlink = 12090
    case api_v001_tpe_verifyaccountformapping = 12091
    case api_v001_tpe_getstatusmapaccount = 12092
    case api_v001_tpe_removemapaccount = 12093
    case api_v001_tpe_getbanklistgateway = 12904
    case connector_v001_tpe_getbalance = 12100
    case connector_v001_tpe_createwalletorder = 12101
    case connector_v001_tpe_getinsideappresource = 12102
    case connector_v001_tpe_getorderinfo = 12103
    case connector_v001_tpe_gettransstatus = 12104
    case connector_v001_tpe_transhistory = 12105
    case connector_v001_tpe_v001getplatforminfo = 12106
    case connector_v001_tpe_getappinfo = 12107
    case connector_v001_tpe_getbanklist = 12108
    case connector_v001_tpe_submittrans = 12109
    case connector_v001_tpe_atmauthenpayer = 12110
    case connector_v001_tpe_authcardholderformapping = 12111
    case connector_v001_tpe_getstatusmapcard = 12112
    case connector_v001_tpe_verifycardformapping = 12113
    case connector_v001_tpe_getstatusbyapptransidforclient = 12114
    case connector_v001_tpe_sdkwriteatmtime = 12115
    case connector_v001_tpe_removemapcard = 12116
    case connector_v001_tpe_sdkerrorreport = 12117
    case connector_v001_tpe_submitmapaccount = 12118
    case connector_v001_tpe_registeropandlink = 12119
    case connector_v001_tpe_verifyaccountformapping = 12120
    case connector_v001_tpe_getstatusmapaccount = 12121
    case connector_v001_tpe_removemapaccount = 12122
    case connector_v001_tpe_getbanklistgateway = 12123
    case api_v001_tpe_getlistpaymentcodes = 12124
    case api_v001_tpe_qrcodepayverifypin = 12125
    case api_um_listusersbyphoneforclientasync = 12126
    case api_um_getrequeststatusforclient = 12127
    case connector_um_listusersbyphoneforclientasync = 12128
    case connector_um_getrequeststatusforclient = 12129
    case api_filter_content = 12197
    case api_redpackage_createbundleorder = 12200
    case api_redpackage_submittosendbundle = 12201
    case api_redpackage_submitopenpackage = 12202
    case api_redpackage_getsentbundlelist = 12203
    case api_redpackage_getrevpackagelist = 12204
    case api_redpackage_getpackagesinbundle = 12205
    case api_redpackage_getappinfo = 12206
    case api_redpackage_submittosendbundlebyzalopayinfo = 12207
    case api_redpackage_getlistpackagestatus = 12208
    case connector_redpackage_createbundleorder = 12210
    case connector_redpackage_submittosendbundle = 12211
    case connector_redpackage_submitopenpackage = 12212
    case connector_redpackage_getsentbundlelist = 12213
    case connector_redpackage_getrevpackagelist = 12214
    case connector_redpackage_getpackagesinbundle = 12215
    case connector_redpackage_getappinfo = 12216
    case connector_redpackage_submittosendbundlebyzalopayinfo = 12217
    case connector_redpackage_getlistpackagestatus = 12218
    case api_notification_ping_pong = 12300
    case api_get_voucher_status = 12500
    case api_use_voucher = 12501
    case api_revert_voucher = 12502
    case connector_get_voucher_status = 12503
    case connector_use_voucher = 12504
    case connector_revert_voucher = 12505
    case api_get_voucher_history = 12506
    case connector_get_voucher_history = 12507
    case api_getusevoucherstatus = 12508
    case api_getrevertvoucherstatus = 12509
    case api_topup_getpricelist = 12601
    case api_topup_getrecentlist = 12602
    case api_topup_gettransbyzptransid = 12603
    case api_topup_createorder = 12604
    case api_topup_updateorder = 12605
    case balance_launched = 13000
    case balance_touch_back = 13001
    case balance_touch_addcash = 13010
    case balance_addcash_touch_back = 13011
    case balance_addcash_input = 13012
    case balance_addcash_result = 13013
    case balance_touch_withdraw = 13020
    case balance_withdraw_touch_back = 13021
    case balance_withdraw_touch_100 = 13022
    case balance_withdraw_touch_200 = 13023
    case balance_withdraw_touch_500 = 13024
    case balance_withdraw_touch_1000 = 13025
    case balance_withdraw_touch_2000 = 13026
    case balance_withdraw_touch_other = 13027
    case balance_withdraw_result = 13028
    case linkbank_launched = 13100
    case linkbank_touch_back = 13101
    case linkbank_touch_add = 13102
    case linkbank_touch_add_icon = 13103
    case linkbank_add_touch_back = 13104
    case linkbank_add_select_jcb = 13105
    case linkbank_add_select_visa = 13106
    case linkbank_add_select_master = 13107
    case linkbank_add_select_bank_code = 13110
    case linkbank_add_bank_touch_close = 13111
    case linkbank_add_bank_close_touch_yes = 13112
    case linkbank_add_bank_close_touch_no = 13113
    case linkbank_add_bank_cardnumber_input = 13114
    case linkbank_add_bank_cardnumber_forward = 13115
    case linkbank_add_bank_date_back = 13116
    case linkbank_add_bank_date_forward = 13117
    case linkbank_add_bank_name_back = 13118
    case linkbank_add_bank_name_forward = 13119
    case linkbank_add_bank_OTP = 13120
    case linkbank_add_bank_OTP_continue = 13121
    case linkbank_add_result = 13122
    case linkbank_delete = 13123
    case linkbank_delete_result = 13124
    case pay_launched = 13200
    case pay_touch_back = 13201
    case pay_touch_scanqr = 13202
    case pay_touch_nfc = 13203
    case pay_touch_bluetooth = 13204
    case pay_scanqr_touch_photo = 13205
    case pay_scanqr_result_fail = 13206
    case pay_scanqr_result_fail_close = 13207
    case pay_scanqr_result_sucess_moneytransfer = 13208
    case pay_scanqr_result_sucess_payment = 13209
    case pay_nfc_result_fail = 13210
    case pay_nfc_result_success = 13211
    case moneytransfer_launched = 13300
    case moneytransfer_touch_back = 13301
    case moneytransfer_touch_phonebook = 13302
    case moneytransfer_touch_phonenumber = 13303
    case moneytransfer_touch_zpid = 13304
    case moneytransfer_touch_recent1 = 13305
    case moneytransfer_touch_recent2 = 13306
    case moneytransfer_touch_recent3 = 13307
    case moneytransfer_touch_recent4 = 13308
    case moneytransfer_touch_recent5 = 13309
    case moneytransfer_touch_recent6 = 13310
    case moneytransfer_touch_recent7 = 13311
    case moneytransfer_touch_recent8 = 13312
    case moneytransfer_touch_recent9 = 13313
    case moneytransfer_touch_recent10 = 13314
    case moneytransfer_touch_recent10plus = 13315
    case moneytransfer_zpid_touch_back = 13316
    case moneytransfer_zpdi_touch_continue = 13317
    case moneytransfer_input_launched = 13318
    case moneytransfer_input_touch_back = 13319
    case moneytransfer_input_amount = 13320
    case moneytransfer_input_amount_touch_clear = 13321
    case moneytransfer_input_message = 13322
    case moneytransfer_input_message_touch_clear = 13323
    case moneytransfer_input_touch_continue = 13324
    case moneyreceive_launched = 13400
    case moneyreceive_touch_back = 13401
    case moneyreceive_result = 13402
    case moneyreceive_touch_input = 13403
    case moneyreceive_input_touch_back = 13404
    case moneyreceive_input_amount = 13405
    case moneyreceive_input_amount_touch_clear = 13406
    case moneyreceive_input_message = 13407
    case moneyreceive_input_message_touch_clear = 13408
    case moneyreceive_input_touch_confirm = 13409
    case moneyreceive_touch_clear = 13410
    case quickpay_launched = 13500
    case quickpay_touch_back = 13501
    case quickpay_touch_cont = 13502
    case quickpay_touch_addcash = 13503
    case quickpay_touch_withdraw = 13504
    case quickpay_codeview = 13505
    case quickpay_codeview_touch_back = 13506
    case quickpay_codeview_touch_barcode = 13507
    case quickpay_codeview_touch_qrcode = 13508
    case quickpay_codeview_touch_customize = 13509
    case quickpay_customize_touch_setupinfor = 13510
    case quickpay_customize_touch_changecode = 13511
    case quickpay_customize_touch_instruction = 13512
    case quickpay_result_success = 13513
    case quickpay_result_fail = 13514
    case redpacket_launched = 14000
    case redpacket_home_touch_start = 14001
    case redpacket_home_touch_back = 14002
    case redpacket_home_touch_sent = 14003
    case redpacket_home_touch_received = 14004
    case redpacket_lixi_touch_back = 14005
    case redpacket_lixi_back_stay = 14006
    case redpacket_lixi_back_close = 14007
    case redpacket_lixi_touch_theme = 14008
    case redpacket_lixi_touch_complete = 14009
    case redpacket_lixi_touch_phonebook = 14010
    case redpacket_lixi_touch_amount = 14011
    case redpacket_theme_touch_back = 14012
    case redpacket_theme_touch_image = 14013
    case redpacket_theme_touch_button = 14014
    case redpacket_theme_wish_expand = 14015
    case redpacket_theme_wish_hide = 14016
    case redpacket_theme_wish_edit = 14017
    case redpacket_theme_preview = 14018
    case redpacket_theme_done = 14019
    case redpacket_preview_back = 14020
    case redpacket_preview_done = 14021
    case redpacket_pay_success = 14022
    case redpacket_success_touch_home = 14023
    case redpacket_success_touch_again = 14024
    case redpacket_pay_fail = 14025
    case redpacket_receive_open = 14026
    case redpacket_receive_close = 14027
    case redpacket_details_equal_back = 14028
    case redpacket_details_equal_play_again = 14029
    case redpacket_details_equal_share = 14030
    case redpacket_details_random_back = 14031
    case redpacket_details_random_ranking = 14032
    case redpacket_details_random_share = 14033
    case redpacket_ranking_play_again = 14034
    case redpacket_ranking_share = 14035
    case redpacket_share_facebook = 14036
    case redpacket_share_fb_messenger = 14037
    case redpacket_share_messages = 14038
    case redpacket_share_others = 14039
    case redpacket_sent_touch_details = 14040
    case redpacket_received_touch_details = 14041
    case redpacket_theme_swipe_1 = 14070
    case redpacket_theme_swipe_2 = 14071
    case redpacket_theme_swipe_3 = 14072
    case redpacket_theme_swipe_4 = 14073
    case redpacket_theme_swipe_5 = 14074
    case redpacket_theme_swipe_6 = 14075
    case redpacket_theme_swipe_7 = 14076
    case redpacket_theme_swipe_8 = 14077
    case redpacket_theme_swipe_9 = 14078
    case redpacket_theme_swipe_10 = 14079
    case redpacket_theme_swipe_11 = 14080
    case redpacket_theme_swipe_12 = 14081
    case redpacket_theme_swipe_13 = 14082
    case redpacket_theme_swipe_14 = 14083
    case redpacket_theme_swipe_15 = 14084
    case redpacket_theme_swipe_16 = 14085
    case redpacket_theme_swipe_17 = 14086
    case redpacket_theme_swipe_18 = 14087
    case redpacket_theme_swipe_19 = 14088
    case redpacket_theme_swipe_20 = 14089
    case redpacket_theme_swipe_21 = 14090
    case topup_launched = 14100
    case topup_touch_back = 14101
    case topup_touch_phonebook = 14102
    case topup_input_phonenumber = 14103
    case topup_touch_buy_prepaid_card = 14104
    case topup_touch_topup_for_other_phonenumber = 14105
    case topup_touch_item_mobilecard = 14106
    case topup_touch_item_electricity = 14107
    case topup_touch_item_water = 14108
    case topup_touch_item_internet = 14109
    case topup_touch_recent = 14110
    case topup_recent_touch_close = 14111
    case topup_recent_touch_item = 14112
    case phonecard_launched = 14113
    case phonecard_touch_back = 14114
    case phonecard_touch_history = 14115
    case phonecard_touch_telco = 14116
    case phonecard_touch_value = 14117
    case phonecard_touch_quantity_minus = 14118
    case phonecard_touch_quantity_plus = 14119
    case phonecard_touch_confirm = 14120
    case phonecard_success_transaction_touch_close = 14121
    case phonecard_transaction_detail_touch_copy = 14122
    case phonecard_transaction_detail_touch_call = 14123
    case phonecard_transaction_detail_touch_code = 14124
    case phonecard_transaction_detail_copy_touch_syntax = 14125
    case phonecard_transaction_detail_copy_touch_code = 14126
    case phonecard_transaction_detail_copy_touch_series = 14127
    case phonecard_transaction_detail_copy_touch_close = 14128
    case phonecard_history_touch_copy = 14129
    case phonecard_history_touch_call = 14130
    case phonecard_history_touch_code = 14131
    case phonecard_history_copy_touch_syntax = 14132
    case phonecard_history_copy_touch_code = 14133
    case phonecard_history_copy_touch_series = 14134
    case phonecard_history_copy_touch_close = 14135
    case electric_launched = 14400
    case electric_touch_back = 14401
    case electric_select_provider = 14402
    case electric_touch_check = 14403
    case electric_touch_add_bill = 14404
    case electric_provider_touch_back = 14405
    case electric_provider_input = 14406
    case electric_input_touch_provider = 14407
    case electric_touch_continue = 14408
    case electric_touch_report = 14409
    case electric_report_swipe = 14410
    case water_launched = 14500
    case water_touch_back = 14501
    case water_select_provider = 14502
    case water_touch_check = 14503
    case water_touch_add_bill = 14504
    case water_provider_touch_back = 14505
    case water_provider_input = 14506
    case water_input_touch_provider = 14507
    case water_touch_continue = 14508
    case internet_launched = 14600
    case internet_touch_back = 14601
    case internet_select_provider = 14602
    case internet_touch_check = 14603
    case internet_touch_add_bill = 14604
    case internet_provider_touch_back = 14605
    case internet_provider_input = 14606
    case internet_input_touch_provider = 14607
    case internet_touch_continue = 14608
    case tv_launched = 14700
    case tv_touch_back = 14701
    case tv_select_provider = 14702
    case tv_touch_check = 14703
    case tv_touch_add_bill = 14704
    case tv_provider_touch_back = 14705
    case tv_provider_input = 14706
    case tv_input_touch_provider = 14707
    case tv_touch_continue = 14708
    case passopen_input_touch = 15000
    case passopen_input_success = 15001
    case passopen_input_fail = 15002
    case passopen_forget_touch = 15003
    case passopen_forget_reset = 15004
    case passopen_forget_close = 15005
    case keyboard_touch_0 = 15100
    case keyboard_touch_000 = 15101
    case keyboard_touch_plus = 15102
    case keyboard_touch_minus = 15103
    case keyboard_touch_multiplication = 15104
    case keyboard_touch_divide = 15105
    case keyboard_touch_delete = 15106
    case popupsdk_launched = 15200
    case popupsdk_source = 15201
    case popupsdk_voucher = 15202
    case popupsdk_pay = 15203
    case popupsdk_close = 15204
    case popupsdk_out_main = 15205
    case popupsdk_source_back = 15206
    case popupsdk_voucher_back = 15207
    case popupsdk_add = 15208
    case popupsdk_password_input = 15209
    case popupsdk_password_fail = 15210
    case popupsdk_password_back = 15211
    case popupsdk_otp_input = 15212
    case popupsdk_otp_fail = 15213
    case popupsdk_otp_resend = 15214
    case popupsdk_otp_close = 15215
    case popupsdk_auth_confirm = 15216
    case popupsdk_auth_cancel = 15217
    case popupsdk_success = 15218
    case popupsdk_fail = 15219
    case popupsdk_voucher_non = 15220
    case sdk_open = 15270
    case sdk_zalopay = 15271
    case sdk_changesource = 15272
    case sdk_add = 15273
    case sdk_voucher = 15274
    case sdk_pay = 15275
    case sdk_password_input = 15276
    case sdk_password_fail = 15277
    case sdk_password_back = 15278
    case sdk_otp_input = 15279
    case sdk_otp_fail = 15280
    case sdk_otp_resend = 15281
    case sdk_otp_close = 15282
    case sdk_auth_confirm = 15283
    case sdk_success = 15284
    case sdk_fail = 15285
    case sdk_voucher_non = 15286
    case sdk_cancel = 15287
    case contact_launched = 15300
    case contact_setting_update = 15301
    case contact_touch_favorite = 15302
    case contact_touch_unfavorite = 15303
    case contact_touch_back = 15304
    case contact_search_input = 15305
    case contact_touch_zalopay = 15306
    case contact_touch_all = 15307
    case contact_touch_refresh_top = 15308
    case contact_touch_refresh_bot = 15309
    case contact_touch_pull_to_refresh = 15310
    case contact_search_phonenumber = 15311
    case contact_search_name = 15312
    case contact_switch_keyboard_to_123 = 15313
    case contact_switch_keyboard_to_abc = 15314
    case contact_touch_friend_favorite = 15315
    case contact_touch_friend_nonfavorite = 15316
    case contact_touch_nonzpfriend = 15317
    case contact_nonzpfriend_touch_close = 15318
    case contact_nonzpfriend_touch_invite = 15319
    case contact_nonzpfriend_invite_touch_zalo = 15320
    case contact_read_contact_deny = 15321
    case contact_launch_from_transfer = 15322
    case contact_launch_from_topup = 15323

    case contact_touch_alphabet = 15330
    case contact_touch_zpid = 15331
    case touch3d_qr = 15900
    case touch3d_transactionlogs = 15901

    case contact_launch_from_lixi = 15324
    

}

@objcMembers
public final class ZPTrackerEvents: NSObject {
    public static func eventActionFromId(_ eventId: ZPAnalyticEventAction) -> String {
        switch (eventId) {
        case .app_launch:
            return "app_launch"
        case .onboarding_input_payment_passcode:
            return "onboarding_input_payment_passcode"
        case .onboarding_confirm_payment_passcode_success:
            return "onboarding_confirm_payment_passcode_success"
        case .onboarding_confirm_payment_passcode_fail:
            return "onboarding_confirm_payment_passcode_fail"
        case .onboarding_confirm_payment_passcode_back:
            return "onboarding_confirm_payment_passcode_back"
        case .onboarding_input_phone_number_invalid:
            return "onboarding_input_phone_number_invalid"
        case .onboarding_input_phone_number_duplicate:
            return "onboarding_input_phone_number_duplicate"
        case .onboarding_submit_phone_number:
            return "onboarding_submit_phone_number"
        case .onboarding_input_otp_success:
            return "onboarding_input_otp_success"
        case .onboarding_input_otp_fail:
            return "onboarding_input_otp_fail"
        case .onboarding_resend_otp:
            return "onboarding_resend_otp"
        case .onboarding_submit_otp:
            return "onboarding_submit_otp"
        case .onboardingkyc_view_inform:
            return "onboardingkyc_view_inform"
        case .onboardingkyc_input_phone_number_invalid:
            return "onboardingkyc_input_phone_number_invalid"
        case .onboardingkyc_input_phone_number_duplicate:
            return "onboardingkyc_input_phone_number_duplicate"
        case .onboardingkyc_input_real_name_invalid:
            return "onboardingkyc_input_real_name_invalid"
        case .onboardingkyc_input_personal_id_invalid:
            return "onboardingkyc_input_personal_id_invalid"
        case .onboardingkyc_input_citizen_id_invalid:
            return "onboardingkyc_input_citizen_id_invalid"
        case .onboardingkyc_input_passport_invalid:
            return "onboardingkyc_input_passport_invalid"
        case .onboardingkyc_input_dob_invalid:
            return "onboardingkyc_input_dob_invalid"
        case .onboardingkyc_view_term_and_condition:
            return "onboardingkyc_view_term_and_condition"
        case .onboardingkyc_submit_user_info:
            return "onboardingkyc_submit_user_info"
        case .onboardingkyc_input_otp_success:
            return "onboardingkyc_input_otp_success"
        case .onboardingkyc_input_otp_fail:
            return "onboardingkyc_input_otp_fail"
        case .onboardingkyc_resend_otp:
            return "onboardingkyc_resend_otp"
        case .onboardingkyc_submit_otp:
            return "onboardingkyc_submit_otp"
        case .onboardingkyc_input_payment_passcode:
            return "onboardingkyc_input_payment_passcode"
        case .onboardingkyc_confirm_payment_passcode_success:
            return "onboardingkyc_confirm_payment_passcode_success"
        case .onboardingkyc_confirm_payment_passcode_fail:
            return "onboardingkyc_confirm_payment_passcode_fail"
        case .onboardingkyc_confirm_payment_passcode_back:
            return "onboardingkyc_confirm_payment_passcode_back"
        case .onboardingkyc_submit_payment_passcode:
            return "onboardingkyc_submit_payment_passcode"
        case .login_touch:
            return "login_touch"
        case .login_success:
            return "login_success"
        case .login_fail:
            return "login_fail"
        case .login_touch_register:
            return "login_touch_register"
        case .home_launched:
            return "home_launched"
        case .home_touch_pay:
            return "home_touch_pay"
        case .home_touch_balance:
            return "home_touch_balance"
        case .home_touch_search:
            return "home_touch_search"
        case .home_touch_notification:
            return "home_touch_notification"
        case .home_touch_notification_new:
            return "home_touch_notification_new"
        case .home_touch_home:
            return "home_touch_home"
        case .home_touch_transactionlog:
            return "home_touch_transactionlog"
        case .home_touch_promotion:
            return "home_touch_promotion"
        case .home_touch_promotion_new:
            return "home_touch_promotion_new"
        case .home_touch_profile:
            return "home_touch_profile"
        case .home_touch_bank:
            return "home_touch_bank"
        case .home_touch_moneytransfer:
            return "home_touch_moneytransfer"
        case .home_touch_moneyreceive:
            return "home_touch_moneyreceive"
        case .home_touch_service:
            return "home_touch_service"
        case .home_touch_id_app:
            return "home_touch_id_%ld"
        case .search_touch_result:
            return "search_touch_result"
        case .search_touch_suggest:
            return "search_touch_suggest"
        case .search_touch_back:
            return "search_touch_back"
        case .notification_launched:
            return "notification_launched"
        case .notification_touch_transactionitem:
            return "notification_touch_transactionitem"
        case .transactionlog_touch_back:
            return "transactionlog_touch_back"
        case .transactionlog_alltrans_launched:
            return "transactionlog_alltrans_launched"
        case .transactionlog_alltrans_touch_back:
            return "transactionlog_alltrans_touch_back"
        case .transactionlog_sendtransfer_launched:
            return "transactionlog_sendtransfer_launched"
        case .transactionlog_sendtransfer_touch_back:
            return "transactionlog_sendtransfer_touch_back"
        case .transactionlog_sendtransfer_touch_sendmore:
            return "transactionlog_sendtransfer_touch_sendmore"
        case .transactionlog_receivetransfer_launched:
            return "transactionlog_receivetransfer_launched"
        case .transactionlog_receivetransfer_touch_back:
            return "transactionlog_receivetransfer_touch_back"
        case .transactionlog_receivetransfer_touch_sendback:
            return "transactionlog_receivetransfer_touch_sendback"
        case .transactionlog_receivetransfer_touch_thanks:
            return "transactionlog_receivetransfer_touch_thanks"
        case .transactionlog_touch_filter:
            return "transactionlog_touch_filter"
        case .transactionlog_filter_touch_time:
            return "transactionlog_filter_touch_time"
        case .transactionlog_filter_time_touch_close:
            return "transactionlog_filter_time_touch_close"
        case .transactionlog_filter_time_touch_apply:
            return "transactionlog_filter_time_touch_apply"
        case .transactionlog_filter_touch_deposit:
            return "transactionlog_filter_touch_deposit"
        case .transactionlog_filter_touch_receive:
            return "transactionlog_filter_touch_receive"
        case .transactionlog_filter_touch_send:
            return "transactionlog_filter_touch_send"
        case .transactionlog_filter_touch_payment:
            return "transactionlog_filter_touch_payment"
        case .transactionlog_filter_touch_redpacket:
            return "transactionlog_filter_touch_redpacket"
        case .transactionlog_filter_touch_withdraw:
            return "transactionlog_filter_touch_withdraw"
        case .transactionlog_filter_touch_success:
            return "transactionlog_filter_touch_success"
        case .transactionlog_filter_touch_fail:
            return "transactionlog_filter_touch_fail"
        case .transactionlog_filter_touch_cancel:
            return "transactionlog_filter_touch_cancel"
        case .transactionlog_filter_touch_apply:
            return "transactionlog_filter_touch_apply"
        case .promotion_launched:
            return "promotion_launched"
        case .promotion_touch_detail:
            return "promotion_touch_detail"
        case .promotion_detail_touch_back:
            return "promotion_detail_touch_back"
        case .promotion_detail_touch_share:
            return "promotion_detail_touch_share"
        case .promotion_detail_share_zalo:
            return "promotion_detail_share_zalo"
        case .promotion_detail_share_urlcopy:
            return "promotion_detail_share_urlcopy"
        case .promotion_detail_share_refresh:
            return "promotion_detail_share_refresh"
        case .promotion_detail_share_tobrowser:
            return "promotion_detail_share_tobrowser"
        case .promotion_detail_share_touch_close:
            return "promotion_detail_share_touch_close"
        case .voucher_detail_touch_back:
            return "voucher_detail_touch_back"
        case .voucher_popup_save:
            return "voucher_popup_save"
        case .voucher_popup_close:
            return "voucher_popup_close"
        case .cashback_popup_accept:
            return "cashback_popup_accept"
        case .me_touch_info:
            return "me_touch_info"
        case .me_touch_balance:
            return "me_touch_balance"
        case .me_touch_bank:
            return "me_touch_bank"
        case .me_touch_supportcenter:
            return "me_touch_supportcenter"
        case .me_touch_feedback:
            return "me_touch_feedback"
        case .me_touch_about:
            return "me_touch_about"
        case .me_about_touch_rate:
            return "me_about_touch_rate"
        case .me_about_touch_features:
            return "me_about_touch_features"
        case .me_about_touch_term:
            return "me_about_touch_term"
        case .me_touch_security:
            return "me_touch_security"
        case .me_security_touch_back:
            return "me_security_touch_back"
        case .me_security_touch_touchid:
            return "me_security_touch_touchid"
        case .me_security_touch_privacy:
            return "me_security_touch_privacy"
        case .me_security_touch_changepassword:
            return "me_security_touch_changepassword"
        case .me_security_changepassword_back:
            return "me_security_changepassword_back"
        case .me_security_changepassword_input:
            return "me_security_changepassword_input"
        case .me_security_changepassword_continue:
            return "me_security_changepassword_continue"
        case .me_security_changepassword_result:
            return "me_security_changepassword_result"
        case .me_security_touch_pass_open:
            return "me_security_touch_pass_open"
        case .me_security_touch_pass_time_set:
            return "me_security_touch_pass_time_set"
        case .me_security_touch_pass_time_now:
            return "me_security_touch_pass_time_now"
        case .me_security_touch_pass_time_30:
            return "me_security_touch_pass_time_30"
        case .me_security_touch_pass_time_60:
            return "me_security_touch_pass_time_60"
        case .me_security_touch_pass_time_180:
            return "me_security_touch_pass_time_180"
        case .me_security_touch_pass_time_300:
            return "me_security_touch_pass_time_300"
        case .me_security_touch_pass_time_close:
            return "me_security_touch_pass_time_close"
        case .me_security_touch_cover:
            return "me_security_touch_cover"
        case .me_security_touch_logout:
            return "me_security_touch_logout"
        case .me_touch_giftlist:
            return "me_touch_giftlist"
        case .me_giftlist_touch_detail:
            return "me_giftlist_touch_detail"
        case .Timing_GetOrder:
            return "Timing_GetOrder"
        case .Timing_ScanQR:
            return "Timing_ScanQR"
        case .api_um_createaccesstoken:
            return "api_um_createaccesstoken"
        case .api_um_removeaccesstoken:
            return "api_um_removeaccesstoken"
        case .api_um_verifycodetest:
            return "api_um_verifycodetest"
        case .api_um_updateprofile:
            return "api_um_updateprofile"
        case .api_um_verifyotpprofile:
            return "api_um_verifyotpprofile"
        case .api_um_recoverypin:
            return "api_um_recoverypin"
        case .api_um_getUserinfo:
            return "api_um_getUserinfo"
        case .api_um_getUserprofilelevel:
            return "api_um_getUserprofilelevel"
        case .api_um_getUserinfobyzalopayname:
            return "api_um_getUserinfobyzalopayname"
        case .api_um_getUserinfobyzalopayid:
            return "api_um_getUserinfobyzalopayid"
        case .api_um_checkzalopaynameexist:
            return "api_um_checkzalopaynameexist"
        case .api_um_updatezalopayname:
            return "api_um_updatezalopayname"
        case .api_um_validatepin:
            return "api_um_validatepin"
        case .api_um_sendnotification:
            return "api_um_sendnotification"
        case .api_um_checklistzaloidforclient:
            return "api_um_checklistzaloidforclient"
        case .api_ummerchant_getmerchantUserinfo:
            return "api_ummerchant_getmerchantUserinfo"
        case .api_ummerchant_getlistmerchantUserinfo:
            return "api_ummerchant_getlistmerchantUserinfo"
        case .api_umupload_preupdateprofilelevel3:
            return "api_umupload_preupdateprofilelevel3"
        case .api_um_listcardinfoforclient:
            return "api_um_listcardinfoforclient"
        case .api_um_listbankaccountforclient:
            return "api_um_listbankaccountforclient"
        case .api_um_loginviazalo:
            return "api_um_loginviazalo"
        case .api_um_registerphonenumber:
            return "api_um_registerphonenumber"
        case .api_um_authenphonenumber:
            return "api_um_authenphonenumber"
        case .api_zpc_getuserinfobyphone:
            return "api_zpc_getuserinfobyphone"
        case .api_um_listusersbyphoneforclient:
            return "api_um_listusersbyphoneforclient"
        case .api_um_updateuserinfo4client:
            return "api_um_updateuserinfo4client"
        case .api_um_getsessionloginforclient:
            return "api_um_getsessionloginforclient"
        case .api_um_removesessionloginforclient:
            return "api_um_removesessionloginforclient"
        case .api_um_getuserprofilesimpleinfo:
            return "api_um_getuserprofilesimpleinfo"
        case .connector_um_createaccesstoken:
            return "connector_um_createaccesstoken"
        case .connector_um_removeaccesstoken:
            return "connector_um_removeaccesstoken"
        case .connector_um_verifycodetest:
            return "connector_um_verifycodetest"
        case .connector_um_updateprofile:
            return "connector_um_updateprofile"
        case .connector_um_verifyotpprofile:
            return "connector_um_verifyotpprofile"
        case .connector_um_recoverypin:
            return "connector_um_recoverypin"
        case .connector_um_getUserinfo:
            return "connector_um_getUserinfo"
        case .connector_um_getUserprofilelevel:
            return "connector_um_getUserprofilelevel"
        case .connector_um_getUserinfobyzalopayname:
            return "connector_um_getUserinfobyzalopayname"
        case .connector_um_getUserinfobyzalopayid:
            return "connector_um_getUserinfobyzalopayid"
        case .connector_um_checkzalopaynameexist:
            return "connector_um_checkzalopaynameexist"
        case .connector_um_updatezalopayname:
            return "connector_um_updatezalopayname"
        case .connector_um_validatepin:
            return "connector_um_validatepin"
        case .connector_um_sendnotification:
            return "connector_um_sendnotification"
        case .connector_um_checklistzaloidforclient:
            return "connector_um_checklistzaloidforclient"
        case .connector_ummerchant_getmerchantUserinfo:
            return "connector_ummerchant_getmerchantUserinfo"
        case .connector_ummerchant_getlistmerchantUserinfo:
            return "connector_ummerchant_getlistmerchantUserinfo"
        case .connector_umupload_preupdateprofilelevel3:
            return "connector_umupload_preupdateprofilelevel3"
        case .connector_um_listcardinfoforclient:
            return "connector_um_listcardinfoforclient"
        case .connector_um_listbankaccountforclient:
            return "connector_um_listbankaccountforclient"
        case .connector_um_loginviazalo:
            return "connector_um_loginviazalo"
        case .connector_um_registerphonenumber:
            return "connector_um_registerphonenumber"
        case .connector_um_authenphonenumber:
            return "connector_um_authenphonenumber"
        case .connector_zpc_getuserinfobyphone:
            return "connector_zpc_getuserinfobyphone"
        case .connector_um_listusersbyphoneforclient:
            return "connector_um_listusersbyphoneforclient"
        case .connector_um_updateuserinfo4client:
            return "connector_um_updateuserinfo4client"
        case .connector_um_getsessionloginforclient:
            return "connector_um_getsessionloginforclient"
        case .connector_um_removesessionloginforclient:
            return "connector_um_removesessionloginforclient"
        case .connector_um_getuserprofilesimpleinfo:
            return "connector_um_getuserprofilesimpleinfo"
        case .api_v001_tpe_getbalance:
            return "api_v001_tpe_getbalance"
        case .api_v001_tpe_createwalletorder:
            return "api_v001_tpe_createwalletorder"
        case .api_v001_tpe_getinsideappresource:
            return "api_v001_tpe_getinsideappresource"
        case .api_v001_tpe_getorderinfo:
            return "api_v001_tpe_getorderinfo"
        case .api_v001_tpe_gettransstatus:
            return "api_v001_tpe_gettransstatus"
        case .api_v001_tpe_transhistory:
            return "api_v001_tpe_transhistory"
        case .api_v001_tpe_v001getplatforminfo:
            return "api_v001_tpe_v001getplatforminfo"
        case .api_v001_tpe_getappinfo:
            return "api_v001_tpe_getappinfo"
        case .api_v001_tpe_getbanklist:
            return "api_v001_tpe_getbanklist"
        case .api_v001_tpe_submittrans:
            return "api_v001_tpe_submittrans"
        case .api_v001_tpe_atmauthenpayer:
            return "api_v001_tpe_atmauthenpayer"
        case .api_v001_tpe_authcardholderformapping:
            return "api_v001_tpe_authcardholderformapping"
        case .api_v001_tpe_getstatusmapcard:
            return "api_v001_tpe_getstatusmapcard"
        case .api_v001_tpe_verifycardformapping:
            return "api_v001_tpe_verifycardformapping"
        case .api_v001_tpe_getstatusbyapptransidforclient:
            return "api_v001_tpe_getstatusbyapptransidforclient"
        case .api_v001_tpe_sdkwriteatmtime:
            return "api_v001_tpe_sdkwriteatmtime"
        case .api_v001_tpe_removemapcard:
            return "api_v001_tpe_removemapcard"
        case .api_v001_tpe_sdkerrorreport:
            return "api_v001_tpe_sdkerrorreport"
        case .api_v001_tpe_submitmapaccount:
            return "api_v001_tpe_submitmapaccount"
        case .api_v001_zp_upload_clientlogs:
            return "api_v001_zp_upload_clientlogs"
        case .api_v001_tpe_registeropandlink:
            return "api_v001_tpe_registeropandlink"
        case .api_v001_tpe_verifyaccountformapping:
            return "api_v001_tpe_verifyaccountformapping"
        case .api_v001_tpe_getstatusmapaccount:
            return "api_v001_tpe_getstatusmapaccount"
        case .api_v001_tpe_removemapaccount:
            return "api_v001_tpe_removemapaccount"
        case .api_v001_tpe_getbanklistgateway:
            return "api_v001_tpe_getbanklistgateway"
        case .connector_v001_tpe_getbalance:
            return "connector_v001_tpe_getbalance"
        case .connector_v001_tpe_createwalletorder:
            return "connector_v001_tpe_createwalletorder"
        case .connector_v001_tpe_getinsideappresource:
            return "connector_v001_tpe_getinsideappresource"
        case .connector_v001_tpe_getorderinfo:
            return "connector_v001_tpe_getorderinfo"
        case .connector_v001_tpe_gettransstatus:
            return "connector_v001_tpe_gettransstatus"
        case .connector_v001_tpe_transhistory:
            return "connector_v001_tpe_transhistory"
        case .connector_v001_tpe_v001getplatforminfo:
            return "connector_v001_tpe_v001getplatforminfo"
        case .connector_v001_tpe_getappinfo:
            return "connector_v001_tpe_getappinfo"
        case .connector_v001_tpe_getbanklist:
            return "connector_v001_tpe_getbanklist"
        case .connector_v001_tpe_submittrans:
            return "connector_v001_tpe_submittrans"
        case .connector_v001_tpe_atmauthenpayer:
            return "connector_v001_tpe_atmauthenpayer"
        case .connector_v001_tpe_authcardholderformapping:
            return "connector_v001_tpe_authcardholderformapping"
        case .connector_v001_tpe_getstatusmapcard:
            return "connector_v001_tpe_getstatusmapcard"
        case .connector_v001_tpe_verifycardformapping:
            return "connector_v001_tpe_verifycardformapping"
        case .connector_v001_tpe_getstatusbyapptransidforclient:
            return "connector_v001_tpe_getstatusbyapptransidforclient"
        case .connector_v001_tpe_sdkwriteatmtime:
            return "connector_v001_tpe_sdkwriteatmtime"
        case .connector_v001_tpe_removemapcard:
            return "connector_v001_tpe_removemapcard"
        case .connector_v001_tpe_sdkerrorreport:
            return "connector_v001_tpe_sdkerrorreport"
        case .connector_v001_tpe_submitmapaccount:
            return "connector_v001_tpe_submitmapaccount"
        case .connector_v001_tpe_registeropandlink:
            return "connector_v001_tpe_registeropandlink"
        case .connector_v001_tpe_verifyaccountformapping:
            return "connector_v001_tpe_verifyaccountformapping"
        case .connector_v001_tpe_getstatusmapaccount:
            return "connector_v001_tpe_getstatusmapaccount"
        case .connector_v001_tpe_removemapaccount:
            return "connector_v001_tpe_removemapaccount"
        case .connector_v001_tpe_getbanklistgateway:
            return "connector_v001_tpe_getbanklistgateway"
        case .api_v001_tpe_getlistpaymentcodes:
            return "api_v001_tpe_getlistpaymentcodes"
        case .api_v001_tpe_qrcodepayverifypin:
            return "api_v001_tpe_qrcodepayverifypin"
        case .api_um_listusersbyphoneforclientasync:
            return "api_um_listusersbyphoneforclientasync"
        case .api_um_getrequeststatusforclient:
            return "api_um_getrequeststatusforclient"
        case .connector_um_listusersbyphoneforclientasync:
            return "connector_um_listusersbyphoneforclientasync"
        case .connector_um_getrequeststatusforclient:
            return "connector_um_getrequeststatusforclient"
        case .api_filter_content:
            return "api_filter_content"
        case .api_redpackage_createbundleorder:
            return "api_redpackage_createbundleorder"
        case .api_redpackage_submittosendbundle:
            return "api_redpackage_submittosendbundle"
        case .api_redpackage_submitopenpackage:
            return "api_redpackage_submitopenpackage"
        case .api_redpackage_getsentbundlelist:
            return "api_redpackage_getsentbundlelist"
        case .api_redpackage_getrevpackagelist:
            return "api_redpackage_getrevpackagelist"
        case .api_redpackage_getpackagesinbundle:
            return "api_redpackage_getpackagesinbundle"
        case .api_redpackage_getappinfo:
            return "api_redpackage_getappinfo"
        case .api_redpackage_submittosendbundlebyzalopayinfo:
            return "api_redpackage_submittosendbundlebyzalopayinfo"
        case .api_redpackage_getlistpackagestatus:
            return "api_redpackage_getlistpackagestatus"
        case .connector_redpackage_createbundleorder:
            return "connector_redpackage_createbundleorder"
        case .connector_redpackage_submittosendbundle:
            return "connector_redpackage_submittosendbundle"
        case .connector_redpackage_submitopenpackage:
            return "connector_redpackage_submitopenpackage"
        case .connector_redpackage_getsentbundlelist:
            return "connector_redpackage_getsentbundlelist"
        case .connector_redpackage_getrevpackagelist:
            return "connector_redpackage_getrevpackagelist"
        case .connector_redpackage_getpackagesinbundle:
            return "connector_redpackage_getpackagesinbundle"
        case .connector_redpackage_getappinfo:
            return "connector_redpackage_getappinfo"
        case .connector_redpackage_submittosendbundlebyzalopayinfo:
            return "connector_redpackage_submittosendbundlebyzalopayinfo"
        case .connector_redpackage_getlistpackagestatus:
            return "connector_redpackage_getlistpackagestatus"
        case .api_notification_ping_pong:
            return "api_notification_ping_pong"
        case .api_get_voucher_status:
            return "api_get_voucher_status"
        case .api_use_voucher:
            return "api_use_voucher"
        case .api_revert_voucher:
            return "api_revert_voucher"
        case .connector_get_voucher_status:
            return "connector_get_voucher_status"
        case .connector_use_voucher:
            return "connector_use_voucher"
        case .connector_revert_voucher:
            return "connector_revert_voucher"
        case .api_get_voucher_history:
            return "api_get_voucher_history"
        case .connector_get_voucher_history:
            return "connector_get_voucher_history"
        case .api_getusevoucherstatus:
            return "api_getusevoucherstatus"
        case .api_getrevertvoucherstatus:
            return "api_getrevertvoucherstatus"
        case .api_topup_getpricelist:
            return "api_topup_getpricelist"
        case .api_topup_getrecentlist:
            return "api_topup_getrecentlist"
        case .api_topup_gettransbyzptransid:
            return "api_topup_gettransbyzptransid"
        case .api_topup_createorder:
            return "api_topup_createorder"
        case .api_topup_updateorder:
            return "api_topup_updateorder"
        case .balance_launched:
            return "balance_launched"
        case .balance_touch_back:
            return "balance_touch_back"
        case .balance_touch_addcash:
            return "balance_touch_addcash"
        case .balance_addcash_touch_back:
            return "balance_addcash_touch_back"
        case .balance_addcash_input:
            return "balance_addcash_input"
        case .balance_addcash_result:
            return "balance_addcash_result"
        case .balance_touch_withdraw:
            return "balance_touch_withdraw"
        case .balance_withdraw_touch_back:
            return "balance_withdraw_touch_back"
        case .balance_withdraw_touch_100:
            return "balance_withdraw_touch_100"
        case .balance_withdraw_touch_200:
            return "balance_withdraw_touch_200"
        case .balance_withdraw_touch_500:
            return "balance_withdraw_touch_500"
        case .balance_withdraw_touch_1000:
            return "balance_withdraw_touch_1000"
        case .balance_withdraw_touch_2000:
            return "balance_withdraw_touch_2000"
        case .balance_withdraw_touch_other:
            return "balance_withdraw_touch_other"
        case .balance_withdraw_result:
            return "balance_withdraw_result"
        case .linkbank_launched:
            return "linkbank_launched"
        case .linkbank_touch_back:
            return "linkbank_touch_back"
        case .linkbank_touch_add:
            return "linkbank_touch_add"
        case .linkbank_touch_add_icon:
            return "linkbank_touch_add_icon"
        case .linkbank_add_touch_back:
            return "linkbank_add_touch_back"
        case .linkbank_add_select_jcb:
            return "linkbank_add_select_jcb"
        case .linkbank_add_select_visa:
            return "linkbank_add_select_visa"
        case .linkbank_add_select_master:
            return "linkbank_add_select_master"
        case .linkbank_add_select_bank_code:
            return "linkbank_add_select_%@"
        case .linkbank_add_bank_touch_close:
            return "linkbank_add_bank_touch_close"
        case .linkbank_add_bank_close_touch_yes:
            return "linkbank_add_bank_close_touch_yes"
        case .linkbank_add_bank_close_touch_no:
            return "linkbank_add_bank_close_touch_no"
        case .linkbank_add_bank_cardnumber_input:
            return "linkbank_add_bank_cardnumber_input"
        case .linkbank_add_bank_cardnumber_forward:
            return "linkbank_add_bank_cardnumber_forward"
        case .linkbank_add_bank_date_back:
            return "linkbank_add_bank_date_back"
        case .linkbank_add_bank_date_forward:
            return "linkbank_add_bank_date_forward"
        case .linkbank_add_bank_name_back:
            return "linkbank_add_bank_name_back"
        case .linkbank_add_bank_name_forward:
            return "linkbank_add_bank_name_forward"
        case .linkbank_add_bank_OTP:
            return "linkbank_add_bank_OTP"
        case .linkbank_add_bank_OTP_continue:
            return "linkbank_add_bank_OTP_continue"
        case .linkbank_add_result:
            return "linkbank_add_result"
        case .linkbank_delete:
            return "linkbank_delete"
        case .linkbank_delete_result:
            return "linkbank_delete_result"
        case .pay_launched:
            return "pay_launched"
        case .pay_touch_back:
            return "pay_touch_back"
        case .pay_touch_scanqr:
            return "pay_touch_scanqr"
        case .pay_touch_nfc:
            return "pay_touch_nfc"
        case .pay_touch_bluetooth:
            return "pay_touch_bluetooth"
        case .pay_scanqr_touch_photo:
            return "pay_scanqr_touch_photo"
        case .pay_scanqr_result_fail:
            return "pay_scanqr_result_fail"
        case .pay_scanqr_result_fail_close:
            return "pay_scanqr_result_fail_close"
        case .pay_scanqr_result_sucess_moneytransfer:
            return "pay_scanqr_result_sucess_moneytransfer"
        case .pay_scanqr_result_sucess_payment:
            return "pay_scanqr_result_sucess_payment"
        case .pay_nfc_result_fail:
            return "pay_nfc_result_fail"
        case .pay_nfc_result_success:
            return "pay_nfc_result_success"
        case .moneytransfer_launched:
            return "moneytransfer_launched"
        case .moneytransfer_touch_back:
            return "moneytransfer_touch_back"
        case .moneytransfer_touch_phonebook:
            return "moneytransfer_touch_phonebook"
        case .moneytransfer_touch_phonenumber:
            return "moneytransfer_touch_phonenumber"
        case .moneytransfer_touch_zpid:
            return "moneytransfer_touch_zpid"
        case .moneytransfer_touch_recent1:
            return "moneytransfer_touch_recent1"
        case .moneytransfer_touch_recent2:
            return "moneytransfer_touch_recent2"
        case .moneytransfer_touch_recent3:
            return "moneytransfer_touch_recent3"
        case .moneytransfer_touch_recent4:
            return "moneytransfer_touch_recent4"
        case .moneytransfer_touch_recent5:
            return "moneytransfer_touch_recent5"
        case .moneytransfer_touch_recent6:
            return "moneytransfer_touch_recent6"
        case .moneytransfer_touch_recent7:
            return "moneytransfer_touch_recent7"
        case .moneytransfer_touch_recent8:
            return "moneytransfer_touch_recent8"
        case .moneytransfer_touch_recent9:
            return "moneytransfer_touch_recent9"
        case .moneytransfer_touch_recent10:
            return "moneytransfer_touch_recent10"
        case .moneytransfer_touch_recent10plus:
            return "moneytransfer_touch_recent10plus"
        case .moneytransfer_zpid_touch_back:
            return "moneytransfer_zpid_touch_back"
        case .moneytransfer_zpdi_touch_continue:
            return "moneytransfer_zpdi_touch_continue"
        case .moneytransfer_input_launched:
            return "moneytransfer_input_launched"
        case .moneytransfer_input_touch_back:
            return "moneytransfer_input_touch_back"
        case .moneytransfer_input_amount:
            return "moneytransfer_input_amount"
        case .moneytransfer_input_amount_touch_clear:
            return "moneytransfer_input_amount_touch_clear"
        case .moneytransfer_input_message:
            return "moneytransfer_input_message"
        case .moneytransfer_input_message_touch_clear:
            return "moneytransfer_input_message_touch_clear"
        case .moneytransfer_input_touch_continue:
            return "moneytransfer_input_touch_continue"
        case .moneyreceive_launched:
            return "moneyreceive_launched"
        case .moneyreceive_touch_back:
            return "moneyreceive_touch_back"
        case .moneyreceive_result:
            return "moneyreceive_result"
        case .moneyreceive_touch_input:
            return "moneyreceive_touch_input"
        case .moneyreceive_input_touch_back:
            return "moneyreceive_input_touch_back"
        case .moneyreceive_input_amount:
            return "moneyreceive_input_amount"
        case .moneyreceive_input_amount_touch_clear:
            return "moneyreceive_input_amount_touch_clear"
        case .moneyreceive_input_message:
            return "moneyreceive_input_message"
        case .moneyreceive_input_message_touch_clear:
            return "moneyreceive_input_message_touch_clear"
        case .moneyreceive_input_touch_confirm:
            return "moneyreceive_input_touch_confirm"
        case .moneyreceive_touch_clear:
            return "moneyreceive_touch_clear"
        case .quickpay_launched:
            return "quickpay_launched"
        case .quickpay_touch_back:
            return "quickpay_touch_back"
        case .quickpay_touch_cont:
            return "quickpay_touch_cont"
        case .quickpay_touch_addcash:
            return "quickpay_touch_addcash"
        case .quickpay_touch_withdraw:
            return "quickpay_touch_withdraw"
        case .quickpay_codeview:
            return "quickpay_codeview"
        case .quickpay_codeview_touch_back:
            return "quickpay_codeview_touch_back"
        case .quickpay_codeview_touch_barcode:
            return "quickpay_codeview_touch_barcode"
        case .quickpay_codeview_touch_qrcode:
            return "quickpay_codeview_touch_qrcode"
        case .quickpay_codeview_touch_customize:
            return "quickpay_codeview_touch_customize"
        case .quickpay_customize_touch_setupinfor:
            return "quickpay_customize_touch_setupinfor"
        case .quickpay_customize_touch_changecode:
            return "quickpay_customize_touch_changecode"
        case .quickpay_customize_touch_instruction:
            return "quickpay_customize_touch_instruction"
        case .quickpay_result_success:
            return "quickpay_result_success"
        case .quickpay_result_fail:
            return "quickpay_result_fail"
        case .redpacket_launched:
            return "redpacket_launched"
        case .redpacket_home_touch_start:
            return "redpacket_home_touch_start"
        case .redpacket_home_touch_back:
            return "redpacket_home_touch_back"
        case .redpacket_home_touch_sent:
            return "redpacket_home_touch_sent"
        case .redpacket_home_touch_received:
            return "redpacket_home_touch_received"
        case .redpacket_lixi_touch_back:
            return "redpacket_lixi_touch_back"
        case .redpacket_lixi_back_stay:
            return "redpacket_lixi_back_stay"
        case .redpacket_lixi_back_close:
            return "redpacket_lixi_back_close"
        case .redpacket_lixi_touch_theme:
            return "redpacket_lixi_touch_theme"
        case .redpacket_lixi_touch_complete:
            return "redpacket_lixi_touch_complete"
        case .redpacket_lixi_touch_phonebook:
            return "redpacket_lixi_touch_phonebook"
        case .redpacket_lixi_touch_amount:
            return "redpacket_lixi_touch_amount"
        case .redpacket_theme_touch_back:
            return "redpacket_theme_touch_back"
        case .redpacket_theme_touch_image:
            return "redpacket_theme_touch_image"
        case .redpacket_theme_touch_button:
            return "redpacket_theme_touch_button"
        case .redpacket_theme_wish_expand:
            return "redpacket_theme_wish_expand"
        case .redpacket_theme_wish_hide:
            return "redpacket_theme_wish_hide"
        case .redpacket_theme_wish_edit:
            return "redpacket_theme_wish_edit"
        case .redpacket_theme_preview:
            return "redpacket_theme_preview"
        case .redpacket_theme_done:
            return "redpacket_theme_done"
        case .redpacket_preview_back:
            return "redpacket_preview_back"
        case .redpacket_preview_done:
            return "redpacket_preview_done"
        case .redpacket_pay_success:
            return "redpacket_pay_success"
        case .redpacket_success_touch_home:
            return "redpacket_success_touch_home"
        case .redpacket_success_touch_again:
            return "redpacket_success_touch_again"
        case .redpacket_pay_fail:
            return "redpacket_pay_fail"
        case .redpacket_receive_open:
            return "redpacket_receive_open"
        case .redpacket_receive_close:
            return "redpacket_receive_close"
        case .redpacket_details_equal_back:
            return "redpacket_details_equal_back"
        case .redpacket_details_equal_play_again:
            return "redpacket_details_equal_play_again"
        case .redpacket_details_equal_share:
            return "redpacket_details_equal_share"
        case .redpacket_details_random_back:
            return "redpacket_details_random_back"
        case .redpacket_details_random_ranking:
            return "redpacket_details_random_ranking"
        case .redpacket_details_random_share:
            return "redpacket_details_random_share"
        case .redpacket_ranking_play_again:
            return "redpacket_ranking_play_again"
        case .redpacket_ranking_share:
            return "redpacket_ranking_share"
        case .redpacket_share_facebook:
            return "redpacket_share_facebook"
        case .redpacket_share_fb_messenger:
            return "redpacket_share_fb_messenger"
        case .redpacket_share_messages:
            return "redpacket_share_messages"
        case .redpacket_share_others:
            return "redpacket_share_others"
        case .redpacket_sent_touch_details:
            return "redpacket_sent_touch_details"
        case .redpacket_received_touch_details:
            return "redpacket_received_touch_details"
        case .redpacket_theme_swipe_1:
            return "redpacket_theme_swipe_1"
        case .redpacket_theme_swipe_2:
            return "redpacket_theme_swipe_2"
        case .redpacket_theme_swipe_3:
            return "redpacket_theme_swipe_3"
        case .redpacket_theme_swipe_4:
            return "redpacket_theme_swipe_4"
        case .redpacket_theme_swipe_5:
            return "redpacket_theme_swipe_5"
        case .redpacket_theme_swipe_6:
            return "redpacket_theme_swipe_6"
        case .redpacket_theme_swipe_7:
            return "redpacket_theme_swipe_7"
        case .redpacket_theme_swipe_8:
            return "redpacket_theme_swipe_8"
        case .redpacket_theme_swipe_9:
            return "redpacket_theme_swipe_9"
        case .redpacket_theme_swipe_10:
            return "redpacket_theme_swipe_10"
        case .redpacket_theme_swipe_11:
            return "redpacket_theme_swipe_11"
        case .redpacket_theme_swipe_12:
            return "redpacket_theme_swipe_12"
        case .redpacket_theme_swipe_13:
            return "redpacket_theme_swipe_13"
        case .redpacket_theme_swipe_14:
            return "redpacket_theme_swipe_14"
        case .redpacket_theme_swipe_15:
            return "redpacket_theme_swipe_15"
        case .redpacket_theme_swipe_16:
            return "redpacket_theme_swipe_16"
        case .redpacket_theme_swipe_17:
            return "redpacket_theme_swipe_17"
        case .redpacket_theme_swipe_18:
            return "redpacket_theme_swipe_18"
        case .redpacket_theme_swipe_19:
            return "redpacket_theme_swipe_19"
        case .redpacket_theme_swipe_20:
            return "redpacket_theme_swipe_20"
        case .redpacket_theme_swipe_21:
            return "redpacket_theme_swipe_21"
        case .topup_launched:
            return "topup_launched"
        case .topup_touch_back:
            return "topup_touch_back"
        case .topup_touch_phonebook:
            return "topup_touch_phonebook"
        case .topup_input_phonenumber:
            return "topup_input_phonenumber"
        case .topup_touch_buy_prepaid_card:
            return "topup_touch_buy_prepaid_card"
        case .topup_touch_topup_for_other_phonenumber:
            return "topup_touch_topup_for_other_phonenumber"
        case .topup_touch_item_mobilecard:
            return "topup_touch_item_mobilecard"
        case .topup_touch_item_electricity:
            return "topup_touch_item_electricity"
        case .topup_touch_item_water:
            return "topup_touch_item_water"
        case .topup_touch_item_internet:
            return "topup_touch_item_internet"
        case .topup_touch_recent:
            return "topup_touch_recent"
        case .topup_recent_touch_close:
            return "topup_recent_touch_close"
        case .topup_recent_touch_item:
            return "topup_recent_touch_item"
        case .phonecard_launched:
            return "phonecard_launched"
        case .phonecard_touch_back:
            return "phonecard_touch_back"
        case .phonecard_touch_history:
            return "phonecard_touch_history"
        case .phonecard_touch_telco:
            return "phonecard_touch_telco"
        case .phonecard_touch_value:
            return "phonecard_touch_value"
        case .phonecard_touch_quantity_minus:
            return "phonecard_touch_quantity_minus"
        case .phonecard_touch_quantity_plus:
            return "phonecard_touch_quantity_plus"
        case .phonecard_touch_confirm:
            return "phonecard_touch_confirm"
        case .phonecard_success_transaction_touch_close:
            return "phonecard_success_transaction_touch_close"
        case .phonecard_transaction_detail_touch_copy:
            return "phonecard_transaction_detail_touch_copy"
        case .phonecard_transaction_detail_touch_call:
            return "phonecard_transaction_detail_touch_call"
        case .phonecard_transaction_detail_touch_code:
            return "phonecard_transaction_detail_touch_code"
        case .phonecard_transaction_detail_copy_touch_syntax:
            return "phonecard_transaction_detail_copy_touch_syntax"
        case .phonecard_transaction_detail_copy_touch_code:
            return "phonecard_transaction_detail_copy_touch_code"
        case .phonecard_transaction_detail_copy_touch_series:
            return "phonecard_transaction_detail_copy_touch_series"
        case .phonecard_transaction_detail_copy_touch_close:
            return "phonecard_transaction_detail_copy_touch_close"
        case .phonecard_history_touch_copy:
            return "phonecard_history_touch_copy"
        case .phonecard_history_touch_call:
            return "phonecard_history_touch_call"
        case .phonecard_history_touch_code:
            return "phonecard_history_touch_code"
        case .phonecard_history_copy_touch_syntax:
            return "phonecard_history_copy_touch_syntax"
        case .phonecard_history_copy_touch_code:
            return "phonecard_history_copy_touch_code"
        case .phonecard_history_copy_touch_series:
            return "phonecard_history_copy_touch_series"
        case .phonecard_history_copy_touch_close:
            return "phonecard_history_copy_touch_close"
        case .electric_launched:
            return "electric_launched"
        case .electric_touch_back:
            return "electric_touch_back"
        case .electric_select_provider:
            return "electric_select_provider"
        case .electric_touch_check:
            return "electric_touch_check"
        case .electric_touch_add_bill:
            return "electric_touch_add_bill"
        case .electric_provider_touch_back:
            return "electric_provider_touch_back"
        case .electric_provider_input:
            return "electric_provider_input"
        case .electric_input_touch_provider:
            return "electric_input_touch_provider"
        case .electric_touch_continue:
            return "electric_touch_continue"
        case .electric_touch_report:
            return "electric_touch_report"
        case .electric_report_swipe:
            return "electric_report_swipe"
        case .water_launched:
            return "water_launched"
        case .water_touch_back:
            return "water_touch_back"
        case .water_select_provider:
            return "water_select_provider"
        case .water_touch_check:
            return "water_touch_check"
        case .water_touch_add_bill:
            return "water_touch_add_bill"
        case .water_provider_touch_back:
            return "water_provider_touch_back"
        case .water_provider_input:
            return "water_provider_input"
        case .water_input_touch_provider:
            return "water_input_touch_provider"
        case .water_touch_continue:
            return "water_touch_continue"
        case .internet_launched:
            return "internet_launched"
        case .internet_touch_back:
            return "internet_touch_back"
        case .internet_select_provider:
            return "internet_select_provider"
        case .internet_touch_check:
            return "internet_touch_check"
        case .internet_touch_add_bill:
            return "internet_touch_add_bill"
        case .internet_provider_touch_back:
            return "internet_provider_touch_back"
        case .internet_provider_input:
            return "internet_provider_input"
        case .internet_input_touch_provider:
            return "internet_input_touch_provider"
        case .internet_touch_continue:
            return "internet_touch_continue"
        case .tv_launched:
            return "tv_launched"
        case .tv_touch_back:
            return "tv_touch_back"
        case .tv_select_provider:
            return "tv_select_provider"
        case .tv_touch_check:
            return "tv_touch_check"
        case .tv_touch_add_bill:
            return "tv_touch_add_bill"
        case .tv_provider_touch_back:
            return "tv_provider_touch_back"
        case .tv_provider_input:
            return "tv_provider_input"
        case .tv_input_touch_provider:
            return "tv_input_touch_provider"
        case .tv_touch_continue:
            return "tv_touch_continue"
        case .passopen_input_touch:
            return "passopen_input_touch"
        case .passopen_input_success:
            return "passopen_input_success"
        case .passopen_input_fail:
            return "passopen_input_fail"
        case .passopen_forget_touch:
            return "passopen_forget_touch"
        case .passopen_forget_reset:
            return "passopen_forget_reset"
        case .passopen_forget_close:
            return "passopen_forget_close"
        case .keyboard_touch_0:
            return "keyboard_touch_0"
        case .keyboard_touch_000:
            return "keyboard_touch_000"
        case .keyboard_touch_plus:
            return "keyboard_touch_plus"
        case .keyboard_touch_minus:
            return "keyboard_touch_minus"
        case .keyboard_touch_multiplication:
            return "keyboard_touch_multiplication"
        case .keyboard_touch_divide:
            return "keyboard_touch_divide"
        case .keyboard_touch_delete:
            return "keyboard_touch_delete"
        case .popupsdk_launched:
            return "popupsdk_launched"
        case .popupsdk_source:
            return "popupsdk_source"
        case .popupsdk_voucher:
            return "popupsdk_voucher"
        case .popupsdk_pay:
            return "popupsdk_pay"
        case .popupsdk_close:
            return "popupsdk_close"
        case .popupsdk_out_main:
            return "popupsdk_out_main"
        case .popupsdk_source_back:
            return "popupsdk_source_back"
        case .popupsdk_voucher_back:
            return "popupsdk_voucher_back"
        case .popupsdk_add:
            return "popupsdk_add"
        case .popupsdk_password_input:
            return "popupsdk_password_input"
        case .popupsdk_password_fail:
            return "popupsdk_password_fail"
        case .popupsdk_password_back:
            return "popupsdk_password_back"
        case .popupsdk_otp_input:
            return "popupsdk_otp_input"
        case .popupsdk_otp_fail:
            return "popupsdk_otp_fail"
        case .popupsdk_otp_resend:
            return "popupsdk_otp_resend"
        case .popupsdk_otp_close:
            return "popupsdk_otp_close"
        case .popupsdk_auth_confirm:
            return "popupsdk_auth_confirm"
        case .popupsdk_auth_cancel:
            return "popupsdk_auth_cancel"
        case .popupsdk_success:
            return "popupsdk_success"
        case .popupsdk_fail:
            return "popupsdk_fail"
        case .popupsdk_voucher_non:
            return "popupsdk_voucher_non"
        case .sdk_open:
            return "sdk_open"
        case .sdk_zalopay:
            return "sdk_zalopay"
        case .sdk_changesource:
            return "sdk_changesource"
        case .sdk_add:
            return "sdk_add"
        case .sdk_voucher:
            return "sdk_voucher"
        case .sdk_pay:
            return "sdk_pay"
        case .sdk_password_input:
            return "sdk_password_input"
        case .sdk_password_fail:
            return "sdk_password_fail"
        case .sdk_password_back:
            return "sdk_password_back"
        case .sdk_otp_input:
            return "sdk_otp_input"
        case .sdk_otp_fail:
            return "sdk_otp_fail"
        case .sdk_otp_resend:
            return "sdk_otp_resend"
        case .sdk_otp_close:
            return "sdk_otp_close"
        case .sdk_auth_confirm:
            return "sdk_auth_confirm"
        case .sdk_success:
            return "sdk_success"
        case .sdk_fail:
            return "sdk_fail"
        case .sdk_voucher_non:
            return "sdk_voucher_non"
        case .sdk_cancel:
            return "sdk_cancel"
        case .contact_launched:
            return "contact_launched"
        case .contact_setting_update:
            return "contact_setting_update"
        case .contact_touch_favorite:
            return "contact_touch_favorite"
        case .contact_touch_unfavorite:
            return "contact_touch_unfavorite"
        case .contact_touch_back:
            return "contact_touch_back"
        case .contact_search_input:
            return "contact_search_input"
        case .contact_touch_zalopay:
            return "contact_touch_zalopay"
        case .contact_touch_all:
            return "contact_touch_all"
        case .contact_touch_refresh_top:
            return "contact_touch_refresh_top"
        case .contact_touch_refresh_bot:
            return "contact_touch_refresh_bot"
        case .contact_touch_pull_to_refresh:
            return "contact_touch_pull_to_refresh"
        case .contact_search_phonenumber:
            return "contact_search_phonenumber"
        case .contact_search_name:
            return "contact_search_name"
        case .contact_switch_keyboard_to_123:
            return "contact_switch_keyboard_to_123"
        case .contact_switch_keyboard_to_abc:
            return "contact_switch_keyboard_to_abc"
        case .contact_touch_friend_favorite:
            return "contact_touch_friend_favorite"
        case .contact_touch_friend_nonfavorite:
            return "contact_touch_friend_nonfavorite"
        case .contact_touch_nonzpfriend:
            return "contact_touch_nonzpfriend"
        case .contact_nonzpfriend_touch_close:
            return "contact_nonzpfriend_touch_close"
        case .contact_nonzpfriend_touch_invite:
            return "contact_nonzpfriend_touch_invite"
        case .contact_nonzpfriend_invite_touch_zalo:
            return "contact_nonzpfriend_invite_touch_zalo"
        case .contact_read_contact_deny:
            return "contact_read_contact_deny"
        case .contact_launch_from_transfer:
            return "contact_launch_from_transfer"
        case .contact_launch_from_topup:
            return "contact_launch_from_topup"
        case .contact_launch_from_lixi:
            return "contact_launch_from_lixi"
        case .contact_touch_alphabet:
            return "contact_touch_alphabet"
        case .contact_touch_zpid:
            return "contact_touch_zpid"
        case .touch3d_qr:
            return "touch3d_qr"
        case .touch3d_transactionlogs:
            return "touch3d_transactionlogs"
        }
    }

    public static func eventCategoryFromId(_ eventId: ZPAnalyticEventAction) -> String {
        switch (eventId) {
        case .app_launch:
            return "Startup"
        case .onboarding_input_payment_passcode:
            return "Onboarding"
        case .onboarding_confirm_payment_passcode_success:
            return "Onboarding"
        case .onboarding_confirm_payment_passcode_fail:
            return "Onboarding"
        case .onboarding_confirm_payment_passcode_back:
            return "Onboarding"
        case .onboarding_input_phone_number_invalid:
            return "Onboarding"
        case .onboarding_input_phone_number_duplicate:
            return "Onboarding"
        case .onboarding_submit_phone_number:
            return "Onboarding"
        case .onboarding_input_otp_success:
            return "Onboarding"
        case .onboarding_input_otp_fail:
            return "Onboarding"
        case .onboarding_resend_otp:
            return "Onboarding"
        case .onboarding_submit_otp:
            return "Onboarding"
        case .onboardingkyc_view_inform:
            return "Onboarding KYC"
        case .onboardingkyc_input_phone_number_invalid:
            return "Onboarding KYC"
        case .onboardingkyc_input_phone_number_duplicate:
            return "Onboarding KYC"
        case .onboardingkyc_input_real_name_invalid:
            return "Onboarding KYC"
        case .onboardingkyc_input_personal_id_invalid:
            return "Onboarding KYC"
        case .onboardingkyc_input_citizen_id_invalid:
            return "Onboarding KYC"
        case .onboardingkyc_input_passport_invalid:
            return "Onboarding KYC"
        case .onboardingkyc_input_dob_invalid:
            return "Onboarding KYC"
        case .onboardingkyc_view_term_and_condition:
            return "Onboarding KYC"
        case .onboardingkyc_submit_user_info:
            return "Onboarding KYC"
        case .onboardingkyc_input_otp_success:
            return "Onboarding KYC"
        case .onboardingkyc_input_otp_fail:
            return "Onboarding KYC"
        case .onboardingkyc_resend_otp:
            return "Onboarding KYC"
        case .onboardingkyc_submit_otp:
            return "Onboarding KYC"
        case .onboardingkyc_input_payment_passcode:
            return "Onboarding KYC"
        case .onboardingkyc_confirm_payment_passcode_success:
            return "Onboarding KYC"
        case .onboardingkyc_confirm_payment_passcode_fail:
            return "Onboarding KYC"
        case .onboardingkyc_confirm_payment_passcode_back:
            return "Onboarding KYC"
        case .onboardingkyc_submit_payment_passcode:
            return "Onboarding KYC"
        case .login_touch:
            return "Login"
        case .login_success:
            return "Login"
        case .login_fail:
            return "Login"
        case .login_touch_register:
            return "Login"
        case .home_launched:
            return "Home"
        case .home_touch_pay:
            return "Home"
        case .home_touch_balance:
            return "Home"
        case .home_touch_search:
            return "Home"
        case .home_touch_notification:
            return "Home"
        case .home_touch_notification_new:
            return "Home"
        case .home_touch_home:
            return "Home"
        case .home_touch_transactionlog:
            return "Home"
        case .home_touch_promotion:
            return "Home"
        case .home_touch_promotion_new:
            return "Home"
        case .home_touch_profile:
            return "Home"
        case .home_touch_bank:
            return "Home"
        case .home_touch_moneytransfer:
            return "Home"
        case .home_touch_moneyreceive:
            return "Home"
        case .home_touch_service:
            return "Home"
        case .home_touch_id_app:
            return "Home"
        case .search_touch_result:
            return "Search"
        case .search_touch_suggest:
            return "Search"
        case .search_touch_back:
            return "Search"
        case .notification_launched:
            return "Notification"
        case .notification_touch_transactionitem:
            return "Notification"
        case .transactionlog_touch_back:
            return "Transaction Log"
        case .transactionlog_alltrans_launched:
            return "Transaction Log"
        case .transactionlog_alltrans_touch_back:
            return "Transaction Log"
        case .transactionlog_sendtransfer_launched:
            return "Transaction Log"
        case .transactionlog_sendtransfer_touch_back:
            return "Transaction Log"
        case .transactionlog_sendtransfer_touch_sendmore:
            return "Transaction Log"
        case .transactionlog_receivetransfer_launched:
            return "Transaction Log"
        case .transactionlog_receivetransfer_touch_back:
            return "Transaction Log"
        case .transactionlog_receivetransfer_touch_sendback:
            return "Transaction Log"
        case .transactionlog_receivetransfer_touch_thanks:
            return "Transaction Log"
        case .transactionlog_touch_filter:
            return "Transaction Log"
        case .transactionlog_filter_touch_time:
            return "Transaction Log"
        case .transactionlog_filter_time_touch_close:
            return "Transaction Log"
        case .transactionlog_filter_time_touch_apply:
            return "Transaction Log"
        case .transactionlog_filter_touch_deposit:
            return "Transaction Log"
        case .transactionlog_filter_touch_receive:
            return "Transaction Log"
        case .transactionlog_filter_touch_send:
            return "Transaction Log"
        case .transactionlog_filter_touch_payment:
            return "Transaction Log"
        case .transactionlog_filter_touch_redpacket:
            return "Transaction Log"
        case .transactionlog_filter_touch_withdraw:
            return "Transaction Log"
        case .transactionlog_filter_touch_success:
            return "Transaction Log"
        case .transactionlog_filter_touch_fail:
            return "Transaction Log"
        case .transactionlog_filter_touch_cancel:
            return "Transaction Log"
        case .transactionlog_filter_touch_apply:
            return "Transaction Log"
        case .promotion_launched:
            return "Promotion"
        case .promotion_touch_detail:
            return "Promotion"
        case .promotion_detail_touch_back:
            return "Promotion"
        case .promotion_detail_touch_share:
            return "Promotion"
        case .promotion_detail_share_zalo:
            return "Promotion"
        case .promotion_detail_share_urlcopy:
            return "Promotion"
        case .promotion_detail_share_refresh:
            return "Promotion"
        case .promotion_detail_share_tobrowser:
            return "Promotion"
        case .promotion_detail_share_touch_close:
            return "Promotion"
        case .voucher_detail_touch_back:
            return "Promotion"
        case .voucher_popup_save:
            return "Promotion"
        case .voucher_popup_close:
            return "Promotion"
        case .cashback_popup_accept:
            return "Promotion"
        case .me_touch_info:
            return "Me_Info"
        case .me_touch_balance:
            return "Me"
        case .me_touch_bank:
            return "Me"
        case .me_touch_supportcenter:
            return "Me"
        case .me_touch_feedback:
            return "Me"
        case .me_touch_about:
            return "Me"
        case .me_about_touch_rate:
            return "Me"
        case .me_about_touch_features:
            return "Me"
        case .me_about_touch_term:
            return "Me"
        case .me_touch_security:
            return "Me_Security"
        case .me_security_touch_back:
            return "Me_Security"
        case .me_security_touch_touchid:
            return "Me_Security"
        case .me_security_touch_privacy:
            return "Me_Security"
        case .me_security_touch_changepassword:
            return "Me_Security"
        case .me_security_changepassword_back:
            return "Me_Security"
        case .me_security_changepassword_input:
            return "Me_Security"
        case .me_security_changepassword_continue:
            return "Me_Security"
        case .me_security_changepassword_result:
            return "Me_Security"
        case .me_security_touch_pass_open:
            return "Me_Security"
        case .me_security_touch_pass_time_set:
            return "Me_Security"
        case .me_security_touch_pass_time_now:
            return "Me_Security"
        case .me_security_touch_pass_time_30:
            return "Me_Security"
        case .me_security_touch_pass_time_60:
            return "Me_Security"
        case .me_security_touch_pass_time_180:
            return "Me_Security"
        case .me_security_touch_pass_time_300:
            return "Me_Security"
        case .me_security_touch_pass_time_close:
            return "Me_Security"
        case .me_security_touch_cover:
            return "Me_Security"
        case .me_security_touch_logout:
            return "Me_Security"
        case .me_touch_giftlist:
            return "Me_Gift"
        case .me_giftlist_touch_detail:
            return "Me_Gift"
        case .Timing_GetOrder:
            return "Timing Get Order"
        case .Timing_ScanQR:
            return "Timing Scan QR"
        case .api_um_createaccesstoken:
            return "Timing UM"
        case .api_um_removeaccesstoken:
            return "Timing UM"
        case .api_um_verifycodetest:
            return "Timing UM"
        case .api_um_updateprofile:
            return "Timing UM"
        case .api_um_verifyotpprofile:
            return "Timing UM"
        case .api_um_recoverypin:
            return "Timing UM"
        case .api_um_getUserinfo:
            return "Timing UM"
        case .api_um_getUserprofilelevel:
            return "Timing UM"
        case .api_um_getUserinfobyzalopayname:
            return "Timing UM"
        case .api_um_getUserinfobyzalopayid:
            return "Timing UM"
        case .api_um_checkzalopaynameexist:
            return "Timing UM"
        case .api_um_updatezalopayname:
            return "Timing UM"
        case .api_um_validatepin:
            return "Timing UM"
        case .api_um_sendnotification:
            return "Timing UM"
        case .api_um_checklistzaloidforclient:
            return "Timing UM"
        case .api_ummerchant_getmerchantUserinfo:
            return "Timing UM"
        case .api_ummerchant_getlistmerchantUserinfo:
            return "Timing UM"
        case .api_umupload_preupdateprofilelevel3:
            return "Timing UM"
        case .api_um_listcardinfoforclient:
            return "Timing UM"
        case .api_um_listbankaccountforclient:
            return "Timing UM"
        case .api_um_loginviazalo:
            return "Timing UM"
        case .api_um_registerphonenumber:
            return "Timing UM"
        case .api_um_authenphonenumber:
            return "Timing UM"
        case .api_zpc_getuserinfobyphone:
            return "Timing UM"
        case .api_um_listusersbyphoneforclient:
            return "Timing UM"
        case .api_um_updateuserinfo4client:
            return "Timing UM"
        case .api_um_getsessionloginforclient:
            return "Timing UM"
        case .api_um_removesessionloginforclient:
            return "Timing UM"
        case .api_um_getuserprofilesimpleinfo:
            return "Timing UM"
        case .connector_um_createaccesstoken:
            return "Timing UM"
        case .connector_um_removeaccesstoken:
            return "Timing UM"
        case .connector_um_verifycodetest:
            return "Timing UM"
        case .connector_um_updateprofile:
            return "Timing UM"
        case .connector_um_verifyotpprofile:
            return "Timing UM"
        case .connector_um_recoverypin:
            return "Timing UM"
        case .connector_um_getUserinfo:
            return "Timing UM"
        case .connector_um_getUserprofilelevel:
            return "Timing UM"
        case .connector_um_getUserinfobyzalopayname:
            return "Timing UM"
        case .connector_um_getUserinfobyzalopayid:
            return "Timing UM"
        case .connector_um_checkzalopaynameexist:
            return "Timing UM"
        case .connector_um_updatezalopayname:
            return "Timing UM"
        case .connector_um_validatepin:
            return "Timing UM"
        case .connector_um_sendnotification:
            return "Timing UM"
        case .connector_um_checklistzaloidforclient:
            return "Timing UM"
        case .connector_ummerchant_getmerchantUserinfo:
            return "Timing UM"
        case .connector_ummerchant_getlistmerchantUserinfo:
            return "Timing UM"
        case .connector_umupload_preupdateprofilelevel3:
            return "Timing UM"
        case .connector_um_listcardinfoforclient:
            return "Timing UM"
        case .connector_um_listbankaccountforclient:
            return "Timing UM"
        case .connector_um_loginviazalo:
            return "Timing UM"
        case .connector_um_registerphonenumber:
            return "Timing UM"
        case .connector_um_authenphonenumber:
            return "Timing UM"
        case .connector_zpc_getuserinfobyphone:
            return "Timing UM"
        case .connector_um_listusersbyphoneforclient:
            return "Timing UM"
        case .connector_um_updateuserinfo4client:
            return "Timing UM"
        case .connector_um_getsessionloginforclient:
            return "Timing UM"
        case .connector_um_removesessionloginforclient:
            return "Timing UM"
        case .connector_um_getuserprofilesimpleinfo:
            return "Timing UM"
        case .api_v001_tpe_getbalance:
            return "Timing TPE"
        case .api_v001_tpe_createwalletorder:
            return "Timing TPE"
        case .api_v001_tpe_getinsideappresource:
            return "Timing TPE"
        case .api_v001_tpe_getorderinfo:
            return "Timing TPE"
        case .api_v001_tpe_gettransstatus:
            return "Timing TPE"
        case .api_v001_tpe_transhistory:
            return "Timing TPE"
        case .api_v001_tpe_v001getplatforminfo:
            return "Timing TPE"
        case .api_v001_tpe_getappinfo:
            return "Timing TPE"
        case .api_v001_tpe_getbanklist:
            return "Timing TPE"
        case .api_v001_tpe_submittrans:
            return "Timing TPE"
        case .api_v001_tpe_atmauthenpayer:
            return "Timing TPE"
        case .api_v001_tpe_authcardholderformapping:
            return "Timing TPE"
        case .api_v001_tpe_getstatusmapcard:
            return "Timing TPE"
        case .api_v001_tpe_verifycardformapping:
            return "Timing TPE"
        case .api_v001_tpe_getstatusbyapptransidforclient:
            return "Timing TPE"
        case .api_v001_tpe_sdkwriteatmtime:
            return "Timing TPE"
        case .api_v001_tpe_removemapcard:
            return "Timing TPE"
        case .api_v001_tpe_sdkerrorreport:
            return "Timing TPE"
        case .api_v001_tpe_submitmapaccount:
            return "Timing TPE"
        case .api_v001_zp_upload_clientlogs:
            return "Timing TPE"
        case .api_v001_tpe_registeropandlink:
            return "Timing TPE"
        case .api_v001_tpe_verifyaccountformapping:
            return "Timing TPE"
        case .api_v001_tpe_getstatusmapaccount:
            return "Timing TPE"
        case .api_v001_tpe_removemapaccount:
            return "Timing TPE"
        case .api_v001_tpe_getbanklistgateway:
            return "Timing TPE"
        case .connector_v001_tpe_getbalance:
            return "Timing TPE"
        case .connector_v001_tpe_createwalletorder:
            return "Timing TPE"
        case .connector_v001_tpe_getinsideappresource:
            return "Timing TPE"
        case .connector_v001_tpe_getorderinfo:
            return "Timing TPE"
        case .connector_v001_tpe_gettransstatus:
            return "Timing TPE"
        case .connector_v001_tpe_transhistory:
            return "Timing TPE"
        case .connector_v001_tpe_v001getplatforminfo:
            return "Timing TPE"
        case .connector_v001_tpe_getappinfo:
            return "Timing TPE"
        case .connector_v001_tpe_getbanklist:
            return "Timing TPE"
        case .connector_v001_tpe_submittrans:
            return "Timing TPE"
        case .connector_v001_tpe_atmauthenpayer:
            return "Timing TPE"
        case .connector_v001_tpe_authcardholderformapping:
            return "Timing TPE"
        case .connector_v001_tpe_getstatusmapcard:
            return "Timing TPE"
        case .connector_v001_tpe_verifycardformapping:
            return "Timing TPE"
        case .connector_v001_tpe_getstatusbyapptransidforclient:
            return "Timing TPE"
        case .connector_v001_tpe_sdkwriteatmtime:
            return "Timing TPE"
        case .connector_v001_tpe_removemapcard:
            return "Timing TPE"
        case .connector_v001_tpe_sdkerrorreport:
            return "Timing TPE"
        case .connector_v001_tpe_submitmapaccount:
            return "Timing TPE"
        case .connector_v001_tpe_registeropandlink:
            return "Timing TPE"
        case .connector_v001_tpe_verifyaccountformapping:
            return "Timing TPE"
        case .connector_v001_tpe_getstatusmapaccount:
            return "Timing TPE"
        case .connector_v001_tpe_removemapaccount:
            return "Timing TPE"
        case .connector_v001_tpe_getbanklistgateway:
            return "Timing TPE"
        case .api_v001_tpe_getlistpaymentcodes:
            return "Timing TPE"
        case .api_v001_tpe_qrcodepayverifypin:
            return "Timing TPE"
        case .api_um_listusersbyphoneforclientasync:
            return "Timing UM"
        case .api_um_getrequeststatusforclient:
            return "Timing UM"
        case .connector_um_listusersbyphoneforclientasync:
            return "Timing UM"
        case .connector_um_getrequeststatusforclient:
            return "Timing UM"
        case .api_filter_content:
            return "Timing ATLAS"
        case .api_redpackage_createbundleorder:
            return "Timing RedPacket"
        case .api_redpackage_submittosendbundle:
            return "Timing RedPacket"
        case .api_redpackage_submitopenpackage:
            return "Timing RedPacket"
        case .api_redpackage_getsentbundlelist:
            return "Timing RedPacket"
        case .api_redpackage_getrevpackagelist:
            return "Timing RedPacket"
        case .api_redpackage_getpackagesinbundle:
            return "Timing RedPacket"
        case .api_redpackage_getappinfo:
            return "Timing RedPacket"
        case .api_redpackage_submittosendbundlebyzalopayinfo:
            return "Timing RedPacket"
        case .api_redpackage_getlistpackagestatus:
            return "Timing RedPacket"
        case .connector_redpackage_createbundleorder:
            return "Timing RedPacket"
        case .connector_redpackage_submittosendbundle:
            return "Timing RedPacket"
        case .connector_redpackage_submitopenpackage:
            return "Timing RedPacket"
        case .connector_redpackage_getsentbundlelist:
            return "Timing RedPacket"
        case .connector_redpackage_getrevpackagelist:
            return "Timing RedPacket"
        case .connector_redpackage_getpackagesinbundle:
            return "Timing RedPacket"
        case .connector_redpackage_getappinfo:
            return "Timing RedPacket"
        case .connector_redpackage_submittosendbundlebyzalopayinfo:
            return "Timing RedPacket"
        case .connector_redpackage_getlistpackagestatus:
            return "Timing RedPacket"
        case .api_notification_ping_pong:
            return "Timing Notification"
        case .api_get_voucher_status:
            return "Timing Voucher"
        case .api_use_voucher:
            return "Timing Voucher"
        case .api_revert_voucher:
            return "Timing Voucher"
        case .connector_get_voucher_status:
            return "Timing Voucher"
        case .connector_use_voucher:
            return "Timing Voucher"
        case .connector_revert_voucher:
            return "Timing Voucher"
        case .api_get_voucher_history:
            return "Timing Voucher"
        case .connector_get_voucher_history:
            return "Timing Voucher"
        case .api_getusevoucherstatus:
            return "Timing Voucher"
        case .api_getrevertvoucherstatus:
            return "Timing Voucher"
        case .api_topup_getpricelist:
            return "Timing Topup"
        case .api_topup_getrecentlist:
            return "Timing Topup"
        case .api_topup_gettransbyzptransid:
            return "Timing Topup"
        case .api_topup_createorder:
            return "Timing Topup"
        case .api_topup_updateorder:
            return "Timing Topup"
        case .balance_launched:
            return "Balance"
        case .balance_touch_back:
            return "Balance"
        case .balance_touch_addcash:
            return "Balance_AddCash"
        case .balance_addcash_touch_back:
            return "Balance_AddCash"
        case .balance_addcash_input:
            return "Balance_AddCash"
        case .balance_addcash_result:
            return "Balance_AddCash"
        case .balance_touch_withdraw:
            return "Balance_Withdraw"
        case .balance_withdraw_touch_back:
            return "Balance_Withdraw"
        case .balance_withdraw_touch_100:
            return "Balance_Withdraw"
        case .balance_withdraw_touch_200:
            return "Balance_Withdraw"
        case .balance_withdraw_touch_500:
            return "Balance_Withdraw"
        case .balance_withdraw_touch_1000:
            return "Balance_Withdraw"
        case .balance_withdraw_touch_2000:
            return "Balance_Withdraw"
        case .balance_withdraw_touch_other:
            return "Balance_Withdraw"
        case .balance_withdraw_result:
            return "Balance_Withdraw"
        case .linkbank_launched:
            return "Bank"
        case .linkbank_touch_back:
            return "Bank"
        case .linkbank_touch_add:
            return "Bank"
        case .linkbank_touch_add_icon:
            return "Bank"
        case .linkbank_add_touch_back:
            return "Bank"
        case .linkbank_add_select_jcb:
            return "Bank"
        case .linkbank_add_select_visa:
            return "Bank"
        case .linkbank_add_select_master:
            return "Bank"
        case .linkbank_add_select_bank_code:
            return "Bank"
        case .linkbank_add_bank_touch_close:
            return "Bank"
        case .linkbank_add_bank_close_touch_yes:
            return "Bank"
        case .linkbank_add_bank_close_touch_no:
            return "Bank"
        case .linkbank_add_bank_cardnumber_input:
            return "Bank"
        case .linkbank_add_bank_cardnumber_forward:
            return "Bank"
        case .linkbank_add_bank_date_back:
            return "Bank"
        case .linkbank_add_bank_date_forward:
            return "Bank"
        case .linkbank_add_bank_name_back:
            return "Bank"
        case .linkbank_add_bank_name_forward:
            return "Bank"
        case .linkbank_add_bank_OTP:
            return "Bank"
        case .linkbank_add_bank_OTP_continue:
            return "Bank"
        case .linkbank_add_result:
            return "Bank"
        case .linkbank_delete:
            return "Bank"
        case .linkbank_delete_result:
            return "Bank"
        case .pay_launched:
            return "Pay"
        case .pay_touch_back:
            return "Pay"
        case .pay_touch_scanqr:
            return "Pay"
        case .pay_touch_nfc:
            return "Pay"
        case .pay_touch_bluetooth:
            return "Pay"
        case .pay_scanqr_touch_photo:
            return "Pay"
        case .pay_scanqr_result_fail:
            return "Pay"
        case .pay_scanqr_result_fail_close:
            return "Pay"
        case .pay_scanqr_result_sucess_moneytransfer:
            return "Pay"
        case .pay_scanqr_result_sucess_payment:
            return "Pay"
        case .pay_nfc_result_fail:
            return "Pay"
        case .pay_nfc_result_success:
            return "Pay"
        case .moneytransfer_launched:
            return "Money Transfer"
        case .moneytransfer_touch_back:
            return "Money Transfer"
        case .moneytransfer_touch_phonebook:
            return "Money Transfer"
        case .moneytransfer_touch_phonenumber:
            return "Money Transfer"
        case .moneytransfer_touch_zpid:
            return "Money Transfer"
        case .moneytransfer_touch_recent1:
            return "Money Transfer"
        case .moneytransfer_touch_recent2:
            return "Money Transfer"
        case .moneytransfer_touch_recent3:
            return "Money Transfer"
        case .moneytransfer_touch_recent4:
            return "Money Transfer"
        case .moneytransfer_touch_recent5:
            return "Money Transfer"
        case .moneytransfer_touch_recent6:
            return "Money Transfer"
        case .moneytransfer_touch_recent7:
            return "Money Transfer"
        case .moneytransfer_touch_recent8:
            return "Money Transfer"
        case .moneytransfer_touch_recent9:
            return "Money Transfer"
        case .moneytransfer_touch_recent10:
            return "Money Transfer"
        case .moneytransfer_touch_recent10plus:
            return "Money Transfer"
        case .moneytransfer_zpid_touch_back:
            return "Money Transfer"
        case .moneytransfer_zpdi_touch_continue:
            return "Money Transfer"
        case .moneytransfer_input_launched:
            return "Money Transfer"
        case .moneytransfer_input_touch_back:
            return "Money Transfer"
        case .moneytransfer_input_amount:
            return "Money Transfer"
        case .moneytransfer_input_amount_touch_clear:
            return "Money Transfer"
        case .moneytransfer_input_message:
            return "Money Transfer"
        case .moneytransfer_input_message_touch_clear:
            return "Money Transfer"
        case .moneytransfer_input_touch_continue:
            return "Money Transfer"
        case .moneyreceive_launched:
            return "Money Receive"
        case .moneyreceive_touch_back:
            return "Money Receive"
        case .moneyreceive_result:
            return "Money Receive"
        case .moneyreceive_touch_input:
            return "Money Receive"
        case .moneyreceive_input_touch_back:
            return "Money Receive"
        case .moneyreceive_input_amount:
            return "Money Receive"
        case .moneyreceive_input_amount_touch_clear:
            return "Money Receive"
        case .moneyreceive_input_message:
            return "Money Receive"
        case .moneyreceive_input_message_touch_clear:
            return "Money Receive"
        case .moneyreceive_input_touch_confirm:
            return "Money Receive"
        case .moneyreceive_touch_clear:
            return "Money Receive"
        case .quickpay_launched:
            return "Quickpay"
        case .quickpay_touch_back:
            return "Quickpay"
        case .quickpay_touch_cont:
            return "Quickpay"
        case .quickpay_touch_addcash:
            return "Quickpay"
        case .quickpay_touch_withdraw:
            return "Quickpay"
        case .quickpay_codeview:
            return "Quickpay"
        case .quickpay_codeview_touch_back:
            return "Quickpay"
        case .quickpay_codeview_touch_barcode:
            return "Quickpay"
        case .quickpay_codeview_touch_qrcode:
            return "Quickpay"
        case .quickpay_codeview_touch_customize:
            return "Quickpay"
        case .quickpay_customize_touch_setupinfor:
            return "Quickpay"
        case .quickpay_customize_touch_changecode:
            return "Quickpay"
        case .quickpay_customize_touch_instruction:
            return "Quickpay"
        case .quickpay_result_success:
            return "Quickpay"
        case .quickpay_result_fail:
            return "Quickpay"
        case .redpacket_launched:
            return "Red Packet"
        case .redpacket_home_touch_start:
            return "Red Packet"
        case .redpacket_home_touch_back:
            return "Red Packet"
        case .redpacket_home_touch_sent:
            return "Red Packet"
        case .redpacket_home_touch_received:
            return "Red Packet"
        case .redpacket_lixi_touch_back:
            return "Red Packet"
        case .redpacket_lixi_back_stay:
            return "Red Packet"
        case .redpacket_lixi_back_close:
            return "Red Packet"
        case .redpacket_lixi_touch_theme:
            return "Red Packet"
        case .redpacket_lixi_touch_complete:
            return "Red Packet"
        case .redpacket_lixi_touch_phonebook:
            return "Red Packet"
        case .redpacket_lixi_touch_amount:
            return "Red Packet"
        case .redpacket_theme_touch_back:
            return "Red Packet"
        case .redpacket_theme_touch_image:
            return "Red Packet"
        case .redpacket_theme_touch_button:
            return "Red Packet"
        case .redpacket_theme_wish_expand:
            return "Red Packet"
        case .redpacket_theme_wish_hide:
            return "Red Packet"
        case .redpacket_theme_wish_edit:
            return "Red Packet"
        case .redpacket_theme_preview:
            return "Red Packet"
        case .redpacket_theme_done:
            return "Red Packet"
        case .redpacket_preview_back:
            return "Red Packet"
        case .redpacket_preview_done:
            return "Red Packet"
        case .redpacket_pay_success:
            return "Red Packet"
        case .redpacket_success_touch_home:
            return "Red Packet"
        case .redpacket_success_touch_again:
            return "Red Packet"
        case .redpacket_pay_fail:
            return "Red Packet"
        case .redpacket_receive_open:
            return "Red Packet"
        case .redpacket_receive_close:
            return "Red Packet"
        case .redpacket_details_equal_back:
            return "Red Packet"
        case .redpacket_details_equal_play_again:
            return "Red Packet"
        case .redpacket_details_equal_share:
            return "Red Packet"
        case .redpacket_details_random_back:
            return "Red Packet"
        case .redpacket_details_random_ranking:
            return "Red Packet"
        case .redpacket_details_random_share:
            return "Red Packet"
        case .redpacket_ranking_play_again:
            return "Red Packet"
        case .redpacket_ranking_share:
            return "Red Packet"
        case .redpacket_share_facebook:
            return "Red Packet"
        case .redpacket_share_fb_messenger:
            return "Red Packet"
        case .redpacket_share_messages:
            return "Red Packet"
        case .redpacket_share_others:
            return "Red Packet"
        case .redpacket_sent_touch_details:
            return "Red Packet"
        case .redpacket_received_touch_details:
            return "Red Packet"
        case .redpacket_theme_swipe_1:
            return "Red Packet"
        case .redpacket_theme_swipe_2:
            return "Red Packet"
        case .redpacket_theme_swipe_3:
            return "Red Packet"
        case .redpacket_theme_swipe_4:
            return "Red Packet"
        case .redpacket_theme_swipe_5:
            return "Red Packet"
        case .redpacket_theme_swipe_6:
            return "Red Packet"
        case .redpacket_theme_swipe_7:
            return "Red Packet"
        case .redpacket_theme_swipe_8:
            return "Red Packet"
        case .redpacket_theme_swipe_9:
            return "Red Packet"
        case .redpacket_theme_swipe_10:
            return "Red Packet"
        case .redpacket_theme_swipe_11:
            return "Red Packet"
        case .redpacket_theme_swipe_12:
            return "Red Packet"
        case .redpacket_theme_swipe_13:
            return "Red Packet"
        case .redpacket_theme_swipe_14:
            return "Red Packet"
        case .redpacket_theme_swipe_15:
            return "Red Packet"
        case .redpacket_theme_swipe_16:
            return "Red Packet"
        case .redpacket_theme_swipe_17:
            return "Red Packet"
        case .redpacket_theme_swipe_18:
            return "Red Packet"
        case .redpacket_theme_swipe_19:
            return "Red Packet"
        case .redpacket_theme_swipe_20:
            return "Red Packet"
        case .redpacket_theme_swipe_21:
            return "Red Packet"
        case .topup_launched:
            return "Top-up"
        case .topup_touch_back:
            return "Top-up"
        case .topup_touch_phonebook:
            return "Top-up"
        case .topup_input_phonenumber:
            return "Top-up"
        case .topup_touch_buy_prepaid_card:
            return "Top-up"
        case .topup_touch_topup_for_other_phonenumber:
            return "Top-up"
        case .topup_touch_item_mobilecard:
            return "Top-up"
        case .topup_touch_item_electricity:
            return "Top-up"
        case .topup_touch_item_water:
            return "Top-up"
        case .topup_touch_item_internet:
            return "Top-up"
        case .topup_touch_recent:
            return "Top-up"
        case .topup_recent_touch_close:
            return "Top-up"
        case .topup_recent_touch_item:
            return "Top-up"
        case .phonecard_launched:
            return "PhoneCard"
        case .phonecard_touch_back:
            return "PhoneCard"
        case .phonecard_touch_history:
            return "PhoneCard"
        case .phonecard_touch_telco:
            return "PhoneCard"
        case .phonecard_touch_value:
            return "PhoneCard"
        case .phonecard_touch_quantity_minus:
            return "PhoneCard"
        case .phonecard_touch_quantity_plus:
            return "PhoneCard"
        case .phonecard_touch_confirm:
            return "PhoneCard"
        case .phonecard_success_transaction_touch_close:
            return "PhoneCard"
        case .phonecard_transaction_detail_touch_copy:
            return "PhoneCard"
        case .phonecard_transaction_detail_touch_call:
            return "PhoneCard"
        case .phonecard_transaction_detail_touch_code:
            return "PhoneCard"
        case .phonecard_transaction_detail_copy_touch_syntax:
            return "PhoneCard"
        case .phonecard_transaction_detail_copy_touch_code:
            return "PhoneCard"
        case .phonecard_transaction_detail_copy_touch_series:
            return "PhoneCard"
        case .phonecard_transaction_detail_copy_touch_close:
            return "PhoneCard"
        case .phonecard_history_touch_copy:
            return "PhoneCard"
        case .phonecard_history_touch_call:
            return "PhoneCard"
        case .phonecard_history_touch_code:
            return "PhoneCard"
        case .phonecard_history_copy_touch_syntax:
            return "PhoneCard"
        case .phonecard_history_copy_touch_code:
            return "PhoneCard"
        case .phonecard_history_copy_touch_series:
            return "PhoneCard"
        case .phonecard_history_copy_touch_close:
            return "PhoneCard"
        case .electric_launched:
            return "Electricity"
        case .electric_touch_back:
            return "Electricity"
        case .electric_select_provider:
            return "Electricity"
        case .electric_touch_check:
            return "Electricity"
        case .electric_touch_add_bill:
            return "Electricity"
        case .electric_provider_touch_back:
            return "Electricity"
        case .electric_provider_input:
            return "Electricity"
        case .electric_input_touch_provider:
            return "Electricity"
        case .electric_touch_continue:
            return "Electricity"
        case .electric_touch_report:
            return "Electricity"
        case .electric_report_swipe:
            return "Electricity"
        case .water_launched:
            return "Water"
        case .water_touch_back:
            return "Water"
        case .water_select_provider:
            return "Water"
        case .water_touch_check:
            return "Water"
        case .water_touch_add_bill:
            return "Water"
        case .water_provider_touch_back:
            return "Water"
        case .water_provider_input:
            return "Water"
        case .water_input_touch_provider:
            return "Water"
        case .water_touch_continue:
            return "Water"
        case .internet_launched:
            return "Internet"
        case .internet_touch_back:
            return "Internet"
        case .internet_select_provider:
            return "Internet"
        case .internet_touch_check:
            return "Internet"
        case .internet_touch_add_bill:
            return "Internet"
        case .internet_provider_touch_back:
            return "Internet"
        case .internet_provider_input:
            return "Internet"
        case .internet_input_touch_provider:
            return "Internet"
        case .internet_touch_continue:
            return "Internet"
        case .tv_launched:
            return "TV"
        case .tv_touch_back:
            return "TV"
        case .tv_select_provider:
            return "TV"
        case .tv_touch_check:
            return "TV"
        case .tv_touch_add_bill:
            return "TV"
        case .tv_provider_touch_back:
            return "TV"
        case .tv_provider_input:
            return "TV"
        case .tv_input_touch_provider:
            return "TV"
        case .tv_touch_continue:
            return "TV"
        case .passopen_input_touch:
            return "Pass Login"
        case .passopen_input_success:
            return "Pass Login"
        case .passopen_input_fail:
            return "Pass Login"
        case .passopen_forget_touch:
            return "Pass Login"
        case .passopen_forget_reset:
            return "Pass Login"
        case .passopen_forget_close:
            return "Pass Login"
        case .keyboard_touch_0:
            return "Keyboard"
        case .keyboard_touch_000:
            return "Keyboard"
        case .keyboard_touch_plus:
            return "Keyboard"
        case .keyboard_touch_minus:
            return "Keyboard"
        case .keyboard_touch_multiplication:
            return "Keyboard"
        case .keyboard_touch_divide:
            return "Keyboard"
        case .keyboard_touch_delete:
            return "Keyboard"
        case .popupsdk_launched:
            return "SDK Popup"
        case .popupsdk_source:
            return "SDK Popup"
        case .popupsdk_voucher:
            return "SDK Popup"
        case .popupsdk_pay:
            return "SDK Popup"
        case .popupsdk_close:
            return "SDK Popup"
        case .popupsdk_out_main:
            return "SDK Popup"
        case .popupsdk_source_back:
            return "SDK Popup"
        case .popupsdk_voucher_back:
            return "SDK Popup"
        case .popupsdk_add:
            return "SDK Popup"
        case .popupsdk_password_input:
            return "SDK Popup"
        case .popupsdk_password_fail:
            return "SDK Popup"
        case .popupsdk_password_back:
            return "SDK Popup"
        case .popupsdk_otp_input:
            return "SDK Popup"
        case .popupsdk_otp_fail:
            return "SDK Popup"
        case .popupsdk_otp_resend:
            return "SDK Popup"
        case .popupsdk_otp_close:
            return "SDK Popup"
        case .popupsdk_auth_confirm:
            return "SDK Popup"
        case .popupsdk_auth_cancel:
            return "SDK Popup"
        case .popupsdk_success:
            return "SDK Popup"
        case .popupsdk_fail:
            return "SDK Popup"
        case .popupsdk_voucher_non:
            return "SDK Popup"
        case .sdk_open:
            return "SDK Default"
        case .sdk_zalopay:
            return "SDK Default"
        case .sdk_changesource:
            return "SDK Default"
        case .sdk_add:
            return "SDK Default"
        case .sdk_voucher:
            return "SDK Default"
        case .sdk_pay:
            return "SDK Default"
        case .sdk_password_input:
            return "SDK Default"
        case .sdk_password_fail:
            return "SDK Default"
        case .sdk_password_back:
            return "SDK Default"
        case .sdk_otp_input:
            return "SDK Default"
        case .sdk_otp_fail:
            return "SDK Default"
        case .sdk_otp_resend:
            return "SDK Default"
        case .sdk_otp_close:
            return "SDK Default"
        case .sdk_auth_confirm:
            return "SDK Default"
        case .sdk_success:
            return "SDK Default"
        case .sdk_fail:
            return "SDK Default"
        case .sdk_voucher_non:
            return "SDK Default"
        case .sdk_cancel:
            return "SDK Default"
        case .contact_launched:
            return "Contactlist"
        case .contact_setting_update:
            return "Contactlist"
        case .contact_touch_favorite:
            return "Contactlist"
        case .contact_touch_unfavorite:
            return "Contactlist"
        case .contact_touch_back:
            return "Contactlist"
        case .contact_search_input:
            return "Contactlist"
        case .contact_touch_zalopay:
            return "Contactlist"
        case .contact_touch_all:
            return "Contactlist"
        case .contact_touch_refresh_top:
            return "Contactlist"
        case .contact_touch_refresh_bot:
            return "Contactlist"
        case .contact_touch_pull_to_refresh:
            return "Contactlist"
        case .contact_search_phonenumber:
            return "Contactlist"
        case .contact_search_name:
            return "Contactlist"
        case .contact_switch_keyboard_to_123:
            return "Contactlist"
        case .contact_switch_keyboard_to_abc:
            return "Contactlist"
        case .contact_touch_friend_favorite:
            return "Contactlist"
        case .contact_touch_friend_nonfavorite:
            return "Contactlist"
        case .contact_touch_nonzpfriend:
            return "Contactlist"
        case .contact_nonzpfriend_touch_close:
            return "Contactlist"
        case .contact_nonzpfriend_touch_invite:
            return "Contactlist"
        case .contact_nonzpfriend_invite_touch_zalo:
            return "Contactlist"
        case .contact_read_contact_deny:
            return "Contactlist"
        case .contact_launch_from_transfer:
            return "Contactlist"
        case .contact_launch_from_topup:
            return "Contactlist"
        case .contact_touch_alphabet:
            return "Contactlist"
        case .contact_touch_zpid:
            return "Contactlist"
        case .touch3d_qr:
            return "3D Touch"
        case .touch3d_transactionlogs:
            return "3D Touch"
        default:
            return ""
        }
    }
    
}