//
//  ZaloPayAnalyticsHeaderHelper.m
//  ZaloPayAnalyticsSwift
//
//  Created by Dung Vu on 4/10/18.
//  Copyright © 2018 VNG. All rights reserved.
//
#import "ZaloPayAnalyticsHeaderHelper.h"
@implementation NSException(Extension)
+ (instancetype)tryBlockError:(void (^)(void))block {
    @try {
        block();
    }
    @catch (NSException *exception) {
        return exception;
    }
    return nil;
    
}
@end

