import os
import csv
import json

def read_csv(file_path):
    data = []
    with open(file_path, 'rb') as csvfile:
        event_reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        rownum = 0
        for row in event_reader:
            if rownum > 0:
                data.append(row)
            rownum += 1
    return data

def map_to_json(data):
    jsonData = {}
    for row in data:
        key = row[1]
        values = row[2].split(",")
        values = list(map(lambda value: value.strip(), values))
        jsonData[key] = values
    return jsonData

def write_output(file_path, data):
    print("Updating %s" % file_path)
    with open(file_path, 'w') as outfile:
        json.dump(data, outfile, indent=2)

def main():
    import sys

    data = read_csv(sys.argv[1])
    jsonData = map_to_json(data)
    write_output("special_keys_tracking_log.json", jsonData)

if __name__ == '__main__':
    main()