
#!/bin/sh -e
sh ./carthage_local_build.sh ZaloPayModuleProtocols
sh ./carthage_local_build.sh ZaloPayCommonSwift
sh ./carthage_local_build.sh GoogleReporter
sh ./carthage_local_build.sh ZaloPayAnalyticsSwift
sh ./carthage_local_build.sh ZaloPayConfig
sh ./carthage_local_build.sh ZaloPayNetwork
sh ./carthage_local_build.sh ZaloPayProfile
sh ./carthage_local_build.sh ZaloPayShareControl
sh ./carthage_local_build.sh ZaloPayUI
sh ./carthage_local_build.sh ZaloPayWeb
