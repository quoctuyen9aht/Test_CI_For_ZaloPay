
#!/bin/sh -e

xcconfig=$(mktemp /tmp/static.xcconfig.XXXXXX)
trap 'rm -f "$xcconfig"' INT TERM HUP EXIT

echo "LD = $PWD/./ld.py" >> $xcconfig
echo "ARCHS = x86_64 i386 arm64 armv7 armv7s" >> $xcconfig
echo "DEBUG_INFORMATION_FORMAT = dwarf" >> $xcconfig
echo "IPHONEOS_DEPLOYMENT_TARGET = 8.0" >> $xcconfig
echo "ENABLE_BITCODE = NO" >> $xcconfig
echo "MACH_O_TYPE = mh_dylib" >> $xcconfig

export XCODE_XCCONFIG_FILE="$xcconfig"

framework_name=$1
framework_patch='./'"$framework_name"

framework_output_patch='./'"$framework_name"'/Carthage/Build/iOS/'"$framework_name"'.framework'
main_output_patch='./ZaloPayFramework/'

carthage build --no-skip-current --project-directory "$framework_patch"

cp -R "$framework_output_patch" "$main_output_patch"
rm -rf "$framework_patch"'/Carthage'
