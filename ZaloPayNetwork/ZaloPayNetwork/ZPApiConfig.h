//
//  ZPApiConfig.h
//  ZaloPayNetwork
//
//  Created by bonnpv on 5/10/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZPApiConfig : NSObject
@property (nonatomic) BOOL usingConnector;
@property (nonatomic, strong) NSDictionary *connectorApiPath;
@property (nonatomic) float timeoutInterval;
- (void)updateConfigFromJson:(NSDictionary *)config;
@end
