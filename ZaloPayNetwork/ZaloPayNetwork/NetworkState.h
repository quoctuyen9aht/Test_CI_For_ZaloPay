//
//  NetworkState.h
//  ZaloPayNetwork
//
//  Created by bonnpv on 6/24/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveObjC/ReactiveObjC.h>

@interface NetworkState : NSObject
@property (nonatomic, readonly) BOOL isReachable;
@property (nonatomic, readonly) RACSignal *networkSignal;
+ (instancetype)sharedInstance;
- (NSString*)connectionType;
@end
