//
//  NSError+ZaloPayAPI.m
//  ZaloPayNetwork
//
//  Created by bonnpv on 6/9/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "NSError+ZaloPayAPI.h"
#import <ZaloPayCommon/NSCollection+JSON.h>
#import "ZaloPayErrorCode.h"
#import <ZaloPayCommon/ZPConstants.h>
#import <ZaloPayCommon/R.h>
#import <AFNetworking/AFNetworking.h>

@implementation NSError (ZaloPayAPI)

+ (instancetype)errorFromDic:(NSDictionary *)dic {
    int errorCode = [dic intForKey:@"returncode"];
    return  [NSError errorWithDomain:@"API Return Error"
                                code:errorCode
                            userInfo:dic];
}

+ (NSDictionary *)zpNetworkErrorDic {
    static NSDictionary *errorDic;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (errorDic != nil ) {
            return;
        }
        NSMutableDictionary *errorMap = [@{
                                           @(NSURLErrorUnknown): @(true),
                                           @(NSURLErrorCancelled): @(true),
                                           @(NSURLErrorBadURL): @(true),
                                           //                      @(NSURLErrorTimedOut): @(true),
                                           @(NSURLErrorUnsupportedURL): @(true),
                                           @(NSURLErrorCannotFindHost): @(true),
                                           @(NSURLErrorCannotConnectToHost): @(true),
                                           @(NSURLErrorNetworkConnectionLost): @(true),
                                           @(NSURLErrorDNSLookupFailed): @(true),
                                           @(NSURLErrorHTTPTooManyRedirects): @(true),
                                           @(NSURLErrorResourceUnavailable): @(true),
                                           @(NSURLErrorNotConnectedToInternet): @(true),
                                           @(NSURLErrorRedirectToNonExistentLocation): @(true),
                                           @(NSURLErrorBadServerResponse): @(true),
                                           @(NSURLErrorUserCancelledAuthentication): @(true),
                                           @(NSURLErrorUserAuthenticationRequired): @(true),
                                           @(NSURLErrorZeroByteResource): @(true),
                                           @(NSURLErrorCannotDecodeRawData): @(true),
                                           @(NSURLErrorCannotDecodeContentData): @(true),
                                           @(NSURLErrorCannotParseResponse): @(true),
                                           } mutableCopy];
        if (@available(iOS 9, *)) {
            [errorMap setObject:@(true) forKey:@(NSURLErrorAppTransportSecurityRequiresSecureConnection)];
            [errorMap setObject:@(true) forKey:@(NSURLErrorDataLengthExceedsMaximum)];
        }
        if (@available(iOS 10.3, *)) {
            [errorMap setObject:@(true) forKey:@(NSURLErrorFileOutsideSafeArea)];
        } 
        errorDic = errorMap;
    });
    
    return errorDic;
}

- (BOOL)isNetworkConnectionError {
    if ([self isApiError]) {
        return false;
    }
    return [[NSError zpNetworkErrorDic] boolForKey:@(self.code)];
}

- (BOOL)isRequestTimeout {
    return self.code == NSURLErrorTimedOut;
}

- (BOOL)isApiError {
    return [self.userInfo isKindOfClass:[NSDictionary class]] &&
           [self.userInfo objectForKey:@"returncode"] != nil;
}

- (NSString *)apiErrorMessage {
    NSDictionary *userInfo = self.userInfo;
    NSString *message = [userInfo stringForKey:@"returnmessage"];
    if (message.length > 0) {
        return message;
    }
    
    if ([self isNetworkConnectionError]) {
        return [R string_NetworkError_NoConnectionMessage];
    }
    
    if ([self isRequestTimeout]) {
        return [R string_NetworkError_ConnectionTimeOut];
    }
    
    if ([self httpErrorCode] == 404) {
        return [R string_Http_Error_404];
    }
    
    return [R string_NetworkError_TryAgainMessage];
}

- (NSInteger)httpErrorCode {    
    NSHTTPURLResponse *httpResponse = [self.userInfo objectForKey:AFNetworkingOperationFailingURLResponseErrorKey];
    if ([httpResponse isKindOfClass:[NSHTTPURLResponse class]]) {
        return  httpResponse.statusCode;
    }
    return 0;
}

@end
