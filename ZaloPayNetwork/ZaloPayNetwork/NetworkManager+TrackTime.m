//
//  NetworkManager+TrackTime.m
//  ZaloPayNetwork
//
//  Created by Dung Vu on 9/7/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import "NetworkManager+TrackTime.h"
#import <ReactiveObjC/ReactiveObjC.h>
#import "NetworkManager.h"
#import <objc/runtime.h>

extern NSString *kBaseUrl;
extern NSString *kUrlTopUp;
extern NSString *kUrlVoucher;
extern NSString *kLixiApiUrl;
extern NSString *kUploadClientLog;

@interface NetworkManager(Time)
@property (nonatomic, strong) NSMutableDictionary *allDurations;
@end

@interface NSThread(RunBlock)
- (void)performBlock:(void(^)(void))block;
- (void)performBlock:(void (^)(void))block afterDelay:(NSTimeInterval)delay;
@end

#pragma mark - Record Protocol
@interface NetworkManagerRecorderProtocol()<NSURLSessionDataDelegate>
@property (strong, nonatomic) NSDate *startDate;
@property (strong, nonatomic) NSURLSessionDataTask *currentTask;
@property (strong, nonatomic) NSThread *clientThread;
@property (strong, nonatomic) NSOperationQueue *currentQueue;
@end

@implementation NetworkManagerRecorderProtocol

+ (BOOL)canInitWithRequest:(NSURLRequest *)request {
    NSString *requestHost = request.URL.host;
    BOOL isCheck = [self isRequestNeedTracking:requestHost];
    if (isCheck) {
        [request setTrackId:@([[NSDate date] timeIntervalSince1970])];
    }
    return isCheck;
}

+ (BOOL)canInitWithTask:(NSURLSessionTask *)task {
    NSString *requestHost = task.originalRequest.URL.host;
    BOOL isCheck = [self isRequestNeedTracking:requestHost];
    if (isCheck) {
        NSNumber *trackId = @([[NSDate date] timeIntervalSince1970]);
        [task.currentRequest setTrackId:trackId];
    }
    return isCheck;
}

+ (BOOL)isRequestNeedTracking:(NSString *)host {
    if ([host length] == 0) {
        return NO;
    }
    // Check
    NSArray<NSString *> *urlsCheck = @[kBaseUrl, kUrlTopUp, kUrlVoucher, kLixiApiUrl, kUploadClientLog];
    NSInteger result = [urlsCheck indexOfObjectPassingTest:^BOOL(NSString * _Nonnull url, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *hostCheck = [NSURL URLWithString:url].host;
        BOOL isCheck = [hostCheck isEqualToString:host];
        return isCheck;
    }];
    
    return result != NSNotFound;
}

+ (NSURLRequest *)canonicalRequestForRequest:(NSURLRequest *)request {
    return request;
}

- (void)startLoading {
    if (!self.currentQueue) {
        self.currentQueue = [NSOperationQueue new];
        _currentQueue.name = @"com.zalopay.checkTime";
    }
    self.clientThread = [NSThread currentThread];
    // Prevent crash if request of protocol nil
    if (!self.request) {
        @weakify(self);
        [self.clientThread performBlock:^{
            @strongify(self);
            [self.client URLProtocolDidFinishLoading:self];
        }];
        return;
    }
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    configuration.timeoutIntervalForRequest = [[NetworkManager sharedInstance] getNetworkTimeoutInterval];
    __weak typeof(self) wSelf = self;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:wSelf delegateQueue:_currentQueue];
    self.currentTask = [session dataTaskWithRequest:self.request];
    self.startDate = [NSDate date];
    [_currentTask resume];
}

- (void)stopLoading {
    if (!self.clientThread) {
        return;
    }
    @weakify(self);
    [self.clientThread performBlock:^{
        @strongify(self);
        [self.currentTask cancel];
        self.currentTask = nil;
    }];
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data {
    [session finishTasksAndInvalidate];
    if (!self.clientThread) {
        return;
    }
    @weakify(self);
    [self.clientThread performBlock:^{
        @strongify(self);
        [self.client URLProtocol:self didLoadData:data];
    }];
    
}
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    [session finishTasksAndInvalidate];
    if (!self.clientThread) {
        return;
    }
    
    @weakify(self);
    [self.clientThread performBlock:^{
        @strongify(self);
        if (error) {
            [self.client URLProtocol:self didFailWithError:error];
        } else {
            NSTimeInterval duration = [[NSDate date] timeIntervalSinceDate:self.startDate];
            [self.request setDurationRequest:duration * 1000];
            [self.client URLProtocolDidFinishLoading:self];
        }
    }];
    
}
@end
static void * NSURLRequest_EventId = &NSURLRequest_EventId;
@implementation NSURLRequest (Extension)
- (void)setTrackId:(NSNumber *)trackId {
    objc_setAssociatedObject(self, NSURLRequest_EventId, trackId, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)trackId {
    return objc_getAssociatedObject(self, NSURLRequest_EventId);
}

- (void)setDurationRequest:(NSTimeInterval)duration {
    NetworkManager *manager = [NetworkManager sharedInstance];
    NSNumber *trackId = [self trackId];
    NSString *key = [NSString stringWithFormat:@"%@%@",[trackId stringValue] ?: @"", self.URL.absoluteString];
    @synchronized (manager.allDurations) {
        [manager.allDurations setValue:@(duration) forKey:key];
    };
}
- (NSTimeInterval)getTimeDurationRequest {
    NetworkManager *manager = [NetworkManager sharedInstance];
    NSTimeInterval result;
    NSNumber *trackId = [self trackId];
    NSString *key = [NSString stringWithFormat:@"%@%@",[trackId stringValue] ?: @"", self.URL.absoluteString];
    
    @synchronized (manager.allDurations) {
        NSNumber *d = [manager.allDurations valueForKey:key];
        [manager.allDurations removeObjectForKey:key];
        result = d ? [d doubleValue] : -1;
    };
    
    return result;
}
@end

@implementation NSThread(RunBlock)
- (void)performBlock:(void(^)(void))block {
    [self performSelector:@selector(onlyForRunBlock:) onThread:self withObject:block waitUntilDone:NO];
}
- (void)performBlock:(void (^)(void))block afterDelay:(NSTimeInterval)delay {
    @weakify(self);
    [self performBlock:^{
        @strongify(self);
        [self performSelector:@selector(onlyForRunBlock:) withObject:block afterDelay:delay];
    }];
}

- (void)onlyForRunBlock:(void(^)(void))block
{
    block();
}

@end

@implementation NSURLSessionTask (NetworkManager)
static void * NSURLSessionTask_Request_EventId = &NSURLSessionTask_Request_EventId;

- (NSNumber *)requestEventId {
    return objc_getAssociatedObject(self, NSURLSessionTask_Request_EventId);
}

- (void)setRequestEventId:(NSNumber *)requestEventId{
    objc_setAssociatedObject(self, NSURLSessionTask_Request_EventId, requestEventId, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


@end
