//
//  ZPNetWorkApi.h
//  ZaloPayNetwork
//
//  Created by bonnpv on 3/22/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

NSInteger requestEventIdFromApi(NSString* api);
NSInteger connectorEventIdFromApi(NSString* api);

NSString* badwordUrlFromPath(NSString *path);

extern NSString *api_um_loginviazalo;
extern NSString *api_um_createaccesstoken;
extern NSString *api_um_loginbyphone;
extern NSString *api_um_lockuserforclient;
extern NSString *api_um_registerphonenumber;
extern NSString *api_um_authenphonenumber;
extern NSString *api_um_authenphonenumberv2;
extern NSString *api_um_authenloginbyphone;
extern NSString *api_um_removeaccesstoken;
extern NSString *api_um_verifycodetest;
extern NSString *api_v001_tpe_getorderinfo;
extern NSString *api_v001_tpe_createorder;
extern NSString *api_v001_tpe_createwalletorder;
extern NSString *api_um_getuserinfo;
extern NSString *api_um_checklistzaloidforclient;
extern NSString *api_um_updateprofile;
extern NSString *api_um_verifyotpprofile;
extern NSString *api_um_recoverypin;
extern NSString *api_umupload_preupdateprofilelevel3;
extern NSString *api_um_getuserinfobyzalopayid;
extern NSString *api_um_getuserinfobyphone;
extern NSString *api_um_verifyaccesstoken;
extern NSString *api_um_sendnotification;
extern NSString *api_um_checkzalopaynameexist;
extern NSString *api_um_updatezalopayname;
extern NSString *api_um_getuserinfobyzalopayname;
extern NSString *api_um_validatepin;
extern NSString *api_v001_tpe_getbalance;
extern NSString *api_um_listcardinfo;
extern NSString *api_um_getuserprofilelevel;
extern NSString *api_um_getuserprofilesimpleinfo;
extern NSString *api_v001_tpe_getinsideappresource;
extern NSString *api_v001_tpe_transhistory;
extern NSString *api_ummerchant_getmerchantuserinfo;
extern NSString *api_ummerchant_getlistmerchantuserinfo;
extern NSString *api_v001_tpe_gettransstatus;
extern NSString *api_redpackage_createbundleorder;
extern NSString *api_redpackage_submittosendbundle;
extern NSString *api_redpackage_submittosendbundlebyzalopayinfo;
extern NSString *api_redpackage_submitopenpackage;
extern NSString *api_redpackage_getsentbundlelist;
extern NSString *api_redpackage_getrevpackagelist;
extern NSString *api_redpackage_getpackagesinbundle;
extern NSString *api_redpackage_getappinfo;
extern NSString *api_redpackage_getbundlestatus;
extern NSString *api_redpackage_getlistpackagestatus;
extern NSString *api_v001_tpe_v001getplatforminfo;
extern NSString *api_v001_tpe_getappinfo;
extern NSString *api_v001_tpe_getbanklist;
extern NSString *api_v001_tpe_submittrans;
extern NSString *api_v001_tpe_atmauthenpayer;
extern NSString *api_v001_tpe_authcardholderformapping;
extern NSString *api_v001_tpe_getstatusmapcard;
extern NSString *api_v001_tpe_verifycardformapping;
extern NSString *api_v001_tpe_getstatusbyapptransidforclient;
extern NSString *api_v001_tpe_sdkwriteatmtime;
extern NSString *api_v001_tpe_removemapcard;
extern NSString *api_v001_tpe_sdkerrorreport;
extern NSString *api_um_listcardinfoforclient;
extern NSString *api_v001_tpe_submitmapaccount;
extern NSString *api_um_listbankaccountforclient;
extern NSString *api_v001_zp_upload_clientlogs;
extern NSString *api_getvoucherstatus;
extern NSString *api_getvoucherhistory;
extern NSString *api_usevoucher;
extern NSString *api_getusevoucherstatus;
extern NSString *api_getrevertvoucherstatus;
extern NSString *api_revertvoucher;

// Direct discount
extern NSString *api_getpromotioncampaigns;
extern NSString *api_registerpromotion;
extern NSString *api_getregisterpromotionstatus;
extern NSString *api_updatepromotion;
extern NSString *api_revertpromotion;
extern NSString *api_getrevertpromotionstatus;


extern NSString *api_um_listusersbyphoneforclient;
extern NSString *api_v001_tpe_registeropandlink;
extern NSString *api_um_getsessionloginforclient;
extern NSString *api_um_removesessionloginforclient;
extern NSString *api_um_updateuserinfo4client;
extern NSString *api_v001_tpe_qr_getpaymentcode;
extern NSString *api_v001_tpe_qr_verifypinqrcodepay;
extern NSString *api_um_listusersbyphoneforclientasync;
extern NSString *api_um_getrequeststatusforclient;
extern NSString *api_tpe_getbanklistgateway;
extern NSString *api_inspector_v1_inspect;
extern NSString *api_v001_tpe_removemapaccount;
extern NSString *api_v001_tpe_getstatusmapaccount;
extern NSString *api_v001_tpe_verifyaccountformapping;
extern NSString *api_um_registeruserinfo;
extern NSString *api_um_registerpin;
extern NSString *api_um_setkyc;
extern NSString *api_umupload_statusupdateproflvl3;


