//
//  ZPReactNetwork.h
//  ZaloPayNetwork
//
//  Created by bonnpv on 9/19/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveObjC/ReactiveObjC.h>

@interface ZPReactNetwork : NSObject
- (RACSignal *)requestWithMethod:(NSString *)method
                          header:(NSDictionary *)headers
                       urlString:(NSString *)urlString
                          params:(NSDictionary *)param
                            body:(NSString *)bodyString;
@end
