//
//  RACSignal+RetryWithFilerError.m
//  ZaloPayNetwork
//
//  Created by bonnpv on 5/17/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "RACSignal+RetryFilter.h"
static RACDisposable *zpsubscribeForever (RACSignal *signal, void (^next)(id), void (^error)(NSError *, RACDisposable *), void (^completed)(RACDisposable *)) {
    next = [next copy];
    error = [error copy];
    completed = [completed copy];
    
    RACCompoundDisposable *compoundDisposable = [RACCompoundDisposable compoundDisposable];
    
    RACSchedulerRecursiveBlock recursiveBlock = ^(void (^recurse)(void)) {
        RACCompoundDisposable *selfDisposable = [RACCompoundDisposable compoundDisposable];
        [compoundDisposable addDisposable:selfDisposable];
        
        __weak RACDisposable *weakSelfDisposable = selfDisposable;
        
        RACDisposable *subscriptionDisposable = [signal subscribeNext:next error:^(NSError *e) {
            @autoreleasepool {
                error(e, compoundDisposable);
                [compoundDisposable removeDisposable:weakSelfDisposable];
            }
            
            recurse();
        } completed:^{
            @autoreleasepool {
                completed(compoundDisposable);
                [compoundDisposable removeDisposable:weakSelfDisposable];
            }
            
            recurse();
        }];
        
        [selfDisposable addDisposable:subscriptionDisposable];
    };
    
    // Subscribe once immediately, and then use recursive scheduling for any
    // further resubscriptions.
    recursiveBlock(^{
        RACScheduler *recursiveScheduler = RACScheduler.currentScheduler ?: [RACScheduler scheduler];
        
        RACDisposable *schedulingDisposable = [recursiveScheduler scheduleRecursiveBlock:recursiveBlock];
        [compoundDisposable addDisposable:schedulingDisposable];
    });
    
    return compoundDisposable;
}

@implementation RACSignal (RetryFilter)

- (RACSignal*)retry:(int)retryCount filter:(ZPErrorFilter)filter {
    return [[RACSignal createSignal:^(id<RACSubscriber> subscriber) {
        __block NSInteger currentRetryCount = 0;
        return zpsubscribeForever(self,
                                  ^(id x) {
                                      [subscriber sendNext:x];
                                  },
                                  ^(NSError *error, RACDisposable *disposable) {
                                      if ((retryCount == 0 || currentRetryCount < retryCount) && filter(error)) {
                                          currentRetryCount++;
                                          return;
                                      }
                                      [disposable dispose];
                                      [subscriber sendError:error];
                                  },
                                  ^(RACDisposable *disposable) {
                                      [disposable dispose];
                                      [subscriber sendCompleted];
                                  });
    }] setNameWithFormat:@"[%@] -retry:withErrorFilter: %lu", self.name, (unsigned long)retryCount];
}

@end
