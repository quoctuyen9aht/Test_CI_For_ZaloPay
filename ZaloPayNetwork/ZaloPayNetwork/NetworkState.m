//
//  NetworkState.m
//  ZaloPayNetwork
//
//  Created by bonnpv on 6/24/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "NetworkState.h"
#import "ZPConnectionManager.h"
#import <AFNetworking/AFNetworking.h>

@interface NetworkState ()
@property (nonatomic, strong) RACSubject *internalSignal;
@property (nonatomic, strong) RACSignal <NSNumber *>*eNetwork;
@end

@implementation NetworkState

+ (instancetype)sharedInstance {
    static NetworkState *_return;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _return = [[NetworkState alloc] init];
    });
    return _return;
}

- (id)init {
    self = [super init];
    if (self) {
        _internalSignal = [RACSubject subject];
        RACSignal *authenSignal = [ZPConnectionManager sharedInstance].eAuthenSuccess;
        self.eNetwork = [[[[_internalSignal combineLatestWith:authenSignal] map:^NSNumber *_Nullable(RACTuple  *_Nullable value) {
            BOOL v1 = [value.first boolValue];
            BOOL v2 = [value.second boolValue];
            return @(v1 || v2);
        }] distinctUntilChanged] replayLazily];
        [self startMonitorNetwork];
    }
    return self;
}

- (RACSignal *)networkSignal {
    return _eNetwork;
}

- (BOOL)isReachable {
    if ([[ZPConnectionManager sharedInstance] isAuthenSuccess]) {
        return YES;
    }
    return [AFNetworkReachabilityManager sharedManager].isReachable;
}

- (void)startMonitorNetwork {
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        NSNumber *connected = @([AFNetworkReachabilityManager sharedManager].isReachable);
        [self.internalSignal sendNext:connected];
    }];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
}

- (NSString*)connectionType {
    AFNetworkReachabilityStatus networkStatus = [[AFNetworkReachabilityManager sharedManager] networkReachabilityStatus];
    switch (networkStatus) {
        case AFNetworkReachabilityStatusReachableViaWiFi:
            return @"wifi";
        case AFNetworkReachabilityStatusReachableViaWWAN:
            return @"wwan";
        default:
            break;
    }
    return @"unknown";
}


@end
