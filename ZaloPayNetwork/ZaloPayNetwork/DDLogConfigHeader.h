//
//  DDLogConfigHeader.h
//  ZaloPayNetwork
//
//  Created by bonnpv on 6/7/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <CocoaLumberjack/CocoaLumberjack.h>
extern const DDLogLevel zaloPayLogLevel;
#define ddLogLevel  zaloPayLogLevel

