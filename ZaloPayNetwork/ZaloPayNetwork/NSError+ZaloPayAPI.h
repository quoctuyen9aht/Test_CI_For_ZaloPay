//
//  NSError+ZaloPayAPI.h
//  ZaloPayNetwork
//
//  Created by bonnpv on 6/9/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSError (ZaloPayAPI)
+ (instancetype)errorFromDic:(NSDictionary *)dic;
- (NSString *)apiErrorMessage;

- (BOOL)isNetworkConnectionError;
- (BOOL)isRequestTimeout;
- (BOOL)isApiError;
- (NSInteger)httpErrorCode;
@end
