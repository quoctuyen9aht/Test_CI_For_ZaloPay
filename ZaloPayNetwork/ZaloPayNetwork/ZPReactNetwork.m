//
//  ZPReactNetwork.m
//  ZaloPayNetwork
//
//  Created by bonnpv on 9/19/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import "ZPReactNetwork.h"
#import "DDLogConfigHeader.h"
#import "NSError+ZaloPayAPI.h"

#import <ZaloPayCommon/NSCollection+JSON.h>
#import <AFNetworking/AFNetworking.h>
#import <ZaloPayAnalyticsSwift/ZaloPayAnalyticsSwift-Swift.h>
#import <ZaloPayUI/ZPAppFactory.h>
#import <ZaloPayUI/ZPTrackingHelper.h>
#import "NetworkManager.h"

extern NSString *kUrlTopUp;

@interface ZPReactNetwork ()
@property (nonatomic, strong) NSURLSession *client;
@end

@implementation ZPReactNetwork

NSDictionary *gTopupAPIMap;

+ (void)load {
    gTopupAPIMap = @{
         @"/getpricelist": @(ZPAnalyticEventActionApi_topup_getpricelist),
         @"/getrecentlist": @(ZPAnalyticEventActionApi_topup_getrecentlist),
         @"/gettransbyzptransid": @(ZPAnalyticEventActionApi_topup_gettransbyzptransid),
         @"/createorder": @(ZPAnalyticEventActionApi_topup_createorder),
         @"/updateorder": @(ZPAnalyticEventActionApi_topup_updateorder),
     };
}

- (NSURLSession *)client {
    if (!_client) {
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        sessionConfiguration.timeoutIntervalForRequest = [[NetworkManager sharedInstance] getNetworkTimeoutInterval];
        _client = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    }
    return _client;
}

- (RACSignal *)requestWithMethod:(NSString *)method
                          header:(NSDictionary *)headers
                       urlString:(NSString *)urlString
                          params:(NSDictionary *)param
                            body:(NSString *)bodyString {
    return [[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        NSURLRequest *request = [self urlRequestWithMethod:method
                                                    header:headers
                                                 urlString:urlString
                                                     params:param
                                                      body:bodyString];
        
        DDLogDebug(@"request = %@",request);
        NSInteger eventId = [self eventIdFromUrlString:request.URL];
        double startTime = eventId <= 0 ? 0 : [[NSDate date] timeIntervalSince1970] * 1000;
        
        NSURLSessionDataTask *task = [self.client dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            NSString *stringData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            DDLogDebug(@"error : %@",error);
            DDLogDebug(@"data = %@",stringData);
            if (error) {
                NSError *zaloPayError = [self errorFromNetworkError:error];
                [subscriber sendError:zaloPayError];
                return;
            }
            
            if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                NSInteger statusCode = [(NSHTTPURLResponse *)response statusCode];
                if (statusCode >= 400) {
                    NSError *networkError = [[NSError alloc ] initWithDomain:@"ZaloPay"
                                                                        code:statusCode
                                                                    userInfo: @{
                                                                                @"error_code": @(statusCode),
                                                                                @"error_message": @""
                                                                                }];
                    [subscriber sendError:networkError];
                    return;
                }
            }
            
            if (eventId > 0) {
                double endTime = [[NSDate date] timeIntervalSince1970] * 1000;
                NSNumber *value = @(endTime - startTime);
                [[ZPTrackingHelper shared].eventTracker trackTiming:eventId value:value];
            }
            
            [subscriber sendNext:stringData];
            [subscriber sendCompleted];
        }];
        [task resume];
        return [RACDisposable disposableWithBlock:^{
            [task cancel];
        }];
    }] replayLazily];
}

- (NSURLRequest *)urlRequestWithMethod:(NSString *)method
                                header:(NSDictionary *)headers
                             urlString:(NSString *)urlString
                                 params:(NSDictionary *)param
                                  body:(NSString *)bodyString {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    if ([headers isKindOfClass:[NSDictionary class]] && headers.count > 0) {
        request.allHTTPHeaderFields = headers;
    }
    
    if (![method isKindOfClass:[NSString class]] || method.length == 0) {
        method = @"GET";
    }
    
    request.HTTPMethod = method;
    request.timeoutInterval = [[NetworkManager sharedInstance] getNetworkTimeoutInterval];
    
    if ([bodyString isKindOfClass:[NSString class]] && bodyString.length > 0) {
        [request setHTTPBody:[bodyString dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    if ([param isKindOfClass:[NSDictionary class]] && param.count > 0) {
        NSString *query = AFQueryStringFromParameters(param);
        request.URL = [NSURL URLWithString:[[request.URL absoluteString] stringByAppendingFormat:request.URL.query ? @"&%@" : @"?%@", query]];
    }
    
    return request;
}

//{
//error_code: int, // giá trị mã lỗi
//error_message: String, // chứa thông điệp liên quan đến mã lỗi
//}
//2 -> 999	Máy chủ đang bị lỗi. Vui lòng thử lại sau	Những lỗi tương ứng với HTTP code thuộc dòng 4xx và 5xx
//-2000 -> -1	Mạng kết nối không ổn định. Vui lòng kiểm tra kết nối và thử lại 	Lỗi liên quan đến việc không thể tạo kết nối tới máy chủ
//0	Quá thời gian kết nối	Lỗi timeout khi kết nối và đợi nhận dữ liệu phản hồi từ máy chủ

- (NSError *)errorFromNetworkError:(NSError *)error {
    NSDictionary *userInfo = [self userInforFromError:error];
    return [[NSError alloc ] initWithDomain:@"ZaloPay" code:error.code userInfo:userInfo];
}

- (NSDictionary *)userInforFromError:(NSError *)error {
    NSString *message;
    NSInteger code = error.code;
    if ([error isRequestTimeout]) {
        message = @"Hết thời gian kết nối";
        code = 0;
    } else if (error.code >= 2) {
        message = @"Máy chủ đang bị lỗi. Vui lòng thử lại sau";
    } else if (error.code <= -1) {
        message = @"Mạng kết nối không ổn định. Vui lòng kiểm tra kết nối và thử lại";
    } else {
        message = @"Có lỗi xảy ra bạn vui lòng thử lại sau";
    }
    
    return @{
             @"error_code": @(code),
             @"error_message": message
             };
}

- (NSInteger)eventIdFromUrlString:(NSURL *)url {
//    NSString *urlString = url.relativeString;
//    
//    if ([urlString hasSuffix:@"/esale/zalopayshop/v4/getshopitemlist"]) {
//        return EventAction_esale_api_v4_getshopitemlist;
//    }
//    if ([urlString hasSuffix:@"/esale/zalopayshop/v4/createorder"]) {
//        return EventAction_esale_api_v4_createorder;
//    }
//    if ([urlString hasSuffix:@"/esale/zalopayshop/v4/gethistory"]) {
//        return EventAction_esale_api_v4_gethistory;
//    }
//    if ([urlString hasSuffix:@"/esale/zalopayshop/v4/getresult"]) {
//        return EventAction_esale_api_v4_getresult;
//    }
//    if ([urlString hasSuffix:@"/esale/zalopayshop/v4/carddetail"]) {
//        return EventAction_esale_api_v4_carddetail;
//    }
//    if ([urlString hasSuffix:@"/esale/zalopayshop/v4/getproviders"]) {
//        return EventAction_esale_api_v4_getproviders;
//    }
//    if ([urlString hasSuffix:@"/esale/zalopayshop/v4/querybill"]) {
//        return EventAction_esale_api_v4_querybill;
//    }
    
    NSString *apiPath = [url path];
    NSString *urlPath = [NSString stringWithFormat:@"%@://%@/", url.scheme, url.host];

    if (![urlPath hasPrefix:kUrlTopUp]) {
        return -1;
    }
    
    int apiId = [gTopupAPIMap intForKey:apiPath defaultValue:-1];
    return apiId;
}


@end
