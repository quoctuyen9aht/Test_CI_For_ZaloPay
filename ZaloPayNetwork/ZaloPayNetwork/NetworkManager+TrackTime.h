//
//  NetworkManager+TrackTime.h
//  ZaloPayNetwork
//
//  Created by Dung Vu on 9/7/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface NetworkManagerRecorderProtocol : NSURLProtocol
@end

@interface NSURLRequest (Extension)
@property (nonatomic, strong) NSNumber *trackId;
- (void)setDurationRequest:(NSTimeInterval)duration;
- (NSTimeInterval)getTimeDurationRequest;
@end

@interface NSURLSessionTask (NetworkManager)
@property (nonatomic, strong) NSNumber *requestEventId;
@end
