//
//  RACSignal+RetryWithFilerError.h
//  ZaloPayNetwork
//
//  Created by bonnpv on 5/17/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <ReactiveObjC/ReactiveObjC.h>
typedef  BOOL (^ZPErrorFilter)(NSError *error);

@interface RACSignal (RetryFilter)
- (RACSignal*)retry:(int)count filter:(ZPErrorFilter)filter;
@end
