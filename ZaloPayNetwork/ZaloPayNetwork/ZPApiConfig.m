//
//  ZPApiConfig.m
//  ZaloPayNetwork
//
//  Created by bonnpv on 5/10/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPApiConfig.h"
#import <ZaloPayCommon/NSCollection+JSON.h>
#import <ZaloPayCommon/ZPConstants.h>

@implementation ZPApiConfig
- (id)init {
    self =  [super init];
    if (self) {
        self.usingConnector = true;
    }
    return self;
}

- (void)updateConfigFromJson:(NSDictionary *)config {
    NSDictionary *dicAPI = [config dictionaryForKey:@"api"];
    if (dicAPI == nil) {
        return;
    }
    
    BOOL usingConnector = [[dicAPI stringForKey:@"api_route"] isEqualToString:@"connector"];
    NSArray *apiPath = [dicAPI arrayForKey:@"api_names"];
    float apiTimeoutInterval = [dicAPI doubleForKey:@"timeout_interval" defaultValue: timeoutIntervalForRequest];
    NSMutableDictionary *map = [NSMutableDictionary dictionary];
    for (NSString *path in apiPath) {
        [map setObject:@(TRUE) forKey:path];
    }
    @synchronized (self) {
        self.connectorApiPath = map;
        self.usingConnector = usingConnector;
        self.timeoutInterval = apiTimeoutInterval;
    }
}

@end
