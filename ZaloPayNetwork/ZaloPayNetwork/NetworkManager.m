//
//  NetworkManager.m
//  ZaloPayNetwork
//
//  Created by Phạm Văn Bôn on 4/1/15.
//  Copyright (c) 2015 Bon. All rights reserved.
//

#import "NetworkManager.h"
#import "DDLogConfigHeader.h"
#import "ZPConnectionManager.h"
#import "ZPApiConfig.h"
#import "RACSignal+RetryFilter.h"
#import "NetworkState.h"
#import <ReactiveObjC/ReactiveObjC.h>
#import <ZaloPayCommon/NSCollection+JSON.h>
#import <ZaloPayCommon/ZPConstants.h>
#import <AFNetworking/AFNetworking.h>
#import <ZaloPayUI/ZPAppFactory.h>
#import <ZaloPayUI/ZPTrackingHelper.h>
#import "ZaloPayErrorCode.h"
#import "NetworkManager+TrackTime.h"
#import "NetworkProxyHandler.h"
#import "NSError+ZaloPayAPI.h"
#import "ZPNetWorkApi.h"
#import <ZaloPayCommon/FileUtils.h>
#import <ZaloPayAnalyticsSwift/ZaloPayAnalyticsSwift-Swift.h>

extern NSString *kUploadUrl;
extern NSString *kUploadClientLog;

#define kRequestRetry           2


#pragma mark - Network proxy initializer

static NSMutableArray<Class> *ZPNNetworkProxyClasses;
NSArray<Class> *ZPNGetNetworkProxyClasses(void)
{
    return ZPNNetworkProxyClasses;
}

/**
 * Register the given class as a network proxy handler. All handlers must be registered
 * prior to the network instance initialization.
 */
void ZPNRegisterProxyHandler(Class);
void ZPNRegisterProxyHandler(Class moduleClass)
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        ZPNNetworkProxyClasses = [NSMutableArray new];
    });
    
    if (![moduleClass conformsToProtocol:@protocol(NetworkProxyHandler)]) {
        DDLogError(@"%@ does not conform to the NetworkProxyHandler protocol", moduleClass);
        return;
    }
    
    // Register network proxy
    [ZPNNetworkProxyClasses addObject:moduleClass];
}

#pragma mark - Network Manager

@protocol  ZPHTTPSessionManagerCompleteTask<NSObject>
@required
- (void)taskDidComplete:(NSURLSessionTask *)task error:(NSError *)error;
@end


@interface ZPHTTPSessionManager : AFHTTPSessionManager
@property (nonatomic, weak) id<ZPHTTPSessionManagerCompleteTask> timingDelegate;
@end

@implementation ZPHTTPSessionManager

- (void)URLSession:(__unused NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    [self.timingDelegate taskDidComplete:task error:error];
    [super URLSession:session task:task didCompleteWithError:error];
}

@end

@interface NetworkManager () <ZPHTTPSessionManagerCompleteTask>
@property (nonatomic, strong) NSString *doimain;
@property (nonatomic, strong) ZPHTTPSessionManager *client;
@property (nonatomic, copy) PrehandleServerError prehandleError;
@property (nonatomic, copy) AccessTokeUpdateHandle accesstokeUpdateHandle;
@property (nonatomic, strong) ZPApiConfig *apiConfig;
@property (nonatomic, strong) NSMutableDictionary *allDurations;

@property (nonatomic, strong) NSArray<id<NetworkProxyHandler>> *networkProxies;
@end

@implementation NetworkManager

+ (instancetype)sharedInstance {
    static NetworkManager *instane;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instane = [[NetworkManager alloc] initWithBaseUrl:kBaseUrl];
    });
    return instane;
}

- (id)initWithBaseUrl:(NSString *)baseUrl {
    self = [super init];
    if (self) {
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        sessionConfiguration.timeoutIntervalForRequest = timeoutIntervalForRequest;
        NSURL *url = [NSURL URLWithString:baseUrl];
        self.doimain = url.host;
        // Register Tracking Protocol
        NSMutableOrderedSet *mutableClass = [NSMutableOrderedSet orderedSetWithArray:sessionConfiguration.protocolClasses];
        [mutableClass insertObject:[NetworkManagerRecorderProtocol class] atIndex:0];
        sessionConfiguration.protocolClasses = [mutableClass array];
        self.allDurations = [NSMutableDictionary new];
        
        self.client = [[ZPHTTPSessionManager alloc] initWithBaseURL:url
                                               sessionConfiguration:sessionConfiguration];
        self.client.timingDelegate = self;
        if (self.client) {
            NSMutableSet *contentType = [[NSMutableSet alloc] initWithSet:self.client.responseSerializer.acceptableContentTypes];
            [contentType addObject:@"text/plain"];
            [contentType addObject:@"text/html"];
            self.client.responseSerializer.acceptableContentTypes = contentType;
            self.client.completionQueue = dispatch_queue_create("zalopay.network.queue", DISPATCH_QUEUE_CONCURRENT);
        }
        self.apiConfig = [[ZPApiConfig alloc] init];
    }
    
    
    // Inject network proxy handlers
    NSMutableArray<id<NetworkProxyHandler>> *networkProxies = [NSMutableArray new];
    for (Class networkProxy in ZPNGetNetworkProxyClasses()) {
        id obj = [networkProxy new];
        [networkProxies addObject:obj];
    }
    
    self.networkProxies = networkProxies;
    
    // Initialize network proxy handlers
    for (id<NetworkProxyHandler> networkProxy in self.networkProxies) {
        [networkProxy initialize];
    }
    return self;
}

- (void)taskDidComplete:(NSURLSessionTask *)task error:(NSError *)error {
    if (error) {
        return;
    }
    NSInteger eventId = [task.requestEventId integerValue];
    NSURLRequest *request = task.currentRequest;
    NSTimeInterval timeRequest = [request getTimeDurationRequest];
    
#if DEBUG
    DDLogInfo(@"eventId : %d", (int)eventId);
    DDLogInfo(@"Time : %lf", timeRequest);
#endif
    if (timeRequest <= 0 || eventId <= 0) {
        return;
    }
    
    [self trackRequestWithEventId:eventId withTiming:timeRequest];
}

- (void)prehandleError:(PrehandleServerError)handle {
    self.prehandleError = handle;
}

- (void)accessTokenChangeHandle:(AccessTokeUpdateHandle)handle {
    self.accesstokeUpdateHandle = handle;
}

- (void)cancelAllRequest {
    [self.client.operationQueue cancelAllOperations];
}

- (void)configConnectorApi:(NSDictionary *)json {
    [self.apiConfig updateConfigFromJson:json];
    self.client.session.configuration.timeoutIntervalForRequest = [self getNetworkTimeoutInterval];
}

#pragma mark - Public Function

- (NSDictionary *)requestParamWithParam:(NSDictionary *)params {
    NSMutableDictionary *requestParams = [NSMutableDictionary dictionary];
    if (params) {
        [requestParams addEntriesFromDictionary:params];
    }
    NSString *appVersion = [NSString stringWithFormat:@"%@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
    
    [requestParams setObjectCheckNil:appVersion forKey:@"appversion"];
    [requestParams setObjectCheckNil:[NetworkManager sharedInstance].accesstoken forKey:@"accesstoken"];
    [requestParams setObjectCheckNil:[NetworkManager sharedInstance].paymentUserId forKey:@"userid"];
    [requestParams setObjectCheckNil:@"ios" forKey:@"platform"];
    return requestParams;
}

- (RACSignal *)requestWithPath:(NSString *)path parameters:(NSDictionary *)params {
    return [[[self creatRequestWithPath:path
                                 params:params
                                  isGet:true] retry:kRequestRetry
                                              filter:^BOOL(NSError *error) {
                                                  return [error isRequestTimeout] && [NetworkState sharedInstance].isReachable;
                                    }] replayLazily];
}

- (RACSignal *)postRequestWithPath:(NSString *)path parameters:(NSDictionary *)params {
    return [[self creatRequestWithPath:path
                                params:params
                                 isGet:false] replayLazily];
}

- (void)logError:(NSError *)error request:(NSURLRequest *)request {
    if ([error isNetworkConnectionError] || [error isRequestTimeout]) {
        return;
    }
    DDLogInfo(@"error [%@] in request [%@]",error,request.URL);
}

- (void)trackRequestWithEventId:(NSInteger)eventId withTiming:(double)requestTime {
    if (requestTime >= [self getNetworkTimeoutInterval] * 1000) {
        DDLogInfo(@"error request duration > timeout");
        return;
    }
    [[ZPTrackingHelper shared].eventTracker trackTiming:eventId value:@(requestTime)];
}

- (NSInteger)httpCodeFromError:(NSError *)error {
    return [error httpErrorCode];    
}

- (void)trackApiErrorHttpCode:(NSInteger)httpCode
                  networkCode:(NSInteger)networkCode
                   serverCode:(NSInteger)serverCode
                      apiPath:(NSString *)path {
    
    if (path.length == 0) {
        path = @"";
    }
    uint64_t timestamp = [[NSDate date] timeIntervalSince1970] * 1000;
    NSDictionary *log = @{@"api_name": path,
                          @"http_code": @(httpCode),
                          @"server_code": @(serverCode),
                          @"network_code": @(networkCode),
                          @"timestamp": @(timestamp)};
    [[ZPAppFactory sharedInstance] trackApiFail:log];
}

- (BOOL)isIgnoreErrorHandle:(NSString *)path {
    if (path.length == 0) {
        return NO;
    }
    static NSDictionary *ignoreMaps;
    if (!ignoreMaps) {
        ignoreMaps = @{api_v001_tpe_sdkwriteatmtime : @(TRUE),
                       api_v001_tpe_sdkerrorreport : @(TRUE),
                       api_v001_zp_upload_clientlogs: @(TRUE)
                       };
    }
    return [[ignoreMaps objectForKey:path] boolValue];
}

- (NSError *)preHandleResult:(NSDictionary *)responseData
                       error:(NSError *)error
                  subscriber:(id<RACSubscriber>)subscriber
                        path:(NSString *)path
                      params:(NSDictionary *)params {
    for (id<NetworkProxyHandler> networkProxy in self.networkProxies) {
        if (![networkProxy respondsToSelector:@selector(preHandleResult:error:path:params:)]) {
            continue;
        }
        
        [networkProxy preHandleResult:responseData error:error path:path params:params];
    }
    NSError *e = [self trackResult:responseData error:error path:path];
    e ? [subscriber sendError:e] : ^{
        [subscriber sendNext:responseData];
        [subscriber sendCompleted];
    }();
    
    return e;
}

- (NSError *)trackResult:(NSDictionary *)responseData
                   error:(NSError *)error
                    path:(NSString *)path {
    if (error) {
        return error;
    }
    
    if (![responseData isKindOfClass:[NSDictionary class]] || responseData.count == 0) {
        NSError *dataError = [NSError errorWithDomain:@"ResponseDataError" code:-1 userInfo:nil];
        return dataError;
    }
    
    BOOL isNeedCheckAccessToken = ![path isEqualToString:api_um_removeaccesstoken];
    if (!isNeedCheckAccessToken) {
        // Don't need check for api_um_removeaccesstoken
        return nil;
    }
    
    NSString *accesstoken = [responseData stringForKey:@"accesstoken"];
    if (accesstoken.length && ![accesstoken isEqualToString:self.accesstoken]) {
        self.accesstoken = accesstoken;
        if (self.accesstokeUpdateHandle) {
            self.accesstokeUpdateHandle(accesstoken);
        }
    }
    
    int errorCode = [responseData intForKey:@"returncode"];
    if (errorCode != ZALOPAY_ERRORCODE_SUCCESSFUL) {
        NSError *returnError = [NSError errorFromDic:responseData];
        
        if (self.prehandleError && [self isIgnoreErrorHandle:path] == false) {
            self.prehandleError(errorCode,[returnError apiErrorMessage]);
        }
        return returnError;
    }
    return nil;
}

- (BOOL)isConnectorRequest:(NSString *)apiPath {    
    return apiPath.length > 0 &&
           [ZPConnectionManager sharedInstance].isAuthenSuccess &&
           self.apiConfig.usingConnector &&
           [self.apiConfig.connectorApiPath objectForKey:apiPath];
}

- (RACSignal *)creatRequestWithPath:(NSString *)path
                             params:(NSDictionary *)params
                              isGet:(BOOL)isGet {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSDictionary *reqestParam = [self requestParamWithParam:params];
        for (id<NetworkProxyHandler> networkProxy in self.networkProxies) {
            if (![networkProxy respondsToSelector:@selector(handleRequestWithPath:params:subscribe:)]) {
                continue;
            }
            
            if ([networkProxy handleRequestWithPath:path params:params subscribe:subscriber]) {
                return nil;
            }
        }

        if ([self isConnectorRequest:path]) {
            [self socketRequestWithPath:path params:reqestParam subscribe:subscriber];
        } else if (isGet) {
             [[self getWithPath:path params:reqestParam] subscribe:subscriber];
        } else {
             [[self postWithPath:path params:reqestParam] subscribe:subscriber];
        }
        return nil;
    }];
}


#pragma mark - Socket request

- (void)socketRequestWithPath:(NSString *)path params:(NSDictionary *)params subscribe:(id<RACSubscriber>)subscriber {
    [[[ZPConnectionManager sharedInstance] requestWithDomain:self.doimain
                                                        path:path
                                                        data:params] subscribeNext:^(NSDictionary *json) {
        NSError *error = [self preHandleResult:json error:nil subscriber:subscriber path:path params:params];
        if (error) {
            [self trackApiErrorHttpCode:0
                            networkCode:0
                             serverCode:error.code
                                apiPath:path];
        }
    } error:^(NSError *error) {
        [self trackApiErrorHttpCode:0
                        networkCode:0
                         serverCode:error.code
                            apiPath:path];
        [self preHandleResult:nil error:error subscriber:subscriber path:path params:params];
    }];
}


#pragma mark - http Get

- (RACSignal *)getWithPath:(NSString *)path params:(NSDictionary *)requestParams {
    @weakify(self);
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self);
        NSURLSessionDataTask *requestTask = [self.client GET:path
                                                  parameters:requestParams
                                                    progress:nil
                                                     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                                         DDLogInfo(@"<-- %@", task.currentRequest.URL);
                                                         DDLogInfo(@"<-- %@", responseObject);
                                                         NSError *error = [self preHandleResult:responseObject
                                                                                          error:nil
                                                                                     subscriber:subscriber
                                                                                           path:path
                                                                                         params:requestParams];
                                                         if (error) {
                                                             [self logError:error request:task.currentRequest];
                                                             [self trackApiErrorHttpCode:0
                                                                             networkCode:0
                                                                              serverCode:error.code
                                                                                 apiPath:path];
                                                         }
                                                     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                                         [self logError:error request:task.currentRequest];
                                                         NSInteger httpCode = [self httpCodeFromError:error];
                                                         [self trackApiErrorHttpCode:httpCode
                                                                         networkCode:error.code
                                                                          serverCode:0
                                                                             apiPath:path];

                                                         [self preHandleResult:nil
                                                                         error:error
                                                                    subscriber:subscriber
                                                                          path:path
                                                                        params:requestParams];
                                                     }];
        requestTask.requestEventId = @(requestEventIdFromApi(path));
        return [RACDisposable disposableWithBlock:^{
            [requestTask cancel];
        }];
    }];
}

#pragma mark - http Post

- (RACSignal *)postWithPath:(NSString *)path params:(NSDictionary *)requestParams {
    @weakify(self);
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self);
        NSURLSessionDataTask *requestTask = [self.client POST:path
                                                   parameters:requestParams
                                                     progress:nil
                                                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                                          DDLogInfo(@"<-- %@", task.currentRequest.URL);
                                                          DDLogInfo(@"<-- %@", responseObject);
                                                          NSError *error = [self preHandleResult:responseObject
                                                                                           error:nil
                                                                                      subscriber:subscriber
                                                                                            path:path
                                                                                          params:requestParams];
                                                          if (error) {
                                                              [self logError:error request:task.currentRequest];
                                                              [self trackApiErrorHttpCode:0
                                                                              networkCode:0
                                                                               serverCode:error.code
                                                                                  apiPath:path];
                                                          }
                                                      } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                                          DDLogInfo(@"<-- %@", task.currentRequest.URL);
                                                          DDLogInfo(@"<-- error : %@", error);
                                                          [self logError:error request:task.currentRequest];
                                                          NSInteger httpCode = [self httpCodeFromError:error];
                                                          [self trackApiErrorHttpCode:httpCode
                                                                          networkCode:error.code
                                                                           serverCode:0
                                                                              apiPath:path];
                                                          [self preHandleResult:nil
                                                                          error:error
                                                                     subscriber:subscriber
                                                                           path:path
                                                                         params:requestParams];
                                                      }];
        
        DDLogInfo(@" --> %@", requestTask.originalRequest.URL);
        DDLogInfo(@" --> %@", requestParams);
        requestTask.requestEventId = @(requestEventIdFromApi(path));
        return [RACDisposable disposableWithBlock:^{
            [requestTask cancel];
        }];
    }];
}

#pragma mark - Upload File

- (RACSignal *)uploadWithPath:(NSString *)path
                        param:(NSDictionary *)params
                    formBlock:(void (^)(id <AFMultipartFormData> formData))block {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSDictionary *requestParam = [self requestParamWithParam:params];
        NSURL *url = [NSURL URLWithString:path relativeToURL:[NSURL URLWithString:kUploadUrl]];
        NSString *urlString = url.absoluteString;
        DDLogInfo(@"upload url string :%@",urlString);
        NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST"
                                                                                                  URLString:urlString
                                                                                                 parameters:requestParam constructingBodyWithBlock:block
                                                                                                      error:nil];
        request.timeoutInterval = [self getNetworkTimeoutInterval];
        DDLogInfo(@"--> %@",request.URL);
        NSURLSessionUploadTask *uploadTask = [self.client
                                              uploadTaskWithStreamedRequest:request
                                              progress:nil
                                              completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                                                  if (error) {
                                                      [self logError:error request:request];
                                                  }
                                                  [self preHandleResult:responseObject
                                                                  error:error
                                                             subscriber:subscriber
                                                                   path:path
                                                                 params:params];
                                              }];
        uploadTask.requestEventId = @(requestEventIdFromApi(path));
        [uploadTask resume];
        return nil;
    }];
}

#pragma mark - Download File

- (NSURLSessionDownloadTask* )downloadFileWithUrl:(NSString *)urlString
                                   saveToFilePath:(NSString *)savePath
                                         progress:(DowloadProgressBlock)progress
                                         complete:(DowloadCompleteBlock)complete {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [request setValue:@"application/zip" forHTTPHeaderField:@"Content-Type"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.completionQueue = dispatch_queue_create("zalopay.network.download.queue", DISPATCH_QUEUE_CONCURRENT);
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request
                                                                     progress:^(NSProgress * _Nonnull downloadProgress) {
                                                                         if (progress) {
                                                                             progress(downloadProgress.fractionCompleted);
                                                                         }
                                                                     }
                                                                  destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
                                                                      return [NSURL fileURLWithPath:savePath];
                                                                  } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
                                                                      if (complete) {
                                                                          complete([filePath resourceSpecifier], error);
                                                                      }
                                                                  }];
    [downloadTask resume];
    return downloadTask;
}

#pragma mark - Upload log
- (RACSignal *)uploadClientLogWithPath:(NSString *)path
                                 param:(NSDictionary *)params
                             formBlock:(void (^)(id <AFMultipartFormData> formData))block
{
    
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSDictionary *requestParam = [self requestParamWithParam:params];
        NSURL *url = [NSURL URLWithString:path relativeToURL:[NSURL URLWithString:kUploadClientLog]];
        NSString *urlString = url.absoluteString;
        for (id<NetworkProxyHandler> proxyHandler in self.networkProxies) {
            if (![proxyHandler respondsToSelector:@selector(handleRequestWithPath:params:subscribe:)]) {
                continue;
            }
            
            if ([proxyHandler handleRequestWithPath:urlString params:requestParam subscribe:subscriber]) {
                return nil;
            }
        }
        
        DDLogInfo(@"upload url string :%@",urlString);
        NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST"
                                                                                                  URLString:urlString
                                                                                                 parameters:requestParam constructingBodyWithBlock:block
                                                                                                      error:nil];
        DDLogInfo(@"--> %@",request.URL);
        NSURLSessionUploadTask *uploadTask = [self.client
                                              uploadTaskWithStreamedRequest:request
                                              progress:nil
                                              completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                                                  if (error) {
                                                      DDLogInfo(@"upload error: %@",error);
                                                      [self logError:error request:request];
                                                  }
                                                  DDLogInfo(@"upload result: %@",responseObject);
                                                  [self preHandleResult:responseObject
                                                                  error:error
                                                             subscriber:subscriber
                                                                   path:path
                                                                 params:params];
                                              }];
        uploadTask.requestEventId = @(requestEventIdFromApi(path));
        [uploadTask resume];
        return nil;
    }];
}

#pragma mark - Request With User Agent

- (RACSignal *)requestWithUrlString:(NSString *)urlString userAgent:(NSString *)userAgent {
    return [[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSURLSessionConfiguration *defaultConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        defaultConfiguration.timeoutIntervalForRequest = [self getNetworkTimeoutInterval];
        
        NSURLSession *session = [NSURLSession sessionWithConfiguration:defaultConfiguration];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: urlString]];
        if (userAgent) {
            [request setValue:userAgent forHTTPHeaderField:@"User-Agent"];
        }
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            if (data) {
                NSString *stringData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSDictionary *json = [stringData JSONValue];
                if ([json isKindOfClass:[NSDictionary class]]) {
                    [subscriber sendNext:json];
                    [subscriber sendCompleted];
                    return;
                }
            }
            [subscriber sendError:error];
        }];
        [task resume];
        return [RACDisposable disposableWithBlock:^{
            [task cancel];
        }];
    }] replayLazily];
}

- (RACSignal *)networkSignal {
    return [NetworkState sharedInstance].networkSignal;
}

- (BOOL)isReachable {
    return [NetworkState sharedInstance].isReachable;
}

- (NSString*)connectionType {
    return [NetworkState sharedInstance].connectionType;
}

- (float)getNetworkTimeoutInterval {
    return self.apiConfig.timeoutInterval > 0 ? self.apiConfig.timeoutInterval : timeoutIntervalForRequest;
}

- (RACSignal *)downloadZipFileWithUrl:(NSString *)urlString
                               toPath:(NSString *)savePath
                          attractPath:(NSString *)attractPath
                             progress:(DowloadProgressBlock)progress {
    return [[[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        DDLogInfo(@"downloadUrl :%@",urlString);
        NSURLSessionDownloadTask *task = [[NetworkManager sharedInstance] downloadFileWithUrl:urlString
                                                                               saveToFilePath:savePath
                                                                                     progress:progress
                                                                                     complete:^(NSString *filePath, NSError *error) {
                                                                                         if (error) {
                                                                                             DDLogInfo(@"done download with error :%@",error);
                                                                                             [subscriber sendError:error];
                                                                                             return;
                                                                                         }
                                                                                         BOOL success = [FileUtils unzipFileAtPath:savePath
                                                                                                                      toFolderPath:attractPath
                                                                                                                     removeZipFile:YES];
                                                                                         
                                                                                         if (success) {
                                                                                             [subscriber sendCompleted];
                                                                                             return;
                                                                                         }
                                                                                         [subscriber sendError:nil];
                                                                                     }];
        return [RACDisposable disposableWithBlock:^{
            if (task.state == NSURLSessionTaskStateCompleted) {
                return;
            }
            [task cancel];
        }];
    }] deliverOn:[RACScheduler scheduler]] replayLazily];
    
    
}
@end


