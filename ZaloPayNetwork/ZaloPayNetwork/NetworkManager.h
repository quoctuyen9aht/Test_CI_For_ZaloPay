//
//  NetworkManager.h
//
//
//  Created by Phạm Văn Bôn on 4/1/15.
//  Copyright (c) 2015 Bon. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <ZaloPayModuleProtocols/ZaloPayNetworkProtocol.h>
//
//@protocol AFMultipartFormData;
//@protocol RACSubscriber;
//@protocol ZaloPayNetworkProtocol;
//@class RACSignal;
//
//typedef  void (^DowloadCompleteBlock)(NSString *filePath, NSError *error);
//typedef  void (^DowloadProgressBlock)(float progress);
//typedef  void (^PrehandleServerError)(int errorCode, NSString *message);
//typedef  void (^AccessTokeUpdateHandle)(NSString *accesstoken);

extern NSString *kBaseUrl;

@interface NetworkManager : NSObject <ZaloPayNetworkProtocol>
@property (nonatomic, strong) NSString *accesstoken;
@property (nonatomic, strong) NSString *paymentUserId;
+ (instancetype)sharedInstance;
- (void)configConnectorApi:(NSDictionary *)json;
- (void)cancelAllRequest;
- (id)initWithBaseUrl:(NSString *)baseUrl;
- (NSDictionary *)requestParamWithParam:(NSDictionary *)params;
- (void)prehandleError:(PrehandleServerError)handle;
- (void)accessTokenChangeHandle:(AccessTokeUpdateHandle)handle;
- (NSError *)preHandleResult:(NSDictionary *)responseData
                       error:(NSError *)error
                  subscriber:(id<RACSubscriber>)subscriber
                        path:(NSString *)path
                      params:(NSDictionary *)params;

- (RACSignal *)requestWithPath:(NSString *)path
                    parameters:(NSDictionary *)params;

- (RACSignal *)postRequestWithPath:(NSString *)path
                        parameters:(NSDictionary *)params;

- (NSURLSessionDownloadTask* )downloadFileWithUrl:(NSString *)urlString
                                   saveToFilePath:(NSString *)savePath
                                         progress:(DowloadProgressBlock)progress
                                         complete:(DowloadCompleteBlock)complete;
- (RACSignal *)uploadWithPath:(NSString *)path
                        param:(NSDictionary *)params
                    formBlock:(void (^)(id <AFMultipartFormData> formData))block;
- (RACSignal *)uploadClientLogWithPath:(NSString *)path
                         param:(NSDictionary *)params
                     formBlock:(void (^)(id <AFMultipartFormData> formData))block;

- (RACSignal *)requestWithUrlString:(NSString *)urlString userAgent:(NSString *)userAgent;

- (RACSignal *)creatRequestWithPath:(NSString *)path
                             params:(NSDictionary *)params
                              isGet:(BOOL)isGet;

- (void)trackRequestWithEventId:(NSInteger)eventId withTiming:(double)requestTime;
- (float)getNetworkTimeoutInterval;
- (NSString*)connectionType;
- (BOOL)isReachable;
- (RACSignal *)downloadZipFileWithUrl:(NSString *)urlString
                               toPath:(NSString *)savePath
                          attractPath:(NSString *)attractPath
                             progress:(DowloadProgressBlock)progress;
@end


