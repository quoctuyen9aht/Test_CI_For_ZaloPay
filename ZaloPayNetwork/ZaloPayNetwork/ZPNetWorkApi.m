//
//  ZPNetWorkApi.m
//  ZaloPayNetwork
//
//  Created by bonnpv on 3/22/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPNetWorkApi.h"
#import <ZaloPayAnalyticsSwift/ZaloPayAnalyticsSwift-Swift.h>
#import <UIKit/UIKit.h>
#import <ZaloPayCommon/NSCollection+JSON.h>

extern NSString *kUrlVoucher;
extern NSString *kUrlFilter;

NSString* badwordUrlFromPath(NSString *path) {
    return [NSString stringWithFormat:@"%@/%@", kUrlFilter, path];
}

NSDictionary* createApiEventMap () {
    NSString *revertvoucher = [kUrlVoucher stringByAppendingString:api_revertvoucher];
    NSString *getvoucherstatus = [kUrlVoucher stringByAppendingString:api_getvoucherstatus];
    NSString *usevoucher = [kUrlVoucher stringByAppendingString:api_usevoucher];
    NSString *getusevoucherstatus = [kUrlVoucher stringByAppendingString:api_getusevoucherstatus];
    NSString *getrevertvoucherstatus = [kUrlVoucher stringByAppendingString:api_getrevertvoucherstatus];
    NSString *getvoucherhistory = [kUrlVoucher stringByAppendingString:api_getvoucherhistory];
    NSString *badword = badwordUrlFromPath(api_inspector_v1_inspect);
    return  @{
              api_um_createaccesstoken : @(ZPAnalyticEventActionApi_um_createaccesstoken),
              api_um_removeaccesstoken :@(ZPAnalyticEventActionApi_um_removeaccesstoken),
              api_um_verifycodetest :@(ZPAnalyticEventActionApi_um_verifycodetest),
              api_v001_tpe_getorderinfo :@(ZPAnalyticEventActionApi_v001_tpe_getorderinfo),
              api_v001_tpe_createorder :@(ZPAnalyticEventActionApi_v001_tpe_createwalletorder),
              api_v001_tpe_createwalletorder :@(ZPAnalyticEventActionApi_v001_tpe_createwalletorder),
              api_um_getuserinfo :@(ZPAnalyticEventActionApi_um_getUserinfo),
              api_um_checklistzaloidforclient :@(ZPAnalyticEventActionApi_um_checklistzaloidforclient),
              api_um_updateprofile :@(ZPAnalyticEventActionApi_um_updateprofile),
              api_um_verifyotpprofile :@(ZPAnalyticEventActionApi_um_verifyotpprofile),
              api_um_recoverypin :@(ZPAnalyticEventActionApi_um_recoverypin),
              api_umupload_preupdateprofilelevel3 :@(ZPAnalyticEventActionApi_umupload_preupdateprofilelevel3),
              api_um_getuserinfobyzalopayid :@(ZPAnalyticEventActionApi_um_getUserinfobyzalopayid),
              //                api_um_verifyaccesstoken :@(ZPAnalyticEventActionApi_um_verifyaccesstoken),
              api_um_sendnotification :@(ZPAnalyticEventActionApi_um_sendnotification),
              api_um_checkzalopaynameexist :@(ZPAnalyticEventActionApi_um_checkzalopaynameexist),
              api_um_updatezalopayname :@(ZPAnalyticEventActionApi_um_updatezalopayname),
              api_um_getuserinfobyzalopayname :@(ZPAnalyticEventActionApi_um_getUserinfobyzalopayname),
              api_um_validatepin :@(ZPAnalyticEventActionApi_um_validatepin),
              api_v001_tpe_getbalance :@(ZPAnalyticEventActionApi_v001_tpe_getbalance),
              api_um_listcardinfo :@(ZPAnalyticEventActionApi_um_listcardinfoforclient),
              api_um_getuserprofilelevel :@(ZPAnalyticEventActionApi_um_getUserprofilelevel),
              api_v001_tpe_getinsideappresource :@(ZPAnalyticEventActionApi_v001_tpe_getinsideappresource),
              api_v001_tpe_transhistory :@(ZPAnalyticEventActionApi_v001_tpe_transhistory),
              api_ummerchant_getmerchantuserinfo :@(ZPAnalyticEventActionApi_ummerchant_getmerchantUserinfo),
              api_ummerchant_getlistmerchantuserinfo :@(ZPAnalyticEventActionApi_ummerchant_getlistmerchantUserinfo),
              api_v001_tpe_gettransstatus :@(ZPAnalyticEventActionApi_v001_tpe_gettransstatus),
              api_redpackage_createbundleorder :@(ZPAnalyticEventActionApi_redpackage_createbundleorder),
              api_redpackage_submittosendbundle :@(ZPAnalyticEventActionApi_redpackage_submittosendbundle),
              api_redpackage_submittosendbundlebyzalopayinfo :@(ZPAnalyticEventActionApi_redpackage_submittosendbundlebyzalopayinfo),
              api_redpackage_submitopenpackage :@(ZPAnalyticEventActionApi_redpackage_submitopenpackage),
              api_redpackage_getsentbundlelist :@(ZPAnalyticEventActionApi_redpackage_getsentbundlelist),
              api_redpackage_getrevpackagelist :@(ZPAnalyticEventActionApi_redpackage_getrevpackagelist),
              api_redpackage_getpackagesinbundle :@(ZPAnalyticEventActionApi_redpackage_getpackagesinbundle),
              api_redpackage_getappinfo :@(ZPAnalyticEventActionApi_redpackage_getappinfo),
              //                api_redpackage_getbundlestatus :@(ZPAnalyticEventActionApi_redpackage_getbundlestatus),
              api_redpackage_getlistpackagestatus :@(ZPAnalyticEventActionApi_redpackage_getlistpackagestatus),
              api_v001_tpe_v001getplatforminfo :@(ZPAnalyticEventActionApi_v001_tpe_v001getplatforminfo),
              api_v001_tpe_getappinfo :@(ZPAnalyticEventActionApi_v001_tpe_getappinfo),
              api_v001_tpe_getbanklist :@(ZPAnalyticEventActionApi_v001_tpe_getbanklist),
              api_v001_tpe_submittrans : @(ZPAnalyticEventActionApi_v001_tpe_submittrans),
              api_v001_tpe_atmauthenpayer:@(ZPAnalyticEventActionApi_v001_tpe_atmauthenpayer),
              api_v001_tpe_authcardholderformapping:@(ZPAnalyticEventActionApi_v001_tpe_authcardholderformapping),
              api_v001_tpe_getstatusmapcard:@(ZPAnalyticEventActionApi_v001_tpe_getstatusmapcard),
              api_v001_tpe_verifycardformapping:@(ZPAnalyticEventActionApi_v001_tpe_verifycardformapping),
              api_v001_tpe_getstatusbyapptransidforclient:@(ZPAnalyticEventActionApi_v001_tpe_getstatusbyapptransidforclient),
              api_v001_tpe_sdkwriteatmtime:@(ZPAnalyticEventActionApi_v001_tpe_sdkwriteatmtime),
              api_v001_tpe_removemapcard:@(ZPAnalyticEventActionApi_v001_tpe_removemapcard),
              api_v001_tpe_sdkerrorreport:@(ZPAnalyticEventActionApi_v001_tpe_sdkerrorreport),
              api_v001_tpe_registeropandlink:@(ZPAnalyticEventActionApi_v001_tpe_registeropandlink),
              api_um_listcardinfoforclient:@(ZPAnalyticEventActionApi_um_listcardinfoforclient),
              api_v001_tpe_submitmapaccount:@(ZPAnalyticEventActionApi_v001_tpe_submitmapaccount),
              api_v001_zp_upload_clientlogs:@(ZPAnalyticEventActionApi_v001_zp_upload_clientlogs),
              api_um_loginviazalo:@(ZPAnalyticEventActionApi_um_loginviazalo),
              api_um_registerphonenumber:@(ZPAnalyticEventActionApi_um_registerphonenumber),
              api_um_authenphonenumber:@(ZPAnalyticEventActionApi_um_authenphonenumber),
              api_um_getuserinfobyphone:@(ZPAnalyticEventActionApi_zpc_getuserinfobyphone),
              api_um_listusersbyphoneforclient: @(ZPAnalyticEventActionApi_um_listusersbyphoneforclient),
              getvoucherstatus:@(ZPAnalyticEventActionApi_get_voucher_status),
              usevoucher:@(ZPAnalyticEventActionApi_use_voucher),
              revertvoucher:@(ZPAnalyticEventActionApi_revert_voucher),
              getusevoucherstatus: @(ZPAnalyticEventActionApi_getusevoucherstatus),
              getrevertvoucherstatus: @(ZPAnalyticEventActionApi_getrevertvoucherstatus),
              getvoucherhistory: @(ZPAnalyticEventActionApi_get_voucher_history),
              api_v001_tpe_qr_getpaymentcode:@(ZPAnalyticEventActionApi_v001_tpe_getlistpaymentcodes),
              api_v001_tpe_qr_verifypinqrcodepay:@(ZPAnalyticEventActionApi_v001_tpe_qrcodepayverifypin),
              api_um_listusersbyphoneforclientasync: @(ZPAnalyticEventActionApi_um_listusersbyphoneforclientasync),
              api_um_getrequeststatusforclient: @(ZPAnalyticEventActionApi_um_getrequeststatusforclient),
              badword: @(ZPAnalyticEventActionApi_filter_content),
              api_v001_tpe_verifyaccountformapping: @(ZPAnalyticEventActionApi_v001_tpe_verifyaccountformapping),
              api_v001_tpe_getstatusmapaccount: @(ZPAnalyticEventActionApi_v001_tpe_getstatusmapaccount),
              api_v001_tpe_removemapaccount: @(ZPAnalyticEventActionApi_v001_tpe_removemapaccount),
              api_um_getuserprofilesimpleinfo: @(ZPAnalyticEventActionApi_um_getuserprofilesimpleinfo)
              };
    
}

NSDictionary *createConnectorApiEventMap  () {
    return@{
            api_um_createaccesstoken : @(ZPAnalyticEventActionConnector_um_createaccesstoken),
            api_um_removeaccesstoken :@(ZPAnalyticEventActionConnector_um_removeaccesstoken),
            api_um_verifycodetest :@(ZPAnalyticEventActionConnector_um_verifycodetest),
            api_v001_tpe_getorderinfo :@(ZPAnalyticEventActionConnector_v001_tpe_getorderinfo),
            api_v001_tpe_createorder :@(ZPAnalyticEventActionConnector_v001_tpe_createwalletorder),
            api_v001_tpe_createwalletorder :@(ZPAnalyticEventActionConnector_v001_tpe_createwalletorder),
            api_um_getuserinfo :@(ZPAnalyticEventActionConnector_um_getUserinfo),
            api_um_checklistzaloidforclient :@(ZPAnalyticEventActionConnector_um_checklistzaloidforclient),
            api_um_updateprofile :@(ZPAnalyticEventActionConnector_um_updateprofile),
            api_um_verifyotpprofile :@(ZPAnalyticEventActionConnector_um_verifyotpprofile),
            api_um_recoverypin :@(ZPAnalyticEventActionConnector_um_recoverypin),
            api_umupload_preupdateprofilelevel3 :@(ZPAnalyticEventActionConnector_umupload_preupdateprofilelevel3),
            api_um_getuserinfobyzalopayid :@(ZPAnalyticEventActionConnector_um_getUserinfobyzalopayid),
            //                api_um_verifyaccesstoken :@(ZPAnalyticEventActionConnector_um_verifyaccesstoken),
            api_um_sendnotification :@(ZPAnalyticEventActionConnector_um_sendnotification),
            api_um_checkzalopaynameexist :@(ZPAnalyticEventActionConnector_um_checkzalopaynameexist),
            api_um_updatezalopayname :@(ZPAnalyticEventActionConnector_um_updatezalopayname),
            api_um_getuserinfobyzalopayname :@(ZPAnalyticEventActionConnector_um_getUserinfobyzalopayname),
            api_um_validatepin :@(ZPAnalyticEventActionConnector_um_validatepin),
            api_v001_tpe_getbalance :@(ZPAnalyticEventActionConnector_v001_tpe_getbalance),
            api_um_listcardinfo :@(ZPAnalyticEventActionConnector_um_listcardinfoforclient),
            api_um_getuserprofilelevel :@(ZPAnalyticEventActionConnector_um_getUserprofilelevel),
            api_v001_tpe_getinsideappresource :@(ZPAnalyticEventActionConnector_v001_tpe_getinsideappresource),
            api_v001_tpe_transhistory :@(ZPAnalyticEventActionConnector_v001_tpe_transhistory),
            api_ummerchant_getmerchantuserinfo :@(ZPAnalyticEventActionConnector_ummerchant_getmerchantUserinfo),
            api_ummerchant_getlistmerchantuserinfo :@(ZPAnalyticEventActionConnector_ummerchant_getlistmerchantUserinfo),
            api_v001_tpe_gettransstatus :@(ZPAnalyticEventActionConnector_v001_tpe_gettransstatus),
            api_redpackage_createbundleorder :@(ZPAnalyticEventActionConnector_redpackage_createbundleorder),
            api_redpackage_submittosendbundle :@(ZPAnalyticEventActionConnector_redpackage_submittosendbundle),
            api_redpackage_submittosendbundlebyzalopayinfo :@(ZPAnalyticEventActionConnector_redpackage_submittosendbundlebyzalopayinfo),
            api_redpackage_submitopenpackage :@(ZPAnalyticEventActionConnector_redpackage_submitopenpackage),
            api_redpackage_getsentbundlelist :@(ZPAnalyticEventActionConnector_redpackage_getsentbundlelist),
            api_redpackage_getrevpackagelist :@(ZPAnalyticEventActionConnector_redpackage_getrevpackagelist),
            api_redpackage_getpackagesinbundle :@(ZPAnalyticEventActionConnector_redpackage_getpackagesinbundle),
            api_redpackage_getappinfo :@(ZPAnalyticEventActionConnector_redpackage_getappinfo),
            //                api_redpackage_getbundlestatus :@(ZPAnalyticEventActionConnector_redpackage_getbundlestatus),
            api_redpackage_getlistpackagestatus :@(ZPAnalyticEventActionConnector_redpackage_getlistpackagestatus),
            api_v001_tpe_v001getplatforminfo :@(ZPAnalyticEventActionConnector_v001_tpe_v001getplatforminfo),
            api_v001_tpe_getappinfo :@(ZPAnalyticEventActionConnector_v001_tpe_getappinfo),
            api_v001_tpe_getbanklist :@(ZPAnalyticEventActionConnector_v001_tpe_getbanklist),
            api_v001_tpe_submittrans : @(ZPAnalyticEventActionConnector_v001_tpe_submittrans),
            api_v001_tpe_atmauthenpayer:@(ZPAnalyticEventActionConnector_v001_tpe_atmauthenpayer),
            api_v001_tpe_authcardholderformapping:@(ZPAnalyticEventActionConnector_v001_tpe_authcardholderformapping),
            api_v001_tpe_getstatusmapcard:@(ZPAnalyticEventActionConnector_v001_tpe_getstatusmapcard),
            api_v001_tpe_verifycardformapping:@(ZPAnalyticEventActionConnector_v001_tpe_verifycardformapping),
            api_v001_tpe_getstatusbyapptransidforclient:@(ZPAnalyticEventActionConnector_v001_tpe_getstatusbyapptransidforclient),
            api_v001_tpe_sdkwriteatmtime:@(ZPAnalyticEventActionConnector_v001_tpe_sdkwriteatmtime),
            api_v001_tpe_removemapcard:@(ZPAnalyticEventActionConnector_v001_tpe_removemapcard),
            api_v001_tpe_sdkerrorreport:@(ZPAnalyticEventActionConnector_v001_tpe_sdkerrorreport),
            api_v001_tpe_registeropandlink:@(ZPAnalyticEventActionConnector_v001_tpe_registeropandlink),
            api_um_listcardinfoforclient:@(ZPAnalyticEventActionConnector_um_listcardinfoforclient),
            api_v001_tpe_submitmapaccount:@(ZPAnalyticEventActionConnector_v001_tpe_submitmapaccount),
            api_um_getuserinfobyphone:@(ZPAnalyticEventActionConnector_zpc_getuserinfobyphone),
            api_um_listusersbyphoneforclient: @(ZPAnalyticEventActionConnector_um_listusersbyphoneforclient),
            api_um_listusersbyphoneforclientasync: @(ZPAnalyticEventActionConnector_um_listusersbyphoneforclientasync),
            api_um_getrequeststatusforclient: @(ZPAnalyticEventActionConnector_um_getrequeststatusforclient),
            api_v001_tpe_verifyaccountformapping: @(ZPAnalyticEventActionConnector_v001_tpe_verifyaccountformapping),
            api_v001_tpe_getstatusmapaccount: @(ZPAnalyticEventActionConnector_v001_tpe_getstatusmapaccount),
            api_v001_tpe_removemapaccount: @(ZPAnalyticEventActionConnector_v001_tpe_removemapaccount),
            api_um_getuserprofilesimpleinfo: @(ZPAnalyticEventActionConnector_um_getuserprofilesimpleinfo)
            };
}

NSInteger requestEventIdFromApi(NSString *api) {
    static NSDictionary *dic;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dic = createApiEventMap();
    });
    return [dic intForKey:api defaultValue:0];
}

NSInteger connectorEventIdFromApi(NSString *api) {
    static NSDictionary *dic ;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dic = createConnectorApiEventMap();
    });
    return [dic intForKey:api defaultValue:0];
}

NSString *api_um_loginviazalo               = @"um/loginviazalo";
NSString *api_um_createaccesstoken          = @"um/createaccesstoken";
NSString *api_um_loginbyphone               = @"um/loginbyphone";
NSString *api_um_lockuserforclient          = @"um/lockuserforclient";
NSString *api_um_registerphonenumber        = @"um/registerphonenumber";
NSString *api_um_authenphonenumber          = @"um/authenphonenumber";
NSString *api_um_authenphonenumberv2          = @"um/authenphonenumberv2";
NSString *api_um_authenloginbyphone         = @"um/authenloginbyphone";
NSString *api_um_removeaccesstoken          = @"um/removeaccesstoken";
NSString *api_um_verifycodetest             = @"um/verifycodetest";
NSString *api_v001_tpe_getorderinfo         = @"v001/tpe/getorderinfo";
NSString *api_v001_tpe_createorder          = @"v001/tpe/createorder";
NSString *api_v001_tpe_createwalletorder    = @"v001/tpe/createwalletorder";
NSString *api_um_getuserinfo                = @"um/getuserinfo";
NSString *api_um_checklistzaloidforclient   = @"um/checklistzaloidforclient";
NSString *api_um_updateprofile              = @"um/updateprofile";
NSString *api_um_verifyotpprofile           = @"um/verifyotpprofile";
NSString *api_um_recoverypin                = @"um/recoverypin";
NSString *api_umupload_preupdateprofilelevel3 = @"umupload/preupdateprofilelevel3";

// Retrieve update-level-3 status
NSString *api_umupload_statusupdateproflvl3 = @"/umupload/statusupdateproflvl3";

NSString *api_um_getuserinfobyzalopayid     = @"um/getuserinfobyzalopayid";
NSString *api_um_getuserinfobyphone     = @"um/getuserinfobyphone";

NSString *api_um_verifyaccesstoken                  = @"um/verifyaccesstoken";
NSString *api_um_sendnotification                   = @"um/sendnotification";
NSString *api_um_checkzalopaynameexist              = @"um/checkzalopaynameexist";
NSString *api_um_updatezalopayname                  = @"um/updatezalopayname";
NSString *api_um_getuserinfobyzalopayname           = @"um/getuserinfobyzalopayname";
NSString *api_um_validatepin                        = @"um/validatepin";
NSString *api_v001_tpe_getbalance                   = @"v001/tpe/getbalance";
NSString *api_um_listcardinfo                       = @"um/listcardinfo";
NSString *api_um_getuserprofilelevel                = @"um/getuserprofilelevel";
NSString *api_um_getuserprofilesimpleinfo           = @"um/getuserprofilesimpleinfo";
NSString *api_v001_tpe_getinsideappresource         = @"v001/tpe/getinsideappresource";
NSString *api_v001_tpe_transhistory                 = @"v001/tpe/transhistory";
NSString *api_ummerchant_getmerchantuserinfo        = @"ummerchant/getmerchantuserinfo";
NSString *api_ummerchant_getlistmerchantuserinfo    = @"ummerchant/getlistmerchantuserinfo";
NSString *api_v001_tpe_gettransstatus               = @"v001/tpe/gettransstatus";
NSString *api_redpackage_createbundleorder          = @"redpackage/createbundleorder";
NSString *api_redpackage_submittosendbundle                 = @"redpackage/submittosendbundle";
NSString *api_redpackage_submittosendbundlebyzalopayinfo    = @"redpackage/submittosendbundlebyzalopayinfo";
NSString *api_redpackage_submitopenpackage                  = @"redpackage/submitopenpackage";
NSString *api_redpackage_getsentbundlelist                  = @"redpackage/getsentbundlelist";
NSString *api_redpackage_getrevpackagelist                  = @"redpackage/getrevpackagelist";
NSString *api_redpackage_getpackagesinbundle                = @"redpackage/getpackagesinbundle";
NSString *api_redpackage_getappinfo                 = @"redpackage/getappinfo";
NSString *api_redpackage_getbundlestatus            = @"redpackage/getbundlestatus";
NSString *api_redpackage_getlistpackagestatus       = @"redpackage/getlistpackagestatus";
NSString *api_v001_tpe_v001getplatforminfo          = @"v001/tpe/v001getplatforminfo";
NSString *api_v001_tpe_getappinfo                   = @"v001/tpe/getappinfov1";
NSString *api_v001_tpe_getbanklist                  = @"v001/tpe/getbanklist";
NSString *api_v001_tpe_submittrans                  = @"v001/tpe/submittrans";
NSString *api_v001_tpe_atmauthenpayer               = @"v001/tpe/atmauthenpayer";
NSString *api_v001_tpe_authcardholderformapping     =  @"v001/tpe/authcardholderformapping";
NSString *api_v001_tpe_getstatusmapcard             = @"v001/tpe/getstatusmapcard";
NSString *api_v001_tpe_verifycardformapping         = @"v001/tpe/verifycardformapping";
NSString *api_v001_tpe_getstatusbyapptransidforclient   = @"v001/tpe/getstatusbyapptransidforclient";
NSString *api_v001_tpe_sdkwriteatmtime                  = @"v001/tpe/sdkwriteatmtime";
NSString *api_v001_tpe_removemapcard    = @"v001/tpe/removemapcard";
NSString *api_v001_tpe_sdkerrorreport   = @"v001/tpe/sdkerrorreport";
NSString *api_um_listcardinfoforclient  = @"um/listcardinfoforclient";
NSString *api_v001_tpe_submitmapaccount = @"v001/tpe/submitmapaccount";
NSString *api_um_listbankaccountforclient = @"um/listbankaccountforclient";
NSString *api_v001_zp_upload_clientlogs = @"zp-upload/clientlogs";
NSString *api_getvoucherstatus = @"/getvoucherstatus";
NSString *api_getvoucherhistory = @"/getvoucherhistory";
NSString *api_usevoucher = @"/usevoucher";
NSString *api_getusevoucherstatus = @"/getusevoucherstatus";
NSString *api_getrevertvoucherstatus = @"/getrevertvoucherstatus";
NSString *api_revertvoucher = @"/revertvoucher";

// Direct discount
NSString *api_getpromotioncampaigns = @"pp/getcampaigns";
NSString *api_registerpromotion = @"pp/registerpromotion";
NSString *api_getregisterpromotionstatus = @"pp/getregisterstatus";
NSString *api_updatepromotion = @"pp/updatepromotion";
NSString *api_revertpromotion = @"pp/revertpromotion";
NSString *api_getrevertpromotionstatus = @"/pp/getrevertstatus";

NSString *api_um_listusersbyphoneforclient = @"/um/listusersbyphoneforclient";
NSString *api_v001_tpe_registeropandlink            = @"v001/tpe/registeropandlink";
NSString *api_um_getsessionloginforclient           = @"/um/getsessionloginforclient";
NSString *api_um_removesessionloginforclient        = @"/um/removesessionloginforclient";
NSString *api_um_updateuserinfo4client              = @"/um/updateuserinfo4client";
NSString *api_v001_tpe_qr_getpaymentcode = @"v001/tpe/getlistpaymentcodes";
NSString *api_v001_tpe_qr_verifypinqrcodepay = @"v001/tpe/qrcodepayverifypin";
NSString *api_um_listusersbyphoneforclientasync          = @"um/listusersbyphoneforclientasync";
NSString *api_um_getrequeststatusforclient          = @"um/getrequeststatusforclient";
NSString *api_tpe_getbanklistgateway          = @"v001/tpe/getbanklistgateway";
NSString *api_inspector_v1_inspect            = @"api/inspector/v1.0/inspect";
NSString *api_v001_tpe_verifyaccountformapping      = @"v001/tpe/verifyaccountformapping";
NSString *api_v001_tpe_getstatusmapaccount          = @"v001/tpe/getstatusmapaccount";
NSString *api_v001_tpe_removemapaccount             = @"v001/tpe/removemapaccount";
NSString *api_um_registeruserinfo          = @"um/registeruserinfo";
NSString *api_um_registerpin         = @"um/registerpin";
NSString *api_um_setkyc = @"um/setkyc";

