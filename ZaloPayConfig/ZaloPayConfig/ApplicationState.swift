//
//  ApplicationState.swift
//  ZaloPay
//
//  Created by CPU11680 on 1/22/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift
import ZaloPayCommonSwift
import ReactiveObjC

fileprivate let zalopay_config = "zalopay_config"
fileprivate let bank_config    = "bank_config"

public enum ParseError: Error {
    case other(error: Error)
    case invalidContent
    case inValidVerification
    
    public var code: Int {
        switch self {
        case .invalidContent:
            return 1
        case .inValidVerification:
            return 2
        case .other:
            return 1
        }
    }
}

@objc public enum ZPApplicationState : Int {
    case anonymous
    case authenticated
}

@objcMembers
public class ApplicationState: NSObject{
    
    public static let sharedInstance = ApplicationState()
    public var state = ZPApplicationState.anonymous
    private (set) var sdkConfig: JSON?
    private (set) var config: JSON?
    {
        didSet {
            ZPApplicationConfig.reload()
        }
    }

    @objc public func zalopayConfig() -> JSON? {
        return self.config
    }
    
    @objc public func getSDKConfig() -> JSON? {
        return self.sdkConfig
    }
    
    public func loadConfig(usingLocal: Bool, path: String) -> Observable<Void> {
        weak var weakSelf = self
        func `default`() {
            guard let this = weakSelf else {
                return
            }
            this.config = try? this.configLocalData(zalopay_config)
            this.sdkConfig = try? this.configLocalData(bank_config)
        }
        
        guard !usingLocal else {
            return Observable.just(`default`())
        }
        
        return Observable.create({ (subcriber) -> Disposable in
            guard let wSelf = weakSelf else {
                subcriber.onCompleted()
                return Disposables.create()
            }
            
            do {
                wSelf.config = try wSelf.verifyConfig(zalopay_config, path: path)
                wSelf.sdkConfig = try wSelf.configServerData(bank_config, path: path)
            } catch {
                subcriber.onError(error)
            }
            
            subcriber.onNext(())
            subcriber.onCompleted()
            return Disposables.create()
        }).do(onDispose: {
            // Check value
            guard weakSelf?.config == nil else {
                return
            }
            `default`()
        })
    }
    
    private func verifyConfig(_ name: String, path: String) throws -> JSON {
        let configPath = filePath(path, name: name)
        let content = try self.content(fromPath: configPath)
        let verify = try ConfigDataParser.verifyContent(content, withSignaturePath: path)
        
        if verify == false {
            throw ParseError.inValidVerification
        }
        
        guard let json = content.toJSON() else {
            throw ParseError.invalidContent
        }
        return json
    }
    
    private func filePath(_ path: String, name: String) -> String {
        return "\(path)/config/\(name).json"
    }
    
    func configServerData(_ fileName: String, path : String) throws -> JSON {
        let jsonPath = filePath(path, name: fileName)
        return try jsonFrom(jsonPath)
    }
    
    func configLocalData(_ fileName: String) throws -> JSON {
        let jsonPath = Bundle.main.path(forResource: fileName, ofType: "json") ?? ""
        return try jsonFrom(jsonPath)
    }

    private func jsonFrom(_ path: String) throws -> JSON {
        if FileManager.default.fileExists(atPath: path) == false {
            throw ParseError.invalidContent
        }
        let jsonString = try String(contentsOfFile: path, encoding: .utf8)
        return jsonString.toJSON() ?? [:]
    }
    
    private func content(fromPath: String) throws -> String {
        return try String(contentsOfFile: fromPath, encoding: .utf8)
    }
}




