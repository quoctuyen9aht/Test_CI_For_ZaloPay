//
//  ConfigDataParser.swift
//  ZaloPay
//
//  Created by tridm2 on 10/2/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import SwiftyRSA
import ZaloPayCommonSwift
import ReactiveObjC

@objcMembers
final class ConfigDataParser: NSObject {
    static private func getPublicKey() throws -> PublicKey {
        guard let path = Bundle.main.path(forResource: "public_key", ofType: "txt") else {
            throw NSError(domain: NSURLErrorDomain, code: NSURLErrorFileDoesNotExist, userInfo: nil)
        }
        let pemContent = try String(contentsOfFile: path, encoding: .utf8)
        return try PublicKey(pemEncoded: pemContent)
    }
    
    static private func getSignature(from path : String) throws -> Signature {
        let signPath = String(format: "%@/config/signature.txt", path)
        let sign = try String(contentsOfFile: signPath, encoding: .utf8)
        return try Signature(base64Encoded: sign)
    }
    
    static public func verifyContent(_ content: String,withSignaturePath path : String) throws -> Bool {
        let publicKey = try getPublicKey()
        let signature = try getSignature(from:path)
        let clearContent = try ClearMessage(string: content, using: .utf8)
        return try clearContent.verify(with: publicKey, signature: signature, digestType: .sha256)
    }
    
//    static public func parse(content: String, withSignaturePath path : String) -> (JSON?) {
//        guard let _ =  (try? verifyContent(content, withSignaturePath: path)) else {
//            subscriber.sendNext(ZPConfigResult(validVerification: false, config: nil, isSuccessfull: false))
//            return nil
//        }
//        return content.toJSON()
//        let data = content.data(using: .utf8) ?? Data()
//        let result = try? JSONSerialization.jsonObject(with: data, options: [])
//        let config = result as? JSON
//        if config == nil {
//            subscriber.sendNext(ZPConfigResult(validVerification: true, config: nil, isSuccessfull: false))
//        }
//        return config
//    }
}
