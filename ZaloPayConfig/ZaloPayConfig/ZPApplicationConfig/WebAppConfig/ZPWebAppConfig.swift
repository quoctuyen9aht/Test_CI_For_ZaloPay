//
//  ZPWebAppConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPWebAppConfigProtocol {
    func getAllowUrls() -> [String]
}

public class ZPWebAppConfig : ZPBaseConfigMapple {
    private var allowUrls: [String]?
    
    public override func mapping(map: Map) {
        allowUrls      <- map["allow_urls"]
    }
}

// MARK: Getter
extension ZPWebAppConfig : ZPWebAppConfigProtocol {
    public func getAllowUrls() -> [String] {
        return allowUrls ?? []
    }
}
