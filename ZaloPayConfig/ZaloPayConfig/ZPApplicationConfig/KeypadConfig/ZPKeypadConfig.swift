//
//  ZPKeypadConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/28/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPKeypadConfigProtocol {
    func getTransfer() -> Int
    func getReceive() -> Int
    func getRecharge() -> Int
}

public class ZPKeypadConfig : ZPBaseConfigMapple {
    private var transfer: Int?
    private var receive: Int?
    private var recharge: Int?
    
    public override func mapping(map: Map) {
        transfer        <- map["transfer"]
        receive         <- map["receive"]
        recharge        <- map["recharge"]
    }
}

// MARK: Getter
extension ZPKeypadConfig : ZPKeypadConfigProtocol {
    public func getTransfer() -> Int {
        return transfer ?? 1
    }
    
    public func getReceive() -> Int {
        return receive ?? 1
    }
    
    public func getRecharge() -> Int {
        return recharge ?? 1
    }
}
