//
//  ZPInternalAppSearchConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPInternalAppSearchConfigProtocol {
    func getInsideAppId() -> Int
    func getAppType() -> Int
    func getAppName() -> String
    func getIconName() -> String
    func getIconColor() -> String
    func getModuleName() -> String?
}

class ZPInternalAppSearchConfig : ZPBaseConfigMapple {
    private var insideAppId: Int?
    private var appType: Int?
    private var appName: String?
    private var iconName: String?
    private var iconColor: String?
    private var moduleName: String?
    
    override func mapping(map: Map) {
        insideAppId        <- map["inside_app_id"]
        appType            <- map["app_type"]
        appName            <- map["app_name"]
        iconName           <- map["icon_name"]
        iconColor          <- map["icon_color"]
        moduleName         <- map["module_name"]

    }
}

// MARK: Getter
extension ZPInternalAppSearchConfig : ZPInternalAppSearchConfigProtocol {
    func getInsideAppId() -> Int {
        return insideAppId ?? 1
    }
    
    func getAppType() -> Int {
        return appType ?? 0
    }
    
    func getAppName() -> String {
        return appName ?? ""
    }
    
    func getIconName() -> String {
        return iconName ?? ""
    }
    
    func getIconColor() -> String {
        return iconColor ?? ""
    }
    func getModuleName() -> String? {
        return moduleName
    }
}
