//
//  ZPSearchConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPSearchConfigProtocol {
    func getInsideApp() -> [ZPInternalAppSearchConfigProtocol]
    func getNumberSearchApp() -> Int
    func getIgnoreApps() -> [Int]
}

public class ZPSearchConfig : ZPBaseConfigMapple {
    private var insideApp: [ZPInternalAppSearchConfig]?
    private var numberSearchApp: Int?
    private var ignoreApps: [Int]?
    
    public override func mapping(map: Map) {
        insideApp              <- map["inside_app"]
        numberSearchApp        <- map["number_search_app"]
        ignoreApps             <- map["ignore_apps"]
    }
}

// MARK: Getter
extension ZPSearchConfig : ZPSearchConfigProtocol {
    public func getInsideApp() -> [ZPInternalAppSearchConfigProtocol] {
        return insideApp ?? []
    }
    
    public func getNumberSearchApp() -> Int {
        return numberSearchApp ?? 0
    }
    
    public func getIgnoreApps() -> [Int] {
        return ignoreApps ?? []
    }
}
