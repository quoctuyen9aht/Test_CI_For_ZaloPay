//
//  ZPPaymentMethodQuickPayConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/28/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPPaymentMethodQuickPayConfigProtocol {
    func getPmcid() -> Int
    func getName() -> String
    func getLogo() -> String
}

class ZPPaymentMethodQuickPayConfig : ZPBaseConfigMapple {
    private var pmcid: Int?
    private var name: String?
    private var logo: String?
    
    override func mapping(map: Map) {
        pmcid       <- map["pmcid"]
        name        <- map["name"]
        logo        <- map["logo"]
    }
}

// MARK: Getter
extension ZPPaymentMethodQuickPayConfig : ZPPaymentMethodQuickPayConfigProtocol {
    func getPmcid() -> Int {
        return pmcid ?? 38
    }
    
    func getName() -> String {
        return name ?? ""
    }
    
    func getLogo() -> String {
        return logo ?? ""
    }
}
