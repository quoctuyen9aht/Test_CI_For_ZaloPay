//
//  ZPQuickPayConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/28/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPQuickPayConfigProtocol {
    func getEnable() -> Int
    func getUserManualUrl() -> String
    func getTimeRefresh() -> Int
    func getMaxPaymentCode() -> Int
    func getPaymentLimit() -> Int
    func getPaymentMethod() -> [ZPPaymentMethodQuickPayConfigProtocol]
    func getTransactionStatusTimeout() -> Int
}

public class ZPQuickPayConfig : ZPBaseConfigMapple {
    private var enable: Int?
    private var userManualUrl: String?
    private var timeRefresh: Int?
    private var maxPaymentCode: Int?
    private var paymentLimit: Int?
    private var paymentMethod: [ZPPaymentMethodQuickPayConfig]?
    private var transactionStatusTimeout: Int?
    
    public override func mapping(map: Map) {
        enable                          <- map["enable"]
        userManualUrl                   <- map["user_manual_url"]
        timeRefresh                     <- map["time_refresh"]
        maxPaymentCode                  <- map["max_payment_code"]
        paymentLimit                    <- map["payment_limit"]
        paymentMethod                   <- map["payment_method"]
        transactionStatusTimeout        <- map["transaction_status_timeout"]
    }
}

// MARK: Getter
extension ZPQuickPayConfig : ZPQuickPayConfigProtocol {
    public func getEnable() -> Int {
        return enable ?? 0
    }
    
    public func getUserManualUrl() -> String {
        return userManualUrl ?? ""
    }
    
    public func getTimeRefresh() -> Int {
        return timeRefresh ?? 0
    }
    
    public func getMaxPaymentCode() -> Int {
        return maxPaymentCode ?? 5
    }
    
    public func getPaymentLimit() -> Int {
        return paymentLimit ?? 200000
    }
    
    public func getPaymentMethod() -> [ZPPaymentMethodQuickPayConfigProtocol] {
        return paymentMethod ?? []
    }
    
    public func getTransactionStatusTimeout() -> Int {
        return transactionStatusTimeout ?? 5000
    }
}
