//
//  ObjectMapper+Extension.swift
//  ZaloPay
//
//  Created by Bon Bon on 2/26/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

public protocol ZPMappable: Mappable {
    
}

extension ZPMappable  {
    static func fromJSON(_ json: [String: Any]) -> Self? {
        return Mapper<Self>().map(JSON: json)
    }
}
