//
//  ZPTabmeConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPTabMeConfigProtocol {
    func getQuickCommentUrl() -> String
    func getEnableRegisterProfileZalopayId() -> Int
}

public class ZPTabMeConfig : ZPBaseConfigMapple {
    private var quickCommentUrl: String?
    private var enableRegisterProfileZalopayId: Int?
    
    public override func mapping(map: Map) {
        quickCommentUrl                      <- map["quick_comment_url"]
        enableRegisterProfileZalopayId       <- map["enable_register_profile_zalopayid"]
    }
}

// MARK: Getter
extension ZPTabMeConfig : ZPTabMeConfigProtocol {
    public func getQuickCommentUrl() -> String {
        return quickCommentUrl ?? ""
    }
    
    public func getEnableRegisterProfileZalopayId() -> Int {
        return enableRegisterProfileZalopayId ?? 0
    }
}
