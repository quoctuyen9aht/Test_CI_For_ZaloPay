//
//  ZPAppPermission.swift
//  ZaloPay
//
//  Created by thi la on 6/11/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol AppPermissionProtocol {
    func getAppPermission() -> [String: Any]
}

public class ZPAppPermission: ZPBaseConfigMapple {
    private var appPermission: [String: Any] = [:]
    
    public override func mapping(map: Map) {
        appPermission <- map["app_infos"]
    }
}

// MARK: Getter
extension ZPAppPermission : AppPermissionProtocol {

    public func getAppPermission() -> [String: Any] {
        return appPermission
    }
}
