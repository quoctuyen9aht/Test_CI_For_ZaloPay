//
//  ZPGatewayConfig.swift
//  ZaloPay
//
//  Created by Dung Vu on 5/11/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

public protocol GatewayConfigProtocol {
    func canSupport(appid: Int) -> Bool
}

public struct ZPGatewayConfig: ZPMappable, GatewayConfigProtocol {
    var listSupport: [Int]?
    public init?(map: Map) {
    }
    
    public mutating func mapping(map: Map) {
        listSupport <- map["bank_gateway_app_list"]
    }
    
    public func canSupport(appid: Int) -> Bool {
        return listSupport?.contains(appid) ?? false
    }
}
