//
//  ZPServerLogConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/28/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPServerLogConfigProtocol {
    func getTimeSendLog() -> Double
}

public class ZPServerLogConfig  : ZPBaseConfigMapple {
    private var timeSendLog: Double?
    
    public override func mapping(map: Map) {
        timeSendLog     <- map["timeSendLog"]
    }
}

// MARK: Getter
extension ZPServerLogConfig : ZPServerLogConfigProtocol {
    public func getTimeSendLog() -> Double {
        // Set default by 120s
        return timeSendLog ?? 120
    }
}
