//
//  ZPNotificationConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPNotificationConfigProtocol {
    func getVibrate() -> [Int]
}

public class ZPNotificationConfig : ZPBaseConfigMapple {
    private var vibrate: [Int]?
    
    public override func mapping(map: Map) {
        vibrate     <- map["vibrate"]
    }
}

// MARK: Getter
extension ZPNotificationConfig : ZPNotificationConfigProtocol {
    public func getVibrate() -> [Int] {
        return vibrate ?? []
    }
}
