//
//  ZPEnableOBConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPEnableOBConfigProtocol {
    func getBankCode() -> String
    func getBankName() -> String
    func getNumberLastDigit() -> Int
    func getPhone() -> String
    func getSMS() -> String
    func getPrice() -> String
    func getSupportUrl() -> String
    func getRegisterUrl() -> String
    func getMaxRetry() -> Int
    func getPhoneSupport() -> String
    func getUsingSMS() -> Int
}

class ZPEnableOBConfig : ZPBaseConfigMapple {
    private var bankCode: String?
    private var bankName: String?
    private var numberLastDigit: Int?
    private var phone: String?
    private var sms: String?
    private var price: String?
    private var supportUrl: String?
    private var registerUrl: String?
    private var maxRetry: Int?
    private var phoneSupport: String?
    private var usingSMS: Int?
    
    override func mapping(map: Map) {
        bankCode            <- map["bankCode"]
        bankName            <- map["bankName"]
        numberLastDigit     <- map["numberLastDigit"]
        phone               <- map["phone"]
        sms                 <- map["sms"]
        price               <- map["price"]
        supportUrl          <- map["supportUrl"]
        registerUrl         <- map["registerUrl"]
        maxRetry            <- map["max_retry"]
        phoneSupport        <- map["phoneSupport"]
        usingSMS            <- map["usingSMS"]
    }
}

// MARK: Getter
extension ZPEnableOBConfig : ZPEnableOBConfigProtocol {
    func getBankCode() -> String {
        return bankCode ?? ""
    }
    
    func getBankName() -> String {
        return bankName ?? ""
    }
    
    func getNumberLastDigit() -> Int {
        return numberLastDigit ?? 1
    }
    
    func getPhone() -> String {
        return phone ?? ""
    }
    
    func getSMS() -> String {
        return sms ?? ""
    }
    
    func getPrice() -> String {
        return price ?? ""
    }
    
    func getSupportUrl() -> String {
        return supportUrl ?? ""
    }
    
    func getRegisterUrl() -> String {
        return registerUrl ?? ""
    }
    
    func getMaxRetry() -> Int {
        return maxRetry ?? 0
    }
    
    func getPhoneSupport() -> String {
        return phoneSupport ?? ""
    }
    
    func getUsingSMS() -> Int {
        return usingSMS ?? 0
    }
}
