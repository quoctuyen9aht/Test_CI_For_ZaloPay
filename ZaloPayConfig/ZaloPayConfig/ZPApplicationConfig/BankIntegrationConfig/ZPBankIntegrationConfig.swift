//
//  ZPBankIntegrationConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPBankIntegrationConfigProtocol {
    func getEnableOB() -> [ZPEnableOBConfigProtocol]
}

public class ZPBankIntegrationConfig : ZPBaseConfigMapple {
    var enableOB: [ZPEnableOBConfig]?
    
    public override func mapping(map: Map) {
        enableOB        <- map["enableOB"]
    }
}

// MARK: Getter
extension ZPBankIntegrationConfig : ZPBankIntegrationConfigProtocol {
    public func getEnableOB() -> [ZPEnableOBConfigProtocol] {
        return enableOB ?? []
    }
}
