//
//  ZPCAppConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPCAppConfigProtocol {
    func getEnableMergeContactName() -> Int
    func getFriendFavorite() -> Int
    func getMaxSearchTextLength() -> Int
}

public class ZPCAppConfig : ZPBaseConfigMapple {
    private var enableMergeContactName: Int?
    private var friendFavorite: Int?
    private var maxSearchTextLength: Int?
    
    public override func mapping(map: Map) {
        enableMergeContactName         <- map["enable_merge_contact_name"]
        friendFavorite                 <- map["friend_favorite"]
        maxSearchTextLength            <- map["max_search_text_length"]
    }
}

// MARK: Getter
extension ZPCAppConfig : ZPCAppConfigProtocol {
    public func getEnableMergeContactName() -> Int {
        return enableMergeContactName ?? 0
    }
    
    public func getFriendFavorite() -> Int {
        return friendFavorite ?? 0
    }
    
    public func getMaxSearchTextLength() -> Int {
        return maxSearchTextLength ?? 26
    }
}
