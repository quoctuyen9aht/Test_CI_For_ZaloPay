//
//  ZPSDKConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/28/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPSDKConfigProtocol {
    func getPopupModeAppList() -> [Int]
    func getSuccessScreenDelay() -> Int
    func getBins() -> [ZPSDKBinsConfigProtocol]
}

public class ZPSDKConfig  : ZPBaseConfigMapple {
    private var popupModeAppList: [Int]?
    private var successScreenDelay: Int?
    private var bins: [ZPSDKBinsConfig]?
    
    public override func mapping(map: Map) {
        popupModeAppList         <- map["popup_mode_app_list"]
        successScreenDelay       <- map["success_screen_delay"]
        bins                     <- map["bins"]
    }
}

// MARK: Getter
extension ZPSDKConfig : ZPSDKConfigProtocol {
    public func getPopupModeAppList() -> [Int] {
        return popupModeAppList ?? [Int]()
    }
    
    public func getSuccessScreenDelay() -> Int {
        return successScreenDelay ?? 0
    }
    
    public func getBins() -> [ZPSDKBinsConfigProtocol] {
        return bins ?? [ZPSDKBinsConfigProtocol]()
    }
}
