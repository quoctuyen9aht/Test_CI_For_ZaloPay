//
//  ZPSDKBinsConfig.swift
//  ZaloPay
//
//  Created by vuongvv on 5/10/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import ObjectMapper

@objc public protocol ZPSDKBinsConfigProtocol {
    func getBin() -> [Int]
    func getMessageLinkcardResult() -> String
}

public class ZPSDKBinsConfig: ZPBaseConfigMapple {
    private var bin: [Int]?
    private var message_linkcard_result: String?
    
    public override func mapping(map: Map) {
        bin                            <- map["bin"]
        message_linkcard_result        <- map["message_linkcard_result"]
    }
}
// MARK: Getter
extension ZPSDKBinsConfig : ZPSDKBinsConfigProtocol {
    public func getBin() -> [Int] {
        return bin ?? [Int]()
    }
    public func getMessageLinkcardResult() -> String {
        return message_linkcard_result ?? ""
    }
}
