//
//  ZPKYCConfig.swift
//  ZaloPay
//
//  Created by Dung Vu on 4/10/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

public protocol ZPKYCValueProtocol {
    var mapCardEnable: Bool { get }
    var transactionLimitEnable: Bool { get }
    var limitTransaction: Int? { get }
}

public struct ZPKYCConfig: ZPMappable, ZPKYCValueProtocol {
    public var mapCardEnable: Bool = false
    public var transactionLimitEnable: Bool = false
    public var limitTransaction: Int?
    public init?(map: Map) {}
    
    public mutating func mapping(map: Map) {
        let transform = TransformOf<Int, Any>.init(fromJSON: {
            let result = $0 as? Int
            #if DEBUG
                assert(result != nil, "Check config")
            #endif
            return result ?? 500000
        }) {
            return $0
        }
        
        limitTransaction <- (map["transaction_amount_limit"], transform)
        mapCardEnable <- map["map_card_enable"]
        transactionLimitEnable <- map["transaction_limit_enable"]
    
    }
}
