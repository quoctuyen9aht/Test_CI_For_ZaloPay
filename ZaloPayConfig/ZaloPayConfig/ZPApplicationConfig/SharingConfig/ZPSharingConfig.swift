//
//  ZPSharingConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/28/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPSharingConfigProtocol {
    func getShareViaSocial() -> String
    func getFacebookFanpage() -> String
}

public class ZPSharingConfig  : ZPBaseConfigMapple {
    private var shareViaSocial: String?
    private var facebookFanpage: String?
    
    public override func mapping(map: Map) {
        shareViaSocial          <- map["shareViaSocial"]
        facebookFanpage         <- map["facebook_fanpage"]
    }
}

// MARK: Getter
extension ZPSharingConfig : ZPSharingConfigProtocol {
    public func getShareViaSocial() -> String {
        return shareViaSocial ?? ""
    }
    
    public func getFacebookFanpage() -> String {
        return facebookFanpage ?? ""
    }
}
