//
//  ZPApplicationConfigResource.swift
//  ZaloPay
//
//  Created by Dung Vu on 6/5/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import ObjectMapper
import CocoaLumberjackSwift
import RxSwift
import ZaloPayCommonSwift

final class ZPApplicationConfigResource {
    private var rawConfig: [String: Any]?
    private lazy var lock: NSLock = NSLock()
    private let disposeBag = DisposeBag()
    
    
    private subscript(key: String) -> [String: Any]? {
        var result: [String: Any]?
        if (rawConfig != nil) {
            self.excute {
                result = rawConfig?.value(forKey: key, defaultValue: nil)
            }
        }
        
        return result
    }
 
    
    /// Reload resource
    func reload() {
        let value = ApplicationState.sharedInstance.config?.copy()
        self.excute {
           rawConfig = value
        }
    }
    
    private func excute(block: () -> ()) {
        lock.lock()
        defer {
            lock.unlock()
        }
        block()
    }
    
    
    public func value<T: ZPMappable>(type: T.Type, by key: String) -> T? {
        DDLogInfo("\(type)")
        guard let json: [String: Any] = self[key] else {
            return nil
        }
        DDLogInfo("json parse : \(json)")
        return type.fromJSON(json)
    }
}

