//
//  ZPDisclaimerConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPDisclaimerConfigProtocol {
    func getContent() -> String
    func getListInternalId() -> [Int]
    func getExternalApps() -> [ZPExternalAppDisclaimerConfigProtocol]
}

public class ZPDisclaimerConfig : ZPBaseConfigMapple {
    private var content: String?
    private var listInternalId: [Int]?
    private var externalApps: [ZPExternalAppDisclaimerConfig]?
    
    public override func mapping(map: Map) {
        content                 <- map["content"]
        listInternalId          <- map["list_id_internal"]
        externalApps            <- map["external_apps"]
    }
}

// MARK: Getter
extension ZPDisclaimerConfig : ZPDisclaimerConfigProtocol {
    public func getContent() -> String {
        return content ?? ""
    }
    
    public func getListInternalId() -> [Int] {
        return listInternalId ?? []
    }
    
    public func getExternalApps() -> [ZPExternalAppDisclaimerConfigProtocol] {
        return externalApps ?? []
    }
}
