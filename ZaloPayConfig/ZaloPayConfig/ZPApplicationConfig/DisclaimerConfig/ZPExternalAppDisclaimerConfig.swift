//
//  ZPExternalAppDisclaimerConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPExternalAppDisclaimerConfigProtocol {
    func getAppId() -> Int
    func getMerchantUrl() -> String
    func getMerchantName() -> String
    func getUserInfo() -> [String]
}

class ZPExternalAppDisclaimerConfig : ZPBaseConfigMapple {
    private var appId: Int?
    private var merchantUrl: String?
    private var merchantName: String?
    private var userInfo: [String]?
    
    override func mapping(map: Map) {
        appId              <- map["app_id"]
        merchantUrl        <- map["merchant_url"]
        merchantName       <- map["merchant_name"]
        userInfo           <- map["user_info"]
    }
}

// MARK: Getter
extension ZPExternalAppDisclaimerConfig : ZPExternalAppDisclaimerConfigProtocol {
    func getAppId() -> Int {
        return appId ?? 0
    }
    
    func getMerchantUrl() -> String {
        return merchantUrl ?? ""
    }
    
    func getMerchantName() -> String {
        return merchantName ?? ""
    }
    
    func getUserInfo() -> [String] {
        return userInfo ?? []
    }
}
