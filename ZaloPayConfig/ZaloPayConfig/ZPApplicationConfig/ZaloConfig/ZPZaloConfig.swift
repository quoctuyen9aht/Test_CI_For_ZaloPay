//
//  ZPZaloConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/28/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPZaloConfigProtocol {
    func getTransferMode() -> String
}

public class ZPZaloConfig  : ZPBaseConfigMapple {
    private var transferMode: String?
    
    public override func mapping(map: Map) {
        transferMode       <- map["transfer_mode"]
    }
}

// MARK: Getter
extension ZPZaloConfig : ZPZaloConfigProtocol {
    public func getTransferMode() -> String {
        return transferMode ?? ""
    }
}
