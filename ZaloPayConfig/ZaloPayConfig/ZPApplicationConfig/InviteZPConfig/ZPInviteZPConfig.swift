//
//  ZPInviteZPConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/28/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPInviteZPConfigProtocol {
    func getEnable() -> Int
    func getDeeplinkShare() -> String
}

public class ZPInviteZPConfig : ZPBaseConfigMapple {
    private var enable: Int?
    private var deeplinkShare: String?
    
    public override func mapping(map: Map) {
        enable              <- map["enable"]
        deeplinkShare      <- map["deeplink_share"]
    }
}

// MARK: Getter
extension ZPInviteZPConfig : ZPInviteZPConfigProtocol {
    public func getEnable() -> Int {
        return enable ?? 0
    }
    
    public func getDeeplinkShare() -> String {
        return deeplinkShare ?? ""
    }
}
