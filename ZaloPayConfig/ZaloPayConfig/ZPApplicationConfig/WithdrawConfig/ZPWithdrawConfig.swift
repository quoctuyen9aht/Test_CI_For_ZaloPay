//
//  ZPWithdrawConfig.swift
//  ZaloPay
//
//  Created by Bon Bon on 2/26/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPWithdrawConfigProtocol {
    func getWithdrawMoney() -> [Int]
    func getMultipleWithdrawMoney() -> Int
    func getMinWithdrawMoney() -> Int
}

public class ZPWithdrawConfig : ZPBaseConfigMapple {
   private var withdrawMoney: [Int]?
   private var multipleWithdrawMoney: Int?
   private var minWithdrawMoney: Int?
    
    public override func mapping(map: Map) {
        withdrawMoney             <- map["withdraw_money"]
        multipleWithdrawMoney     <- map["multiple_withdraw_money"]
        minWithdrawMoney          <- map["min_withdraw_money"]
    }
}

// MARK: Getter
extension ZPWithdrawConfig : ZPWithdrawConfigProtocol {
    public func getWithdrawMoney() -> [Int] {
        return withdrawMoney ?? []
    }
    
    public func getMultipleWithdrawMoney() -> Int {
        return multipleWithdrawMoney ?? 0
    }
    
    public func getMinWithdrawMoney() -> Int {
        return minWithdrawMoney ?? 0
    }
}
