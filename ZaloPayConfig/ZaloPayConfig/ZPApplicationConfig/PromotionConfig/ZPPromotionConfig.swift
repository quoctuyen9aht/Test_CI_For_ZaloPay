//
//  ZPPromotionConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPPromotionConfigProtocol {
    func getVoucher() -> ZPVoucherPromotionConfigProtocol?
}

public class ZPPromotionConfig : ZPBaseConfigMapple {
    private var voucher: ZPVoucherPromotionConfig?
    
    public override func mapping(map: Map) {
        voucher     <- map["voucher"]
    }
}

// MARK: Getter
extension ZPPromotionConfig : ZPPromotionConfigProtocol {
    public func getVoucher() -> ZPVoucherPromotionConfigProtocol? {
        return voucher
    }
}
