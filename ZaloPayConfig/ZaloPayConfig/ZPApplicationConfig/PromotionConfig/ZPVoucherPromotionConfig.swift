//
//  ZPVoucherPromotionConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPVoucherPromotionConfigProtocol {
    func getAllowPaymentUseVoucher() -> Int
}

public class ZPVoucherPromotionConfig : ZPBaseConfigMapple {
    private var allowPaymentUseVoucher: Int?
    
    public override func mapping(map: Map) {
        allowPaymentUseVoucher       <- map["allow_payment_use_voucher"]
    }
}

// MARK: Getter
extension ZPVoucherPromotionConfig : ZPVoucherPromotionConfigProtocol {
    public func getAllowPaymentUseVoucher() -> Int {
        return allowPaymentUseVoucher ?? 0
    }
}
