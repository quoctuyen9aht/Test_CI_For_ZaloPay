//
//  ZPTabHomeConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPTabHomeConfigProtocol {
    func getInternalApps() -> [ZPInternalAppTabHomeConfigProtocol]
}

public class ZPTabHomeConfig : ZPBaseConfigMapple {
    private var internalApps: [ZPInternalAppTabHomeConfig]?
    
    public override func mapping(map: Map) {
        internalApps       <- map["internal_apps"]
    }
}

// MARK: Getter
extension ZPTabHomeConfig : ZPTabHomeConfigProtocol {
    public func getInternalApps() -> [ZPInternalAppTabHomeConfigProtocol] {
        return internalApps ?? []
    }
}
