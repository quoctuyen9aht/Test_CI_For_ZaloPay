//
//  ZPInternalAppTabHomeConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPInternalAppTabHomeConfigProtocol {
    func getAppId() -> Int
    func getOrder() -> Int
    func getDisplayName() -> String
    func getIconName() -> String
    func getIconColor() -> String
}

class ZPInternalAppTabHomeConfig : ZPBaseConfigMapple {
    private var appId: Int?
    private var order: Int?
    private var displayName: String?
    private var iconName: String?
    private var iconColor: String?
    
    override func mapping(map: Map) {
        appId               <- map["appId"]
        order               <- map["order"]
        displayName         <- map["display_name"]
        iconName            <- map["icon_name"]
        iconColor           <- map["icon_color"]
    }
}

// MARK: Getter
extension ZPInternalAppTabHomeConfig : ZPInternalAppTabHomeConfigProtocol {
    func getAppId() -> Int {
        return appId ?? 0
    }
    
    func getOrder() -> Int {
        return order ?? 0
    }
    
    func getDisplayName() -> String {
        return displayName ?? ""
    }
    
    func getIconName() -> String {
        return iconName ?? ""
    }
    
    func getIconColor() -> String {
        return iconColor ?? ""
    }
}
