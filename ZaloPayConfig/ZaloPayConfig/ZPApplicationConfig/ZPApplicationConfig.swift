//
//  ZPApplicationConfig.swift
//  ZaloPay
//
//  Created by Bon Bon on 2/26/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation

@objcMembers
public final class ZPApplicationConfig : NSObject {
    fileprivate static let resource = ZPApplicationConfigResource()
    static func reload() {
        
        resource.reload()
    }
    
    public class func getGeneralConfig() -> ZPGeneralConfigProtocol? {
        return "general" --> ZPGeneralConfig.self
    }
    
    public class func getZPCAppConfig() -> ZPCAppConfigProtocol? {
        return "zpc" --> ZPCAppConfig.self
    }
    
    public class func getSearchConfig() -> ZPSearchConfigProtocol? {
        return "search" --> ZPSearchConfig.self
    }
    
    public class func getApiConfig() -> ZPApiConfigProtocol? {
        return "api" --> ZPApiConfig.self
    }
    
    public class func getWithdrawConfig() -> ZPWithdrawConfigProtocol? {
        return "withdraw" --> ZPWithdrawConfig.self
    }
    
    public class func getWebAppConfig() -> ZPWebAppConfigProtocol? {
        return "webapp" --> ZPWebAppConfig.self
    }
    
    public class func getTabMeConfig() -> ZPTabMeConfigProtocol? {
        return "tab_me" --> ZPTabMeConfig.self
    }
    
    public class func getNotificationConfig() -> ZPNotificationConfigProtocol? {
        return "notification" --> ZPNotificationConfig.self
    }
    
    public class func getTabHomeConfig() -> ZPTabHomeConfigProtocol? {
        return "tab_home" --> ZPTabHomeConfig.self
    }
    
    public class func getPromotionConfig() -> ZPPromotionConfigProtocol? {
        return "promotion" --> ZPPromotionConfig.self
    }
    
    public class func getBankIntegrationConfig() -> ZPBankIntegrationConfigProtocol? {
        return "bankIntegration" --> ZPBankIntegrationConfig.self
    }
    
    public class func getUpdateProfileLevelConfig() -> ZPUpdateProfileLevelConfigProtocol? {
        return "updateprofilelevel" --> ZPUpdateProfileLevelConfig.self
    }
    
    public class func getDisclaimerConfig() -> ZPDisclaimerConfigProtocol? {
        return "disclaimer" --> ZPDisclaimerConfig.self
    }
    
    public class func getProfileConfig() -> ZPProfileConfigProtocol? {
        return "profile" --> ZPProfileConfig.self
    }
    
    public class func getInviteZPConfig() -> ZPInviteZPConfigProtocol? {
        return "inviteZP" --> ZPInviteZPConfig.self
    }
    
    public class func getQuickPayConfig() -> ZPQuickPayConfigProtocol? {
        return "quickpay" --> ZPQuickPayConfig.self
    }
    
    public class func getKeypadConfig() -> ZPKeypadConfigProtocol? {
        return "configKeyPad" --> ZPKeypadConfig.self
    }
    
    public class func getTransferConfig() -> ZPTransferConfigProtocol? {
        return "transfer" --> ZPTransferConfig.self
    }
    
    public class func getZaloConfig() -> ZPZaloConfigProtocol? {
        return "zalo" --> ZPZaloConfig.self
    }
    
    public class func getServerLogConfig() -> ZPServerLogConfigProtocol? {
        return "serverLog" --> ZPServerLogConfig.self
    }
    
    public class func getSDKConfig() -> ZPSDKConfigProtocol? {
        return "sdk" --> ZPSDKConfig.self
    }
    
    public class func getSharingConfig() -> ZPSharingConfigProtocol? {
        return "sharing" --> ZPSharingConfig.self
    }
    
    public static func getKYCValue() -> ZPKYCValueProtocol? {
        return "kyc" --> ZPKYCConfig.self
    }
    
    public static func gatewayConfig() -> GatewayConfigProtocol? {
        return "gateway" --> ZPGatewayConfig.self
    }
    
    public static func silosConfig() -> SilosConfigProtocol? {
        return "logSilos" --> ZPSilosConfig.self
    }
    
    public class func appPermission() -> AppPermissionProtocol? {
        return "permission" --> ZPAppPermission.self
    }
    
}

infix operator -->
fileprivate func --> <T: ZPMappable>(lhs: String, rhs: T.Type) -> T? {
    return ZPApplicationConfig.resource.value(type: rhs, by: lhs)
}
