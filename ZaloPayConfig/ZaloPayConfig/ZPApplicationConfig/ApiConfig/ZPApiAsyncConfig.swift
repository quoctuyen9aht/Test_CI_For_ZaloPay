//
//  ZPApiAsyncConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPApiAsyncConfigProtocol {
    func getCanGetListUserByPhone() -> Bool
}

public class ZPApiAsyncConfig : ZPBaseConfigMapple {
    private var canGetListUserByPhone: Bool?
    
    public override func mapping(map: Map) {
        canGetListUserByPhone      <- map["get_list_user_by_phone"]
    }
}

// MARK: Getter
extension ZPApiAsyncConfig : ZPApiAsyncConfigProtocol {
    public func getCanGetListUserByPhone() -> Bool {
        return canGetListUserByPhone ?? false
    }
}
