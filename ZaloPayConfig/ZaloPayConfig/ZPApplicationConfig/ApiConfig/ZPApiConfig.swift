//
//  ZPApiConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPApiConfigProtocol {
    func getApiRoute() -> String
    func getApiNames() -> [String]
    func getCallApiAsync() -> ZPApiAsyncConfigProtocol?
}

public class ZPApiConfig : ZPBaseConfigMapple {
    private var apiRoute: String?
    private var apiNames: [String]?
    private var callApiAsync: ZPApiAsyncConfig?
    
    public override func mapping(map: Map) {
        apiRoute           <- map["api_route"]
        apiNames           <- map["api_names"]
        callApiAsync       <- map["call_api_async"]
    }
}

// MARK: Getter
extension ZPApiConfig : ZPApiConfigProtocol {
    public func getApiRoute() -> String {
        return apiRoute ?? ""
    }
    
    public func getApiNames() -> [String] {
        return apiNames ?? []
    }
    
    public func getCallApiAsync() -> ZPApiAsyncConfigProtocol? {
        return callApiAsync
    }
}
