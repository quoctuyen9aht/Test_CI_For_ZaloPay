//
//  ZPProfileConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/28/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPProfileConfigProtocol {
    func getManageLoginDevices() -> Bool
}

public class ZPProfileConfig : ZPBaseConfigMapple {
    private var manageLoginDevices: Int?
    
    public override func mapping(map: Map) {
        manageLoginDevices      <- map["manageLoginDevices"]
    }
}

// MARK: Getter
extension ZPProfileConfig : ZPProfileConfigProtocol {
    public func getManageLoginDevices() -> Bool {
        return (manageLoginDevices ?? 0).toBool()
    }
}
