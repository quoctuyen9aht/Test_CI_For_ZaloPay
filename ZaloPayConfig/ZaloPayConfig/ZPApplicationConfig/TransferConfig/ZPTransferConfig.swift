//
//  ZPTransferConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/28/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper
import ZaloPayCommonSwift

@objc public protocol ZPTransferConfigProtocol {
    func getMaxLengthMessage() -> Int
}

public class ZPTransferConfig  : ZPBaseConfigMapple {
    private var maxLengthMessage: Int?
    
    public override func mapping(map: Map) {
        maxLengthMessage      <- map["max_length_message"]
    }
}

// MARK: Getter
extension ZPTransferConfig : ZPTransferConfigProtocol {
    public func getMaxLengthMessage() -> Int {
        return maxLengthMessage ?? Int(ZPConstants.TRANSFER_MESSAGE_LENGTH)
    }
}
