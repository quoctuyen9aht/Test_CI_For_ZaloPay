//
//  ZPPhoneFormatConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPPhoneFormatConfigProtocol {
    func getMinLength() -> Int
    func getMaxLength() -> Int
    func getPatterns() -> [String]
    func getInvalidLengthMessage() -> String
    func getGeneralMessage() -> String
}

class ZPPhoneFormatConfig : ZPBaseConfigMapple {
    private var minLength: Int?
    private var maxLength: Int?
    private var patterns: [String]?
    private var invalidLengthMessage: String?
    private var generalMessage: String?
    
    override func mapping(map: Map) {
        minLength                   <- map["min_length"]
        maxLength                   <- map["max_length"]
        patterns                    <- map["patterns"]
        invalidLengthMessage        <- map["invalid_length_message"]
        generalMessage              <- map["general_message"]
    }
}

// MARK: Getter
extension ZPPhoneFormatConfig : ZPPhoneFormatConfigProtocol {
    func getMinLength() -> Int {
        return minLength ?? 0
    }
    
    func getMaxLength() -> Int {
        return maxLength ?? 0
    }
    
    func getPatterns() -> [String] {
        return patterns ?? []
    }
    
    func getInvalidLengthMessage() -> String {
        return invalidLengthMessage ?? ""
    }
    
    func getGeneralMessage() -> String {
        return generalMessage ?? ""
    }
}
