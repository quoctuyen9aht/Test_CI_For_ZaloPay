//
//  ZPGeneralConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPGeneralConfigProtocol {
    func getPhoneFormat() -> ZPPhoneFormatConfigProtocol?
    func getMaxCCLinks() -> Int
}

class ZPGeneralConfig : ZPBaseConfigMapple {
    private var phoneFormat: ZPPhoneFormatConfig?
    private var maxCCLinks: Int?
    
    override func mapping(map: Map) {
        phoneFormat       <- map["phone_format"]
        maxCCLinks        <- map["max_cc_links"]
    }
}

// MARK: Getter
extension ZPGeneralConfig : ZPGeneralConfigProtocol {
    func getPhoneFormat() -> ZPPhoneFormatConfigProtocol? {
        return phoneFormat
    }
    
    func getMaxCCLinks() -> Int {
        return maxCCLinks ?? 0
    }
}
