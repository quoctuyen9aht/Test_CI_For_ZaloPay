//
//  ZPSilosConfig.swift
//  ZaloPay
//
//  Created by Dung Vu on 6/11/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

public protocol SilosConfigProtocol {
    var enable: Bool { get }
}

public struct ZPSilosConfig: ZPMappable, SilosConfigProtocol {
    public var enable: Bool = false
    public init?(map: Map) {}
    public mutating func mapping(map: Map) {
        enable <- map["enable"]
    }
}
