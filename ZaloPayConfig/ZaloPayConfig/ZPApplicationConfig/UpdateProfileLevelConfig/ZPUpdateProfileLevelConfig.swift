//
//  ZPUpdateProfileLevelConfig.swift
//  ZaloPay
//
//  Created by tridm2 on 2/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

@objc public protocol ZPUpdateProfileLevelConfigProtocol {
    func getImagesize() -> Int
    func getImageQuality() -> [String:Any]
}

public class ZPUpdateProfileLevelConfig : ZPBaseConfigMapple {
    private var imagesize: Int?
    private var imagequality: [String:Any]?
    public override func mapping(map: Map) {
        imagesize       <- map["imagesize"]
        imagequality    <- map["imagequality"]
    }
}

// MARK: Getter
extension ZPUpdateProfileLevelConfig : ZPUpdateProfileLevelConfigProtocol {
    public func getImagesize() -> Int {
        return imagesize ?? 1024
    }
    public func getImageQuality() -> [String:Any] {
        return imagequality ?? [:]
    }
}
