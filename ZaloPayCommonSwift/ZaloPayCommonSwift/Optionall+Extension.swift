//
//  Optionanl+Extension.swift
//  ZaloPayCommonSwift
//
//  Created by Bon Bon on 4/10/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import Foundation

//https://appventure.me/2018/01/10/optional-extensions/

public extension Optional {
    /// Returns true if the optional is empty
    public var isNil: Bool {
        return self == nil
    }
    
    /// Returns true if the optional is not empty
    public var isSome: Bool {
        return self != nil
    }
    
    /// Return the value of the Optional or the `default` parameter
    /// - param: The value to return if the optional is empty
    public func or(_ default: Wrapped) -> Wrapped {
        return self ?? `default`
    }
    
    /// Returns the unwrapped value of the optional *or*
    /// the result of an expression `else`
    /// I.e. optional.or(else: print("Arrr"))
    public func or(else: @autoclosure () -> Wrapped) -> Wrapped {
        return self ?? `else`()
    }
    
    /// Returns the unwrapped value of the optional *or*
    /// the result of calling the closure `else`
    /// I.e. optional.or(else: {
    /// ... do a lot of stuff
    /// })
    public func or(else: () -> Wrapped) -> Wrapped {
        return self ?? `else`()
    }
    
    /// Returns the unwrapped contents of the optional if it is not empty
    /// If it is empty, throws exception `throw`
    public func or(throw exception: Error) throws -> Wrapped {
        guard let unwrapped = self else { throw exception }
        return unwrapped
    }
    
}

public extension Optional {
    /// Executes the closure `some` if and only if the optional has a value
    public func on(some: () throws -> Void) rethrows {
        if self != nil { try some() }
    }
    
    /// Executes the closure `none` if and only if the optional has no value
    public func on(none: () throws -> Void) rethrows {
        if self == nil { try none() }
    }
}
