//
//  String+Extensions.swift
//  ZaloPayCommonSwift
//
//  Created by thi la on 1/8/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import Foundation

public extension NSString {
    @objc public static func isEqualOrNewerThanVersion(currentVersion:String,checkVersion:String) -> Bool {
        let result = currentVersion.compare(checkVersion, options: .numeric)
        return result == .orderedSame || result == .orderedDescending
    }
}

public extension String {
    public func toJSON() -> JSON? {
        return self.data(using: .utf8)?.toJSON()
    }
}

public extension Data {
    public func toJSON() -> JSON? {
        return (try? JSONSerialization.jsonObject(with: self)) as? JSON
    }
}

