//
//  UIDevice+Extension.swift
//  ZaloPayCommonSwift
//
//  Created by Bon Bon on 6/26/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import Foundation

public extension UIDevice {    
    public func isJailBroken() -> Bool {
        if TARGET_OS_SIMULATOR == 1 {
            return false
        }
        var file = fopen("/Applications/Cydia.app", "r")
        if (file != nil) {
            fclose(file)
            return true
        }
        file = fopen("/Library/MobileSubstrate/MobileSubstrate.dylib", "r")
        if (file != nil) {
            fclose(file)
            return true
        }
        file = fopen("/bin/bash", "r")
        if (file != nil) {
            fclose(file)
            return true
        }
        file = fopen("/usr/sbin/sshd", "r")
        if (file != nil) {
            fclose(file)
            return true
        }
        file = fopen("/etc/apt", "r")
        if (file != nil) {
            fclose(file)
            return true
        }
        file = fopen("/usr/bin/ssh", "r")
        if (file != nil) {
            fclose(file)
            return true
        }
        
        // Check existence of files that are common for jailbroken devices
        
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: "/Applications/Cydia.app") || fileManager.fileExists(atPath: "/Library/MobileSubstrate/MobileSubstrate.dylib") || fileManager.fileExists(atPath: "/bin/bash") || fileManager.fileExists(atPath: "/usr/sbin/sshd") || fileManager.fileExists(atPath: "/etc/apt") || fileManager.fileExists(atPath: "/usr/bin/ssh") {
            return true
        }
        
        if NSFoundationVersionNumber > 1144.17 {
            return false
        }
        
        // Check  Reading and writing in system directories (sandbox violation)
        
        let error : Error? = nil
        let string = ""
        do {
            try string.write(toFile: "/private/jailbreak.txt", atomically: true, encoding: String.Encoding.utf8)
        } catch {
            print(error)
        }
        if error == nil {
            return true
        } else {
            do {
                try fileManager.removeItem(atPath: "/private/jailbreak.txt")
            } catch {
                print(error)
            }
            
        }
        
        if UIApplication.shared.canOpenURL(URL(string: "cydia://package/com.example.package")!) {
            return true
        }
        
        return false
    }
}
