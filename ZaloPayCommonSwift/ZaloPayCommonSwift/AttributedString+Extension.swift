//
//  AttributedString+Extension.swift
//  ZaloPayCommonSwift
//
//  Created by Dung Vu on 6/6/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import Foundation

public extension NSAttributedString {
    @objc func addAttribute(from att: NSAttributedString?) -> NSAttributedString {
        // Make copy
        let mutableString = NSMutableAttributedString(attributedString: self)
        guard let att = att, !att.string.isEmpty else {
            return mutableString
        }
        let s = att.string
        let main = mutableString.string
        let range = (main as NSString).range(of: s)
        guard range.location != NSNotFound else {
            return mutableString
        }
        att.enumerateAttributes(in: NSMakeRange(0, s.count), options: []) { (dict, _, _) in
            mutableString.addAttributes(dict, range: range)
        }
        return mutableString
    }
}

public func += (lhs: inout NSAttributedString?, rhs: NSAttributedString?) {
    lhs = lhs?.addAttribute(from: rhs)
}

public func += (lhs: inout NSAttributedString, rhs: NSAttributedString?) {
    lhs = lhs.addAttribute(from: rhs)
}
