//
//  ZPConstants.swift
//  ZaloPayCommonSwift
//
//  Created by VuongVV on 8/30/17.
//  Copyright © 2017 VNG. All rights reserved.
//

import UIKit

public class ZPConstants : NSObject {

    public static let ZaloPayAppStoreId: Int = 1112407590
    public static let ZaloPayAppStoreUrl: String = "https://itunes.apple.com/us/app/zalo-pay-thanh-toan-trong/id1112407590?mt=8"
    public static let kZaloPayClientAppId: Int = 1
    public static let reactInternalAppId: Int = 1
    public static let withdrawAppId: Int = 2
    public static let showshowAppId: Int = 22
    public static let lixiAppId: Int = 6
    public static let topupPhoneAppId: Int = 11
    public static let topupPhoneV2AppId: Int = 61
    public static let appDichVuId: Int = 15
    public static let appVoucherId: Int = 5
    public static let NUMBER_OF_DIGIT: Int = 6
    public static let NUMBER_OF_OTP: Int = 6
    public static let TRANSFER_MESSAGE_LENGTH: Int = 50
    public static let minAccountNameLength: Int = 4
    public static let maxAccountNameLength: Int = 24
    public static let EMAIL_LENGTH: Int = 45
    public static let IDENTYFIER_NUMBER_LENGTH: Int = 45
    public static let FEEDBACK_MESSAGE_LENGTH: Int = 400
    public static let timeoutIntervalForRequest: Float = 10.0
    public static let timeoutIntervalForConnectorRequest: Float = 5.0
    public static let delayUpTransacLogServer: Double = 10.0
    public static let WithDrawNumberOther: Int = -1111
    public static let WithDrawMinDefault: Int = 20000
    public static let WithDrawMultipleDefault: Int = 10000
    public static let kAppIdGetStatus:Int = 3
    
    public static let minRechargeValue: Int = 20000
    public static let maxRechargeValue: Int = 100000000
    public static let defaultMinTransfer: Int = 10000
    public static let defaultMaxTransfer: Int = 5000000
    public static let defaultMaxWithdraw: Int = 100000000
    public static let transferNameMaxLength: Int = 30
    public static let minVietcombankValue: Int = 50000

    // MARK: - Key
    public static let ZPConfigCacheKey: String = "ZPConfigCacheKey"
    public static let ZPBannerKey: String = "ZPBannerKey"
    public static let ZPBankSupportKey: String = "ZPBankSupportKey"
    public static let ZPProfileLevel3_Email: String = "ZPProfileLevel3_Email"
    public static let ZPProfileLevel3_Identifier: String = "ZPProfileLevel3_Identifier"
    public static let ZPProfileLevel3_FrontIdentifyCard: String = "ZPProfileLevel3_FrontIdentifyCard"
    public static let ZPProfileLevel3_BackSideIdentifyCard: String = "ZPProfileLevel3_BackSideIdentifyCard"
    public static let ZPProfileLevel3_Avatar: String = "ZPProfileLevel3_Avatar"
    public static let Force_Update_Appversion_Key: String = "Force_Update_Appversion_Key"
    public static let ZPEnableRechargeKey: String = "ZPEnableRechargeKey"
    public static let ZPInApp_Notification_Key: String = "ZPInApp_Notification_Key"
    public static let Reload_AccountList_Notification: String = "Reload_AccountList_Notification"
    public static let Reload_CardList_Notification: String = "Reload_CardList_Notification"
    public static let Reload_Profile_Notification: String = "Reload_Profile_Notification"
    public static let GMSService_API: String = "AIzaSyBBynYIi93D36Jbq9LsfLou_enqA3dbRFc"
    
    // MARK: - Param key
    public static let ZPCreateWalletOrderTranstypeKey: String = "transtype"
    public static let ZPCreateWalletOrderExtKey: String = "ext"
    public static let ZPCreateWalletOrderSenderKey: String = "sender"
    public static let ZPCreateWalletOrderSenderPhoneNumberKey: String = "phonenumber"
    public static let Key_Store_Database_Promotion: String = "Key_Store_Database_Promotion"
    public static let ZPNumberAppSearched: String = "number_search_app"
    public static let Quick_Comment_URL: String = "https://goo.gl/forms/FCO642guFSbpYwlt2"
    public static let ViewModeKeyboard_ABC: String = "keyboardABC"
    public static let ViewModeKeyboard_Phone: String = "keyboardPhone"
}
