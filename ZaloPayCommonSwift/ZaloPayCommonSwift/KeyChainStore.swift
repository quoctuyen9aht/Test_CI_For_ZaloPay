//
//  KeyChainStore.swift
//  ZaloPay
//
//  Created by Dung Vu on 11/14/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation

@objcMembers
public class KeyChainStore: NSObject {
    private static let defaultService = Bundle.main.bundleIdentifier
    private static func baseQuery(with key: String?, from service: String?, accessGroup group: String?) -> [String: Any] {
        let service = service ?? self.defaultService
        var query: [String: Any] = [:]
        query[kSecClass.description] = kSecClassGenericPassword.description
        query[kSecAttrService.description] = service
        query[kSecAttrGeneric.description] = key
        query[kSecAttrAccount.description] = key
#if !TARGET_IPHONE_SIMULATOR && __IPHONE_OS_VERSION_MIN_REQUIRED
        query[kSecAttrAccessGroup.description] = group
#endif
        return query
    }
}

extension CFString: CustomStringConvertible {
    public var description: String {
        return self as String
    }
}
// MARK: - Get Set Value
public extension KeyChainStore {
    static func items(from service: String?, accessGroup group: String? = nil) -> [NSDictionary]? {
        var query = self.baseQuery(with: nil, from: service, accessGroup: group)
        query[kSecReturnData.description] = true
        query[kSecMatchLimit.description] = kSecMatchLimitAll.description
        query[kSecReturnAttributes.description] = true
        var result: AnyObject?
        defer {
            autoreleasepool { () -> () in
                result = nil
            }
        }
        let status = withUnsafeMutablePointer(to: &result) {
            SecItemCopyMatching(query as CFDictionary, $0)
        }
        if status == noErr {
            return result as? [NSDictionary]
        }
        return nil
    }
    // MARK: - Get data
    static func data(forKey key: String?, service s: String?, accessGroup group: String? = nil) -> Data? {
        guard let key = key else {
            return nil
        }
        var query = self.baseQuery(with: key, from: s, accessGroup: group)
        query[kSecReturnData.description] = true
        query[kSecMatchLimit.description] = kSecMatchLimitOne.description
        var result: AnyObject?
        defer {
            autoreleasepool { () -> () in
                result = nil
            }
        }
        
        let lastResultCode = withUnsafeMutablePointer(to: &result) {
            SecItemCopyMatching(query as CFDictionary, UnsafeMutablePointer($0))
        }
        
        if lastResultCode == noErr { return result as? Data }
        return nil
    }
    // MARK: - Set data
    @discardableResult
    static func set(data d: Data?, forKey key: String?, service s: String?, accessGroup group: String? = nil) -> Bool {
        
        guard let key = key else {
            return false
        }
        guard let data = d else {
            // Remove
            self.remove(forKey: key, service: s, accessGroup: group)
            return true
        }
        var query = self.baseQuery(with: key, from: s, accessGroup: group)
        let status = SecItemCopyMatching(query as CFDictionary, nil)
        switch status {
        case errSecSuccess:
            let attributesToUpdate = [kSecValueData.description : data]
            // Update
            let updateStatus = SecItemUpdate(query as CFDictionary, attributesToUpdate as CFDictionary)
            return updateStatus == errSecSuccess
        case errSecItemNotFound:
            query[kSecValueData.description] = data
            let newStatus = SecItemAdd(query as CFDictionary, nil)
            return newStatus == errSecSuccess
        default:
            return false
        }
    }
    
    // MARK: - Remove
    @discardableResult
    static func remove(forKey key: String?, service s: String?, accessGroup group: String? = nil) -> Bool {
        guard let key = key else {
            return false
        }
        let query = self.baseQuery(with: key, from: s, accessGroup: group)
        let status = SecItemDelete(query as CFDictionary)
        return !(status != errSecSuccess && status != errSecItemNotFound)
    }
    
    @discardableResult
    static func removeAllItems(service s: String? = nil, accessGroup group: String? = nil) -> Bool {
        guard let items = self.items(from: s, accessGroup: group) as? [[String: Any]],items.count > 0 else {
            return true
        }
        var itemsCheck = items.map { v -> [String: Any] in
            var temp = v
            temp[kSecClass.description] = kSecClassGenericPassword.description
            return temp
        }
        defer {
            autoreleasepool { () -> () in
                itemsCheck.removeAll()
            }
        }
        
        for item in itemsCheck {
            let status = SecItemDelete(item as CFDictionary)
            if status != errSecSuccess { return false }
        }
        return true
    }
    
    // MARK: - String
    @discardableResult
    static func set(string str: String?, forKey key: String?, service s: String? = nil, accessGroup group: String? = nil) -> Bool {
        let data = str?.data(using: .utf8)
        return self.set(data: data, forKey: key, service: s, accessGroup: group)
    }
    
    static func string(forKey key: String?, service s: String? = nil, accessGroup group: String? = nil) -> String? {
        guard let data = self.data(forKey: key, service: s, accessGroup: group) else {
            return nil
        }
        let result = String(data: data, encoding: .utf8)
        return result
    }
    
    // MARK: - Object
    static func set<E: Encodable>(object o: E?, forKey key: String?, service s: String? = nil, accessGroup group: String? = nil) -> Bool {
        guard let o = o else {
            return false
        }
        let encoder = JSONEncoder()
        let data = try? encoder.encode(o)
        return self.set(data: data, forKey: key, service: s, accessGroup: group)
    }
    
    static func get<F: Decodable>(withType type: F.Type, forKey key: String?, service s: String? = nil, accessGroup group: String? = nil) -> F?
    {
        guard let data = self.data(forKey: key, service: s, accessGroup: group) else {
            return nil
        }
        let decoder = JSONDecoder()
        let result = try? decoder.decode(type, from: data)
        return result
    }
    
}
