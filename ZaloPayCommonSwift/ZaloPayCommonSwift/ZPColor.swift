//
//  ZPColor.swift
//  ZaloPayCommonSwift
//
//  Created by thi la on 8/29/17.
//  Copyright © 2017 VNG. All rights reserved.
//

import Foundation


public extension UIColor {

    public class var line: UIColor {
        return UIColor(hexString: ZPColor.lineColor) ?? UIColor()
    }
    public class func zpBaseColor () -> UIColor {
        return UIColor(hexString: ZPColor.zaloPayBaseColor) ?? UIColor()
    }
    
    public convenience init?(hexString: String){
        var hex = hexString
        
        if hex.hasPrefix("#") {
            hex = hex.replacingOccurrences(of: "#", with: "")
        }
        guard let hexVal = Int(hex, radix: 16) else {
            return nil
        }
        self.init(hexValue: hexVal)
    }
    
    public convenience init(hexValue: Int) {
        let divisor = CGFloat(255)
        let red     = CGFloat((hexValue & 0xFF0000) >> 16) / divisor
        let green   = CGFloat((hexValue & 0x00FF00) >>  8) / divisor
        let blue    = CGFloat( hexValue & 0x0000FF       ) / divisor
        self.init(red: red, green: green, blue: blue, alpha: 1)
    }
    
//    public class func randomColor() -> UIColor {
//       return UIColor(red: .random(), green: .random(), blue: .random(), alpha: 1.0)
//    }
//    
//    public class func subText() -> UIColor {
//        return UIColor(hexValue:0x727f8c)
//    }
//    
//    public class func defaultText() -> UIColor {
//        return UIColor(hexValue:0x24272b)
//    }
//    
//    public class func midNightBlueColor() -> UIColor {
//        return UIColor(hexValue:0x2c3e50)
//    }
//    
//    public class func zaloBaseColor() -> UIColor {
//        return UIColor(hexValue: 0x008fe5)
//    }
//    
//    public class func lineColor() -> UIColor {
//        return UIColor(hexValue: 0xe3e6e7)
//    }
//    
//    public class func errorColor() -> UIColor {
//        return UIColor(hexValue: 0xfe0000)
//    }
//    
//    public class func defaultBackground() -> UIColor {
//        return UIColor(hexValue:0xf0f4f7)
//    }
//    
//    public class func placeHolderColor() -> UIColor {
//        return UIColor(hexValue: 0xacb3ba)
//    }
//    
//    public class func disableButtonColor() -> UIColor {
//        return UIColor(hexValue: 0xc7cad3)
//    }
//    
//    public class func highlightButtonColor() -> UIColor {
//        return UIColor(hexValue: 0x0174af)
//    }
//    
//    public class func payColor() -> UIColor {
//        return UIColor(hexValue: 0x04be04)
//    }
//    
//    public class func payHighlightColor() -> UIColor {
//        return UIColor(hexValue: 0x21d921)
//    }
//    
//    public class func selectedColor() -> UIColor {
//        return UIColor(hexValue: 0xe7e9eb)
//    }
//    
//    public class func buttonHighlightColor() -> UIColor {
//        return UIColor(hexValue: 0x49bbff)
//    }
//    
//    public class func zp_blue() -> UIColor {
//        return UIColor(hexValue: 0x4387f6)
//    }
//    
//    public class func zp_green() -> UIColor {
//        return UIColor(hexValue: 0x129d5a)
//    }
//    
//    public class func zp_red() -> UIColor {
//        return UIColor(hexValue: 0xde4438)
//    }
//    
//    public class func zp_yellow() -> UIColor {
//        return UIColor(hexValue: 0xf6b501)
//    }
//    
//    public class func zp_gray() -> UIColor {
//        return UIColor(hexValue: 0x53718b)
//    }
//    
//    public class func defaultAppIconColor() -> UIColor {
//        return UIColor(hexValue: 0xbbc1c8)
//    }
//    
//    public class func hex_0x333333() -> UIColor {
//        return UIColor(hexValue: 0x333333)
//    }
//    
//    public class func hex_0x2b2b2b() -> UIColor {
//        return UIColor(hexValue: 0x2b2b2b)
//    }
//    
//    public class func hex_0xacb3ba() -> UIColor {
//        return UIColor(hexValue: 0xacb3ba)
//    }
//    
//    public class func hex_0x686871() -> UIColor {
//        return UIColor(hexValue: 0x686871)
//    }
//    
//    public class func rechargeColor() -> UIColor {
//        return UIColor(hexValue: 0x139b5b)
//    }
//    
//    public class func withdrawColor() -> UIColor {
//        return UIColor(hexValue: 0xf6b501)
//    }
//    
//    public class func hex_0xff5239() -> UIColor {
//        return UIColor(hexValue: 0xff5239)
//    }
//    
//    public class func hex_0x3c4854() -> UIColor {
//        return UIColor(hexValue: 0x3c4854)
//    }
//    
//    public class func hex_0xffffc1() -> UIColor {
//        return UIColor(hexValue: 0xffffc1)
//    }
//    
//    public class func hex_0xc7c7cc() -> UIColor {
//        return UIColor(hexValue: 0xc7c7cc)
//    }
//    
//    public class func hex_0x4abbff() -> UIColor {
//        return UIColor(hexValue: 0x4abbff)
//    }
//    
//    public class func hex_0xe6f6ff() -> UIColor {
//        return UIColor(hexValue: 0xe6f6ff)
//    }
//    
//    public class func hex_0xe3e6e7() -> UIColor {
//        return UIColor(hexValue: 0xe3e6e7)
//    }
//    
//    public class func hex_0xEAF7FF() -> UIColor {
//        return UIColor(hexValue: 0xeaf7ff)
//    }
    
}

struct ZPColor {
    static let lineColor = "e3e6e7"
    static let zaloPayBaseColor = "008fe5"
}

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}
