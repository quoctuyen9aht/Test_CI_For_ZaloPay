//
//  URL+Extension.swift
//  ZaloPayCommonSwift
//
//  Created by thi la on 6/22/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import Foundation

public extension URL {
    public static func zaloUrl() -> URL {
        return URL(string: "zalo://")!
    }
    
    public func isNeedHandleScheme() -> Bool {
        guard let scheme = self.scheme else {
            return false
        }
        return (!scheme.hasPrefix("http")
            && UIApplication.shared.canOpenURL(self))
    }
}
