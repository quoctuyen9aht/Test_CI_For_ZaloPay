//
//  UINavigationController+Extension.swift
//  ZaloPay
//
//  Created by Dung Vu on 4/10/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {
  @objc public class func currentActiveNavigationController() -> UINavigationController? {
        if let navi = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController as? UINavigationController {
            return navi
        }
        let windows = UIApplication.shared.windows;
        for oneWindow in windows {
            if let tab = oneWindow.rootViewController as? UITabBarController {
                return tab.selectedViewController as? UINavigationController
            }
            if let navi = oneWindow.rootViewController as? UINavigationController {
                return navi
            }
        }
        return nil
    }
    
    @objc public class func navigationBarHeight() -> Float {
        if let navigationBar = currentActiveNavigationController()?.navigationBar {
            return Float(navigationBar.frame.origin.y + navigationBar.frame.size.height)
        }
        return 64
    }
    
    @objc public class func previousViewController(fromBackEvent: Bool = false) -> UIViewController? {
        guard let currentNaviController = self.currentActiveNavigationController() else {
            return nil
        }
        
        if let _ = currentNaviController.presentedViewController {
            return currentNaviController.viewControllers.last
        }
        let n = currentNaviController.viewControllers.count
        
        if fromBackEvent {
            return n >= 1 ? currentNaviController.viewControllers[n-1] : nil
        }

        return n >= 2 ? currentNaviController.viewControllers[n-2] : nil
    }
    
}
