//
//  UINavigationController+Style.swift
//  ZaloPay
//
//  Created by vuongvv on 9/29/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

public extension UINavigationController {
    
    @objc public func clearColorNavigationBarStyle() {
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = true
        view.backgroundColor = UIColor.clear
        navigationBar.backgroundColor = UIColor.clear
        isNavigationBarHidden = false
    }
    
    @objc public func defaultNavigationBarStyle() {
        navigationBar.setBackgroundImage(nil, for: .default)
        navigationBar.barTintColor = UIColor.zpBaseColor()
        navigationBar.tintColor = UIColor.white
        navigationBar.isTranslucent = false
        let font = UIFont.sfuiMedium(size: 18)
        navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: font]
    }
    
    @objc public class func defaltAppearanceStyle() {
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().barTintColor = UIColor.zpBaseColor()
        UINavigationBar.appearance().titleTextAttributes = [
            NSAttributedStringKey.foregroundColor : UIColor.white
        ]
        UINavigationBar.appearance().tintColor = UIColor.white
    }
    
}
