//
//  UIFont+Extension.swift
//  ZaloPayCommonSwift
//
//  Created by Bon Bon on 6/26/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import Foundation

public extension UIFont {
    public class func sfuiMedium(size: CGFloat) -> UIFont {
        return UIFont.init(name: "SFUIText-Medium", size: size) ?? UIFont.systemFont(ofSize: size)
    }
}
