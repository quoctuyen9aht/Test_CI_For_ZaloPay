//
//  ZPLineView.swift
//  ZaloPayCommonSwift
//
//  Created by thi la on 8/29/17.
//  Copyright © 2017 VNG. All rights reserved.
//

import UIKit
import SnapKit

@objcMembers
public class ZPLineView: UIView {
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.line
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func alignToTop() {
        self.snp.makeConstraints { make in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(1)
        }
    }
    
    public func alignToBottom() {
        self.snp.makeConstraints { make in
            make.bottom.equalTo(0)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(1)
        }
        
    }
    
    public func alignToLeft() {
        self.snp.makeConstraints { make in
            make.top.equalTo(0)
            make.bottom.equalTo(0)
            make.left.equalTo(0)
            make.width.equalTo(1)
        }
    }
    
    public func alignToRight() {
        self.snp.makeConstraints { make in
            make.top.equalTo(0)
            make.bottom.equalTo(0)
            make.right.equalTo(0)
            make.width.equalTo(1)
        }
    }
    
    public func alignToBottomWith(leftMargin margin: Int) {
        self.snp.makeConstraints { make in
            make.bottom.equalTo(0)
            make.left.equalTo(margin)
            make.right.equalTo(0)
            make.height.equalTo(1)
        }
    }
}

