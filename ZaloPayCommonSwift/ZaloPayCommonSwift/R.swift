//
//  R.swift
//  ZaloPayResource
//
//  Auto-generated.
//  Copyright (c) 2016-2017 VNG Corporation. All rights reserved.
//

import Foundation

@objcMembers
public final class R: NSObject {
    public static func string_ButtonLabel_OK() -> String 
	{ 
		return "Đồng ý" 
	}

	public static func string_ButtonLabel_Close() -> String 
	{ 
		return "Đóng" 
	}

	public static func string_ButtonLabel_Cancel() -> String 
	{ 
		return "Hủy" 
	}

	public static func string_ButtonLabel_Upgrade() -> String 
	{ 
		return "Cập Nhật" 
	}

	public static func string_ButtonLabel_Next() -> String 
	{ 
		return "Tiếp Tục" 
	}

	public static func string_ButtonLabel_Skip() -> String 
	{ 
		return "Bỏ Qua" 
	}

	public static func string_ButtonLabel_Later() -> String 
	{ 
		return "Để sau" 
	}

	public static func string_ButtonLabel_Start() -> String 
	{ 
		return "Bắt Đầu" 
	}

	public static func string_ButtonLabel_Detail() -> String 
	{ 
		return "Chi Tiết" 
	}

	public static func string_ButtonLabel_Retry() -> String 
	{ 
		return "Thử lại" 
	}

	public static func string_ButtonLabel_RemoveCard() -> String 
	{ 
		return "Xoá thẻ" 
	}

	public static func string_ButtonLabel_Done() -> String 
	{ 
		return "Xong" 
	}

	public static func string_ButtonLabel_Send() -> String 
	{ 
		return "Gửi" 
	}

	public static func string_ButtonLabel_Remove() -> String 
	{ 
		return "Xoá" 
	}

	public static func string_ButtonLabel_Keep() -> String 
	{ 
		return "Giữ lại" 
	}

	public static func string_ButtonLabel_Confirm_To_Pay() -> String 
	{ 
		return "Xác Nhận Thanh Toán" 
	}

	public static func string_ButtonLabel_Detail_Exchange() -> String 
	{ 
		return "Chi Tiết Giao Dịch" 
	}

	public static func string_ButtonLabel_UpgradeDialog() -> String 
	{ 
		return "Cập nhật" 
	}

	public static func string_ButtonLabel_Login() -> String 
	{ 
		return "Đăng nhập" 
	}

	public static func string_ButtonLabel_Back() -> String 
	{ 
		return "Quay lại" 
	}

	public static func string_ButtonLabel_ContinueWait() -> String 
	{ 
		return "Tiếp tục chờ" 
	}

	public static func string_ButtonLabel_LinkCard() -> String 
	{ 
		return "Liên kết" 
	}

	public static func string_ButtonLabel_SkipUpgrade() -> String 
	{ 
		return "Bỏ cập nhật" 
	}

	public static func string_ButtonLabel_Stay() -> String 
	{ 
		return "Ở lại" 
	}

	public static func string_ButtonLabel_Confirm() -> String 
	{ 
		return "Xác nhận" 
	}

	public static func string_ButtonLabel_Change() -> String 
	{ 
		return "Thay đổi" 
	}

	public static func string_Message_Input_PIN_ManageCard() -> String 
	{ 
		return "Nhập mật khẩu để truy cập" 
	}

	public static func string_Message_Input_PIN_Profile() -> String 
	{ 
		return "Nhập mật khẩu để truy cập" 
	}

	public static func string_Message_Input_PIN_Transactions() -> String 
	{ 
		return "Nhập mật khẩu thanh toán để truy cập" 
	}

	public static func string_Message_Signout_Confirmation() -> String 
	{ 
		return "Bạn muốn đăng xuất tài khoản này?" 
	}

	public static func string_Message_Session_Expire() -> String 
	{ 
		return "Phiên làm việc đã hết hạn.\nVui lòng đăng nhập lại để tiếp tục." 
	}

	public static func string_Message_Server_Maintain() -> String 
	{ 
		return "Hệ thống đang bảo trì.\nVui lòng thử lại sau." 
	}

	public static func string_Message_Lock_Account() -> String 
	{ 
		return "Tài khoản hiện đang bị khoá.\nVui lòng liên hệ 1900 54 54 36 để được trợ giúp" 
	}

	public static func string_Dialog_Title_Notification() -> String 
	{ 
		return "Thông báo" 
	}

	public static func string_Dialog_Title_RevertMoney() -> String 
	{ 
		return "Thu hồi tiền" 
	}

	public static func string_Message_Update_Profile() -> String 
	{ 
		return "Hãy cập nhật thông tin để sử dụng tính năng này" 
	}

	public static func string_Withdraw_Title() -> String 
	{ 
		return "Rút tiền" 
	}

	public static func string_Withdraw_Description_Message() -> String 
	{ 
		return "Rút tiền về thẻ/tài khoản đã liên kết" 
	}

	public static func string_Withdraw_Description_Link_Success_Alert() -> String 
	{ 
		return "Bạn đã Liên kết thẻ ngân hàng thành công. Bạn có muốn tiếp tục rút tiền ?" 
	}

	public static func string_Withdraw_Description_Link_Bold_Title() -> String 
	{ 
		return "Liên kết thẻ ngân hàng" 
	}

	public static func string_AddCash_Title() -> String 
	{ 
		return "Nạp tiền" 
	}

	public static func string_AddCash_Description_Message() -> String 
	{ 
		return "Nạp tiền vào tài khoản ZaloPay" 
	}

	public static func string_AddCash_TextPlaceHolder() -> String 
	{ 
		return "Số tiền (VND)" 
	}

	public static func string_Account_Name_NonEmpty_Message() -> String 
	{ 
		return "Bạn chưa nhập ZaloPay ID" 
	}

	public static func string_Account_Name_Length_Message() -> String 
	{ 
		return "ZaloPay ID phải từ 4-24 ký tự" 
	}

	public static func string_Account_Name_Space_Message() -> String 
	{ 
		return "ZaloPay ID không có dấu cách" 
	}

	public static func string_Account_Name_Special_Char_Message() -> String 
	{ 
		return "ZaloPay ID không có kí tự đặc biệt" 
	}

	public static func string_Transfer_InputAccontName() -> String 
	{ 
		return "Nhập ZaloPay ID" 
	}

	public static func string_Account_Name_Warning_Message() -> String 
	{ 
		return "ZaloPay ID được tạo một lần duy nhất và không được thay đổi. Bạn có chắc muốn dùng ID này không?" 
	}

	public static func string_InputMoney_Syntax_Error_Message() -> String 
	{ 
		return "Bạn nhập sai cú pháp" 
	}

	public static func string_InputMoney_Cant_Divide_By_Zero_Error_Message() -> String 
	{ 
		return "Không thể chia cho 0" 
	}

	public static func string_InputMoney_Max_Character_Error_Message() -> String 
	{ 
		return "Không được vượt quá 30 kí tự" 
	}

	public static func string_InputMoney_SmallAmount_Error_Message() -> String 
	{ 
		return "Số tiền tối thiểu %@" 
	}

	public static func string_InputMoney_BigAmount_Error_Message() -> String 
	{ 
		return "Số tiền không được vượt quá %@" 
	}

	public static func string_InputMessage_Error_Message() -> String 
	{ 
		return "Lời nhắn không được vượt quá %i kí tự" 
	}

	public static func string_InputNode_Error_Message() -> String 
	{ 
		return "Ghi chú không được vượt quá %i kí tự" 
	}

	public static func string_Title_PIN() -> String 
	{ 
		return "Nhập mật khẩu để thanh toán bằng" 
	}

	public static func string_Title_Phone_Number() -> String 
	{ 
		return "Số điện thoại" 
	}

	public static func string_MessageInput_PIN_Require() -> String 
	{ 
		return "Mật khẩu thanh toán gồm %i chữ số" 
	}

	public static func string_MessageInput_Phone_Invalid() -> String 
	{ 
		return "Số điện thoại không hợp lệ" 
	}

	public static func string_MessageInput_Phone_Empty() -> String 
	{ 
		return "Bạn chưa nhập số điện thoại" 
	}

	public static func string_Settings_Protect_Account() -> String 
	{ 
		return "Thiết lập bảo vệ tài khoản" 
	}

	public static func string_ChangePIN_Title() -> String 
	{ 
		return "Tạo lại mật khẩu thanh toán" 
	}

	public static func string_ChangePIN_Message_Success() -> String 
	{ 
		return "Đổi mật khẩu thanh toán thành công" 
	}

	public static func string_ChangePIN_Title_OldPIN() -> String 
	{ 
		return "Nhập mật khẩu thanh toán cũ" 
	}

	public static func string_ChangePIN_Title_NewPIN() -> String 
	{ 
		return "Nhập mật khẩu thanh toán mới" 
	}

	public static func string_ChangePIN_Message_Duplicate() -> String 
	{ 
		return "Mật khẩu thanh toán mới và cũ phải khác nhau" 
	}

	public static func string_ChangePIN_Note() -> String 
	{ 
		return "<p>Quên mật khẩu thanh toán vui lòng liên hệ <a href='zp-app://support'>1900 54 54 36</a></p>" 
	}

	public static func string_UpdateProfile2_SuccessMessage() -> String 
	{ 
		return "Thông tin của bạn đã được cập nhật thành công" 
	}

	public static func string_UpdateProfile2_Information() -> String 
	{ 
		return "Tạo mật khẩu để thanh toán bằng số dư hoặc thẻ đã liên kết" 
	}

	public static func string_Force_Update_App_Message() -> String 
	{ 
		return "Phiên bản ZaloPay của bạn không còn được hỗ trợ. Vui lòng cập nhật phiên bản mới nhất để tiếp tục sử dụng." 
	}

	public static func string_Notice_Update_App_Message() -> String 
	{ 
		return "Ứng dụng ZaloPay đã có phiên bản mới. Vui lòng cập nhật để trải nghiệm những tính năng mới nhất!" 
	}

	public static func string_AutoMapCardNotify_Title() -> String 
	{ 
		return "Thông Báo" 
	}

	public static func string_AutoMapCardNotify_MapCardTitle() -> String 
	{ 
		return "Thẻ vừa thanh toán đã được liên kết vào tài khoản ZaloPay của bạn." 
	}

	public static func string_AutoMapCardNotify_UseCardTitle() -> String 
	{ 
		return "Bạn có thể sử dụng thẻ này vào các lần thanh toán sau mà không cần nhập lại số thẻ." 
	}

	public static func string_AutoMapCardNotify_SercurityTitle() -> String 
	{ 
		return "ZaloPay thực hiện theo Tiêu Chuẩn Bảo Mật PCI-DSS và không lưu thông tin thẻ của bạn." 
	}

	public static func string_AutoMapCardNotify_ManageCard() -> String 
	{ 
		return "Xem Chi Tiết" 
	}

	public static func string_NotifyJailbreak_Message() -> String 
	{ 
		return "Thiết bị đang sử dụng đã bị Jailbreak. Sử dụng ZaloPay trên thiết bị này sẽ không đảm bảo an toàn và rất có thể dẫn đến việc lộ thông tin tài khoản. ZaloPay khuyến cáo bạn không nên tiếp tục sử dụng ứng dụng ZaloPay trên thiết bị này. Nhấn nút \"Tiếp Tục\" để tiếp tục sử dụng ZaloPay." 
	}

	public static func string_PayBillSetAppId_ErrorMessage() -> String 
	{ 
		return "Có lỗi xảy ra khi lấy thông tin của ứng dụng. Vui lòng thử lại sau." 
	}

	public static func string_AutoMapCard_FeeTitle() -> String 
	{ 
		return "Miễn phí giao dịch" 
	}

	public static func string_Transfer_ZaloPayId() -> String 
	{ 
		return "ZaloPay ID" 
	}

	public static func string_Transfer_PhoneNumber() -> String 
	{ 
		return "Số ĐT" 
	}

	public static func string_Transfer_ZaloPayContact() -> String 
	{ 
		return "Danh bạ" 
	}

	public static func string_Home_PayBill() -> String 
	{ 
		return "THANH TOÁN" 
	}

	public static func string_Home_Balance() -> String 
	{ 
		return "SỐ DƯ" 
	}

	public static func string_Home_LinkCard() -> String 
	{ 
		return "NGÂN HÀNG" 
	}

	public static func string_Home_TransferMoney() -> String 
	{ 
		return "Chuyển Tiền" 
	}

	public static func string_Home_ReceiveMoney() -> String 
	{ 
		return "Nhận Tiền" 
	}

	public static func string_Home_Lixi() -> String 
	{ 
		return "Lì Xì" 
	}

	public static func string_HomeReactNative_WaitForDownloading() -> String 
	{ 
		return "Đang tải dữ liệu. Vui lòng đợi trong ít phút." 
	}

	public static func string_HomeReactNative_DownloadAgainMessage() -> String 
	{ 
		return "Tải dữ liệu bị lỗi. Bạn có muốn tải lại không ?" 
	}

	public static func string_HomeReactNative_DownloadAgain() -> String 
	{ 
		return "Tải lại" 
	}

	public static func string_HomeReactNative_DownloadSuccess() -> String 
	{ 
		return "Tải dữ liệu thành công" 
	}

	public static func string_HomeReactNative_DownloadError() -> String 
	{ 
		return "Tải dữ liệu thất bại. Bạn có muốn thử tải lại không?" 
	}

	public static func string_Home_InternetConnectionError() -> String 
	{ 
		return "Không có kết nối mạng" 
	}

	public static func string_Home_CheckInternetConnection() -> String 
	{ 
		return "Kiểm tra kết nối" 
	}

	public static func string_LeftMenu_Home() -> String 
	{ 
		return "Trang Chủ" 
	}

	public static func string_LeftMenu_Notification() -> String 
	{ 
		return "Thông Báo" 
	}

	public static func string_LeftMenu_Transaction_Title() -> String 
	{ 
		return "GIAO DỊCH" 
	}

	public static func string_LeftMenu_PayBill() -> String 
	{ 
		return "Thanh Toán" 
	}

	public static func string_LeftMenu_Recharge() -> String 
	{ 
		return "Nạp Tiền" 
	}

	public static func string_LeftMenu_Transfer() -> String 
	{ 
		return "Chuyển Tiền" 
	}

	public static func string_LeftMenu_LinkCard() -> String 
	{ 
		return "Ngân Hàng" 
	}

	public static func string_LeftMenu_Transaction_History() -> String 
	{ 
		return "Lịch Sử Thanh Toán" 
	}

	public static func string_LeftMenu_Application_Title() -> String 
	{ 
		return "HỖ TRỢ" 
	}

	public static func string_LeftMenu_FAQ() -> String 
	{ 
		return "FAQ" 
	}

	public static func string_LeftMenu_TermOfUse() -> String 
	{ 
		return "Trung Tâm Hỗ Trợ" 
	}

	public static func string_LeftMenu_Quick_Comment() -> String 
	{ 
		return "Góp Ý Nhanh" 
	}

	public static func string_LeftMenu_Support() -> String 
	{ 
		return "Liên Hệ Hỗ Trợ" 
	}

	public static func string_LeftMenu_VoucherList() -> String 
	{ 
		return "Danh Sách Quà Tặng" 
	}

	public static func string_LeftMenu_About() -> String 
	{ 
		return "Thông Tin Ứng Dụng" 
	}

	public static func string_LeftMenu_SigOut() -> String 
	{ 
		return "Đăng Xuất" 
	}

	public static func string_LeftMenu_ZaloPayIdEmpty() -> String 
	{ 
		return "Chưa cập nhật" 
	}

	public static func string_LeftMenu_ZaloPayId() -> String 
	{ 
		return "ZaloPay ID: %@" 
	}

	public static func string_LinkCard_Title() -> String 
	{ 
		return "LIÊN KẾT THẺ" 
	}

	public static func string_LinkBank_Title() -> String 
	{ 
		return "LIÊN KẾT TÀI KHOẢN" 
	}

	public static func string_UpdateProfileLevel2TermOfUse() -> String 
	{ 
		return "<font color='#727f8c'>Tôi đã đọc và đồng ý với <a href='termsOfUse'>thoả thuận sử dụng</a> của ZaloPay.</font>" 
	}

	public static func string_UpdateProfileLevel2_InputMK() -> String 
	{ 
		return "Nhập mật khẩu thanh toán" 
	}

	public static func string_UpdateProfileLevel2_ShowMK() -> String 
	{ 
		return "Hiển thị mật khẩu thanh toán" 
	}

	public static func string_UpdateProfileLevel2_InputPhone() -> String 
	{ 
		return "Nhập số điện thoại" 
	}

	public static func string_UpdateProfileLevel2_ZaloPayIdContain() -> String 
	{ 
		return "ZaloPay ID bao gồm:" 
	}

	public static func string_UpdateProfileLevel2_ZaloPayIdLength() -> String 
	{ 
		return "Độ dài từ 4-24 ký tự (gồm chữ và số)" 
	}

	public static func string_UpdateProfileLevel2_Character() -> String 
	{ 
		return "Không phân biệt chữ hoa, thường" 
	}

	public static func string_UpdateProfileLevel2_Rule() -> String 
	{ 
		return "Chỉ được đăng kí một lần duy nhất" 
	}

	public static func string_UpdateProfileLevel2_Title() -> String 
	{ 
		return "Thông tin cá nhân" 
	}

	public static func string_UpdateProfileLevel3_Email() -> String 
	{ 
		return "Email" 
	}

	public static func string_UpdateProfileLevel3_CMND() -> String 
	{ 
		return "CMND/Hộ chiếu" 
	}

	public static func string_UpdateProfileLevel3_TermOfUse() -> String 
	{ 
		return "<font color='#727f8c'>Tôi đã đọc và đồng ý với <a href='termsOfUse'>thoả thuận sử dụng</a> của ZaloPay</font>" 
	}

	public static func string_UpdateProfileLevel3_Update_Title() -> String 
	{ 
		return "Cập nhật" 
	}

	public static func string_UpdateProfileLevel3_Email_Invalid() -> String 
	{ 
		return "Email không hợp lệ." 
	}

	public static func string_UpdateProfileLevel3_Invalid_Front_IdentifierPhoto() -> String 
	{ 
		return "Vui lòng cập nhật ảnh mặt trước CMND/Hộ chiếu." 
	}

	public static func string_UpdateProfileLevel3_Invalid_BackSize_IdentifierPhoto() -> String 
	{ 
		return "Vui lòng cập nhật ảnh mặt sau CMND/Hộ chiếu." 
	}

	public static func string_UpdateProfileLevel3_Invalid_Avatar() -> String 
	{ 
		return "Vui lòng cập nhật hình đại diện." 
	}

	public static func string_UpdateProfileLevel3_UploadSuccess_Message() -> String 
	{ 
		return "Thông tin của bạn đã được gửi đi và đang chờ duyệt." 
	}

	public static func string_UpdateProfileLevel3_Invalid_EmailLength() -> String 
	{ 
		return "Email không được vượt quá %d kí tự" 
	}

	public static func string_UpdateProfileLevel3_Invalid_IdentifierLength() -> String 
	{ 
		return "CMND/Hộ chiếu không được vượt quá %d kí tự" 
	}

	public static func string_UpdateProfileLevel2_SuccessMessage() -> String 
	{ 
		return "Cập nhật số điện thoại thành công. Hãy cập nhật Email và CMND để có thể rút tiền." 
	}

	public static func string_Withdraw_NotEnoughMoney() -> String 
	{ 
		return "Số dư không đủ" 
	}

	public static func string_WithdrawCondition_Title_Requiment() -> String 
	{ 
		return "Để sử dụng tính năng Rút tiền, bạn cần liên kết đến một trong các ngân hàng sau" 
	}

	public static func string_WithdrawCondition_Connect_Now() -> String 
	{ 
		return "Liên Kết Ngay" 
	}

	public static func string_WithdrawCondition_Title() -> String 
	{ 
		return "Điều kiện rút tiền" 
	}

	public static func string_WithdrawCondition_Info() -> String 
	{ 
		return "THÔNG TIN CÁ NHÂN" 
	}

	public static func string_WithdrawCondition_Update() -> String 
	{ 
		return "Cập nhật" 
	}

	public static func string_WithdrawCondition_Phone() -> String 
	{ 
		return "Số điện thoại" 
	}

	public static func string_WithdrawCondition_Password() -> String 
	{ 
		return "Mật khẩu thanh toán" 
	}

	public static func string_WithdrawCondition_UpdateInfo() -> String 
	{ 
		return "Cần cập nhật đầy đủ thông tin cá nhân sau:" 
	}

	public static func string_WithdrawCondition_LinkCard() -> String 
	{ 
		return "LIÊN KẾT THẺ" 
	}

	public static func string_WithdrawCondition_AddCard() -> String 
	{ 
		return "Thêm" 
	}

	public static func string_WithdrawCondition_LinkCardTitle() -> String 
	{ 
		return "Cần liên kết một trong các thẻ ATM nội địa:" 
	}

	public static func string_WithdrawCondition_NeedMoreCondition() -> String 
	{ 
		return "Bạn chưa đủ điều kiện để rút tiền" 
	}

	public static func string_WithdrawCondition_NeedUpdateMoreInfo() -> String 
	{ 
		return "Để sử dụng được tính năng rút tiền, bạn cần phải cập nhật các thông tin sau:" 
	}

	public static func string_ZaloPayId_NotAvalaibale() -> String 
	{ 
		return "Chưa cập nhật" 
	}

	public static func string_WalletAction_Title() -> String 
	{ 
		return "Số dư TK ZaloPay" 
	}

	public static func string_WalletAction_MoreAbout() -> String 
	{ 
		return " Tìm hiểu thêm về ZaloPay" 
	}

	public static func string_WalletAction_Rechager() -> String 
	{ 
		return "Nạp Tiền" 
	}

	public static func string_WalletAction_Rechager_Description() -> String 
	{ 
		return "Từ ngân hàng liên kết vào TK ZaloPay" 
	}

	public static func string_WalletAction_Withdraw_Description() -> String 
	{ 
		return "Từ TK ZaloPay về ngân hàng liên kết" 
	}

	public static func string_WalletAction_Withdraw() -> String 
	{ 
		return "Rút Tiền" 
	}

	public static func string_WalletAction_Withdraw_Not_Update_ZaloPayID() -> String 
	{ 
		return "ZaloPay ID: Chưa cập nhật" 
	}

	public static func string_WalletAction_Ballance() -> String 
	{ 
		return "Số dư của tôi (VND)" 
	}

	public static func string_WalletAction_ZaloPayId() -> String 
	{ 
		return "ZaloPay ID" 
	}

	public static func string_Login_Slogan() -> String 
	{ 
		return "Thanh toán trong 2 giây" 
	}

	public static func string_Login_Button_Title() -> String 
	{ 
		return "Đăng nhập với Zalo" 
	}

	public static func string_Login_AcceptCard() -> String 
	{ 
		return "Chấp nhận thẻ" 
	}

	public static func string_Login_Zalo_Error() -> String 
	{ 
		return "Có lỗi xảy ra trong quá trình đăng nhập qua Zalo.\n Vui lòng thử lại sau." 
	}

	public static func string_Login_VCB_Wrong_Input() -> String 
	{ 
		return "Tên truy cập hoặc mật khẩu không chính xác.\nLưu ý: Dịch vụ sẽ bị tạm khóa nếu nhập sai mật khẩu quá 5 lần." 
	}

	public static func string_Recharge_Button_Title() -> String 
	{ 
		return "Nạp Tiền" 
	}

	public static func string_Recharge_Min_Value() -> String 
	{ 
		return "Tối thiểu %@" 
	}

	public static func string_Recharge_Min_Multiple_Value() -> String 
	{ 
		return "Tối thiểu từ %@ và là bội số của %@" 
	}

	public static func string_Recharge_Condition() -> String 
	{ 
		return "Số tiền phải là bội số của 10.000 VND" 
	}

	public static func string_MapCard_Message() -> String 
	{ 
		return "Xác thực liên kết thẻ" 
	}

	public static func string_LinkCard_Remove_Success() -> String 
	{ 
		return "Huỷ thẻ thành công" 
	}

	public static func string_LinkCard_Remove_Error() -> String 
	{ 
		return "Huỷ thẻ thất bại" 
	}

	public static func string_LinkCard_Confirm_Remove() -> String 
	{ 
		return "Bạn có chắc chắn muốn huỷ thẻ không?" 
	}

	public static func string_LinkCard_ManagaCard() -> String 
	{ 
		return "Ngân hàng" 
	}

	public static func string_LinkCard_Phone_Notice() -> String 
	{ 
		return "Để liên kết thành công, số điện thoại đăng ký TK %@ phải là %@" 
	}

	public static func string_LinkCard_Maintain_Message() -> String 
	{ 
		return "Ngân hàng đang bảo trì. Vui lòng quay lại sau ít phút" 
	}

	public static func string_LinkCard_Maintain_Status() -> String 
	{ 
		return "Đang bảo trì" 
	}

	public static func string_LinkCard_NotSupport_Status() -> String 
	{ 
		return "Không hỗ trợ" 
	}

	public static func string_LinkCard_NotSupport_CreditCard() -> String 
	{ 
		return "Chỉ hỗ trợ thẻ Debit" 
	}

	public static func string_MainProfile_UpdateProfileLevel2Message() -> String 
	{ 
		return "Vui lòng nhập số điện thoại trước khi cập nhật email và CMND/Hộ chiếu." 
	}

	public static func string_PayMerchantBill_Title() -> String 
	{ 
		return "Thanh toán đơn hàng" 
	}

	public static func string_PayMerchantBill_Pay_Button() -> String 
	{ 
		return "Thanh toán" 
	}

	public static func string_UpdateAccountName_Description() -> String 
	{ 
		return "ZaloPay ID để nhận/chuyển khoản cho bạn bè nhanh nhất" 
	}

	public static func string_UpdateAccountName_ZaloPayId() -> String 
	{ 
		return "ZaloPay ID" 
	}

	public static func string_UpdateAccountName_Title() -> String 
	{ 
		return "Cập nhật ZaloPay ID" 
	}

	public static func string_UpdateAccountName_Success_Message() -> String 
	{ 
		return "Cập nhật ZaloPay ID thành công" 
	}

	public static func string_UpdateAccountName_Check() -> String 
	{ 
		return "Kiểm Tra" 
	}

	public static func string_UpdateAccountName_Register() -> String 
	{ 
		return "Đăng Ký" 
	}

	public static func string_TransferMoney_Title() -> String 
	{ 
		return "Danh bạ Zalo" 
	}

	public static func string_TransferMoney_RecentTransaction() -> String 
	{ 
		return "GIAO DỊCH GẦN ĐÂY" 
	}

	public static func string_TransferMoney_ZaloPayFriend() -> String 
	{ 
		return "BẠN BÈ SỬ DỤNG ZALOPAY" 
	}

	public static func string_TransferMoney_Friend() -> String 
	{ 
		return "BẠN BÈ CHƯA SỬ DỤNG ZALOPAY" 
	}

	public static func string_TransferMoney_Warning() -> String 
	{ 
		return "%@ chưa sử dụng ZaloPay. Mời %@ dùng ngay để chuyển tiền miễn phí." 
	}

	public static func string_TransferMoney_WarningTitle() -> String 
	{ 
		return "Thông báo" 
	}

	public static func string_TransferToAccount_TextPlaceHolder() -> String 
	{ 
		return "ZaloPay ID" 
	}

	public static func string_TransferToAccount_CurrentUserAccountMessage() -> String 
	{ 
		return "Không thể chuyển tiền đến tài khoản của chính bạn." 
	}

	public static func string_TransferHome_EmptyZaloPayId() -> String 
	{ 
		return "Chưa cập nhật" 
	}

	public static func string_TransferHome_ToZaloPayID() -> String 
	{ 
		return "ZaloPay ID" 
	}

	public static func string_TransferHome_ToZaloPayContact() -> String 
	{ 
		return "Danh bạ" 
	}

	public static func string_TransferHome_ToPhoneNumber() -> String 
	{ 
		return "Số điện thoại" 
	}

	public static func string_TransferHome_DescriptionToPhoneNumber() -> String 
	{ 
		return "Chuyển tiền qua số điện thoại" 
	}

	public static func string_TransferHome_DescriptionToZaloPayContact() -> String 
	{ 
		return "Chuyển tiền cho bạn bè trong danh bạ" 
	}

	public static func string_TransferHome_DescriptionToZaloPayID() -> String 
	{ 
		return "Chuyển tiền qua ZaloPay ID" 
	}

	public static func string_TransferHome_Title() -> String 
	{ 
		return "Chuyển tiền" 
	}

	public static func string_TransferHome_BottomTitle() -> String 
	{ 
		return "Bạn có thể dùng tính năng Chuyển tiền để trả tiền hoặc gửi tiền cho bạn bè" 
	}

	public static func string_TransferReceive_AmountPlaceHolder() -> String 
	{ 
		return "Số tiền (VND)" 
	}

	public static func string_TransferReceive_MessagePlaceHolder() -> String 
	{ 
		return "Lời nhắn" 
	}

	public static func string_TransferReceive_GetInforErrorMessage() -> String 
	{ 
		return "Không lấy được thông tin của người này. Vui lòng thử lại sau." 
	}

	public static func string_TransferReceive_EmptyZaloPayId() -> String 
	{ 
		return "Chưa cập nhật" 
	}

	public static func string_TransferReceive_ZaloPayId() -> String 
	{ 
		return "ZaloPay ID: %@" 
	}

	public static func string_Jailbreak_Warning_Title() -> String 
	{ 
		return "Cảnh báo" 
	}

	public static func string_JailBreak_JailbreakDevice() -> String 
	{ 
		return "Thiết bị đang sử dụng đã bị Jailbreak" 
	}

	public static func string_JailBreak_WarningMessage1() -> String 
	{ 
		return "Sử dụng ZaloPay trên thiết bị này sẽ không đảm bảo an toàn và có thể dẫn đến việc lộ thông tin tài khoản." 
	}

	public static func string_JailBreak_WarningMessage2() -> String 
	{ 
		return "ZaloPay khuyến cáo không nên tiếp tục sử dụng ZaloPay trên thiết bị này. Nhấn nút \"Tiếp Tục\" để sử dụng ZaloPay?" 
	}

	public static func string_JailBreak_DoNotAskAgain() -> String 
	{ 
		return "Không hỏi lại cho những lần sau" 
	}

	public static func string_Intro_Title_1() -> String 
	{ 
		return "ĐĂNG NHẬP VỚI ZALO" 
	}

	public static func string_Intro_Description_1() -> String 
	{ 
		return "Không cần đăng ký mới\n Đăng nhập dễ dàng với tài khoản Zalo.\n" 
	}

	public static func string_Intro_Title_2() -> String 
	{ 
		return "TỰ ĐẶT ZALOPAY ID" 
	}

	public static func string_Intro_Description_2() -> String 
	{ 
		return "ZaloPay ID dùng để chuyển khoản hay nhận tiền giống như tài khoản ngân hàng." 
	}

	public static func string_Intro_Title_3() -> String 
	{ 
		return "NHIỀU DỊCH VỤ TIỆN ÍCH" 
	}

	public static func string_Intro_Description_3() -> String 
	{ 
		return "Hỗ trợ thanh toán nhiều dịch vụ thiết yếu như tiền điện, tiền nước, Internet, nạp tiền điện thoại, ..." 
	}

	public static func string_Intro_Title_4() -> String 
	{ 
		return "LÌ XÌ" 
	}

	public static func string_Intro_Description_4() -> String 
	{ 
		return "Lì xì cho cả nhóm hoặc cá nhân\n Tăng niềm vui bằng cách lì xì số tiền ngẫu nhiên hay bằng nhau." 
	}

	public static func string_Intro_Title_5() -> String 
	{ 
		return "THANH TOÁN TRONG 2 GIÂY" 
	}

	public static func string_Intro_Description_5() -> String 
	{ 
		return "Giao dịch siêu nhanh, ổn định, thành công cao." 
	}

	public static func string_TransferQRCode_Delete() -> String 
	{ 
		return "Xóa" 
	}

	public static func string_TransferQRCode_Money() -> String 
	{ 
		return "Số tiền" 
	}

	public static func string_TransferQRCode_ErrorMessage() -> String 
	{ 
		return "Có lỗi khi tạo mã QR" 
	}

	public static func string_TransferQRCode_Title() -> String 
	{ 
		return "Thiết lập số tiền" 
	}

	public static func string_TransferQRCode_AmountPlaceHolder() -> String 
	{ 
		return "Số tiền (VND)" 
	}

	public static func string_TransferQRCode_MessagePlaceHolder() -> String 
	{ 
		return "Lời nhắn" 
	}

	public static func string_TransferQRCode_ReceiveMoneTitle() -> String 
	{ 
		return "Nhận tiền" 
	}

	public static func string_TransferQRCode_TransferLoading() -> String 
	{ 
		return "Đang chuyển tiền..." 
	}

	public static func string_TransferQRCode_TransferSuccess() -> String 
	{ 
		return "Chuyển thành công" 
	}

	public static func string_TransferQRCode_TransferFail() -> String 
	{ 
		return "Chuyển thất bại" 
	}

	public static func string_TransferQRCode_TransferCancel() -> String 
	{ 
		return "Hủy chuyển tiền" 
	}

	public static func string_TransferQRCode_Total() -> String 
	{ 
		return "Tổng cộng" 
	}

	public static func string_MapCard_BankSupport() -> String 
	{ 
		return "NGÂN HÀNG HỖ TRỢ" 
	}

	public static func string_MapCard_AddCard() -> String 
	{ 
		return "Thêm Thẻ" 
	}

	public static func string_QRPay_Title() -> String 
	{ 
		return "Thanh toán" 
	}

	public static func string_QRPay_AskPermissionMessage() -> String 
	{ 
		return "Để sử dụng tính năng này, vui lòng cho phép ZaloPay truy cập camera của bạn." 
	}

	public static func string_QRPay_AskPermissionEnable() -> String 
	{ 
		return "Cho phép" 
	}

	public static func string_QRPay_ErrorTitle() -> String 
	{ 
		return "Mã QR không hợp lệ" 
	}

	public static func string_QRPay_ErrorMessage() -> String 
	{ 
		return "Mã QR không hợp lệ. Vui lòng cung cấp lại mã QR." 
	}

	public static func string_QRPay_ScanOrther() -> String 
	{ 
		return "Quét mã QR khác" 
	}

	public static func string_QRPay_ScanToPay() -> String 
	{ 
		return "Quét mã QR" 
	}

	public static func string_QRPay_ScanToPayGuide() -> String 
	{ 
		return "Hướng camera vào mã QR để quét thanh toán" 
	}

	public static func string_QRPay_CodePay() -> String 
	{ 
		return "Mã thanh toán" 
	}

	public static func string_InternetConnection_Title() -> String 
	{ 
		return "Hướng dẫn kết nối mạng" 
	}

	public static func string_InternetConnection_Wifi() -> String 
	{ 
		return "<font size=\"16\" color=\"#24272b\"><b>Bật Wi-Fi</b>: chọn <b>\"Settings\"</b> (cài đặt) - <b>\"Wi-Fi\"</b> và chọn mạng Wi-Fi có sẵn.</font>" 
	}

	public static func string_InternetConnection_3G() -> String 
	{ 
		return "<font size=\"16\" color=\"#24272b\"><b>Bật 3G</b>: chọn <b>\"Settings\"</b> (cài đặt) - <b>\"Cellular\"</b> (di động) và bật <b>\"Cellular Data\"</b> (dữ liệu di động).</font>" 
	}

	public static func string_InternetConnection_NoInternetTitle() -> String 
	{ 
		return "Không có kết nối mạng.\nHãy bật Wi-Fi hoặc 3G." 
	}

	public static func string_InternetConnection_Guide() -> String 
	{ 
		return "VUI LÒNG LÀM THEO HƯỚNG DẪN:" 
	}

	public static func string_InvitationCode_NoCode() -> String 
	{ 
		return "Bạn chưa nhập mã" 
	}

	public static func string_InvitationCode_Support() -> String 
	{ 
		return "Nhập mã kích hoạt ứng dụng.\nĐể có mã kích hoạt, liên hệ hotro@zalopay.vn" 
	}

	public static func string_InvitationCode_Error() -> String 
	{ 
		return "Bạn đã nhập sai mã quá 5 lần. Vui lòng đợi 1 phút để nhập lại mã." 
	}

	public static func string_NetworkError_NoConnectionMessage() -> String 
	{ 
		return "Vui lòng kiểm tra kết nối mạng." 
	}

	public static func string_NetworkError_ConnectionTimeOut() -> String 
	{ 
		return "Thời gian kết nối đến máy chủ đã hết. Vui lòng thử lại sau." 
	}

	public static func string_NetworkError_TryAgainMessage() -> String 
	{ 
		return "Có lỗi xảy ra. Vui lòng thử lại sau." 
	}

	public static func string_Dialog_SuccessTitle() -> String 
	{ 
		return "Thành công" 
	}

	public static func string_Dialog_FailTitle() -> String 
	{ 
		return "Thất bại" 
	}

	public static func string_Dialog_NewVersion() -> String 
	{ 
		return "Phiên bản mới" 
	}

	public static func string_Dialog_Warning() -> String 
	{ 
		return "Cảnh báo" 
	}

	public static func string_Dialog_Notification() -> String 
	{ 
		return "Thông báo" 
	}

	public static func string_Dialog_Confirm() -> String 
	{ 
		return "Xác nhận" 
	}

	public static func string_KeyboardManage_Done() -> String 
	{ 
		return "Xong" 
	}

	public static func string_ReactNative_ExceptionMessage() -> String 
	{ 
		return "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau." 
	}

	public static func string_QRReceive_StartScan() -> String 
	{ 
		return "%@ đã quét mã QR" 
	}

	public static func string_QRReceive_SendSuccess() -> String 
	{ 
		return "%@ gửi tiền thành công" 
	}

	public static func string_QRReceive_SendFail() -> String 
	{ 
		return "%@ gửi tiền thất bại" 
	}

	public static func string_QRReceive_Cancel() -> String 
	{ 
		return "%@ hủy gửi tiền" 
	}

	public static func string_ReceiveInput_Confirm() -> String 
	{ 
		return "Xác Nhận" 
	}

	public static func string_MainProfile_Email() -> String 
	{ 
		return "Email" 
	}

	public static func string_MainProfile_Waiting() -> String 
	{ 
		return "Đang chờ duyệt" 
	}

	public static func string_MainProfile_CMND() -> String 
	{ 
		return "CMND/Hộ chiếu" 
	}

	public static func string_MainProfile_Phone() -> String 
	{ 
		return "Số điện thoại" 
	}

	public static func string_MainProfile_ZaloPayId() -> String 
	{ 
		return "ZaloPay ID" 
	}

	public static func string_MainProfile_BirthDay() -> String 
	{ 
		return "Ngày sinh" 
	}

	public static func string_MainProfile_Gender() -> String 
	{ 
		return "Giới tính" 
	}

	public static func string_MainProfile_Male() -> String 
	{ 
		return "Nam" 
	}

	public static func string_MainProfile_RealName() -> String 
	{ 
		return "Tên thật" 
	}

	public static func string_MainProfile_AccountType() -> String 
	{ 
		return "Loại tài khoản" 
	}

	public static func string_MainProfile_AccountType_NotVerified() -> String 
	{ 
		return "Chưa định danh" 
	}

	public static func string_MainProfile_AccountType_Verified() -> String 
	{ 
		return "Đã định danh" 
	}

	public static func string_MainProfile_AccountType_Wating() -> String 
	{ 
		return "Đang chờ duyệt" 
	}

	public static func string_MainProfile_AccountInfoTitle() -> String 
	{ 
		return "Thông tin tài khoản" 
	}

	public static func string_MainProfile_Female() -> String 
	{ 
		return "Nữ" 
	}

	public static func string_MainProfile_Id() -> String 
	{ 
		return "Mã định danh" 
	}

	public static func string_MainProfile_Content_BirthDay_Gender() -> String 
	{ 
		return "Chỉnh sửa Ngày sinh và Giới tính tại ứng dụng Zalo" 
	}

	public static func string_MainProfile_Content_Phone_ID() -> String 
	{ 
		return "Cập nhật Số điện thoại và ZaloPay ID để thực hiện các giao dịch chuyển tiền, rút tiền và lì xì cho bạn bè" 
	}

	public static func string_MainProfile_Content_Email_CMND() -> String 
	{ 
		return "Cập nhật Email và CMND/Hộ chiếu để tăng hạn mức tối đa các giao dịch" 
	}

	public static func string_MainProfile_Update_Address_Note() -> String 
	{ 
		return "Nhập thông tin bên dưới để giao dịch và thanh toán được an toàn" 
	}

	public static func string_MainProfile_Update_Address_Description() -> String 
	{ 
		return "Tôi đã đọc và đồng ý với %@ của ZaloPay" 
	}

	public static func string_MainProfile_Update_Address_Term() -> String 
	{ 
		return "thoả thuận sử dụng" 
	}

	public static func string_MainProfile_Update_Address_Title() -> String 
	{ 
		return "Địa chỉ" 
	}

	public static func string_MainProfile_Update_Address_Error_Maximum() -> String 
	{ 
		return "Địa chỉ không được vượt quá 160 ký tự." 
	}

	public static func string_Setting_TouchId_Description() -> String 
	{ 
		return "Xác thực bằng vân tay thay vì sử dụng mật khẩu thanh toán.\nLưu ý: Bất cứ vân tay nào đã đăng ký trong hệ thống đều xác thực được khi thanh toán." 
	}

	public static func string_Setting_TouchId_Title() -> String 
	{ 
		return "Sử dụng TouchID" 
	}

	public static func string_Setting_FaceId_Description() -> String 
	{ 
		return "Xác thực bằng Face ID thay vì sử dụng mật khẩu thanh toán. Lưu ý: Bất cứ ai có khuôn mặt giống bạn đều có nguy cơ xác thực được khi thanh toán." 
	}

	public static func string_Setting_FaceId_Title() -> String 
	{ 
		return "Sử dụng Face ID" 
	}

	public static func string_Setting_Ask_Password_Description() -> String 
	{ 
		return "Hỏi mật khẩu hoặc xác thực vân tay mỗi khi truy xuất danh sách liên kết Ngân Hàng" 
	}

	public static func string_Setting_Ask_Password_Title() -> String 
	{ 
		return "Bảo vệ thông tin cá nhân" 
	}

	public static func string_Setting_Update_Password_Description() -> String 
	{ 
		return "Mật khẩu dùng để xác thực mỗi khi thực hiện thanh toán" 
	}

	public static func string_Setting_Update_Password_Title() -> String 
	{ 
		return "Đổi mật khẩu" 
	}

	public static func string_Setting_TouchID_NotAvailable() -> String 
	{ 
		return "Thiết bị của bạn không hỗ trợ TouchID" 
	}

	public static func string_Setting_Manage_Login_Device_Description() -> String 
	{ 
		return "Liệt kê các thiết bị hiện tại đang thực hiện đăng nhập bằng tài khoản ZaloPay này" 
	}

	public static func string_Setting_Manage_Login_Device_Title() -> String 
	{ 
		return "Danh sách thiết bị đăng nhập" 
	}

	public static func string_Setting_Authentication_Open_Title() -> String 
	{ 
		return "Yêu cầu xác thực khi mở ứng dụng" 
	}

	public static func string_Setting_Authentication_Open_Description() -> String 
	{ 
		return "Yêu cầu xác thực bằng mật khẩu hoặc vân tay đẻ mở ứng dụng. Khuyến cáo bật tùy chọn này để tăng tính bảo mật." 
	}

	public static func string_Setting_Authentication_Enter_Password() -> String 
	{ 
		return "NHẬP MẬT KHẨU XÁC THỰC" 
	}

	public static func string_Setting_Authentication_Enter_Password_Description() -> String 
	{ 
		return "Lưu ý: Mật khẩu này không phải mã PIN thẻ/tài khoản ngân hàng." 
	}

	public static func string_Setting_Authentication_Forget_Password() -> String 
	{ 
		return "Quên mật khẩu?" 
	}

	public static func string_Setting_Authentication_Forget_Password_Content() -> String 
	{ 
		return "Để đặt lại mật khẩu, vui lòng liên hệ Hotline 1900 54 54 36. Bạn có muốn tiếp tục không?" 
	}

	public static func string_Setting_Authentication_Login_By_TouchID() -> String 
	{ 
		return "Chạm phím Home để truy cập" 
	}

	public static func string_Setting_Splash_Screen_Title() -> String 
	{ 
		return "Lớp phủ màn hình đa nhiệm" 
	}

	public static func string_Setting_Splash_Screen_Description() -> String 
	{ 
		return "Khi tùy chọn này được bật, người khác sẽ không thể thấy được nội dung hiển thị trên màn hình của ứng dụng của ZaloPay. Khuyến cáo bật tùy chọn này để tăng tính bảo mật." 
	}

	public static func string_Setting_Auto_Lock_Title() -> String 
	{ 
		return "Tùy chỉnh thời gian tự động khóa" 
	}

	public static func string_Setting_Auto_Lock_Description() -> String 
	{ 
		return "Ngay sau khi ẩn ứng dụng" 
	}

	public static func string_Setting_Auto_Lock_Menu_Title() -> String 
	{ 
		return "Thời gian" 
	}

	public static func string_Setting_Auto_Lock_Cancel_Title() -> String 
	{ 
		return "Đóng" 
	}

	public static func string_Setting_Auto_Lock_Right_Now_Title() -> String 
	{ 
		return "Ngay lập tức" 
	}

	public static func string_Setting_Auto_Lock_After_Second_Title() -> String 
	{ 
		return "%@ giây" 
	}

	public static func string_Setting_Auto_Lock_After_Minute_Title() -> String 
	{ 
		return "%@ phút" 
	}

	public static func string_TouchID_Message() -> String 
	{ 
		return "Chạm nút Home để xác thực" 
	}

	public static func string_TouchID_Input_Password() -> String 
	{ 
		return "Nhập mật khẩu" 
	}

	public static func string_TouchID_Authen_Message() -> String 
	{ 
		return "Nhập mật khẩu thanh toán để xác nhận" 
	}

	public static func string_TouchID_Confirm_Pay_Message() -> String 
	{ 
		return "Chạm phím Home để xác nhận thanh toán" 
	}

	public static func string_Setting_Ask_Password_Description_WithOutTouchID() -> String 
	{ 
		return "Hỏi mật khẩu mỗi khi truy xuất danh sách liên kết Ngân Hàng" 
	}

	public static func string_Setting_Ask_Password_FaceID_Description() -> String 
	{ 
		return "Hỏi mật khẩu hoặc xác thực bằng Face ID mỗi khi truy xuất danh sách liên kết Ngân Hàng" 
	}

	public static func string_Setting_TouchID_RemoveTouchID() -> String 
	{ 
		return "Bạn có muốn tắt thiết lập bảo vệ bằng vân tay" 
	}

	public static func string_Setting_Remove_Ask_Password() -> String 
	{ 
		return "Vui lòng nhập mật khẩu thanh toán để thiết lập" 
	}

	public static func string_ZaloFriend_Empty_Title() -> String 
	{ 
		return "Bạn chưa có bạn bè trên Zalo." 
	}

	public static func string_Feedback_Categorys() -> String 
	{ 
		return "Phân loại" 
	}

	public static func string_Feedback_Withdraw() -> String 
	{ 
		return "Rút Tiền" 
	}

	public static func string_Feedback_Description() -> String 
	{ 
		return "Thông tin cần hỗ trợ (bắt buộc)" 
	}

	public static func string_Feedback_Error_Description_Require() -> String 
	{ 
		return "Mô tả (bắt buộc)" 
	}

	public static func string_Feedback_Error_Description() -> String 
	{ 
		return "Mô tả" 
	}

	public static func string_Feedback_Email() -> String 
	{ 
		return "Email" 
	}

	public static func string_Feedback_Email_Require() -> String 
	{ 
		return "Email (bắt buộc)" 
	}

	public static func string_Feedback_UserInfo() -> String 
	{ 
		return "Gửi kèm thông tin người dùng" 
	}

	public static func string_Feedback_DeviceInfo() -> String 
	{ 
		return "Gửi kèm thông tin thiết bị" 
	}

	public static func string_Feedback_AppInfo() -> String 
	{ 
		return "Gửi kèm thông tin ứng dụng" 
	}

	public static func string_Feedback_Contact_CSKH() -> String 
	{ 
		return "Vui lòng nhập thông tin để gửi đến bộ phận CSKH" 
	}

	public static func string_Feedback_ScreenShoot() -> String 
	{ 
		return "Đính kèm màn hình (%d/4)" 
	}

	public static func string_Feedback_Title() -> String 
	{ 
		return "Yêu cầu hỗ trợ" 
	}

	public static func string_Feedback_Select_Category() -> String 
	{ 
		return "CHỌN LOẠI BẠN CẦN HỖ TRỢ" 
	}

	public static func string_Feedback_Description_Empty() -> String 
	{ 
		return "Bạn chưa nhập thông tin cần hỗ trợ." 
	}

	public static func string_Feedback_Email_NotAvailable() -> String 
	{ 
		return "Thiết bị của bạn không hỗ trợ gửi email." 
	}

	public static func string_Feedback_Email_Title() -> String 
	{ 
		return "Phản hồi/Góp ý ZaloPay" 
	}

	public static func string_Feedback_TransactionId() -> String 
	{ 
		return "Mã giao dịch" 
	}

	public static func string_Feedback_Email_Receiver() -> String 
	{ 
		return "hotro@zalopay.vn" 
	}

	public static func string_Feedback_Message_InvalidLength() -> String 
	{ 
		return "Mô tả không được vượt quá %d kí tự" 
	}

	public static func string_Feedback_Error_Title() -> String 
	{ 
		return "Gửi báo lỗi" 
	}

	public static func string_Withdraw_Maintain_Message() -> String 
	{ 
		return "Tính năng Rút Tiền đang được bảo trì. Thời gian bảo trì từ %@ ngày %@ đến %@ ngày %@." 
	}

	public static func string_LinkCard_NoCard_Title() -> String 
	{ 
		return "Bạn chưa liên kết thẻ" 
	}

	public static func string_LinkCard_NoCard_Message() -> String 
	{ 
		return "Liên kết thẻ để thanh toán các dịch vụ được dễ dàng và nhanh chóng." 
	}

	public static func string_LinkCard_NoCard_AddCard() -> String 
	{ 
		return "   Liên Kết Thẻ" 
	}

	public static func string_LinkAccount_NoAccount_Title() -> String 
	{ 
		return "Bạn chưa liên kết tài khoản" 
	}

	public static func string_LinkAccount_NoAccount_Message() -> String 
	{ 
		return "Liên kết tài khoản ngân hàng để thanh toán các dịch vụ được dễ dàng và nhanh chóng." 
	}

	public static func string_LinkAccount_NoAccount_AddAcount() -> String 
	{ 
		return "   Liên Kết Tài Khoản" 
	}

	public static func string_LinkAccount_Select_Bank_To_Link_Account() -> String 
	{ 
		return "Chọn ngân hàng để liên kết tài khoản:" 
	}

	public static func string_LinkAccount_Phone() -> String 
	{ 
		return "Điện thoại liên kết" 
	}

	public static func string_LinkAccount_MinVerison() -> String 
	{ 
		return "Để thực hiện %@ với %@, vui lòng cập nhật phiên bản ZaloPay mới nhất." 
	}

	public static func string_WithdrawCondition_LinkAccountTitle() -> String 
	{ 
		return "Cần liên kết một trong các tài khoản:" 
	}

	public static func string_Zalo_Scheme_SwitchAccountMessage() -> String 
	{ 
		return "Bạn đang đăng nhập tài khoản %@. Bạn có muốn đăng nhập lại ngay không?" 
	}

	public static func string_Zalo_Scheme_FriendNotUsingZaloPay() -> String 
	{ 
		return "Bạn này chưa sử dụng ZaloPay. Hãy mời bạn sử dụng ZaloPay để tiếp tục chuyển tiền." 
	}

	public static func string_Zalo_Scheme_Lixi_FriendNotUsingZaloPay() -> String 
	{ 
		return "%@ chưa đăng ký ZaloPay nên không thể nhận lì xì, vui lòng báo %@ đăng ký ZaloPay và thử lại sau." 
	}

	public static func string_Zalo_Scheme_Lixi_Default_DisplayName() -> String 
	{ 
		return "Người nhận" 
	}

	public static func string_Zalo_Scheme_SwitchAccount() -> String 
	{ 
		return "Đăng nhập" 
	}

	public static func string_Suggest_TouchID_DoNotAskAgain() -> String 
	{ 
		return "Không hỏi lại gợi ý này" 
	}

	public static func string_Suggest_TouchID_Title() -> String 
	{ 
		return "Gợi ý sử dụng TouchID" 
	}

	public static func string_Suggest_TouchID_Message() -> String 
	{ 
		return "Bạn có muốn sử dụng TouchID thay vì nhập mật khẩu thanh toán" 
	}

	public static func string_Confirm_TouchId_Payment_Later() -> String 
	{ 
		return "Bạn có muốn sử dụng xác thực bằng vân tay cho những lần thanh toán sau không?" 
	}

	public static func string_Confirm_FaceId_Payment_Later() -> String 
	{ 
		return "Bạn có muốn sử dụng xác thực bằng khuôn mặt cho những lần thanh toán sau không?" 
	}

	public static func string_Use_TouchId_Payment_Later() -> String 
	{ 
		return "Sử dụng vân tay cho lần thanh toán sau" 
	}

	public static func string_Zalo_Scheme_PayingBill() -> String 
	{ 
		return "Bạn đang có một giao dịch đang trong quá trình thanh toán. Vui lòng kết thúc thanh toán cho giao dịch đó và thử lại" 
	}

	public static func string_Message_Lock_Account_Format() -> String 
	{ 
		return "Tài khoản (%@) đang bị khóa. Vui lòng liên hệ hỗ trợ khách hàng 1900 54 54 36 để biết thêm chi tiết" 
	}

	public static func string_LinkBank_BankSupport() -> String 
	{ 
		return "NGÂN HÀNG HỖ TRỢ" 
	}

	public static func string_BankCard_Support_ByZaloPay() -> String 
	{ 
		return "Các thẻ ngân hàng được ZaloPay hỗ trợ" 
	}

	public static func string_Bank_Support_LinkCard() -> String 
	{ 
		return " Ngân hàng hỗ trợ liên kết thẻ" 
	}

	public static func string_WebApp_BottomSheet_SupportFor() -> String 
	{ 
		return "Được cung cấp bởi %@" 
	}

	public static func string_WebApp_BottomSheet_ShareTo_Zalo() -> String 
	{ 
		return "Chia sẻ với Zalo" 
	}

	public static func string_WebApp_BottomSheet_Copy_URL() -> String 
	{ 
		return "Sao chép URL" 
	}

	public static func string_WebApp_BottomSheet_Refresh() -> String 
	{ 
		return "Làm mới" 
	}

	public static func string_WebApp_BottomSheet_Open_Browser() -> String 
	{ 
		return "Mở trong trình duyệt" 
	}

	public static func string_LinkBank_BankExist() -> String 
	{ 
		return "Quý khách đã liên kết với ngân hàng %@" 
	}

	public static func string_LinkBank_BankExist_Title() -> String 
	{ 
		return "Không thể liên kết" 
	}

	public static func string_LinkBank_Minimum_Require() -> String 
	{ 
		return "và số dư tối thiểu trong TK %@ là %@ VND" 
	}

	public static func string_LinkBank_Message_Input_Password() -> String 
	{ 
		return "Nhập mật khẩu để xác nhận hủy liên kết" 
	}

	public static func string_LinkBank_BIDV_Notify_Message() -> String 
	{ 
		return "<font color='#24272b'>Để liên kết thành công, bạn cần đăng ký sử dụng dịch vụ BIDV Online / BIDV Smartbanking. \n\nNếu chưa đăng ký dịch vụ, vui lòng đăng ký tại quầy giao dịch BIDV hoặc <a href='https://ebank.bidv.com.vn/DKNHDT/dk_bidvonline.htm' style='text-decoration:underline;'>tại đây</a>.</font>" 
	}

	public static func string_LinkBank_BIDV_Accept_Title_Button() -> String 
	{ 
		return "Tôi đã đăng ký" 
	}

	public static func string_BankName_Not_Exist() -> String 
	{ 
		return "Ngân hàng không tồn tại" 
	}

	public static func string_Search_Suggest_Find_Keyword() -> String 
	{ 
		return "Nhập từ khoá để tìm kiếm ứng dụng" 
	}

	public static func string_Search_Suggest_App_Title() -> String 
	{ 
		return "ỨNG DỤNG TIÊU BIỂU" 
	}

	public static func string_Search_Result_App_Tille() -> String 
	{ 
		return "ỨNG DỤNG" 
	}

	public static func string_Search_No_Result_Description() -> String 
	{ 
		return "Vui lòng kiểm tra lại chính tả hoặc sử dụng các từ khoá tổng quát hơn" 
	}

	public static func string_Search_No_Result() -> String 
	{ 
		return "Không tìm thấy kết quả cho từ khoá \"%@\"" 
	}

	public static func string_Search_Read_All_Result() -> String 
	{ 
		return "Xem tất cả" 
	}

	public static func string_Search_Field_PlaceHolder() -> String 
	{ 
		return "Tìm kiếm" 
	}

	public static func string_Tabbar_Home() -> String 
	{ 
		return "Trang Chủ" 
	}

	public static func string_Tabbar_ShowShow() -> String 
	{ 
		return "Quanh Đây" 
	}

	public static func string_Tabbar_Transaction() -> String 
	{ 
		return "Giao Dịch" 
	}

	public static func string_Tabbar_Profile() -> String 
	{ 
		return "Cá Nhân" 
	}

	public static func string_Tabbar_Promotion() -> String 
	{ 
		return "Ưu Đãi" 
	}

	public static func string_QRPay_ErrorUnsupport() -> String 
	{ 
		return "Vui lòng cập nhật phiên bản ZaloPay mới nhất để quét QR." 
	}

	public static func string_Profile_Balance() -> String 
	{ 
		return "TK ZaloPay" 
	}

	public static func string_Profile_Bank() -> String 
	{ 
		return "Ngân Hàng" 
	}

	public static func string_LinkBank_Only_Support_Vietcombank() -> String 
	{ 
		return "Hiện tại ZaloPay chỉ hỗ trợ liên kết tài khoản Vietcombank. Liên kết tài khoản các ngân hàng khác sẽ được hỗ trợ trong thời gian tới." 
	}

	public static func string_LinkBank_Delete_Notification_Message() -> String 
	{ 
		return "Bạn cần phải đăng nhập tài khoản %@ để huỷ liên kết" 
	}

	public static func string_Http_Error_404() -> String 
	{ 
		return "Phiên bản đang chạy chưa được hỗ trợ. Vui lòng liên hệ bộ phận CSKH." 
	}

	public static func string_FetchTransactionLogsFail() -> String 
	{ 
		return "Không tải được nội dung" 
	}

	public static func string_TouchID_Enable_System_Setting() -> String 
	{ 
		return "Bạn cần bật cài đặt vân tay và mật khẩu cho máy" 
	}

	public static func string_ButtonLabel_Setting() -> String 
	{ 
		return "Cài Đặt" 
	}

	public static func string_ButtonLabel_Show() -> String 
	{ 
		return "Hiện" 
	}

	public static func string_ButtonLabel_Hide() -> String 
	{ 
		return "Ẩn" 
	}

	public static func string_Pay_Bill_Merchant_Name() -> String 
	{ 
		return "Dịch vụ" 
	}

	public static func string_Pay_Bill_Fee_Title() -> String 
	{ 
		return "Phí giao dịch" 
	}

	public static func string_Pay_Bill_Receiver() -> String 
	{ 
		return "Người nhận" 
	}

	public static func string_Pay_Bill_Fee_Free() -> String 
	{ 
		return "Miễn phí" 
	}

	public static func string_TouchID_Enable_Success_Message() -> String 
	{ 
		return "Kích hoạt vân tay thành công" 
	}

	public static func string_Payment_Method_Orther_Method() -> String 
	{ 
		return "PHƯƠNG THỨC THANH TOÁN KHÁC" 
	}

	public static func string_Onboard_Create_Password_Title() -> String 
	{ 
		return "TẠO MẬT KHẨU THANH TOÁN" 
	}

	public static func string_Onboard_KYC_Phone() -> String 
	{ 
		return "Số điện thoại" 
	}

	public static func string_Onboard_KYC_Hint_Phone() -> String 
	{ 
		return "Số điện thoại" 
	}

	public static func string_Onboard_KYC_Empty_Phone() -> String 
	{ 
		return "Bạn chưa nhập Số điện thoại" 
	}

	public static func string_Onboard_KYC_Invalid_Phone() -> String 
	{ 
		return "Số điện thoại không hợp lệ" 
	}

	public static func string_Onboard_KYC_Name() -> String 
	{ 
		return "Họ và tên" 
	}

	public static func string_Onboard_KYC_Hint_Name() -> String 
	{ 
		return "Họ tên thật, có dấu, viết cách" 
	}

	public static func string_Onboard_KYC_Empty_Name() -> String 
	{ 
		return "Bạn chưa nhập Họ tên" 
	}

	public static func string_Onboard_KYC_Invalid_Name() -> String 
	{ 
		return "Họ tên không hợp lệ" 
	}

	public static func string_Onboard_KYC_DOB() -> String 
	{ 
		return "Ngày sinh" 
	}

	public static func string_Onboard_KYC_Hint_DOB() -> String 
	{ 
		return "Ngày sinh" 
	}

	public static func string_Onboard_KYC_Invalid_DOB() -> String 
	{ 
		return "Bạn cần trên 15 tuổi" 
	}

	public static func string_Onboard_KYC_Gender() -> String 
	{ 
		return "Giới tính" 
	}

	public static func string_Onboard_KYC_Identify() -> String 
	{ 
		return "Số CMND" 
	}

	public static func string_Onboard_KYC_Hint_Identify() -> String 
	{ 
		return "Số CMND" 
	}

	public static func string_Onboard_KYC_Number_Identify() -> String 
	{ 
		return "Số %@" 
	}

	public static func string_Onboard_KYC_Empty_Identify() -> String 
	{ 
		return "Bạn chưa nhập %@" 
	}

	public static func string_Onboard_KYC_Invalid_Identify() -> String 
	{ 
		return "%@ không hợp lệ" 
	}

	public static func string_Onboard_KYC_Rule_Identify() -> String 
	{ 
		return "CMND có 9 hoặc 12 ký tự" 
	}

	public static func string_Onboard_KYC_CMND_Identify() -> String 
	{ 
		return "CMND" 
	}

	public static func string_Onboard_KYC_Passport_Identify() -> String 
	{ 
		return "Passport" 
	}

	public static func string_Onboard_KYC_CC_Identify() -> String 
	{ 
		return "Căn cước" 
	}

	public static func string_Onboard_KYC_Phone_Match_ZaloPhone_Note() -> String 
	{ 
		return "Số điện thoại đăng ký phải trùng với số Zalo" 
	}

	public static func string_Onboard_KYC_Title_Mapcard() -> String 
	{ 
		return "Thông tin cá nhân đăng ký với ngân hàng" 
	}

	public static func string_Onboard_KYC_Message_Mapcard() -> String 
	{ 
		return "Nhập đúng thông tin bạn đã đăng ký với ngân hàng để liên kết thẻ thành công." 
	}

	public static func string_Onboard_KYC_Message_Onboarding() -> String 
	{ 
		return "Để bảo mật tài khoản, vui lòng cung cấp thông tin cá nhân của chính bạn." 
	}

	public static func string_Onboard_KYC_Message_Transaction() -> String 
	{ 
		return "Nhập đúng thông tin thật của bạn để tiếp tục thực hiện giao dịch" 
	}

	public static func string_Onboard_KYC_Message_Profile() -> String 
	{ 
		return "Cung cấp thông tin thật của bạn để định danh tài khoản" 
	}

	public static func string_Onboard_KYC_Create_Password_Note() -> String 
	{ 
		return "Mật khẩu để bảo mật tài khoản và xác nhận giao dịch khi thanh toán" 
	}

	public static func string_Onboard_KYC_Create_Password_Footer() -> String 
	{ 
		return "Lưu ý: Mật khẩu này không phải mã PIN thẻ/tài khoản ngân hàng" 
	}

	public static func string_Onboard_KYC_OTP_Send_To_Phone() -> String 
	{ 
		return "Mã xác thực đã gửi đến %@" 
	}

	public static func string_Onboard_Confirm_Password_Title() -> String 
	{ 
		return "XÁC NHẬN MẬT KHẨU" 
	}

	public static func string_Onboard_KYC_Confirm_Password_Note() -> String 
	{ 
		return "Nhập lại mật khẩu thanh toán" 
	}

	public static func string_Onboard_Create_Password_Note() -> String 
	{ 
		return "Để xác nhận giao dịch của bạn khi thanh toán" 
	}

	public static func string_Onboard_Confirm_Password_Note() -> String 
	{ 
		return "Vui lòng không chia sẻ mật khẩu với bất kì ai" 
	}

	public static func string_Onboard_Confirm_Password_Error() -> String 
	{ 
		return "Mật khẩu thanh toán không trùng khớp" 
	}

	public static func string_Onboard_Input_Phone_Title() -> String 
	{ 
		return "SỐ ĐIỆN THOẠI DI ĐỘNG" 
	}

	public static func string_Onboard_Input_Phone_Note() -> String 
	{ 
		return "Đăng ký một lần và không thay đổi" 
	}

	public static func string_Onboard_Input_Phone_Error() -> String 
	{ 
		return "Số điện thoại không hợp lệ" 
	}

	public static func string_Onboard_Input_Phone_Correct() -> String 
	{ 
		return "Số điện thoại hợp lệ" 
	}

	public static func string_Onboard_Input_OTP_Title() -> String 
	{ 
		return "XÁC THỰC SỐ ĐIỆN THOẠI" 
	}

	public static func string_Onboard_Input_OTP_Note() -> String 
	{ 
		return "Mã xác thực đã gửi đến %@" 
	}

	public static func string_Onboard_Input_OTP_Next_Title() -> String 
	{ 
		return "Xác nhận" 
	}

	public static func string_Onboard_Input_OTP_Refresh() -> String 
	{ 
		return "%@ Mã xác thực khác" 
	}

	public static func string_Onboard_Input_OTP_Refresh_Counting() -> String 
	{ 
		return "Gửi lại mã trong %02ld:%02ld" 
	}

	public static func string_Onboard_Input_OTP_Over_Limit() -> String 
	{ 
		return "Bạn đã nhập sai mã xác thực quá số lần quy định. Vui lòng đăng nhập lại để tiếp tục." 
	}

	public static func string_Onboard_Input_OTP_Error() -> String 
	{ 
		return "Mã xác thực không hợp lệ" 
	}

	public static func string_Onboard_Input_OTP_Dialog_Message() -> String 
	{ 
		return "Mã xác thực đang được gửi đến %@. Bạn có muốn tiếp tục chờ không?" 
	}

	public static func string_Onboard_Input_OTP_Resend_Success() -> String 
	{ 
		return "Đã gửi lại mã xác thực" 
	}

	public static func string_Onboard_Input_Phone_Next() -> String 
	{ 
		return "Tiếp tục" 
	}

	public static func string_Onboard_Input_OTP_Confirm() -> String 
	{ 
		return "Xác Nhận" 
	}

	public static func string_KYC_Alert() -> String 
	{ 
		return "Để tiếp tục thực hiện giao dịch, hãy cung cấp thông tin của chính bạn." 
	}

	public static func string_Withdraw_MoneyNumber_Title() -> String 
	{ 
		return "VUI LÒNG CHỌN SỐ TIỀN CẦN RÚT" 
	}

	public static func string_Withdraw_Free_Charge_Title() -> String 
	{ 
		return "Miễn phí rút tiền" 
	}

	public static func string_Withdraw_Balance() -> String 
	{ 
		return "Số dư TK ZaloPay" 
	}

	public static func string_Profile_Title() -> String 
	{ 
		return "Cá nhân" 
	}

	public static func string_Receiver() -> String 
	{ 
		return "Người nhận" 
	}

	public static func string_Withdraw_Not_Enough_Money_Info() -> String 
	{ 
		return "Số dư không đủ" 
	}

	public static func string_Payment_Method_No_Method_Support() -> String 
	{ 
		return "Không có kênh nào hỗ trợ mệnh giá này" 
	}

	public static func string_Time_Title() -> String 
	{ 
		return "Thời gian" 
	}

	public static func string_Result_Title_Processing() -> String 
	{ 
		return "Đang được xử lý" 
	}

	public static func string_Result_Title_Network_Problem() -> String 
	{ 
		return "Mạng không ổn định" 
	}

	public static func string_Atm_Map_Card_Min_Max_Message() -> String 
	{ 
		return "Ngân hàng %@ chỉ hỗ trợ thanh toán số tiền từ %@ đến %@. Vui lòng thử lại." 
	}

	public static func string_TransferMoney_Password_Title() -> String 
	{ 
		return "Nhập mật khẩu để chuyển tiền bằng" 
	}

	public static func string_Recharge_Password_Title() -> String 
	{ 
		return "Nhập mật khẩu để nạp tiền bằng" 
	}

	public static func string_Withdraw_Password_Title() -> String 
	{ 
		return "Nhập mật khẩu để rút tiền bằng" 
	}

	public static func string_App_DichVu() -> String 
	{ 
		return "Thanh toán đơn hàng" 
	}

	public static func string_Change_Password_Successful() -> String 
	{ 
		return "Đổi mật khẩu thành công" 
	}

	public static func string_Change_Password_Input_Current() -> String 
	{ 
		return "Nhập mật khẩu hiện tại" 
	}

	public static func string_Change_Password_Input_New() -> String 
	{ 
		return "Nhập mật khẩu mới" 
	}

	public static func string_Change_Password_Confirm() -> String 
	{ 
		return "Xác nhận mật khẩu mới" 
	}

	public static func string_Change_Password_Input_OTP() -> String 
	{ 
		return "Nhập mã OTP" 
	}

	public static func string_Change_Password_Input_Wrong() -> String 
	{ 
		return "Mật khẩu không trùng khớp" 
	}

	public static func string_Change_Password_Call_Support() -> String 
	{ 
		return "Quên mật khẩu vui lòng gọi " 
	}

	public static func string_Change_Password_Phone_Support() -> String 
	{ 
		return "1900 54 54 36" 
	}

	public static func string_Change_Password_Cancel_Message() -> String 
	{ 
		return "Bạn có muốn tiếp tục đổi mật khẩu không?" 
	}

	public static func string_Withdraw_Other_Number() -> String 
	{ 
		return "Số khác" 
	}

	public static func string_Withdraw_Button_Title() -> String 
	{ 
		return "Rút Tiền" 
	}

	public static func string_Method_Handler_AllMethodUnSupportCurrentBill() -> String 
	{ 
		return "<font color='#000000'>Số dư không đủ, bạn cần <a href='deposit'>Nạp thêm</a></font>" 
	}

	public static func string_Method_AddNewCardButton() -> String 
	{ 
		return "Thêm liên kết" 
	}

	public static func string_ZPC_favorite_friend() -> String 
	{ 
		return "YÊU THÍCH" 
	}

	public static func string_ZPC_guide_add_favorite() -> String 
	{ 
		return "Chọn vào ngôi sao tại tên bạn bè để thêm người đó vào danh sách Yêu thích" 
	}

	public static func string_ZPC_empty_favorite() -> String 
	{ 
		return "Chưa có bạn bè yêu thích" 
	}

	public static func string_ZPC_zalo_contact() -> String 
	{ 
		return "Danh sách bạn bè" 
	}

	public static func string_ZPC_edit_text() -> String 
	{ 
		return "Sửa" 
	}

	public static func string_ZPC_guide_search() -> String 
	{ 
		return "Nhập tên hoặc số điện thoại" 
	}

	public static func string_ZPC_favorite_text() -> String 
	{ 
		return "Ưa thích" 
	}

	public static func string_ZPC_update_contact_text() -> String 
	{ 
		return "Cập nhật danh bạ" 
	}

	public static func string_ZPC_update_contact_description() -> String 
	{ 
		return "Bạn bè từ danh bạ điện thoại và danh bạ Zalo sẽ tự động thêm vào danh bạ ZaloPay" 
	}

	public static func string_ZPC_last_update_text() -> String 
	{ 
		return "Lần cập nhật cuối" 
	}

	public static func string_ZPC_inorge_update_contact_warning() -> String 
	{ 
		return "Bỏ cập nhật danh bạ ?" 
	}

	public static func string_ZPC_inorge_update_contact_description() -> String 
	{ 
		return "Bỏ cập nhật, danh bạ điện thoại sẽ không cập nhật vào danh bạ ZaloPay. Bạn muốn tiếp tục cập nhật không?" 
	}

	public static func string_ZPC_friend_from_contact() -> String 
	{ 
		return "Bạn bè từ danh bạ điện thoại" 
	}

	public static func string_ZPC_friend_from_zalo() -> String 
	{ 
		return "Bạn bè từ danh bạ zalo" 
	}

	public static func string_ZPC_update_contact_miss() -> String 
	{ 
		return "Chưa cập nhật danh bạ" 
	}

	public static func string_ZPC_remove_friend_from_favorite() -> String 
	{ 
		return "đã bị xoá khỏi danh sách Yêu thích" 
	}

	public static func string_ZPC_add_friend_into_favorite() -> String 
	{ 
		return "đã được thêm vào danh sách Yêu thích" 
	}

	public static func string_ZPC_max_favorite_text() -> String 
	{ 
		return "Danh sách yêu thích đã đầy" 
	}

	public static func string_ZPC_max_red_packet_text() -> String 
	{ 
		return "Tối đa chỉ chọn được %d người" 
	}

	public static func string_ZPC_confirm_remove_red_packet() -> String 
	{ 
		return "Bạn có muốn xóa người này khỏi danh sách người nhận lì xì không?" 
	}

	public static func string_ZPC_Search_Empty() -> String 
	{ 
		return "Không tìm thấy kết quả phù hợp\nVui lòng nhập tên khác hoặc số điện thoại hợp lệ" 
	}

	public static func string_ZPC_update_contact_done() -> String 
	{ 
		return "Đã cập nhật danh bạ" 
	}

	public static func string_ZPC_my_phone_number() -> String 
	{ 
		return "Số của tôi" 
	}

	public static func string_ZPC_number_contacts() -> String 
	{ 
		return "bạn" 
	}

	public static func string_Voucher_message_alert() -> String 
	{ 
		return "Bạn muốn bỏ sử dụng khuyến mãi?" 
	}

	public static func string_Voucher_button_next() -> String 
	{ 
		return "Sử dụng mã KM" 
	}

	public static func string_Voucher_input_title() -> String 
	{ 
		return "Nhập mã khuyến mãi" 
	}

	public static func string_Voucher_title_view() -> String 
	{ 
		return "Mã khuyến mãi" 
	}

	public static func string_Voucher_button_input() -> String 
	{ 
		return "Nhập" 
	}

	public static func string_Voucher_before_input_title() -> String 
	{ 
		return "Giá ban đầu" 
	}

	public static func string_Voucher_delete_message() -> String 
	{ 
		return "Bạn muốn ngưng sử dụng mã khuyến mãi?" 
	}

	public static func string_Voucher_expiredate() -> String 
	{ 
		return "Còn %ld ngày" 
	}

	public static func string_Voucher_discount_amount() -> String 
	{ 
		return "giảm %ld" 
	}

	public static func string_Voucher_view_all() -> String 
	{ 
		return "xem tất cả" 
	}

	public static func string_Voucher_view_more() -> String 
	{ 
		return "* Chỉ hiển thị Quà tặng phù hợp, xem tất cả" 
	}

	public static func string_Voucher_have_gift_text() -> String 
	{ 
		return "Bạn có Ưu đãi" 
	}

	public static func string_Voucher_select_text() -> String 
	{ 
		return "Chọn" 
	}

	public static func string_Voucher_select_gift_text() -> String 
	{ 
		return "Chọn Ưu đãi" 
	}

	public static func string_Voucher_gift_text() -> String 
	{ 
		return "Ưu đãi" 
	}

	public static func string_Topup_phonev2_message() -> String 
	{ 
		return "Vui lòng kiểm tra số dư theo cú pháp *101#" 
	}

	public static func string_TransferMoney_Ext_ZaloPayId() -> String 
	{ 
		return "ZaloPay ID" 
	}

	public static func string_TransferMoney_Account_Locked() -> String 
	{ 
		return "Tài khoản %@ đang bị khoá" 
	}

	public static func string_TransferMoney_Phone_Number() -> String 
	{ 
		return "Số ĐT" 
	}

	public static func string_TransferMoney_Phone_None() -> String 
	{ 
		return "chưa cập nhật" 
	}

	public static func string_Topup_No_Phone() -> String 
	{ 
		return "%@ chưa có số điện thoại. Vui lòng chọn bạn khác để tiếp tục." 
	}

	public static func string_TransferMoney_ZaloAccount_PhoneNumber() -> String 
	{ 
		return "*** %@" 
	}

	public static func string_ViettinBank_Register_Online_Title() -> String 
	{ 
		return "Đăng ký thanh toán trực tuyến" 
	}

	public static func string_ViettinBank_Register_Online_Error_Message() -> String 
	{ 
		return "Số điện thoại hoặc CMND chưa trùng khớp" 
	}

	public static func string_ViettinBank_Register_Online_Title_TextField() -> String 
	{ 
		return "Số CMND/Hộ chiếu" 
	}

	public static func string_ViettinBank_Register_Online_Notice() -> String 
	{ 
		return "Lưu ý: Số điện thoại %@ & số CMND/Hộ chiếu phải trùng với thông tin đã đăng ký với ngân hàng." 
	}

	public static func string_ViettinBank_Register_Online_Alert_Message() -> String 
	{ 
		return "Bạn có chắc muốn thoát Đăng ký thanh toán trực tuyến?" 
	}

	public static func string_ViettinBank_Register_Online_Wrong_OTP() -> String 
	{ 
		return "Nhập sai OTP 3 lần" 
	}

	public static func string_ViettinBank_Register_Online_Phone() -> String 
	{ 
		return "tel://1900558868" 
	}

	public static func string_ViettinBank_Register_Online_Cancel_Input_OTP() -> String 
	{ 
		return "Bạn có muốn huỷ nhập OTP không?" 
	}

	public static func string_Sacombank_Register_Online_Step1() -> String 
	{ 
		return "Bấm nút ĐĂNG KÝ để sao chép cú pháp %@" 
	}

	public static func string_Sacombank_Register_Online_Step2() -> String 
	{ 
		return "Gửi SMS đến %@ (phí %@VND/sms)" 
	}

	public static func string_AppToApp_Popup_When_IsBilling() -> String 
	{ 
		return "Bạn đang có 1 giao dịch đang thực hiện. Vui lòng hãy thực hiện xong giao dịch hiện tại." 
	}

	public static func string_Dialog_Disclaimer_Merchant_App() -> String 
	{ 
		return "Nội dung tiếp theo là do %@ cung cấp. Nếu bạn thấy nội dung vi phạm hay không phù hợp, hãy liên lạc với bộ phận chăm sóc khách hàng của ZaloPay để thông báo. Bạn có muốn tiếp tục không?" 
	}

	public static func string_ZPC_Unsave_Phone_Number() -> String 
	{ 
		return "Số chưa lưu" 
	}

	public static func string_Clearing_call_CS_text() -> String 
	{ 
		return "Hoặc liên hệ bộ phận CSKH để được hỗ trợ\n1900 54 54 36 " 
	}

	public static func string_Clearing_login_text() -> String 
	{ 
		return "Sau khi đăng nhập, vui lòng thanh lý toàn bộ số dư và huỷ liên kết ngân hàng." 
	}

	public static func string_Clearing_account_locked() -> String 
	{ 
		return "Tài khoản đang bị khóa, vui lòng liên hệ bộ phận CSKH để được hỗ trợ 1900545436." 
	}

	public static func string_Clearing_overtime() -> String 
	{ 
		return "Hết thời gian thanh lý, vui lòng liên hệ bộ phận CSKH để được hỗ trợ 1900545436." 
	}

	public static func string_Clearing_login_undefine_error() -> String 
	{ 
		return "Có lỗi trong quá trình đăng nhập, vui lòng thử lại sau." 
	}

	public static func string_Clearing_input_pin() -> String 
	{ 
		return "NHẬP MẬT KHẨU THANH TOÁN" 
	}

	public static func string_Clearing_over_wrong_pin() -> String 
	{ 
		return "Sai mật khẩu thanh toán quá số lần quy định, vui lòng thử lại sau 15 phút." 
	}

	public static func string_Clearing_over_wrong_otp() -> String 
	{ 
		return "Sai mã xác thực quá số lần quy định, vui lòng thử lại sau 15 phút." 
	}

	public static func string_Clearing_over_change_otp() -> String 
	{ 
		return "Đổi mã xác thực quá số lần quy định, vui lòng thử lại sau 15 phút." 
	}

	public static func string_Clearing_withdraw_faild_min() -> String 
	{ 
		return "Số dư sau khi rút cần > " 
	}

	public static func string_Clearing_transfer_faild_min() -> String 
	{ 
		return "Số dư sau khi chuyển cần > " 
	}

	public static func string_Clearing_verify_OTP() -> String 
	{ 
		return "NHẬP MÃ XÁC THỰC TÀI KHOẢN" 
	}

	public static func string_Clearing_reject_remove_card() -> String 
	{ 
		return "Chưa huỷ liên kết ngân hàng." 
	}

	public static func string_Clearing_reject_transaction() -> String 
	{ 
		return "Người dùng huỷ bỏ giao dịch." 
	}

	public static func string_Clearing_not_enough_money_withdraw() -> String 
	{ 
		return "<font color='#000000'>Số dư không đủ, vui lòng <a href='transfer'>Chuyển Tiền</a></font>" 
	}

	public static func string_Disclaimer_Accept_Button_Title() -> String 
	{ 
		return "Tôi đã hiểu" 
	}

	public static func string_Voucher_server_maintenance() -> String 
	{ 
		return "Dịch vụ đang được bảo trì. Quý khách vui lòng quay lại sau." 
	}

	public static func string_Voucher_empty() -> String 
	{ 
		return "Bạn chưa có quà tặng." 
	}

	public static func string_InvitationZP_send() -> String 
	{ 
		return "Gửi lời mời" 
	}

	public static func string_Account_Unavailable() -> String 
	{ 
		return "Tài khoản %@ đang không khả dụng. Xin vui lòng quay lại sau." 
	}

	public static func string_TransferMoney_Warning_Not_Invite() -> String 
	{ 
		return "%@ chưa sử dụng ZaloPay.\nBạn hãy mời %@ sử dụng ZaloPay để chuyển tiền nhanh chóng và dễ dàng." 
	}

	public static func string_Download_Resources_Error() -> String 
	{ 
		return "Có lỗi trong quá trình tải dữ liệu,\n Vui lòng quay trở lại sau" 
	}

	public static func string_QuickPay_Title() -> String 
	{ 
		return "Thanh toán" 
	}

	public static func string_QuickPay_Result_Success() -> String 
	{ 
		return "Thanh toán thành công" 
	}

	public static func string_QuickPay_Result_Fail() -> String 
	{ 
		return "Thanh toán thất bại" 
	}

	public static func string_QuickPay_Setting_Title() -> String 
	{ 
		return "Thông tin cài đặt" 
	}

	public static func string_QuickPay_Setting() -> String 
	{ 
		return "Thông tin cài đặt" 
	}

	public static func string_QuickPay_ChangePaymentCode() -> String 
	{ 
		return "Đổi mã khác" 
	}

	public static func string_QuickPay_Support() -> String 
	{ 
		return "Hướng dẫn sử dụng" 
	}

	public static func string_QuickPay_Disconnect_TopView() -> String 
	{ 
		return "Mất kết nối mạng! \n Bạn vẫn có thể thanh toán." 
	}

	public static func string_QuickPay_Input_Password_Function() -> String 
	{ 
		return "Nhập mật khẩu để kích hoạt tính năng" 
	}

	public static func string_QuickPay_ChangePaymentCode_Alert() -> String 
	{ 
		return "Đổi mã khác thành công." 
	}

	public static func string_QuickPay_Error_Input_Password() -> String 
	{ 
		return "Lỗi khi nhập mã pin." 
	}

	public static func string_QuickPay_Secuirty_Warning() -> String 
	{ 
		return "Mã Thanh Toán \n Mã dùng để thanh toán cho người bán\n Vui lòng không chia sẻ với người khác" 
	}

	public static func string_QuickPay_Secuirty_Warning_DontShare() -> String 
	{ 
		return "Lưu ý: Tuyệt đối không gửi hoặc chia sẻ mã cho người khác" 
	}

	public static func string_QuickPay_Secuirty_Warning_Max() -> String 
	{ 
		return "Thanh toán tối đa %@ VND/giao dịch" 
	}

	public static func string_QuickPay_Secuirty_Warning_DontShow() -> String 
	{ 
		return "Không hiển thị lần sau" 
	}

	public static func string_QuickPay_Result_Original_Amount() -> String 
	{ 
		return "Giá ban đầu" 
	}

	public static func string_QuickPay_Setting_WhenDisconnect() -> String 
	{ 
		return "Khi không có kết nối mạng" 
	}

	public static func string_QuickPay_Setting_MaxAmount_NotIputPassword() -> String 
	{ 
		return "HẠN MỨC KHÔNG CẦN NHẬP MKTT" 
	}

	public static func string_QuickPay_Setting_MaxAmount() -> String 
	{ 
		return "THANH TOÁN TỐI ĐA" 
	}

	public static func string_QuickPay_Setting_ListChannel() -> String 
	{ 
		return "NGUỒN THANH TOÁN" 
	}

	public static func string_QuickPay_Setting_LockScreen() -> String 
	{ 
		return "KHOÁ MÀN HÌNH ỨNG DỤNG" 
	}

	public static func string_QuickPay_Disconnect() -> String 
	{ 
		return "Vui lòng kết nối mạng để tiếp tục\n sử dụng tính năng này" 
	}

	public static func string_QuickPay_Button_OK() -> String 
	{ 
		return "Đồng Ý" 
	}

	public static func string_Download_Resources_Loading() -> String 
	{ 
		return "Đang tải dữ liệu\n Vui lòng chờ trong ít phút!" 
	}

	public static func string_QuickPay_Slide_Intro_SubText_1() -> String 
	{ 
		return "Là hình thức thanh toán ở các cửa hàng tiện lợi, quán cafe, nhà hàng, quán ăn..." 
	}

	public static func string_QuickPay_Slide_Intro_SubText_2() -> String 
	{ 
		return "Người dùng đưa mã thanh toán cho bên cung cấp dịch vụ để quét mã" 
	}

	public static func string_QuickPay_Slide_Intro_SubText_3() -> String 
	{ 
		return "Tiện lợi-An toàn-Siêu nhanh-Miễn phí" 
	}

	public static func string_QuickPay_Slide_Intro_Title_1() -> String 
	{ 
		return "Trả tiền" 
	}

	public static func string_QuickPay_Slide_Intro_Title_2() -> String 
	{ 
		return "Hướng dẫn" 
	}

	public static func string_QuickPay_Slide_Intro_Title_3() -> String 
	{ 
		return "Ưu điểm" 
	}

	public static func string_QuickPay_GetStatus_Timeout() -> String 
	{ 
		return "Giao dịch đang xử lý, vui lòng kiểm tra kết quả giao dịch trong phần Lịch sử giao dịch sau 20 phút" 
	}

	public static func string_QuickPay_Error_VerifyPin() -> String 
	{ 
		return "Có lỗi xảy ra khi xác nhận mã thanh toán." 
	}

	public static func string_QuickPay_Error_GetStatus() -> String 
	{ 
		return "Có lỗi xảy ra khi lấy kết quả giao dịch." 
	}

	public static func string_QuickPay_UpdatePin_Success() -> String 
	{ 
		return "Đã cập nhật mã thanh toán" 
	}

	public static func string_QuickPay_Detail_BarCode() -> String 
	{ 
		return "Bấm vào mã vạch để lấy mã thanh toán" 
	}

	public static func string_LinkBank_Notice_VTB_Message() -> String 
	{ 
		return "Để liên kết thẻ thành công, số điện thoại đăng ký thẻ Vietinbank phải là \n %@" 
	}

	public static func string_TransferMoney_Warning_Note() -> String 
	{ 
		return "\nLưu ý: Hiện tại chỉ hỗ trợ mời bạn bè trên Zalo" 
	}

	public static func string_Send_RedPacket_Warning() -> String 
	{ 
		return "%@ chưa sử dụng ZaloPay. Bạn hãy mời %@ sử dụng ZaloPay để nhận lì xì nhé." 
	}

	public static func string_TransferMoney_Max_Input_Value() -> String 
	{ 
		return "Giá trị nhập vào cần nhỏ hơn %@" 
	}

	public static func string_RedPacket_Title_View_Selected() -> String 
	{ 
		return "DANH SÁCH ĐÃ CHỌN" 
	}

	public static func string_RedPacket_Guide_Text() -> String 
	{ 
		return "Từ nay bạn có thể gửi %@ cho cả những người chưa có ZaloPay. Hãy nhắc bạn bè của bạn đăng ký ZaloPay để nhận %@ nhé." 
	}

	public static func string_Voucher_Use_For_New_PaymentMethod() -> String 
	{ 
		return "Đã áp dụng Ưu đãi mới cho kênh %@" 
	}

	public static func string_Voucher_Not_Use_For_New_PaymentMethod() -> String 
	{ 
		return "Ưu đãi không áp dụng cho kênh %@" 
	}

	public static func string_Sharing_Cannot_Send_Message() -> String 
	{ 
		return "Không thể gửi tin nhắn do thiết bị của bạn không hỗ trợ" 
	}

	public static func string_Sharing_Facebook_Title() -> String 
	{ 
		return "Chia sẻ trên Facebook" 
	}

	public static func string_Sharing_Zalo_Title() -> String 
	{ 
		return "Chia sẻ trên Zalo" 
	}

	public static func string_Sharing_Messenger_Title() -> String 
	{ 
		return "Gửi tin nhắn trên Messenger" 
	}

	public static func string_Sharing_IMessage_Title() -> String 
	{ 
		return "Gửi tin nhắn" 
	}

	public static func string_Sharing_Other_Title() -> String 
	{ 
		return "Khác" 
	}

	public static func string_Voucher_Condition_LessThanMinPPAmount() -> String 
	{ 
		return "Số tiền thanh toán phải lớn hơn hạn mức giao dịch tối thiểu của %@ là %ld VND" 
	}

	public static func string_Voucher_Condition_GreaterThanMaxPPAmount() -> String 
	{ 
		return "Số tiền thanh toán phải nhỏ hơn hạn mức giao dịch tối đa của %@ là %ld VND" 
	}

	public static func string_Endow_Warning_NotUse() -> String 
	{ 
		return "Bạn không muốn áp dụng Ưu đãi đang có cho giao dịch này?" 
	}

	public static func string_Endow_Having_Text() -> String 
	{ 
		return "Đang có khuyến mãi" 
	}

	public static func string_Giftcode_Cashback_Title() -> String 
	{ 
		return "Hoàn tiền" 
	}

	public static func string_Giftcode_Voucher_Title() -> String 
	{ 
		return "Voucher" 
	}

	public static func string_LinkBank_VCCB_Notify_Message() -> String 
	{ 
		return "<font color='#24272b'>Để liên kết thành công, bạn cần đăng ký sử dụng Dịch vụ Ngân hàng điện tử của Ngân hàng Bản Việt. \n\nNếu chưa đăng ký, vui lòng đăng ký tại quầy giao dịch của ngân hàng.</font>" 
	}

	public static func string_LinkBank_VCCB_Accept_Title_Button() -> String 
	{ 
		return "Tôi đã đăng ký" 
	}

	public static func string_ZPC_not_exist_message() -> String 
	{ 
		return "không tồn tại" 
	}

	public static func string_ZPC_search_with_ZaloPayId() -> String 
	{ 
		return "Tìm kiếm theo ZaloPay ID cho" 
	}

	public static func string_Gateway_Header_LinkBank() -> String 
	{ 
		return "NGÂN HÀNG LIÊN KẾT" 
	}

	public static func string_Gateway_Header_Payment() -> String 
	{ 
		return "NGÂN HÀNG KHÁC" 
	}

	public static func string_Gateway_Title_Source() -> String 
	{ 
		return "Nguồn tiền khác" 
	}

	public static func string_Gateway_Notice_LessMinAmount() -> String 
	{ 
		return "Giao dịch tối thiểu từ %@" 
	}

	public static func string_Gateway_Notice_GreaterMaxAmount() -> String 
	{ 
		return "Giao dịch tối đa %@" 
	}

	public static func string_ZPC_guide_search_name_or_phone() -> String 
	{ 
		return "Nhập tên hoặc số điện thoại" 
	}

	public static func string_QRScan_ExchangeProcessingMessage() -> String 
	{ 
		return "Giao dịch đang xử lý, vui lòng chờ trong giây lát" 
	}

	public static func string_QRScan_NoConnectionMessage() -> String 
	{ 
		return "Mất kết nối mạng, vui lòng kiểm tra lại" 
	}

}

