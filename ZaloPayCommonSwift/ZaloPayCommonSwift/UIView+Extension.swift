//
//  UIView+Extention.swift
//  ZaloPay
//
//  Created by tienlv on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import Foundation
import CocoaLumberjackSwift
fileprivate let KEY_BOARD_ANIMATION_DURATION = 0.25
fileprivate let KEY_BOARD_ANIMATION_CURVE =  7

public extension UIView {
    public class func zp_viewFromSameNib() -> Self? {
        return self.viewHelper(fromSameNib: nil)
    }
    
    public class func zp_view(fromSameNib bundle: Bundle?) -> Self? {
        return self.viewHelper(fromSameNib: bundle)
    }
    fileprivate class func viewHelper<T:UIView>(fromSameNib bundle: Bundle?) -> T? {
//        let nameSpaceClassName = NSStringFromClass(self) as NSString
//        let className = nameSpaceClassName.components(separatedBy: ".").last! as String
        let className = "\(self)"
        return self.view(fromNibName: className, bundle: bundle)
    }

    fileprivate class func view<T>(fromNibName nibName: String, bundle: Bundle?) -> T? {
        let internalBundel = bundle ?? Bundle.main
        var result: T?
        
        let topLevelObjects = internalBundel.loadNibNamed(nibName, owner: nil, options: nil) as! [UIView]
        for currentObject: UIView in topLevelObjects {
            if (currentObject.isKind(of: self.classForCoder())) {
                result = currentObject as? T
                break
            }
        }
        return result
    }

    public func zp_viewController() -> UIViewController? {
        guard let parentView = superview else {
            return nil
        }
        
        let s = sequence(first: parentView) {
            guard !($0.next is UIViewController) else {
                return nil
            }
            return $0.superview
            }.map({ $0 })
        return s.last?.next as? UIViewController
    }
    
    public func zp_superTableView() -> UITableView? {
        var next = superview
        while (next != nil) {
            if (next is UITableView) {
                return (next as? UITableView)!
            }
            next = next?.superview
        }
        return nil
    }
    
    public func zp_showShadow() {
        zp_showShadow(withOutBezierPath: 5.0, color: .black, opacity: 0.5)
    }
    
    public func zp_showShadow(_ offSet: Float, color: UIColor, opacity: Float) {
        let shadowPath = UIBezierPath(rect: self.bounds)
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: CGFloat(0.0), height: CGFloat(offSet))
        self.layer.shadowOpacity = opacity
        self.layer.shadowPath = shadowPath.cgPath
    }
    
    public func zp_showShadow(withOutBezierPath offSet: Float, color: UIColor, opacity: Float) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: CGFloat(0.0), height: CGFloat(offSet))
        self.layer.shadowOpacity = opacity
    }
    
    public func zp_debugLayout() {
    }
    public func zp_roundRect(_ radius: Float) {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = CGFloat(radius)
    }
}
