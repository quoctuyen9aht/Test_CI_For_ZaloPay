//
//  ZPAnswersLog.swift
//  ZaloPay
//
//  Created by Dung Vu on 6/2/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//
import Foundation
import Crashlytics
import ZaloPayAnalyticsSwift
final class ZPAnswersLog {}
// MARK: - Get Event
extension ZPAnswersLog:ZPTrackerProtocol {
    func trackEvent(_ eventId: ZPAnalyticEventAction, value eventValue: NSNumber?, label eventLabel: String?) {
        self.trackEvent(eventId, value: eventValue)
    }
    
    func trackEvent(_ eventId: ZPAnalyticEventAction, valueString eventValue: String?, label eventLabel: String?) {
        self.trackEvent(eventId, value: eventLabel)
    }
    
    func trackEvent(_ eventId: ZPAnalyticEventAction, value eventValue: Any?) {
        let category = ZPTrackerEvents.eventCategoryFromId(eventId)
        let action = ZPTrackerEvents.eventActionFromId(eventId)
        
        var att: [String: Any] = ["action": action]
        att["value"] = eventValue
        
        Answers.logCustomEvent(withName: category, customAttributes: att)
    }
    
    func trackTiming(_ eventId: ZPAnalyticEventAction, value eventValue: NSNumber?) {}
    func trackScreen(_ screenName: String?) {
        guard let s = screenName, s.isEmpty.not else {
            return
        }
        
        Answers.logContentView(withName: s, contentType: "Screen", contentId: nil, customAttributes: nil)
    }
    func trackCustomEvent(_ eventCategory: String?, action eventAction:String?, label eventLabel:String?, value eventValue: NSNumber?) {}
}
