//
//  ZPDefaultEventLog.swift
//  ZaloPay
//
//  Created by Dung Vu on 6/2/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import Foundation
import CocoaLumberjackSwift
import ZaloPayAnalyticsSwift

final class ZPDefaultEventLog {}
// MARK: - Get Event
extension ZPDefaultEventLog: ZPTrackerProtocol {
    func trackEvent(_ eventId: ZPAnalyticEventAction, value eventValue: NSNumber?, label eventLabel: String?) {
        self.trackEvent(eventId, value: eventValue)
    }
    
    func trackEvent(_ eventId: ZPAnalyticEventAction, valueString eventValue: String?, label eventLabel: String?) {
        self.trackEvent(eventId, value: nil)
    }
    
    public func trackEvent(_ eventId: ZPAnalyticEventAction, value eventValue: NSNumber?) {
        let category = ZPTrackerEvents.eventCategoryFromId(eventId)
        let action = ZPTrackerEvents.eventActionFromId(eventId)
        DDLogDebug("EventTracker: [category: \(category), eventId: \(eventId), action: \(action)]")
    }
    
    public func trackTiming(_ eventId: ZPAnalyticEventAction, value eventValue: NSNumber?) {
        let category = ZPTrackerEvents.eventCategoryFromId(eventId)
        let action = ZPTrackerEvents.eventActionFromId(eventId)
        
        DDLogDebug("TimingEvent: [category: \(category), action: \(action), time: \((eventValue ?? 0))(ms)")
    }
    
    public func trackScreen(_ screenName: String?) {
        guard let s = screenName, s.isEmpty.not else {
            return
        }
        DDLogDebug("EventTracker: [hit screen: \(s)]")
    }
    
    public func trackCustomEvent(_ eventCategory: String?, action eventAction: String?, label eventLabel: String?, value eventValue: NSNumber?) {
        DDLogDebug("trackCustomEvent: [category: \(eventCategory ?? ""), action: \(eventAction ?? ""), label: \(eventLabel ?? ""), value: \(eventValue ?? 0)")
    }
}
