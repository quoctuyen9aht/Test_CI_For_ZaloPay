//
//  EventLogger.swift
//  ZaloPay
//
//  Created by Dung Vu on 6/2/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import Foundation
import Crashlytics

@objc enum ZPEventType: Int {
    case Login = 0
    case Logout
    case DepositMoney
    case AddCard
    case RemoveCard
    case Unknown
}

@objc enum ZPEventAction: Int {
    case Begin = 0
    case End
    case Cancel
    case Succeeded
    case Failed
    case Unknown
}

extension ZPEventType {
    var name: String {
        switch self {
        case .Login:
            return "Login"
        case .Logout:
            return "Logout"
        case .DepositMoney:
            return "Deposit Money"
        case .AddCard:
            return "Add Card"
        case .RemoveCard:
            return "Remove Card"
        case .Unknown:
            return "Unknown"
        }
    }
}

extension ZPEventAction {
    var name: String {
        switch self {
        case .Begin:
            return "begin"
        case .Cancel:
            return "cancel"
        case .End:
            return "end"
        case .Failed:
            return "failed"
        case .Succeeded:
            return "succeeded"
        case .Unknown:
            return "Unknown"
        }
    }
}
@objcMembers
final class EventLogger {
    static let sharedInstance = EventLogger()
}

extension EventLogger {
    func logEvent(_ event: ZPEventType,with action:ZPEventAction) {
        Answers.logCustomEvent(withName: event.name, customAttributes: ["action": action.name])
    }
    
    func logEventCustom(_ event: ZPEventType,with customAction:AnyObject?) {
        guard let a = customAction else {
            return
        }
        
        Answers.logCustomEvent(withName: event.name, customAttributes: ["action": a])
    }
}
