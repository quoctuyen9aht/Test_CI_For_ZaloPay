//
//  ZPGoogleAnalytic.swift
//  ZaloPay
//
//  Created by Dung Vu on 6/2/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import GoogleReporter
import ZaloPayAnalyticsSwift

final class ZPGoogleAnalytic {
    fileprivate lazy var appName: String = {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleName") as? String ?? ""
    }()
}
// MARK: - Track Event
extension ZPGoogleAnalytic: ZPTrackerProtocol {
    func trackEvent(_ eventId: ZPAnalyticEventAction, valueString eventValue: String?, label eventLabel: String?) {
        self.trackEvent(eventId, value: eventValue, labelEvent: eventLabel)
    }
    
    func trackEvent(_ eventId: ZPAnalyticEventAction, value eventValue: NSNumber?, label eventLabel: String?) {
        self.trackEvent(eventId, value: eventValue, labelEvent: eventLabel)
    }
    
    /// - Parameters:
    ///   - eventId: enum type in Tracking file
    ///   - eventValue: string or number value that need track
    ///   - labelName: previous Screen when action happen
    func trackEvent(_ eventId: ZPAnalyticEventAction, value eventValue: Any?, labelEvent labelName: String?) {
        let category = ZPTrackerEvents.eventCategoryFromId(eventId)
        var action = ZPTrackerEvents.eventActionFromId(eventId)
        let label = labelName ?? ""
        
        /*
         v=1                                   // Version.
         &tid=UA-XXXXX-Y                       // Tracking ID / Property ID.
         &cid=555                              // Anonymous Client ID.
         &t=event                              // Event hit type.
         &ec=UX                                // Event Category. Required.
         &ea=click                             // Event Action. Required.
         &el=Results                           // Event label.
         */

        if let v = eventValue as? Int {
            action = String(format: ZPTrackerEvents.eventActionFromId(eventId), v)
        }
        if let v = eventValue as? String {
            action = String(format: ZPTrackerEvents.eventActionFromId(eventId), v)
        }
        GoogleReporter.shared.event(category, action: action, label: label)
    }
    
    func trackTiming(_ eventId: ZPAnalyticEventAction, value eventValue: NSNumber?) {
        let category = ZPTrackerEvents.eventCategoryFromId(eventId)
        let action = ZPTrackerEvents.eventActionFromId(eventId)
        let label = action
        /*
         &t=timing        // Timing hit type.
         &utc=jsonLoader  // Timing category.
         &utv=load        // Timing variable.
         &utt=5000        // Timing time.
         &utl=jQuery      // Timing label.
         */
        
        GoogleReporter.shared.timing(category, action: action, label: label, parameters: ["utt": "\((eventValue ?? 0).intValue)"])
    }
    
    func trackScreen(_ screenName: String?) {
        guard let s = screenName, s.isEmpty.not else {
            return
        }
        
        /*
         &t=screenview               // Screenview hit type.
         &an=funTimes                // App name.
         &av=1.5.0                   // App version.
         &aid=com.foo.App            // App Id.
         &aiid=com.android.vending   // App Installer Id.
         
         &cd=Home                    // Screen name / content description.
         */
        
        //        s.remove("ViewController")
        //remove app name
        //        s.remove("\(appName).", options: .caseInsensitive, range: nil)
        //        let nName = "[iOS][\(s)]"
        
        GoogleReporter.shared.screenView(s)
    }
    
    func trackCustomEvent(_ eventCategory: String?, action eventAction:String?, label eventLabel:String?, value eventValue: NSNumber?) {
        var parameters: [String: String] = [:]
        if let v = eventValue {
            parameters = ["ev": "\(v)"]
        }
        guard let eventCategory = eventCategory,
            let eventAction = eventAction,
            let eventLabel = eventLabel else {
            return
        }
        GoogleReporter.shared.event(eventCategory, action: eventAction, label: eventLabel, parameters: parameters)
    }
}
