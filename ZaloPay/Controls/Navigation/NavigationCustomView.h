//
//  NavigationCustomView.h
//  ZaloPay
//
//  Created by Dung Vu on 3/6/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
@class NavigationCustomView;
@protocol NavigationCustomViewDelegate <NSObject>
- (void) didTapAtIndexOnToolbar: (NSInteger) idx;
@end

@interface NavigationCustomView : UIView
@property (assign, nonatomic) id<NavigationCustomViewDelegate> delegate;
- (UIView *)notificationItem;

@end

@interface NavigationCustomItem : UIView
@property (nonatomic, strong) ZPIconFontImageView *imageView;
@end
