//
//  UINavigationBar+CustomView.swift
//  ZaloPay
//
//  Created by Dung Vu on 3/23/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
@objc protocol TransparentProtocol: class {
    var alpha: CGFloat { get set }
    var transform: CGAffineTransform { get set }
}

extension UIView: TransparentProtocol {}

extension UINavigationBar {
    private struct NavigationBarAssociate {
        static var name = "UINavigationBarCustomView"
    }
    
    @objc var overlayView: UIView? {
        get{
            return objc_getAssociatedObject(self, &NavigationBarAssociate.name) as? UIView
        }
        set {
            defer {
                objc_setAssociatedObject(self, &NavigationBarAssociate.name, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
            guard let nView = newValue else { return }
            
            nView.frame = CGRect(origin: CGPoint.zero, size: CGSize(width: self.bounds.width, height: 44))
            nView.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(nView)
            nView.snp.makeConstraints { (make) in
                make.left.equalTo(0)
                make.right.equalTo(0)
                make.size.equalTo(nView.frame.size)
                make.bottom.equalTo(0)
            }            
        }
    }
    
    @objc public func setAlphaElement(_ percent: CGFloat) {
        let t = CGAffineTransform.identity
        let naviItem = self.items?.first
        let leftViews = naviItem?.leftBarButtonItems?.compactMap({ $0.customView }) ?? []
        let titleView = naviItem?.titleView
        let rightViews = naviItem?.rightBarButtonItems?.compactMap({ $0.customView }) ?? []
        
        var allViews = leftViews + rightViews
        if let t = titleView {
           allViews.append(t)
        }
        allViews.forEach({
            $0.alpha = percent
            $0.transform = t
        })
    }
    
    @objc public func setAlphaOverlayView(_ percent: CGFloat) {
        // Track in 3 level : 0 - 0.2 , 0.2 - 0.3 , 0.3 - 1
        // Level 1 : alpha items navigation bar
        // Level 2 : hide all items
        // Level 3 : alpha overlayview
        var alpha = min(percent, 1)
        alpha = max(alpha, 0)
        
        guard let overlayView = self.overlayView else { return }
        switch alpha {
        case 0...0.2:
            overlayView.alpha = 0
            setAlphaElement((0.2 - alpha) / 0.2)
        case 0.2..<0.3:
            overlayView.alpha = 0
            setAlphaElement(0)
        default:
            overlayView.alpha = (alpha - 0.3) / 0.7
            setAlphaElement(0)
        }
    }
    
    @objc public func reset() {
        guard let overlayView = self.overlayView else { return }
        overlayView.snp.removeConstraints()
        overlayView.removeFromSuperview()
        self.overlayView = nil
    }
    
    
}
