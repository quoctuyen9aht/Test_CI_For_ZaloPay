//
//  ZPNavigationBar.swift
//  ZaloPay
//
//  Created by Bon Bon on 1/10/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit

class ZPNavigationBar: UINavigationBar {
   
    override func layoutSubviews() {
        super.layoutSubviews();
        if #available(iOS 11, *){
            self.layoutMargins = UIEdgeInsets()
            for subview in self.subviews {
                if String(describing: subview.classForCoder).contains("ContentView") {
                    let oldEdges = subview.layoutMargins
                    subview.layoutMargins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: oldEdges.right)
                }
            }
        }
    }

}
