//
//  NavigationCustomView.m
//  ZaloPay
//
//  Created by Dung Vu on 3/6/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "NavigationCustomView.h"
#import <ZaloPayCommon/NSArray+ConstrainsLayout.h>

@implementation NavigationCustomItem

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    [self setupView];
    return self;
}

- (void) setupView{
    self.imageView = [[ZPIconFontImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    self.imageView.contentMode = UIViewContentModeCenter;
    [self addSubview:self.imageView];
}

@end


@interface NavigationCustomView()
@end

@implementation NavigationCustomView

static CGFloat indentItem = 10;
static CGFloat marginItem = 30;
static CGFloat widthItem = 30;
static CGFloat topItem = 10;

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupView];
}

- (void) setupView {
    NSMutableArray *items = [NSMutableArray new];
    NSArray *arrString = @[@"header_payqrcode", @"header_linkbank", @"header_search",@"nav_notify"];
    __block CGFloat widthTapSearch = 0;
    __block CGFloat offsetXTapSearch = 0;
    [arrString enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        CGFloat x = idx * widthItem + idx * marginItem + indentItem;
        BOOL isLastObject = idx == [arrString count] - 1;
        
        if (isLastObject) {
            offsetXTapSearch = x - marginItem;
            x = self.bounds.size.width - (widthItem + indentItem);
            widthTapSearch = x - offsetXTapSearch;
        }
        
        NavigationCustomItem *item = [[NavigationCustomItem alloc] initWithFrame:CGRectMake(x, topItem, widthItem, widthItem)];
        [item.imageView setIconFont:obj];
        item.imageView.defaultView.textAlignment = NSTextAlignmentCenter;
        [item.imageView.defaultView setFont:[UIFont zaloPayWithSize:24]];
        [item.imageView setIconColor:[UIColor whiteColor]];
        [item.imageView setUserInteractionEnabled:YES];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnItem:)];
        [item.imageView addGestureRecognizer:tap];
        item.imageView.tag = 100 + idx;
        [items addObject:item];
        [self addSubview:item];
        if (isLastObject) { item.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin; }
    }];
    
    UIButton *btnSearch = [[UIButton alloc] initWithFrame:CGRectMake(offsetXTapSearch, topItem , widthTapSearch, widthItem)];
    
    [btnSearch setBackgroundColor:[UIColor clearColor] forState:UIControlStateNormal];
    [btnSearch setBackgroundColor:[UIColor clearColor] forState:UIControlStateSelected];
    @weakify(self);
    [[btnSearch rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        if (self.delegate && [self.delegate respondsToSelector:@selector(didTapAtIndexOnToolbar:)]) {
            [self.delegate didTapAtIndexOnToolbar:2];
        }
    }];
    
    
    [self addSubview:btnSearch];
    btnSearch.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
}

- (void)tapOnItem: (UITapGestureRecognizer *) tap {
    if (_delegate && [_delegate respondsToSelector:@selector(didTapAtIndexOnToolbar:)]) {
        [_delegate didTapAtIndexOnToolbar:[tap view].tag - 100];
    }
}

- (UIView *)notificationItem {
    return [self viewWithTag:103];
}
@end
