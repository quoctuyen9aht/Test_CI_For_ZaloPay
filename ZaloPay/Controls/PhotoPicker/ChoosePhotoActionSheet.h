//
//  ChoosePhotoActionSheet.h
//  ZaloPay
//
//  Created by bonnpv on 7/4/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ChoosePhotoActionSheet;

@protocol ChoosePhotoActionSheetDelegate <NSObject>
- (void)sheetDidClose:(ChoosePhotoActionSheet *)sheet;
- (void)sheetChoosePhoto:(ChoosePhotoActionSheet *)sheet;
- (void)sheetChooseCamera:(ChoosePhotoActionSheet *)sheet;
@end

@interface ChoosePhotoActionSheet : UIView
@property (nonatomic, weak) id <ChoosePhotoActionSheetDelegate> delegate;
@end
