//
//  ZalayPhotoPicker.h
//  ZaloPay
//
//  Created by bonnpv on 6/29/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void (^SelectPhotoHandle)(UIImage *result);

@interface ZaloPayPhotoPicker : NSObject
@property (nonatomic) BOOL allowEdit;
@property (nonatomic, strong) UIImage *image;
- (void)selectPhotoFromViewController:(UIViewController *)viewController
                         withComplete:(SelectPhotoHandle)completeHandle;

- (void)selectPhotoInLibraryWithController:(UIViewController *)viewControlelr
                                  complete:(SelectPhotoHandle)completeHandle;
@end
