//
//  ZalayPhotoPicker.m
//  ZaloPay
//
//  Created by bonnpv on 6/29/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZaloPayPhotoPicker.h"
#import ZALOPAY_MODULE_SWIFT
#import "ChoosePhotoActionSheet.h"
#import <ZaloPayCommonSwift/ZaloPayCommonSwift-Swift.h>

@interface ZaloPayPhotoPicker ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate, ChoosePhotoActionSheetDelegate>
@property (nonatomic, weak) ChoosePhotoActionSheet *sheetView;
@property (nonatomic, weak) UIViewController *sourceViewController;
@property (nonatomic, copy) SelectPhotoHandle completeHandle;
@end

@implementation ZaloPayPhotoPicker

- (id)init {
    self = [super init];
    if (self) {
        self.allowEdit = NO;
    }
    return self;
}

- (void)selectPhotoFromViewController:(UIViewController *)viewController
                                 withComplete:(SelectPhotoHandle)completeHandle {
    self.sourceViewController = viewController;
    self.completeHandle = completeHandle;
    [self startSelectPhoto];
}

- (void)startSelectPhoto {
    self.sheetView = [ChoosePhotoActionSheet viewFromSameNib];
    self.sheetView.delegate = self;
    [self.sourceViewController.navigationController.view addSubview:self.sheetView];
    [self.sheetView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@0);
        make.bottom.equalTo(@0);
        make.left.equalTo(@0);
        make.right.equalTo(@0);
    }];
}

- (void)showCamera {
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        return;
    }
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = self.allowEdit;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self.sourceViewController presentViewController:picker animated:YES completion:NULL];
}

- (void)showPhotoList {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = self.allowEdit;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self.sourceViewController presentViewController:picker animated:YES completion:NULL];
}

- (void)selectPhotoInLibraryWithController:(UIViewController *)viewControlelr
                                  complete:(SelectPhotoHandle)completeHandle {
    self.sourceViewController = viewControlelr;
    self.completeHandle = completeHandle;
    [self showPhotoList];
}

#pragma mark - Picker Delegate
    
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    UIImage *choosenImage = info[UIImagePickerControllerOriginalImage];
    self.image = choosenImage;
    if (self.completeHandle) {
        self.completeHandle(choosenImage);
        self.completeHandle = nil;
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
    if (self.completeHandle) {
        self.completeHandle(nil);
        self.completeHandle = nil;
    }
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    [navigationController defaultNavigationBarStyle];
}

#pragma mark - Sheet Delegate 

- (void)sheetDidClose:(ChoosePhotoActionSheet *)sheet {
    [self.sheetView removeFromSuperview];
    self.sheetView = nil;
}

- (void)sheetChoosePhoto:(ChoosePhotoActionSheet *)sheet {
    [self sheetDidClose:sheet];
    [self showPhotoList];
}

- (void)sheetChooseCamera:(ChoosePhotoActionSheet *)sheet {
    [self sheetDidClose:sheet];
    [self showCamera];
}

@end
