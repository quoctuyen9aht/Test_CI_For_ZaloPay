//
//  ChoosePhotoActionSheet.m
//  ZaloPay
//
//  Created by bonnpv on 7/4/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ChoosePhotoActionSheet.h"

@interface ChoosePhotoActionSheet ()
@property (nonatomic, weak) IBOutlet UIView *backgroundContentView;
@property (nonatomic, weak) IBOutlet UIView *backgroundViewAction;
@property (nonatomic, weak) IBOutlet UIView *lineView;
@property (nonatomic, weak) IBOutlet UIButton *buttonCancel;
@property (nonatomic, weak) IBOutlet ZPIconFontImageView *iconLibrary;
@property (nonatomic, weak) IBOutlet ZPIconFontImageView *iconCamera;
- (IBAction)closeButtonClick:(id)sender;
- (IBAction)photoButtoClick:(id)sender;
- (IBAction)cameraButtonClick:(id)sender;
@end

@implementation ChoosePhotoActionSheet

- (void)awakeFromNib {
    [super awakeFromNib];
    self.lineView.backgroundColor = [UIColor lineColor];
    [self.buttonCancel roundRect:6];
    self.backgroundColor = [UIColor colorWithWhite:0.1 alpha:0.8];
    self.backgroundContentView.backgroundColor = [UIColor clearColor];
    self.backgroundViewAction.backgroundColor = [UIColor whiteColor];
    [self.backgroundViewAction roundRect:6];
    
    [self.iconCamera setIconFont:@"profile_camera"];
    [self.iconLibrary setIconFont:@"profile_library"];
    
    UIColor *color = [UIColor colorWithHexValue:0x008fe5];
    [self.iconLibrary setIconColor:color];
    [self.iconCamera setIconColor:color];
}

- (IBAction)closeButtonClick:(id)sender {
    [self.delegate sheetDidClose:self];
}

- (IBAction)photoButtoClick:(id)sender {
    [self.delegate sheetChoosePhoto:self];
}

- (IBAction)cameraButtonClick:(id)sender {
    [self.delegate sheetChooseCamera:self];
}
@end
