//
//  ZPBaseSharing.swift
//  ZaloPay
//
//  Created by tridm2 on 2/6/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift

struct ZPItemShare {
    let link: URL?
    let image: UIImage?
    let message: String?
}

extension ZPItemShare {
    func objectsShare() -> [Any] {
        var result:[Any] = []
        result.add(element: message)
        result.add(element: link)
        result.add(element: image)
        return result
    }
}


class ZPBaseSharing : NSObject {
    var item: ZPItemShare!
    weak var rootVC: UIViewController?
    var signal: Observable<Bool> {
        return eventResult.asObserver()
    }
    
    let eventResult = PublishSubject<Bool>()
    
    required init(with controller: UIViewController?, using share: ZPItemShare) {
        super.init()
        self.rootVC = controller
        self.item = share
    }
    
    func share() -> Observable<Bool> {
        return Observable.empty()
    }
    
    func checkAndInstallApp(urlApp: URL?, appId: String) -> Bool {
        guard let urlApp = urlApp else {
            return false
        }
        
        let isAppExisted = UIApplication.shared.canOpenURL(urlApp)
        if isAppExisted {
            return true
        }
        
        if let url = URL(string: "\(itunesAppLink)\(appId)") {
            UIApplication.shared.openURL(url)
        }
        
        return false
    }
}
