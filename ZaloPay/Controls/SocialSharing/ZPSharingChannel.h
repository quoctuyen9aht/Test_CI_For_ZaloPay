//
//  ZPSharingChannel.h
//  ZaloPay
//
//  Created by tridm2 on 2/5/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#ifndef ZPSharingChannel_h
#define ZPSharingChannel_h

typedef NS_ENUM(NSInteger, ZPSharingResult) {
    ZPSharingResult_succeed = 1,
    ZPSharingResult_failed = 0,
    ZPSharingResult_canceled = -1
};

typedef NS_ENUM(NSInteger, ZPSharingChannel) {
    ZPSharingChannel_facebook = 1,
//    ZPSharingChannel_zalo = 2,
//    ZPSharingChannel_messenger = 3,
//    ZPSharingChannel_imessage = 4,
//    ZPSharingChannel_other = 5
    ZPSharingChannel_messenger = 2,
    ZPSharingChannel_imessage = 3,
    ZPSharingChannel_other = 4
};

#endif /* ZPSharingChannel_h */
