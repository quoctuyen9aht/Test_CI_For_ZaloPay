//
//  ZPCenterSharing.swift
//  ZaloPay
//
//  Created by tridm2 on 1/25/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift
import ZaloPayAnalyticsSwift

@objcMembers
class ZPCenterSharing : NSObject {
    public static let sharedInstance = ZPCenterSharing()
    
    private static let channelMapping: [ZPSharingChannel: ZPBaseSharing.Type] = [
        ZPSharingChannel.facebook: ZPSharingFacebook.self,
//        ZPSharingChannel.zalo: ZPSharingZalo.self,
        ZPSharingChannel.messenger: ZPSharingMessenger.self,
        ZPSharingChannel.imessage: ZPSharingIMessage.self,
        ZPSharingChannel.other: ZPSharingOS.self
    ]
    
    private static let EventMapping: [ZPSharingChannel: ZPAnalyticEventAction] = [
        ZPSharingChannel.facebook: ZPAnalyticEventAction.redpacket_share_facebook,
//        ZPSharingChannel.zalo: ZPAnalyticEventAction.redpacket_share_zalo,
        ZPSharingChannel.messenger: ZPAnalyticEventAction.redpacket_share_fb_messenger,
        ZPSharingChannel.imessage: ZPAnalyticEventAction.redpacket_share_messages,
        ZPSharingChannel.other: ZPAnalyticEventAction.redpacket_share_others
    ]
    
    private let disposeBag = DisposeBag()
    private var sharringChannels: [ZPBaseSharing] = []
    
    @objc func share(from vc: UIViewController?, on channel: ZPSharingChannel, message: JSON, callback: ((Int) -> Void)?) {
        guard let sharingClass = ZPCenterSharing.channelMapping[channel] else {
            callback?(ZPSharingResult.failed.rawValue)
            return
        }
        if let eventID = ZPCenterSharing.EventMapping[channel] {
            ZPTrackingHelper.shared().trackEvent(eventID)
        }
        
        let item = ZPItemShare(link: URL.init(string: message.string(forKey: "link")),
                               image: message.value(forKey: "image", defaultValue: nil),
                               message: message.string(forKey: "message"))
    
        let sharingChannel = sharingClass.init(with: vc, using: item)
        sharringChannels.append(sharingChannel)
        let sharingSignal: Observable<Bool> = sharingChannel.share()
        sharingSignal
            .map({ $0 ? ZPSharingResult.succeed.rawValue : ZPSharingResult.canceled.rawValue })
            .catchErrorJustReturn(ZPSharingResult.failed.rawValue)
            .bind { [weak self] in
                callback?($0)
                _ = self?.sharringChannels.popLast()
            }.disposed(by: disposeBag)
    }
}
