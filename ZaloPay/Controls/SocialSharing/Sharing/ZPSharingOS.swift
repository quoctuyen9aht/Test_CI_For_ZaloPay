//
//  ZPSharingOS.swift
//  ZaloPay
//
//  Created by tridm2 on 1/31/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift

class ZPSharingOS : ZPBaseSharing {
   
    private func showDialog(objectsToShare: [Any]) {
        let shareVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        shareVC.excludedActivityTypes = [UIActivityType.saveToCameraRoll]
        shareVC.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.right
        shareVC.completionWithItemsHandler = { [weak self](_, complete, _, e) -> () in
            guard let e = e else {
                self?.eventResult.onNext(complete)
                self?.eventResult.onCompleted()
                return
            }
            self?.eventResult.onError(e)
        }
        self.rootVC?.present(shareVC, animated: true, completion: nil)
    }
    
    override func share() -> Observable<Bool> {
        let objectsToShare: [Any]  = self.item.objectsShare()
        showDialog(objectsToShare: objectsToShare)
        return signal
    }
}
