//
//  ZPSharingIMessage.swift
//  ZaloPay
//
//  Created by tridm2 on 1/30/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import MessageUI
import RxSwift

class ZPSharingIMessage : ZPBaseSharing {
    private func setupContentForSMS(sms: MFMessageComposeViewController) {
        let content = self.item.message ?? ""
        sms.body = content
    }
    
    private func setupImageForSMS(sms: MFMessageComposeViewController) {
        let image = self.item.image ?? #imageLiteral(resourceName: "historylist_empty")
        if let imageData = UIImageJPEGRepresentation(image, 1.0) {
            sms.addAttachmentData(imageData, typeIdentifier: kUTTypePNG as String, filename: "LiXi.png")
        }
    }
    
    private func setupLinkForSMS(sms: MFMessageComposeViewController) {
        guard let link = self.item.link else {
            return
        }
        sms.addAttachmentURL(link, withAlternateFilename: nil)
    }
    
    private func showErrorDialog() {
        let attributesRegular : [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : UIFont.sfuiTextRegular(withSize: 15), NSAttributedStringKey.foregroundColor : UIColor.darkGray]
        let msg = R.string_Sharing_Cannot_Send_Message()!
        let attributedString:NSMutableAttributedString = ZPDialogView.attributeString(withMessage: msg)
        attributedString.addAttributes(attributesRegular, range: NSRange.init(location: 0, length: msg.count))
        
        ZPDialogView.showDialog(with:DialogTypeNotification,
                                title: nil,
                                attributeMessage: attributedString ,
                                buttonTitles: [R.string_ButtonLabel_OK()],
                                handler: nil)
    }
    
    private func createSMSController() -> MFMessageComposeViewController? {
        guard MFMessageComposeViewController.canSendText() else {
            return nil
        }
        
        let smsController = MFMessageComposeViewController()
        smsController.messageComposeDelegate = self
        return smsController
    }
    
    private func setupSMS(sms smsController: MFMessageComposeViewController) {
        setupContentForSMS(sms: smsController)
        guard self.item.link != nil else {
            setupImageForSMS(sms: smsController)
            return
        }
        setupLinkForSMS(sms: smsController)
    }

    
    override func share() -> Observable<Bool> {
        let controller = createSMSController()
        if controller == nil {
            showErrorDialog()
        }
        
        guard let vc = self.rootVC, let smsController = controller else {
            return Observable.just(false)
        }
        
        setupSMS(sms: smsController)
        vc.present(smsController, animated: true, completion: nil)
        
        return signal
    } 
}

// MARK: - SMS
extension ZPSharingIMessage: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
        case .sent:
            eventResult.onNext(true)
            eventResult.onCompleted()
        case .cancelled:
            eventResult.onNext(false)
            eventResult.onCompleted()
        case .failed:
            let error = NSError(domain: NSURLErrorDomain, code: NSURLErrorCancelled, userInfo: [NSLocalizedDescriptionKey : "Cancel"])
            eventResult.onError(error)
        }
        controller.dismiss(animated: true, completion: nil)
    }
}
