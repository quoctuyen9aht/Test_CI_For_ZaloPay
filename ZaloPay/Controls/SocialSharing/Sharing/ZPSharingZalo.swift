//
//  ZPSharingZalo.swift
//  ZaloPay
//
//  Created by tridm2 on 1/25/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloSDKCoreKit
import RxSwift
import ZaloPayProfile

class ZPSharingZalo : ZPBaseSharing {
    let bag = DisposeBag()
    
    override func share() -> Observable<Bool> {
        guard let vc = self.rootVC else {
            return Observable.just(false)
        }
        
        let link = self.item.link?.absoluteString
        let body = self.item.message
        let feed = ZOFeed(link: link, appName: "ZaloPay", message: body, others: nil)
        ZaloSDKApiWrapper.sharedInstance.shareFeedZalo(feed, andParentVC: vc).subscribe(onNext: { [weak self] (result) in
            if result {
                let error = NSError(domain: NSURLErrorDomain, code: NSURLErrorCancelled, userInfo: [NSLocalizedDescriptionKey : "Failed"])
                self?.eventResult.onError(error)
                return
            }
            self?.eventResult.onNext(true)
            self?.eventResult.onCompleted()
            }, onError: { [weak self] (error) in
                if (error.errorCode() == Int(kZaloSDKErrorCodeRequestCanceled.rawValue)) {
                    self?.eventResult.onNext(false)
                    self?.eventResult.onCompleted()
                    return;
                }
                self?.eventResult.onError(error)
        }).disposed(by: bag)
        return signal
    }
}
