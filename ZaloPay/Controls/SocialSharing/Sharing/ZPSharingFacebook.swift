//
//  ZPSharingFacebook.swift
//  ZaloPay
//
//  Created by tridm2 on 1/25/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import FBSDKCoreKit
import FBSDKLoginKit
import RxSwift

class ZPSharingFacebook : ZPBaseSharing {
    private func showSharingImageDialog(from vc: UIViewController) {
        let content = FBSDKSharePhotoContent()
        let photo = FBSDKSharePhoto()
        photo.image = self.item.image ?? #imageLiteral(resourceName: "historylist_empty")
        photo.isUserGenerated = true
        content.photos = [photo]
        FBSDKShareDialog.show(from: vc, with: content, delegate: self)
    }
    
    private func showSharingLinkDialog(from vc: UIViewController) {
        guard let link = self.item.link else {
            eventResult.onNext(false)
            eventResult.onCompleted()
            return
        }
        
        let content = FBSDKShareLinkContent()
        content.contentURL = link
        content.quote = self.item.message ?? ""
        FBSDKShareDialog.show(from: vc, with: content, delegate: self)
    }
    
    private func showSharingDialog(from vc: UIViewController) {
        guard self.item.link != nil else {
            showSharingImageDialog(from: vc)
            return
        }
        showSharingLinkDialog(from: vc)
    }
    
    override func share() -> Observable<Bool> {
        let isFbAppExisted = self.checkAndInstallApp(urlApp: URL(string: "fbapi://"), appId: "id284882215")
        guard isFbAppExisted, let vc = self.rootVC else {
            let error = NSError(domain:"Facebook", code: -1, userInfo:nil)
            return Observable.error(error)
        }
        
        showSharingDialog(from: vc)
        return signal
    }
}

extension ZPSharingFacebook : FBSDKSharingDelegate {
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable : Any]!) {
        eventResult.onNext(true)
        eventResult.onCompleted()
    }
    
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        eventResult.onError(error)
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        eventResult.onNext(false)
        eventResult.onCompleted()
    }
}
