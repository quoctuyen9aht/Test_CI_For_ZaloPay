//
//  ZPSharingFBMessenger.swift
//  ZaloPay
//
//  Created by tridm2 on 1/25/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import FBSDKShareKit
import RxSwift

class ZPSharingMessenger : ZPBaseSharing {
    private func showSharingImageDialog() {
        let content = FBSDKSharePhotoContent()
        let photo = FBSDKSharePhoto()
        photo.image = self.item.image ?? #imageLiteral(resourceName: "historylist_empty")
        photo.isUserGenerated = true
        content.photos = [photo]
        FBSDKMessageDialog.show(with: content, delegate: self)
    }
    
    private func showSharingLinkDialog() {
        guard let link = self.item.link else {
            eventResult.onNext(false)
            eventResult.onCompleted()
            return
        }
        
        let content = FBSDKShareLinkContent()
        content.contentURL = link
        content.quote = self.item.message ?? ""
        FBSDKMessageDialog.show(with: content, delegate: self)
    }
    
    private func showSharingDialog() {
        guard self.item.link != nil else {
            showSharingImageDialog()
            return
        }
        showSharingLinkDialog()
    }
    
    override func share() -> Observable<Bool> {
        let isMessengerAppExisted = self.checkAndInstallApp(urlApp: URL(string: "fb-messenger-share-api://"), appId: "id454638411")
        if isMessengerAppExisted.not {
            let error = NSError(domain:"Messager", code: -1, userInfo:nil)
            return Observable.error(error)
        }
        showSharingDialog()
        return signal
    }
}

extension ZPSharingMessenger : FBSDKSharingDelegate {
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable : Any]!) {
        eventResult.onNext(true)
        eventResult.onCompleted()
    }
    
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        eventResult.onError(error)
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        eventResult.onNext(false)
        eventResult.onCompleted()
    }
}
