//
//  PMTextField.h
//  ZaloPay
//
//  Created by PhucPv on 4/25/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum {
    PMTextFieldTypeNone = 0,
    PMTextFieldTypeCurrency,
    PMTextFieldTypeCurrencyWithNumberPadCustom,
    PMTextFieldTypeCardNumber,
    
}PMTextFieldType;

@interface PMTextField : UITextField
@property (nonatomic, assign) PMTextFieldType type;
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
@end
