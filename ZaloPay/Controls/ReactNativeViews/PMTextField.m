//
//  PMTextField.m
//  ZaloPay
//
//  Created by PhucPv on 4/25/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "PMTextField.h"
#import <ZaloPayCommon/UITextField+Extension.h>
#import "PMNumberPad.h"
@interface PMTextField () <APNumberPadDelegate>
@end
@implementation PMTextField
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (self.type == PMTextFieldTypeCurrency || self.type == PMTextFieldTypeCurrencyWithNumberPadCustom) {
//        __block NSString *message;
        u_int64_t maxValue = INT64_MAX;
//        message = @"Quá giá trị cho phép";
//        @weakify(self);
        BOOL result = [UITextField converToDecimalWithTextField:textField
                                  shouldChangeCharactersInRange:range
                                              replacementString:string
                                                       maxValue:maxValue
                                                     errorBlock:^{
//                                                         @strongify(self);
                                                     }];
        return result;
    } else if (self.type == PMTextFieldTypeCardNumber) {
        __block NSString *text = [textField text];
        NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789\b"];
        string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
        if ([string rangeOfCharacterFromSet:[characterSet invertedSet]].location != NSNotFound) {
            return NO;
        }
        
        text = [text stringByReplacingCharactersInRange:range withString:string];
        text = [text stringByReplacingOccurrencesOfString:@" - " withString:@""];
        
        NSString *newString = @"";
        while (text.length > 0) {
            NSString *subString = [text substringToIndex:MIN(text.length, 4)];
            newString = [newString stringByAppendingString:subString];
            if (subString.length == 4) {
                newString = [newString stringByAppendingString:@" - "];
            }
            text = [text substringFromIndex:MIN(text.length, 4)];
        }
        newString = [newString stringByTrimmingCharactersInSet:[characterSet invertedSet]];
        [textField setText:newString];
        return NO;
    }
    return YES;
}

- (void)setType:(PMTextFieldType)type {
    _type = type;
    switch (type) {
        case PMTextFieldTypeCardNumber:
        case PMTextFieldTypeCurrency:
            self.keyboardType = UIKeyboardTypeNumberPad;
            break;
        case PMTextFieldTypeCurrencyWithNumberPadCustom: {
            self.inputView = ({
                PMNumberPad *numberPad = [PMNumberPad numberPadWithDelegate:self];
                numberPad;
            });
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - APNumberPadDelegate

- (void)numberPad:(PMNumberPad *)numberPad functionButtonAction:(UIButton *)functionButton textInput:(UIResponder<UITextInput> *)textInput {
    if (functionButton == numberPad.leftFunctionButton) {
        NSRange selectedRange = [PMNumberPad selectedRange:textInput];
        UITextField *textField = (UITextField *)textInput;
        if ([textField.delegate respondsToSelector:@selector(textField:shouldChangeCharactersInRange:replacementString:)]) {
            [textField.delegate textField:(UITextField *)textInput shouldChangeCharactersInRange:selectedRange replacementString:@"000"];
        } else {
            [textField insertText:@"000"];
        }
    } else if (functionButton == numberPad.doneButton) {
        [textInput resignFirstResponder];
    }
}

@end
