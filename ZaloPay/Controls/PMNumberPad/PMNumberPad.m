//
//  PMNumberPad.m
//  ZaloPayCommon
//
//  Created by PhucPv on 4/21/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "PMNumberPad.h"
#import <APNumberPad/APNumberButton.h>

@protocol PMNumberPadStyle <APNumberPadStyle>
+ (UIImage *)hideKeyBoardButtonImage;
@end

@interface PMNumberPadStyles : NSObject <PMNumberPadStyle>
@end

static inline UIColor * APNP_RGBa(int r, int g, int b, CGFloat alpha) {
    return [UIColor colorWithRed:r / 255.f
                           green:g / 255.f
                            blue:b / 255.f
                           alpha:alpha];
}
@implementation PMNumberPadStyles

#pragma mark - Pad

+ (CGRect)numberPadFrame {
    return CGRectMake(0.f, 0.f, 320.f, 216.f + ([UIView isIPhoneX] ? 20.f : 0.f));
}

+ (CGFloat)separator {
    return [UIScreen mainScreen].scale == 2.f ? 0.5f : 1.f;
}

+ (UIColor *)numberPadBackgroundColor {
    return APNP_RGBa(183, 186, 191, 1.f);
}

#pragma mark - Number button

+ (UIFont *)numberButtonFont {
    return [UIFont SFUITextRegularWithSize:28];
}

+ (UIColor *)numberButtonTextColor {
    return [UIColor blackColor];
}

+ (UIColor *)numberButtonBackgroundColor {
    return [UIColor whiteColor];
}

+ (UIColor *)numberButtonHighlightedColor {
    return APNP_RGBa(188, 192, 198, 1.f);
}

#pragma mark - Function button

+ (UIFont *)functionButtonFont {
    return [UIFont SFUITextRegularWithSize:28];
}

+ (UIColor *)functionButtonTextColor {
    return [UIColor blackColor];
}

+ (UIColor *)functionButtonBackgroundColor {
    return [UIColor whiteColor];
}

+ (UIColor *)functionButtonHighlightedColor {
    return APNP_RGBa(188, 192, 198, 1.f);
}

+ (UIImage *)clearFunctionButtonImage {
    return [UIImage imageNamed:@"PMNumberPad.bundle/ic_del"];
}

+ (UIImage *)hideKeyBoardButtonImage {
    return [UIImage imageNamed:@"PMNumberPad.bundle/ic_keyboard"];
}
@end
@interface APNumberPad (Extention)
@property (copy, readwrite, nonatomic) NSArray *numberButtons;
@property (strong, readwrite, nonatomic) APNumberButton *leftButton;
- (APNumberButton *)functionButton;
- (void)functionButtonAction:(id)sender;
@property (weak, readwrite, nonatomic) UIResponder<UITextInput> *textInput;
@property (nonatomic) Class<PMNumberPadStyle> styleClass;
@property (weak, readwrite, nonatomic) id<APNumberPadDelegate> delegate;
@end

@implementation PMNumberPad
+ (instancetype)numberPadWithDelegate:(id<APNumberPadDelegate>)delegate {
    PMNumberPad *numberPad =  [[self class] numberPadWithDelegate:delegate
                                              numberPadStyleClass:[PMNumberPadStyles class]];
    
    numberPad.hideKeyboardButton = [numberPad functionButton];
    [numberPad.hideKeyboardButton addTarget:numberPad
                                     action:@selector(hideKeyboardButtonAction:)
                           forControlEvents:UIControlEventTouchUpInside];
    [numberPad addSubview:numberPad.hideKeyboardButton];
    
    [numberPad.hideKeyboardButton setImage:[PMNumberPadStyles hideKeyBoardButtonImage]
                                  forState:UIControlStateNormal];
    
    
    numberPad.doneButton = [numberPad functionButton];
    [numberPad.doneButton addTarget:numberPad
                             action:@selector(functionButtonAction:)
                   forControlEvents:UIControlEventTouchUpInside];
    [numberPad addSubview:numberPad.doneButton];
    
    [numberPad.doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    numberPad.doneButton.titleLabel.font = [numberPad.styleClass numberButtonFont];
    [numberPad.doneButton setTitle:@"Xong" forState:UIControlStateNormal];
    [numberPad.doneButton setBackgroundImage:[UIImage imageFromColor:[UIColor disableButtonColor]]
                                    forState:UIControlStateDisabled];
    
    [numberPad.doneButton setBackgroundImage:[UIImage imageFromColor:[UIColor zaloBaseColor]]
                                    forState:UIControlStateNormal];
    
    [numberPad.doneButton setBackgroundImage:[UIImage imageFromColor:[UIColor highlightButtonColor]]
                                    forState:UIControlStateHighlighted];
    
    [numberPad.leftButton setTitle:@".000" forState:UIControlStateNormal];
    return numberPad;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    int rows = 4;
    int sections = 4;
    
    CGFloat sep = [self.styleClass separator];
    CGFloat left = 0.f;
    CGFloat top = 0.f;
    
#if defined(__LP64__) && __LP64__
    CGFloat buttonHeight = trunc((CGRectGetHeight(self.bounds) - ([UIView isIPhoneX] ? 20.f : 0.f) - sep * (rows - 1)) / rows) + sep;
#else
    CGFloat buttonHeight = truncf((CGRectGetHeight(self.bounds) - ([UIView isIPhoneX] ? 20.f : 0.f) - sep * (rows - 1)) / rows) + sep;
#endif
    
    CGSize buttonSize = CGSizeMake((CGRectGetWidth(self.bounds) - sep * (sections - 1)) / sections, buttonHeight);
    
    // Number buttons (1-9)
    //
    for (int i = 1; i < self.numberButtons.count - 1; i++) {
        APNumberButton *numberButton = self.numberButtons[i];
        numberButton.frame = CGRectMake(left, top, buttonSize.width, buttonSize.height);
        
        if (i % (sections - 1) == 0) {
            left = 0.f;
            top += buttonSize.height + sep;
        } else {
            left += buttonSize.width + sep;
        }
    }
    
    // Function button
    //
    left = 0.f;
    self.leftButton.frame = CGRectMake(left, top, buttonSize.width, buttonSize.height);
    
    // Number buttons (0)
    //
    left += buttonSize.width + sep;
    UIButton *zeroButton = self.numberButtons.firstObject;
    zeroButton.frame = CGRectMake(left, top, buttonSize.width, buttonSize.height);
    
    // Clear button
    //
    left += buttonSize.width + sep;
    self.hideKeyboardButton.frame = CGRectMake(left, top, buttonSize.width, buttonSize.height);
    
    left += buttonSize.width + sep;
    top = 0.0f;
    
    self.clearButton.frame = CGRectMake(left, top, buttonSize.width, buttonSize.height *2 + sep);
    top = buttonSize.height *2 + sep + sep;
    self.doneButton.frame = CGRectMake(left, top, buttonSize.width, buttonSize.height *2 + sep);
}

- (void)hideKeyboardButtonAction:(id)sender {
    [self.textInput resignFirstResponder];
}
@end
