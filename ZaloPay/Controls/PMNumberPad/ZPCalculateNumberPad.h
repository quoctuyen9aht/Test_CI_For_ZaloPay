//
//  PMCNumberPad.h
//  ZaloPay
//
//  Created by nhatnt on 21/11/2017.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <APNumberPad/APNumberPad.h>
#import <APNumberPad/APNumberPadStyle.h>
#import <APNumberPad/APNumberButton.h>
#import "PMNumberPad.h"
#import "ZaloPayCommon/ZPFloatTextInputView.h"

#define plusCharacter @"+"
#define minusCharacter @"-"
#define divideCharacter @"÷"
#define multiplyCharacter @"×"
#define autoFillCharacter @"000"
#define zeroCharacter @"0"
#define doneCharacter @"Xong"
#define acCharacter @"AC"

typedef NS_ENUM(NSInteger, ZPCalculationError) {
    ZPCalc_none                     = 0,
    ZPCalc_Wrong_Syntax             = -1,
    ZPCalc_Divide_By_Zero           = -2
};

typedef NS_ENUM(NSInteger, ZPCalcFunction) {
    ZPCalc_Add                      = 1,
    ZPCalc_Minus                    = 2,
    ZPCalc_Divide                   = 3,
    ZPCalc_Multiply                 = 4
};


@class ZPCalculateNumberPad;

@protocol PMCNumberPadControl
- (void)pmcContinueButtonClicked:(id)sender;
- (void)pmcKeyboard:(ZPCalculateNumberPad *)keyboard checkMinvalue:(int64_t)value fromFloatTextField:(ZPFloatTextInputView *)textInput;
@end

@interface ZPCalculateNumberPad : APNumberPad <APNumberPadDelegate>
@property (strong, readwrite, nonatomic) APNumberButton *hideKeyboardButton;
@property (strong, readwrite, nonatomic) APNumberButton *doneButton;
@property (strong, readwrite, nonatomic) APNumberButton *acButton;
@property (strong, readwrite, nonatomic) APNumberButton *addButton;
@property (strong, readwrite, nonatomic) APNumberButton *minusButton;
@property (strong, readwrite, nonatomic) APNumberButton *multiplyButton;
@property (strong, readwrite, nonatomic) APNumberButton *divideButton;
@property (strong, readwrite, nonatomic) APNumberButton *autoFillButton;
@property (nonatomic, assign) ZPCalculationError calcError;

- (long long)calc:(ZPFloatTextInputView *)textInputView;
- (BOOL)wrongSyntax:(ZPFloatTextInputView *)textInputView;
- (BOOL)isExpression:(NSString*)stringInput;

- (void)control:(ZPFloatTextInputView *)textInput
           with:(UIButton *)functionButton
 viewController:(id<PMCNumberPadControl, UITextFieldDelegate>)viewController;
@end
