//
//  PMNumberPad.h
//  ZaloPayCommon
//
//  Created by PhucPv on 4/21/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <APNumberPad/APNumberPad.h>
#import <APNumberPad/APNumberPadStyle.h>
#import <APNumberPad/APNumberButton.h>

@protocol PMNumberPadControl
- (void)pmContinueButtonClicked:(id)sender;
@end


@interface APNumberPad (selectRange)
+ (NSRange)selectedRange:(id<UITextInput>)textInput;
@end

@interface PMNumberPad : APNumberPad
@property (strong, readwrite, nonatomic) APNumberButton *hideKeyboardButton;
@property (strong, readwrite, nonatomic) APNumberButton *doneButton;
@end

