//
//  PMCNumberPad.m
//  ZaloPay
//
//  Created by nhatnt on 21/11/2017.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import "ZPCalculateNumberPad.h"
#import <APNumberPad/APNumberButton.h>
#import ZALOPAY_MODULE_SWIFT
#import <ZaloPayAnalyticsSwift/ZaloPayAnalyticsSwift-Swift.h>

@protocol PMCNumberPadStyle
+ (UIImage *)hideKeyBoardButtonImage;
@end

@interface PMCNumberPadStyles : NSObject <PMCNumberPadStyle>
@end

static inline UIColor * APNP_RGBa(int r, int g, int b, CGFloat alpha) {
    return [UIColor colorWithRed:r / 255.f
                           green:g / 255.f
                            blue:b / 255.f
                           alpha:alpha];
}

@implementation PMCNumberPadStyles
#pragma mark - Pad

+ (CGRect)numberPadFrame {
    float width = [UIScreen mainScreen].applicationFrame.size.width;
    if([UIView isIPhone5] || [UIView isIPhone4])
    {
        return CGRectMake(0.f, 0.f, width, 216.f + ([UIView isIPhoneX] ? 20.f : 0.f));
    }
    return CGRectMake(0.f, 0.f, width, 300.0 + ([UIView isIPhoneX] ? 20.f : 0.f));
}

+ (CGFloat)separator {
    return [UIScreen mainScreen].scale == 2.f ? 0.5f : 1.f;
}

+ (UIColor *)numberPadBackgroundColor {
    return APNP_RGBa(227, 230, 231, 1.f);
}

#pragma mark - Number button

+ (UIFont *)numberButtonFont {
    return [UIFont SFUITextRegularWithSize:27.5];
}

+ (UIColor *)numberButtonTextColor {
    return [UIColor blackColor];
}

+ (UIColor *)numberButtonBackgroundColor {
    return [UIColor whiteColor];
}

+ (UIColor *)numberButtonHighlightedColor {
    return APNP_RGBa(188, 192, 198, 1.f);
}

#pragma mark - Function button

+ (UIFont *)functionButtonFont {
    return [UIFont SFUITextRegularWithSize:27.5];
}

+ (UIColor *)functionButtonTextColor {
    return [UIColor blackColor];
}

+ (UIColor *)functionButtonBackgroundColor {
    return [UIColor whiteColor];
}

+ (UIColor *)functionButtonHighlightedColor {
    return APNP_RGBa(188, 192, 198, 1.f);
}

+ (UIImage *)clearFunctionButtonImage {
    return [UIImage imageNamed:@"PMCNumberPad.bundle/ic_del"];
}

+ (UIImage *)hideKeyBoardButtonImage {
    return [UIImage imageNamed:@"PMCNumberPad.bundle/ic_keyboard"];
}
@end

@interface APNumberPad (Extention)
@property (copy, readwrite, nonatomic) NSArray *numberButtons;
@property (strong, readwrite, nonatomic) APNumberButton *leftButton;
- (APNumberButton *)functionButton;
- (void)functionButtonAction:(id)sender;
@property (weak, readwrite, nonatomic) UIResponder<UITextInput> *textInput;
@property (nonatomic) Class<PMCNumberPadStyle> styleClass;
@property (weak, readwrite, nonatomic) id<APNumberPadDelegate> delegate;
@end

@implementation ZPCalculateNumberPad
+ (instancetype)numberPadWithDelegate:(id<APNumberPadDelegate>)delegate {
    
    ZPCalculateNumberPad *numberPad =  [[self class] numberPadWithDelegate:delegate
                                                       numberPadStyleClass:[PMCNumberPadStyles class]];
    numberPad.calcError = ZPCalc_none;
    numberPad.autoresizingMask = UIViewAutoresizingNone; // for support rotation
    
    
    
    numberPad.frame = [PMCNumberPadStyles numberPadFrame];
    numberPad.hideKeyboardButton = [numberPad functionButton];
    [numberPad.hideKeyboardButton addTarget:numberPad
                                     action:@selector(hideKeyboardButtonAction:)
                           forControlEvents:UIControlEventTouchUpInside];
    [numberPad addSubview:numberPad.hideKeyboardButton];
    [numberPad.hideKeyboardButton setImage:[PMCNumberPadStyles hideKeyBoardButtonImage]
                                  forState:UIControlStateNormal];
    [numberPad.hideKeyboardButton.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(10);
    }];
    numberPad.hideKeyboardButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    UIColor *colorFunction = [UIColor colorWithRed:0.93 green:0.94 blue:0.94 alpha:1.0];
    
    numberPad.acButton = [self createButtonWithName:acCharacter font:[UIFont SFUITextRegularWithSize:20] forNumberPad:numberPad];
    numberPad.divideButton = [self createButtonWithName:divideCharacter font:[numberPad.styleClass functionButtonFont] forNumberPad:numberPad];
    numberPad.addButton = [self createButtonWithName:plusCharacter font:[numberPad.styleClass functionButtonFont] forNumberPad:numberPad];
    numberPad.multiplyButton = [self createButtonWithName:multiplyCharacter font:[numberPad.styleClass functionButtonFont] forNumberPad:numberPad];
    numberPad.minusButton = [self createButtonWithName:minusCharacter font:[numberPad.styleClass functionButtonFont] forNumberPad:numberPad];
    numberPad.autoFillButton = [self createButtonWithName:autoFillCharacter font:[numberPad.styleClass numberButtonFont] forNumberPad:numberPad];
    [numberPad.autoFillButton setBackgroundImage:[UIImage imageFromColor: UIColor.whiteColor] forState:UIControlStateNormal];
    [numberPad.clearButton setBackgroundImage:[UIImage imageFromColor: colorFunction] forState:UIControlStateNormal];
    [numberPad.leftButton setTitle:zeroCharacter forState:UIControlStateNormal];
    numberPad.doneButton = [self createButtonWithName:doneCharacter font:[UIFont SFUITextRegularWithSize:22] forNumberPad:numberPad];
    [numberPad.doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [numberPad.doneButton setBackgroundImage:[UIImage imageFromColor:[UIColor disableButtonColor]]
                                    forState:UIControlStateDisabled];
    [numberPad.doneButton setBackgroundImage:[UIImage imageFromColor:[UIColor zaloBaseColor]]
                                    forState:UIControlStateNormal];
    [numberPad.doneButton setBackgroundImage:[UIImage imageFromColor:[UIColor highlightButtonColor]]
                                    forState:UIControlStateHighlighted];
    
    return numberPad;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    int rows = 5;
    int sections = 4;
    CGFloat sep = [self.styleClass separator];
    CGFloat left = 0.f;
    CGFloat top = 0.f;
    
#if defined(__LP64__) && __LP64__
    CGFloat buttonHeight = trunc((CGRectGetHeight(self.bounds) - ([UIView isIPhoneX] ? 20.f : 0.f) - sep * (rows - 1)) / rows) + sep;
#else
    CGFloat buttonHeight = truncf((CGRectGetHeight(self.bounds) - ([UIView isIPhoneX] ? 20.f : 0.f) - sep * (rows - 1)) / rows) + sep;
#endif
    
    CGSize buttonSize = CGSizeMake((CGRectGetWidth(self.bounds) - sep * (sections - 1)) / sections, buttonHeight);
    
    // First row of numberpad
    self.acButton.frame = CGRectMake(left, top, buttonSize.width, buttonSize.height);
    left+= buttonSize.width + sep;
    self.divideButton.frame = CGRectMake(left, top, buttonSize.width, buttonSize.height);
    left+= buttonSize.width + sep;
    self.multiplyButton.frame = CGRectMake(left, top, buttonSize.width, buttonSize.height);
    // Clear button
    left+= buttonSize.width + sep;
    self.clearButton.frame = CGRectMake(left, top, buttonSize.width, buttonSize.height);
    
    //Right column of numberPad
    top += buttonSize.height + sep;
    self.minusButton.frame = CGRectMake(left, top, buttonSize.width, buttonSize.height);
    top += buttonSize.height + sep;
    self.addButton.frame = CGRectMake(left, top, buttonSize.width, buttonSize.height);
    top += buttonSize.height + sep;
    self.doneButton.frame = CGRectMake(left, top, buttonSize.width, buttonSize.height * 2 + sep);
    
    // Number button (0)
    left = 0.f;
    top = 0.f;
    
    top += (buttonSize.height + sep) * sections;
    UIButton *zeroButton = self.numberButtons.firstObject;
    zeroButton.frame = CGRectMake(left, top, buttonSize.width, buttonSize.height);
    self.leftButton.frame = CGRectMake(left, top, buttonSize.width, buttonSize.height);
    
    // Function button
    left+= buttonSize.width + sep;
    self.autoFillButton.frame = CGRectMake(left, top, buttonSize.width, buttonSize.height);
    
    // Hide keyboard button
    left += buttonSize.width + sep;
    self.hideKeyboardButton.frame = CGRectMake(left, top, buttonSize.width, buttonSize.height);
    
    // Number buttons (1-9)
    for (int i = 1; i < self.numberButtons.count - 1; i++) {
        APNumberButton *numberButton = self.numberButtons[i];
        if ((i - 1) % (sections - 1) == 0) {
            left = 0.f;
            top -= buttonSize.height + sep;
        } else {
            left += buttonSize.width + sep;
        }
        numberButton.frame = CGRectMake(left, top, buttonSize.width, buttonSize.height);
    }
    
}

- (void)hideKeyboardButtonAction:(id)sender {
    [self.textInput resignFirstResponder];
    
}

- (BOOL)wrongSyntax:(ZPFloatTextInputView *)textInputView {
    NSString *textInput = textInputView.textField.text;
    
    NSMutableArray* input = [NSMutableArray new];
    input = [self convertToPostfix:textInput];
    for (int index = 0; index < input.count; index++){
        if([input[index] isEqual:@""]) {
            //[textInputView showErrorAuto:R.string_InputMoney_Syntax_Error_Message];
            [textInputView.textField becomeFirstResponder];
            return TRUE;
        }
    }
    return FALSE;
}

- (long long)calc:(ZPFloatTextInputView *)textInputView {
    NSString *textInput = textInputView.textField.text;
    self.calcError = ZPCalc_none;
    
    NSMutableArray* input = [NSMutableArray new];
    NSMutableArray* stack = [NSMutableArray new];
    input = [self convertToPostfix:textInput];
    long long result = 0;
    for (int index = 0; index < input.count; index++){
        if([input[index] isEqual:@""]) {
            [textInputView showErrorAuto:R.string_InputMoney_Syntax_Error_Message];
            self.calcError = ZPCalc_Wrong_Syntax;
            return LONG_MIN;
        }
        
        if (![self isOperator:input[index]]) {
            [stack addObject:input[index]];
            continue;
        }
        
        long long num2 = [self validMoneyNumber:stack.lastObject];
        [stack removeLastObject];
        
        long long num1 = [self validMoneyNumber:stack.lastObject];
        [stack removeLastObject];
        
        if ([input[index] isEqual: plusCharacter]) {
            result = num1 + num2;
            [stack addObject:[NSString stringWithFormat:@"%lli", result]];
        }
        if ([input[index] isEqual: minusCharacter]) {
            result = num1 - num2;
            [stack addObject:[NSString stringWithFormat:@"%lli", result]];
        }
        if ([input[index] isEqual: divideCharacter]) {
            if (num2 == 0) {
                [textInputView showErrorAuto:R.string_InputMoney_Cant_Divide_By_Zero_Error_Message];
                self.calcError = ZPCalc_Divide_By_Zero;
                return LONG_MIN;
            }
            result = num1 / num2;
            [stack addObject:[NSString stringWithFormat:@"%lli", result]];
        }
        if ([input[index] isEqual: multiplyCharacter]) {
            result = num1 * num2;
            [stack addObject:[NSString stringWithFormat:@"%lli", result]];
        }
    }
    return result;
}


- (BOOL)isExpression:(NSString *)stringInput {
    if([stringInput containsString:plusCharacter] || [stringInput containsString:minusCharacter]
       || [stringInput containsString:divideCharacter] || [stringInput containsString:multiplyCharacter]) {
        return TRUE;
    }
    return FALSE;
}

- (int)getPriority: (NSString*)operation {
    if(![self isOperator:operation] || operation == nil){
        return 0;
    }
    if([operation isEqual: multiplyCharacter] || [operation isEqual: divideCharacter]) {
        return 2;
    }
    else {
        return 1;
    }
}

- (BOOL)isOperator: (NSString*)character {
    NSCharacterSet *s = [NSCharacterSet characterSetWithCharactersInString:@"+-×÷"];
    NSRange r = [character rangeOfCharacterFromSet:s];
    if (r.location != NSNotFound) {
        return TRUE;
    }
    return FALSE;
}

- (NSMutableArray*)convertToPostfix: (NSString*)expression {
    NSMutableArray* stack = [NSMutableArray new];
    NSMutableArray* output = [NSMutableArray new];
    
    NSArray *listItems = [expression componentsSeparatedByString:@" "];
    for (int index = 0; index < listItems.count; index++) {
        if([self isOperator:listItems[index]]){
            if([self getPriority:listItems[index]] <= [self getPriority:stack.lastObject]){
                while (stack.count > 0) {
                    [output addObject:stack.lastObject];
                    [stack removeLastObject];
                }
            }
            [stack addObject:listItems[index]];
        } else {
            [output addObject:listItems[index]];
        }
    }
    while (stack.count > 0) {
        [output addObject:stack.lastObject];
        [stack removeLastObject];
    }
    return output;
}

- (void)control:(ZPFloatTextInputView *)textInput
           with:(UIButton *)functionButton
 viewController:(id<PMCNumberPadControl, UITextFieldDelegate>)viewController {
    
    NSString *textFill = @"";
    if (functionButton == self.leftButton) {
        [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionKeyboard_touch_0];
    }
    if (functionButton == _acButton) {
        [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionKeyboard_touch_delete];
        textInput.textField.text = @"";
    }
    if (functionButton == _doneButton) {
        BOOL isExpression = [self isExpression:textInput.textField.text];
        
        if (!isExpression) {
            [viewController pmcContinueButtonClicked:nil];
            return;
        }
        long long result = [self calc:textInput];
        if (result == LONG_MIN) {
            return;
        }
        
        [viewController pmcKeyboard:self checkMinvalue:result fromFloatTextField:textInput];
        
        if(result < 0){
            return;
        }
        NSString* stringResult = [[[NSString stringWithFormat:@"%lli", result] onlyDigit] formatMoneyValue];
        textInput.textField.text = stringResult;
        return;
    }
    if (functionButton == _addButton) {
        [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionKeyboard_touch_plus];
        textFill = [NSString stringWithFormat:@" %@ ", plusCharacter];
        [self addFunctionCharacter:textFill inTextInput:textInput of:viewController];
    }
    if (functionButton == _minusButton) {
        [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionKeyboard_touch_minus];
        textFill = [NSString stringWithFormat:@" %@ ", minusCharacter];
        [self addFunctionCharacter:textFill inTextInput:textInput of:viewController];
    }
    if (functionButton == _multiplyButton) {
        [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionKeyboard_touch_multiplication];
        textFill = [NSString stringWithFormat:@" %@ ", multiplyCharacter];
        [self addFunctionCharacter:textFill inTextInput:textInput of:viewController];
    }
    if (functionButton == _divideButton) {
        [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionKeyboard_touch_divide];
        textFill = [NSString stringWithFormat:@" %@ ", divideCharacter];
        [self addFunctionCharacter:textFill inTextInput:textInput of:viewController];
    }
    
    if (functionButton == _autoFillButton) {
        [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionKeyboard_touch_000];
        textFill = autoFillCharacter;
        BOOL isInsert = [viewController textField:textInput.textField shouldChangeCharactersInRange:NSMakeRange(textInput.textField.text.length, 0)
                                replacementString:textFill];
        if (isInsert) {
            [textInput.textField insertText:textFill];
        }
    }
}

-(void)addFunctionCharacter:(NSString*)character inTextInput:(ZPFloatTextInputView *)textInput of:(id<UITextFieldDelegate>)viewController{
    NSCharacterSet *digits = [NSCharacterSet decimalDigitCharacterSet];
    if ([textInput.textField.text isEqual: @""]) {
        return;
    }
    if (![digits characterIsMember:[textInput.textField.text characterAtIndex:textInput.textField.text.length-1]]) {
        return;
    }
    BOOL isInsert = [viewController textField:textInput.textField shouldChangeCharactersInRange:NSMakeRange(textInput.textField.text.length, 0)
                            replacementString:character];
    if (isInsert) {
        [textInput.textField insertText:character];
    }
}

-(long long)validMoneyNumber:(NSString*)string{
    string = [string stringByReplacingOccurrencesOfString:@"." withString:@""];
    return  [string longLongValue];
}

+(APNumberButton*) createButtonWithName:(NSString*)nameButton font:(UIFont*)font forNumberPad:(ZPCalculateNumberPad*)numberPad {
    UIColor *colorFunction = [UIColor colorWithRed:0.93 green:0.94 blue:0.94 alpha:1.0];
    
    APNumberButton *button = [[APNumberButton alloc] init];
    
    button = [numberPad functionButton];
    [button addTarget:numberPad
               action:@selector(functionButtonAction:)
     forControlEvents:UIControlEventTouchUpInside];
    button.titleLabel.font = font;
    [button setTitle:nameButton forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageFromColor: colorFunction]
                      forState:UIControlStateNormal];
    [numberPad addSubview: button];
    return button;
}

@end
