//
//  ZPAuthNumberPad.m
//  ZaloPay
//
//  Created by nhatnt on 17/03/2018.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import "ZPAuthNumberPad.h"
#import ZALOPAY_MODULE_SWIFT

@protocol ZPAuthNumberPadStyle <APNumberPadStyle>
+ (UIImage *)TouchIDButtonImage;
@end

@interface ZPAuthNumberPadStyles : NSObject <ZPAuthNumberPadStyle>
@end

static inline UIColor * APNP_RGBa(int r, int g, int b, CGFloat alpha) {
    return [UIColor colorWithRed:r / 255.f
                           green:g / 255.f
                            blue:b / 255.f
                           alpha:alpha];
}
@implementation ZPAuthNumberPadStyles

#pragma mark - Pad

+ (CGRect)numberPadFrame {
    return CGRectMake(0.f, 0.f, 320.f, 216.f + ([UIView isIPhone4] ? 0.f : 20.f) + ([UIView isIPhoneX] ? 20.f : 0.f));
}

+ (CGFloat)separator {
    return [UIScreen mainScreen].scale == 2.f ? 15.f : 16.f;
}

+ (UIColor *)numberPadBackgroundColor {
    return APNP_RGBa(102, 201, 232, 1.f);
}

#pragma mark - Number button

+ (UIFont *)numberButtonFont {
    return [UIFont SFUITextRegularWithSize:28];
}

+ (UIColor *)numberButtonTextColor {
    return [UIColor whiteColor];
}

+ (UIColor *)numberButtonBackgroundColor {
    return [UIColor zaloBaseColor];
}

+ (UIColor *)numberButtonHighlightedColor {
    return APNP_RGBa(0, 136, 218, 1.f);
}

#pragma mark - Function button

+ (UIFont *)functionButtonFont {
    return [UIFont SFUITextRegularWithSize:28];
}

+ (UIColor *)functionButtonTextColor {
    return [UIColor blackColor];
}

+ (UIColor *)functionButtonBackgroundColor {
    return [UIColor zaloBaseColor];
}

+ (UIColor *)functionButtonHighlightedColor {
    return APNP_RGBa(0, 136, 218, 1.f);
}

+ (UIImage *)clearFunctionButtonImage {
    return nil;
}

+ (UIImage *)TouchIDButtonImage {
    return [UIImage imageWithIcon:@"key_touchid"
                  backgroundColor:[UIColor clearColor]
                        iconColor:[UIColor whiteColor]
                          andSize:CGSizeMake(30, 30)];
}

@end

@interface APNumberPad (Extention)
@property (copy, readwrite, nonatomic) NSArray *numberButtons;
@property (weak, readwrite, nonatomic) id<APNumberPadDelegate> delegate;
@end


@implementation ZPAuthNumberPad
+ (instancetype)numberPadWithDelegate:(id<APNumberPadDelegate>)delegate {
    ZPAuthNumberPad *numberPad =  [[self class] numberPadWithDelegate:delegate
                                              numberPadStyleClass:[ZPAuthNumberPadStyles class]];

    numberPad.autoresizingMask = UIViewAutoresizingNone; // for support rotation
    numberPad.frame = [ZPAuthNumberPadStyles numberPadFrame];
    [numberPad.leftFunctionButton setImage:[ZPAuthNumberPadStyles TouchIDButtonImage] forState:UIControlStateNormal];
    [numberPad.leftFunctionButton.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(10);
    }];
    numberPad.leftFunctionButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    [numberPad.clearButton setIconFont:@"key_delete" forState:UIControlStateNormal];
    [numberPad.clearButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    numberPad.clearButton.titleLabel.font = [UIFont iconFontWithSize:25];
    numberPad.clearButton.frame = CGRectMake(0, 0, 30, 30);
    return numberPad;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    int rows = 4;
    int sections = 3;
    
    CGFloat sep = [self.styleClass separator];
    CGFloat left = 15.f;
    CGFloat top = 0.f;
    
#if defined(__LP64__) && __LP64__
    CGFloat buttonHeight = trunc((CGRectGetHeight(self.bounds) - sep ) / rows);
#else
    CGFloat buttonHeight = truncf((CGRectGetHeight(self.bounds) - sep ) / rows);
#endif
    
    CGSize buttonSize = CGSizeMake((CGRectGetWidth(self.bounds) - sep * (sections + 1)) / sections , buttonHeight);
    
    // Number buttons (1-9)
    //
    for (int i = 1; i < self.numberButtons.count - 1; i++) {
        APNumberButton *numberButton = self.numberButtons[i];
        numberButton.frame = CGRectMake(left, top, buttonSize.width, buttonSize.height);
        [self addSubview: [self createUnderlineViewWithLeftFrame:left withTopFrame:top + buttonSize.height widthFrame:buttonSize.width]];
        
        if (i % sections == 0) {
            left = 15.f;
            top += buttonSize.height;
        } else {
            left += buttonSize.width + sep;
        }
    }
    
    //Function button
    //
    left = 15.f;

    if (!UIView.isIPhoneX && self.isEnableTouchID) {
        self.leftFunctionButton.frame = CGRectMake(left, top, buttonSize.width, buttonSize.height);
    }
    
    // Number buttons (0)
    //
    left += buttonSize.width + sep;
    UIButton *zeroButton = self.numberButtons.firstObject;
    zeroButton.frame = CGRectMake(left, top, buttonSize.width, buttonSize.height);
    [self addSubview: [self createUnderlineViewWithLeftFrame:left withTopFrame:top + buttonSize.height  widthFrame:buttonSize.width]];
    
    // Clear button
    //
    left += buttonSize.width + sep;
    self.clearButton.frame = CGRectMake(left, top, buttonSize.width, buttonSize.height);
}

- (BOOL) isEnableTouchID {
    BOOL usingTouchId =  [[ZPSettingsConfig sharedInstance] usingTouchId];
    BOOL touchIdExist = [[ZPTouchIDService sharedInstance] isTouchIDExist];
    BOOL isEnableTouchId = [[ZPTouchIDService sharedInstance] isEnableTouchID];
    return usingTouchId && touchIdExist && isEnableTouchId;
}

- (UIView *) createUnderlineViewWithLeftFrame:(CGFloat)left withTopFrame:(CGFloat)top widthFrame:(CGFloat)width {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(left, top, width, 0.5f)];
    view.backgroundColor = APNP_RGBa(102, 188, 239, 1.f);
    return view;
}

@end
