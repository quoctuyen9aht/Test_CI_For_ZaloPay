//
//  ZPKeyboardConfig.swift
//  ZaloPay
//
//  Created by Bon Bon on 12/19/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayConfig

enum ZPCalculateKeyboardSource: String {
    case recharge = "recharge"
    case transfer = "transfer"
    case receive = "receive"
}

class ZPKeyboardConfig {
    
    class func enableCalculateKeyboard(_ source: ZPCalculateKeyboardSource) -> Bool {
        guard let config = ZPApplicationConfig.getKeypadConfig() else {
            return true
        }
        
        switch (source) {
        case .recharge:
            return config.getRecharge().toBool()
        case .transfer:
            return config.getTransfer().toBool()
        case .receive:
            return config.getReceive().toBool()
        }
    }
}
