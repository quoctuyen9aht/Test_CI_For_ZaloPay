//
//  MDHTMLLabelCustom.m
//  ZaloPay
//
//  Created by vuongvv on 5/28/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import "MDHTMLLabelCustom.h"
#import <ZaloPayCommon/UIView+Config.h>
@implementation MDHTMLLabelCustom
- (NSString *)detectURLsInText:(NSString *)text
{
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:NULL];
    
    NSUInteger matchDetectorStartLocation = 0;
    NSTextCheckingResult *match = [detector firstMatchInString:text
                                                       options:kNilOptions
                                                         range:NSMakeRange(matchDetectorStartLocation, text.length)];
    
    while (match != nil && match.range.location != NSNotFound)
    {
        NSUInteger matchLength = match.range.length;
        if (match.resultType == NSTextCheckingTypeLink)
        {
            // if there's no "<a" or "href" before the link regardless of spaces. e.g. this is valid <a href = "SomeURL">
            BOOL insideHref = NO;
            // an href
            // there has to be room for at least "<a href='" before we bother checking this
            if ((match.range.location - matchDetectorStartLocation) >= 8) {
                NSRange prevTextRange = NSMakeRange(matchDetectorStartLocation, (match.range.location - 1) - matchDetectorStartLocation);
                NSRange prevHrefRange = [text rangeOfString:@"href" options:NSCaseInsensitiveSearch | NSBackwardsSearch range:prevTextRange];
                NSRange prevStartATagRange = [text rangeOfString:@"<a" options:NSCaseInsensitiveSearch | NSBackwardsSearch range:prevTextRange];
                NSRange prevEndTagRange = [text rangeOfString:@">" options:NSCaseInsensitiveSearch | NSBackwardsSearch range:prevTextRange];
                insideHref = (prevHrefRange.location != NSNotFound && prevStartATagRange.location != NSNotFound &&
                              NSMaxRange(prevStartATagRange) < prevHrefRange.location &&
                              (prevEndTagRange.location == NSNotFound || (prevEndTagRange.location != NSNotFound &&
                                                                          prevStartATagRange.location >= NSMaxRange(prevEndTagRange))));
            }
            
            BOOL wrappedInAnchors = [text rangeOfString:@"a>" options:NSCaseInsensitiveSearch | NSBackwardsSearch range:match.range].location != NSNotFound;
            
            if (!insideHref && !wrappedInAnchors)
            {
                // Custom remove http://
                // maybe have path
                NSString *urlShow = [NSString stringWithFormat:@"%@%@",match.URL.host,match.URL.path];
                int fontSize = [UIView isScreen2x] ? 14 : 16;
                NSString *wrappedURL = [NSString stringWithFormat:@"<font size='%i' color='#727F8C'><label href='%@'>%@</label></font>", fontSize,match.URL.absoluteString, urlShow];
                text = [text stringByReplacingCharactersInRange:match.range
                                                     withString:wrappedURL];
                matchLength = wrappedURL.length;
            }
        }
        
        matchDetectorStartLocation = match.range.location + matchLength;
        match = [detector firstMatchInString:text
                                     options:kNilOptions
                                       range:NSMakeRange(matchDetectorStartLocation, text.length - matchDetectorStartLocation)];
    }
    
    return text;
}
@end
