//
//  ZPContactListPresenter.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 7/19/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayProfile

class ZPContactListPresenter: ZPContactListPresenterProtocol {
    weak var view: ZPContactListViewProtocol?
    var interactor: ZPContactListInteractorInputProtocol?
    var router: ZPContactListRouterProtocol?
    var completeHandle:((ZPContactSwift?) -> ())?
    var redPacketCompleteHandle:(([ZPContactSwift]?, ZPCEndSelectedRedPacket) -> ())?
    var updatingCompleteHandle:(() -> Void)?
    
    lazy var contactList = ZPContactList()
    lazy var inviteFriendInteractor = ZPCInviteFriendModel()
    
    func updateListContact(completeHandle: (() -> Void)?) {
        guard self.contactList.isPermissionGetAddressBook() else {
            showAllowAccessContactScreen()
            return
        }
        self.updatingCompleteHandle = completeHandle
        self.getListContact(sync: true)
    }
    
    func getListContact(sync: Bool) {
        let zpMode = ((self.view?.getMode() ?? .all) == .zalopay)
        view?.showLoading()
        if sync {
            interactor?.retrieveZPContactList(zaloPayMode: zpMode, syncContact: true)
        } else {
            interactor?.retrieveCachedZPContactList(zaloPayMode: zpMode)
        }
    }
    
    func requestUserInfoWithZaloPayId(accountName: String) {
        // conflict showing loading with searchWithText
        self.view?.showLoading()
        self.interactor?.fetchDataSelectingZaloPayId(accountName: accountName)
    }
    
    func processGroupData(_ zaloPayMode: Bool) {
        interactor?.processGroupData(zaloPayMode)
    }
    
    func getCurrentContactMode() -> ContactMode {
        return (interactor?.getCurrentContactMode())!
    }
    
    func getEnableFavoriteMode() -> Bool {
        var state = interactor?.getEnableFavoriteMode() ?? false
        state = state && self.view?.config.sourceZPC != .fromRedPacket
        return state
    }
    
    func showZPUpdateConatct() {
        router?.presentZPUpdateContactScreen(from: view!)
    }
    
    func showAllowAccessContactScreen() {
        router?.presentZPAllowAccessContactScreen(from: view!)
    }
    
    func searchWithText(_ text: String) {
        if self.contactList.isPhone(text) {
            view?.showLoading()
        }
        let zpMode = self.view?.getMode() ?? .all
        interactor?.searchWithText(text, zaloPayMode: (zpMode == .zalopay))
    }
    
    func viewWillPopout() {
        if self.completeHandle != nil {
            self.completeHandle!(nil)
        }
    }

    func didSelectZaloUser(_ zaloUser:ZPContactSwift, reqUserInfo: Bool) {
        
        if reqUserInfo {
            view?.showLoading()
            self.interactor?.retrieveUserInfoFromServer(by: zaloUser.phoneNumber)
            return
        }
        // If launch ZPC from Topup (phone recharge) => handle for all contacts
        if view?.config.sourceZPC != .fromTopup && view?.config.sourceZPC != .fromRedPacket {
            if zaloUser.usedZaloPay == false {
                let source = view?.config.sourceZPC ?? .fromTransferMoney_Contact
                self.inviteFriendInteractor.alertConfirm(zaloUser, source: source)
                return
            }
        }
        if view?.config.sourceZPC == ZPCSource.fromRedPacket {
            view?.selectedUserToRedPacket(zaloUser)
        } else {
            self.completeHandle?(zaloUser)
        }
        
    }
    func btnNextRedPacketTouch(_ arrContacts: [ZPContactSwift],_ endSelectedRedPacket:ZPCEndSelectedRedPacket) {
        if self.redPacketCompleteHandle != nil {
            self.redPacketCompleteHandle!(arrContacts,endSelectedRedPacket)
        }
    }
    
    func setViewMode() {
        guard let v = view else {
            return
        }
        interactor?.setViewModelZPC(source : v.config.sourceZPC!, viewMode : v.config.viewMode!)
    }
    
    func prepareUsersRedPacket(_ users: [Any]?) {
        interactor?.prepareUsersRedPacket(users)
    }
    
    func getConfigEnableInviteZP() -> Bool {
        return self.inviteFriendInteractor.getConfigEnableInviteZP()
    }
    
    func getDateUpdate() -> String? {
        return self.interactor?.retrieveDateUpdate()
    }
    
    func getTextGuide() -> String? {
        return self.interactor?.retrieveGuideContent()
    }
}

extension ZPContactListPresenter: ZPContactListInteractorOutputProtocol {
    func loadSucceedZaloPayId(user: UserTransferData) {
        self.view?.hideLoading()
        self.router?.openTransferReceiveController(transferData: user)
    }
    
    func loadFailedZaloPayId(message: String) {
        self.view?.hideLoading()
        self.view?.showErrorSelectingZaloPayId(withText: message)
    }
    
    func showErrorZaloPayId(withText content: String) {
        self.view?.showErrorSelectingZaloPayId(withText: content)
    }
    
    func didRetrieveZPContacts(_ dataSection: [String], dictionary: [String : [ZPContactSwift]], dataFavorite: NSArray, totalFriend: Int) {
        view?.hideLoading()
        updatingCompleteHandle?()
        updatingCompleteHandle = nil
        
        var sectionKeys = dataSection
        var dictContact = dictionary
        
        if self.getEnableFavoriteMode().not,
            let interactor = self.interactor,
            let favorite = dataFavorite as? [ZPContactSwift] {
            sectionKeys = interactor.combineDataToDictionary(&dictContact, data: favorite)
        }
        
        interactor?.filterAllRedPacketContacts(from: &dictContact)
        sectionKeys = sectionKeys.filter { (key) -> Bool in
            return dictContact[key]!.count > 0
        }
        view?.showZPContacts(sectionKeys, dictionary: dictContact, dataFavorite: dataFavorite, totalFriend: totalFriend)
    }
       
    func didPrepareUsersRedPacket(_ contacts: [ZPContactSwift]) {
        view?.addContactsRedPacketWhenReady(contacts)
    }
    
    func didRetrieveUserInfo(_ zaloUser: ZPContactSwift) {
        view?.hideLoading()
        self.didSelectZaloUser(zaloUser, reqUserInfo: false)
    }
    
    func resultSearch(_ listSearchView: NSMutableArray,_ listFriendRedPacket: NSMutableArray, _ listFriendFavorite: NSArray,_ phoneModel : ZPContactSwift?) {
        view?.hideLoading()
        if self.getEnableFavoriteMode().not,
                let interactor = self.interactor,
                var search = listSearchView as? [ZPContactSwift],
                let favorite = listFriendFavorite as? [ZPContactSwift] {
            interactor.combineDataToArray(&search, data: favorite)
            view?.resultDataSearch(NSMutableArray(array: search), listFriendRedPacket, listFriendFavorite, phoneModel)
            return
        }
        view?.resultDataSearch(listSearchView, listFriendRedPacket, listFriendFavorite, phoneModel)
    }
    func searchUserPhoneNotNetwork(_ error:NSError) {
        view?.alertSearchUserPhoneNotNetwork(error)
    }
}

extension ZPContactListPresenter : ZPContactListViewDelegateProtocol {
    func addedRedPacketContact(_ contact: ZPContactSwift) {
        let zpContactID = contact.zpContactID
        if self.interactor?.redPacketZPContactIds.contains(zpContactID) == false {
            self.interactor?.redPacketZPContactIds.append(zpContactID)
        }
        
    }
    
    func removedRedPacketContact(_ contact: ZPContactSwift) {
        let zpContactID = contact.zpContactID
        if let idx = self.interactor?.redPacketZPContactIds.index(of: zpContactID) {
            self.interactor?.redPacketZPContactIds.remove(at: idx)
        }
        self.interactor?.arrayRedPacketUnsaveContactsRemove(contact: contact)
    }
}
