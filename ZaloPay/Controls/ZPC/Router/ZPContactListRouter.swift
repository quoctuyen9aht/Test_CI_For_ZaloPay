//
//  ZPContactListRouter.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 7/19/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayCommonSwift
import ZaloPayProfile

@objcMembers
public class ZPContactListRouter: NSObject, ZPContactListRouterProtocol {
    
    func openViewController(viewController: UIViewController) {
        guard let navi = ZPAppFactory.sharedInstance().rootNavigation() else {
            return
        }
        navi.pushViewController(viewController, animated: true)
    }
    
    func openTransferReceiveController(transferData user: UserTransferData) {
        let viewController = TransferReceiverRouter.assembleModule(transferData: user)
        self.openViewController(viewController: viewController)
    }
    
   
    weak var zpUpdateContactViewController:ZPUpdateContactViewController?
    
    class func launchFromTopup(_ contactMode:ContactMode,
                                 currentPhone:String?,
                                 displayMode:ZPCListViewMode,
                                 viewMode:String?,
                                 navigationTitle:String?,
                                 completion: @escaping (ZPContactSwift?) -> ()) {
        return launchContactList(contactMode,
                                 currentPhone: currentPhone,
                                 displayMode: displayMode,
                                 viewMode: viewMode,
                                 navigationTitle: navigationTitle,
                                 source: ZPCSource.fromTopup,
                                 completion: completion)
        
    }
    
    
    
    class func createZPContactListModule(_ contactMode:ContactMode,
                                         currentPhone:String?,
                                         displayMode:ZPCListViewMode,
                                         viewMode:String?,
                                         source:ZPCSource?,
                                         navigationTitle:String?) -> UIViewController {
        
        let navController = getViewController("ZPContactListViewControllerNav")
        if let view = navController.childViewControllers.first as? ZPContactListViewController {
            let presenter: ZPContactListPresenterProtocol & ZPContactListInteractorOutputProtocol = ZPContactListPresenter()
            let interactor: ZPContactListInteractorInputProtocol = ZPContactListInteractor()
            let router: ZPContactListRouterProtocol = ZPContactListRouter()
            
            view.presenter = presenter
            view.config.sourceZPC = source
            view.config.viewMode = view.config.viewMode ?? viewMode
            view.config.displayMode = displayMode
            
            presenter.view = view
            presenter.router = router
            presenter.interactor = interactor
            interactor.presenter = presenter
            
            interactor.setContactMode(contactMode)
            if navigationTitle != nil {
                view.navigationTitle = navigationTitle
            }
            
            view.config.isKeyBoardNumperPad = interactor.isNumberPadKeyboad(source!)
            view.config.viewMode = view.config.isKeyBoardNumperPad  ? ViewModeKeyboard_Phone : ViewModeKeyboard_ABC

            if currentPhone != nil && currentPhone != "" {
                view.config.currentPhone = currentPhone
            }
            return view
        }
        return UIViewController()
    }
    
    
    class func launchFromRedPacket(_ contactMode:ContactMode,
                                   backgroundColor:String?,
                                   displayMode:Int,
                                   viewMode:String?,
                                   maxUsersSelected: Int,
                                   arrayRedPacket:Array<Any>?,
                                   navigationTitle:String?,
                                   completion: @escaping ([ZPContactSwift]?,ZPCEndSelectedRedPacket) -> ()) {
        let mode = ZPCListViewMode(rawValue: displayMode) ?? .normal
        return launchContactList(contactMode,
                                 backgroundColor: UIColor(fromHexString: backgroundColor) ,
                                 maxUsersSelected: maxUsersSelected,
                                 arrayRedPacket: arrayRedPacket,
                                 displayMode: mode,
                                 viewMode: viewMode,
                                 navigationTitle: navigationTitle,
                                 source: ZPCSource.fromRedPacket,
                                 completion: completion)
        
    }
    
    class func createZPContactListModuleFromRedPacket(_ contactMode:ContactMode,
                                         backgroundColor:UIColor?,
                                         maxUsersSelected: Int,
                                         arrayRedPacket:Array<Any>?,
                                         displayMode:ZPCListViewMode,
                                         viewMode:String?,
                                         source:ZPCSource?,
                                         navigationTitle:String?) -> UIViewController {
        
        let navController = getViewController("ZPContactListViewControllerNav")
        if let view = navController.childViewControllers.first as? ZPContactListViewController {
            let presenter: ZPContactListPresenterProtocol & ZPContactListInteractorOutputProtocol = ZPContactListPresenter()
            let interactor: ZPContactListInteractorInputProtocol = ZPContactListInteractor()
            let router: ZPContactListRouterProtocol = ZPContactListRouter()
            
            view.presenter = presenter
            view.config.sourceZPC = source
            view.config.viewMode = view.config.viewMode ?? viewMode
            view.config.displayMode = displayMode
            
            presenter.view = view
            presenter.router = router
            presenter.interactor = interactor
            interactor.presenter = presenter
            
            interactor.setContactMode(contactMode)
            if navigationTitle != nil {
                view.navigationTitle = navigationTitle
            }
            
            view.config.maxForRedPacKet = maxUsersSelected
            view.config.backgroundColor = backgroundColor ?? UIColor.zaloBase()
            view.config.isKeyBoardNumperPad = interactor.isNumberPadKeyboad(source!)
            view.config.viewMode = view.config.isKeyBoardNumperPad  ? ViewModeKeyboard_Phone : ViewModeKeyboard_ABC
            view.config.arrUsersRedPacket = arrayRedPacket
            
            return view
        }
        return UIViewController()
    }
    
    class func launchContactList(_ contactMode:ContactMode,
                                 backgroundColor:UIColor?,
                                 maxUsersSelected: Int,
                                 arrayRedPacket:Array<Any>?,
                                 displayMode:ZPCListViewMode,
                                 viewMode:String?,
                                 navigationTitle:String?,
                                 source: ZPCSource,
                                 completion: @escaping ([ZPContactSwift]?, ZPCEndSelectedRedPacket) -> ()) {
        
        let controller = createZPContactListModuleFromRedPacket(contactMode,
                                                                backgroundColor: backgroundColor,
                                                                maxUsersSelected: maxUsersSelected,
                                                                arrayRedPacket: arrayRedPacket,
                                                                displayMode: displayMode,
                                                                viewMode: viewMode,
                                                                source: source,
                                                                navigationTitle: navigationTitle) as? ZPContactListViewController
        if let presenter = controller?.presenter as? ZPContactListPresenter {
            presenter.redPacketCompleteHandle = completion
        }
        let navi = getNavigation();
        navi?.pushViewController(controller!, animated: true)
    }

    class func launchContactList(_ contactMode:ContactMode,
                                 currentPhone:String?,
                                 displayMode:ZPCListViewMode,
                                 viewMode:String?,
                                 navigationTitle:String?,
                                    source: ZPCSource,
                                 completion: @escaping (ZPContactSwift?) -> ()) {
        let controller = createZPContactListModule(contactMode,
                                                   currentPhone: currentPhone,
                                                   displayMode: displayMode,
                                                   viewMode: viewMode,
                                                   source: source,
                                                   navigationTitle:navigationTitle) as? ZPContactListViewController
        if let presenter = controller?.presenter as? ZPContactListPresenter {
            presenter.completeHandle = completion
        }
        let navi = getNavigation();
        navi?.pushViewController(controller!, animated: true)
    }
    
    class func getNavigation() -> UINavigationController? {     
        return ZPAppFactory.sharedInstance().rootNavigation();
    }
    
    
    class func getViewController(_ name: String) -> UIViewController {
        let storyboard = UIStoryboard(name: "ZPContact", bundle: Bundle.main)
        guard let controller = storyboard.instantiateViewController(withIdentifier: name) as UIViewController? else {
            return UIViewController()
        }
        return controller
    }
    
    func presentZPUpdateContactScreen(from view: ZPContactListViewProtocol) {
        zpUpdateContactViewController = ZPContactListRouter.getViewController("ZPUpdateContactViewController") as? ZPUpdateContactViewController
        zpUpdateContactViewController?.presenter = view.presenter
                
        if let sourceView = view as? UIViewController {
            sourceView.navigationController?.pushViewController(zpUpdateContactViewController!, animated: true)
        }
    }

    func presentZPAllowAccessContactScreen(from view: ZPContactListViewProtocol) {
        let zpAllowAccessContactViewController = ZPContactListRouter.getViewController("ZPAllowAccessContactViewController") as? ZPAllowAccessContactViewController
        let navi = ZPContactListRouter.getNavigation()
        navi?.pushViewController(zpAllowAccessContactViewController!, animated: true)
    }    
    
}
