//
//  ZPContactListProtocol.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 7/19/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import Foundation
import UIKit
import ZaloPayProfile

enum ZPCSource: String {
    case fromTransferMoney_Contact          = "ZPCSource_fromTransferMoney_Contact"
    case fromTransferMoney_PhoneNumber      = "ZPCSource_fromTransferMoney_PhoneNumber"
    case fromTopup                          = "ZPCSource_fromTopup"
    case fromTransferNone                   = "ZPCSource_fromTransferNone"
    case fromRedPacket                      = "ZPCSource_fromRedPacket"
}

@objc enum ZPCListViewMode: Int {
    case normal = 1, zalopay, all
}

@objc enum ZPCEndSelectedRedPacket: Int {
    case next = 1, close
}


protocol ZPContactListViewProtocol: class {
    var presenter: ZPContactListPresenterProtocol? { get set }
    var config: ZPCConfig { get }
    
    // PRESENTER -> VIEW
    func showZPContacts(_ dataSection: [String], dictionary: [String : [ZPContactSwift]], dataFavorite:NSArray, totalFriend: Int)
    func resultDataSearch(_ listSearchView: NSMutableArray,_ listFriendRedPacket: NSMutableArray, _ listFriendFavorite: NSArray, _ phoneModel: ZPContactSwift?)
    
    func showLoading()
    
    func hideLoading()
        
    func getMode() -> ZPCListViewMode
    
    func addContactsRedPacketWhenReady(_ contacts: [ZPContactSwift])    
    
    func selectedUserToRedPacket(_ zpContact: ZPContactSwift)
    
    func alertSearchUserPhoneNotNetwork(_ error:NSError)
    
    func showErrorSelectingZaloPayId(withText content: String)
}

protocol ZPContactListViewDelegateProtocol: class {
    func addedRedPacketContact(_ contact: ZPContactSwift)
    func removedRedPacketContact(_ contact: ZPContactSwift)
}

protocol ZPContactListRouterProtocol: class {
    // PRESENTER -> ROUTER
    func presentZPUpdateContactScreen(from view: ZPContactListViewProtocol)
    func presentZPAllowAccessContactScreen(from view: ZPContactListViewProtocol)
    
    // Selecting Zalo Pay Id
    func openTransferReceiveController(transferData user: UserTransferData)
}

protocol ZPContactListPresenterProtocol: class {
    var view: ZPContactListViewProtocol? { get set }
    var interactor: ZPContactListInteractorInputProtocol? { get set }
    var router: ZPContactListRouterProtocol? { get set }
    
    // VIEW -> PRESENTER
    func showZPUpdateConatct()
    func showAllowAccessContactScreen()
    func getCurrentContactMode() -> ContactMode
    func getEnableFavoriteMode() -> Bool
    func searchWithText(_ text:String)
    func didSelectZaloUser(_ zaloUser:ZPContactSwift, reqUserInfo: Bool)
    func viewWillPopout()
    func updateListContact(completeHandle: (() -> Void)?)
    func getListContact(sync: Bool)
    func setViewMode()
    func processGroupData(_ zaloPayMode: Bool)
    func btnNextRedPacketTouch(_ arrContacts:[ZPContactSwift],_ endSelectedRedPacket:ZPCEndSelectedRedPacket)
    func prepareUsersRedPacket(_ users: [Any]?)
    
    func getConfigEnableInviteZP() -> Bool
    func searchUserPhoneNotNetwork(_ error:NSError)
    func getDateUpdate() -> String?
    func getTextGuide() -> String?
    func requestUserInfoWithZaloPayId(accountName: String)

}

protocol ZPContactListInteractorOutputProtocol: class {
    // INTERACTOR -> PRESENTER
    func didRetrieveZPContacts(_ dataSection:[String], dictionary:[String: [ZPContactSwift]], dataFavorite:NSArray, totalFriend: Int)

    func didPrepareUsersRedPacket(_ contacts: [ZPContactSwift])
    func resultSearch(_ listSearchView: NSMutableArray,_ listFriendRedPacket: NSMutableArray, _ listFriendFavorite: NSArray, _ phoneModel: ZPContactSwift?)
    func didRetrieveUserInfo(_ zaloUser: ZPContactSwift)
    func searchUserPhoneNotNetwork(_ error:NSError)
    
    func loadSucceedZaloPayId(user: UserTransferData)
    func loadFailedZaloPayId(message: String)
    func showErrorZaloPayId(withText content: String)
}

protocol ZPContactListInteractorInputProtocol: class {
    var redPacketZPContactIds: [String] { get set }
    var redPacketUnsaveZPContacts:[ZPContactSwift]{get set}
    var presenter: ZPContactListInteractorOutputProtocol? { get set }
    
    // PRESENTER -> INTERACTOR
    func prepareUsersRedPacket(_ users: [Any]?)
    func retrieveZPContactList(zaloPayMode: Bool, syncContact: Bool)
    func retrieveCachedZPContactList(zaloPayMode: Bool)
    func retrieveUserInfoFromServer(by phone: String)
    func getCurrentContactMode() -> ContactMode
    func getEnableFavoriteMode() -> Bool
    func searchWithText(_ text:String, zaloPayMode: Bool)
    func setContactMode(_ contactMode:ContactMode)
    func setViewModelZPC(source : ZPCSource, viewMode : String)
    func loadViewModeZPC(source: ZPCSource) -> String
    func isNumberPadKeyboad(_ source: ZPCSource) -> Bool
    func processGroupData(_ zaloPayMode: Bool)
    func filterAllRedPacketContacts(from dict: inout [String:[ZPContactSwift]])
    func combineDataToArray(_ array: inout [ZPContactSwift], data: [ZPContactSwift])
    func combineDataToDictionary(_ dict: inout [String:[ZPContactSwift]], data: [ZPContactSwift]) -> [String]
    func arrayRedPacketUnsaveContactsRemove(contact:ZPContactSwift)

    func getAllRedPacketContacts() -> [ZPContactSwift]
    func isZPCLaunchedTheFirst() -> Bool
    func retrieveDateUpdate() -> String?
    func retrieveGuideContent() -> String?
    func fetchDataSelectingZaloPayId(accountName: String)

}
