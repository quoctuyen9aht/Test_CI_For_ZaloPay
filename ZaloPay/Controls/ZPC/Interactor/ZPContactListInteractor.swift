//
//  ZPContactListInteractor.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 7/19/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import CocoaLumberjackSwift
import PINCache
import Synchronized
import ZaloPayProfile
import ZaloPayConfig

class ZPContactListInteractor: ZPContactListInteractorInputProtocol {
    var allFriend: NSArray?
    var contactMode = ContactMode_ZPC_All
    var redPacketZPContactIds = [String]()
    var redPacketUnsaveZPContacts = [ZPContactSwift]()
    var zaloFriendTable: ZPZaloFriendTable!
    var cacheDataTable: ZPCacheDataTable!
    var contactList: ZPContactList!

    weak var presenter: ZPContactListInteractorOutputProtocol?

    lazy var memoryCache: PINMemoryCache = {
        let cache = PINMemoryCache()
        cache.removeAllObjectsOnMemoryWarning = true
        cache.ageLimit = 300
        return cache
    }()


    init() {
        contactList = ZPContactList()
        zaloFriendTable = ZPZaloFriendTable(db: PerUserDataBase.sharedInstance())
        cacheDataTable = ZPCacheDataTable(db: PerUserDataBase.sharedInstance())
    }

    func getAllRedPacketContacts() -> [ZPContactSwift] {
        if var contacts = allFriend as? [ZPContactSwift] {
            contacts.append(contentsOf: redPacketUnsaveZPContacts)
            let arrRedPackets:[ZPContactSwift] = contacts.filter({ (contact) -> Bool in
                return redPacketZPContactIds.contains(contact.zpContactID)
            })
            return arrRedPackets
        }

        return [ZPContactSwift]()
    }
    func isZPCLaunchedTheFirst() -> Bool {
        let defaults = UserDefaults.standard
        if defaults.string(forKey: "isZPCLaunchedOnce") != nil {
            return false
        }else{
            defaults.set(true, forKey: "isZPCLaunchedOnce")
            defaults.synchronize()
            return true
        }

    }

    func arrayRedPacketUnsaveContactsRemove(contact:ZPContactSwift) {
        let index = self.redPacketUnsaveZPContacts.index { (unsaveContact) -> Bool in
            return unsaveContact.zpContactID == contact.zpContactID
        } ?? -1
        if index > -1 {
            self.redPacketUnsaveZPContacts.remove(at: index)
        }
    }

    func prepareUsersRedPacket(_ users: [Any]?) {
        guard let arrUsersRedPacket = users as? Array<[String: Any]> else {
            return
        }

        DispatchQueue.executeInMainThread {
            [weak self] in
            var contacts = [ZPContactSwift]()
            var unSaveContacts = [ZPContactSwift]()

            for user in arrUsersRedPacket {
                let redPacketContact = ZPContactSwift.createContact(fromReactDictionary: user)
                redPacketContact.isRedPacket = true
                contacts.append(redPacketContact)

                let zpContactId = redPacketContact.zpContactID
                if let isExist = self?.redPacketZPContactIds.contains(zpContactId) {
                    if isExist.not {
                        self?.redPacketZPContactIds.append(zpContactId)
                    }
                }
            }

            if let arrAllFriends = self?.allFriend as? [ZPContactSwift] {

                for contactRedPacket in contacts {
                    let isExist = arrAllFriends.contains(where: { (contact) -> Bool in
                        return contactRedPacket.zpContactID == contact.zpContactID
                    })
                    if isExist.not {
                        unSaveContacts.append(contactRedPacket)
                    }
                }
                self?.redPacketUnsaveZPContacts.append(contentsOf: unSaveContacts)

            }

            self?.presenter?.didPrepareUsersRedPacket(contacts)
        }
    }

    func setContactMode(_ contactMode:ContactMode) {
        self.contactMode = contactMode
    }

    func retrieveZPContactList(zaloPayMode: Bool, syncContact: Bool) {
        DispatchQueue.global(qos: .background).async {
            self.contactList.getDataContactList(contactMode: self.contactMode,
                                                            syncData: syncContact) { [weak self] (result)  in
                self?.allFriend = result.sortedArray(using: [NSSortDescriptor(key: "asciiDisplayName", ascending: true)]) as NSArray?
                self?.processGroupData(zaloPayMode)
            }
        }
    }

    func retrieveCachedZPContactList(zaloPayMode: Bool) {
        if self.allFriend == nil {
            self.retrieveZPContactList(zaloPayMode: zaloPayMode, syncContact: false)
        } else {
            self.processGroupData(zaloPayMode)
        }
    }

    func filterAllRedPacketContacts(from dict: inout [String:[ZPContactSwift]]) {
        for key in dict.keys {
            dict[key] = dict[key]!.filter {
                let zpContactID = $0.zpContactID
                return redPacketZPContactIds.contains(zpContactID).not
            }
        }
    }

    func handleUserInfoData(dic: NSDictionary, accountName: String) ->UserTransferData {

        let nDict = dic as? [String: Any] ?? [:]

        let phone = nDict.value(forKey: "phonenumber", defaultValue: Int64(0))
        let displayName = nDict.value(forKey: "displayname", defaultValue: "")
        let zaloPayId = nDict.value(forKey: "userid", defaultValue: "")
        let avatar = nDict.value(forKey: "avatar", defaultValue: "")

        let user = UserTransferData()
        user.displayName = displayName;
        user.paymentUserId = zaloPayId
        user.phone = String(phone)
        user.avatar = avatar
        user.accountName = accountName
        user.mode = .toZaloPayID
        user.source = .fromTransferActivity
        return user
    }

    func fetchDataSelectingZaloPayId(accountName: String) {
        NetworkManager.sharedInstance().getUserInfo(withAcountName: accountName).deliverOnMainThread().subscribeNext({ [weak self](result) in
            if let dic = result as? NSDictionary {
                let zaloPayId = dic["userid"] as? String
                if(zaloPayId == NetworkManager.sharedInstance().paymentUserId) {
                    self?.presenter?.loadFailedZaloPayId(message: R.string_TransferToAccount_CurrentUserAccountMessage())
                    return;
                }
                if let user = self?.handleUserInfoData(dic: dic, accountName: accountName) {
                    self?.presenter?.loadSucceedZaloPayId(user: user)
                }
            }

            }, error:({[weak self] (error) in
                if let err = error as NSError? {
                    self?.presenter?.loadFailedZaloPayId(message:err.apiErrorMessage())
                }
            }))
    }

    func combineDataToArray(_ array: inout [ZPContactSwift], data: [ZPContactSwift]) {
        for contact in data {
            if contact.isRedPacket.not {
                array.append(contact)
            }
        }

        array.sort { $0.displayName.compare($1.displayName) == .orderedAscending }
    }

    func combineDataToDictionary(_ dict: inout [String:[ZPContactSwift]], data: [ZPContactSwift]) -> [String] {
        for contact in data {
            let key = contact.firstCharacterInDisplayName ?? ""
            var contacts = dict[key] ?? [ZPContactSwift]()
            contacts.append(contact)
            dict[key] = contacts
        }

        let allKeys = dict.keys.sorted(by: <)
        return allKeys
    }

    // step1
    private func friendsInContact() -> Observable<[String: [ZPContactSwift]]> {
        guard  let friends: [ZPContactSwift] = allFriend as? [ZPContactSwift], friends.count > 0 else {
            return Observable.just([:])
        }

        return synchronized(friends as AnyObject) { () -> Observable<[String: [ZPContactSwift]]> in
            var dict = [String: [ZPContactSwift]]()
            for user in friends {
                let key = user.firstCharacterInDisplayName ?? ""
                var temp = dict[key] ?? []
                temp.append(user)
                dict[key] = temp
            }
            return Observable.just(dict)
        }
    }

    // step2
    private func favoriteContactList() -> Observable<[ZPContactSwift]> {
        return Observable.create({ (s) -> Disposable in
            let favorites = self.contactList.getListFavorite()
            s.onNext(favorites as? [ZPContactSwift] ?? [])
            s.onCompleted()
            return Disposables.create()
        })
    }

    private let disposeBag = DisposeBag()
    private typealias ResultFindContact = (dictFriends: [String: [ZPContactSwift]], listFavor: [ZPContactSwift])
    private struct DisplayedData {
        let keys: [String]
        let dictFriends: [String: [ZPContactSwift]]
        let favContacts: [ZPContactSwift]
        let totalFriends: Int
    }

    func processGroupData(_ zaloPayMode: Bool) {
        // step1 + step 2 -> present ui
        let efriendsInContact = self.friendsInContact()
        let efavoriteContactList = self.favoriteContactList()
        efriendsInContact
            .flatMap { (dictFriends) -> Observable<ResultFindContact> in
                return efavoriteContactList.map({ return ResultFindContact(dictFriends, $0) })
            }.map({ (result) -> DisplayedData in
                let favContacts = result.listFavor
                var dictFriends = result.dictFriends
                var totalFriends = favContacts.count

                let favContactId = favContacts.compactMap({ $0.zpContactID }).filter({ $0.isEmpty.not })
                let tempDictFriends = dictFriends
                for (key, contacts) in tempDictFriends {
                    let temp = contacts.filter({
                        (zaloPayMode ? $0.zaloPayId.isEmpty.not : true) &&
                        favContactId.contains($0.zpContactID ?? "").not
                    })
                    dictFriends[key] = temp.isEmpty ? nil : temp
                }
                let allKeys = dictFriends.keys.sorted(by: <)
                totalFriends += dictFriends.map({ $0.value.count }).reduce(0, { $0 + $1 })
                return DisplayedData(keys: allKeys, dictFriends: dictFriends, favContacts: favContacts, totalFriends: totalFriends)
            }).observeOn(MainScheduler.instance).subscribe(onNext: { [weak self](r) in
                self?.presenter?.didRetrieveZPContacts(r.keys,
                                                       dictionary: r.dictFriends,
                                                       dataFavorite: r.favContacts as NSArray,
                                                       totalFriend: r.totalFriends)
            }).disposed(by: disposeBag)
    }

    func searchWithText(_ text: String, zaloPayMode: Bool) {

        let textSearchTemp = normalizePhoneNumber(text)
        let allFriends = (self.allFriend as? [ZPContactSwift]) ?? []
        let allFavorites = self.contactList.getListFavorite()
        let allRedPackets = self.redPacketUnsaveZPContacts;

        let searchResult = ZPCSearchOffline.searchMultiWordsIn(array: allFriends, textSearch: textSearchTemp)
        let favoriteResult = ZPCSearchOffline.searchMultiWordsIn(array: allFavorites, textSearch: textSearchTemp)
        let redpacketResult = ZPCSearchOffline.searchMultiWordsIn(array: allRedPackets, textSearch: textSearchTemp)

        if self.contactList.isPhone(text) == false || searchResult.count > 0 {
            responseResult(searchResult, nil, favContacts: favoriteResult, redPacket: redpacketResult, zaloPayMode: zaloPayMode)
            return
        }
        self.retrieveUserInfoFromServer(by: text) { [weak self] (phoneModel, fromCached, error) in
            self?.responseResult(searchResult, phoneModel, favContacts: favoriteResult,redPacket: redpacketResult, zaloPayMode: zaloPayMode)
        }
    }

    func searchIn(array: [ZPContactSwift], textSearch: String) -> [ZPContactSwift] {
        if textSearch.isEmpty {
            return array
        }
        let filter = "asciiDisplayName CONTAINS[cd] %@ || phoneNumber CONTAINS[cd] %@"
        let txtSearch = textSearch.ascci().uppercased()
        let predicate = NSPredicate(format: filter, txtSearch ,txtSearch)
        let result = (array as NSArray).filtered(using: predicate) as! [ZPContactSwift]
        return result
    }

    func responseResult(_ result: [ZPContactSwift],
                        _ phoneModel: ZPContactSwift?,
                         favContacts: [ZPContactSwift],
                         redPacket: [ZPContactSwift],
                         zaloPayMode: Bool) {

        var res = result.filter {
            let contactId = $0.zpContactID
            let inFavorite = favContacts.contains(where: { (contact) -> Bool in return contact.zpContactID == contactId})
            let inRedPacket = self.redPacketZPContactIds.contains(where: { (oneId) -> Bool in oneId == contactId})
            return inFavorite.not && inRedPacket.not
        }
        for contact in favContacts {
            let zpContactID = contact.zpContactID
            if self.redPacketZPContactIds.contains(zpContactID) {
                contact.isRedPacket = true
            }
        }

        let arrayRedPacket = NSMutableArray()
        for contact in result {
            let zpContactID = contact.zpContactID
            if self.redPacketZPContactIds.contains(zpContactID) {
                contact.isRedPacket = true
                arrayRedPacket.add(contact)
            }
        }
        if zaloPayMode {
            res = res.filter {return $0.zaloPayId.isEmpty.not}
        }
        arrayRedPacket.addObjects(from: redPacket)
        self.presenter?.resultSearch(NSMutableArray(array: res), arrayRedPacket, NSMutableArray(array: favContacts), phoneModel)
    }

    func retrieveUserInfoFromServer(by phone: String) {
        guard let friends = allFriend, friends.contains(where: {
            guard let contact = $0 as? ZPContactSwift else {
                return false
            }
            return contact.phoneNumber == phone
        }) else {
            self.presenter?.didRetrieveUserInfo(ZPContactSwift.fromPhone(phone: phone, displayName: R.string_ZPC_Unsave_Phone_Number()))
            return
        }

        self.retrieveUserInfoFromServer(by: phone) { [weak self] (phoneModel, fromCached, error) in
            let isNew = error.not && fromCached.not
            if isNew {
                guard var contacts = self?.allFriend as? [ZPContactSwift],
                    let idx = contacts.index(where: { return $0.phoneNumber == phone }) else {
                    return
                }

                self?.contactList.updatePhoneContact(contact: phoneModel, phone: phone)
                if let contact = self?.contactList.getContactInfo(by: phone) {
                    contacts[idx] = contact
                }
                self?.allFriend = NSArray(array: contacts)
            }
            self?.presenter?.didRetrieveUserInfo(phoneModel)
        }
    }

    private func retrieveUserInfoFromServer(by phone: String, completed: @escaping (ZPContactSwift, Bool, Bool) -> Void) {
        if let phoneModel = self.memoryCache.object(forKey: phone) as? ZPContactSwift {
            completed(phoneModel, true, false)
            return
        }

        NetworkManager.sharedInstance().getUserInfo(byPhone: phone).deliverOnMainThread().subscribeNext({ [weak self] (result) in

            guard self != nil,
                let contact = result as? [String : Any],
                let phoneModel = ZPContactSwift.user(fromServerDictionary: contact) else {
                    let phoneModel = ZPContactSwift.fromPhone(phone: phone, displayName: R.string_ZPC_Unsave_Phone_Number())
                    self?.memoryCache.setObject(phoneModel, forKey: phone)
                    completed(phoneModel, false, true)
                    return
            }

            self?.memoryCache.setObject(phoneModel, forKey: phone)
            completed(phoneModel, false, false)
        }, error:({ [weak self] (error) in
            let phoneModel = ZPContactSwift.fromPhone(phone: phone, displayName: R.string_ZPC_Unsave_Phone_Number())
            self?.memoryCache.setObject(phoneModel, forKey: phone)
            completed(phoneModel, false, true)
            if let err = error as NSError?, err.isApiError().not {
                self?.presenter?.searchUserPhoneNotNetwork(err)
            }
        }))
    }

    func normalizePhoneNumber(_ text:String) -> String {
        let characterset = CharacterSet(charactersIn: "0123456789+-()/r/n ")
        if text.rangeOfCharacter(from: characterset.inverted) == nil {
            return  text.components(separatedBy:CharacterSet.decimalDigits.inverted).joined()
        }
        return text
    }

    func getCurrentContactMode() -> ContactMode {
        return contactMode
    }

    func getEnableFavoriteMode() -> Bool {
//        let enable = ApplicationState.getBoolConfig(fromDic: "zpc", andKey: "friend_favorite", andDefault: false)
//        return enable

        let enable = ZPApplicationConfig.getZPCAppConfig()?.getFriendFavorite() ?? 0
        return enable.toBool()
    }


    func setViewModelZPC(source : ZPCSource, viewMode : String) {
        zaloFriendTable.setStateKeyboardWithValue(viewMode, forKey: source.rawValue)
    }

    func loadViewModeZPC(source: ZPCSource) -> String {
        return zaloFriendTable.getStateKeyboard(forKey: source.rawValue) ?? ""
    }

    func isNumberPadKeyboad(_ source: ZPCSource) -> Bool {

        let viewMode : String = self.loadViewModeZPC(source: source)
        var flag = (viewMode == ViewModeKeyboard_Phone)

        if viewMode == "" {
            switch source {
            case .fromTransferMoney_PhoneNumber:
                flag = true
            case .fromTransferMoney_Contact:
                fallthrough
            default:
                flag = false
            }
        }
        return flag
    }

    func retrieveDateUpdate() -> String? {
        var text = R.string_ZPC_update_contact_miss()
        if self.contactList.isPermissionGetAddressBook() {
            let lastTime = cacheDataTable.cacheDataValue(forKey: keyStoreDateUpdateContactList)
            if lastTime != nil {
                text = lastTime
            }
        }
        return text
    }
    func retrieveGuideContent() -> String? {
        let appModule = ReactNativeAppManager.sharedInstance().getApp(lixiAppId)
        let appNameString = appModule.appname?.lowercased() ?? ""
        return String(format: R.string_RedPacket_Guide_Text(), appNameString, appNameString)
    }
}
