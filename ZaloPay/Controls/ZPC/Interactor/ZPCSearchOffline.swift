//
//  ZPCSearchOffline.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 5/8/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayProfile

class ZPCSearchOffline: NSObject {
    
    class func searchMultiWordsIn(array: [ZPContactSwift], textSearch: String) -> [ZPContactSwift] {
        if textSearch.isEmpty {
            return array
        }
        let splitText = textSearch.ascci().uppercased().split(separator: " ")
        let filteredContacts = array.filter {
            // Count total number of valid word in textSearch
            var checkCount = 0
            for text in splitText {
                // Bool flag turned true if this text (word) is found
                var thisTextIsValid: Bool = false
                let asciiDisplayName = $0.asciiDisplayName ?? ""
                for componentName in asciiDisplayName.split(separator: " ") {
                    if componentName.contains(text) && componentName.first == text.first {
                        thisTextIsValid = true
                        break
                    }
                }
                // One more valid search word
                if thisTextIsValid == true {
                    checkCount = checkCount + 1
                }
            }
            // If every words in textSearch is valid (display name)
            // OR
            // phone number containing textSearch 
            if checkCount == splitText.count || $0.phoneNumber.contains(textSearch) {
                return true
            }
            return false
        }
        return filteredContacts
    }
    
    class func searchPredicateIn(array: [ZPContactSwift], textSearch: String) -> [ZPContactSwift] {
        if textSearch.isEmpty {
            return array
        }
        let filter = "asciiDisplayName CONTAINS[cd] %@ || phoneNumber CONTAINS[cd] %@"
        let txtSearch = textSearch.ascci().uppercased()
        let predicate = NSPredicate(format: filter, txtSearch ,txtSearch)
        let filteredContacts = (array as NSArray).filtered(using: predicate) as! [ZPContactSwift]
        return filteredContacts
    }
}
