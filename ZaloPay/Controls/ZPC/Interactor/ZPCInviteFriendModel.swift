//
//  ZPCInviteFriendModel.swift
//  ZaloPay
//
//  Created by Bon Bon on 1/6/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayCommonSwift
import ZaloPayProfile
import ZaloPayAnalyticsSwift
import RxSwift
import ZaloPayProfile
import ZaloPayConfig

class ZPCInviteFriendModel {
    let bag = DisposeBag()
    
    private func inviteConfig() -> ZPInviteZPConfigProtocol? {
        return ZPApplicationConfig.getInviteZPConfig()
    }
    
    func getConfigEnableInviteZP() -> Bool {
        return inviteConfig()?.getEnable().toBool() ?? false
    }
    
    func shareMessageViaZalo(withUser user:ZPContactSwift) {
        let urlShare = inviteConfig()?.getDeeplinkShare() ?? ""
        if urlShare.count == 0 {
            return
        }
        let userid = ZPProfileManager.shareInstance.userLoginData?.paymentUserId ?? ""
        let timestamp = String(Int64(NSDate().timeIntervalSince1970 * 1000))
        let linkShare = String(format: kBaseUrl + urlShare, userid,user.phoneNumber,timestamp)
        let fromVC = UINavigationController.currentActiveNavigationController()?.topViewController

        ZaloSDKApiWrapper.sharedInstance.sendMessageZalo(linkShare,andParentVC:fromVC).subscribe(onNext: { (result) in
            }, onError: { (error) in
        }).disposed(by: bag)
    }
    
    func alertConfirm(_ user:ZPContactSwift, source: ZPCSource) {
        let messageAttributedString = self.getMessageAttributedStringFrom(user, source: source)
        if getConfigEnableInviteZP() {
            ZPDialogView.showDialogBoldFinal(with: DialogTypeNotification, title: R.string_TransferMoney_WarningTitle(), attributeMessage: messageAttributedString, buttonTitles: [R.string_ButtonLabel_Close(),R.string_InvitationZP_send()]) { [weak self](index) in
                if index == 1 {
                    self?.shareMessageViaZalo(withUser: user)
                    ZPTrackingHelper.shared().trackEvent(.contact_nonzpfriend_touch_invite)
                } else {
                    ZPTrackingHelper.shared().trackEvent(.contact_nonzpfriend_touch_close)
                }
            }
        }else {
            ZPDialogView.showDialog(with: DialogTypeNotification, title: R.string_TransferMoney_WarningTitle(), attributeMessage: messageAttributedString, buttonTitles: [R.string_ButtonLabel_Close()], handler: nil)
        }
    }
    
    func getMessageAttributedStringFrom(_ user: ZPContactSwift, source: ZPCSource) -> NSMutableAttributedString {
        let displayName = getUserDisplayName(user)
        let strWarning = source == ZPCSource.fromRedPacket ? R.string_Send_RedPacket_Warning() : R.string_TransferMoney_Warning()
        let msg = getConfigEnableInviteZP() ? String(format: strWarning!, displayName, displayName) : String(format: R.string_TransferMoney_Warning_Not_Invite(), displayName, displayName)
        let attributedString:NSMutableAttributedString = ZPDialogView.attributeString(withMessage: msg)
        
        let zpContactID = user.zpContactID ?? ""
        if zpContactID.count == 0 || displayName.count == 0 {
            return attributedString
        }
        
        let fullStringWithID = String(format: R.string_TransferMoney_Warning(), zpContactID, zpContactID)
        let fullNameArr = fullStringWithID.components(separatedBy:zpContactID)
        if fullNameArr.count == 0 {
            return attributedString
        }
        let range1 = NSMakeRange(0, displayName.count)
        let range2 = NSMakeRange(displayName.count + fullNameArr[1].count, displayName.count)
        let font = UIFont.sfuiTextSemibold(withSize: 15)
        
        if range1.length > 0 && range2.length > 0 && font != nil {
            let subAttrs = [NSAttributedStringKey.font : font!]
            attributedString.setAttributes(subAttrs, range: range1)
            attributedString.setAttributes(subAttrs, range: range2)
        }
        
        let noteString = R.string_TransferMoney_Warning_Note()
        let noteAttrString = NSMutableAttributedString(string: noteString!)
        let fontNote = UIFont.sfuiTextRegular(withSize: 13)
        noteAttrString.setAttributes([NSAttributedStringKey.font : fontNote!, NSAttributedStringKey.foregroundColor : UIColor.subText()], range: NSMakeRange(0, noteString!.count))
        attributedString.append(noteAttrString)
        return attributedString
    }
    
    func getUserDisplayName(_ user: ZPContactSwift) -> String {
        let displayName = user.displayName ?? ""
        if displayName.count > 0 && user.isSavePhoneNumber {
            return displayName
        }
        return user.phoneNumber ?? ""
    }
}
