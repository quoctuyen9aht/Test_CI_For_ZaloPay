//
//  ZPContactList.swift
//  ZaloPay
//
//  Created by Duy Dao Pham Hoang on 7/13/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import AddressBook
import CocoaLumberjackSwift 
import ZaloPayProfile
import ZaloPayConfig

private let countZalo:Int           = 50
private let zalopayId               = "zalopayId"
private let zaloId                  = "zaloId"
private let phoneNumber             = "phoneNumber"
private let favorite                = "favorite"
private let dateModified            = "dateModified"
let keyStoreDateUpdateContactList   = "keyStoreDateUpdateContactList"

class ZPContactList: NSObject {
    var zaloFriendTable: ZPZaloFriendTable!
    var cacheDataTable: ZPCacheDataTable!
    
    override init() {
        super.init()
        zaloFriendTable = ZPZaloFriendTable(db: PerUserDataBase.sharedInstance())
        cacheDataTable = ZPCacheDataTable(db: PerUserDataBase.sharedInstance())
    }
    
    func isPermissionGetAddressBook() -> Bool {
        return ABAddressBookGetAuthorizationStatus() == .authorized
    }
    
    func getDataContactList(contactMode:ContactMode = ContactMode_ZPC_All,
                            syncData: Bool,
                            callBack:@escaping ((NSArray) -> Void)) {

        let status = ABAddressBookGetAuthorizationStatus()
        if status != .notDetermined {
            getZaloFiendAndAdressBook(contactMode, syncData: syncData, callBack: callBack)
            return;
        }
        
        askPermission { (accept: Bool) in
            self.getZaloFiendAndAdressBook(contactMode, syncData: true, callBack: callBack)
        }
    }
    
    func askPermission(_ callback: @escaping((Bool) -> Void)) {
        var error: Unmanaged<CFError>?
        let addressBook = ABAddressBookCreateWithOptions(nil, &error)
        if (error != nil) {
            openSetting()
            return
        }
        ABAddressBookRequestAccessWithCompletion(addressBook as ABAddressBook) {
            (granted: Bool, error: CFError!) in
            callback(granted)
        }
    }
    
    func openSetting() {
        if let url =  URL(string: UIApplicationOpenSettingsURLString)  {
            UIApplication.shared.openURL(url)
        }
    }
    
    func getZaloFiendAndAdressBook(_ contactMode: ContactMode,
                                        syncData: Bool,
                                    callBack:@escaping ((NSArray) -> Void)) {
        
        if (!syncData) {

            let contacts = ZPProfileManager.shareInstance.getZaloPayContactList(fromDB: contactMode.rawValue) as NSArray
            if (contacts.count > 0) {
                callBack(contacts);
                return
            }            
        }
        
        ZPProfileManager.shareInstance.loadZaloUserFriendList(forceSyncContact: true).subscribeCompleted( {
            let contacts = ZPProfileManager.shareInstance.getZaloPayContactList(fromDB: contactMode.rawValue) as NSArray
            callBack(contacts);
        })
        let s = Date().string(using: "dd/MM/yyyy HH:mm")
        cacheDataTable.cacheDataUpdateValue(s, forKey:keyStoreDateUpdateContactList)
    }
            
    func getListFavorite(_ contactMode: ContactMode = ContactMode_ZPC_All) -> [ZPContactSwift] {
        return ZPProfileManager.shareInstance.getZaloPayContactListFavorite(fromDB: contactMode.rawValue)
    }
    
    func setFavorite(_ user:ZPContactSwift, isFavorite:Bool) {
        let params = NSMutableDictionary()
        params.setObjectCheckNil(user.zaloPayId, forKey: zalopayId as NSCopying)
        params.setObjectCheckNil(user.zaloId, forKey: zaloId as NSCopying)
        params.setObjectCheckNil(user.phoneNumber, forKey: phoneNumber as NSCopying)
        
        if isFavorite == true {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyyMMddHHmmss"
            let stringDate = dateFormatter.string(from: Date())
            params.setObjectCheckNil("1", forKey: favorite as NSCopying)
            params.setObjectCheckNil(stringDate, forKey: dateModified as NSCopying)
        }
        else {
            params.setObjectCheckNil(nil, forKey: favorite as NSCopying)
            params.setObjectCheckNil(nil, forKey: dateModified as NSCopying)
        }
        zaloFriendTable.setFavoriteOrIgnore(params as! [AnyHashable : Any])
    }
    
    func saveUnsavePhoneContact(_ contact: ZPContactSwift?) {
        if contact != nil {
            zaloFriendTable.saveUnSavePhoneContact(contact)
        }
    }
    
    func updatePhoneContact(contact: ZPContactSwift, phone: String) {
        zaloFriendTable.updateFriendProfile(toZPC: contact, phone: phone)
    }
    
    func getContactInfo(by phone: String) -> ZPContactSwift? {
        return ZPProfileManager.shareInstance.getContactInfo(byPhone: phone)
    }
    
    func getUnSavePhoneContactFrom(_ phone:String) -> ZPContactSwift? {
        return ZPProfileManager.shareInstance.getUnsavePhoneContactFromDB(withPhoneStringInput: phone)
    }
    
    func phonePatterns() -> [String] {
        let dic_phone_format = ZPApplicationConfig.getGeneralConfig()?.getPhoneFormat()
        let phontPatterns = dic_phone_format?.getPatterns()
        return phontPatterns ?? []
    }
    
    func isPhone(_ text: String) -> Bool {
        let patterns = phonePatterns()
        for regex in patterns {
            let p = NSPredicate(format: "SELF MATCHES %@", regex)
            if p.evaluate(with: text) {
                return true
            }
            continue
        }
        return false
    }
}
