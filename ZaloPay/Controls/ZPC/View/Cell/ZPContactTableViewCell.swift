//
//  ZPContactTableViewCell.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 7/19/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit
import ZaloPayProfile

struct FavoriteButtonParameters {
    static let leftLblAddFavorite = 7
    static let topLblAddFavorite = 44
    static let heightLblAddFavorite = 16
    
    static let leftBtnFavorite = 28
    static let topBtnFavorite = 16
    static let widthBtnFavorite = 20
}

protocol ZPContactTableViewCellDelegate: class {
    func handleFavoriteFriendList(_ zpContact: ZPContactSwift,_ zpContactTableViewCell:ZPContactTableViewCell)
    func handleSelectedRedPacket(_ zpContact: ZPContactSwift)
}

class ZPContactTableViewCell: UITableViewCell {
    
    weak var zpContactTableViewCellDelegate: ZPContactTableViewCellDelegate?
    var item: ZPContactSwift?
    
    @IBOutlet weak var avatarImageView: ZPIconFontImageView!
    @IBOutlet weak var zpImageView: ZPIconFontImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var viewEmptyAvatar: UIView!
    @IBOutlet weak var firstCharNameLabel: UILabel!
    @IBOutlet weak var iconFavoriteView: UIView!
    @IBOutlet weak var iconFavorite: ZPIconFontImageView!
    @IBOutlet weak var centerIconFavorite: ZPIconFontImageView!
    
    @IBOutlet weak var leftContraintAvatar: NSLayoutConstraint!
    @IBOutlet weak var widthContraintIconCheckRedPacket: NSLayoutConstraint!
    @IBOutlet weak var centerLabelName: NSLayoutConstraint!
    @IBOutlet weak var checkButton: UIButton!
    var isChecked: Bool = false
    var sourceOfZPC: ZPCSource?
    var mainColor: UIColor!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.titleLabel.font = UIFont.sfuiTextRegular(withSize: UILabel.mainTextSize())
        self.titleLabel.textColor = UIColor.defaultText()
        self.avatarImageView.roundRect(Float(self.avatarImageView.frame.size.width/2))
        self.zpImageView.roundRect(Float(self.zpImageView.frame.size.width/2))
        self.viewEmptyAvatar.roundRect(Float(self.viewEmptyAvatar.frame.size.width/2))
        self.zpImageView.layer.borderWidth = 1.5
        self.zpImageView.layer.borderColor = UIColor.white.cgColor
        self.selectionStyle = .none
        
        self.mainColor = UIColor.zaloBase()
    }
    
    func setState(checked: Bool) {
        isChecked = checked
        let iconName = isChecked ? "red_checkactive" : "red_checknormal"
        let iconColor = isChecked ? self.mainColor : UIColor.hex_0xc7c7cc()
        
        self.checkButton.titleLabel?.font = UIFont.iconFont(withSize: 22)
        self.checkButton.setIconFont(iconName, for: .normal)
        self.checkButton.setTitleColor(iconColor, for: .normal)
    }
    
    @IBAction func checkButtonTapped(_ sender: Any) {
        //setState(checked: !isChecked)
        zpContactTableViewCellDelegate?.handleSelectedRedPacket(self.item!)
    }
    func setUpImageViewAvatar(with item:ZPContactSwift) {
        self.avatarImageView.isHidden = true
        self.firstCharNameLabel.isHidden = false
        self.firstCharNameLabel.text = item.displayName.getFirstCharacters(numWords: 2)
        if item.avatar.isEmpty.not {
            self.avatarImageView.sd_setImage(with: URL(string: item.avatar), completed: { [weak self] (image, error, nil, url)  in
                if image != nil {
                    self?.avatarImageView.isHidden = false
                    self?.firstCharNameLabel.isHidden = true
                    self?.avatarImageView.image = image
                }
            })
        }
    }
    
    func setItem(item: ZPContactSwift, isFavorite: Bool, enableFavoriteFriendFeature: Bool) {
        self.item = item
        let hidePhone = shouldHidePhone(item)
        self.setUpImageViewAvatar(with:item)
        
        self.zpImageView.isHidden = item.zaloPayId.isEmpty
        
        let borderColor = (item.zaloPayId.isEmpty ? UIColor.clear : mainColor).cgColor
        self.avatarImageView.layer.borderColor = borderColor
        self.avatarImageView.layer.borderWidth = item.zaloPayId.isEmpty ? 0 : 1
        self.viewEmptyAvatar.layer.borderColor = borderColor
        self.viewEmptyAvatar.layer.borderWidth = item.zaloPayId.isEmpty ? 0 : 1
        
        self.titleLabel.text = item.displayName ?? ""
        
        self.phoneLabel.text = item.phoneNumber.formatPhoneNumber()
        self.phoneLabel.isHidden = hidePhone
        
        self.titleLabel.snp.updateConstraints { (make) in
            make.bottom.centerY.equalTo(self.avatarImageView).offset(hidePhone ? 0 : -8)
        }
        self.phoneLabel.snp.updateConstraints { (make) in
            make.top.centerY.equalTo(self.avatarImageView).offset(10)
        }
        
        self.iconFavorite.setIconFont("ct-list_fav-choose")
        self.iconFavorite.setIconColor(mainColor)
        self.centerIconFavorite.setIconFont("ct-list_fav-choose")
        self.centerIconFavorite.setIconColor(isFavorite ? mainColor : UIColor.white)
        
        if enableFavoriteFriendFeature {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ZPContactTableViewCell.favoriteButtonClicked))
            self.iconFavoriteView.addGestureRecognizer(tapGesture)
        } else {
            self.iconFavoriteView.isHidden = true
            self.iconFavorite.setIconFont("")
        }
        if sourceOfZPC != ZPCSource.fromRedPacket {
            self.widthContraintIconCheckRedPacket.constant = 0
            self.leftContraintAvatar.constant = 0
        } else {
            self.widthContraintIconCheckRedPacket.constant = 25
            self.leftContraintAvatar.constant = 12
            //self.centerLabelName.constant = hidePhone ? 0 : -8
            self.setState(checked: item.isRedPacket)
            
            if item.displayName == item.phoneNumber {
                self.avatarImageView.isHidden = false
                self.firstCharNameLabel.isHidden = true
                self.avatarImageView.setIconFont("profile_avatadefault")
                self.avatarImageView.setIconColor(UIColor(fromHexString: "#C9D4E5"))
            } else {
                self.setUpImageViewAvatar(with:item)
            }
        }
        
        
        
    }
    
    func shouldHidePhone(_ contact: ZPContactSwift) -> Bool {
        // là user zalo + không có trong contact -> ẩn
        
        if (contact.phoneNumber.isEmpty) {
            return true
        }
        if contact.displayName == contact.phoneNumber {
            return true
        }
//        guard let zaloId = contact.zaloId else {
//            return false
//        }
        let zaloId = contact.zaloId
        if zaloId.isEmpty {
            return false
        }
//        guard let contactDisplayName = contact.displayNameUCB else {
//            return true
//        }
        let contactDisplayName = contact.displayNameUCB
        return contactDisplayName.isEmpty
    }


    func heightContentCell() -> CFloat {
        return 72
    }
    
    func changeColorText(_ textSearch: String) {
        if textSearch.count == 0 {
            return
        }
        let text = textSearch.lowercased()
        hightlightTitle(text: text)
        hightlightPhone(text: text)
    }
    
    func hightlightTitle(text: String) {
        guard let title = titleLabel.text else  {
            return
        }
        if title.count == 0 {
            return
        }
        let lowerTitle = title.lowercased() as NSString
        let range = lowerTitle.range(of: text)
        if range.length == 0 {
            return
        }
        
        let attribute = NSMutableAttributedString(string: title)
        attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.zaloBase() , range: range)
        attribute.addAttribute(NSAttributedStringKey.font, value: UIFont.sfuiTextMedium(withSize: UILabel.mainTextSize()) , range: range)
        titleLabel.attributedText = attribute
    }
    
    func hightlightPhone(text: String) {
        guard let item = self.item else {
            return
        }
//        guard let phoneFromLabel = phoneLabel.text, let phone = item.phoneNumber else {
//            return
//        }
        let phone = item.phoneNumber
        guard let phoneFromLabel = phoneLabel.text else {
            return
        }
        if phone.count == 0 || phoneFromLabel.count == 0{
            return
        }
        let lowerPhone = phone.lowercased() as NSString
        let rangePhone = lowerPhone.range(of: text)
        if rangePhone.length == 0 {
            return
        }
        
        let lowerLabelPhone = phoneFromLabel.lowercased() as NSString
        let rangeSpace = lowerLabelPhone.range(of: " ")
        if rangeSpace.length == 0 {
            return
        }
        var finalRange : NSRange
        let indexSpace = rangeSpace.location
        if indexSpace >= rangePhone.location && indexSpace <= rangePhone.location + rangePhone.length {
            finalRange = NSRange(location: rangePhone.location, length: rangePhone.length + 1)
        } else {
            finalRange = lowerLabelPhone.range(of: text)
        }
        let attributePhone = NSMutableAttributedString(string: phoneFromLabel)
        attributePhone.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.zaloBase() , range: finalRange)
        attributePhone.addAttribute(NSAttributedStringKey.font, value: UIFont.sfuiTextMedium(withSize: UILabel.subTitleSize()) , range: finalRange)
        phoneLabel.attributedText = attributePhone
    }
    
    override func prepareForReuse() {
        self.avatarImageView.sd_cancelCurrentImageLoad()
        self.titleLabel.attributedText = nil
        self.phoneLabel.attributedText = nil
        self.avatarImageView.image = nil
        self.firstCharNameLabel.text = nil
    }
    
    @objc func favoriteButtonClicked() {
        zpContactTableViewCellDelegate?.handleFavoriteFriendList(self.item!, self)
        //self.resetCellState()
    }
}
