//
//  ZPContactCollectionViewCell.swift
//  ZaloPay
//
//  Created by tridm2 on 12/6/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayProfile


class ZPContactCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var zpImageView: ZPIconFontImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var viewEmptyAvatar: UIView!
    @IBOutlet weak var firstCharNameLabel: UILabel!
    
    var item:ZPContactSwift?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.titleLabel.font = UIFont.sfuiTextRegular(withSize: UILabel.smallMainTextSize())
        self.titleLabel.textColor = UIColor.defaultText()
        self.avatarImageView.roundRect(Float(self.avatarImageView.frame.size.width/2))
        self.zpImageView.roundRect(Float(self.zpImageView.frame.size.width/2))
        self.viewEmptyAvatar.roundRect(Float(self.viewEmptyAvatar.frame.size.width/2))
        self.zpImageView.layer.borderWidth = 1.5
        self.zpImageView.layer.borderColor = UIColor.white.cgColor
    }
    
    func setItem(item: ZPContactSwift) {
        self.item = item
        self.avatarImageView.isHidden = true
        self.firstCharNameLabel.isHidden = false
        self.firstCharNameLabel.text = item.displayName.getFirstCharacters(numWords: 2)
        if item.avatar.isEmpty.not {
            self.avatarImageView.sd_setImage(with: URL(string: item.avatar), completed: { [weak self] (image, error, nil, url)  in
                if image != nil {
                    self?.avatarImageView.isHidden = false
                    self?.firstCharNameLabel.isHidden = true
                    self?.avatarImageView.image = image
                }
            })
        }
        
        self.zpImageView.isHidden = item.zaloPayId.isEmpty
        
        self.avatarImageView.layer.borderWidth = !item.zaloPayId.isEmpty ? 1 : 0
        self.avatarImageView.layer.borderColor = (!item.zaloPayId.isEmpty ? UIColor.zp_red() : UIColor.clear).cgColor
        
        self.viewEmptyAvatar.layer.borderWidth = !item.zaloPayId.isEmpty ? 1 : 0
        self.viewEmptyAvatar.layer.borderColor = (!item.zaloPayId.isEmpty ? UIColor.zp_red() : UIColor.clear).cgColor
        
        self.titleLabel.text = item.displayName ?? ""
    }
}
