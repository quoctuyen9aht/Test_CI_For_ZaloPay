//
//  ZPContactSelectZaloPayIdCell.swift
//  ZaloPay
//
//  Created by ThanhVo on 5/8/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit

class ZPContactSelectZaloPayIdCell: UITableViewCell {

    @IBOutlet weak var topSeperatorView: UIView!
    @IBOutlet weak var iconImageView: ZPIconFontImageView!
    @IBOutlet weak var selectingLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        self.iconImageView.setIconFont("zalopay_id")
        self.iconImageView.setIconColor(UIColor.zp_yellow())
    }

    
    func setSelectingCellText(text: String, showSeperator: Bool) {
        // Show top seperator if the cell is not alone
        self.topSeperatorView.isHidden = !showSeperator
        
        var messageText = ""
        if let message = R.string_ZPC_search_with_ZaloPayId() {
            messageText = message
        }
        
        let attributeString: NSMutableAttributedString = NSMutableAttributedString.init(string: "\(messageText) \"\(text)\"")
        let zaloPayIdRange = NSRange(location: messageText.count + 1, length: text.count + 2)
        attributeString.addAttributes([NSAttributedStringKey.foregroundColor: UIColor.zaloBase()], range: zaloPayIdRange)
        attributeString.addAttributes([NSAttributedStringKey.font: UIFont.sfuiTextRegular(withSize: 16)], range: NSRange(location: 0, length: attributeString.length))
        selectingLabel.attributedText = attributeString
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
}
