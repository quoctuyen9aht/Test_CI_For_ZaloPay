//
//  ZPCSelectedRedPacketView.swift
//  ZaloPay
//
//  Created by tridm2 on 12/6/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayProfile

fileprivate let kContactCellName = "ZPContactCollectionViewCell"

struct UIRedPacketParameters {
    static let height = 100
    static let sizeCell = CGSize(width: 66, height: 77)
}

protocol ZPCRedPacketSelectedViewDelegate: class {
    func didTapRedPacketContact(_ zpContact: ZPContactSwift)
}

class ZPCRedPacketSelectedView: UIView {
    var maxForRedPacKet: Int = 10
    var collectionView: UICollectionView!
    var arrContacts: [ZPContactSwift]!
    
    weak var delegate: ZPCRedPacketSelectedViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        arrContacts = [ZPContactSwift]()
        setCollectionView()
        updateCollectionView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func removeContact(contact: ZPContactSwift) {
        if var idx = self.arrContacts.index(where: { (item) -> Bool in
            return item.zpContactID == contact.zpContactID
        }) {
            self.arrContacts.remove(at: idx)
            if self.arrContacts.count == 0 {
                self.collectionView.reloadData()
            }else {
                self.collectionView.performBatchUpdates({
                    self.collectionView.deleteItems(at: [IndexPath(row: idx , section: 0)])
                }, completion: { (_) in
                    idx = min(self.arrContacts.count - 1, idx + 1)
                    self.updateCollectionView(scrollTo: idx)
                })
            }
            
            
        }
        contact.isRedPacket = false
    }
    
    func addContact(contact: ZPContactSwift) -> Bool {
        if self.arrContacts.count == maxForRedPacKet {
            return false
        }
        self.arrContacts.append(contact)
        if self.arrContacts.count == 1 {
            self.collectionView.reloadData()
        }else {
            self.collectionView.performBatchUpdates({
                self.collectionView.insertItems(at: [IndexPath(row: self.arrContacts.count - 1, section: 0)])
            }, completion: { (_) in
                self.updateCollectionView(scrollTo: self.arrContacts.count - 1)
            })
        }
        contact.isRedPacket = true
        return true
    }
    
    private func updateCollectionView(scrollTo idx: Int = 0) {
        if self.arrContacts.count > 0 {
            let indexPath = IndexPath(item: idx, section: 0)
            self.collectionView.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition.right, animated: true)
        }
    }
    
    func contains(_ contact: ZPContactSwift) -> Bool {
        return arrContacts.contains {
            $0.zpContactID == contact.zpContactID
        }
    }
    
    func setCollectionView() {
        let layoutFavorite: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layoutFavorite.sectionInset = UIEdgeInsets(top: 0, left: 7, bottom: 0, right: 0)
        layoutFavorite.scrollDirection = .horizontal
        layoutFavorite.itemSize = UIRedPacketParameters.sizeCell
        layoutFavorite.minimumInteritemSpacing = 0
        // layoutFavorite.minimumLineSpacing = 0
        

        self.collectionView = UICollectionView.init(frame: self.bounds, collectionViewLayout: layoutFavorite)
        self.collectionView.register(UINib(nibName: kContactCellName, bundle: nil), forCellWithReuseIdentifier: kContactCellName)
        self.collectionView.showsHorizontalScrollIndicator = false
        self.collectionView.backgroundColor = UIColor.white
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.addSubview(self.collectionView)
        self.collectionView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
            make.left.equalToSuperview()
        }
    }
}

extension ZPCRedPacketSelectedView: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kContactCellName, for: indexPath) as? ZPContactCollectionViewCell {
            let contact = self.arrContacts[indexPath.row]
            cell.setItem(item: contact)
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrContacts.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let contact = self.arrContacts[indexPath.row]
        delegate?.didTapRedPacketContact(contact)
    }
}

