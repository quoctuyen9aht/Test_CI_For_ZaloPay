//
//  ZPCRedPacketHeaderView.swift
//  ZaloPay
//
//  Created by tridm2 on 1/4/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayCommonSwift
import ZaloPayProfile

protocol ZPCRedPacketHeaderViewDelegate: class {
    func didTapRedPacketContact(_ zpContact: ZPContactSwift)
}

fileprivate let kContactCellName = "ZPContactTableViewCell"

class ZPCRedPacketHeaderView: UIView {
    var maxForRedPacKet = 10
    var rowHeight: CGFloat = 72
    var searchText: String = ""
    var mainColor = UIColor.zaloBase()
    
    var tableView: UITableView!
    var arrContacts: [ZPContactSwift]!
    
    var labelSectionTitle:UILabel!
    
    weak var delegate: ZPCRedPacketHeaderViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        arrContacts = [ZPContactSwift]()
        setupTableView()
        updateTableView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func addContactToArrContacts(from contact:ZPContactSwift)  {
        let isExist = self.arrContacts.contains(where: { (contactSelected) -> Bool in
            return contactSelected.zpContactID == contact.zpContactID
        })
        if isExist.not {
            self.arrContacts.append(contact)
        }
    }
    
    func removeContact(contact: ZPContactSwift) {
        if var idx = self.arrContacts.index(where: { (item) -> Bool in
            return item.zpContactID == contact.zpContactID
        }) {
            contact.isRedPacket = false
            self.arrContacts.remove(at: idx)
            if self.arrContacts.count == 0 {
                self.tableView.reloadData()
            } else {
                self.tableView.deleteRows(at: [IndexPath(row: idx, section: 0)], with: .fade)
                idx = min(idx, self.arrContacts.count - 1)
            }
            updateTableView()
        }
    }
    
    func addContact(contact: ZPContactSwift) -> Bool {
        
        if self.arrContacts.count >= maxForRedPacKet {
            return false
        }
        
        let isExistContact = self.arrContacts.contains { (zpContact) -> Bool in
            return zpContact.zpContactID == contact.zpContactID
        }
        if isExistContact {
            return false
        }
        
        contact.isRedPacket = true
        self.addContactToArrContacts(from:contact)
        if self.arrContacts.count == 1 {
            self.tableView.reloadData()
        } else {
            self.tableView.insertRows(at: [IndexPath(row: self.arrContacts.count - 1, section: 0)], with: .fade)
        }
        
        updateTableView()
        return true
    }
    func setDataSource(_ dataSource: NSArray) {
        let favorites = dataSource.count > 0 ? NSMutableArray(array: dataSource) : NSMutableArray()
        self.arrContacts = favorites as! [ZPContactSwift]
        self.tableView.reloadData()
        self.updateTableView()
    }
    
    private func updateTableView() {
        
        self.labelSectionTitle.isHidden = arrContacts.count == 0
        var height = CGFloat(UIFavoriteParameters.heightLabelSectionTitle + UIFavoriteParameters.topLabelSectionTitle)
        if arrContacts.count > 0 {
            height += rowHeight * CGFloat(arrContacts.count)
        } else {
            height = 0
        }
        
        let frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: height)
        self.frame = frame
        
        UIView.transition(with: self, duration: 0.1, options: .transitionCrossDissolve, animations: {
            [weak self] in
            guard let wSelf = self else {
                return
            }
            wSelf.layoutIfNeeded()
            }, completion: nil
        )
    }
    
    func contains(_ contact: ZPContactSwift) -> Bool {
        return arrContacts.contains {
            $0.zpContactID == contact.zpContactID
        }
    }
    
    private func setupTableView() {
        
        
        labelSectionTitle = UILabel()
        labelSectionTitle.zpBoxTitleGayRegular()
        self.addSubview(labelSectionTitle)
        self.labelSectionTitle.snp.makeConstraints { make in
            make.top.equalTo(UIFavoriteParameters.topLabelSectionTitle)
            make.left.equalTo(UIFavoriteParameters.leftPadding)
            make.width.equalTo(UIFavoriteParameters.widthLabelSectionTitle)
            make.height.equalTo(UIFavoriteParameters.heightLabelSectionTitle)
        }
        self.labelSectionTitle.text = R.string_RedPacket_Title_View_Selected()
        
        self.tableView = UITableView()
        self.tableView.separatorColor = UIColor.clear
        self.tableView.backgroundColor = UIColor.white
        self.tableView.rowHeight = rowHeight
        self.addSubview(self.tableView)
        
        self.tableView.snp.makeConstraints { make in
            make.top.equalTo(self.labelSectionTitle.snp.bottom)
            make.right.equalTo(0)
            make.bottom.equalTo(0)
            make.left.equalTo(0)
        }
        
        let bottomLineView = ZPLineView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 1))
        bottomLineView.backgroundColor = UIColor.line()
        self.addSubview(bottomLineView)
        bottomLineView.alignToBottom()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: kContactCellName, bundle: nil), forCellReuseIdentifier: kContactCellName)
    }
    
    func prepareCell(_ tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: kContactCellName, for: indexPath) as? ZPContactTableViewCell {
            let contact = arrContacts[indexPath.row]
            cell.sourceOfZPC = .fromRedPacket
            cell.mainColor = mainColor
            cell.zpContactTableViewCellDelegate = self
            let isUnsaveContactNotZP = contact.isSavePhoneNumber.not && contact.usedZaloPay.not
            if isUnsaveContactNotZP {
                contact.displayName = contact.phoneNumber
            }
            cell.setItem(item: contact,
                         isFavorite: true,
                         enableFavoriteFriendFeature: false
            )
            cell.changeColorText(searchText)
            return cell
        }
        
        return UITableViewCell()
    }
}

extension ZPCRedPacketHeaderView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrContacts.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return prepareCell(tableView, at: indexPath)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let contact = arrContacts[indexPath.row]
        delegate?.didTapRedPacketContact(contact)
    }
}
extension ZPCRedPacketHeaderView: ZPContactTableViewCellDelegate {
    func handleFavoriteFriendList(_ zpContact: ZPContactSwift, _ zpContactTableViewCell: ZPContactTableViewCell) {
        
    }
    
    func handleSelectedRedPacket(_ zpContact: ZPContactSwift) {
        delegate?.didTapRedPacketContact(zpContact)
    }
}
