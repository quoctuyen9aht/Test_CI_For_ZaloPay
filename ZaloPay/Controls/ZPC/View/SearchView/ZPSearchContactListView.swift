//
//  ZPSearchContactListView.swift
//  ZaloPay
//
//  Created by Duy Dao Pham Hoang on 7/20/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayCommonSwift

private let boder:CGFloat = 12
private let iconWidth:CGFloat = 20

protocol ZPContactSearchViewDelegate: class {
    func didResignFirstResponder()
}

class ZPSearchContactListView: UIView {
    var topLine: ZPLineView!
    var bottomLine: ZPLineView!
    var textField: UITextFieldWithClearButton?
    var focusBorderColor = UIColor.zaloBase()
    weak var zpcSearchViewDelegate: ZPContactSearchViewDelegate?
    
    
    fileprivate lazy var isSmallSize: Bool = UIScreen.main.bounds.width == 320
    fileprivate lazy var xTextField: CGFloat = {
        return self.isSmallSize ? boder * 3 : boder * 4
    }()
    
    init(frame: CGRect, isPhoneNumberPad:Bool, searchPlaceHolder: String) {
        super.init(frame: frame)
        setupView(isPhoneNumberPad, searchPlaceHolder: searchPlaceHolder)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setForcusState(_ isForcus: Bool) {
        let lineColor = isForcus ? focusBorderColor : UIColor.line
        self.bottomLine.backgroundColor = lineColor
    }
    
    func hideShowBtnChangeKeyBoard() {
        if (textField?.text?.isEmpty)! {
            self.textField?.frame = CGRect(x: xTextField, y: boder, width: self.frame.size.width - iconWidth - boder*2, height: self.frame.size.height - boder*2)
            self.textField?.rightView?.isHidden = true
        } else {
            self.textField?.frame = CGRect(x: xTextField, y: boder, width: self.frame.size.width - xTextField, height: self.frame.size.height - boder*2)
            self.textField?.rightView?.isHidden = false
        }
    }
    
    func setupView(_ isPhoneNumberPad:Bool, searchPlaceHolder: String) {
        self.backgroundColor = .white
        self.textField = UITextFieldWithClearButton(frame: CGRect(x: xTextField, y: boder, width: self.frame.size.width - iconWidth - boder*2, height: self.frame.size.height - boder*2))
        self.textField?.text = ""
        self.textField?.font = UIFont.sfuiTextRegular(withSize: isSmallSize ? 14 : 16)
        self.textField?.attributedPlaceholder = NSAttributedString(string: searchPlaceHolder, attributes: [NSAttributedStringKey.font : UIFont.sfuiTextRegular(withSize: isSmallSize ? 12 : 14)])
        
        self.addSubview(self.textField!)
        
        let icon = ZPIconFontImageView(frame: CGRect(x: boder, y: (self.frame.size.height - iconWidth)/2, width: iconWidth, height: iconWidth))
        icon.setIconFont("general_search")
        icon.setIconColor(.lightGray)
        self.addSubview(icon)

        topLine = ZPLineView()
        self.addSubview(topLine)
        topLine.alignToTop()
        
        bottomLine = ZPLineView()
        self.addSubview(bottomLine)
        bottomLine.alignToBottom()
        
        let separatorView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 1.0/UIScreen.main.scale))
        separatorView.backgroundColor = UIColor.lightGray
        self.textField?.inputAccessoryView = separatorView
        
    }

}

extension ZPSearchContactListView: APNumberPadDelegate {
    typealias TypeInput = UIResponder & UITextInput
    func numberPad(_ numberPad: APNumberPad!, functionButtonAction functionButton: UIButton!, textInput: TypeInput!) {
        zpcSearchViewDelegate?.didResignFirstResponder()
        self.textField?.resignFirstResponder()
    }
}
class UITextFieldWithClearButton : UITextField {
    
    var clearButton: UIButton
    let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        let r = UIEdgeInsetsInsetRect(bounds, padding)
        return r
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        var r = UIEdgeInsetsInsetRect(bounds, padding)
        // Need to adjust for clear button
        r = { () -> CGRect in
            var f = r
            f.size.width -= clearButton.bounds.width / 2
            return f
        }()
        return r
    }
    override init(frame: CGRect) {
        clearButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40 ))
        super.init(frame: frame)
        clearButton.setIconFont("general_del", for: .normal)
        clearButton.setTitleColor(UIColor.defaultAppIcon(), for: .normal)
        clearButton.titleLabel?.font = UIFont.iconFont(withSize: 18)
        self.rightView = clearButton
        self.clearButtonMode = .never
        self.rightViewMode = UITextFieldViewMode.always
        self.rightView?.isHidden = true
    }
    
    required init(coder aDecoder: NSCoder) {
        clearButton = UIButton()
        super.init(coder: aDecoder)!
    }
}
