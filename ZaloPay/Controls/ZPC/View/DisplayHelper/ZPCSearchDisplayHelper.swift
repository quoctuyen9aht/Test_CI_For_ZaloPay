//
//  ZPCSearchDisplayHelper.swift
//  ZaloPay
//
//  Created by tridm2 on 12/6/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayAnalyticsSwift
import ZaloPayProfile

fileprivate let kContactCellName = "ZPContactTableViewCell"
fileprivate let kContactSelectZaloPayIdCellName = "ZPContactSelectZaloPayIdCell"

protocol ZPCSearchDisplayHelperProtocol: UITableViewDataSource, UITableViewDelegate {
    var dataSource: [ZPContactSwift] { get set }
    var enableFavorite: Bool { get set }
    var searchText: String { get set }
    var controller: ZPContactListViewController? { get set }
}

class ZPCSearchDisplayHelper: NSObject {
    var dataSource: Array = [ZPContactSwift]()
    var enableFavorite: Bool = true
    var searchText: String = ""
    weak var controller: ZPContactListViewController?
}

extension ZPCSearchDisplayHelper: ZPCSearchDisplayHelperProtocol  {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // The last cell case
        if indexPath.row == dataSource.count {
            if let cell = tableView.dequeueReusableCell(withIdentifier: kContactSelectZaloPayIdCellName, for: indexPath) as? ZPContactSelectZaloPayIdCell {
                let showSeperator = (dataSource.count != 0)
                cell.setSelectingCellText(text: searchText, showSeperator: showSeperator)
                return cell
            }
        }
        if let cell = tableView.dequeueReusableCell(withIdentifier: kContactCellName, for: indexPath) as? ZPContactTableViewCell {
            configCell(cell, indexPath)
            return cell
        }
        return UITableViewCell()
    }
    
    func configCell(_ cell: ZPContactTableViewCell, _ indexPath: IndexPath) {
        cell.zpContactTableViewCellDelegate = self.controller
        
        let friend = dataSource[indexPath.row]
        let isFavorite = self.controller!.isFavorite(friend)
        cell.sourceOfZPC = self.controller?.config.sourceZPC
        cell.mainColor = self.controller?.config.backgroundColor
        friend.isRedPacket = self.controller!.isRedPacketContain(contact: friend)
        cell.setItem(item: friend,
                     isFavorite: isFavorite,
                     enableFavoriteFriendFeature: enableFavorite)
        cell.changeColorText(searchText)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Only show last cell - select zalo pay ID cell if source = transfer money
        if self.controller?.config.sourceZPC == ZPCSource.fromTransferNone ||
            self.controller?.config.sourceZPC == ZPCSource.fromTransferMoney_Contact ||
            self.controller?.config.sourceZPC == ZPCSource.fromTransferMoney_PhoneNumber {
            return dataSource.count + 1
        }else {
            return dataSource.count
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        // The last cell: selecting ZaloPay Id
        if indexPath.row == dataSource.count {
            self.controller?.selectingZaloPayIdRowDidTouched()
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.contact_touch_zpid)
            return
        }
        
        let zaloUser = dataSource[indexPath.row]
        if self.controller?.config.sourceZPC == ZPCSource.fromRedPacket {
            self.controller?.handleSelectedRedPacket(zaloUser)
        }else {
            self.controller?.didSelectZaloUser(zaloUser)
        }
    }
}

extension ZPCSearchDisplayHelper: UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.controller?.view.endEditing(true)
    }
}
