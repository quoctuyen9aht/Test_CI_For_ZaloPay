//
//  ZPCDisplayHelper.swift
//  ZaloPay
//
//  Created by tridm2 on 12/6/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayAnalyticsSwift
import RxSwift
import ZaloPayProfile

fileprivate let kContactCellName = "ZPContactTableViewCell"

protocol ZPCDisplayHelperProtocol: UITableViewDataSource, UITableViewDelegate {
    var allKey: [String] { get set }
    var friendMap: [String:Array<ZPContactSwift>] { get set }
    var enableFavorite: Bool { get set }
    var controller: ZPContactListViewController? { get set }
    func configTrackingTouchAlphabet()
}

class ZPCDisplayHelper: NSObject {
    var allKey = [String]()
    var friendMap = [String:Array<ZPContactSwift>]()
    var enableFavorite: Bool = true
    weak var controller: ZPContactListViewController?
    let alphabetStrings = ["#", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
    public func configTrackingTouchAlphabet() {
        let selector = #selector(tableView(_:sectionForSectionIndexTitle:at:))
        let until = self.rac_willDeallocSignal()
        self.rac_signal(for: selector).take(until: until).throttle(2.0).subscribeNext {(value) in
            //self?.controller?.trackEvent(ZPAnalyticEventAction.contact_touch_alphabet)
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.contact_touch_alphabet)
        }
    }
}

extension ZPCDisplayHelper: ZPCDisplayHelperProtocol {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: kContactCellName, for: indexPath) as? ZPContactTableViewCell {
            cell.sourceOfZPC = self.controller?.config.sourceZPC
            cell.mainColor = self.controller?.config.backgroundColor
            configCell(cell, indexPath)
            return cell
        }
        
        return UITableViewCell()
    }
    
    func configCell(_ cell: ZPContactTableViewCell, _ indexPath: IndexPath) {
        cell.zpContactTableViewCellDelegate = self.controller
        
        guard let zaloUser = self.getItem(indexPath.section, indexPath.row) else {
            return
        }
        
        let isFavorite = self.controller!.isFavorite(zaloUser)
        cell.tag = indexPath.row
        zaloUser.isRedPacket = self.controller!.isRedPacketContain(contact: zaloUser)
        cell.setItem(item: zaloUser,
                     isFavorite: isFavorite,
                     enableFavoriteFriendFeature: enableFavorite
        )
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section >= allKey.count {
            return 0
        }
        let key = allKey[section]
        if let items = friendMap[key] {
            return items.count
        }
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return allKey.count
    }
    
    // Add right-side alphabet index list
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return alphabetStrings
    }
    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        if title == "#" {
            return 0
        }
        if let index = allKey.index(of: title) {
            return index
        }
        return -1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(UIZPCParameters.heightSection)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: CGFloat(UIZPCParameters.heightSection)))
        if section > 0 {
            let lineView = UIView(frame: CGRect(x: CGFloat(UIZPCParameters.leftPadding), y: 0, width: UIScreen.main.bounds.width - CGFloat(UIZPCParameters.leftPadding), height: 1))
            lineView.backgroundColor = UIColor.groupTableViewBackground
            headerView.addSubview(lineView)
        }
        guard let zaloUser = getItem(section, 0) else {
            return headerView
        }
        let lableSectionTitle = UILabel(frame: CGRect(
            x: CGFloat(UIZPCParameters.leftPadding),
            y: CGFloat(UIZPCParameters.topLableSectionTitle),
            width: CGFloat(UIZPCParameters.widthLableSectionTitle),
            height: CGFloat(UIZPCParameters.heightLableSectionTitle))
        )
        lableSectionTitle.text = zaloUser.firstCharacterInDisplayName
        lableSectionTitle.textColor = UIColor.subText()
        lableSectionTitle.font = UIFont.sfuiTextRegular(withSize: 13)
        headerView.addSubview(lableSectionTitle)
        headerView.backgroundColor = UIColor.white
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let zaloUser = getItem(indexPath.section, indexPath.row) else {
            return
        }
    
        if self.controller?.config.sourceZPC == ZPCSource.fromRedPacket {
            self.controller?.handleSelectedRedPacket(zaloUser)
        }else {
            self.controller?.didSelectZaloUser(zaloUser)
        }
            
        
        
    }
    
    private func getItem(_ section: Int,_ row: Int) -> ZPContactSwift? {
        let numSection = allKey.count
        if section >= numSection {
            return nil
        }
        let key = allKey[section]
        if let items = friendMap[key] {
            return (row < items.count) ? items[row] : nil
        }
        return nil
    }
    
}

extension ZPCDisplayHelper: UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.controller?.view.endEditing(true)
    }
}
