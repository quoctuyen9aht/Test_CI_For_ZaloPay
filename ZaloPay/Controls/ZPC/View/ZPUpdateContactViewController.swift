//
//  ZPUpdateContactViewController.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 7/26/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayAnalyticsSwift

class ZPUpdateContactViewController: BaseViewController {
    var presenter: ZPContactListPresenterProtocol?
    var listAllFriend:NSMutableArray?
    var numberAdressBook = 0
    var numberZaloFriend = 0
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var labelLastUpdate: UILabel!
    @IBOutlet weak var labelLastUpdateValue: UILabel!
    @IBOutlet weak var buttonUpdate: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = R.string_ZPC_update_contact_text()!
        buttonUpdate.setupZaloPay()
        buttonUpdate.layer.cornerRadius = buttonUpdate.frame.size.height/2
        
        self.view.backgroundColor = .white
        self.getDateUpdate()
    }
    
    func getDateUpdate() {
        labelLastUpdateValue?.text = self.presenter?.getDateUpdate()
    }
    
    @IBAction func handleUpdateContact(_ sender: Any) {
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.contact_setting_update)
        presenter?.updateListContact {[weak self] () in
            self?.showToast(with: R.string_ZPC_update_contact_done(), delay: 0)
            self?.getDateUpdate()
        }
    }
}
