//
//  ZPCFavoriteHeaderView.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 7/27/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayCommonSwift
import ZaloPayProfile

struct UIFavoriteParameters {
    static let leftPadding = 12
    
    static let topLabelSectionTitle = 12
    static let widthLabelSectionTitle = 200
    static let heightLabelSectionTitle = 15
}

protocol ZPCFavoriteHeaderViewDelegate: class {
    func didScroll()
    func didSelectContact(_ zpContact: ZPContactSwift)
    func removeFriendFavorite(_ zpContact:ZPContactSwift)
    func isRedPacketContain(contact: ZPContactSwift) -> Bool
}

fileprivate let kContactCellName = "ZPContactTableViewCell"

class ZPCFavoriteHeaderView: UIView {
    var maxNumberFavoriteFriends = 10
    var rowHeight: CGFloat = 72
    var maxViewHeight: CGFloat = 216 // 72 * 3
    var realNumberFavoriteFriends = 0
    
    weak var zpcFavoriteHeaderViewDelegate: ZPCFavoriteHeaderViewDelegate?
    var tableViewFavorite: UITableView!
    
    var labelSectionTitle:UILabel!
    var labelEmpty: UILabel!
    var favoriteMap = [String: Bool]()

    var searchText: String = ""
    var sourceOfZPC: ZPCSource?
    var mainColor = UIColor.zaloBase()

    var arrayListFriendFavorite = NSMutableArray()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.buildTableViewHeader()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func isFavorite(_ user:ZPContactSwift) -> Bool {
        let key = user.zpContactID
        return self.favoriteMap[key] != nil
    }
    
    func setDataSource(_ dataSource: NSArray, isTemp: Bool = false) {
        let favorites = dataSource.count > 0 ? NSMutableArray(array: dataSource) : NSMutableArray()
        realNumberFavoriteFriends = isTemp ? realNumberFavoriteFriends : favorites.count
        self.arrayListFriendFavorite = favorites
        for item in self.arrayListFriendFavorite {
            if let contact = item as? ZPContactSwift {
                let zpContactID = contact.zpContactID
                self.favoriteMap[zpContactID] = true
            }
        }
        self.updateViewLayout()
    }
    
    func numOfFriends() -> Int {
        return arrayListFriendFavorite.count
    }
    
    func removeFavorite(contact: ZPContactSwift) {
        if contact.zpContactID == nil {
            return
        }
        let zpContactID = contact.zpContactID
        if let res = self.favoriteMap.removeValue(forKey: zpContactID) {
            realNumberFavoriteFriends -= (res ? 1 : 0)
        }
        
        self.arrayListFriendFavorite.remove(contact)
        self.updateViewLayout()
    }
    
    func addFavorite(contact: ZPContactSwift) -> Bool {
        if contact.zpContactID == nil {
            return false
        }
        if realNumberFavoriteFriends >= self.maxNumberFavoriteFriends {
            return false
        }
        realNumberFavoriteFriends += 1
        let zpContactID = contact.zpContactID
        self.favoriteMap.updateValue(true, forKey: zpContactID)
        self.arrayListFriendFavorite.add(contact)
        self.updateViewLayout()
        return true
    }
    
    func register() {
        self.tableViewFavorite.register(UINib(nibName: kContactCellName, bundle: nil), forCellReuseIdentifier: kContactCellName)
    }
    
    func setFavoriteTableView() {
        self.tableViewFavorite = UITableView()
        self.tableViewFavorite.separatorColor = UIColor.clear
        self.tableViewFavorite.backgroundColor = UIColor.white
        self.tableViewFavorite.rowHeight = rowHeight
        self.addSubview(self.tableViewFavorite)
        
        self.tableViewFavorite.snp.makeConstraints { make in
            make.top.equalTo(self.labelSectionTitle.snp.bottom)
            make.right.equalTo(0)
            make.bottom.equalTo(0)
            make.left.equalTo(0)
        }
        
        self.tableViewFavorite.delegate = self
        self.tableViewFavorite.dataSource = self
        
        register()
    }
    
    func buildTableViewHeader() {
        self.arrayListFriendFavorite = NSMutableArray()
        labelSectionTitle = UILabel()
        labelSectionTitle.zpBoxTitleGayRegular()
        self.addSubview(labelSectionTitle)
        self.labelSectionTitle.snp.makeConstraints { make in
            make.top.equalTo(UIFavoriteParameters.topLabelSectionTitle)
            make.left.equalTo(UIFavoriteParameters.leftPadding)
            make.width.equalTo(UIFavoriteParameters.widthLabelSectionTitle)
            make.height.equalTo(UIFavoriteParameters.heightLabelSectionTitle)
        }
        
        self.backgroundColor = UIColor.white
        self.setFavoriteTableView()
        
        let bottomLineView = ZPLineView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 1))
        bottomLineView.backgroundColor = UIColor.line()
        self.addSubview(bottomLineView)
        bottomLineView.alignToBottom()
        
        labelEmpty = UILabel()
        labelEmpty.text = R.string_ZPC_empty_favorite()!
        labelEmpty.numberOfLines = 0
        labelEmpty.zpBoxSubTitleGrayRegular()
        self.addSubview(labelEmpty)
        self.labelEmpty.snp.makeConstraints { make in
            make.top.equalTo(self.labelSectionTitle.snp.bottom).offset(UIFavoriteParameters.topLabelSectionTitle - 2)
            make.left.equalTo(self.labelSectionTitle.snp.left)
            make.right.equalTo(0)
            make.height.equalTo(UIFavoriteParameters.heightLabelSectionTitle)
        }
        
        self.updateViewLayout()
    }
    
    func updateViewLayout() {
        labelSectionTitle.text = "\(R.string_ZPC_favorite_friend()!) (\((self.arrayListFriendFavorite.count))/\(maxNumberFavoriteFriends))"
        
        labelEmpty.isHidden = (arrayListFriendFavorite.count > 0)
        
        var height = CGFloat(UIFavoriteParameters.heightLabelSectionTitle + UIFavoriteParameters.topLabelSectionTitle)
        if arrayListFriendFavorite.count > 0 {
            height += min((CGFloat(arrayListFriendFavorite.count) * tableViewFavorite.rowHeight), maxViewHeight)
        } else {
            height += CGFloat(UIFavoriteParameters.heightLabelSectionTitle + 2*UIFavoriteParameters.topLabelSectionTitle + 6)
        }
        
        let frame = CGRect(x: 0, y: 0, width: tableViewFavorite.frame.size.width, height: height)
        self.frame = frame
        
        arrayListFriendFavorite.sort { (v1, v2) -> ComparisonResult in
            let name1 = (v1 as? ZPContactSwift)?.displayName ?? "???"
            let name2 = (v2 as? ZPContactSwift)?.displayName ?? "???"
            let res = name1.compare(name2)
            return res
        }
        
        tableViewFavorite.reloadData()
    }
    
    func reloadCell(at contact: ZPContactSwift) {
        guard let friends = self.arrayListFriendFavorite as? [ZPContactSwift] else {
            return
        }
        
        if let idx = friends.index(where: { (item) -> Bool in return item.zpContactID == contact.zpContactID }) {
            let indexpath = IndexPath(item: idx, section: 0)
            tableViewFavorite.reloadRows(at: [indexpath], with: .fade)
        }
    }
    
    func prepareCell(_ tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: kContactCellName, for: indexPath) as? ZPContactTableViewCell {
            guard let zaloUser = arrayListFriendFavorite.safeObject(at: indexPath.row) as? ZPContactSwift else  {
                return cell
            }
            cell.sourceOfZPC = sourceOfZPC
            cell.mainColor = mainColor
            cell.zpContactTableViewCellDelegate = self
            
            zaloUser.isRedPacket = zpcFavoriteHeaderViewDelegate?.isRedPacketContain(contact: zaloUser) ?? false
            cell.setItem(item: zaloUser,
                         isFavorite: true,
                         enableFavoriteFriendFeature: true
            )
            cell.changeColorText(searchText)
            return cell
        }
        
        return UITableViewCell()
    }
}

extension ZPCFavoriteHeaderView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayListFriendFavorite.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return prepareCell(tableView, at: indexPath)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let zaloUser = arrayListFriendFavorite.safeObject(at: indexPath.row) as? ZPContactSwift else  {
            return
        }
        zpcFavoriteHeaderViewDelegate?.didSelectContact(zaloUser)
    }
}

extension ZPCFavoriteHeaderView: UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.zpcFavoriteHeaderViewDelegate?.didScroll()
    }
}

extension ZPCFavoriteHeaderView: ZPContactTableViewCellDelegate {
    func handleSelectedRedPacket(_ zpContact: ZPContactSwift) {
        zpcFavoriteHeaderViewDelegate?.didSelectContact(zpContact)
    }
    
    func handleFavoriteFriendList(_ zpContact: ZPContactSwift, _ zpContactTableViewCell: ZPContactTableViewCell) {
        removeFavorite(contact: zpContact)
        zpcFavoriteHeaderViewDelegate?.removeFriendFavorite(zpContact)
    }
}
