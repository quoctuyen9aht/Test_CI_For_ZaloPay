//
//  ZPCSearchEmptyView.swift
//  ZaloPay
//
//  Created by Bon Bon on 1/6/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayCommonSwift

class ZPCSearchEmptyView: UIView {
    class func emptyView(for tableView: UITableView) -> ZPCSearchEmptyView{
        let w = tableView.frame.size.width
        let h = UINavigationController.currentActiveNavigationController()?.topViewController?.view.frame.size.height ?? tableView.frame.size.height
        let frame = CGRect(x: 0, y: 0, width: w, height: h)
        let view = ZPCSearchEmptyView(frame: frame,imageName: "search_empty", textLabel: R.string_ZPC_Search_Empty())
        return view
    }
    init(frame: CGRect, imageName: String, textLabel: String) {
        super.init(frame: frame)
        let imageView = UIImageView(image: UIImage(named:imageName))
        self.addSubview(imageView)
        imageView.snp.makeConstraints { (make) in
            make.size.equalTo(imageView.image!.size)
            make.top.equalTo(50)
            make.centerX.equalTo(self.snp.centerX)
        }
        
        let label = UILabel()
        self.addSubview(label)
        label.zpPlaceHolderRegular()
        label.text = textLabel
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = UIColor.placeHolder()
        label.snp.makeConstraints { (make) in
            make.width.equalTo(self.frame.size.width - 50)
            make.top.equalTo(imageView.snp.bottom).offset(21)
            make.centerX.equalTo(imageView.snp.centerX)
        }
        
        self.backgroundColor = .clear
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
}
