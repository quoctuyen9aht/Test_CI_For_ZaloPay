//
//  ZPContactListViewController.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 7/19/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import CocoaLumberjackSwift
import ZaloPayAnalyticsSwift
import ZaloPayProfile
import ZaloPayConfig

struct UIZPCParameters {
    static let leftPadding = 12
    static let topLableSectionTitle = 12
    static let widthLableSectionTitle = 10
    static let heightLableSectionTitle = 15

    static let heightSection = 27
    static let frameRightButton = CGRect(x: 12, y: 0, width: 26, height: 26)
    static let leftTitleView = -52

    static let leftViewGuide = 41
    static let rightViewGuide = -13
    static let backgroundColorViewGuide = "#fff8d9"
    static let borderColorViewGuide = "#ecc17e"
    static let topLabelGuide = 5
    static let leftLabelGuide = 8
    static let rightLabelGuide = -10
    static let widthImageViewArrow = 25


}

fileprivate let kContactCellName = "ZPContactTableViewCell"
fileprivate let kContactSelectZaloPayIdCellName = "ZPContactSelectZaloPayIdCell"

class ZPCConfig {
    let maxFavoriteFriend = 10
    let footerViewHeight = 115
    let rowHeight: CGFloat = 72
    let maxFavoriteViewHeight: CGFloat = 720 // maxFavoriteFriend * rowHeight
    var currentPhone: String?
    var viewMode: String?
    var numberPad: Bool?
    var isKeyBoardNumperPad = false {
        didSet {
            isSentTracking = false
        }
    }
    var isSentTracking = false
    var sourceZPC: ZPCSource?
    var displayMode: ZPCListViewMode = .normal
    var backgroundColor = UIColor.zaloBase()

    var maxForRedPacKet:Int = 100
    var arrUsersRedPacket: Array<Any>?
}

class ZPContactListViewController: BaseViewController  {
    @IBOutlet weak var segmentBar: UISegmentedControl!
    @IBOutlet weak var segmentBarView: UIView!
    @IBOutlet weak var searchBarView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    var refreshControl: ZPRefreshControl?

    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var viewButtonNext: UIView!
    @IBOutlet weak var heightViewButtonNext: NSLayoutConstraint!
    @IBOutlet weak var bottomViewButtonNext: NSLayoutConstraint!

    @IBOutlet weak var heightZPCRedPacketSelectedView: NSLayoutConstraint!
    @IBOutlet weak var redPacketSelectedView: UIView!
    var redPacketHeaderView: ZPCRedPacketHeaderView!
    var navTitleView:ZPCNavigationBarTitleView!
    var contactList: ZPContactList!
    let disposeBag = DisposeBag()

    var presenter: ZPContactListPresenterProtocol?
    var tableFooterView: ZPContactListFooterView?

    var searchView: ZPSearchContactListView?
    var favoriteView : ZPCFavoriteHeaderView?
    var navigationTitle: String?

    let config = ZPCConfig()
    var displayHelper: ZPCDisplayHelperProtocol!
    var searchDisplay: ZPCSearchDisplayHelperProtocol!
    var endSelectedRedPacket: ZPCEndSelectedRedPacket = .next
    let viewScreenMask = UIView(frame: UIScreen.main.bounds)
    let maxSearchLength = ZPApplicationConfig.getZPCAppConfig()?.getMaxSearchTextLength() ?? 26

    override func viewDidLoad() {
        super.viewDidLoad()
        if (self.config.sourceZPC == ZPCSource.fromRedPacket && self.config.displayMode == .normal) || self.config.sourceZPC == ZPCSource.fromTransferMoney_PhoneNumber {
            self.segmentBar.selectedSegmentIndex = 1
        }
        self.contactList = ZPContactList()
        self.setupRedPacketView()
        self.addSearchView(config.isKeyBoardNumperPad)
        self.setupSegmentBar()
        self.setupNavigationBar()
        //let enableFavorite = self.presenter?.getEnableFavoriteMode() ?? false
        let enableFavorite = false
        self.setupDisplayHelper(enableFavorite)
        self.setupTableView(enableFavorite)
        self.setupRefreshControl()

        self.presenter?.getListContact(sync: false)
        config.numberPad = self.config.viewMode == ViewModeKeyboard_Phone

        let eAppear = self.rx.methodInvoked(#selector(UIViewController.viewDidAppear(_:))).take(1)
        let doneLoad = self.rx.methodInvoked(#selector(ZPContactListViewController.showZPContacts(_:dictionary:dataFavorite:totalFriend:))).take(1)

        _ = Observable.combineLatest(eAppear, doneLoad).subscribe( onCompleted: { [weak self] in
            if self?.config.sourceZPC != ZPCSource.fromRedPacket {
                self?.searchView?.setForcusState(true)
                self?.searchView?.textField?.becomeFirstResponder()
            }else {
                if (self?.presenter?.interactor?.isZPCLaunchedTheFirst())! {
                    self?.setupGuideViewForRedPacket()
                }
            }
        })
        self.observerKeyboard()
        ZPTrackingHelper.shared().trackScreen(withName:self.screenName())
        //ZPAppFactory.sharedInstance().trackScreen(self.screenName())

    }

    override func screenName() -> String! {
        return ZPTrackerScreen.Screen_iOS_Contact
    }

    func setupDisplayHelper(_ enableFavorite: Bool) {
        self.displayHelper = ZPCDisplayHelper()
        self.searchDisplay = ZPCSearchDisplayHelper()
        self.displayHelper.controller = self
        self.searchDisplay.controller = self
        self.displayHelper.enableFavorite = enableFavorite
        self.searchDisplay.enableFavorite = enableFavorite
        self.displayHelper.configTrackingTouchAlphabet()
    }

    private func setupRefreshControl() {
        self.refreshControl = ZPRefreshControl(with: self.tableView)
        // Red color in case red packet
        if self.config.sourceZPC == ZPCSource.fromRedPacket {
            self.refreshControl?.loadingIcon.tintColor = UIColor.zp_red()
        }
        if #available(iOS 10.0, *) {
            self.tableView.refreshControl = self.refreshControl
        } else {
            self.tableView.addSubview(self.refreshControl!)
        }

        self.refreshControl?.addTarget(self, action: #selector(self.refreshListContact), for: UIControlEvents.valueChanged)
        self.refreshControl?.resetAnimation()
    }

    override func backButtonClicked(_ sender: Any!) {
        endSelectedRedPacket = .close
        super.backButtonClicked(sender)
    }

    override func backEventId() -> Int {
        return ZPAnalyticEventAction.contact_touch_back.rawValue
    }

    override func viewWillSwipeBack() {
        presenter?.viewWillPopout()
        self.config.viewMode = config.numberPad! ? ViewModeKeyboard_Phone : ViewModeKeyboard_ABC
        presenter?.setViewMode()
    }

    // MARK: Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateNavigationBarColor()
        ZPTrackingHelper.shared().trackScreen(with: self)
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.contact_launched)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if self.config.sourceZPC == .fromRedPacket, let arrRedPackets = self.presenter?.interactor?.getAllRedPacketContacts() {
            presenter?.btnNextRedPacketTouch(arrRedPackets, self.endSelectedRedPacket)
        }
    }

    // MARK: internal method
    func setupTableView(_ enableFavorite: Bool) {
        self.tableView.register(UINib(nibName: kContactCellName, bundle: nil), forCellReuseIdentifier: kContactCellName)
        self.tableView.register(UINib(nibName: kContactSelectZaloPayIdCellName, bundle: nil), forCellReuseIdentifier: kContactSelectZaloPayIdCellName)

        self.tableView.separatorColor = UIColor.clear
        self.tableView.backgroundColor = UIColor.white
        self.tableView.rowHeight = CGFloat(config.rowHeight)

        // Dont show right index list when row count is below
        self.tableView.sectionIndexMinimumDisplayRowCount = 5

        if enableFavorite {
            favoriteView = self.createFavoriteFriendView()
            favoriteView?.sourceOfZPC = self.config.sourceZPC
            favoriteView?.mainColor = self.config.backgroundColor
            self.tableView.tableHeaderView = favoriteView
        }
        self.setupTableFooterView()
    }
    @objc func tapViewScreenMask(_ ges: UIGestureRecognizer) {
        if ges.state == .ended {
            UIView.animate(withDuration: 0.5, animations: {
                self.viewScreenMask.alpha = 0.0
            })

        }
    }
    func setupGuideViewForRedPacket() {

        // mask view
        viewScreenMask.backgroundColor = .clear
        let tapMaskView = UITapGestureRecognizer(target: self, action: #selector(tapViewScreenMask))
        viewScreenMask.addGestureRecognizer(tapMaskView)
        viewScreenMask.alpha = 0
        ZPUIAppDelegate.sharedInstance.window?.addSubview(viewScreenMask)

        let viewGuide = UIView()
        viewGuide.backgroundColor = UIColor(hexString:UIZPCParameters.backgroundColorViewGuide)
        viewGuide.roundRect(4, borderColor: UIColor(hexString: UIZPCParameters.borderColorViewGuide), borderWidth: 1)
        viewScreenMask.addSubview(viewGuide)

        // label guide
        let labelGuide = UILabel()
        labelGuide.backgroundColor = .clear
        labelGuide.text = self.presenter?.getTextGuide()
        labelGuide.textColor = UIColor.subText()
        labelGuide.font = UIFont.sfuiTextRegularSmall()
        labelGuide.numberOfLines = 0
        labelGuide.sizeToFit()
        viewGuide.addSubview(labelGuide)
        labelGuide.snp.makeConstraints { (make) in
            make.top.equalTo(UIZPCParameters.topLabelGuide)
            make.left.equalTo(UIZPCParameters.leftLabelGuide)
            make.right.equalTo(UIZPCParameters.rightLabelGuide)
        }
        let topViewGuide = UIApplication.shared.statusBarFrame.height + (self.navigationController?.navigationBar.frame.size.height)! + (self.searchBarView?.frame.size.height)! + 10
        viewGuide.snp.makeConstraints { (make) in
            make.top.equalTo(topViewGuide)
            make.left.equalTo(UIZPCParameters.leftViewGuide)
            make.right.equalTo(UIZPCParameters.rightViewGuide)
            make.bottom.equalTo(labelGuide.snp.bottom).offset(5)
        }

        // image
        let imageViewArrow = UIImageView()
        imageViewArrow.image = UIImage(named: "arrow_tip")
        imageViewArrow.layer.borderColor = UIColor(hexString: UIZPCParameters.backgroundColorViewGuide)?.cgColor
        viewScreenMask.insertSubview(imageViewArrow, aboveSubview: viewGuide)
        imageViewArrow.snp.makeConstraints { (make) in
            make.top.equalTo(topViewGuide - 10)
            make.left.equalTo(UIScreen.main.bounds.size.width * 3/4)
            make.width.equalTo(UIZPCParameters.widthImageViewArrow)
            make.bottom.equalTo(viewGuide.snp.top).offset(1)
        }

        UIView.animate(withDuration: 0.5, animations: {
            self.viewScreenMask.alpha = 1.0
        })

    }

    private func reloadData() {
        self.tableView.reloadData()
        if #available(iOS 10, *) {

        } else if let header = self.tableView.tableHeaderView { // iOS 9 or older
            self.tableView.tableHeaderView = header
        }
    }

    func setupTableFooterView() {
        tableFooterView = ZPContactListFooterView(frame: CGRect(x: 0, y: 0, width: Int(tableView.bounds.width), height: config.footerViewHeight), sourceZPC: self.config.sourceZPC, color: self.config.backgroundColor)
        tableFooterView?.zpcListFooterViewDelegate = self
        tableView.tableFooterView = tableFooterView
    }

    func createFavoriteFriendView() -> ZPCFavoriteHeaderView {
        let frame = CGRect(x: 0, y: 0, width: Int(UIScreen.main.bounds.width), height: 0)
        let view = ZPCFavoriteHeaderView.init(frame: frame)
        view.maxNumberFavoriteFriends = config.maxFavoriteFriend
        view.rowHeight = config.rowHeight
        view.maxViewHeight = config.maxFavoriteViewHeight
        view.zpcFavoriteHeaderViewDelegate = self
        return view
    }

    func addSearchView(_ isKeyBoardNumperPad: Bool) {
        let width = CGFloat(self.view.frame.size.width)
        let height = CGFloat(self.searchBarView.frame.size.height)

        var searchPlaceHolder = R.string_ZPC_guide_search()
        if self.config.sourceZPC == ZPCSource.fromTopup || self.config.sourceZPC == ZPCSource.fromRedPacket {
            searchPlaceHolder = R.string_ZPC_guide_search_name_or_phone()
        }

        searchView = ZPSearchContactListView(frame: CGRect(x: 0, y: 0, width:width, height: height), isPhoneNumberPad:isKeyBoardNumperPad, searchPlaceHolder: searchPlaceHolder!)
        searchView?.focusBorderColor = self.config.backgroundColor
        searchView?.textField?.addTarget(self, action: #selector(resignSearchText), for:.editingDidEndOnExit)
        self.searchBarView.addSubview(searchView!)
        searchView?.zpcSearchViewDelegate = self
        self.setupSearchSignal()
        self.searchView?.textField?.delegate = self
    }

    func prehandleSearchText(_ text: NSString?) -> String {
        let toSearch = (text ?? "") as String
        if toSearch != UIPasteboard.general.string {
            return toSearch;
        }
        let orginText = toSearch.replacingOccurrences(of: "\\p{Cf}", with: "", options: .regularExpression)
        let txt = String(orginText).replacingOccurrences(of: " ", with: "", options: .regularExpression)
        if let normalPhone = (txt as NSString).normalizePhoneNumber(), (normalPhone as NSString).isPhoneNumber() {
            return normalPhone
        }
        return orginText;
    }

    func selectingZaloPayIdRowDidTouched() {
        presenter?.requestUserInfoWithZaloPayId(accountName: (searchView?.textField?.text)!)
    }

    func setupSearchSignal () {
        searchView?.textField?.clearButton.rx.tap.bind {[weak self] _ in
            self?.searchView?.textField?.text = ""
            self?.searchWithText("")
            }.disposed(by: self.disposeBag)

        let until = self.rac_willDeallocSignal()
        searchView?.textField?.rac_textSignal().take(until: until).throttle(0.3).filter({ (text) -> Bool in
            return text != nil
        }).subscribeNext({ [weak self] (text) in
            let toSearch = self?.prehandleSearchText(text) ?? ""
            self?.searchView?.textField?.text = toSearch;
            self?.searchWithText(toSearch)
        })

        searchView?.textField?.rx.value.filter({ [weak self]  in self?.config.isSentTracking == false && ($0?.count == 0).not }).bind(onNext: { [weak self](_) in
            guard let wSelf = self else {
                return
            }
            wSelf.config.isSentTracking = true
            let type = wSelf.config.isKeyBoardNumperPad ? ZPAnalyticEventAction.contact_search_phonenumber : ZPAnalyticEventAction.contact_search_name
            ZPTrackingHelper.shared().trackEvent(type)
            ZPTrackingHelper.shared().trackEvent(.contact_search_input)
        }).disposed(by: self.disposeBag)
    }

    func observerKeyboard() {
        let keyboardShow = NotificationCenter.default.rx.notification(.UIKeyboardWillShow)
        let keyboardHide = NotificationCenter.default.rx.notification(.UIKeyboardWillHide)
        keyboardHide.subscribe(onNext: { [weak self] notification in
            self?.keyBoardWillHide(notification: notification)
        }).disposed(by: disposeBag)
        keyboardShow.subscribe(onNext: { [weak self] notification in
            self?.keyBoardWillShow(notification: notification)
        }).disposed(by: disposeBag)
    }

    @IBAction func segmentBarValueChanged(_ sender: Any) {
        guard let segmentBar = sender as? UISegmentedControl else {
            return
        }
        let idx = segmentBar.selectedSegmentIndex
        let eventId = (idx == 0) ? ZPAnalyticEventAction.contact_touch_zalopay : ZPAnalyticEventAction.contact_touch_all
        ZPTrackingHelper.shared().trackEvent(eventId)
        presenter?.processGroupData(self.getMode() == ZPCListViewMode.zalopay)
    }

    func setupSegmentBar() {
        self.segmentBar.layer.cornerRadius = 5.0
        self.segmentBar.layer.borderWidth = 1.0
        self.segmentBar.layer.masksToBounds = true

        self.segmentBar.layer.borderColor = UIColor.white.cgColor
        self.segmentBar.tintColor = UIColor.white
        self.segmentBar.backgroundColor = self.config.backgroundColor
        self.segmentBarView.backgroundColor = self.config.backgroundColor

        if self.config.displayMode != .normal {
            self.segmentBarView.isHidden = true
            self.segmentBarView.snp.updateConstraints { make in
                make.height.equalTo(0)
            }
        }
    }

    func setupRedPacketView() {
        redPacketHeaderView = ZPCRedPacketHeaderView(frame: redPacketSelectedView.bounds)
        redPacketHeaderView.maxForRedPacKet = self.config.maxForRedPacKet
        redPacketHeaderView.mainColor = self.config.backgroundColor
        redPacketHeaderView.delegate = self
        heightZPCRedPacketSelectedView.constant = 0
        redPacketSelectedView.isHidden = true

        self.heightViewButtonNext.constant = config.sourceZPC == ZPCSource.fromRedPacket ? 80 : 0
        if config.sourceZPC == ZPCSource.fromRedPacket {
            self.updateShowRedPacketView(animated: false)
        }
        self.buttonNext.roundRect(4)
    }


    func addRedPackets(_ contacts: [ZPContactSwift]) {
        redPacketHeaderView.arrContacts.append(contentsOf: contacts)
        self.redPacketHeaderView.setDataSource(redPacketHeaderView.arrContacts as NSArray)
        self.updateNavTitleViewText()
        self.updateShowRedPacketView(animated: true)
    }

    func addContactsRedPacketWhenReady(_ contacts: [ZPContactSwift]) {
        if redPacketHeaderView != nil
        {
            self.addRedPackets(contacts)
        } else {
            self.rx.methodInvoked(#selector(viewDidLoad)).take(1).bind(onNext: { [weak self](_) in
                self?.addRedPackets(contacts)
            }).disposed(by: disposeBag)
        }
    }

    func updateShowRedPacketView(animated: Bool) {
        let numberSelectedContacts = self.redPacketHeaderView.arrContacts.count
        self.buttonNext.isEnabled = numberSelectedContacts > 0
        self.buttonNext.setBackgroundColor(self.config.backgroundColor, for: .normal)
        self.buttonNext.setBackgroundColor(UIColor.disableButton(), for: .disabled)
        //self.buttonNext.setBackgroundColor(UIColor.buttonRedPacketHighlight(), for: .highlighted)
        if animated {
            self.view.updateConstraints()
        }
    }

    @IBAction func btnNextTouch(_ sender: Any) {
        endSelectedRedPacket = .next
        self.navigationController?.popViewController(animated: true)
    }

    func setupNavigationBar() {
        let button = UIButton(frame: UIZPCParameters.frameRightButton)
        button.titleLabel?.font = UIFont.iconFont(withSize: UIZPCParameters.frameRightButton.size.height)
        button.setIconFont("onboarding_refresh", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 7, bottom: 0, right: -7)
        button.rx.tap.bind {
            ZPTrackingHelper.shared().trackEvent(.contact_touch_refresh_top)
        }.disposed(by: disposeBag)
        let rightButton = UIBarButtonItem.init(customView: button)
        button.addTarget(self, action: #selector(refreshListContact), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = rightButton

        let width = UIScreen.main.bounds.width
        let height = (self.navigationController!.navigationBar.frame.height)

        navTitleView = ZPCNavigationBarTitleView(frame:CGRect(x:CGFloat(UIZPCParameters.leftTitleView) , y:0, width:width, height:height))
        self.updateNavTitleViewText()
        self.navigationItem.titleView = navTitleView
        navTitleView .backgroundColor = self.config.backgroundColor
        self.updateNavigationBarColor()
    }

    func updateNavigationBarColor() {
        self.navigationController?.navigationBar.backgroundColor = self.config.backgroundColor
        self.navigationController?.navigationBar.barTintColor = self.config.backgroundColor
    }

    func updateNavTitleViewText() {
        if self.config.sourceZPC == ZPCSource.fromRedPacket {
            let numberCurrentRedpackets = self.presenter?.interactor?.redPacketZPContactIds.count ?? 0
            let finalTitle = navigationTitle! + " (" + String(describing: numberCurrentRedpackets) + "/" + String(describing: self.config.maxForRedPacKet) + ")"
            navTitleView.setCustomTitle(finalTitle)
        } else {
            navTitleView.setCustomTitle(navigationTitle)
        }
    }

    @objc func goToZPUpdateContactScreen()  {
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.contact_touch_refresh_top)
        presenter?.showZPUpdateConatct()
    }

    func keyBoardWillShow(notification: Notification) {
        searchView?.setForcusState(true)
        var keyboardRectangle = CGRect.zero

        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            animateViewInSyncWithKeyboard(keyboardNotification: notification) { [weak self] in
                if self?.config.sourceZPC == ZPCSource.fromRedPacket {
                    self?.bottomViewButtonNext.constant = keyboardHeight
                    return
                }
                // Adjust table view height to shorten section index list
                self?.tableViewBottomConstraint.constant = keyboardHeight
            }
        }
    }

    func keyBoardWillHide(notification: Notification) {
        self.tableView.contentInset = UIEdgeInsets.zero
        searchView?.setForcusState(false)
        animateViewInSyncWithKeyboard(keyboardNotification: notification) { [weak self] in
            if self?.config.sourceZPC == ZPCSource.fromRedPacket {
                self?.bottomViewButtonNext.constant = 0
                return
            }
            // Adjust table view height to shorten section index list
            self?.tableViewBottomConstraint.constant = 0
        }
    }

    // Perform view animation smoothly along with keyboard show/hide animation
    // (Example: in case animating a bottom view when keyboard showing up)
    private func animateViewInSyncWithKeyboard(keyboardNotification: Notification, animations: @escaping () -> Void) {
        // Get keyboard animation constants
        guard let keyboardAnimationDuration = keyboardNotification.userInfo?.double(forKey: UIKeyboardAnimationDurationUserInfoKey) else { return }
        guard let keyboardAnimationCurve = keyboardNotification.userInfo?.uInt(forKey: UIKeyboardAnimationCurveUserInfoKey) else {return}
        let options = UIViewAnimationOptions.init(rawValue: UInt(keyboardAnimationCurve))

        UIView.animate(withDuration: keyboardAnimationDuration, delay: 0, options: options, animations: {  [weak self] in
            animations()
            self?.view.layoutIfNeeded()
        }, completion: nil)
    }


    func searchWithText(_ text:String) {
        searchView?.hideShowBtnChangeKeyBoard()
        presenter?.searchWithText(text)
    }

    @objc func resignSearchText() {
        self.handleResignKeyboard()
        self.searchView?.textField?.resignFirstResponder()
    }

    func handleResignKeyboard() {
        if self.config.sourceZPC == .fromRedPacket {
            return
        }
        guard searchDisplay.dataSource.count == 1 else {
            return
        }
        if let zaloUser = searchDisplay.dataSource.first {
            self.didSelectZaloUser(zaloUser)
        }
    }
}

extension ZPContactListViewController {
    func didSelectZaloUser(_ zaloUser:ZPContactSwift) {
        if self.isFavorite(zaloUser) {
            ZPTrackingHelper.shared().trackEvent(.contact_touch_friend_favorite)
        } else {
            ZPTrackingHelper.shared().trackEvent(.contact_touch_friend_nonfavorite)
        }
        if zaloUser.usedZaloPay.not {
            ZPTrackingHelper.shared().trackEvent(.contact_touch_nonzpfriend)
        }
        let currentContactMode = self.presenter?.getCurrentContactMode() ?? ContactMode_ZPC_All
        var shouldRequest = (searchView?.textField?.text?.isEmpty.not ?? false) && !zaloUser.usedZaloPay
        shouldRequest = shouldRequest && (currentContactMode != ContactMode_ZPC_PhoneBook)
        presenter?.didSelectZaloUser(zaloUser, reqUserInfo: shouldRequest)
    }
}

extension ZPContactListViewController: ZPContactListViewProtocol {
    func showErrorSelectingZaloPayId(withText content: String) {
        guard let searchText = searchView?.textField?.text else {
            return
        }
        let zaloPayID: NSAttributedString = NSAttributedString.init(string: " \"\(searchText)\" ", attributes: [NSAttributedStringKey.font : UIFont.sfuiTextSemibold(withSize: 15)])
        let attributedMessage:NSMutableAttributedString = ZPDialogView.attributeString(withMessage: R.string_Transfer_ZaloPayId())
        attributedMessage.append(zaloPayID)
        attributedMessage.append(NSAttributedString.init(string: R.string_ZPC_not_exist_message()))

        ZPDialogView.showDialog(with: DialogTypeNotification, title: nil, attributeMessage: attributedMessage, buttonTitles: [R.string_ButtonLabel_Close()], handler: nil)
    }

    func searchIfNeed() -> Bool {
        let text = searchView?.textField?.text ?? ""
        if text.isEmpty {
            return false
        }
        searchWithText(text)
        return true
    }

    @objc func showZPContacts(_ dataSection: [String],
                        dictionary: [String: [ZPContactSwift]],
                        dataFavorite: NSArray,
                        totalFriend: Int) {
        self.presenter?.prepareUsersRedPacket(self.config.arrUsersRedPacket)
        self.config.arrUsersRedPacket = nil
        // Setup favorite friends
        self.favoriteView?.setDataSource(dataFavorite)

        // Setup data
        self.displayHelper.allKey = dataSection
        self.displayHelper.friendMap = dictionary

        // Setup tableview with specific mode
        let currentPhone = config.currentPhone
        if currentPhone != nil && currentPhone != "" {
            searchView?.textField?.text = currentPhone
            searchWithText(currentPhone!)
            config.currentPhone = nil
        } else if searchIfNeed().not { // normal
            self.showNormalMode()
        }

        self.hideLoading()
        self.updateTitleFooter(totalFriend: totalFriend)
    }

    func updateTitleFooter(totalFriend: Int) {
        self.tableFooterView?.numberContactsLabel.text = "\(totalFriend) \(R.string_ZPC_number_contacts()!)"
    }

    func resultDataSearch(_ listSearchView: NSMutableArray,_ listFriendRedPacket: NSMutableArray, _ listFriendFavorite: NSArray, _ phoneModel: ZPContactSwift?) {
        // Setup favorite friends
        let text = searchView?.textField?.text ?? ""
        favoriteView?.searchText = text
        self.redPacketHeaderView.searchText = text
        self.favoriteView?.setDataSource(listFriendFavorite, isTemp: true)

        self.redPacketHeaderView.setDataSource(listFriendRedPacket)

        if text.isEmpty.not {
            if let result = listSearchView as? [ZPContactSwift] {
                showSearchMode(searchText: text, result: result, additionalPhone: phoneModel)
            }
            return
        }
        self.presenter?.getListContact(sync: false)
    }

    func showLoading() {
        ZPAppFactory.sharedInstance().showHUDAdded(to: self.view)
    }

    func hideLoading() {
        ZPAppFactory.sharedInstance().hideAllHUDs(for: self.view)
    }

    func getMode() -> ZPCListViewMode {
        if self.config.displayMode == .normal {
            let idx = segmentBar.selectedSegmentIndex
            return idx == 0 ? .zalopay : .all
        }
        return self.config.displayMode
    }

    func selectedUserToRedPacket(_ zpContact: ZPContactSwift) {
        self.handleSelectedRedPacket(zpContact)
    }

    func alertSearchUserPhoneNotNetwork(_ error:NSError) {
        if self.config.sourceZPC == ZPCSource.fromTransferNone
        || self.config.sourceZPC == ZPCSource.fromTransferMoney_Contact
        || self.config.sourceZPC == ZPCSource.fromTransferMoney_PhoneNumber {
            ZPDialogView.showDialogWithError(error, handle: nil)
        }

    }
}

extension ZPContactListViewController: ZPCAdditionalPhoneDelegate {
    func showNormalMode() {
        tableView.delegate = displayHelper
        tableView.dataSource = displayHelper
        self.reloadData()
        tableView.tableHeaderView = favoriteView ?? redPacketHeaderView
        tableView.tableFooterView = tableFooterView
    }

    func showSearchMode(searchText: String, result: Array<ZPContactSwift>, additionalPhone: ZPContactSwift?) {
        searchDisplay.dataSource = result
        searchDisplay.searchText = searchText
        tableView.delegate = searchDisplay
        tableView.dataSource = searchDisplay
        self.reloadData()
        tableView.tableFooterView = nil

        let numberFriendCustom = self.config.sourceZPC == ZPCSource.fromRedPacket ? self.redPacketHeaderView.arrContacts.count : favoriteView?.numOfFriends()
        if result.count == 0 && (numberFriendCustom == 0 || numberFriendCustom == nil){
            if additionalPhone == nil {
                if self.config.sourceZPC == ZPCSource.fromRedPacket || self.config.sourceZPC == ZPCSource.fromTopup {
                    tableView.tableHeaderView = ZPCSearchEmptyView.emptyView(for: tableView)

                } else {
                     tableView.tableHeaderView = nil // ZPCSearchEmptyView.emptyView(for: tableView)
                }
                return

            }
            let header = ZPCAdditionalPhoneView.view(additionalPhone)
            header.delegate  = self
            tableView.tableHeaderView = header
        } else {
            tableView.tableHeaderView = favoriteView ?? redPacketHeaderView
        }
    }

    func selectModel(_ contactModel: ZPContactSwift?) {
        guard let contact = contactModel else {
            return
        }

        if self.config.sourceZPC == .fromRedPacket {
            self.searchView?.textField?.text = ""
            let isExist = self.presenter?.interactor?.redPacketUnsaveZPContacts.contains(where: { (unsaveContact) -> Bool in
                return unsaveContact.zpContactID == contact.zpContactID
            })
            if (isExist?.not)! && (self.presenter?.interactor?.redPacketZPContactIds.count)! < self.config.maxForRedPacKet {
                self.presenter?.interactor?.redPacketUnsaveZPContacts.append(contact)
            }

        }
        didSelectZaloUser(contact)
    }
}

//MARK: handleFavoriteFriendList
extension ZPContactListViewController: ZPContactTableViewCellDelegate {
    func isFavorite(_ user:ZPContactSwift) -> Bool {
        guard let favorite = self.favoriteView else {
            return false
        }
        return favorite.isFavorite(user)
    }

    func handleFavoriteFriendList(_ zpContact: ZPContactSwift, _ zpContactTableViewCell: ZPContactTableViewCell) {
        if self.isFavorite(zpContact) {
            removeFavorite(contact: zpContact)
            favoriteView?.removeFavorite(contact: zpContact)
        } else {
            addFavorite(contact: zpContact)
        }
        self.reloadData()
    }

    func removeFavorite(contact: ZPContactSwift) {
        let key = contact.firstCharacterInDisplayName ?? ""
        var dictionary = self.displayHelper.friendMap
        var contacts = dictionary[key] ?? [ZPContactSwift]()
        if self.getMode() == ZPCListViewMode.zalopay {
            if contact.zaloPayId.isEmpty.not {
                contacts.append(contact)
            } else {
                var totalFriend = 0
                let friendMap = self.displayHelper.friendMap
                let keys = friendMap.keys
                for k in keys {
                    if k == key {
                        totalFriend += contacts.count
                        continue
                    }
                    totalFriend += friendMap[k]!.count
                }
                totalFriend += self.favoriteView?.arrayListFriendFavorite.count ?? 0
                self.updateTitleFooter(totalFriend: totalFriend)
            }
        } else {
            contacts.append(contact)
        }
        dictionary[key] = contacts.isEmpty ? nil : contacts
        self.displayHelper.friendMap = dictionary
        self.displayHelper.allKey = dictionary.keys.sorted(by: <)
        self.contactList.setFavorite(contact, isFavorite: false)
        _ = self.searchIfNeed()
        self.showToast(with: "\(contact.displayName ?? "")\r\n\(R.string_ZPC_remove_friend_from_favorite()!)", delay:0)
    }

    func addFavorite(contact: ZPContactSwift) {
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.contact_touch_favorite)
        if (favoriteView?.addFavorite(contact: contact))! {
            let key = contact.firstCharacterInDisplayName ?? ""
            var dictionary = self.displayHelper.friendMap
            let contacts = dictionary[key] ?? [ZPContactSwift]()
            dictionary[key] = (contacts.count > 1) ? contacts.filter { $0.zpContactID != contact.zpContactID } : nil
            self.displayHelper.friendMap = dictionary
            self.displayHelper.allKey = dictionary.keys.sorted(by: <)
            self.contactList.setFavorite(contact, isFavorite: true)
            _ = self.searchIfNeed()
            self.showToast(with: "\(contact.displayName ?? "")\r\n\(R.string_ZPC_add_friend_into_favorite()!)", delay:0)
        } else {
            let attributesRegular : [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : UIFont.sfuiTextRegular(withSize: 15), NSAttributedStringKey.foregroundColor : UIColor.darkGray]

            let msg = R.string_ZPC_max_favorite_text()
            let attributedString:NSMutableAttributedString = ZPDialogView.attributeString(withMessage: msg)
            attributedString.addAttributes(attributesRegular, range: NSRange.init(location: 0, length: (msg?.count)!))


            ZPDialogView.showDialog(with:DialogTypeNotification,
                                    title: nil,
                                    attributeMessage: attributedString ,
                                    buttonTitles: [R.string_ButtonLabel_OK()],
                                    handler: nil)
        }
    }
    func handleSelectedRedPacket(_ zpContact: ZPContactSwift) {
        addRedPacket(contact: zpContact)
    }

    func reloadTableViewWhenChangeListRedPacket(with zpContact: ZPContactSwift, isAdd:Bool) {

        let text = searchView?.textField?.text ?? ""
        let indexPath = self.getIndexPath(from: zpContact)
        if indexPath.row == -1 {
            self.searchWithText(text)
            return
        }

        if text.isEmpty {
            let key = zpContact.firstCharacterInDisplayName ?? ""
            var contacts = self.displayHelper.friendMap[key]

            if isAdd {
                contacts?.remove(at: indexPath.row)
                if contacts?.count == 0 {
                    self.displayHelper.allKey.remove(at: indexPath.section)
                }
            }else {
                contacts?.append(zpContact)
                if self.displayHelper.allKey.contains(key).not {
                    self.displayHelper.allKey.append(key)
                }
            }
            self.displayHelper.friendMap.updateValue(contacts!, forKey: key)
            self.displayHelper.allKey = self.displayHelper.allKey.sorted(by: <)
            self.reloadData()
        }else {

            if isAdd {
                self.searchDisplay.dataSource.remove(at: indexPath.row)
            }else {
                let isExist = self.searchDisplay.dataSource.contains(where: { (contact) -> Bool in
                    return zpContact.zpContactID == contact.zpContactID
                })
                if isExist.not {
                    self.searchDisplay.dataSource.append(zpContact)
                }

            }
            searchView?.textField?.text = isAdd ? "" : text
            self.searchWithText(searchView?.textField?.text ?? "")
        }

    }

    func addRedPacket(contact: ZPContactSwift) {
        if (self.presenter?.interactor?.redPacketZPContactIds.count ?? 0) >= self.config.maxForRedPacKet {
            self.showToastNoneIcon(with: String(format: R.string_ZPC_max_red_packet_text(), self.config.maxForRedPacKet), delay:0)
            searchView?.textField?.text = ""
            self.searchWithText("")
            return
        }
        let isAdded = self.redPacketHeaderView.addContact(contact: contact)
        if isAdded {
            (self.presenter as? ZPContactListViewDelegateProtocol)?.addedRedPacketContact(contact)
            self.updateShowRedPacketView(animated: true)
            self.reloadTableViewWhenChangeListRedPacket(with: contact, isAdd: true)
            self.updateNavTitleViewText()
        }
    }

    func removeRedPacket(contact: ZPContactSwift) {
        self.redPacketHeaderView.removeContact(contact: contact)
        (self.presenter as? ZPContactListViewDelegateProtocol)?.removedRedPacketContact(contact)
        self.reloadTableViewWhenChangeListRedPacket(with: contact, isAdd: false)
        self.updateShowRedPacketView(animated: true)
    }

    func isRedPacketContain(contact: ZPContactSwift) -> Bool {
        return self.redPacketHeaderView.contains(contact)
    }
}

extension ZPContactListViewController: ZPCRedPacketHeaderViewDelegate {
    func didTapRedPacketContact(_ zpContact: ZPContactSwift) {
        self.removeRedPacket(contact: zpContact)
        self.updateNavTitleViewText()
    }

    func reloadCell(at contact: ZPContactSwift) {
        if self.isFavorite(contact) {
            self.favoriteView?.reloadCell(at: contact)
            return
        }
        let indexPath = self.getIndexPath(from: contact)
        if indexPath.row > -1 {
            self.tableView.reloadRows(at: [indexPath], with: .fade)
        }
    }

    func getIndexPath(from contact: ZPContactSwift) -> IndexPath {
        var section = 0
        var idx = -1
        let text = searchView?.textField?.text ?? ""
        if text.isEmpty {
            let allKey = self.displayHelper.allKey
            let friendMap = self.displayHelper.friendMap
            section = allKey.index(where: { (key) -> Bool in
                if let contacts = friendMap[key] {
                    idx = contacts.index(where: { (item) -> Bool in
                        return item.zpContactID == contact.zpContactID
                    }) ?? -1
                    return idx != -1
                }
                return false
            }) ?? -1
        } else {
            let contacts = self.searchDisplay.dataSource
            idx = contacts.index(where: { (item) -> Bool in
                return item.zpContactID == contact.zpContactID
            }) ?? -1
        }


        let indexPath = IndexPath(item: idx, section: section)
        return indexPath

    }
}



extension ZPContactListViewController: ZPCFavoriteHeaderViewDelegate {

    func didScroll() {
        self.view.endEditing(true)
    }

    func didSelectContact(_ zpContact: ZPContactSwift) {
        self.didSelectZaloUser(zpContact)
    }

    func removeFriendFavorite(_ zpContact:ZPContactSwift){
        ZPTrackingHelper.shared().trackEvent(.contact_touch_unfavorite)
        removeFavorite(contact: zpContact)
        self.reloadData()
        self.favoriteView?.tableViewFavorite.reloadData()
    }
}

// MARK: *** ZPContactListFooterViewDelegate
extension ZPContactListViewController: ZPContactListFooterViewDelegate {
    func didTapUpdatingButton() {
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.contact_touch_refresh_bot)
        refreshListContact()
    }
    @objc func refreshListContact() {
        presenter?.updateListContact {[weak self] () in
            let scale = UIScreen.main.nativeBounds.size.height <= 1136 ? 0.25 : 0.35
            let top = Double(UIScreen.main.bounds.size.height) * scale
            self?.showToastCustomAboveCenter(with: R.string_ZPC_update_contact_done(), delay: 0, top: Int(top))
            self?.refreshControl?.endRefreshing()
        }
    }
}

extension ZPContactListViewController: ZPContactSearchViewDelegate {
    func didResignFirstResponder() {
        self.handleResignKeyboard()
    }
}

extension ZPContactListViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= self.maxSearchLength
    }
}
