//
//  ZPAllowAccessContactViewController.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 7/26/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit

class ZPAllowAccessContactViewController: BaseViewController {
    lazy var contactList: ZPContactList = ZPContactList()
    
    func showLoading() {
        //HUD.show(.progress)
        ZPAppFactory.sharedInstance().showHUDAdded(to: self.view)
    }
    
    func hideLoading() {
        //HUD.hide()
        ZPAppFactory.sharedInstance().hideAllHUDs(for: self.view)
    }
    
    func processGetDataAndAskPermission() {
        self.contactList.getDataContactList(syncData: true) { (_) in }
    }
    
    @IBAction func buttonAllowTouch(_ sender: Any) {
        //        processGetDataAndAskPermission()
        self.contactList.openSetting()
    }
    
    @IBOutlet weak var buttonAllow: UIButton!
    
    @IBAction func buttonLaterTouch(_ sender: Any) {
        let attributesRegular : [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : UIFont.sfuiTextRegular(withSize: 15), NSAttributedStringKey.foregroundColor : UIColor.darkGray]
        let msg = "\(R.string_ZPC_inorge_update_contact_description()!)"
        let attributedString:NSMutableAttributedString = ZPDialogView.attributeString(withMessage: msg)
        
        attributedString.addAttributes(attributesRegular, range: NSRange.init(location: 0, length: R.string_ZPC_inorge_update_contact_description()!.count))
        
        ZPDialogView.showDialog(with:DialogTypeNotification,
                                title: nil,
                                attributeMessage: attributedString ,
                                buttonTitles: [R.string_ButtonLabel_Upgrade(), R.string_ButtonLabel_Skip()],
                                handler: { (idx) in
                                    if idx != 0 {
                                        self.navigationController?.popViewController(animated: true)
                                    }else {
                                        //                                        self.processGetDataAndAskPermission()
                                        self.contactList.openSetting()
                                    }
        })
        
    }
    @IBOutlet weak var buttonLater: UIButton!
    
    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = R.string_ZPC_update_contact_text()!
        buttonAllow.setupZaloPay()
        buttonAllow.layer.cornerRadius = self.buttonAllow.frame.size.height/2
        
        
        let underlineAttribute = [NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue]
        let underlineAttributedString = NSAttributedString(string: (buttonLater.titleLabel?.text)!, attributes: underlineAttribute)
        buttonLater.setAttributedTitle(underlineAttributedString, for: .normal)
        // Do any additional setup after loading the view.
        if UIView.isIPhone4() {
            self.updateUI()
            return;
        }
        self.imageView.snp.updateConstraints { (make) in
            make.top.equalTo(self.view).offset(UIScreen.main.bounds.size.height * 96 / 736 )
        }
    }
    
    func updateUI() {
        self.imageView.snp.updateConstraints { (make) in
            make.top.equalTo(10)
        }
    }
}
