//
//  ZPContactListFooterView.swift
//  ZaloPay
//
//  Created by TriDao on 9/5/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit

protocol ZPContactListFooterViewDelegate: class {
    func didTapUpdatingButton()
}

class ZPContactListFooterView: UIView {
    var numberContactsLabel: UILabel!
    var updatingContactsButton: UIButton!
    weak var zpcListFooterViewDelegate: ZPContactListFooterViewDelegate?
    var sourceZPC: ZPCSource?
    var mainColor: UIColor = UIColor.zaloBase()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    init(frame: CGRect, sourceZPC: ZPCSource?, color: UIColor = UIColor.zaloBase()) {
        super.init(frame: frame)
        self.sourceZPC = sourceZPC
        self.mainColor = color
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupView() {
        self.backgroundColor = UIColor.white
        
        // Create number of contacts label
        numberContactsLabel = UILabel()
        numberContactsLabel.textAlignment = .center
        numberContactsLabel.font = UIFont.sfuiTextRegular(withSize: 12)
        
        numberContactsLabel.textColor = UIColor.subText()
        self.addSubview(numberContactsLabel)
        numberContactsLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self).offset(10)
            make.left.equalTo(self)
            make.right.equalTo(self)
            make.height.equalTo(22)
        }
        
        // Create updating button
        updatingContactsButton = UIButton()
        updatingContactsButton.setTitle(R.string_ButtonLabel_Upgrade(), for: .normal)
        updatingContactsButton.titleLabel?.font = UIFont.sfuiTextRegular(withSize: 16)
        
        var buttonBackgroundColor = UIColor.zaloBase()
        if sourceZPC == ZPCSource.fromRedPacket {
            buttonBackgroundColor = mainColor
            updatingContactsButton.setBackgroundColor(UIColor.buttonRedPacketHighlight(), for: .highlighted)
        }
        updatingContactsButton.setBackgroundColor(buttonBackgroundColor, for: .normal)
        
        updatingContactsButton.addTarget(self, action: #selector(handleUpdatingContact(_:)), for: .touchUpInside)
        self.addSubview(updatingContactsButton)
        updatingContactsButton.snp.makeConstraints { (make) in
            make.top.equalTo(numberContactsLabel.snp.bottom).offset(10)
            make.centerX.equalTo(self)
            make.width.equalTo(137.8)
            make.height.equalTo(35)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updatingContactsButton.clipsToBounds = true
        updatingContactsButton.layer.cornerRadius = 0.5 * updatingContactsButton.bounds.height
    }
    
    @objc func handleUpdatingContact(_ sender: Any) {
        zpcListFooterViewDelegate?.didTapUpdatingButton()
    }
}
