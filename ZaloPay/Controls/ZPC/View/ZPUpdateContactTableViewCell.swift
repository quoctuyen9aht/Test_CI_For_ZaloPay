//
//  ZPUpdateContactTableViewCell.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 7/26/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit

class ZPUpdateContactTableViewCell: UITableViewCell {

    @IBOutlet weak var zpImageView: ZPIconFontImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        zpImageView.setIconColor(UIColor.zp_blue())
    }

    func loadCell(_ value:String,_ iconFont: String,_ textTitle:String) {
        
        self.titleLabel.text = textTitle
        zpImageView.setIconFont(iconFont)
        zpImageView.setIconColor(UIColor.zaloBase())
        numberLabel.text = value
        self.accessoryType = (numberLabel.text?.characters.count)! > 0 ? .none : .disclosureIndicator
    }
    
}
