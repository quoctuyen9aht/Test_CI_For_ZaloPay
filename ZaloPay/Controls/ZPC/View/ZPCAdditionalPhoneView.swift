//
//  ZPCAdditionalPhoneView.swift
//  ZaloPay
//
//  Created by bonnpv on 8/5/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit
import RxCocoa
import RxSwift
import ZaloPayProfile

protocol ZPCAdditionalPhoneDelegate: class {
    func selectModel(_ contactModel: ZPContactSwift?);
}

class ZPCAdditionalPhoneView: UIView {
    let avatar = ZPIconFontImageView()
    let phoneLabel = UILabel()
    let descriptionLabel = UILabel()
    var zpImageView: ZPIconFontImageView!
    
    var contactModel: ZPContactSwift?
    weak var delegate: ZPCAdditionalPhoneDelegate?
    
    class func view(_ model: ZPContactSwift?) -> ZPCAdditionalPhoneView {
        let width = UIScreen.main.applicationFrame.size.width
        let frame = CGRect(x: 0, y: 0, width: width, height: 72)
        let _return = ZPCAdditionalPhoneView(frame: frame)
        _return.setModel(model)
        return _return
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setModel(_ model: ZPContactSwift?) {
        guard let item = model else  {
            return
        }
        
        if (item.phoneNumber.isEmpty) {
            return
        }
        let phone = item.phoneNumber
        self.contactModel = model
        zpImageView.isHidden = model?.zaloPayId.isEmpty ?? true
        phoneLabel.text = phone
        descriptionLabel.text = model?.displayName ?? R.string_ZPC_Unsave_Phone_Number()
        if item.avatar.isEmpty.not, let url = URL(string: item.avatar) {
            avatar.sd_setImage(with: url)
        } else {
            avatar.setIconFont("profile_avatadefault")
        }
    }    
    
    func setupView () {
        self.addSubview(avatar)
        self.addSubview(phoneLabel)
        self.addSubview(descriptionLabel)
        self.backgroundColor = .white
        
        avatar.roundRect(20)
        //avatar.backgroundColor = .blue
        //avatar.image =  UIImage.defaultAvatar()
        
        
        avatar.snp.makeConstraints { (make) in
            make.left.equalTo(12)
            make.centerY.equalTo(self.snp.centerY)
            make.width.equalTo(40)
            make.height.equalTo(40)
        }
        avatar.setIconFont("profile_avatadefault")
        avatar.setIconColor(UIColor(fromHexString: "#C9D4E5"))
        avatar.defaultView.font = UIFont.iconFont(withSize: 40)
        
        zpImageView = ZPIconFontImageView(frame: CGRect(x: 0, y: 0, width: 18, height: 18))
        self.addSubview(zpImageView)
        zpImageView.roundRect(Float(self.zpImageView.frame.size.width/2))
        zpImageView.layer.borderWidth = 1.5
        zpImageView.layer.borderColor = UIColor.white.cgColor
        zpImageView.image = UIImage(named: "zpc_favi")
        zpImageView.snp.makeConstraints { (make) in
            make.right.equalTo(avatar.snp.right).offset(3)
            make.bottom.equalTo(avatar.snp.bottom).offset(3)
            make.width.equalTo(18)
            make.height.equalTo(18)
        }
        
        phoneLabel.zpMediumTitleStyle()
        phoneLabel.textColor = UIColor.zaloBase()
        phoneLabel.snp.makeConstraints { (make) in
            make.top.equalTo(19)
            make.left.equalTo(avatar.snp.right).offset(12)
            make.right.equalTo(0)
            make.height.equalTo(20)
        }
        
        descriptionLabel.zpDetailGrayStyle()
        descriptionLabel.snp.makeConstraints { (make) in
            make.top.equalTo(phoneLabel.snp.bottom).offset(3)
            make.left.equalTo(avatar.snp.right).offset(12)
            make.right.equalTo(0)
            make.height.equalTo(15)
        }
        
        let button = UIButton.init(type: .custom)
        self.addSubview(button)
        button.snp.makeConstraints { (make) in
            make.top.bottom.left.right.equalTo(0)
        }
        button.addTarget(self, action: #selector(selectPhone), for: .touchUpInside)
    }
    
    @objc func selectPhone() {
        guard let delegate = self.delegate else  {
            return
        }
        delegate.selectModel(self.contactModel)
    }
}
