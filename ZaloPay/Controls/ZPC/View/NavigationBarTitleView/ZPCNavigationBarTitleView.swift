//
//  ZPCNavigationBarTitleView.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 7/26/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit

class ZPCNavigationBarTitleView: UIView {

    var label:UILabel?
    var attStringTitle:String?
    var currentTitle:String?
    override init(frame: CGRect) {
        super.init(frame:frame)
        updateUINavigationBarTitle()
        //setUpNavigation()
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        self.init(coder: aDecoder)
    }
    
    func setCustomTitle(_ title:String?) {
        currentTitle = title
        self.setUpNavigation()
    }
    
    func updateUINavigationBarTitle() {
        label = UILabel(frame: self.frame)
        label?.backgroundColor = UIColor.clear
        label?.numberOfLines = 2
        label?.font = UIFont.boldSystemFont(ofSize: 18.0)
        label?.textAlignment = .center
        label?.textColor = UIColor.white
        self.addSubview(label!)
        
    }
    
    func setUpNavigation() {

        var titleTemp = R.string_ZPC_zalo_contact()!
        if currentTitle != nil && (currentTitle?.isEmpty.not)! {
            titleTemp = currentTitle!
        }
        attStringTitle = titleTemp
        label?.text = attStringTitle!
    }
    
}
