//
//  ZPInfoPopup.swift
//  ZaloPay
//
//  Created by vuongvv on 1/15/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit

fileprivate let kType = "type"
fileprivate let kTitle = "title"
fileprivate let kMessage = "message"
fileprivate let kHint = "hint"
fileprivate let kTheme = "theme"
fileprivate let kActions = "actions"
fileprivate let kAction = "action"
fileprivate let kActionTitle = "title"

fileprivate let kCongras = "CHÚC MỪNG"

fileprivate let kImgHeader = "app_ios_infopopup_giftbox"
fileprivate let kImgBGTop = "app_ios_infopopup_redbg"
fileprivate let kImgBGBottom = "app_ios_infopopup_whitebg"

@objcMembers
class ZPLiXiWishesPopupData: ZPPopupData {
    var type: Int = 0
    var title = ""
    var theme: Int = 0
    var message = ""
    var hint = ""
    var action: Int = 0
    var actionTitle = ""
    override init() {
        super.init()
    }
    init(_ dic: Dictionary<AnyHashable, Any>) {
        self.type = dic.value(forKey: kType, defaultValue: 0)
        self.title = dic.value(forKey: kTitle, defaultValue: "")
        self.message = dic.value(forKey: kMessage, defaultValue: "")
        self.hint = dic.value(forKey: kHint, defaultValue: "")
        self.theme = dic.value(forKey: kTheme, defaultValue: 1)
        let actions = dic.value(forKey: kActions, defaultValue: [Dictionary<AnyHashable, Any>]())
        if actions.count > 0 {
            let actionFirst = actions.first!
            self.action = actionFirst.value(forKey: kAction, defaultValue: 0)
            self.actionTitle = actionFirst.value(forKey: kActionTitle, defaultValue: "")
        }
    }

    static func test() -> ZPLiXiWishesPopupData {
        let str = "{\"type\":1,\"title\":\"Bạn được nhận lời chúc\",\"message\":\"Cả năm may mắn\",\"hint\":\"Lì xi càng nhanh quà mau nhanh tới\",\"actions\":[{\"action\":1,\"title\":\"Đóng\"}],\"theme\":1}"
        let dict = NSDictionary.convertToDictionary(text: str)
        return ZPLiXiWishesPopupData(dict!)
    }
}

class ZPLiXiWishesPopup: ZPPopup {

    var popupHeaderView = UIImageView()
    var popupContentView = UIView()
    var labelInfo = UILabel()
    //Sub
    var labelTitleTopCM = UILabel()
    var labelTitle = UILabel()
    var labelDescription = MDHTMLLabelCustom()
    var acceptButton = UIButton()
    var popupBackgroundImageView = UIImageView()
    var popupBackgroundImageViewBottom = UIImageView()

    var infoData = ZPLiXiWishesPopupData()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func config2x() -> [String: Any] {
        return [
            "popupHeight": isIP5() ? 350 : 380,
            "bgTopHeight": isIP5() ? 200 : 230,
            "popupLeftRightMargin": isIP5() ? 15 : 30,
            "headerTopOffset": 23,
            "titleTopCM": 34,
            "titleTop": isIP5() ? 0 : 10,
            "titleHeightCM": 40,
            "titleHeight": 18,
            "descriptionHeight": 60,
            "descriptionTop": 10,
            "labelMoneyTop": isIP5() ? 15 : 30,
            "labelMoneyLeft": 10,
            "acceptButtonTop": 10,
            "detailLabelTop": 30
        ]
    }

    func config3x() -> [String: Any] {
        return [
            "popupHeight": 380,
            "bgTopHeight": 230,
            "popupLeftRightMargin": 30,
            "headerTopOffset": 27,
            "titleTopCM": 34,
            "titleTop": 14,
            "titleHeightCM": 40,
            "titleHeight": 20,
            "descriptionHeight": 66,
            "descriptionTop": 10,
            "labelMoneyTop": 30,
            "labelMoneyLeft": 10,
            "acceptButtonTop": 10,
            "detailLabelTop": 30,
        ]
    }
    override func setData(data: ZPPopupData) {
        guard let dataGet = data as? ZPLiXiWishesPopupData else {
            return
        }
        self.infoData = dataGet
        self.setupView()

        self.labelTitle.text = self.infoData.title
        self.labelInfo.text = self.infoData.message
        self.labelDescription.htmlText = self.infoData.hint
        self.acceptButton.setTitle(self.infoData.actionTitle, for: UIControlState.normal)

    }

    func setupView() {

        self.popupContentView = UIView()
        self.popupContentView.backgroundColor = UIColor.clear
        self.popupContentView.roundRect(6.0)
        self.addSubview(self.popupContentView)
        self.sendSubview(toBack: self.popupContentView)

        let fontSize: CGFloat = UIView.isScreen2x() ? 14 : 16

        self.labelTitle = UILabel()
        self.labelTitle.textColor = .white
        self.labelTitle.textAlignment = .center
        self.labelTitle.font = UIFont.sfuiTextMedium(withSize: fontSize)
        self.labelTitle.numberOfLines = 0

        var fontSizeTopCM: CGFloat = UIView.isScreen2x() ? 28 : 30
        fontSizeTopCM = self.isIP5() ? 26 : fontSizeTopCM
        self.labelTitleTopCM = UILabel()
        self.labelTitleTopCM.textColor = .white
        self.labelTitleTopCM.textAlignment = .center
        self.labelTitleTopCM.font = UIFont.sfuiTextSemibold(withSize: fontSizeTopCM)
        self.labelTitleTopCM.text = kCongras

        self.popupBackgroundImageView = UIImageView(image: UIImage.fromInternalAppThemeLiXiWishes(kImgBGTop, andThemeId: self.infoData.theme))
        self.popupBackgroundImageViewBottom = UIImageView(image: UIImage.fromInternalAppThemeLiXiWishes(kImgBGBottom, andThemeId: self.infoData.theme))

        let fontInfo: CGFloat = UIView.isScreen2x() ? 30 : 32
        self.labelInfo = UILabel()
        self.labelInfo.textAlignment = .center
        self.labelInfo.adjustsFontSizeToFitWidth = true
        self.labelInfo.baselineAdjustment = .alignCenters
        self.labelInfo.numberOfLines = 1
        self.labelInfo.textColor = UIColor(hexValue: 0xF5DC31)
        self.labelInfo.font = UIFont.sfuiTextSemibold(withSize: fontInfo)

        self.acceptButton = UIButton(type: .custom)
        self.acceptButton.setTitle("", for: UIControlState.normal)
        self.acceptButton.setTitleColor(UIColor.fromInternalAppThemeLiXiWishes(theme: self.infoData.theme), for: UIControlState.normal)
        self.acceptButton.titleLabel?.font = UIFont.sfuiTextRegular(withSize: 17)
        self.acceptButton.setBackgroundColor(UIColor.white, for: UIControlState.normal)
        self.acceptButton.roundRect(22)
        self.acceptButton.layer.borderColor = UIColor.fromInternalAppThemeLiXiWishes(theme: self.infoData.theme).cgColor
        self.acceptButton.layer.borderWidth = 1
        self.acceptButton.addTarget(self, action: #selector(detailButtonClick), for: UIControlEvents.touchUpInside)

        self.labelDescription = MDHTMLLabelCustom()
        self.labelDescription.textColor = UIColor(hexValue: 0x727F8C)
        self.labelDescription.numberOfLines = 0
        self.labelDescription.font = UIFont.sfuiTextRegular(withSize: fontSize)
        self.labelDescription.textAlignment = .center

        self.popupContentView.addSubview(self.popupBackgroundImageView)
        self.popupContentView.addSubview(self.popupBackgroundImageViewBottom)
        self.popupContentView.addSubview(self.labelTitleTopCM)
        self.popupContentView.addSubview(self.labelTitle)
        self.popupContentView.addSubview(self.labelInfo)
        self.popupContentView.addSubview(self.labelDescription)
        self.popupContentView.addSubview(self.acceptButton)

        //Header
        self.popupHeaderView = UIImageView(image: UIImage.fromInternalAppThemeLiXiWishes(kImgHeader, andThemeId: self.infoData.theme))
        self.addSubview(self.popupHeaderView)

        self.setupViewConstrain()
        self.addTarget(self, action: #selector(self.hide), for: UIControlEvents.touchDown)
        self.configToast()
    }

    func setupViewConstrain() {

        let config = UIView.isScreen2x() ? self.config2x() : self.config3x()
        self.popupContentView.snp.makeConstraints { (make) in
            make.left.equalTo(config.int(forKey: "popupLeftRightMargin"))
            make.right.equalTo(-config.int(forKey: "popupLeftRightMargin"))
            make.centerY.equalToSuperview().offset(25)
            make.height.equalTo(config.int(forKey: "popupHeight"))
        }

        var size = self.validImageSize(self.popupHeaderView.frame.size, maxWidth: self.popupWidth())
        self.popupHeaderView.snp.makeConstraints { (make) in
            make.size.equalTo(size)
            make.centerX.equalToSuperview()
            make.bottom.equalTo(self.popupContentView.snp.top).offset(config.int(forKey: "headerTopOffset"))
        }

        self.labelTitleTopCM.snp.makeConstraints { (make) in
            make.top.equalTo(config.int(forKey: "titleTopCM"))
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(config.int(forKey: "titleHeightCM"))
        }

        self.labelTitle.snp.makeConstraints { (make) in
            make.top.equalTo(self.labelTitleTopCM.snp.bottom).offset(config.int(forKey: "titleTop"))
            make.left.equalTo(8)
            make.right.equalTo(-8)
            make.height.greaterThanOrEqualTo(config.int(forKey: "titleHeight"))
        }

        size = self.validImageSize(self.popupBackgroundImageView.frame.size, maxWidth: self.popupWidth())
        size.height = CGFloat(config.int(forKey: "bgTopHeight"))
        self.popupBackgroundImageView.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.height.equalTo(size.height)
            make.left.equalTo(0)
            make.right.equalTo(0)
        }

        self.labelInfo.snp.makeConstraints { (make) in
            make.left.equalTo(config.int(forKey: "labelMoneyLeft"))
            make.right.equalTo(-config.int(forKey: "labelMoneyLeft"))
            make.height.equalTo(65)
            make.top.equalTo(self.labelTitle.snp.bottom).offset(config.int(forKey: "labelMoneyTop"))
        }

        size = self.validImageSize(self.popupBackgroundImageViewBottom.frame.size, maxWidth: self.popupWidth())
        self.popupBackgroundImageViewBottom.snp.makeConstraints { (make) in
            make.top.equalTo(self.popupBackgroundImageView.snp.bottom).offset(-1)
            make.bottom.equalTo(0)
            make.left.equalTo(0)
            make.right.equalTo(0)
        }

        self.labelDescription.snp.makeConstraints { (make) in
            make.top.equalTo(self.popupBackgroundImageViewBottom.snp.top).offset(config.int(forKey: "descriptionTop"))
            make.height.equalTo(config.int(forKey: "descriptionHeight"))
            make.left.equalTo(10)
            make.right.equalTo(-10)
        }

        self.acceptButton.snp.makeConstraints { (make) in
            make.top.equalTo(self.labelDescription.snp.bottom).offset(config.int(forKey: "acceptButtonTop"))
            make.centerX.equalToSuperview()
            make.width.equalTo(200)
            make.height.equalTo(44)
        }

    }
}
