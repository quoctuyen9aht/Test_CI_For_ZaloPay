//
//  ZPPopup.swift
//  ZaloPay
//
//  Created by vuongvv on 3/20/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
@objcMembers
@objc class ZPPopupData:NSObject {}
class ZPPopup: UIControl,ZPGiftPopupToastProtocol, ZPPopupHideProtocol {
    var toastView: ZPGiftToastView?
    var parentView: UIView {
        return self
    }
    var detailHandle: (() -> ())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @objc class func showWithData(data: ZPPopupData, detailAction: @escaping (() -> ())) -> ZPPopup? {
        let main = getMainWindow()
        guard let windowMain = main else {
            return nil
        }
        var framePopup = windowMain.frame
        framePopup.origin.y = framePopup.size.height
        
        let popup = self.createPopup(data: data, frame: framePopup)
        popup.detailHandle = detailAction
        popup.setData(data: data)
        windowMain.addSubview(popup)
        
        framePopup.origin.y = 0
        UIView.animate(withDuration: 0.4, animations: {
            popup.frame = framePopup
        }) { (_) in
            popup.backgroundColor = UIColor(white: 0, alpha: 0.5)
        }
        return popup
    }
    class func getMainWindow() -> UIWindow? {
        return UIApplication.shared.keyWindow
    }
    func popupWidth() -> CGFloat {
        return UIScreen.main.applicationFrame.size.width
    }
    func isIP5() -> Bool {
        return UIScreen.main.bounds.size.width <= 320
    }
    func validImageSize(_ size: CGSize, maxWidth width: CGFloat) -> CGSize {
        var sizeAdjust = size
        if sizeAdjust.width > width {
            sizeAdjust.height = width * sizeAdjust.height / sizeAdjust.width
            sizeAdjust.width = CGFloat(width)
        }
        return sizeAdjust
    }
    class func createPopup(data:ZPPopupData,frame: CGRect) -> ZPPopup{
        if let _ = data as? ZPLiXiWishesPopupData{
            return ZPLiXiWishesPopup(frame: frame)
        }
        if let _ = data as? ZPVoucherData{
            return ZPVoucherPopup(frame: frame)
        }
        if let _ = data as? ZPCashbackData{
            return ZPCashBackPopup(frame: frame)
        }
        return ZPPopup(frame: frame)
    }
    func setData(data: ZPPopupData) {
    }
    @objc func hide() {
        var frame = self.frame
        frame.origin.y = frame.size.height
        self.backgroundColor = UIColor.clear
        UIView.animate(withDuration: 0.4, animations: {
            self.frame = frame
        }) { (_) in
            self.removeFromSuperview()
        }
    }
    @objc func detailButtonClick() {
        if (self.detailHandle != nil) {
            self.detailHandle!()
        }
        self.hide()
    }
}
