//
//  ZPVoucherPopup.swift
//  ZaloPay
//
//  Created by vuongvv on 1/15/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit

fileprivate let kType = "type"
fileprivate let kTitle = "title"
fileprivate let kAmount = "amount"
fileprivate let kCampaign = "campaign"
fileprivate let kTheme = "theme"
fileprivate let kVoucherCode = "vouchercode"
fileprivate let kValue = "value"
fileprivate let kValueType = "valuetype"
fileprivate let kVoucherType = "vouchertype"
fileprivate let kDiscountAmount = "discountamount"
fileprivate let kVoucherCondition = "vouchercondition"
fileprivate let kVoucherConditionDesc = "voucherconditiondesc"
fileprivate let kActions = "actions"
fileprivate let kAction = "action"
fileprivate let kActionTitle = "title"

fileprivate let kCongras = "CHÚC MỪNG"

fileprivate let kImgHeader = "app_ios_voucherpopup_giftbox"
fileprivate let kImgBGTop = "app_ios_voucherpopup_redbg"
fileprivate let kImgBGBottom = "app_ios_voucherpopup_whitebg"

@objcMembers
class ZPVoucherData: ZPPopupData {
    var type: Int = 0
    var title = ""
    var amount: UInt64 = 0
    var campaign = ""
    var action: Int = 0
    var actionTitle = ""
    var theme: Int = 0
    var vouchercode = ""
    var value: Int = 0
    var valuetype: Int = 0
    var vouchertype: Int = 0
    var discountamount: UInt64 = 0
    var vouchercondition = ""
    var voucherconditiondesc = ""
    override init() {
        super.init()
    }
    init(_ dic: Dictionary<AnyHashable, Any>) {
        self.type = dic.value(forKey: kType, defaultValue: 0)
        self.title = dic.value(forKey: kTitle, defaultValue: "")
        self.amount = dic.value(forKey: kAmount, defaultValue: UInt64(0))
        self.campaign = dic.value(forKey: kCampaign, defaultValue: "")
        self.theme = dic.value(forKey: kTheme, defaultValue: 1)
        self.vouchercode = dic.value(forKey: kVoucherCode, defaultValue: "")
        self.value = dic.value(forKey: kValue, defaultValue: 0)
        self.valuetype = dic.value(forKey: kValueType, defaultValue: 2)
        self.vouchertype = dic.value(forKey: kVoucherType, defaultValue: 0)
        self.discountamount = dic.value(forKey: kDiscountAmount, defaultValue: UInt64(0))
        self.vouchercondition = dic.value(forKey: kVoucherCondition, defaultValue: "")
        self.voucherconditiondesc = dic.value(forKey: kVoucherConditionDesc, defaultValue: "")

        let actions = dic.value(forKey: kActions, defaultValue: [Dictionary<AnyHashable, Any>]())
        if actions.count > 0 {
            let actionFirst = actions.first!
            self.action = actionFirst.value(forKey: kAction, defaultValue: 0)
            self.actionTitle = actionFirst.value(forKey: kActionTitle, defaultValue: "")
        }
    }

    static func test() -> ZPVoucherData {
        let str = "{\"type\":2,\"title\":\"BẠN ĐƯỢC TẶNG VOUCHER\",\"valuetype\":1,\"value\":50,\"amount\":2000,\"discountamount\":50000,\"campaign\":\"Tặng voucher\",\"voucherconditiondesc\":\"Tặng 1 voucher vuivui.com/thoi-trang\",\"actions\":[{\"action\":2,\"title\":\"Lưu lại\"}],\"theme\":2}"
        let dict = NSDictionary.convertToDictionary(text: str)
        return ZPVoucherData(dict!)
    }
}

class ZPVoucherPopup: ZPPopup{

    var popupHeaderView = UIImageView()
    var popupContentView = UIView()
    var viewContainerMoney = UIView()
    //Sub
    var labelMoney = UILabel()
    var labelTitleTopCM = UILabel()
    var labelTitle = UILabel()
    var labelDescription = MDHTMLLabelCustom()
    var acceptButton = UIButton()
    var popupBackgroundImageView = UIImageView()
    var popupBackgroundImageViewBottom = UIImageView()

    var voucherData = ZPVoucherData()
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func config2x() -> [String: Any] {
        return [
            "popupHeight": isIP5() ? 350 : 380,
            "bgTopHeight": isIP5() ? 200 : 230,
            "popupLeftRightMargin": isIP5() ? 15 : 30,
            "headerTopOffset": 23,
            "titleTopCM": 34,
            "titleTop": isIP5() ? 0 : 10,
            "titleHeightCM": 40,
            "titleHeight": 18,
            "descriptionHeight": 60,
            "descriptionTop": 10,
            "labelMoneyTop": isIP5() ? 15 : 30,
            "labelMoneyLeft": 40,
            "acceptButtonTop": 10,
            "detailLabelTop": 30
        ]
    }

    func config3x() -> [String: Any] {
        return [
            "popupHeight": 380,
            "bgTopHeight": 230,
            "popupLeftRightMargin": 30,
            "headerTopOffset": 27,
            "titleTopCM": 34,
            "titleTop": 14,
            "titleHeightCM": 40,
            "titleHeight": 20,
            "descriptionHeight": 66,
            "descriptionTop": 10,
            "labelMoneyTop": 30,
            "labelMoneyLeft": 40,
            "acceptButtonTop": 10,
            "detailLabelTop": 30,
        ]
    }
    override func setData(data: ZPPopupData) {
        guard let dataGet = data as? ZPVoucherData else {
            return
        }
        self.voucherData = dataGet
        self.setupView()

        self.labelMoney.attributedText = self.voucherData.valuetype == ZPVoucherValueType.percent.rawValue ? self.getMoneyPercent(withValue: self.voucherData.value) : self.getMoneyAttribute(withValue: self.voucherData.discountamount)
        self.labelDescription.htmlText = self.voucherData.voucherconditiondesc
        self.labelTitle.text = self.voucherData.title
        self.acceptButton.setTitle(self.voucherData.actionTitle, for: UIControlState.normal)

        if self.voucherData.valuetype == ZPVoucherValueType.percent.rawValue {
            self.viewContainerMoney.snp.remakeConstraints({ (make) in
                make.width.equalTo(100)
                make.height.equalTo(65)
                make.centerX.equalToSuperview()
                make.top.equalTo(self.labelTitle.snp.bottom).offset(30)
            })
        }
    }

    func setupView() {

        self.popupContentView = UIView()
        self.popupContentView.backgroundColor = UIColor.clear
        self.popupContentView.roundRect(6.0)
        self.addSubview(self.popupContentView)
        self.sendSubview(toBack: self.popupContentView)

        let fontSize: CGFloat = UIView.isScreen2x() ? 14 : 16

        self.labelTitle = UILabel()
        self.labelTitle.textColor = .white
        self.labelTitle.textAlignment = .center
        self.labelTitle.font = UIFont.sfuiTextMedium(withSize: fontSize)
        self.labelTitle.numberOfLines = 0

        var fontSizeTopCM: CGFloat = UIView.isScreen2x() ? 28 : 30
        fontSizeTopCM = self.isIP5() ? 26 : fontSizeTopCM
        self.labelTitleTopCM = UILabel()
        self.labelTitleTopCM.textColor = .white
        self.labelTitleTopCM.textAlignment = .center
        self.labelTitleTopCM.font = UIFont.sfuiTextSemibold(withSize: fontSizeTopCM)
        self.labelTitleTopCM.text = kCongras

        self.popupBackgroundImageView = UIImageView(image: UIImage.fromInternalAppThemeVoucher(kImgBGTop, andThemeId: self.voucherData.theme))
        self.popupBackgroundImageViewBottom = UIImageView(image: UIImage.fromInternalAppThemeVoucher(kImgBGBottom, andThemeId: self.voucherData.theme))

        self.viewContainerMoney = UIView()
        self.viewContainerMoney.backgroundColor = UIColor(white: 1.0, alpha: 0.1)
        self.viewContainerMoney.roundRect(4.0)
        self.viewContainerMoney.layer.borderColor = UIColor(white: 1.0, alpha: 0.2).cgColor
        self.viewContainerMoney.layer.borderWidth = 1

        self.labelMoney = UILabel()
        self.labelMoney.textAlignment = .center
        self.labelMoney.adjustsFontSizeToFitWidth = true
        self.labelMoney.baselineAdjustment = .alignCenters
        self.labelMoney.numberOfLines = 1
        self.labelMoney.backgroundColor = UIColor.clear

        self.acceptButton = UIButton(type: .custom)
        self.acceptButton.setTitle("", for: UIControlState.normal)
        self.acceptButton.setTitleColor(UIColor.fromInternalAppThemeVoucher(theme: self.voucherData.theme), for: UIControlState.normal)
        self.acceptButton.titleLabel?.font = UIFont.sfuiTextRegular(withSize: 17)
        self.acceptButton.setBackgroundColor(UIColor.white, for: UIControlState.normal)
        self.acceptButton.roundRect(22)
        self.acceptButton.layer.borderColor = UIColor.fromInternalAppThemeVoucher(theme: self.voucherData.theme).cgColor
        self.acceptButton.layer.borderWidth = 1
        self.acceptButton.addTarget(self, action: #selector(detailButtonClick), for: UIControlEvents.touchUpInside)

        self.labelDescription = MDHTMLLabelCustom()
        self.labelDescription.textColor = UIColor(hexValue: 0x727F8C)
        self.labelDescription.numberOfLines = 0
        self.labelDescription.font = UIFont.sfuiTextRegular(withSize: fontSize)
        self.labelDescription.textAlignment = .center

        self.popupContentView.addSubview(self.popupBackgroundImageView)
        self.popupContentView.addSubview(self.popupBackgroundImageViewBottom)
        self.popupContentView.addSubview(self.labelTitleTopCM)
        self.popupContentView.addSubview(self.labelTitle)
        self.popupContentView.addSubview(self.viewContainerMoney)
        self.popupContentView.addSubview(self.labelDescription)
        self.popupContentView.addSubview(self.acceptButton)

        self.viewContainerMoney.addSubview(self.labelMoney)
        //Header
        self.popupHeaderView = UIImageView(image: UIImage.fromInternalAppThemeVoucher(kImgHeader, andThemeId: self.voucherData.theme))
        self.addSubview(self.popupHeaderView)

        self.setupViewConstrain()
        self.addTarget(self, action: #selector(self.hide), for: UIControlEvents.touchDown)

        self.configToast()
    }

    func setupViewConstrain() {

        let config = UIView.isScreen2x() ? self.config2x() : self.config3x()
        self.popupContentView.snp.makeConstraints { (make) in
            make.left.equalTo(config.int(forKey: "popupLeftRightMargin"))
            make.right.equalTo(-config.int(forKey: "popupLeftRightMargin"))
            make.centerY.equalToSuperview().offset(25)
            make.height.equalTo(config.int(forKey: "popupHeight"))
        }

        var size = self.validImageSize(self.popupHeaderView.frame.size, maxWidth: self.popupWidth())
        self.popupHeaderView.snp.makeConstraints { (make) in
            make.size.equalTo(size)
            make.centerX.equalToSuperview()
            make.bottom.equalTo(self.popupContentView.snp.top).offset(config.int(forKey: "headerTopOffset"))
        }

        self.labelTitleTopCM.snp.makeConstraints { (make) in
            make.top.equalTo(config.int(forKey: "titleTopCM"))
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(config.int(forKey: "titleHeightCM"))
        }

        self.labelTitle.snp.makeConstraints { (make) in
            make.top.equalTo(self.labelTitleTopCM.snp.bottom).offset(config.int(forKey: "titleTop"))
            make.left.equalTo(8)
            make.right.equalTo(-8)
            make.height.greaterThanOrEqualTo(config.int(forKey: "titleHeight"))
        }

        size = self.validImageSize(self.popupBackgroundImageView.frame.size, maxWidth: self.popupWidth())
        size.height = CGFloat(config.int(forKey: "bgTopHeight"))
        self.popupBackgroundImageView.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.height.equalTo(size.height)
            make.left.equalTo(0)
            make.right.equalTo(0)
        }


        self.viewContainerMoney.snp.makeConstraints { (make) in
            make.left.equalTo(config.int(forKey: "labelMoneyLeft"))
            make.right.equalTo(-config.int(forKey: "labelMoneyLeft"))
            make.height.equalTo(65)
            make.top.equalTo(self.labelTitle.snp.bottom).offset(config.int(forKey: "labelMoneyTop"))
        }

        self.labelMoney.snp.makeConstraints { (make) in
            make.left.equalTo(8)
            make.right.equalTo(-8)
            make.bottom.equalTo(-8)
            make.top.equalTo(8)
        }

        size = self.validImageSize(self.popupBackgroundImageViewBottom.frame.size, maxWidth: self.popupWidth())
        self.popupBackgroundImageViewBottom.snp.makeConstraints { (make) in
            make.top.equalTo(self.popupBackgroundImageView.snp.bottom).offset(-1)
            make.bottom.equalTo(0)
            make.left.equalTo(0)
            make.right.equalTo(0)
        }

        self.labelDescription.snp.makeConstraints { (make) in
            make.top.equalTo(self.popupBackgroundImageViewBottom.snp.top).offset(config.int(forKey: "descriptionTop"))
            make.height.equalTo(config.int(forKey: "descriptionHeight"))
            make.left.equalTo(10)
            make.right.equalTo(-10)
        }

        self.acceptButton.snp.makeConstraints { (make) in
            make.top.equalTo(self.labelDescription.snp.bottom).offset(config.int(forKey: "acceptButtonTop"))
            make.centerX.equalToSuperview()
            make.width.equalTo(200)
            make.height.equalTo(44)
        }

    }
    func getMoneyAttribute(withValue balanceValue: UInt64) -> NSMutableAttributedString {
        var text: String = ("\(balanceValue)" as NSString).formatCurrency() ?? ""
        let vndLength: Int = 4
        let range = text.count > vndLength ? NSRange(location: text.count - vndLength, length: vndLength) : NSRange(location: 0, length: 0)
        if text.count == 0 {
            text = "\(balanceValue)"
        }
        let fontSize: CGFloat = UIView.isScreen2x() ? 40 : 45
        let vndSize: CGFloat = UIView.isScreen2x() ? 16 : 18
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 1.0
        style.alignment = .center

        let rangeText = NSRange(location: 0, length: text.count)
        let attributedText = NSMutableAttributedString(string: text)
        attributedText.addAttribute(NSAttributedStringKey.paragraphStyle, value: style, range: rangeText)
        attributedText.addAttribute(NSAttributedStringKey.font, value: UIFont.sfuiTextSemibold(withSize: fontSize), range: rangeText)
        attributedText.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.white, range: rangeText)

        if range.length > 0 {
            attributedText.addAttribute(NSAttributedStringKey.font, value: UIFont.sfuiTextRegular(withSize: vndSize), range: range)
        }
        return attributedText
    }

    func getMoneyPercent(withValue percent: Int) -> NSMutableAttributedString {
        let text: String = "\(percent)%"
        let percentLength: Int = 1
        let range = text.count > percentLength ? NSRange(location: text.count - percentLength, length: percentLength) : NSRange(location: 0, length: 0)

        let fontSize: CGFloat = UIView.isScreen2x() ? 40 : 45
        let vndSize: CGFloat = UIView.isScreen2x() ? 18 : 20
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 1.0
        style.alignment = .center
        let fontBig = UIFont.sfuiTextSemibold(withSize: fontSize) ?? UIFont.systemFont(ofSize: 15)

        let rangeText = NSRange(location: 0, length: text.count)
        let attributedText = NSMutableAttributedString(string: text)
        attributedText.addAttribute(NSAttributedStringKey.paragraphStyle, value: style, range: rangeText)
        attributedText.addAttribute(NSAttributedStringKey.font, value: fontBig, range: rangeText)
        attributedText.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.white, range: rangeText)

        if range.length > 0 {
            let font = UIFont.sfuiTextSemibold(withSize: vndSize) ?? UIFont.systemFont(ofSize: 15)
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.paragraphSpacing = 0.25 * font.lineHeight

            let offsetAmount = fontBig.capHeight - font.capHeight
            attributedText.addAttribute(NSAttributedStringKey.font, value: font, range: range)
            attributedText.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: range)
            attributedText.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: range)
        }
        return attributedText
    }
}
