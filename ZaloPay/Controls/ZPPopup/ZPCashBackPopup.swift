//
//  ZPCashBackPopupSwift.swift
//  ZaloPay
//
//  Created by vuongvv on 1/15/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit

fileprivate let kType = "type"
fileprivate let kTitle = "title"
fileprivate let kAmount = "amount"
fileprivate let kCampaign = "campaign"
fileprivate let kTheme = "theme"
fileprivate let kActions = "actions"
fileprivate let kAction = "action"
fileprivate let kActionTitle = "title"

fileprivate let kImgHeader = "cashback_top"
fileprivate let kImgBGTop = "cashback_background"

@objcMembers
class ZPCashbackData: ZPPopupData {
    var type: Int = 0
    var title = ""
    var amount: UInt64 = 0
    var campaign = ""
    var action: Int = 0
    var actionTitle = ""
    var theme: Int = 0
    override init() {
        super.init()
    }
    init(_ dic: Dictionary<AnyHashable, Any>) {
        self.type = dic.value(forKey: kType, defaultValue: 0)
        self.title = dic.value(forKey: kTitle, defaultValue: "")
        self.amount = dic.value(forKey: kAmount, defaultValue: UInt64(0))
        self.campaign = dic.value(forKey: kCampaign, defaultValue: "")
        self.theme = dic.value(forKey: kTheme, defaultValue: 1)
        let actions = dic.value(forKey: kActions, defaultValue: [Dictionary<AnyHashable, Any>]())
        if actions.count > 0 {
            let actionFirst = actions.first!
            self.action = actionFirst.value(forKey: kAction, defaultValue: 0)
            self.actionTitle = actionFirst.value(forKey: kActionTitle, defaultValue: "")
        }
    }

    static func test() -> ZPCashbackData {
        let str = "{\"type\":1,\"title\":\"BẠN ĐƯỢC HOÀN TIỀN\",\"amount\":2000,\"campaign\":\"Từ CT Ngày Vàng ZaloPay Nạp ĐT - Hoàn tiền may mắn lên đến 500k\",\"actions\":[{\"action\":1,\"title\":\"Chi tiết giao dịch\"}],\"theme\":2}"
        let dict = NSDictionary.convertToDictionary(text: str)
        return ZPCashbackData(dict!)
    }

}

@objcMembers
class ZPCashBackPopup: ZPPopup{
    var cashbackData = ZPCashbackData()
    var popupHeaderView = UIImageView()
    var popupContentView = UIView()

    //Sub
    var labelMoney = UILabel()
    var labelTitle = UILabel()
    var labelDescription = MDHTMLLabelCustom()
    var labelDetail = MDHTMLLabelCustom()
    var detailButton = UIButton()
    var popupBackgroundImageView = UIImageView()

    //Configs
    var config2x: [String: Any] = [
        "popupHeight": 359,
        "headerTopOffset": 20,
        "titleTop": 30,
        "titleHeight": 18,
        "descriptionHeight": 60,
        "descriptionTop": -20,
        "labelMoneyLeft": 20,
        "detailButtonTop": 20,
        "detailLabelTop": 20
    ]
    var config3x: [String: Any] = [
        "popupHeight": 414,
        "headerTopOffset": 24,
        "titleTop": 34,
        "titleHeight": 20,
        "descriptionHeight": 66,
        "descriptionTop": 0,
        "labelMoneyLeft": 20,
        "detailButtonTop": 30,
        "detailLabelTop": 30
    ]
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setupView() {

        //Body
        self.popupContentView = UIView()
        self.popupContentView.backgroundColor = UIColor.white
        self.addSubview(self.popupContentView)
        self.sendSubview(toBack: self.popupContentView)

        let fontSize: CGFloat = UIView.isScreen2x() ? 15 : 17
        self.labelTitle = UILabel()
        self.labelTitle.textColor = UIColor.zp_red()
        self.labelTitle.textAlignment = .center
        self.labelTitle.font = UIFont.sfuiTextMedium(withSize: fontSize)

        self.popupBackgroundImageView = UIImageView(image: UIImage.fromInternalAppThemeCashBack(kImgBGTop, andThemeId: self.cashbackData.theme))

        self.labelMoney = UILabel()
        self.labelMoney.textAlignment = .center

        self.detailButton = UIButton(type: .custom)
        self.detailButton.setTitle("", for: UIControlState.normal)
        self.detailButton.setTitleColor(UIColor.white, for: UIControlState.normal)
        self.detailButton.titleLabel?.font = UIFont.sfuiTextSemibold(withSize: 17)
        self.detailButton.setBackgroundColor(UIColor.fromInternalAppThemeCashBack(theme: self.cashbackData.theme), for: UIControlState.normal)
        self.detailButton.roundRect(22)
        self.detailButton.addTarget(self, action: #selector(self.detailButtonClick), for: UIControlEvents.touchUpInside)

        self.labelDescription = MDHTMLLabelCustom()
        self.labelDescription.textColor = UIColor.defaultText()
        self.labelDescription.numberOfLines = 0
        self.labelDescription.font = UIFont.sfuiTextRegular(withSize: fontSize)
        self.labelDescription.textAlignment = .center

        self.labelDetail = MDHTMLLabelCustom()
        self.labelDetail.delegate = self
        self.labelDetail.textColor = UIColor.zaloBase()
        self.labelDetail.numberOfLines = 0
        self.labelDetail.font = UIFont.sfuiTextRegular(withSize: fontSize)
        self.labelDetail.textAlignment = .center

        self.popupContentView.addSubview(self.labelTitle)
        self.popupContentView.addSubview(self.popupBackgroundImageView)
        self.popupContentView.addSubview(self.labelMoney)
        self.popupContentView.addSubview(self.labelDescription)
        self.popupContentView.addSubview(self.detailButton)
        self.popupContentView.addSubview(self.labelDetail)

        //Header
        self.popupHeaderView = UIImageView(image: UIImage.fromInternalAppThemeCashBack(kImgHeader, andThemeId: self.cashbackData.theme))
        self.addSubview(self.popupHeaderView)

        self.setupViewConstrain()
        self.addTarget(self, action: #selector(self.hide), for: UIControlEvents.touchDown)
        self.configToast()
    }

    func setupViewConstrain() {
        let config = UIView.isScreen2x() ? self.config2x : self.config3x
        self.popupContentView.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.bottom.equalTo(0)
            make.height.equalTo(config.int(forKey: "popupHeight"))
        }

        var size = self.validImageSize(self.popupHeaderView.frame.size, maxWidth: self.popupWidth())
        self.popupHeaderView.snp.makeConstraints { (make) in
            make.size.equalTo(size)
            make.centerX.equalToSuperview()
            make.bottom.equalTo(self.popupContentView.snp.top).offset(config.int(forKey: "headerTopOffset"))
        }

        self.labelTitle.snp.makeConstraints { (make) in
            make.top.equalTo(config.int(forKey: "titleTop"))
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(config.int(forKey: "titleHeight"))
        }

        size = self.validImageSize(self.popupBackgroundImageView.frame.size, maxWidth: self.popupWidth())
        self.popupBackgroundImageView.snp.makeConstraints { (make) in
            make.top.equalTo(self.labelTitle.snp.bottom)
            make.centerX.equalToSuperview()
            make.size.equalTo(size)
        }

        self.labelMoney.snp.makeConstraints { (make) in
            make.left.equalTo(config.int(forKey: "labelMoneyLeft"))
            make.right.equalTo(-config.int(forKey: "labelMoneyLeft"))
            make.height.equalTo(81)
            make.centerY.equalTo(self.popupBackgroundImageView.snp.centerY)
        }

        self.labelDescription.snp.makeConstraints { (make) in
            make.top.equalTo(self.popupBackgroundImageView.snp.bottom).offset(config.int(forKey: "descriptionTop"))
            make.height.equalTo(config.int(forKey: "descriptionHeight"))
            make.left.equalTo(10)
            make.right.equalTo(-10)
        }
        self.detailButton.snp.makeConstraints { (make) in
            make.top.equalTo(self.labelDescription.snp.bottom).offset(config.int(forKey: "detailButtonTop"))
            make.centerX.equalToSuperview()
            make.width.equalTo(180)
            make.height.equalTo(44)
        }

        self.labelDetail.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.right.equalTo(-10)
            make.top.equalTo(self.detailButton.snp.bottom).offset(config.int(forKey: "detailLabelTop"))
            make.centerX.equalToSuperview()
        }
    }    
    override func setData(data: ZPPopupData) {
        guard let dataGet = data as? ZPCashbackData else {
            return
        }
        self.cashbackData = dataGet
        self.setupView()

        self.labelMoney.attributedText = self.getMoneyAttribute(withValue: self.cashbackData.amount)
        self.labelDescription.htmlText =  self.cashbackData.campaign
        self.labelDetail.htmlText = "<a href='zp-app://detail'><u>Chia sẻ</u></a>"
        self.labelTitle.text = self.cashbackData.title.uppercased()
        self.detailButton.setTitle( self.cashbackData.actionTitle, for: UIControlState.normal)
    }
    func captureScreen() -> UIImage? {
        guard let keyWindow = UIApplication.shared.keyWindow else {
            return nil
        }
        let rect = keyWindow.bounds
        UIGraphicsBeginImageContext(rect.size)
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        keyWindow.layer.render(in: context)
        guard let img = UIGraphicsGetImageFromCurrentImageContext() else {
            return nil
        }
        UIGraphicsEndImageContext()
        return img
    }

    func shareSocial() {
        let message = self.cashbackData.campaign
        guard let image = self.captureScreen(), let link = NSURL(string: "https://zalopay.vn") else {
            return
        }
        let share = UIActivityViewController(activityItems: [message, link, image], applicationActivities: nil)
        share.popoverPresentationController?.permittedArrowDirections = .right
        share.excludedActivityTypes = [UIActivityType.saveToCameraRoll]
        ZPAppFactory.topMostController().present(share, animated: true, completion: nil)
    }
    func getMoneyAttribute(withValue balanceValue: UInt64) -> NSMutableAttributedString {
        var text: String = ("\(balanceValue)" as NSString).formatCurrency() ?? ""
        let vndLength: Int = 4
        let range = text.count > vndLength ? NSRange(location: text.count - vndLength, length: vndLength) : NSRange(location: 0, length: 0)
        if text.count == 0 {
            text = "\(balanceValue)"
        }
        let fontSize: CGFloat = UIView.isScreen2x() ? 50 : 60
        let vndSize: CGFloat = UIView.isScreen2x() ? 20 : 25
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 1.0
        style.alignment = .center

        let rangeText = NSRange(location: 0, length: text.count)
        let attributedText = NSMutableAttributedString(string: text)
        attributedText.addAttribute(NSAttributedStringKey.paragraphStyle, value: style, range: rangeText)
        attributedText.addAttribute(NSAttributedStringKey.font, value: UIFont.sfuiTextSemibold(withSize: fontSize), range: rangeText)
        attributedText.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.hex_0xff5239(), range: rangeText)

        if range.length > 0 {
            attributedText.addAttribute(NSAttributedStringKey.font, value: UIFont.sfuiTextRegular(withSize: vndSize), range: range)
        }
        return attributedText
    }
}

extension ZPCashBackPopup: MDHTMLLabelDelegate {
    func htmlLabel(_ label: MDHTMLLabel!, didSelectLinkWith URL: URL!) {
        self.shareSocial()
        self.hide()
    }
}

