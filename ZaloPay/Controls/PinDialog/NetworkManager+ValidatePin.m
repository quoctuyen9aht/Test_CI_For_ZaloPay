//
//  NetworkManager+ValidatePin.m
//  ZaloPay
//
//  Created by PhucPv on 9/9/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "NetworkManager+ValidatePin.h"
#import <ZaloPayCommon/NSString+sha256.h>
#import <ZaloPayNetwork/ZPNetWorkApi.h>
@implementation NetworkManager(ValidatePin)
- (RACSignal *)validatePin:(NSString *)pin {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:pin forKey:@"pin"];
    return [self requestWithPath:api_um_validatepin parameters:params];
}
@end
