//
//  NetworkManager+ValidatePin.h
//  ZaloPay
//
//  Created by PhucPv on 9/9/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <ZaloPayNetwork/NetworkManager.h>

@interface NetworkManager(ValidatePin)
- (RACSignal *)validatePin:(NSString *)pin;
@end
