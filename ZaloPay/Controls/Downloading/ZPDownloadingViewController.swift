//
//  ZPDownloadingViewController.swift
//  ZaloPay
//
//  Created by thi la on 12/8/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import SnapKit
import RxCocoa
import ZaloPayCommonSwift
import ZaloPayWeb

fileprivate struct ImageName {
    static let loading = "loading_cloud"
    static let error = "error_cloud"
}

fileprivate struct NibName {
    static let loadingView = "ZPLoadingView"
}

@objcMembers

class ZPDownloadingViewController: ReactModuleViewController {
    var loadingView: ZPLoadingView?
    var loadingHeaderView: ZPLoadingHeaderView?
    var progressView: ZPWProgressView?

    private lazy var bag = DisposeBag()
    
    override func setupReactModule() {
        let model = ReactNativeAppManager.sharedInstance().getApp(self.appId)
        if model.appState == ReactNativeAppStateReadyToUse {
            self.loadReactView()
            return
        }
        loadingView = createLoadingView()
        loadingHeaderView = createHeaderView()
        progressView = setupProgressView()
        self.setupBackButton()
        loadingHeaderView?.titleLabel.text = model.appname ?? ""
        observeResource(model)
        model.appState == ReactNativeAppStateDownloadError ? loadingView?.loadingError() : loadingView?.startLoading()
    }
    
    func observeResource(_ model: ReactAppModel) {
        let until = self.rx.deallocated
        _ = model.rx.observe(ReactNativeAppState.self, "appState").takeUntil(until).throttle(0.5, scheduler: MainScheduler.instance).observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] (appState) in
                if appState == ReactNativeAppStateReadyToUse {
                    self?.doneDownloadReactApp()
                } else if appState == ReactNativeAppStateDownloadError {
                    self?.loadingView?.loadingError()
                    self?.progressView?.setProgress(0.0, animated: true)
                }
            })
        
        _ = model.rx.observe(Float.self, "percentDownloaded").takeUntil(until).observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] (value) in
                self?.progressView?.setProgress(value ?? 0, animated: true)
            })
    }
    
    func retryDownloadResource(_ sender: UIButton) {
        loadingView?.startLoading()
        loadingHeaderView?.isHidden = false
        let appModel = ReactNativeAppManager.sharedInstance().getApp(self.appId)
            // không cần xử lý ở đây: lý do đã observer trạng thái của model bên trên.
        ReactNativeAppManager.sharedInstance().downloadApp(appModel, progress: nil).deliverOnMainThread().subscribeNext({  (_) in })
    }
    
    func createLoadingView() -> ZPLoadingView? {
        if let view = Bundle.main.loadNibNamed(NibName.loadingView, owner: self, options: nil)?.first as? ZPLoadingView {
            view.retryDownload.addTarget(self, action: #selector(self.retryDownloadResource(_ :)), for: .touchUpInside)
            self.view.addSubview(view)
            view.snp.makeConstraints { (make) in
                make.centerX.centerY.equalTo(self.view)
                make.size.equalTo(self.view.frame.size)
            }
            return view
        }
        return nil;
    }
    
    func createHeaderView() -> ZPLoadingHeaderView {
        let header = ZPLoadingHeaderView.creatHeadView()
        self.view.addSubview(header)
        let height = UINavigationController.navigationBarHeight()
        header.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.height.equalTo(height)
            make.left.equalTo(0)
            make.right.equalTo(0)
        }
        return header
    }  
    
    func setupProgressView() -> ZPWProgressView {
        let progress = ZPWProgressView(frame: CGRect.zero, color: UIColor.zp_green())
        self.view.addSubview(progress)
        progress.snp.makeConstraints { (make) in
            make.top.equalTo(self.loadingHeaderView?.snp.bottom ?? 64)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(2)
        }
        progress.isHidden = false
        return progress
    }
    func setupBackButton() {
        let btnBack = UIButton(type: .custom)
        btnBack.frame = CGRect(origin: .zero, size: CGSize(width: 40, height: 40))
        btnBack.setIconFont("general_backios", for: .normal)
        btnBack.titleLabel?.font = UIFont.iconFont(withSize: 20)
        btnBack.setTitleColor(.white, for: .normal)
        btnBack.rx.tap.bind {[weak self] in
            self?.backButtonClicked(nil)
        }.disposed(by: bag)
        loadingHeaderView?.addSubview(btnBack)
        if let headerView = loadingHeaderView {
            btnBack.snp.makeConstraints { make in
                make.centerY.equalTo(headerView.titleLabel.snp.centerY)
                make.left.equalTo(5)
            }
        }
    
    }
    
    func loadReactView() {
        if self.bridge == nil {
            self.bridge = RCTBridge.externalBridge(self.appId)
        }
        super.setupReactModule()
    }
    
    func doneDownloadReactApp() {
        self.loadingView?.endLoading()
        loadingView?.isHidden = true
        loadingHeaderView?.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.loadReactView()
    }
}

class ZPLoadingView: UIView {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var message: UILabel!
    var pinView: DGActivityIndicatorView!
    @IBOutlet weak var retryDownload: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        message.zpMainGrayRegular()
        retryDownload.setupZaloPay()
        retryDownload.layer.cornerRadius = 22
        retryDownload.setTitle(R.string_HomeReactNative_DownloadAgain(), for:.normal)
        imageView.image = UIImage(named: ImageName.loading)
        message.text = R.string_Download_Resources_Loading()
        
        self.pinView = DGActivityIndicatorView(type: .ballPulse, tintColor: UIColor.subText())
        self.pinView.bounds = CGRect(origin: .zero, size: CGSize(width: 36, height: 10))
        self.addSubview(self.pinView)
        self.pinView.snp.makeConstraints { (make) in
            make.top.equalTo(message.snp.bottom).offset(26)
            make.centerX.equalTo(self)
            make.width.equalTo(36)
            make.height.equalTo(10)
        }
    }
    
    func startLoading() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
            self?.pinView.startAnimating()
        }
        imageView.image = UIImage(named: ImageName.loading)
        retryDownload.isHidden = true
        message.text = R.string_Download_Resources_Loading()
    }
    
    func endLoading() {
        pinView.stopAnimating()
    }
    
    func loadingError() {
        pinView.stopAnimating()
        imageView.image = UIImage(named: ImageName.error)
        retryDownload.isHidden = false
        message.text = R.string_Download_Resources_Error()
    }
}

class ZPLoadingHeaderView : UIView {
    public lazy var titleLabel: UILabel = {
       let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.white
        label.font = UIFont.sfuiTextMedium(withSize: 18)
        label.textAlignment = .center
        self.addSubview(label)
        label.snp.makeConstraints({ (make) in
            make.bottom.equalTo(0)
            make.height.equalTo(44)
            make.left.equalTo(0)
            make.right.equalTo(0)
        })
       return label
    }()
    
    class func creatHeadView() ->ZPLoadingHeaderView {
        let header = ZPLoadingHeaderView()
        header.backgroundColor = UIColor.zaloBase()
        return header
    }
}
