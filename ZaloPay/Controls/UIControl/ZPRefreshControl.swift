//
//  ZPRefreshControl.swift
//  ZaloPay
//
//  Created by tridm2 on 1/23/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

fileprivate class ZPRefreshControlConfig {
    let timeRotation = 0.75
}

@objcMembers
class ZPRefreshControl: UIRefreshControl {
    
    var loadingIcon: UIImageView!
    private weak var scrollView: UIScrollView?
    private let disposebag = DisposeBag()
    private var startDegree: Float = 0
    private let config = ZPRefreshControlConfig()
    private var isDoneRefresh: Bool = true
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    convenience init(with scrollView: UIScrollView?) {
        self.init(frame: .zero)
        self.scrollView = scrollView
        self.scrollView?.rx.contentOffset.filter({ $0.y <= 0 }).bind(onNext: { [weak self] (point) in
            guard let wSelf = self, (wSelf.bounds == .zero).not, wSelf.isRefreshing.not else {
                return
            }
            
            if point.y == 0 {
                wSelf.isDoneRefresh = true
            }
            
            wSelf.loadingIcon.layer.removeAllAnimations()
            wSelf.rotateLoadingIcon(offset: -point.y)
        }).disposed(by: disposebag)
    }
    
    private func rotateLoadingIcon(offset: CGFloat) {
        let height = self.bounds.height
        let ratio = offset / height
        self.loadingIcon.alpha = ratio < 0.7 ? max(ratio - 0.3, 0) : ratio
        self.startDegree = Float(ratio) * 2 * Float.pi
        self.loadingIcon.transform = CGAffineTransform.init(rotationAngle: CGFloat(self.startDegree))
    }
    
    private func removeDefaultLoadingIcon() {
        self.tintColor = .clear
    }
    
    private func setZPLoadingIcon() {
        loadingIcon = UIImageView()
        self.addSubview(loadingIcon)
        loadingIcon.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        self.loadingIcon.tintColor = UIColor.zaloBase()
        loadingIcon.image = #imageLiteral(resourceName: "refresh")
    }
    
    private func setup() {
        removeDefaultLoadingIcon();
        setZPLoadingIcon()
    }
    
    private func addAnimationForLoadingIcon() {
        let anim = CABasicAnimation(keyPath: "transform.rotation.z")
        anim.fromValue = 0
        anim.toValue = 2 * Float.pi
        anim.duration = config.timeRotation
        anim.repeatCount = Float.infinity
        loadingIcon.layer.add(anim, forKey: nil)
    }
    
    private func runLoadingIconAnimation() {
        self.loadingIcon.alpha = 1
        self.loadingIcon.layer.removeAllAnimations()
        self.loadingIcon.transform = .identity
        addAnimationForLoadingIcon()
    }
    
    func resetAnimation() {
        if isDoneRefresh.not {
            let point = self.scrollView?.contentOffset.with{ $0.y = -self.bounds.height } ?? CGPoint.zero
            self.scrollView?.setContentOffset(point, animated: true)
        }
        runLoadingIconAnimation()
    }
    
    override func beginRefreshing() {
        super.beginRefreshing()
        runLoadingIconAnimation()
        isDoneRefresh = false
    }

}
