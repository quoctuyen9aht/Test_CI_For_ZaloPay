//
//  ZPGiftToastView.swift
//  ZaloPay
//
//  Created by Dung Vu on 2/5/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

@objcMembers
class ZPGiftToastConfig: NSObject {
    static let `default` = ZPGiftToastConfig()
    
    var defaultTopMessage: CGFloat = 24
    var defaultTopNumber: CGFloat = 20
    var numberOfLines: Int = 0
    var hMinimum: CGFloat = 66
    var fontMessage: UIFont = UIFont.sfuiTextRegular(withSize: 16)
    var fontNumberIcon: UIFont = UIFont.sfuiTextMedium(withSize: 18)
    var backgroundMessage: UIColor = #colorLiteral(red: 1, green: 1, blue: 0.9411764706, alpha: 1)
    var borderColor: UIColor = #colorLiteral(red: 0.9254901961, green: 0.7568627451, blue: 0.4941176471, alpha: 1)
    var textColor: UIColor = #colorLiteral(red: 0.1411764706, green: 0.1529411765, blue: 0.168627451, alpha: 1)
    var textNumberColor: UIColor = .white
    var backgroundTextNumber: UIColor = #colorLiteral(red: 1, green: 0.231372549, blue: 0.1882352941, alpha: 1)
}

@objcMembers
class ZPGiftToastView: UIView {
    // Using for set message
    var message: String = "" {
        didSet{
            self.messageLbl.text = message
            layoutIfNeeded()
        }
    }
    
    // Using for number icon
    var numberGift: String = "" {
        didSet{
            self.iconLbl.isHidden = numberGift.count == 0
            self.iconLbl.setTitle(numberGift, for: .normal)
            layoutIfNeeded()
        }
    }
    
    private lazy var containerView: UIView = {
        let nView = UIView(frame: .zero)
        nView.layer.borderWidth = 1
        nView.layer.borderColor = self.config.borderColor.cgColor
        nView.backgroundColor = self.config.backgroundMessage
        self.addSubview(nView)
        return nView
    }()
    
    private lazy var messageLbl: UILabel = {
        let label = UILabel(frame: .zero).then {
            $0.numberOfLines = self.config.numberOfLines
            $0.font = self.config.fontMessage
            $0.textColor = self.config.textColor
        }
        self.containerView.addSubview(label)
        return label
    }()
    
    private lazy var iconLbl: UIButton = {
        let label = UIButton(type: .custom).then {
            $0.contentEdgeInsets = UIEdgeInsetsMake(4, 4, 4, 4)
            $0.titleLabel?.font = self.config.fontNumberIcon
            $0.setTitleColor(self.config.textNumberColor, for: .normal)
            $0.backgroundColor = self.config.backgroundTextNumber
            $0.clipsToBounds = true
        }
        self.addSubview(label)
        return label
    }()
    
    private var completion: (() -> ())?
    private var showMore: (() -> ())?
    private var config: ZPGiftToastConfig!
    private weak var parentView: UIView?
    private let disposeBag = DisposeBag()
    private lazy var isIPhoneX: Bool = {
        return UIScreen.main.bounds.height == 812
    }()
    
    private lazy var delta: CGFloat = {
        return self.isIPhoneX ? 20 : 0
    }()
    
    convenience init(with config: ZPGiftToastConfig = .default,
                     on view: UIView,
                     tap showMore:(() -> ())?,
                     handler completion:(() -> ())? = nil) {
        self.init(frame: .zero)
        self.backgroundColor = .clear
        self.config = config
        self.parentView = view
        self.completion = completion
        self.showMore = showMore
        setupDisplay()
        setupEvent()
    }
    
    private func setupEvent() {
        self.iconLbl.rx.tap.bind { [weak self] in
            self?.showMore?()
        }.disposed(by: disposeBag)
    }
    
    private func setupDisplay() {
        guard let pView = parentView else {
            return
        }
        pView.addSubview(self)
        self.snp.makeConstraints { (m) in
            m.top.equalToSuperview()
            m.left.equalToSuperview()
            m.right.equalToSuperview()
            m.height.greaterThanOrEqualTo(0)
        }
        
        self.containerView.snp.makeConstraints { (m) in
            m.edges.equalToSuperview()
        }

        // Message label
        self.messageLbl.snp.makeConstraints { (m) in
            m.top.equalTo(self.config.defaultTopMessage + self.delta)
            m.left.equalTo(12)
            m.width.lessThanOrEqualTo(250)
            m.height.greaterThanOrEqualTo(19)
        }
        
        // Number label
        self.iconLbl.snp.makeConstraints { (m) in
            m.top.equalTo(self.config.defaultTopNumber + self.delta)
            m.right.equalTo(-12)
        }
        self.iconLbl.titleLabel?.textAlignment = .center
        self.containerView.transform = CGAffineTransform(translationX: 0, y: -500)
        self.containerView.isUserInteractionEnabled = false
        self.containerView.isHidden = true
        self.numberGift = ""
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        guard superview != nil else {
            return
        }
        self.containerView.snp.updateConstraints { (m) in
            m.height.equalTo(self.messageLbl.snp.height).offset(self.config.defaultTopMessage * 2 + self.delta).priority(.medium)
        }
    }
    func show() {
        self.containerView.isHidden = false
        self.iconLbl.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
       
        UIView.animate(withDuration: 0.3, delay: 0, options: .allowUserInteraction, animations: { [weak self] in
            self?.iconLbl.transform = CGAffineTransform.identity
        }, completion: nil)
        
        UIView.animateKeyframes(withDuration: 3, delay: 0, options: [.beginFromCurrentState,.allowUserInteraction], animations: {[weak self] in
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.03, animations: {
                self?.containerView.transform = CGAffineTransform.identity
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.9, relativeDuration: 0.3, animations: {
                self?.containerView.transform = CGAffineTransform(translationX: 0, y: -500)
            })
        }) {[weak self](c ) in
            if c {
                self?.containerView.isHidden = true
                self?.completion?()
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let r = self.iconLbl.bounds
        let maxS = max(r.height, r.width)
        var newR = r
        newR.size = CGSize(width: maxS, height: maxS)
        self.iconLbl.bounds = newR
        self.iconLbl.layer.cornerRadius = maxS / 2
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let r = self.convert(point, to: self.iconLbl)
        if self.iconLbl.bounds.contains(r).not {
            return nil
        }
        return super.hitTest(point, with: event)
    }
}
