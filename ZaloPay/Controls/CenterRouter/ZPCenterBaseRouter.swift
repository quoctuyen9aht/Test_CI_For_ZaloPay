//
//  ZPCenterBaseRouter.swift
//  ZaloPay
//
//  Created by tridm2 on 1/16/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation

class ZPCenterBaseRouter : ZPRouterProtocol {
    required init() {}
    
    func show(from controller: UIViewController?, animated: Bool,options:ZPCenterRouterOptions? = nil) {
        let vc = options != nil ? createVC(options:options): createVC()
        if let c = controller {
            c.navigationController?.pushViewController(vc, animated: animated)
            return
        }
        UINavigationController.currentActiveNavigationController()?.pushViewController(vc, animated: animated)
    }
    
    func createVC() -> UIViewController {
        return UIViewController()
    }
    func createVC(options:ZPCenterRouterOptions?) -> UIViewController {
        return UIViewController()
    }
}
