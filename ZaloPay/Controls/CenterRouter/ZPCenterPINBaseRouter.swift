//
//  ZPCenterPINBaseRouter.swift
//  ZaloPay
//
//  Created by tridm2 on 1/16/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation

class ZPCenterPINBaseRouter : ZPCenterBaseRouter {
    override func show(from controller: UIViewController?, animated: Bool,options:ZPCenterRouterOptions? = nil) {
        if shouldAskPIN().not {
            super.show(from: controller, animated: animated)
            return
        }
        
        let msgInputPIN = getMsgInputPIN()
        let isForceInput = isForceInputPIN()
        ZPAppFactory.sharedInstance().showPinDialogMessage(
            msgInputPIN, isForceInput: isForceInput,
            success: { [weak controller, animated] in
                super.show(from: controller, animated: animated)
            }, error: nil
        )
    }
    
    func shouldAskPIN() -> Bool {
        return true
    }
    
    func isForceInputPIN() -> Bool {
        return false
    }
    
    func getMsgInputPIN() -> String {
        return ""
    }
}
