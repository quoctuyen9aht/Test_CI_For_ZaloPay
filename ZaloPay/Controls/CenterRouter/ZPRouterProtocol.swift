//
//  ZPRouterProtocol.swift
//  ZaloPay
//
//  Created by tridm2 on 12/18/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation

protocol ZPRouterProtocol: class {
    init()
    // Nếu param controller là nil thì sẽ show từ navigation controller hiện tại
    func show(from controller: UIViewController?, animated: Bool,options:ZPCenterRouterOptions?)
}
