//
//  ZPCenterRouter.swift
//  ZaloPay
//
//  Created by tridm2 on 12/14/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayAnalyticsSwift
import ZaloPayCommonSwift

@objc enum RouterId: Int {
    case setting            = 1
    case userProfile        = 2
    case wallet             = 3
    case qrScan             = 4
    case receiveMoney       = 5
    case transferMoney      = 6
    case linkCard           = 7
    case paymentCode        = 8
    case recharge           = 9
    case voucher            = 10
    case withdraw           = 11
}

@objc class ZPCenterRouterOptions:NSObject {
    var QuickPayForceToQRCode = false
}
class ZPCenterRouter: NSObject {
    private static let routerMapping: [RouterId: ZPRouterProtocol.Type] = [
        .withdraw:      ZPCenterWithdrawRouter.self,
        .setting:       ZPCenterSettingRouter.self,
        .userProfile:   ZPCenterUserProfileRouter.self,
        .wallet:        ZPCenterWalletRouter.self,
        .qrScan:        ZPCenterQRScannerRouter.self,
        .receiveMoney:  ZPCenterReceiveMoneyRouter.self,
        .transferMoney: ZPCenterTransferMoneyRouter.self,
        .linkCard:      ZPCenterLinkCardRouter.self,
        .paymentCode:   ZPCenterPaymentCodeRouter.self,
        .recharge:      ZPCenterRechargeRouter.self,
        .voucher:       ZPCenterVoucherListRouter.self,
    ]
    
    @objc class func launch(routerId: RouterId, from controller: UIViewController?, animated: Bool = true) {
        if let router = ZPCenterRouter.routerMapping[routerId]  {
            router.init().show(from: controller, animated: animated, options: nil)
        }
    }
    
    @objc class func launchExternalApp(appId: Int, appType: ReactAppType, from controller: UIViewController?, moduleName: String? = nil, properties: [String:Any]? = nil) {
        guard let vc = controller ?? UINavigationController.currentActiveNavigationController() else {
            return
        }
        
        if appType == ReactAppTypeWeb {
            if appId == ZPConstants.appDichVuId {
                ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.home_touch_service)
            } else {
                ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.home_touch_id_app, value: appId as NSNumber)
            }
            ZPWebAppRouter.openWeb(withAppId: appId, viewController: vc)
            return
        }
        
        // ReactAppTypeNative
        let reactAppRouter = ZPReactNativeAppRouter()
        if let name = moduleName,
            var allProperties = ReactNativeAppManager.sharedInstance().reactProperties(appId) as? [String:Any] {
            if let p = properties {
                for (k,v) in p {
                    allProperties[k] = v
                }
            }
            let app = ReactNativeAppManager.sharedInstance().getApp(appId)
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.home_touch_id_app, value: appId as NSNumber)
            reactAppRouter.openApp(app, from: vc, moduleName: name, withProps: allProperties as NSDictionary)
            return
        }
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.home_touch_id_app, value: appId as NSNumber)
        reactAppRouter.openExternalApp(appId, from: vc)
    }
    @objc class func launch(routerId: RouterId, from controller: UIViewController?,animated: Bool = true, options:ZPCenterRouterOptions? = nil) {
        if let router = ZPCenterRouter.routerMapping[routerId]  {
            router.init().show(from: controller, animated: animated, options: options)
        }
    }
}
