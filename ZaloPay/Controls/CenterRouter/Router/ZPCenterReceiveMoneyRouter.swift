//
//  ZPCenterReceiveMoneyRouter.swift
//  ZaloPay
//
//  Created by tridm2 on 1/10/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation

class ZPCenterReceiveMoneyRouter : ZPCenterBaseRouter {
    override func createVC() -> UIViewController {
        return ZPReceiveMoneyRouter.assemblyModule()
    }
}
