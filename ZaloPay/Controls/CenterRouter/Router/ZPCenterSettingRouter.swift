//
//  ZPCenterSettingRouter.swift
//  ZaloPay
//
//  Created by tridm2 on 12/19/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation

class ZPCenterSettingRouter: ZPCenterBaseRouter {
    override func createVC() -> UIViewController {
        return ZPSettingRouter.assemblyModule()
    }
}
