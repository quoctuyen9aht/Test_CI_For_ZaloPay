//
//  ZPCenterLinkCardRouter.swift
//  ZaloPay
//
//  Created by tridm2 on 1/10/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation

class ZPCenterLinkCardRouter : ZPCenterPINBaseRouter {
    override func createVC() -> UIViewController {
        return ZPLinkCardViewControllerSwift()
    }
    
    override func shouldAskPIN() -> Bool {
        return ZPBankApiWrapperSwift.sharedInstance.shouldAskPIN()
    }
    
    override func getMsgInputPIN() -> String {
        return R.string_Message_Input_PIN_ManageCard()
    }
}
