//
//  ZPCenterWalletRouter.swift
//  ZaloPay
//
//  Created by tridm2 on 1/3/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation

class ZPCenterWalletRouter : ZPCenterBaseRouter {
    override func createVC() -> UIViewController {
        return ZPWalletActionRouter.assembleModule()
    }
}
