//
//  ZPCenterRechargerRouter.swift
//  ZaloPay
//
//  Created by Bon Bon on 3/1/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit

class ZPCenterRechargeRouter: ZPCenterBaseRouter {
    
    override func show(from controller: UIViewController?, animated: Bool,options:ZPCenterRouterOptions? = nil) {
        if totalCardAndBankAccount() > 0 {
            self.showRechargeView()
            return
        }
        guard let viewController = controller ?? UINavigationController.currentActiveNavigationController()?.topViewController else {
            return
        }
        // không dùng weak để giữ instance hiện tại đến khi complete hoặc error
        
        ZPBankApiWrapperSwift.sharedInstance.mapBank(with: viewController,
                                                  appId: kZaloPayClientAppId,
                                                  transType: .walletTopup).subscribeNext({ [weak viewController] (data) in
            viewController?.popMe(animated: false)
            self.showRechargeView()
        }) { [weak viewController] (error) in
            viewController?.popMe(animated: false)
        }
    }
           
    private func totalCardAndBankAccount() -> Int  {
        let allCard = ZaloPayWalletSDKPayment.sharedInstance().paymentSavedCard() ?? []
        let sCard = allCard
            .compactMap({ $0 as? ZPSavedCard })
            .filter({ ZPBankSupport.isCCCard($0.bankCode).not })
        let sBankAccount = ZaloPayWalletSDKPayment.sharedInstance().paymentSavedBankAccount() ?? []
        return sCard.count + sBankAccount.count
    }
    
    func showRechargeView() {
        let navi = UINavigationController.currentActiveNavigationController()
        let recharge = ZPRechargeRouter.assemblyModule()
        navi?.pushViewController(recharge, animated: true)
    }
}
