//
//  ZPCenterVoucherListRouter.swift
//  ZaloPay
//
//  Created by Bon Bon on 3/2/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit

class ZPCenterVoucherListRouter: ZPCenterBaseRouter {
    override func createVC() -> UIViewController {
        return ZPVoucherHistoryRouter.createZPVoucherHistoryModule()
    }
}
