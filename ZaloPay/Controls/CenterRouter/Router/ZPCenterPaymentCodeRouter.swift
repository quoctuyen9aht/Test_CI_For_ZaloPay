//
//  ZPCenterPaymentCodeRouter.swift
//  ZaloPay
//
//  Created by tridm2 on 1/15/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation

class ZPCenterPaymentCodeRouter : ZPCenterBaseRouter {
    override func createVC() -> UIViewController {
        return ZPPaymentCodeRouter.assemblyModule()
    }
    override func createVC(options:ZPCenterRouterOptions?) -> UIViewController {
        return ZPPaymentCodeRouter.assemblyModule(forceGoToGenQRCode:options?.QuickPayForceToQRCode ?? false)
    }
}
