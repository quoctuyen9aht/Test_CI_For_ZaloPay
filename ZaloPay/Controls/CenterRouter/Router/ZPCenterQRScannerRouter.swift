//
//  ZPCenterQRScannerRouter.swift
//  ZaloPay
//
//  Created by tridm2 on 1/9/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayConfig

class ZPCenterQRScannerRouter : ZPCenterBaseRouter {
    fileprivate let enableQuickPay = ZPApplicationConfig.getQuickPayConfig()?.getEnable().toBool() ?? false
    override func createVC() -> UIViewController {
        return enableQuickPay ? ZPQRPayPageViewController() : ZPQRPayRouter.createQRPayViewController()
    }
}
