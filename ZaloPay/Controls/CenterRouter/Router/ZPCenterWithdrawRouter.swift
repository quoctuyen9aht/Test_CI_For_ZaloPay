//
//  ZPCenterWithdrawRouter.swift
//  ZaloPay
//
//  Created by tridm2 on 12/14/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CocoaLumberjackSwift

class ZPCenterWithdrawRouter: ZPCenterBaseRouter {
    fileprivate let disposeBag = DisposeBag()
    
    fileprivate func eWithdraw(isApplyCondition: Bool, from controller: UIViewController?) -> Observable<UIViewController> {
        guard isApplyCondition.not else {
            return Observable.just(ZPWithdrawRouter.assembleModule())
        }
        
        return WithdrawConditionView.showCondition(on: controller).flatMap({ [weak controller](_) -> Observable<UIViewController> in
            controller?.popMe()
            return ZPCenterWithdrawRouter.showAlertNextAction().map({ (_) in
                return ZPWithdrawRouter.assembleModule()
            })
        })
    }
    
    fileprivate static func showAlertNextAction() -> Observable<Int> {
        let message = R.string_Withdraw_Description_Link_Success_Alert() ?? ""
        let boldStr = R.string_Withdraw_Description_Link_Bold_Title() ?? ""
        let att = NSMutableAttributedString(string: message,
                                            attributes: [NSAttributedStringKey.font: UIFont.sfuiTextRegular(withSize: 14),
                                                         NSAttributedStringKey.foregroundColor: UIColor.defaultText()])
        
        let range = (message as NSString).range(of: boldStr)
        if range.location != NSNotFound {
            att.addAttributes([NSAttributedStringKey.font: UIFont.sfuiTextSemibold(withSize: 14)], range: range)
        }
        
        return Observable.create({ (s) -> Disposable in
            ZPDialogView.showDialog(with: DialogTypeNotification,
                                    title: nil,
                                    attributeMessage: att,
                                    buttonTitles: [R.string_ButtonLabel_Next(),R.string_ButtonLabel_Later()])
            { (idx) in
                defer {
                    s.onCompleted()
                }
                guard (idx == 1).not else {
                    return
                }
                s.onNext(idx)
            }
            return Disposables.create()
        })
    }
    
    override func createVC() -> UIViewController {
        return ZPWithdrawRouter.assembleModule()
    }
    
    override func show(from controller: UIViewController?, animated: Bool,options:ZPCenterRouterOptions? = nil) {
        
        let viewController = controller ?? UINavigationController.currentActiveNavigationController()?.topViewController
        
        if (ZPWalletManagerSwift.sharedInstance.isMaintainWithdraw()) {
            ZPWalletManagerSwift.sharedInstance.alertMaintainWithdraw()
            return;
        }
        
        // Không dùng weak self để giữ lại disposeBag không bị huỷ
        // cho tới khi signal đã chạy xong theo cách bình thường (onCompleted hay onError được gọi)
        (~ZPWithdrawHelperSwift.isShowWithDraw()).map { (result) -> Bool in
            return result?.boolValue ?? false
        }.flatMap({ [weak viewController] result -> Observable<UIViewController> in
            return self.eWithdraw(isApplyCondition: result, from: viewController)
        }).subscribe(onNext: { [weak viewController, animated](vc) in
            super.show(from: viewController, animated: animated)
        }, onError: { [weak viewController](e) in
            viewController?.popMe()
        }).disposed(by: disposeBag)
    }
}
