//
//  ZPCenterProfileRouter.swift
//  ZaloPay
//
//  Created by tridm2 on 1/2/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayProfile

class ZPCenterUserProfileRouter : ZPCenterPINBaseRouter {
    override func createVC() -> UIViewController {
        // Show account information depends on profile level
        if let level = ZPProfileManager.shareInstance.userLoginData?.profilelevel {
            if level < ZPProfileLevel.level3.rawValue {
                // Show AccountInfo (account not yet verified)
                return AccountInfoViewController()
            }
        }
        // Show MainProfile (account verified)
        return ZPMainProfileViewController()
    }
    
    override func shouldAskPIN() -> Bool {
        return true
    }
    
    override func getMsgInputPIN() -> String {
        return R.string_Message_Input_PIN_Profile()
    }
}
