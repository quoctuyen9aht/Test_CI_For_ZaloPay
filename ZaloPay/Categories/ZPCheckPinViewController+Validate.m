//
//  ZPCheckPinViewController + Validate.m
//  ZaloPay
//
//  Created by Dung Vu on 8/18/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import "ZPCheckPinViewController+Validate.h"
#import <ZaloPayCommon/NSString+sha256.h>
#import "NetworkManager+ValidatePin.h"
#import <ZaloPayNetwork/NSError+ZaloPayAPI.h>
@implementation ZPCheckPinViewController (Validate)
+ (void)showWithMessage:(NSString *)message
          handleSuccess:(void (^)(NSString *pass))handle
            handleError:(void (^)(void))errorBlock {
    [self showCheckPin:message handleSuccess:handle handleError:errorBlock];
}

+ (void)showCheckPin:(NSString *)message
       handleSuccess:(void (^)(NSString *pass))handle
         handleError:(void (^)(void))errorBlock {
    
    UIViewController *root = [[UIApplication sharedApplication].delegate.window rootViewController];
    
    if (!root) {
        return;
    }
    __block NSString *p;
    [[[[ZPCheckPinViewController showOn:root
                                  using:message
                             methodName:nil
                            methodImage:nil suggestTouchId:NO] doNext:^(NSString *pin) {
        p = [pin sha256];
    }] flattenMap:^__kindof RACSignal * _Nullable(NSString * _Nullable pin)  {
        return [self vertify:pin];
    }] subscribeNext:^(NSNumber *value) {
        BOOL isOK = [value boolValue];
        if (isOK) {
            handle(p);
            [ZPCheckPinViewController dissmiss:YES];
        }else {
            p = nil;
        }
    } completed:^{
        if (!p && errorBlock) {
            errorBlock();
        }
        p = nil;
    }];
}

+ (RACSignal *) vertify:(NSString *)pin {
    [ZPCheckPinViewController startLoading];
    NSString *sha = [pin sha256];
    return [[[[[NetworkManager sharedInstance] validatePin:sha] deliverOnMainThread] flattenMap:^__kindof RACSignal * _Nullable(id  _Nullable value)  {
        return [RACSignal return:@(YES)];
    }] catch:^RACSignal *(NSError *error) {
        [ZPCheckPinViewController showErrorMessage:[error apiErrorMessage]];
        return [RACSignal return:@(NO)];
    }];
}


@end
