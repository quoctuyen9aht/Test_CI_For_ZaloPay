//
//  NSException+Extension.m
//  ZaloPay
//
//  Created by Dung Vu on 1/19/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import "NSException+Extension.h"
@implementation NSException(Extension)
+ (instancetype)tryBlock:(void (^)(void))block {
    @try {
        block();
    }
    @catch (NSException *exception) {
        return exception;
    }
    return nil;

}
@end
