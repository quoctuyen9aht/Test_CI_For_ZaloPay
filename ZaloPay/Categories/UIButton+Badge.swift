//
//  UIButton+Badge.swift
//  ZaloPay
//
//  Created by Dung Vu on 4/14/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

public protocol Then {}

extension Then where Self: Any {
    public func with(_ block: (inout Self) -> Void) -> Self {
        var copy = self
        block(&copy)
        return copy
    }
    public func `do`(_ block: (Self) -> Void) {
        block(self)
    }
    
}

extension Then where Self: AnyObject {
    public func then(_ block: (Self) -> Void) -> Self {
        block(self)
        return self
    }
    
}

extension NSObject: Then {}
extension CGPoint: Then {}
extension CGRect: Then {}
extension CGSize: Then {}
extension CGVector: Then {}
extension UIEdgeInsets: Then {}

fileprivate class BadgeDisplay {
    var badgeValue: String?
    var badgeBGColor: UIColor = .red
    var badgeTextColor: UIColor = .white
    var badgeFont: UIFont = UIFont.systemFont(ofSize: 12)
    var badgePaddingTop: CGFloat = 0
    var badgePaddingRight: CGFloat = 2
    var badgePaddingTextWithBG: CGFloat = 6
    var shouldHideBadgeAtZero: Bool = true
    var shouldAnimateBadge: Bool = true
}

fileprivate struct UIButtonBadge {
    static var Name = "UIButtonBadgeName"
}
fileprivate let kTagLabelBage = 65784
@objc protocol ShowBadgeProtocol: NSObjectProtocol {
    var badgePaddingTop: CGFloat { get set }
    var badgePaddingRight: CGFloat { get set }
    var badgePaddingTextWithBG: CGFloat { get set }
    var badgeTextColor: UIColor { get set }
    var badgeFont: UIFont { get set }
    var badgeValue: String? { get set }
    var shouldHideBadgeAtZero: Bool { get set }
    var shouldAnimateBadge: Bool { get set }
}


//extension UIView: ShowBadgeProtocol {}

extension UIView: ShowBadgeProtocol {
    fileprivate var badgeDisplay: BadgeDisplay {
        guard let bDisplay = objc_getAssociatedObject(self, &UIButtonBadge.Name) as? BadgeDisplay else {
           let nBadge = initBadgeDisplay()
            objc_setAssociatedObject(self, &UIButtonBadge.Name, nBadge, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            return nBadge
        }
        
        return bDisplay
    }
    
    /// Badge Label
    fileprivate var badge: UILabel {
        guard let lbl = self.viewWithTag(kTagLabelBage) as? UILabel else {
            return createBadgeLabel()
        }
        return lbl
    }
    
    fileprivate func initBadgeDisplay() -> BadgeDisplay {
        let nBadgeDisplay = BadgeDisplay()
        return nBadgeDisplay
    }
    
    fileprivate func createBadgeLabel() -> UILabel {
        let defaultSize = CGSize(width: 18, height: 18)
        let nLbl = UILabel(frame: CGRect(origin: .zero, size: defaultSize))
        nLbl.backgroundColor = badgeBGColor
        nLbl.textColor = badgeTextColor
        nLbl.font = badgeFont
        nLbl.tag = kTagLabelBage
        nLbl.textAlignment = .center
        nLbl.layer.cornerRadius = defaultSize.width / 2
        nLbl.clipsToBounds = true
        nLbl.isHidden = shouldHideBadgeAtZero
        self.addSubview(nLbl)
        nLbl.snp.makeConstraints {
            $0.top.equalTo(badgePaddingTop)
            $0.right.equalTo(badgePaddingRight)
            $0.size.equalTo(defaultSize)
        }
        return nLbl
    }
    
    // MARK: Public Properties
    
    /// Set Distance With Top
    var badgePaddingTop: CGFloat {
        get{
            return badgeDisplay.badgePaddingTop
        }
        set{
            badgeDisplay.badgePaddingTop = newValue
            updateLayoutBadgeLabel()
        }
    }
    
    /// Set Distance With Right
    var badgePaddingRight: CGFloat {
        get{
            return badgeDisplay.badgePaddingRight
        }
        set{
            badgeDisplay.badgePaddingRight = newValue
            updateLayoutBadgeLabel()
        }
    }
    /// Set Distance Text With Background
    var badgePaddingTextWithBG: CGFloat {
        get{
            return badgeDisplay.badgePaddingTextWithBG
        }
        set{
            badgeDisplay.badgePaddingTextWithBG = newValue
            updateLayoutBadgeLabel()
        }
    }
    
    /// Set Color Background For Badge
    var badgeBGColor: UIColor {
        get{
            return badgeDisplay.badgeBGColor
        }
        set{
            badgeDisplay.badgeBGColor = newValue
            updateDisplayBadgeLabel()
        }
    }
    
    /// Set Color Text For Badge
    var badgeTextColor: UIColor {
        get{
            return badgeDisplay.badgeTextColor
        }
        set{
            badgeDisplay.badgeTextColor = newValue
            updateDisplayBadgeLabel()
        }
    }
    
    /// Set Font For Badge
    var badgeFont: UIFont {
        get{
            return badgeDisplay.badgeFont
        }
        set{
            badgeDisplay.badgeFont = newValue
            updateDisplayBadgeLabel(true)
        }
    }
    
    /// Set Value To Badge
    var badgeValue: String? {
        get{
            return badgeDisplay.badgeValue
        }
        set{
            badgeDisplay.badgeValue = newValue
            badge.text = newValue
            updateLayoutBadgeLabel()
            animateBadgeLabel()
        }
    }
    
    /// Using Show Badge With No Value
    var shouldHideBadgeAtZero: Bool {
        get{
            return badgeDisplay.shouldHideBadgeAtZero
        }
        set{
            badgeDisplay.shouldHideBadgeAtZero = newValue
        }
    }
    
    /// Using Animate Badge
    var shouldAnimateBadge: Bool {
        get{
            return badgeDisplay.shouldAnimateBadge
        }
        set{
            badgeDisplay.shouldAnimateBadge = newValue
        }
    }
    
    // MARK: -- Update Font, Text color... for badget
    fileprivate func updateDisplayBadgeLabel(_ isNeedUpdateLayout: Bool = false) {
        self.badge.textColor = badgeTextColor
        self.badge.backgroundColor = badgeBGColor

        guard isNeedUpdateLayout else { return }
        self.badge.font = badgeFont
        updateLayoutBadgeLabel()
    }
    
    // MARK: -- Update Layout
    fileprivate func updateLayoutBadgeLabel() {
        let v = badge.text ?? ""
        let font = self.badgeFont
        let sizeTxt = v.size(maxHeight: font.lineHeight, using: font).with {
            $0.width += badgePaddingTextWithBG
            $0.height += badgePaddingTextWithBG
            let maxS = max($0.width, $0.height)
            $0 = CGSize(width: maxS, height: maxS)
        }
        self.badge.snp.updateConstraints({ $0.size.equalTo(sizeTxt) })
        self.badge.layer.cornerRadius = sizeTxt.width / 2
        self.badge.isHidden = false
    }
    
    // MARK: -- Animate
    fileprivate func animateBadgeLabel() {
        let v = badge.text ?? ""
        let isHasValue = v.count > 0
        defer{
           self.badge.isHidden = isHasValue.not && shouldHideBadgeAtZero
        }
        guard shouldAnimateBadge, isHasValue else { return }
        self.badge.isHidden = false
        let anim = CABasicAnimation(keyPath: "transform.scale")
        anim.fromValue = 1.5
        anim.toValue = 1
        anim.duration = 0.3
        anim.timingFunction = CAMediaTimingFunction(controlPoints: 0.4, 1.3, 1, 1)
        self.badge.layer.add(anim, forKey: "bounceAnimation")
    }

}

extension UIBarButtonItem {
    /// Set Distance With Top
    @objc var badgePaddingTop: CGFloat {
        get{
            return customView?.badgePaddingTop ?? 0
        }
        set{
            customView?.badgePaddingTop = newValue
        }
    }
    
    /// Set Distance With Right
    @objc var badgePaddingRight: CGFloat {
        get{
            return customView?.badgePaddingRight ?? 0
        }
        set{
            customView?.badgePaddingRight = newValue
        }
    }
    /// Set Distance Text With Background
    @objc var badgePaddingTextWithBG: CGFloat {
        get{
            return customView?.badgePaddingTextWithBG ?? 0
        }
        set{
            customView?.badgePaddingTextWithBG = newValue
        }
    }
    
    /// Set Color Background For Badge
    @objc var badgeBGColor: UIColor {
        get{
            return customView?.badgeBGColor ?? .white
        }
        set{
            customView?.badgeBGColor = newValue
        }
    }
    
    /// Set Color Text For Badge
    @objc var badgeTextColor: UIColor {
        get{
            return customView?.badgeTextColor ?? .white
        }
        set{
            customView?.badgeTextColor = newValue
        }
    }
    
    /// Set Font For Badge
    @objc var badgeFont: UIFont {
        get{
            return customView?.badgeFont ?? UIFont.systemFont(ofSize: 12)
        }
        set{
            customView?.badgeFont = newValue
        }
    }
    
    /// Set Value To Badge
    @objc var badgeValue: String? {
        get{
            return customView?.badgeValue
        }
        set{
            customView?.badgeValue = newValue
        }
    }
    
    /// Using Show Badge With No Value
    @objc var shouldHideBadgeAtZero: Bool {
        get{
            return customView?.shouldHideBadgeAtZero ?? false
        }
        set{
            customView?.shouldHideBadgeAtZero = newValue
        }
    }
    
    /// Using Animate Badge
    @objc var shouldAnimateBadge: Bool {
        get{
            return customView?.shouldAnimateBadge ?? false
        }
        set{
            customView?.shouldAnimateBadge = newValue
        }
    }
}


extension String {
    func size(_ maxWidth: CGFloat = .greatestFiniteMagnitude,
              maxHeight: CGFloat = .greatestFiniteMagnitude, using font: UIFont) -> CGSize {
        let constraintRect = CGSize(width: maxWidth, height: maxHeight)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        return boundingBox.size
    }
}
