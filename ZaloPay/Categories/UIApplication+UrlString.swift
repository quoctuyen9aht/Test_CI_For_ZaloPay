//
//  UIApplication+Urlstring.swift
//  ZaloPay
//
//  Created by bonnpv on 10/31/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CocoaLumberjackSwift

extension UIApplication {
    func openUrlString(_ urlString: String?) {
        if let urlString = urlString,
            let url = URL(string: urlString) {
            openURL(url)
            return
        }
        DDLogError("open invalid url: \( urlString ?? "nil string")")
    }
    
    var bindingUrl: Binder<String?> {
        return Binder(self, binding: {
            $0.openUrlString($1)
        })
    }
}
