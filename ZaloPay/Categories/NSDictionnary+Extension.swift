//
//  NSDictionnary+Extension.swift
//  ZaloPay
//
//  Created by vuongvv on 1/16/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit

extension NSDictionary {
    class func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}
