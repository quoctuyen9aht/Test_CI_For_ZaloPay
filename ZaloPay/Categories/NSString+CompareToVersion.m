//
//  NSString+CompareToVersion.m
//  Skylight BVBA
//
//  Created by Stijn Mathysen on 27/02/14.
//  Copyright (c) 2014 Stijn Mathysen. Released under the MIT license
//

#import "NSString+CompareToVersion.h"

@implementation NSString (CompareToVersion)

- (BOOL)isEqualOrNewerThanVersion:(NSString *)version{
    NSComparisonResult result = [self compare:version options:NSNumericSearch];
    return result == NSOrderedSame || result == NSOrderedDescending;
}


@end
