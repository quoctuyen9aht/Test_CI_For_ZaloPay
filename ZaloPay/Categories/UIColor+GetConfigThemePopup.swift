//
//  UIColor+GetConfigThemePopup.swift
//  ZaloPay
//
//  Created by vuongvv on 11/21/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

let kPathConfigInternalAppThemeCashBack      = "%@/assets/theme/cashback/%i/config.json"
let kPathConfigInternalAppThemeVoucher       = "%@/assets/theme/voucher/%i/config.json"
let kPathConfigInternalAppThemeLiXiWishes       = "%@/assets/theme/lixiwishes/%i/config.json"
let kKeyGetButtonColor = "button-color"
extension UIColor {
    @objc static func fromInternalAppThemeCashBack(theme: Int) -> UIColor {
        return self.fromConfig(path: kPathConfigInternalAppThemeCashBack, theme: theme, defaultColor: "#ff5239")
    }
    @objc static func fromInternalAppThemeVoucher(theme: Int) -> UIColor {
        return self.fromConfig(path: kPathConfigInternalAppThemeVoucher, theme: theme, defaultColor: "#FC393D")
    }
    @objc static func fromInternalAppThemeLiXiWishes(theme: Int) -> UIColor {
        return self.fromConfig(path: kPathConfigInternalAppThemeLiXiWishes, theme: theme, defaultColor: "#FC393D")
    }
    static func fromConfig(path:String,theme: Int,defaultColor:String) -> UIColor {
        let url = self.getPathInternalApp(path: path, andThemeId: theme)
        let dic = getDicfromInternalAppPath(url)
        let colorStr = dic.string(forKey: kKeyGetButtonColor)
        return UIColor.init(hexString: colorStr ?? defaultColor) ?? UIColor.red
    }
    static func getPathInternalApp(path: String, andThemeId theme: Int) -> URL {
        let appModel = ReactNativeAppManager.sharedInstance().getApp(showshowAppId)
        let intenalPath: String = ReactNativeAppManager.sharedInstance().getRunFolder(appModel)
        let pathFile = String(format: path, intenalPath, theme)
        return URL(fileURLWithPath: pathFile)
    }
    static func getDicfromInternalAppPath(_ urlFile: URL) -> NSDictionary {
        guard FileManager.default.fileExists(atPath: urlFile.path),let dataFile = try? Data(contentsOf: urlFile),let parseJSON = try? JSONSerialization.jsonObject(with: dataFile as Data, options: .allowFragments) else {
                return NSDictionary()
        }
        guard let json = parseJSON as? NSDictionary else {
            return NSDictionary()
        }
        return json
    }
}
