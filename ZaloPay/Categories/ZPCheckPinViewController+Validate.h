//
//  ZPCheckPinViewController + Validate.h
//  ZaloPay
//
//  Created by Dung Vu on 8/18/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ZaloPayWalletSDK/ZPCheckPinViewController.h>

@class ZPCheckPinViewController;
@interface ZPCheckPinViewController (Validate)
+ (void)showWithMessage:(NSString *)message handleSuccess:(void (^)(NSString *pass))handle handleError:(void (^)(void))errorBlock;
@end
