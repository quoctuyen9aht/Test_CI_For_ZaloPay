//
//  NSString+Validation.m
//  ZaloPay
//
//  Created by PhucPv on 5/22/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <ZaloPayCommon/NSString+Validation.h>
#import <NBPhoneNumber.h>
#import <NBPhoneNumberUtil.h>

#define kPhoneNumberLength      7
#define kTelcoPrefixSeparator   @" "

@implementation NSString(Validation)
- (BOOL)isValidEmail
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

- (BOOL)isValidPhoneNumber {
    NBPhoneNumberUtil *phoneUtil = [[NBPhoneNumberUtil alloc] init];
    NSError *error = nil;
    NBPhoneNumber *phoneNumber_ = [phoneUtil parse:self
                                     defaultRegion:@"VN"
                                             error:&error];
    if (!error) {
        return [phoneUtil isValidNumber:phoneNumber_];
    }
    return NO;
}

- (BOOL)isNumber {
    NSCharacterSet *alphaNums = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *inStringSet = [NSCharacterSet characterSetWithCharactersInString:self];
    return [alphaNums isSupersetOfSet:inStringSet];;
}

- (NSString *)formatPhoneNumber {
    if ([self length] <= kPhoneNumberLength) {
        return self;
    }
    
    NSMutableString *mstr = [self mutableCopy];
    NSInteger pos = [self length] - kPhoneNumberLength;
    [mstr insertString:kTelcoPrefixSeparator atIndex:pos];
    
    return mstr;
}

/**
 Using Format String with plus other separator.
 Ex: 45556666777 -> 455 566 667 77
 
 @param groupLenght number charactor separator
 @param separator character separator
 @param isLeftToRight will keep character follow direction
 @return new string format
 */
- (NSString *)format:(NSInteger)groupLenght
                with:(NSString *)separator
           direction:(BOOL)isLeftToRight {
    if (groupLenght == 0 || !separator) {
        return self;
    }
    
    NSInteger remainCharacter = [self length] % groupLenght;
    NSInteger totalGroup = [self length] / groupLenght;
    if (remainCharacter > 0 && !isLeftToRight) {
        totalGroup = MAX(0, totalGroup - 1);
    }
    
    NSMutableString *mstr = [self mutableCopy];
    
    for (NSInteger idx = 1; idx <= totalGroup; idx += 1) {
        NSInteger pos = (isLeftToRight ? remainCharacter : 0) + idx * groupLenght + (idx - 1) * [separator length];
        [mstr insertString:separator atIndex:pos];
    }
    
    return mstr;
}

/**
 Using for keep secrect some character.
 Ex: 0168999999 -> 0168***999
 
 @param startLenght lenght start not hidden
 @param endLenght lenght last string not hidden
 @param character character 'll use for hidden
 @return new string
 */
- (NSString *)hiddenWith:(NSInteger)startLenght
                     end:(NSInteger)endLenght
           withCharacter:(NSString *)character
{
    // Valid
    if ([self length] <= (startLenght + endLenght)) {
        return self;
    }
    
    NSInteger posEndChange = [self length] - endLenght;
    NSInteger lenghtHidden = MAX(0, posEndChange - startLenght);
    
    NSString *str = [@"" stringByPaddingToLength:lenghtHidden withString:character startingAtIndex:0];
    NSString *newResult = [[self copy] stringByReplacingCharactersInRange:NSMakeRange(startLenght, lenghtHidden) withString:str];
    
    return newResult;
}

- (NSString*) stringWithHexBytes {
    NSData* data = [self dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableString *stringBuffer = [NSMutableString stringWithCapacity:([data length] * 2)];
    const unsigned char *dataBuffer = [data bytes];
    for (int i = 0; i < [data length]; ++i) {
        [stringBuffer appendFormat:@"%02X", (unsigned int)dataBuffer[i]];
    }

    return stringBuffer;
}

@end
