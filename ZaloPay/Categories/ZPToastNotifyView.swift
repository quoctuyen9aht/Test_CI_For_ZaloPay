//
//  ZPToastNotifyView.swift
//  ZaloPay
//
//  Created by Dung Vu on 7/6/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit

class ZPToastNotifyConfig: NSObject {
    static let `default` = ZPToastNotifyConfig()
    
    var fontSizeIcon: CGFloat = 33
    var colorText: UIColor = .white
    var colorBackground: UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.54)
    var iconCode: String = "general_check"
    var fontSizeDescription: CGFloat = 15
    var cornerRadius: CGFloat = 8
    var numLinesMessage: Int = 0
    
    // Design
    var topIcon: CGFloat = 10
    var paddingIconAndMessage: CGFloat = 10
    var paddingMessage: CGFloat = 10
    var widthMessageDefault: CGFloat = 208
    var widthMessageMaximum: CGFloat = 300
    var bottomMessage: CGFloat = 10
}
@objcMembers
final class ZPToastNotifyView: NSObject {
    private override init() {
        super.init()
    }
    
    
    static func showToastDefault(on view: UIView,
                                 delay: TimeInterval,
                                 with msg: String?,
                                 completion:(() -> ())?)
    {
        self.showToast(on: view, delay: delay, with: msg, using: .default, completion: completion)
    }
    
    static func showToast(on view: UIView,
                          delay: TimeInterval,
                          with msg: String?,
                          using config: ZPToastNotifyConfig,
                          update: ((UIView) ->())? = nil,
                          completion:(() -> ())?)
    {
        let toast = createToast(using: config, with: msg).then {
            view.addSubview($0)
            let v = $0
            update?(v) ?? { () -> () in
                v.snp.makeConstraints {
                    $0.center.equalTo(view.snp.center)
                }
            }()
        }
        
        // Animation
        toast.isHidden = false
        UIView.animateKeyframes(withDuration: 2, delay: delay, options: .calculationModeLinear, animations: {[weak toast] in
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.3, animations: {
                toast?.alpha = 1
            })
            
            UIView.addKeyframe(withRelativeStartTime: 1.5, relativeDuration: 0.4, animations: {
                toast?.alpha = 0
            })
        }) {[weak toast](_) in
            toast?.isHidden = true
            toast?.snp.removeConstraints()
            toast?.removeFromSuperview()
            completion?()
        }
        
    }
    
    fileprivate static func createToast(using config: ZPToastNotifyConfig, with msg: String?) -> UIView {
        // main
        let v = UIView(frame: .zero)
        v.backgroundColor = config.colorBackground
        v.layer.cornerRadius = config.cornerRadius
        v.clipsToBounds = true
        
        // icon
        let icon = UILabel(frame: .zero).then {
            $0.font = UIFont.zaloPay(withSize: config.fontSizeIcon)
            $0.textColor = config.colorText
            v.addSubview($0)
            $0.snp.makeConstraints({
                $0.top.equalTo(config.topIcon)
                $0.centerX.equalTo(v.snp.centerX)
                $0.width.greaterThanOrEqualTo(0)
                $0.height.greaterThanOrEqualTo(0)
            })
            $0.text = UILabel.iconCode(withName: config.iconCode)
        }
        
        // Description
        _ = UILabel(frame: .zero).then({
            $0.font = UIFont.sfuiTextRegular(withSize: config.fontSizeDescription)
            $0.textAlignment = .center
            $0.textColor = config.colorText
            $0.numberOfLines = config.numLinesMessage
            v.addSubview($0)
            $0.snp.makeConstraints({
                $0.top.equalTo(icon.snp.bottom).offset(config.paddingIconAndMessage)
                $0.left.equalTo(config.paddingMessage)
                $0.right.equalTo(-config.paddingMessage).priority(.medium)
                $0.width.greaterThanOrEqualTo(config.widthMessageDefault)
                $0.width.lessThanOrEqualTo(config.widthMessageMaximum)
                $0.height.greaterThanOrEqualTo(0)
                $0.bottom.equalTo(-config.bottomMessage).priority(.medium)
            })
            $0.text = msg
        })
        v.layoutIfNeeded()
        v.isHidden = true
        v.alpha = 0
        return v
    }
}

