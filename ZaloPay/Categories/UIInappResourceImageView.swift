//
//  UIImageView+InappResource.swift
//  ZaloPay
//
//  Created by Bon Bon on 11/28/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
@objcMembers
class UIInappResourceImageView: UIImageView {
    var labelTitle: UILabel?
    func setImageFromResource(imagName: String, title: String, titleColor: UIColor) {
        // reset image if it exist
        self.image = nil
        if let image = UIImage.optionalImageFromInternalApp(imagName) {
            self.image = image
            self.labelTitle?.isHidden = true
            return
        }
        if let titleLabel = self.labelTitle {
            update(titleLabel: titleLabel, text: title, color: titleColor)
            return
        }
        let label = UILabel()
        self.addSubview(label)
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .center
        label.autoresizingMask =  [.flexibleWidth, .flexibleHeight]
        self.labelTitle = label
        update(titleLabel: label, text: title, color: titleColor)
    }
    
    private func update(titleLabel: UILabel, text: String, color: UIColor) {
        var frame = self.frame
        frame.origin.x = 0
        frame.origin.y = 0
        titleLabel.isHidden = false
        titleLabel.frame = frame
        titleLabel.text = text;
        titleLabel.textColor = color
    }
}


