//
//  NSAttributedString+Currency.swift
//  ZaloPay
//
//  Created by vuongvv on 9/29/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

extension NSAttributedString {
    class func attributedStringWith(amount: Int64, font: UIFont, subFont: UIFont, color: UIColor) -> NSAttributedString{
       return NSAttributedString.attributedStringWith(amount: amount, font: font, subFont: subFont, color: color, textAlignment: .center)
    }
    class func attributedStringWith(amount: Int64, font: UIFont, subFont: UIFont, color: UIColor, textAlignment: NSTextAlignment) -> NSAttributedString{
        let text: String = "\(amount)".formatCurrency()
        let range = text.count > 3 ? NSRange(location: text.count - 3, length: 3) : NSRange(location: 0, length: 0)
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 1.0
        style.alignment = textAlignment
        let attributtes = [NSAttributedStringKey.paragraphStyle: style, NSAttributedStringKey.font: font, NSAttributedStringKey.foregroundColor: color]
        let _return = NSMutableAttributedString(string: text, attributes: attributtes)
        if range.length > 0 {
            _return.addAttribute(NSAttributedStringKey.font, value: subFont, range: range)
        }
        return _return
    }
}
