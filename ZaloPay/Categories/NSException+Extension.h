//
//  NSException+Extension.h
//  ZaloPay
//
//  Created by Dung Vu on 1/19/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#ifndef NSException_Extension_h
#define NSException_Extension_h
#import <Foundation/Foundation.h>

@interface NSException(Extension)
+ (instancetype _Nullable)tryBlock: (void(^_Nonnull)(void))block;
@end

#endif /* NSException_Extension_h */
