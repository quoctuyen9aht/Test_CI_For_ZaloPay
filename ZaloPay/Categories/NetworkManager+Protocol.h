//
//  NetworkManager+Protocol.h
//  ZaloPay
//
//  Created by nhatnt on 19/01/2018.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ZaloPayWalletSDK/ZaloPayWalletSDKNetworkProtocol.h>

@class NetworkManager;
@protocol ZaloPayWalletSDKNetworkProtocol;
@interface NetworkManager(SDK)<ZaloPayWalletSDKNetworkProtocol>
@end

