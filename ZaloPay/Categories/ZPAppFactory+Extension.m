//
//  ZPAppFactory+Extension.m
//  ZaloPay
//
//  Created by Nguyễn Hữu Hoà on 9/15/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <ZaloPayUI/ZPAppFactory.h>
#import <ZaloPayWalletSDK/ZPCheckPinViewController.h>
#import <ZaloPayFeedbackCollector/ZPFeedbackViewController.h>
//#import <ZaloPayProfile/ZPContactSwift.h>
#import <ZaloPayWalletSDK/ZPProgressHUD.h>
#import <ZaloPayCommon/NSCollection+JSON.h>
#import <ZaloPayWalletSDK/ZaloPayWalletSDKPayment.h>
#import <ZaloPayDatabase/ZPNotifyTable.h>
#import <ZaloPayAnalyticsSwift/ZaloPayAnalyticsSwift-Swift.h>
#import <ZaloPayCommonSwift/ZaloPayCommonSwift-Swift.h>
//#import <ZaloPayWeb/ZaloPayWeb-Swift.h>

#import "ZPSharingChannel.h"
#import "NetworkManager+ValidatePin.h"
#import "BillDetailViewControllerDelegate.h"
#import "ZPNotificationService.h"
#import "ZPConnectionManager+SendMessage.h"
#import "NetworkManager+Profile.h"
#import "ZPRecentTransferMoneyTable.h"
#import <ZaloPayConfig/ZaloPayConfig-Swift.h>

#import "ZaloPayProfile/ZaloPayProfile-Swift.h"

#import ZALOPAY_MODULE_SWIFT

//https://gitlab.zalopay.vn/zalopay-apps/task/issues/921
typedef enum {
    ReactNativeTransferMode_ZaloPayId = 1,
    ReactNativeTransferMode_Phone = 2,
    ReactNativeTransferMode_HidePhone = 3,
} ReactNativeTransferMode;


@implementation ZPAppFactory (Extension)

#pragma mark - Feedback

- (void)showFeedbackViewController {
    UINavigationController *nav = [self rootNavigation];
    if ([nav isKindOfClass:[UINavigationController class]]) {
        ZPFeedbackViewController *controller = [[ZPFeedbackViewController alloc] init];
        [nav pushViewController:controller animated:true];
    }

}

- (void)showVoucherView {
    ZPVoucherHistoryListViewController *controller = (ZPVoucherHistoryListViewController *)[ZPVoucherHistoryRouter createZPVoucherHistoryModule];
    [[UINavigationController currentActiveNavigationController] pushViewController:controller animated:TRUE];
}

#pragma mark - Check PIN

- (void)promptPIN:(ReactNativeChannelType)channel callback:(void (^)(id))callback error:(void (^)(NSError *))errorBlock {
    if (channel == ReactNativeChannelType_Transaction) {
        if (callback) {
            callback(@1);
        }
        return;
    }
    
//    if (channel == ReactNativeChannelType_Profile) {
//        if (callback) {
//            callback(@1);
//        }
//        return;
//    }
    
    if (channel == ReactNativeChannelType_Bank) {
        NSInteger totalCard = [[ZPBankApiWrapperSwift sharedInstance] listCards].count;
        NSInteger totalAccount = [[ZPBankApiWrapperSwift sharedInstance] listAccounts].count;
        if (totalCard == 0 && totalAccount == 0) {
            if (callback) {
                callback(@1);
            }
            return;
        }
    }
    
    NSString *message;
    BOOL isForceInput = NO;
    switch (channel) {
        case ReactNativeChannelType_Profile:
            message = [R string_Message_Input_PIN_Profile];
            break;
        case ReactNativeChannelType_Bank:
            message = [R string_Message_Input_PIN_ManageCard];
            break;
        case ReactNativeChannelType_ClearingAccount:
            message = [R string_Message_Input_PIN_Transactions];
            isForceInput = YES;
            break;
        default:
            message = [R string_Message_Input_PIN_Profile];
            break;
           
//        case 3:
//            message = [R string_Message_Input_PIN_Transactions];
//            break;
//        default:
//            message = [R string_Message_Input_PIN_Transactions];
//            break;
    }
    
    [self showPinDialogMessage:message
                  isForceInput:isForceInput success:^{
        if (callback) {
            callback(@1);
        }
                  
                  }error:^(NSError *error) {
                      errorBlock(error);
                  }];
}

- (NSString *)cacheKeyForLastInput {
    NSString * value = [NSString stringWithFormat:@"PIN_DIALOG_LAST_INPUT_%@", [NetworkManager sharedInstance].paymentUserId];
    return value;
}

- (void)saveLastTimeInputPin {
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    [ZPCache setObject:@(currentTime) forKey:[self cacheKeyForLastInput]];
}

- (BOOL)isRecentAuthenSuccess {
    NSNumber *lastInput = [ZPCache objectForKey:[self cacheKeyForLastInput]];
    if (lastInput == nil) {
        return false;
    }
    NSTimeInterval timestamp = [lastInput longLongValue];
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    return  (currentTime - timestamp < 5 * 60);    
}

- (void)showPinDialogMessage:(NSString *)message
                isForceInput:(BOOL)force
                     success:(void (^)(void))handle
                       error:(void (^)(NSError *))errorBlock {
    if (!force) {
        if ([self isRecentAuthenSuccess]) {
            if (handle) {
                handle();
            }
            return;
        }
        
        if ([ZPSettingsConfig sharedInstance].askForPassword == false) {
            if (handle) {
                handle();
            }
            return;
        }
    }
    
    if ([ZPSettingsConfig sharedInstance].usingTouchId == false) {
        [self doShowWithMessage:message success:handle error:errorBlock];
        return;
    }
    
    [[ZPTouchIDService sharedInstance] authenFromTouchIDWithCompleteHandle:^(TouchIDResult result) {
        if (result == TouchIDResultSuccess) {
            if (!force) {
                 [self saveLastTimeInputPin];
            }
            if (handle) {
                handle();
            }
            return;
        }

        if (result == TouchIDResultUserCancel || result == TouchIDResultActivated) {
            return;
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            // delay -> fix key board with clear color after hiding touchID
            [self doShowWithMessage:message success:handle error:errorBlock];
        });
    } messsage: [UIView isIPhoneX] ? [R string_TouchID_Password_Message] : [R string_TouchID_Message]];
}


- (void)vertify:(NSString *)pin
        success:(void (^)(void))handle
          error:(void (^)(NSError *))errorBlock
{
    
    [ZPCheckPinViewController startLoading];
    NSString *sha = [pin sha256];
    [[[[NetworkManager sharedInstance] validatePin:sha] deliverOnMainThread] subscribeError:^(NSError *error) {
        [ZPCheckPinViewController showErrorMessage:[error apiErrorMessage]];
        if (errorBlock) {
            errorBlock(error);
        }
    } completed:^{
        [[ZPTouchIDService sharedInstance]  saveZaloPayPassword:sha];
        [ZPCheckPinViewController dissmiss:YES];
        if (handle) {
            handle();
        }
    }];
}

- (void)doShowWithMessage:(NSString *)message
                  success:(void (^)(void))handle
                    error:(void (^)(NSError *))errorBlock {
    UIViewController *root = [[UIApplication sharedApplication].delegate.window rootViewController];
    
    if (!root) {
        return;
    }
    @weakify(self);
    [[ZPCheckPinViewController showOn:root using:message methodName:nil methodImage:nil suggestTouchId:YES] subscribeNext:^(NSString *password) {
        @strongify(self);
        [self vertify:password success:handle error:errorBlock];
    }];
}

#pragma mark - Pay Bill

- (void)showZaloPayGateway:(UIViewController *)fromcontroller
             completeBlock:(void (^)(NSDictionary *data))completeBlock
               cancelBlock:(void (^)(NSDictionary *data))cancelBlock
                errorBlock:(void (^)(NSDictionary *data))errorBlock
                     appId:(NSInteger)appId
                  billData:(NSDictionary *)billData {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (billData.count == 0 ) {
            if (errorBlock) {
                errorBlock(nil);
            }
            return;
        }
        ZPBill *bill = [[ZPBill alloc] initWithData:billData
                                  transType:ZPTransTypeBillPay
                                orderSource:OrderSource_MerchantApp
                                      appId:appId];
        [self showZaloPayGateway:fromcontroller
                           appId:appId
                            with:bill
                   completeBlock:completeBlock
                     cancelBlock:cancelBlock
                      errorBlock:errorBlock];
        
    });
}

- (void)showZaloPayGateway:(UIViewController *)fromVC
                    appId:(NSInteger)appId
                    with:(ZPBill *)bill
           completeBlock:(void (^)(NSDictionary *data))completeBlock
             cancelBlock:(void (^)(NSDictionary *data))cancelBlock
              errorBlock:(void (^)(NSDictionary *data))errorBlock {
    
    BOOL isShowPopup = NO;
    NSArray *popupModeAppList = [[ZPApplicationConfig getSDKConfig] getPopupModeAppList];
    if (popupModeAppList) {
        isShowPopup = [popupModeAppList containsObject:@(appId)];
    }
    
    isShowPopup ? [self showPopupPayment:fromVC
                                    with:bill
                           completeBlock:completeBlock
                             cancelBlock:cancelBlock
                              errorBlock:errorBlock] :
                [self showPaymentNormal:fromVC
                                   with:bill
                          completeBlock:completeBlock
                            cancelBlock:cancelBlock
                             errorBlock:errorBlock];
}

    
- (void)showPopupPayment:(UIViewController *)fromVC
                     with:(ZPBill *)bill
            completeBlock:(void (^)(NSDictionary *data))completeBlock
              cancelBlock:(void (^)(NSDictionary *data))cancelBlock
               errorBlock:(void (^)(NSDictionary *data))errorBlock {
    [[ZaloPayWalletSDKPayment sharedInstance] showPopupPayment:fromVC with:bill complete:^(id object) {
        if (completeBlock) {
            NSDictionary *param = [NSDictionary castFrom:object] ?: @{};
            completeBlock(param);
        }
    } error:^(NSError *e) {
        if ([e code] == NSURLErrorCancelled) {
            if (cancelBlock) {
                cancelBlock(e.userInfo);
            }
            return;
        }
        
        if (errorBlock) {
            errorBlock(e.userInfo);
        }
    }];
}

- (void)showPaymentNormal: (UIViewController *)fromVC
                    with:(ZPBill *)bill
           completeBlock:(void (^)(NSDictionary *data))completeBlock
             cancelBlock:(void (^)(NSDictionary *data))cancelBlock
              errorBlock:(void (^)(NSDictionary *data))errorBlock {
    
    BillDetailViewController *handler = [[BillDetailViewController alloc] init:completeBlock
                                                                                    cancel:cancelBlock
                                                                                     error:errorBlock
                                                                           getAppInfoError:^(NSDictionary *data) {
                                                                               [self alertGetAppInfoError];
                                                                               if (errorBlock) {
                                                                                   errorBlock(data);
                                                                               }
                                                                           }];
    [handler startProcessBill:bill from:fromVC];
}
    

- (void)alertGetAppInfoError {
    [ZPDialogView showDialogWithType:DialogTypeWarning
                             message:[R string_PayBillSetAppId_ErrorMessage]
                   cancelButtonTitle:[R string_ButtonLabel_Close]
                    otherButtonTitle:nil
                      completeHandle:nil];
}

- (BOOL)isPayingBill {
    UINavigationController *nav = [self rootNavigation];
    for (UIViewController *vc in nav.viewControllers) {
        if ([vc isKindOfClass:[BillDetailViewController class]]) {
            return true;
        }
    }
    return [[ZaloPayWalletSDKPayment sharedInstance] checkIsCurrentPaying];
}

- (void)showNoInternetConnectionView {
    UINavigationController *nav = [self rootNavigation];
    if ([nav isKindOfClass:[UINavigationController class]]) {
        ZPInternetConnectionViewController *controller = [[ZPInternetConnectionViewController alloc] init];
        [nav pushViewController:controller animated:YES];
    }
}

- (void)logout {
    [[LoginManagerSwift sharedInstance] logout];
}


#pragma mark - Loading View

- (void)setHUDWithTarget:(id) target sel:(SEL) sel {
    [ZPProgressHUD.sharedView addTarget:target action:sel forControlEvents:UIControlEventTouchUpInside];
}

- (void)removeHUDWithTarget:(id) target sel:(SEL) sel {
    [ZPProgressHUD.sharedView removeTarget:target action:sel forControlEvents:UIControlEventTouchUpInside];
}

- (void)showHUDAddedTo:(UIView *)view {
    //    [LoadingView showHUDAddedTo:view];
    [ZPProgressHUD showWithStatus:@""];
}

- (void)hideAllHUDsForView:(UIView *)view {
    //    [LoadingView hideAllHUDsForView:view];
    [ZPProgressHUD dismiss];
}

- (void)payMerchantBillFromNotification:(NSString *)notificationid {
    [[ZPNotificationService sharedInstance] payMerchantBill:notificationid];
}

- (void)showBankView {
    [ZPCenterRouter launchWithRouterId:RouterIdLinkCard from:nil animated:true];
//    UINavigationController *nav = [self rootNavigation];
//    if (![nav isKindOfClass:[UINavigationController class]]) {
//        return;
//    }
//    ZPLinkCardViewController *bankVC = [ZPLinkCardViewController new];
//    [nav pushViewController:bankVC animated:true];
}

#pragma mark - Check Config network

- (UINavigationController *)rootNavigation {
    return [UINavigationController currentActiveNavigationController];
}

- (void)sendRemoveMessageWithMtaid:(uint64_t)mtaid mtuid:(uint64_t)mtuid {
    [[ZPConnectionManager sharedInstance] sendDeleteStatusWithMtaid:mtaid mtuid:mtuid];
}

- (void)sendReadMessageWithMtaid:(uint64_t)mtaid mtuid:(uint64_t)mtuid {
    [[ZPConnectionManager sharedInstance] sendReadStatusWithMtaid:mtaid mtuid:mtuid];
}

- (void)trackApiFail:(NSDictionary *)log {
    [[ZPTrackingHelper shared].eventTracker writeLogErrorApiWithErrorLog:log];
}
- (void)trackNotification:(NSDictionary *)params{
    ZPAppleNotificationModel *md =  [ZPAppleNotificationModel new];
    md.pushId = [params stringForKey:@"notificationId" defaultValue:@""];
    md.timeStamp = [params uint64ForKey:@"timeStamp" defaultValue:0];
    md.source = ZPNotiSourceMarketing;
    NSDictionary *log = [md getInforFrom:ZPUserNotiActionOpen];
    [[ZPTrackingHelper shared].eventTracker writeLogNotificationWithLog:log];
    [ZPAppleNotificationServices sharedInstance].notiReceiveLast = md;
}
- (void)trackBackButtonClickEventId:(NSInteger)eventId;{
    [[ZPTrackingHelper shared] trackEvent:eventId];
}
- (void)navigateProtectAccount {
    [ZPCenterRouter launchWithRouterId:RouterIdSetting from:nil animated:true];
//    UINavigationController *navi = [self rootNavigation];
//    UIViewController *setting = [ZPSettingRouter assemblyModule];
//    [navi pushViewController:setting animated:YES];
}

- (void)launchContactList:(NSString *)currentPhone
                 viewMode:(NSString *)viewMode
          navigationTitle:(NSString *)navigationTitle
           completeHandle:(void (^)(NSDictionary *data))completeBlock {
    
    [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionContact_launch_from_topup];
    [ZPContactListRouter launchFromTopup:ContactMode_ZPC_PhoneBook
                            currentPhone:currentPhone
                             displayMode:ZPCListViewModeAll
                                viewMode:viewMode
                         navigationTitle:navigationTitle
                              completion:^(ZPContactSwift * contact)
    {
        if (contact == nil) {
            [self selectContactData:@{} completeHandle:completeBlock];
            return;
        }
        if (contact.phoneNumber.length == 0) {
            [self alertNoPhoneNumber:contact];
            return;
        }
        
        if (contact.displayNameUCB.length == 0 && contact.phoneNumber.length > 0) {
            // case bạn zalo trong danh sách yêu thích mà không có trong danh bạ -> xử lý giống như ko có số điện thoại.
            [self alertNoPhoneNumber:contact];
            return;
        }
        
        NSString *phoneNumber = contact.phoneNumber;
        NSString *avatar = contact.avatar;
        NSString *phonename = contact.displayName;
        
        NSMutableDictionary* data = [NSMutableDictionary new];
        [data setObjectCheckNil:phoneNumber forKey:@"phonenumber"];
        [data setObjectCheckNil:avatar forKey:@"avatar"];
        [data setObjectCheckNil:phonename forKey:@"phonename"];
        
        [self selectContactData:data completeHandle:completeBlock];
    }];
}
    
    - (void)launchContactList:(NSString *)navigationTitle
              backgroundColor:(NSString *)backgroundColor
                selectedArray:(NSArray *)selectedArray
             maxUsersSelected:(int)maxUsersSelected
                         mode:(int)mode
               completeHandle:(void (^)(NSDictionary *data))completeBlock {
        
        [ZPContactListRouter launchFromRedPacket:ContactMode_ZPC_All backgroundColor:backgroundColor displayMode:mode viewMode:ViewModeKeyboard_ABC maxUsersSelected:maxUsersSelected arrayRedPacket:selectedArray navigationTitle:navigationTitle completion:^(NSArray<ZPContactSwift *> * arrUsersRedPacket, ZPCEndSelectedRedPacket endSelectedRedPacket) {
            
            NSMutableArray *selectedArray = [NSMutableArray new];
            for (ZPContactSwift * contact in arrUsersRedPacket) {
                NSMutableDictionary* data = [NSMutableDictionary new];
                [data setObjectCheckNil:contact.phoneNumber forKey:@"phonenumber"];
                [data setObjectCheckNil:contact.avatar forKey:@"avatar"];
                [data setObjectCheckNil:contact.zaloPayId forKey:@"zalopayid"];
                [data setObjectCheckNil:contact.displayName forKey:@"displayname"];
                [data setObjectCheckNil:contact.displayNameZLF forKey:@"zaloname"];
                [data setObjectCheckNil:contact.displayNameUCB forKey:@"contactdisplayname"];
                [data setObjectCheckNil:contact.zaloId forKey:@"zaloid"];
                [data setObjectCheckNil:contact.zpContactID forKey:@"zpcontactid"];
                
                [selectedArray addObject:data];
            }
            
            [self selectArrContactRedPacketData:selectedArray endSelectedRedPacket:endSelectedRedPacket completeHandle:completeBlock];
        }];
        
        
    }
    

- (void)selectArrContactRedPacketData:(NSArray *)selectedArray endSelectedRedPacket:(ZPCEndSelectedRedPacket)endSelectedRedPacket  completeHandle:(void (^)(NSDictionary *data))completeBlock {
    int code = endSelectedRedPacket == ZPCEndSelectedRedPacketNext ? 1 : 4;
    NSDictionary *_return = @{@"code" : @(code), @"data" :selectedArray};
    if (completeBlock) {
        completeBlock(_return);
    }
    
}
    
- (void)selectContactData:(NSDictionary *)data completeHandle:(void (^)(NSDictionary *data))completeBlock {
    NSDictionary *_return;
    if ([[data allKeys] count] == 0) {
        _return = @{@"code" : @(4),
                    @"data" :data};
        if (completeBlock) {
            completeBlock(_return);
        }
    } else {
        _return = @{@"code" : @(1), @"data" :data};
        if (completeBlock) {
            completeBlock(_return);
            [[self rootNavigation] popViewControllerAnimated:YES];
        }
    }
}

- (void)alertNoPhoneNumber:(ZPContactSwift *) contact {
    NSString *displayName = contact.displayName;
    
    NSString *message = [NSString stringWithFormat:[R string_Topup_No_Phone], displayName];
    NSMutableAttributedString * attributedString = [ZPDialogView attributeStringWithMessage:message];
    NSRange range = NSMakeRange(0, displayName.length);
    UIFont *font = [UIFont SFUITextSemiboldWithSize:15];
    [attributedString addAttributes:@{NSFontAttributeName: font} range:range];
    
    [ZPDialogView showDialogWithType:DialogTypeNotification
                               title:[R string_Dialog_Notification]
                    attributeMessage:attributedString
                        buttonTitles:@[[R string_ButtonLabel_Close]]
                             handler:nil];
}

#pragma mark - check pin without save pass

- (RACSignal *)checkPinNoCacheWithMessage:(NSString *)message {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        if ([ZPSettingsConfig sharedInstance].usingTouchId) {
            [[self authenUsingTouchIdWithMessage:message] subscribe:subscriber];
        }else {
            [[self inputPinWith:message] subscribe:subscriber];
        }
        
        return nil;
    }];
}

- (RACSignal *)authenUsingTouchIdWithMessage:(NSString *)message  {
    
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [[ZPTouchIDService sharedInstance] authenFromTouchIDWithCompleteHandle:^(TouchIDResult result) {
            if (result == TouchIDResultSuccess) {
                [subscriber sendNext:@(YES)];
                [subscriber sendCompleted];
                return;
            }

            if (result == TouchIDResultUserCancel) {
                [subscriber sendNext:@(NO)];
                [subscriber sendCompleted];
                return;
            }
            
            if (result == TouchIDResultActivated) {
                [subscriber sendCompleted];
                return;
            }
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                // delay -> fix key board with clear color after hiding touchID
                [[self inputPinWith:message] subscribe:subscriber];
            });
        } messsage:[UIView isIPhoneX] ? [R string_TouchID_Password_Message] : [R string_TouchID_Message]];
        
        return nil;
    }];
}

- (RACSignal *)inputPinWith:(NSString *)message {
    UIViewController *root = [[UIApplication sharedApplication].delegate.window rootViewController];
    
    if (!root) {
        return [RACSignal empty];
    }
    
    return [[[ZPCheckPinViewController showOn:root
                                      using:message
                                 methodName:nil
                                methodImage:nil
                             suggestTouchId:YES]
            flattenMap:^__kindof RACSignal * _Nullable(NSString * _Nullable pin)
    {
        return [self vertify:pin];
    }] doNext:^(NSNumber *value) {
        if ([value boolValue]) {
            [ZPCheckPinViewController dissmiss:NO];
        }
    }];
}

- (RACSignal *) vertify:(NSString *)pin {
    [ZPCheckPinViewController startLoading];
    NSString *sha = [pin sha256];
    return [[[[[NetworkManager sharedInstance] validatePin:sha] deliverOnMainThread] flattenMap:^__kindof RACSignal * _Nullable(id value) {
        return [RACSignal return:@(YES)];
    }] catch:^RACSignal *(NSError *error) {
        [ZPCheckPinViewController showErrorMessage:[error apiErrorMessage]];
        return [RACSignal return:@(NO)];
    }];
}

- (void)transferMoney:(NSString *)userId
               amount:(NSInteger)amount
              message:(NSString *)message
         transferMode:(NSString *)transferMode
        completeBlock:(void (^)(NSDictionary *data))completeBlock
          cancelBlock:(void (^)(NSDictionary *data))cancelBlock
           errorBlock:(void (^)(NSDictionary *data))errorBlock {
    
    if (![userId isKindOfClass:[NSString class]] || userId.length == 0) {
        if (errorBlock) {
            errorBlock(nil);
        }
        return;
    }
    
    if ([NetworkState sharedInstance].isReachable == false) {
        [ZPDialogView showDialogWithType:DialogTypeNoInternet
                                   title:[R string_Dialog_Warning]
                                 message:[R string_NetworkError_NoConnectionMessage]
                            buttonTitles:@[[R string_ButtonLabel_Close]]
                                 handler:nil];
        return;
    }
    
    [[ZPAppFactory sharedInstance] showHUDAddedTo:nil];
    @weakify(self);
    uint64_t zaloPayId = [userId longLongValue];
    [[[[NetworkManager sharedInstance] getUserInfoByZaloPayId:zaloPayId] deliverOnMainThread] subscribeNext:^(NSDictionary *userDict) {
        @strongify(self);
        [[ZPAppFactory sharedInstance] hideAllHUDsForView:nil];
        
        ZPRecentTransferMoneyTable* recentTransferMoneyTable = [[ZPRecentTransferMoneyTable alloc] initWithDB:[PerUserDataBase sharedInstance]];
        UserTransferData *oldUser = [recentTransferMoneyTable getRecentUserTransfer:userId];
        NSString *phone = oldUser.phone;
        NSString *accountName = [userDict stringForKey:@"zalopayname"];
        ReactNativeTransferMode reactTransferMode = (ReactNativeTransferMode)[transferMode integerValue];
        TransferMode mode = [self tranferModeFromReactNative:reactTransferMode accountName:accountName];
        
        UserTransferData *transferUser = [[UserTransferData alloc] init];
        transferUser.paymentUserId = [@(zaloPayId) stringValue];
        transferUser.message = message;
        transferUser.transferMoney = [@(amount) stringValue];
        transferUser.accountName = accountName;
//        transferUser.transferMoney = [@(amount) formatMoneyValue];
//        transferUser.message = message;
        transferUser.mode = mode;
        transferUser.source = ActivateSourceFromTransferActivity;
        transferUser.displayName = [userDict stringForKey:@"displayname"];
        transferUser.avatar = [userDict stringForKey:@"avatar"];
        phone = phone.length > 0 ? phone : [[[NSString stringWithFormat:@"%@",[userDict objectForKey:@"phonenumber"]] normalizePhoneNumber]  formatPhoneNumber];
        transferUser.phone = phone;
        
        
        [self transferMoneyToUser:transferUser
                    completeBlock:completeBlock
                      cancelBlock:cancelBlock
                       errorBlock:errorBlock];
    } error:^(NSError *error) {
        [[ZPAppFactory sharedInstance] hideAllHUDsForView:nil];
        [ZPDialogView showDialogWithError:error handle:nil];
        if (errorBlock) {
            errorBlock(error.userInfo);
        }
    }];
}

- (TransferMode)tranferModeFromReactNative:(ReactNativeTransferMode)mode accountName:(NSString *)accountName {
    // hiển thị zalopayid;
    if (mode == ReactNativeTransferMode_ZaloPayId && accountName.length > 0) {
        return TransferModeToZaloPayID;
    }
    
    // hiển thị số điện thoại.
    if (mode == ReactNativeTransferMode_Phone) {
        return TransferModeToPhoneNumber;
    }
    // che số điện thoại chỉ hiển thị 4 số cuối.
    return TransferModeToZaloPayUser;
}

- (void)transferMoneyToUser:(UserTransferData *)user
              completeBlock:(void (^)(NSDictionary *data))completeBlock
                cancelBlock:(void (^)(NSDictionary *data))cancelBlock
                 errorBlock:(void (^)(NSDictionary *data))errorBlock{
    
    ZPTransferReceiverWrapper *controller = [[ZPTransferReceiverWrapper alloc] initWithTransferData:user
                                                                                        orderSource:OrderSource_None preHandle:^{
        
    } cancelHandle:^{
        if (cancelBlock) {
            cancelBlock(nil);
        }
    } errorHandle:^{
        if (errorBlock) {
            errorBlock(nil);
        }
    } completeHandle:^{
        if (completeBlock) {
            completeBlock(nil);
        }
    }];
    [[self rootNavigation] pushViewController:controller animated:true];
}

- (void)setHiddenTabbar:(BOOL)hide {
    [[ZPUIAppDelegate sharedInstance] setHiddenTabbar:hide];    
}

- (void)openApp:(NSInteger)appId moduleName:(NSString *)moduleName properties:(NSDictionary *)properties {    
//    ZPReactNativeAppRouter *reactAppRouter = [[ZPReactNativeAppRouter alloc] init];
//    ReactAppModel *app = [[ReactNativeAppManager sharedInstance] getApp:appId];
    UINavigationController *nav = [[ZPAppFactory sharedInstance] rootNavigation];
//    [reactAppRouter openApp:app from:nav.topViewController moduleName:moduleName withProps:properties];
    [ZPCenterRouter launchExternalAppWithAppId:appId appType:ReactAppTypeNative from:nav.topViewController moduleName:moduleName properties:properties];
}

- (void)launchApp:(NSInteger)appId
       moduleName:(NSString *)moduleName
       properties:(NSDictionary *)properties
          success:(void (^)(NSDictionary *data))successBlock
            error:(void (^)(NSDictionary *data))errorBlock
       unvailable:(void (^)(NSDictionary *data))unvailableBlock {
    
    NSArray *allApp = [ReactNativeAppManager sharedInstance].activeAppIds;
    if (![allApp containsObject:@(appId)]) {
        if (unvailableBlock) {
            unvailableBlock(nil);
        }
        return;
    }
    
    UINavigationController *nav = [[ZPAppFactory sharedInstance] rootNavigation];
    
    if (appId == withdrawAppId) {
        // open App Native WithDraw
        [ZPCenterRouter launchWithRouterId:RouterIdWithdraw from:nil animated:true];
        //        ZPWithdrawViewControllerSwift *withdrawVC = [ZPWithdrawRouter assembleModule];
        //        [nav pushViewController:withdrawVC animated:YES];
        if (successBlock) {
            successBlock(nil);
        }
        return;
    }
    
    ReactAppModel *app = [[ReactNativeAppManager sharedInstance] getApp:appId];
    if (app.appType == ReactAppTypeWeb) {
//        [ZPWebAppRouter openWebWithAppId:appId viewController:topViewController];
        [ZPCenterRouter launchExternalAppWithAppId:appId appType:ReactAppTypeWeb from:nav.topViewController moduleName:nil properties:nil];
        if (successBlock) {
            successBlock(nil);
        }
        return;
    }
    
    moduleName = moduleName.length > 0 ?  moduleName : [[ReactNativeAppManager sharedInstance] defaultModuleName:appId];
//    NSMutableDictionary *allProperties = [[[ReactNativeAppManager sharedInstance] reactProperties:appId] mutableCopy];
//    if (properties.count > 0) {
//        [allProperties addEntriesFromDictionary:properties];
//    }
//
    [self openApp:appId moduleName:moduleName properties:properties];
    if (successBlock) {
        successBlock(nil);
    }
}

- (void)launchWithRouterId:(int)routerId from:(UIViewController*)controller animated:(BOOL)animated title:(NSString*)title {
    [ZPCenterRouter launchWithRouterId:routerId from:controller animated:animated];
}

- (NSDictionary *)getUserInfosWith:(NSInteger)appId {
    return [ZPReactNativeAppRouter getUserInfosWithAppId:appId];
    //return [ZPWWebAppFunctionWrapper userInfosWithAppId:appId];
}
- (ReactModuleViewController *)createReactViewController:(NSDictionary *)properties {
    return [[ZPDownloadingViewController alloc] initWithProperties:properties];
    
}
- (NSArray*)getListNotificationWithLixi:(NSArray*)arrMtaid arrMtuid:(NSArray*)arrMtuid{
    ZPNotifyTable* notifyTable = [[ZPNotifyTable alloc] initWithDB:[PerUserDataBase sharedInstance]];
    return [notifyTable getListNotificationWithLixi:arrMtaid arrMtuid:arrMtuid];
}

- (void)beginSharing:(UIViewController *)parent image:(UIImage *)image withContent:(NSString *)content andLink:(NSString *)link {
    ZPSharingViewController* vc = [[ZPSharingViewController alloc] init];
    [vc beginSharingFrom:parent image:image content:content link:link];
}
    
- (void)beginSharing:(UIViewController *)parent withMessage:(NSString *)message andCaption:(NSString *)caption {
    ZPSharingViewController* vc = [[ZPSharingViewController alloc] init];
    [vc beginSharingFrom:parent message:message caption:caption];
}

- (void)shareFanpageOnFacebook:(UIViewController *)parent callback:(void(^)(NSInteger))callback {
//    NSDictionary *config = [[ApplicationState sharedInstance] zalopayConfig];
//    NSDictionary *sharingConfig = [config dictionaryForKey:@"sharing"];
    
    id<ZPSharingConfigProtocol> sharingConfig = [ZPApplicationConfig getSharingConfig];
    if (sharingConfig == nil) {
        if (callback) {
            callback(ZPSharingResult_failed);
        }
        return;
    }
//    NSString *fanpageLink = [sharingConfig stringForKey:@"facebook_fanpage" defaultValue:@""];
    NSString *fanpageLink = [sharingConfig getFacebookFanpage];
    NSMutableDictionary *message = [[NSMutableDictionary alloc] init];
    [message setValue:fanpageLink forKey:@"link"];
    [[ZPCenterSharing sharedInstance] shareFrom:parent on:ZPSharingChannel_facebook message:message callback:callback];
}

- (void)showUserProfileViewController {
    [self launchWithRouterId:RouterIdUserProfile from:nil animated:YES title:nil];

}
#pragma mark - Load Web View
- (void)launchWebApp:(NSString*)url {
    UINavigationController *nav = [self rootNavigation];
    if ([nav isKindOfClass:[UINavigationController class]]) {
        UIViewController *controller = [ZPReactNativeAppRouter getInternalWebAppWithUrl:url];
        [nav pushViewController:controller animated:true];

//        ZPWInternalWebApp *controller = [[ZPWInternalWebApp alloc] initWithUrl:url];
//        [controller setUpWebView:false showShareBtn:true];
//        [nav pushViewController:controller animated:true];
    }
}
#pragma mark - get app permission from config
- (NSArray*)getAppPermission:(NSInteger)appId {
    NSDictionary *appPermisstion = [[ZPApplicationConfig appPermission] getAppPermission];
    NSString *key = [@(appId) stringValue];
    return [appPermisstion arrayForKey: key];
}
@end
