//
//  UIScrollView+ScrollToBottom.swift
//  ZaloPay
//
//  Created by vuongvv on 9/29/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import UIKit

extension UIScrollView {
    @objc func scroll(toBottom animation: Bool) {
        self.scroll(toBottomOffset: 0, animation: animation)
    }
    
    @objc func scroll(toBottomOffset offset: CGFloat, animation: Bool) {
        let offSetY = contentSize.height - frame.size.height - offset
        if offSetY > 0 {
            setContentOffset(CGPoint(x: 0, y: offSetY), animated: animation)
        }
    }
}
