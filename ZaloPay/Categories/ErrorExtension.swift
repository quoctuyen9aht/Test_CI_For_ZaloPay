//
//  ErrorExtension.swift
//  ZaloPay
//
//  Created by bonnpv on 4/8/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import Foundation

extension Error {
    fileprivate var _error: NSError {
        return self as NSError
    }
    func errorCode() -> Int {
        return _error.code
    }
    
    func userInfoData() -> NSDictionary {
        return _error.userInfo as NSDictionary
    }
}
