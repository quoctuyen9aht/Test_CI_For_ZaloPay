//
//  UIImage+InappResource.swift
//  ZaloPay
//
//  Created by vuongvv on 9/29/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

let kPathImageInternalApp = "%@/assets/%@%@.png"
let kPathImageInternalAppThemeCashBack = "%@/assets/theme/cashback/%@/%@%@.png"
let kPathImageInternalAppThemeVoucher = "%@/assets/theme/voucher/%@/%@%@.png"
let kPathImageInternalAppThemeLiXiWishes = "%@/assets/theme/lixiwishes/%@/%@%@.png"
let kDefaultFolder = "default"
extension UIImage {
    @objc static func fromInternalApp(_ name: String) -> UIImage {
        return optionalImageFromInternalApp(name) ?? UIImage(from: .clear)
    }
    
    @objc static func fromInternalAppThemeCashBack(_ name: String, andThemeId theme: Int) -> UIImage {
        var imageGet = self.fromInternalAppPath(self.getPathInternalApp(name, andPath: kPathImageInternalAppThemeCashBack, andThemeId: "\(theme)"))
        if imageGet == nil {
            imageGet = self.fromInternalAppPath(self.getPathInternalApp(name, andPath: kPathImageInternalAppThemeCashBack, andThemeId: kDefaultFolder))
        }
        return imageGet ?? UIImage(from: .clear)
    }
    
    @objc static func fromInternalAppThemeVoucher(_ name: String, andThemeId theme: Int) -> UIImage {
        var imageGet = self.fromInternalAppPath(self.getPathInternalApp(name, andPath: kPathImageInternalAppThemeVoucher, andThemeId: "\(theme)"))
        if imageGet == nil {
            imageGet = self.fromInternalAppPath(self.getPathInternalApp(name, andPath: kPathImageInternalAppThemeVoucher, andThemeId: kDefaultFolder))
        }
        return imageGet ?? UIImage(from: .clear)
    }
    @objc static func fromInternalAppThemeLiXiWishes(_ name: String, andThemeId theme: Int) -> UIImage {
        var imageGet = self.fromInternalAppPath(self.getPathInternalApp(name, andPath: kPathImageInternalAppThemeLiXiWishes, andThemeId: "\(theme)"))
        if imageGet == nil {
            imageGet = self.fromInternalAppPath(self.getPathInternalApp(name, andPath: kPathImageInternalAppThemeLiXiWishes, andThemeId: kDefaultFolder))
        }
        return imageGet ?? UIImage(from: .clear)
    }
    static func optionalImageFromInternalApp(_ name: String) -> UIImage? {
       let url = self.getPathInternalApp(name, andPath: kPathImageInternalApp, andThemeId: "")
       return fromInternalAppResource(url)
    }
    
    private static func getPathInternalApp(_ name: String, andPath path: String, andThemeId theme: String) -> URL {
        let appModel = ReactNativeAppManager.sharedInstance().getApp(showshowAppId)
        let intenalPath: String = ReactNativeAppManager.sharedInstance().getRunFolder(appModel)
        let scale: String = UIScreen.main.scale > 2.0 ? "@3x" : "@2x"
        let pathFile = theme.count == 0 ? String(format: path, intenalPath, name, scale) : String(format: path, intenalPath, theme, name, scale)
        return URL(fileURLWithPath: pathFile)
    }
    
    private static func fromInternalAppPath(_ urlFile: URL) -> UIImage? {
        return fromInternalAppResource(urlFile)
    }
    
    private static func fromInternalAppResource(_ urlFile: URL) -> UIImage? {
        guard FileManager.default.fileExists(atPath: urlFile.path),
            let dataFile = try? Data(contentsOf: urlFile),
            let result = UIImage(data: dataFile, scale: UIScreen.main.scale) else {
                return nil
        }
        return result
    }
}
