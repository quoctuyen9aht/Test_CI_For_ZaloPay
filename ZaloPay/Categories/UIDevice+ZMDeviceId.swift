//
//  UIDevice+ZMDeviceId.swift
//  ZaloPay
//
//  Created by vuongvv on 9/29/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayCommonSwift

let kDeviceUUIDKey = "ABDKLAJSDLKJOIQUOIASD"
let kDeviceService = "123uaksjdo21"
extension UIDevice {
    @objc class func zmDeviceUUID() -> String {
        guard let value = KeyChainStore.string(forKey: kDeviceUUIDKey, service: kDeviceService) else {
            let string = NSUUID().uuidString
            KeyChainStore.set(string: string, forKey: kDeviceUUIDKey, service: kDeviceService)
            return string
        }
        return value
    }
}

