//
//  UIViewController+Toast.swift
//  ZaloPay
//
//  Created by bonnpv on 10/31/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import SnapKit

extension UIViewController {
    public func showToast(with msg: String?, delay: TimeInterval) {
        ZPToastNotifyView.showToastDefault(on: self.view, delay: delay, with: msg, completion: nil)
    }
    public func showToastNoneIcon(with msg: String?, delay: TimeInterval) {
        let config = ZPToastNotifyConfig()
        config.iconCode = ""
        config.topIcon = 0
        ZPToastNotifyView.showToast(on: self.view, delay: delay, with: msg, using: config, completion: nil)
    }
    public func showToastCustomAboveCenter(with msg: String?, delay: TimeInterval, top: Int) {
        let config = ZPToastNotifyConfig()
        ZPToastNotifyView.showToast(on: self.view, delay: delay, with: msg, using: config, update: { (view) in
            view.snp.makeConstraints({ (make) in
                make.centerX.equalTo(self.view)
                make.top.equalTo(top)
            })
        }, completion: nil)
    }
}
