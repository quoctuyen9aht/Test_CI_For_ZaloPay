//
//  Utils.swift
//  ZaloPay
//
//  Created by Dung Vu on 6/29/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import UIKit
import ZaloPayAnalyticsSwift
import ZaloPayCommonSwift

// MARK: -- Data
extension Data {
    init?(contentsOfOptional url: URL?) {
        guard let url = url else { return nil }
        try? self.init(contentsOf: url)
    }
    
    func hexEncodedString() -> String {
        return map { String(format: "%02X", UInt($0)) }.joined()
    }
}

// MARK: -- Bool
extension Bool {
    var not: Bool {
        return !self
    }
}
// MARK: -- Lazy value
private enum LazyValue<E, T> {
    case notYetComputed((E) -> T)
    case computed(T)
}

final class LazyBox<E, T> {
    init(computation: @escaping (E) -> T) {
        _value = .notYetComputed(computation)
    }
    private let queue = DispatchQueue(label: "LazyBox.value")
    private var _value: LazyValue<E, T>
    subscript(value: E) -> T {
        return queue.sync {
            switch self._value {
            case .notYetComputed(let computation):
                let result = computation(value)
                self._value = .computed(result)
                return result
            case .computed(let result):
                return result
            }
        }
    }
}

// MARK: -- UIImage
@available (iOS 10, *)
struct TextRender {
    let text: String
    let font: UIFont
    let size: CGSize
    let color: UIColor
    
    private let _metadata = LazyBox<TextRender, [NSAttributedStringKey: Any]> {
        [ .font: $0.font, .foregroundColor: $0.color]
    }
    
    private let _sizeText = LazyBox<TextRender, CGSize> {
        let metadata = $0.metadata
        return ($0.text as NSString).size(withAttributes: metadata)
    }
    
    var metadata: [NSAttributedStringKey: Any] {
        return _metadata[self]
    }
    
    var sizeText: CGSize {
        return _sizeText[self]
    }
}
extension UIImage {
    @available (iOS 10, *)
    static func draw(using infor: TextRender) -> UIImage? {
        let text = infor.text
        let size = infor.size
        let att = infor.metadata
        let sT = infor.sizeText
        let color = infor.color
        let render = UIGraphicsImageRenderer(size: size)
        let result = render.image { (context) in
            UIColor.clear.setFill()
            let rect = CGRect(x: (size.width - sT.width) / 2, y: (size.height - sT.height) / 2, width: size.width, height: size.height)
            color.setFill()
            (text as NSString).draw(in: rect, withAttributes: att)
        }
        return result
    }
    
    static func render(using data: Data, by size: CGSize) -> UIImage? {
        guard let img = UIImage(data: data) else {
            return nil
        }
        
        if #available (iOS 10, *) {
            let render = UIGraphicsImageRenderer(size: size)
            let result = render.image { (context) in
                img.draw(in: CGRect(origin: .zero, size: size))
            }
            return result
        }
        
        return img.scaled(to: size)
    }
    
    static func drawText(_ text: String, font: UIFont, size: CGSize, color: UIColor) -> UIImage? {
        if #available (iOS 10, *) {
            let textRender = TextRender(text: text, font: font, size: size, color: color)
            return self.draw(using: textRender)
        }
        
        let att: [NSAttributedStringKey: Any] = [
            .font: font,
            .foregroundColor: color
        ]
        let sT = (text as NSString).size(withAttributes: att)
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        context.setFillColor(UIColor.clear.cgColor)
        let rect = CGRect(x: (size.width - sT.width) / 2, y: (size.height - sT.height) / 2, width: size.width, height: size.height)
        color.setFill()
        (text as NSString).draw(in: rect, withAttributes: att)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
}

// MARK: -- Dictionary
infix operator +
func +<T ,V>(lhs: [T: V], rhs: [T: V]) -> [T: V] {
    var lhs = lhs
    rhs.forEach({ lhs[$0.key] = $0.value })
    return lhs
}

extension Dictionary where Key == String {
    /// Find value by path (note only use for dict [String: Any])
    ///
    /// - Parameters:
    ///   - path: this is string that has format: "key1.key2.key3"
    ///   - defaultValue: value default if it doesn't find
    /// - Returns: value

    func value<T>(forPath path: String, defaultValue: @autoclosure () -> T) -> T {
        guard self.keys.count > 0 else {
            return defaultValue()
        }
        var allNode = path.components(separatedBy: ".")
        guard allNode.count > 0 else { return defaultValue() }
        // condition
        var temp: JSON = self
        defer {
            temp.removeAll()
            allNode.removeAll()
        }
        let totalNode = allNode.count - 1
        finding: for node in allNode.enumerated() {
            if node.offset == totalNode {
                // Happy case
                return temp.value(forKey: node.element, defaultValue: defaultValue)
            }
            guard let next = temp[node.element] as? JSON else { break finding }
            temp = next
        }
        return defaultValue()
    }
}
// MARK: -- String
extension String{
    func remove(at str: String? , options: String.CompareOptions = [], range: Range<String.Index>? = nil) -> String {
        guard self.isEmpty.not else {
            return self
        }
        //Validate
        guard let str = str , str.isEmpty.not else  {
            return self
        }
        return self.replacingOccurrences(of: str, with: "", options: options, range: range)
    }
    
    mutating func remove(_ str: String? , options: String.CompareOptions = [], range: Range<String.Index>? = nil) {
        self = self.remove(at: str, options: options, range: range)
    }
 
    func getFirstCharacters(numWords: Int) -> String {
        guard (numWords <= 0).not else {
            return ""
        }
        let words = self.trimmingCharacters(in: .whitespacesAndNewlines).split(separator: " ")
        let n = words.compactMap({ $0.first }).map({ String($0) }).joined()
        let startIdx = n.startIndex
        let endIdx = n.endIndex
        guard let end = n.index(startIdx, offsetBy: numWords, limitedBy: endIdx) else {
            return n
        }
        return String(n[startIdx..<end])
    }
    
    func capitalizingFirstLetter() -> String {
        let str = lowercased()
        return prefix(1).uppercased() + str.dropFirst()
    }
    
    func toBool() -> Bool? {
        switch self {
        case "True", "true", "yes", "1":
            return true
        case "False", "false", "no", "0":
            return false
        default:
            return nil
        }
    }
}

extension String {
    subscript(i: Int) -> Character? {
        guard let idx = index(self.startIndex, offsetBy: i, limitedBy: self.endIndex) else {
            return nil
        }
        return self[idx]
    }
}

// MARK: -- Date
extension Date {
    func string(using format: String = "yyyyMMddHHmm") -> String {
        let formater = DateFormatter()
        formater.dateFormat = format
        return formater.string(from: self)
    }
}

// MARK: -- RACSignal
fileprivate func asObservable<E>(_ signal: RACSignal<E>) -> Observable<E?> {
    return Observable.create({ (s) -> Disposable in
        let racDisposable = signal.subscribeNext({
            s.onNext($0)
        }, error: { (e) in
            s.onError(e ?? NSError())
        }, completed: {
            s.onCompleted()
        })
        return Disposables.create {
            racDisposable.dispose()
        }
    })
}

prefix operator ~
prefix func ~<E>(s: RACSignal<E>?) -> Observable<E?> {
    guard let s = s else { return Observable.empty() }
    return asObservable(s)
}

postfix operator ~
postfix func ~<E>(o:Observable<E>) -> RACSignal<E> {
    return RACSignal.createSignal({ (s) -> RACDisposable? in
        let disposeAble = o.subscribe(onNext: {
            s.sendNext($0)
        }, onError: {
            s.sendError($0)
        }, onCompleted: {
            s.sendCompleted()
        })
        return RACDisposable(block: {
            disposeAble.dispose()
        })
    })
}

func event<E, F>(from signal: RACSignal<E>?,_ transform: @escaping (E?) throws -> F?) -> Observable<F?> {
    return (~signal).map({ try transform($0) })
}

// Ignore value
func eventIgnoreValue<E>(from signal: RACSignal<E>?) -> Observable<Void> {
    return (~signal).map({ _ in })
}


// MARK: -- Observable
extension ObservableType {
    static func eventLoading(with observable: Observable<E>) -> Observable<E> {
        return Driver.just("loading").do(onNext: { (_) in
                ZPAppFactory.sharedInstance().showHUDAdded(to: nil)
            }).asObservable()
            .flatMap({ _ in return observable })
            .observeOn(MainScheduler.instance)
            .do(onDispose: {
                ZPAppFactory.sharedInstance().hideAllHUDs(for: nil)
        })
    }
    
    // This 'll not emit anything, only return complete (ignore error)
    func ignoreElements() -> Observable<E> {
        return self.catchError({ _ in return Observable<E>.empty()}).filter({ _ in return false })
    }
}

public protocol OptionalType {
    associatedtype Wrapped
    var value: Wrapped? { get }
}
extension Optional: OptionalType {
    public var value: Wrapped? {
        return self
    }
}

extension ObservableType where E: OptionalType {
    typealias F = E.Wrapped
    func filterNil() -> Observable<F>{
        return self.flatMap { element -> Observable<F> in
            guard let value = element.value else {
                return Observable<F>.empty()
            }
            return Observable<F>.just(value)
        }
    }
}


infix operator ~>
func ~><E>(lhs: E, binding: Binder<E>) -> Disposable {
    return Driver.just(lhs).drive(binding)
}

private struct ActivityToken<E> : ObservableConvertibleType, Disposable {
    private let _source: Observable<E>
    private let _dispose: Cancelable
    
    init(source: Observable<E>, disposeAction: @escaping () -> ()) {
        _source = source
        _dispose = Disposables.create(with: disposeAction)
    }
    
    func dispose() {
        _dispose.dispose()
    }
    
    func asObservable() -> Observable<E> {
        return _source
    }
}

/**
 Enables monitoring of sequence computation.
 
 If there is at least one sequence computation in progress, `true` will be sent.
 When all activities complete `false` will be sent.
 */
public class ActivityIndicator : SharedSequenceConvertibleType {
    public typealias E = Bool
    public typealias SharingStrategy = DriverSharingStrategy
    
    private let _lock = NSRecursiveLock()
    private let _variable = Variable(0)
    private let _loading: SharedSequence<SharingStrategy, Bool>
    
    public init() {
        _loading = _variable.asDriver()
            .map { $0 > 0 }
            .distinctUntilChanged()
    }
    
    fileprivate func trackActivityOfObservable<O: ObservableConvertibleType>(_ source: O) -> Observable<O.E> {
        return Observable.using({ () -> ActivityToken<O.E> in
            self.increment()
            return ActivityToken(source: source.asObservable(), disposeAction: self.decrement)
        }) { t in
            return t.asObservable()
        }
    }
    
    private func increment() {
        _lock.lock()
        _variable.value = _variable.value + 1
        _lock.unlock()
    }
    
    private func decrement() {
        _lock.lock()
        _variable.value = _variable.value - 1
        _lock.unlock()
    }
    
    public func asSharedSequence() -> SharedSequence<SharingStrategy, E> {
        return _loading
    }
}

extension ObservableConvertibleType {
    public func trackActivity(_ activityIndicator: ActivityIndicator) -> Observable<E> {
        return activityIndicator.trackActivityOfObservable(self)
    }
}
//
//// MARK: - Tracker
//extension UIViewController: ZPTrackerProtocol {
//    public func trackEvent(_ eventId: ZPAnalyticEventAction, value eventValue: NSNumber?, label eventLabel: String?) {
//        ZPTrackingHelper.shared().eventTracker?.trackEvent(eventId, value: eventValue, label: eventLabel)
//    }
//    
//    public func trackEvent(_ eventId: ZPAnalyticEventAction, valueString eventValue: String?, label eventLabel: String?) {
//        ZPTrackingHelper.shared().eventTracker?.trackEvent(eventId, valueString: eventValue, label: eventLabel)
//    }
//    
//    public func trackEvent(_ eventId: ZPAnalyticEventAction, value eventValue: NSNumber?) {
//        let valueString = (eventValue as? Int)?.toString()
//        ZPTrackingHelper.shared().eventTracker?.trackEvent(eventId, valueString: valueString, label: nil)
//    }
//    
//    public func trackEvent(_ eventId: ZPAnalyticEventAction) {
//        ZPTrackingHelper.shared().trackEvent(eventId)
//    }
//    
//    public func trackTiming(_ eventId: ZPAnalyticEventAction, value eventValue: NSNumber?) {}
//    
//    public func trackScreen(_ screenName: String?) {
//        let nameTracking = screenName ?? "\(type(of: self))"
//        ZPTrackingHelper.shared().eventTracker?.trackScreen(nameTracking)
//    }
//    
//    public func trackCustomEvent(_ eventCategory: String?, action eventAction: String?, label eventLabel: String?, value eventValue: NSNumber?) {
//        ZPTrackingHelper.shared().eventTracker?.trackCustomEvent(eventCategory, action: eventAction, label: eventLabel, value: eventValue)
//    }
//}

// MARK: - UITableViewCell && UICollectionViewCell
@objc protocol ReusableBagProtocol {
    func prepareForReuse()
}
extension UITableViewCell: ReusableBagProtocol{}
extension UICollectionViewCell: ReusableBagProtocol{}
fileprivate struct Bag {
    static var name = "Bag"
}
extension ReusableBagProtocol where Self: NSObject {
    fileprivate func resetBag() {
        objc_setAssociatedObject(self, &Bag.name, nil, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
    var rxDisposeBag: DisposeBag! {
        guard let bag = objc_getAssociatedObject(self, &Bag.name) as? DisposeBag else {
            // Create new
            let newBag = DisposeBag()
            objc_setAssociatedObject(self, &Bag.name, newBag, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            // Will Reset If Reuse
            self.rx.methodInvoked(#selector(prepareForReuse)).takeUntil(self.rx.deallocating).bind(onNext: { [weak self](_) in
                self?.resetBag()
            }).disposed(by: newBag)
            return newBag
        }
        return bag
    }
}


// MARK: - Controller
protocol MakeControllerProtocol: class {}
extension UIViewController: MakeControllerProtocol {}
extension MakeControllerProtocol where Self:UIViewController {
    /// Create controller from storyboard
    ///
    /// - Parameters:
    ///   - name: name storyboard, default is Main
    ///   - identify: identify in storyboard if nil it'll use default controller
    ///   - bundle: bundler that include storyboard
    /// - Returns: new controller
    static func loadFromStoryBoard(with name: String = "Main",
                                   _ identify: String? = nil,
                                   at bundle: Bundle? = nil) -> Self {
        let storyBoard = UIStoryboard(name: name, bundle: bundle)
        let controller: UIViewController?
        // case identify not nil
        if let identify = identify , identify.isEmpty.not {
            controller = storyBoard.instantiateViewController(withIdentifier: identify)
        } else {
            controller = storyBoard.instantiateInitialViewController()
        }
        
        guard let result = controller as? Self else {
            fatalError("Error create controller")
        }
        
        return result
    }
}

extension UIViewController {
    func popMe(animated animate: Bool = true) {
        guard let navi = self.navigationController, let idx = navi.viewControllers.index(of: self) else {
            return
        }
        // Check if at last will ignore
        if navi.viewControllers.count == (idx + 1) {
            return
        }
        navi.popToViewController(self, animated: animate)
    }
}

// MARK: - Enum
protocol EnumCollectionProtocol: Hashable {}
extension EnumCollectionProtocol {
    typealias E = Self
    typealias Result = Array<E>
    static func allCases() -> Result {
        var raw = 0
        let generator = AnyIterator({ () -> E? in
            let current = withUnsafePointer(to: &raw) {
                $0.withMemoryRebound(to: E.self, capacity: 1) { $0.pointee }
            }
            guard current.hashValue == raw else { return nil }
            raw += 1
            return current
        })
        
        return Result(generator)
    }
}

// MARK: - DispatchQueue
extension DispatchQueue {
    static func getCurrentThreadLabel() -> String? {
        let name = __dispatch_queue_get_label(nil)
        return String(cString: name, encoding: .utf8)
    }
    
    static func executeInMainThread(_ block: @escaping () ->()) {
        let currentThreadName = self.getCurrentThreadLabel()
        guard currentThreadName == self.main.label else {
            self.main.async { block() }
            return
        }
        block()
    }
}
// MARK: - Array
extension Array {
    subscript (safe index: Int) -> Element? {
        return indices ~= index ? self[index] : nil
    }
    
    mutating func add(element: Element?) {
        guard let e = element else { return }
        self.append(e)
    }
}

// MARK: - Finding FirstResponder
func findingFirstResponder(from v: UIView?) -> UIView? {
    guard let v = v else {
        return nil
    }
    if v.isFirstResponder {
        return v
    }
    
    for childView in v.subviews {
        if childView.isFirstResponder {
            return childView
        }else if let nView = findingFirstResponder(from: childView) {
            return nView
        }
    }
    return nil
}

// MARK: - Catch Exception from Objc
func catchException(in block: @escaping () ->()) throws {
    let e = NSException.try {
        block()
    }
    guard let exception = e else {
        return
    }
    let error = NSError(domain: exception.name.rawValue, code: NSURLErrorUnknown, userInfo: exception.userInfo as? JSON)
    throw error
}

// MARK: - Cell
protocol CellFactoryProtocol {}
extension CellFactoryProtocol where Self: UITableViewCell {
    
    /// Create a tableview cell
    ///
    /// - Parameters:
    ///   - tableView: table that registerd cell with a identifier
    ///   - identifier: identifier for reuse cell
    /// - Returns: cell ,in case it can't make a cell -> move to fatal to check
    static func make(from tableView: UITableView, identifier: String? = nil) -> Self {
        let identifier = identifier ?? "\(self)"
        guard let result = tableView.dequeueReusableCell(withIdentifier: identifier) as? Self else {
            fatalError("Check identifier cell")
        }
        return result
    }
}

extension CellFactoryProtocol where Self: UICollectionViewCell {
    /// Create a collectionview cell
    ///
    /// - Parameters:
    ///   - collectionView: collectionView that registerd cell with a identifier
    ///   - identifier: identifier for reuse cell
    /// - Returns: cell ,in case it can't make a cell -> move to fatal to check
    static func make(from collectionView: UICollectionView, identifier: String? = nil, idx: IndexPath) -> Self {
        let identifier = identifier ?? "\(self)"
        guard let result = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: idx) as? Self else {
            fatalError("Check identifier cell")
        }
        return result
    }
}
extension UITableViewCell: CellFactoryProtocol{}
extension UICollectionViewCell: CellFactoryProtocol{}


// MARK: - Number Currency
// Only appear value in this file
fileprivate let currencyFormatter: NumberFormatter = {
    let new: NumberFormatter = NumberFormatter.init()
    new.locale = Locale.init(identifier: "vi-VN")
    new.numberStyle = .currency
    new.currencySymbol = "VND"
    return new
}()

extension NSNumber {
    
    /// Format a value to string currncy
    ///
    /// - Returns: string (example: 1 VND)
    func formatValueCurrency() -> String? {
        return currencyFormatter.string(from: self)
    }
}

extension Numeric {
    /// Change number value money, don't need to cast value , you can use like : 1.currency , 1.1.currency , any kind of number
    var currency: String? {
        return (self as? NSNumber)?.formatValueCurrency()
    }
}

extension UILabel {
    func setLineSpacing(lineSpacing: CGFloat = 0.0, lineHeightMultiple: CGFloat = 0.0) {
        guard let labelText = self.text else { return }
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.lineHeightMultiple = lineHeightMultiple
        let nAtt = NSAttributedString(string: labelText, attributes: [.paragraphStyle : paragraphStyle])
        self.attributedText += nAtt
    }
}
