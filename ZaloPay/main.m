//
//  main.m
//  ZaloPay
//
//  Created by tinvukhac on 12/7/15.
//  Copyright © 2015-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZALOPAY-Swift.h"



int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ZPAppDelegate class]));
    }
}
