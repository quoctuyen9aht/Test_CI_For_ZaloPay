//
//  NetworkManager+Payment.h
//  ZaloPay
//
//  Created by bonnpv on 4/26/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//


/*
 - appID : 7
 - key 1 : E45rmFdzRyOkdI8
 - key 2 : dUSvvr9onsBzoDX
 */
@interface NetworkManager (Order)
//- (RACSignal *)createOrder:(NSString *)transactionId
//                   appUser:(NSString *)appUser
//                   appTime:(NSString *)appTime
//                      item:(NSString *)item
//                 embedData:(NSString *)embedData
//                    amount:(long)amount
//               description:(NSString *)description
//                 payOption:(int)payOption
//                       mac:(NSString *)mac;

- (RACSignal *)getOrderFromAppId:(NSInteger)appid transToken:(NSString *)transToken ;

- (RACSignal *)getWalletOrdederWithAmount:(long)amount
                       andTransactionType:(int)transactionType
                                  appUser:(NSString *)appUser
                              description:(NSString *)description
                                    appId:(NSInteger)appId
                                embeddata:(NSString *)embeddata
                                     item:(NSString*)item;

- (RACSignal *)getZaloPayId:(NSString *)zaloId;

- (RACSignal *)getListZaloPayId:(NSArray *)zaloIds;
- (RACSignal *)getListZaloPayInfoFromPhones:(NSArray *)phones;
@end
