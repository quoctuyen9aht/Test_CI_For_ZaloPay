//
//  NetworkManager+Wallet.h
//  ZaloPay
//
//  Created by bonnpv on 5/4/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//


@interface NetworkManager (Wallet)
- (RACSignal *)getBalance;
- (RACSignal *)getListCardInfo;
- (RACSignal *)getProfileLevel;
@end
