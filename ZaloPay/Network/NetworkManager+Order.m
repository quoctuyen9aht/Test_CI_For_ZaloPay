//
//  NetworkManager+Payment.m
//  ZaloPay
//
//  Created by bonnpv on 4/26/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "NetworkManager+Order.h"
#import <ZaloPayNetwork/ZPNetWorkApi.h>
#import <ZaloPayCommon/NSCollection+JSON.h>
#import <ZaloPayNetwork/NSError+ZaloPayAPI.h>
#import <ZaloPayNetwork/ZaloPayErrorCode.h>
#import <ZaloPayCommon/ZPConstants.h>
#import <ZaloPayConfig/ZaloPayConfig-Swift.h>
#import ZALOPAY_MODULE_SWIFT

#define kGetListPhoneAsyncTimeOut    30.0

@implementation NetworkManager (Order)

- (RACSignal *)getOrderFromAppId:(NSInteger)appid transToken:(NSString *)transToken {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:transToken forKey:@"zptranstoken"];
    [params setObjectCheckNil:@(appid) forKey:@"appid"];
    return [self requestWithPath:api_v001_tpe_getorderinfo parameters:params];
}

//- (RACSignal *)createOrder:(NSString *)transactionId
//                   appUser:(NSString *)appUser
//                   appTime:(NSString *)appTime
//                      item:(NSString *)item
//                 embedData:(NSString *)embedData
//                    amount:(long)amount
//               description:(NSString *)description
//                 payOption:(int)payOption
//                       mac:(NSString *)mac {
//    
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setObjectCheckNil:transactionId forKey:@"apptransid"];
//    [params setObjectCheckNil:appUser forKey:@"appuser"];
//    [params setObjectCheckNil:appTime forKey:@"apptime"];
//    [params setObjectCheckNil:item forKey:@"item"];
//    [params setObjectCheckNil:embedData forKey:@"embeddata"];
//    [params setObjectCheckNil:@(amount) forKey:@"amount"];
//    [params setObjectCheckNil:description forKey:@"description"];
//    [params setObject:@(payOption) forKey:@"payoption"];
//    [params setObjectCheckNil:mac forKey:@"mac"];
//    
//    return [self requestWithPath:api_v001_tpe_createorder parameters:params];
//}

- (RACSignal *)getWalletOrdederWithAmount:(long)amount
                       andTransactionType:(int)transactionType
                                  appUser:(NSString *)appUser
                              description:(NSString *)description
                                    appId:(NSInteger)appId
                                embeddata:(NSString *)embeddata
                                     item:(NSString*) item {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@(amount) forKey:@"amount"];
    [params setObject:@(transactionType) forKey:@"transtype"];
    [params setObjectCheckNil:appUser forKey:@"appuser"];
    [params setObjectCheckNil:description forKey:@"description"];
    [params setObjectCheckNil:@(appId) forKey:@"appid"];
    [params setObjectCheckNil:embeddata forKey:@"embeddata"];
    [params setObjectCheckNil:item forKey:@"item"];
    return [self requestWithPath:api_v001_tpe_createwalletorder parameters:params];
}

- (RACSignal *)getZaloPayId:(NSString *)zaloId {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:zaloId forKey:@"loginuid"];
    [params setObjectCheckNil:@(1) forKey:@"systemlogin"];
    return [self requestWithPath:api_um_getuserinfo parameters:params];
}

- (RACSignal *)getListZaloPayId:(NSArray *)zaloIds {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:[zaloIds componentsJoinedByString:@","] forKey:@"zaloidlist"];
    return [self requestWithPath:api_um_checklistzaloidforclient parameters:params];
}

- (RACSignal *)getListZaloPayInfoFromPhones:(NSArray *)phones {
//    NSDictionary *dicAPI = [ApplicationState getDictionaryConfigFromDic:@"api" andKey:@"call_api_async" andDefault:[NSDictionary new]];
//    BOOL enableAsync = [dicAPI boolForKey:@"get_list_user_by_phone" defaultValue:false];
//    return enableAsync ? [self getListZaloPayInfoFromPhonesUseAsync:phones] : [self getListZaloPayInfoFromPhonesSync:phones];
    
    BOOL enableAsync = [[[ZPApplicationConfig getApiConfig] getCallApiAsync] getCanGetListUserByPhone];
    return enableAsync ? [self getListZaloPayInfoFromPhonesUseAsync:phones] : [self getListZaloPayInfoFromPhonesSync:phones];
}

- (RACSignal *)getListZaloPayInfoFromPhonesUseAsync:(NSArray *)phones {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [[self  getListZaloPayInfoFromPhonesAsync:phones] subscribeNext:^(NSDictionary* result) {
            [subscriber sendNext:result];
            [subscriber sendCompleted];
        } error:^(NSError *error) {
            if ([error isApiError] == false) {
                [subscriber sendError:error];
                return;
            }
            NSDictionary *responseData = error.userInfo;
            uint64_t requestId = [responseData uint64ForKey:@"requestid"];
            if (requestId <= 0 || error.code <= 0) {
                [subscriber sendError:error];
                return;
            }
            [self getStatusProcessingAsync:requestId subscriber:subscriber startDate:[NSDate date]];
        }];
        return nil;
    }];
}
- (RACSignal *)getListZaloPayInfoFromPhonesSync:(NSArray *)phones {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:[phones componentsJoinedByString:@","] forKey:@"phonelist"];
    return [self requestWithPath:api_um_listusersbyphoneforclient parameters:params];
}
- (RACSignal *)getListZaloPayInfoFromPhonesAsync:(NSArray *)phones {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:[phones componentsJoinedByString:@","] forKey:@"phonelist"];
    return [self requestWithPath:api_um_listusersbyphoneforclientasync parameters:params];
}

- (void)getStatusProcessingAsync:(uint64_t)requestId
                      subscriber:(id<RACSubscriber>)subscriber
                       startDate:(NSDate*)startDate {
    
    NSDictionary *params = @{@"requestid" : @(requestId)};
    
    [[self requestWithPath:api_um_getrequeststatusforclient parameters:params] subscribeNext:^(NSDictionary* result) {
        NSDictionary *dictionary = [NSDictionary castFrom:[[result stringForKey:@"data"] JSONValue]];
        [subscriber sendNext:dictionary];
        [subscriber sendCompleted];
    } error:^(NSError *error) {
        if ([error isApiError] == false) {
            [subscriber sendError:error];
            return;
        }
        if ([[NSDate date] timeIntervalSinceDate:startDate] > kGetListPhoneAsyncTimeOut) {
            [subscriber sendError:error];
            return;
        }
        NSDictionary *responseData = error.userInfo;
        uint64_t rqId = [responseData uint64ForKey:@"requestid"];
        if (rqId <= 0 || error.code <= 0) {
            [subscriber sendError:error];
            return;
        }
        [self getStatusProcessingAsync:rqId subscriber:subscriber startDate:startDate];
    }];
}
@end
