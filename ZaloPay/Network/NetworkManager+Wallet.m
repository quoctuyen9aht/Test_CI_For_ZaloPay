//
//  NetworkManager+Wallet.m
//  ZaloPay
//
//  Created by bonnpv on 5/4/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "NetworkManager+Wallet.h"
#import <ZaloPayNetwork/ZPNetWorkApi.h>
@implementation NetworkManager (Wallet)
- (RACSignal *)getBalance {
    return [self requestWithPath:api_v001_tpe_getbalance parameters:nil];
}

- (RACSignal *)getListCardInfo {
    return [self requestWithPath:api_um_listcardinfo parameters:nil];
}

- (RACSignal *)getProfileLevel {
   return [self requestWithPath:api_um_getuserprofilelevel parameters:nil];
}

@end
