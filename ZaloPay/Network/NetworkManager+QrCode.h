//
//  NetworkManager+QrCode.h
//  ZaloPay
//
//  Created by vuongvv on 10/20/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

@interface NetworkManager (QrCode)

- (RACSignal *)getPaymentCode:(int)pmcid
                              chargeinfo:(NSString *)chargeinfo
                              count:(int)count
                              remaincode:(int)remaincode;
- (RACSignal *)verifyPinQrcodePay:(NSString *)transid
                          pin:(NSString *)pin
                         time:(NSTimeInterval)time;
- (RACSignal *)getStatusTransId:(NSString *)transid
                          appid:(int)appid
                      startDate:(NSDate*)startDate;
@end
