//
//  NetworkManager+QrCode.m
//  ZaloPay
//
//  Created by vuongvv on 10/20/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import "NetworkManager+QrCode.h"
#import "ZPPaymentCodeModel.h"
#import "ZPPaymentCodeResult.h"
#import <ZaloPayNetwork/ZPNetWorkApi.h>
#import <ZaloPayConfig/ZaloPayConfig-Swift.h>
//#import "ApplicationState.h"
//#import "ApplicationState+GetConfig.h"
#import ZALOPAY_MODULE_SWIFT

@implementation NetworkManager (QrCode)
- (RACSignal *)getPaymentCode:(int)pmcid
                       chargeinfo:(NSString *)chargeinfo
                        count:(int)count
                remaincode:(int)remaincode
{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:chargeinfo forKey:@"chargeinfo"];
    [params setObject:@(pmcid) forKey:@"pmcid"];
    [params setObject:@(count) forKey:@"count"];
    [params setObject:@(remaincode) forKey:@"remaincode"];
    return [self requestWithPath:api_v001_tpe_qr_getpaymentcode parameters:params];
}

- (RACSignal *)verifyPinQrcodePay:(NSString *)transid
                              pin:(NSString *)pin
                             time:(NSTimeInterval)time{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:transid forKey:@"zptransid"];
    [params setObjectCheckNil:pin forKey:@"pin"];
    [params setObject:@(round(time*1000)) forKey:@"time"];
    return [self requestWithPath:api_v001_tpe_qr_verifypinqrcodepay parameters:params];
}

- (RACSignal *)getStatusTransId:(NSString *)transid
                     appid:(int)appid
                 startDate:(NSDate*)startDate{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:transid forKey:@"zptransid"];
    [params setObjectCheckNil:@(appid) forKey:@"appid"];
    
    return [[[self creatRequestWithPath:api_v001_tpe_gettransstatus params:params isGet:NO] flattenMap:^__kindof RACSignal * _Nullable(NSDictionary  *_Nullable response) {
        ZPPaymentCodeResult *result = [[ZPPaymentCodeResult alloc] initWith:response];
        
        //Still processing
        if (result.isprocessing) {
            DDLogInfo(@"Response : %@", response);
            return [RACSignal error:[NSError errorWithDomain:NSCocoaErrorDomain code:NSURLErrorBadServerResponse userInfo:response]];
        }
        
        return [RACSignal return:result];
    }] catch:^RACSignal *(NSError *error) {
        
        NSDictionary *response = error.userInfo;
        ZPPaymentCodeResult *result = [[ZPPaymentCodeResult alloc] initWith:response];
        
        if (![response objectForKey:@"isprocessing"]) {
            return [RACSignal error:error];
        }
        
        if (result.isprocessing) {
            
            NSDate *newDate = [NSDate date];
            NSTimeInterval t = [newDate timeIntervalSinceDate:startDate];
        
//            int timeout = [ApplicationState getIntConfigFromDic:@"quickpay" andKey:@"transaction_status_timeout" andDefault:5000] / 1000;
            id<ZPQuickPayConfigProtocol> quickPayConfig = [ZPApplicationConfig getQuickPayConfig];
            NSInteger timeout = quickPayConfig ? [quickPayConfig getTransactionStatusTimeout] : 5000;
            timeout /= 1000;
            
            //Retry
            if (t < timeout) {
                RACSignal *retry = [[[RACSignal interval:1 onScheduler:[RACScheduler scheduler]] take:1] flattenMap:^__kindof RACStream *_Nullable(id value) {
                    // Retry
                    return [self getStatusTransId:transid appid:appid startDate:startDate];
                }];
                
                return retry;
            }
            //Error
            else {
                //timeout
                NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:error.userInfo];
                [userInfo setValue:R.string_QuickPay_GetStatus_Timeout forKey:@"returnmessage"];
                return [RACSignal error:[[NSError alloc] initWithDomain:@"Time out"
                                   code:NSURLErrorTimedOut
                               userInfo:userInfo]];
            }
        }
        else {
            return result.returncode == 1 ? [RACSignal return:result] : [RACSignal error:error];
        }
    }];
}

@end
