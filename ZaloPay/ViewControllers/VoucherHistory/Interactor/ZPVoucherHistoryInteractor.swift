//
//  ZPVoucherHistoryInteractor.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 9/5/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

class ZPVoucherHistoryInteractor: ZPVoucherHistoryInteractorInputProtocol {
    weak var presenter: ZPVoucherHistoryInteractorOutputProtocol?
    var arrVoucherHistories = [ZPVoucherHistory]()
    
    func retrieveZPVoucherHistory() {
        let manager = ZaloPayWalletSDKPayment.sharedInstance().zpVoucherManager
       _ = manager?.loadAllAvailableVoucher().deliverOnMainThread().subscribeNext({ [weak self](vouchers) in
            let allVoucher = vouchers as! [ZPVoucherHistory]
            self?.arrVoucherHistories = allVoucher
            self?.presenter?.didRetrieveVoucherHistories(allVoucher)
            },error:({ [weak self] (error) in
                self?.presenter?.onError(error)
            }))
        
    }
    
    func searchWithText(_ text: String) {
        let arraySearch = NSMutableArray()
        if text.isEmpty {
            arraySearch.addObjects(from: self.arrVoucherHistories)
        }else {
            let arrVouchers = NSArray(array: self.arrVoucherHistories)
            let filter = "vouchercode CONTAINS[cd] %@ || voucherHistoryDescription CONTAINS[cd] %@"
            let predicate = NSPredicate(format: filter,(text.ascci() ?? "").uppercased() ,(text.ascci() ?? "").uppercased())
            let result = arrVouchers.filtered(using: predicate)
            arraySearch.addObjects(from: result)
        }
        self.presenter?.resultSearch(arraySearch)
    }
    
}
