//
//  ZPVoucherHistoryRouter.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 9/5/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayWeb

@objcMembers
public class ZPVoucherHistoryRouter:NSObject, ZPVoucherHistoryRouterProtocol {
    public class func createZPVoucherHistoryModule() -> UIViewController {
        let navController = mainStoryboard.instantiateViewController(withIdentifier: "ZPVoucherHistoryListViewControllerNav")
        if let view = navController.childViewControllers.first as? ZPVoucherHistoryListViewController {
            let presenter: ZPVoucherHistoryPresenterProtocol & ZPVoucherHistoryInteractorOutputProtocol = ZPVoucherHistoryPresenter()
            let interactor: ZPVoucherHistoryInteractorInputProtocol = ZPVoucherHistoryInteractor()
//            let localDataManager: ZPVoucherHistoryLocalDataManagerInputProtocol = ZPVoucherHistoryLocalDataManager()
//            let remoteDataManager: ZPVoucherHistoryRemoteDataManagerInputProtocol = ZPVoucherHistoryRemoteDataManager()
            let router: ZPVoucherHistoryRouterProtocol = ZPVoucherHistoryRouter()
            
            view.presenter = presenter
            presenter.view = view 
            presenter.router = router
            presenter.interactor = interactor
            interactor.presenter = presenter
//            interactor.localDatamanager = localDataManager
//            interactor.remoteDatamanager = remoteDataManager
            //remoteDataManager.remoteRequestHandler = interactor
            
            return view
        }
        return UIViewController()
    }
    
    static var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "VoucherHistory", bundle: Bundle.main)
    }
    
    func presentDetailScreen(from view: ZPVoucherHistoryListViewProtocol, voucher : ZPVoucherHistory) {
        guard voucher.detail_url.count > 0 else { return  }
        if let sourceView = view as? UIViewController {
            let web = ZPWWebViewController(url: voucher.detail_url, webHandle: ZPWebHandle())
            web.titleView.isHidden = true
            sourceView.navigationController?.pushViewController(web, animated: true)
        }
    }
    
}
