//
//  ZPVoucherHistoryPresenter.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 9/5/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
class ZPVoucherHistoryPresenter: ZPVoucherHistoryPresenterProtocol {

    weak var view: ZPVoucherHistoryListViewProtocol?
    var interactor: ZPVoucherHistoryInteractorInputProtocol?
    var router: ZPVoucherHistoryRouterProtocol?
    
    func viewDidLoad() {
        view?.showLoading()
        interactor?.retrieveZPVoucherHistory()
    }
    func searchWithText(_ text: String) {
        interactor?.searchWithText(text)
    }
    func reloadData(){
        interactor?.retrieveZPVoucherHistory()
    }
    func showDetailVoucher(voucher : ZPVoucherHistory) {
        router?.presentDetailScreen(from: view!, voucher: voucher)
    }
}

extension ZPVoucherHistoryPresenter: ZPVoucherHistoryInteractorOutputProtocol {
    
    func didRetrieveVoucherHistories(_ voucherHistories: [ZPVoucherHistory]) {
        view?.hideLoading()
//        view?.showPosts(with: posts)
        view?.showVoucherHistories(with: voucherHistories)
    }
    func resultSearch(_ listSearchView:NSMutableArray) {
        view?.hideLoading()
        view?.resultDataSearch(listSearchView)
    }
    func onError(_ error:Error?) {
        view?.hideLoading()
        view?.showError(error)
    }
    
}
