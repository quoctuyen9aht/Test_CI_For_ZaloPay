//
//  ZPVoucherHistoryProtocol.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 9/5/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

protocol ZPVoucherHistoryListViewProtocol: class {
    var presenter: ZPVoucherHistoryPresenterProtocol? { get set }
    
    // PRESENTER -> VIEW
    func showVoucherHistories(with voucherHistories: [ZPVoucherHistory])
    func resultDataSearch(_ listSearchView:NSMutableArray)
    
    func showError(_ error:Error?)
    
    func showLoading()
    
    func hideLoading()
}

protocol ZPVoucherHistoryRouterProtocol: class {
    static func createZPVoucherHistoryModule() -> UIViewController
    // PRESENTER -> WIREFRAME
    func presentDetailScreen(from view: ZPVoucherHistoryListViewProtocol, voucher : ZPVoucherHistory)
}

protocol ZPVoucherHistoryPresenterProtocol: class {
    var view: ZPVoucherHistoryListViewProtocol? { get set }
    var interactor: ZPVoucherHistoryInteractorInputProtocol? { get set }
    var router: ZPVoucherHistoryRouterProtocol? { get set }
    
    // VIEW -> PRESENTER
    func viewDidLoad()
    func searchWithText(_ text:String)
    func reloadData()
    func showDetailVoucher(voucher : ZPVoucherHistory)
}

protocol ZPVoucherHistoryInteractorOutputProtocol: class {
    // INTERACTOR -> PRESENTER
    func didRetrieveVoucherHistories(_ voucherHistories: [ZPVoucherHistory])
    func resultSearch(_ listSearchView:NSMutableArray)
    func onError(_ error:Error?)
}

protocol ZPVoucherHistoryInteractorInputProtocol: class {
    var presenter: ZPVoucherHistoryInteractorOutputProtocol? { get set }
    // PRESENTER -> INTERACTOR
    func retrieveZPVoucherHistory()
    func searchWithText(_ text:String)
}


