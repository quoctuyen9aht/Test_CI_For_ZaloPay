//
//  ZPVoucherHistorySearchBar.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 9/6/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//


import UIKit

private let boder:CGFloat = 12
private let iconWidth:CGFloat = 20
private let heightBtnChangeKeyboard:CGFloat = 31
private let widthBtnChangeKeyboard:CGFloat = 39


class ZPVoucherHistorySearchBar: UIView {
    
    var textField: UITextFieldWithClearButton?
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupView() {
        self.backgroundColor = UIColor.zaloBase()
        self.textField = UITextFieldWithClearButton(frame: CGRect(
            x: boder,
            y: 0,
            width: self.frame.size.width - boder * 2,
            height: self.frame.size.height - boder)
        )
        self.textField?.backgroundColor = .white
        self.textField?.layer.cornerRadius = 2
        self.textField?.text = ""
        self.textField?.placeholder = R.string_Voucher_title_view()
        self.textField?.font = UIFont.sfuiTextRegular(withSize: 16)
        self.addSubview(self.textField!)
        
        let separatorView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 1.0/UIScreen.main.scale))
        separatorView.backgroundColor = UIColor.lightGray
        self.textField?.inputAccessoryView = separatorView
        
    }
    
   
    
}

