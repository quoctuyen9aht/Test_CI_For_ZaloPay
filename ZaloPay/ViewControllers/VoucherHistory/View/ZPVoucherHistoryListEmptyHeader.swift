//
//  ZPVoucherHistoryListEmptyHeader2.swift
//  ZaloPay
//
//  Created by vuongvv on 9/14/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

class ZPVoucherHistoryListEmptyHeader: UICollectionReusableView {
    @IBOutlet weak var imv: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var btnUpdate: UIButton!
    
    var isServerMaintenance = false {
        didSet {
            self.loadContent()
        }
    }
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    override func awakeFromNib() {
        self.label.font = UIFont.sfuiTextRegular(withSize: UIView.isScreen2x() ? 14 : 16)
        self.label.textColor = UIColor(hexValue:0xACB3BA)
        self.label.sizeToFit()
        
        self.btnUpdate.setupZaloPay()
        self.btnUpdate.layer.cornerRadius = self.btnUpdate.frame.size.height/2
        
    }
    
    func loadContent() {
        let imgName = self.isServerMaintenance ? "ico_maintenance" : "historylist_empty"
        let img = UIImage(named: imgName)
        imv.image = img
        let textLabel = self.isServerMaintenance ? R.string_Voucher_server_maintenance() : R.string_Voucher_empty()
        self.label.text = textLabel
        //self.btnUpdate.isHidden = self.isServerMaintenance
    }
}
