//
//  ZPVoucherHistoryCollectionViewCell.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 9/5/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
//protocol ZPVoucherHistoryCollectionViewCellDelegate: class {
//    func useVoucher(_ voucher: ZPVoucherHistory,_ cell:ZPVoucherHistoryCollectionViewCell)
//}
class ZPVoucherHistoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var viewBorder: UIView!
    @IBOutlet weak var imageViewVoucher: UIImageView!
    @IBOutlet weak var imageViewBGBottomVoucher: UIImageView!
    @IBOutlet weak var labelVoucherExpired: UILabel!
    @IBOutlet weak var labelVoucherValue: UILabel!
    var zpVoucherHistory: ZPVoucherHistory?
//    weak var zpVoucherHistoryCollectionViewCellDelegate: ZPVoucherHistoryCollectionViewCellDelegate?
    @IBOutlet weak var iconTime :ZPIconFontImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        contentView.backgroundColor = UIColor.white
        
        labelVoucherValue.font = UIFont.sfuiTextMedium(withSize: 36)
        labelVoucherExpired.font = UIFont.sfuiTextRegular(withSize: 13)
        
        iconTime.setIconFont("general_timevoucher")
        iconTime.setIconColor(UIColor(hexValue: 0x727f8c))
        labelVoucherExpired.textColor = UIColor(hexValue:0x727f8c)
        
        viewBorder.layer.cornerRadius = 3
        viewBorder.layer.masksToBounds = true
        viewBorder.layer.borderWidth = 1
        viewBorder.layer.borderColor = UIColor(hexValue: 0xe3e6e7).cgColor

        imageViewBGBottomVoucher.contentMode = UIViewContentMode.scaleToFill
        imageViewVoucher.contentMode = UIViewContentMode.scaleToFill
    }
    
    func showData(_ zpVoucherHistory: ZPVoucherHistory) {
        self.zpVoucherHistory = zpVoucherHistory
        labelVoucherValue.text = self.zpVoucherHistory?.valueDisplayCollectionViewCell
        labelVoucherExpired.text = self.zpVoucherHistory?.countExpiredDateDisplay
        imageViewBGBottomVoucher.image = UIImage.fromInternalApp("app_ios_voucher_giftlist_bg")
        imageViewVoucher.image = UIImage.defaultAvatar()
        if !zpVoucherHistory.avatar.isEmpty {
            self.imageViewVoucher.sd_setImage(with: URL(string: zpVoucherHistory.avatar))
        }
        
    }
    override func prepareForReuse() {
        self.imageViewVoucher.sd_cancelCurrentImageLoad()
    }
    
//    @IBAction func buttonUseVoucherTouch(_ sender: Any) {
//        zpVoucherHistoryCollectionViewCellDelegate?.useVoucher(self.zpVoucherHistory!,self)
//    }
}
