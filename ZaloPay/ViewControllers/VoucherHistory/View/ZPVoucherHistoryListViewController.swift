//
//  ZPVoucherHistoryListViewController.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 9/5/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import ZaloPayAnalyticsSwift

@objcMembers
class ZPVoucherHistoryListViewController: BaseViewController {
    
    var presenter: ZPVoucherHistoryPresenterProtocol?
    var voucherHistories: [ZPVoucherHistory] = []
    let disposeBag = DisposeBag()
    @IBOutlet weak var collectionView: UICollectionView!
    var searchView:ZPVoucherHistorySearchBar?
    let reactAppRouter = ZPReactNativeAppRouter()
    
    let kZPVoucherHistoryListEmptyHeader = "ZPVoucherHistoryListEmptyHeader"
    var isFinishLoad = false
    var isServerMaintenance = false
    var refreshControl: UIRefreshControl?
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        presenter?.viewDidLoad()
        refreshControl = UIRefreshControl()
        refreshControl?.attributedTitle = NSAttributedString(string: R.string_ButtonLabel_Upgrade())
        refreshControl?.addTarget(self, action: #selector(reloadData), for: UIControlEvents.valueChanged)
        collectionView.addSubview(refreshControl!)
        //self.setupSearchBar()
    }
    
    override func screenName() -> String! {
        return ZPTrackerScreen.Screen_iOS_Promotion_VoucherDetailPage
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ZPTrackingHelper.shared().trackScreen(with: self)
    }
    
    //MARK: Internal Method
//    func createSearchEmptyView() -> SearchEmptyView {
//        let w = self.collectionView.frame.size.width
//        let frame = CGRect(x: 0, y: 40, width: w, height: self.view.frame.size.height)
//        let view = SearchEmptyView(frame: frame,imageName: "search_empty", textLabel: R.string_ZPC_Search_Empty())
//        return view
//    }
//    func setupSearchBar() {
//        self.searchEmptyView = self.createSearchEmptyView()
//        self.view.addSubview(self.searchEmptyView!)
//        self.searchEmptyView?.isHidden = true
//        self.searchView = ZPVoucherHistorySearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
//        self.view.addSubview(searchView!)
//        searchView?.textField?.addTarget(self, action: #selector(resignSearchText), for:.editingDidEndOnExit)
//        
//        self.setupSearchSignal()
//        
//    }
    
//    func setupSearchSignal () {
//        
//        searchView?.textField?.clearButton.rx.tap.bind {[weak self] _ in
//            self?.searchView?.textField?.text = ""
//            self?.searchWithText("")
//            }.addDisposableTo(self.disposeBag)
//        
//        
//        searchView?.textField?.rx.value.asObservable().throttle(0.3, scheduler: MainScheduler.instance).filter({ (text) -> Bool in
//            return text != nil
//        }).subscribe(onNext: { [weak self] text in
//            self?.searchWithText(text!)
//        }).addDisposableTo(self.disposeBag)
//    }
    
    //MARK: observer Keyboard
//    func keyBoardWillShow(notification: Notification) {
//        
//        var keyboardRectangle = CGRect.zero
//        
//        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
//            keyboardRectangle = keyboardFrame.cgRectValue
//            let keyboardHeight = keyboardRectangle.height
//            self.collectionView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: keyboardHeight, right: 0)
//        }
//    }
//    func observerKeyboard() {
//        let disappear = self.rx.sentMessage(#selector(self.viewWillDisappear(_:)))
//        let keyboardShow = NotificationCenter.default.rx.notification(.UIKeyboardWillShow)
//        let keyboardHide = NotificationCenter.default.rx.notification(.UIKeyboardWillHide)
//        _ = keyboardHide.takeUntil(disappear).subscribe(onNext: { [weak self] notification in
//            self?.keyBoardWillHide(notification: notification)
//        })
//        _ = keyboardShow.takeUntil(disappear).subscribe(onNext: { [weak self] notification in
//            self?.keyBoardWillShow(notification: notification)
//        })
//    }
//    
//    func keyBoardWillHide(notification: Notification) {
//        self.collectionView.contentInset = UIEdgeInsets.zero
//    }
//    
//    //MARK: Search
//    func searchWithText(_ text:String) {
//        self.searchView?.textField?.clearButton.isHidden = text.isEmpty
//        presenter?.searchWithText(text)
//    }
//    
//    func resignSearchText() {
//        self.searchView?.textField?.resignFirstResponder()
//    }
    @objc func reloadData(){
        self.presenter?.reloadData()
    }
    
}

extension ZPVoucherHistoryListViewController: ZPVoucherHistoryListViewProtocol {
    
    func showVoucherHistories(with voucherHistories:[ZPVoucherHistory]) {
        self.loadCollectionView(voucherHistories)
    }
    
    func resultDataSearch(_ listSearchView:NSMutableArray) {
//        if let result = listSearchView as? [ZPVoucherHistory] {
//            self.loadCollectionView(result)
//        }
    }
    
    func loadCollectionView(_ datas: [ZPVoucherHistory]) {
        self.voucherHistories = datas
        //self.collectionView.isHidden = self.voucherHistories.count == 0
        //self.searchEmptyView?.isHidden = !self.collectionView.isHidden
//        if self.voucherHistories.count > 0 {
//            self.collectionView.reloadData()
//        }
        
        isFinishLoad = true
        self.collectionView.reloadData()
    }
    
    func showError(_ error: Error?) {
        let code = error?.errorCode() ?? 0
        if code == ZALOPAY_ERRORCODE_SERVER_MAINTENANCE.rawValue {
            self.isServerMaintenance = true
        } else {
            ZPDialogView .showDialogWithError(error, handle: nil)
        }
        self.loadCollectionView([ZPVoucherHistory]())
    }
    
    func showLoading() {
        ZPAppFactory.sharedInstance().showHUDAdded(to: self.view)
    }
    
    func hideLoading() {
        ZPAppFactory.sharedInstance().hideAllHUDs(for: self.view)
        refreshControl?.endRefreshing()
    }
    
}
extension ZPVoucherHistoryListViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: Float = Float((collectionView.frame.size.width - 12 * 3) / 2)
        let scale = UIScreen.main.bounds.width <= 320 ? Float(1.5) : Float(1.25)
        return CGSize(width: CGFloat(width), height: CGFloat(width * scale))
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return voucherHistories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: "ZPVoucherHistoryCollectionViewCell", for: indexPath) as? ZPVoucherHistoryCollectionViewCell {
            let data = voucherHistories[indexPath.row]
            cell.showData(data)
            //cell.zpVoucherHistoryCollectionViewCellDelegate = self
            return cell
        }
        return UICollectionViewCell()
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return isFinishLoad && voucherHistories.count == 0 ? self.collectionView.frame.size : CGSize.zero
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
            
        case UICollectionElementKindSectionHeader:
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: kZPVoucherHistoryListEmptyHeader, for: indexPath) as! ZPVoucherHistoryListEmptyHeader
            headerView.isServerMaintenance = self.isServerMaintenance
            headerView.btnUpdate.addTarget(self, action: #selector(reloadData), for: UIControlEvents.touchUpInside)
            return headerView
            
        default:
            return UICollectionReusableView()
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let voucher = voucherHistories[indexPath.row] as ZPVoucherHistory? {
            self.presenter?.showDetailVoucher(voucher: voucher)
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.me_giftlist_touch_detail)
        }
    }
}

//extension ZPVoucherHistoryListViewController: ZPVoucherHistoryCollectionViewCellDelegate {
//    func useVoucher(_ voucher: ZPVoucherHistory,_ cell:ZPVoucherHistoryCollectionViewCell) {
//        if Int(voucher.useconditonappid) != -1 {
//            reactAppRouter.openExternalApp(Int(voucher.useconditonappid), from: self)
//        }
//    }
//}

