//
//  ZPWebAppConfigs.swift
//  ZaloPay
//
//  Created by thi la on 11/20/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayConfig

class ZPWebAppConfigs {
    
    func userInfos(appId: Int) -> [String] {
        return appConfigs(appId: appId)?.getUserInfo() ?? []
    }
    
    func merchantName(appId: Int) -> String {
        return appConfigs(appId: appId)?.getMerchantName() ?? ""
    }
    
    func appConfigs(appId: Int) -> ZPExternalAppDisclaimerConfigProtocol? {
        let externalApps = ZPApplicationConfig.getDisclaimerConfig()?.getExternalApps()
        let appConfig = externalApps?.first(where: { $0.getAppId() == appId })
        return appConfig
    }
}
