//
//  ZPWalletActionAccountView.swift
//  ZaloPay
//
//  Created by tienlv on 4/13/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayCommonSwift
class ZPWalletActionAccountView: UIView {

    var accountLabelTitle: UILabel!
    var accountLabelValue: UILabel!
    var accountLine: ZPLineView!
    var accountButton: UIButton!
    
    weak var accountProtocol: ZPWalletActionAccountProtocol!
    
    override init(frame: CGRect) {
        super .init(frame: frame)
        self.backgroundColor = .white
        self.accountLabelTitle = UILabel()
        self.addSubview(self.accountLabelTitle)
        self.accountLabelTitle.text = R.string_WalletAction_ZaloPayId()
        self.accountLabelTitle.textColor = UIColor.subText()
        self.accountLabelTitle.font = UIFont.sfuiTextRegularMedium()
        
        self.accountLabelValue = UILabel()
        self.addSubview(self.accountLabelValue)
        self.accountLabelValue.text = ""
        self.accountLabelValue.textColor = UIColor.defaultText();
        self.accountLabelValue.font = UIFont.sfuiTextRegularMedium()
        self.accountLabelValue.textAlignment = .right
        self.accountLabelTitle.isUserInteractionEnabled = false
        self.accountLabelValue.isUserInteractionEnabled = false
        self.accountLine = ZPLineView();
        self.addSubview(self.accountLine)
        self.setupLayout()
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        self.init(coder: aDecoder)
    }
    func setupLayout() {
        let button = UIButton(type: UIButtonType.custom)
        self.addSubview(button)
        button.setBackgroundColor(UIColor.selected(), for: UIControlState.highlighted)
        button.addTarget(self, action:  #selector(accountButtonClicked), for: .touchUpInside)
        self.sendSubview(toBack: button)
        self.accountButton = button
        
        self.accountLabelTitle.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.width.equalTo(200)
            make.top.equalTo(0)
            make.bottom.equalTo(0)
        }
        
        self.accountLabelValue.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.bottom.equalTo(0)
            make.right.equalTo(-10)
            make.left.equalTo(self.snp.centerX)
        }
        
        button.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.top.equalTo(0)
            make.bottom.equalTo(0)
        }
        
        self.accountLine.alignToBottom()
    }
    
    @objc func accountButtonClicked() {
        self.accountProtocol.updateAccountNameClick()
    }
    
    func isUserInteractionEnabled(enable: Bool) {
        self.accountButton.isUserInteractionEnabled = enable
    }
    
    func setAccountName(name: String) {
        self.accountLabelValue?.text = name
    }
}
