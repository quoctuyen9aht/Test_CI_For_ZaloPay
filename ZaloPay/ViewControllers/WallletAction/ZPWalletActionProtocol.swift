//
//  ZPWalletActionProtocol.swift
//  ZaloPay
//
//  Created by tienlv on 4/12/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import Foundation
import UIKit

protocol ZPWalletActionViewProtocol: class {
    func showBalance(balance: String)
    func addActionView(WithItems items:[ZPWalletActionEntity])
    func updateAccountName(accountName: String)
}

protocol ZPWalletActionPresenterProtocol: class {
    func actionClick(withTag: Int)
    func moreInfoButtonClick()
    func loadData()
    func updateAccountNameClick()
    func historyButtonClick()
}

protocol ZPWalletActionRouterProtocol: class {
    func openViewController(viewController: UIViewController)
    func showWithdraw(fromViewController controller: UIViewController)
}

protocol ZPWalletActionInteractorProtocol: class {
    func fetchData() -> [ZPWalletActionEntity]
    func registerBalance()
    func getBalance() -> String
    func getAccountName() -> String
}

protocol ZPWalletActionInteractorCallBackProtocol: class {
    func reloadBalance()
}

protocol ZPWalletActionAccountProtocol: class {
    func updateAccountNameClick()
}

