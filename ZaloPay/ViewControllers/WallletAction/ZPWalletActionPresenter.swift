//
//  ZPWalletActionPresenter.swift
//  ZaloPay
//
//  Created by tienlv on 4/13/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import CocoaLumberjackSwift
import ZaloPayProfile
import ZaloPayAnalyticsSwift

class ZPWalletActionPresenter: NSObject, ZPWalletActionPresenterProtocol {

    weak var view: ZPWalletActionViewProtocol?
    var interactor: ZPWalletActionInteractorProtocol?
    var router: ZPWalletActionRouterProtocol?
    let disposeBag = DisposeBag()
    
    convenience init?(with view: ZPWalletActionViewProtocol?) {
        guard let v = view else {
            return nil
        }
        self.init()
        self.view = v
        router = ZPWalletActionRouter()
        interactor = ZPWalletActionInteractor(with: self)
        registerBalance()
    }
    
    func registerBalance() {
        self.interactor?.registerBalance()
    }
    
    func actionClick(withTag tag: Int) {
        let controller = self.view as? UIViewController
        switch tag {
        case WalletActionType.WalletActionTypeRecharge.rawValue:
            ZPCenterRouter.launch(routerId: .recharge, from: controller)
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.balance_touch_addcash)
        case WalletActionType.WalletActionTypeWithdraw.rawValue:                        
            ZPCenterRouter.launch(routerId: .withdraw, from: controller)
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.balance_touch_withdraw)
        default:
            #if DEBUG
                fatalError("Please check for this case")
            #endif
        }
    }
    
    func moreInfoButtonClick() {
        if let help = ZPReactNativeAppRouter().supportCenterViewController() {
            self.router?.openViewController(viewController: help)
        }        
    }
    
    func loadData() {
        let data = self.interactor?.fetchData()
        reloadBalance()
        self.view?.addActionView(WithItems: data!)
        self.view?.updateAccountName(accountName: (self.interactor?.getAccountName())!)
    }
    
    func reloadBalance() {
        let str = self.interactor?.getBalance() ?? ""
        self.showBalance(balance: str)
    }

    func updateAccountNameClick() {
        guard let accountNameChecked = ZPProfileManager.shareInstance.userLoginData?.accountName else {
            return
        }
        if (accountNameChecked.count > 0) {
            return
        }
        let controller = UpdateAcountNameViewController()
        controller.delegate = self
        self.router?.openViewController(viewController: controller)
    }
    
    func historyButtonClick() {
        guard let viewController = ZPReactNativeAppRouter().listTransactionHistoryViewController() else{
            return
        }
        self.router?.openViewController(viewController: viewController)
    }
}

extension ZPWalletActionPresenter: ZPWalletActionInteractorCallBackProtocol {
    func showBalance(balance: String) {
        self.view?.showBalance(balance: balance)
    }
}

extension ZPWalletActionPresenter: UpdateAcountNameViewControllerDelegate
{
    func updateAccountNameCompleteSuccess(_ controller: UpdateAcountNameViewController!){
        self.view?.updateAccountName(accountName: (self.interactor?.getAccountName())!)
    }
}
