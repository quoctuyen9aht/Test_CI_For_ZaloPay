//
//  ZPWalletActionInteractor.swift
//  ZaloPay
//
//  Created by tienlv on 4/12/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayProfile

class ZPWalletActionInteractor: NSObject, ZPWalletActionInteractorProtocol {

    private weak var presenter: ZPWalletActionInteractorCallBackProtocol?
    
    convenience init(with presenter: ZPWalletActionInteractorCallBackProtocol) {
        self.init()
        self.presenter = presenter
    }
    
    func fetchData() -> [ZPWalletActionEntity]{
        var items = [ZPWalletActionEntity]()
        if (ZPWalletManagerSwift.sharedInstance.enableRecharge) {
            let recharge = ZPWalletActionEntity(imageName: "app_recharge",
                                                title: R.string_WalletAction_Rechager(),
                                                desc:R.string_WalletAction_Rechager_Description(),
                                                type: WalletActionType.WalletActionTypeRecharge,
                                                color: UIColor.recharge())
            items .append(recharge)
        }
        let withdraw = ZPWalletActionEntity(imageName: "app_withdraw",
                                            title: R.string_WalletAction_Withdraw(),
                                            desc: R.string_WalletAction_Withdraw_Description(),
                                            type: WalletActionType.WalletActionTypeWithdraw,
                                            color: UIColor.withdraw())
        items.append(withdraw)
        return items
    }
    
    func registerBalance() {
        ZMEventManager.registerHandler(self, eventType: EventTypeUpdateBalance.rawValue) { [weak self](_) in
            // Update
            guard let wSelf = self else { return }
            DispatchQueue.main.async {
                wSelf.presenter?.reloadBalance()
            }
        }
    }
    
    
    func getBalance() -> String {
        let balanceValue = ZPWalletManagerSwift.sharedInstance.currentBalance
        let result = balanceValue?.formatMoneyValue()
        return result ?? ""
    }
    
    func getAccountName() ->String {
        guard let u = ZPProfileManager.shareInstance.userLoginData else {
            return ""
        }
        return u.accountName
    }
}
