//
//  ZPWalletActionEntity.swift
//  ZaloPay
//
//  Created by tienlv on 4/12/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit

enum WalletActionType: Int {
    case WalletActionTypeWithdraw = 0
    case WalletActionTypeRecharge = 1
}

class ZPWalletActionEntity {

    var imageName: String?
    var title: String?
    var desc: String?
    var type: WalletActionType?
    var color: UIColor?
    
    init(imageName: String, title: String, desc:String, type: WalletActionType, color: UIColor) {
        self.imageName = imageName
        self.title = title
        self.desc = desc
        self.type = type
        self.color = color
    }
    
    init() {
        
    }
}
