//
//  ZPWalletActionHeaderView.swift
//  ZaloPay
//
//  Created by tienlv on 4/13/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit


protocol ZPWalletActionHeaderViewDelegate {
    func processUpdateZaloPayId()
}
class ZPWalletActionHeaderView: UIView {

    var headerImageView: ZPIconFontImageView?
    var headerLabelTitle: UILabel?
    var headerBalance: UILabel?
    var imageBottom: UIImageView?
    var btnUpdateID: UIButton?
    var lblZaloPayID: UILabel?
    var delegate: ZPWalletActionHeaderViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
//        super.backgroundColor = UIColor.zaloBase()
        self.backgroundColor = UIColor.white
        self.clipsToBounds = true
        if (UIScreen.main.applicationFrame.size.height <= 568)
        {
            if (UIScreen.main.applicationFrame.size.height < 500){
                imageBottom = UIImageView(frame: CGRect.zero)
            }
            else
            {
                imageBottom = UIImageView(frame: CGRect(x: 0, y: self.frame.size.height - (self.frame.size.width * 0.24), width: self.frame.size.width, height: self.frame.size.width * 0.405))
            }
        }
        else
        {
            imageBottom = UIImageView(frame: CGRect(x: 0, y: self.frame.size.height - (self.frame.size.width * 0.24), width: self.frame.size.width, height: self.frame.size.width * 0.405))
        }
        
        imageBottom?.clipsToBounds = true
        imageBottom?.contentMode = .scaleAspectFill
        imageBottom?.image = UIImage(named: "cover_large.jpg")
        self.addSubview(imageBottom!)
        
        self.headerImageView = ZPIconFontImageView(frame: CGRect(x: 0, y: 0, width: 68, height: 68))
        self.headerImageView?.setIconFont("header_overbalance")
        self.headerImageView?.setIconColor(UIColor.zaloBase())
        self.addSubview(self.headerImageView!)
        
        self.headerBalance = UILabel()
        if (UIScreen.main.applicationFrame.size.height < 500) {
            self.headerBalance?.font = UIFont.sfuiTextRegular(withSize: 50)
        }
        else
        {
            self.headerBalance?.font = UIFont.sfuiTextRegularBalanceValue()
        }
        self.addSubview(self.headerBalance!)
        self.headerBalance?.textAlignment = .center
        self.headerBalance?.textColor = UIColor.zaloBase()
        
        lblZaloPayID = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.size.width - 40 , height: 20))
        lblZaloPayID?.textAlignment = .center
        lblZaloPayID?.font = UIFont.defaultSFUITextRegular()
        lblZaloPayID?.textColor = UIColor.subText()
        lblZaloPayID?.isHidden = true
        self.addSubview(lblZaloPayID!)

        btnUpdateID = UIButton(frame: CGRect(x: 0, y: 0, width: 229 , height: 20))
        btnUpdateID?.addTarget(self, action: #selector(processUpdateZaloPayID), for: .touchUpInside)
        self.addSubview(btnUpdateID!)
        let icon: String = UILabel.iconCode(withName: "general_arrowright") ?? ""
        let titleNormal = String(format: "%@ %@", R.string_WalletAction_Withdraw_Not_Update_ZaloPayID(), "\(icon)")
        let r = (titleNormal as NSString).range(of: icon)
        let attN = NSMutableAttributedString(string: titleNormal, attributes: [NSAttributedStringKey.foregroundColor : UIColor.zaloBase(),NSAttributedStringKey.font:UIFont.defaultSFUITextRegular()])
        
        if let f = UIFont.zaloPay(withSize: 14) , r.length > 0 {
            attN.addAttributes([NSAttributedStringKey.font : f], range: r)
        }
        
        btnUpdateID?.setAttributedTitle(attN, for: .normal)
        btnUpdateID?.isHidden = false
        self.setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @objc func processUpdateZaloPayID()
    {
        delegate?.processUpdateZaloPayId()
    }
    
    func setupLayout() {
        let imageTopOffset = 30
//        if (UIScreen.main.applicationFrame.size.height < 500) {
//            imageTopOffset = 0
//        }
        self.headerImageView?.snp.makeConstraints { (make) in
            make.top.equalTo(imageTopOffset)
            make.centerX.equalTo(self.snp.centerX)
            make.width.equalTo(68)
            make.height.equalTo(68)
        }
        
        self.btnUpdateID?.snp.makeConstraints { (make) in
            make.top.equalTo((self.headerImageView?.snp.bottom)!).offset(74)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(20)
        }
        
        self.lblZaloPayID?.snp.makeConstraints { (make) in
            make.top.equalTo((self.headerImageView?.snp.bottom)!).offset(74)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(20)
        }
        
        self.headerBalance?.snp.makeConstraints { (make) in
            make.top.equalTo((self.headerImageView?.snp.bottom)!).offset(10)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(58)
        }
    }
    
    func setBalance(balance: String) {
        self.headerBalance?.text = balance
    }


}
