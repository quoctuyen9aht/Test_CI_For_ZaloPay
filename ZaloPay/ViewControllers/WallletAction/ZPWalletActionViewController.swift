//
//  ZPWalletActionViewController.swift
//  ZaloPay
//
//  Created by tienlv on 4/12/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit
import ZaloPayCommonSwift
import ZaloPayAnalyticsSwift
import ZaloPayConfig

@objc class ZPWalletActionViewController: BaseViewController {

    lazy var presenter: ZPWalletActionPresenterProtocol? = ZPWalletActionPresenter(with: self)
    var actionBackground: UIView!
    var buttonMoreInfo: UIButton!
    var headerView: ZPWalletActionHeaderView!
    var accountView: ZPWalletActionAccountView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = R.string_WalletAction_Title()
        self.view.backgroundColor = .white
        self.setupView()
        self.presenter?.loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ZPTrackingHelper.shared().trackScreen(with: self)
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.balance_launched)
    }
    
    override func screenName() -> String! {
        return ZPTrackerScreen.Screen_iOS_Balance
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
   
    override func backButtonClicked(_ sender: Any!) {
        super.backButtonClicked(sender)
    }
    
    override func backEventId() -> Int {
        return ZPAnalyticEventAction.balance_touch_back.rawValue;
    }
    
    func setupNavigationBar() {
        let button = UIButton(type: UIButtonType.custom)
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont.iconFont(withSize: 22)
        button.setIconFont("general_history", for:UIControlState.normal)
        button.addTarget(self, action:  #selector(historyButtonClick), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: -10, width: 40, height: 40)
        let barButton = UIBarButtonItem.init(customView: button)
        let fixedSpace = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = -10
        self.navigationItem.rightBarButtonItems = [fixedSpace, barButton]
    }
    
    func setupView() {
        self.addHeaderView()
//        self.addAcountView()
//        self.setupButtonMoreInfo()
    }

    func getHeaderViewHeight() -> Int {
        var height = 0
        
        if (UIScreen.main.applicationFrame.size.height < 500) {
            // iPhone 4 (s)
            height = 225
        } else if (UIScreen.main.applicationFrame.size.height <= 568) {
            // iPhone 5 (s)
            height = 295
        } else {
            // default height for large screen on iPhone 6/7 (S+)
            height = 320
        }
        
        return height
    }
    
    func addHeaderView() {
        let height = self.getHeaderViewHeight()
        let width = UIScreen.main.applicationFrame.size.width
        self.headerView = ZPWalletActionHeaderView(frame: CGRect(x: 0, y: 0, width: Int(width), height: height))
        headerView.delegate = self
        self.view.addSubview(self.headerView)
    }
    
    func setupButtonMoreInfo() {
        self.buttonMoreInfo = UIButton(type: UIButtonType.system)
        self.buttonMoreInfo.setTitle(R.string_WalletAction_MoreAbout(), for: UIControlState.normal)
        self.buttonMoreInfo.setImage(UIImage(named: "wallet_help"), for: UIControlState.normal)
        self.buttonMoreInfo.titleLabel?.font =  UIFont.sfuiTextRegularSmall()
        self.view.addSubview(self.buttonMoreInfo)
        self.buttonMoreInfo.snp.makeConstraints { (make) in
            make.bottom.equalTo(6)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(56)
        }
       self.buttonMoreInfo.addTarget(self, action:  #selector(moreInfoButtonClick), for: .touchUpInside)
    }
    
    func addActionView(action:ZPWalletActionEntity, actionIndex:Int, isLastIndex:Bool ) {
        let cellHeight = 74
        let topOffset = actionIndex*cellHeight
        let horizontalButton = HorizontalButton(imageName: action.imageName!, title: action.title!, desc:action.desc!, color: action.color!)
        self.actionBackground.addSubview(horizontalButton)
        horizontalButton.setTag(tag: (action.type?.rawValue)!)
        horizontalButton.addBottomLine()
        
        horizontalButton.addTarget(self, action:  #selector(didTapButton(button:)), for: .touchUpInside)
        horizontalButton.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(cellHeight)
            make.top.equalTo(topOffset)
        }
    }
    
    @objc func didTapButton(button:UIButton) {
        self.presenter?.actionClick(withTag: button.tag)
    }
    
    @objc func moreInfoButtonClick() {
        self.presenter?.moreInfoButtonClick()
    }
    
    @objc func historyButtonClick() {        
        self.presenter?.historyButtonClick()
    }
}

extension ZPWalletActionViewController: ZPWalletActionViewProtocol {
    func showBalance(balance: String) {
        self.headerView.setBalance(balance: balance)
    }

    func addActionView(WithItems items:[ZPWalletActionEntity]) {
        let cellHeight = 74
        self.actionBackground = UIView()
        self.view.addSubview(self.actionBackground)
        self.actionBackground.backgroundColor = .white
        
        self.actionBackground.snp.makeConstraints { (make) in
            make.top.equalTo(self.headerView.snp.bottom).offset(0)
            make.height.equalTo(items.count*cellHeight)
            make.left.equalTo(0)
            make.right.equalTo(0)
        }
        
        let topLine = ZPLineView()
        self.actionBackground.addSubview(topLine)
        let bottomLine = ZPLineView()
        self.actionBackground.addSubview(bottomLine)
        topLine.alignToTop()
        bottomLine.alignToBottom()
        for index in 0..<items.count  {
            self.addActionView(action: items[index], actionIndex: index, isLastIndex: (index==items.count-1))
        }
    }
    func updateAccountName(accountName: String) {
        
        //Check config
//        let enableZaloPayId = ApplicationState.getBoolConfig(fromDic: "tab_me", andKey: "enable_register_profile_zalopayid", andDefault: false)
        let enableZaloPayId = ZPApplicationConfig.getTabMeConfig()?.getEnableRegisterProfileZalopayId().toBool() ?? false
        
        if !enableZaloPayId {
            headerView.btnUpdateID?.isHidden = true
            headerView.lblZaloPayID?.isHidden = true
            return
        }
        
        if accountName.count > 0 {
            headerView.btnUpdateID?.isHidden = true
            headerView.lblZaloPayID?.isHidden = false
            headerView.lblZaloPayID?.text = String(format:"%@: %@", R.string_Transfer_ZaloPayId(), accountName)
        }
        else {
            headerView.lblZaloPayID?.isHidden = true
            headerView.btnUpdateID?.isHidden = false
        }
    }
}

extension ZPWalletActionViewController: ZPWalletActionHeaderViewDelegate {
    func processUpdateZaloPayId() {
        self.presenter?.updateAccountNameClick()
    }
}





