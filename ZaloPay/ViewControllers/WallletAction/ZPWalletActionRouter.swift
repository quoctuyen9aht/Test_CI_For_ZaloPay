//
//  ZPWalletActionRouter.swift
//  ZaloPay
//
//  Created by tienlv on 4/12/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

@objcMembers
class ZPWalletActionRouter: NSObject, ZPWalletActionRouterProtocol {
    func showWithdraw(fromViewController controller: UIViewController) {
        ZPCenterRouter.launch(routerId: .withdraw, from: controller)
    }
    
    func openViewController(viewController: UIViewController) {
        ZPAppFactory.sharedInstance().rootNavigation()?.pushViewController(viewController, animated: true)
    }
    
    class func assembleModule() ->  ZPWalletActionViewController {
        let viewController = ZPWalletActionViewController()
        return viewController
    }
}
