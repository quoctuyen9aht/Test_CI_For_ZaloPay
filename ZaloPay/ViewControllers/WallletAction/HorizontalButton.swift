//
//  HorizontalButton.swift
//  ZaloPay
//
//  Created by tienlv on 4/13/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit
import ZaloPayCommonSwift

class HorizontalButton: UIView {

    var button: UIButton!
    
    convenience init(imageName: String, title: String, desc:String, color: UIColor) {
        
        self.init(frame: CGRect.zero)
        
        let cellHeight: CGFloat = 74
        self.button = UIButton(type: UIButtonType.custom)
        self.addSubview(self.button)
        self.button.setBackgroundColor(UIColor.selected(), for: UIControlState.highlighted)
        
        self.button?.snp.makeConstraints({ (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.top.equalTo(0)
            make.bottom.equalTo(0)
        })
        
        let imageView = ZPIconFontImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        imageView.setIconFont(imageName)
        imageView.setIconColor(color)
        self.addSubview(imageView)
        
        let label = UILabel()
        self.addSubview(label)
        label.text = title
        label.zpMainBlackRegular()
        
        let labelDesc = UILabel()
        self.addSubview(labelDesc)
        labelDesc.text = desc
        labelDesc.zpDetailGrayStyle()
        
//        if (UIScreen.main.applicationFrame.size.height <= 568) {
//            labelDesc.font = UIFont.sfuiTextRegular(withSize: 12)
//        }
//        else
//        {
//            labelDesc.font = UIFont.sfuiTextRegular(withSize: 13)
//        }
//        
//        labelDesc.textColor = UIColor.subText()
        
        let arrow = ZPIconFontImageView(frame: CGRect(x: 0, y: 0, width: 14, height: 14))
        arrow.setIconFont("general_arrowright")
        arrow.setIconColor(UIColor.lightGray)
        self.addSubview(arrow)
        
        imageView.snp.makeConstraints { (make) in
            make.size.equalTo(imageView.frame.size)
            make.left.equalTo(10)
            make.top.equalTo((cellHeight - imageView.frame.size.width)/2)
        }
        
        label.snp.makeConstraints { (make) in
            make.left.equalTo(imageView.snp.right).offset(10)
            make.width.equalTo(200)
            make.top.equalTo(17)
            make.height.equalTo(20)
        }
        
        labelDesc.snp.makeConstraints { (make) in
            make.left.equalTo(imageView.snp.right).offset(10)
            make.width.equalTo(250)
            make.top.equalTo(42)
            make.height.equalTo(15)
        }
        
        arrow.snp.makeConstraints { (make) in
            make.right.equalTo(-10)
            make.size.equalTo(arrow.frame.size)
            make.top.equalTo ((cellHeight - arrow.frame.size.height)/2)
        }
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        self.init(coder: aDecoder)
    }
    
    func addTarget(_ target: Any?, action: Selector, for controlEvents: UIControlEvents) {
        self.button.addTarget(target, action: action, for: controlEvents)
    }
    
    func addBottomLine() {
        let lineView = ZPLineView()
        self.addSubview(lineView)
        lineView.alignToBottomWith(leftMargin: 50)
    }
    
    func setTag(tag: Int) {
        self.button.tag = tag
    }

}
