//
//  ZPProfileActionCell.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 9/22/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit

class ZPProfileActionCell: UITableViewCell {
    
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var iconImgView: ZPIconFontImageView!
    
    var isSmallScreen = false
    var notifyView: UIView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblContent.font = UIFont.sfuiTextRegular(withSize: UILabel.mainTextSize())
        self.isSmallScreen = UIScreen.main.bounds.size.width <= 320
        if (notifyView != nil) {
            notifyView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 8))
            notifyView?.backgroundColor = UIColor(fromHexString: "#FF0000")
            notifyView?.layer.cornerRadius = 4
            notifyView?.clipsToBounds = true
            lblContent.addSubview(self.notifyView!)
            self.notifyView?.isHidden = true
            notifyView?.snp.makeConstraints({ (make) in
                make.top.equalTo(-2)
                make.right.equalTo(5)
                make.size.equalTo(CGSize(width: 8, height: 8))
            })
        }
    }
    
    override func prepareForReuse() {
        super .prepareForReuse()
        lblDescription.isHidden = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setItem(_ dict : [String : Any]) {
        self.lblContent.text = dict.value(forKey: "title", defaultValue: "")
        if let icon = dict["icon"] as? [String : Any] {
            self.iconImgView.setIconFont(icon["name"] as! String)
            self.iconImgView.setIconColor(icon["color"] as! UIColor)
        }
        let type = dict["descriptionType"] as! ZPProfileActionDescriptionType
        switch type {
        case .descriptionUnknown:
            lblDescription.isHidden = true
            break
        case .descriptionVND:
            lblDescription.isHidden = false
            guard let message = dict["message"] as? String else {return}
            lblDescription.text = message
            lblDescription.font = UIFont.sfuiTextRegular(withSize: 15)
            lblDescription.textColor = UIColor(fromHexString: "#24272b")
            let split : Array = message.components(separatedBy: " ")
            let money = split[0]
            let att = NSMutableAttributedString(attributedString: lblDescription.attributedText!)
            att.addAttribute(NSAttributedStringKey.font, value: UIFont.sfuiTextRegular(withSize: isSmallScreen ? 18 : 24), range: NSMakeRange(0, money.count))
            lblDescription.attributedText = att
            break
        case .descriptionMessage:
            lblDescription.isHidden = false
            guard let message = dict["message"] as? String else {return}
            lblDescription.font = UIFont.sfuiTextRegular(withSize: 15)
            lblDescription.text = message
            lblDescription.textColor = UIColor(fromHexString: "#008fe5")
        }
        self.layoutSubviews()
    }

}

