//
//  ZPProfileLogoutCell.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 9/27/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

class ZPProfileLogoutCell: UITableViewCell {
    @IBOutlet weak var iconImg: ZPIconFontImageView!
    @IBOutlet weak var lblTextLogOut: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblTextLogOut.font = UIFont.sfuiTextRegular(withSize: UILabel.mainTextSize())
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setItem(dict: JSON) {
        lblTextLogOut.text = dict.value(forKey: "Content", defaultValue: "")
        let icon = dict.value(forKey: "icon", defaultValue: JSON())
        iconImg.setIconFont(icon.value(forKey: "name", defaultValue: ""))
        iconImg.setIconColor(icon.value(forKey: "color", defaultValue: UIColor.zaloBase()))
    }
}
