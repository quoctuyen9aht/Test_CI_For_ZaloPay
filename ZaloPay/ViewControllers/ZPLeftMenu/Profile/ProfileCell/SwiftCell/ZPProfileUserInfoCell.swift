//
//  ZPProfileUserInfoCell.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 9/27/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

class ZPProfileUserInfoCell: UITableViewCell {

    @IBOutlet weak var arrow: UIImageView!
    @IBOutlet weak var avatarImgView: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserZaloId: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        avatarImgView.layer.cornerRadius = self.avatarImgView.bounds.size.width / 2
        avatarImgView.clipsToBounds = true
        lblUserName.font = UIFont.sfuiTextRegular(withSize: UILabel.navigationSize())
        lblUserZaloId.font = UIFont.sfuiTextRegular(withSize: UILabel.subTitleSize())
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
