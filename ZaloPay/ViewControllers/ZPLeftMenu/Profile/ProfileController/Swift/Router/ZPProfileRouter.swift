//
//  ZPProfileRouter.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 9/25/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayCommonSwift
import ZaloPayAnalyticsSwift
import ZaloPayConfig
import ZaloPayWeb

@objc class ZPProfileRouter: NSObject, ZPProfileRouterProtocol {
    
    func getQuickCommentURLString() -> String {
        return ZPApplicationConfig.getTabMeConfig()?.getQuickCommentUrl() ?? ""
    }
    
    func handle(from controller: BaseViewController?, with action: ZPProfileAction) {
        if let controller = controller {
            var naviToController : UIViewController? = nil
//            let eventLabel = ZPTrackingEvent.instance.previousName
            var eventID: ZPAnalyticEventAction = .home_touch_profile
//            var requirePIN = false
            
            switch action {
            case .showInforUser:
                ZPCenterRouter.launch(routerId: .userProfile, from: controller)
                ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.me_touch_info)
//                naviToController = ZPMainProfileViewController()
//                requirePIN = true
                return
            case .showSetting:
                ZPCenterRouter.launch(routerId: .setting, from: nil)
                ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.me_touch_security)
//                naviToController = ZPSettingRouter.assemblyModule()
//                eventID = ZPAnalyticEventAction.me_touch_security
                return
            case .showRemain:
                
                let enableQuickPay = ZPApplicationConfig.getQuickPayConfig()?.getEnable().toBool() ?? false
                let routerId : RouterId = enableQuickPay ? .paymentCode : .wallet
                ZPCenterRouter.launch(routerId: routerId, from: nil)
                //ZPCenterRouter.launch(routerId: .wallet, from: nil)
                ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.me_touch_balance)
//                naviToController = ZPWalletActionRouter.assembleModule()
//                eventID = ZPAnalyticEventAction.me_touch_balance
                return
            case .addBank:
                ZPCenterRouter.launch(routerId: .linkCard, from: controller)
                ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.me_touch_bank)
//                naviToController = ZPLinkCardViewController()
//                eventID = ZPAnalyticEventAction.me_touch_bank
//                requirePIN = true
                return
            case .billWaiting:
                ZPCenterRouter.launch(routerId: .qrScan, from: nil)
//                ZPAppFactory.sharedInstance().trackEvent(ZPAnalyticEventAction.me_touch_billing)
                return
//                naviToController = QRPayScannerViewController()
//                eventID = ZPAnalyticEventAction.me_touch_billing
//                break
            case .callCenter:
                naviToController = ZPReactNativeAppRouter().supportCenterViewController()
                eventID = ZPAnalyticEventAction.me_touch_supportcenter
                ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.me_touch_supportcenter)
                break
            case .quickComment:
                let urlString = getQuickCommentURLString()
                if urlString.count > 0 {
                    naviToController = ZPWWebViewController(url: urlString, webHandle: ZPWebHandle())
                }
                eventID = ZPAnalyticEventAction.me_touch_feedback
                ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.me_touch_feedback)
                break
            case .appInfor:
                naviToController = viewController(fromTitle: R.string_LeftMenu_About(), params: ["moduleName": "About"])
                eventID = ZPAnalyticEventAction.me_touch_about
                ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.me_touch_about)
                break
            case .logOut:
                ZPDialogView.showDialog(with: DialogTypeNotification, message: R.string_Message_Signout_Confirmation(), cancelButtonTitle: R.string_ButtonLabel_Cancel(), otherButtonTitle: [R.string_LeftMenu_SigOut()], completeHandle: { (buttonIndex, cancelButtonIndex) in
                    if buttonIndex != cancelButtonIndex {
                        LoginManagerSwift.sharedInstance.logout()
                    }
                })
//                eventID = ZPAnalyticEventAction.me_touch_logout
//                ZPAppFactory.sharedInstance().trackEvent(eventID)
                return
            case .total:
                break
            case .voucherList:
                ZPCenterRouter.launch(routerId: .voucher, from: controller)
                ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.me_touch_giftlist)
                return
            }
            
            guard let navi = naviToController else { return }

            
            showViewController(in: controller, with: action, controllerShow: navi, eventID: eventID, requirePIN: false)
        }
    }
    
    func viewController(fromTitle title: String, params: JSON) -> BaseViewController {      
        var properties = params
        let zaloPayId: String = NetworkManager.sharedInstance().paymentUserId
        properties["zalopay_userid"] = zaloPayId
        
        guard let viewController = ZPDownloadingViewController(properties: properties) else {
            return BaseViewController()
        }
        viewController.title = title
        if let moduleName = params["moduleName"] as? String {
            viewController.moduleName = moduleName
        }
        return viewController
    }
    
    func showViewController(in controller: BaseViewController, with action: ZPProfileAction, controllerShow: UIViewController, eventID: ZPAnalyticEventAction, requirePIN: Bool) {
        ZPTrackingHelper.shared().trackEvent(eventID)
//        if requirePIN {
//            if controllerShow.isKind(of:  ZPLinkCardViewController.self) {
//                controller.showListCardViewControllerAnimation(true, success: nil)
//            }
//            return
//        }
        UINavigationController.currentActiveNavigationController()?.pushViewController(controllerShow, animated: true)
    }
    
}





