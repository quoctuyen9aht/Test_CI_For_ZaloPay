//
//  ZPProfileEntity.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 9/22/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayProfile
enum ZPProfileAction : Int {
    case showInforUser = 0
    case showSetting
    case showRemain
    case addBank
    case voucherList
    case billWaiting
    case callCenter
    case quickComment
    case appInfor
    case logOut
    case total
}

enum ZPProfileActionDescriptionType : Int {
    case descriptionUnknown = 0
    case descriptionVND
    case descriptionMessage
}

@objc class ZPProfileEntity: NSObject {
    var user : ZaloUserSwift?
    var dataSource = [[ZPProfileAction]]()
    
    override init() {
        super.init()
        dataSource = setupDatasource()
    }
    
    func setupDatasource() -> [[ZPProfileAction]] {
        let listApp = ReactNativeAppManager.sharedInstance().activeAppIds as? [Int] ?? []
        if listApp.count > 0 {
            let isShow  = listApp.contains(appVoucherId)
            if isShow {
                return [[ZPProfileAction.showInforUser, ZPProfileAction.showSetting],
                        [ZPProfileAction.showRemain, ZPProfileAction.addBank, ZPProfileAction.voucherList],
                        [ZPProfileAction.callCenter, ZPProfileAction.quickComment, ZPProfileAction.appInfor]]
            }
        }
        return [[ZPProfileAction.showInforUser, ZPProfileAction.showSetting],
                [ZPProfileAction.showRemain, ZPProfileAction.addBank],
                [ZPProfileAction.callCenter, ZPProfileAction.quickComment, ZPProfileAction.appInfor]]
    }
    
    func getItem(_ action: ZPProfileAction) -> JSON {
        
        var result: JSON
        switch action {
        case .showRemain:
            result = ["title" : R.string_Profile_Balance(), "icon" : ["name" : "personal_balance", "color" : UIColor.zp_green()], "descriptionType" : ZPProfileActionDescriptionType.descriptionVND]
        case .showSetting:
            result = ["title" : R.string_Settings_Protect_Account(), "icon" : ["name" : "personal_settingaccount", "color" : UIColor.zp_gray()], "descriptionType" : ZPProfileActionDescriptionType.descriptionUnknown]
            break
        case .addBank:
            result = ["title" : R.string_Profile_Bank(), "icon" : ["name" : "personal_linkbank", "color" : UIColor.zp_yellow()], "descriptionType" : ZPProfileActionDescriptionType.descriptionMessage]
        case .voucherList:
            result = ["title" : R.string_LeftMenu_VoucherList(), "icon" : ["name" : "personal_voucherlist", "color" : UIColor.zp_red()], "descriptionType" : ZPProfileActionDescriptionType.descriptionUnknown]
        case .billWaiting:
            result = ["title" : "Hoá Đơn", "icon" : ["name" : "personal_bill", "color" : UIColor.zp_blue()], "descriptionType" : ZPProfileActionDescriptionType.descriptionMessage]
        case .callCenter:
            result = ["title" : R.string_LeftMenu_TermOfUse(), "icon" : ["name" : "personal_supportcenter", "color" : UIColor.zp_gray()], "descriptionType" : ZPProfileActionDescriptionType.descriptionUnknown]
        case .quickComment:
            result = ["title" : R.string_LeftMenu_Quick_Comment(), "icon" : ["name" : "personal_feedback", "color" : UIColor.zp_gray()], "descriptionType" : ZPProfileActionDescriptionType.descriptionUnknown]
        case .appInfor:
            result = ["title" : R.string_LeftMenu_About(), "icon" : ["name" : "personal_infomation", "color" : UIColor.zp_gray()], "descriptionType" : ZPProfileActionDescriptionType.descriptionUnknown]
        case .logOut:
            result = ["Content" : R.string_LeftMenu_SigOut(), "icon" : ["name" : "personal_logout", "color" : UIColor.zp_gray()], "color" : UIColor.zp_gray()]
        default:
            fatalError("Please check this case")
        }
        return result
    }
}


