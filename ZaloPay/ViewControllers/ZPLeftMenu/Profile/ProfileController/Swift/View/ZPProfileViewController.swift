//
//  ZPProfileViewController.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 9/25/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import ZaloPayAnalyticsSwift

class ZPProfileViewController: BaseViewController {

    lazy var tableView: UITableView = UITableView(frame: view.bounds, style: .grouped)
    var presenter: ZPProfilePresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = R.string_Profile_Title()
        setupTableView()
        observerBallanceChange()
    }
    
    func observerBallanceChange() {
        ZMEventManager.registerHandler(self, eventType: EventTypeUpdateBalance.rawValue) { [weak self] (event) in
            self?.tableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.tableView.reloadData()
        ZPTrackingHelper.shared().trackScreen(with: self)        
    }
    
    override func screenName() -> String! {
        return ZPTrackerScreen.Screen_iOS_Me
    }
    
    func setupTableView() {
        self.tableView.backgroundColor = UIColor.defaultBackground()
        presenter = ZPProfilePresenter()
        presenter?.rootController = self
        presenter?.view = self
        self.tableView.register(UINib(nibName: "ZPProfileLogoutCell", bundle: nil), forCellReuseIdentifier: "ZPProfileLogoutCell")
        tableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        tableView.delegate = self.presenter
        tableView.dataSource = self.presenter
        view.addSubview(tableView)
    }
}

extension ZPProfileViewController: ZPProfileViewProtocol {
    func reloadTable() {
        self.tableView.reloadData()
    }
}

