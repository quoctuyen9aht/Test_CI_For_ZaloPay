//
//  ZPProfilePresenter.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 9/22/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import ZaloPayProfile
import ZaloPayCommonSwift


final class ZPProfilePresenter: NSObject, ZPProfilePresenterProtocol {
    
    var view: ZPProfileViewProtocol?
    private lazy var interactor: ZPProfileInteractorProtocol = ZPProfileInteractor()
    private lazy var router: ZPProfileRouterProtocol = ZPProfileRouter()
    weak var rootController: BaseViewController?
    private lazy var handlerAction: PublishSubject<IndexPath> = PublishSubject()
    
    override init() {
        super.init()
        interactor.presenter = self
        interactor.requestUserProfile()
    }
    
    func doneLoadUserProfile() {
        self.view?.reloadTable()
    }       
}

extension ZPProfilePresenter : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.interactor.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.interactor.numberRowsInSection(section: section)
    }
    
    func setUpProfileCell(_ cell: ZPProfileUserInfoCell) {
        let user =  ZPProfileManager.shareInstance.currentZaloUser
        let avatar = user?.avatar ??  ""
        let displayName = user?.displayName ?? ""
        let url = URL(string: avatar)
        cell.lblUserName.text = displayName
        
        // Add a check icon if user account is verified
        if let level = ZPProfileManager.shareInstance.userLoginData?.profilelevel {
            if level == ZPProfileLevel.level3.rawValue {
                let iconFont = UILabel.iconCode(withName: "verify_success1") ?? ""
                cell.lblUserName.font = UIFont.zaloPay(withSize: 18)
                let attributedText = NSMutableAttributedString(string: displayName + " ")
                attributedText.append(NSAttributedString(string: iconFont, attributes: [NSAttributedStringKey.foregroundColor: UIColor.zaloBase(), NSAttributedStringKey.font: UIFont.iconFont(withSize: 16)]))
                cell.lblUserName.attributedText = attributedText
            }
        }
        
        cell.avatarImgView.sd_setImage(with: url, placeholderImage: UIImage.defaultAvatar())
        let accountPhone = (ZPProfileManager.shareInstance.userLoginData?.phoneNumber.formatPhoneNumber()).or(else: "")
        cell.lblUserZaloId.text = accountPhone.isEmpty ? R.string_LeftMenu_ZaloPayIdEmpty() : accountPhone

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            switch indexPath.item {
            case 0:
                let cell = ZPProfileUserInfoCell.zp_cellFromSameNib(for: tableView)
                setUpProfileCell(cell)
                return cell
            case 1:
                let cell = ZPProfileActionCell.zp_cellFromSameNib(for: tableView)
                let dict = interactor.item(at: indexPath)
                cell.setItem(dict)
                return cell
            default:
                break
            }
            return UITableViewCell()
        } else {
            let dict: JSON = interactor.item(at: indexPath)
            switch indexPath.section {
                case 1: fallthrough
                case 2:
                    let cell = ZPProfileActionCell.zp_cellFromSameNib(for: tableView)
                    var newDict: JSON = dict
                    var isShowNotify = false
                    if indexPath.section == 1 {
                        switch indexPath.item {
                        case 0:
                            let balance = ZPWalletManagerSwift.sharedInstance.currentBalance ?? 0
                            let balanceDisplay = NSNumber(value: balance.int64Value).formatMoneyValue() ?? ""
                            let message = "\(balanceDisplay) VND"
                            newDict["message"] = message
                        case 1:
                            let total = interactor.getNumberBank()
                            let msg = total == 0 ? "Liên kết ngay" : "\(total) liên kết"
                            newDict["message"] = msg
                            break
                        case 2:
                            isShowNotify = false
                        default:
                            break
                        }
                    }
                cell.notifyView?.isHidden = !isShowNotify
                cell.setItem(newDict )
                return cell
            case 3:
                let cell = ZPProfileLogoutCell.zp_cellFromSameNib(for: tableView)
                cell.setItem(dict: dict)
                return cell
            default:
                break
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       if indexPath == .zero {
            return 80
       }
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 14
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let action = self.interactor.getActionAt(indexPath)
        self.router.handle(from: self.rootController, with: action)
    }
}



