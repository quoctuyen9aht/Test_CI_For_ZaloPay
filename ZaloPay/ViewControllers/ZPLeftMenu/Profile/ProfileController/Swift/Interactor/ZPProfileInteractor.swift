//
//  ZPProfileInteractor.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 9/22/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ZaloPayProfile

class ZPProfileInteractor: NSObject, ZPProfileInteractorProtocol {
    weak var presenter: ZPProfilePresenterProtocol?
    private lazy var entity : ZPProfileEntity = ZPProfileEntity()
    override init() {
        super.init()
    }
    
    func requestUserProfile() {
        ZPProfileManager.shareInstance.loadZaloUserProfile().deliverOnMainThread().subscribeNext { [weak self](value) in
            self?.presenter?.doneLoadUserProfile()
        }        
    }
      
    func numberOfSections() -> Int {
        return entity.dataSource.count
    }
    
    func numberRowsInSection(section: Int) -> Int {
        return self.entity.dataSource[section].count
    }
    
    func getActionAt(_ indexPath: IndexPath) -> ZPProfileAction {
        return entity.dataSource.lazy[indexPath.section].lazy[indexPath.row]
    }
    
    func item(at indexPath: IndexPath) -> [String: Any] {
        let action: ZPProfileAction = getActionAt(indexPath)
        return entity.getItem(action)
    }
    
    func getNumberBank() -> Int {
        let walletManager = ZaloPayWalletSDKPayment.sharedInstance()
        let allCard = (walletManager?.paymentSavedCard() ?? []).count
        let allAccount = (walletManager?.paymentSavedBankAccount() ?? []).count
        return allCard + allAccount
    }
}
