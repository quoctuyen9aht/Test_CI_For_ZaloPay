//
//  ZPProfileProtocol.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 9/28/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift
import ZaloPayProfile
import ZaloPayAnalyticsSwift

protocol ZPProfileViewProtocol: class {
    func reloadTable()
}

protocol ZPProfilePresenterProtocol: class {
    var view: ZPProfileViewProtocol? {get set}
    func doneLoadUserProfile();
}

protocol ZPProfileInteractorProtocol: class {
    var presenter: ZPProfilePresenterProtocol? {get set}
    func requestUserProfile()
    func numberOfSections() -> Int
    func numberRowsInSection(section: Int) -> Int
    func getActionAt(_ indexPath: IndexPath) -> ZPProfileAction
    func item(at indexPath: IndexPath) -> [String: Any]
    func getNumberBank() -> Int
}

protocol ZPProfileRouterProtocol: class {
    func handle(from controller: BaseViewController?, with action: ZPProfileAction)
    func viewController(fromTitle title: String, params: Dictionary<String, Any>) -> BaseViewController
    func showViewController(in controller: BaseViewController, with action: ZPProfileAction, controllerShow: UIViewController, eventID: ZPAnalyticEventAction, requirePIN: Bool)
}

