//
//  ZPHomeProtocols.swift
//  ZaloPay
//
//  Created by tridm2 on 10/27/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayAnalyticsSwift

protocol ZPHomeInteractorProtocol: class {
    var presenter: ZPHomePresenterProtocol? { get set }
    
    // Presenter -> Interactor
    func getActiveApps()
    func setupEvent()
    func trackEvent(trackId: ZPAnalyticEventAction)
}

protocol ZPHomeDisclaimerProtocol: class {
    // Presenter -> Interactor
    func getDisclaimeMessage(_ appId: Int) -> String
    func markDoneShowDisclaimer(_ appId: Int)
    func shouldShowDisclaimPopup(_ appId: Int) -> Bool
    func checkAndShowDisclaimer(appId: Int, completeHandle: @escaping ()->Void)
}

protocol ZPHomePresenterProtocol: class {
    var interactor: ZPHomeInteractorProtocol? { get set }
    var router: ZPHomeRouterProtocol? { get set }
    var view: ZPHomeViewControllerProtocol? { get set }
    
    // View -> Presenter
    func prepare()
    func openHomePageItem(_ data: ZPHomeFeatureModel)
    func handleTappedOnNaviCustomView(at idx: Int)
    func openScreen(_ type: ScreenType)
    func showNotification()
    func showCachedActiveApps()
    
    // Interactor -> Presenter
    func recvTotalUnreadMsg(_ totalMessage: String)
    func recvInternetConnectionStatus(_ status: Bool)
    func recvDataSource(_ dataSource: Array<ZPHomePageItemBase>)
}

protocol ZPHomeRouterProtocol: class {
    static func createHomeViewController() -> ZPHomeViewController
    
    // Presenter -> Router
    func willPushFrom(_ viewController: BaseViewController)
    func showScreen(_ type: ScreenType) -> ZPAnalyticEventAction?
    func showNotification()
    func openApp(with data: ZPHomeFeatureModel)
}

protocol ZPHomeViewControllerProtocol: class {
    var presenter: ZPHomePresenterProtocol? { get set }
    
    // Presenter -> View
    func reloadData(_ dataSource: Array<ZPHomePageItemBase>)
    func badgeChanged(_ badgeValue: String)
    func internetConnectionStatusChanged(_ status: Bool)
}
