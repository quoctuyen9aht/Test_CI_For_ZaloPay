//
//  ZPInternetConnectionView.swift
//  ZaloPay
//
//  Created by tridm2 on 10/25/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit
import ZaloPayCommonSwift


class ZPInternetConnectionView: UIView {
    var midView: UIView!
    var bottomView: UIView!
    var fontSize: CGFloat!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupView() {
        self.fontSize = 16.0
        self.addMidView()
        self.addBottomView()
    }
    
    func addMidView() {
        self.midView = UIView()
        self.addSubview(midView)
        
        let image = UIImage(named: "internetIcon")
        let imageView = UIImageView(image: image)
        self.midView.addSubview(imageView)
        
        imageView.snp.makeConstraints { make in
            make.top.equalTo(60)
            make.centerX.equalTo(self.midView.snp.centerX)
            make.width.equalTo(image?.size.width ?? 0)
            make.height.equalTo(image?.size.height ?? 0)
        }
        
        
        let htmlLabel = UILabel()
        self.midView.addSubview(htmlLabel)
        htmlLabel.numberOfLines = 0
        htmlLabel.text = R.string_InternetConnection_NoInternetTitle()
        htmlLabel.textColor = UIColor.hex_0xff7c00()
        htmlLabel.font = UIFont.sfuiTextRegular(withSize: 17)
        htmlLabel.textAlignment = .center
        
        let size = htmlLabel.sizeThatFits(CGSize(width: self.frame.size.width - 20, height: 1000))
        htmlLabel.snp.makeConstraints { make in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(size.height)
            make.top.equalTo(imageView.snp.bottom).offset(28)
        }
        
        
        let label = UILabel()
        label.font = UIFont.sfuiTextRegular(withSize: self.fontSize)
        label.text = R.string_InternetConnection_Guide()
        label.textColor = UIColor.subText()
        label.sizeToFit()
        self.midView.addSubview(label)
        
        label.snp.makeConstraints { make in
            make.top.equalTo(htmlLabel.snp.bottom).offset(50)
            make.height.equalTo(label.frame.size.height)
            make.width.equalTo(label.frame.size.width)
            make.left.equalTo(10)
        }
        
        
        self.midView.snp.makeConstraints { make in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.bottom.equalTo(label.snp.bottom).offset(5)
        }
    }
    
    func addBottomView() {
        self.bottomView = UIView()
        self.addSubview(self.bottomView)
        self.bottomView.backgroundColor = UIColor.white
        
        let topLineView = ZPLineView()
        self.bottomView.addSubview(topLineView)
        topLineView.alignToTop()
        
        let bottomLineView = ZPLineView()
        self.bottomView.addSubview(bottomLineView)
        bottomLineView.alignToBottom()
        
        let mdhtmlLabel = MDHTMLLabel()
        self.bottomView.addSubview(mdhtmlLabel)
        mdhtmlLabel.numberOfLines = 0
        mdhtmlLabel.htmlText = R.string_InternetConnection_Wifi()
        let mdhtmlLabelSize = mdhtmlLabel.sizeThatFits(CGSize(width: self.frame.size.width - 20, height: 1000))
        mdhtmlLabel.snp.makeConstraints { make in
            make.left.equalTo(10)
            make.size.equalTo(mdhtmlLabelSize)
            make.top.equalTo(20)
        }
        
        let mdhtmlLabel2 = MDHTMLLabel()
        self.bottomView.addSubview(mdhtmlLabel2)
        mdhtmlLabel2.numberOfLines = 0
        mdhtmlLabel2.htmlText = R.string_InternetConnection_3G()
        let mdhtmlLabelSize2 = mdhtmlLabel2.sizeThatFits(CGSize(width: self.frame.size.width - 20, height: 1000))
        mdhtmlLabel2.snp.makeConstraints { make in
            make.left.equalTo(10)
            make.size.equalTo(mdhtmlLabelSize2)
            make.top.equalTo(mdhtmlLabel.snp.bottom).offset(16)
        }
        
        self.bottomView.snp.makeConstraints { make in
            make.top.equalTo(self.midView.snp.bottom)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.bottom.equalTo(mdhtmlLabel2.snp.bottom).offset(20)
        }
    }
}
@objcMembers
class ZPInternetConnectionViewController: BaseViewController {
    var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = R.string_InternetConnection_Title()
        self.addTableView()
    }
    
    func addTableView() {
        self.tableView = UITableView()
        self.tableView.backgroundColor = UIColor.clear
        self.view.addSubview(self.tableView)
        
        self.tableView.snp.makeConstraints { make in
            make.top.equalTo(0)
            make.bottom.equalTo(0)
            make.left.equalTo(0)
            make.right.equalTo(0)
        }
        
        let height = max(self.view.frame.size.height - 64, 450)
        self.tableView.tableHeaderView = ZPInternetConnectionView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: height))
    }
}
