//
//  ZPHomeViewController.swift
//  ZaloPay
//
//  Created by tridm2 on 10/25/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import CoreGraphics
import ZaloPayCommonSwift
import ZaloPayAnalyticsSwift

class ZPHomeViewController: BaseViewController {
    var presenter: ZPHomePresenterProtocol?
    
    var dataSource: Array<ZPHomePageItemBase>?
    var collectionView: UICollectionView!
    var headerView: ZPHomeHeaderView!
//    var connectionView: UIView?
    var mapCardData: Dictionary<AnyHashable, Any>!
    var searchField: ZPSearchNavigationView?
    
    var badgeValue: String!
    var notificationLabel: UILabel! // iOS 11
    var notificationButtonItem: UIBarButtonItem!
    var customView: NavigationCustomView?
    
    let headerViewHeight: CGFloat = 118.0

    fileprivate let disposeBag = DisposeBag()
    private var statePopup = ZPGiftPopupShow.none
    private var hasNetwork: Bool = true {
        didSet {
            DispatchQueue.main.async {
                self.collectionView?.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ZPGiftPopupManager.sharedInstance.delegate = self
        setupDisplay()
        presenter?.showCachedActiveApps()
        presenter?.prepare()
        setupEvent()
        checkGiftItems()
        createConnectionStatusView()
    }
    
    override func screenName() -> String! {
        return ZPTrackerScreen.Screen_iOS_Home
    }
    
    // MARK: - Prepare Display
    fileprivate func setupDisplay() {
        UIApplication.shared.setStatusBarStyle(.lightContent, animated: false)
        self.createCollectionView()
        self.setupNavigationBar()
        self.automaticallyAdjustsScrollViewInsets = true
    }
    
    // MARK: - Check gift list
    fileprivate func checkGiftItems() {
//        let totalGift = ZPGiftPopupManager.sharedInstance.totalUnredMessage()
//        if totalGift <= 0  {
//            return
//        }
//        statePopup = totalGift > 1 ? .list : .popup
        
        ZPGiftPopupManager.sharedInstance.handleUnreadMessages()
    }
    
    // MARK: - Prepare Event
    fileprivate func setupEvent() {
        NotificationCenter.default.rx
            .notification(.UIApplicationDidBecomeActive)
            .takeUntil(self.rx.deallocating)
            .bind
            { [weak self](_) in
                self?.trackCurrentOffset(false)
            }.disposed(by: disposeBag)
        
        self.collectionView.rx.contentOffset.asDriver().drive(onNext: { [weak self](point) in
            guard let wSelf = self,
                let bar = wSelf.navigationController?.navigationBar else { return }
            let offsetY = point.y
            let heightHeader = wSelf.headerView.frame.size.height
            let ratio = min(abs(offsetY / heightHeader), 1)
            
            func reset(_ alpha: CGFloat) {
                bar.setAlphaOverlayView(alpha)
                wSelf.headerView.alpha = 1 - alpha
                wSelf.headerView.subviews.forEach({ $0.transform = .identity })
            }
            
            TrackOffset: if offsetY >= -heightHeader {
                guard offsetY < 0 else {
                    offsetY > 0 ? reset(1) : {
                        bar.setAlphaOverlayView(1)
                        wSelf.headerView.alpha = 0
                    }()
                    break TrackOffset
                }
                
                bar.setAlphaOverlayView(1 - ratio)
                wSelf.headerView.alpha = ratio < 1 ? max(0, ratio - 0.5) : 1
                let d = CGAffineTransform.identity.translatedBy(x: 0, y: 80 * (1 - ratio))
                let nT: CGAffineTransform = ratio != 1 ? d.translatedBy(x: 0, y: -abs(offsetY * 0.0002)) : d
                wSelf.headerView.subviews.forEach({ $0.transform = nT })
            } else {
                reset(0)
            }
        }).disposed(by: disposeBag)
        
        self.rx.sentMessage(#selector(viewDidLayoutSubviews)).bind { [weak self](_) in
            guard self?.trackCurrentOffset(false) == true else { return }
            let overlayView = self?.navigationController?.navigationBar.overlayView
            overlayView?.alpha = 0
            UIView.animate(withDuration: 0.3, animations: { overlayView?.alpha = 1 })
        }.disposed(by: disposeBag)
        
    }
    
    // MARK: create custom navigation
    private func setupCustomViewNavigation() {
        self.collectionView.clipsToBounds = false
        guard let customView = Bundle.main.loadNibNamed("NavigationCustomView", owner: nil, options: nil)?.first as? NavigationCustomView else {
            return
        }
        customView.delegate = self
        if let notifiItem = customView.notificationItem() {
            notifiItem.badgePaddingTop = 0
            notifiItem.badgeValue = self.badgeValue
        }
        self.customView = customView
        
        if let bar = self.navigationController?.navigationBar {
            bar.overlayView = customView
            bar.setAlphaOverlayView(0)
        }
        self.customView?.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ZPTrackingHelper.shared().trackScreen(with: self)
        
        self.isAppear = true
        showGift()
        guard let naviController = self.navigationController else {
            return
        }
        
        if naviController.isNavigationBarHidden {
            naviController.setNavigationBarHidden(false, animated: false)
        }
        
        ZPNotificationService.sharedInstance().updateTotalUnreadNotify()
        guard naviController.navigationBar.overlayView == nil else {
            self.customView?.isHidden = false
            return
        }
        self.setupCustomViewNavigation()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.isAppear = false
        self.navigationController?.navigationBar.reset()
        
        //Expand menu
        if self.collectionView == nil {
            return
        }
        DispatchQueue.main.async {
            let point = CGPoint(x: 0, y: -self.headerView.frame.size.height)
            self.collectionView.setContentOffset(point, animated: false)
            self.customView?.isHidden = true
        }
    }
    private func createSearchBar() -> ZPSearchNavigationView {
        let naviSize = self.navigationController?.navigationBar.frame.size ?? CGSize.zero
        let adjustSize: CGFloat = UIView.isScreen2x() ? 60.0 : 67.0
        let searchBar = ZPSearchNavigationView(frame: CGRect(x: 0, y: 5, width: naviSize.width - adjustSize, height: 33))
        
        let searchTapRes = UITapGestureRecognizer(target: self, action: #selector(onSearchClick))
        searchTapRes.delegate = self
        searchBar.addGestureRecognizer(searchTapRes)
        
        return searchBar
    }
    
    private func createNotiButton() -> UIButton {
        let notiBtn = UIButton(type: .custom)
        notiBtn.titleLabel?.textColor = UIColor.white
        notiBtn.titleLabel?.font = UIFont.iconFont(withSize: 24)
        notiBtn.setIconFont("nav_notify", for: .normal)
        notiBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        notiBtn.addTarget(self, action: #selector(notifyButtonClick), for: .touchUpInside)
        
        if #available(iOS 11, *) {
            notiBtn.contentEdgeInsets = UIEdgeInsets.zero.with { $0.left = -5 }
            notiBtn.titleLabel?.snp.makeConstraints({ (make) in
                make.top.equalToSuperview()
                make.centerX.equalToSuperview()
            })
            notiBtn.titleLabel?.badgePaddingRight = 5
            self.notificationLabel = notiBtn.titleLabel ?? UILabel()
            self.notificationLabel.badgeBGColor = UIColor.hex_0xff3824()
            self.notificationLabel.badgeFont = UIFont.sfuiTextRegular(withSize: 13)
        }
        
        return notiBtn
    }
    
    private func setupNotiButton() {
        if self.notificationButtonItem == nil {
            let notiBtn = createNotiButton()
            self.notificationButtonItem = UIBarButtonItem(customView: notiBtn)
            self.notificationButtonItem!.badgeBGColor = UIColor.hex_0xff3824()
            self.notificationButtonItem!.badgeFont = UIFont.sfuiTextRegular(withSize: 13)
            self.notificationButtonItem!.shouldAnimateBadge = true
        }
        
        let fixedSpace = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        fixedSpace.width = -12
        
        self.navigationItem.rightBarButtonItems = [fixedSpace, self.notificationButtonItem!]
        
        self.trackCurrentOffset(false)
    }
    
    private func setupNavigationBar() {
        if #available(iOS 11, *) {
            let searchBar = self.createSearchBar()
            self.searchField = searchBar
            
            let naviTitleView = UIView()
            
            naviTitleView.addSubview(searchBar)
            searchBar.snp.makeConstraints { make in
                make.size.equalTo(searchBar.bounds.size)
                make.left.equalToSuperview().offset(18)
                make.top.equalToSuperview()
                make.bottom.equalToSuperview()
            }
            
            let notiBtn = createNotiButton()
            naviTitleView.addSubview(notiBtn)
            let naviWidth = self.navigationController?.navigationBar.frame.size.width ?? 0
            notiBtn.snp.makeConstraints { make in
                make.width.equalTo(naviWidth - searchBar.bounds.size.width - 10)
                make.top.equalToSuperview()
                make.right.equalToSuperview()
                make.bottom.equalToSuperview()
                make.left.equalTo(searchBar.snp.right)
            }
            
            naviTitleView.setNeedsLayout()
            self.navigationItem.titleView = naviTitleView
            
            naviTitleView.rx.observe(CGFloat.self, "alpha").filter({ [weak self] in
                return ($0 == 1) && (self?.navigationController?.navigationBar.overlayView?.alpha == 1)
            }).bind(onNext: {
                [weak self] _ in
                self?.navigationController?.navigationBar.setAlphaElement(0)
            }).disposed(by: disposeBag)
            
            return
        }
        
        self.setupNotiButton()
        self.searchField = self.createSearchBar()
        self.searchField!.rx.observe(CGFloat.self, "alpha").filter({ [weak self] in
            return ($0 == 1) && (self?.navigationController?.navigationBar.overlayView?.alpha == 1)
        }).bind(onNext: {
            [weak self] _ in
            self?.navigationController?.navigationBar.setAlphaElement(0)
        }).disposed(by: disposeBag)
        
        self.navigationItem.titleView = self.searchField
    }
    
    private func createCollectionView() {
        let flowLayout = ZPSeparatorCollectionViewFlowLayout()
        flowLayout.prepare()
        flowLayout.strokeWidth = ZPHomePageItemBase.spacingItem
        flowLayout.scrollDirection = .vertical
        
        // CollectionView
        self.collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: flowLayout)
        self.collectionView.showsVerticalScrollIndicator = false
        self.collectionView.addParallax(with: UIImage(from: UIColor.zaloBase()), andHeight: 0.1)
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.register(ZPHomeItemCollectionViewCell.self)
        self.register(UICollectionViewCell.self)
        self.register(ZPHomePageEmptyCell.self)
        self.view.addSubview(self.collectionView)
        self.collectionView.alwaysBounceVertical = true
        self.collectionView.snp.makeConstraints { $0.edges.equalTo(UIEdgeInsetsMake(0, 0, 0, 0)) }
        self.collectionView.backgroundColor = .white
        
        // Header
        let frame = CGRect(x: 0, y: -headerViewHeight, width: self.view.frame.size.width, height: headerViewHeight)
        self.headerView = ZPHomeHeaderView(frame: frame)
        self.headerView.clipsToBounds = true
        self.headerView.delegate = self
        self.collectionView.addSubview(self.headerView)
        self.collectionView.contentInset = UIEdgeInsetsMake(CGFloat(headerViewHeight), 0, 0, 0)
    }
    
    fileprivate func register<T>(_ cellClass: T.Type) where T: UICollectionViewCell {
        self.collectionView.register(cellClass, forCellWithReuseIdentifier: "\(cellClass)")
    }
}

// MARK: Scrollview delegate
extension ZPHomeViewController: ZPHomeViewControllerProtocol {
    func reloadData(_ dataSource: Array<ZPHomePageItemBase>) {
        self.dataSource = dataSource
        self.collectionView.reloadData()
    }
    
    func badgeChanged(_ badgeValue: String) {
        self.badgeValue = badgeValue
        if #available(iOS 11, *) {
            self.notificationLabel.badgeValue = badgeValue
        } else {
            self.notificationButtonItem.badgeValue = badgeValue
        }
        
        guard let notifiItem = customView?.notificationItem() else {
            return
        }
        notifiItem.badgeValue = badgeValue
    }
    
    func internetConnectionStatusChanged(_ status: Bool) {
        self.hasNetwork = status
        
    }
}

// MARK: Scrollview delegate
extension ZPHomeViewController: UIScrollViewDelegate {
    @discardableResult
    func trackCurrentOffset(_ animated: Bool) -> Bool {
        let lastOffsetY = self.collectionView.contentOffset.y
        let heightHeader = self.headerView.frame.size.height
        let ratio = min(abs(lastOffsetY / heightHeader), 1)
        guard lastOffsetY < 0 else {
            self.navigationController?.navigationBar.setAlphaOverlayView(1)
            self.headerView.alpha = 0
            return true
        }
        
        if lastOffsetY > -heightHeader {
            let needChange = ratio > 0.5
            let offset = needChange ? CGPoint(x: 0, y: -heightHeader) : .zero
            self.collectionView.setContentOffset(offset, animated: animated)
            return needChange.not
        } else if lastOffsetY == -heightHeader { return false }
        
        return true
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        guard scrollView == self.collectionView && !decelerate else { return }
        self.trackCurrentOffset(true)
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard scrollView === self.collectionView else { return }
        self.trackCurrentOffset(true)
    }
}

// MARK: UICollectionView Delegate, DataSource
extension ZPHomeViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let itemData = self.dataSource![indexPath.row]
        let identify = "\(itemData.cellClass())"
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identify, for: indexPath) as? ZPHomeCollectionViewCellBase else {
            fatalError("Please setup cell")
        }
        cell.setItem(itemData)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSource?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.dataSource![indexPath.row].size()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return ZPHomePageItemBase.spacingItem
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return ZPHomePageItemBase.spacingItem
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let itemData = self.dataSource![indexPath.row]
        if let dataModel = (itemData as? ZPHomePageItem)?.dataModel {
            self.presenter!.openHomePageItem(dataModel)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let v = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ZPHomeConnectionRibbonView", for: indexPath)
        if let connectionView = v as? ZPHomeConnectionRibbonView {
            connectionView.delegate = self
        }
        return v
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return hasNetwork.not ? CGSize(width: UIScreen.main.bounds.width, height: 40) : .zero
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return .zero
    }
}

// MARK: Navigation custom delegate
extension ZPHomeViewController: NavigationCustomViewDelegate {
    func didTapAtIndex(onToolbar idx: Int) {
        self.presenter?.handleTappedOnNaviCustomView(at: idx)
    }
}

// MARK: UIGestureRecognizerDelegate
extension ZPHomeViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

// MARK: ZPHomeHeaderViewDelegate
extension ZPHomeViewController: ZPHomeHeaderViewDelegate {
    func didTouchScanQR() {
        self.presenter?.openScreen(ScreenType.ScanQR)
    }
    
    func didTouchBalance() {
        self.presenter?.openScreen(ScreenType.Balance)
    }
    
    func didTouchListCard() {
        self.presenter?.openScreen(ScreenType.ListCard)
    }
    
    func didTouchSearch() {
        self.openSearchScreen()
    }
    
    @objc func onSearchClick(_ ges: UIGestureRecognizer) {
        if ges.state == .ended {
            self.openSearchScreen()
        }
    }
    
    func openSearchScreen() {
        self.presenter?.openScreen(ScreenType.Search)
    }
    
    @objc func notifyButtonClick() {
        let eventAction = self.badgeValue.isEmpty ? ZPAnalyticEventAction.home_touch_notification : ZPAnalyticEventAction.home_touch_notification_new
        ZPTrackingHelper.shared().trackEvent(eventAction)
        self.presenter?.showNotification()
    }
}


// MARK: Internet connection status
extension ZPHomeViewController: ZPHomeConnectionDelegate {
    private func createConnectionStatusView() {
        self.collectionView?.register(ZPHomeConnectionRibbonView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader , withReuseIdentifier: "ZPHomeConnectionRibbonView")
        
        self.collectionView?.register(ZPHomeConnectionRibbonView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter , withReuseIdentifier: "ZPHomeConnectionRibbonView")
    }
    
    @objc func showInternetConnection() {
        self.presenter?.openScreen(ScreenType.InternetConnection)
    }
}

extension ZPHomeViewController: ZPGiftPopupShowHandlerProtocol {
    func handlerShowPopup(with action: ZPGiftPopupShow) {
        statePopup = action
        guard isAppear else {
            return
        }
        showGift()
    }
    
    func showGift() {
        guard statePopup != .none else {
            return
        }
        defer {
            // reset none
            statePopup = .none
        }
        switch statePopup {
        case .list:
            ZPGiftPopupManager.sharedInstance.showListPopup()            
        case .popup:
            let items = ZPGiftPopupManager.sharedInstance.allMessage.filter({ $0.message.isUnread })
            guard items.count > 0 else {
                return
            }
            
            ZPGiftPopupManager.sharedInstance.showPopup(items[0].message)
        default:
            break
        }
    }
}
class HomeButtonCustom: UIButton {}

protocol ZPHomeConnectionDelegate: AnyObject {
    func showInternetConnection()
}

@objcMembers
class ZPHomeConnectionRibbonView: UICollectionReusableView {
    weak var delegate: ZPHomeConnectionDelegate?
    
    override var reuseIdentifier: String? {
        return "\(ZPHomeConnectionDelegate.self)"
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }
    
    func setupLayout() {
        self.backgroundColor = UIColor.hex_0xffffc1()
        let label = UILabel()
        self.addSubview(label)
        label.textAlignment = .right
        label.text = R.string_Home_InternetConnectionError()
        label.font = UIFont.sfuiTextRegular(withSize: 15)
        label.textColor = UIColor.hex_0x585858()
        label.snp.makeConstraints { make in
            make.left.equalTo(0)
            make.right.equalTo(self.snp.centerX).offset(20)
            make.top.equalTo(0)
            make.bottom.equalTo(0)
        }
        
        let button = UIButton(type: .custom)
        self.addSubview(button)
        button.titleLabel?.font = UIFont.sfuiTextRegular(withSize: 15)
        button.setTitle(R.string_Home_CheckInternetConnection(), for: .normal)
        button.contentHorizontalAlignment = .left
        button.addTarget(self, action: #selector(showInternetConnection), for: .touchUpInside)
        button.setTitleColor(UIColor.hex_0xcd7a04(), for: .normal)
        button.snp.makeConstraints { make in
            make.left.equalTo(label.snp.right).offset(2)
            make.top.equalTo(0)
            make.bottom.equalTo(0)
            make.right.equalTo(0)
        }
        
//        let line = ZPLineView()
//        self.addSubview(line)
//        line.alignToBottom()
    }
    
    func showInternetConnection() {
        self.delegate?.showInternetConnection()
    }
}
