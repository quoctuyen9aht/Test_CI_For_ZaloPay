//
//  ZPHomeFeatureModel.swift
//  ZaloPay
//
//  Created by bonnpv on 9/28/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayAnalyticsSwift

public class ZPHomeFeatureModel {
    public var classType : AnyClass?
    public var trackId: ZPAnalyticEventAction?
    public var appId: Int = 0
    public var appType: Int = 0
    public var moduleName: String?
    public var title: String?
    public var routerId: Int = -1

    public class func fromDictionary(_ data: Dictionary<String, Any>) -> ZPHomeFeatureModel{
        let zpHomeFeatureModel = ZPHomeFeatureModel()
        zpHomeFeatureModel.appId = data.int(forKey: "appId")
        zpHomeFeatureModel.appType = data.int(forKey: "apptype")
        zpHomeFeatureModel.moduleName = data.string(forKey: "moduleName")
        zpHomeFeatureModel.classType = data["class"] as? AnyClass
        zpHomeFeatureModel.title =  data.string(forKey: "title")
        zpHomeFeatureModel.routerId = data.int(forKey: "routerid")
        zpHomeFeatureModel.trackId = ZPAnalyticEventAction.init(rawValue: data.int(forKey: "trackId")) 
        return zpHomeFeatureModel
    }
}

