//
//  ZPHomePageItem.swift
//  ZaloPay
//
//  Created by bonnpv on 9/28/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

@objcMembers
public class ZPHomePageItemBase: NSObject {
    public static let spacingItem: CGFloat = 1.0
    

    fileprivate func cellHeight() -> CGFloat {
        if UIView.isScreen2x() {
            return 108
        }
        if UIView.isIPhoneX() {
            return 131
        }
        return 126
    }
    
    public func size() -> CGSize {
        return CGSize.zero
    }
    
    public func cellClass() -> ZPHomeCollectionViewCellBase.Type {
        return ZPHomeCollectionViewCellBase.self
    }
}


public class  ZPHomePageItem : ZPHomePageItemBase {
    public var title: String?
    public var desc: String?
    public var imageName: String?
    public var color: UIColor?
    public var appId: Int = 0
    public var order:Int = 0
    public var dataModel: ZPHomeFeatureModel?
    
    public override init() {
        super.init()
    }
    
    public class func itemWith(title: String, desc: String, imageName: String) -> ZPHomePageItem{
        let item: ZPHomePageItem = ZPHomePageItem()
        item.title = title
        item.desc = desc
        item.imageName = imageName
        return item
    }
    
    override public func size() -> CGSize {
        let size: CGSize = UIScreen.main.bounds.size
        let width = (size.width - ZPHomePageItemBase.spacingItem*2 - 0.1)/3
        let height = cellHeight()
        return CGSize(width: width, height: height)
    }
    
    override public func cellClass() -> ZPHomeCollectionViewCellBase.Type {
        return ZPHomeItemCollectionViewCell.self
    }
}

public class  ZPHomePageBottomEmtyItem : ZPHomePageItemBase {
    
    override public func size() -> CGSize {
        let size: CGSize = UIScreen.main.bounds.size
        return CGSize(width: size.width, height: 1)
    }
    
    override public func cellClass() -> ZPHomeCollectionViewCellBase.Type {
        return ZPHomePageEmptyCell.self
    }
}

public class ZPHomePageRightEmtyItem : ZPHomePageBottomEmtyItem {
    override public func size() -> CGSize {
        return CGSize(width: 1, height: cellHeight())
    }
}

public class ZPHomePagePaddingItem : ZPHomePageBottomEmtyItem {
    override public func size() -> CGSize {
        let size: CGSize = UIScreen.main.bounds.size
        return CGSize(width:size.width , height: 14)
    }
}
