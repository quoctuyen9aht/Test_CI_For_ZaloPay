//
//  ZPLargeIconTitleButton.swift
//  ZaloPay
//
//  Created by Huu Hoa Nguyen on 10/8/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import UIKit

class ZPLargeIconTitleButton : UIView {
    private lazy var imageView = ZPIconFontImageView.init(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
    private lazy var label = UILabel.init()
    private lazy var subTitle = UILabel.init()

    init() {
        super.init(frame: CGRect.zero)
        
        addAllSubView()
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        addAllSubView()
        setupLayout()
    }
    
    func setIcon(name: String!) {
        self.imageView.setIconFont(name)
    }
    
    func setTitle(_ text: String?) {
        self.label.text = text
    }
    
    public func setSubTitle(_ text: String?) {
        self.subTitle.text = text
    }

    private func addAllSubView() {
        self.imageView.contentMode = .center
        self.imageView.setIconColor(UIColor.white)
        self.addSubview(self.imageView)

        let fontSize: CGFloat = UIView.isScreen2x() ? 14.5 : 15
        self.label.font = UIFont.sfuiTextRegular(withSize: fontSize)
        self.label.textColor = UIColor.init(hexValue: 0xd6f0f6)
        self.label.textAlignment = .center
        self.addSubview(self.label)

        self.subTitle.font = UIFont.sfuiTextRegular(withSize: fontSize)
        self.subTitle.adjustsFontSizeToFitWidth = true
        self.subTitle.textColor = UIColor.init(hexValue: 0x9cdaff)
        self.subTitle.textAlignment = .center
        self.addSubview(self.subTitle)
    }
    
    private func setupLayout() {
        self.imageView.snp.makeConstraints ({ (make) in
            make.size.equalTo(self.imageView.frame.size)
            make.top.equalTo(12)
            make.centerX.equalToSuperview()
        })
        
        // Force layout first to calculate bottom for imageview
        self.layoutSubviews()
        
        self.label.snp.makeConstraints({ (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(20)
            make.top.equalTo(self.imageView.snp.bottom).offset(10)
        })
        
        self.subTitle.snp.makeConstraints ({ (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(15)
            make.bottom.equalTo(-10)
        })
    }
}

