//
//  ZPHomeCollectionViewCell.swift
//  ZaloPay
//
//  Created by bonnpv on 9/28/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit

@objcMembers
public class ZPHomeCollectionViewCellBase: UICollectionViewCell {
    public func setItem(_ item: ZPHomePageItemBase) {
    }
}

public class ZPHomeItemCollectionViewCell : ZPHomeCollectionViewCellBase {
    public var imageView: ZPIconFontImageView?
    public var label: UILabel?
    
    let lableTopPadding = 8
    
    func iconSize() -> CGFloat {
        return UIView.isScreen2x() ? 38 : 40
    }
    
    func paddingTop() -> Float {
        return UIView.isScreen2x() ? 16 : 30
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        let conteView = self.contentView
        let size = iconSize()
        let padding = paddingTop()
        let imageView: ZPIconFontImageView = ZPIconFontImageView(frame:CGRect(x: 0, y: 0, width: size,height: size))
        self.imageView = imageView
        contentView.addSubview(imageView)
        
        let label = UILabel()
        self.label = label
        contentView.addSubview(label)
        label.textAlignment = .center
        label.zpMainBlackRegularHomeCell()
        label.numberOfLines = 0
        label.contentMode = .top
        
        imageView.snp.makeConstraints { (make) in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(padding)
            make.size.equalTo(size)
        }
        label.snp.makeConstraints { (make) in
            make.top.equalTo(imageView.snp.bottom).offset(lableTopPadding)
            make.bottom.equalTo(-padding)
            make.left.equalTo(5)
            make.right.equalTo(-5)
        }
        self.backgroundColor = .white
        conteView.backgroundColor = .white
    }
    
    override public func setItem(_ item: ZPHomePageItemBase) {
        guard let item = item as? ZPHomePageItem else {
            return
        }
        
        self.label?.text = item.title
        guard let imgName = item.imageName else {
            showDefaultIcon()
            return
        }
        if UILabel.iconCode(withName: imgName).count == 0 {
            showDefaultIcon()
            return
        }
        let color = item.color ?? UIColor.defaultAppIcon()
        self.imageView?.setIconFont(imgName)
        self.imageView?.setIconColor(color)
    }
    
    func showDefaultIcon() {
        self.imageView?.setIconFont("general_icondefault")
        self.imageView?.setIconColor(UIColor.defaultAppIcon())
    }
}

public class ZPHomePageEmptyCell : ZPHomeCollectionViewCellBase {
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
    }
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
