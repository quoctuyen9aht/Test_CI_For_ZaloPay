//
//  ZPSeparatorCollectionViewFlowLayout.swift
//  ZaloPay
//
//  Created by tridm2 on 10/30/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

extension UICollectionViewFlowLayout {
    func indexPathLastInSection(_ indexPath: IndexPath) -> Bool {
        guard let collectionView = self.collectionView,
            var lastItem = collectionView.dataSource?.collectionView(collectionView, numberOfItemsInSection: indexPath.section) else {
            return false
        }
        lastItem = lastItem - 1
        return  lastItem == indexPath.row
    }
    
    func indexPathInLastLine(_ indexPath: IndexPath) -> Bool {
        guard let collectionView = self.collectionView,
            var lastItemRow = collectionView.dataSource?.collectionView(collectionView, numberOfItemsInSection: indexPath.section) else {
                return false
        }
        lastItemRow = lastItemRow - 1
        
        let lastItem = IndexPath(item: lastItemRow, section: indexPath.section)
        let lastItemAttributes = layoutAttributesForItem(at: lastItem)
        let thisItemAttributes = layoutAttributesForItem(at: indexPath)
        return lastItemAttributes?.frame.origin.y == thisItemAttributes?.frame.origin.y
    }
    
    func indexPathLastInLine(_ indexPath: IndexPath) -> Bool {
        let nextIndexPath = IndexPath(item: indexPath.row + 1, section: indexPath.section)
        let cellAttributes: UICollectionViewLayoutAttributes? = layoutAttributesForItem(at: indexPath)
        let nextCellAttributes: UICollectionViewLayoutAttributes? = layoutAttributesForItem(at: nextIndexPath)
        return cellAttributes?.frame.origin.y != nextCellAttributes?.frame.origin.y
    }
}

class LineView: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.line()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

class ZPSeparatorCollectionViewFlowLayout: UICollectionViewLeftAlignedLayout {
    var strokeWidth: CGFloat!
    
    override func prepare() {
        // Registers my decoration views.
        register(LineView.self, forDecorationViewOfKind: "Vertical")
        register(LineView.self, forDecorationViewOfKind: "Horizontal")
    }
    
    override func layoutAttributesForDecorationView(ofKind decorationViewKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        // Prepare some variables.
        let nextIndexPath = IndexPath(item: indexPath.row + 1, section: indexPath.section)
        guard let cellAttributes = layoutAttributesForItem(at: indexPath),
            let nextCellAttributes = layoutAttributesForItem(at: nextIndexPath) else {
                return nil
        }
        let layoutAttributes = UICollectionViewLayoutAttributes(forDecorationViewOfKind: decorationViewKind, with: indexPath)
        let baseFrame = cellAttributes.frame
        let nextFrame = nextCellAttributes.frame
        
        var spaceToNextItem: CGFloat = 0
        if nextFrame.origin.y == baseFrame.origin.y {
            spaceToNextItem = nextFrame.origin.x - baseFrame.origin.x - baseFrame.size.width
        }
        if (decorationViewKind == "Vertical") {
            let padding: CGFloat = 0
            
            // Positions the vertical line for this item.
            let x = baseFrame.origin.x + baseFrame.size.width + (spaceToNextItem - strokeWidth) / 2.0
            layoutAttributes.frame = CGRect(x: x, y: baseFrame.origin.y + padding, width: strokeWidth, height: baseFrame.size.height - padding * 2)
        }
        else {
            // Positions the horizontal line for this item.
            layoutAttributes.frame = CGRect(x: baseFrame.origin.x, y: baseFrame.origin.y + baseFrame.size.height, width: baseFrame.size.width + spaceToNextItem, height: strokeWidth)
        }
        layoutAttributes.zIndex = -1
        return layoutAttributes
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard let baseLayoutAttributes = super.layoutAttributesForElements(in: rect) else {
            return nil
        }
        
        var layoutAttributes = baseLayoutAttributes
        for thisLayoutItem in baseLayoutAttributes {
            if thisLayoutItem.representedElementCategory == .cell {
                // Adds vertical lines when the item isn't the last in a section or in line.
                if !(self.indexPathLastInSection(thisLayoutItem.indexPath) || self.indexPathLastInLine(thisLayoutItem.indexPath)) {
                    if let newLayoutItem = layoutAttributesForDecorationView(ofKind: "Vertical", at: thisLayoutItem.indexPath) {
                        layoutAttributes.append(newLayoutItem)
                    }
                }
                // Adds horizontal lines when the item isn't in the last line.
                if !self.indexPathInLastLine(thisLayoutItem.indexPath) {
                    if let newHorizontalLayoutItem = layoutAttributesForDecorationView(ofKind: "Horizontal", at: thisLayoutItem.indexPath) {
                        layoutAttributes.append(newHorizontalLayoutItem)
                    }
                }
            }
        }
        return layoutAttributes
    }
}
