//
//  ZPHomeHeaderView.swift
//  ZaloPay
//
//  Created by Huu Hoa Nguyen on 10/8/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation

@objc public protocol ZPHomeHeaderViewDelegate : NSObjectProtocol {
    func didTouchScanQR()
    func didTouchBalance()
    func didTouchListCard()
    func didTouchSearch()
}
@objcMembers
public class ZPHomeHeaderView : UIView {
    public weak var delegate: ZPHomeHeaderViewDelegate?
    
    private lazy var scanButton = ZPLargeIconTitleButton.init()
    private lazy var balanceButton = ZPLargeIconTitleButton.init()
    private lazy var listCardButton = ZPLargeIconTitleButton.init()

    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
    }
    
    private func setupView() {
        self.addSubview(self.scanButton)
        self.addSubview(self.balanceButton)
        self.addSubview(self.listCardButton)

        self.scanButton.snp.makeConstraints ({ (make) in
            make.width.equalTo(self.snp.width).dividedBy(3)
            make.left.equalTo(0)
            make.top.equalTo(0)
            make.bottom.equalTo(0)
        })
        self.balanceButton.snp.makeConstraints ({ (make) in
            make.width.equalTo(self.snp.width).dividedBy(3)
            make.left.equalTo(self.scanButton.snp.right)
            make.top.equalTo(0)
            make.bottom.equalTo(0)
        })
        self.listCardButton.snp.makeConstraints ({ (make) in
            make.width.equalTo(self.snp.width).dividedBy(3)
            make.left.equalTo(self.balanceButton.snp.right)
            make.top.equalTo(0)
            make.bottom.equalTo(0)
        })
        
        self.backgroundColor = UIColor.zaloBase()
        
//        let enableQuickPay = ApplicationState.getBoolConfig(fromDic: "quickpay", andKey: "enable", andDefault: false)
//        let enableQuickPay = ZPApplicationConfig.getQuickPayConfig()?.getEnable().toBool() ?? false
        
        let titleBalanceBtn = ""//enableQuickPay == true ? R.string_QuickPay_Title().uppercased() : ""//R.string_Home_Balance()
        self.scanButton.setTitle(R.string_Home_PayBill())
        self.balanceButton.setTitle(titleBalanceBtn)
        self.listCardButton.setTitle(R.string_Home_LinkCard())
        
        let iconScanButton =  "header_payqrcode" //enableQuickPay == true ? "header_qrcode" : "header_payqrcode"
        let iconBalanceButton = "header_overbalance" //enableQuickPay == true ? "header_quickpay" : "header_overbalance"
        
        self.scanButton.setIcon(name: iconScanButton)
        self.balanceButton.setIcon(name: iconBalanceButton)
        self.listCardButton.setIcon(name: "header_linkbank")
        
        let scanQrTapGesture = UITapGestureRecognizer.init(target: self, action: #selector(scanButtonClick(_:)))
        self.scanButton.addGestureRecognizer(scanQrTapGesture)
        
        let balanceTapGesture = UITapGestureRecognizer.init(target: self, action: #selector(balanceButtonClick))
        self.balanceButton.addGestureRecognizer(balanceTapGesture)
        let listCardTapGesture = UITapGestureRecognizer.init(target: self, action: #selector(listCardButtonClick))
        self.listCardButton.addGestureRecognizer(listCardTapGesture)
        
        //if enableQuickPay.not {
            self.registerUpdateBalance()
        //}
    }
    
    func registerUpdateBalance() {
        self.updateBalanceView()
        
        ZMEventManager.registerHandler(self, eventType: EventTypeUpdateBalance.rawValue, with: {[weak self] (event: ZMEvent?) in
            DispatchQueue.main.async {
                self?.updateBalanceView()
            }
        })
    }
    
    func updateBalanceView() {
        guard let balanceValue = ZPWalletManagerSwift.sharedInstance.currentBalance else {
            self.balanceButton.setTitle("")
            return
        }
        self.showBalanceWithValue(balanceValue.int64Value)
    }
    
    func showBalanceWithValue(_ balanceValue: Int64) {
        if (balanceValue < 0) {
            // don't show balance value if
            // + balance is invalid
            // + or don't have correct value
            self.balanceButton.setTitle("")
            return
        }
    
//        NSString *text = [@(balanceValue) formatMoneyValue];
        self.balanceButton.setTitle(NSNumber.init(value: balanceValue).formatMoneyValue())
    }

    @objc func scanButtonClick(_ sender: UIGestureRecognizer) {
        if (sender.state != .ended) {
            return
        }
        
        self.delegate?.didTouchScanQR()
    }
    
    @objc func balanceButtonClick(_ sender: UIGestureRecognizer) {
        if (sender.state != .ended) {
            return
        }
        
        self.delegate?.didTouchBalance()
    }
    
    @objc func listCardButtonClick(_ sender: UIGestureRecognizer) {
        if (sender.state != .ended) {
            return
        }
        
        self.delegate?.didTouchListCard()
    }
}
