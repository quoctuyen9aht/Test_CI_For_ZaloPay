//
//  ZPDisclaimerModel.swift
//  ZaloPay
//
//  Created by bonnpv on 11/21/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayProfile
import ZaloPayConfig

class ZPDisclaimerModel: NSObject {
    let cacheDataTable = ZPCacheDataTable(db: PerUserDataBase.sharedInstance())
}

extension ZPDisclaimerModel: ZPHomeDisclaimerProtocol {
    func getDisclaimeMessage(_ appId: Int) -> String {
        let disclaimer = self.getDisclaimerConfig()
//        var message = disclaimer.value(forKey: "content", defaultValue: "")
//        let externalApps = disclaimer.value(forKey: "external_apps", defaultValue: Array<JSON>())
        
        var message = disclaimer?.getContent() ?? ""
        let externalApps = disclaimer?.getExternalApps() ?? []
        
        let infos = self.getMerchantInfo(from: appId, externalApps: externalApps)
        
        let merchantName = infos?.getMerchantName() ?? ""
        let merchantUrl = infos?.getMerchantUrl() ?? ""
        
//        var variables = infos.value(forKey: "variables", defaultValue: JSON())
        var variables = JSON() // ZPExternalAppDisclaimerConfigProtocol doesn't have 'variables' property
        variables["merchant_name"] = merchantName
        variables["merchantUrl"] = merchantUrl
        
        variables.enumerated().forEach({ (result) in
            let key = "{\(result.element.key)}"
            let value = result.element.value as? String ?? ""
            message = message.replacingOccurrences(of: key, with: value)
        })
        
        return message
    }
    
    func markDoneShowDisclaimer(_ appId: Int) {
        let key = self.disclaimerKey(from: appId)
        UserDefaults.standard.set(true, forKey: key)
    }
    
    private func getDisclaimerConfig() -> ZPDisclaimerConfigProtocol? {
        return ZPApplicationConfig.getDisclaimerConfig()
    }
    
    private func getMerchantInfo(from appId: Int, externalApps: [ZPExternalAppDisclaimerConfigProtocol]) -> ZPExternalAppDisclaimerConfigProtocol? {
        return externalApps.first(where: { $0.getAppId() == appId })
    }
    
    func checkAndShowDisclaimer(appId: Int, completeHandle: @escaping ()->Void) {
        guard shouldShowDisclaimPopup(appId) else {
            completeHandle()
            return
        }
        
        let msg = getDisclaimeMessage(appId)
        ZPDialogView.showDialog(    
            with: DialogTypeNotification,
            message: msg,
            cancelButtonTitle: R.string_Disclaimer_Accept_Button_Title(),
            otherButtonTitle: nil,
            completeHandle: {[weak self] _,_ in
                self?.markDoneShowDisclaimer(appId)
                completeHandle()
            }
        )
    }
       
    // chỉ hiển thị popup disclaimer 1 lần.
    // nếu click đồng ý -> đánh dấu done lần tới ko hỏi lại.
    
    func shouldShowDisclaimPopup(_ appId: Int) -> Bool {
        return self.isExternalApp(appId) && !self.isDoneShowDisclaim(appId)
    }
    
    private func isExternalApp(_ appId: Int) -> Bool {
        guard appId > 0 else {
            return false
        }
        let disclaimerData = self.getDisclaimerConfig()
//        let listIdInternal = disclaimerData.value(forKey: "list_id_internal", defaultValue: Array<Int>())
        let listIdInternal = disclaimerData?.getListInternalId() ?? []
        return listIdInternal.contains(appId).not
    }
    
    private func isDoneShowDisclaim(_ appId: Int) -> Bool {
        let key = self.disclaimerKey(from: appId)
        let value = UserDefaults.standard.bool(forKey: key)
        let deprecateKey = self.disclaimerKeyFromAppId(appId)
        let deprecateValue = ((cacheDataTable?.cacheDataValue(forKey: deprecateKey) ?? "") as NSString).boolValue
        if value == false && deprecateValue == true {
            UserDefaults.standard.set(true, forKey: key)
        }
        return value || deprecateValue
    }
    
    private func disclaimerKey(from appId: Int) -> String {
        let userid = ZPProfileManager.shareInstance.userLoginData?.paymentUserId ?? ""
        return "\(Merchant_Disclaimer_Key)_\(appId)_\(userid)"
    }
 
    private func disclaimerKeyFromAppId(_ appId: Int) -> String {
        return "\(Merchant_Disclaimer_Key)_\(appId)"
    }
}
