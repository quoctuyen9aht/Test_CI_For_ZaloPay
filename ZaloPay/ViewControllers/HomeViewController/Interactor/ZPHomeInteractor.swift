//
//  ZPHomeInteractor.swift
//  ZaloPay
//
//  Created by tridm2 on 10/27/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import ZaloPayAnalyticsSwift
import ZaloPayConfig

class ZPHomeInteractor: NSObject, ZPHomeInteractorProtocol {
    private static let transferAppId = -1
    private static let receiveAppId = -2
    private static let rechargeAppId = -3
    private static let ignoreAppIds = [reactInternalAppId, withdrawAppId, appVoucherId, showshowAppId]
    
    weak var presenter: ZPHomePresenterProtocol?
    
    fileprivate let disposeBag = DisposeBag()
    func trackEvent(trackId: ZPAnalyticEventAction) {
        ZPTrackingHelper.shared().trackEvent(trackId)
    }
    
    private func createSignalUpdate() -> RACSignal<AnyObject> {
        let zpWalletManager = ZPWalletManagerSwift.sharedInstance
        let s1 = zpWalletManager.getlAllMerchantApp()
        let s2 = zpWalletManager.getPlatformInfo()
        return s1.merge(s2)
    }
    
    func setupEvent() {
        // Badge
        ZPNotificationService.sharedInstance().signalTotalUnreadMessage().subscribeNext({
            [weak self] totalMessage in
            var msg = ""
            if let totalMessage = totalMessage as? NSNumber, totalMessage.intValue > 0 {
                msg = totalMessage.intValue > maxCountNotifications ? "\(maxCountNotifications)" + "+" : "\(totalMessage)"
            }
            self?.presenter?.recvTotalUnreadMsg(msg)
        })
        
        // Internet connection status
        let authenSuccessObservable = (~ZPConnectionManager.sharedInstance().eAuthenSuccess).map({ $0?.boolValue ?? true })
        let networkSignalObservable = (~NetworkState.sharedInstance().networkSignal).map { ($0 as? Bool) ?? true }
        let combineLatestObservable = Observable.combineLatest([authenSuccessObservable, networkSignalObservable])
            .takeUntil(self.rx.deallocating)
            .observeOn(MainScheduler.instance)
        
        combineLatestObservable
            .map({ $0.reduce(false, { $0 || $1 }) })
            .distinctUntilChanged()
            .bind
        { [weak self] in
            self?.presenter?.recvInternetConnectionStatus($0)
        }.disposed(by: disposeBag)
        
        Observable<Int>.empty().delay(5, scheduler: MainScheduler.instance).subscribe { [weak self] in
            self?.presenter?.recvInternetConnectionStatus(NetworkState.sharedInstance().isReachable)
        }.disposed(by: disposeBag)
    
        // Load React Native Apps
        (~createSignalUpdate()).observeOn(SerialDispatchQueueScheduler(qos: .background)).subscribe(onError: { [weak self](_) in
            self?.downloadAppResource()
        }, onCompleted: { [weak self] in
            self?.handleAppData()
        }).disposed(by: disposeBag)
        
        // Reload platform
        ZPWalletManagerSwift.sharedInstance.signalUpdatePlatform.take(until: self.rac_willDeallocSignal()).subscribeNext {
            [weak self] x in
            self?.updateReactNativeApp()
        }
    }
    
    private func updateReactNativeApp() {
        ZPWalletManagerSwift.sharedInstance.getlAllMerchantApp().subscribeCompleted {
            [weak self] in
            self?.handleAppData()
        }
    }
    
    private func handleAppData() {
        self.getActiveApps()
        self.downloadAppResource()
        let activeAppId = ReactNativeAppManager.sharedInstance().activeAppIds
        ReactNativeAppManager.sharedInstance().preloadAllMerchantUserInfo(activeAppId)
    }
    
    private func downloadAppResource() {
        ZPWalletManagerSwift.sharedInstance.downloadAppResource().deliverOnMainThread().take(until: self.rac_willDeallocSignal()).subscribeCompleted {
            [weak self] in
            self?.getActiveApps()
            RCTBridge.internal()
        }
    }
    
    func getActiveApps() {
        let sortApps = ReactNativeAppManager.sharedInstance().activeAppIds as? [Int] ?? []
        let ignoreAppIds = ZPHomeInteractor.ignoreAppIds
        let homePageItems = sortApps.filter({ ignoreAppIds.contains($0).not }).map({ self.createHomePageItem(with: $0) })
        self.buildDataSource(by: homePageItems)
    }
    private func buildDataSource(by items: Array<ZPHomePageItemBase>) {
        var arrayData:[ZPHomePageItemBase] = items
        func lastObjects() -> [ZPHomePageItemBase] {
            var result: [ZPHomePageItemBase] = (arrayData.count % 3) != 0 ? [ZPHomePageRightEmtyItem()] : []
            result.append(ZPHomePageBottomEmtyItem())
            return result
        }
        defer {
            arrayData += lastObjects()
            DispatchQueue.executeInMainThread {
                self.presenter?.recvDataSource(arrayData)
            }
        }
        
        self.createMainPaymentScenariosList().forEach {
            let order = $0.order - 1
            switch order {
            case ...0:
                arrayData.insert($0, at: 0)
            case arrayData.count...:
                arrayData.append($0)
            default:
                arrayData.insert($0, at: order)
            }
        }
    }
    
    private func createMainPaymentScenariosList() -> Array<ZPHomePageItem> {
        return self.getInternalAppsFromConfig().map({ self.createHomeItem($0) })
    }
    
    private func getInternalAppsFromConfig() -> [JSON] {
        guard let internalApp = ZPApplicationConfig.getTabHomeConfig()?.getInternalApps() else {
            return [[:]]
        }
        
        let routerMap: [Int: RouterId] = [ZPHomeInteractor.transferAppId : .transferMoney,
                                          ZPHomeInteractor.receiveAppId  : .receiveMoney,
                                          ZPHomeInteractor.rechargeAppId : .recharge]
        let trackIdMap: [Int: ZPAnalyticEventAction] = [ZPHomeInteractor.transferAppId : .home_touch_moneytransfer,
                                          ZPHomeInteractor.receiveAppId  : .home_touch_moneyreceive]
        
        return internalApp.map { (app) -> JSON in
            let appId = app.getAppId()
            let routerId = routerMap[appId]?.rawValue ?? 0
            let trackId = trackIdMap[appId]?.rawValue
            let icon_color = UIColor(hexString: app.getIconColor()) ?? UIColor.zaloBase()
            
            var result: JSON = [
                "display_name"  : app.getDisplayName(),
                "appId"         : appId,
                "icon_name"     : app.getIconName(),
                "icon_color"    : icon_color,
                "routerid"      : routerId,
                "order"         : app.getOrder()
                ]
            result["trackId"] = trackId
            return result
        }
    }    
    
    private func createHomePageItem(with appModel: ReactAppModel, moduleName: String) -> ZPHomePageItem {
        let title = appModel.appname ?? ""
        let iconName = appModel.iconName ?? ""
        let color: UIColor = UIColor(hexString: (appModel.color ?? "")) ?? UIColor.zaloBase()
        let dic: JSON = [
            "display_name": title,
            "moduleName": moduleName,
            "class": ZPDownloadingViewController.self,
            "appId": appModel.appid,
            "apptype": appModel.appType.rawValue,
            "icon_name": iconName,
            "icon_color": color
        ]
        let item = self.createHomeItem(dic)
        return item
    }
    
    private func createHomePageItem(with appId: Int) -> ZPHomePageItem {
        let app = ReactNativeAppManager.sharedInstance().getApp(appId)
        let moduleName = appId == lixiAppId ? ReactModule_RedPacket : ReactModule_PaymentMain
        return self.createHomePageItem(with: app, moduleName: moduleName)
    }
    
    private func createHomeItem(_ data: JSON) -> ZPHomePageItem {
        let itemData = ZPHomePageItem()
        itemData.title = data.string(forKey: "display_name")
        itemData.imageName = data.string(forKey: "icon_name")
        itemData.appId = data.int(forKey: "appId")
        itemData.color = data["icon_color"] as? UIColor
        itemData.order = data.int(forKey: "order", defaultValue: -1)
        itemData.dataModel = ZPHomeFeatureModel.fromDictionary(data)
        return itemData
    }
}

