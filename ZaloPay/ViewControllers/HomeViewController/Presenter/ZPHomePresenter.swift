//
//  ZPHomePresenter.swift
//  ZaloPay
//
//  Created by tridm2 on 10/27/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayAnalyticsSwift

class ZPHomePresenter: ZPHomePresenterProtocol {
    var interactor: ZPHomeInteractorProtocol?
    var router: ZPHomeRouterProtocol?
    weak var view: ZPHomeViewControllerProtocol?
    lazy var disclaimer = ZPDisclaimerModel()
    
    func prepare() {
        if let viewController = self.view as? BaseViewController {
            self.router?.willPushFrom(viewController)
        }
        
        self.interactor?.setupEvent()
    }
    
    func openHomePageItem(_ data: ZPHomeFeatureModel) {
        if let trackId = data.trackId {
            self.interactor?.trackEvent(trackId: trackId)
        }
        self.disclaimer.checkAndShowDisclaimer(appId: data.appId) { [weak self] in
             self?.router?.openApp(with: data)
        }
    }
    
    func handleTappedOnNaviCustomView(at idx: Int) {
        switch (idx) {
        case 0: //Scan QR
            self.openScreen(ScreenType.ScanQR)
        case 1: //Add card
            self.openScreen(ScreenType.ListCard)
        case 2: //Search
            self.openScreen(ScreenType.Search)
        case 3: //Notification
            self.showNotification()
        default:
            break
        }
    }
    
    func openScreen(_ type: ScreenType) {
        if let trackId = self.router?.showScreen(type) {
            self.interactor?.trackEvent(trackId: trackId)
        }
    }
    
    func showNotification() {
        self.router?.showNotification()
    }
    
    func showCachedActiveApps() {
        self.interactor?.getActiveApps()
    }
    
    func recvTotalUnreadMsg(_ totalMessage: String) {
        self.view?.badgeChanged(totalMessage)
    }
    
    func recvInternetConnectionStatus(_ status: Bool) {
        self.view?.internetConnectionStatusChanged(status)
    }
    
    func recvDataSource(_ dataSource: Array<ZPHomePageItemBase>) {
        self.view?.reloadData(dataSource)
    }
}
