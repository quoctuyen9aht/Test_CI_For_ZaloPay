//
//  ZPHomeRouter.swift
//  ZaloPay
//
//  Created by tridm2 on 10/27/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CocoaLumberjackSwift
import ZaloPayAnalyticsSwift
import ZaloPayConfig

enum ScreenType: Int {
    case ScanQR, Balance, ListCard, Search, InternetConnection
}

class ZPHomeRouter: ZPHomeRouterProtocol {
    weak var vc: BaseViewController!
    let reactAppRouter = ZPReactNativeAppRouter()
    private let disposeBag = DisposeBag()
    
    class func createHomeViewController() -> ZPHomeViewController {
        let view = ZPHomeViewController()
        let interactor = ZPHomeInteractor()
        let presenter = ZPHomePresenter()
        let router = ZPHomeRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return view
    }
    
    func willPushFrom(_ viewController: BaseViewController) {
        vc = viewController
    }
    
    func showScreen(_ type: ScreenType) -> ZPAnalyticEventAction? {
        var viewController: UIViewController!
        var trackId : ZPAnalyticEventAction?
        switch (type) {
        case .ScanQR:
            ZPCenterRouter.launch(routerId: .qrScan, from: vc)
            return ZPAnalyticEventAction.home_touch_pay
        case .Balance:
//            let enableQuickPay = ApplicationState.getBoolConfig(fromDic: "quickpay", andKey: "enable", andDefault: false)
            let enableQuickPay = ZPApplicationConfig.getQuickPayConfig()?.getEnable().toBool() ?? false
            let routerId : RouterId = enableQuickPay ? .paymentCode : .wallet
            ZPCenterRouter.launch(routerId: routerId, from: vc)
            return ZPAnalyticEventAction.home_touch_balance
        case .ListCard:
            ZPCenterRouter.launch(routerId: .linkCard, from: vc)
            return ZPAnalyticEventAction.home_touch_bank
        case .Search:
            let view = ZPSearchViewController()
            let presenter = ZPSearchPresenter()
            let interactor = ZPSearchInteractor()
            let router = ZPSearchRouter()
            
            view.presenter = presenter
            presenter.view = view
            presenter.router = router
            presenter.interactor = interactor
            viewController = view
            trackId = ZPAnalyticEventAction.home_touch_search
        case .InternetConnection:
            viewController = ZPInternetConnectionViewController()
            break
        }        
        vc.navigationController?.pushViewController(viewController, animated: true)
        return trackId
    }
    
    func showNotification() {      
        self.reactAppRouter.showNotificationView(from: vc)
        ZPNotificationService.sharedInstance().clearUnreadNotification()
    }
    
    func openApp(with data: ZPHomeFeatureModel) {
        if data.classType?.isSubclass(of: ZPDownloadingViewController.self) ?? false {
            let appType = ReactAppType(rawValue: UInt32(data.appType))
            ZPCenterRouter.launchExternalApp(appId: data.appId, appType: appType, from: vc, moduleName: data.moduleName)
            return
        }
        
        if let routerId = RouterId(rawValue: data.routerId) {
            ZPCenterRouter.launch(routerId: routerId, from: vc)
        }
        
    }    
}
