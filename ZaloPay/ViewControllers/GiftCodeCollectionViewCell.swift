//
//  GiftCodeCollectionViewCell.swift
//  ZaloPay
//
//  Created by thi la on 2/5/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit

class GiftCodeCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var info: UILabel!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var lineView: UIView!
    private var overlayView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.borderWidth = 0.5
        self.layer.cornerRadius = 4.0
        self.layer.borderColor = UIColor.placeHolder().cgColor
        self.overlayView = UIView(frame: self.bounds)
        self.overlayView.backgroundColor = UIColor.defaultBackground()
        self.overlayView.alpha = 0.65
        self.contentView.addSubview(overlayView)
        self.contentView.sendSubview(toBack: overlayView)
    }

    func setupData(_ data: GiftCodeCellModel) {
        let image = UIImage.fromInternalApp(data.imageName)
        self.imageView.image = image
        self.info.text = data.info
        if data.isRead {
            updateUIClicked()
        } else {
            updateUIDefault()
        }
    }
    
    func updateUIClicked() {
        self.lineView.isHidden = true
        self.overlayView.isHidden = false
        self.imageView.alpha = 0.8
        self.info.zpMainGrayRegular()
        self.info.textColor = UIColor.hex_0x727f8c()
        self.bottomView.backgroundColor = UIColor.defaultBackground()
    
    }
    
    func updateUIDefault() {
        self.lineView.isHidden = false
        self.overlayView.isHidden = true
        self.imageView.alpha = 1.0
        self.bottomView.backgroundColor = UIColor.white
        self.info.zpMediumTitleStyle()
        self.info.textColor = UIColor.defaultText()
    }
}

struct GiftCodeCellModel {
    let imageName: String
    var isRead: Bool {
        return self.message.isUnread.not
    }
    let info: String
    let message: ZPNotifyMessage

    init(imageName: String, info: String, message: ZPNotifyMessage) {
        self.imageName = imageName
        self.info = info
        self.message = message
    }
}
