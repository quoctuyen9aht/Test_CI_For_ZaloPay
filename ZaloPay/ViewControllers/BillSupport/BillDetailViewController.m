//
//  BillDetailViewController.m
//  ZaloPay
//
//  Created by bonnpv on 5/9/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "BillDetailViewController.h"
#import <ZaloPayAppManager/TransactionHistoryManager.h>
#import <ZaloPayFeedbackCollector/ZPFeedbackModel+ErrorFeedback.h>
#import <ZaloPayFeedbackCollector/ZPFeedbackViewController.h>
#import <ZaloPayCommon/NSCollection+JSON.h>
#import <ZaloPayNetwork/NetworkState.h>
#import <ZaloPayNetwork/ZaloPayErrorCode.h>
#import <ZaloPayNetwork/ZPNetWorkApi.h>
#import "ZPBill+ZaloPay.h"
#import "LoginManager.h"
#import "ZPRechargeMainViewController.h"
#import "ZPSelectBankView.h"
#import "UpdateProfileMainViewController.h"
#import ZALOPAY_MODULE_SWIFT
#import "ZPListBankTableViewController.h"
#import "ZPBankApiWrapper.h"
#import "ZPBankMapping.h"
#import "ZPReactNativeAppRouter.h"

@interface BillDetailViewController ()<ZaloPayWalletSDKCallBackDelegate>
@property (nonatomic, weak) UIViewController *fromViewController;
@property (nonatomic, copy) BillHandle completeHandle;
@property (nonatomic, copy) BillHandle errorHandle;
@property (nonatomic, copy) BillHandle cancelHandle;
@property (nonatomic, copy) BillHandle setAppIdErrorHandle;
@end

@implementation BillDetailViewController

- (id)initWithComplete:(BillHandle)complete cancel:(BillHandle)cancel error:(BillHandle)error {
    @weakify(self);
    return [self initWithComplete:complete
                           cancel:cancel
                            error:error
                  getAppInfoError:^(NSDictionary *data) {
                      @strongify(self);
                      [self handleGetAppInfoError:nil];
                  }];
}

- (id)initWithComplete:(BillHandle)complete
                cancel:(BillHandle)cancel
                 error:(BillHandle)error
       getAppInfoError:(BillHandle)appInfoError {
    self = [super init];
    if (self) {
        self.completeHandle = complete;
        self.errorHandle = error;
        self.cancelHandle = cancel;
        self.setAppIdErrorHandle = appInfoError;
    }
    return self;
}

- (void)setUpUiBar {
    self.navigationItem.leftBarButtonItems = [self leftBarButtonItems];
}

- (BOOL)isAllowSwipeBack {
    return NO;
}

- (NSArray *)leftBarButtonItems {
    UIButton * backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.multipleTouchEnabled = NO;
    backButton.exclusiveTouch = YES;
    [backButton setTitle:[R string_ButtonLabel_Cancel] forState:UIControlStateNormal];
    [backButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    backButton.titleLabel.font = [UIFont SFUITextRegularWithSize:15];
    backButton.frame = CGRectMake(0, 0, 44, 44);
//    [backButton addTarget:self action:@selector(backButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    UIBarButtonItem *negativeSeparator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSeparator.width = -12;
    return  @[negativeSeparator, backBarButtonItem];
}

#pragma mark - Internal Function
- (void) moveToRecharge {
    @weakify(self);
    ZPRechargePayBillSupportSwift *recharge = [[ZPRechargePayBillSupportSwift alloc] initWithComplete:^{
        @strongify(self);
        [self.navigationController popToViewController:self animated:NO];
        [self.bill updateBalance];
        [self reloadBill];
    } cancel:^{
        @strongify(self);
        [self.navigationController popToViewController:self animated:NO];
        [self reloadBill];
    } error:^{
        @strongify(self);
        [self.navigationController popToViewController:self animated:NO];
        [self reloadBill];
    }];
    [self.navigationController pushViewController:recharge animated:YES];
}
- (void)intenalRecharge {
    // Check Card
    if ([[ZPBankApiWrapper sharedInstance] hasBankToRecharge]) {
        [self moveToRecharge];
        return;
    }
    @weakify(self);
    [[[ZPBankApiWrapper sharedInstance] mapBankWith:self allowCCCard:NO] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        [self.navigationController popToViewController:self animated:NO];
        [self moveToRecharge];
    } error:^(NSError * _Nullable error) {
        @strongify(self);
        [self.navigationController popToViewController:self animated:NO];
        [self reloadBill];
    }];
}

- (BOOL)shouldDisableCCCard {
    return self.bill.transType == ZPTransTypeTranfer ||
           self.bill.transType == ZPTransTypeWithDraw ||
           self.bill.transType == ZPTransTypeWalletTopup ||
           self.bill.appId == lixiAppId;
}
    
- (void)internalListBankMapCard {
    BOOL enableCCCard = ![self shouldDisableCCCard];
    [[ZPListBankTableViewController showListBankOn:self
                                   isSupportCCCard:enableCCCard] subscribeNext:^(ZPBankSupport *bank) {
        if (!bank) {
            [self reloadBill];
            return;
        }
        if (bank.type == ZPBSupportType_Card) {
            if (bank.isCreditCard && ![[ZaloPayWalletSDKPayment sharedInstance] canMapCCCard]) {
                return;
            }
            
            //Check prevent map cc when pay transfer,deposit,redpacket
            NSString *errorMessage = [[ZaloPayWalletSDKPayment sharedInstance] checkErrorMappCCCardWith:self.bill.transType appId:self.bill.appId];
            self.bill.errorCCCanNotMapWhenPay = errorMessage;
            self.bill.isOnlySupportCCDebit = YES;
            if (bank.isCreditCard && errorMessage.length > 0) {
                [self showAlertPreventCCSavedCardWhenPay:errorMessage];
                return;
            }
            
            [self internalMapCard];
            return;
        }
        if ([ZPBankMapping bankType:bank.code first6CardNo:nil] == ZPVietcombank) {
            [self alertLinkVietCombankAccountFrom:self bankCode:bank.code];
            return;
        }
        [self internalMapAccount:bank.code];
    }];
}

- (void)internalMapCard {
    @weakify(self);
    BillDetailViewController *billDetailViewController = [[BillDetailViewController alloc] initWithComplete:^(NSDictionary *data) {
        @strongify(self);
        [self.navigationController popToViewController:self animated:NO];
        [self reloadBill];
    } cancel:^(NSDictionary *data) {
        @strongify(self);
        BOOL isPopToBill = [data boolForKey:@"popToBill" defaultValue:false];
        if (isPopToBill){
            [self.navigationController popToViewController:self animated:NO];
            [self reloadBill];
            return;
        }
        [self popToBankListVC];
    } error:^(NSDictionary *data) {
        @strongify(self);
        [self.navigationController popToViewController:self animated:NO];
        [self reloadBill];
    }];
    
    ZPBill *bill = [ZPBill mapCard];
    bill.shouldSkipSuccessView = YES;
    bill.errorCCCanNotMapWhenPay = self.bill.errorCCCanNotMapWhenPay;
    bill.isOnlySupportCCDebit = self.bill.isOnlySupportCCDebit;
    [billDetailViewController startProcessBill:bill from:self];
}

- (void)internalMapAccount:(NSString *)bankCode {
    
    @weakify(self);
    BillDetailViewController *billDetailViewController = [[BillDetailViewController alloc] initWithComplete:^(NSDictionary *data) {
        @strongify(self);
        [self.navigationController popToViewController:self animated:NO];
        [self reloadBill];
    } cancel:^(NSDictionary *data) {
        @strongify(self);
        BOOL isPopToBill = [data boolForKey:@"popToBill" defaultValue:false];
        if (isPopToBill){
            [self.navigationController popToViewController:self animated:NO];
            [self reloadBill];
            return;
        }
        [self popToBankListVC];
    } error:^(NSDictionary *data) {
        @strongify(self);
        [self.navigationController popToViewController:self animated:NO];
        [self reloadBill];
    }];
    
    ZPBill *bill = [ZPBill addBankAcount:bankCode];
    bill.shouldSkipSuccessView = YES;
    bill.errorCCCanNotMapWhenPay = self.bill.errorCCCanNotMapWhenPay;
    [billDetailViewController startProcessBill:bill from:self];
}

- (void)internalSelectBank {
    @weakify(self);
    [ZPSelectBankView showWithType:ZPBSupportType_Account withComplete:^(ZPBankSupport *bank) {
        @strongify(self);
        if (bank) {
            [self internalMapAccount:bank.code];
            return;
        }
        [self reloadBill];
    }];
}

- (void)updateProfileLevel3 {
    
    BOOL isDoneUpload = [[[PerUserDataBase sharedInstance] cacheDataValueForKey:kDisableUpdateProfileLevel3] boolValue];
    if (isDoneUpload) {
        [ZPDialogView showDialogWithType:DialogTypeInfo
                                 message:[R string_UpdateProfileLevel3_UploadSuccess_Message]
                       cancelButtonTitle:[R string_ButtonLabel_Close]
                       ortherButtonTitle:nil
                          completeHandle:nil];
         [self paymentReponseFail:nil];
        return;
    }
    
    UINavigationController *currentNavi = self.navigationController;
    UpdateProfileMainViewController *viewController = [[UpdateProfileMainViewController alloc]initWithCancel:nil
     error:nil
    andComplete:^{
        [currentNavi dismissViewControllerAnimated:true completion:nil];
    }];
    UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:viewController];
    [navi defaultNavigationBarStyle];
    [currentNavi presentViewController:navi animated:true completion:^{
        [self paymentReponseFail:nil];
    }];
}

- (void)updateProfileLevelAndLinkAccount:(NSString *)bankCode {
    [self internalMapAccount:bankCode];
}

- (void)updateProfileLevelAndSelectBank {
    [self internalSelectBank];
}

#pragma mark - Navigator

- (void)updateBalanceWithData:(NSDictionary *)data {
    long balance = [data doubleForKey:@"balance"];
    [[ZPWalletManager sharedInstance] updateBalanceAndTransactionLogWithBalanceValue:balance];
}

- (void)reloadBill {
    dispatch_main_async_safe(^{
        [self startGetAppInfo];
    });
}

- (BOOL)handleBillWithInvalidAppId:(ZPBill *)bill {
    if (bill.appId <= 0) {
        if (self.setAppIdErrorHandle) {
            self.setAppIdErrorHandle(nil);
            [self clearAllHandle];
        }
        return YES;
    }
    return NO;
}

- (void)startGetAppInfo {
    if ([self handleBillWithInvalidAppId:self.bill]) {
        return;
    }
    [[ZPAppFactory sharedInstance] showHUDAddedTo:self.view];
    
    NSTimeInterval startTime = [[NSDate date] timeIntervalSince1970] * 1000;
    // note: không dùng weakify ở đây để chờ signal thực hiện xong.
    [[[[self loadAppInfoAndVoucher] deliverOnMainThread] finally:^{
        [[ZPAppFactory sharedInstance] hideAllHUDsForView:nil];
    }]subscribeNext:^(ZPAppData *app) {
        [self hanldeAppInfo:app];
        [self trackAPIGetAppInfo:startTime returnCode:ZALOPAY_ERRORCODE_SUCCESSFUL];
    } error:^(NSError *error) {
        
        ZPAOrderData *data = [ZPAOrderData getAppInfo:self.bill.appTransID
                                                appid:[@(self.bill.appId) stringValue]
                                            transtype:self.bill.transType
                                               source:self.bill.ordersource
                                               result:OrderStepResult_Fail
                                        server_result:(int)error.code];
        [[ZPAppFactory sharedInstance].orderTracking startTrackOrder:data];

        if (self.setAppIdErrorHandle) {
            self.setAppIdErrorHandle(nil);
        }
        // case user vào thanh toán thiếu tiền -> liên kết thẻ -> back ra reload bill nhưng lúc getappinfo bị lỗi -> bị trắng trang
        if (self.navigationController && self.errorHandle) {
            self.errorHandle(nil);
        }
        [self clearAllHandle];
        [self trackAPIGetAppInfo:startTime returnCode:NSURLErrorTimedOut];
    }];
}

- (RACSignal *)loadAppInfoAndVoucher {
    ZaloPayWalletSDKPayment *sdk = [ZaloPayWalletSDKPayment sharedInstance];
    NSArray *transtypes = @[@(self.bill.transType)];
    if (self.bill.transType != ZPTransTypeBillPay) {
        return [sdk getAppInfoWithId:self.bill.appId transtypes:transtypes];
    }

    ZPVoucherManager *voucherManager = sdk.zpVoucherManager;
    RACSignal *vSignal = [voucherManager preloadVoucherForBill:self.bill];    
    return [vSignal concat:[sdk getAppInfoWithId:self.bill.appId transtypes:transtypes]];
}

- (void)trackAPIGetAppInfo:(NSTimeInterval)startTime returnCode:(int)returnCode {
    NSTimeInterval endTime = [[NSDate date] timeIntervalSince1970] * 1000;
    [[ZPAppFactory sharedInstance].orderTracking trackingApptransId:self.bill.appTransID
                                   apiId:@(requestEventIdFromApi(api_v001_tpe_getappinfo))
                               timeBegin:@(startTime)
                                 timeEnd:@(endTime)
                              returnCode:@(returnCode)];
}

- (void)handleGetAppInfoError:(NSError *)error {
    
    if ([NetworkState sharedInstance].isReachable == false) {
        [ZPDialogView showDialogWithType:DialogTypeNoInternet
                                   title:[R string_Dialog_Warning]
                                 message:[R string_NetworkError_NoConnectionMessage]
                            buttonTitles:@[[R string_ButtonLabel_Close]]
                                 handler:nil];
        return;
    }
    [ZPDialogView showDialogWithType:DialogTypeWarning
                             message:[R string_PayBillSetAppId_ErrorMessage]
                   cancelButtonTitle:[R string_ButtonLabel_Close]
                   ortherButtonTitle:nil
                      completeHandle:nil];
}

- (void)hanldeAppInfo:(ZPAppData *)app {
    UINavigationController *navi = self.fromViewController.navigationController;
    [[ZaloPayWalletSDKPayment sharedInstance] startHandleBill:self.bill withDelegate:self navigaionController:navi];    
    ZPAOrderData *data = [ZPAOrderData getAppInfo:self.bill.appTransID
                                            appid:[@(self.bill.appId) stringValue]
                                        transtype:self.bill.transType
                                           source:self.bill.ordersource
                                           result:OrderStepResult_Success
                                    server_result:1];
    [[ZPAppFactory sharedInstance].orderTracking startTrackOrder:data];
}

#pragma mark - Function

- (void)startProcessBill:(ZPBill *)bill from:(UIViewController *)fromViewController {
    if ([[ZaloPayWalletSDKPayment sharedInstance] checkIsCurrentPaying]){
        return;
    }
    self.bill = bill;
    self.fromViewController = fromViewController;
    [self startGetAppInfo];
}
-(void)popToBankListVC{
    
    //Back to select bank
    NSMutableArray *controllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    for (UIViewController *controller in controllers) {
        if ( [controller isKindOfClass:[ZPListBankTableViewController class]] ) {
            [self.navigationController popToViewController:controller animated:NO];
            break;
        }
    }
    
    //[self reloadBill];
}
#pragma mark - SDK Notify Delegate

- (void)paymentNotifyCloseWithKey:(NSString*)key andData:(NSDictionary*)data {}

- (void)paymentNotifySuccess:(NSDictionary*)data {
    [self updateBalanceWithData:data];
    if ([self.delegate respondsToSelector:@selector(paymentNotifySuccess:)]) {
        [self.delegate paymentNotifySuccess:data];
    }
}

- (void)paymentNotifyFail:(NSDictionary*)data {
    if ([self.delegate respondsToSelector:@selector(paymentNotifyFail:)]) {
        [self.delegate paymentNotifyFail:data];
    }
}

- (void)paymentNotifyUnidentified {
    if ([self.delegate respondsToSelector:@selector(paymentNotifyUnidentified)]) {
        [self.delegate paymentNotifyUnidentified];
    }
}

#pragma mark - SDK Delegate

- (void) paymentDidCloseWithKey:(NSString*)key andData:(NSDictionary*)data {
    DDLogInfo(@"key = %@",key);
    DDLogInfo(@"data = %@",data);
    
    if (![key isKindOfClass:[NSString class]]) {
        key = @"";
    }
    
    if (![data isKindOfClass:[NSDictionary class]]) {
        data = @{};
    }
    
    if ([key isEqualToString:ZP_NOTIF_PAYMENT_GO_MAPCARD]) {
        [self internalMapCard];
        return;
    }
   
    if ([key isEqualToString:ZP_NOTIF_PAYMENT_GO_LISTBANK_MAPCARD]) {
        [self internalListBankMapCard];
        return;
    }
    
    if ([key isEqualToString:ZP_NOTIF_PAYMENT_GO_LINKACOUNT]) {
        NSString *bankCode = [data stringForKey:@"bankCode"];
        [self internalMapAccount:bankCode];
        return;
    }
    
    if ([key isEqualToString:ZP_NOTIF_PAYMENT_GO_SELECTBANK]) {
        [self internalSelectBank];
        return;
    }
    
    if ([key isEqualToString:ZP_NOTIF_PAYMENT_MY_WALLET]) {
        [self intenalRecharge];
        return;
    }
    
    if ([key isEqualToString:ZP_NOTIF_UPGRADE_LEVEL_AND_GO_LINKACOUNT]) {
        NSString *bankCode = [data stringForKey:@"bankCode"];
        [self updateProfileLevelAndLinkAccount:bankCode];
        return;
    }
    
    if ([key isEqualToString:ZP_NOTIF_UPGRADE_LEVEL_AND_SELECTBANK]) {
        [self updateProfileLevelAndSelectBank];
        return;
    }
    
    if ([key isEqualToString:ZP_NOTIF_UPGRADE_LEVEL_3]) {
        [self updateProfileLevel3];
        return;
    }
    if ([key isEqualToString:ZP_NOTIF_PAYMENT_IS_MAINTAINANCE]) {
        [self clearAllHandle];
        return;
    }
    if ([key isEqualToString:ZP_NOTIF_PAYMENT_CANCEL_REGISTER_BANK]) {
        self.cancelHandle(@{@"popToBill":@true});
        [self clearAllHandle];
        return;
    }
//    if ([key isEqualToString:ZP_NOTIF_VIEW_BANK_ACCOUNTS]) {
//        if (self.cancelHandle) {
//            NSDictionary *action = @{@"action": @(ZPBillAction_ShowListAccount)};
//            self.cancelHandle(action);
//            [self clearAllHandle];
//        }
//        return;
//    }

    if (self.cancelHandle) {
        self.cancelHandle(nil);
        [self clearAllHandle];
    }
}
- (void)paymentTrackingEventWithData:(NSDictionary *)data {
    
}

- (void) paymentReponseSuccess:(NSDictionary*)data {
    DDLogInfo(@"paymentReponseSuccess : %@",data);
    if (self.completeHandle) {
        self.completeHandle(data);
        [self clearAllHandle];
    }
}

- (void) paymentReponseFail:(NSDictionary *)data {
//    @"errorcode" : @(response.errorCode),
//    @"message" : response.message,
    
    if (self.errorHandle) {
        self.errorHandle(data);
        [self clearAllHandle];
    }
    [[TransactionHistoryManager shareInstance] loadFailTransaction];
}

- (void) paymentReponseUnidentified {
    [self paymentReponseFail:nil];
}

- (void)clearAllHandle {
    self.completeHandle = nil;
    self.errorHandle = nil;
    self.cancelHandle = nil;
    self.setAppIdErrorHandle = nil;
}

- (void) paymentNeedToForceUpdateNewVersion {
    [[LoginManagerSwift sharedInstance] alertForceUpdateApp];
}

- (void)paymentNotifyShowFAQ {
    UIViewController* help = [[ZPReactNativeAppRouter new] supportCenterViewController];
    [self.navigationController pushViewController:help animated:YES];
}

- (void)paymentNotifyShowSupportPage:(NSDictionary*)data {
    NSString *category = [data stringForKey:@"paytype"];
    NSString *transaction = [data stringForKey:@"zptransid"];
    UIImage *image = [data objectForKey:@"capturescreen"];
    NSString *errormessage = [data stringForKey:@"errormessage"];
    NSNumber *errorCode = [data numericForKey:@"errorcode" defaultValue:@(0)];
    if (![image isKindOfClass:[UIImage class]]) {
        image = nil;
    }
    ZPFeedbackModel *model = [[ZPFeedbackModel alloc] init];
    [model configDataSourceWithCategory:category
                          transactionId:transaction
                                  image:image
                                message:errormessage
                              errorCode:errorCode];
    
    ZPFeedbackViewController *controller = [[ZPFeedbackViewController alloc] initWithModel:model];
    [self.navigationController pushViewController:controller animated:TRUE];
}
#pragma mark - Alert

- (void)alertLinkVietCombankAccountFrom:(UIViewController *)viewController bankCode:(NSString *)bankCode {
    [ZPDialogView showDialogWithType:DialogTypeNotification
                               title:[R string_Dialog_Notification]
                    attributeMessage:[[ZPBankApiWrapper sharedInstance] getAttAlertLinkVietCombankAccountFrom:bankCode]
                        buttonTitles:@[[R string_ButtonLabel_OK], [R string_ButtonLabel_Close]]
                             handler:^(NSInteger index) {
                                 if (index == 0) {
                                     [self internalMapAccount:bankCode];
                                 }
                             }];
}

- (void)showAlertPreventCCSavedCardWhenPay:(NSString*)error {
    [ZPDialogView showDialogWithType:DialogTypeNotification
                             message:error
                   cancelButtonTitle:[R string_ButtonLabel_Close]
                   ortherButtonTitle:nil
                      completeHandle:nil];
    
}

@end
