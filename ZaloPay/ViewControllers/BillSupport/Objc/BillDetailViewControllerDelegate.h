//
//  BillDetailViewController.h
//  ZaloPay
//
//  Created by bonnpv on 5/9/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

@protocol BillDetailViewControllerDelegate<NSObject>
@optional
- (void)paymentNotifySuccess:(NSDictionary*)data;
- (void)paymentNotifyFail:(NSDictionary*)data;
- (void)paymentNotifyUnidentified;
- (void)handleSetAppIdError:(NSInteger)appId;
@end


