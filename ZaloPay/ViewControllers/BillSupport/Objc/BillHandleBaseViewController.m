//
//  BillHandleBaseViewController.m
//  ZaloPay
//
//  Created by bonnpv on 5/13/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "BillHandleBaseViewController.h"
#import <ZaloSDKCoreKit/ZaloSDKCoreKit.h>
#import <ZaloPayAnalytics/ZPAOrderData.h>
#import <ZaloPayWalletSDK/ZPProgressHUD.h>
#import <ZaloPayConfig/ZaloPayConfig-Swift.h>

#import ZALOPAY_MODULE_SWIFT

@interface BillHandleBaseViewController ()
@property (nonatomic) BOOL isLoading;
@end

@implementation BillHandleBaseViewController

- (void)backButtonClicked:(id)sender {
    if (!self.isLoading) {
        [super backButtonClicked:sender];
    }
}

#pragma mark - Loading

- (void)showLoadingView {
    if (!self.isLoading) {
        [[ZPAppFactory sharedInstance] showHUDAddedTo:self.navigationController.view];
        self.isLoading = TRUE;
    }
}

- (void)hideLoadingView {
    [[ZPAppFactory sharedInstance] hideAllHUDsForView:self.navigationController.view];
    self.isLoading = FALSE;
}

#pragma mark - Bill

- (void)showBillViewController:(ZPBill *)billInfo{
    [self usingSDKPoup:billInfo.appId] ? [self showPopupSDK:billInfo] : [self showNormalSDK:billInfo];
}

- (BOOL)usingSDKPoup:(NSInteger)appId {
    NSArray *popupModeAppList = [[ZPApplicationConfig getSDKConfig] getPopupModeAppList];
    return  [popupModeAppList containsObject:@(appId)];
}

- (void)showNormalSDK:(ZPBill *)billInfo {
    @weakify(self);
    BillDetailViewController *billDetailViewController = [[BillDetailViewController alloc] init:^(NSDictionary *data){
        @strongify(self);
        [self payBillDidCompleteSuccess];
    } cancel:^(NSDictionary *data){
        @strongify(self);
        [self payBillDidCancel];
    } error:^(NSDictionary *data){
        @strongify(self);
        [self payBillDidError];
    } getAppInfoError:^(NSDictionary *data) {
        @strongify(self);
        [self handleSetAppIdError:billInfo.appId];
    }];
    billDetailViewController.delegate = self;
    [billDetailViewController startProcessBill:billInfo from:self];
    self.isLoading = false;
}

- (void)showPopupSDK:(ZPBill *)bill {
    @weakify(self);
    [[ZaloPayWalletSDKPayment sharedInstance] showPopupPayment:self
                                                          with:bill
                                                      complete:^(id object)
    {
        @strongify(self);
        [self hideLoadingView];
        [self payBillDidCompleteSuccess];
    } error:^(NSError *e) {
        [self hideLoadingView];
        if ([e code] == NSURLErrorCancelled) {
            @strongify(self);
            [self payBillDidCancel];
            return;
        }
        @strongify(self);
        [self payBillDidError];
    }];
}

- (void)alertGetAppInfoError {
    [ZPDialogView showDialogWithType:DialogTypeWarning
                             message:[R string_PayBillSetAppId_ErrorMessage]
                   cancelButtonTitle:[R string_ButtonLabel_Close]
                    otherButtonTitle:nil
                      completeHandle:nil];
}



- (void)handleSetAppIdError:(NSInteger)appId {
    [ZPDialogView showDialogWithType:DialogTypeError
                             message:[R string_PayBillSetAppId_ErrorMessage]
                   cancelButtonTitle:[R string_ButtonLabel_Close]
                    otherButtonTitle:nil
                      completeHandle:nil];
}

#pragma mark - Bill Delegate

- (void)toHomePage {
    if (self.navigationController) {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}

- (void)payBillDidCompleteSuccess {
    [self toHomePage];
}

- (void)payBillDidCancel {
    [self.navigationController popToViewController:self animated:YES];
}

- (void)payBillDidError {
    [self toHomePage];
}

@end
