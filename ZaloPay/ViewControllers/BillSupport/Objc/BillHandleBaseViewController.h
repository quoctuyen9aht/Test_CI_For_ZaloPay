//
//  BillHandleBaseViewController.h
//  ZaloPay
//
//  Created by bonnpv on 5/13/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <ZaloPayAnalytics/ZPAOrderTracking.h>
#import "BillDetailViewControllerDelegate.h"

@interface BillHandleBaseViewController : BaseViewController <BillDetailViewControllerDelegate>
- (void)showLoadingView;
- (void)hideLoadingView;

- (void)showBillViewController:(ZPBill *)billInfo;
- (void)handleSetAppIdError:(NSInteger)appId;
- (void)payBillDidCompleteSuccess;
- (void)payBillDidCancel;
- (void)payBillDidError;
- (void)toHomePage;
@end
