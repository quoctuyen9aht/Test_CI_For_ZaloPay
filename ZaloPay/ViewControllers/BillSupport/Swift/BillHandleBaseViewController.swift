//
//  BillHandleBaseViewController.swift
//  ZaloPay
//
//  Created by tridm2 on 12/26/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

//import UIKit

//@objc class BillHandleBaseViewController: BaseViewController {
//    var isLoading = false
//    
//    override func backButtonClicked(_ sender: Any) {
//        if isLoading.not {
//            super.backButtonClicked(sender)
//        }
//    }
//    
//    // MARK: Loading
//    func showLoadingView() {
//        if isLoading.not {
//            ZPAppFactory.sharedInstance().showHUDAdded(to: navigationController?.view)
//            isLoading = true
//        }
//    }
//    
//    func hideLoadingView() {
//        ZPAppFactory.sharedInstance().hideAllHUDs(for: navigationController?.view)
//        isLoading = false
//    }
//
//    
//    // MARK: Bill
//    func showBillViewController(_ billInfo: ZPBill) {
//        let billDetailViewController = BillDetailViewController(complete: { [weak self] (data) in
//            self?.payBillDidCompleteSuccess()
//        }, cancel: { [weak self] (data) in
//            self?.payBillDidCancel()
//        }, error: { [weak self] (data) in
//            self?.payBillDidError()
//        }) { [weak self] (data) in
//            self?.handleSetAppIdError(Int(billInfo.appId))
//        }
//        
//        billDetailViewController?.delegate = self
//        billDetailViewController?.startProcessBill(billInfo, from: self)
//        self.isLoading = false
//    }
//    
//    // MARK: BillDetailViewControllerDelegate
//    func toHomePage() {
//        if navigationController != nil {
//            navigationController?.popToRootViewController(animated: false)
//        }
//    }
//    
//    func payBillDidCompleteSuccess() {
//        toHomePage()
//    }
//    
//    func payBillDidCancel() {
//        navigationController?.popToViewController(self, animated: true)
//    }
//    
//    func payBillDidError() {
//        toHomePage()
//    }
//}
//
//extension BillHandleBaseViewController: BillDetailViewControllerDelegate {
//    func paymentNotifySuccess(_ data: [AnyHashable: Any]) {
//    }
//    
//    func paymentNotifyFail(_ data: [AnyHashable: Any]) {
//    }
//    
//    func paymentNotifyUnidentified() {
//    }
//    
//    func handleSetAppIdError(_ appId: Int) {
//        ZPDialogView.showDialog(with: DialogTypeError,
//                                message: R.string_PayBillSetAppId_ErrorMessage(),
//                                cancelButtonTitle: R.string_ButtonLabel_Close(),
//                                otherButtonTitle: nil,
//                                completeHandle: nil)
//    }
//}

