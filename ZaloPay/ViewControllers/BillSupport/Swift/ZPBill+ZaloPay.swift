//
//  ZPBill+ZaloPay.swift
//  ZaloPay
//
//  Created by tridm2 on 12/26/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayProfile
import ZaloPayCommonSwift

@objc extension ZPBill {
    class func getRemovedBankAccount(bankCode: String) -> ZPIBankingAccountBill {
        let bill = ZPIBankingAccountBill(data: nil,
                                         transType: .atmMapCard,
                                         orderSource: Int32(OrderSource_None.rawValue),
                                         appId: kZaloPayClientAppId)
        bill.bankCode = bankCode
        bill.isMapAccount = false
        return bill
    }
    
    class func getAddedBankAcount(bankCode: String) -> ZPIBankingAccountBill {
        let bill = ZPIBankingAccountBill(data: nil,
                                         transType: .atmMapCard,
                                         orderSource: Int32(OrderSource_None.rawValue),
                                         appId: kZaloPayClientAppId)
        
        let phone = ZPProfileManager.shareInstance.userLoginData?.phoneNumber ?? ""
        bill.appUserInfo["phone"] = phone
        
        bill.bankCode = bankCode
        bill.isMapAccount = true
        bill.bankCustomerId = NetworkManager .sharedInstance().paymentUserId
        return bill
    }
    
    class func getMappedCard() -> ZPBill {
        return ZPBill(data: nil,
                      transType: .atmMapCard,
                      orderSource: Int32(OrderSource_None.rawValue),
                      appId: kZaloPayClientAppId)
    }
    
    @objc public convenience init (data: [String:Any]?, transType: ZPTransType, orderSource: Int32, appId: Int) {
        if let data = data {
            let items = data.string(forKey: "item")
            let apptransid = data.string(forKey: "apptransid")
            let billdescription = data.string(forKey: "description")
            let embeddata = data.string(forKey: "embeddata")
            let appuser = data.string(forKey: "appuser")
            let amount = data.int(forKey: "amount")
            let time = data.int64(forKey: "apptime")
            let mac = data.string(forKey: "mac")
            // NSDictionary *userInfo;
            
            self.init(items: items, andAppTranxID: apptransid, andDescription: billdescription, andEmbedData: embeddata)
            self.appUser = appuser
            self.amount = amount
            self.time = time
            self.mac = mac
            self.rawData = data
        } else {
            self.init()
        }
        self.appId = Int32(appId)
        self.transType = transType
        self.ordersource = orderSource
        
//        NSMutableDictionary *appUserInfo = userInfo.count == 0 ? [NSMutableDictionary dictionary] :
//        [NSMutableDictionary dictionaryWithDictionary:userInfo];
        var appUserInfo = [AnyHashable:Any]()
        
        let balance = ZPWalletManagerSwift.sharedInstance.currentBalance
        
        if let currentZaloUser = ZPProfileManager.shareInstance.currentZaloUser {
            appUserInfo["zaloid"] = currentZaloUser.userId
        }
        
        appUserInfo["balance"] = balance
        
        let lastCoordinate = ZPLocationManagerSwift.share.getCoordinate()
        appUserInfo["latitude"] = lastCoordinate.latitude
        appUserInfo["longitude"] = lastCoordinate.longitude
        
        self.appUserInfo = appUserInfo
    }
    
    func updateBalance() {
        var dict = [AnyHashable:Any]()
        if let appUserInfo = self.appUserInfo {
            dict += appUserInfo
        }
        
        let balance = ZPWalletManagerSwift.sharedInstance.currentBalance
        dict["balance"] = balance
        
        self.appUserInfo = dict
    }
    
    func addUserInfo(_ info: [AnyHashable:Any]) {
        var dict = [AnyHashable:Any]()
        if let appUserInfo = self.appUserInfo {
            dict += appUserInfo
        }
        dict += info
        self.appUserInfo = dict
    }
}
