//
//  BillDetailViewController.swift
//  ZaloPay
//
//  Created by tridm2 on 12/26/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import RxCocoa
import CocoaLumberjackSwift
import RxSwift

typealias BillHandle = ([AnyHashable:Any]?) -> Void

//@objc protocol BillDetailViewControllerDelegate: class {
//    func paymentNotifySuccess(_ data: [AnyHashable: Any]?)
//    func paymentNotifyFail(_ data: [AnyHashable: Any]?)
//    func paymentNotifyUnidentified()
//    func handleSetAppIdError(_ appId: Int)
//}

@objcMembers
public class BillDetailViewController : BaseViewController {
    var bill: ZPBill!
    weak var delegate: BillDetailViewControllerDelegate?
    var fromViewController: UIViewController?
    var completeHandle: BillHandle?
    var errorHandle: BillHandle?
    var cancelHandle: BillHandle?
    var setAppIdErrorHandle: BillHandle?
    var transactionHistoryManager: TransactionHistoryManager!
    private let disposeBag = DisposeBag()
    lazy var updateLevel3helper = ZPUploadProfileLevel3Helper()
    
    convenience init(_ complete: @escaping BillHandle, cancel: @escaping BillHandle, error: @escaping BillHandle) {
        self.init(complete, cancel: cancel, error: error, getAppInfoError: nil)
        setAppIdErrorHandle = { [weak self] (data) in
            self?.handleGetAppInfoError()
        }
    }
    
    convenience init(_ complete: @escaping BillHandle, cancel: @escaping BillHandle, error: @escaping BillHandle, getAppInfoError appInfoError: BillHandle?) {
        self.init()
        completeHandle = complete
        errorHandle = error
        cancelHandle = cancel
        setAppIdErrorHandle = appInfoError
        self.transactionHistoryManager = TransactionHistoryManager()
    }
    
//    func setupUiBar() {
//        navigationItem.leftBarButtonItems = self.createLeftBarButtonItems()
//    }
    
    public override func isAllowSwipeBack() -> Bool {
        return false
    }
    
    private func createLeftBarButtonItems() -> [UIBarButtonItem] {
        let backButton = UIButton(type: .custom)
        backButton.isMultipleTouchEnabled = false
        backButton.isExclusiveTouch = true
        backButton.setTitle(R.string_ButtonLabel_Cancel(), for: .normal)
        backButton.setTitleColor(UIColor.white, for: .normal)
        backButton.titleLabel?.font = UIFont.sfuiTextRegular(withSize: 15)
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        //    [backButton addTarget:self action:@selector(backButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        let backBarButtonItem = UIBarButtonItem(customView: backButton)
        let negativeSeparator = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        negativeSeparator.width = -12
        return [negativeSeparator, backBarButtonItem]
    }
    
    // MARK: Internal Function
    private func popViewToSelfAndReload() {
        self.navigationController?.popToViewController(self, animated: false)
        self.reloadBill()
    }
    
    private func moveToRecharge() {
        let recharge = ZPRechargePayBillSupportSwift(complete: { [weak self] in
            self?.bill.updateBalance()
            self?.popViewToSelfAndReload()
        }, cancel: { [weak self] in
            self?.popViewToSelfAndReload()
        }, error: { [weak self] in
            self?.popViewToSelfAndReload()
        })
        self.navigationController?.pushViewController(recharge, animated: true)
    }
    
    private func intenalRecharge() {
        // Check Card
        if ZPBankApiWrapperSwift.sharedInstance.hasBankToRecharge() {
            self.moveToRecharge()
            return
        }
        ZPBankApiWrapperSwift.sharedInstance.mapBank(with: self,
                                                  appId: kZaloPayClientAppId,
                                                  transType: .walletTopup).subscribeNext({ [weak self] (x) in
            guard let wSelf = self else {
                return
            }
            wSelf.navigationController?.popToViewController(wSelf, animated: false)
            wSelf.moveToRecharge()
        }, error: { [weak self] (err) in
            self?.popViewToSelfAndReload()
        })
    }
    
    private func signalMapBank(_ enableCCCard: Bool) -> RACSignal<AnyObject>{
        let signal: RACSignal<AnyObject>
        let appId = Int(self.bill?.appId ?? -1)
        let canUseGateway = ZPSDKHelper().canUseGateway(appId)
        if canUseGateway.not {
            signal = ZPListBankTableViewController.showListBank(on: self, isSupportCCCard: enableCCCard, for: bill.transType)
        } else {
            signal = ZPListBankTableViewController.showListUsingPayment(on: self, isSupportCCCard: enableCCCard, appId: appId, for: bill.transType, amount: Int64(bill.amount))
        }
        return signal
    }
    
    private func map(using bank:ZPBankSupport, enableCCCard: Bool) {
        if bank.type == ZPBSupportType_Card {
            let isCC = bank.isCreditCard || bank.isCCDebitCard
            let isMaxCCSavedCard = ZaloPayWalletSDKPayment.sharedInstance().isMaxCCSavedCard()
            if isCC && isMaxCCSavedCard {
                ZaloPayWalletSDKPayment.sharedInstance().showAlertMaxCCSavedCard()
                return
            }
            
            //Check prevent map cc when pay transfer,deposit,redpacket
            let errMessage = ZaloPayWalletSDKPayment.sharedInstance().checkErrorMappCCCard(with: self.bill.transType, appId: self.bill.appId) ?? ""
            self.bill.errorCCCanNotMapWhenPay = errMessage
            self.bill.unsuportCCCredit = enableCCCard.not && ZPSDKHelper().isSupportCCDebit()
            if (bank.isCreditCard || bank.isCCDebitCard) && errMessage.isEmpty.not {
                self.showAlertPreventCCSavedCard(whenPay: errMessage)
                return
            }
            self.internalMapCard()
            return
        }
        
        if ZPBankMapping.bankType(bank.code, first6CardNo: nil) == .ZPVietcombank {
            self.alertLinkVietCombankAccount(from: self, bankCode: bank.code, bankName: bank.name)
            return
        }
        self.internalMapAccount(bankCode: bank.code)
    }
    
    private func payByGateway(from bank: ZPBankSupport) {
        let bankCode = bank.code
        self.bill.bankCode = bankCode
        guard let url = URL(string: kUrlGateway) else {
            #if DEBUG
                fatalError("Check url")
            #else
                return
            #endif
            
        }
        self.bill.feeCharge = Int(bank.feeRate)
        (~ZaloPayWalletSDKPayment.sharedInstance().runGateway(self.bill, on: self, url: url, type: .payment)).subscribe(onNext: { [weak self](result) in
            let notifyName = Notification.Name.init(ZPTransactionsUpdated)
            NotificationCenter.default.post(name: notifyName, object: ["statusType" : 1], userInfo: [:])
            self?.completeHandle?(result as? [AnyHashable : Any])
        }, onError: { [weak self](e) in
            self?.errorHandle?([:])
        }).disposed(by: disposeBag)
    }
    
    private func internalListBankMapCard() {
        let enableCCCard = !ZPBankApiWrapperSwift.sharedInstance.sdkDisableCCCardAppId(appId: Int(self.bill.appId), transType: self.bill.transType)
        let signal = signalMapBank(enableCCCard)
        // ZPSDKHelper.shouldDisableCCCard(self.bill).not
        signal.subscribeNext({ [weak self] (x) in
            guard let wSelf = self,
                let bank = x as? ZPBankSupport else { // không có thẻ nào được user chọn từ danh sách các thẻ
                self?.reloadBill()
                return
            }
            
            if bank.statePayment == .gateway {
                // pay by gateway
                wSelf.payByGateway(from: bank)
                return;
            }
            wSelf.map(using: bank, enableCCCard: enableCCCard)
        })
    }
    
    private func createBillDetailViewController() -> BillDetailViewController {
        let billDetailViewController = BillDetailViewController({ [weak self] (data) in
            self?.popViewToSelfAndReload()
        }, cancel: { [weak self] (data) in
            let isPopToBill = data?.value(forKey: "popToBill", defaultValue: false) ?? false
            if isPopToBill {
                self?.popViewToSelfAndReload()
                return
            }
            self?.popToBankListVC()
        }, error: { [weak self] (data) in
            self?.popViewToSelfAndReload()
        })
        return billDetailViewController
    }
    
    private func internalMapCard() {
        let billDetailViewController = createBillDetailViewController()
        let bill = ZPBill.getMappedCard()
        bill.shouldSkipSuccessView = true
        bill.errorCCCanNotMapWhenPay = self.bill.errorCCCanNotMapWhenPay
        bill.unsuportCCCredit = self.bill.unsuportCCCredit
        billDetailViewController.startProcessBill(bill, from: self)
    }
    
    private func internalMapAccount(bankCode: String) {
        let billDetailViewController = createBillDetailViewController()
        let bill = ZPBill.getAddedBankAcount(bankCode: bankCode)
        bill.shouldSkipSuccessView = true
        bill.errorCCCanNotMapWhenPay = self.bill.errorCCCanNotMapWhenPay
        billDetailViewController.startProcessBill(bill, from: self)
    }
    
    private func internalSelectBank() {
        ZPSelectBankView.show(with: ZPBSupportType_Account) { [weak self] (bank) in
            if bank != nil {
                self?.internalMapAccount(bankCode: bank!.code)
                return
            }
            self?.reloadBill()
        }
    }
    
    private func updateProfileLevel3() {
        if updateLevel3helper.isDoneUpload() {
            updateLevel3helper.alertDoneUpload()
            self.paymentReponseFail(nil)
            return
        }
//        let controller = updateLevel3helper.updateLevel3ViewController({
//
//        }, error: {
//
//        }) { [weak self]() in
//            self?.navigationController?.dismiss(animated: true, completion: nil)
//        }
        let defaultController = BaseViewController()
        let navi = UINavigationController(rootViewController: defaultController)
        navi.defaultNavigationBarStyle()
        self.navigationController?.present(navi, animated: true) { [weak self] in
            self?.paymentReponseFail(nil)
        }
        
        updateLevel3helper.uploadLevel(from: defaultController, cancel: { [weak self] in
            self?.navigationController?.dismiss(animated: true, completion: nil)
        }, error: {
            
        }, complete: { [weak self] in
             self?.navigationController?.dismiss(animated: true, completion: nil)
        })
        
    }
    
    private func updateProfileLevelAndLinkAccount(_ bankCode: String) {
        self.internalMapAccount(bankCode: bankCode)
    }
    
    // MARK: Navigator
    private func updateBalance(withData data: [AnyHashable: Any]) {
        let balance = Int(data.double(forKey: "balance"))
        ZPWalletManagerSwift.sharedInstance.updateBalanceAndTransactionLog(withBalanceValue: balance)
    }
    
    private func reloadBill() {
        DispatchQueue.executeInMainThread { [weak self] in
            self?.startGetAppInfo()
        }
    }
    
    private func handleBillWithInvalidAppId(_ bill: ZPBill) -> Bool {
        if bill.appId <= 0 {
            if let handle = self.setAppIdErrorHandle {
                handle(nil)
                self.clearAllHandle()
            }
            return true
        }
        return false
    }
    
    private func startGetAppInfo() {
        if self.handleBillWithInvalidAppId(self.bill) {
            return
        }
        ZPAppFactory.sharedInstance().showHUDAdded(to: self.view)
        
        let startTime = Date().timeIntervalSince1970 * 1000
        
        // note: không dùng weakify ở đây để chờ signal thực hiện xong.
        self.loadAppInfoAndVoucher()?.deliverOnMainThread().finally {
            ZPAppFactory.sharedInstance().hideAllHUDs(for: nil)
        }.subscribeNext({ (app) in
            self.handleAppInfo(app)
            self.trackAPIGetAppInfo(startTime, returnCode: Int(ZALOPAY_ERRORCODE_SUCCESSFUL.rawValue))
        }, error: { (err) in
            guard let bill = self.bill else {
                return
            }
            
            let data = ZPAOrderData.getAppInfo(bill.appTransID,
                                               appid: "\(bill.appId)",
                                               transtype: Int32(bill.transType.rawValue),
                                               source: OrderSource(rawValue: UInt32(bill.ordersource)),
                                               result: .fail,
                                               server_result: Int32(err?.errorCode() ?? 0))
            ZPAppFactory.sharedInstance().orderTracking.startTrackOrder(data)
            
            if let handle = self.setAppIdErrorHandle {
                handle(nil)
            }
            // case user vào thanh toán thiếu tiền -> liên kết thẻ -> back ra reload bill nhưng lúc getappinfo bị lỗi -> bị trắng trang
            if self.navigationController != nil && self.errorHandle != nil {
                self.errorHandle!(nil);
            }
            self.clearAllHandle()
            self.trackAPIGetAppInfo(startTime, returnCode:NSURLErrorTimedOut);
        })
    }
    
    private func loadAppInfoAndVoucher() -> RACSignal<ZPAppData>? {
        guard let sdk = ZaloPayWalletSDKPayment.sharedInstance() else {
            return nil
        }
        
        let transtype = self.bill.transType
        
        if self.bill.transType != .billPay {
            return sdk.getAppInfo(withId: Int(self.bill.appId), transtypes: ["\(transtype)"]) as? RACSignal<ZPAppData>
        }
        let appid = self.bill.appId
        let voucherManager = sdk.zpVoucherManager
        let promotionManager = sdk.zpPromotionManager
        let signal = (voucherManager?.preloadVoucher(for: self.bill) ?? RACSignal.empty()).doCompleted {
            #if DEBUG
                print("1")
            #endif
            
        }
        
        let signalGetPromotions = (promotionManager?.loadRemotePromotions(for: self.bill) ?? RACSignal.empty()).doCompleted {
            #if DEBUG
            print("0")
            #endif
            
        }
        let signalLoadGateway = (sdk.getBankPaymentGateway(Int(appid)) ?? RACSignal.empty()).doCompleted {
            #if DEBUG
                print("2")
            #endif
        }
        
        let signalLoadAppInfor = (sdk.getAppInfo(withId: Int(self.bill.appId), transtypes: ["\(transtype)"]) ?? RACSignal.empty()).doCompleted {
            #if DEBUG
                print("3")
            #endif
        }
        let signalGetInfor = ((signalGetPromotions.concat(signal)).concat(signalLoadGateway)).concat(signalLoadAppInfor)
        return (signalGetInfor.filter({ $0 is ZPAppData })) as? RACSignal<ZPAppData>
    }
    
    private func trackAPIGetAppInfo(_ startTime: TimeInterval, returnCode: Int) {
        let endTime = Date().timeIntervalSince1970 * 1000
        ZPAppFactory.sharedInstance().orderTracking.trackingApptransId(self.bill.appTransID,
                                                                       apiId: requestEventIdFromApi(api_v001_tpe_getappinfo) as NSNumber,
                                                                       timeBegin: startTime as NSNumber,
                                                                       timeEnd: endTime as NSNumber,
                                                                       returnCode: returnCode as NSNumber)
    }
    
    private func handleGetAppInfoError() {
        if NetworkState.sharedInstance().isReachable.not {
            ZPDialogView.showDialog(with: DialogTypeNoInternet,
                                    title: R.string_Dialog_Warning(),
                                    message: R.string_NetworkError_NoConnectionMessage(),
                                    buttonTitles: [R.string_ButtonLabel_Close()],
                                    handler: nil)
            return
        }
        
        ZPDialogView.showDialog(with: DialogTypeWarning,
                                message: R.string_PayBillSetAppId_ErrorMessage(),
                                cancelButtonTitle: R.string_ButtonLabel_Close(),
                                otherButtonTitle: nil,
                                completeHandle: nil)
    }
    
    private func handleAppInfo(_ app: ZPAppData?) {
        let navi: UINavigationController? = self.fromViewController?.navigationController
        ZaloPayWalletSDKPayment.sharedInstance().startHandleBill(self.bill, withDelegate: self, navigaionController: navi)
        let data = ZPAOrderData.getAppInfo(
            bill.appTransID,
            appid: "\(bill.appId)",
            transtype: Int32(bill.transType.rawValue),
            source: OrderSource(rawValue: OrderSource.RawValue(bill.ordersource)),
            result: .success,
            server_result: 1)
        ZPAppFactory.sharedInstance().orderTracking.startTrackOrder(data)
    }
    
    // MARK: Function
    func startProcessBill(_ bill: ZPBill, from fromViewController: UIViewController?) {
        if ZaloPayWalletSDKPayment.sharedInstance().checkIsCurrentPaying() {
            return
        }
        self.bill = bill
        self.fromViewController = fromViewController
        startGetAppInfo()
    }
    
    private func popToBankListVC() {
        //Back to select bank
        let controllers = navigationController?.viewControllers ?? [UIViewController]()
        
        for controller in controllers {
            if (controller is ZPListBankTableViewController) {
                self.navigationController?.popToViewController(controller, animated: false)
                break
            }
        }
        //[self reloadBill];
    }
    
    private func clearAllHandle() {
        completeHandle = nil
        errorHandle = nil
        cancelHandle = nil
        setAppIdErrorHandle = nil
    }
}

// MARK: ZaloPayWalletSDKCallBackDelegate
extension BillDetailViewController : ZaloPayWalletSDKCallBackDelegate {
    // MARK: SDK Notify Delegate
    public func paymentNotifyClose(withKey key: String, andData data: [AnyHashable: Any]) {
    }
    
    public func paymentNotifySuccess(_ data: [AnyHashable: Any]?) {
        if data != nil {
            self.updateBalance(withData: data!)
        }
        self.delegate?.paymentNotifySuccess?(data)
    }
    
    public func paymentNotifyFail(_ data: [AnyHashable: Any]?) {
        self.delegate?.paymentNotifyFail?(data)
    }
    
    public func paymentNotifyUnidentified() {
        self.delegate?.paymentNotifyUnidentified?()
    }
    
    // MARK: SDK Delegate
    public func paymentDidClose(withKey key: String, andData data: [AnyHashable: Any]?) {
        DDLogInfo("key = \(key)")
        DDLogInfo("data = \(data ?? [AnyHashable: Any]())")
        
//        if (![key isKindOfClass:[NSString class]]) {
//            key = @"";
//        }
//
//        if (![data isKindOfClass:[NSDictionary class]]) {
//            data = @{};
//        }
        switch key {
        case ZP_NOTIF_PAYMENT_GO_MAPCARD:
            internalMapCard()
        case ZP_NOTIF_PAYMENT_GO_LISTBANK_MAPCARD:
            internalListBankMapCard()
        case ZP_NOTIF_PAYMENT_GO_LINKACOUNT:
            let bankCode = data?.string(forKey: "bankCode") ?? ""
            internalMapAccount(bankCode: bankCode)
        case ZP_NOTIF_PAYMENT_GO_SELECTBANK:
            internalSelectBank()
        case ZP_NOTIF_PAYMENT_MY_WALLET:
            intenalRecharge()
        case ZP_NOTIF_UPGRADE_LEVEL_AND_GO_LINKACOUNT:
            let bankCode = data?.string(forKey: "bankCode") ?? ""
            updateProfileLevelAndLinkAccount(bankCode)
        case ZP_NOTIF_UPGRADE_LEVEL_3:
            updateProfileLevel3()
        case ZP_NOTIF_PAYMENT_IS_MAINTAINANCE:
            clearAllHandle()
        case ZP_NOTIF_PAYMENT_CANCEL_REGISTER_BANK:
            self.cancelHandle?(["popToBill": true])
            clearAllHandle()
        case ZP_NOTIF_VIEW_BANK_ACCOUNTS:
            self.showListCardViewController()
        default:
            if let handel = cancelHandle {
                handel(nil)
                clearAllHandle()
            }
        }
    }
    
    private func showListCardViewController() {
        guard let root  = self.navigationController?.viewControllers.first else {
            return
        }
        
        self.navigationController?.popToRootViewController(animated: false)
        ZPCenterRouter.launch(routerId: .linkCard, from: root)
    }
    
    public func paymentTrackingEvent(withData data: [AnyHashable: Any]?) {
    }
    
    public func paymentReponseSuccess(_ data: [AnyHashable: Any]?) {
        DDLogInfo("paymentReponseSuccess : \(data ?? [AnyHashable: Any]())")
        if let handle = completeHandle {
            handle(data)
            clearAllHandle()
        }
    }
    
    public func paymentReponseFail(_ data: [AnyHashable: Any]?) {
        //    @"errorcode" : @(response.errorCode),
        //    @"message" : response.message,
        if let handle = errorHandle {
            handle(data)
            clearAllHandle()
        }
        self.transactionHistoryManager.loadFailTransaction()
    }
    
    public func paymentReponseUnidentified() {
        paymentReponseFail(nil)
    }
    
    func paymentNeedToForceUpdateNewVersion() {
        LoginManagerSwift.sharedInstance.alertForceUpdateApp()
    }
    
    public func paymentNotifyShowFAQ() {
        let help: UIViewController? = ZPReactNativeAppRouter().supportCenterViewController()
        navigationController?.pushViewController(help ?? UIViewController(), animated: true)
    }
    
    public func paymentNotifyShowSupportPage(_ data: [AnyHashable: Any]?) {
        let category = data?.string(forKey: "paytype") ?? ""
        let transaction = data?.string(forKey: "zptransid") ?? ""
        let image = data?["capturescreen"] as? UIImage
        let errormessage = data?.string(forKey: "errormessage") ?? ""
        let errorCode = data?.int(forKey: "errorcode") ?? 0
        
        let model = ZPFeedbackModel()
        model.configDataSource(withCategory: category,
                               transactionId: transaction,
                               image: image,
                               message: errormessage,
                               errorCode: errorCode as NSNumber)
        if let controller = ZPFeedbackViewController(model: model) {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    // MARK: Alert
    private func alertLinkVietCombankAccount(from viewController: UIViewController, bankCode: String, bankName: String) {
        let message = ZPBankApiWrapperSwift.sharedInstance.getAttAlertLinkVietCombankAccount(from: bankCode, bankName: bankName)
        ZPDialogView.showDialog(with: DialogTypeNotification,
                                title: R.string_Dialog_Notification(),
                                attributeMessage: message,
                                buttonTitles: [R.string_ButtonLabel_OK(), R.string_ButtonLabel_Close()],
                                handler: { [weak self] (index) in
            if index == 0 {
                self?.internalMapAccount(bankCode: bankCode)
            }
        })
    }
    
    private func showAlertPreventCCSavedCard(whenPay error: String) {
        ZPDialogView.showDialog(with: DialogTypeNotification,
                                message: error,
                                cancelButtonTitle: R.string_ButtonLabel_Close(),
                                otherButtonTitle: nil,
                                completeHandle: nil)
    }
}
