//
//  ZPMainProfileMainProfileFooterModel.swift
//  ZaloPay
//
//  Created by nhatnt on 22/01/2018.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation

class ZPMainProfileMainProfileFooterModel: NSObject {
    // MARK: *** UI Elements
    var content: String!
    var height: CGFloat!
    var font: UIFont!
    var isNeedLabel: Bool!
    
    init(newContent: String) {
        super.init()
        self.font = UIFont.sfuiTextRegular(withSize: UILabel.subTitleSize())
        self.content = newContent
        self.isNeedLabel = newContent.count > 0
        self.content = newContent
        self.height = self.calculateSize(content: content)
    }
    
    func calculateSize(content: String) -> CGFloat{
        if content.isEmpty {
            return 0.1
        }

        let height = content.size(UIScreen.main.bounds.size.width - 20, using: self.font).height + 20
        return height
    }
}

