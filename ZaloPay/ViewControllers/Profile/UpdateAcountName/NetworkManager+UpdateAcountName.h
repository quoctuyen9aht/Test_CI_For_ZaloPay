//
//  NetworkManager+UpdateAcountName.h
//  ZaloPay
//
//  Created by bonnpv on 8/12/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

//#import "NetworkManager.h"

@interface NetworkManager (UpdateAcountName)
- (RACSignal *)checkZaloPayAcountName:(NSString *)acountName;
- (RACSignal *)updateZaloPayAcountName:(NSString *)acountName;
- (RACSignal *)getUserInfoWithAcountName:(NSString *)acountName;
@end
