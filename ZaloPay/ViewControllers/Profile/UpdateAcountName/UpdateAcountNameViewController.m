//
//  UpdateAcountNameViewController.m
//  ZaloPay
//
//  Created by bonnpv on 8/12/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "UpdateAcountNameViewController.h"
#import <ZaloPayCommon/ZPFloatTextInputView.h>
#import <ZaloPayUI/BaseViewController+Keyboard.h>
#import "NetworkManager+UpdateAcountName.h"
#import "UIView+GuideAccountName.h"
#import <ZaloPayNetwork/NSError+ZaloPayAPI.h>
#import <ZaloPayAnalyticsSwift/ZaloPayAnalyticsSwift-Swift.h>
#import ZALOPAY_MODULE_SWIFT

typedef enum {
    AcountNameStateNone,
    AcountNameStateInvalid,
    AcountNameStateValid,
} AcountNameState;

@interface UpdateAcountNameViewController()<UITextFieldDelegate>
@property (nonatomic, strong) UILabel *labelTitle;
@property (nonatomic, strong) UIView *textFieldBackground;
@property (nonatomic, strong) ZPFloatTextInputView *textFieldAccountName;
@property (nonatomic, strong) UIButton *nextButton;
@property (nonatomic) AcountNameState acountState;
@property (nonatomic, strong) UIButton *buttonValid;
@property (nonatomic, strong) NSCharacterSet * charSet;
@property (nonatomic, strong) UIView *guideAccountName;
@property (nonatomic, strong) UIScrollView *scrollView;
@end


@implementation UpdateAcountNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = [R string_UpdateAccountName_Title];
    [self addScrollView];
    
    [self setupView];
    [self addNextButton];
    [self.textFieldAccountName.textField becomeFirstResponder];
//    [[ZPAppFactory sharedInstance].eventTracker trackEvent:EventAction_UpdateZPN_Launch];
//    [[ZPAppFactory sharedInstance] trackScreen:[self screenName]];
//    [[ZPAppFactory sharedInstance] trackEvent:ZPAnalyticEventActionMe_profile_zpid_launch];
    self.textFieldAccountName.textField.delegate = self;
    [self.textFieldAccountName.textField addTarget:self action:@selector(textChanged:) forControlEvents:UIControlEventEditingChanged];
    @weakify(self);
    [[self.textFieldAccountName.textField rac_signalForControlEvents:UIControlEventEditingDidEndOnExit] subscribeNext:^(id x) {
        @strongify(self);
        [self.textFieldAccountName.textField resignFirstResponder];
    }];
}

//- (NSString *)screenName {
//    return ZPTrackerScreen.Screen_Me_Profile_ZPID;
//}

- (void)backButtonClicked:(id)sender{
    [super backButtonClicked:sender];
}

//- (ZPAnalyticEventAction)backEventId {
//    return ZPAnalyticEventActionMe_profile_zpid_touch_back;
//}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    float h = self.guideAccountName.frame.origin.y + self.guideAccountName.frame.size.height;
    float w = [UIScreen mainScreen].applicationFrame.size.width;
    self.scrollView.contentSize = CGSizeMake(w, h);
}

- (void)addScrollView {
    self.scrollView = [[UIScrollView alloc] init];
    [self.view addSubview:self.scrollView];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(0);
        make.right.equalTo(0);
        make.top.equalTo(0);
        make.height.equalTo([self scrollHeight]);
    }];
}

- (float)scrollHeight {
    if (IS_IPHONE_35_INCH) {
        return 130;
    }
    return 202;
}

- (float)buttonOffset {
    if (IS_IPHONE_35_INCH ||  IS_IPHONE_40_INCH) {
        return 10;
    }
    return 45;
}



- (void)setupView {
    self.labelTitle  = [[UILabel alloc] init];
    self.labelTitle.numberOfLines = 0;
    self.labelTitle.text = [R string_UpdateAccountName_Description];
    self.labelTitle.font = [UIFont SFUITextRegularWithSize:13];
    self.labelTitle.textColor = [UIColor subText];
    [self.scrollView addSubview:self.labelTitle];
    
    self.textFieldBackground = [[UIView alloc] init];
    self.textFieldBackground.backgroundColor = [UIColor whiteColor];
    [self.scrollView addSubview:self.textFieldBackground];
    
    self.textFieldAccountName = [[ZPFloatTextInputView alloc] init];
    [self.textFieldBackground addSubview:self.textFieldAccountName];
    self.textFieldAccountName.backgroundColor = [UIColor whiteColor];
    self.textFieldAccountName.textField.placeholder = [R string_UpdateAccountName_ZaloPayId];
    self.textFieldAccountName.textField.autocorrectionType = UITextAutocorrectionTypeNo;
    
    self.buttonValid = [UIButton buttonWithType:UIButtonTypeCustom];
    self.buttonValid.titleLabel.font = [UIFont iconFontWithSize:22];
    [self.buttonValid addTarget:self action:@selector(buttonValidClick) forControlEvents:UIControlEventTouchUpInside];
    [self.textFieldBackground addSubview:self.buttonValid];
    self.textFieldAccountName.textField.clearButtonMode = UITextFieldViewModeNever;
    
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = [UIColor lineColor];
    [self.scrollView addSubview:lineView];
    
    
    self.guideAccountName = [UIView guideAccountNameWithTextColor:[UIColor subText]
                                                             font:[UIFont SFUITextRegularWithSize:14]];
    [self.scrollView addSubview:self.guideAccountName];
    [self.labelTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(5);
        make.left.equalTo(5);
        make.width.equalTo(self.view.frame.size.width - 10);
        make.height.equalTo(40);
    }];
    
    [self.textFieldBackground mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.labelTitle.mas_bottom).with.offset(5);
        make.left.equalTo(0);
        make.width.equalTo(self.scrollView.mas_width);
        make.height.equalTo(60);
    }];
    
    [self.textFieldAccountName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.left.equalTo(0);
        make.right.equalTo(0);
    }];
    [self.buttonValid mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.bottom.equalTo(0);
        make.right.equalTo(-5);
        make.width.equalTo(40);
    }];
    
    [self.guideAccountName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(10);
        make.right.equalTo(10);
        make.top.equalTo(self.textFieldBackground.mas_bottom);
    }];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textFieldBackground.mas_top);
        make.left.equalTo(0);
        make.width.equalTo(self.scrollView.mas_width);
        make.height.equalTo(1);
    }];
}


- (void)addNextButton {
    self.nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.nextButton.enabled = false;
    [self.nextButton setupZaloPayButton];
    [self.nextButton setTitle:[R string_UpdateAccountName_Check] forState:UIControlStateNormal];
    [self.view addSubview:self.nextButton];
    [self.nextButton addTarget:self
                        action:@selector(nextButtonClick)
              forControlEvents:UIControlEventTouchUpInside];
    
    [self.nextButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@10);
        make.right.equalTo(@-10);
        make.height.equalTo(@(kZaloButtonHeight));
        make.top.equalTo(self.scrollView.mas_bottom).offset([self buttonOffset]);
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.textFieldAccountName resignFirstResponder];
}

#pragma mark - Action

- (void)setAcountState:(AcountNameState)acountState {
    if (_acountState == AcountNameStateValid  && acountState == AcountNameStateNone) {
//        [[ZPAppFactory sharedInstance].eventTracker trackEvent:EventAction_UpdateZPN_Edit_AfterCheck];
    }
    _acountState = acountState;
    switch (_acountState) {
        case AcountNameStateNone: {
            self.buttonValid.hidden = YES;
            [self.textFieldAccountName clearErrorText];
            [self.nextButton setTitle:[R string_UpdateAccountName_Check] forState:UIControlStateNormal];
        }
            break;
        case AcountNameStateInvalid: {
            self.buttonValid.hidden = NO;
            [self.buttonValid setIconFont:@"general_del" forState:UIControlStateNormal];
            [self.buttonValid setTitleColor:[UIColor hex_0xc7c7cc] forState:UIControlStateNormal];
            [self.nextButton setTitle:[R string_UpdateAccountName_Check] forState:UIControlStateNormal];
        }
            break;
        case AcountNameStateValid: {
            [self.textFieldAccountName clearErrorText];
            self.buttonValid.hidden = NO;
            [self.buttonValid setIconFont:@"general_success" forState:UIControlStateNormal];
            [self.buttonValid setTitleColor:[UIColor zaloBaseColor] forState:UIControlStateNormal];
            [self.nextButton setTitle:[R string_UpdateAccountName_Register] forState:UIControlStateNormal];
        }
            break;
        default:
            break;
    }
}

- (void)buttonValidClick {
    if (self.acountState == AcountNameStateInvalid) {
        self.textFieldAccountName.textField.text = @"";
        self.acountState = AcountNameStateNone;
//        [[ZPAppFactory sharedInstance].eventTracker trackEvent:EventAction_UpdateZPN_Edit_Delete];
    }
}

- (void)nextButtonClick {
    NSString *acountName = [self.textFieldAccountName.textField.text lowercaseString];
    if (![self validInputData:acountName]) {
        self.acountState = AcountNameStateInvalid;
//        [[ZPAppFactory sharedInstance].eventTracker trackEvent:EventAction_UpdateZPN_Edit_Invalid];
        return;
    }
    
    if (self.acountState == AcountNameStateNone) {
        [self requestCheckAccountName:acountName];
        return;
    }
    
    if (self.acountState == AcountNameStateValid) {
        [self alertUpdateAcountName:acountName];
        return;
    }
}

- (void)alertUpdateAcountName:(NSString *)acountName {
    @weakify(self);
    [ZPDialogView showDialogWithType:DialogTypeNotification
                             message:[R string_Account_Name_Warning_Message]
                   cancelButtonTitle:[R string_ButtonLabel_Cancel]
                    otherButtonTitle:@[[R string_ButtonLabel_OK]]
                      completeHandle:^(int buttonIndex, int cancelButtonIndex) {
                          if (buttonIndex != cancelButtonIndex) {
                              @strongify(self);
                              [self requestUpdateAcountName:acountName];
                          }
                      }];
}

- (BOOL)validInputData:(NSString *)acountName {
    if (acountName.length < minAccountNameLength || acountName.length > maxAccountNameLength) {
        [self.textFieldAccountName showErrorWithText:[R string_Account_Name_Length_Message]];
        return FALSE;
    }
    
    if ([acountName rangeOfString:@" "].length > 0) {
        [self.textFieldAccountName showErrorWithText:[R string_Account_Name_Space_Message]];
        return FALSE;
    }
    
    if ([acountName rangeOfCharacterFromSet:self.charSet].location != NSNotFound) {
        [self.textFieldAccountName showErrorWithText:[R string_Account_Name_Special_Char_Message]];
        return FALSE;
    }
    return TRUE;
}

- (NSCharacterSet *)charSet {
    if (!_charSet) {
        _charSet = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"] invertedSet];
    }
    return _charSet;
}

- (void)requestCheckAccountName:(NSString *)acountName {
//    [[ZPAppFactory sharedInstance].eventTracker trackEvent:EventAction_UpdateZPN_PressCheck];
    
    [[ZPAppFactory sharedInstance] showHUDAddedTo:self.view];
    @weakify(self);
    [[[[NetworkManager sharedInstance] checkZaloPayAcountName:acountName] deliverOnMainThread] subscribeNext:^(id x) {
        @strongify(self);
        [[ZPAppFactory sharedInstance] hideAllHUDsForView:self.view];
        self.acountState = AcountNameStateValid;
//        [[ZPAppFactory sharedInstance].eventTracker trackEvent:EventAction_UpdateZPN_Valid];
    } error:^(NSError *error) {
        @strongify(self);
        if ([error isNetworkConnectionError]) {
            self.acountState = AcountNameStateNone;
        }else {
            self.acountState = AcountNameStateInvalid;
//            [[ZPAppFactory sharedInstance].eventTracker trackEvent:EventAction_UpdateZPN_InUsed];
        }
        [self.textFieldAccountName showErrorWithText:[error apiErrorMessage]];
        [[ZPAppFactory sharedInstance] hideAllHUDsForView:self.view];
    }];
}

- (void)requestUpdateAcountName:(NSString *)acountName {
    [[ZPAppFactory sharedInstance] showHUDAddedTo:self.view];
    @weakify(self);
    [[[[NetworkManager sharedInstance] updateZaloPayAcountName:acountName] deliverOnMainThread] subscribeNext:^(NSDictionary *result) {
        @strongify(self);
        [[ZPAppFactory sharedInstance] hideAllHUDsForView:self.view];
        [self alertSuccess];
        
        [[LoginManagerSwift sharedInstance] updateAccountNameToLoginDataWithAccountName:acountName];
        
        [self.navigationController popViewControllerAnimated:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:Reload_Profile_Notification object:nil];
        [self.delegate updateAccountNameCompleteSuccess:self];
    } error:^(NSError *error) {
        @strongify(self);
        [self.textFieldAccountName showErrorWithText:[error apiErrorMessage]];
        [[ZPAppFactory sharedInstance] hideAllHUDsForView:self.view];
        
//        if (![error isNetworkConnectionError]) {
//            [[ZPAppFactory sharedInstance].eventTracker trackEvent:EventAction_UpdateZPN_InUsed2];
//        }
    }];
}

- (void)alertSuccess {
    [ZPDialogView showDialogWithType:DialogTypeNotification
                             message:[R string_UpdateAccountName_Success_Message]
                   cancelButtonTitle:[R string_ButtonLabel_Close]
                    otherButtonTitle:nil
                      completeHandle:nil];
}

#pragma mark - Text Field Delegate

- (BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    
    NSInteger length = textField.text.length + string.length;
    if (length > maxAccountNameLength) {
        [self.textFieldAccountName showErrorWithText:[R string_Account_Name_Length_Message]];
        return FALSE;
    }
    return TRUE;
}

- (void)textChanged:(UITextField *)textField {
    NSString *text = self.textFieldAccountName.textField.text;
    self.nextButton.enabled = text.length >= minAccountNameLength;
    self.textFieldAccountName.textField.text = [text lowercaseString];
    [self.textFieldAccountName clearErrorText];
    
    if (self.acountState != AcountNameStateNone) {
        self.acountState = AcountNameStateNone;
    }
    
}


@end
