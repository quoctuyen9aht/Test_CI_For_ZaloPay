//
//  UpdateAcountNameViewController.h
//  ZaloPay
//
//  Created by bonnpv on 8/12/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

@class UpdateAcountNameViewController;
@protocol UpdateAcountNameViewControllerDelegate <NSObject>
- (void)updateAccountNameCompleteSuccess:(UpdateAcountNameViewController *)controller;
@end

@interface UpdateAcountNameViewController : BaseViewController
@property (nonatomic, weak) id <UpdateAcountNameViewControllerDelegate> delegate;
@end
