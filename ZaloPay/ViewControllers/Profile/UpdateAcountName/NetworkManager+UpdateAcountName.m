//
//  NetworkManager+UpdateAcountName.m
//  ZaloPay
//
//  Created by bonnpv on 8/12/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "NetworkManager+UpdateAcountName.h"
#import <ZaloPayNetwork/ZPNetWorkApi.h>
@implementation NetworkManager (UpdateAcountName)
- (RACSignal *)checkZaloPayAcountName:(NSString *)acountName {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:acountName forKey:@"zalopayname"];
    return [self requestWithPath:api_um_checkzalopaynameexist parameters:params];
}

- (RACSignal *)updateZaloPayAcountName:(NSString *)acountName {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:acountName forKey:@"zalopayname"];
    return [self postRequestWithPath:api_um_updatezalopayname parameters:params];
}

- (RACSignal *)getUserInfoWithAcountName:(NSString *)acountName {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:acountName forKey:@"zalopayname"];
    return [self requestWithPath:api_um_getuserinfobyzalopayname parameters:params];
}
@end
