//
//  AccountInfoViewController.swift
//  ZaloPay
//
//  Created by ThanhVo on 6/21/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayProfile
import RxSwift
import ZaloPayNetwork
import ZaloPayCommonSwift

enum ProfileVerificationStatus: Int {
    case undefined = -1, notVerified, processing, accepted, denied
    
    var status: String {
        switch self {
        case .undefined:
            return ""
        case .notVerified, .denied:
            return R.string_MainProfile_AccountType_NotVerified()
        case .processing:
            return R.string_MainProfile_AccountType_Wating()
        case .accepted:
            return R.string_MainProfile_AccountType_Wating()
        }
    }
    
    var color: UIColor {
        switch self {
        case .undefined:
            return .clear
        case .notVerified, .denied:
            return .deepOrange()
        case .processing:
            return .yellowOchre()
        case .accepted:
            return .yellowOchre()
        }
    }
}

// This screen is shown when user profile's level is not 3
// Note: code Borrowed from ZPMainProfileViewController
class AccountInfoViewController: BaseViewController {
    let disposeBag = DisposeBag()
    var tableView: UITableView!
    var dataSource: [[AnyObject?]] = []    
    lazy var updateLevel3Model = ZPUploadProfileLevel3Helper()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = R.string_MainProfile_AccountInfoTitle()
        self.createTableView()
        self.reloadDataSource()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkUpdateProfileStatus()
    }
    
    func createTableView() {
        self.tableView = UITableView.init(frame: CGRect.zero, style: UITableViewStyle.grouped)
        self.view.addSubview(self.tableView)
        self.tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.backgroundColor = UIColor.defaultBackground()
        self.tableView.separatorColor = UIColor.line()
        let cellNib = UINib(nibName: "ZPProfileUserInfoCell", bundle: nil)
        self.tableView.register(cellNib, forCellReuseIdentifier: "ZPProfileUserInfoCell")
    }
    
    
    // Get status update-profile-level3
    func requestUpdateProfileStatus() -> RACSignal<AnyObject> {
        return NetworkManager.sharedInstance().checkStatusUpdateProfileLevel3()
    }
    
    func checkUpdateProfileStatus() {
        let signal: Observable<AnyObject?> = ~self.requestUpdateProfileStatus()
        signal.map({ response -> ProfileVerificationStatus in
            guard let response = response as? JSON else {
                return .undefined
            }
            let rawValue = response.value(forKey: "status", defaultValue: 0)
            return ProfileVerificationStatus.init(rawValue: rawValue) ?? .undefined
        }).subscribe(onNext: { (status) in
            // Locate the status model in datasource
            let statusModel = self.dataSource.flatMap { $0 }.compactMap({ $0 as? ZPUserItemModel }).filter { (model) -> Bool in
                return model.type == ZPUserItemModelTypeVerificationStatus
            }
            // Modify the status model
            if let model = statusModel.first {
                model.value = status.status
                model.color = status.color
                model.doneUploadData = (status == ProfileVerificationStatus.processing)
                DispatchQueue.executeInMainThread {
                    self.tableView.reloadData()
                }
            }
        }, onError: { (e) in
            // Error handling
        }).disposed(by: disposeBag)
    }
    
    
    func reloadDataSource() {
        let user = ZPProfileManager.shareInstance.currentZaloUser
        self.dataSource = self.dataSourceProfile(user: user)
        self.tableView.reloadData()
    }
    
    func dataSourceProfile(user: ZaloUserSwift?) -> [[AnyObject?]] {
        var result: [[AnyObject?]] = []
        // Top model (avatar + name + phone)
        let pin = ZPPinModel.init()
        pin.title = R.string_Settings_Protect_Account()
        
        // Account status now get from API request, just init an empty place for it here
        let accountStatusModel = ZPUserItemModel.item(withTitle: R.string_MainProfile_AccountType(), value: " ")
        accountStatusModel?.type = ZPUserItemModelTypeVerificationStatus
        accountStatusModel?.hasValue = false
        result.append([pin, accountStatusModel])
        
        // Dia chi
        let locationModel = ZPUserItemModel.item(withTitle: R.string_MainProfile_Update_Address_Title(),value: ZPProfileManager.shareInstance.address)
        locationModel?.type = ZPUserItemModelTypeLocation
        result.append([locationModel])
        
        // Zalopay ID
        let accountNameStr = ZPProfileManager.shareInstance.userLoginData?.accountName ?? ""
        if !accountNameStr.isEmpty {
            let accountModel = ZPUserItemModel.item(withTitle: R.string_MainProfile_ZaloPayId(), value: accountNameStr)
            accountModel?.type = ZPUserItemModelTypeAcountName
            result.append([accountModel])
        }
        
        // Ma dinh danh
        let zaloPayId = ZPProfileManager.shareInstance.userLoginData?.paymentUserId ?? ""
        let zaloPayIDModel = ZPUserItemModel.item(withTitle: R.string_MainProfile_Id(),value: zaloPayId)
        result.append([zaloPayIDModel])
        
        return result
    }
}

extension AccountInfoViewController: UITableViewDataSource {
    func itemAtIndexPath(indexPath: IndexPath) -> AnyObject? {
        let array = self.dataSource[indexPath.section]
        return array[indexPath.row]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let array = self.dataSource[section]
        return array.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath == IndexPath(item: 0, section: 0) {
            return 80
        }
        return CGFloat(ZPUserInfomationTableViewCell.cellHeight())
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 0.1 : 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Get cell item
        guard let item = self.itemAtIndexPath(indexPath: indexPath) else {
            return UITableViewCell()
        }
        // User detail info
        if let userModel = item as? ZPUserItemModel {
            let cell = ZPUserInfomationTableViewCell.zp_cellFromSameNib(for: tableView)
            cell.setItem(userModel)
            cell.selectionStyle = userModel.hasValue == true ? UITableViewCellSelectionStyle.none : UITableViewCellSelectionStyle.gray
            return cell
        }
        // User avatar + name + phone block
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ZPProfileUserInfoCell", for: indexPath) as? ZPProfileUserInfoCell {
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.arrow.isHidden = true
            
            let currentUser = ZPProfileManager.shareInstance.currentZaloUser
            let name = currentUser?.displayName ?? ""
            let path = currentUser?.avatar ?? ""
            
            cell.lblUserName.text = name
            let url = URL.init(string: path)
            cell.avatarImgView.sd_setImage(with: url, placeholderImage: UIImage.defaultAvatar())
            
            var acccountPhone = ZPProfileManager.shareInstance.userLoginData?.phoneNumber.formatPhoneNumber() ?? ""
            acccountPhone = acccountPhone.count > 0 ? acccountPhone : R.string_LeftMenu_ZaloPayIdEmpty()
            cell.lblUserZaloId.text = acccountPhone
            return cell
        }
        fatalError("Error create ZPProfileUserInfoCell")
    }
}

extension AccountInfoViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let item = self.itemAtIndexPath(indexPath: indexPath) as? ZPUserItemModel else {
            return
        }
        
        if item.type == ZPUserItemModelTypeLocation {
            updateAddress()
            return
        }
        
        if item.type == ZPUserItemModelTypeVerificationStatus {
            item.doneUploadData ? alertDoneUpdate() : updateLevel3()
        }
    }
    
    func updateAddress() {
        ZPProfileUpdateLocationViewController.show(on: self).subscribe(onNext: {[weak self] (x) in
            ZPProfileManager.shareInstance.address = x
            self?.reloadDataSource()
        }).disposed(by: disposeBag)
    }
    
    func alertDoneUpdate() {
        if let dialog = ZPDialogView(type: DialogTypeSuccess, message: R.string_UpdateProfileLevel3_UploadSuccess_Message(), items: nil) {
            dialog.show()
        }
    }
    
    func updateLevel3 () {        
        updateLevel3Model.uploadLevel(from: self, cancel: { [weak self] in
            self?.popMe()
        }, error: {
            
        }, complete: {
            [weak self] in
            self?.popMe()
        })
    }
}
