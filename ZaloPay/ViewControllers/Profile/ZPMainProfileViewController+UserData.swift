//
//  ZPMainProfileViewController+UserData.swift
//  ZaloPay
//
//  Created by nhatnt on 23/01/2018.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import SDWebImage
import CocoaLumberjackSwift
import ZaloPayProfile
import ZaloPayConfig

extension ZPMainProfileViewController {
    
    func reloadDataSource() {
        let user = ZPProfileManager.shareInstance.currentZaloUser
        self.dataSource = self.dataSourceProfile(user: user)
//        self.createFooters()
        self.tableView.reloadData()
    }
    
    func fetchUserData() {
        ZPAppFactory.sharedInstance().showHUDAdded(to: self.view)
        
        self.requestProfileInfo().subscribeNext({ (value) in
            DDLogInfo("Response \(String(describing: value))!!!!!!!!!!!!!!")
            guard let jsonvalue = value as? JSON else {
                return
            }
            ZPProfileManager.shareInstance.address = jsonvalue.string(forKey: "permanentaddress")
        }, error: { [weak self] (error) in
            self?.requestUserProfile()
        }) { [weak self] in
            self?.requestUserProfile()
        }
    }
    
    func requestUserProfile() {
        ZPProfileManager.shareInstance.loadZaloUserProfile().deliverOnMainThread().subscribeError({ [weak self] (error) in
            ZPAppFactory.sharedInstance().hideAllHUDs(for: self?.view)
            self?.reloadDataSource()
        }, completed: {  [weak self] in
            ZPAppFactory.sharedInstance().hideAllHUDs(for: self?.view)
            self?.reloadDataSource()
        })
    }
    
    func requestProfileInfo() -> RACSignal<AnyObject>{
        return ZPWalletManagerSwift.sharedInstance.updateUserInfo()
    }
    
    // Not used function
//    func shouldLoadProfileFromServer() -> Bool {
//        guard let zaloPayId = ZPProfileManager.shareInstance.userLoginData?.paymentUserId else {
//            return true
//        }
//        let key = String(format: "last_time_load_profile_key_%@", zaloPayId)
//        guard let lastTime = UserDefaults.standard.object(forKey: key) as? Double else {
//            return true
//        }
//
//        let currentTime = Date.init().timeIntervalSince1970
//        let timeCheck : Double = 24 * 60 * 60 * 60 //24hours
//
//        if currentTime - lastTime > timeCheck {
//            UserDefaults.standard.set(currentTime, forKey: key)
//            UserDefaults.standard.synchronize()
//            return true
//        }
//
//        let level = ZPProfileManager.shareInstance.userLoginData?.profilelevel
//
//        if level == ZPProfileLevel.level2.hashValue {
//            let phone = ZPProfileManager.shareInstance.userLoginData?.phoneNumber ?? ""
//            return phone.count == 0
//        }
//        if level == ZPProfileLevel.level3.hashValue {
//            let email = ZPProfileManager.shareInstance.email ?? ""
//            return email.count == 0
//        }
//        return false
//    }
    
    func dataSourceProfile(user: ZaloUserSwift?) -> [[AnyObject?]] {
        let email = ZPProfileManager.shareInstance.email ?? ""
        // 3 type of identity
        let userIdentifier = ZPProfileManager.shareInstance.identifier  ?? ""
        let userPassport = ZPProfileManager.shareInstance.passport  ?? ""
        let userCC = ZPProfileManager.shareInstance.cc  ?? ""
        
        let paymentUserId = ZPProfileManager.shareInstance.userLoginData?.paymentUserId ?? ""
        let zaloPayId = ZPProfileManager.shareInstance.userLoginData?.accountName ?? ""
        var currentUserName = ZPProfileManager.shareInstance.currentZaloUser?.displayName ?? ""
        
        // kyc info (if exists)
        let kycFullName = ZPProfileManager.shareInstance.fullName
        let kycGender = ZPProfileManager.shareInstance.gender
        let kycDayOfBirth = ZPProfileManager.shareInstance.dob
        
        var result: [[AnyObject?]] = []
        
//        var isDoneUpload = false
//        let cacheDataTable = ZPCacheDataTable(db: PerUserDataBase.sharedInstance())
//        if let doneUpload = (cacheDataTable?.cacheDataValue(forKey: kDisableUpdateProfileLevel3))?.toBool() {
//            isDoneUpload = doneUpload
//        }
        // Identifier
//        var indetifierModel = ZPUserItemModel.item(withTitle: R.string_MainProfile_CMND(), value: R.string_MainProfile_Waiting())
//        if isDoneUpload && userIdentifier.count == 0 {
//            indetifierModel?.color = UIColor.placeHolder()
//            indetifierModel?.hasValue = false
//            indetifierModel?.doneUploadData = true
//        } else {
//            indetifierModel = ZPUserItemModel.item(withTitle: R.string_MainProfile_CMND(), value: userIdentifier)
//        }
//        indetifierModel?.type = ZPUserItemModelTypeIdentifier
        
//        let phoneModel = ZPUserItemModel.item(withTitle: R.string_MainProfile_Phone(), value: zaloPayPhoneNumber)
//        phoneModel?.type = ZPUserItemModelTypePhone
       
        // Real name
        if let kycFullName = kycFullName {
            currentUserName = kycFullName
        }
        let realNameModel = ZPUserItemModel.item(withTitle: R.string_MainProfile_RealName(), value: currentUserName)
        
        // Gender
        var genderString = user?.gender == .male ? R.string_MainProfile_Male() : R.string_MainProfile_Female()
        if let kycGender = kycGender {
            genderString = kycGender == .male ? R.string_MainProfile_Male() : R.string_MainProfile_Female()
        }
        let genderModel = ZPUserItemModel.item(withTitle: R.string_MainProfile_Gender(),
                                               value: genderString)
        
        // Day of birth
        var birthDayString = ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        if let birthDay = user?.birthDay {
            birthDayString = dateFormatter.string(from: birthDay)
        }
        if let kycDayOfBirth = kycDayOfBirth {
            birthDayString = dateFormatter.string(from: kycDayOfBirth)
        }
        let dobModel = ZPUserItemModel.item(withTitle: R.string_MainProfile_BirthDay(), value: birthDayString)
        var infos = [realNameModel, genderModel, dobModel]
        
        // 3 type of identity, only show if value not empty
        if !userIdentifier.isEmpty {
            let indetifierModel = ZPUserItemModel.item(withTitle: "CMND", value: userIdentifier)
            infos.append(indetifierModel)
        }
        if !userPassport.isEmpty {
            let passportModel = ZPUserItemModel.item(withTitle: "Hộ chiếu", value: userPassport)
            infos.append(passportModel)
        }
        if !userCC.isEmpty {
            let ccModel = ZPUserItemModel.item(withTitle: "CC", value: userCC)
            infos.append(ccModel)
        }
        result.append(infos)
        
        //Location
        let locationModel = ZPUserItemModel.item(withTitle: R.string_MainProfile_Update_Address_Title(),value: ZPProfileManager.shareInstance.address)
        locationModel?.type = ZPUserItemModelTypeLocation
        var moreInfos = [locationModel]
        
        // Email
        if !email.isEmpty {
            let emailModel = ZPUserItemModel.item(withTitle: R.string_MainProfile_Email(), value: email)
            moreInfos.insert(emailModel, at: 0)
        }
        result.append(moreInfos)
        
        //check remove zalopayID by config JSON
        let enable_register_profile_zalopayid = ZPApplicationConfig.getTabMeConfig()?.getEnableRegisterProfileZalopayId()
        if enable_register_profile_zalopayid == 1 || zaloPayId.count > 0 {
            // ZaloPay Id
            let zaloPayIdModel = ZPUserItemModel.item(withTitle: R.string_MainProfile_ZaloPayId(), value: zaloPayId)
            zaloPayIdModel?.type = ZPUserItemModelTypeAcountName
            result.append([zaloPayIdModel])
        }
        
//        var phones = [phoneModel]
        

        
//        var emailModel = ZPUserItemModel.item(withTitle: R.string_MainProfile_Email(), value: R.string_MainProfile_Waiting())
//        emailModel?.type = ZPUserItemModelTypeEmail
//        if isDoneUpload && email.count == 0 {
//            emailModel?.color = UIColor.placeHolder()
//            emailModel?.hasValue = false
//            emailModel?.doneUploadData = true
//        } else {
//            emailModel = ZPUserItemModel.item(withTitle: R.string_MainProfile_Email(), value: email)
//            moreInfos.insert(emailModel, at: 0)
//        }
        
        // Ma dinh danh
        let paymentUserIdModel = ZPUserItemModel.item(withTitle: R.string_MainProfile_Id(), value: paymentUserId)
        result.append([paymentUserIdModel])
        
        // Top block: avatar + name + phone
        let pin = ZPPinModel.init()
        pin.title = R.string_Settings_Protect_Account()
        
        // Account status
        let accountStatusModel = ZPUserItemModel.item(withTitle: R.string_MainProfile_AccountType(), value: R.string_MainProfile_AccountType_Verified())
        accountStatusModel?.color = UIColor.zaloBase()

        result.insert([pin, accountStatusModel], at: 0)
        return result
    }
    
    private func checkHasValue(_ data: [ZPUserItemModel]?) -> Bool {
        guard let items = data else {
            return false
        }
        
        var isHasValue = true
        for model in items {
            isHasValue = isHasValue && model.hasValue
        }
        return isHasValue
    }
    
//    func createFooters() {
//        var footers = Array<ZPMainProfileMainProfileFooterModel>.init()
//        for (index, items) in self.dataSource.enumerated() {
//            var content: String = ""
//            switch index {
//            case 1:
//                content = R.string_MainProfile_Content_BirthDay_Gender()
//                break
//            case 2:
//                if self.checkHasValue(items.compactMap({ $0 as? ZPUserItemModel})) {
//                    break
//                }
//                content = R.string_MainProfile_Content_Phone_ID()
//                break
//            case 3:
//                if self.checkHasValue(items.compactMap({ $0 as? ZPUserItemModel})) {
//                    break
//                }
//                content = R.string_MainProfile_Content_Email_CMND()
//                break
//            default:
//                break
//            }
//            let footer = ZPMainProfileMainProfileFooterModel.init(newContent: content)
//            footers.append(footer)
//        }
//        self.footers = footers
//    }
}
