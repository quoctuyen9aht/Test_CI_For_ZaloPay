//
//  NetworkManager+Profile.m
//  ZaloPay
//
//  Created by PhucPv on 6/3/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "NetworkManager+Profile.h"
#import <ZaloPayCommon/NSString+sha256.h>
#import <AFNetworking/AFNetworking.h>
//#import "ZaloSDKApiWrapper.h"
#import <ZaloPayNetwork/ZPNetWorkApi.h>
#import <ZaloPayWalletSDK/Extensions/UIDevice+ZPExtension.h>
#import <ZaloPayProfile/ZaloPayProfile-Swift.h>
#import ZALOPAY_MODULE_SWIFT

@implementation NetworkManager(Profile)

- (RACSignal *)lockUserForClient {
    return [self requestWithPath:api_um_lockuserforclient parameters:nil];
}

- (RACSignal *)loginWithPhone:(NSString *)phoneNumber
                          pin:(NSString *)pin {
    //frontendid=4
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    NSString *devicemodel = [[UIDevice currentDevice] zpDeviceModel];
    NSString * deviceId = [UIDevice zmDeviceUUID];
    [param setObjectCheckNil:phoneNumber forKey:@"phonenumber"];
    [param setObjectCheckNil:[pin sha256] forKey:@"pin"];
    [param setObjectCheckNil:deviceId forKey:@"deviceid"];
    [param setObjectCheckNil:devicemodel forKey:@"devicemodel"];
    [param setObjectCheckNil:@"4" forKey:@"frontendid"];
    return [self requestWithPath:api_um_loginbyphone parameters:param];
}
- (RACSignal *)registerWithPhone:(NSString *)phoneNumber
                             pin:(NSString *)pin {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObjectCheckNil:phoneNumber forKey:@"phonenumber"];
    [param setObjectCheckNil:[pin sha256] forKey:@"pin"];
    [param setObjectCheckNil:[[ZaloSDKApiWrapper sharedInstance] zaloUserId] forKey:@"zaloid"];
    [param setObjectCheckNil:[[ZaloSDKApiWrapper sharedInstance] zaloOauthcode] forKey:@"oauthcode"];
    return [self postRequestWithPath:api_um_registerphonenumber parameters:param];
}


- (RACSignal *)authenPhoneWithOTP:(NSString *)otp {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObjectCheckNil:otp forKey:@"otp"];
    [param setObjectCheckNil:[[ZaloSDKApiWrapper sharedInstance] zaloUserId] forKey:@"zaloid"];
    [param setObjectCheckNil:[[ZaloSDKApiWrapper sharedInstance] zaloOauthcode] forKey:@"oauthcode"];
    return [self postRequestWithPath:api_um_authenphonenumber parameters:param];
}

- (RACSignal *)authenPhoneV2WithOTP:(NSString *)otp {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObjectCheckNil:otp forKey:@"otp"];
    [param setObjectCheckNil:[[ZaloSDKApiWrapper sharedInstance] zaloUserId] forKey:@"zaloid"];
    [param setObjectCheckNil:[[ZaloSDKApiWrapper sharedInstance] zaloOauthcode] forKey:@"oauthcode"];
    return [self postRequestWithPath:api_um_authenphonenumberv2 parameters:param];
}

- (RACSignal *)authenLoginByPhone:(NSString *)phoneNumber andOTP:(NSString *)otp {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObjectCheckNil:otp forKey:@"otp"];
    [param setObjectCheckNil:phoneNumber forKey:@"phonenumber"];
    return [self postRequestWithPath:api_um_authenloginbyphone parameters:param];
}

- (RACSignal *)updateProfileWithPhoneNumber:(NSString *)phoneNumber
                                        pin:(NSString *)pin {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObjectCheckNil:phoneNumber forKey:@"phonenumber"];
    [param setObjectCheckNil:[pin sha256] forKey:@"pin"];
    return [self postRequestWithPath:api_um_updateprofile parameters:param];
}
- (RACSignal *)updateProfile2WithPhoneNumber:(NSString *)phoneNumber
                                         pin:(NSString *)pin
                                 accountName:(NSString *)accountName {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObjectCheckNil:phoneNumber forKey:@"phonenumber"];
    [param setObjectCheckNil:[pin sha256] forKey:@"pin"];
    [param setObjectCheckNil:accountName forKey:@"zalopayname"];
    return [self postRequestWithPath:api_um_updateprofile parameters:param];
}

- (RACSignal *)verifyProfileWithOtp:(NSString *)otp {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObjectCheckNil:otp forKey:@"otp"];
    return [self postRequestWithPath:api_um_verifyotpprofile parameters:param];
}

- (RACSignal *)recoveryPinWithNewPin:(NSString *)pin oldPin:(NSString *)oldPin {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObjectCheckNil:[pin sha256] forKey:@"pin"];
    [param setObjectCheckNil:[oldPin sha256] forKey:@"oldpin"];
    return [self requestWithPath:api_um_recoverypin parameters:param];
}
- (RACSignal *)recoveryPinWithOtp:(NSString *)otp {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObjectCheckNil:otp forKey:@"otp"];
    return [self postRequestWithPath:api_um_recoverypin parameters:param];
}

- (RACSignal *)requestUpdateProfileWithEmail:(NSString *)email
                                  identifier:(NSString *)identifier
                        frontIdentifierPhoto:(UIImage *)frontPhoto
                      backsideIdentiferPhoto:(UIImage *)backsidePhoto
                                      avatar:(UIImage *)avatar
                                imageQuality:(float)quality {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:identifier forKey:@"identitynumber"];
    [params setObjectCheckNil:email forKey:@"email"];
    return [self uploadWithPath:api_umupload_preupdateprofilelevel3
                          param:params
                      formBlock:^(id<AFMultipartFormData> formData) {
                          
        NSData *fimg = UIImageJPEGRepresentation(frontPhoto, quality);
        NSData *bimg =  UIImageJPEGRepresentation(backsidePhoto, quality);
        NSData *avataimg = UIImageJPEGRepresentation(avatar, quality);
                          
        [formData appendPartWithFileData:fimg name:@"fimg" fileName:@"fimg.png" mimeType:@"image/jpeg"];
        [formData appendPartWithFileData:bimg name:@"bimg" fileName:@"bimg.png" mimeType:@"image/jpeg"];
        [formData appendPartWithFileData:avataimg name:@"avataimg" fileName:@"avataimg.png" mimeType:@"image/jpeg"];
                      }];
}

// Request update all field - KYC verify via photo
- (RACSignal *)requestUpdateProfileWithFullName:(NSString *)fullName
                                         gender:(NSInteger)gender
                                     dayOfBirth:(NSDate *)dob
                                 identifierType:(NSInteger)idType
                                identifierValue:(NSString *)idValue
                           frontIdentifierPhoto:(UIImage *)frontPhoto
                         backsideIdentiferPhoto:(UIImage *)backsidePhoto
                                         avatar:(UIImage *)avatar
                                   imageQuality:(float)quality {
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSNumber *dobEpoch = [[NSNumber alloc] initWithDouble:dob.timeIntervalSince1970];
    [params setObjectCheckNil:fullName forKey:@"name"];
    [params setValue:@(gender) forKey:@"gender"];
    [params setObjectCheckNil:dobEpoch forKey:@"dob"];
    [params setValue:@(idType) forKey:@"identitytype"];
    [params setObjectCheckNil:idValue forKey:@"identitynumber"];
    
    
    return [self uploadWithPath:api_umupload_preupdateprofilelevel3
                          param:params
                      formBlock:^(id<AFMultipartFormData> formData) {
                          
                          NSData *fimg = UIImageJPEGRepresentation(frontPhoto, quality);
                          NSData *bimg =  UIImageJPEGRepresentation(backsidePhoto, quality);
                          NSData *avataimg = UIImageJPEGRepresentation(avatar, quality);
                          
                          [formData appendPartWithFileData:fimg name:@"fimg" fileName:@"fimg.png" mimeType:@"image/jpeg"];
                          [formData appendPartWithFileData:bimg name:@"bimg" fileName:@"bimg.png" mimeType:@"image/jpeg"];
                          [formData appendPartWithFileData:avataimg name:@"avataimg" fileName:@"avataimg.png" mimeType:@"image/jpeg"];
                      }];
}

- (RACSignal *)checkStatusUpdateProfileLevel3 {
    return [self requestWithPath:api_umupload_statusupdateproflvl3 parameters:@{@"systemlogin": @(1)}];
}


- (RACSignal *)getUserInfoByZaloPayId:(int64_t)zaloPayId {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:@(zaloPayId) forKey:@"requestid"];
    return [self requestWithPath:api_um_getuserinfobyzalopayid parameters:params];
}

- (RACSignal *)getUserInfoByPhone:(NSString *)phoneNumber {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:phoneNumber forKey:@"phonenumber"];
    return [self requestWithPath:api_um_getuserinfobyphone parameters:params];
}

- (RACSignal *)getCurrenUserProfile {
    return [self requestWithPath:api_um_getuserprofilesimpleinfo parameters:@{@"systemlogin": @(1)}];
}

- (RACSignal *)registerUserInfo:(NSString *)zalophone
                         gender:(NSNumber *)gender
                       fullName:(NSString *)fullName
                            dob:(NSDate *)dob
                         idType:(NSNumber *)idType
                         idValue:(NSString *)idValue {
    NSNumber *dobEpoch = [[NSNumber alloc] initWithDouble:dob.timeIntervalSince1970];
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObjectCheckNil:zalophone forKey:@"zalophone"];
    [param setObjectCheckNil:gender forKey:@"gender"];
    [param setObjectCheckNil:fullName forKey:@"fullname"];
    [param setObjectCheckNil:dobEpoch forKey:@"birthday"];
    [param setObjectCheckNil:idType forKey:@"idtype"];
    [param setObjectCheckNil:idValue forKey:@"idvalue"];
    [param setObjectCheckNil:[[ZaloSDKApiWrapper sharedInstance] zaloUserId] forKey:@"zaloid"];
    [param setObjectCheckNil:[[ZaloSDKApiWrapper sharedInstance] zaloOauthcode] forKey:@"oauthcode"];
    return [self postRequestWithPath:api_um_registeruserinfo parameters:param];
}

- (RACSignal *)registerPin:(NSString *)pin {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObjectCheckNil:[pin sha256] forKey:@"pin"];
    [param setObjectCheckNil:[[ZaloSDKApiWrapper sharedInstance] zaloUserId] forKey:@"zaloid"];
    [param setObjectCheckNil:[[ZaloSDKApiWrapper sharedInstance] zaloOauthcode] forKey:@"oauthcode"];
    return [self postRequestWithPath:api_um_registerpin parameters:param];
}

@end
