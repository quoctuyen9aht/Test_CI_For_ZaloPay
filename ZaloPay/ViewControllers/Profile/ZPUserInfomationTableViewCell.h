//
//  ZPUserInfomationTableViewCell.h
//  ZaloPay
//
//  Created by PhucPv on 5/21/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ZaloPayCommon/UITableViewCell+Extension.h>
@class ZPUserItemModel;
@interface ZPUserInfomationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImage;
+ (float)cellHeight;
- (void)setItem:(ZPUserItemModel *)item;
@end
