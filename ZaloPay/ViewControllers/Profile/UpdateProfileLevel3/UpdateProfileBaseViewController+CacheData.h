//
//  UpdateProfileBaseViewController+CacheData.h
//  ZaloPay
//
//  Created by bonnpv on 9/5/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "UpdateProfileBaseViewController.h"

@interface UpdateProfileBaseViewController (CacheData)
@property (nonatomic) NSString *cacheEmail;
@property (nonatomic) NSString *cacheIdentifier;
@property (nonatomic) UIImage *cacheAvatar;
@property (nonatomic) UIImage *cacheFontIdentityCard;
@property (nonatomic) UIImage *cacheBackSizeIdentityCard;
@end
