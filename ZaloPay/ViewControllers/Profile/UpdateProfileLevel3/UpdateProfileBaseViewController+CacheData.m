//
//  UpdateProfileBaseViewController+CacheData.m
//  ZaloPay
//
//  Created by bonnpv on 9/5/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "UpdateProfileBaseViewController+CacheData.h"
#import <ZaloPayProfile/ZaloPayProfile-Swift.h>


@implementation UpdateProfileBaseViewController (CacheData)

- (NSString *)cacheEmail {
    NSString *key = [self cacheKey:ZPProfileLevel3_Email];
    return [ZPCache objectForKey:key];
}

- (void)setCacheEmail:(NSString *)cacheEmail {
    NSString *key = [self cacheKey:ZPProfileLevel3_Email];
    [self setCacheData:cacheEmail forKey:key];
}

#pragma mark - Identifier

- (NSString *)cacheIdentifier {
    NSString *key = [self cacheKey:ZPProfileLevel3_Identifier];
    return [ZPCache objectForKey:key];
}

- (void)setCacheIdentifier:(NSString *)cacheIdentifier {
    NSString *key = [self cacheKey:ZPProfileLevel3_Identifier];
    [self setCacheData:cacheIdentifier forKey:key];
}

#pragma mark - FrontCard

- (UIImage *)cacheFontIdentityCard {
    NSString *key = [self cacheKey:ZPProfileLevel3_FrontIdentifyCard];
    return [ZPCache objectForKey:key];
}

- (void)setCacheFontIdentityCard:(UIImage *)cacheFontIdentityCard {
    NSString *key = [self cacheKey:ZPProfileLevel3_FrontIdentifyCard];
    [self setCacheData:cacheFontIdentityCard forKey:key];
}

#pragma mark - BackSideCard

- (UIImage *)cacheBackSizeIdentityCard {
    NSString *key = [self cacheKey:ZPProfileLevel3_BackSideIdentifyCard];
    return [ZPCache objectForKey:key];
}

- (void)setCacheBackSizeIdentityCard:(UIImage *)cacheBackSizeIdentityCard {
    NSString *key = [self cacheKey:ZPProfileLevel3_BackSideIdentifyCard];
    [self setCacheData:cacheBackSizeIdentityCard forKey:key];
}

#pragma mark - BackSideCard

- (UIImage *)cacheAvatar {
    NSString *key = [self cacheKey:ZPProfileLevel3_Avatar];
    return [ZPCache objectForKey:key];
}

- (void)setCacheAvatar:(UIImage *)cacheAvatar {
    NSString *key = [self cacheKey:ZPProfileLevel3_Avatar];
    [self setCacheData:cacheAvatar forKey:key];
}

#pragma mark - Base

- (void)setCacheData:(id<NSCoding>)object forKey:(NSString *)key {
    [ZPCache setObject:object forKey:key];
}

- (NSString *)cacheKey:(NSString *)baseKey {
    NSString *uid = [ZPProfileManager shareInstance].userLoginData.paymentUserId;
    return [NSString stringWithFormat:@"%@_%@",baseKey,uid];
}

@end
