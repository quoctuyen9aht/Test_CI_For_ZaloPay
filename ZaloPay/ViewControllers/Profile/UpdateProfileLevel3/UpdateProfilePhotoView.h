//
//  UpdateProfileUploadPhotoView.h
//  ZaloPay
//
//  Created by bonnpv on 6/29/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpdateProfilePhotoView : UIView
@property (nonatomic, strong) IBOutlet UILabel *labelIdentifiPhotoDescription;
@property (nonatomic, strong) IBOutlet UILabel *labelFrontPhoto;
@property (nonatomic, strong) IBOutlet UIButton *buttonFrontPhoto;
@property (nonatomic, strong) IBOutlet UILabel *labelBacksidePhoto;
@property (nonatomic, strong) IBOutlet UIButton *buttonBacksidePhoto;
@property (nonatomic, strong) IBOutlet UILabel *labelAvatarDescription;
@property (nonatomic, strong) IBOutlet UILabel *labelAvatarPhoto;
@property (nonatomic, strong) IBOutlet UIButton *buttonAvatarPhoto;
@property (nonatomic, strong) IBOutlet UIImageView *imageViewFrontPhoto;
@property (nonatomic, strong) IBOutlet UIImageView *imageViewBacksidePhoto;
@property (nonatomic, strong) IBOutlet UIImageView *imageViewAvatar;
@property (nonatomic, strong) IBOutlet UIView *line1;
@property (nonatomic, strong) IBOutlet UIView *line2;
@property (nonatomic, strong) IBOutlet UIView *line3;
@property (nonatomic, strong) IBOutlet UIView *line4;
@property (nonatomic, strong) IBOutlet UIView *line5;
@property (nonatomic, strong) IBOutlet UIView *avatarBackground;
@end
