//
//  UpdateProfileMainViewController.h
//  ZaloPay
//
//  Created by bonnpv on 6/28/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "UpdateProfileBaseViewController.h"

@interface UpdateProfileMainViewController : UpdateProfileBaseViewController

@end
