//
//  UpdateProfileMainViewController.m
//  ZaloPay
//
//  Created by bonnpv on 6/28/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "UpdateProfileMainViewController.h"
#import ZALOPAY_MODULE_SWIFT
#import "UpdateProfileInputInfoView.h"
#import "UpdateProfileUploadPhotoViewController.h"
#import "UpdateProfileBaseViewController+CacheData.h"

#import <ZaloPayUI/BaseViewController+Keyboard.h>
#import <ZaloPayCommon/Macro.h>
#import <ZaloPayUI/BaseViewController+ZPScrollViewController.h>
#import <ZaloPayCommon/NSString+Validation.h>
#import <ZaloPayCommon/NSString+Asscci.h>
#import <ZaloPayAnalyticsSwift/ZaloPayAnalyticsSwift-Swift.h>
#import <ZaloPayCommonSwift/ZaloPayCommonSwift-Swift.h>

@interface UpdateProfileMainViewController ()<UITextFieldDelegate>{
    
    BOOL isFirstInputIndenity;
}
@property (nonatomic, strong) UpdateProfileInputInfoView *inputInfoView;
@end


@implementation UpdateProfileMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self registerHandleKeyboardNotification];
    [self checkEnableNextButton];
//    [[ZPAppFactory sharedInstance] trackEvent:ZPAnalyticEventActionMe_profile_touch_identity];
    [[ZPTrackingHelper shared].eventTracker trackScreen:[self screenName]];
    
    isFirstInputIndenity = false;
}

//- (NSString *)screenName {
//    return ZPTrackerScreen.Screen_Me_Profile_Identify;
//}

- (void)setUpUiBar { // luôn luôn show back button
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.leftBarButtonItems = [self leftBarButtonItems];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.inputInfoView.textFieldIdentify.textField.isFirstResponder == false) {
        [self.inputInfoView.textFieldEmail.textField becomeFirstResponder];
    }
}

- (float)scrollHeight {
    return 225;
}

- (float)buttonToScrollOffset {
    return  kZaloButtonToScrollOffset;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    if (IS_IOS7) {
        [self updateScrollContentSize];
    }
    if (IS_IPHONE_35_INCH) {
        [self.scrollView scrollToBottomOffset:45 animation:false];
    }
    if (IS_IPHONE_40_INCH) {
        [self.scrollView scrollToBottom:false];
    }
}

- (void)checkEnableNextButton {
    self.buttonNext.enabled = self.inputInfoView.textFieldEmail.textField.text.length > 0 &&
                              self.inputInfoView.textFieldIdentify.textField.text.length > 0;
}

- (void)backButtonClicked:(id)sender {
    [super backButtonClicked:sender];
}

//- (ZPAnalyticEventAction)backEventId {
//    return ZPAnalyticEventActionMe_profile_identity_back;
//}

- (void)viewWillSwipeBack {
    self.inputInfoView.textFieldEmail.textField.delegate = nil;
    self.inputInfoView.textFieldIdentify.textField.delegate = nil;
    
    dispatch_async(dispatch_get_global_queue(0, DISPATCH_TIME_NOW), ^{
        self.cacheEmail = self.inputInfoView.textFieldEmail.textField.text;
        self.cacheIdentifier = self.inputInfoView.textFieldIdentify.textField.text;
    });
    
    if (self.cancel) {
        self.cancel();
    }
    [self.navigationController defaultNavigationBarStyle];
}

- (UIView *)bottomView {
    return self.inputInfoView;
}

- (UpdateProfileInputInfoView *)inputInfoView {
    if (!_inputInfoView) {
        _inputInfoView = [UpdateProfileInputInfoView viewFromSameNib];
        _inputInfoView.textFieldEmail.textField.text = self.cacheEmail;
        _inputInfoView.textFieldIdentify.textField.text = self.cacheIdentifier;
        
        _inputInfoView.textFieldEmail.textField.delegate = self;
        _inputInfoView.textFieldIdentify.textField.delegate = self;
        
        [_inputInfoView.textFieldEmail.textField addTarget:self
                                                    action:@selector(textFieldDidChange:)
                                          forControlEvents:UIControlEventEditingChanged];
        [_inputInfoView.textFieldIdentify.textField addTarget:self
                                                       action:@selector(textFieldDidChange:)
                                             forControlEvents:UIControlEventEditingChanged];
    }
    return _inputInfoView;
}

- (void)nextButtonClick {
    if (![self isValidData]) {
        return;
    }
    NSString *email = self.inputInfoView.textFieldEmail.textField.text;
    NSString *identifier = self.inputInfoView.textFieldIdentify.textField.text;
    [self.inputInfoView.textFieldEmail resignFirstResponder];
    [self.inputInfoView.textFieldIdentify resignFirstResponder];
    UpdateProfileUploadPhotoViewController *upload = [[UpdateProfileUploadPhotoViewController alloc] initWithCancel:self.cancel
                                                                                                              error:self.error
                                                                                                        andComplete:self.complete];
//    upload.email = email;
//    upload.identifier = identifier;
//    self.cacheEmail = email;
//    self.cacheIdentifier = identifier;
    [self.navigationController pushViewController:upload animated:YES];
}

- (BOOL)isValidData {
    if (self.inputInfoView.textFieldEmail.textField.text.length > EMAIL_LENGTH) {
        NSString *message = [NSString stringWithFormat:[R string_UpdateProfileLevel3_Invalid_EmailLength], EMAIL_LENGTH];
        [self.inputInfoView.textFieldEmail showErrorAuto:message];
        return false;
    }
    
    NSString *email = self.inputInfoView.textFieldEmail.textField.text;
    if (![email isValidEmail]) {
        [self.inputInfoView.textFieldEmail showErrorAuto:[R string_UpdateProfileLevel3_Email_Invalid]];
        [self.inputInfoView.textFieldEmail becomeFirstResponder];
        return false;
    }
    
    if (self.inputInfoView.textFieldIdentify.textField.text.length > IDENTYFIER_NUMBER_LENGTH) {
        NSString *message = [NSString stringWithFormat:[R string_UpdateProfileLevel3_Invalid_IdentifierLength], IDENTYFIER_NUMBER_LENGTH];
        [self.inputInfoView.textFieldIdentify showErrorAuto:message];
        return false;
    }
    return true;
}

#pragma mark - override keyboard notification

- (UIScrollView *)internalScrollView {
    return self.scrollView;
}

- (UIButton *)internalBottomButton {
    return self.buttonNext;
}

#pragma mark - Text Field Delegate 

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    DDLogInfo(@"string = %@",string);
    NSInteger length = textField.text.length + string.length;
    
    if (textField == self.inputInfoView.textFieldEmail.textField) {
        return [self validEmailLength:length];
    }
    
    if (textField == self.inputInfoView.textFieldIdentify.textField) {
        return [self validIdentifierLength:length];
    }
    
    return  YES;
}

- (BOOL)validEmailLength:(NSInteger)length {
    if (length > EMAIL_LENGTH) {
        NSString *message = [NSString stringWithFormat:[R string_UpdateProfileLevel3_Invalid_EmailLength], EMAIL_LENGTH];
        [self.inputInfoView.textFieldEmail showErrorAuto:message];
        return false;
    }
    [self.inputInfoView.textFieldEmail clearErrorText];
    return true;
}

- (BOOL)validIdentifierLength:(NSInteger)length {
    if (length > IDENTYFIER_NUMBER_LENGTH) {
        NSString *message = [NSString stringWithFormat:[R string_UpdateProfileLevel3_Invalid_IdentifierLength], IDENTYFIER_NUMBER_LENGTH];
        [self.inputInfoView.textFieldIdentify showErrorAuto:message];
        return false;
    }
    [self.inputInfoView.textFieldIdentify clearErrorText];
    return true;
}

- (void)textFieldDidChange:(UITextField*)textField {
    
    if (!isFirstInputIndenity){
        isFirstInputIndenity = true;
//        [[ZPAppFactory sharedInstance] trackEvent:ZPAnalyticEventActionMe_profile_identity_input];
    }
    
    [self checkEnableNextButton];
    if (textField.text.length != 0) {
        textField.text = [[textField.text ascciString] lowercaseString];
        return;
    }
    
    if (textField == self.inputInfoView.textFieldEmail.textField) {
        [self.inputInfoView.textFieldEmail clearErrorText];
    }
    
    if (textField == self.inputInfoView.textFieldIdentify.textField) {
        [self.inputInfoView.textFieldIdentify clearErrorText];
    }
}

@end
