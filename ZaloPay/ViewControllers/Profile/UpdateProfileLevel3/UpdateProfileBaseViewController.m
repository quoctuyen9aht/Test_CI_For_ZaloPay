//
//  UpdateProfileBaseViewController.m
//  ZaloPay
//
//  Created by bonnpv on 6/28/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "UpdateProfileBaseViewController.h"
#import ZALOPAY_MODULE_SWIFT
#import <ZaloPayProfile/ZaloPayProfile-Swift.h>

@implementation UpdateProfileBaseViewController

- (id)initWithCancel:(UpdateProfileHandle)cancel
               error:(UpdateProfileHandle)error
         andComplete:(UpdateProfileHandle)complete {

    self = [super init];
    if (self) {
        self.cancel = cancel;
        self.error = error;
        self.complete = complete;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.title = [R string_UpdateProfileLevel3_Update_Title];
//    @weakify(self);
//    [[[[ZPProfileManager shareInstance] loadZaloUserProfile] deliverOnMainThread]subscribeNext:^(ZaloUserSwift *user) {
//        @strongify(self);
//        if (user.displayName.length > 0) {
//            self.title = user.displayName;
//        }
//    }];
    [self addScollView];
    [self addBottomButton];
}

- (float)scrollHeight {
    return 182;
}

- (float)buttonToScrollOffset {
    return 10;
}

- (void)updateScrollContentSize {
    UIView *bottomView = [self bottomView];
    float contentSizeHeight =  bottomView.frame.size.height;
    float contentSizeWidth = [UIScreen mainScreen].applicationFrame.size.width;
    self.scrollView.contentSize = CGSizeMake(contentSizeWidth, contentSizeHeight);
}

- (UIView *)bottomView {
    return nil;
}

- (void)addScollView {
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:self.scrollView];
    self.scrollView.backgroundColor = [UIColor defaultBackground];
    [self setupScrollViewFrame];
    
    UIView *bottomView = [self bottomView];
    [self updateScrollContentSize];
    
    [self.scrollView addSubview:bottomView];
    
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.left.equalTo(0);
        make.width.equalTo([UIScreen mainScreen].applicationFrame.size.width);
        make.height.equalTo(bottomView.frame.size.height);
    }];
}

- (void)setupScrollViewFrame {
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.height.equalTo([self scrollHeight]);
        make.left.equalTo(0);
        make.width.equalTo([UIScreen mainScreen].applicationFrame.size.width);
    }];
    [self.scrollView layoutIfNeeded];
}

- (void)addBottomButton {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:button];
    [button setTitle:[R string_ButtonLabel_Next] forState:UIControlStateNormal];
    [button setupZaloPayButton];
        
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(10);
        make.right.equalTo(-10);
        make.top.equalTo(self.scrollView.mas_bottom).with.offset([self buttonToScrollOffset]);
        make.height.equalTo(kZaloButtonHeight);
    }];
    [button roundRect:3];
    [button addTarget:self action:@selector(nextButtonClick) forControlEvents:UIControlEventTouchUpInside];
    self.buttonNext = button;
}

- (void)nextButtonClick {

}

@end
