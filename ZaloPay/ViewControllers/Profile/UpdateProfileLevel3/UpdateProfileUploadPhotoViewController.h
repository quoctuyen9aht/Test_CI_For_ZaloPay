//
//  UpdateProfileUploadPhotoViewController.h
//  ZaloPay
//
//  Created by bonnpv on 6/29/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "UpdateProfileBaseViewController.h"

@interface UpdateProfileUploadPhotoViewController : UpdateProfileBaseViewController
//@property (nonatomic, strong) NSString *email;
//@property (nonatomic, strong) NSString *identifier;

@property (nonatomic, strong) NSString *fullName;
@property (nonatomic) NSInteger gender;
@property (nonatomic, strong) NSDate *dob;
@property (nonatomic) NSInteger idType;
@property (nonatomic, strong) NSString *idValue;


@end
