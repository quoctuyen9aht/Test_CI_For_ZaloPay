//
//  UpdateProfileInputInfoView.h
//  ZaloPay
//
//  Created by bonnpv on 6/28/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ZaloPayCommon/ZPFloatTextInputView.h>

@interface UpdateProfileInputInfoView : UIView
@property (nonatomic, strong) ZPFloatTextInputView *textFieldEmail;
@property (nonatomic, strong) ZPFloatTextInputView *textFieldIdentify;
@end
