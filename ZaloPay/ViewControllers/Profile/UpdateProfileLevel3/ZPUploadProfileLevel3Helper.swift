//
//  ZPUploadProfileLevel3Helper.swift
//  ZaloPay
//
//  Created by Bon Bon on 3/19/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import ObjectMapper
class ZPUploadProfileLevel3Helper {
    private weak var fromController: UIViewController?
    private let cacheDataTable: ZPCacheDataTable = ZPCacheDataTable(db: PerUserDataBase.sharedInstance())
    
    public func isDoneUpload() -> Bool {
        return cacheDataTable.cacheDataValue(forKey: kDisableUpdateProfileLevel3)?.toBool() ?? false
    }
    
    public  func alertDoneUpload()  {
        ZPDialogView.showDialog(with: DialogTypeInfo,
                                message: R.string_UpdateProfileLevel3_UploadSuccess_Message(),
                                cancelButtonTitle: R.string_ButtonLabel_Close(),
                                otherButtonTitle: nil,
                                completeHandle: nil)
        
    }
    
//    public  func updateLevel3ViewController(_ cancel: @escaping () -> Void,
//                                               error: @escaping () -> Void,
//                                            complete: @escaping () -> Void) -> UIViewController {
//        return UpdateProfileMainViewController(cancel: cancel, error: error, andComplete: complete)!
//    }
//    
    
    public func uploadLevel(from: UIViewController?,
                            cancel: @escaping () -> Void,
                            error: @escaping () -> Void,
                            complete: @escaping () -> Void) {
        self.fromController = from
        ZaloPayWalletSDKPayment.sharedInstance().runKYCFlow(from,
                                                            title: R.string_UpdateProfileLevel2_Title(),
                                                            infor: nil,
                                                            type: ZPKYCType.profile).subscribeNext({ [weak self](result) in
                                                                let dict = (result as? JSON) ?? [:]
                                                                self?.uploadPhoto(json:dict,
                                                                                  cancel: cancel,
                                                                                  error: error,
                                                                                  complete: complete)
                                                            }) { (error) in
                                                                cancel()
        }
    }
    
    
    private func uploadPhoto(json: JSON,
                             cancel: @escaping () -> Void,
                             error: @escaping () -> Void,
                             complete: @escaping () -> Void) {
        
        guard let userInfo = kycFromDic(json) else {
            return
        }
        
        let controller = UpdateProfileUploadPhotoViewController(cancel: cancel, error:error, andComplete:complete)!
        controller.fullName = userInfo.fullName
        controller.idValue = userInfo.idValue
        if let gender = userInfo.gender {
            controller.gender = gender
        }
        controller.dob = userInfo.dob
        if let idType = userInfo.idType {
            controller.idType = idType
        }
        self.fromController?.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func kycFromDic(_ kycDic: JSON) -> ZPKYCOnboardingEntity? {
        guard let infor = kycDic["kyc"] as? String else {
            return nil
        }
        return Mapper<ZPKYCOnboardingEntity>().map(JSONString: infor)
    }

}
