//
//  UpdateProfileUploadPhotoView.m
//  ZaloPay
//
//  Created by bonnpv on 6/29/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "UpdateProfilePhotoView.h"

@implementation UpdateProfilePhotoView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = [UIColor clearColor];
    self.line1.backgroundColor = [UIColor lineColor];
    self.line2.backgroundColor = [UIColor lineColor];
    self.line3.backgroundColor = [UIColor lineColor];
    self.line4.backgroundColor = [UIColor lineColor];
    self.line5.backgroundColor = [UIColor lineColor];
    
    self.labelAvatarDescription.numberOfLines = 0;
    self.labelIdentifiPhotoDescription.numberOfLines = 0;
    
    self.labelIdentifiPhotoDescription.textColor = [UIColor subText];
    self.labelAvatarDescription.textColor = [UIColor subText];
    self.labelFrontPhoto.textColor = [UIColor subText];
    self.labelAvatarPhoto.textColor = [UIColor subText];
    self.labelBacksidePhoto.textColor = [UIColor subText];
    
    UIFont *font = [UIFont SFUITextRegularWithSize:15];
    self.labelIdentifiPhotoDescription.font = [UIFont SFUITextRegularWithSize:13];
    self.labelFrontPhoto.font = font;
    self.labelBacksidePhoto.font = font;
    self.labelAvatarDescription.font = [UIFont SFUITextRegularWithSize:13];
    self.labelAvatarPhoto.font = font;
  
    [self setupButton:self.buttonFrontPhoto];
    [self setupButton:self.buttonAvatarPhoto];
    [self setupButton:self.buttonBacksidePhoto];
}

- (void)setupButton:(UIButton *)button {
    NSString *normalIcon = @"profile_camera";
    NSString *removeIcon = @"general_del";
    UIColor *color = [UIColor colorWithHexValue:0x848d99];
    
    button.titleLabel.font = [UIFont iconFontWithSize:22];
    [button setTitleColor:color forState:UIControlStateNormal];
    [button setIconFont:normalIcon forState:UIControlStateNormal];
    [button setIconFont:removeIcon forState:UIControlStateSelected];
    
}

@end
