//
//  UpdateProfileUploadPhotoViewController.m
//  ZaloPay
//
//  Created by bonnpv on 6/29/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "UpdateProfileUploadPhotoViewController.h"
#import "UpdateProfilePhotoView.h"
#import "ZaloPayPhotoPicker.h"
#import "NetworkManager+Profile.h"
#import ZALOPAY_MODULE_SWIFT
#import "UpdateProfileBaseViewController+CacheData.h"
//#import "ApplicationState+GetConfig.h"
#import <ZaloPayNetwork/NSError+ZaloPayAPI.h>
#import <ZaloPayNetwork/ZaloPayErrorCode.h>
#import <ZaloPayConfig/ZaloPayConfig-Swift.h>
#import <ZaloPayCommonSwift/ZaloPayCommonSwift-Swift.h>

@interface UpdateProfileUploadPhotoViewController ()
@property (nonatomic, strong) UpdateProfilePhotoView *uploadPhotoView;
@property (nonatomic, strong) ZaloPayPhotoPicker *frontPicker;
@property (nonatomic, strong) ZaloPayPhotoPicker *backSidePicker;
@property (nonatomic, strong) ZaloPayPhotoPicker *avatarPicker;
@property (nonatomic, strong) ZPCacheDataTable* cacheDataTable;
@end

@implementation UpdateProfileUploadPhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.cacheDataTable = [[ZPCacheDataTable alloc] initWithDB:[PerUserDataBase sharedInstance]];
    self.title = @"Hình ảnh";
    [self showCacheData];
}

- (float)scrollHeight {
    return 330;
}

- (float)buttonToScrollOffset {
    if (IS_IPHONE_35_INCH || IS_IPHONE_40_INCH) {
        return 10;
    }
    return 45;
}

- (void)showCacheData {
    [[ZPAppFactory sharedInstance] showHUDAddedTo:nil];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.avatarPicker.image = self.cacheAvatar;
        [self doneSelectAvatar:self.cacheAvatar];
        
        self.backSidePicker.image = self.cacheBackSizeIdentityCard;
        [self doneSelectBackSideImage:self.cacheBackSizeIdentityCard];
        
        self.frontPicker.image = self.cacheFontIdentityCard;
        [self doneSelectFontImage:self.cacheFontIdentityCard];
        [[ZPAppFactory sharedInstance] hideAllHUDsForView:nil];
    });
}

- (void)viewWillSwipeBack {
    dispatch_async(dispatch_get_global_queue(0, DISPATCH_TIME_NOW), ^{
        self.cacheAvatar = self.avatarPicker.image;
        self.cacheBackSizeIdentityCard = self.backSidePicker.image;
        self.cacheFontIdentityCard = self.frontPicker.image;
    });
}

- (UIView *)bottomView {
    return self.uploadPhotoView;
}

- (UpdateProfilePhotoView *)uploadPhotoView {
    if (!_uploadPhotoView) {
        _uploadPhotoView = [UpdateProfilePhotoView viewFromSameNib];
        [_uploadPhotoView.buttonFrontPhoto addTarget:self
                                              action:@selector(selectFrontIdentifierPhoto:)
                                    forControlEvents:UIControlEventTouchUpInside];
        [_uploadPhotoView.buttonBacksidePhoto addTarget:self
                                                 action:@selector(selectBackSideIdentifierPhoto:)
                                       forControlEvents:UIControlEventTouchUpInside];
        [_uploadPhotoView.buttonAvatarPhoto addTarget:self
                                               action:@selector(selectAvatarPhoto:)
                                     forControlEvents:UIControlEventTouchUpInside];
    }
    return _uploadPhotoView;
}

- (void)setupScrollViewFrame {
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.height.equalTo([self scrollHeight]);
        make.left.equalTo(0);
        make.right.equalTo(0);
    }];
}

- (BOOL)validInputData {
    if (self.frontPicker.image == nil) {
        [self showDialogWithMessage:[R string_UpdateProfileLevel3_Invalid_Front_IdentifierPhoto]];
        return FALSE;
    }
    if (self.backSidePicker.image == nil) {
        [self showDialogWithMessage:[R string_UpdateProfileLevel3_Invalid_BackSize_IdentifierPhoto]];
        return FALSE;
    }
    
    if (self.avatarPicker.image == nil) {
        [self showDialogWithMessage:[R string_UpdateProfileLevel3_Invalid_Avatar]];
        return FALSE;
    }
    return TRUE;
}

- (void)showDialogWithMessage:(NSString *)message {
    [ZPDialogView showDialogWithType:DialogTypeNotification
                             message:message
                   cancelButtonTitle:[R string_ButtonLabel_Close]
                    otherButtonTitle:nil
                      completeHandle:nil];
}


- (void)nextButtonClick {
    if ([self validInputData]) {
        [self requestUpdateProfile];
    }
}


- (void)requestUpdateProfile {
   [[ZPAppFactory sharedInstance] showHUDAddedTo:self.view];
    
    float maxSize = [self maxSizeFromConfig];
    UIImage *front = [self validImageSize:self.frontPicker.image maxSize:maxSize];
    UIImage *back = [self validImageSize:self.backSidePicker.image maxSize:maxSize];
    UIImage *avatar = [self validImageSize:self.avatarPicker.image maxSize:maxSize];
    @weakify(self);
//    [[[[NetworkManager sharedInstance] requestUpdateProfileWithEmail:self.email
//                                                          identifier:self.identifier
//                                                frontIdentifierPhoto:front
//                                              backsideIdentiferPhoto:back
//                                                              avatar:avatar
//                                                        imageQuality:[self imageQuality]] deliverOnMainThread] subscribeNext:^(id x) {
//        @strongify(self);
//        [self handleUploadSuccess:[R string_UpdateProfileLevel3_UploadSuccess_Message]];
//    } error:^(NSError *error) {
//        @strongify(self);
//        if (error.code == ZALOPAY_ERRORCODE_DONE_UPLOAD_PROFILE) {
//            [self handleUploadSuccess:[error apiErrorMessage]];
//            return;
//        }
//        [self alertError:[error apiErrorMessage]];
//        [[ZPAppFactory sharedInstance] hideAllHUDsForView:self.view];
//    }];
    
    
    // New request: KYC verify via photo
    [[[[NetworkManager sharedInstance] requestUpdateProfileWithFullName:self.fullName
                                                                 gender:self.gender
                                                             dayOfBirth:self.dob
                                                         identifierType:self.idType
                                                        identifierValue:self.idValue
                                                   frontIdentifierPhoto:front
                                                 backsideIdentiferPhoto:back
                                                                 avatar:avatar
                                                           imageQuality:[self imageQuality]] deliverOnMainThread] subscribeNext:^(id x) {
        @strongify(self);
        [self handleUploadSuccess:[R string_UpdateProfileLevel3_UploadSuccess_Message]];
    } error:^(NSError *error) {
        @strongify(self);
        if (error.code == ZALOPAY_ERRORCODE_DONE_UPLOAD_PROFILE) {
            [self handleUploadSuccess:[error apiErrorMessage]];
            return;
        }
        [self alertError:[error apiErrorMessage]];
        [[ZPAppFactory sharedInstance] hideAllHUDsForView:self.view];
    }];
    //
}

- (float)maxSizeFromConfig {
//    NSDictionary *config = [ApplicationState sharedInstance].zalopayConfig;
//    NSDictionary *profiles = [config dictionaryForKey:@"updateprofilelevel" defaultValue:@{}];
//    return [profiles doubleForKey:@"imagesize" defaultValue:1024];
    id<ZPUpdateProfileLevelConfigProtocol> updateProfileLevelConfig = [ZPApplicationConfig getUpdateProfileLevelConfig];
    return updateProfileLevelConfig ? [updateProfileLevelConfig getImagesize] : 1024;
}

- (UIImage *)validImageSize:(UIImage *)image maxSize:(float)maxSize{
    float width = image.size.width;
    float height = image.size.height;
    
    if (width < maxSize && height < maxSize) {
        return image;
    }
    if (width > height) {
        height = maxSize * height / width;
        width = maxSize;
    } else {
        width = maxSize * width / height;
        height = maxSize;
    }
    return [image scaledToSize:CGSizeMake(width, height)];
}

- (float)imageQuality {
    float h = MAX(UIScreen.mainScreen.bounds.size.width, UIScreen.mainScreen.bounds.size.height);
    id<ZPUpdateProfileLevelConfigProtocol> updateProfileLevelConfig = [ZPApplicationConfig getUpdateProfileLevelConfig];
    NSDictionary *dicQuality =  [updateProfileLevelConfig getImageQuality];
    
    float quality = 0.5;
    
    NSArray *arrKeysSorted =  [dicQuality.allKeys sortedArrayUsingComparator:^NSComparisonResult(NSString*  _Nonnull key1, NSString*  _Nonnull key2) {
        return [key1 floatValue] > [key2 floatValue];
    }];
    for (NSString *key in arrKeysSorted) {
        if (h <= [key floatValue]) {
            return ([dicQuality doubleForKey:key defaultValue:0.5]);
        }
    }
    return quality;
}

- (void)alertError:(NSString *)message {
    @weakify(self);
    [ZPDialogView showDialogWithType:DialogTypeNotification
                             message:message
                   cancelButtonTitle:[R string_ButtonLabel_Retry]
                    otherButtonTitle:@[[R string_ButtonLabel_Close]]
                      completeHandle:^(int buttonIndex, int cancelButtonIndex) {
                          @strongify(self);
                          if (buttonIndex == cancelButtonIndex) {
                              [self requestUpdateProfile];
                          }
                      }];

}
- (void)handleUploadSuccess:(NSString *)message {
    [[ZPAppFactory sharedInstance] hideAllHUDsForView:self.view];
    [self.cacheDataTable cacheDataUpdateValue:[@(TRUE) stringValue] forKey:kDisableUpdateProfileLevel3];
    if (self.complete) {
        [self.navigationController defaultNavigationBarStyle];
        self.complete();
    }
    [self showAlertSuccess:message];
    self.cacheAvatar = nil;
    self.cacheFontIdentityCard = nil;
    self.cacheBackSizeIdentityCard = nil;
//    self.email = nil;
//    self.identifier = nil;
    
    self.fullName = nil;
    self.gender = 0;
    self.dob = nil;
    self.idType = 0;
    self.idValue = nil;
}

- (void)showAlertSuccess:(NSString *)message {
    ZPDialogView *alertView = [[ZPDialogView alloc] initWithType:DialogTypeInfo
                                                         message:message
                                                           items:nil];
    [alertView show];
}

#pragma mark - Funtion

- (ZaloPayPhotoPicker *)frontPicker {
    if (!_frontPicker) {
        _frontPicker = [[ZaloPayPhotoPicker alloc] init];
    }
    return _frontPicker;
}

- (ZaloPayPhotoPicker *)backSidePicker {
    if (!_backSidePicker) {
        _backSidePicker = [[ZaloPayPhotoPicker alloc] init];
    }
    return _backSidePicker;
}

- (ZaloPayPhotoPicker *)avatarPicker {
    if (!_avatarPicker) {
        _avatarPicker = [[ZaloPayPhotoPicker alloc] init];
    }
    return _avatarPicker;
}

#pragma mark - Action

- (IBAction)selectFrontIdentifierPhoto:(id)sender {
    
    UIButton *button = self.uploadPhotoView.buttonFrontPhoto;
    if (button.selected) {
        self.uploadPhotoView.labelFrontPhoto.hidden = NO;
        self.uploadPhotoView.imageViewFrontPhoto.image = nil;
        self.frontPicker.image = nil;
        button.selected = FALSE;
        return;
    }
    @weakify(self);
    [self.frontPicker selectPhotoFromViewController:self withComplete:^(UIImage *result) {
        @strongify(self);
        [self doneSelectFontImage:result];
    }];
}

- (void)doneSelectFontImage:(UIImage *)result {
    if (result) {
        self.uploadPhotoView.imageViewFrontPhoto.image = result;
        self.uploadPhotoView.labelFrontPhoto.hidden = YES;
        self.uploadPhotoView.buttonFrontPhoto.selected = TRUE;
    }
}

- (IBAction)selectBackSideIdentifierPhoto:(id)sender {
    UIButton *button = self.uploadPhotoView.buttonBacksidePhoto;
    if (button.selected) {
        self.uploadPhotoView.labelBacksidePhoto.hidden = NO;
        self.uploadPhotoView.imageViewBacksidePhoto.image = nil;
        self.backSidePicker.image = nil;
        button.selected = FALSE;
        return;
    }
    @weakify(self);
    [self.backSidePicker selectPhotoFromViewController:self withComplete:^(UIImage *result) {
        @strongify(self);
        [self doneSelectBackSideImage:result];
    }];
}

- (void)doneSelectBackSideImage:(UIImage *)result {
    if (result) {
        self.uploadPhotoView.imageViewBacksidePhoto.image = result;
        self.uploadPhotoView.labelBacksidePhoto.hidden = YES;
        self.uploadPhotoView.buttonBacksidePhoto.selected = TRUE;
    }
}

- (IBAction)selectAvatarPhoto:(id)sender {
    UIButton *button = self.uploadPhotoView.buttonAvatarPhoto;
    if (button.selected) {
        self.uploadPhotoView.labelAvatarPhoto.hidden = NO;
        self.uploadPhotoView.imageViewAvatar.image = nil;
        self.avatarPicker.image = nil;
        button.selected = FALSE;
        [self updateAvatarBackgroundWithHeight:60];
        return;
    }
    @weakify(self);
    [self.avatarPicker selectPhotoFromViewController:self withComplete:^(UIImage *result) {
        @strongify(self);
        [self doneSelectAvatar:result];
    }];
}

- (void)doneSelectAvatar:(UIImage *)result {
    if (result) {
        self.uploadPhotoView.imageViewAvatar.image = result;
        self.uploadPhotoView.labelAvatarPhoto.hidden = YES;
        self.uploadPhotoView.buttonAvatarPhoto.selected = TRUE;
        [self updateAvatarBackgroundWithHeight:90];
    }
}

- (void)updateAvatarBackgroundWithHeight:(float)height {
    [self.uploadPhotoView.avatarBackground mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(height);
    }];
//    [self.uploadPhotoView.imageViewAvatar mas_updateConstraints:^(MASConstraintMaker *make) {
//        make.height.equalTo(height - 10);
//        make.width.equalTo(height - 10);
//    }];
    
    [self.uploadPhotoView.avatarBackground.superview  layoutIfNeeded];
    
}

@end
