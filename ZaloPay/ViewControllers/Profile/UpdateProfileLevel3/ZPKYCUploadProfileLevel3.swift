////
////  ZPUploadProfileLevel3KYC.swift
////  ZaloPay
////
////  Created by CPU11901 on 6/21/18.
////  Copyright © 2018 VNG Corporation. All rights reserved.
////
//
//import UIKit
//import RxCocoa
//import RxSwift
//import ObjectMapper
//import ZaloPayAnalyticsSwift
//
//class ZPKYCUploadProfileLevel3 {
//    private weak var viewController: UIViewController?
//    
//    private let disposeBag = DisposeBag()
//    
//    func runKYCFlow(on viewController: UIViewController) {
//        self.viewController = viewController
//        let kycFlow = ~ZaloPayWalletSDKPayment.sharedInstance().runKYCFlow(viewController, title: R.string_UpdateProfileLevel2_Title(), infor: nil, type: ZPKYCType.profile)
//        
//        kycFlow.debug().subscribe(onNext: { (result) in
//            #if DEBUG
//            print(result ?? [:])
//            #endif
//            
//            let dict = result as? JSON
//            self.performPhotoScreen(data: dict ?? [:])
//        }, onError: { (e) in
//            self.completedFlow()
//        }, onCompleted: {
//        }).disposed(by: disposeBag)
//    }
//    
//    private func performPhotoScreen(data: JSON) {
//        guard let userInfo = kycFromDic(data) else {
//            return
//        }
//        let updateProfileUploadPhotoVC = UpdateProfileUploadPhotoViewController(cancel: {
//            // User tapped on back button on UpdateProfileUploadPhotoViewController screen
//        }, error: {
//            // UpdateProfileUploadPhotoViewController will show alert => Don't process here
//        }, andComplete: { [weak self] in
//            self?.completedFlow()
//        })
//        
//        guard let viewController = updateProfileUploadPhotoVC else {
//            completedFlow()
//            return
//        }
//        
//        // Pass user infos parameters to next screen (upload photo screen)
//        viewController.fullName = userInfo.fullName
//        if let gender = userInfo.gender {
//            viewController.gender = gender
//        }
//        viewController.dob = userInfo.dob
//        if let idType = userInfo.idType {
//            viewController.idType = idType
//        }
//        viewController.idValue = userInfo.idValue
//        self.viewController?.navigationController?.pushViewController(viewController, animated: true)
//    }
//    
//    private func completedFlow() {
//        guard  let from = self.viewController else {
//            self.viewController?.navigationController?.popToRootViewController(animated: true)
//            return
//        }
//        self.viewController?.navigationController?.popToViewController(from, animated: true)
//    }
//    
//    private func kycFromDic(_ kycDic: JSON) -> ZPKYCOnboardingEntity? {
//        guard let infor = kycDic["kyc"] as? String else {
//            return nil
//        }
//        return Mapper<ZPKYCOnboardingEntity>().map(JSONString: infor)
//    }
//}
