//
//  UpdateProfileInputInfoView.m
//  ZaloPay
//
//  Created by bonnpv on 6/28/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "UpdateProfileInputInfoView.h"
#import <MDHTMLLabel/MDHTMLLabel.h>
#import ZALOPAY_MODULE_SWIFT

@interface UpdateProfileInputInfoView ()<MDHTMLLabelDelegate>
@property (nonatomic, strong) IBOutlet UIView *contentView;
@property (nonatomic, strong) IBOutlet UIView *line1;
@property (nonatomic, strong) IBOutlet UILabel *labelTitle;
@property (nonatomic, strong) MDHTMLLabel *htmlLabel;
@end

@implementation UpdateProfileInputInfoView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.line1.backgroundColor = [UIColor lineColor];
    self.labelTitle.textColor = [UIColor subText];
    self.labelTitle.font = [UIFont SFUITextRegularWithSize:15];
    self.labelTitle.numberOfLines = 0;
    self.labelTitle.adjustsFontSizeToFitWidth = YES;
    if ([UIScreen mainScreen].applicationFrame.size.width == 320) {
        CGRect frame = self.labelTitle.frame;
        frame.origin.y = frame.origin.y - 15;
        self.labelTitle.frame = frame;
    }
    self.textFieldEmail = [[ZPFloatTextInputView alloc] init];
    self.textFieldIdentify = [[ZPFloatTextInputView alloc] init];
    [self.contentView addSubview:self.textFieldEmail];
    [self.contentView addSubview:self.textFieldIdentify];
    self.textFieldEmail.textField.placeholder = [R string_UpdateProfileLevel3_Email];
    self.textFieldIdentify.textField.placeholder = [R string_UpdateProfileLevel3_CMND];
    self.textFieldEmail.textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.textFieldEmail.textField.keyboardType = UIKeyboardTypeASCIICapable;
    self.textFieldEmail.textField.autocapitalizationType = NO;
    self.textFieldEmail.textField.autocorrectionType = NO;
    self.textFieldIdentify.textField.keyboardType = UIKeyboardTypeASCIICapable;
    self.textFieldIdentify.textField.autocapitalizationType = NO;
    self.textFieldIdentify.textField.autocorrectionType = NO;
    
    self.textFieldEmail.lineInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [self.textFieldEmail mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.left.equalTo(0);
        make.right.equalTo(0);
    }];
    [self.textFieldIdentify mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textFieldEmail.mas_bottom);
        make.left.equalTo(0);
        make.right.equalTo(0);
    }];
    [self htmlLabel];
}

- (MDHTMLLabel*)htmlLabel {
    if (!_htmlLabel) {
        MDHTMLLabel *htmlLabel = [[MDHTMLLabel alloc] init];
        [self addSubview:htmlLabel];
        float font = 15;
        if ([UIScreen mainScreen].applicationFrame.size.width == 320) {
            font = 14;
        }
        htmlLabel.delegate = self;
        htmlLabel.font = [UIFont SFUITextRegularWithSize:font];
        htmlLabel.htmlText = [R string_UpdateProfileLevel3_TermOfUse];
        htmlLabel.numberOfLines = 0;
        
        CGSize labelSize = [htmlLabel sizeThatFits:CGSizeMake(CGRectGetWidth([UIScreen mainScreen].bounds) - 40, 0)];
        
        [htmlLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(10);
            make.width.equalTo([UIScreen mainScreen].applicationFrame.size.width - 20);
            make.bottom.equalTo(0);
            make.height.equalTo(labelSize.height);
        }];
        _htmlLabel = htmlLabel;
    }
    return _htmlLabel;
}

#pragma mark - Html Delegate

- (void)HTMLLabel:(MDHTMLLabel *)label didSelectLinkWithURL:(NSURL*)URL {
    if ([[URL absoluteString] isEqualToString:@"termsOfUse"]) {
        NSDictionary *params = @{@"view": @"termsOfUse"};
        ZPDownloadingViewController *viewController = [[ZPDownloadingViewController alloc] initWithProperties:params];
        viewController.moduleName = @"About";
        [self.viewController.navigationController pushViewController:viewController animated:YES];
    }
}
@end
