//
//  UpdateProfileBaseViewController.h
//  ZaloPay
//
//  Created by bonnpv on 6/28/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//


typedef void (^UpdateProfileHandle)(void);

@interface UpdateProfileBaseViewController : BaseViewController
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIButton *buttonNext;

@property (nonatomic, copy) UpdateProfileHandle cancel;
@property (nonatomic, copy) UpdateProfileHandle error;
@property (nonatomic, copy) UpdateProfileHandle complete;

- (id)initWithCancel:(UpdateProfileHandle)cancel
               error:(UpdateProfileHandle)error
         andComplete:(UpdateProfileHandle)complete;

- (void)nextButtonClick;
- (void)setupScrollViewFrame;
- (void)updateScrollContentSize;
@end
