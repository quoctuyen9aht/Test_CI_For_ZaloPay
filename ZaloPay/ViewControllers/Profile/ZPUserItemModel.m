//
//  ZPUserItemModel.m
//  ZaloPay
//
//  Created by PhucPv on 5/22/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPUserItemModel.h"

@implementation ZPUserItemModel

+ (instancetype)itemWithTitle:(NSString *)title
                        value:(NSString *)value {
    ZPUserItemModel *userItemModel = [ZPUserItemModel new];
    userItemModel.title = title;
    userItemModel.value = value.length > 0 ? value : @"Chưa cập nhật  ";
    userItemModel.color = value.length > 0 ? [UIColor defaultText] : [UIColor placeHolderColor];
    userItemModel.hasValue = value.length >0;
    return userItemModel;
}

@end

@implementation ZPPinModel

@end
