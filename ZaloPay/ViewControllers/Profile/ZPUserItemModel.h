//
//  ZPUserItemModel.h
//  ZaloPay
//
//  Created by PhucPv on 5/22/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum {
    ZPUserItemModelTypeNone,
    ZPUserItemModelTypePhone,
    ZPUserItemModelTypeEmail,
    ZPUserItemModelTypeIdentifier,
    ZPUserItemModelTypeAcountName,
    ZPUserItemModelTypeLocation,
    ZPUserItemModelTypeVerificationStatus
} ZPUserItemModelType;

@interface ZPUserItemModel : NSObject
@property (nonatomic, strong) UIColor *color;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *value;
@property (nonatomic) BOOL hasValue;
@property (nonatomic) BOOL doneUploadData;
@property (nonatomic) ZPUserItemModelType type;

+ (instancetype)itemWithTitle:(NSString *)title
                        value:(NSString *)value;                                    
@end

@interface ZPPinModel : NSObject
@property (nonatomic, strong) NSString *title;
@end
