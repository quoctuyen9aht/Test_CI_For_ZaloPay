//
//  ZPUserInfomationTableViewCell.m
//  ZaloPay
//
//  Created by PhucPv on 5/21/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPUserInfomationTableViewCell.h"
#import "ZPUserItemModel.h"
@implementation ZPUserInfomationTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.titleLabel setFont:[UIFont SFUITextRegularWithSize:[UILabel mainTextSize]]];
    [self.valueLabel setFont:[UIFont SFUITextRegularWithSize:[UILabel mainTextSize]]];
    [self.titleLabel setTextColor:[UIColor subText]];            
    UIView *selectView = [[UIView alloc] init];
    selectView.backgroundColor = [UIColor selectedColor];
    [self setSelectedBackgroundView:selectView];
}

- (void)setItem:(ZPUserItemModel *)item {
    self.titleLabel.text = item.title;
    self.valueLabel.text = item.value;
    self.arrowImage.hidden = !(item.hasValue == false && item.doneUploadData == false);
    // Value font italic style in KYC case "Đang chờ duyệt"
    if (item.doneUploadData && item.type == ZPUserItemModelTypeVerificationStatus) {
        [self.valueLabel zpMainBlackItalic];
        self.arrowImage.hidden = false;
    }
    self.valueLabel.textColor = item.color;
    if (@available(iOS 11, *)) {
        [self.arrowImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.mas_right).offset(-12);
        }];
    }
    [self updateLabelFrame];
}

- (void)updateLabelFrame {
    [self.titleLabel sizeToFit];
    float height = [ZPUserInfomationTableViewCell cellHeight];
    CGRect frame = CGRectMake(10, 0, self.titleLabel.frame.size.width, height);
    self.titleLabel.frame = frame;
    
    [self.valueLabel sizeToFit];
    frame = self.valueLabel.frame;
    
    float arrowWidth = self.arrowImage.hidden? 20 : self.arrowImage.frame.size.width + 20;
    frame.origin.x = self.titleLabel.frame.origin.x + self.titleLabel.frame.size.width + 20;
    frame.origin.y = 0;
    frame.size.width = [UIScreen mainScreen].applicationFrame.size.width - frame.origin.x - arrowWidth;
    frame.size.height = height;
    self.valueLabel.frame = frame;
}

+ (float)cellHeight {
    if ([UIView isIPhone6Plus]) {
        return 60.0f;
    }
    return 50.0f;
}
@end
