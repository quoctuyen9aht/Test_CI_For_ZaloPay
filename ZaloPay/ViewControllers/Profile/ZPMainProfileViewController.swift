//
//  ZPMainProfileViewController.swift
//  ZaloPay
//
//  Created by nhatnt on 22/01/2018.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import CocoaLumberjackSwift
import RxSwift
import ZaloPayCommonSwift
import ZaloPayAnalyticsSwift
import ZaloPayProfile

@objcMembers
class ZPMainProfileViewController: BaseViewController {
    
    fileprivate let disposeBag = DisposeBag()
    // MARK: - UI Elements
    var tableView: UITableView!
//    var footers: [ZPMainProfileMainProfileFooterModel] = []
    var dataSource: [[AnyObject?]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.baseViewControllerViewDidLoad()
        self.createTableView()
        self.tableView.estimatedSectionHeaderHeight = 0 //set to 0 to disable
        self.fetchUserData()
        self.navigationController?.defaultNavigationBarStyle()
        self.automaticallyAdjustsScrollViewInsets = true
        self.title = R.string_UpdateProfileLevel2_Title()
        self.observeNotification()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ZPTrackingHelper.shared().trackScreen(with: self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func screenName() -> String! {
        return ZPTrackerScreen.Screen_iOS_Me_Profile
    }
    
    func setupRightBarButton() {
        let button = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 26, height: 26))
        button.titleLabel?.textColor = UIColor.white
        button.titleLabel?.font = UIFont.iconFont(withSize: 26)
        button.setIconFont("personal_settingaccount", for: UIControlState.normal)
        button.rx.tap.bind { [weak self] in
            let settings: ZPSettingViewController = ZPSettingRouter.assemblyModule()
            self?.navigationController?.pushViewController(settings, animated: true)
        }.disposed(by: disposeBag)
        
        let negativeSeparator  = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSeparator.width = -12
        
        let rightBar = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItems = [negativeSeparator, rightBar]
    }
    
    func createTableView() {
        self.tableView = UITableView.init(frame: CGRect.zero, style: UITableViewStyle.grouped)
        self.view.addSubview(self.tableView)
        self.tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.backgroundColor = UIColor.defaultBackground()
        self.tableView.separatorColor = UIColor.line()
        let cellNib = UINib(nibName: "ZPProfileUserInfoCell", bundle: nil)
        self.tableView.register(cellNib, forCellReuseIdentifier: "ZPProfileUserInfoCell")
    }
    
    func observeNotification() {
        NotificationCenter.default.rx
            .notification(Notification.Name.Reload_Profile_)
            .takeUntil(self.rx.deallocated).bind(onNext: { [weak self](_) in
                self?.reloadDataSource()
            })
            .disposed(by: disposeBag)
        NotificationCenter.default.rx.notification(Notification.Name.Reload_Profile_, object: nil).takeUntil(self.rx.deallocated).bind(onNext: { [weak self](_) in
                self?.reloadDataSource()
            }).disposed(by: disposeBag)
    }
    
    //MARK: - HeaderClick
    func profileHeaderClick() {
        let accountName = ZPProfileManager.shareInstance.userLoginData?.accountName ?? ""
        if accountName.count > 0 {
            return
        }
        self.updateAccountName()
    }
    
    func updateAccountName() {
        let vc = UpdateAcountNameViewController()
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK: - UITableView
extension ZPMainProfileViewController: UITableViewDataSource, UITableViewDelegate {
    func itemAtIndexPath(indexPath: IndexPath) -> AnyObject? {
        let array = self.dataSource[indexPath.section]
        return array[indexPath.row]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let array = self.dataSource[section]
        return array.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath == IndexPath(item: 0, section: 0) {
            return 80
        }
        return CGFloat(ZPUserInfomationTableViewCell.cellHeight())
    }
    
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return self.footers[section].height;
//    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 0.1 : 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let item = self.itemAtIndexPath(indexPath: indexPath) else {
            return UITableViewCell()
        }
        if let userModel = item as? ZPUserItemModel {
            let cell = ZPUserInfomationTableViewCell.zp_cellFromSameNib(for: tableView)
            cell.setItem(userModel)
            cell.selectionStyle = userModel.hasValue == true ? UITableViewCellSelectionStyle.none : UITableViewCellSelectionStyle.gray
            return cell
        }
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ZPProfileUserInfoCell", for: indexPath) as? ZPProfileUserInfoCell {
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.arrow.isHidden = true
            
            let currentUser = ZPProfileManager.shareInstance.currentZaloUser
            let name = currentUser?.displayName ?? ""
            let path = currentUser?.avatar ?? ""
            
            // User account is verified (level 3) => checked icon next to username
            let iconFont = UILabel.iconCode(withName: "verify_success1") ?? ""
            cell.lblUserName.font = UIFont.zaloPay(withSize: 18)
            let attributedText = NSMutableAttributedString(string: name + " ")
            attributedText.append(NSAttributedString(string: iconFont, attributes: [NSAttributedStringKey.foregroundColor: UIColor.zaloBase(), NSAttributedStringKey.font: UIFont.iconFont(withSize: 16)]))
            cell.lblUserName.attributedText = attributedText
            
            let url = URL.init(string: path)
            cell.avatarImgView.sd_setImage(with: url, placeholderImage: UIImage.defaultAvatar())
            
            var acccountPhone = ZPProfileManager.shareInstance.userLoginData?.phoneNumber.formatPhoneNumber() ?? ""
            acccountPhone = acccountPhone.count > 0 ? acccountPhone : R.string_LeftMenu_ZaloPayIdEmpty()
            cell.lblUserZaloId.text = acccountPhone
            DDLogInfo("\(name) \(path) \(acccountPhone)")
            return cell
        }
        fatalError("Error create ZPProfileUserInfoCell")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let item = self.itemAtIndexPath(indexPath: indexPath) as? ZPUserItemModel,
            let level = ZPProfileManager.shareInstance.userLoginData?.profilelevel else {
                return
        }

        if item.type == ZPUserItemModelTypeIdentifier || item.type == ZPUserItemModelTypeEmail {
            if level < ZPProfileLevel.level3.rawValue {
                self.updateProfileLevel3()
                return
            }
        }
        
        if item.type == ZPUserItemModelTypeAcountName && item.hasValue.not {
            self.updateAccountName()
            return
        }
        
        if item.type == ZPUserItemModelTypeLocation {
            ZPProfileUpdateLocationViewController.show(on: self).subscribe(onNext: {[weak self] (x) in
                DDLogInfo("\(x) new location");
                ZPProfileManager.shareInstance.address = x
                self?.reloadDataSource()
            }, onError: { (error) in
                DDLogInfo("\(error.userInfoData()) new location");
            }).disposed(by: disposeBag)
            return
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        let model = self.footers[section]
//        if model.isNeedLabel.not {
//            return nil
//        }
//        let footer = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: model.height))
//        let label = UILabel.init(frame: CGRect.init(x: 10, y: 0, width: UIScreen.main.bounds.size.width - 20, height: model.height))
//        label.autoresizingMask = [.flexibleLeftMargin, .flexibleTopMargin, .flexibleWidth]
//        label.numberOfLines = Int(round(model.height / model.font.lineHeight))
//        label.textAlignment = NSTextAlignment.left
//        label.textColor = UIColor.subText()
//        label.font = model.font
//        label.text = model.content
//        footer.addSubview(label)
//        return footer
//    }
    
    //MARK: Reset Pin
    func alertSuccess() {
        if let dialog = ZPDialogView(type: DialogTypeSuccess, message: R.string_ChangePIN_Message_Success(), items: nil) {
            dialog.show()
        }
    }
    
    func alertUpdateProfileLevel3() {
        ZPDialogView.showDialog(with: DialogTypeConfirm,
                                title: nil,
                                message: R.string_UpdateProfileLevel2_SuccessMessage(),
                                buttonTitles: [R.string_ButtonLabel_OK(), R.string_ButtonLabel_Cancel()])
        { [weak self](idx) in
            guard idx == 1 else {
                return
            }
            self?.updateProfileLevel3()
        }
    }
    
    func updateProfileLevel3() {
        let cacheDataTable = ZPCacheDataTable(db: PerUserDataBase.sharedInstance())
        let isDoneUploead = ((cacheDataTable?.cacheDataValue(forKey: kDisableUpdateProfileLevel3) ?? "") as NSString).boolValue
        if isDoneUploead {
            self.alertDoneUploadProfile()
            return
        }
        self.startUpdateProfileLevel3()
    }
    
    func startUpdateProfileLevel3() {
        let viewController = UpdateProfileMainViewController.init(cancel: {}, error: {
            self.navigationController?.popToViewController(self, animated: true)
        }) {
            self.reloadDataSource()
            self.navigationController?.popToViewController(self, animated: true)
        }
        guard let vc = viewController else {
            return
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func alertDoneUploadProfile() {
        ZPDialogView.showDialog(with: DialogTypeConfirm,
                                title: nil,
                                message: R.string_UpdateProfileLevel3_UploadSuccess_Message(),
                                buttonTitles: [R.string_ButtonLabel_Close()], handler: nil)
    }
}

//MARK: - UpdateAcountName Delegate
extension ZPMainProfileViewController: UpdateAcountNameViewControllerDelegate {
    func updateAccountNameCompleteSuccess(_ controller: UpdateAcountNameViewController) {
        self.reloadDataSource()
    }
}
