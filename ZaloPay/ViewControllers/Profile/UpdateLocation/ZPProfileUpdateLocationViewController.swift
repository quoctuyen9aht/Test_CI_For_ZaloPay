//
//  ZPProfileUpdateLocationViewController.swift
//  ZaloPay
//
//  Created by Dung Vu on 11/13/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa
import ZaloPayAnalyticsSwift
import ZaloPayProfile

fileprivate let kOffsetItem = 10
@objcMembers
class ZPProfileUpdateLocationViewController: BaseViewController {
    fileprivate lazy var presenter = ZPProfileUpdateLocationPresenter(with: self)
    private(set) var textInputView: ZPFloatTextInputView!
    private(set) var buttonNext: UIButton!
    let resultEvent = PublishSubject<String>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDisplay()
        presenter.setupEvent()
    }
    
    override func screenName() -> String! {
        return ZPTrackerScreen.Screen_iOS_Me_Profile_Address
    }
    
    private func setupDisplay() {
        self.title = R.string_UpdateProfileLevel2_Title()
        // First Label
        let lblTitle = UILabel(frame: .zero).then {
            $0.numberOfLines = 0
            $0.text = R.string_MainProfile_Update_Address_Note()
            $0.font = .sfuiTextRegular(withSize: 15)
            $0.textColor = .subText()
        }
        self.view.addSubview(lblTitle)
        // Text Field
        let textField = ZPFloatTextInputView().then {
            $0.backgroundColor = .white
            $0.textField.placeholder = R.string_MainProfile_Update_Address_Title()
            $0.textField.autocorrectionType = .no
        }
        self.view.addSubview(textField)
        self.textInputView = textField
        self.textInputView.textField.text = ZPProfileManager.shareInstance.address
        
        // Term of use html text
        let htmlLabel: MDHTMLLabel = MDHTMLLabel()
        htmlLabel.delegate = self
        htmlLabel.htmlText = R.string_UpdateProfileLevel3_TermOfUse()
        htmlLabel.numberOfLines = 0
        htmlLabel.font = UIFont.sfuiTextRegular(withSize: 15)
        
        let labelSize = htmlLabel.sizeThatFits(CGSize(width: UIScreen.main.bounds.width - 40, height: 0))
        
        self.view.addSubview(htmlLabel)
        
        // Button
        let buttonNext = UIButton(type: .custom).then {
            $0.setupZaloPay()
            $0.setTitle(R.string_ButtonLabel_Upgrade(), for: .normal)
        }
        self.view.addSubview(buttonNext)
        self.buttonNext = buttonNext
        // Layout
        lblTitle.snp.makeConstraints {
            $0.top.equalTo(kOffsetItem)
            $0.centerX.equalToSuperview()
            $0.width.equalTo(self.view.snp.width).offset(-kOffsetItem * 2)
            $0.height.greaterThanOrEqualTo(0)
        }
        
        textField.snp.makeConstraints {
            $0.top.equalTo(lblTitle.snp.bottom).offset(kOffsetItem)
            $0.centerX.equalToSuperview()
            $0.width.equalToSuperview()
        }
        
        htmlLabel.snp.makeConstraints {
            $0.top.equalTo(textField.snp.bottom).offset(kOffsetItem)
            $0.centerX.equalToSuperview()
            $0.width.equalTo(lblTitle.snp.width)
            $0.height.equalTo(labelSize.height)
        }

        buttonNext.snp.makeConstraints {
            $0.top.equalTo(htmlLabel.snp.bottom).offset(kOffsetItem)
            $0.centerX.equalToSuperview()
            $0.width.equalTo(lblTitle.snp.width)
            $0.height.equalTo(48)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.textInputView.textField.becomeFirstResponder()        
        ZPTrackingHelper.shared().trackScreen(with: self)
    }
    
    override func backButtonClicked(_ sender: Any!) {
        viewWillSwipeBack()
    }
    
    override func viewWillSwipeBack() {
        let e = NSError(domain: NSURLErrorDomain, code: NSURLErrorCancelled, userInfo: [NSLocalizedDescriptionKey: "Cancel"])
        self.resultEvent.onError(e)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard self.textInputView.textField.isFirstResponder else {
            return
        }
        self.textInputView.textField.resignFirstResponder()
    }
}

extension ZPProfileUpdateLocationViewController: MDHTMLLabelDelegate {
    func htmlLabel(_ label: MDHTMLLabel?, didSelectLinkWith URL: URL?) {
        if (URL?.absoluteString == "termsOfUse") {
            self.presenter.showTerm()
        }
    }
}

extension ZPProfileUpdateLocationViewController {
    static func show(on vc: UIViewController?) -> Observable<String> {
        guard let vc = vc else {
            return Observable.empty()
        }
        
        return Observable.create({ [weak vc] (s) -> Disposable in
            let locationVC = ZPProfileUpdateLocationViewController()
            _ = locationVC.resultEvent.subscribe(s)
            vc?.navigationController?.pushViewController(locationVC, animated: true)
            return Disposables.create {
                locationVC.navigationController?.popViewController(animated: true)
            }
        })
    }
    
    // Using for Object C
    static func showObjC(on vc: UIViewController?) -> RACSignal<NSString> {
        return self.show(on: vc).map({ $0 as NSString })~
    }
}
