//
//  ZPProfileUpdateLocationInteractor.swift
//  ZaloPay
//
//  Created by Dung Vu on 11/13/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct ZPProfileUpdateLocationInteractor {
    func updateLocation(using text:String) -> Observable<Result<String>> {
        return Observable
            .eventLoading(with: NetworkManager.sharedInstance().updateLocationProfile(using: text))
            .map({ .success($0) })
            .catchError({ Observable.just(.fail($0)) })
    }
}

extension NetworkManager {
    func updateLocationProfile(using text: String) -> Observable<String> {
        guard NetworkState.sharedInstance().isReachable else {
            let e = NSError(domain: NSURLErrorDomain, code: NSURLErrorNotConnectedToInternet, userInfo: ["returnmessage": R.string_NetworkError_NoConnectionMessage()])
            return Observable.error(e)
        }
        var params: JSON = [:]
        params["updateinfo"] = "1"
        let jsonLocation = ["1": text]
        guard let jsonData = try? JSONSerialization.data(withJSONObject: jsonLocation, options: []) else {
            return Observable.empty()
        }
        let jsonString = String(data: jsonData, encoding: .utf8)
        params["updatedata"] = jsonString
        let signal = NetworkManager.sharedInstance().creatRequest(withPath: api_um_updateuserinfo4client, params: params, isGet: false)
        return (~signal).map({ _ in text })
    }
}
