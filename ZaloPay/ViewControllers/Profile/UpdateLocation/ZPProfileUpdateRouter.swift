//
//  ZPProfileUpdateRouter.swift
//  ZaloPay
//
//  Created by Dung Vu on 11/13/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import CocoaLumberjackSwift

struct ZPProfileUpdateRouter {
    private weak var presenter: ZPProfileUpdateLocationPresenter?
    init(with presenter: ZPProfileUpdateLocationPresenter?) {
        self.presenter = presenter
    }
    
    func success(with newLocation: String) {
        let e = self.presenter?.controller?.resultEvent
        e?.onNext(newLocation)
        e?.onCompleted()
        
    }
    
    func handler(from e: Error) {
        DDLogInfo(e.localizedDescription)
        ZPDialogView.showDialogWithError(e, handle: nil)
    }
    
    func showTerm() {
        let params = ["view": "termsOfUse"]
        guard let termVC = ZPDownloadingViewController(properties: params) else {
            return
        }
        termVC.moduleName = "About"
        self.presenter?.controller?.navigationController?.pushViewController(termVC, animated: true)
    }
}
