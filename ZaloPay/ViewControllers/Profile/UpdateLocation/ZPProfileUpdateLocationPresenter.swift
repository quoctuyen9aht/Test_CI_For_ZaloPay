//
//  ZPProfileUpdateLocationPresenter.swift
//  ZaloPay
//
//  Created by Dung Vu on 11/13/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

fileprivate let kMaxLocationCharacter = 160
class ZPProfileUpdateLocationPresenter: NSObject {
    weak var controller: ZPProfileUpdateLocationViewController?
    private let disposeBag = DisposeBag()
    private let currentInput = Variable<String>("")
    private lazy var interactor = ZPProfileUpdateLocationInteractor()
    private lazy var router = ZPProfileUpdateRouter(with: self)
    
    convenience init(with controller: ZPProfileUpdateLocationViewController?) {
        self.init()
        self.controller = controller
    }
    
    public func setupEvent() {
        guard let controller = self.controller else { return }
        currentInput.asDriver()
            .map({ $0.count > 0 })
            .distinctUntilChanged()
            .drive(controller.buttonNext.rx.isEnabled).disposed(by: disposeBag)
        let textField = controller.textInputView.textField
        textField?.delegate = self
        textField?.rx.text.map({ $0 ?? ""}).bind(to: currentInput).disposed(by: disposeBag)
        textField?.rx.controlEvent(.editingChanged).bind { [weak controller] in
            controller?.textInputView.clearErrorText()
        }.disposed(by: disposeBag)
        controller.buttonNext.rx.tap.bind { [weak self] in
            self?.updateLocation()
        }.disposed(by: disposeBag)
    }
    
    public func showTerm() {
        self.router.showTerm()
    }
    
    private func updateLocation() {
        self.controller?.textInputView.textField.resignFirstResponder()
        self.interactor.updateLocation(using: currentInput.value).bind { [weak self] in
            switch $0 {
            case .success(let value):
                self?.router.success(with: value)
            case .fail(let error):
                self?.router.handler(from: error)
            }
        }.disposed(by: disposeBag)
    }
}

extension ZPProfileUpdateLocationPresenter: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let currentText = textField.text else {
            return true
        }
        let newText = (currentText as NSString).replacingCharacters(in: range, with: string)
        let next = (newText.count > kMaxLocationCharacter).not
        if next.not {
            self.controller?.textInputView.showError(withText: R.string_MainProfile_Update_Address_Error_Maximum())
        }
        return next
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
}
