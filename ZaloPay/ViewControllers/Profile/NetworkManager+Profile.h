//
//  NetworkManager+Profile.h
//  ZaloPay
//
//  Created by PhucPv on 6/3/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <ZaloPayNetwork/NetworkManager.h>

@interface NetworkManager(Profile)
- (RACSignal *)lockUserForClient;
- (RACSignal *)loginWithPhone:(NSString *)phoneNumber
                             pin:(NSString *)pin;
- (RACSignal *)registerWithPhone:(NSString *)phoneNumber
                             pin:(NSString *)pin;
- (RACSignal *)authenPhoneWithOTP:(NSString *)otp;
- (RACSignal *)authenPhoneV2WithOTP:(NSString *)otp;
- (RACSignal *)authenLoginByPhone:(NSString *)phoneNumber andOTP:(NSString *)otp;
- (RACSignal *)updateProfileWithPhoneNumber:(NSString *)phoneNumber
                                        pin:(NSString *)pin;
- (RACSignal *)updateProfile2WithPhoneNumber:(NSString *)phoneNumber
                                         pin:(NSString *)pin
                                 accountName:(NSString *)accountName;
- (RACSignal *)verifyProfileWithOtp:(NSString *)otp;

- (RACSignal *)recoveryPinWithNewPin:(NSString *)pin oldPin:(NSString *)oldPin;
- (RACSignal *)recoveryPinWithOtp:(NSString *)otp;
- (RACSignal *)requestUpdateProfileWithEmail:(NSString *)email
                                  identifier:(NSString *)identifier
                        frontIdentifierPhoto:(UIImage *)frontPhoto
                      backsideIdentiferPhoto:(UIImage *)backsidePhoto
                                      avatar:(UIImage *)avatar
                                imageQuality:(float)quality;

// Update all field - KYC verify via photo
- (RACSignal *)requestUpdateProfileWithFullName:(NSString *)fullName
                                         gender:(NSInteger)gender
                                     dayOfBirth:(NSDate *)dob
                                 identifierType:(NSInteger)idType
                                identifierValue:(NSString *)idValue
                        frontIdentifierPhoto:(UIImage *)frontPhoto
                      backsideIdentiferPhoto:(UIImage *)backsidePhoto
                                      avatar:(UIImage *)avatar
                                imageQuality:(float)quality;


- (RACSignal *)checkStatusUpdateProfileLevel3;

- (RACSignal *)getUserInfoByZaloPayId:(int64_t)zaloPayId;
- (RACSignal *)getUserInfoByPhone:(NSString *)phoneNumber;
- (RACSignal *)getCurrenUserProfile;
- (RACSignal *)registerUserInfo:(NSString *)zalophone
                         gender:(NSNumber *)gender
                       fullName:(NSString *)fullName
                            dob:(NSDate *)dob
                         idType:(NSNumber *)idType
                        idValue:(NSString *)idValue;
- (RACSignal *)registerPin:(NSString *)pin;
@end
