//
//  GuideAccountNameView.h
//  ZaloPay
//
//  Created by PhucPv on 9/20/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (GuideAccountName)
+ (UIView *)guideAccountNameWithTextColor:(UIColor *)textColor
                                     font:(UIFont *)font;
@end
