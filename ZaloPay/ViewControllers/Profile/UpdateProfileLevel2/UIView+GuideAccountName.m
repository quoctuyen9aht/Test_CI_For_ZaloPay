//
//  GuideAccountNameView.m
//  ZaloPay
//
//  Created by PhucPv on 9/20/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "UIView+GuideAccountName.h"

@implementation UIView(GuideAccountName)
+ (UIView *)guideAccountNameWithTextColor:(UIColor *)textColor
                                     font:(UIFont *)font{
    UIView *background = [[UIView alloc] init];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 12, 300, 20)];
    label.text = @"ZaloPay ID bao gồm:";
    label.backgroundColor = [UIColor clearColor];
    label.font = font;
    label.textColor = textColor;
    [background addSubview:label];
    
    NSArray *arrayString = @[@"Độ dài từ 4-24 ký tự (gồm chữ và số)",
                             @"Không phân biệt chữ hoa, thường",
                             @"Chỉ được đăng kí một lần duy nhất"];
    float left = 6;
    float h = 20;
    
    UIImage *image = [UIImage filledImageFrom:[UIImage imageNamed:@"profile_check_mark"] withColor:textColor];
    UIView *lastView = label;
    for (NSString *oneString in arrayString) {
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        [imageView setContentMode:UIViewContentModeCenter];
        [background addSubview:imageView];
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(lastView.mas_bottom);
            make.width.equalTo(h);
            make.height.equalTo(h);
            make.left.equalTo(left);
        }];
        UILabel *_label = [[UILabel alloc] init];
        _label.backgroundColor = [UIColor clearColor];
        _label.font = font;
        _label.textColor = textColor;
        _label.text = oneString;
        [background addSubview:_label];
        
        [_label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(imageView.mas_right).with.offset(4);
            make.top.equalTo(lastView.mas_bottom);
            make.right.equalTo(0);
            make.height.equalTo(h);
        }];
        lastView = _label;
    }
    [background mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(lastView.mas_bottom);
    }];
    return background;
}
@end
