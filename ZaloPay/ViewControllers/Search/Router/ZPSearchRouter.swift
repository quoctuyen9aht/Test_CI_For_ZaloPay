//
//  ZPSearchRouter.swift
//  ZaloPay
//
//  Created by tridm2 on 9/18/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import CocoaLumberjackSwift

@objc class ZPSearchRouter: NSObject, ZPSearchRouterProtocol {
    weak var viewController: BaseViewController?
    lazy var disclaimer = ZPDisclaimerModel()
    private let disposeBag = DisposeBag()
    
    func showItem(_ item: ZPSearchEntity, from viewController: ZPSearchViewProtocol) {
        if let dataModel = item.dataModel,
            let vc = viewController as? BaseViewController {
            self.viewController = vc
            self.openFeatureItem(with: dataModel)
        }
    }
    
    private static let FunctionIdMapping: [FunctionId:RouterId] = [
        .ZPWithdraw:    .withdraw,
        .ZPMainProfile: .userProfile,
        .ZPWallet:      .wallet,
        .ZPSetting:     .setting,
        .QRPayScanner:  .qrScan,
        .ZPReceive:     .receiveMoney,
        .ZPTransfer:    .transferMoney,
        .LinkCard:      .linkCard,
        .ZPRechargeMain:.recharge
    ]
    
    private func openFeatureItem(with data: ZPFeatureDataModel) {
        if let trackId = data.trackId, trackId.rawValue > 0 {
            ZPTrackingHelper.shared().trackEvent(trackId)
        }
        
        if let funcId = data.functionId, let routerId = ZPSearchRouter.FunctionIdMapping[funcId] {
            ZPCenterRouter.launch(routerId: routerId, from: viewController)
            return
        }
        
        let vcClass: AnyClass = getClass(with: data.functionId)
        if !(vcClass is ZPDownloadingViewController.Type) {
            self.openViewController(with: data, viewControllerClass: vcClass)
            return
        }        
        self.disclaimer.checkAndShowDisclaimer(appId: data.appId) { [weak self] in
            self?.openExternalApp(with: data)
        }
    }
    
    private func openExternalApp(with data: ZPFeatureDataModel) {
        let appType = data.appType ?? ReactAppTypeNative
        ZPCenterRouter.launchExternalApp(appId: data.appId, appType: appType, from: viewController, moduleName: data.moduleName)
    }

    private func openViewController(with data: ZPFeatureDataModel, viewControllerClass: AnyClass) {
        if viewControllerClass == ZPChangePasswordViewController.self {
            self.resetPin()
            return
        }
        guard let viewController = self.viewController else {
            return
        }
        var vc: UIViewController
        let vcClass = viewControllerClass as! UIViewController.Type
        vc = vcClass.init()
        vc.title = data.title
        viewController.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func resetPin() {
        if let searchVC = self.viewController as? ZPSearchViewController {
            searchVC.searchView?.searchTextField.resignFirstResponder()
        }
        
        ZPChangePasswordViewController.showChangePassword(on: self.viewController).subscribe(onNext: { (_) in
            DDLogInfo("Change Pass Successfull")
        }, onError: { (e) in
            let message = (e.userInfoData() as? JSON)?.value(forKey: "returnmessage", defaultValue: "")
            ZPDialogView.showDialog(with: DialogTypeNotification,
                                    message: message,
                                    cancelButtonTitle: R.string_ButtonLabel_Close(),
                                    otherButtonTitle: nil,
                                    completeHandle: nil)
        }).disposed(by: disposeBag)
    }
    
    private func getClass(with id: FunctionId?) -> AnyClass {
        if let id = id {
            switch (id) {
            case .LinkCard:
                return BillDetailViewController.self
            case .ReactModule:
                return ZPDownloadingViewController.self
            case .ZPResetPin:
                return ZPChangePasswordViewController.self
            default:
                return ZPDownloadingViewController.self
            }
        }
        return ZPDownloadingViewController.self
    }
}
