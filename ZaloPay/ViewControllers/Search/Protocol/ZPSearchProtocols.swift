//
//  ZPSearchProtocols.swift
//  ZaloPay
//
//  Created by tridm2 on 9/13/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation

@objc protocol ZPSearchInteractorProtocol: class {
    func search(text: String) -> Array<ZPSearchEntity>
    func promotionDataSource() -> Array<ZPSearchEntity>
    func allFeatures() -> Array<ZPSearchEntity>
}

@objc protocol ZPSearchPresenterProtocol: class {
    var view: ZPSearchViewProtocol? {get set}
    var interactor: ZPSearchInteractorProtocol? {get set}
    var router: ZPSearchRouterProtocol? {get set}
    
    func search(text: String)
    func promotionDataSource() -> Array<ZPSearchEntity>
    func showItem(_ item: ZPSearchEntity)
}

@objc protocol ZPSearchRouterProtocol: class {
    var viewController: BaseViewController? {get set}
    
    func showItem(_ item: ZPSearchEntity, from controller: ZPSearchViewProtocol)
}

@objc protocol ZPSearchViewProtocol: class {
    var presenter: ZPSearchPresenterProtocol? {get set}
    
    func search(text: String, completeWith result: Array<ZPSearchEntity>)
}
