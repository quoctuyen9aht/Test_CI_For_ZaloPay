//
//  ZPSearchPresenter.swift
//  ZaloPay
//
//  Created by tridm2 on 9/18/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

@objc class ZPSearchPresenter: NSObject, ZPSearchPresenterProtocol {
    var view: ZPSearchViewProtocol?
    var interactor: ZPSearchInteractorProtocol?
    var router: ZPSearchRouterProtocol?
    
    func search(text: String) {
        if let interactor = self.interactor {
            let result = interactor.search(text: text)
            self.view?.search(text: text, completeWith: result)
        }
    }
    
    func promotionDataSource() -> Array<ZPSearchEntity> {
        return self.interactor?.promotionDataSource() ?? []
    }
    
    func showItem(_ item: ZPSearchEntity) {
        if let view = self.view {
            self.router?.showItem(item, from: view)
        }
    }
}
