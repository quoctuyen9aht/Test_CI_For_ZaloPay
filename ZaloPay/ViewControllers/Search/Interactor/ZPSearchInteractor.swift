//
//  ZPSearchInteractor.swift
//  ZaloPay
//
//  Created by tridm2 on 9/18/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayConfig

@objc class ZPSearchInteractor: NSObject, ZPSearchInteractorProtocol {
    var resource: Array<ZPSearchEntity>!
    
    override init() {
        super.init()
        self.setupData()
    }
    
    func setupData() {
        if let sortApps: Array<Int> = ReactNativeAppManager.sharedInstance().activeAppIds as? Array<Int> {
            var featureItems = Array<ZPSearchEntity>()
            // featureItems.addObjectsFromArray(self.createMainPaymentScenariosList())
//            let ignoreAppIds = ApplicationState.getArrayConfig(fromDic: "search", andKey: "ignore_apps", andDefault: [reactInternalAppId, withdrawAppId, appVoucherId]) as? Array<Int> ?? Array<Int>()
            let ignoreAppIds = ZPApplicationConfig.getSearchConfig()?.getIgnoreApps() ?? [reactInternalAppId, withdrawAppId, appVoucherId]
            for appId in sortApps {
                if ignoreAppIds.contains(appId) == false {
                    let itemData = self.getFeatureItem(with: appId)
                    featureItems.append(itemData)
                }
            }
            
            let featureFromConfig = self.getFeatureArrayFromConfig()
            featureItems.append(contentsOf: featureFromConfig)
            self.resource = featureItems
        }
    }
    
    func allFeatures() -> Array<ZPSearchEntity> {
        return self.resource;
    }
    
    //- (NSArray *)createMainPaymentScenariosList {
    //    NSMutableArray *services = [NSMutableArray new];
    //    NSArray *serviceItems =
    //    @[
    //      @{
    //          @"title": [R string_Home_TransferMoney],
    //          @"icon": @"app_1_transfers",
    //          @"class": [TransferHomeViewController class],
    //          @"color": [UIColor zp_blue],
    //          @"requireLevel2": @(TRUE)
    //          },
    //      @{
    //          @"title": [R string_Home_ReceiveMoney],
    //          @"icon": @"app_1_receivemoney",
    //          @"class": [ZPReceiveMoneyViewController class],
    //          @"color": [UIColor zp_green]
    //          },
    //      ];
    //
    //    for (NSDictionary* item in serviceItems) {
    //        [services addObject:[self createFeatureItem:item]];
    //    }
    //    return services;
    //}
        
    func getFeatureItem(with appId: Int) -> ZPSearchEntity {
        let app = ReactNativeAppManager.sharedInstance().getApp(appId)
        let moduleName = appId == lixiAppId ? ReactModule_RedPacket : ReactModule_PaymentMain
        return self.homePageItem(with: app, moduleName:moduleName)
    }
    
    func homePageItem(with app: ReactAppModel, moduleName: String) -> ZPSearchEntity {
        let title: String = app.appname.count > 0 ? app.appname : ""
        let iconName: String = app.iconName.count > 0 ? app.iconName : ""
        let color: UIColor =  UIColor(fromHexString: app.color) ?? UIColor.zaloBase()
        let dic: Dictionary<String, Any> = [
            "title": title,
            "moduleName": moduleName,
            "class": ZPDownloadingViewController.self,
            "appid": app.appid,
            "apptype": app.appType,
            "icon": iconName,
            "color": color
        ]
        
        let item = self.createFeatureItem(from: dic)
        return item
    }
    
    func createFeatureItem(from data: Dictionary<String, Any>) -> ZPSearchEntity {
        let itemData = ZPSearchEntity()
        itemData.originTitle = data.value(forKey: "title", defaultValue: "")
        itemData.imageName = data.value(forKey: "icon", defaultValue: "")
        itemData.color = data.value(forKey: "color", defaultValue: UIColor.defaultBackground())
        itemData.dataModel = ZPFeatureDataModel.objectWithExternalDictionnary(data)
        return itemData
    }
    
    func getFeatureArrayFromConfig() -> Array<ZPSearchEntity> {
        var result = Array<ZPSearchEntity>()
//        if let items = ApplicationState.getArrayConfig(fromDic: "search", andKey: "inside_app", andDefault: Array<ZPSearchEntity>()) as? Array<Dictionary<String, Any>> {
        let items = ZPApplicationConfig.getSearchConfig()?.getInsideApp() ?? []
        for item in items {
            let temp = ZPSearchEntity()
//            temp.originTitle = item.value(forKey: "app_name", defaultValue: "")
//            temp.imageName = item.value(forKey: "icon_name", defaultValue: "")
//            temp.color = UIColor(hexString: item.value(forKey: "icon_color", defaultValue: ""))
//            temp.dataModel = ZPFeatureDataModel.objectWithInternalDictionnary(item)
            
            temp.originTitle = item.getAppName()
            temp.imageName = item.getIconName()
            temp.color = UIColor(hexString: item.getIconColor())
            temp.dataModel = ZPFeatureDataModel.objectWithConfig(item)
            
            result.append(temp)
        }
        return result
    }
    
    func promotionDataSource() -> Array<ZPSearchEntity> {
//        let numberSearchApps = ApplicationState.getIntConfig(fromDic: "search", andKey: "number_search_app", andDefault: 3)
        let numberSearchApps = ZPApplicationConfig.getSearchConfig()?.getNumberSearchApp() ?? 3
        if self.resource == nil {
            return []
        }
        if numberSearchApps >= self.resource.count {
            return self.resource
        }
        return Array(self.resource[0..<Int(numberSearchApps)])
    }
    
    // MARK: Search
    
    func search(text: String) -> Array<ZPSearchEntity> {
        if text.count == 0 {
            return Array<ZPSearchEntity>()
        }
        
        let keyword = text.lowercased().ascci()
        if keyword == nil || keyword?.count == 0 {
            return Array<ZPSearchEntity>()
        }
        
        if self.resource == nil {
            return Array<ZPSearchEntity>()
        }
        var result = Array<ZPSearchEntity>()
        for item in self.resource {
            let range = (item.ascciTitle as NSString).range(of: keyword!)
            if range.length > 0 {
                item.title = highlighText(item.originTitle, range: range)
                result.append(item)
            }
            
        }
        return result
    }
    
    func highlighText(_ rootString: String, range: NSRange) -> NSMutableAttributedString {
        let mutableAttributedString = NSMutableAttributedString(string: rootString)
        mutableAttributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.zaloBase(), range: range)
        return mutableAttributedString
    }
}

