//
//  ZPSearchFeatureModel.m
//  ZaloPay
//
//  Created by tienlv on 3/27/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPSearchInteractor.h"
#import "ZPBankApiWrapper.h"
#import "ZALOPAY-Swift.h"
#import "ZPSearchEntity.h"
#import "ApplicationState.h"
#import <ZaloPayCommon/NSString+Asscci.h>
#import <ZaloPayAppManager/PaymentReactNativeApp/ReactAppModel.h>
#import <ZaloPayAppManager/PaymentReactNativeApp/ReactNativeAppManager.h>
#import <ZaloPayAppManager/PaymentReactNativeApp/ReactNativeAppManager+DownloadData.h>
#import <ZaloPayAppManager/ReactModuleViewController.h>
#import <ZaloPayCommon/ZPConstants.h>

@interface ZPSearchInteractor()
@property (nonatomic, strong) NSMutableArray *resource;
@end

@implementation ZPSearchInteractor

- (id)init {
    self = [super init];
    if (self) {
        [self setupData];
    }
    return self;
}

- (void)setupData {
    NSArray *sortApps = [ReactNativeAppManager sharedInstance].activeAppIds;
    NSMutableArray *featureItems = [NSMutableArray array];
//    [featureItems addObjectsFromArray:[self createMainPaymentScenariosList]];
    NSArray *ignoreAppIds = [ApplicationState getArrayConfigFromDic:@"search" andKey:@"ignore_apps" andDefault:@[@(reactInternalAppId),@(withdrawAppId),@(appVoucherId)]];
    
    for (NSNumber *appId in sortApps) {
        if ([ignoreAppIds containsObject:appId] == false) {
            ZPSearchEntity *itemData = [self featureItemWithAppId:[appId integerValue]];
            [featureItems addObject:itemData];
        }
    }
    
    NSArray *featureFromConfig = [self featureArrayFromConfig];
    [featureItems addObjectsFromArrayNotNil:featureFromConfig];
    _resource = featureItems;
}

- (NSArray *)allFeatures {
    return _resource;
}

//- (NSArray *)createMainPaymentScenariosList {
//    NSMutableArray *services = [NSMutableArray new];
//    NSArray *serviceItems =
//    @[
//      @{
//          @"title": [R string_Home_TransferMoney],
//          @"icon": @"app_1_transfers",
//          @"class": [TransferHomeViewController class],
//          @"color": [UIColor zp_blue],
//          @"requireLevel2": @(TRUE)
//          },
//      @{
//          @"title": [R string_Home_ReceiveMoney],
//          @"icon": @"app_1_receivemoney",
//          @"class": [ZPReceiveMoneyViewController class],
//          @"color": [UIColor zp_green]
//          },
//      ];
//    
//    for (NSDictionary* item in serviceItems) {
//        [services addObject:[self createFeatureItem:item]];
//    }
//    return services;
//}

- (ZPSearchEntity *)lixiHomePageItem:(ReactAppModel *)app {
    NSString *title = app.appname.length > 0 ? app.appname : [R string_Home_Lixi];
    NSDictionary *data = @{
                           @"title": title,
                           @"class": [ReactModuleViewController class],
                           @"moduleName": @"RedPacket",
                           @"icon": @"app_6_red",
                           @"color": [UIColor zp_red],
                           @"appid":@(reactInternalAppId),
                           @"requireLevel2": @(TRUE)
                           };
    ZPSearchEntity *item = [self createFeatureItem:data];
    return item;
}

- (ZPSearchEntity *)featureItemWithAppId:(NSInteger)appId {
    ReactAppModel *app = [[ReactNativeAppManager sharedInstance] getApp:appId];
    if (appId == lixiAppId) {
        return [self lixiHomePageItem:app];
    }
    return  [self homePageItemWithApp:app];
}

- (ZPSearchEntity *)homePageItemWithApp:(ReactAppModel *)appModel {
    NSString *title = appModel.appname.length > 0 ? appModel.appname : @"";
    NSString *iconName = appModel.iconName.length > 0 ? appModel.iconName : @"";
    UIColor *color = appModel.color.length > 0 ? [UIColor colorFromHexString:appModel.color] : [UIColor zaloBaseColor];
    NSDictionary *dic = @{@"title":title,
                          @"moduleName":@"PaymentMain",
                          @"class":[ReactModuleViewController class],
                          @"appid":@(appModel.appid),
                          @"apptype":@(appModel.appType),
                          @"icon": iconName,
                          @"color": color
                          };
    ZPSearchEntity *item = [self createFeatureItem:dic];
    return item;
}

- (ZPSearchEntity *)createFeatureItem:(NSDictionary *)data {
    ZPSearchEntity *itemData = [[ZPSearchEntity alloc] init];
    itemData.originTitle = [data objectForKey:@"title"];
    itemData.imageName = [data objectForKey:@"icon"];
    itemData.color = [data objectForKey:@"color"];
    itemData.dataModel = [ZPFeatureDataModel objectWithExternalDictionnary:data];
    return itemData;
}


- (NSArray *)featureArrayFromConfig{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSArray *items = [ApplicationState getArrayConfigFromDic:@"search" andKey:@"inside_app" andDefault:[NSArray new]];
    for (NSDictionary *item in items) {
        ZPSearchEntity *tamp = [[ZPSearchEntity alloc] init];
        tamp.originTitle = [item objectForKey:@"app_name"];
        tamp.imageName =[ item objectForKey:@"icon_name"];
        tamp.color = [UIColor colorFromHexString:[ item stringForKey:@"icon_color"]];
        tamp.dataModel = [ZPFeatureDataModel objectWithInternalDictionnary:item];
        [result addObject:tamp];
    }
    return result;
}

- (NSArray *)promotionDataSource {
    int numberSearchApp  = [ApplicationState getIntConfigFromDic:@"search" andKey:@"number_search_app" andDefault:3];
    return [self.resource subArrayAtIndex:0 withTotalItem: numberSearchApp];
}

#pragma mark search

- (NSArray *)search:(NSString *)keyword {
    if(!keyword || keyword.length == 0) {
        return nil;
    }
    keyword = [[keyword lowercaseString] ascciString];
    if(!keyword || keyword.length == 0) {
        return nil;
    }
    NSMutableArray *result = [[NSMutableArray alloc] init];
    for (ZPSearchEntity *item in self.resource) {
        NSRange range = [item.ascciTitle rangeOfString:keyword];
        if (range.location != NSNotFound) {
            item.title = (NSString *)[self highlighText:item.originTitle keyword:keyword range:range];
            [result addObject:item];
        }
    }
    return result;
}

- (NSMutableAttributedString *)highlighText:(NSString *)rootString keyword:(NSString *)keyword range:(NSRange)range{
    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:rootString];
    [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:[UIColor zaloBaseColor] range:range];
    return mutableAttributedString;
}

@end
