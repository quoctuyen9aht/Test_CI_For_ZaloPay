//
//  ZPFeatureDataModel.swift
//  ZaloPay
//
//  Created by tridm2 on 9/13/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayCommonSwift
import ZaloPayAnalyticsSwift
import ZaloPayConfig

enum FunctionId: Int {
    case ZPWithdraw     = 1,
    ZPRechargeMain,
    LinkCard,
    QRPayScanner,
    SupportCenter       = 6,
    ReactModule,
    ZPMainProfile,
    ZPWallet            = 10,
    ZPResetPin,
    ZPSetting,
    ZPTransfer,
    ZPReceive
}

@objc class ZPFeatureDataModel: NSObject {
    var appId: Int!
    var appType: ReactAppType?
    var moduleName: String?
    var trackId: ZPAnalyticEventAction?
    var title: String!
    var functionId: FunctionId?
    
    class func objectWithInternalDictionnary(_ data: Dictionary<String, Any>) -> ZPFeatureDataModel {
        let itemModel = ZPFeatureDataModel()
        itemModel.appId = ZPConstants.reactInternalAppId
        itemModel.functionId = FunctionId(rawValue: data.value(forKey: "inside_app_id", defaultValue: 1))
        itemModel.moduleName = data.value(forKey: "module_name", defaultValue: "")
        itemModel.trackId = ZPAnalyticEventAction(rawValue: data.int(forKey: "trackId", defaultValue: 0))
        itemModel.title = data.value(forKey: "app_name", defaultValue: "")
        return itemModel
    }
    
    class func objectWithConfig(_ config: ZPInternalAppSearchConfigProtocol) -> ZPFeatureDataModel {
        let itemModel = ZPFeatureDataModel()
        itemModel.appId = ZPConstants.reactInternalAppId
        itemModel.functionId = FunctionId(rawValue: config.getInsideAppId())
//        itemModel.moduleName = ""
//        itemModel.trackId = 0
        itemModel.moduleName = config.getModuleName()
        itemModel.title = config.getAppName()
        return itemModel
    }
    
    class func objectWithExternalDictionnary(_ data: Dictionary<String, Any>) -> ZPFeatureDataModel {
        let itemModel = ZPFeatureDataModel()
        itemModel.appId = data.value(forKey: "appid", defaultValue: 0)
        itemModel.appType = data.value(forKey: "apptype", defaultValue: ReactAppType(rawValue: 1))
        itemModel.moduleName = data.value(forKey: "moduleName", defaultValue: "")
        itemModel.trackId = ZPAnalyticEventAction(rawValue: data.int(forKey: "trackId", defaultValue: 0))
        itemModel.title = data.value(forKey: "title", defaultValue: "")
        return itemModel
    }  
}
