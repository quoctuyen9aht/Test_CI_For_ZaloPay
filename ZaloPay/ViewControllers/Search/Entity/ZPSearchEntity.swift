//
//  ZPSearchEntity.swift
//  ZaloPay
//
//  Created by tridm2 on 9/13/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

@objc class ZPSearchEntity: NSObject {
    var originTitle: String {
        get {
            return (title as! NSMutableAttributedString).string
        }
        set {
            title = NSMutableAttributedString(string: newValue)
            ascciTitle = newValue.count > 0 ?  newValue.ascci().lowercased() : ""
        }
    }
    
    var ascciTitle: String!
    var title: Any!
    var imageName: String!
    var color: UIColor?
    var dataModel: ZPFeatureDataModel?
}
