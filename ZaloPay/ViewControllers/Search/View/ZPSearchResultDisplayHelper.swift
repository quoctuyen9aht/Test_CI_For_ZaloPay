//
//  ZPSearchResultDisplayHelper.swift
//  ZaloPay
//
//  Created by tridm2 on 9/14/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayAnalyticsSwift

class ZPSearchResultDisplayHelper: BaseViewController {
    var viewController: ZPSearchViewController?
    var allResult: Array<ZPSearchEntity>!
    var displayResult: Array<ZPSearchEntity>!
    
    var dataSource: Array<ZPSearchEntity> {
        get {
            return allResult
        }
        set {
            self.allResult = newValue;
            let nResults = newValue.count < 3 ? newValue.count : 3
            self.displayResult =  Array(newValue[0..<nResults])
        }
    }
}

extension ZPSearchResultDisplayHelper: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identify = String(describing: ZPSearchItemCollectionViewCell.self)
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identify, for: indexPath) as? ZPSearchItemCollectionViewCell {
            let itemData = self.displayResult[indexPath.row]
            let offset: Float
            if self.displayResult.count == self.allResult.count {
                offset = indexPath.row < (self.allResult.count - 1) ? 50 : 0
            } else {
                offset = indexPath.row < 2 ? 50 : 0
            }
            let isFirst = !(indexPath.row > 0)
            cell.setItem(itemData, offset: offset, isFirstOffset: isFirst)
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.displayResult.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.size.width, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let itemData = self.displayResult[indexPath.row]
        self.viewController?.presenter?.showItem(itemData)
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.search_touch_result)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if self.displayResult.count == self.allResult.count {
            return CGSize.zero
        }
        return CGSize(width: collectionView.frame.size.width, height: 52)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.size.width, height: 52)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionFooter {
            let footer = ZPSearchCollectionFooterView.footerView(collectionView, at: indexPath)
            footer.readMoreButton.rac_signal(for: .touchUpInside).subscribeNext {
                [weak self] (x) in
                if let wSelf = self {
                    wSelf.displayResult = wSelf.allResult
                    collectionView.reloadData()
                }
            }
            return footer
        }
        
        return ZPSearchCollectionFeatureHeaderView.headerView(collectionView, at: indexPath)
    }
}
