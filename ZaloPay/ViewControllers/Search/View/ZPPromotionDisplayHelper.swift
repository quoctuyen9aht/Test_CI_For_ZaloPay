//
//  ZPPromotionDisplayHelper.swift
//  ZaloPay
//
//  Created by tridm2 on 9/14/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayAnalyticsSwift

class ZPPromotionDisplayHelper: BaseViewController {
    var viewController: ZPSearchViewController?
    var dataSource: Array<ZPSearchEntity>!
}

extension ZPPromotionDisplayHelper: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identify = String(describing: ZPSearchSuggestCollectionViewCell.self)
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identify, for: indexPath) as? ZPSearchSuggestCollectionViewCell {
            let itemData = self.dataSource[indexPath.row]
            cell.setItem(itemData)
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSource.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let winSize = UIScreen.main.bounds.size
        let height: CGFloat = UIView.isScreen2x() ? 115.0 : 128.0
        let width: CGFloat = winSize.width / 3
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let itemData = self.dataSource[indexPath.row]
        self.viewController?.presenter?.showItem(itemData)
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.search_touch_suggest)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.size.width, height: 115)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        return ZPSearchCollectionHeaderView.headerView(collectionView, at: indexPath)
    }
}
