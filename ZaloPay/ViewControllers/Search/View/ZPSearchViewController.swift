//
//  ZPSearchViewController.swift
//  ZaloPay
//
//  Created by tridm2 on 9/14/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit
import ZaloPayCommonSwift
import ZaloPayAnalyticsSwift

enum InternalFeaturetype: Int {
    case InternalFeatureWithDraw        =       1,
    InternalFeatureRecharge,
    InternalFeatureLinkCard,
    InternalFeaturePayQR,
    InternalFeatureTransactionHistory,
    InternalFeatureSupportCenter,
    InternalFeatureInformation,
    InternalFeatureAccountInformation,
    InternalFeatureNotify
}

@objc class ZPSearchViewController: BaseViewController {
    var collectionView: UICollectionView!
    var noResultView: UIView!
    var labelNoResult: UILabel!
    var promotionDisplay: ZPPromotionDisplayHelper?
    var resultDisplay: ZPSearchResultDisplayHelper?
    var searchView: ZPSearchNavigationView?
    var presenter: ZPSearchPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.resultDisplay = ZPSearchResultDisplayHelper()
        self.resultDisplay?.viewController = self
        
        self.promotionDisplay = ZPPromotionDisplayHelper()
        self.promotionDisplay?.viewController = self
        
        self.addSearchBar()
        self.createCollectionView()
        self.handleKeyboardNotify()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ZPTrackingHelper.shared().trackScreen(with: self)
    }
    
    override func screenName() -> String! {
        return ZPTrackerScreen.Screen_iOS_Search
    }
    
    override func backButtonClicked(_ sender: Any) {
        super.backButtonClicked(sender)
    }
    
    override func backEventId() -> Int {
        return ZPAnalyticEventAction.search_touch_back.rawValue
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.searchView?.searchTextField.resignFirstResponder()
    }
    
    // MARK: *** setupPromoteView
    
    func addSearchBar() {
        let navigationSize = self.navigationController?.navigationBar.frame.size
        self.searchView = ZPSearchNavigationView(frame: CGRect(x: 0, y: 0, width: navigationSize!.width * 0.9, height: 33))
        self.searchView?.activeSearch()
        self.navigationItem.titleView = self.searchView
        self.searchView?.delegate = self
        self.searchView?.searchTextField.becomeFirstResponder()
    }
    
    func createCollectionView() {
        let flowLayout = ZPSeparatorCollectionViewFlowLayout()
        flowLayout.prepare()
        flowLayout.strokeWidth = 0
        flowLayout.scrollDirection = .vertical
        
        self.collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: flowLayout)
        self.collectionView.showsVerticalScrollIndicator = false
        
        self.registerCellClass(ZPSearchItemCollectionViewCell.self)
        self.registerCellClass(ZPSearchSuggestCollectionViewCell.self)
        self.view.addSubview(self.collectionView!)
        self.collectionView?.alwaysBounceVertical = true
        
        self.collectionView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top)
            make.bottom.equalTo(self.view.snp.bottom)
            make.left.equalTo(self.view.snp.left)
            make.right.equalTo(self.view.snp.right)
        }
        
        self.collectionView.backgroundColor = UIColor.defaultBackground()
        self.collectionView.register(ZPSearchCollectionFooterView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: ZPSearchCollectionFooterView.identifier)
        
        self.collectionView.register(ZPSearchCollectionHeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: ZPSearchCollectionHeaderView.identifier)
        
        self.collectionView.register(ZPSearchCollectionFeatureHeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: ZPSearchCollectionFeatureHeaderView.identifier)
        
        self.showPromotion()
    }
    
    func registerCellClass(_ Class: AnyClass) {
        self.collectionView.register(Class, forCellWithReuseIdentifier: String(describing: Class) )
    }
    
    // MARK: *** setupResultView
    
    func getNoResultView() -> UIView {
        if self.noResultView == nil {
            self.noResultView = UIView(frame: self.view.frame)
            self.noResultView.backgroundColor = UIColor.defaultBackground()
            
            let imgView = UIImageView(image: UIImage(named: "search_empty"))
            self.noResultView.addSubview(imgView)
            
            imgView.snp.makeConstraints { (make) in
                make.size.equalTo(imgView.image!.size)
                make.top.equalTo(40)
                make.centerX.equalTo(self.noResultView.snp.centerX)
            }
            
            let label = UILabel()
            label.numberOfLines = 0
            label.textAlignment = .center
            self.noResultView.addSubview(label)
            label.snp.makeConstraints { (make) in
                make.left.equalTo(20)
                make.right.equalTo(-20)
                make.top.equalTo(imgView.snp.bottom).offset(25)
                make.height.equalTo(50)
            }
            label.textColor = UIColor.subText()
            label.font.withSize(17)
            self.view.addSubview(self.noResultView)
            self.noResultView.snp.makeConstraints { (make) in
                make.top.equalTo(0)
                make.left.equalTo(0)
                make.right.equalTo(0)
                make.bottom.equalTo(0)
            }
            self.labelNoResult = label
            
            let descriptionLabel = UILabel()
            descriptionLabel.numberOfLines = 0
            descriptionLabel.lineBreakMode = .byWordWrapping
            descriptionLabel.textAlignment = .center
            descriptionLabel.font.withSize(13)
            descriptionLabel.textColor = UIColor.subText()
            descriptionLabel.text = R.string_Search_No_Result_Description()
            self.noResultView.addSubview(descriptionLabel)
            let padding = UIView.isScreen2x() ? 30 : 70
            descriptionLabel.snp.makeConstraints { (make) in
                make.top.equalTo(label.snp.bottom).offset(5)
                make.left.equalTo(padding)
                make.right.equalTo(-padding)
                make.height.equalTo(50)
            }
        }
        
        return self.noResultView
    }
    
    func showNoResultView(with text: String) {
        if self.labelNoResult != nil && text.count > 0 {
            self.labelNoResult.text = String(format: R.string_Search_No_Result(), text)
        }
    }
    
    func handleKeyboardNotify() {
        self.handleKeyboardShow({ [weak self] (keyboardSize, animationDuration) in
                self?.collectionView.snp.updateConstraints { (make) in
                make.bottom.equalTo(-keyboardSize.height)
            }
            }, andHide: { [weak self] (animationDuration) in
                self?.collectionView.snp.updateConstraints { (make) in
                    make.bottom.equalTo(0)
                }
        })
    }
}

// MARK: *** ZPSearchNavigationViewDelegate
extension ZPSearchViewController: ZPSearchNavigationViewDelegate {
    func didEnterKeyword(_ keyword: String) {
        self.presenter?.search(text: keyword)
    }
}

// MARK: *** ZPSearchViewProtocol
extension ZPSearchViewController: ZPSearchViewProtocol {
    func search(text: String, completeWith result: Array<ZPSearchEntity>) {
        if text.count == 0 {
            self.getNoResultView().isHidden = true
            self.showPromotion()
            self.searchView?.searchTextField.becomeFirstResponder()
        } else {
            if self.noResultView != nil {
                self.noResultView.isHidden = true
            }
            self.getNoResultView().isHidden = result.count > 0
            self.showNoResultView(with: text)
            if result.count > 0 {
                self.showSearchResult(result)
            }
        }
    }
    
    func showPromotion() {
        if let presenter = self.presenter {
            self.promotionDisplay?.dataSource = presenter.promotionDataSource()
        }
        self.collectionView.delegate = self.promotionDisplay
        self.collectionView.dataSource = self.promotionDisplay
        self.collectionView.reloadData()
    }
    
    func showSearchResult(_ result: Array<ZPSearchEntity>) {
        self.resultDisplay?.dataSource = result
        self.collectionView.delegate = self.resultDisplay
        self.collectionView.dataSource = self.resultDisplay
        self.collectionView.reloadData()
    }
}
