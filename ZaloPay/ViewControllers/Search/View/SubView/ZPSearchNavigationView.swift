//
//  ZPSearchNavigationView.swift
//  ZaloPay
//
//  Created by tridm2 on 9/13/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit


protocol ZPSearchNavigationViewDelegate: class {
    func didEnterKeyword(_ keyword: String)
}

class ZPSearchNavigationView: UIView {
    var imageView: ZPIconFontImageView!
    var searchTextField: UITextField!
    var clearView: UIView!
    weak var delegate: ZPSearchNavigationViewDelegate?
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: self.frame.width, height: self.frame.height)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addAllSubView()
        self.setupLayout()
        self.respondKeyword()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func addAllSubView() {
        self.searchTextField = UITextField()
        self.searchTextField.font?.withSize(13)
        self.searchTextField.placeholder = R.string_Search_Field_PlaceHolder()
        self.searchTextField.setValue(UIColor.hex_0xB3E0FB(), forKeyPath: "_placeholderLabel.textColor")
        self.searchTextField.delegate = self
        self.addSubview(self.searchTextField)
        
        self.imageView = ZPIconFontImageView(frame: CGRect(x: 0, y: 0, width: 17, height: 17))
        self.imageView.setIconFont("general_search")
        self.imageView.setIconColor(UIColor.hex_0xB3E0FB())
        self.addSubview(self.imageView)
        
        let clearTape = UITapGestureRecognizer(target: self, action: #selector(onClearText(_:)))
        clearTape.numberOfTapsRequired = 1
        self.clearView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        self.addSubview(self.clearView)
        self.clearView.addGestureRecognizer(clearTape)
        self.clearView.isUserInteractionEnabled = true
        self.backgroundColor = UIColor.hex_0X10A5FF()
        self.layer.cornerRadius = 3
    }
    
    @objc func onClearText(_ ges: UIGestureRecognizer) {
        if ges.state == .ended {
            self.searchTextField.text = ""
            self.search(with: self.searchTextField.text!)
        }
    }
    
    func setupLayout() {
        self.imageView.snp.makeConstraints { (make) in
            make.size.equalTo(self.imageView.frame.size)
            make.centerY.equalTo(self.snp.centerY)
            make.right.equalTo(self.snp.right).offset(-15)
        }
        
        self.clearView.snp.makeConstraints { (make) in
            make.size.equalTo(self.clearView.frame.size)
            make.centerY.equalTo(self.snp.centerY)
            make.right.equalTo(self.snp.right)
        }
        
        self.searchTextField.snp.makeConstraints { (make) in
            //make.width.equalTo(self.snp.width).offset(-50)
            make.right.equalTo(self.imageView.snp.left)
            make.centerY.equalTo(self.snp.centerY)
            make.left.equalTo(self.snp.left).offset(15)
            make.height.equalTo(self.snp.height).offset(-10)
        }
    }
    
    func respondKeyword() {
        // [[[[self.searchTextField.rac_textSignal skip:2] distinctUntilChanged] throttle:0.3] subscribeNext:^(NSString* text)
        self.searchTextField.rac_textSignal().skip(2).distinctUntilChanged().subscribeNext {
            [weak self] (val) in
            if let text = val as String? {
                self?.search(with: text)
            }
        }
    }
    
    func search(with text: String) {
        let iconName: String = text.count > 0 ? "general_del" : "general_search"
        self.imageView.setIconFont(iconName)
        self.delegate?.didEnterKeyword(text)
    }
    
    func activeSearch() {
        self.backgroundColor = UIColor.white
        self.imageView.setIconColor(UIColor.placeHolder())
        self.searchTextField.setValue(UIColor.placeHolder(), forKeyPath: "_placeholderLabel.textColor")
        self.searchTextField.textColor = UIColor.defaultText()
        self.searchTextField.tintColor = UIColor.zp_blue()
    }
}

extension ZPSearchNavigationView: UITextFieldDelegate {
    
}
