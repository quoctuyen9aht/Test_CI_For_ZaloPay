//
//  ZPSearchCollectionFooterView.swift
//  ZaloPay
//
//  Created by tridm2 on 9/13/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayCommonSwift

class ZPSearchCollectionFooterView: UICollectionReusableView {
    var readMoreButton: UIButton!
    
    class func footerView(_ collectionView: UICollectionView, at indexPath: IndexPath) -> ZPSearchCollectionFooterView {
        if let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: ZPSearchCollectionFooterView.identifier, for: indexPath) as? ZPSearchCollectionFooterView {
            return footerView
        }
        
        return ZPSearchCollectionFooterView(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
    }
    
    static var identifier: String {
        return String(describing: ZPSearchCollectionFooterView.self)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupView() {
        readMoreButton = UIButton(type: .custom)
        let fontSize: CGFloat = UIScreen.main.bounds.size.height <= 568 ? 15.0 : 17.0
        readMoreButton.titleLabel?.font.withSize(fontSize)
        readMoreButton.setTitle(R.string_Search_Read_All_Result(), for: .normal)
        readMoreButton.setTitleColor(UIColor.hex_0x727f8c(), for: .normal)
        readMoreButton.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: 52)
        self.addSubview(readMoreButton)
        
        let line = ZPLineView()
        self.addSubview(line)
        line.alignToBottom()
        self.backgroundColor = UIColor.white
    }
}
