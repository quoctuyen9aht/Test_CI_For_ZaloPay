//
//  ZPSearchCollectionHeaderView.swift
//  ZaloPay
//
//  Created by tridm2 on 9/13/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit
import ZaloPayCommonSwift

class ZPSearchCollectionHeaderView: UICollectionReusableView {
    class func headerView(_ collectionView: UICollectionView, at indexPath: IndexPath) -> ZPSearchCollectionHeaderView {
        if let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: ZPSearchCollectionHeaderView.identifier, for: indexPath) as? ZPSearchCollectionHeaderView {
            return headerView
        }
        return ZPSearchCollectionHeaderView(frame: CGRect(x: 0, y: 0, width: collectionView.frame.size.width, height: 115))
    }
    
    static var identifier: String {
        return String(describing: ZPSearchCollectionHeaderView.self)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.defaultBackground()
        self.addDeclareLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func addDeclareLabel() {
        let background = UIView()
        self.addSubview(background)
        background.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.top.equalTo(0)
            make.height.equalTo(72)
        }
        background.backgroundColor = UIColor.white
        
        let declareLineView = ZPLineView()
        background.addSubview(declareLineView)
        declareLineView.alignToBottom()
        
        let declareLabel = UILabel()
        declareLabel.text = R.string_Search_Suggest_Find_Keyword()
        declareLabel.textColor = UIColor.subText()
        declareLabel.font.withSize(15)
        background.addSubview(declareLabel)
        declareLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(background.snp.centerX)
            make.centerY.equalTo(background.snp.centerY)
        }
        
        let promoteLabel = UILabel()
        promoteLabel.text = R.string_Search_Suggest_App_Title()
        promoteLabel.textColor = UIColor.subText()
        promoteLabel.font.withSize(13)
        self.addSubview(promoteLabel)
        promoteLabel.snp.makeConstraints { (make) in
            make.top.equalTo(background.snp.bottom).offset(18)
            make.left.equalTo(12)
        }
        
        let promoteLineView = ZPLineView()
        self.addSubview(promoteLineView)
        promoteLineView.alignToBottom()
    }
}
