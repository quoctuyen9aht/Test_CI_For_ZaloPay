//
//  ZPSearchItemCollectionViewCell.swift
//  ZaloPay
//
//  Created by tridm2 on 9/13/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit
import ZaloPayCommonSwift

class ZPSearchItemCollectionViewCell: UICollectionViewCell {
    var myImageView: ZPIconFontImageView!
    var label: UILabel!
    var lineView: ZPLineView!
    var labelSize: CGSize {
//        if UIView.isScreen2x() {
//            return CGSize(width: 85, height: 40)
//        }
        return CGSize(width: 90, height: 40)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupView() {
        let contentView = self.contentView
        let sizeImage = 30
        self.myImageView = ZPIconFontImageView(frame: CGRect(x: 0, y: 0, width: sizeImage, height: sizeImage))
        contentView.addSubview(self.myImageView)
        
        self.label = UILabel()
        contentView.addSubview(self.label)
        self.label.textAlignment = .center
        self.label.numberOfLines = 0
        self.label.textColor = UIColor.defaultText()
        let fontSize: CGFloat = UIView.isScreen2x() ? 15.0 : 16.0
        self.label.font.withSize(fontSize)
        self.label.contentMode = .top
        self.myImageView.contentMode = .scaleToFill
        
        self.myImageView.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.contentView.snp.centerX)
            make.centerY.equalTo(self.contentView.snp.centerY).offset(-15)
            make.size.equalTo(self.myImageView.frame.size)
        }
        
         //    int top = [UIView isScreen2x] ? 8 : 12
        self.label.snp.makeConstraints { (make) in
            make.top.equalTo(self.myImageView.snp.bottom).offset(15)
            make.centerX.equalTo(self.myImageView.snp.centerX)
            make.size.equalTo(self.labelSize)
        }
        
        self.backgroundColor = UIColor.white
        contentView.backgroundColor = UIColor.white
    }
    
    func setItem(_ item: ZPSearchEntity, offset: Float, isFirstOffset: Bool) {
        if let title = item.title as? NSMutableAttributedString {
            self.label.attributedText = title
        } else {
            self.label.text = item.title as? String
        }
        
        self.addLineView()
        self.lineView.alignToBottomWith(leftMargin: Int(offset))
        
        if UILabel.iconInfo(withName: item.imageName).count > 0 {
            self.myImageView.setIconFont(item.imageName)
            self.myImageView.setIconColor(item.color)
        } else {
            self.myImageView.setIconFont("general_icondefault")
            self.myImageView.setIconColor(UIColor.defaultAppIcon())
        }
        
        if isFirstOffset {
            let top = ZPLineView()
            top.tag = 1
            self.addSubview(top)
            top.alignToTop()
        }
        
        self.myImageView.snp.remakeConstraints { (make) in
            make.left.equalTo(10)
            make.centerY.equalTo(self.contentView.snp.centerY)
            make.size.equalTo(self.myImageView.frame.size)
        }
        
        self.label.snp.remakeConstraints { (make) in
            make.left.equalTo(self.myImageView.snp.right).offset(10)
            make.centerY.equalTo(self.contentView.snp.centerY)
        }
        
        self.setNeedsLayout()
    }
    
    func addLineView() {
        self.lineView = ZPLineView()
        self.addSubview(lineView)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.lineView.removeFromSuperview()
        let removeView = self.viewWithTag(1)
        removeView?.removeFromSuperview()
    }
}

class ZPSearchSuggestCollectionViewCell: ZPSearchItemCollectionViewCell {
    func setItem(_ item: ZPSearchEntity) {
        self.label.text = item.originTitle;
        let size: CGSize = self.label.sizeThatFits(labelSize)
        self.label.snp.updateConstraints { (make) in
            make.height.equalTo(size.height)
        }
        if UILabel.iconInfo(withName: item.imageName).count > 0 {
            self.myImageView.setIconFont(item.imageName)
            self.myImageView.setIconColor(item.color!)
        } else {
            self.myImageView.setIconFont("general_icondefault")
            self.myImageView.setIconColor(UIColor.defaultAppIcon())
        }
        
        self.addLineView()
        self.lineView.alignToBottom()
    }
}
