//
//  ZPSearchCollectionFeatureHeaderView.swift
//  ZaloPay
//
//  Created by tridm2 on 9/13/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit

class ZPSearchCollectionFeatureHeaderView: UICollectionReusableView {
    class func headerView(_ collectionView: UICollectionView, at indexPath: IndexPath) -> ZPSearchCollectionFeatureHeaderView {
        if let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: ZPSearchCollectionFeatureHeaderView.identifier, for: indexPath) as? ZPSearchCollectionFeatureHeaderView {
            return headerView;
        }
        return ZPSearchCollectionFeatureHeaderView(frame: CGRect(x: 0, y: 0, width: collectionView.frame.size.width, height: 52))
    }
    
    static var identifier: String {
        return String(describing: ZPSearchCollectionFeatureHeaderView.self)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.defaultBackground()
        self.addAppLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func addAppLabel() {
        let appLabel = UILabel()
        self.addSubview(appLabel)
        appLabel.snp.makeConstraints { (make) in
            make.left.equalTo(12);
            make.top.equalTo(21);
            make.width.equalTo(100);
            make.height.equalTo(30);
        }
        appLabel.text = R.string_Search_Result_App_Tille()
        appLabel.textColor = UIColor.subText()
        appLabel.font.withSize(13)
    }
}
