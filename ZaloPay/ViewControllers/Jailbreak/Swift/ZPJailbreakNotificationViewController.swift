//
//  ZPJailbreakNotificationViewController.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 10/5/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

typealias ZPJailBreakHandle = (_ isCheck: Bool?, _ isContinue: Bool?) -> Void

class ZPJailbreakNotificationViewController: UIViewController {
    
    
    var handle : ZPJailBreakHandle = { isCheck, isContinue in}
    var contentView: ZPJailbreakView? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = R.string_Jailbreak_Warning_Title()
        self.view.backgroundColor = UIColor.white
        let tableView = UITableView()
        view.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.bottom.equalTo(0)
            make.left.equalTo(0)
            make.right.equalTo(0)
        }
        self.contentView = ZPJailbreakView()
        tableView.tableHeaderView = self.contentView
        self.contentView?.buttonCheckBox?.addTarget(self, action: #selector(checkboxButtonClick), for: .touchUpInside)
        self.contentView?.buttonContinue?.addTarget(self, action: #selector(continueButtonClick), for: .touchUpInside)
        self.contentView?.buttonClose?.addTarget(self, action: #selector(cancelButtonClick), for: .touchUpInside)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.defaultNavigationBarStyle()
    }
    
    @objc func checkboxButtonClick(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        sender.layer.borderColor = sender.isSelected == true ? UIColor.zaloBase().cgColor : UIColor.gray.cgColor
    }
    
    @objc func cancelButtonClick(_ sender: UIButton) {
        if let contentView = self.contentView {
            if let buttonCheckBox = contentView.buttonCheckBox {
                self.handle((buttonCheckBox.isSelected), false)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @objc func continueButtonClick(_ sender: UIButton) {
        if let contentView = self.contentView {
            if let buttonCheckBox = contentView.buttonCheckBox {
                self.handle((buttonCheckBox.isSelected), true)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
}
