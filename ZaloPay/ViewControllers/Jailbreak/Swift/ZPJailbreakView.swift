//
//  ZPJailbreakView.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 10/5/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

class ZPJailbreakView: UIView {
    
    var buttonCheckBox: UIButton?
    var buttonClose: UIButton?
    var buttonContinue: UIButton?
    
    init() {
        var h: Float = Float(max(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height))
        h = max(568, h) - 64
        super.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: CGFloat(h)))
        createContentView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func fontSize() -> Float {
        return UIView.isScreen2x() ? 13 : 15
    }
    
    func createContentView() {
        
        let imageView = UIImageView(image: UIImage(named: "jailbreak"))
        self.addSubview(imageView)
        
        let titleFontSize = UIView.isScreen2x() ? 15 : 16
        var font = UIFont.sfuiTextSemibold(withSize: CGFloat(titleFontSize))
        
        let title : UILabel
        title = labelWithText(text: R.string_JailBreak_JailbreakDevice(), font: font!)
        self.addSubview(title)
        font = UIFont.sfuiTextRegular(withSize: CGFloat(fontSize()))
        
        let label1 : UILabel
        label1 = labelWithText(text: R.string_JailBreak_WarningMessage1(), font: font!)
        self.addSubview(label1)
        
        let label2 : UILabel
        label2 = labelWithText(text: R.string_JailBreak_WarningMessage2(), font: font!)
        self.addSubview(label2)
        
        let checkBox : UIView? = addCheckButton()
        let continueButton : UIButton? = addContinueButton()
        let closeButton: UIButton? = addCloseButton()
        buttonClose = closeButton
        buttonContinue = continueButton
        
        let paddingLeft = 10
        var paddingTop = UIView.isScreen2x() ? 8 : 15
        var titleLabelPaddingTop = UIView.isScreen2x() ? 17 : 35
        var titleLabelPaddingBottom = UIView.isScreen2x() ? 12 : 20
        var imagePaddingTop = UIView.isScreen2x() ? 8 : 25
        let h : Float = Float(max(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height))
        
        if h == 667.0 {
            paddingTop = 12
            titleLabelPaddingTop = 30
            titleLabelPaddingBottom = 20
            imagePaddingTop = 20
        }
        
        imageView.snp.makeConstraints { (make) in
            make.top.equalTo(imagePaddingTop)
            make.size.equalTo((imageView.image?.size)!)
            make.centerX.equalTo(self.snp.centerX)
        }
        
        title.snp.makeConstraints { (make) in
            make.top.equalTo(imageView.snp.bottom).offset(titleLabelPaddingTop)
            make.left.equalTo(paddingLeft)
            make.right.equalTo(-paddingLeft)
            make.height.equalTo(title.frame.size.height)
        }
        
        label1.snp.makeConstraints { (make) in
            make.top.equalTo(title.snp.bottom).offset(titleLabelPaddingBottom)
            make.left.equalTo(paddingLeft)
            make.right.equalTo(-paddingLeft)
            make.height.equalTo(label1.frame.size.height)
        }
        
        label2.snp.makeConstraints { (make) in
            make.top.equalTo(label1.snp.bottom).offset(paddingTop)
            make.left.equalTo(paddingLeft)
            make.right.equalTo(-paddingLeft)
            make.height.equalTo(label2.frame.size.height)
        }
        
        checkBox?.snp.makeConstraints({ (make) in
            
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(30)
            make.top.equalTo(label2.snp.bottom).offset(paddingTop)
        })
        
        let buttonHeight : Float = UIView.isScreen2x() ? Float(50) : Float(kZaloButtonHeight)
        let buttonPadding : Float = Float(UIView.isScreen2x() ? paddingTop : 8)
        
        continueButton?.snp.makeConstraints({ (make) in
            make.left.equalTo(paddingLeft);
            make.right.equalTo(-paddingLeft);
            make.height.equalTo(buttonHeight);
            make.top.equalTo((checkBox?.snp.bottom)!).offset(paddingTop)
        })
        
        closeButton?.snp.makeConstraints({ (make) in
            make.left.equalTo(paddingLeft);
            make.right.equalTo(-paddingLeft);
            make.height.equalTo(buttonHeight);
            make.top.equalTo((continueButton?.snp.bottom)!).offset(buttonPadding)
        })
    }
    
    func addContinueButton() -> UIButton {
        
        let button = UIButton(type: .custom)
        self.addSubview(button)
        button.setBackgroundColor(UIColor.white, for: .normal)
        button.setTitle(R.string_ButtonLabel_Next(), for: .normal)
        button.setTitleColor(UIColor.zaloBase(), for: .normal)
        button.roundRect(5)
        button.layer.borderColor = UIColor.zaloBase().cgColor
        button.layer.borderWidth = 1.0
        return button
    }
    
    
    func addCloseButton() -> UIButton {
        
        let button = UIButton(type: .custom)
        self.addSubview(button)
        button.setBackgroundColor(UIColor.zaloBase(), for: .normal)
        button.setTitle(R.string_ButtonLabel_Close(), for: .normal)
        button.roundRect(5)
        return button
    }
    
    func addCheckButton() -> UIView {
        
        let checkBoxView = UIView()
        self.addSubview(checkBoxView)
        let button = UIButton(type: .custom)
        checkBoxView.addSubview(button)
        
        let label = UILabel()
        label.text = R.string_JailBreak_DoNotAskAgain()
        label.textColor = UIColor.defaultText()
        label.font = UIFont.sfuiTextRegular(withSize: CGFloat(fontSize()))
        checkBoxView.addSubview(label)
        
        button.snp.makeConstraints { (make) in
            make.left.equalTo(10);
            make.width.equalTo(22);
            make.height.equalTo(22);
            make.centerY.equalTo(checkBoxView.snp.centerY);
        }
        
        label.snp.makeConstraints { (make) in
            make.left.equalTo(button.snp.right).offset(10)
            make.right.equalTo(0)
            make.top.equalTo(0)
            make.bottom.equalTo(0)
        }
        
        button.setImage(UIImage(named: "withdraw_check") , for: .selected)
        button.setBackgroundColor(UIColor.zaloBase(), for: .selected)
        button.setBackgroundColor(UIColor.white, for: .normal)
        button.roundRect(2)
        button.layer.borderWidth = 1.0
        button.layer.borderColor = UIColor.zaloBase().cgColor
        button.isSelected = true
        self.buttonCheckBox = button
        return checkBoxView
    }
    
    func labelWithText(text: String, font: UIFont) -> UILabel {
        
        let label = UILabel()
        label.text = text
        label.textColor = UIColor.defaultText()
        label.font = font
        label.frame = CGRect(x: 0, y: 0, width: self.frame.size.width - 20, height: 1000)
        label.numberOfLines = 0
        label.sizeToFit()
        return label
    }
    
    
}
