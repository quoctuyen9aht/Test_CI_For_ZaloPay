//
//  LoginManager+JailbreakIphone.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 10/5/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayCommonSwift

public extension LoginManagerSwift {
    
    func showJailbreakWarning() {
        let controller = ZPJailbreakNotificationViewController()
        controller.handle = {(isCheck, isContinue) in
            if isCheck! {
                UserDefaults.standard.set(false, forKey: "iphone_jail_break_key")
                UserDefaults.standard.synchronize()
            }
            if !isContinue! {
                exit(0)
            }
            self.start()
        }
        
        let navi = UINavigationController.init(rootViewController: controller)
        appDelegate().window?.rootViewController = navi
    }
    
    func shouldShowJailbrokenWarning() -> Bool {
        let jailBreakKey = "iphone_jail_break_key"
        var jailBreakValue:Bool? = UserDefaults.standard.object(forKey: jailBreakKey) as? Bool
        if (jailBreakValue == nil) {
            jailBreakValue = UIDevice.current.isJailBroken()
            UserDefaults.standard.set(jailBreakValue, forKey: jailBreakKey)
            UserDefaults.standard.synchronize()
        }
        return jailBreakValue!
    }
}
