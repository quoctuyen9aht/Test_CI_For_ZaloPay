//
//  TransferAppInfo.swift
//  ZaloPay
//
//  Created by CPU11680 on 1/30/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayCommonSwift
import CocoaLumberjackSwift
import ZaloPayConfig

@objc public enum MoneyTransferProgress : Int32 {
    case scan = 1
    case success = 2
    case fail = 3
    case cancel = 4
    case finish = 5
}

@objcMembers
public class TransferAppInfo: NSObject{
    
    class func embededData(withDisplayName displayName: String, avatar: String, moneyTransferProgress progress: MoneyTransferProgress, amount: Int64, transId: String?) -> String {
        return NSString.embededDataQRTransfer(withDisplayName: displayName, avatar: avatar, progress: Int32(progress.rawValue), amount: amount, transId: transId)
    }
    
    class func validMessage(_ message: String, textInput textInputView: ZPFloatTextInputView) -> Bool {
        let result = self.validMessage(message)
        if !result {
//            let transferMaxMessageLength = ApplicationState.getIntConfig(fromDic: "transfer", andKey: "max_length_message", andDefault: Int(TRANSFER_MESSAGE_LENGTH))
            let transferMaxMessageLength = ZPApplicationConfig.getTransferConfig()?.getMaxLengthMessage() ?? Int(TRANSFER_MESSAGE_LENGTH)
            let errorMessage = String(format: R.string_InputNode_Error_Message(), transferMaxMessageLength)
            textInputView.showError(withText: errorMessage)
        }
        else {
            textInputView.clearErrorText()
        }
        return result
    }
    
    class func isLessThan(_ totalValue: Int64, textInput textInputView: ZPFloatTextInputView, isFocusedTextInput focused: Bool) -> Bool {
        let result = self.isLessThan(totalValue)
        if result {
            let config = ZPWalletManagerSwift.sharedInstance.config
            let min = NSNumber(value: config.minTransfer) // minTransfer not nil
            let money = min.formatCurrency()
            let message = String(format: R.string_InputMoney_SmallAmount_Error_Message(), money!) // money not nil
            textInputView.showError(withText: message)
            if focused {
                textInputView.textField.becomeFirstResponder()
            }
        }
        return result
    }
    
    class func isGreaterThanMax(_ totalValue: Int64, textInput textInputView: ZPFloatTextInputView, isFocusedTextInput focused: Bool) -> Bool {
        let result = self.isGreaterThanMax(totalValue)
        if result {
            let config = ZPWalletManagerSwift.sharedInstance.config
            let max = NSNumber(value: config.maxTransfer) // maxTransfer not nil
            let money = max.formatCurrency()
            let message = String(format: R.string_InputMoney_BigAmount_Error_Message(), money!) // money not nil
            textInputView.showErrorAuto(message)
            if focused {
                textInputView.textField.becomeFirstResponder()
            }
        }
        return result
    }
    
    @discardableResult
    class func isLessThan(_ totalValue: Int64, textInput textInputView: ZPFloatTextInputView) -> Bool {
        return self.isLessThan(totalValue, textInput: textInputView, isFocusedTextInput: true)
    }
    
    @discardableResult
    class func isGreaterThanMax(_ totalValue: Int64, textInput textInputView: ZPFloatTextInputView) -> Bool {
        return self.isGreaterThanMax(totalValue, textInput: textInputView, isFocusedTextInput: true)
    }
    
    class func validMessage(_ message: String) -> Bool {
//        let transferMaxMessageLength = ApplicationState.getIntConfig(fromDic: "transfer", andKey: "max_length_message", andDefault: Int(TRANSFER_MESSAGE_LENGTH))
        let transferMaxMessageLength = ZPApplicationConfig.getTransferConfig()?.getMaxLengthMessage() ?? Int(TRANSFER_MESSAGE_LENGTH)
        return (message.count) <= transferMaxMessageLength
    }
    
    class func isLessThan(_ totalValue: Int64) -> Bool {
        let min = ZPWalletManagerSwift.sharedInstance.config.minTransfer
        return Int(totalValue) < min
    }
    
    class func isGreaterThanMax(_ totalValue: Int64) -> Bool {
        let max = ZPWalletManagerSwift.sharedInstance.config.maxTransfer
        return Int(totalValue) > max
    }
    
    class func getWarningMessage(_ totalValue: Int64) -> String {
        let config = ZPWalletManagerSwift.sharedInstance.config
        if self.isGreaterThanMax(totalValue) {
            let max = NSNumber(value: config.maxTransfer)
            let money = max.formatCurrency()
            return String(format: R.string_InputMoney_BigAmount_Error_Message(), money!) // money not nil because maxTransfer not nil
        }
        else if self.isLessThan(totalValue) {
            let min = NSNumber(value: config.minTransfer)
            let money = min.formatCurrency()
            return String(format: R.string_InputMoney_SmallAmount_Error_Message(), money!) // money not nil because minTransfer not nil
        }
        return ""
    }
}

extension ZPQRTransferAppMessage {
    func parse(fromEmbedData embedData: String) {
        let data = embedData.data(using: .utf8) as NSData? // not certain
        guard let json = data?.jsonValue() as? [String: Any] else {
            return
        }
        self.progress = json.int32(forKey: "mt_progress")
        self.senderDisplayName = json["displayname"] as? String
        self.senderAvatar = json["avatar"] as? String
        self.amount = json.int64(forKey: "amount")
        self.transId = json.string(forKey: "transid")
    }
}
