//
//  UIPasteboard+Transfer.m
//  ZaloPay
//
//  Created by bonnpv on 8/29/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "UIPasteboard+Transfer.h"
#import <ZaloPayCommon/NSCollection+JSON.h>

#define kPasteboardName     @"RecentTransferPasteboard"

@implementation UIPasteboard (Transfer)

+ (UIPasteboard *)recentTransferPasteboard {
    return [self pasteboardWithName:kPasteboardName create:YES];
}

- (void)setTransferData:(NSDictionary *)transferData {
    NSString *string = @"";
    if (transferData) {
        string = [transferData JSONRepresentation];
    }
    self.string = string;
}

- (NSDictionary *)transferData {
    NSDictionary *recentDic = nil;
    @try {
        recentDic = [self.string JSONValue];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    return recentDic;
}

@end
