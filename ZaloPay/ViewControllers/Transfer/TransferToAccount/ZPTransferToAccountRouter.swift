//
//  ZPTransferToAccountRouter.swift
//  ZaloPay
//
//  Created by tienlv on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit

@objcMembers
class ZPTransferToAccountRouter: NSObject, ZPTransferToAccountRouterProtocol {
    func openViewController(viewController: UIViewController) {
        guard let navi = ZPAppFactory.sharedInstance().rootNavigation() else {
            return
        }
        navi.pushViewController(viewController, animated: true)
    }
    
    func openTransferReceiveController(transferData user: UserTransferData) {
        let viewController = TransferReceiverRouter.assembleModule(transferData: user)
        self.openViewController(viewController: viewController)
    }
    
    class func assembleModule() {
        let viewController = TransferToAccountViewController()
        let presenter = ZPTransferToAccountPresenter()
        let router = ZPTransferToAccountRouter()
        let interactor = ZPTransferToAccountInteractor()
        
        viewController.presenter = presenter
        interactor.presenter = presenter
        presenter.view = viewController
        presenter.router = router
        presenter.interactor = interactor
        
        router.openViewController(viewController: viewController)
    }
}
