//
//  ZPTransferToAccountProtocol.swift
//  ZaloPay
//
//  Created by tienlv on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import Foundation
import UIKit


// TODO: These tasks've been moved to ZPC. This TransferToAccount should be cleaned.
protocol ZPTransferToAccountViewProtocol: class {
    var presenter: ZPTransferToAccountPresenterProtocol? {get set}
    
    func showHub()
    func hideHub()
    func showError(withText content: String,isFirstResponse: Bool)
}

protocol ZPTransferToAccountPresenterProtocol: class {
    
    var view: ZPTransferToAccountViewProtocol? {get set}
    var interactor: ZPTransferToAccountInteractorProtocol? {get set}
    var router: ZPTransferToAccountRouterProtocol? {get set}
    
    func requestUserInfoWithName(accountName: String)
}

protocol ZPTransferToAccountRouterProtocol: class {
    func openTransferReceiveController(transferData user: UserTransferData)
}

protocol ZPTransferToAccountInteractorProtocol: class {
    var presenter: ZPTransferToAccountInteractorCallBackProtocol? {get set}
    func fetchData(accountName: String)
}

protocol ZPTransferToAccountInteractorCallBackProtocol: class {
    func loadSucceed(user: UserTransferData)
    func loadFailed(message: String)
    func showError(withText content: String,isFirstResponse: Bool)
}
