//
//  ZPTransferToAccountPresenter.swift
//  ZaloPay
//
//  Created by tienlv on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit

class ZPTransferToAccountPresenter: ZPTransferToAccountPresenterProtocol {
    var view: ZPTransferToAccountViewProtocol?
    var interactor: ZPTransferToAccountInteractorProtocol?
    var router: ZPTransferToAccountRouterProtocol?
    
    func requestUserInfoWithName(accountName: String) {
        self.view?.showHub()
        self.interactor?.fetchData(accountName: accountName)
    }

}

extension ZPTransferToAccountPresenter: ZPTransferToAccountInteractorCallBackProtocol {
    func loadSucceed(user: UserTransferData) {
        //DDLogInfo(@"get user info data :%@",dic);
        self.view?.hideHub()
        self.router?.openTransferReceiveController(transferData:user)
    }
    func loadFailed(message: String) {
        //DDLogInfo(@"get user info error :%@",error);
        self.view?.hideHub()
        self.view?.showError(withText: message, isFirstResponse: true)
    }
     func showError(withText content: String,isFirstResponse: Bool) {
        self.view?.showError(withText: content,isFirstResponse: isFirstResponse)
    }
}
