//
//  TransferToAccountViewController.swift
//  ZaloPay
//
//  Created by tienlv on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit
import ZaloPayAnalyticsSwift

class TransferToAccountViewController: BaseViewController, ZPTransferToAccountViewProtocol, UITextFieldDelegate {

    var presenter: ZPTransferToAccountPresenterProtocol?
    
    var accountNameInput: ZPFloatTextInputView!
    lazy var bottomButton: UIButton! = {
        let buttomButton = UIButton(type: .custom).build({
            self.view.addSubview($0)
            $0.setTitle(R.string_ButtonLabel_Next(), for: .normal)
            $0.setupZaloPay()
            $0.addTarget(self, action: #selector(continueButtonClicked), for: .touchUpInside)
            $0.snp.makeConstraints { (make) in
                make.left.equalTo(10);
                make.right.equalTo(-10);
                make.height.equalTo(kZaloButtonHeight);
                make.top.equalTo(self.accountNameInput.snp.bottom).offset(kZaloButtonToScrollOffset);
            }
            $0.zp_roundRect(3)
        })
        return buttomButton
    }()
    override func backButtonClicked(_ sender: Any!) {
        super.backButtonClicked(sender)
    }
    
    override func backEventId() -> Int {
        return ZPAnalyticEventAction.moneytransfer_zpid_touch_back.rawValue
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        super.title = R.string_TransferHome_Title()
        self.addAccountNameTextField()
        bottomButton.isEnabled = false
        ZPTrackingHelper.shared().trackEvent(.moneytransfer_touch_zpid)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ZPTrackingHelper.shared().trackScreen(with: self)
    }
    
    override func screenName() -> String! {
        return ZPTrackerScreen.Screen_iOS_MoneyTransfer_InputZPID
    }
    
    func addAccountNameTextField() {
        self.accountNameInput = ZPFloatTextInputView()
        self.view.addSubview(accountNameInput)
        self.accountNameInput.backgroundColor = .white
        self.accountNameInput.textField.placeholder = R.string_TransferToAccount_TextPlaceHolder()
        self.accountNameInput.snp.makeConstraints { (make) in
            make.top.equalTo(0);
            make.left.equalTo(0);
            make.right.equalTo(0);
        }
        
        self.accountNameInput.textField.delegate = self
        self.accountNameInput.textField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        self.accountNameInput.textField.keyboardType = .asciiCapable
        self.accountNameInput.textField.autocapitalizationType = .none
        self.accountNameInput.textField.autocorrectionType = .no
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.accountNameInput.textField.becomeFirstResponder();
    }

    @objc func textFieldDidChange(textField: UITextField) {
        bottomButton.isEnabled = textField.text!.count >= Int(minAccountNameLength)
        if textField.text?.count == 0 {
            accountNameInput.clearErrorText()
        } else {
            textField.text = textField.text!.ascci().lowercased()
        }
    }
    
    @objc func continueButtonClicked(_ sender: Any!) {
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.moneytransfer_zpdi_touch_continue)
        let accountName = self.accountNameInput.textField.text
        self.accountNameInput.textField.resignFirstResponder()
        self.accountNameInput.clearErrorText()
        self.presenter?.requestUserInfoWithName(accountName: accountName!)
    }
    
    func showHub() {
        ZPAppFactory.sharedInstance().showHUDAdded(to: self.view)
    }
    
    func hideHub() {
        ZPAppFactory.sharedInstance().hideAllHUDs(for: self.view)
    }
    
    func showError(withText content: String,isFirstResponse: Bool) {
        self.accountNameInput.showError(withText:content)
        if(isFirstResponse) {
            self.accountNameInput.textField.becomeFirstResponder()
        }
    }
    
    func textField(_ textField: UITextField,
                            shouldChangeCharactersIn range: NSRange,
                            replacementString string: String) -> Bool {
        //DDLogInfo(@"string = %@",string)
        let length = (textField.text?.count)! + (string.count )
        if length > Int(maxAccountNameLength) {
            self.showError(withText: R.string_Account_Name_Length_Message(),isFirstResponse: false)
            return false
        }
        if (string == " ") {
            if length != 1 {
                self.showError(withText: R.string_Account_Name_Space_Message(),isFirstResponse: false)
            }
            return false
        }
        self.accountNameInput.clearErrorText()
        return true
    }
}


