//
//  ZPTransferToAccountInteractor.swift
//  ZaloPay
//
//  Created by tienlv on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift

class ZPTransferToAccountInteractor: ZPTransferToAccountInteractorProtocol {

    func handleUserInfoData(dic: NSDictionary, accountName: String) ->UserTransferData {
        
        let nDict = dic as? [String: Any] ?? [:]
        
        let phone = nDict.value(forKey: "phonenumber", defaultValue: Int64(0))
        let displayName = nDict.value(forKey: "displayname", defaultValue: "")
        let zaloPayId = nDict.value(forKey: "userid", defaultValue: "")
        let avatar = nDict.value(forKey: "avatar", defaultValue: "")
        
        let user = UserTransferData()
        user.displayName = displayName;
        user.paymentUserId = zaloPayId
        user.phone = String(phone)
        user.avatar = avatar
        user.accountName = accountName
        user.mode = .toZaloPayID
        user.source = .fromTransferActivity
        return user
    }
    
    var presenter: ZPTransferToAccountInteractorCallBackProtocol?
    func fetchData(accountName: String) {
        NetworkManager.sharedInstance().getUserInfo(withAcountName: accountName).deliverOnMainThread().subscribeNext({ [weak self](result) in
            let dic = result as! NSDictionary
            let zaloPayId = dic["userid"] as? String
            if(zaloPayId == NetworkManager.sharedInstance().paymentUserId) {
                self?.presenter?.loadFailed(message: R.string_TransferToAccount_CurrentUserAccountMessage())
                return;
            }
            
            self?.presenter?.loadSucceed(user:(self?.handleUserInfoData(dic: dic, accountName: accountName))!)
            }, error:({[weak self] (error) in
                let err = error! as NSError
                self?.presenter?.loadFailed(message:err.apiErrorMessage())
            }))
    }

}
