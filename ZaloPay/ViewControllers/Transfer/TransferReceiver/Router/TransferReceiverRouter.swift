//
//  TransferReceiverRouter.swift
//  ZaloPay
//
//  Created by nhatnt on 09/11/2017.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

@objcMembers
class TransferReceiverRouter: NSObject{
    func openViewController(viewController: UIViewController) {
        guard let navi = ZPAppFactory.sharedInstance().rootNavigation() else {
            return
        }
        navi.pushViewController(viewController, animated: true)
    }
    
    class func assembleModule(transferData: UserTransferData, orderSource: OrderSource = OrderSource_None) -> TransferReceiverViewControllerSwift {
        let viewController = TransferReceiverViewControllerSwift.init(transferData: transferData)
        let presenter = TransferReceiverPresenter()
        let interactor = TransferReceiverInteractor()
        
        viewController.presenter = presenter
        interactor.output = presenter
        interactor.orderSource = orderSource
        presenter.view = viewController
        presenter.interactor = interactor
        
        return viewController
    }
}
