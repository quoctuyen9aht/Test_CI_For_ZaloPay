//
//  ZPTranferReceiverProtocol.swift
//  ZaloPay
//
//  Created by nhatnt on 03/11/2017.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation

protocol TransferReceiverPresenterInterface: class {
    var view: TransferReceiverViewInterface? {get set}
    var interactor: TransferReceiverInteractorInput? {get set}
    
    func requestZaloPayIdIfNeed(userTransferData: UserTransferData)
    func requestCreateOrder(totalValue: Int64, message: String, userTransferData: UserTransferData)
    func sendNotifcationWhenQRTransfer(progress: MoneyTransferProgress, transId: String, userTransferData: UserTransferData)
    func validateMessageLength(_ message: String) ->Bool
    func payBillDidCompleteSuccess(data: UserTransferData)
}

// PRESENTER -> INTERACTOR
protocol TransferReceiverInteractorInput: class {
    var output: TransferReceiverInteractorOutput? {get set}
    func validateMessageLength(_ message: String) ->Bool
    func fetchData(accountName: String)
    func requestUserProfile(userTransferData: UserTransferData)
    func requestCreateOrder(totalValue: Int64, message: String, userTransferData: UserTransferData)
    func sendNotifcationWhenQRTransfer(progress: MoneyTransferProgress, transId: String, userTransferData: UserTransferData)
    func saveRecentTransferFriend(data: UserTransferData)
}

// INTERACTOR -> PRESENTER
protocol TransferReceiverInteractorOutput: class {
    func loadSucceed(user: UserTransferData)
    func loadFailed(message: String)

    func showLoadingView()
    func hideLoadingView()
    
    func showUserData(user: UserTransferData)
    func showBillViewController(billInfo: ZPBill)
    func setTextFieldResignFirstResponder()
    func failCreateOrder(error: Error?)
    func failVerifyOrderMessage(error: Error?)
    func invalidOrderMessage(errorMessage: String, modified: String)
}

protocol TransferReceiverViewInterface: class {
    var presenter: TransferReceiverPresenterInterface? {get set}
    
    func showHub()
    func hideHub()
    func showLoadingView()
    func hideLoadingView()
    func showError(withText content: String)
    
    func setUserData(user: UserTransferData)
    func showUserData(user: UserTransferData)
    func showBill(billInfo: ZPBill)

    func setTextFieldResignFirstResponder()
    func createOrderFailed(error: Error?)
    func verifyOrderMessageFailed(error: Error?)
    func invalidOrderMessage(errorMessage: String, modified: String)
}
