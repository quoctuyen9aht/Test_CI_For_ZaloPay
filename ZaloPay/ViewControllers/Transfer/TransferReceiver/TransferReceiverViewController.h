//
//  TransferReceiverViewController.h
//  ZaloPay
//
//  Created by PhucPv on 4/19/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "BillHandleBaseViewController.h"
#import "UserTransferData.h"

@interface TransferReceiverViewController : BillHandleBaseViewController
@property (nonatomic, assign) OrderSource orderSource;

- (id)initWithTransferData:(UserTransferData *)user;
- (void)reloadWithUser:(UserTransferData *)user;

@end

@interface ZPTransferReceiverWrapper : TransferReceiverViewController
@property (nonatomic, copy) dispatch_block_t preHandle;
@property (nonatomic, copy) dispatch_block_t cancelHandle;
@property (nonatomic, copy) dispatch_block_t errorHandle;
@property (nonatomic, copy) dispatch_block_t completeHandle;

- (id)initWithTransferData:(UserTransferData *)userTransferData
                 preHandle:(dispatch_block_t)preHandle
              cancelHandel:(dispatch_block_t)cancel
               errorHandle:(dispatch_block_t)error
            completeHandle:(dispatch_block_t)completion;

- (void)setPreHandle:(dispatch_block_t)preHandle
        cancelHandel:(dispatch_block_t)cancel
         errorHandle:(dispatch_block_t)error
      completeHandle:(dispatch_block_t)complete;

@end

@interface ZPTransferReceiverWebAppWrapper: ZPTransferReceiverWrapper

@end
