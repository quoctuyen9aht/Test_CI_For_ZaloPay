//
//  NetworkManager+Transfer.m
//  ZaloPay
//
//  Created by PhucPv on 8/30/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "NetworkManager+Transfer.h"
#import <ZaloPayNetwork/ZPNetWorkApi.h>
@implementation NetworkManager(Transfer)
- (RACSignal *)sendNotificationWithReceiverId:(NSString *)receiverId
                                  embededData:(NSString *)embededData{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObjectCheckNil:receiverId forKey:@"receiverid"];
    [param setObjectCheckNil:embededData forKey:@"embededdata"];
    return [self requestWithPath:api_um_sendnotification parameters:param];

}
@end
