//
//  TransferRecieveHeader.h
//  ZaloPay
//
//  Created by bonnpv on 6/10/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransferRecieveHeader : UIView
@property (nonatomic, weak) IBOutlet UIImageView *avatar;
@property (nonatomic, weak) IBOutlet UILabel *labelName;
@property (nonatomic, weak) IBOutlet UILabel *labelPhone;
@end
