//
//  TransferReceiverViewController.m
//  ZaloPay
//
//  Created by PhucPv on 4/19/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "TransferReceiverViewController.h"
#import <ZaloPayCommon/NSArray+ConstrainsLayout.h>
#import <ZaloPayCommon/UITextField+Extension.h>
#import <ZaloPayCommon/NSString+Decimal.h>
#import <ZaloPayCommon/BaseViewController+Keyboard.h>
#import "PMNumberPad.h"
#import <ZaloSDKCoreKit/ZaloSDKCoreKit.h>
#import <ZaloPayCommon/ZPAppFactory.h>
#import <ZaloPayWalletSDK/ZaloPayWalletSDKPayment.h>
#import <ZaloPayCommon/ZPFloatTextInputView.h>
#import <ZaloPayCommon/BaseViewController+ZPScrollViewController.h>
#import <ZaloPayCommonSwift/ZaloPayCommonSwift-Swift.h>
#import <ZaloPayCommon/ZPConstants.h>

#import "UIButton+TransferMoney.h"
#import "NetworkManager+Order.h"
#import "ZPBill+ZaloPay.h"
#import "TransferRecieveHeader.h"
#import "BillDetailViewController.h"
#import "UserTransferData.h"
#import "ZPWalletManager.h"
#import "PerUserDataBase+RecentTransferMoney.h"
#import "UIPasteboard+Transfer.h"
#import "NetworkManager+Transfer.h"
#import ZALOPAY_MODULE_SWIFT
#import "TransferAppInfo.h"
#import "NetworkManager+UpdateAcountName.h"
#import <ZaloPayCommon/NSString+Validation.h>
#import <ZaloPayNetwork/NSError+ZaloPayAPI.h>
#import <ZaloPayAnalytics/TrackerEvents.h>
#import <ZaloPayAnalytics/TrackerScreen.h>

@interface TransferReceiverViewController ()<UITextFieldDelegate, APNumberPadDelegate, UITextFieldDelegate>
@property (nonatomic, strong) TransferRecieveHeader *headerView;
@property (nonatomic, strong) ZPFloatTextInputView *amountTextField;
@property (nonatomic, strong) ZPFloatTextInputView *messageTextField;
@property (nonatomic, strong) UIButton *nextButton;
@property (nonatomic) UserTransferData* transferData;
@property (nonatomic, strong) UIView *inputView;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSString *transId;
@end

@implementation TransferReceiverViewController

- (id)initWithTransferData:(UserTransferData *)transferData {
    self = [super init];
    if (self) {
        self.transferData = transferData;
    }
    return self;
}

- (void)reloadWithUser:(UserTransferData *)transferData {
    if ([transferData.zaloId isEqualToString:self.transferData.zaloId]) {
        return;
    }
    self.transferData = transferData;
    [self requestZaloPayIdIfNeed];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = [R string_TransferHome_Title];
    if (self.transferData.source == FromQRCodeType2 ||
        self.transferData.source == FromWebApp_QRType2 ||
        self.transferData.source == FromQRCodeType3) {
        [self setupVendorView];
        if ([self.view viewWithTag:2017]) {
            self.nextButton.enabled = YES;
        }
    }
    else{
        [self setupView];
        [self loadTransferData];
    }
    [self requestZaloPayIdIfNeed];
    [self registerHandleKeyboardNotification];
    [[ZPAppFactory sharedInstance].eventTracker trackScreen:Screen_MoneyTransfer_InputAmount];
    
}
- (NSString *)screenName {
    return Screen_MoneyTransfer_InputAmount;
}
- (void)setUpUiBar {
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.leftBarButtonItems = [self leftBarButtonItems];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (![ZPDialogView isDialogShowing]) {
        [self.amountTextField.textField becomeFirstResponder];
    }
}

- (float)scrollHeight {
    return  220;
}

- (float)buttonToScrollOffset {
    return kZaloButtonToScrollOffset;
}

- (void)showUserData {
    self.headerView.labelName.text = self.transferData.displayName;
    [self.headerView.avatar sd_setImageWithURL:[NSURL URLWithString:self.transferData.avatar]
                              placeholderImage:[UIImage defaultAvatar]];
    self.headerView.labelPhone.text = [self userInfo];
}
- (NSString *)userInfo {
    if (self.transferData.mode != TransferToZaloPayID) {
        NSString *phone = self.transferData.phone;
        if (phone.length == 0) {
            phone = R.string_TransferMoney_Phone_None;
        }
        return [NSString stringWithFormat:@"%@: %@",R.string_TransferMoney_Phone_Number, phone];
    }
    NSString *text = self.transferData.accountName;
    if (text.length == 0) {
        text = [R string_TransferReceive_EmptyZaloPayId];
    }
    return  [NSString stringWithFormat:[R string_TransferReceive_ZaloPayId], text];
}

- (void)backButtonClicked:(id)sender {
    [self backActionHandle];
    [super backButtonClicked:sender];
    //[[ZPAppFactory sharedInstance].eventTracker trackEvent:EventAction_MoneyTransfer_ChangeReceiver];
}

- (void)backActionHandle {
    [self saveTransferData];
    [self moneyTransferProgressChanged:MoneyTransferProgressCancel];
}

- (void)saveTransferData {
    NSString *amount = self.amountTextField.textField.text;
    NSString *message = self.messageTextField.textField.text;
    if ([amount isEqualToString:self.transferData.transferMoney] &&
        [message isEqualToString:self.transferData.message]) {
        return;
    }
    [[RACScheduler scheduler] afterDelay:.1 schedule:^{
        [UIPasteboard recentTransferPasteboard].transferData = @{@"amount":amount ?: @"",
                                                                 @"message":message ?: @""};
    }];
}

- (void)loadTransferData {
    
    if (self.transferData.transferMoney) {
        if ([self.transferData.transferMoney floatValue] > 0) {
            self.amountTextField.textField.text = self.transferData.transferMoney;
        }
        self.messageTextField.textField.text = self.transferData.message;
    } else {
        NSDictionary *dic = [UIPasteboard recentTransferPasteboard].transferData;
        self.amountTextField.textField.text = [dic stringForKey:@"amount"];
        self.messageTextField.textField.text = [dic stringForKey:@"message"];
    }
    self.nextButton.enabled = self.transferData.transferMoney.length > 0;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.scrollView.contentSize = [self scrollViewContentSize];
    if (IS_IPHONE_35_INCH) {
        [self.scrollView scrollToBottom:false];
    }
}

- (CGSize)scrollViewContentSize {
    return CGSizeMake(self.view.frame.size.width, self.headerView.frame.size.height + self.inputView.frame.size.height);
}

- (void)setupView {
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, [self scrollHeight])];
    [self.view addSubview:self.scrollView];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(0);
        make.right.equalTo(0);
        make.top.equalTo(0);
        make.height.equalTo([self scrollHeight]);
    }];
    
    TransferRecieveHeader *headerView = [TransferRecieveHeader viewFromSameNib];
    CGRect frame = headerView.frame;
    frame.origin.x = (self.view.frame.size.width - frame.size.width)/2;
    headerView.frame = frame;
    [self.scrollView addSubview:headerView];
    self.headerView = headerView;
    UIView *inputView = [self createInputView:headerView];
    
    self.inputView = inputView;
    
    [inputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(headerView.mas_bottom);
        make.left.equalTo(0);
        make.width.equalTo([UIScreen mainScreen].applicationFrame.size.width);
        make.height.equalTo(120);
    }];
    
    [self addTransferButton];
}
-(void)setupVendorView{
    UILabel *lblWarning = [self createWarningView];
    [self.view addSubview:lblWarning];
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, [self scrollHeight])];
    [self.view addSubview:self.scrollView];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(0);
        make.right.equalTo(0);
        make.top.equalTo(lblWarning.mas_bottom);
        make.height.equalTo([self scrollHeight]);
    }];
    
    self.headerView = [TransferRecieveHeader viewFromSameNib];
    CGRect frame = self.headerView.frame;
    frame.size.width = self.view.frame.size.width;
    frame.size.height = 110;
    self.headerView.frame = frame;
    self.headerView.backgroundColor = [UIColor whiteColor];
    [self.scrollView addSubview:self.headerView];
    
    self.inputView = [self createVendorView];
    [self.scrollView addSubview:self.inputView];
    [self.inputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headerView.mas_bottom);
        make.left.equalTo(0);
        make.width.equalTo([UIScreen mainScreen].applicationFrame.size.width);
    }];
    
    [self addTransferButton];
  
}
-(UILabel *)createWarningView{
    CGRect frame = CGRectZero;
    NSString *warnigString = @"";
    UILabel *lblWarning = [[UILabel alloc] init];
    lblWarning.textAlignment = NSTextAlignmentCenter;
    lblWarning.font = [UIFont systemFontOfSize:14];
    lblWarning.numberOfLines = 0;
    lblWarning.backgroundColor = [UIColor colorWithHexValue:0xffffc1];
    long amount = [[self.transferData.transferMoney onlyDigit] doubleValue];
    warnigString = [TransferAppInfo getWarningMessage:amount];
    if (warnigString.length > 0) {
        frame.size.height = 40;
    }
    else{
        frame.size.height = 0;
        lblWarning.tag = 2017;
    }
    frame.size.width = [UIScreen mainScreen].applicationFrame.size.width;
    lblWarning.frame = frame;
    lblWarning.text = warnigString;
    return lblWarning;
}
- (void)addTransferButton {
    self.nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.nextButton.enabled = NO;
    [self.nextButton setupZaloPayButton];
    
    [self.nextButton setTitle:[R string_ButtonLabel_Next] forState:UIControlStateNormal];
    [self.view addSubview:self.nextButton];
    [self.nextButton addTarget:self
                        action:@selector(nextButtonTouchUpInside:)
              forControlEvents:UIControlEventTouchUpInside];
    [self.nextButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(10);
        make.right.equalTo(-10);
        make.height.equalTo(kZaloButtonHeight);
        make.top.equalTo(self.scrollView.mas_bottom).with.offset([self buttonToScrollOffset]);
    }];
}

- (UIView *)createInputView:(UIView *)headerView {
    UIView *inputView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 122)];
    inputView.backgroundColor = [UIColor whiteColor];
    self.amountTextField = [[ZPFloatTextInputView alloc] init];
    self.messageTextField = [[ZPFloatTextInputView alloc] init];
    self.amountTextField.lineInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    self.amountTextField.textField.delegate = self;
    self.messageTextField.textField.delegate = self;
    
    self.amountTextField.textField.placeholder = [R string_TransferReceive_AmountPlaceHolder];
    self.messageTextField.textField.placeholder = [R string_TransferReceive_MessagePlaceHolder];
    self.amountTextField.textField.delegate = self;
    
    [inputView addSubview:self.amountTextField];
    [inputView addSubview:self.messageTextField];
    [self.scrollView addSubview:inputView];
    
    @weakify(self);
    self.amountTextField.textField.inputView = ({
        @strongify(self);
        PMNumberPad *numberPad = [PMNumberPad numberPadWithDelegate:self];
        numberPad;
    });
    
    [[self.amountTextField.textField rac_textSignal] subscribeNext:^(NSString *text) {
        @strongify(self);
        [self textField:self.amountTextField.textField updateText:text];
    }];
    [[self.messageTextField.textField rac_textSignal] subscribeNext:^(NSString *text) {
        @strongify(self);
        [self textField:self.messageTextField.textField updateText:text];
    }];
    
    [self.amountTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.left.equalTo(0);
        make.right.equalTo(0);
    }];
    
    [self.messageTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(inputView.mas_bottom);
        make.left.equalTo(0);
        make.right.equalTo(0);
    }];
    
    ZPLineView *line = [[ZPLineView alloc] init];
    [inputView addSubview:line];
    [line alignToTop];
    return inputView;
}
- (UIView *)createVendorView{
    self.amountTextField = [[ZPFloatTextInputView alloc] init];
    self.amountTextField.textField.text = self.transferData.transferMoney;
    self.messageTextField = [[ZPFloatTextInputView alloc] init];
    self.messageTextField.textField.text = self.transferData.message;
    
    NSString *text = [NSString stringWithFormat:@"%@ VND",self.transferData.transferMoney];
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:text];
    [attString addAttribute:NSFontAttributeName value: [UIFont systemFontOfSize:13] range:[text rangeOfString:@"VND"]];
    
    UIView *inputView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 122)];
    inputView.backgroundColor = [UIColor whiteColor];
    
    UILabel *lblAmount = [[UILabel alloc] init];
    lblAmount.textAlignment = NSTextAlignmentCenter;
    lblAmount.font = [UIFont systemFontOfSize:27];
    lblAmount.attributedText = attString;
    [inputView addSubview:lblAmount];
    [lblAmount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(15);
        make.left.equalTo(8);
        make.right.equalTo(-8);
    }];

    UILabel *lblMessage = [[UILabel alloc] init];
    lblMessage.textAlignment = NSTextAlignmentCenter;
    lblMessage.font = [UIFont systemFontOfSize:14];
    lblMessage.numberOfLines = 0;
    lblMessage.text = self.transferData.message;
    [inputView addSubview:lblMessage];
    [lblMessage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lblAmount.mas_bottom).offset(10);
        make.bottom.equalTo(-17);
        make.left.equalTo(50);
        make.right.equalTo(-50);
    }];
    
    ZPLineView *topLine = [[ZPLineView alloc] init];
    [inputView addSubview:topLine];
    [topLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.left.equalTo(0);
        make.right.equalTo(0);
        make.height.equalTo(1);
    }];
    
    ZPLineView *botomLine = [[ZPLineView alloc] init];
    [inputView addSubview:botomLine];
    [botomLine alignToBottom];
    return inputView;
}

#pragma mark - override

- (UIScrollView *)internalScrollView {
    return self.scrollView;
}

- (UIButton *)internalBottomButton {
    return self.nextButton;
}

#pragma mark - Action

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.amountTextField resignFirstResponder];
    [self.messageTextField resignFirstResponder];
}

- (void)nextButtonTouchUpInside:(id)sender {
    
    [[ZPAppFactory sharedInstance].eventTracker trackEvent:EventAction_moneytransfer_touch_continue];
    long deltaBalance = [ZPWalletManager sharedInstance].currentBalance.intValue - (int)[[self.amountTextField.textField.text onlyDigit] integerValue];
    if ([ZPProfileManager shareInstance].userLoginData.isClearing &&
        deltaBalance < 0) {
        [self.amountTextField showErrorWithText:[R string_Withdraw_NotEnoughMoney]];
        return;
    }
    if ([self validInputData]) {
        [self.amountTextField resignFirstResponder];
        [self.messageTextField resignFirstResponder];

        if ([ZPProfileManager shareInstance].userLoginData.isClearing &&
             deltaBalance < 1000 && deltaBalance > 0) {
            int64_t min = [ZPWalletManager sharedInstance].config.minTransfer;
            NSString *money = [@(min) formatCurrency];
            [self.amountTextField showErrorWithText:[NSString stringWithFormat:@"%@ %@",R.string_Clearing_transfer_faild_min,money]];
            return;
        }
        [self requestCreateOrder];
    }
}

- (BOOL)validInputData {
    if (self.transferData.paymentUserId.length == 0) {
        [ZPDialogView showDialogWithType:DialogTypeError
                                 message:[R string_TransferReceive_GetInforErrorMessage]
                       cancelButtonTitle:[R string_ButtonLabel_Close]
                       ortherButtonTitle:nil
                          completeHandle:nil];
        return FALSE;
    }
    long totalValue = [[self.amountTextField.textField.text onlyDigit] doubleValue];
    if ([TransferAppInfo isLessThan:totalValue textInput:self.amountTextField]) {
        [self setNextButtonEnable:FALSE];
        return FALSE;
    }
    return TRUE;
}

- (void)requestZaloPayIdIfNeed {
    
    if (self.transferData.mode == TransferToZaloPayID && self.transferData.accountName.length > 0) {
        [self requestUserInfoWithAccountName:self.transferData.accountName];
        return;
    }
    if (self.transferData.paymentUserId.length > 0) {
        [self showUserData];
        return;
    }
    [self showLoadingView];
    @weakify(self);
    [[[[ZPProfileManager shareInstance] requestUserProfile:self.transferData.zaloId] deliverOnMainThread] subscribeNext:^(ZaloUser *user) {
        @strongify(self);
        [self hideLoadingView];
        if (user) {
            self.transferData.paymentUserId = user.zaloPayId;
            self.transferData.phone = user.phone;
            self.transferData.accountName = user.zaloPayName;
            if (self.transferData.avatar.length == 0) {
                self.transferData.avatar = user.avatar;
            }
            if (self.transferData.displayName.length == 0) {
                self.transferData.displayName = user.displayName;
            }
        }
        [self showUserData];
    } error:^(NSError *error) {
        @strongify(self);
        [self hideLoadingView];
        [self showUserData];
        [self.amountTextField.textField resignFirstResponder];
        [self.messageTextField.textField resignFirstResponder];
        [ZPDialogView showDialogWithError:error handle:nil];
    }];
}

- (void)requestUserInfoWithAccountName:(NSString *)accountName {
    
    @weakify(self);
    [[ZPAppFactory sharedInstance] showHUDAddedTo:nil];
    [[[[NetworkManager sharedInstance] getUserInfoWithAcountName:[accountName lowercaseString]] deliverOnMainThread] subscribeNext:^(NSDictionary *dic) {
        DDLogInfo(@"get user info data :%@",dic);
        [[ZPAppFactory sharedInstance] hideAllHUDsForView:nil];
        [self handleUserInfoData:dic accountName:accountName];
    } error:^(NSError *error) {
        @strongify(self);
        DDLogInfo(@"get user info error :%@",error);
        [[ZPAppFactory sharedInstance] hideAllHUDsForView:nil];

        [ZPDialogView showDialogWithType:DialogTypeError
                                 message:[error apiErrorMessage]
                       cancelButtonTitle:[R string_ButtonLabel_Close]
                       ortherButtonTitle:nil
                          completeHandle:nil];
        [self payBillDidError];
    }];
}

- (void)handleUserInfoData:(NSDictionary *)dic accountName:(NSString *)acountName{
    NSString *phone = [NSString stringWithFormat:@"%@",[dic objectForKey:@"phonenumber"]];
    NSString *displayName = [dic stringForKey:@"displayname"];
    NSString *zaloPayId = [dic stringForKey:@"userid"];
    NSString *avatar = [dic stringForKey:@"avatar"];
    
    if ([zaloPayId isEqualToString:[NetworkManager sharedInstance].paymentUserId]) {
        [ZPDialogView showDialogWithType:DialogTypeError
                                 message:[R string_TransferToAccount_CurrentUserAccountMessage]
                       cancelButtonTitle:[R string_ButtonLabel_Close]
                       ortherButtonTitle:nil
 completeHandle:nil];
        [self payBillDidError];
        return;
    }
    if (self.transferData) {
        self.transferData.displayName = displayName;
        self.transferData.paymentUserId = zaloPayId;
        self.transferData.phone = phone;
        self.transferData.avatar = avatar;
        self.transferData.accountName = acountName;
    }
    [self showUserData];
}

- (void)trackInputMessage:(NSString *)message {
    [[ZPAppFactory sharedInstance].eventTracker trackEvent:EventAction_moneytransfer_input_amount];
}

- (NSString *)buildItemExtString {
    NSMutableDictionary *info = [NSMutableDictionary dictionary];
    [info setObject:@(ZPTransTypeTranfer) forKey:ZPCreateWalletOrderTranstypeKey];
    NSString *displayName = self.transferData.displayName;
    NSString *phone = self.transferData.phone;
    NSString *zalopayid = self.transferData.accountName;
    NSString *ext =  [NSString stringWithFormat:@"%@:%@", R.string_Receiver, displayName];
    if (self.transferData.mode == TransferToZaloPayID && zalopayid.length > 0) {
        ext = [NSString stringWithFormat:@"%@\t%@:%@", ext, R.string_TransferMoney_Ext_ZaloPayId ,zalopayid];
    }
    if (self.transferData.mode != TransferToZaloPayID && phone.length > 0) {
        NSString *formatPhone = phone;
        ext = [NSString stringWithFormat:@"%@\t%@:%@",ext, R.string_Title_Phone_Number, formatPhone];
    }
    [info setObject:ext forKey:ZPCreateWalletOrderExtKey];
    [info setObject:@{ZPCreateWalletOrderSenderPhoneNumberKey:[ZPProfileManager shareInstance].userLoginData.phoneNumber} forKey:ZPCreateWalletOrderSenderKey];
    return [info JSONRepresentation];
}

- (void)requestCreateOrder {
    int totalValue = (int)[[self.amountTextField.textField.text onlyDigit] integerValue];
    NSString *message = self.messageTextField.textField.text;
    [self trackInputMessage:message];
    @weakify(self);
    [self showLoadingView];
    self.nextButton.userInteractionEnabled = NO;

    NSString *appUser = [NSString stringWithFormat:@"1;%@", self.transferData.paymentUserId];
    NSString *displayName = self.transferData.displayName.length > 0 ? self.transferData.displayName : @"";
    NSString *itemStr = [self buildItemExtString];
    [self.amountTextField.textField resignFirstResponder];
    [self.messageTextField.textField resignFirstResponder];
    
    [[[[NetworkManager sharedInstance] getWalletOrdederWithAmount:totalValue
                                               andTransactionType:ZPTransTypeTranfer
                                                          appUser:appUser
                                                      description:message
                                                            appId:kZaloPayClientAppId
                                                        embeddata:displayName
                                                             item:itemStr] deliverOnMainThread] subscribeNext:^(NSDictionary *json) {
        @strongify(self);
        ZPBill *billInfo = [ZPBill billWithData:json transType:ZPTransTypeTranfer orderSource:self.orderSource appId:kZaloPayClientAppId];
        [billInfo addUserInfo:[self userInfoDic]];
        self.transId = billInfo.appTransID;        
        [self showBillViewController:billInfo];
        self.nextButton.userInteractionEnabled = YES;
    } error:^(NSError *error) {
        @strongify(self);
        [ZPDialogView showDialogWithError:error handle:^{
            [self.amountTextField.textField becomeFirstResponder];
        }];
        [self hideLoadingView];
        self.nextButton.userInteractionEnabled = YES;
    }];
}

- (NSDictionary *)userInfoDic {
    NSString *avatar = self.transferData.avatar.length > 0 ? self.transferData.avatar : @"";
    NSString *name = self.transferData.displayName.length > 0 ? self.transferData.displayName :@"";
    NSString *phone = self.transferData.phone.length > 0 ? self.transferData.phone : @"";
    NSString *zaloPayName = self.transferData.accountName.length > 0? self.transferData.accountName: @"";
    NSString *fullName = self.transferData.displayName.length > 0 ? self.transferData.displayName : @"";
    UIImage *friendAvatar = [self avatarFromUrl:avatar];
    
    ZaloUser *user = [ZPProfileManager shareInstance].currentZaloUser;
    NSString *currentUserAvatarUrl = user.avatar.length > 0 ? user.avatar : @"";
    UIImage *currentUserAvatar = [self avatarFromUrl:currentUserAvatarUrl];
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] initWithDictionary:@{@"avatar": avatar,
                                                                                      @"name": name,
                                                                                      @"phone": phone,
                                                                                      @"zalopayname": zaloPayName,
                                                                                      @"zalofullname": fullName,
                                                                                      @"currentUserAvatarUrl": currentUserAvatarUrl
                                                                                      }];
    [userInfo setObjectCheckNil:friendAvatar forKey:@"friendAvatar"];
    [userInfo setObjectCheckNil:currentUserAvatar forKey:@"currentUserAvatar"];

    return userInfo;
}

- (UIImage *)avatarFromUrl:(NSString *)avatarUrl {
    if(avatarUrl.length == 0) {
        return nil;
    }
    
    NSString *cacheKey = [[SDWebImageManager sharedManager] cacheKeyForURL:[NSURL URLWithString:avatarUrl]];
    UIImage *cacheAvatar = [[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:cacheKey];
    return cacheAvatar;
}

#pragma mark - Bill Delegate

- (void)payBillDidCompleteSuccess {
    self.transferData.message = self.messageTextField.textField.text;
    self.transferData.transferMoney = self.amountTextField.textField.text;
    [[PerUserDataBase sharedInstance] addRecentTransferFriend:self.transferData];
    [UIPasteboard recentTransferPasteboard].transferData = nil;
    [super payBillDidCompleteSuccess];
}

- (void)paymentNotifySuccess:(NSDictionary*)data {
    [self moneyTransferProgressChanged:MoneyTransferProgressSuccess];
}

- (void)paymentNotifyFail:(NSDictionary*)data {
    [self moneyTransferProgressChanged:MoneyTransferProgressFail];
}

- (void)paymentNotifyUnidentified {
    [self moneyTransferProgressChanged:MoneyTransferProgressFail];
}

- (void)moneyTransferProgressChanged:(MoneyTransferProgress)progress {
    if (self.transferData.mode == TransferToZaloPayUser) {
        self.transferData.transferMoney = self.amountTextField.textField.text;
        [self sendNotifcationWhenQRTransfer:progress];
    }
}

- (void)sendNotifcationWhenQRTransfer:(MoneyTransferProgress)progress {
    NSString *transId;
    if (progress == MoneyTransferProgressSuccess) {
        transId = self.transId;
    }
    @weakify(self);
    [[[ZPProfileManager shareInstance] loadZaloUserProfile] subscribeNext:^(ZaloUser *currentUser) {
        @strongify(self);
        NSString *embededData = [TransferAppInfo embededDataWithDisplayName:currentUser.displayName
                                                                     avatar:currentUser.avatar
                                                      moneyTransferProgress:progress
                                                                     amount:[[self.transferData.transferMoney onlyDigit] longLongValue]
                                                                    transId:transId];
        [[[NetworkManager sharedInstance] sendNotificationWithReceiverId:self.transferData.paymentUserId
                                                             embededData:embededData] subscribeCompleted:^{
            DDLogInfo(@"Transfer: Send Notification Scan QR");
        }];
    }];
}

#pragma mark - Text Field Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField == self.amountTextField.textField) {
        int64_t totalValue = [[newString onlyDigit] longLongValue];
        return ![TransferAppInfo isGreaterThanMax:totalValue textInput:self.amountTextField];
    } else if (textField == self.messageTextField.textField) {
        if (![TransferAppInfo validMessage:newString]) {
            [self.messageTextField showErrorAuto:[NSString stringWithFormat:[R string_InputMessage_Error_Message],
                                                  TRANSFER_MESSAGE_LENGTH]];
        }
        return [TransferAppInfo validMessage:newString];
    }
    return TRUE;
}

#pragma mark - APNumberPadDelegate

- (void)numberPad:(PMNumberPad *)numberPad
functionButtonAction:(UIButton *)functionButton
        textInput:(UIResponder<UITextInput> *)textInput {
    NSString *text = @"000";
    if (functionButton == numberPad.leftFunctionButton) {
        BOOL isInsert = [self textField:self.amountTextField.textField
          shouldChangeCharactersInRange:NSMakeRange(self.amountTextField.textField.text.length, 0)
                      replacementString:text];
        if (isInsert) {
            [self.amountTextField.textField insertText:text];
        }
    } else if (functionButton == numberPad.doneButton) {
        [self nextButtonTouchUpInside:nil];
    }
}

- (void)setNextButtonEnable:(BOOL)enable {
    if ([self.amountTextField.textField.inputView isKindOfClass:[PMNumberPad class]]) {
        ((PMNumberPad *)self.amountTextField.textField.inputView).doneButton.enabled = enable;
    }
    self.nextButton.enabled = enable;
}

- (void)textField:(UITextField *)textField updateText:(NSString *)text {
    if (textField == self.amountTextField.textField) {
        self.amountTextField.textField.text = [[text onlyDigit] formatMoneyValue];
    }
    BOOL enableButton = TRUE;
    
    if (![TransferAppInfo validMessage:self.messageTextField.textField.text]) {
        enableButton = FALSE;
    }
    int64_t totalValue = [[self.amountTextField.textField.text onlyDigit] longLongValue];
    if ([TransferAppInfo isGreaterThanMax:totalValue] || totalValue == 0) {
        enableButton = FALSE;
    }
    [self setNextButtonEnable:enableButton];
}

@end

#pragma mark - ZPTransferReceiverWrapper

@implementation ZPTransferReceiverWrapper

- (id)initWithTransferData:(UserTransferData *)userTransferData
              preHandle:(dispatch_block_t)preHandle
           cancelHandel:(dispatch_block_t)cancel
            errorHandle:(dispatch_block_t)error
         completeHandle:(dispatch_block_t)complete {
    
    self = [super initWithTransferData:userTransferData];
    if (self) {
        [self setPreHandle:preHandle cancelHandel:cancel errorHandle:error completeHandle:complete];
    }
    
    return self;
}

- (void)setPreHandle:(dispatch_block_t)preHandle
        cancelHandel:(dispatch_block_t)cancel
         errorHandle:(dispatch_block_t)error
      completeHandle:(dispatch_block_t)complete{
    self.preHandle = preHandle;
    self.cancelHandle = cancel;
    self.errorHandle = error;
    self.completeHandle = complete;
}

- (void)clearHandle {
    self.cancelHandle = nil;
    self.errorHandle = nil;
    self.completeHandle = nil;
}

#pragma mark - Override

- (void)saveTransferData {
    
}

- (void)backButtonClicked:(id)sender {
    if (self.preHandle) {
        self.preHandle();
    }
    if (self.cancelHandle) {
        self.cancelHandle();
    }
    [self clearHandle];
    [super backButtonClicked:sender];
    [UIPasteboard recentTransferPasteboard].transferData = nil;
}

- (void)payBillDidError {
    if (self.preHandle) {
        self.preHandle();
    }
    if (self.errorHandle) {
        self.errorHandle();
    }
    [self clearHandle];
    [super payBillDidError];
}

- (void)payBillDidCompleteSuccess {
    if (self.preHandle) {
        self.preHandle();
    }
    if (self.completeHandle) {
        self.completeHandle();
    }
    [self clearHandle];
    [super payBillDidCompleteSuccess];
}

@end


#pragma mark - ZPTransferReceiverWebAppWrapper

@implementation ZPTransferReceiverWebAppWrapper

#pragma mark - Override

- (void)backButtonClicked:(id)sender {
    if (self.preHandle) {
        self.preHandle();
    }
    if (self.cancelHandle) {
        self.cancelHandle();
    }
    [self clearHandle];
}

- (void)payBillDidError {
    
    if (self.preHandle) {
        self.preHandle();
    }
    if (self.errorHandle) {
        self.errorHandle();
    }
    [self clearHandle];
}

- (void)payBillDidCompleteSuccess {
    
    if (self.preHandle) {
        self.preHandle();
    }
    if (self.completeHandle) {
        self.completeHandle();
    }
    [self clearHandle];
}

@end
