//
//  ZPTransferReceiverWebAppWrapper.swift
//  ZaloPay
//
//  Created by nhatnt on 29/11/2017.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation

class ZPTransferReceiverWebAppWrapper: ZPTransferReceiverWrapper {
    override func backButtonClicked(_ sender: Any!) {
       viewWillSwipeBack()
    }
    
    override func viewWillSwipeBack() {
        if let preHandle = self.preHandle  {
            preHandle()
        }
        
        if let cancelHandle = self.cancelHandle  {
            cancelHandle()
        }
        
        self.clearHandle()
    }
    
    
    override func payBillDidError() {
        if let preHandle = self.preHandle  {
            preHandle()
        }
        
        if let cancelHandle = self.cancelHandle  {
            cancelHandle()
        }
        
        self.clearHandle()
    }
    
    override func payBillDidCompleteSuccess() {
        if let preHandle = self.preHandle  {
            preHandle()
        }
        
        if let cancelHandle = self.cancelHandle  {
            cancelHandle()
        }
        
        self.clearHandle()
    }
}
