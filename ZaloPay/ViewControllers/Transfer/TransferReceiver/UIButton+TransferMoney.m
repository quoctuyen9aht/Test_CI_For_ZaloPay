//
//  UIButton+TransferMoney.m
//  ZaloPay
//
//  Created by PhucPv on 4/19/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import "UIButton+TransferMoney.h"

@implementation UIButton(TransferMoney)

- (void)baseTransferMoney {
    [self setBackgroundColor:[UIColor colorWithHexValue:0x7fc7f2]
                    forState:UIControlStateDisabled];
    [self setBackgroundColor:[UIColor colorWithHexValue:0x008fe5]
                    forState:UIControlStateNormal];
    [self setBackgroundColor:[UIColor colorWithHexValue:0x0174af]
                    forState:UIControlStateHighlighted];
    self.layer.cornerRadius = 3.0;
    self.layer.masksToBounds = YES;
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.titleLabel setFont:[UIFont SFUITextMediumWithSize:17]];
}

@end

@implementation UIUnderlinedButton

+ (UIUnderlinedButton*) underlinedButton {
    UIUnderlinedButton* button = [[UIUnderlinedButton alloc] init];
    return button;
}

- (void) drawRect:(CGRect)rect {
    CGRect textRect = self.titleLabel.frame;
    
    // need to put the line at top of descenders (negative value)
    CGFloat descender = self.titleLabel.font.descender + 5;
    
    CGContextRef contextRef = UIGraphicsGetCurrentContext();
    
    // set to same colour as text
    CGContextSetStrokeColorWithColor(contextRef, self.titleLabel.textColor.CGColor);
    
    CGContextMoveToPoint(contextRef, textRect.origin.x, textRect.origin.y + textRect.size.height + descender);
    
    CGContextAddLineToPoint(contextRef, textRect.origin.x + textRect.size.width, textRect.origin.y + textRect.size.height + descender);
    
    CGContextClosePath(contextRef);
    
    CGContextDrawPath(contextRef, kCGPathStroke);
}
@end
