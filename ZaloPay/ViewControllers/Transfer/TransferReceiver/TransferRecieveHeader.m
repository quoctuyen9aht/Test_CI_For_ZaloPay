//
//  TransferRecieveHeader.m
//  ZaloPay
//
//  Created by bonnpv on 6/10/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "TransferRecieveHeader.h"

@implementation TransferRecieveHeader

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.avatar roundRect:self.avatar.frame.size.width/2];
    self.avatar.backgroundColor = [UIColor grayColor];
    self.backgroundColor = [UIColor clearColor];
    self.labelName.adjustsFontSizeToFitWidth = YES;
    self.labelName.font = [UIFont SFUITextRegularWithSize:16];
    self.labelPhone.font = [UIFont SFUITextRegularWithSize:12];
    self.labelPhone.textColor = [UIColor subText];
    self.labelName.textColor =  [UIColor defaultText];
}

@end
