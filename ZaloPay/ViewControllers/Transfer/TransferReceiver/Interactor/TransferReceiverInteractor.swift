//
//  TransferReceiverInteractor.swift
//  ZaloPay
//
//  Created by nhatnt on 03/11/2017.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayCommonSwift
import CocoaLumberjackSwift
import SDWebImage
import ZaloPayProfile
import ZaloPayAnalyticsSwift
import ZaloPayConfig

class TransferReceiverInteractor: TransferReceiverInteractorInput {
    //Reference to the View (weak to avoid retain cycle)
    weak var output: TransferReceiverInteractorOutput?
    var orderSource = OrderSource_None
    private let recentTransferMoneyTable: ZPRecentTransferMoneyTable = ZPRecentTransferMoneyTable(db: PerUserDataBase.sharedInstance())
    
    func handleUserInfoData(dic: NSDictionary, accountName: String) ->UserTransferData {
        let nDict = dic as? [String: Any] ?? [:]
        
        let phone = nDict.value(forKey: "phonenumber", defaultValue: Int64(0))
        let displayName = nDict.value(forKey: "displayname", defaultValue: "")
        let zaloPayId = nDict.value(forKey: "userid", defaultValue: "")
        let avatar = nDict.value(forKey: "avatar", defaultValue: "")
        
        let user = UserTransferData()
        user.displayName = displayName;
        user.paymentUserId = zaloPayId
        user.phone = String(phone)
        user.avatar = avatar
        user.accountName = accountName
        user.mode = .toZaloPayID
        user.source = .fromTransferActivity
        return user
    }
    
    func fetchData(accountName: String) {
        ZPAppFactory.sharedInstance().showHUDAdded(to: nil)
        NetworkManager.sharedInstance().getUserInfo(withAcountName: accountName.lowercased()).deliverOnMainThread().subscribeNext({ [weak self](result) in
            guard let strongSelf = self else {
                return
            }
            let dic = result as! NSDictionary
            strongSelf.output?.loadSucceed(user:strongSelf.handleUserInfoData(dic: dic, accountName: accountName))
            }, error:({[weak self] (error) in
                let err = error! as NSError
                self?.output?.loadFailed(message:err.apiErrorMessage())
            }))
    }
    
    func requestUserProfile(userTransferData: UserTransferData) {
        ZPProfileManager.shareInstance
            .requestUserProfile(userId: userTransferData.zaloId ?? "")
            .deliverOnMainThread().subscribeNext({[unowned self](result) in
                self.output?.hideLoadingView()
                if let user = result as? ZaloUserSwift {
                    userTransferData.paymentUserId = user.zaloPayId
                    userTransferData.phone = user.phone
                    userTransferData.accountName = user.zaloPayName
                    userTransferData.avatar = userTransferData.avatar ?? user.avatar
                    userTransferData.displayName = userTransferData.displayName ?? user.displayName
                }}, error:({[unowned self] (error) in
                    self.output?.hideLoadingView()
                    self.output?.showUserData(user: userTransferData)
                    self.output?.setTextFieldResignFirstResponder()
                    ZPDialogView.showDialogWithError(error, handle: nil)
                }))
    }
    
    func requestOrder(totalValue: Int64, message: String, userTransferData: UserTransferData) {
        //visible nextButton
        let uid = userTransferData.paymentUserId ?? ""
        let appUser = String(format: "1;%@", uid)
        let displayName = userTransferData.displayName ?? ""
        let itemStr = self.buildItemExtString(userTransferData: userTransferData)
        
        DispatchQueue.main.async {
            self.output?.setTextFieldResignFirstResponder()
        }
        NetworkManager.sharedInstance().getWalletOrdeder(withAmount: Int(totalValue), andTransactionType: Int32(ZPTransType.tranfer.rawValue), appUser: appUser, description: message, appId: kZaloPayClientAppId, embeddata: displayName, item: itemStr)
            .subscribeNext({[weak self] json in
                guard let dict = json as? JSON,
                    let strongSelf = self else {
                        return
                }
                let billInfo = ZPBill(data: dict, transType: ZPTransType.tranfer, orderSource: Int32(strongSelf.orderSource.rawValue), appId: kZaloPayClientAppId)
                billInfo.addUserInfo(strongSelf.userInfoDic(userTransferData: userTransferData) as! [AnyHashable : Any])
                
                DispatchQueue.main.async {
                    strongSelf.output?.showBillViewController(billInfo: billInfo)
                }
            }, error: {[weak self] error in
                DispatchQueue.main.async {
                    self?.output?.failCreateOrder(error: error)
                }
            })
    }
    
    func requestCreateOrder(totalValue: Int64, message: String, userTransferData: UserTransferData) {
        self.trackInputMessage(message: message)
        self.output?.showLoadingView()
        
        // check message content before request order
        NetworkManager.sharedInstance().filterTextContent(message)
            .subscribeNext({ [weak self] dict in
                guard let dict = dict as? JSON else {
                    self?.requestOrder(totalValue: totalValue, message: "", userTransferData: userTransferData)
                    return
                }
                let returnMessage = dict.value(forKey:"returnmessage", defaultValue: "")
                let status = dict.value(forPath:"data.status", defaultValue: false)
                let modified = dict.value(forPath:"data.modified", defaultValue: message)
                
                if status {
                    self?.requestOrder(totalValue: totalValue, message: message, userTransferData: userTransferData)
                    return
                }
                DispatchQueue.main.async {
                    self?.output?.invalidOrderMessage(errorMessage: returnMessage, modified: modified)
                }
            }, error: { [weak self] err in
                if let error = err as NSError?, error.isApiError().not {
                    self?.requestOrder(totalValue: totalValue, message: message, userTransferData: userTransferData)
                    return
                }
                DispatchQueue.main.async {
                    self?.output?.failVerifyOrderMessage(error: err)
                }
            })
    }
    
    func userInfoDic(userTransferData: UserTransferData) -> NSDictionary {
        let avatar = userTransferData.avatar ??  ""
        let name = userTransferData.displayName ??  ""
        let phone =  userTransferData.phone ?? ""
        let zaloPayName = userTransferData.accountName ?? ""
        let fullName = userTransferData.displayName ?? ""
        let friendAvatar = self.avatarFromUrl(avatarUrl: avatar)

        let user = ZPProfileManager.shareInstance.currentZaloUser
        let currentUserAvatarUrl =  user?.avatar ?? ""
        let currentUserAvatar = self.avatarFromUrl(avatarUrl: currentUserAvatarUrl) ?? UIImage()
        

        let userInfo = NSMutableDictionary.init(dictionary: ["avatar": avatar, "name": name, "phone": phone, "zalopayname": zaloPayName, "zalofullname": fullName, "currentUserAvatarUrl": currentUserAvatarUrl])
        userInfo.setObjectCheckNil(friendAvatar, forKey: "friendAvatar" as NSCopying)
        userInfo.setObjectCheckNil(currentUserAvatar, forKey: "currentUserAvatar" as NSCopying)
        return userInfo
    }
    
    func avatarFromUrl(avatarUrl: String?) -> UIImage?{
        if avatarUrl?.count == 0 {
            return nil
        }
        
        let cacheKey = SDWebImageManager.shared().cacheKey(for: URL.init(string: avatarUrl!))
        let cacheAvatar = SDImageCache.shared().imageFromMemoryCache(forKey: cacheKey)
        return cacheAvatar
    }
    
    func buildItemExtString(userTransferData: UserTransferData) -> String {   
        let info = NSMutableDictionary()
        info.setObject(ZPTransType.tranfer.rawValue, forKey: ZPCreateWalletOrderTranstypeKey as NSCopying)
        let displayName = userTransferData.displayName ?? ""
        let phone = userTransferData.getPhoneDisplayFromSource()
        let zalopayid = userTransferData.accountName ?? ""
        var ext = String(format: "%@:%@", R.string_Receiver(), displayName)
        
        if userTransferData.mode == .toZaloPayID && zalopayid.count > 0 {
            ext = String(format: "%@\t%@:%@", ext, R.string_TransferMoney_Ext_ZaloPayId(), zalopayid)
        }
        
        if userTransferData.mode != .toZaloPayID && phone.count > 0 {
            ext = String(format: "%@\t%@:%@", ext, R.string_Title_Phone_Number(), phone)
        }
        let currentUserPhone = ZPProfileManager.shareInstance.userLoginData?.phoneNumber ?? ""
        info.setObject(ext, forKey: ZPCreateWalletOrderExtKey as NSCopying)
        info.setObject([ZPCreateWalletOrderSenderPhoneNumberKey: currentUserPhone], forKey: ZPCreateWalletOrderSenderKey as NSCopying)
        return info.jsonRepresentation()
    }
    
    
    func trackInputMessage(message: String){
        ZPTrackingHelper.shared().trackEvent(.moneytransfer_input_message)
    }
    
    
    func sendNotifcationWhenQRTransfer(progress: MoneyTransferProgress, transId: String, userTransferData: UserTransferData) {
        ZPProfileManager.shareInstance.loadZaloUserProfile().subscribeNext { (currentUser) in
            let moneyString = userTransferData.transferMoney ?? ""
            let moneyValue = Int64(moneyString.onlyDigit()) ?? 0
            guard let user = currentUser as? ZaloUserSwift else {
                return
            }
            let embededData = TransferAppInfo.embededData(withDisplayName: user.displayName, avatar: user.avatar , moneyTransferProgress: progress, amount: moneyValue, transId: transId)
            NetworkManager.sharedInstance().sendNotification(withReceiverId: userTransferData.paymentUserId, embededData: embededData).subscribeCompleted({
                DDLogInfo("Transfer: Send Notification Scan QR")
            })
        }
    }
    
    func validateMessageLength(_ message: String) ->Bool {
        let max = ZPApplicationConfig.getTransferConfig()?.getMaxLengthMessage() ?? 50
        return message.count <= max
    }
    
    func saveRecentTransferFriend(data: UserTransferData) {
        recentTransferMoneyTable.addRecentTransferFriend(data)
    }
}
