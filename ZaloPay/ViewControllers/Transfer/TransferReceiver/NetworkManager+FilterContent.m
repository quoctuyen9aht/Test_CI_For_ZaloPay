//
//  NetworkManager+FilterContent.m
//  ZaloPay
//
//  Created by Bon Bon on 5/22/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import "NetworkManager+FilterContent.h"
#import "ZPAppDelegateConstants.h"
#import <ZaloPayNetwork/ZPNetWorkApi.h>

@implementation NetworkManager (FilterContent)
- (RACSignal *)filterTextContent:(NSString *)content {
    NSString *encodedContent = [content stringByAddingPercentEncodingWithAllowedCharacters: [NSCharacterSet URLHostAllowedCharacterSet]];
    NSString *url = badwordUrlFromPath(api_inspector_v1_inspect);
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:encodedContent forKey:@"description"];
    return [[NetworkManager sharedInstance] creatRequestWithPath:url params:params isGet:true];
}
@end
