//
//  ZPTransferReceiverWrapper.swift
//  ZaloPay
//
//  Created by nhatnt on 29/11/2017.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
@objcMembers
class ZPTransferReceiverWrapper: TransferReceiverViewControllerSwift {
    var preHandle: (()->Void)?
    var cancelHandle: (()->Void)?
    var errorHandle: (()->Void)?
    var completeHandle: (()->Void)?       
    
    convenience init(transferData: UserTransferData,
                     orderSource: OrderSource = OrderSource_None,
                     preHandle: @escaping ()->Void,
                     cancelHandle: @escaping ()->Void,
                     errorHandle: @escaping ()->Void,
                     completeHandle: @escaping ()->Void) {
        self.init(transferData: transferData)
        
        let presenter = TransferReceiverPresenter()
        let interactor = TransferReceiverInteractor()
        interactor.orderSource = orderSource
        self.presenter = presenter
        interactor.output = presenter
        presenter.view = self
        presenter.interactor = interactor
        
        self.setHandle(preHandle: preHandle,
                       cancelHandle: cancelHandle,
                       errorHandle: errorHandle,
                       completeHandle: completeHandle)
    }
    
    func setHandle(preHandle: @escaping (()->Void), cancelHandle: @escaping (()->Void), errorHandle: @escaping ()->Void, completeHandle: @escaping ()->Void){
        self.preHandle = preHandle
        self.cancelHandle = cancelHandle
        self.errorHandle = errorHandle
        self.completeHandle = completeHandle
    }
    
    func clearHandle() {
        self.completeHandle = nil
        self.cancelHandle = nil
        self.errorHandle = nil
    }
    
    override func saveTransferData() {
        
    }
    
    override func viewWillSwipeBack() {
        super.viewWillSwipeBack()
        if let preHandle = self.preHandle  {
            preHandle()
        }
        
        if let cancelHandle = self.cancelHandle  {
            cancelHandle()
        }
        self.clearHandle()
        UIPasteboard.recentTransfer().transferData = nil
    }
    
    override func payBillDidError() {
        if let preHandle = self.preHandle  {
            preHandle()
        }
        
        if let cancelHandle = self.cancelHandle  {
            cancelHandle()
        }
        self.clearHandle()
        super.payBillDidError()
    }
    
    override func payBillDidCompleteSuccess() {
        if let preHandle = self.preHandle  {
            preHandle()
        }
        
        if let cancelHandle = self.cancelHandle  {
            cancelHandle()
        }
        self.clearHandle()
        super.payBillDidCompleteSuccess()
    }
}
