//
//  TransferReceiverViewControllerSwift.swift
//  ZaloPay
//
//  Created by nhatnt on 07/11/2017.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import ZaloPayCommonSwift
import CocoaLumberjackSwift
import ZaloPayAnalyticsSwift
import ZaloPayProfile

@objcMembers
class TransferReceiverViewControllerSwift: BillHandleBaseViewController, TransferReceiverViewInterface{
    let minValueTransferClearing = 1000
    
    // MARK: *** UI Elements
    var headerView = TransferRecieveHeaderSwift()
    var scrollView: UIScrollView!
    var nextButton = UIButton()
    var inputHeaderView: UIView!
    lazy var amountTextField =  ZPCalFloatTextInputView()
    lazy var messageTextField = ZPFloatTextInputView()
    
    private let disposeBag = DisposeBag()
    
    // MARK: *** Data model
    var userTransferData = UserTransferData()
    var transId = String()
    var presenter: TransferReceiverPresenterInterface?
    // MARK: *** Initialize
    convenience init(transferData: UserTransferData) {
        self.init()
        self.userTransferData = transferData
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = R.string_TransferHome_Title()
        
        let source = self.userTransferData.source
        if source == .fromQRCodeType2 || source == .fromWebAppQRType2 || source == .fromQRCodeType3 {
            self.setupVendorView()
        } else {
            self.setupView()
            self.loadTransferData()
        }
        self.handleTextFieldEvent()
        self.handleNextButtonEvent()
        self.handleTextFieldShowError()
        self.presenter?.requestZaloPayIdIfNeed(userTransferData: userTransferData)
        self.registerHandleKeyboardNotification()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ZPTrackingHelper.shared().trackScreen(with: self)
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.moneytransfer_input_launched)
    }
    
    override func backEventId() -> Int {
        return ZPAnalyticEventAction.moneytransfer_input_touch_back.rawValue
    }
    
    override func screenName() -> String! {
        return ZPTrackerScreen.Screen_iOS_MoneyTransfer_InputAmount
    }
    
    func handleTextFieldEvent() {
        messageTextField.textField.rx.controlEvent(.editingDidBegin).bind {(_) in
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.moneytransfer_input_message)
        }.disposed(by: disposeBag)
        messageTextField.textField.rx.controlEvent(.editingChanged).bind { [weak self] (_) in
            //Track clear amountTextField
            if let text = self?.messageTextField.textField.text, text.isEmpty {
                ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.moneytransfer_input_message_touch_clear)
            }
        }.disposed(by: disposeBag)
        messageTextField.textField.rx.controlEvent(.editingDidEndOnExit).bind { [weak self] (_) in
            self?.amountTextField.textField.becomeFirstResponder()
        }.disposed(by: disposeBag)
        
        amountTextField.textField.rx.controlEvent(.editingDidBegin).bind {(_) in
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.moneytransfer_input_amount)
        }.disposed(by: disposeBag)
        amountTextField.textField.rx.controlEvent(.editingChanged).bind { [weak self] (_) in
            //Track clear amountTextField
            if let text = self?.amountTextField.textField.text, text.isEmpty {
                ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.moneytransfer_input_amount_touch_clear)
            }
        }.disposed(by: disposeBag)
        amountTextField.textField.rx.controlEvent(.editingDidEnd).bind { [weak self](_) in
            self?.messageTextField.textField.becomeFirstResponder()
        }.disposed(by: disposeBag)
        
        amountTextField.textField.rx.text.map({ $0?.isEmpty.not ?? false }).filter({ $0 == false }).bind { [weak self]  in
            self?.nextButton.isEnabled = $0
            if let keyboard = self?.amountTextField.textField.inputView as? ZPCalculateNumberPad  {
                keyboard.doneButton.isEnabled = $0
            }
        }.disposed(by: disposeBag)
    }
    
    func handleNextButtonEvent() {
        self.nextButton.rx.controlEvent(UIControlEvents.touchUpInside).bind {[weak self](_) in

            guard let wSelf = self else {
                return
            }
            if wSelf.validateUserInputData() {
                let totalValue = (wSelf.amountTextField.textField.text ?? "").onlyDigit().toInt64()
                let message = wSelf.messageTextField.textField.text ?? ""
                wSelf.setTextFieldResignFirstResponder()
                wSelf.presenter?.requestCreateOrder(totalValue: totalValue, message: message, userTransferData: wSelf.userTransferData)
            }
        }.disposed(by: disposeBag)
    }
    
    func handleTextFieldShowError() {
        (~amountTextField.eError).bind { [weak self] in
            guard let wSelf = self else {
                return
            }
            let result = ($0 as? NSNumber)?.boolValue.not ?? false
            wSelf.nextButton.isEnabled = result
        }.disposed(by: disposeBag)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !ZPDialogView.isDialogShowing() {
            self.amountTextField.textField.becomeFirstResponder()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.scrollView.contentSize = self.scrollViewContentSize()
        if UIView.isIPhone4() {
            self.scrollView.scroll(toBottom: false)
        }
    }
    
    func scrollViewContentSize() -> CGSize {
        let height = self.inputHeaderView.frame.origin.y + self.inputHeaderView.frame.size.height
        let width = self.view.frame.size.width
        return CGSize(width: width, height: height)
    }
    
    func setupView() {
        self.scrollView = UIScrollView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: CGFloat(self.scrollHeight())))
        self.view.addSubview(self.scrollView)
        self.scrollView.snp.makeConstraints{ (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.top.equalTo(0)
            make.height.equalTo(self.scrollHeight())
        }
        
        let headerView = TransferRecieveHeaderSwift.instanceFromNib()
        var frame = headerView.frame
        frame.origin.x = (self.view.frame.size.width - frame.size.width)/2
        
        headerView.frame = frame
        self.scrollView.addSubview(headerView)
        self.headerView = headerView as! TransferRecieveHeaderSwift
        let inputView = self.createInputView(headerView: headerView)
        
        self.inputHeaderView = inputView
        
        inputView.snp.makeConstraints { (make) in
            make.top.equalTo(headerView.snp.bottom).offset(15)
            make.left.equalTo(0)
            make.width.equalTo(UIScreen.main.applicationFrame.size.width)
            make.height.equalTo(120)
        }
        
        self.addTransferButton()
    }
    
    func addTransferButton() {
        self.nextButton = UIButton.init(type: UIButtonType.custom)
        self.nextButton.isEnabled = false
        self.nextButton.setupZaloPay()
        self.nextButton.setTitle(R.string_ButtonLabel_Next(), for: UIControlState.normal)
        self.view.addSubview(self.nextButton)
        if ZPKeyboardConfig.enableCalculateKeyboard(.transfer) {
            let numberPad: ZPCalculateNumberPad = ZPCalculateNumberPad(delegate: self)
            numberPad.doneButton.isEnabled = false
            self.amountTextField.textField.inputView = numberPad
        } else {
            let numberPad: PMNumberPad = PMNumberPad(delegate: self)
            numberPad.doneButton.isEnabled = false
            self.amountTextField.textField.inputView = numberPad
        }
        self.nextButton.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.right.equalTo(-10)
            make.height.equalTo(kZaloButtonHeight)
            make.top.equalTo(self.scrollView.snp.bottom).offset(self.buttonToScrollOffset())
        }
    }
    
    func createInputView(headerView: UIView) -> UIView{
        let inputView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 122))
        inputView.backgroundColor = UIColor.white
        self.amountTextField.lineInsets = UIEdgeInsets.init(top: 0, left: 10, bottom: 0, right: 0)
        self.amountTextField.textField.delegate = self
        self.messageTextField.textField.delegate = self
        
        self.amountTextField.textField.placeholder = R.string_TransferReceive_AmountPlaceHolder()
        self.messageTextField.textField.placeholder = R.string_TransferReceive_MessagePlaceHolder()
        
        inputView.addSubview(self.amountTextField)
        inputView.addSubview(self.messageTextField)
        self.scrollView.addSubview(inputView)
        
        self.amountTextField.textField.delegate = self
        
        NotificationCenter.default.addObserver(forName: Notification.Name.UITextFieldTextDidChange, object: nil, queue: OperationQueue.main) { [weak self](_) in
            guard let wSelf = self else {
                return
            }
            guard let textField = self?.amountTextField.textField, textField.isFirstResponder, let text = textField.text, text.isEmpty.not else {
                return
            }
            wSelf.textField(textField, updateText: text)
            let nText = textField.text ?? ""
            guard textField.superview is ZPCalFloatTextInputView else { return }
            if let nPosition = textField.position(from: textField.beginningOfDocument, offset: nText.count) {
                let range = textField.textRange(from: textField.endOfDocument, to: nPosition)
                DispatchQueue.main.async {
                    textField.selectedTextRange = range
                }
            }
        }
        
        self.amountTextField.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.right.equalTo(0)
        }
        self.messageTextField.snp.makeConstraints { (make) in
            make.bottom.equalTo(inputView.snp.bottom)
            make.left.equalTo(0)
            make.right.equalTo(0)
        }
        
        let line = ZPLineView()
        inputView.addSubview(line)
        line.alignToTop()
        return inputView
    }
    
    func setupVendorView() {
        let amoutString = self.userTransferData.transferMoney ?? ""
        let amount = amoutString.onlyDigit().toInt64()
        let warningString = TransferAppInfo.getWarningMessage(amount) 
        
        let lblWarning = self.createWarningView(warningString)
        self.view.addSubview(lblWarning)
        self.scrollView = UIScrollView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: CGFloat(self.scrollHeight())))
        self.view.addSubview(self.scrollView)
        
        self.scrollView.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.top.equalTo(lblWarning.snp.bottom)
            make.height.equalTo(self.scrollHeight())
        }
        
        self.headerView = TransferRecieveHeaderSwift.instanceFromNib() as! TransferRecieveHeaderSwift
        var frame = self.headerView.frame
        frame.size.width = self.view.frame.size.width
        frame.size.height = 110
        
        self.headerView.frame = frame
        self.headerView.backgroundColor = UIColor.white
        self.scrollView.addSubview(self.headerView)
        
        self.inputHeaderView = self.createVendorView()
        self.scrollView.addSubview(self.inputHeaderView)
        self.inputHeaderView.snp.makeConstraints { (make) in
            make.top.equalTo(self.headerView.snp.bottom)
            make.left.equalTo(0)
            make.width.equalTo(UIScreen.main.applicationFrame.size.width)
        }
        self.addTransferButton()
        self.nextButton.isEnabled = warningString.isEmpty
    }
    
    func createVendorView() -> UIView {
        self.amountTextField = ZPCalFloatTextInputView()
        self.amountTextField.textField.text = self.userTransferData.transferMoney
        self.messageTextField = ZPFloatTextInputView()
        self.messageTextField.textField.text = self.userTransferData.message
        
        let moneyString = (self.userTransferData.transferMoney ?? "0").onlyDigit().formatCurrency()
        let text = moneyString ?? ""
        let amountAttributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13)]
        let attString = NSMutableAttributedString(string: text)
        attString.addAttributes(amountAttributes, range:(text as NSString).range(of: "VND"))
        
        let inputView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 122))
        inputView.backgroundColor = UIColor.white
        
        let lblAmount = UILabel()
        lblAmount.textAlignment = NSTextAlignment.center
        lblAmount.font = UIFont.systemFont(ofSize: 27)
        lblAmount.attributedText = attString
        inputView.addSubview(lblAmount)
        lblAmount.snp.makeConstraints { (make) in
            make.top.equalTo(15)
            make.left.equalTo(8)
            make.right.equalTo(-8)
        }
        
        let lblMessage = UILabel()
        lblMessage.textAlignment = NSTextAlignment.center
        lblMessage.font = UIFont.systemFont(ofSize: 14)
        lblMessage.numberOfLines = 0
        lblMessage.text = self.userTransferData.message
        inputView.addSubview(lblMessage)
        lblMessage.snp.makeConstraints { (make) in
            make.top.equalTo(lblAmount.snp.bottom).offset(10)
            make.bottom.equalTo(-17)
            make.left.equalTo(50)
            make.right.equalTo(-50)
        }
        
        let topLine = ZPLineView()
        inputView.addSubview(topLine)
        topLine.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(1)
        }
        
        let bottomLine = ZPLineView()
        inputView.addSubview(bottomLine)
        bottomLine.alignToBottom()
        return inputView
    }
    
    func createWarningView(_ warningString: String) -> UILabel{
        var frame = CGRect.zero
        let lblWarning = UILabel()
        lblWarning.textAlignment = NSTextAlignment.center
        lblWarning.font = UIFont.systemFont(ofSize: 14)
        lblWarning.numberOfLines = 0
        lblWarning.backgroundColor = UIColor.mm_color(withHex: 0xffffc1)
        if warningString.count > 0 {
            frame.size.height = 40
        } else {
            frame.size.height = 0
        }
        
        frame.size.width = UIScreen.main.applicationFrame.size.width
        lblWarning.frame = frame
        lblWarning.text = warningString
        return lblWarning
    }
    
    // MARK: *** save transferData
    func saveTransferData(){
        let amount = self.amountTextField.textField.text ?? ""
        let message = self.messageTextField.textField.text ?? ""
        if  amount == self.userTransferData.transferMoney &&
            message == self.userTransferData.message {
            return
        }
        let data = ["amount" : amount,
                    "message": message]
        UIPasteboard.recentTransfer().transferData = data
    }
    
    func userInfo(user: UserTransferData) -> String {
        if user.mode != .toZaloPayID {
            let phone  = user.phone ?? ""
            if phone.count == 0 {
                return String(format: "%@: %@", R.string_TransferMoney_Phone_Number(), R.string_TransferMoney_Phone_None())
            }
            // only 4 last number phone
           
            return String(format: "%@: %@", R.string_TransferMoney_Phone_Number(), user.getPhoneDisplayFromSource())
        }
        var text = user.accountName ?? ""
        if text.count == 0 {
            text = R.string_TransferReceive_EmptyZaloPayId()
        }
        return String(format: R.string_TransferReceive_ZaloPayId(), text)
    }
    
    // MARK: *** UI Events
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        setTextFieldResignFirstResponder()
    }
    
    override func viewWillSwipeBack() {
        self.saveTransferData()
        self.moneyTransferProgressChanged(progress: MoneyTransferProgress.cancel)
    }
    
    func reloadUser(transferData: UserTransferData){
        if transferData.zaloId == self.userTransferData.zaloId {
            return
        }
        self.userTransferData = transferData
        self.presenter?.requestZaloPayIdIfNeed(userTransferData: userTransferData)
    }
    
    
    // MARK: *** Connect to presenter
    func loadTransferData() {
        if let moneyString = self.userTransferData.transferMoney,
            let moneyValue = moneyString.onlyDigit(),
            moneyValue.toInt() > 0 {
            
            self.amountTextField.textField.text = moneyValue.formatMoneyValue()
            self.messageTextField.textField.text = (self.userTransferData.message ?? "")
        } else {
            let dic = UIPasteboard.recentTransfer().transferData
            self.amountTextField.textField.text = dic?.string(forKey: "amount")
            self.messageTextField.textField.text = dic?.string(forKey: "message")
        }
        let money = self.amountTextField.textField.text ?? ""
        self.nextButton.isEnabled = money.onlyDigit().toInt() > 0
    }
    
    func showHub() {
        ZPAppFactory.sharedInstance().showHUDAdded(to: self.view)
    }
    
    func hideHub() {
        ZPAppFactory.sharedInstance().hideAllHUDs(for: self.view)
    }
    
    func showError(withText content: String) {
        ZPDialogView.showDialog(with: DialogTypeError, message: content, cancelButtonTitle: R.string_ButtonLabel_Close(), otherButtonTitle: nil, completeHandle: nil)
    }
    
    func setUserData(user: UserTransferData) {
        user.source = self.userTransferData.source
        user.mode = self.userTransferData.mode
        self.userTransferData = user
    }
    
    func showUserData(user: UserTransferData) {
        if let displayName = user.displayName, displayName.count > ZPConstants.transferNameMaxLength  {
            let index = displayName.index(displayName.startIndex, offsetBy: ZPConstants.transferNameMaxLength)
            let subString = String(displayName[..<index])
            self.headerView.labelName.text = "\(subString)..."
        } else {
            self.headerView.labelName.text = user.displayName
        }
        let avatUrl = user.avatar ?? ""
        self.headerView.avatar.sd_setImage(with: URL.init(string: avatUrl), placeholderImage: UIImage.defaultAvatar())
        self.headerView.labelPhone.text = userInfo(user: user)
    }
    
    func setTextFieldResignFirstResponder() {
        self.amountTextField.textField.resignFirstResponder()
        self.messageTextField.textField.resignFirstResponder()
    }
    
    func showBill(billInfo: ZPBill) {
        self.showBillViewController(billInfo)
        self.transId = billInfo.appTransID
        self.nextButton.isUserInteractionEnabled = true
    }
    
    func createOrderFailed(error: Error?){
        ZPDialogView.showDialogWithError(error, handle: {
            self.amountTextField.textField.becomeFirstResponder()
        })
        self.hideLoadingView()
        self.nextButton.isUserInteractionEnabled = true
    }
    
    func verifyOrderMessageFailed(error: Error?) {
        ZPDialogView.showDialogWithError(error, handle: {
            self.amountTextField.textField.becomeFirstResponder()
        })
        self.hideLoadingView()
        self.nextButton.isUserInteractionEnabled = true
    }
    
    func invalidOrderMessage(errorMessage: String, modified: String) {
        self.hideLoadingView()
        self.messageTextField.showError(withText: errorMessage)
        self.messageTextField.textField.text = modified
        self.messageTextField.textField.becomeFirstResponder()
        self.nextButton.isUserInteractionEnabled = true
    }
    
    // MARK: *** UIScrollView Delegate
    override func scrollHeight() -> Float {
        return 255
    }
    
    override func internalScrollView() -> UIScrollView! {
        return self.scrollView
    }
    
    override func internalBottomButton() -> UIButton! {
        return self.nextButton
    }
    
    override func buttonToScrollOffset() -> Float {
        return 40
    }
    
    // MARK: *** BillHandleBase Delegate
    override func payBillDidCompleteSuccess() {
        self.userTransferData.message = self.messageTextField.textField.text
        self.userTransferData.transferMoney = self.amountTextField.textField.text
        self.presenter?.payBillDidCompleteSuccess(data: self.userTransferData)
        UIPasteboard.recentTransfer().transferData = nil
        super.payBillDidCompleteSuccess()
    }
    
    override func paymentNotifySuccess(_ data: [AnyHashable : Any]!) {
        self.moneyTransferProgressChanged(progress: MoneyTransferProgress.success)
    }
    
    override func paymentNotifyFail(_ data: [AnyHashable : Any]!) {
        self.moneyTransferProgressChanged(progress: MoneyTransferProgress.fail)
    }
    
    func moneyTransferProgressChanged(progress: MoneyTransferProgress){
        if self.userTransferData.mode == .toZaloPayUser {
            self.userTransferData.transferMoney = self.amountTextField.textField.text
            var transId = String()
            if progress == MoneyTransferProgress.success {
                transId = self.transId
            }
            presenter?.sendNotifcationWhenQRTransfer(progress: progress, transId: transId, userTransferData: self.userTransferData)
        }
    }
    
    func textField(_ textField: UITextField?, updateText text: String?) {        
        guard let textField = textField,
              let text = text else {
                return
        }
        var enableButton = true
        if textField == self.amountTextField.textField {
            if text == "" {
                self.amountTextField.clearErrorText()
            }
            self.amountTextField.updateTextField(textField, updateText: text)
            if !self.amountTextField.isExpression() {
                let stringTotalValue = (self.amountTextField.textField.text ?? "").onlyDigit() ?? ""
                if stringTotalValue == "" {
                    enableButton = false
                }
                let totalValue = stringTotalValue == "" ? 0 : stringTotalValue.toInt64()
                if TransferAppInfo.isGreaterThanMax(totalValue) {
                    let max = ZPWalletManagerSwift.sharedInstance.config.maxTransfer
                    let money: String = "\(max)".formatCurrency()
                    let message = String(format: R.string_InputMoney_BigAmount_Error_Message(), money)
                    self.amountTextField.showErrorAuto(message)
                }
            }
        }
        let message = self.messageTextField.textField.text ?? ""
        if !(self.presenter?.validateMessageLength(message) ?? false)  {
            enableButton = false
        }
        if (self.amountTextField.textField.isShowError) {
            enableButton = false
        }
        
        setNextButtonEnable(isEnable: enableButton)
    }
}

extension TransferReceiverViewControllerSwift: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var newString = (textField.text ?? "") as NSString
        newString = newString.replacingCharacters(in: range, with: string) as NSString
        
        if textField == self.messageTextField.textField {
            return TransferAppInfo.validMessage(newString as String, textInput: self.messageTextField)
        }
        if textField != self.amountTextField.textField {
            return true;
        }
        
        self.amountTextField.clearErrorText()
        
        if self.amountTextField.textField.inputView is PMNumberPad {
            let totalValue = newString.onlyDigit().toInt64()
            return !TransferAppInfo.isGreaterThanMax(totalValue, textInput: self.amountTextField)
        }
        
        let str = (newString as String?) ?? ""
        if self.amountTextField.isExpression() &&  self.amountTextField.isMaxCharactor(str){
            self.amountTextField.showErrorAuto(R.string_InputMoney_Max_Character_Error_Message())
            return false
        }
        
        if self.amountTextField.isGreaterThanMaxInputValue(str) {
            let max = (kMaxInputValue as NSNumber).formatMoneyValue() ?? ""
            let message = String.init(format: R.string_TransferMoney_Max_Input_Value(), max)
            self.amountTextField.showErrorAuto(message)
            return false
        }
        return true
    }
}

extension TransferReceiverViewControllerSwift:  PMCNumberPadControl {

    func pmcKeyboard(_ keyboard: ZPCalculateNumberPad!, checkMinvalue value: Int64, fromFloatTextField textInput: ZPFloatTextInputView!) {
           TransferAppInfo.isLessThan(value, textInput: textInput)
    }
    
    func pmcContinueButtonClicked(_ sender: Any?) {
        _ = validateUserInputData()
    }
    
    func evaluateExpressions() -> Bool{
        guard let numberPad = self.amountTextField.textField.inputView as? ZPCalculateNumberPad else {
            return false
        }
        if numberPad.isExpression(self.amountTextField.textField.text) {
            return self.evaluateExpressions(numberPad: numberPad)
        }
        return true
    }
    
    func validateUserInputData() -> Bool {
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.moneytransfer_input_touch_continue)
        if ZPKeyboardConfig.enableCalculateKeyboard(.transfer) {
           return evaluateExpressions() && validInputData()
        } else {
            guard self.amountTextField.textField.inputView is PMNumberPad else {
                return false
            }
            return validInputData()
        }
    }
}

extension TransferReceiverViewControllerSwift:  PMNumberPadControl {
    func pmContinueButtonClicked(_ sender: Any!) {
        _ = validateUserInputData()
    }
}
// MARK: *** APNumberPadDelegate Delegate
extension TransferReceiverViewControllerSwift: APNumberPadDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField != self.amountTextField.textField {
            return
        }
        guard let numberPad = self.amountTextField.textField.inputView as? ZPCalculateNumberPad else {
            return
        }
        if !numberPad.isExpression(self.amountTextField.textField.text) {
            return
        }
        let result = numberPad.calc(self.amountTextField)
        if numberPad.calcError == ZPCalculationError.calc_Divide_By_Zero
            || numberPad.calcError == ZPCalculationError.calc_Wrong_Syntax {
            return
        }
        
        if TransferAppInfo.isLessThan(result, textInput: self.amountTextField, isFocusedTextInput: false) || TransferAppInfo.isGreaterThanMax(result, textInput: self.amountTextField, isFocusedTextInput: false) {
            return
        }
        
        self.amountTextField.textField.text = String(result).onlyDigit().formatMoneyValue()
    }
    
    typealias TypeInput = UIResponder & UITextInput
    
    func numberPad(_ numberPad: APNumberPad!, functionButtonAction functionButton: UIButton!, textInput: TypeInput!) {
        if let numberPad = numberPad as? ZPCalculateNumberPad {
            numberPad.control(self.amountTextField, with: functionButton, viewController: self)
            if functionButton == numberPad.doneButton {
                self.amountTextField.textField.resignFirstResponder()
            }
            return
        }
        
        if let numberPad = numberPad as? PMNumberPad  {
            let text: String = "000"
            if functionButton == numberPad.leftFunctionButton {
                let isInsert: Bool = self.textField(amountTextField.textField, shouldChangeCharactersIn: NSRange(location: (amountTextField.textField.text ?? "").count, length: 0), replacementString: text)
                if isInsert {
                    self.amountTextField.textField.insertText(text)
                }
            } else if functionButton == numberPad.doneButton {
                self.amountTextField.textField.resignFirstResponder()
                pmContinueButtonClicked(UIButton())
            }
        }
    }
}

extension TransferReceiverViewControllerSwift {
    func setNextButtonEnable(isEnable: Bool) {
        if let numberPad = self.amountTextField.textField.inputView as? ZPCalculateNumberPad {
            numberPad.doneButton.isEnabled = isEnable
        }
        if let numberPad = self.amountTextField.textField.inputView as? PMNumberPad {
            numberPad.doneButton.isEnabled = isEnable
        }
        self.nextButton.isEnabled = isEnable
    }
    
    func validInputData() -> Bool{
        guard let _ = self.userTransferData.paymentUserId else {
            ZPDialogView.showDialog(with: DialogTypeError, message: R.string_TransferReceive_GetInforErrorMessage(), cancelButtonTitle: R.string_ButtonLabel_Close(), otherButtonTitle: nil, completeHandle: nil)
            return false
        }
        let totalValue = (self.amountTextField.textField.text ?? "").onlyDigit().toInt64()
        
        if isEnoughBalanceMoneyWhenClearing(moneyTransfer: totalValue).not {
            let min = ZPWalletManagerSwift.sharedInstance.config.minTransfer
            let money = String(min).formatCurrency() ?? ""
            self.amountTextField.showError(withText: String(format:"%@ %@", R.string_Clearing_transfer_faild_min(), money))
            return false
        }
        
        let message = self.messageTextField.textField.text ?? ""
        if TransferAppInfo.isLessThan(totalValue, textInput: self.amountTextField) ||
            TransferAppInfo.isGreaterThanMax(totalValue, textInput:self.amountTextField) ||
            !(self.presenter?.validateMessageLength(message) ?? false) {
            self.setNextButtonEnable(isEnable: false)
            return false
        }
        return true
    }
    
    func isEnoughBalanceMoneyWhenClearing(moneyTransfer: Int64) -> Bool {
        let deltaBalance = (ZPWalletManagerSwift.sharedInstance.currentBalance?.int64Value ?? 0) - moneyTransfer
        if (ZPProfileManager.shareInstance.userLoginData?.isClearing ?? false) &&  deltaBalance < 0 {
            return false
        }
        if (ZPProfileManager.shareInstance.userLoginData?.isClearing ?? false) && deltaBalance > 0 && deltaBalance < minValueTransferClearing {
            return false
        }
        return true
    }
    
    func evaluateExpressions(numberPad: ZPCalculateNumberPad) -> Bool {
        let result = numberPad.calc(self.amountTextField)
        if numberPad.calcError == ZPCalculationError.calc_Divide_By_Zero
            || numberPad.calcError == ZPCalculationError.calc_Wrong_Syntax {
            return false
        }
        
        if result < 0 {
            TransferAppInfo.isLessThan(Int64(result), textInput: self.amountTextField)
            numberPad.doneButton.isEnabled = false
            self.nextButton.isEnabled = false
            return false
        }
        
        if isEnoughBalanceMoneyWhenClearing(moneyTransfer: result).not {
            self.amountTextField.showError(withText: R.string_Withdraw_NotEnoughMoney())
            return false
        }
        self.amountTextField.textField.text = String(result).onlyDigit().formatMoneyValue()
        return true
    }
}
