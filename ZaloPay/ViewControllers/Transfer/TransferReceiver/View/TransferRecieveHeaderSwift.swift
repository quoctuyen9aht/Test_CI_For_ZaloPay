//
//  TransferRecieveHeaderSwift.swift
//  ZaloPay
//
//  Created by nhatnt on 08/11/2017.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

class TransferRecieveHeaderSwift: UIView {
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelPhone: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.avatar.roundRect(Float(avatar.frame.size.width/2))
        self.backgroundColor = UIColor.gray
        self.backgroundColor = UIColor.clear
        self.labelName.adjustsFontSizeToFitWidth = true
        self.labelName.font = UIFont.sfuiTextRegular(withSize: 16)
        self.labelPhone.font = UIFont.sfuiTextRegular(withSize: 12)
        self.labelName.textColor = UIColor.defaultText()
        self.labelPhone.textColor = UIColor.subText()
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "TransferRecieveHeaderSwift", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
}
