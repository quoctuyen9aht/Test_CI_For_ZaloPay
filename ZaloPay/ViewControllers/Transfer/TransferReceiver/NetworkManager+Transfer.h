//
//  NetworkManager+Transfer.h
//  ZaloPay
//
//  Created by PhucPv on 8/30/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <ZaloPayNetwork/NetworkManager.h>
@interface NetworkManager(Transfer)
- (RACSignal *)sendNotificationWithReceiverId:(NSString *)receiverId
                                  embededData:(NSString *)embededData;
@end
