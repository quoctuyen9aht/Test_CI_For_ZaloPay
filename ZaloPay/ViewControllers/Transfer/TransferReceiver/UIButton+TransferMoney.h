//
//  UIButton+TransferMoney.h
//  ZaloPay
//
//  Created by PhucPv on 4/19/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface UIUnderlinedButton : UIButton
+ (UIUnderlinedButton*) underlinedButton;
@end

@interface UIButton(TransferMoney)
- (void)baseTransferMoney;
@end
