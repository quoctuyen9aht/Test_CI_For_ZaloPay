//
//  TransferReceiverPresenter.swift
//  ZaloPay
//
//  Created by nhatnt on 03/11/2017.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation

class TransferReceiverPresenter: TransferReceiverPresenterInterface {
    //Reference to the View (weak to avoid retain cycle)
    weak var view: TransferReceiverViewInterface?
    var interactor: TransferReceiverInteractorInput?
    
    func requestZaloPayIdIfNeed(userTransferData: UserTransferData) {
        let uid = userTransferData.paymentUserId ?? ""
        let accountName = userTransferData.accountName ?? ""
        
        if uid.isEmpty.not {
            self.view?.showUserData(user: userTransferData)
            return
        }
        if userTransferData.mode == .toZaloPayID && accountName.isEmpty.not {
            interactor?.fetchData(accountName: accountName)
            return
        }               
        self.view?.showLoadingView()
        interactor?.requestUserProfile(userTransferData: userTransferData)
    }
    
    func requestCreateOrder(totalValue: Int64, message: String, userTransferData: UserTransferData) {
        interactor?.requestCreateOrder(totalValue: totalValue, message: message, userTransferData: userTransferData)
    }
    
    func sendNotifcationWhenQRTransfer(progress: MoneyTransferProgress, transId: String, userTransferData: UserTransferData) {
        interactor?.sendNotifcationWhenQRTransfer(progress: progress, transId: transId, userTransferData: userTransferData)
    }
}

extension TransferReceiverPresenter: TransferReceiverInteractorOutput {
    func showUserData(user: UserTransferData) {
        self.view?.showUserData(user: user)
    }
    
    func showLoadingView() {
        self.view?.showLoadingView()
    }
    
    func loadSucceed(user: UserTransferData) {
        self.view?.setUserData(user: user)
        self.view?.hideHub()
        self.view?.showUserData(user: user)
    }
    
    func loadFailed(message: String) {
        self.view?.hideHub()
        self.view?.showError(withText: message)
    }
    
    func hideLoadingView() {
        self.view?.hideLoadingView()
    }
    
    func showBillViewController(billInfo: ZPBill) {
        self.view?.showBill(billInfo: billInfo)
    }
    
    func setTextFieldResignFirstResponder() {
        self.view?.setTextFieldResignFirstResponder()
    }
    
    func failCreateOrder(error: Error?) {
        self.view?.createOrderFailed(error: error)
    }
    
    func failVerifyOrderMessage(error: Error?) {
        self.view?.verifyOrderMessageFailed(error: error)
    }
    
    func invalidOrderMessage(errorMessage: String, modified: String) {
        self.view?.invalidOrderMessage(errorMessage: errorMessage, modified: modified)
    }
    
    func validateMessageLength(_ message: String) ->Bool {
        return self.interactor?.validateMessageLength(message) ?? false
    }
    
    func payBillDidCompleteSuccess(data: UserTransferData) {
        self.interactor?.saveRecentTransferFriend(data: data)
    }
}
