//
//  NetworkManager+FilterContent.h
//  ZaloPay
//
//  Created by Bon Bon on 5/22/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import <ZaloPayNetwork/NetworkManager.h>

@interface NetworkManager (FilterContent)
- (RACSignal *)filterTextContent:(NSString *)content;
@end
