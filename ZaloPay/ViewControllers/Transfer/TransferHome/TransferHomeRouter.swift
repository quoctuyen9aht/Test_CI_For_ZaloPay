//
//  TransferHomeRouter.swift
//  ZaloPay
//
//  Created tienlv on 4/17/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.


import UIKit
import RxSwift
import RxCocoa
import ZaloPayProfile

@objcMembers
class TransferHomeRouter: NSObject, TransferHomeWireframeProtocol {
    
    static func assembleModule() -> TransferHomeViewController {
        let view = TransferHomeViewController()
        let interactor = TransferHomeInteractor()
        let router = TransferHomeRouter()
        let presenter = TransferHomePresenter(interface: view, interactor: interactor, router: router)
        
        view.presenter = presenter
        interactor.presenter = presenter        
        return view;
    }
    
    func showTransferMoneyTo(_ data: UserTransferData) {
        let viewController = TransferReceiverRouter.assembleModule(transferData: data)
        self.showViewController(viewController: viewController)
    }
    
    func showTransferMoneyTo(contact: ZPContactSwift?, transferMode: TransferMode) {
        let transferUser = UserTransferData.fromZaloPayContact(contact!)
        transferUser.mode = transferMode
        transferUser.source = .fromTransferActivity
        self.showTransferMoneyTo(transferUser)
    }
    
    func showTransferToAccountRouter(){
        ZPTransferToAccountRouter.assembleModule();
    }
    
    func showTransferToAccount(){
        let controller = TransferToAccountViewController()
        self.showViewController(viewController: controller)
    }
    
    func showViewController(type:ActionModeTransfer, completion: @escaping (ZPContactSwift?,TransferMode) -> ()){
        switch type {
        case .toZaloPayContact:
            ZPContactListRouter.launchContactList(ContactMode_ZPC_All,
                                                  currentPhone: nil,
                                                  displayMode: .normal,
                                                  viewMode: ViewModeKeyboard_ABC,
                                                  navigationTitle: nil,
                                                  source: .fromTransferMoney_Contact,
                                                  completion: {contact in
                                                    completion(contact, .toZaloPayContact)
            })
            break
            
        case .toPhoneNumber:
            ZPContactListRouter.launchContactList(ContactMode_ZPC_All,
                                                  currentPhone: nil,
                                                  displayMode: .normal,
                                                  viewMode: ViewModeKeyboard_Phone,
                                                  navigationTitle: nil,
                                                  source: .fromTransferMoney_PhoneNumber,
                                                  completion: {contact in
                                                    completion(contact, .toPhoneNumber)
            })
            break
        default:
            break
        }
    }
    
    fileprivate func showViewController(viewController: UIViewController) {
        ZPAppFactory.sharedInstance().rootNavigation().pushViewController(viewController, animated: true)
    }
}
