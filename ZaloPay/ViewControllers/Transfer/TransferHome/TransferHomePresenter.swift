//
//  TransferHomePresenter.swift
//  ZaloPay
//
//  Created tienlv on 4/17/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.


import UIKit
import RxSwift
import RxCocoa
import ZaloPayAnalyticsSwift
import ZaloPayProfile


class TransferHomePresenter: NSObject, TransferHomePresenterProtocol {

    weak var view: TransferHomeViewProtocol?
    var interactor: TransferHomeInteractorProtocol?
    var router: TransferHomeWireframeProtocol?

    init(interface: TransferHomeViewProtocol, interactor: TransferHomeInteractorProtocol, router: TransferHomeWireframeProtocol) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }

    func trackLaunch(_ screenName:String){
        self.interactor?.trackLaunch(screenName)
    }
    
    func trackEvent(_ trackId: ZPAnalyticEventAction){
        self.interactor?.trackEvent(trackId)
    }
    
    func requestDataSource() {
        self.interactor?.configDataSource()
    }

    func openViewController(withData model: ActionDataEntity) {
        let type = model.type ?? .toZaloPayId
        if type == .toPhoneNumber || type == .toZaloPayContact  {
            ZPTrackingHelper.shared().trackEvent(.contact_launch_from_transfer)
        }
        
        if let data = model.data {
            self.router?.showTransferMoneyTo(data)
            return
        }
        switch model.type! {
        case .toZaloPayContact, .toPhoneNumber:
             ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.moneytransfer_touch_phonebook)
            self.router?.showViewController(type: model.type!,completion:{[weak self] contact, mode in
                self?.didSelectZaloUser(contact, transferMode: mode)
            })
            break
        case .toZaloPayId:
             ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.moneytransfer_touch_phonenumber)
            self.router?.showTransferToAccountRouter()
            break
        default:
             ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.moneytransfer_touch_zpid)
            self.router?.showTransferToAccount()
            break
        }
    }

    func didSelectZaloUser(_ contact: ZPContactSwift?, transferMode: TransferMode) {

        if contact == nil {
            return
        }
        if (self.interactor?.isCurrentUser(contact!))! {
            self.view?.alertTransferToCurrentUser()
            return
        }
        if contact?.status == UserStatus.Available {
            self.router?.showTransferMoneyTo(contact: contact, transferMode: transferMode)
        } else {
            self.view?.alertUserUnavailable(user: contact!)
        }
    }

    
    func retrieveDataSource(_ dataSource:[GroupEntity]){
        self.view?.reloadDataSource(dataSource)
    }

}


