//
//  TransferHomeViewController.swift
//  ZaloPay
//
//  Created tienlv on 4/17/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.


import UIKit
import ZaloPayAnalyticsSwift
import ZaloPayProfile

@objc class TransferHomeViewController: BaseViewController {

    var presenter: TransferHomePresenterProtocol?
    var dataSource: [GroupEntity]?
    var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = R.string_TransferHome_Title()
        self.addTableView()
        self.presenter?.requestDataSource()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ZPTrackingHelper.shared().trackScreen(with: self)
        self.presenter?.trackLaunch(self.screenName())
    }

    override func screenName() -> String! {
        return ZPTrackerScreen.Screen_iOS_MoneyTransfer
    }

    override func backEventId() -> Int {
        return ZPAnalyticEventAction.moneytransfer_touch_back.rawValue
    }

    override func backButtonClicked(_ sender: Any!) {
        super.backButtonClicked(sender)
    }

    override func viewWillSwipeBack() {
        UIPasteboard.recentTransfer().transferData = nil
    }

    func addTableView() {
        self.tableView = UITableView.init(frame: CGRect.zero, style: UITableViewStyle.grouped)
        self.view.addSubview(self.tableView)
        self.tableView.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.top.equalTo(0)
            make.bottom.equalTo(0)
        }
        self.tableView.backgroundColor = UIColor.init(hexValue: 0xFFFFFF)
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.sectionFooterHeight = 0.1
        self.tableView.sectionHeaderHeight = 0.1
    }
}

extension TransferHomeViewController: TransferHomeViewProtocol {
    
    func getUserDisplayName(_ user: ZPContactSwift) -> String {
        let displayName = user.displayName ?? ""
        if displayName.count > 0 && user.isSavePhoneNumber {
            return displayName
        }
        return user.phoneNumber ?? ""
    }
    
    func alertUserUnavailable(user: ZPContactSwift) {
        var message = ""
        let displayName = getUserDisplayName(user)
        if user.status == UserStatus.Lock || user.status == UserStatus.ReceiverLock {
            message = String(format: R.string_TransferMoney_Account_Locked(), displayName)
        } else {
            message = String(format: R.string_Account_Unavailable(), displayName)
        }
        
        ZPDialogView.showDialog(with: DialogTypeNotification,
                                message: message,
                                cancelButtonTitle: R.string_ButtonLabel_Close(),
                                otherButtonTitle: nil,
                                completeHandle: nil)
    }
    
    func alertTransferToCurrentUser() {
        let messsage = R.string_TransferToAccount_CurrentUserAccountMessage()
        ZPDialogView.showDialog(with: DialogTypeNotification,
                                message: messsage,
                                cancelButtonTitle: R.string_ButtonLabel_Close(),
                                otherButtonTitle: nil,
                                completeHandle: nil)
        
    }

    func reloadDataSource(_ dataSource:[GroupEntity]){
        self.dataSource = dataSource;
        self.tableView.reloadData()
    }
}

extension TransferHomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let groupModel = self.dataSource?[indexPath.section] {
            let item = groupModel.items[indexPath.row]
            return CGFloat((item.getCellClass() as! ZPTransferBaseCell.Type).getHeight())
        }
        return 0
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if let groupModel = self.dataSource?[section] {
            if ((groupModel.title?.count)! > 0) {
                return CGFloat(ZPTransferTitleCell.heightTBVCell())
            }
        }
        return 0.1
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let groupModel = self.dataSource?[section] {
            if (groupModel.title?.count == 0) {
                return nil
            }
            let cell = ZPTransferTitleCell()
            cell.labelTitle.text = groupModel.title

            return cell
        }
        return nil
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let groupModel = self.dataSource?[indexPath.section] {
            let item = groupModel.items?[indexPath.row]
            if let _ = item as? TransferRecentEntity,
            let eventAction = ZPAnalyticEventAction.init(rawValue: ZPAnalyticEventAction.moneytransfer_touch_recent1.rawValue.advanced(by: indexPath.row)) {
                if indexPath.row > 10 {
                    self.presenter?.trackEvent(ZPAnalyticEventAction.moneytransfer_touch_recent10plus)
                }
                self.presenter?.trackEvent(eventAction)
            }
            self.presenter?.openViewController(withData: (item?.actionData)!)
        }
    }

    func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        if ((self.dataSource?.count)! >= 2) {
            return 0
        }
        return 378
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if ((self.dataSource?.count)! >= 2) {
            return nil
        }

        return ZPTransferHomeBottomView.zp_viewFromSameNib()
    }
}

extension TransferHomeViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return (self.dataSource?.count)!
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let groupModel = self.dataSource?[section] {
            return groupModel.items!.count
        }
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let groupModel = self.dataSource?[indexPath.section] {
            let item = groupModel.items[indexPath.row]
            let classType = item.getCellClass() as! ZPTransferBaseCell.Type
            let cell = classType.zp_cellFromSameNib(for: tableView)
            var offset = 60
            let lastItem = groupModel.items.last
            if (item === lastItem) {
                offset = 0
            }
            cell.setItem(item: item, offset: offset)
            return cell
        }
        return UITableViewCell()
    }
}
