//
//  ZPTransferHomeBottomView.swift
//  ZaloPay
//
//  Created by tienlv on 4/18/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift

class ZPTransferHomeBottomView: UIView {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    
    lazy var delay:Observable<Int> = Observable<Int>.interval(1.5, scheduler: MainScheduler.instance)
    fileprivate let disposeBag = DisposeBag()
    fileprivate var isStartAnimate: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
        self.labelTitle.font = UIFont.sfuiTextRegular(withSize:13)
        self.labelTitle.textColor = UIColor.subText()
        self.labelTitle.numberOfLines = 0
        self.labelTitle.textAlignment = .center
        self.labelTitle.text = R.string_TransferHome_BottomTitle()
        
        self.imageView.image = UIImage.fromInternalApp("app_ios_transfer_empty_4")
        let images = (1...4).map({ UIImage.fromInternalApp("app_ios_transfer_empty_\($0)") })
        self.imageView.animationDuration = 0.5
        self.imageView.animationRepeatCount = 1
        self.imageView.animationImages = images
        
        if UIScreen.main.bounds.height == 568 { // IPhone5 or IPhoneSE
            updateView()
        } else if UIView.isIPhone4() {
            updateViewForIPhone4()
        }
        
        self.startAnimation()
    }
    
    func updateViewForIPhone4() {
        self.imageView.snp.updateConstraints { (make) in
            make.top.equalTo(self).offset(15)
            make.height.equalTo(130)
            make.width.equalTo(200)
        }
        
        self.labelTitle.snp.updateConstraints { (make) in
            make.top.equalTo(self.imageView.snp.bottom).offset(5)
        }
    }

    
    func updateView() {
        self.imageView.snp.updateConstraints { (make) in
            make.top.equalTo(self).offset(45)
        }
        
        self.labelTitle.snp.updateConstraints { (make) in
            make.top.equalTo(self.imageView.snp.bottom).offset(25)
        }
    }
    
    func repeatAnimation() {
        self.imageView.image = UIImage.fromInternalApp("app_ios_transfer_empty_4")
        self.imageView.startAnimating()
    }
    
    func startAnimation() {
        delay.startWith(1).subscribe(onNext: { [unowned self](_) in
            if !self.isStartAnimate {
                self.repeatAnimation()
            }else {
                self.imageView.image = UIImage.fromInternalApp("app_ios_transfer_empty_1")
            }
            self.isStartAnimate = !self.isStartAnimate
            
        }).disposed(by: disposeBag)
        
    }
    

}
