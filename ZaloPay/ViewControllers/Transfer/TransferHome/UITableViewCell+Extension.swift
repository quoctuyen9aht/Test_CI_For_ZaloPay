//
//  UITableViewCell+Extension.swift
//  ZaloPay
//
//  Created by tienlv on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import Foundation

protocol CellProtocol {}

extension CellProtocol where Self: UITableViewCell {
    static func zp_cellFromSameNib(for tableView: UITableView) -> Self {
        var cell = tableView.dequeueReusableCell(withIdentifier: self.identify())
        if cell == nil {
            cell = self.zp_viewFromSameNib()
        }
        guard let myCell = cell as? Self else {
            fatalError("Can't Find Cell!!!")
        }
        return myCell
    }
    
    static func zp_defaultCell(for tableView: UITableView) -> Self? {
        var cell = tableView.dequeueReusableCell(withIdentifier: self.identify())
        if cell == nil {
            cell = self.init(style: .default, reuseIdentifier: self.identify())
        }
        return cell as? Self
    }
    
    static func zp_cell(for tableView: UITableView) -> Self? {
        return self.zp_defaultCell(for: tableView)
    }

}
extension UITableViewCell: CellProtocol {}


extension UITableViewCell {
    
    public class func identifyTBVCell() -> String {
        return  "\(type(of:self))"
    }
    public class func heightTBVCell() -> Float {
        return 39.0
    }
}
