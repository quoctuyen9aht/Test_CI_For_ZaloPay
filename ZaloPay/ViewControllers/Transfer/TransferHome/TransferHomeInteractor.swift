//
//  TransferHomeInteractor.swift
//  ZaloPay
//
//  Created tienlv on 4/17/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.


import UIKit
import ZaloPayProfile
import ZaloPayAnalyticsSwift

class TransferHomeInteractor: TransferHomeInteractorProtocol {
    weak var presenter: TransferHomePresenterProtocol?
    var recentTransferStorage = RecentTransferStorage()
    
    func configDataSource(){
        
        var items = [GroupEntity]()
        items.append(recentTransferStorage.transferCategoryItems())
        if let recentItem = recentTransferStorage.recentTransferItems() {
            items.append(recentItem)
        }
        self.presenter?.retrieveDataSource(items)
    }
    
    func trackLaunch(_ screenName:String){
        ZPTrackingHelper.shared().trackScreen(withName:screenName)
        self.trackEvent(ZPAnalyticEventAction.moneytransfer_launched)
    }
    
    func trackEvent(_ trackId: ZPAnalyticEventAction) {
        ZPTrackingHelper.shared().trackEvent(trackId)
    }
    
    func isCurrentUser(_ user: ZPContactSwift) -> Bool {
        guard let current = ZPProfileManager.shareInstance.userLoginData else {
            return false
        }
        
        if (user.zaloPayId.isEmpty) {
            return false
        }
//        guard let userZaloPayId = user.zaloPayId  else {
//            return false
//        }
        return current.paymentUserId == user.zaloPayId
    }
    
    
}

class RecentTransferStorage: NSObject {
    private let recentTransferMoneyTable: ZPRecentTransferMoneyTable = ZPRecentTransferMoneyTable(db: PerUserDataBase.sharedInstance())
    
    func transferCategoryItems() -> GroupEntity {
        var items = [TransferCategoryEntity]()
        var category = TransferCategoryEntity.init(title: R.string_TransferHome_ToZaloPayContact(),
                                                   imageName: "sendmoney_contacts",
                                                   description: R.string_TransferHome_DescriptionToZaloPayContact(),
                                                   type: TransferCategoryType.zaloPayContact,
                                                   color: UIColor.zaloBase());
        
        items.append(category)
        category = TransferCategoryEntity.init(title: R.string_TransferHome_ToPhoneNumber(),
                                               imageName: "sendmoney_phonenumber",
                                               description: R.string_TransferHome_DescriptionToPhoneNumber(),
                                               type: TransferCategoryType.phoneNumber,
                                               color: UIColor.zp_green());
        items.append(category)
        
        // ZaloPayId select now moved to ZPC
//        category = TransferCategoryEntity.init(title: R.string_TransferHome_ToZaloPayID(),
//                                               imageName: "sendmoney_zalopayid",
//                                               description: R.string_TransferHome_DescriptionToZaloPayID(),
//                                               type: TransferCategoryType.zaloPayId,
//                                               color: UIColor.zp_yellow());
//        items.append(category)
        return GroupEntity.init(title: "", items: items)
    }
    
    func recentTransferItems() -> GroupEntity? {
        let recentList = recentTransferMoneyTable.recentTransferFriends() as? [UserTransferData] ?? []
        if (recentList.count == 0) {
            return nil
        }
        let items = recentList.map({ item -> TransferRecentEntity in
            
            var iconName: String?
            var iconColor: UIColor?
            let mode = TransferMode(rawValue: Int(item.mode.rawValue))
            if let m = mode {
                iconName = self.iconName(m)
                iconColor = self.color(m)
            }
            
            return TransferRecentEntity(title: item.displayName,
                                        imageName: item.avatar,
                                        description: item.description,
                                        type: mode,
                                        typeString: item.accountName,
                                        iconName: iconName,
                                        iconColor: iconColor,
                                        user: item)
            
        });
        
        return GroupEntity(title: R.string_TransferMoney_RecentTransaction(), items: items)
    }
    
    fileprivate func iconName(_ type: TransferMode) -> String {
        switch type {
        case .toZaloPayContact:
            return "sendmoney_contacts"
        case .toZaloPayID:
            return "sendmoney_zalopayid"
        case .toPhoneNumber, .toZaloPayUser:
            return "sendmoney_phonenumber"
        }
    }
    
    fileprivate func color(_ type: TransferMode) -> UIColor {
        switch type {
        case .toZaloPayContact:
            return UIColor.zaloBase()
        case .toZaloPayID:
            return UIColor.zp_yellow()
        case .toPhoneNumber, .toZaloPayUser:
            return UIColor.zp_green()
        }
    }
}
