//
//  ZPTransferTitleCell.swift
//  ZaloPay
//
//  Created by tienlv on 4/20/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayCommonSwift

class ZPTransferTitleCell: ZPTransferBaseCell {

    lazy var labelTitle: UILabel! = {
        let labelTitle = UILabel().build({
            $0.zpBoxTitleGayRegular()
            $0.backgroundColor = .clear
            self.contentView.addSubview($0)
            $0.snp.makeConstraints { (make) in
                make.top.equalTo(0)
                make.bottom.equalTo(10)
                make.left.equalTo(12)
                make.width.equalTo(self.frame.size.width)
                make.height.equalTo(15)
            }
            self.setupCell()
        })
        
        return labelTitle
    }()
    
    func setupCell() {
        backgroundColor = UIColor(hexValue: 0xffffff)
        contentView.backgroundColor = UIColor(hexValue: 0xffffff)
        self.selectionStyle = .none
    }
    
    override class func height() -> Float {
        return 39
    }
    
}
