//
//  TransferEntity.swift
//  ZaloPay
//
//  Created by tienlv on 4/17/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import Foundation

@objc enum TransferMode: Int {
    case toZaloPayContact  = 0
    case toZaloPayID       = 4
    case toZaloPayUser     = 5
    case toPhoneNumber     = 6
}

/*
 toZaloPayContact        : chuyển tiền đến danh bạ
 toZaloPayID             : chuyển tiền đến zalopay id
 toZaloPayUser           : chuyển tiền QR
 toPhoneNumber           : chuyển tiền đến số điện thoại
 */

@objc enum ActivateSource: Int {
    case fromTransferActivity   = 0
    case fromQRCodeType1        = 1
    case fromQRCodeType2        = 2
    case fromQRCodeType3        = 3
    case fromZalo               = 4
    case fromWebAppQRType2      = 5
}

@objc enum QRTransferType: Int {
    case type1 = 1
    case type2 = 2
    case type3 = 3
}

enum TransferCategoryType: Int {
    case zaloPayId  = 0
    case zaloPayContact  = 1
    case phoneNumber = 2
}

enum ActionModeTransfer: Int {
    case toZaloPayId         =  0
    case toZaloPayContact    =  1
    case toPhoneNumber       =  2
    case toRecentTransaction =  3
}

class TransferModeEntity {
    var imageName: String?
    var title: String?
    var description : String?
    var actionData: ActionDataEntity?
    init(imageName: String?, title: String?, description : String?) {
        self.imageName = imageName
        self.title = title
        self.description = description
    }
    
    func getCellClass() -> AnyClass {
        return ZPTransferBaseCell.self
    }
}

class TransferCategoryEntity: TransferModeEntity {
    var type: TransferCategoryType?
    var iconColor: UIColor?
    init(title: String,
         imageName: String,
         description : String,
         type: TransferCategoryType,
         color: UIColor) {
        super.init(imageName: imageName, title: title, description: description)
        self.type = type
        self.iconColor = color
        self.actionData = ActionDataEntity(type: ActionModeTransfer(rawValue: type.rawValue)!, data: nil)
    }
    override func getCellClass() -> AnyClass  {
        return ZPTransferCategoryCell.self
    }
}

class TransferRecentEntity : TransferModeEntity {
    var type: TransferMode?
    var typeString: String?
    var iconColor: UIColor?
    var iconName: String?
    var user: UserTransferData?
    init(title: String?,
         imageName: String?,
         description : String?,
         type: TransferMode?,
         typeString: String?,
         iconName: String?,
         iconColor: UIColor?,
         user: UserTransferData?) {
        super.init(imageName: imageName, title: title, description: description)
        self.type = type
        self.typeString = typeString
        self.iconName = iconName
        self.iconColor = iconColor
        self.user = user
        self.actionData = ActionDataEntity(type: ActionModeTransfer.toRecentTransaction, data: user)
    }
    override func getCellClass() -> AnyClass  {
        return ZPTransferRecentCell.self
    }
}

struct ActionDataEntity {
    var type: ActionModeTransfer?
    var data: UserTransferData?
    init(type: ActionModeTransfer?, data: UserTransferData?) {
        self.type = type
        self.data = data
    }
}
