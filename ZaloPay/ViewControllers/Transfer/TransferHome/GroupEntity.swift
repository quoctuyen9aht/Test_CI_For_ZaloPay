//
//  GroupEntity.swift
//  ZaloPay
//
//  Created by tienlv on 4/17/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import Foundation

class GroupEntity {
    var title: String?
    var items: [TransferModeEntity]!
    init(title: String, items: [TransferModeEntity]) {
        self.title = title
        self.items = items
    }
}
