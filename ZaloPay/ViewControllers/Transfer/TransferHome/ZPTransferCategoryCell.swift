//
//  ZPTrabsferCategoryCell.swift
//  ZaloPay
//
//  Created by tienlv on 4/18/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import Foundation

@objc class ZPTransferCategoryCell: ZPTransferBaseCell {
    @IBOutlet weak var imageIcon: ZPIconFontImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.labelTitle.adjustsFontSizeToFitWidth = true
        self.labelDescription.zpDetailGrayStyle()
        self.labelDescription.adjustsFontSizeToFitWidth = true
        self.labelTitle.autoresizingMask = [.flexibleWidth, .flexibleRightMargin]
    }
    
    override func setItem(item: TransferModeEntity, offset: Int) {
        super.setItem(item: item, offset: offset)
        self.addLineView()
        self.lineView.alignToBottomWith(leftMargin: offset)
        let category = item as! TransferCategoryEntity
        self.labelTitle.text = category.title
        self.labelDescription.text = category.description
        self.imageIcon.setIconFont(category.imageName)
        self.imageIcon.setIconColor(category.iconColor)
    }
    
    override class func getHeight() ->Float {
        return 72.0
    }
}
