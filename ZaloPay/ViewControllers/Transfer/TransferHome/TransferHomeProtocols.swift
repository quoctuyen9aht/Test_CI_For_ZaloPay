//
//  TransferHomeProtocols.swift
//  ZaloPay
//
//  Created tienlv on 4/17/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//
//

import Foundation
import RxSwift
import RxCocoa
import ZaloPayAnalyticsSwift
import ZaloPayProfile

//MARK: Wireframe -
protocol TransferHomeWireframeProtocol: class {
    func showTransferMoneyTo(_ data: UserTransferData)
    func showTransferMoneyTo(contact: ZPContactSwift?, transferMode: TransferMode)
    func showViewController(type:ActionModeTransfer, completion: @escaping (ZPContactSwift?,TransferMode) -> ())
    func showTransferToAccountRouter()
    func showTransferToAccount()
}
//MARK: Presenter -
protocol TransferHomePresenterProtocol: class {
    var view: TransferHomeViewProtocol? {get set}
    var interactor: TransferHomeInteractorProtocol? {get set}
    var router: TransferHomeWireframeProtocol? {get set}
    
    // View -> Presenter
    func trackLaunch(_ screenName:String)
    func trackEvent(_ trackId: ZPAnalyticEventAction)
    func requestDataSource()
    func openViewController(withData model: ActionDataEntity)
    
    // Interactor -> Presenter
    func retrieveDataSource(_ dataSource:[GroupEntity])
}

//MARK: Interactor -
protocol TransferHomeInteractorProtocol: class {
    var presenter: TransferHomePresenterProtocol?  { get set }
    
    // Presenter -> Interactor
    func configDataSource()
    func isCurrentUser(_ user: ZPContactSwift) -> Bool
    func trackLaunch(_ screenName:String)
    func trackEvent(_ trackId: ZPAnalyticEventAction)
}

//MARK: View -
protocol TransferHomeViewProtocol: class {
    var presenter: TransferHomePresenterProtocol?  { get set }
    func alertTransferToCurrentUser()
    func alertUserUnavailable(user:ZPContactSwift)
    func reloadDataSource(_ dataSource:[GroupEntity])
}
