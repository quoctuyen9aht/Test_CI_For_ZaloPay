//
//  ZPTransferRecentCell.swift
//  ZaloPay
//
//  Created by tienlv on 4/18/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import Foundation

@objc class ZPTransferRecentCell: ZPTransferBaseCell {
    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelType: UILabel!
    var imageType: ZPIconFontImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imageIcon.zp_roundRect((Float)(self.imageIcon.frame.size.width/2))
        self.imageIcon.contentMode = .scaleAspectFill
        
        self.labelTitle.adjustsFontSizeToFitWidth = true
        self.labelTitle.autoresizingMask = [.flexibleWidth, .flexibleRightMargin]
        self.labelType.zpDetailGrayStyle()
        self.imageType = ZPIconFontImageView.init(frame: CGRect(x: UIScreen.main.bounds.size.width-30,y: 20, width: 20, height: 20))
        self.addSubview(self.imageType)
        self.imageIcon.backgroundColor = .gray
        self.labelType.textColor = UIColor.subText()
    }
    
    override func setItem(item: TransferModeEntity, offset: Int) {
        super.setItem(item: item, offset: offset)
        let recent = item as! TransferRecentEntity
        var title = R.string_Transfer_ZaloPayId()
        var account = ""
        let transferType = recent.type!
        
        if (transferType == .toZaloPayID) {
            title = R.string_Transfer_ZaloPayId()
            account = recent.typeString!
            if ((account.count) == 0) {
                account = R.string_TransferHome_EmptyZaloPayId()
            }
        }
        else {
            title = R.string_Transfer_PhoneNumber()
            if let user = recent.user {
                account = user.getPhoneDisplayFromSource()
            }
        }
        
        if recent.title == nil {
            if let user = recent.user {
                 self.labelTitle.text = user.displayName
            }
        } else {
            self.labelTitle.text = recent.title
        }
        
        self.labelType.text = String(format:"%@: %@", title!, account)
        self.imageType.setIconFont(recent.iconName)
        self.imageType.setIconColor(recent.iconColor)
        self.imageIcon.sd_setImage(with: URL(string: item.imageName!), placeholderImage: UIImage(named:"profile_avartar"))
    }
    
  
    override class func getHeight() ->Float {
        return 70.0
    }
}
