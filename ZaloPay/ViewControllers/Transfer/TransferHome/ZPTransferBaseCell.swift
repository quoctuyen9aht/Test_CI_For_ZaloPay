//
//  ZPTransferBaseCell.swift
//  ZaloPay
//
//  Created by tienlv on 4/18/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayCommonSwift

class ZPTransferBaseCell: UITableViewCell {
    var lineView: ZPLineView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.lineView = ZPLineView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.lineView = ZPLineView()
    }
    
    func addLineView() {
        self.addSubview(self.lineView)
    }
    
    func setItem(item: TransferModeEntity, offset: Int) {        
    }
    
    class func getHeight() ->Float {
        return 74.0
    }
}
