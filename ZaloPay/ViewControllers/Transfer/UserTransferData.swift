//
//  UserTransferData.swift
//  ZaloPay
//
//  Created by Bon Bon on 2/22/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayProfile
import ZaloPayProfilePrivate

let kMinPhoneNumberLength      =  10
let kNumberLength              =  4

@objcMembers
class UserTransferData: NSObject {
    var zaloId: String?
    var avatar: String?
    var displayName: String?
    var phone: String?
    var transferMoney: String?
    var message: String?
    var paymentUserId: String?
    var accountName: String?
    var mode: TransferMode = .toZaloPayContact
    var source: ActivateSource = .fromTransferActivity
    
    class func fromZaloPayContact(_ user: ZPContactSwift) -> UserTransferData {
        let displayNameUCB = user.displayNameUCB ?? ""
        let zaloId = user.zaloId ?? ""
        var phone = user.phoneNumber ?? ""
        let _return = UserTransferData()
        _return.zaloId = user.zaloId;
        _return.avatar = user.avatar;
        _return.displayName = displayNameUCB.isEmpty.not ? user.displayNameUCB : user.displayNameZLF;
        _return.paymentUserId = user.zaloPayId;
        _return.accountName = user.zaloPayName;
        
        phone = (phone as NSString).formatPhoneNumber();
        //chuyển tiền từ danh bạn chọn bạn bè zalo không có trong danh bạ.
        if (displayNameUCB.isEmpty && zaloId.isEmpty.not && phone.count >= kMinPhoneNumberLength) {
            phone = _return.formatPhoneNumber(phone)
        }
        _return.phone = phone;
        return _return;
    }
    
    class func fromZaloUser(_ user: ZaloUserSwift) -> UserTransferData {
        let _return = UserTransferData()
        _return.zaloId = user.userId;
        _return.avatar = user.avatar;
        _return.displayName = user.displayName;
        _return.paymentUserId = user.zaloPayId;
        _return.phone = user.phone.formatPhoneNumber();
        _return.accountName = user.zaloPayName;
        return _return;
    }
    
    func getPhoneDisplayFromSource() -> String {
        let phone = self.phone ?? ""
        if phone.isEmpty {
            return R.string_TransferMoney_Phone_None()
        }
        if (phone.count < kMinPhoneNumberLength) {
            return phone;
        }
        // chuyển tiền từ QR có mode = TransferToZaloPayUser
        if (self.mode == .toZaloPayUser) {
            return formatPhoneNumber(phone)
        }
        let sources: [ActivateSource] = [.fromQRCodeType1, .fromQRCodeType2, .fromQRCodeType3, .fromZalo, .fromWebAppQRType2]
        if sources.contains(self.source) {
             return formatPhoneNumber(phone)
        }
        return phone;
    }
    
    func formatPhoneNumber(_ phone: String) -> String {
        let shortPhone = (phone as NSString).substring(from: phone.count - kNumberLength)
        return String.init(format:R.string_TransferMoney_ZaloAccount_PhoneNumber() , shortPhone)
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        guard let other = object as? UserTransferData else {
            return false
        }
        return self.paymentUserId == other.paymentUserId
    }
}
