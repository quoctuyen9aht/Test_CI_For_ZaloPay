//
//  TransferInfo.m
//  ZaloPay
//
//  Created by PhucPv on 8/30/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "TransferAppInfo.h"
#import <ZaloPayCommon/MF_Base64Additions.h>
#import "UserTransferData.h"
#import <ZaloPayCommon/ZPFloatTextInputView.h>
#import <ZaloPayCommon/ZPAppFactory.h>
#import <ZaloPayCommon/Collection+P2PNotification.h>
#import <ZaloPayCommonSwift/ZaloPayCommonSwift-Swift.h>
//#import "ApplicationState+GetConfig.h"
#import ZALOPAY_MODULE_SWIFT
#define kTransferQRType     1

@implementation TransferAppInfo

+ (NSString *)embededDataWithDisplayName:(NSString *)displayName
                                  avatar:(NSString *)avatar
                   moneyTransferProgress:(MoneyTransferProgress)progress
                                  amount:(int64_t)amount
                                 transId:(NSString *)transId {
    
    return [NSString embededDataQRTransferWithDisplayName:displayName
                                                   avatar:avatar
                                                 progress:progress
                                                   amount:amount
                                                  transId:transId];
}

+ (BOOL)validMessage:(NSString *)message textInput:(ZPFloatTextInputView *)textInputView{
    BOOL result = [self validMessage:message];
    if (!result) {
        NSString *errorMessage = [NSString stringWithFormat:[R string_InputNode_Error_Message], [ApplicationState getIntConfigFromDic:@"transfer"
                                                                                                                               andKey:@"max_length_message"
                                                                                                                           andDefault:TRANSFER_MESSAGE_LENGTH]];
        [textInputView showErrorWithText:errorMessage];
    } else {
        [textInputView clearErrorText];
    }
    return result;
}

+ (BOOL)isLessThan:(int64_t)totalValue textInput:(ZPFloatTextInputView *)textInputView isFocusedTextInput:(BOOL)focused {
    BOOL result = [self isLessThan:totalValue];
    if (result) {
        int64_t min = [ZPWalletManager sharedInstance].config.minTransfer;
        NSString *money = [@(min) formatCurrency];
        NSString *message = [NSString stringWithFormat:[R string_InputMoney_SmallAmount_Error_Message], money];
        [textInputView showErrorWithText:message];
//        [[ZPAppFactory sharedInstance].eventTracker trackEvent:EventAction_MoneyTransfer_InputSmallAmount
//                                                         value:@(totalValue)];
        if(focused) {
            [textInputView.textField becomeFirstResponder];
        }
    }
    return result;
}



+ (BOOL)isGreaterThanMax:(int64_t)totalValue textInput:(ZPFloatTextInputView *)textInputView isFocusedTextInput:(BOOL)focused{
    BOOL result = [self isGreaterThanMax:totalValue];
    if (result) {
        int64_t max = [ZPWalletManager sharedInstance].config.maxTransfer;
        NSString *money = [@(max) formatCurrency];
        NSString *message = [NSString stringWithFormat:[R string_InputMoney_BigAmount_Error_Message], money];
        [textInputView showErrorAuto:message];
//        [[ZPAppFactory sharedInstance].eventTracker trackEvent:EventAction_MoneyTransfer_InputBigAmount
//                                                         value:@(totalValue)];
        if(focused) {
            [textInputView.textField becomeFirstResponder];
        }
    }
    return result;
}

+ (BOOL)isLessThan:(int64_t)totalValue textInput:(ZPFloatTextInputView *)textInputView {
    return [self isLessThan:totalValue textInput:textInputView isFocusedTextInput:TRUE];
}



+ (BOOL)isGreaterThanMax:(int64_t)totalValue textInput:(ZPFloatTextInputView *)textInputView {
    return [self isGreaterThanMax:totalValue textInput:textInputView isFocusedTextInput:TRUE];
}

+ (BOOL)validMessage:(NSString *)message {
    return message.length <= [ApplicationState getIntConfigFromDic:@"transfer"
                                                            andKey:@"max_length_message"
                                                        andDefault:TRANSFER_MESSAGE_LENGTH];
}

+ (BOOL)isLessThan:(int64_t)totalValue {
    return totalValue < [ZPWalletManager sharedInstance].config.minTransfer;
}

+ (BOOL)isGreaterThanMax:(int64_t)totalValue{
    return totalValue > [ZPWalletManager sharedInstance].config.maxTransfer;
}
+ (NSString *)getWarningMessage:(int64_t)totalValue{
    if ([self isGreaterThanMax:totalValue]) {
        int64_t max = [ZPWalletManager sharedInstance].config.maxTransfer;
        NSString *money = [@(max) formatCurrency];
        return [NSString stringWithFormat:[R string_InputMoney_BigAmount_Error_Message], money];
    
    } else if([self isLessThan:totalValue]){
        int64_t min = [ZPWalletManager sharedInstance].config.minTransfer;
        NSString *money = [@(min) formatCurrency];
        return [NSString stringWithFormat:[R string_InputMoney_SmallAmount_Error_Message], money];
    }
    return @"";
    
}
@end

@implementation ZPQRTransferAppMessage(Parse)
- (void)parseFromEmbedData:(NSString *)embedData {
    NSString *data = [NSString stringFromBase64String:embedData];
    NSDictionary *json = [data JSONValue];
    if (![json isKindOfClass:[NSDictionary class]]) {
        DDLogInfo(@"Error: %@", json);
        return;
    }
    self.progress = [json intForKey:@"mt_progress"];
    self.senderDisplayName = [json objectForKey:@"displayname"];
    self.senderAvatar = [json objectForKey:@"avatar"];
    self.amount = [json uint64ForKey:@"amount"];
    self.transId = [json stringForKey:@"transid"];
}
@end
