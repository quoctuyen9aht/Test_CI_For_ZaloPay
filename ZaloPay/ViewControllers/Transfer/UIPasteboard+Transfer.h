//
//  UIPasteboard+Transfer.h
//  ZaloPay
//
//  Created by bonnpv on 8/29/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIPasteboard (Transfer)
@property (nonatomic) NSDictionary *transferData;
+ (UIPasteboard *)recentTransferPasteboard;
@end
