//
//  ListDeviceEntity.swift
//  ZaloPay
//
//  Created by Dung Vu on 10/17/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation

struct SessionLoginEntity: Codable {
    var deviceId: String?
    var frontendId: Int?
    var model: String?
    var userId: String?
    var issueDate: TimeInterval?
    var dateExpire: TimeInterval?
    enum CodingKeys: String, CodingKey {
        case dateExpire = "sessionexpiredate"
        case issueDate = "sessionissuedate"
        case userId = "userid"
        case deviceId = "deviceid"
        case frontendId = "frontendid"
        case model = "devicemodel"
    }
}
