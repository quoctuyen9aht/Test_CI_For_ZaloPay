//
//  ListDeviceInteractor.swift
//  ZaloPay
//
//  Created by Dung Vu on 10/17/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CocoaLumberjackSwift

enum Result<T> {
    case success(T)
    case fail(Error)
}

extension IndexPath {
    static var zero: IndexPath {
        return IndexPath(item: 0, section: 0)
    }
}

protocol ListDeviceInteractorDataProtocol {
    associatedtype Data
}

class ListDeviceInteractor: ListDeviceInteractorDataProtocol {
    typealias Data = [SessionLoginEntity]
    // Public
    var dataEvent: Driver<Data> {
        return list.asDriver()
    }
    var errorEvent: Driver<Error?> {
        return error.asDriver()
    }
    
    let activityTracking: ActivityIndicator = ActivityIndicator()
    
    // Private
    private var list: Variable<Data> = Variable([])
    private var error: Variable<Error?> = Variable(nil)
    private let disposeBag = DisposeBag()
    
    fileprivate func requestList() -> Observable<Result<Data>> {
        return NetworkManager.sharedInstance().requestListSession().map({ (json) -> Result<Data> in
            guard let j:[JSON] = json?.value(forKey: "sessionloginlist", defaultValue: []) else {
                throw NSError(domain: NSURLErrorDomain, code: NSURLErrorCannotDecodeRawData, userInfo: [NSLocalizedDescriptionKey : "No Response!!!"])
            }
            let data = try JSONSerialization.data(withJSONObject: j, options: [])
            let decoder = JSONDecoder()
            let list = try decoder.decode(Data.self, from: data)
            return .success(list)
        }).catchError({ return Observable.just(.fail($0))}).trackActivity(activityTracking)
    }
    
    func loadData() {
        Observable.eventLoading(with: self.requestList()).trackActivity(activityTracking).bind { [weak self] in
            switch $0 {
            case .success(let resp):
                self?.list.value = resp
            case .fail(let e):
                self?.error.value = e
            }
        }.disposed(by: disposeBag)
    }
    
    func remove(at index: IndexPath) {
        let idx = index.item
        guard (0..<self.list.value.count).contains(idx) else {
            return
        }
        let item = self.list.value[idx]
        Observable.eventLoading(with: NetworkManager.sharedInstance().removeDevice(from: item)).subscribe(onNext: { [weak self](_) in
            self?.list.value.remove(at: idx)
        }, onError: { [weak self] in
            self?.error.value = $0
        }).disposed(by: disposeBag)
    }
}


// MARK: - Request
extension NetworkManager {
    fileprivate func requestListSession() -> Observable<JSON?> {
        let signal = self.creatRequest(withPath: api_um_getsessionloginforclient, params: nil, isGet: false)
        return event(from: signal, { $0 as? JSON })
    }
    
    fileprivate func removeDevice(from session: SessionLoginEntity) -> Observable<Void> {
        var params: JSON = [:]
        params["deviceid"] = session.deviceId
        params["fronendid"] = session.frontendId
        let signal = self.creatRequest(withPath: api_um_removesessionloginforclient, params: params, isGet: false)
        return eventIgnoreValue(from: signal)
    }
    
}


