//
//  ListDeviceTableViewCell.swift
//  ZaloPay
//
//  Created by Dung Vu on 10/18/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

class ListDeviceTableViewCell: UITableViewCell {

    @IBOutlet weak var btnRemove: UIButton?
    @IBOutlet weak var lblContent: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupButton()
    }
    
    fileprivate func setupButton() {
        let hBtn = btnRemove?.frame.height ?? 0
        self.btnRemove?.layer.cornerRadius = hBtn / 2
        self.btnRemove?.clipsToBounds = true
        self.btnRemove?.setBackgroundColor(UIColor.zaloBase(), for: .normal)
        self.btnRemove?.setBackgroundColor(UIColor.highlightButton(), for: .highlighted)
        self.btnRemove?.setTitleColor(.white, for: .normal)
        self.btnRemove?.setTitle("ĐĂNG XUẤT", for: .normal)
    }
    
    func setupDisplay(from entity: SessionLoginEntity) {
       self.lblContent?.text = entity.model
    }
}
