//
//  ListDeviceViewController.swift
//  ZaloPay
//
//  Created by Dung Vu on 10/17/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

class ListDeviceViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView?.estimatedRowHeight = 60
        self.tableView?.rowHeight = UITableViewAutomaticDimension
    }
}

