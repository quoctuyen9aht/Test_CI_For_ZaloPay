//
//  ListDevicePresenter.swift
//  ZaloPay
//
//  Created by Dung Vu on 10/17/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

protocol ListDevicePresenterProtocol: class {
    var controller: ListDeviceViewController? { get }
}

final class ListDevicePresenter: NSObject, ListDevicePresenterProtocol {
    @IBOutlet weak var controller: ListDeviceViewController? {
        didSet{
            self.setupEvent()
        }
    }
    
    fileprivate var tableView: UITableView? {
        return controller?.tableView
    }
    
    fileprivate let disposeBag = DisposeBag()
    fileprivate let interactor: ListDeviceInteractor
    fileprivate lazy var router = ListDeviceRouter(using: self)
    
    override init() {
        self.interactor = ListDeviceInteractor()
        super.init()
    }
    
    fileprivate func setup() {
        self.tableView?.rx.setDelegate(self).disposed(by: disposeBag)
        self.controller?.title = "Danh sách đăng nhập"
        
        // Add back Button
        let btnBack = UIButton(type: .custom)
        btnBack.frame = CGRect(origin: .zero, size: CGSize(width: 40, height: 40))
        btnBack.setIconFont("general_backios", for: .normal)
        btnBack.titleLabel?.font = UIFont.iconFont(withSize: 20)
        btnBack.setTitleColor(.white, for: .normal)
        if #available(iOS 11, *) {
            btnBack.contentEdgeInsets = UIEdgeInsetsMake(0, -12, 0, 0)
        }
        btnBack.rx.tap.bind { [weak self] (_) in
            self?.router.back()
        }.disposed(by: disposeBag)
        
        let flexiable = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        flexiable.width = -12
        let barButton = UIBarButtonItem(customView: btnBack)
        self.controller?.navigationItem.leftBarButtonItems = [flexiable, barButton]
        
        self.interactor
            .errorEvent
            .drive(onNext: { [weak self] in
            self?.router.handler(from: $0)
        }).disposed(by: disposeBag)
        
        guard let tableView = self.tableView else {
            return
        }
        addRefresh()
        self.interactor
            .dataEvent
            .drive(tableView.rx.items(cellIdentifier: "ListDeviceTableViewCell",
                                      cellType: ListDeviceTableViewCell.self))
            {
                $2.setupDisplay(from: $1)
                
                $2.btnRemove?.rx.tap.map({ 0 }).scan($0) { (check, old) -> Int in
                    return check
                }.bind(onNext: { [weak self] in
                       self?.removeSession(at: $0)
                }).disposed(by: $2.rxDisposeBag)
            }
            .disposed(by: disposeBag)
        self.interactor.loadData()
    }
    
    fileprivate func removeSession(at idx: Int) {
        let nIndexPath = IndexPath(item: idx, section: 0)
        self.interactor.remove(at: nIndexPath)
    }
    
    fileprivate func addRefresh() {
        let refreshControl = UIRefreshControl()
        
        refreshControl.rx.controlEvent(.valueChanged).bind { [weak self, unowned refreshControl] in
            refreshControl.beginRefreshing()
            self?.interactor.loadData()
        }.disposed(by: disposeBag)
        
        self.interactor.activityTracking.asDriver().drive(onNext: { [weak refreshControl] in
            if refreshControl?.isRefreshing == true && $0.not {
                refreshControl?.endRefreshing()
            }
        }).disposed(by: disposeBag)
        self.tableView?.addSubview(refreshControl)
    }
    
    fileprivate func setupEvent() {
        self.controller?.rx.methodInvoked(#selector(UIViewController.viewDidLoad)).bind(onNext: { [weak self](_) in
            self?.setup()
        }).disposed(by: disposeBag)
    }
}

extension ListDevicePresenter: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
}


