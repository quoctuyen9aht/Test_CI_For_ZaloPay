//
//  ListDeviceRouter.swift
//  ZaloPay
//
//  Created by Dung Vu on 10/17/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import UIKit
import CocoaLumberjackSwift

class ListDeviceRouter {
    fileprivate weak var presenter: ListDevicePresenterProtocol?
    init(using presenter: ListDevicePresenterProtocol) {
        self.presenter = presenter
    }
    
    func back() {
        self.presenter?.controller?.navigationController?.popViewController(animated: true)
    }
    
    func handler(from e: Error?) {
        guard let e = e else {
            return
        }
        ZPDialogView.showDialogWithError(e) {
            DDLogInfo("Error !!!!!!")
        }
    }
}
