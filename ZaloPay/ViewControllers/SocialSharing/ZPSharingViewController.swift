//
//  ZPSharingViewController.swift
//  ZaloPay
//
//  Created by tridm2 on 1/30/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import ZaloPayConfig
import ZaloPayShareControl

extension ZPSharingChannel {
    var image: UIImage? {
        switch self {
        case .facebook:
            return UIImage(named: "fb")
//        case .zalo:
//            return UIImage(named: "zalo")
        case .messenger:
            return UIImage(named: "messenger")
        case .imessage:
            return UIImage(named: "imessage")
        case .other:
            return UIImage(named: "other")
        }
    }
    var title: String {
        switch self {
        case .facebook:
            return R.string_Sharing_Facebook_Title()
//        case .zalo:
//            return R.string_Sharing_Zalo_Title()
        case .messenger:
            return R.string_Sharing_Messenger_Title()
        case .imessage:
            return R.string_Sharing_IMessage_Title()
        case .other:
            return R.string_Sharing_Other_Title()
        }
    }
}

@objcMembers
class ZPSharingViewController : NSObject {
    private var zpActionSheetController: ZPShareSheetController?
    private weak var currentVC: UIViewController?
    private let eResult: PublishSubject<ZPSharingChannel> = PublishSubject()
    private var message: JSON = [:]
    private let disposeBag = DisposeBag()
    override init() {
        super.init()
        setupEvent()
    }
    
    private func setupEvent() {
        self.eResult.bind { [weak self](type) in
            defer {
                self?.zpActionSheetController = nil
                self?.currentVC = nil
            }
            self?.zpActionSheetController?.dismissViewController()
            ZPCenterSharing.sharedInstance.share(from: self?.currentVC, on: type, message: self?.message ?? [:], callback: nil)
        }.disposed(by: disposeBag)
        
    }
    private func createActionSheetController(_ vc: UIViewController, message: JSON) -> ZPShareSheetController? {
        let zpSheetConfigure = ZPShareSheetConfigure.zalopay()
        zpSheetConfigure.titleHeader = String(format: R.string_WebApp_BottomSheet_SupportFor(), "zalopay.vn")
        self.zpActionSheetController = ZPShareSheetController(configure: zpSheetConfigure)
        self.currentVC = vc
        self.message = message
        let items = [[ZPSharingChannel.facebook, ZPSharingChannel.messenger, ZPSharingChannel.imessage, ZPSharingChannel.other].map({ createShareModel($0)})]
        self.zpActionSheetController?.items = items
        return zpActionSheetController
    }
    
    private func createShareModel(_ type: ZPSharingChannel) -> ZPItemShareModel {
        return ZPItemShareModel(title: type.title,
                                iconImage: type.image,
                                tapAction: { _ in
                                    self.eResult.onNext(type)
                                    self.eResult.onCompleted()
                                    
        })
    }
    
    private func share(from vc: UIViewController, message: JSON) {
        guard let zpActionSheetController = createActionSheetController(vc, message: message) else {
            return
        }
        vc.present(zpActionSheetController, animated: true, completion: nil)
    }
    
    func beginSharing(from vc: UIViewController, image: UIImage?, content: String, link: String?) {
        var message = JSON()
        message["image"] = image
        message["message"] = content
        message["link"] = link
        share(from: vc, message: message)
    }
    
    func beginSharing(from vc: UIViewController, message: String, caption: String) {
        guard let link = ZPApplicationConfig.getSharingConfig()?.getShareViaSocial() else {
            return
        }        
        var content = JSON()
        content["message"] = "\(caption)\n\(message)"
        content["link"] = link
        share(from: vc, message: content)
    }
}
