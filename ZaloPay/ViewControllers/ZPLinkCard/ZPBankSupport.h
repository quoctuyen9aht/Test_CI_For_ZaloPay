//
//  ZPBankSupport.h
//  ZaloPay
//
//  Created by bonnpv on 10/17/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ZaloPayCommon/CodableObject.h>

typedef enum {
    ZPBSupportType_Card = 1,
    ZPBSupportType_Account = 2,
} ZPBSupportType;

typedef enum {
    ZPBStatus_Active = 1,
    ZPBStatus_Maintain = 2,
    ZPBStatus_InActive = 0
}ZPBStatus;

typedef NS_ENUM(NSInteger, ZPBankPaymentType) {
    ZPBankPaymentTypeNone = 0,
    ZPBankPaymentTypeGateway
};


@interface ZPBankSupport : CodableObject
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *iconName;
@property (nonatomic, strong) NSString *iconNameHorizontal;
@property (nonatomic) BOOL enableWithdraw;
@property (nonatomic) ZPBSupportType type;
@property (nonatomic) ZPBStatus status;
@property (nonatomic) uint64_t maintainFrom;
@property (nonatomic) uint64_t maintainTo;
@property (copy, nonatomic) NSString *messageMaintain;
@property (copy, nonatomic) NSString *minAppVersion;
@property (strong, nonatomic) NSNumber *displayOrder;
@property (nonatomic, readonly) BOOL isMaintain;
@property (nonatomic, readonly) BOOL isCreditCard;
@property (nonatomic, readonly) BOOL isCCDebitCard;
@property (nonatomic, assign) ZPBankPaymentType statePayment;
@property (nonatomic, assign) long long minFee;
@property (nonatomic, assign) long long feeRate;
@property (nonatomic, assign) long long minValue;
@property (nonatomic, assign) long long maxValue;


+ (NSDictionary *)imagesMapper;
+ (NSDictionary *)imagesMapperHorizontal;

+ (BOOL)isCCCard:(NSString *)code;

+ (instancetype)masterCard;

+ (instancetype)jcbCard;

+ (instancetype)visaCard;

+ (BOOL)isCCDebitCard:(NSString *)code;

+ (instancetype)masterDebitCard;

+ (instancetype)jcbDebitCard;

+ (instancetype)visaDebitCard;

+ (instancetype)bankWithCode:(NSString *)code
                        name:(NSString *)name
                       image:(NSString *)image;
+ (instancetype)bankWithCode:(NSString *)code
                        name:(NSString *)name
                       image:(NSString *)image
          iconNameHorizontal:(NSString *)iconNameHorizontal;

- (instancetype)initWith:(NSDictionary *)json;

@end
