//
//  ZPListBankTableViewCell.h
//  ZaloPay
//
//  Created by Dung Vu on 5/25/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
@class UIInappResourceImageView;
typedef NS_ENUM(NSInteger, ZPListBankTableViewCellState) {
    ZPListBankTableViewCellStateNormal,
    ZPListBankTableViewCellStateMaintain,
    ZPListBankTableViewCellStateNotSupport,
    ZPListBankTableViewCellStateSupportDebit,
    ZPListBankTableViewCellStatePaymentUnsupport
};
@class ZPMiniBank;
@class ZPBankSupport;
@interface ZPListBankTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIInappResourceImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblWarning;
@property (assign, nonatomic) ZPListBankTableViewCellState state;
@property (assign, nonatomic) BOOL isMaintain;
@property (copy, nonatomic) NSString *messageMaintain;
- (void)setupDisplayWith:(ZPMiniBank *)miniBank using:(ZPBankSupport *)bank;
- (void)checkSupportPayment:(ZPBankSupport *)bank original:(long long)amount;
@end
