//
//  ZPListBankTableViewCell.m
//  ZaloPay
//
//  Created by Dung Vu on 5/25/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPListBankTableViewCell.h"
#import <ZaloPayWalletSDK/ZPMiniBank.h>
#import "ZPBankSupport.h"
@interface ZPListBankTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *lblNotSuport;
@property (weak, nonatomic) IBOutlet UIImageView *iconArrow;
@end

@implementation ZPListBankTableViewCell

- (void)setupDisplayWith:(ZPMiniBank *)miniBank
                   using:(ZPBankSupport *)bank
{
    // Track maintain
    BOOL isMaintain = bank.isMaintain;
    if (isMaintain) {
        self.messageMaintain = [bank.messageMaintain length] > 0 ? bank.messageMaintain : [R string_LinkCard_Maintain_Message];
    } else if (miniBank.minibankStatus == ZPMiniBankStatus_Maintenance) {
        isMaintain = YES;
        self.messageMaintain = miniBank.maintenancemsg;
    }
    self.isMaintain = isMaintain;
    ZPListBankTableViewCellState state = isMaintain ? ZPListBankTableViewCellStateMaintain : ZPListBankTableViewCellStateNormal;
    self.state = state;
}

- (void)setState:(ZPListBankTableViewCellState)state {
    if (_state == state) {
        return;
    }
    
    _state = state;
    CGFloat alpha;
    NSString *description = @"";
    NSString *descriptionSupport = @"";
    BOOL isHiddenArrow = NO;
    CGFloat b = 9;
    switch (state) {
        case ZPListBankTableViewCellStateNormal:
            alpha = 1;
            break;
        case ZPListBankTableViewCellStateMaintain:
            alpha = 0.3;
            b = 4;
            description = [R string_LinkCard_Maintain_Status];
            break;
        case ZPListBankTableViewCellStateNotSupport:
            alpha = 0.3;
            isHiddenArrow = YES;
            descriptionSupport = [R string_LinkCard_NotSupport_Status];
            break;
        case ZPListBankTableViewCellStateSupportDebit:
            alpha = 1.0;
            descriptionSupport = [R string_LinkCard_NotSupport_CreditCard];
            break;
        case ZPListBankTableViewCellStatePaymentUnsupport:
            alpha = 0.3;
            b = 4;
            descriptionSupport = @"";
            break;
    }

    self.iconArrow.hidden = isHiddenArrow;
    self.lblNotSuport.text = descriptionSupport;
    self.lblWarning.text = description;
    
    for (UIView *v in self.contentView.subviews ) {
        v.alpha = alpha;
    }
    
    if (state == ZPListBankTableViewCellStateSupportDebit) {
        self.lblNotSuport.alpha = 0.3;
    }
    
    [[self.contentView constraints] enumerateObjectsUsingBlock:^(__kindof NSLayoutConstraint * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.identifier isEqualToString:@"bottomDescription"]) {
            obj.constant = b;
            *stop = YES;
        }
    }];
    
    [self layoutIfNeeded];
}

- (void)checkSupportPayment:(ZPBankSupport *)bank original:(long long)amount {
    // Valid
    if (bank.statePayment != ZPBankPaymentTypeGateway || self.state != ZPListBankTableViewCellStateNormal) {
        return;
    }
    
    long long minTransaction = bank.minValue;
    long long maxTransaction = bank.maxValue;
    
    
    void (^Check)(NSString *) = ^(NSString *value) {
        if ([value length] == 0) {
            return;
        }
        self.state = ZPListBankTableViewCellStatePaymentUnsupport;
        self.lblWarning.text = value;
    };
    
    NSString *description = @"";
    if (minTransaction > amount) {
        description = [NSString stringWithFormat:[R string_Gateway_Notice_LessMinAmount], [@(minTransaction) formatCurrency]];
    }
    else if (maxTransaction > 0 && maxTransaction < amount) {
        description = [NSString stringWithFormat:[R string_Gateway_Notice_GreaterMaxAmount], [@(maxTransaction) formatCurrency]];
    }
    
    Check(description);
    
}

@end

