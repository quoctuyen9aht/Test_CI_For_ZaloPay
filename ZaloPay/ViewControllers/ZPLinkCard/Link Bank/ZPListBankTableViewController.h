//
//  ZPListBankTableViewController.h
//  ZaloPay
//
//  Created by Dung Vu on 5/25/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ZaloPayWalletSDK/ZPDefine.h>

@class ZPTrackingEventLabel;

@interface ZPListBankTableViewController : BaseViewController
@property (strong, nonatomic) RACSubject *e;

+ (RACSignal *)showListBankOn:(UIViewController *)controller isSupportCCCard:(BOOL)support forTransType:(ZPTransType)originTransType;

/**
 Using List For Payment

 @param controller show based on this (navigation controller)
 @param support check support cc card
 @param appId app support
 @param originTransType using for sort
 @param amount original amount for bill (using for check minimum payment)
 @return signal return ZPBankSupport
 */

+ (RACSignal *)showListUsingPaymentOn:(UIViewController *)controller
                      isSupportCCCard:(BOOL)support
                                appId:(NSInteger)appId
                         forTransType:(ZPTransType)originTransType
                               amount:(long long)amount;

@end
