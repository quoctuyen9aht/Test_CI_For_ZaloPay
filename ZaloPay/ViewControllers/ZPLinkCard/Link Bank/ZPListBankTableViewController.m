 //
//  ZPListBankTableViewController.m
//  ZaloPay
//
//  Created by Dung Vu on 5/25/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPListBankTableViewController.h"
#import "ZPListBankTableViewCell.h"
#import "ZPBankSupport.h"
#import <ZaloPayUI/UIViewController+BaseViewController.h>
#import "ZPBankMapping.h"
#import <ZaloPayWalletSDK/ZaloPayWalletSDKPayment.h>
#import <ZaloPayWalletSDK/ZPMiniBank.h>
#import <ZaloPayWalletSDK/ZPPaymentChannel.h>
#import <ZaloPayCommon/UILabel+ZaloPayStyle.h>
#import <ZaloPayAnalyticsSwift/ZaloPayAnalyticsSwift-Swift.h>
#import <MDHTMLLabel/MDHTMLLabel.h>
#import ZALOPAY_MODULE_SWIFT
#import <ZaloPayCommon/NSString+Validation.h>
#import <ZaloPayProfile/ZaloPayProfile-Swift.h>

@interface ZPDialogView (html)
- (UILabel *)createMessageLabel:(NSAttributedString *)att;
@end

@interface ZPDialogHTMLView: ZPDialogView<MDHTMLLabelDelegate>
+ (instancetype)showDialogHTMLWithType:(DialogType)dialogType
                   title:(NSString *)title
              htmlString:(NSString *)html
            buttonTitles:(NSArray *)otherButtonTitles
                 handler:(MMPopupItemHandler)handler;
@end

@interface ZPListBankHeaderView: UIView
@property (strong, nonatomic) UILabel *lblDescription;
@end

@interface ZPListBankTableViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) NSArray<ZPBankSupport *> *bankList;
@property (strong, nonatomic) NSArray<ZPMiniBank *>*miniBanks;
@property (assign, nonatomic) BOOL isSupportCCCard;
@property (assign, nonatomic) ZPTransType transType;

@property (assign, nonatomic) BOOL canPayment;
@property (strong, nonatomic) NSArray<ZPBankSupport *> *banksGateway;
@property (assign, nonatomic) NSInteger appId;
@property (assign, nonatomic) long long amount;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation ZPListBankTableViewController

- (instancetype)initUsePayment:(BOOL)isPaymentGateway {
    self = [super initWithNibName:@"ZPListBankTableViewController" bundle:nil];
    self.canPayment = isPaymentGateway;
    self.e = [RACSubject new];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UINib *nib = [UINib nibWithNibName:@"ZPListBankTableViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"ZPListBankTableViewCell"];
//    self.navigationItem.leftBarButtonItems = [self leftBarButtonItems];
    self.title = _canPayment ? [R string_LeftMenu_PayBill] : @"Chọn liên kết";
    self.view.backgroundColor = [UIColor whiteColor];

    [self prepareSource];
}

- (void)prepareSource {
    @weakify(self);
    RACSignal *eUpdateBankList = [[[[ZPWalletManagerSwift sharedInstance] getListBankSupport] deliverOnMainThread] doNext:^(NSArray* bankList) {
        @strongify(self);
        // Find MiniBanks
        self.miniBanks = [[ZaloPayWalletSDKPayment sharedInstance] getMiniBanksForMapping];
        self.bankList = [self sortBanksByStatus:bankList];
    }];
    
    RACSignal *eUpdateBankGateway = !_canPayment ? [RACSignal empty] : [[[[[ZaloPayWalletSDKPayment sharedInstance] getBankPaymentGateway:_appId] deliverOnMainThread] map:^NSArray <ZPBankSupport *> *_Nullable (NSDictionary * _Nullable response) {
        // Parse
        if (!response) {
            return nil;
        }
        @strongify(self);
        NSArray *banks = [response arrayForKey:@"banklist" defaultValue:@[]];
        NSArray <ZPBankSupport *> *result = [banks map:^ZPBankSupport *(id obj) {
            NSDictionary *json = [NSDictionary castFrom:obj];
            if (!json) {
                return nil;
            }
            ZPBankSupport *bank = [[ZPBankSupport alloc] initWith:json];
            bank.statePayment = ZPBankPaymentTypeGateway;
            return bank;
        }];
        // sort
        result = [result sortedArrayUsingComparator:^NSComparisonResult(ZPBankSupport  *_Nonnull obj1, ZPBankSupport  *_Nonnull obj2) {
            return [obj1.displayOrder compare:obj2.displayOrder];
        }];
        
        // Filter bank mantain
        NSMutableArray <ZPBankSupport *> *banksMaintain = [NSMutableArray new];
        result = [result filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
            ZPBankSupport *b = [ZPBankSupport castFrom:evaluatedObject];
            if (!b) {
                return NO;
            }
            BOOL isDisable = b.isMaintain || b.minValue > self.amount;
            if (b.maxValue > 0 && !isDisable) {
                isDisable = b.maxValue < self.amount;
            }
            
            if (isDisable) {
                [banksMaintain addObject:b];
            }
            
            return !isDisable;
        }]];
        
        NSMutableArray <ZPBankSupport *> *banksDisplay = [NSMutableArray new];
        [banksDisplay addObjectsFromArray:result];
        [banksDisplay addObjectsFromArray:banksMaintain];
        
        return banksDisplay;
    }] doNext:^(NSArray <ZPBankSupport *> * _Nullable result) {
        @strongify(self);
        self.banksGateway = result;
    }];
    
    RACSignal *eSource = [[[eUpdateBankList concat:eUpdateBankGateway] doCompleted:^{
        [[ZPAppFactory sharedInstance] hideAllHUDsForView:nil];
        @strongify(self);
        [self.tableView reloadData];
    }] doError:^(NSError * _Nonnull error) {
        [[ZPAppFactory sharedInstance] hideAllHUDsForView:nil];
    }];
    
    
    [[[[[self rac_signalForSelector:@selector(viewWillAppear:)] takeUntil:self.rac_willDeallocSignal] doNext:^(id x) {
        [[ZPAppFactory sharedInstance] showHUDAddedTo:nil];
    }]  flattenMap:^__kindof RACSignal * _Nullable(RACTuple * _Nullable value)  {
        return eSource;
    }] subscribeCompleted:^{}];
    
    [self observerResource];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[ZPTrackingHelper shared] trackScreenWithController:self];
}

- (BOOL)isCCDebitEnabledIn:(NSArray*)bankList {
    return [bankList filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
        ZPBankSupport *bank = [ZPBankSupport castFrom:evaluatedObject];
        if (!bank) {
            return NO;
        }
        return [bank isCCDebitCard];
    }]].count > 0;
}

- (NSArray*)sortBanklist:(NSArray*)bankList {
    NSArray * tempBanks = [[bankList copy] sortedArrayUsingComparator:^NSComparisonResult(ZPBankSupport  *_Nonnull b1, ZPBankSupport  *_Nonnull b2) {
        if (b1.displayOrder == nil || b2.displayOrder == nil) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return [b1.displayOrder compare:b2.displayOrder];
    }];
    return tempBanks;
}

- (NSArray*)sortBanksByStatus:(NSArray*)bankList {
    if (!bankList) {
        return bankList;
    }
    BOOL isCCDebitEnabled = [self isCCDebitEnabledIn:bankList];
    
    // sort list banks by displayorder
    NSArray * tempBanks = [self sortBanklist:bankList];
    
    NSMutableArray * banks = [NSMutableArray new];
    NSMutableArray * banksDisable = [NSMutableArray new];
    // sort list banks by status
    for (NSArray * bank in tempBanks) {
        if (![bank isKindOfClass:[ZPBankSupport class]]) {
            continue;
        }
        
        ZPBankSupport *bankSupport = (ZPBankSupport*)bank;
        // Không support thẻ CC
        if ([bankSupport isCreditCard] && !self.isSupportCCCard){
            if (isCCDebitEnabled) {
                continue;
            } else {
                [banksDisable addObjectNotNil:bank];
                continue;
            }
        }
        // Đã support thẻ CC thì không hiển thị CC Debit
        if ([bankSupport isCCDebitCard] && self.isSupportCCCard){
            continue;
        }
        if (bankSupport.status != ZPBStatus_Active) {
            [banksDisable addObjectNotNil:bank];
            continue;
        }
        if (self.transType == ZPTransTypeWithDraw && !bankSupport.enableWithdraw) {
            [banksDisable addObjectNotNil:bank];
            continue;
        }
        
        ZPMiniBank *miniBank = [self getMiniBankWith:bankSupport];
        if (miniBank.minibankStatus != ZPMiniBankStatus_Enable) {
            [banksDisable addObjectNotNil:bank];
            continue;
        }
        [banks addObjectNotNil:bank];
    }
    [banks addObjectsFromArray:banksDisable];
    return banks;
}

- (void)observerResource {
    @weakify(self);
    [[ReactNativeAppManager sharedInstance] observe:self fromAppId:showshowAppId complete:^{
        @strongify(self);
        [self.tableView reloadData];
    }];
}

- (NSString *)screenName {
    return ZPTrackerScreen.Screen_iOS_Bank_BankList;
}

//- (void)backButtonClicked:(id)sender {
//    [self viewWillSwipeBack];
//}

- (NSInteger)backEventId {
    return ZPAnalyticEventActionLinkbank_add_touch_back;
}

- (void)viewWillSwipeBack {
    [self didChooseBank:nil];
}

- (void)dealloc {
    DDLogInfo(@"abc");
    [self.e sendCompleted];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _canPayment ? 2 : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return [_bankList count];
        case 1:
            return [_banksGateway count];
        default:
            return 0;
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (_canPayment) {
        return 60;
    }
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    ZPListBankHeaderView *headerView = [[ZPListBankHeaderView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40)];
    NSString *title = section == 0 ? [R string_Gateway_Header_LinkBank] : [R string_Gateway_Header_Payment];
    headerView.lblDescription.text = title;
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZPListBankTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZPListBankTableViewCell" forIndexPath:indexPath];
    ZPBankSupport *bank = indexPath.section == 0 ? _bankList[indexPath.item] : _banksGateway[indexPath.item];
    cell.lblDescription.text = bank.name;
    [cell.lblDescription zpMainBlackRegular];
    [cell.lblWarning zpSubTextGrayRegular];
    NSString *nameIco = [[NSString stringWithFormat:@"%@",bank.iconName ?: @""] lowercaseString];
    
    [cell.iconView setImageFromResourceWithImagName:nameIco title:bank.name titleColor:[UIColor zp_gray]];
    if (self.transType == ZPTransTypeWithDraw && !bank.enableWithdraw) {
        cell.state = ZPListBankTableViewCellStateNotSupport;
    } else if (([bank isCCDebitCard] || [bank isCreditCard]) && !self.isSupportCCCard) {
        if ([[ZPSDKHelper new] isSupportCCDebit]) {
            cell.state = ZPListBankTableViewCellStateSupportDebit;
        } else {
            cell.state = ZPListBankTableViewCellStateNotSupport;
        }
    } else {
        ZPMiniBank *miniBank = [self getMiniBankWith:bank];
        [cell setupDisplayWith:miniBank
                         using:bank];
    }

    [cell checkSupportPayment:bank original:_amount];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *subs = [cell subviews];
    BOOL isLast = indexPath.section == 0 ? indexPath.item == [_bankList count] - 1 : indexPath.item == [_banksGateway count] - 1;
    CGFloat w = [UIScreen mainScreen].bounds.size.width;
    for (UIView *child in subs) {
        NSString *nClass = NSStringFromClass([child class]);
        if ([nClass isEqualToString:@"_UITableViewCellSeparatorView"]) {
            child.frame = ^CGRect{
                CGRect f = child.frame;
                f.size.height = 1;
                f.origin.x = isLast ? 0 : 62;
                f.size.width = w - f.origin.x;
                return f;
            }();
            child.backgroundColor = [UIColor lineColor];
            break;
        }
    }
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    // Check Maintain
    ZPListBankTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.state == ZPListBankTableViewCellStateNotSupport) {
        return;
    }
    
    if (cell.isMaintain) {
        [self showAlertNormal:cell.messageMaintain];
        return;
    }
    
    if (indexPath.section == 1) {
        if (cell.state == ZPListBankTableViewCellStatePaymentUnsupport) {
            return;
        }
        ZPBankSupport *bank = _banksGateway[indexPath.item];
        [self didChooseBank:bank];
        return;
    }
    
    ZPBankSupport *bank = _bankList[indexPath.item];
    [self trackEventSelectBank:bank];
    
    if (bank) {
        // UnSupport
        // [[ZPAppFactory sharedInstance] trackEvent:ZPAnalyticEventActionLinkbank_add_select_bank];
        if ([[ZaloPayWalletSDKPayment sharedInstance] chekMinAppVersion:bank.minAppVersion])
        {
            NSString *name = [bank.name stringByReplacingOccurrencesOfString:@"NH " withString:@""];
            NSString *msg = [NSString stringWithFormat:[R string_LinkAccount_MinVerison], @"Liên kết thẻ", name];
            [self showAlertNormal:msg];
            return;
        }
        
        // Check already link account
        if (bank.type == ZPBSupportType_Account && [[ZPSDKHelper new] isBankAlreadyMapped:bank.code]) {
            NSString *name = [ZPBankMapping bankName:bank.code];
            NSString *msg = [NSString stringWithFormat:[R string_LinkBank_BankExist], name];
            [self showAlertWith:msg with:[R string_LinkBank_BankExist_Title]];
            return;
        }
        if ([bank.code isEqualToString:BIDV]) {
            [self showBIDVMapcardDialog:bank];
            return;
        }
        
        if ([bank.code isEqualToString:VCCB]) {
            [self showVCCBMapcardDialog:bank];
            return;
        }
        
        if ([bank.code isEqualToString:VTB]) {
            [self showVTBMapcardDialog:bank];
            return;
        }
    }
    
    [self didChooseBank:bank];
}

- (void) trackEventSelectBank:(ZPBankSupport *)bank {
    if([bank.code isEqualToString: JCB]) {
        [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionLinkbank_add_select_jcb];
    } else if([bank.code isEqualToString: VISA]) {
        [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionLinkbank_add_select_visa];
    } else if([bank.code isEqualToString: MASTER]) {
        [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionLinkbank_add_select_master];
    } else {
        [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionLinkbank_add_select_bank_code valueString:bank.code];
    }
    
//    if([bank.code isEqualToString: VCB]) {
//        [[ZPAppFactory sharedInstance] trackEvent:ZPAnalyticEventActionLinkbank_add_select_];
//    }    if([bank.code isEqualToString: VISA]) {
//        [[ZPAppFactory sharedInstance] trackEvent:ZPAnalyticEventActionLinkbank_add_select_visa];
//    }
//    if([bank.code isEqualToString: VISA]) {
//        [[ZPAppFactory sharedInstance] trackEvent:ZPAnalyticEventActionLinkbank_add_select_visa];
//    }
}

- (void)showBIDVMapcardDialog:(ZPBankSupport *)bank {
    @weakify(self);
    NSString *htmlStr = [R string_LinkBank_BIDV_Notify_Message];
    [ZPDialogHTMLView showDialogHTMLWithType:DialogTypeNotification
                                       title:nil
                                  htmlString:htmlStr
                                buttonTitles:@[[R string_LinkBank_BIDV_Accept_Title_Button]] handler:^(NSInteger index)
     {
         @strongify(self);
         [self didChooseBank:bank];
     }];
}

- (void)showVCCBMapcardDialog:(ZPBankSupport *)bank {
    @weakify(self);
    NSString *htmlStr = [R string_LinkBank_VCCB_Notify_Message];
    [ZPDialogHTMLView showDialogHTMLWithType:DialogTypeNotification
                                       title:nil
                                  htmlString:htmlStr
                                buttonTitles:@[[R string_LinkBank_VCCB_Accept_Title_Button]] handler:^(NSInteger index)
     {
         @strongify(self);
         [self didChooseBank:bank];
     }];
}

- (void)showVTBMapcardDialog:(ZPBankSupport *)bank {
    @weakify(self);
    NSString *phone =  ZPProfileManager.shareInstance.userLoginData.phoneNumber ?: @"";
    phone = [phone format:3 with:@" " direction:YES];
    NSString *message = [NSString stringWithFormat:[R string_LinkBank_Notice_VTB_Message], phone];
    [ZPDialogView showDialogWithType:DialogTypeNotification
                               title:nil
                             message:message
                        buttonTitles:@[[R string_ButtonLabel_OK], [R string_ButtonLabel_Close]]
                             handler:^(NSInteger index)
     {
         @strongify(self);
         if (index == 0) {
             [self didChooseBank:bank];
         }
     }];
}

- (NSString *)bankCodeFromBankSupport:(ZPBankSupport *)bank {
    if (bank.isCreditCard) {
        return CC;
    }
    if (bank.isCCDebitCard) {
        return CCDEBIT;
    }
    return bank.code;
}

- (ZPMiniBank *)getMiniBankWith:(ZPBankSupport *)bank {
    NSString *code = [self bankCodeFromBankSupport:bank];
    NSArray<ZPMiniBank *> *results =  [self.miniBanks filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(ZPMiniBank *_Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
       return [evaluatedObject.bankCode isEqualToString:code];
    }]];
    return results.firstObject;
}

- (void)didChooseBank:(ZPBankSupport *)bank {
    [self.e sendNext:bank];
}

- (void)showAlertGoToLinkCardWith:(NSString *)msg with:(NSString *)title {
    [ZPDialogView showDialogWithType:DialogTypeNotification
                               title:title
                             message:msg
                        buttonTitles:@[[R string_ButtonLabel_Detail], [R string_ButtonLabel_Close]]
                             handler:^(NSInteger index) {
                                 if (index == 0) {
                                     [ZPCenterRouter launchWithRouterId:RouterIdLinkCard from:self animated:YES];
                                 }
                             }];
}

- (void)showAlertWith:(NSString *)msg with:(NSString *)title {
    [ZPDialogView showDialogWithType:DialogTypeNotification
                               title:title
                             message:msg
                        buttonTitles:@[[R string_ButtonLabel_Close]]
                             handler:nil];
}

- (void)showAlertNormal:(NSString *)msg {
    [ZPDialogView showDialogWithType:DialogTypeNotification
                             message:msg
                   cancelButtonTitle:[R string_ButtonLabel_Close]
                    otherButtonTitle:nil
                      completeHandle:nil];
}

+ (RACSignal *)showListBankOn:(UIViewController *)controller
              isSupportCCCard:(BOOL)support
                 forTransType:(ZPTransType)originTransType {
    ZPListBankTableViewController *vc = [[ZPListBankTableViewController alloc] initUsePayment:NO];
    vc.isSupportCCCard = support;
    vc.transType = originTransType;
    [controller.navigationController pushViewController:vc animated:true];
    return vc.e;
}

+ (RACSignal *)showListUsingPaymentOn:(UIViewController *)controller
                      isSupportCCCard:(BOOL)support
                                appId:(NSInteger)appId
                         forTransType:(ZPTransType)originTransType
                               amount:(long long)amount {
    ZPListBankTableViewController *vc = [[ZPListBankTableViewController alloc] initUsePayment:YES];
    vc.isSupportCCCard = support;
    vc.appId = appId;
    vc.transType = originTransType;
    vc.amount = amount;
    [controller.navigationController pushViewController:vc animated:true];
    return vc.e;
}

@end

#pragma mark - header view
@implementation ZPListBankHeaderView
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupLayout];
        self.clipsToBounds = YES;
    }
    return self;
}

- (void)setupLayout {
    self.lblDescription = [[UILabel alloc] initWithFrame:CGRectZero];
    self.lblDescription.textColor = [UIColor subText];
    self.lblDescription.font = [UIFont SFUITextRegularWithSize:15];
    [self addSubview:_lblDescription];
    
    CGFloat paddingLeft = 10;
    [self.lblDescription mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(paddingLeft);
        make.bottom.equalTo(-10);
        make.size.greaterThanOrEqualTo(CGSizeZero);
    }];
    
    UIView *vLine = [[UIView alloc] initWithFrame:CGRectZero];
    vLine.backgroundColor = [UIColor lineColor];
    [self addSubview:vLine];
    
    [vLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.lblDescription.mas_left);
        make.right.equalTo(0);
        make.bottom.equalTo(0);
        make.height.equalTo(1);
    }];
}

@end

#pragma mark - Dialog
@implementation ZPDialogHTMLView
+ (instancetype)showDialogHTMLWithType:(DialogType)dialogType
                             title:(NSString *)title
                        htmlString:(NSString *)html
                      buttonTitles:(NSArray *)otherButtonTitles
                           handler:(MMPopupItemHandler)handler {
    return [self showDialogWithType:dialogType
                               title:title
                            message:html
                        buttonTitles:otherButtonTitles
                             handler:handler];
}

- (UILabel *)createMessageLabel:(NSAttributedString *)att {
    // Create HTML string
    MDHTMLLabel *htmlLabel = [[MDHTMLLabel alloc] init];
    htmlLabel.delegate = self;
    htmlLabel.font = [UIFont SFUITextRegularWithSize:14];
    htmlLabel.htmlText = att.string;
    htmlLabel.numberOfLines = 0;
    htmlLabel.textAlignment = NSTextAlignmentCenter;
    htmlLabel.linkAttributes = @{ NSForegroundColorAttributeName: [UIColor zaloBaseColor],
                                  NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle) };
    return htmlLabel;
}

- (void)HTMLLabel:(MDHTMLLabel *)label didSelectLinkWithURL:(NSURL *)URL {
    [self hide];
    if (!URL || ![[UIApplication sharedApplication] canOpenURL:URL]) {
        return;
    }
    [[UIApplication sharedApplication] openURL:URL];
}
@end
