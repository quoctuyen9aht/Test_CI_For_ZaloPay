//
//  ZPBankApiWrapperSwift.swift
//  ZaloPay
//
//  Created by ThuChau on 4/13/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import UIKit
import ZaloPayProfile
import ZaloPayCommonSwift
import ZaloPayAnalyticsSwift

@objcMembers
class ZPBankApiWrapperSwift: NSObject {
    private weak var fromController : UIViewController?
    static let sharedInstance = ZPBankApiWrapperSwift()

    func shouldAskPIN() -> Bool {
        return listCards().count > 0 || listAccounts().count > 0
    }

    func listCards() -> Array<ZPSavedCard> {
        return ZaloPayWalletSDKPayment.sharedInstance().paymentSavedCard() as? Array<ZPSavedCard> ?? []
    }

    func listAccounts() -> Array<ZPSavedBankAccount> {
        return ZaloPayWalletSDKPayment.sharedInstance().paymentSavedBankAccount() as? Array<ZPSavedBankAccount> ?? []
    }

    func removeCard(card: ZPSavedCard) -> RACSignal<AnyObject> {
        return ZaloPayWalletSDKPayment.sharedInstance().remove(card).deliverOnMainThread()
    }
    func removeBankAccount(_ bankCustomerId: String, bankAccount: ZPSavedBankAccount) -> RACSignal<AnyObject> {
        return ZaloPayWalletSDKPayment.sharedInstance().removeBankAccount(bankCustomerId, bankAccount: bankAccount).deliverOnMainThread()
    }
    func hasBankToRecharge() -> Bool {
        let allSavedCard = listCards().filter { (card) -> Bool in
            return !ZPBankSupport.isCCCard(card.bankCode)
        }
        return listAccounts().count + allSavedCard.count > 0
    }

    func mapBank(with viewController: UIViewController, appId: Int, transType: ZPTransType) -> RACSignal<AnyObject> {
        self.fromController = viewController
        let supportCC = !self.bankListDisableCCard(transType: transType, appId: appId)
        return ZPListBankTableViewController.showListBank(on: viewController, isSupportCCCard: supportCC, for: transType)
            .filter { [weak self] (bank) -> Bool in
                guard let strongSelf = self, let bank = bank as? ZPBankSupport else {
                    return false
                }
                return strongSelf.validateBank(bank: bank)
            }.flattenMap( { [unowned self] bankSupport -> RACSignal<AnyObject>? in
                let bankSupport = bankSupport as! ZPBankSupport
                return self.mapUsing(bank: bankSupport, appId: appId, transType: transType)
            })
    }
    
    func sdkDisableCCCardAppId(appId: Int, transType: ZPTransType) -> Bool {
        return transType == ZPTransType.tranfer ||
            transType == ZPTransType.withDraw ||
            transType == ZPTransType.walletTopup ||
            appId == lixiAppId
    }
    
    // MARK: - Bank Account
    func getAttAlertLinkVietCombankAccount(from bankCode: String, bankName: String) -> NSMutableAttributedString {
        let phone = ZPProfileManager.shareInstance.userLoginData?.phoneNumber.format(3, with: " ", direction: true) ?? ""
        let firstLine = String.init(format: R.string_LinkCard_Phone_Notice(), bankName, phone)
        let secondLine = String.init(format: R.string_LinkBank_Minimum_Require(), bankName, (ZPConstants.minVietcombankValue as NSNumber).formatCurrency().replacingOccurrences(of: " VND", with: ""))
        let message = String.init(format: "%@%@", firstLine, secondLine)
        let style = NSMutableParagraphStyle()
        style.alignment = NSTextAlignment.center
        style.lineSpacing = 1.5
        let attribute = NSMutableAttributedString(string: message, attributes: [.paragraphStyle: style, NSAttributedStringKey.font: UIFont.sfuiTextRegular(withSize: 15)])
        return attribute
    }

    func removeBankAccount(account: ZPSavedBankAccount, viewController: UIViewController) {
        let billDetailViewController = BillDetailViewController.init({ [weak viewController] (data) in
            guard let viewController = viewController else {
                return
            }
            viewController.navigationController?.popToViewController(viewController, animated: true)
            }, cancel: { [weak viewController] (data) in
                guard let viewController = viewController else {
                    return
                }
                viewController.navigationController?.popToViewController(viewController, animated: true)
        }) { [weak viewController] (data) in
            guard let viewController = viewController else {
                return
            }
            viewController.navigationController?.popToViewController(viewController, animated: true)
        }
        billDetailViewController.startProcessBill(ZPBill.getRemovedBankAccount(bankCode: account.bankCode), from: viewController)
    }
    
    func alertBankAcountExist(bankCode: String) {
        guard let name = ZPBankMapping.bankName(bankCode) else {
            return
        }
        let message = String.init(format: R.string_LinkBank_BankExist(), name)
        ZPDialogView.showDialog(with: DialogTypeNotification,
                                title: R.string_LinkBank_BankExist_Title(),
                                message: message,
                                buttonTitles: [R.string_ButtonLabel_Close()],
                                handler: nil)
    }
    
    private func checkBankAccountExist(bankCode: String) -> Bool {
        if let _ = self.listAccounts().first(where: {$0.bankCode == bankCode}) {
            alertBankAcountExist(bankCode: bankCode)
            return true
        }
        return false
    }
    
    // MARK: Private method
    private func map(bill: ZPBill) -> RACSignal<AnyObject> {
        return RACSignal.createSignal({ [weak self] (subscriber) -> RACDisposable? in
            
            let billDetailViewController = BillDetailViewController.init({ (data) in
                ZPTrackingHelper.shared().trackEvent(.linkbank_add_result)
                subscriber.sendNext(data)
                subscriber.sendCompleted()
            }, cancel: { (data) in
                let info = data as? [String : Any] ?? nil
                let error = NSError.init(domain: NSURLErrorDomain, code: NSURLErrorCancelled, userInfo: info)
                subscriber.sendError(error)
            }, error: { (data) in
                let info = data as? [String : Any] ?? nil
                ZPTrackingHelper.shared().trackEvent(.linkbank_add_result)
                let error = NSError.init(domain: NSURLErrorDomain, code: NSURLErrorCancelled, userInfo: info)
                subscriber.sendError(error)
            })
            billDetailViewController.startProcessBill(bill, from: self?.fromController)
            return nil
        })
    }
    
    private func alertLinkVietCombankAccountFromBankCode(code: String, bankName: String) -> RACSignal<AnyObject> {
        let message = getAttAlertLinkVietCombankAccount(from: code, bankName: bankName)
        return RACSignal.createSignal( { subscriber -> RACDisposable? in
            ZPDialogView.showDialog(with: DialogTypeNotification,
                                    title: R.string_Dialog_Notification(),
                                    attributeMessage: message,
                                    buttonTitles: [R.string_ButtonLabel_OK(), R.string_ButtonLabel_Close()],
                                    handler:{ index in
                                        subscriber.sendNext(index)
                                        subscriber.sendCompleted()
            })
            return nil
        })
    }
    
    private func mapUsing(bank: ZPBankSupport, appId: Int, transType: ZPTransType) -> RACSignal<AnyObject> {
        let bill = createBillFrom(bank: bank, appId: appId, transType: transType)
        if ZPBankMapping.bankType(bank.code, first6CardNo: nil) == .ZPVietcombank {
            return alertLinkVietCombankAccountFromBankCode(code: bank.code, bankName: bank.name)
                .filter { value -> Bool in
                    guard let value = value as? Int else {
                        return false
                    }
                    return value == 0
                }.flattenMap( { [weak self] value -> RACSignal<AnyObject> in
                    guard let strongSelf = self else {
                        return RACSignal.empty()
                    }
                    return strongSelf.map(bill: bill)
                })
        }
        ZPAppFactory.sharedInstance().showHUDAdded(to: self.fromController?.view)
        return map(bill: bill)
    }
    
    private func bankListDisableCCard(transType: ZPTransType, appId: Int) -> Bool {
        return transType == ZPTransType.withDraw ||
            transType == ZPTransType.walletTopup ||
            appId == lixiAppId
    }
    
    private func validateBank(bank: ZPBankSupport?) -> Bool {
        guard let bank = bank else {
            return false
        }
        if NetworkState.sharedInstance().isReachable.not {
            alertNotInternetConnection()
            return false
        }
        if ZaloPayWalletSDKPayment.sharedInstance().checkIsCurrentPaying() {
            return false
        }
        if bank.type == ZPBSupportType_Account
            && checkBankAccountExist(bankCode: bank.code) {
            return false
        }
        let isCC = bank.isCreditCard || bank.isCCDebitCard
        let isMaxCCSavedCard = ZaloPayWalletSDKPayment.sharedInstance().isMaxCCSavedCard()
        if isCC && isMaxCCSavedCard {
            ZaloPayWalletSDKPayment.sharedInstance().showAlertMaxCCSavedCard()
            return false
        }
        return true
    }
    
    private func createBillFrom(bank: ZPBankSupport, appId: Int, transType: ZPTransType) -> ZPBill {
        let bill = bank.type == ZPBSupportType_Card ? ZPBill.getMappedCard() : ZPBill.getAddedBankAcount(bankCode: bank.code)
        bill.errorCCCanNotMapWhenPay = ZaloPayWalletSDKPayment.sharedInstance().checkErrorMappCCCard(with: transType, appId: Int32(appId))
        bill.unsuportCCCredit = sdkDisableCCCardAppId(appId: appId, transType: transType) && ZPSDKHelper().isSupportCCDebit()
        return bill
    }
    
    private func alertNotInternetConnection() {
        ZPDialogView.showDialog(with: DialogTypeNoInternet,
                                title: R.string_Dialog_Warning(),
                                message: R.string_NetworkError_NoConnectionMessage(),
                                buttonTitles: [R.string_ButtonLabel_Close()],
                                handler: nil)
    }
}
