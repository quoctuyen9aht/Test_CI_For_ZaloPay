//
//  ZPBankSupport.m
//  ZaloPay
//
//  Created by bonnpv on 10/17/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPBankSupport.h"
#import "ZPBankMapping.h"
#import ZALOPAY_MODULE_SWIFT

@implementation ZPBankSupport

+ (NSDictionary *)imagesMapper {
    return [ZPBankMapping imagesMapper];
}

+ (NSDictionary *)imagesMapperHorizontal {
    return [ZPBankMapping imagesMapperHorizontal];
}

+ (BOOL)isCCCard:(NSString *)code {
    return [[code uppercaseString] isEqualToString:CC];
}

+ (instancetype)masterCard {
    return [self bankWithCode:MASTER name:@"Mastercard" image:@"master"];
}

+ (instancetype)jcbCard {
    return [self bankWithCode:JCB name:@"JCB" image:@"jcb"];
}

+ (instancetype)visaCard {
    return [self bankWithCode:VISA name:@"VISA" image:@"visa"];
}

+ (instancetype)bankWithCode:(NSString *)code
                        name:(NSString *)name
                       image:(NSString *)image {
    ZPBankSupport *bank = [[ZPBankSupport alloc] init];
    bank.code = code;
    bank.name = name;
    bank.iconName = image;
    return bank;
}

+ (instancetype)bankWithCode:(NSString *)code
                        name:(NSString *)name
                       image:(NSString *)image
          iconNameHorizontal:(NSString *)iconNameHorizontal{
    ZPBankSupport *bank = [[ZPBankSupport alloc] init];
    bank.code = code;
    bank.name = name;
    bank.iconName = image;
    bank.iconNameHorizontal = iconNameHorizontal;
    return bank;
}

//{
//          "code": "ZPBIDV", "tpebankcode": "BIDV", "name": "BIDV", "fullname": "BIDV", "status":1, "interfacetype":2, "maintenancefrom":0, "maintenanceto":0, "maintenancemsg": "", "displayorder":4, "feerate":0.00000000, "minfee":0, "feecaltype": "SUM", "supporttype":1
//}

- (instancetype)initWith:(NSDictionary *)json {
    if (self = [super init]) {
        self.code = [json stringForKey:@"tpebankcode"];
        self.name = [json stringForKey:@"fullname"];
        self.status = [json intForKey:@"status"];
        self.type = [json intForKey:@"supporttype"];
        self.displayOrder = [json numericForKey:@"displayorder"];
        self.minFee = [json uint64ForKey:@"minfee"];
        self.feeRate = [json uint64ForKey:@"feerate"];
        self.maintainFrom = [json uint64ForKey:@"maintenancefrom"] / 1000;
        self.maintainTo = [json uint64ForKey:@ "maintenanceto"] / 1000;
        self.messageMaintain = [json stringForKey:@"maintenancemsg"];
        NSDictionary *imagesMapper = [[self class] imagesMapper];
        NSDictionary *imagesMapperHorizontal = [[self class] imagesMapperHorizontal];
        self.iconName = [imagesMapper stringForKey:self.code ?: @""];
        self.iconNameHorizontal = [imagesMapperHorizontal stringForKey:self.code ?: @""];
        self.minValue = [json uint64ForKey:@"minvalue"];
        self.maxValue = [json uint64ForKey:@"maxvalue"];
    };
    return self;
}

- (BOOL)isMaintain {
    return self.status == ZPBStatus_Maintain;
}

- (BOOL)isCreditCard {
    return [self.code isEqualToString:VISA] || [self.code isEqualToString:MASTER] || [self.code isEqualToString:JCB];
}

+ (BOOL)isCCDebitCard:(NSString *)code {
    return [[code uppercaseString] isEqualToString:CCDEBIT];
}

+ (instancetype)masterDebitCard {
    return [self bankWithCode:MASTERDEBIT name:@"Mastercard" image:@"masterdebit"];
}

+ (instancetype)jcbDebitCard {
    return [self bankWithCode:JCBDEBIT name:@"JCB" image:@"jcbdebit"];
}

+ (instancetype)visaDebitCard {
    return [self bankWithCode:VISADEBIT name:@"VISA" image:@"visadebit"];
}

- (BOOL)isCCDebitCard {
    return [self.code isEqualToString:VISADEBIT] || [self.code isEqualToString:MASTERDEBIT] || [self.code isEqualToString:JCBDEBIT];
}

@end
