//
//  ZPSavedCard+ZaloPay.swift
//  ZaloPay
//
//  Created by phuongnl on 4/11/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

@objc extension ZPSavedCard {
    public var bankType: ZPBankType {
        return ZPBankMapping.bankType(self.bankCode, first6CardNo: self.first6CardNo)
    }
}
