//
//  ZPFooterCardView.swift
//  ZaloPay
//
//  Created by phuongnl on 4/9/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayCommonSwift

class ZPFooterCardViewSwift : UIView {
    @IBOutlet weak var button: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    func setupView() {
        var buttonTitle = "Thêm Liên Kết"
        let plus = UILabel.iconCode(withName: "linkaccount_add")!
        buttonTitle  = String(format: "%@  %@", plus, buttonTitle)
        let font = button?.titleLabel?.font ?? UIFont.systemFont(ofSize: 17)
        let attributes = [
            NSAttributedStringKey.font: font,
            NSAttributedStringKey.foregroundColor: UIColor.white
        ]
        let att = NSMutableAttributedString(string: buttonTitle, attributes: attributes)
        att.addAttributes([NSAttributedStringKey.font: UIFont.zaloPay(withSize: 18)], range: NSMakeRange(0, plus.count))
        
        button.setAttributedTitle(att, for: .normal)
        button.setBackgroundColor(UIColor.zaloBase(), for: .normal)
        button.setBackgroundColor(UIColor.buttonHighlight(), for: .highlighted)
        button.roundRect(3)
        
        self.backgroundColor = UIColor.defaultBackground()
    }
}
