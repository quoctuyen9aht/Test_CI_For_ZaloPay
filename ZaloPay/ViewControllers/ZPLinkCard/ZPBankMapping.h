//
//  ZPBankMapping.h
//  ZaloPay
//
//  Created by bonnpv on 12/2/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM(NSInteger, ZPBankType) {
    ZPUnknownBank = 0,
    ZPVisa,
    ZPMaster,
    ZPJCB,
    ZPVisaDebit,
    ZPMasterDebit,
    ZPJCBDebit,
    ZPAbbank,
    ZPAcb,
    ZPAgribank,
    ZPBacabank,
    ZPBidv,
    ZPDaiabank,
    ZPEximbank,
    ZPGpbank,
    ZPHdbank,
    ZPMaritimebank,
    ZPMbbank,
    ZPNamabank,
    ZPNavibank,
    ZPOcb,
    ZPOceanbank,
    ZPPgbank,
    ZPSacombank,
    ZPSaigonbank,
    ZPTechcombank,
    ZPTienphongbank,
    ZPVietabank,
    ZPVietinbank,
    ZPVpbank,
    ZPDongabank,
    ZPVietcombank,
    ZPScbbank,
    ZPVCCBBank
} ;

extern NSString *CC;
extern NSString *CCDEBIT;
extern NSString *MASTER;
extern NSString *JCB;
extern NSString *VISA;
extern NSString *MASTERDEBIT;
extern NSString *JCBDEBIT;
extern NSString *VISADEBIT;
extern NSString *VCB;
extern NSString *VTB;
extern NSString *BIDV;
extern NSString *SCB;
extern NSString *EIB;
extern NSString *SGCB;
extern NSString *VCCB;

@interface ZPBankMapping : NSObject
+ (NSDictionary *)imagesMapper;
+ (NSDictionary *)imagesMapperHorizontal;
+ (ZPBankType)bankType:(NSString *)bankCode first6CardNo:(NSString *)first6CardNo;
+ (NSString *)bankName:(NSString *)bankCode;
+ (NSString *)bankName:(NSString *)bankCode first6CardNo:(NSString *)first6CardNo;
//+ (void)updateBankNamesFromConfig:(NSDictionary *)config;
@end
