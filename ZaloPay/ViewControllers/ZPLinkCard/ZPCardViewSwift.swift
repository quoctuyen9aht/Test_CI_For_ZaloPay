//
//  ZPCardView.swift
//  ZaloPay
//
//  Created by phuongnl on 4/9/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayCommonSwift
import ZaloPayProfile
import SnapKit

class ZPCardViewSwift : UIView {
    private var mapImageView: UIImageView!
    private var iconImageView: UIInappResourceImageView!
    private var seriLabel: UILabel!
    private var savedCard: AnyObject?
    private var lblDescription: UILabel!
    
    public lazy var imagesDictionary: [ZPBankType : String] = [
        .ZPAgribank: "ico_agribank",
        .ZPBidv: "ico_bidv",
        .ZPDongabank: "ico_donga",
        .ZPEximbank: "ico_eximbank",
        .ZPMaritimebank: "ico_maritime",
        .ZPMbbank: "ico_mb",
        .ZPSacombank: "ico_sacombank",
        .ZPVCCBBank: "ico_vccb",
        .ZPTechcombank: "ico_techcombank",
        .ZPTienphongbank: "ico_tpbank",
        .ZPVietabank: "ico_vib",
        .ZPVietcombank: "ico_vietcombank",
        .ZPVietinbank: "ico_vietinbank",
        .ZPVisa: "ico_visa",
        .ZPMaster: "ico_mastercard",
        .ZPJCB: "ico_jcb",
        .ZPVisaDebit: "ico_visadebit",
        .ZPMasterDebit: "ico_mastercarddebit",
        .ZPJCBDebit: "ico_jcbdebit",
        .ZPScbbank: "ico_scb"
    ]
    
    init() {
        super.init(frame: CGRect.zero)
        
        mapImageView = UIImageView()
        self.addSubview(mapImageView)
        mapImageView.snp.makeConstraints { (make) in
            make.right.equalTo(-6)
            make.top.equalTo(5)
        }
        let mapImg = UIImage.fromInternalApp("ico_map")
        mapImageView.image = mapImg
        mapImageView.snp.makeConstraints { (make) in
            make.size.equalTo(mapImg.size)
        }
        mapImageView.contentMode = UIViewContentMode.center
        self.clipsToBounds = true
        
        iconImageView = UIInappResourceImageView()
        iconImageView.contentMode = UIViewContentMode.topLeft
        self.addSubview(iconImageView)
        iconImageView.snp.makeConstraints { (make) in
            make.top.equalTo(17)
            make.left.equalTo(24)
        }
        
        lblDescription = UILabel(frame: CGRect.zero)
        lblDescription.textColor = UIColor.white
        //lblDescription.font = UIFont.sfuiTextRegular(withSize: 15)
        lblDescription.zpMainBlackRegular()
        lblDescription.textColor = UIColor.white
        self.addSubview(lblDescription)
        lblDescription.snp.makeConstraints { (make) in
            make.top.equalTo(iconImageView.snp.bottom).offset(30)
            make.left.equalTo(24)
            make.width.greaterThanOrEqualTo(0)
            make.height.equalTo(16)
        }
        
        seriLabel = UILabel()
        self.addSubview(seriLabel)
        seriLabel.snp.makeConstraints { (make) in
            make.top.equalTo(iconImageView.snp.bottom).offset(25)
            make.left.equalTo(24)
            make.right.equalTo(-26)
            make.height.equalTo(30)
        }
        seriLabel.adjustsFontSizeToFitWidth = true
        seriLabel.font = UIFont.ocraRegular(20)
        seriLabel.textColor = UIColor.white
    }
    
    public required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public class var layerClass: Swift.AnyClass {
        return CAGradientLayer.self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        var bankType: ZPBankType?
        if let card = savedCard as? ZPSavedCard {
            bankType = card.bankType
        } else if let card = savedCard as? ZPSavedBankAccount {
            bankType = card.bankType
        }
        if bankType == nil {
            return
        }
        let colors = self.colorsWithBank(bankType: bankType!)
        guard let gradient = self.layer as? CAGradientLayer else {
            return
        }
        //gradient.frame =  CGRect(origin: .zero, size: self.frame.size)
        gradient.colors = [
            colors[0].cgColor,
            colors[0].cgColor,
            colors[1].cgColor,
            colors[0].cgColor,
            colors[0].cgColor
            //UIColor.white.cgColor
        ]
        gradient.startPoint = CGPoint(x: 0, y: 1)
        gradient.endPoint = CGPoint(x: 1, y: 0)
        //self.layer.insertSublayer(gradient, at: 0)
        
        self.roundRect(10)
        seriLabel.shadowColor = UIColor(white: 0, alpha: 0.52)
        seriLabel.shadowOffset = CGSize(width: 0.0, height: 1.15)
        lblDescription.shadowColor = UIColor(white: 0, alpha: 0.52)
        lblDescription.shadowOffset = CGSize(width: 0.0, height: 1.15)
    }
    
    
    
    
    func setSavedCard(savedCard: AnyObject) {
        var iconName = ""
        var bankName = ""
        
        if let card = savedCard as? ZPSavedCard {
            self.savedCard = card
            let bankType = card.bankType
            iconName = imageName(bankType)
            bankName = ZPBankMapping.bankName(card.bankCode, first6CardNo: card.first6CardNo) ?? ""
            let seri = String(format: "%@******%@", "******", card.last4CardNo)
            lblDescription.text = ""
            seriLabel.snp.updateConstraints { (make) in
                make.left.equalTo(24)
            }
            seriLabel.text = seri.format(4, with: " ", direction: false)
            
        } else if let card = savedCard as? ZPSavedBankAccount {
            self.savedCard = card
            let user = ZPProfileManager.shareInstance.userLoginData
            let type = ZPBankMapping.bankType(card.bankCode, first6CardNo: nil)
            iconName = imageName(type)
            bankName = ZPBankMapping.bankName(card.bankCode) ?? ""
            if card.bankCode == VCB {
                var phone = user != nil ? user!.phoneNumber : ""
                phone = phone.hidden(with: 0, end: 4, withCharacter: "*")
                lblDescription.text = R.string_LinkAccount_Phone()
                seriLabel.text = phone
                let w = lblDescription.systemLayoutSizeFitting(CGSize(width: 140, height: 16), withHorizontalFittingPriority: UILayoutPriority.fittingSizeLevel, verticalFittingPriority: UILayoutPriority.required).width
                seriLabel.snp.updateConstraints { (make) in
                    make.left.equalTo(w + 28)
                }
            } else {
                let seri = String(format: "**********%@", card.lastAccountNo)
                lblDescription.text = ""
                seriLabel.snp.updateConstraints { (make) in
                    make.left.equalTo(24)
                }
                seriLabel.text = seri.format(4, with: " ", direction: false)
            }
        }
        iconImageView.setImageFromResource(imagName: iconName, title: bankName, titleColor: UIColor.white)
        if var size = iconImageView.image?.size {
            if size.width == 0 {
                size = CGSize(width: 100, height: 30)
            }
            iconImageView.snp.makeConstraints { (make) in
                make.size.equalTo(size)
            }
        }
        self.setNeedsLayout()
    }
    
    func colorsWithBank(bankType: ZPBankType) -> Array<UIColor> {
        switch bankType {
        case .ZPBidv:
            return [UIColor(hexValue: 0x010044), UIColor(hexValue: 0x1b2f7b)]
            
        case .ZPEximbank:
            return [UIColor(hexValue: 0x006c97), UIColor(hexValue: 0x00a2e2)]
            
        case .ZPAgribank:
            return [UIColor(hexValue: 0x9f303a), UIColor(hexValue: 0xb3343f)]
            
        case .ZPSacombank:
            return [UIColor(hexValue: 0x0b6198), UIColor(hexValue: 0x4774bb)]
            
        case .ZPVietcombank:
            return [UIColor(hexValue: 0x137a37), UIColor(hexValue: 0x6ac79f)]
            
        case .ZPVietinbank:
            return [UIColor(hexValue: 0xafb5b5), UIColor(hexValue: 0xd7d8d3)]
            
        case .ZPVisa:
            return [UIColor(hexValue: 0xbf903f), UIColor(hexValue: 0xeed86f)]
            
        case .ZPMaster:
            return [UIColor(hexValue: 0x2d294e), UIColor(hexValue: 0x555177)]
            
        case .ZPJCB:
            return [UIColor(hexValue: 0x00134e), UIColor(hexValue: 0x0053ab)]
            
        case .ZPVisaDebit:
            return [UIColor(hexValue: 0xbf903f), UIColor(hexValue: 0xeed86f)]
            
        case .ZPMasterDebit:
            return [UIColor(hexValue: 0x2d294e), UIColor(hexValue: 0x555177)]
            
        case .ZPJCBDebit:
            return [UIColor(hexValue: 0x00134e), UIColor(hexValue: 0x0053ab)]
            
        case .ZPScbbank:
            return [UIColor(hexValue: 0x269144), UIColor(hexValue: 0x47cf27)]
            
        case .ZPVCCBBank:
            return [UIColor(hexValue: 0x00a1e4), UIColor(hexValue: 0x014588)]
            
        default:
            return [UIColor(hexValue: 0xafb5b5), UIColor(hexValue: 0xd7d8d3)]
        }
    }
    
    func imageName(_ bankType: ZPBankType) -> String {
        return self.imagesDictionary[bankType] ?? ""
    }
}
