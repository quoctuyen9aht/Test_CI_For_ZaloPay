//
//  ZPBankMapping.m
//  ZaloPay
//
//  Created by bonnpv on 12/2/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPBankMapping.h"
#import "ZPBankSupport.h"
#import <ZaloPayWalletSDK/ZaloPayWalletSDKPayment.h>
#import ZALOPAY_MODULE_SWIFT

NSString *MASTER    = @"123PMASTER";
NSString *JCB       = @"123PJCB";
NSString *VISA      = @"123PVISA";
NSString *MASTERDEBIT    = @"123PMASTERDEBIT";
NSString *JCBDEBIT       = @"123PJCBDEBIT";
NSString *VISADEBIT      = @"123PVISADEBIT";
NSString *CC        = @"CC";
NSString *CCDEBIT   = @"CCDB";
NSString *VCB       = @"VCB";
NSString *VTB       = @"VTB";
NSString *BIDV      = @"BIDV";
NSString *SCB       = @"SCB";
NSString *EIB       = @"EIB";
NSString *SGCB      = @"SGCB";
NSString *VCCB      = @"VCCB";
NSString *ARB       = @"VARB";
NSString *ACB       = @"ACB";
NSString *VPB       = @"VPB";
NSString *TCB       = @"TCB";
NSString *DAB       = @"DAB";
NSString *MB        = @"MB";

@implementation ZPBankMapping
+ (NSDictionary *)imagesMapper {
    return @{@"123PABB": @"",
             @"123PACB": @"",
             @"123PAGB": @"agri",
             @"123PBAB": @"",
             BIDV: @"bidv",
             @"123PDAIAB": @"",
             EIB: @"exim",
             @"123PGPB": @"",
             @"123PHDB": @"",
             @"123PMRTB": @"",
             @"123PMB": @"",
             @"123PNAB": @"",
             @"123PNVB": @"",
             @"123POCB": @"",
             @"123POCEB": @"",
             @"123PPGB": @"",
             SCB: @"sacom",
             @"123PSGB": @"",
             @"123PTCB": @"",
             @"123PTPB": @"",
             @"123PVAB": @"",
             @"VTB": @"viettin",
             @"123PVPB": @"",
             @"123PDAB": @"",
             VCB: @"vietcom",
             SGCB: @"scb",
             VCCB: @"vccb",
             ARB: @"arb",
             ACB: @"acb",
             VPB: @"vp",
             TCB: @"techcom",
             DAB: @"donga",
             MB: @"mb"
             };
}

+ (NSDictionary *)imagesMapperHorizontal {
    return @{@"123PABB": @"",
             @"123PACB": @"",
             @"123PAGB": @"ico_agribank",
             @"123PBAB": @"",
             BIDV: @"bidv_h",
             @"123PDAIAB": @"",
             EIB: @"eximbank_h",
             @"123PGPB": @"",
             @"123PHDB": @"",
             @"123PMRTB": @"",
             @"123PMB": @"",
             @"123PNAB": @"",
             @"123PNVB": @"",
             @"123POCB": @"",
             @"123POCEB": @"",
             @"123PPGB": @"",
             SCB: @"sacom_h",
             @"123PSGB": @"",
             @"123PTCB": @"",
             @"123PTPB": @"",
             @"123PVAB": @"",
             VTB: @"viettin_h",
             @"123PVPB": @"",
             @"123PDAB": @"",
             VCB: @"vietcombank_h",
             SGCB: @"scb_h",
             MASTERDEBIT : @"masterdebit_h",
             JCBDEBIT: @"jcbdebit_h",
             VISADEBIT: @"visadebit_h",
             VCCB:@"vccb_h",
             ARB:@"ico_agribank",
             ACB: @"",
             VPB: @"",
             TCB:@"ico_techcombank",
             DAB:@"ico_donga",
             MB:@"ico_mb"
             };
}

+ (ZPBankType)bankType:(NSString *)bankCode first6CardNo:(NSString *)first6CardNo {
    static NSDictionary *dic;
    if (!dic) {
        dic = @{@"123PABB": @(ZPAbbank),
                @"123PACB": @(ZPAcb),
                @"123PAGB": @(ZPAgribank),
                @"123PBAB": @(ZPBacabank),
                BIDV: @(ZPBidv),
                @"123PDAIAB": @(ZPDaiabank),
                EIB: @(ZPEximbank),
                @"123PGPB": @(ZPGpbank),
                @"123PHDB": @(ZPHdbank),
                @"123PMRTB": @(ZPMaritimebank),
                @"123PMB": @(ZPMbbank),
                @"123PNAB": @(ZPNamabank),
                @"123PNVB": @(ZPNavibank),
                @"123POCB": @(ZPOcb),
                @"123POCEB": @(ZPOceanbank),
                @"123PPGB": @(ZPPgbank),
                SCB: @(ZPSacombank),
                @"123PSGB": @(ZPSaigonbank),
                @"123PTCB": @(ZPTechcombank),
                @"123PTPB": @(ZPTienphongbank),
                @"123PVAB": @(ZPVietabank),
                VTB: @(ZPVietinbank),
                @"123PVPB": @(ZPVpbank),
                @"123PDAB": @(ZPDongabank),
                VCB: @(ZPVietcombank),
                SGCB: @(ZPScbbank),
                MASTER : @(ZPMaster),
                JCB: @(ZPJCB),
                VISA: @(ZPVisa),
                MASTERDEBIT : @(ZPMasterDebit),
                JCBDEBIT: @(ZPJCBDebit),
                VISADEBIT: @(ZPVisaDebit),
                VCCB:@(ZPVCCBBank),
                ARB:@(ZPAgribank),
                ACB:@(ZPAcb),
                VPB:@(ZPVpbank),
                TCB:@(ZPTechcombank),
                DAB:@(ZPDongabank),
                MB:@(ZPMbbank)
                };
    }
    
    NSNumber *code = [dic objectForKey:bankCode];
    if (code == nil) {
        bankCode = [[ZaloPayWalletSDKPayment sharedInstance] getCcBankCodeFromConfig:first6CardNo];
        if (bankCode) {
            code = [dic objectForKey:bankCode];
        }
    }
    return [code intValue];
}

+ (NSString *)bankName:(NSString *)bankCode {
    if (bankCode.length == 0) {
        return @"";
    }
    static NSMutableDictionary *maper;
    if (maper == nil) {
        maper = [NSMutableDictionary dictionary];
    }
    if (maper.count == 0) {
        @synchronized(maper) {
            NSArray *allBank = [ZPWalletManagerSwift sharedInstance].bankSupports;
            for (ZPBankSupport *oneBank in allBank) {
                NSString *bcode = oneBank.code;
                NSString *name = oneBank.name;
                if (bcode && name) {
                    [maper setObjectCheckNil:name forKey:bcode];
                }
            }
        }
    }
    return [maper stringForKey:bankCode defaultValue:@""];
}

+ (NSString *)bankName:(NSString *)bankCode first6CardNo:(NSString *)first6CardNo {
    if (bankCode.length == 0) {
        return @"";
    }
    static NSMutableDictionary *maper;
    if (maper == nil) {
        maper = [NSMutableDictionary dictionary];
    }
    if (maper.count == 0) {
        @synchronized(maper) {
            NSArray *allBank = [ZPWalletManagerSwift sharedInstance].bankSupports;
            for (ZPBankSupport *oneBank in allBank) {
                NSString *bcode = oneBank.code;
                NSString *name = oneBank.name;
                if (bcode && name) {
                    [maper setObjectCheckNil:name forKey:bcode];
                }
            }
        }
    }
    
    if ([[bankCode uppercaseString] isEqualToString:CC]) {
        bankCode = [[ZaloPayWalletSDKPayment sharedInstance] getCcBankCodeFromConfig:first6CardNo];
    }
    
    return [maper stringForKey:bankCode defaultValue:@""];
    
}

@end
