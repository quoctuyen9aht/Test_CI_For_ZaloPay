//
//  ZPCardCell.swift
//  ZaloPay
//
//  Created by phuongnl on 4/9/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit

class ZPCardCellSwift : ZPLinkCardBaseCellSwift {
    var cardView: ZPCardViewSwift!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.cardView = ZPCardViewSwift()
        self.addSubview(cardView)
        self.cardView.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.right.equalTo(-10)
            make.top.equalTo(10)
            make.bottom.equalTo(10)
        }
        self.setupGesture(for: self.cardView)
        self.clipsToBounds = true
        self.backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse(){
        super.prepareForReuse()
    }
    
    override func resetState(animate: Bool) {
        super.resetState(animate: animate)
        let duration = animate ? 0 : 0.15
        UIView.animate(withDuration: duration) { [weak self] in
            self?.cardView.transform = CGAffineTransform.identity
            self?.cardView.roundRect(10)
        }
    }
    
    func setSavedCard(savedCard: AnyObject) {
        self.cardView.setSavedCard(savedCard: savedCard)
    }
}
