import Foundation
import UIKit
import ZaloPayAnalyticsSwift
import ZaloPayCommonSwift
import RxSwift
import RxCocoa

class ZPLinkCardModelSwift : NSObject {
    weak var viewController: ZPLinkCardViewControllerSwift?
    public var tableView: UITableView?
    private var dataSource = [MakeBillDataProtocol]()
    private var footer: ZPFooterCardViewSwift?
    private let disposeBag = DisposeBag()
    
    override init() {
        super.init()
        self.observeNotification()
        self.createFooter()
        self.observerResource()
    }
    
    func createFooter() {
        let isSmalliPhone = UIScreen.main.bounds.size.width == 320;
        guard let views = Bundle.main.loadNibNamed("ZPFooterCardView", owner: nil, options: nil) as? [UIView] else {
            return
        }
        footer = (isSmalliPhone ? views.last : views.first) as? ZPFooterCardViewSwift
        if var fFooter = footer?.frame {
            let width = UIScreen.main.bounds.size.width
            fFooter.size.width = width
            footer?.frame = fFooter
        }
        footer?.button.rx.controlEvent(UIControlEvents.touchUpInside).subscribe(onNext: {
            [weak self] (_) in
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.linkbank_touch_add)
            self?.addBankButtonClick()
        }).disposed(by: disposeBag)
    }
    
    func sortDataSourceSignal() -> RACSignal<AnyObject> {
        return ZPWalletManagerSwift.sharedInstance.getListBankSupport().map({ (value) -> NSDictionary in
            return ZaloPayWalletSDKPayment.sharedInstance().bankDisplayOrder() as NSDictionary
        }).map({ [weak self] (dict) -> NSMutableArray in
            let dataSourceSorted: NSMutableArray = []
            let allCard = ZPBankApiWrapperSwift.sharedInstance.listCards()
            let allAccount = ZPBankApiWrapperSwift.sharedInstance.listAccounts()
            
            dataSourceSorted.addObjects(fromArrayNotNil: allCard)
            dataSourceSorted.addObjects(fromArrayNotNil: allAccount)
            dataSourceSorted.sort(comparator: { [weak self] (card1, card2) -> ComparisonResult in
                guard let strongSelf = self,
                    let dict = dict else {
                        return ComparisonResult.orderedSame
                }
                let bankCode1 = strongSelf.bankCode(fromObj: card1 as AnyObject)
                let bankCode2 = strongSelf.bankCode(fromObj: card2 as AnyObject)
                let index1 = dict.int(forKey: bankCode1, defaultValue: -1)
                let index2 = dict.int(forKey: bankCode2, defaultValue: -1)
                
                return (index1 < index2) ? ComparisonResult.orderedAscending : ComparisonResult.orderedDescending
            })
            return dataSourceSorted
        })
    }
    
    func bankCode(fromObj obj: AnyObject) -> String {
        var bankCode: String?
        if let saveCard = obj as? ZPSavedCard {
            if saveCard.isCCCard() {
                bankCode = ZaloPayWalletSDKPayment.sharedInstance().getCcBankCode(fromConfig: saveCard.first6CardNo)
            } else {
                bankCode = saveCard.bankCode
            }
        } else if let account = obj as? ZPSavedBankAccount {
            bankCode = account.bankCode
        }
        return bankCode ?? ""
    }
    
    func ensureDataAndReloadCardList(clearCache: Bool) {
        viewController?.startLoading()
        let signal = clearCache == true ? ZaloPayWalletSDKPayment.sharedInstance().loadCardListAndBankAccount() : ZaloPayWalletSDKPayment.sharedInstance().getGatewayInfo()
        signal?.subscribeCompleted{ [weak self] in
            self?.reloadCardList()
            self?.viewController?.doneLoading()
        }
    }
    
    func totalCardAndAccount() -> Int {
        return ZPBankApiWrapperSwift.sharedInstance.listCards().count + ZPBankApiWrapperSwift.sharedInstance.listAccounts().count
    }
    
    func reloadCardList() {
        sortByDisplayOrder()
    }
    
    func sortByDisplayOrder() {
        self.sortDataSourceSignal().subscribeNext({
            [weak self] (dataSource) in
            guard let dataSource = dataSource as? Array<MakeBillDataProtocol> else {
                return
            }
            self?.dataSource = dataSource
            self?.updateHeaderAndFooterTableView()
        })
    }
    
    func tableViewHeader() -> UIView {
        guard let empty = ZPLinkCardEmptyViewSwift.zp_viewFromSameNib() else {
            return UIView()
        }
        empty.setType(type: ZPBSupportType_Card)
        empty.btAddCard.rx.controlEvent(.touchUpInside).subscribe(onNext: {
            [weak self] (_) in
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.linkbank_touch_add)
            self?.addBankButtonClick()
        }).disposed(by: disposeBag)
        
        let h = empty.systemLayoutSizeFitting(CGSize(width: UIScreen.main.bounds.size.width, height: 500), withHorizontalFittingPriority: .required, verticalFittingPriority: .fittingSizeLevel).height
        
        var f = empty.frame
        f.size.width = UIScreen.main.bounds.size.width
        f.size.height = h
        empty.frame = f
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: viewController?.view.frame.size.width ?? 0, height: h))
        headerView.addSubview(empty)
        return headerView
    }
    
    func updateHeaderAndFooterTableView() {
        if dataSource.count == 0 {
            tableView?.tableHeaderView = tableViewHeader()
            tableView?.tableFooterView = nil
        } else {
            tableView?.tableHeaderView = nil
        }
        tableView?.reloadData()
    }
    
    func removeSaveCard(cardIndex: Int) {
        guard let card = dataSource[cardIndex] as? ZPSavedCard else {
            return
        }
        ZPAppFactory.sharedInstance().showHUDAdded(to: viewController?.view)
        let signal: RACSignal = ZPBankApiWrapperSwift.sharedInstance.removeCard(card: card)
        signal.subscribeError({ [weak self] error in
            guard let strongSelf = self,
                let error = error as NSError? else {
                    return
            }
            ZPAppFactory.sharedInstance().hideAllHUDs(for: strongSelf.viewController?.view)
            ZPDialogView.showDialog(with: DialogTypeNotification,
                                    message: error.apiErrorMessage(),
                                    cancelButtonTitle: R.string_ButtonLabel_Close(),
                                    otherButtonTitle: nil,
                                    completeHandle: nil)
            }, completed: { [weak self] in
                guard let strongSelf = self else {
                    return
                }
                ZPAppFactory.sharedInstance().hideAllHUDs(for: strongSelf.viewController?.view)
                strongSelf.dataSource.remove(at: cardIndex)
                strongSelf.tableView?.reloadData()
                if strongSelf.dataSource.count == 0 {
                    strongSelf.updateHeaderAndFooterTableView()
                }
                strongSelf.alertRemoveCardSuccess()
        })
    }
    
    func removeSavedBankAccount(cardIndex: Int) {
        guard let bankAccount = dataSource[cardIndex] as? ZPSavedBankAccount else {
            return
        }
        ZPAppFactory.sharedInstance().showHUDAdded(to: viewController?.view)
        let bankCustomerId = NetworkManager.sharedInstance().paymentUserId ?? ""
        let signal: RACSignal = ZPBankApiWrapperSwift.sharedInstance.removeBankAccount(bankCustomerId, bankAccount: bankAccount)
        signal.subscribeError({ [weak self] error in
            guard let strongSelf = self,
                let error = error as NSError? else {
                    return
            }
            ZPAppFactory.sharedInstance().hideAllHUDs(for: strongSelf.viewController?.view)
            ZPDialogView.showDialog(with: DialogTypeNotification,
                                    message: error.apiErrorMessage(),
                                    cancelButtonTitle: R.string_ButtonLabel_Close(),
                                    otherButtonTitle: nil,
                                    completeHandle: nil)
            }, completed: { [weak self] in
                guard let strongSelf = self else {
                    return
                }
                ZPAppFactory.sharedInstance().hideAllHUDs(for: strongSelf.viewController?.view)
                strongSelf.dataSource.remove(at: cardIndex)
                strongSelf.tableView?.reloadData()
                if strongSelf.dataSource.count == 0 {
                    strongSelf.updateHeaderAndFooterTableView()
                }
                strongSelf.alertRemoveCardSuccess()
        })
    }
    
    func alertRemoveCardSuccess() {
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.linkbank_delete_result)
        ZPDialogView.showDialog(with: DialogTypeSuccess, message: R.string_LinkCard_Remove_Success(), cancelButtonTitle: R.string_ButtonLabel_Close(), otherButtonTitle:nil, completeHandle: nil)
    }
}

extension ZPLinkCardModelSwift : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 && dataSource.count != 0{
            return footer?.frame.size.height ?? 0
        }
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return (section != 1 || dataSource.count == 0) ? nil : footer
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return 0
        }
        return self.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = ZPCardCellSwift.defaultCell(for: tableView) else {
            return UITableViewCell()
        }
        let savedCard = dataSource[safe: indexPath.row]
        cell.setSavedCard(savedCard: savedCard as AnyObject)
        cell.delegate = self
        cell.eChange?.takeUntil(cell.rx.methodInvoked(#selector(cell.prepareForReuse))).subscribe(onNext: { [weak self] (mCell: UITableViewCell) in
            guard let strongSelf = self,
                let table = strongSelf.tableView else {
                    return
            }
            for nCell in table.visibleCells {
                guard nCell != mCell,
                    let nCell = nCell as? ZPCardCellSwift else {
                        return
                }
                if  nCell.state == ZPLinkCardState.open {
                    nCell.resetState(animate: true)
                }
            }
        }).disposed(by: disposeBag)
        return cell
    }
}

extension ZPLinkCardModelSwift: ZPCardCellDelegate {
    func linkCardDidTapDelete(_ cell: ZPLinkCardBaseCellSwift) {
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.linkbank_delete)
        if NetworkState.sharedInstance().isReachable == false {
            alertNotInternetConnection()
            return
        }
        guard let indexPath = tableView?.indexPath(for: cell),
            let savedCard = dataSource[safe: indexPath.row] else {
                return
        }
        var strBankCode = ""
        if let card = savedCard as? ZPSavedCard {
            strBankCode = card.bankCode ?? ""
        } else if let card = savedCard as? ZPSavedBankAccount {
            strBankCode = card.bankCode ?? ""
        }
        self.isBankMaintain(bankCode: strBankCode).subscribe(onNext: { [weak self] (tuple : Array<Any>) in
            guard let first = tuple[0] as? Bool,
                let msg = tuple[1] as? String else {
                    return
            }
            if first {
                ZPDialogView.showDialog(
                    with: DialogTypeNotification,
                    message: msg,
                    cancelButtonTitle: R.string_ButtonLabel_OK(),
                    otherButtonTitle: [],
                    completeHandle: nil)
                return
            }
            if savedCard is ZPSavedCard {
                self?.showAlertDeleteSavedCard(cardIndex: indexPath.row)
                return
            }
            if let account = savedCard as? ZPSavedBankAccount {
                if account.bankCode == VCB {
                    self?.alertDeleteBankAccount(account: account)
                    return
                }
                self?.alertDeleteBankAccountUsingAPI(cardIndex: indexPath.row)
            }
        }).disposed(by: disposeBag)
    }
    
    func alertNotInternetConnection() {
        ZPDialogView.showDialog(with: DialogTypeNoInternet, title: R.string_Dialog_Warning(), message: R.string_NetworkError_NoConnectionMessage(), buttonTitles:[R.string_ButtonLabel_Close()], handler: nil)
    }
}

extension ZPLinkCardModelSwift {
    func isBankMaintain(bankCode: String) -> Observable<Array<Any>> {
        return Observable.just("loading")
            .do(onNext: { (_) in
                ZPAppFactory.sharedInstance().showHUDAdded(to: nil)
            })
            .asObservable()
            .flatMap({ _ -> Observable<Array<Any>> in
                return (~ZPWalletManagerSwift.sharedInstance.getListBankSupport())
                    .map({ (bankList) -> Array<Any?> in
                        guard let bankList = bankList as? Array<ZPBankSupport> else {
                            return [false, ""]
                        }
                        var code = bankCode
                        if ZPBankSupport.isCCCard(code) {
                            code = ZPBankSupport.masterCard().code
                        }
                        guard let bank = bankList.filter({ (evaluatedObject) -> Bool in
                            return evaluatedObject.code == code
                        }).first else {
                            return [false, ""]
                        }
                        let isMaintain = bank.isMaintain
                        if isMaintain {
                            let msg = bank.messageMaintain.count > 0 ? bank.messageMaintain : R.string_LinkCard_Maintain_Message()
                            return [isMaintain, msg]
                        }
                        
                        // Check minibank
                        guard let miniBank = ZaloPayWalletSDKPayment.sharedInstance().getMiniBanksForMapping()?
                            .filter({ (evaluatedObject) -> Bool in
                                return evaluatedObject.bankCode == code
                            }).first else {
                                return [false, ""]
                        }
                        let msg = miniBank.maintenancemsg.count > 0 ? miniBank.maintenancemsg : R.string_LinkCard_Maintain_Message()
                        return [miniBank.minibankStatus == ZPMiniBankStatus.maintenance, msg]
                    })
            })
            .do(onNext:{ _ in
                ZPAppFactory.sharedInstance().hideAllHUDs(for: nil)
            }, onError: { _ in
                ZPAppFactory.sharedInstance().hideAllHUDs(for: nil)
            })
    }
    
    func showAlertDeleteSavedCard(cardIndex: Int) {
        //    Bạn có chắc chắn muốn xoá thẻ không?
        ZPAppFactory.sharedInstance().checkPinNoCache(withMessage: R.string_LinkBank_Message_Input_Password())
            .subscribeNext({
                [weak self] (r) in
                guard let strongSelf = self,
                    let r = r as? Bool else {
                        return
                }
                if r {
                    strongSelf.removeSaveCard(cardIndex: cardIndex)
                }
            })
    }
    
    func alertDeleteBankAccount(account: ZPSavedBankAccount) {
        let name = ZPBankMapping.bankName(account.bankCode) ?? ""
        let message = String(format: R.string_LinkBank_Delete_Notification_Message(), name)
        
        ZPDialogView.showDialog(with: DialogTypeNotification,
                                message: message,
                                cancelButtonTitle: R.string_ButtonLabel_Skip(),
                                otherButtonTitle: [R.string_ButtonLabel_OK()],
                                completeHandle: { [weak self] (buttonIndex, cancelButtonIndex) in
                                    guard let viewController = self?.viewController else {
                                        return
                                    }
                                    if buttonIndex != cancelButtonIndex {
                                        ZPBankApiWrapperSwift.sharedInstance.removeBankAccount(account: account, viewController: viewController)
                                    }
        })
    }
    
    func alertDeleteBankAccountUsingAPI(cardIndex: Int) {
        //    Bạn có chắc chắn muốn xoá thẻ không?
        ZPAppFactory.sharedInstance().checkPinNoCache(withMessage: R.string_LinkBank_Message_Input_Password())
            .subscribeNext({
                [weak self] (r) in
                guard let strongSelf = self,
                    let r = r as? Bool else {
                        return
                }
                if r {
                    strongSelf.removeSavedBankAccount(cardIndex: cardIndex)
                }
            })
    }
    func addBankButtonClick() {
        guard let viewController = viewController else {
            return
        }
        ZPBankApiWrapperSwift.sharedInstance.mapBank(with: viewController, appId: kZaloPayClientAppId, transType: .atmMapCard).subscribeNext({ [weak self] (x) in
            guard let strongSelf = self,
                let viewController = strongSelf.viewController else {
                    return
            }
            viewController.navigationController?.popToViewController(viewController, animated: true)
            }, error: { [weak self] (error) in
                guard let strongSelf = self,
                    let viewController = strongSelf.viewController else {
                        return
                }
                viewController.navigationController?.popToViewController(viewController, animated: true)
        })
    }
    
    func observeNotification() {
        let notifiSignal = NotificationCenter.default.rx.notification(Notification.Name.Reload_AccountList_, object: nil)
        let untilSignal = self.rx.deallocating
        let reloadCardSignal = NotificationCenter.default.rx.notification(Notification.Name.Reload_CardList_, object: nil)
        
        Observable.merge([notifiSignal, reloadCardSignal]).observeOn(MainScheduler.instance).takeUntil(untilSignal).subscribe(onNext: { [weak self] (_) in
            self?.reloadCardList()
        }).disposed(by: disposeBag)
    }
    
    func observerResource() {
        ReactNativeAppManager.sharedInstance().observe(self, fromAppId: showshowAppId, complete: { [weak self] in
            self?.tableView?.reloadData()
        })
    }
}
