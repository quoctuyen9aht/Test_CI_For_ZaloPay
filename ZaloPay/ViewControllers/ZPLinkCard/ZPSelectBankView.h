//
//  ZPSelectBankView.h
//  ZaloPay
//
//  Created by bonnpv on 1/17/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZPBankSupport.h"

typedef  void (^SelectBankCallBack)(ZPBankSupport *bank);

@interface ZPSelectBankView : UIView
+ (void)showWithType:(ZPBSupportType)type withComplete:(SelectBankCallBack)completeBlock;
@end
