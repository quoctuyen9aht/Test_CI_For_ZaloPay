//
//  ZPLinkCardBaseCell.swift
//  ZaloPay
//
//  Created by phuongnl on 4/10/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift
import SnapKit

enum ZPLinkCardState : Int {
    case close = 0
    case open
}

@objc protocol ZPCardCellDelegate : NSObjectProtocol {
    @objc func linkCardDidTapDelete(_ cell: ZPLinkCardBaseCellSwift)
}

class ZPLinkCardBaseCellSwift : UITableViewCell {
    public weak var delegate: ZPCardCellDelegate?
    public var state: ZPLinkCardState?
    public var eChange: PublishSubject<ZPLinkCardBaseCellSwift>?
    public var btnDelete: UIButton!
    public var kMaxDeleteButton: CGFloat = 0
    public var lblName: UILabel!
    private let disposeBag = DisposeBag()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.kMaxDeleteButton = UIScreen.main.bounds.size.width == 320 ? 55 : 76
        self.setupView()
        self.eChange = PublishSubject<ZPLinkCardBaseCellSwift>()
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        btnDelete = UIButton(frame: CGRect.zero)
        btnDelete.backgroundColor = UIColor(hexString: "#ff3a30")
        btnDelete.alpha = 0
        self.addSubview(btnDelete)
        btnDelete.snp.makeConstraints { (make) in
            make.left.equalTo(60)
            make.right.equalTo(0)
            make.top.equalTo(10)
            make.bottom.equalTo(0)
        }
        
        lblName = UILabel(frame: CGRect.zero)
        self.addSubview(lblName)
        lblName.alpha = 0
        lblName.numberOfLines = 0
        lblName.textAlignment = .center
        lblName.zpMainBlackRegular()
        lblName.textColor = UIColor.white
        let left = (kMaxDeleteButton - 40) / 2 + 3
        lblName.snp.makeConstraints { (make) in
            make.right.equalTo(-left)
            make.centerY.equalToSuperview()
            make.height.greaterThanOrEqualTo(0)
            make.width.equalTo(40)
        }
        self.setTitleDelete()
        btnDelete.rx.controlEvent(.touchUpInside).subscribe(onNext: { [weak self] (_) in
            guard let strongSelf = self,
                let delegate = self?.delegate else {
                    return
            }
            if delegate.responds(to: #selector(delegate.linkCardDidTapDelete(_ :))) {
                delegate.linkCardDidTapDelete(strongSelf)
            }
        }).disposed(by: disposeBag)
    }
    
    func setState(state: ZPLinkCardState) {
        if (state != self.state && state == ZPLinkCardState.open) {
            self.eChange?.onNext(self)
        }
        self.state = state
    }
    
    func setTitleDelete() {
        let delete = UILabel.iconCode(withName: "general_delete_card") ?? ""
        lblName.text = String(format: "%@\n%@", delete, R.string_ButtonLabel_Cancel())
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 12
        style.alignment = NSTextAlignment.center
        
        if let attributedText = lblName.attributedText {
            let att = NSMutableAttributedString(attributedString: attributedText)
            att.addAttributes([NSAttributedStringKey.font: UIFont.zaloPay(withSize: 25)], range: NSMakeRange(0, delete.count))
            let count = lblName.text?.count ?? 0
            att.addAttributes([NSAttributedStringKey.paragraphStyle: style], range: NSMakeRange(0, count))
            lblName.attributedText = att
        }
    }
    
    func resetState(animate: Bool) {
        state = ZPLinkCardState.close
        btnDelete.alpha = 0
        lblName.alpha = 0
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.resetState(animate: false)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension ZPLinkCardBaseCellSwift {
    func setupGesture(for cardView: UIView) {
        var panGesture = UIPanGestureRecognizer(target: nil, action: nil)
        panGesture.delegate = self
        cardView.addGestureRecognizer(panGesture)
        
        var tapGesture = UITapGestureRecognizer(target: nil, action: nil)
        tapGesture.numberOfTapsRequired = 1
        cardView.addGestureRecognizer(tapGesture)
        
        var cornerRadius:CGFloat = 4
        if cardView is ZPCardViewSwift {
            cornerRadius = 10
        }
        
        //Pan Gesture
        panGesture.rx.event.takeUntil(self.rx.deallocating).subscribe(onNext: { [weak self] (p: UIPanGestureRecognizer) in
            guard let strongSelf = self else{
                return
            }
            let translation = p.translation(in: cardView)
            let state = p.state
            
            switch state {
            case .began:
                break
                
            case .changed:
                cardView.transform = cardView.transform.translatedBy(x: translation.x, y: 0)
                var nextAlpha: CGFloat = 0
                let offsetX = cardView.transform.tx
                if offsetX < 0 {
                    nextAlpha = min(abs(offsetX) + 40, strongSelf.kMaxDeleteButton) / strongSelf.kMaxDeleteButton
                }
                strongSelf.btnDelete.alpha = nextAlpha
                strongSelf.lblName.alpha = nextAlpha
                cardView.layer.cornerRadius = cornerRadius * (1 - nextAlpha)
                break
                
            case .ended:
                var v:CGFloat = 0
                let offsetX:CGFloat = cardView.transform.tx
                if offsetX <= strongSelf.kMaxDeleteButton * -1 / 2 {
                    v = strongSelf.kMaxDeleteButton * -1
                }
                let nextTransform = v == 0 ? CGAffineTransform.identity : CGAffineTransform(translationX: v, y: 0)
                let nextAlpha:CGFloat = v == 0 ? 0 : 1
                let nextRadius:CGFloat = cornerRadius * (1 - nextAlpha)
                strongSelf.state = v == 0 ? ZPLinkCardState.close : ZPLinkCardState.open
                UIView.animate(withDuration: 0.15, delay: 0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
                    strongSelf.btnDelete.alpha = nextAlpha
                    strongSelf.lblName.alpha = nextAlpha
                    cardView.transform = nextTransform
                    cardView.layer.cornerRadius = nextRadius
                }, completion: nil)
                break
                
            default:
                break
            }
            p.setTranslation(CGPoint.zero, in: cardView)
        }).disposed(by: disposeBag)
        
        tapGesture.rx.event.takeUntil(self.rx.deallocating).subscribe(onNext: { [weak self] (_) in
            guard let strongSelf = self else {
                return
            }
            var v:CGFloat = 0
            let offsetX:CGFloat = cardView.transform.tx
            if offsetX > strongSelf.kMaxDeleteButton * -1 / 2 {
                v = strongSelf.kMaxDeleteButton * -1
            }
            let nextTransform = v == 0 ? CGAffineTransform.identity : CGAffineTransform(translationX: v, y: 0)
            let nextAlpha: CGFloat = v == 0 ? 0 : 1
            let nextRadius: CGFloat = cornerRadius * (1 - nextAlpha)
            strongSelf.state = v == 0 ? ZPLinkCardState.close : ZPLinkCardState.open
            UIView.animate(withDuration: 0.15, delay: 0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
                strongSelf.btnDelete.alpha = nextAlpha
                strongSelf.lblName.alpha = nextAlpha
                cardView.transform = nextTransform
                cardView.layer.cornerRadius = nextRadius
            }, completion: nil)
        }).disposed(by: disposeBag)
    }
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        if let gestureRecognizer = gestureRecognizer as? UIPanGestureRecognizer {
            let translation = gestureRecognizer.translation(in: self)
            return translation.y == 0
        }
        return true
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
