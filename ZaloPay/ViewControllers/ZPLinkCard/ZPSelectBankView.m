//
//  ZPSelectBankView.m
//  ZaloPay
//
//  Created by bonnpv on 1/17/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPSelectBankView.h"
#import "ZPSelectBankCell.h"
#import <ZaloPayCommonSwift/ZaloPayCommonSwift-Swift.h>
#import <ZaloPayCommon/UILabel+ZaloPayStyle.h>
#import ZALOPAY_MODULE_SWIFT
@interface ZPSelectBankView()<UITableViewDelegate, UITableViewDataSource, ZPSelectBankCellDelegate>
@property (nonatomic, strong) NSArray *dataSource;
@property (nonatomic) ZPBSupportType type;
@property (nonatomic, copy) SelectBankCallBack callBack;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, strong) UILabel *labelTitle;
@property (nonatomic, strong) UILabel *labelIcon;
@end

@implementation ZPSelectBankView

+ (void)showWithType:(ZPBSupportType)type withComplete:(SelectBankCallBack)completeBlock {
    NSArray *banks = [ZPWalletManagerSwift sharedInstance].bankSupports;
    if (banks.count > 0) {
        [self processBankList:banks type:type complete:completeBlock];
        return;
    }
    
    [[ZPAppFactory sharedInstance] showHUDAddedTo:nil];
    [[[ZPWalletManagerSwift sharedInstance] getListBankSupport] subscribeCompleted:^{
        [[ZPAppFactory sharedInstance] hideAllHUDsForView:nil];
        NSArray *allBank = [ZPWalletManagerSwift sharedInstance].bankSupports;
        [self processBankList:allBank type:type complete:completeBlock];
    }];
}

+ (void)processBankList:(NSArray *)allBank
                   type:(ZPBSupportType)type
               complete:(SelectBankCallBack)completeBlock {
    NSMutableArray *toShow = [NSMutableArray array];
    for (ZPBankSupport *bank in allBank) {
        if (bank.type == type) {
            [toShow addObject:bank];
        }
    }
    if (toShow.count == 0) {
        return;
    }
    if (toShow.count == 1 && type == ZPBSupportType_Account && completeBlock) {
        // trường hợp chọn ngân hàng để liên kết tài khoản mà chỉ có một ngân hàng -> tự động chọn luôn ngân hàng đó
        completeBlock(toShow.firstObject);
        return;
    }
    [self showBankList:toShow type:(type) withComplete:completeBlock];
}

+ (void)showBankList:(NSArray *)listBank type:(ZPBSupportType)type withComplete:(SelectBankCallBack)completeBlock {
    ZPSelectBankView *view = [[ZPSelectBankView alloc] initWithBankList:listBank type:type];
    view.callBack = completeBlock;
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview:view];
}

- (id)initWithBankList:(NSArray *)bankList type:(ZPBSupportType)type{
    CGRect frame = [UIScreen mainScreen].bounds;
    self = [super initWithFrame:frame];
    if (self) {
        self.dataSource = bankList;
        self.type = type;
        [self setupView];
    }
    return self;
}

- (void)setupView {
    self.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    self.contentView = [[UIView alloc] init];
    [self addSubview:self.contentView];
    self.contentView.backgroundColor = [UIColor whiteColor];
    [self.contentView roundRect:3];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo([self contentPadding]);
        make.right.equalTo(-[self contentPadding]);
        make.height.equalTo([self contentHeight]);
        make.centerY.equalTo(0);
    }];
    [self setupContentView];
}

- (void)setupContentView {
    NSString *title =  [R string_LinkAccount_Select_Bank_To_Link_Account];
    if (self.type == ZPBSupportType_Card) {
        title = [R string_BankCard_Support_ByZaloPay];
    }    
    [self addLabelIcon];
    [self addLabelTitle:title];
    [self addTableView];
    [self addCloseButton];
}

- (float)contentPadding {
    if ([UIView isScreen2x]) {
        return 20.0;
    }
    return 40.0;
}

- (float)padding {
    return 20.0;
}

- (float)tableViewHeight {
    float numberCell = [self.dataSource subArrayCount:2];
    float h = numberCell * [ZPSelectBankCell height];
    return MIN(h, 200);
}

- (float)contentHeight {
    return [self tableViewHeight] + 220;
}

- (void)addLabelIcon {
    self.labelIcon = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 65, 65)];
    [self.contentView addSubview:self.labelIcon];
    self.labelIcon.font = [UIFont iconFontWithSize:self.labelIcon.frame.size.height];
    [self.labelIcon setIconfont:@"dialoge_infor"];
    self.labelIcon.textColor = [UIColor zaloBaseColor];
    [self.labelIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(25);
        make.size.equalTo(self.labelIcon.frame.size);
        make.centerX.equalTo(self.contentView.mas_centerX);
    }];
}

- (void)addLabelTitle:(NSString *)title {
    self.labelTitle = [[UILabel alloc] init];
    [self.contentView addSubview:self.labelTitle];
    //self.labelTitle.font = [UIFont SFUITextRegularWithSize:15];
    [self.labelTitle zpMainBlackRegular];
    self.labelTitle.textAlignment = NSTextAlignmentCenter;
    self.labelTitle.numberOfLines = 0;
    self.labelTitle.textColor = [UIColor defaultText];
    self.labelTitle.text = title;
    [self.labelTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo([self padding]);
        make.right.equalTo(-[self padding]);
        make.top.equalTo(self.labelIcon.mas_bottom).with.offset(20);
        make.height.equalTo(40);
    }];
}

- (void)addTableView {
    self.tableView = [[UITableView alloc] init];
    [self.contentView addSubview:self.tableView];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo([self padding]);
        make.right.equalTo(-[self padding]);
        make.top.equalTo(self.labelTitle.mas_bottom).with.offset(10);
        make.height.equalTo([self tableViewHeight]);
    }];
}

- (void)addCloseButton {
    ZPLineView *line = [[ZPLineView alloc] init];
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(0);
        make.right.equalTo(0);
        make.height.equalTo(1);
        make.top.equalTo(self.tableView.mas_bottom).with.offset(10);
    }];
    self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.closeButton addTarget:self action:@selector(closeButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.closeButton];
    [self.closeButton setTitle:[R string_ButtonLabel_Close] forState:UIControlStateNormal];
    [self.closeButton setTitleColor:[UIColor zaloBaseColor] forState:UIControlStateNormal];
    //self.closeButton.titleLabel.font = [UIFont SFUITextRegularWithSize:18];
    [self.closeButton.titleLabel zpMainBlackRegular];
    [self.closeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(0);
        make.top.equalTo(line.mas_bottom);
        make.left.equalTo(0);
        make.right.equalTo(0);
    }];
}

- (IBAction)closeButtonClick:(id)sender {
    if (self.callBack) {
        self.callBack(nil);
        self.callBack = nil;
    }
    [self removeFromSuperview];
}

#pragma mark - Cell Delegate

- (void)cell:(ZPSelectBankCell *)cell didSelectBank:(ZPBankSupport *)bank {
    if (self.callBack) {
        self.callBack(bank);
        self.callBack = nil;
        [self removeFromSuperview];
    }
}

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataSource subArrayCount:2];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *items = [self.dataSource subArrayAtIndex:indexPath.row withTotalItem:2];
    ZPSelectBankCell *cell = [ZPSelectBankCell defaultCellForTableView:tableView];
    cell.delegate = self;
    [cell setItems:items];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [ZPSelectBankCell height];
}

@end
