//
//  ZPSelectBankCell.m
//  ZaloPay
//
//  Created by bonnpv on 1/17/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPSelectBankCell.h"
#import "ZPBankSupport.h"
@interface ZPSelectBankCell()
@property (nonatomic, strong) NSArray *banks;
@property (nonatomic, strong) UIButton *button1;
@property (nonatomic, strong) UIButton *button2;
@end

@implementation ZPSelectBankCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupCell];
    }
    return self;
}

- (void)setupCell {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    self.button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    self.button1.tag = 0;
    self.button2.tag = 0;
    [self.button1 addTarget:self action:@selector(selectBankClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.button2 addTarget:self action:@selector(selectBankClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.contentView addSubview:self.button1];
    [self.contentView addSubview:self.button2];
    
    [self.button1 roundRect:3 borderColor:[UIColor lineColor] borderWidth:0.5];
    self.button1.backgroundColor = [UIColor whiteColor];
    [self.button2 roundRect:3 borderColor:[UIColor lineColor] borderWidth:0.5];
    self.button2.backgroundColor = [UIColor whiteColor];
    UIImage *image = [UIImage imageNamed:@"viettin"];
    [self.button1 setImage:image forState:UIControlStateNormal];
    [self.button2 setImage:image forState:UIControlStateNormal];
    
    [self.button1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.left.equalTo(0);
        make.bottom.equalTo(-[self padding]);
        make.right.equalTo(self.contentView.mas_centerX).with.offset(-[self padding]/2);
    }];
    [self.button2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.right.equalTo(0);
        make.bottom.equalTo(-[self padding]);
        make.left.equalTo(self.contentView.mas_centerX).with.offset([self padding]/2);
    }];
}

- (float)padding {
    return 10;
}

+ (float)height {
    return 60;
}

- (void)setItems:(NSArray *)items {
    self.banks = items;
    ZPBankSupport *bank1 = [items safeObjectAtIndex:0];
    ZPBankSupport *bank2 = [items safeObjectAtIndex:1];
    [self setupButton:self.button1 withBank:bank1];
    [self setupButton:self.button2 withBank:bank2];
}

- (void)setupButton:(UIButton *)button withBank:(ZPBankSupport *)bank {
    if (bank == nil) {
        button.hidden = true;
        return;
    }
    button.hidden = false;
    UIImage *image = [UIImage imageNamed:bank.iconName];
    [button setImage:image forState:UIControlStateNormal];    
}

- (IBAction)selectBankClick:(UIButton *)sender {
    ZPBankSupport *bank = [self.banks safeObjectAtIndex:sender.tag];
    [self.delegate cell:self didSelectBank:bank];
}

@end
