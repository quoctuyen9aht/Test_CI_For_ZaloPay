//
//  ZPLinkCardViewController.swift
//  ZaloPay
//
//  Created by phuongnl on 3/29/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayAnalyticsSwift
import ZaloPayCommonSwift
import RxSwift
import RxCocoa
import SnapKit
import Synchronized

class ZPLinkCardViewControllerSwift : BaseViewController {
    var linkCardDataSource: ZPLinkCardModelSwift?
    var linkCardTableView: UITableView?
    var refreshControl: ZPRefreshControl?
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = R.string_LinkCard_ManagaCard()
        self.createAddButton()
        self.configTableView()
        self.configDataSource()
    }
    
    func configDataSource() {
        let signal = self.rx.methodInvoked(#selector(viewWillAppear(_:))).skip(1)
        let until = self.rx.deallocating
        
        signal.takeUntil(until).subscribe(onNext: { [weak self] (_) in
            self?.linkCardDataSource?.reloadCardList()
        }).disposed(by: disposeBag)
        linkCardDataSource?.ensureDataAndReloadCardList(clearCache: false)
    }
    
    override func backEventId() -> Int {
        return ZPAnalyticEventAction.linkbank_touch_back.rawValue
    }
    
    
    func createAddButton() {
        let add = UIButton.init(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        add.titleLabel?.font = UIFont.iconFont(withSize: 18)
        add.setIconFont("linkaccount_add", for: .normal)
        add.setIconFont("linkaccount_add", for: .highlighted)
        add.setTitleColor(UIColor.white, for: .normal)
        add.setTitleColor(UIColor(red: 255, green: 255, blue: 255, alpha: 60), for: .highlighted)
        add.addTarget(self, action: #selector(processChangeColor(_:)), for: .touchDown)
        let rightBar = UIBarButtonItem(customView: add)
        self.navigationItem.rightBarButtonItem = rightBar
        
        add.rx.controlEvent(UIControlEvents.touchUpInside).subscribe(onNext: { [weak self] (_) in
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.linkbank_touch_add_icon)
            self?.linkCardDataSource?.addBankButtonClick()
        }).disposed(by: disposeBag)
    }
    
    @objc func processChangeColor(_ sender: UIButton?) {
        sender?.isHighlighted = true
        UIView.animate(withDuration: 1, delay: 0, options: [], animations: {} , completion: { (_) in
            sender?.isHighlighted = false
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.trackingScreen()
        ZPTrackingHelper.shared().trackScreen(with: self)
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.linkbank_launched)
        guard let isNavigationBarHidden = navigationController?.isNavigationBarHidden else {
            return
        }
        if isNavigationBarHidden {
            navigationController?.setNavigationBarHidden(false, animated: false)
        }
        synchronized(self) { ()  in
            refreshControl?.resetAnimation()
        }
    }
    
    override func screenName() -> String {
        let totalAccount = self.linkCardDataSource?.totalCardAndAccount()
        let screenName = totalAccount == 0 ? ZPTrackerScreen.Screen_iOS_Bank_Intro : ZPTrackerScreen.Screen_iOS_Bank_Main
        return screenName
    }
    
    func createTableView() -> UITableView {
        let tableView = UITableView(frame: CGRect.zero, style: .plain)
        view.addSubview(tableView)
        tableView.snp.makeConstraints{ (make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.bottom.equalTo(0)
        }
        tableView.rowHeight = 134
        tableView.backgroundColor = UIColor.defaultBackground()
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        return tableView
    }
    
    func configTableView() {
        linkCardTableView = createTableView()
        linkCardDataSource = ZPLinkCardModelSwift()
        linkCardDataSource?.viewController = self
        linkCardDataSource?.tableView = linkCardTableView
        linkCardTableView?.delegate = linkCardDataSource
        linkCardTableView?.dataSource = linkCardDataSource
        setupControlRefresh()
    }
    
    func setupControlRefresh() {
        refreshControl = ZPRefreshControl(with: linkCardTableView)
        if linkCardTableView == nil || refreshControl == nil {
            return
        }
        if #available(iOS 10.0, *) {
            linkCardTableView?.refreshControl = refreshControl
        } else {
            linkCardTableView?.addSubview(refreshControl!)
        }
        refreshControl!.addTarget(self, action: #selector(refreshTableView), for: UIControlEvents.valueChanged)
    }
    
    @objc func refreshTableView() {
        linkCardDataSource?.ensureDataAndReloadCardList(clearCache: true)
    }
    
    func startLoading() {
        refreshControl?.beginRefreshing()
        ZPAppFactory.sharedInstance().showHUDAdded(to: nil)
    }
    
    @objc func doneLoading() {
        ZPAppFactory.sharedInstance().hideAllHUDs(for: nil)
        synchronized(self) { ()  in
            refreshControl?.endRefreshing()
        }
    }
    
    func trackingScreen() {
        self.rx.methodInvoked(#selector(doneLoading)).take(1).subscribe(onNext: { [weak self] (_) in
            let totalAccount = self?.linkCardDataSource?.totalCardAndAccount()
            let screen = totalAccount == 0 ? ZPTrackerScreen.Screen_iOS_Bank_Intro : ZPTrackerScreen.Screen_iOS_Bank_Main
            ZPTrackingHelper.shared().trackScreen(withName: screen)
        }).disposed(by: disposeBag)
    }
}

