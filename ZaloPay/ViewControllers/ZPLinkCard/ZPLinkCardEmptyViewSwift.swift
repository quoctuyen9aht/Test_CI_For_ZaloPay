//
//  ZPLinkCardEmptyView.swift
//  ZaloPay
//
//  Created by phuongnl on 4/11/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit

class ZPLinkCardEmptyViewSwift : UIView {
    @IBOutlet weak var btAddCard: UIButton!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var lbNoCardTitle: UILabel!
    @IBOutlet weak var lbSercurityTitle: UILabel!
    @IBOutlet weak var lbSercurityDescription: UILabel!
    @IBOutlet weak var sercurityTopLine: UIView!
    
    lazy var lblNoticePhone: UILabel? = {
        return self.viewWithTag(243) as? UILabel
    }()
    
    lazy var iconVCB: UIImageView? = {
        return self.viewWithTag(244) as? UIImageView
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        sercurityTopLine.backgroundColor = UIColor.clear
        cardView.backgroundColor = UIColor.clear
        
        btAddCard.setBackgroundColor(UIColor.zaloBase(), for: .normal)
        btAddCard.roundRect(3)
        btAddCard.titleLabel?.zpDefaultBtnTitleStyle()
        
        cardView.constraints.enumerated().forEach { (e) in
            if e.element.identifier == "heightNoItem" {
                e.element.constant = e.element.constant * UIScreen.main.bounds.size.width / 414
                return
            }
        }
        lbSercurityTitle.zpMediumTitleStyle()
        lbSercurityTitle.textColor = UIColor.defaultText()
        self.backgroundColor = UIColor.defaultBackground()
        self.layoutIfNeeded()
    }
    
    func setType(type: ZPBSupportType) {
        var buttonTitle = "Thêm Liên Kết"
        let title = "Bạn chưa liên kết ngân hàng"
        let plus = UILabel.iconCode(withName: "linkaccount_add") ?? ""
        buttonTitle = String(format: "%@  %@", plus, buttonTitle)
        
        let font = btAddCard.titleLabel?.font ?? UIFont.systemFont(ofSize: 17)
        let att = NSMutableAttributedString(string: buttonTitle, attributes: [NSAttributedStringKey.font: font])
        att.addAttributes([NSAttributedStringKey.font: UIFont.zaloPay(withSize: 18)], range: NSMakeRange(0, plus.count))
        
        lbNoCardTitle.zpPlaceHolderRegular()
        lbNoCardTitle.text = title
        btAddCard.setAttributedTitle(att, for: .normal)
        btAddCard.setBackgroundColor(UIColor.buttonHighlight(), for: .highlighted)
    }
}
