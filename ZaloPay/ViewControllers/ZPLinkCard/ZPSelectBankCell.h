//
//  ZPSelectBankCell.h
//  ZaloPay
//
//  Created by bonnpv on 1/17/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ZPSelectBankCell;
@class ZPBankSupport;

@protocol ZPSelectBankCellDelegate<NSObject>
- (void)cell:(ZPSelectBankCell *)cell didSelectBank:(ZPBankSupport *)bank;
@end

@interface ZPSelectBankCell : UITableViewCell
@property (nonatomic, weak) id<ZPSelectBankCellDelegate>delegate;
+ (float)height;
- (void)setItems:(NSArray *)items;
@end
