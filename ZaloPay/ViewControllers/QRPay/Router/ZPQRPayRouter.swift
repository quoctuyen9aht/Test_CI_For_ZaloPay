//
//  ZPQRPayScannerRouter.swift
//  ZaloPay
//
//  Created by ThuChau on 3/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayAnalyticsSwift
import ZaloPayConfig
import ZaloPayWeb

class ZPQRPayRouter: ZPQRPayRouterProtocol {
    weak var vc: BillHandleBaseViewController?
    
    static func createQRPayViewController(pageVCParent: ZPQRPayPageViewController? = nil) -> ZPQRPayViewController {
        let view = ZPQRPayViewController()
        let interactor = ZPQRPayInteractor()
        let presenter = ZPQRPayPresenter()
        let router = ZPQRPayRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        view.pageVCParent = pageVCParent
        return view
    }
    
    func willPushFrom(_ viewController: BillHandleBaseViewController) {
        self.vc = viewController
    }
    
    func showTransferReceiver(transferData: UserTransferData) {
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.pay_scanqr_result_sucess_moneytransfer)
        let viewController = TransferReceiverRouter.assembleModule(transferData: transferData, orderSource: OrderSource_QR)
        self.vc?.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func showBillViewController(bill: ZPBill) {
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.pay_scanqr_result_sucess_payment)
        self.vc?.showBillViewController(bill)
    }
    
    func showWebApp(withUrl url: String) {
        let web = ZPWQRWebApp(url: url, webHandle: ZPWebHandle())
        self.vc?.navigationController?.pushViewController(web, animated: true)
    }
    func showQuickPay() {
        guard let vcGet = self.vc else {
            return
        }
        ZPCenterRouter.launch(routerId: .paymentCode, from: vcGet)
    }
}
