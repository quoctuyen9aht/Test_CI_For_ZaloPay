//
//  ZPQRPayScannerPresenter.swift
//  ZaloPay
//
//  Created by ThuChau on 3/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift
import ZaloPayCommonSwift

class ZPQRPayPresenter: NSObject, ZPQRPayPresenterProtocol {
    fileprivate let disposeBag = DisposeBag()
    
    var interactor: ZPQRPayInteractorProtocol?
    var router: ZPQRPayRouterProtocol?
    weak var view: ZPQRPayViewControllerProtocol?

    // MARK: - interactor protocols
    func observableNetworkStatus() {
        let authenSuccessObservable = (~ZPConnectionManager.sharedInstance().eAuthenSuccess).map({ $0?.boolValue ?? true })
        let networkSignalObservable = (~NetworkState.sharedInstance().networkSignal).map { ($0 as? Bool) ?? true }
        let combineLatestObservable = Observable.combineLatest([authenSuccessObservable, networkSignalObservable])
            .takeUntil(self.rx.deallocating)
            .observeOn(MainScheduler.instance)
        
        combineLatestObservable
            .map({ $0.reduce(false, { $0 || $1 }) })
            .distinctUntilChanged()
            .bind
            { [weak self] in
                self?.updateScreen(networkStatus: $0)
            }.disposed(by: disposeBag)
    }
    
    func handleQRCodeData(data: String) {
        self.interactor?.handleQRCodeData(data: data)
    }
    
    // MARK: - view protocols
    func updateScreen(networkStatus: Bool) {
        self.view?.updateScreen(networkStatus: networkStatus)
    }
    
    // MARK: - router protocols
    func prepare() {
        if let viewController = self.view as? BillHandleBaseViewController {
            self.router?.willPushFrom(viewController)
        }
    }
    
    func showTransferReceiver(transferData: UserTransferData) {
        self.view?.stopShowLoadingView()
        self.router?.showTransferReceiver(transferData: transferData)
    }
    
    func showBillViewController(bill: ZPBill) {
        self.view?.startProcessingShowBillDetail()
        self.router?.showBillViewController(bill: bill)
    }
    
    func showWebApp(withUrl url: String) {
        self.view?.stopShowLoadingView()
        self.router?.showWebApp(withUrl: url)
    }
    
    func showQuickPay() {
        self.view?.stopShowLoadingView()
        self.router?.showQuickPay()
    }
    
    func onAction(action: QRActionType) {
        switch action {
        case .AlertError(let error):
            self.view?.alertError(error: error);
        case .AlertErrorCurrentUser:
            self.view?.alertCurrentUser();
        case .AlertQRError:
            self.view?.alertQRError()
        case .AlertNewAppVersion:
            self.view?.alertUpdateNewVersion()
        case .AlertNetworkError:
            self.view?.alertNetworkErrorNoConnectionMessage()
        case .ShowWebApp(let url):
            self.showWebApp(withUrl: url)
        case .ProcessOrderData(let bill):
            self.showBillViewController(bill: bill)
        case .ProcessTransferMoney(let data):
            self.showTransferReceiver(transferData: data)
        case .ProcessQRData:
            // skip becase it was handled at other place
            break;
        }
    }
}
