//
//  ZPQRActions.swift
//  ZaloPay
//
//  Created by Huu Hoa Nguyen on 6/20/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation

enum QRActionType {
    case ShowWebApp(url: String)
    case ProcessOrderData(data: ZPBill)
    case ProcessTransferMoney(data: UserTransferData)
    case ProcessQRData(data: [AnyHashable : Any])

    case AlertNewAppVersion
    case AlertError(error: NSError)
    case AlertErrorCurrentUser
    case AlertQRError
    case AlertNetworkError
}
