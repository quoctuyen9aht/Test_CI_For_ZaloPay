//
//  QRCodeType1Handler.swift
//  ZaloPay
//
//  Created by ThuChau on 3/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import CocoaLumberjackSwift
import RxSwift
import RxCocoa
import ZaloPayCommonSwift
import ZaloPayProfile

class ZPQRCodeType1Handler: ZPQRCodeHandler {
    weak var delegate: ZPQRCodeAction?
    
    convenience init(delegate: ZPQRCodeAction) {
        self.init()
        self.delegate = delegate
    }
    
    func canHandle(data: [AnyHashable : Any]) -> Bool {
        let type = data.int(forKey: "type")
        guard type == QRTransferType.type1.rawValue else {
            return false
        }
        
        let uid = data.uInt64(forKey: "uid")
        let paymentUId = NetworkManager.sharedInstance().paymentUserId ?? ""
        if paymentUId.toUInt64() == uid {
            return true
        }
        return checkSum(data: data)
    }
    
    func handle(data: [AnyHashable : Any]) {
        let uid = data.int64(forKey: "uid")
        let message = data.string(forKey: "message")
        let amount = data.int64(forKey: "amount")
        let paymentUId = NetworkManager.sharedInstance().paymentUserId ?? ""

        if paymentUId.toUInt64() == uid {
            self.delegate?.onAction(action: .AlertErrorCurrentUser)
            return
        }
        
        loadZaloUserProfile(userId: String(uid), amount: amount)
        getUserInfo(zaloPayId: uid,
                    amount: amount,
                    message: NSString(fromBase64String: message) as String,
                    source: ActivateSource.fromQRCodeType1,
                    mode: TransferMode.toZaloPayUser)
    }
    
    func getUserInfo(zaloPayId: Int64,
                     amount: Int64,
                     message: String,
                     source: ActivateSource,
                     mode: TransferMode) {
        NetworkManager.sharedInstance().getUserInfo(byZaloPayId: zaloPayId)
            .deliverOnMainThread().subscribeNext({ [weak self] userDict in
                guard let strongSelf = self else {
                    return
                }
                guard let userDict = userDict else {
                    strongSelf.delegate?.onAction(action: .AlertQRError)
                    return
                }
                
                let transferUser = UserTransferData()
                transferUser.paymentUserId = String(zaloPayId)
                transferUser.transferMoney = String(amount).formatMoneyValue()
                transferUser.message = message
                transferUser.source = source
                transferUser.mode = mode
                transferUser.displayName = userDict.string(forKey: "displayname")
                transferUser.avatar = userDict.string(forKey: "avatar")
                let phone = userDict.string(forKey: "phonenumber")
                transferUser.phone = phone?.normalizePhoneNumber().formatPhoneNumber()
                transferUser.accountName = userDict.string(forKey: "zalopayname")
                
                strongSelf.delegate?.onAction(action: .ProcessTransferMoney(data: transferUser))
                
                }, error:({ [weak self] (error) in
                    let err = (error as NSError?) ?? NSError()
                    self?.delegate?.onAction(action: .AlertError(error: err))
                }))
    }
    
    private func loadZaloUserProfile(userId: String, amount: Int64) {
        ZPProfileManager.shareInstance.loadZaloUserProfile().subscribeNext { user in
            guard let user = user as? ZaloUserSwift else {
                return
            }
            let embededData = TransferAppInfo.embededData(withDisplayName: user.displayName,
                                                          avatar: user.avatar ,
                                                          moneyTransferProgress: MoneyTransferProgress.scan,
                                                          amount: amount,
                                                          transId: nil)
            NetworkManager.sharedInstance().sendNotification(withReceiverId: userId, embededData: embededData)
                .subscribeCompleted ({
                    DDLogInfo("Transfer: Send Noti;fication Scan QR");
                })
        }
    }
    
    func checkSum(data: [AnyHashable : Any]) -> Bool {
        let checksum = data.string(forKey: "checksum")
        if checksum == createCheckSum(data: data) {
            return true
        }
        return false
    }
    
    private func createCheckSum(data: [AnyHashable : Any]) -> String {
        let message = data.string(forKey: "message")
        let amount = data.uInt64(forKey: "amount")
        let type = data.int(forKey: "type")
        let uid = data.uInt64(forKey: "uid")
        
        var array = [String]()
        array.append(String(type))
        array.append(String(uid))
        if amount > 0 {
            array.append(String(amount))
        }
        array.append(message)
        
        let sha256: String? = (array.filter({$0.count > 0}).joined(separator: "|") as NSString).sha256()
        if let sum = sha256 {
            return String(sum[..<sum.index(sum.startIndex, offsetBy: 8)])
        }
        return ""
    }
}
