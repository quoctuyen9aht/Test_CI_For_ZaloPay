//
//  ZPQRCodeHandler.swift
//  ZaloPay
//
//  Created by ThuChau on 3/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation

protocol ZPQRCodeAction: class {
    func onAction(action: QRActionType)
}

protocol ZPQRCodeHandler {
    func canHandle(data: [AnyHashable : Any]) -> Bool
    func handle(data: [AnyHashable : Any])
}
