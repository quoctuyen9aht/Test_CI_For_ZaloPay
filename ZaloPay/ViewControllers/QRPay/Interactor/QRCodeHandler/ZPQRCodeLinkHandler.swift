//
//  ZPQRCodeLinkHandler.swift
//  ZaloPay
//
//  Created by ThuChau on 3/28/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayAnalyticsSwift

class ZPQRCodeLinkHandler {
    weak var delegate: ZPQRCodeAction?
    private lazy var userAgent = appVersion()
    
    convenience init(delegate: ZPQRCodeAction) {
        self.init()
        self.delegate = delegate
    }
    
    func canHandle(data: String) -> Bool {
        return data.hasPrefix("http")
    }
    
    func requestUrlString(_ urlString:String) {
        NetworkManager.sharedInstance().request(withUrlString: urlString, userAgent: self.userAgent)
            .deliverOnMainThread().subscribeNext({ [weak self] json in
                guard let strongSelf = self else {
                    return
                }
                guard let json = json as? [AnyHashable : Any] else {
                    strongSelf.delegate?.onAction(action: .AlertQRError)
                    return
                }
                
                strongSelf.delegate?.onAction(action: .ProcessQRData(data: json))

                }, error:{ [weak self] error in
                    guard let strongSelf = self else {
                        return
                    }

                    if error?.errorCode() == NSURLErrorUnsupportedURL {
                        strongSelf.delegate?.onAction(action: .AlertQRError)
                        return
                    }
                    
                    if let e = error, (e as NSError).isNetworkConnectionError() {
                        strongSelf.delegate?.onAction(action: .AlertNetworkError)
                        return
                    }
                    ZPTrackingHelper.shared().trackScreen(withName:ZPTrackerScreen.Screen_iOS_Pay_ScanQR_WebView)
                    strongSelf.delegate?.onAction(action: .ShowWebApp(url: urlString))
            })
    }
    
    private func appVersion() -> String {
        guard var appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String else {
            return ""
        }
        if appVersion.hasSuffix(".0") {
            appVersion = String(appVersion.prefix(appVersion.count - 2))
        }
        return String(format: "ZaloPayClient/%@", appVersion)
    }
}
