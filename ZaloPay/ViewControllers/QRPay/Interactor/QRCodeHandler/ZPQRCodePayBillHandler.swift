//
//  QRCodePayBillHandler.swift
//  ZaloPay
//
//  Created by ThuChau on 3/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import CocoaLumberjackSwift
import ZaloPayCommonSwift

class ZPQRCodePayBillHandler: ZPQRCodeHandler {
    weak var delegate: ZPQRCodeAction?
    
    convenience init(delegate: ZPQRCodeAction) {
        self.init()
        self.delegate = delegate
    }
    
    func canHandle(data: [AnyHashable : Any]) -> Bool {
        let appId = data.int(forKey: "appid")
        let token = data.string(forKey: "zptranstoken")
        let apptransid = data.string(forKey: "apptransid")
        return appId > 0 && (token.count > 0 || apptransid.count > 0)
    }
    
    func handle(data: [AnyHashable : Any]) {
        let appId = data.int(forKey: "appid")
        let token = data.string(forKey: "zptranstoken")
        if appId > 0 && token.count > 0 {
            getOrder(WithAppId: appId, andTransToken: token)
            return
        }
        
        let apptransid = data.string(forKey: "apptransid")
        if appId > 0 && apptransid.count > 0 {
            handleOrder(jsonData: data, withAppId: appId)
            return
        }
        self.delegate?.onAction(action: .AlertQRError)
    }
    
    func getOrder(WithAppId appId: Int, andTransToken token: String) {
        NetworkManager.sharedInstance().getOrderFromAppId(appId, transToken:token)
            .deliverOnMainThread().subscribeNext({ [weak self] dic in
                guard let strongSelf = self else {
                    return
                }
                guard let dic = dic as? [String : Any] else {
                    strongSelf.delegate?.onAction(action: .AlertQRError)
                    return
                }
                
                DDLogInfo("get order : \(dic)");
                strongSelf.handleOrder(jsonData: dic, withAppId: appId)
                
                }, error:{ [weak self] error in
                    let err = (error as NSError?) ?? NSError()
                    self?.delegate?.onAction(action: .AlertError(error: err))
            })
    }
    
    func handleOrder(jsonData dic: [AnyHashable : Any], withAppId appId: Int) {
        guard let dic = dic as? [String : Any] else {
            self.delegate?.onAction(action: .AlertQRError)
            return
        }
        let bill = ZPBill(data: dic,
                          transType: ZPTransType.billPay,
                          orderSource: Int32(OrderSource_QR.rawValue),
                          appId: appId)
        if (bill.amount <= 0) {
            self.delegate?.onAction(action: .AlertQRError)
            return
        }
        self.delegate?.onAction(action: .ProcessOrderData(data: bill))
    }

}
