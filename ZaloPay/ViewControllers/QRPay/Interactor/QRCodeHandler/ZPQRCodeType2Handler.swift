//
//  QRCodeType2Handler.swift
//  ZaloPay
//
//  Created by ThuChau on 3/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation

class ZPQRCodeType2Handler: ZPQRCodeHandler {
    weak var delegate: ZPQRCodeAction?
    
    convenience init(delegate: ZPQRCodeAction) {
        self.init()
        self.delegate = delegate
    }
    
    func canHandle(data: [AnyHashable : Any]) -> Bool {
        let app = data.string(forKey: "app").lowercased()
        let type = data.int(forKey: "t")
        if app == "zp" && type == QRTransferType.type2.rawValue {
            return true
        }
        return false
    }
    
    func handle(data: [AnyHashable : Any]) {
        
        let zpid = data.string(forKey: "zpid")
        let amount = data.int64(forKey: "a")
        let message = data.string(forKey: "m")
        if amount <= 0 || zpid.count == 0 {
            self.delegate?.onAction(action: .AlertQRError)
            return
        }
        
        getUserInfo(acountName: zpid,
                    amount: Int64(amount),
                    message: message,
                    source: ActivateSource.fromQRCodeType2,
                    mode: TransferMode.toZaloPayID)
    }
    
    func getUserInfo(acountName: String,
                     amount: Int64,
                     message: String,
                     source: ActivateSource,
                     mode: TransferMode) {
        NetworkManager.sharedInstance().getUserInfo(withAcountName:acountName)
            .deliverOnMainThread().subscribeNext({ [weak self] userDict in
                guard let strongSelf = self else {
                    return
                }
                guard let userDict = userDict else {
                    strongSelf.delegate?.onAction(action: .AlertQRError)
                    return
                }
                
                let userId = userDict.string(forKey:"userid")
                if NetworkManager.sharedInstance().paymentUserId == userId {
                    strongSelf.delegate?.onAction(action: .AlertErrorCurrentUser)
                    return
                }
                
                let transferUser = UserTransferData()
                transferUser.paymentUserId = userId
                transferUser.transferMoney = String(amount).formatMoneyValue()
                transferUser.message = message
                transferUser.source = source
                transferUser.mode = mode
                transferUser.displayName = userDict.string(forKey: "displayname")
                transferUser.avatar = userDict.string(forKey: "avatar")
                let phone = userDict.string(forKey: "phonenumber")
                transferUser.phone = phone?.normalizePhoneNumber().formatPhoneNumber()
                transferUser.accountName = acountName
                
                strongSelf.delegate?.onAction(action: .ProcessTransferMoney(data: transferUser))
                
                }, error:{ [weak self] error in
                    let err = (error as NSError?) ?? NSError()
                    self?.delegate?.onAction(action: .AlertError(error: err))
            })
    }
}
