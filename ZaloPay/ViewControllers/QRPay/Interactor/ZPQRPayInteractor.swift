//
//  ZPQRPayScannerInteractor.swift
//  ZaloPay
//
//  Created by ThuChau on 3/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import CocoaLumberjackSwift
import RxCocoa
import ZaloPayCommonSwift
import ZaloPayAnalyticsSwift

class ZPQRPayInteractor: NSObject, ZPQRPayInteractorProtocol {
    private lazy var jsonHandlers:[ZPQRCodeHandler] = [ZPQRCodePayBillHandler(delegate: self),
                                                       ZPQRCodeType1Handler(delegate: self),
                                                       ZPQRCodeType2Handler(delegate: self),
                                                       ZPQRCodeType3Handler(delegate: self)]
    private lazy var linkHandler = ZPQRCodeLinkHandler(delegate: self)
    weak var presenter: ZPQRPayPresenterProtocol?
        
    func handleQRCodeData(data: String) {
        //        ZPAppFactory.sharedInstance().trackEvent(ZPAnalyticEventAction.scanqr_result)
        
        if let json = data.jsonValue() as? [AnyHashable : Any] {
            DDLogInfo("json : \(json)");
            handleJSONFromQRCode(data: json)
            return
        }
        
        if linkHandler.canHandle(data: data) {
            linkHandler.requestUrlString(data)
            return
        }
        self.presenter?.onAction(action: .AlertQRError)
    }

    private func handleJSONFromQRCode(data json: [AnyHashable : Any]) {
        if let handler = self.jsonHandlers.first(where: {$0.canHandle(data: json)}) {
            handler.handle(data: json)
            return
        }
        
        let app = json.string(forKey: "app").lowercased()
        if app == "zp" {
            // ZaloPay QR code spec, but current version does not know
            // alert user to update to latest version
            self.presenter?.onAction(action: .AlertNewAppVersion)
            return
        }
        self.presenter?.onAction(action: .AlertQRError)
    }
}

extension ZPQRPayInteractor: ZPQRCodeAction {
    
    func onAction(action: QRActionType) {
        switch action {
        case .ProcessQRData(let data):
            self.handleJSONFromQRCode(data: data)
        default:
            self.presenter?.onAction(action: action)
        }
    }
}
