//
//  ZPQRCodeBottomView.swift
//  ZaloPay
//
//  Created by vuongvv on 5/9/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

@objc public protocol ZPQRCodeBottomViewDelegate : NSObjectProtocol {
    func didTouchScanQR()
    func didTouchQuickPay()
}
@objcMembers
public class ZPQRCodeBottomView : UIView {
    public weak var delegate: ZPQRCodeBottomViewDelegate?
    
    private lazy var scanButton = ZPQRCodeTitleButton.init()
    private lazy var quickpayButton = ZPQRCodeTitleButton.init()
    
    fileprivate let disposeBag = DisposeBag()
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
    }
    
    private func setupView() {
        self.addSubview(self.scanButton)
        self.addSubview(self.quickpayButton)
        
        self.scanButton.snp.makeConstraints ({ (make) in
            make.width.equalTo(self.snp.width).dividedBy(2)
            make.left.equalTo(0)
            make.top.equalTo(0)
            make.bottom.equalTo(0)
        })
        self.quickpayButton.snp.makeConstraints ({ (make) in
            make.width.equalTo(self.snp.width).dividedBy(2)
            make.left.equalTo(self.scanButton.snp.right)
            make.top.equalTo(0)
            make.bottom.equalTo(0)
        })
        
        self.backgroundColor = UIColor(red: 36 / 255, green: 39 / 255, blue: 43 / 255, alpha: 0.6)
        
        self.scanButton.setTitle(R.string_QRPay_ScanToPay())
        self.quickpayButton.setTitle(R.string_QRPay_CodePay())
        
        self.scanButton.setIcon(name: "header_qrcode")
        self.quickpayButton.setIcon(name: "header_payqrcode")
        
        self.scanButton.setSelected(true)
        
        let scanQrTapGesture = UITapGestureRecognizer()
        self.scanButton.addGestureRecognizer(scanQrTapGesture)
        
        let balanceTapGesture = UITapGestureRecognizer()
        self.quickpayButton.addGestureRecognizer(balanceTapGesture)
        
        _ = scanQrTapGesture.rx.event.bind { [weak self](sender) in
            if (sender.state != .ended) {
                return
            }
            self?.delegate?.didTouchScanQR()
        }
        _ = balanceTapGesture.rx.event.bind { [weak self](sender) in
            if (sender.state != .ended) {
                return
            }
            self?.delegate?.didTouchQuickPay()
        }
        
    }
    public func setSelectedButton(index: Int) {
        self.scanButton.setSelected(index == 0)
        self.quickpayButton.setSelected(index == 1)
    }
}
fileprivate enum ZPQRCodeButtonColor {
    case normal
    case selected
    
    var color : UIColor {
        get {
            switch self {
                case .normal:
                    return UIColor(hexValue:0xb1b2b4)
                case .selected:
                    return UIColor(hexValue:0x1791e2)
            }
        }
    }
}
    
class ZPQRCodeTitleButton : UIView {
    private lazy var imageView = ZPIconFontImageView.init(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
    private lazy var label = UILabel.init()
    
    init() {
        super.init(frame: CGRect.zero)
        
        addAllSubView()
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        addAllSubView()
        setupLayout()
    }
    
    func setIcon(name: String!) {
        self.imageView.setIconFont(name)
    }
    
    func setTitle(_ text: String?) {
        self.label.text = text
    }
    
    private func addAllSubView() {
        self.imageView.contentMode = .center
        self.addSubview(self.imageView)
        
        let fontSize: CGFloat = UIView.isScreen2x() ? 14.5 : 15
        self.label.font = UIFont.sfuiTextRegular(withSize: fontSize)
        self.label.textAlignment = .center
        self.addSubview(self.label)
        
        self.setSelected(false)
    }
    
    private func setupLayout() {
        self.imageView.snp.makeConstraints ({ (make) in
            make.size.equalTo(self.imageView.frame.size)
            make.top.equalTo(14)
            make.centerX.equalToSuperview()
        })
        
        // Force layout first to calculate bottom for imageview
        self.layoutSubviews()
        
        self.label.snp.makeConstraints({ (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(16)
            make.top.equalTo(self.imageView.snp.bottom).offset(6)
        })
    }
    func setSelected(_ isSelected:Bool) {
        let color = isSelected ? ZPQRCodeButtonColor.selected.color :  ZPQRCodeButtonColor.normal.color
        self.imageView.setIconColor(color)
        self.label.textColor = color
    }
}
