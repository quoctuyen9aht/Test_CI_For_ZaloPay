//
//  ZPQRCodeImageReader.swift
//  ZaloPay
//
//  Created by Dung Vu on 4/11/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
@objc protocol ZPQRCodeImageReaderDelegate: NSObjectProtocol {
    @objc optional func imageReaderDidCancel(_ reader: ZPQRCodeImageReader?)
    @objc optional func imageReader(_ reader: ZPQRCodeImageReader?, selectImageWithResult json: String?)
}


class ZPQRCodeImageReader: NSObject {
    @objc weak var controller: UIViewController?
    @objc weak var delegate: ZPQRCodeImageReaderDelegate?
    
    fileprivate lazy var picker: ZaloPayPhotoPicker = {
       return ZaloPayPhotoPicker()
    }()
    
    fileprivate lazy var detector:CIDetector? = {
        return CIDetector(ofType: CIDetectorTypeQRCode, context: nil, options: [CIDetectorAccuracy:CIDetectorAccuracyHigh])
    }()
    
    @objc func startSelectImage() {
        guard let c = self.controller else {
            self.delegate?.imageReaderDidCancel?(nil)
            return
        }
        self.picker.selectPhotoInLibrary(with: c) { [unowned self](img) in
            guard let img = img else {
                self.delegate?.imageReaderDidCancel?(nil)
                return
            }
            self.handleImage(img)
        }
    }
    
    fileprivate func handleImage(_ image: UIImage) {
        guard let ciImage = CIImage(image: image) else {
            self.delegate?.imageReader?(nil, selectImageWithResult: nil)
            return
        }
        let features = self.detector?.features(in: ciImage)
        let result = features?.compactMap({ $0 as? CIQRCodeFeature }).reduce("", { $0 + ($1.messageString ?? "")})
        self.delegate?.imageReader?(nil, selectImageWithResult: result)
        
    }
    
}
