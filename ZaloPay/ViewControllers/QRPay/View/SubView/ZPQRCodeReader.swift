//
//  ZPQRCodeReader.swift
//  ZaloPay
//
//  Created by Dung Vu on 4/11/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Foundation
import ZaloPayConfig

enum ZPQRScanState: Int {
    case Scanning = 1
    case Processing
    case Disable
    case Pause
}

final class ZPQRCodeReader: QRCodeReader {
    // MARK: - Variable
    fileprivate lazy var colorDefault: UIColor = {
        return UIColor(white: 0 , alpha: 0.5)
    }()
    
    fileprivate lazy var viewSquare: UIImageView = {
        let imgView = UIImageView(image: UIImage(named: "qrCode_square"))
        imgView.backgroundColor = .clear
        return imgView
    }()
    
    fileprivate lazy var viewTop: UIView = {
        return self.createViewElement()
    }()
    fileprivate lazy var viewBottom: UIView = {
        return self.createViewElement()
    }()

    fileprivate lazy var viewLeft: UIView = {
        return self.createViewElement()
    }()

    fileprivate lazy var viewRight: UIView = {
        return self.createViewElement()
    }()

    fileprivate lazy var labelTitle: UILabel = {
        let nLabel = self.createLabel(with: R.string_QRPay_ScanToPay(), using: UIFont.sfuiTextMedium(withSize: 15))
        return nLabel
    }()
    
    fileprivate lazy var labelDescription: UILabel = {
        let nLabel = self.createLabel(with: R.string_QRPay_ScanToPayGuide(), using: UIFont.sfuiTextRegularItalic(withSize: 14))
        return nLabel
    }()
    
    fileprivate lazy var lineSquare: UIView = {
        return createLineSquare()
    }()

    fileprivate var qrScanFrame: CGRect {
        var frame = self.viewSquare.frame
        frame.origin.x = (UIScreen.main.bounds.width - frame.width) / 2
        frame.origin.y = (UIScreen.main.bounds.height - frame.height) / 2 - 20
        if enableQuickPay {
            frame.origin.y = frame.origin.y - (kHeightQRCodeBottom ) / 2 - kQRCodeBottomSafeInset * 2
        }
        return frame
    }
    
    var viewImage: UIView = UIView()
    fileprivate let disposeBag = DisposeBag()
    fileprivate let enableQuickPay = ZPApplicationConfig.getQuickPayConfig()?.getEnable().toBool() ?? false
    // MARK: - Init
    override init(metadataObjectTypes: [Any]) {
        super.init(metadataObjectTypes: metadataObjectTypes)
        let scanFrame = self.qrScanFrame
        self.viewSquare.frame = scanFrame
        self.configAllBackground()
        self.previewLayer.addSublayer(self.viewSquare.layer)
        NotificationCenter.default.rx.notification(Notification.Name.AVCaptureInputPortFormatDescriptionDidChange)
            .observeOn(MainScheduler.instance).subscribe { [weak self] (_) in
            guard let wSelf = self else { return }
            wSelf.metadataOutput.rectOfInterest = wSelf.previewLayer.metadataOutputRectConverted(fromLayerRect: UIScreen.main.bounds)
            wSelf.viewSquare.frame = scanFrame
//            wSelf.configAllBackground()
        }.disposed(by: disposeBag)
    }
    
    var scanState: ZPQRScanState = .Scanning {
        didSet  {
            stopAnimationLineSquare()
            let description: String?
            switch scanState {
            case .Disable:
                description = R.string_QRScan_NoConnectionMessage()
                break
            case .Processing:
                description = R.string_QRScan_ExchangeProcessingMessage()
                break
            case .Pause:
                description = R.string_QRPay_ScanToPayGuide()
                break 
            default:
                description = R.string_QRPay_ScanToPayGuide()
                startAnimationLineSquare()
            }
            self.labelDescription.text = description
        }
    }
    
    func startAnimationLineSquare() {
        lineSquare.frame = { () -> CGRect in
            var f = self.viewSquare.frame
            f.origin.y = f.origin.y + 1
            f.size.height = 2
            return f
        }()
        UIView.animate(withDuration: 2.2, delay: 0, options: [.repeat, .autoreverse], animations: {
            self.lineSquare.frame = { () -> CGRect in
                var f = self.lineSquare.frame
                f.origin.y = self.viewSquare.frame.origin.y + self.viewSquare.frame.size.height - 4
                return f
            }()
        }, completion: nil)
    }
    
    func stopAnimationLineSquare() {
        self.lineSquare.layer.removeAllAnimations()
    }
}

// MARK: - Config View
extension ZPQRCodeReader {
    fileprivate func configAllBackground() {
        configBackground()
        let (yViewImage,yLblDe) = getPaddingYViewImageAndLblDes()
        if enableQuickPay {
            configImageView(yViewImage)
        }
        configDescription(yLblDe)
    }
    
    fileprivate func createViewElement() -> UIView {
        let nView = UIView()
        nView.backgroundColor = self.colorDefault
        self.previewLayer.addSublayer(nView.layer)
        return nView
    }
    
    fileprivate func createLabel(with text: String?,using font: UIFont) -> UILabel {
        let nLabel = UILabel()
        nLabel.text = text
        nLabel.textColor = .white
        nLabel.font = font
        nLabel.textAlignment = .center
        self.previewLayer.addSublayer(nLabel.layer)
        return nLabel
    }
    
    func createLineSquare() -> UIView {
        let line = UIView()
        line.backgroundColor = UIColor.zaloBase()
        self.previewLayer.addSublayer(line.layer)
        return line
    }
    
    private func configBackground() {
        let border: CGFloat = 2
        let frame = self.viewSquare.frame
        
        self.viewTop.frame = CGRect(origin: .zero, size: CGSize(width: UIScreen.main.bounds.width, height: frame.origin.y + border))
        self.viewBottom.frame = CGRect(x: 0, y: frame.origin.y + frame.size.height - border, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - border)
        self.viewLeft.frame = CGRect(x: 0, y: frame.origin.y + border, width: frame.origin.x + border, height: frame.size.height - 2 * border)
        self.viewRight.frame = { () -> CGRect in
            var f = self.viewLeft.frame
            f.origin.x = frame.origin.x + frame.size.width - border
            f.size.width = UIScreen.main.bounds.width - f.origin.x
            return f
        }()
    }
    
    private func configImageView(_ yViewImage: CGFloat) {
        let frame = self.viewSquare.frame
        viewImage = UIView()
        viewImage.frame = { () -> CGRect in
            var f = self.viewTop.frame
            f.size.height = 38
            f.size.width = 118
            f.origin.y = frame.origin.y - yViewImage
            f.origin.x = (UIScreen.main.bounds.width - f.size.width) / 2
            return f
        }()
        viewImage.backgroundColor = UIColor(red: 36 / 255, green: 39 / 255, blue: 43 / 255, alpha: 0.6)
        viewImage.layer.masksToBounds = true
        viewImage.layer.cornerRadius = viewImage.frame.size.height / 2
        

        if enableQuickPay.not {
            self.labelTitle.frame = { () -> CGRect in
                var f = self.viewTop.frame
                f.size.height = 30
                f.origin.y = frame.origin.y - 70
                return f
            }()
            self.labelDescription.frame = { () -> CGRect in
                var f = self.labelTitle.frame
                f.origin.y = f.origin.y + 22
                return f
            }()
            return
        }
            
        let (yViewImage,_) = getPaddingYViewImageAndLblDes()
        //View image
        viewImage = UIView()
        viewImage.frame = { () -> CGRect in
            var f = self.viewTop.frame
            f.size.height = 38
            f.size.width = 118
            f.origin.y = frame.origin.y - yViewImage
            f.origin.x = (UIScreen.main.bounds.width - f.size.width) / 2
            return f
        }()
        viewImage.backgroundColor = UIColor(red: 36 / 255, green: 39 / 255, blue: 43 / 255, alpha: 0.6)
        viewImage.layer.masksToBounds = true
        viewImage.layer.cornerRadius = viewImage.frame.size.height / 2
        
        let sizeIcon:CGFloat = 20
        let iconSelectImage =  ZPIconFontImageView()
        iconSelectImage.frame = { () -> CGRect in
            var f = self.viewTop.frame
            f.size.height = sizeIcon
            f.size.width = sizeIcon
            f.origin.y = (viewImage.frame.size.height - sizeIcon) / 2
            f.origin.x = 18
            return f
        }()
        iconSelectImage.contentMode = .center
        iconSelectImage.setIconColor(UIColor.init(hexValue: 0xd6f0f6))
        iconSelectImage.setIconFont("img")
        
        let nLabelSelectImage = UILabel()
        nLabelSelectImage.text = "Hình ảnh"
        let fontSize: CGFloat = UIView.isScreen2x() ? 13 : 13.5
        nLabelSelectImage.font = UIFont.sfuiTextRegular(withSize: fontSize)
        nLabelSelectImage.textColor = UIColor.init(hexValue: 0xd6f0f6)
        nLabelSelectImage.textAlignment = .center
        let heightLblSelectImage:CGFloat = 16
        nLabelSelectImage.frame = { () -> CGRect in
            var f = self.viewTop.frame
            f.size.height = heightLblSelectImage
            f.size.width = 60
            f.origin.y = (viewImage.frame.size.height - heightLblSelectImage) / 2
            f.origin.x = iconSelectImage.frame.origin.x + iconSelectImage.frame.size.width + 8
            return f
        }()
        
        viewImage.addSubview(iconSelectImage)
        viewImage.addSubview(nLabelSelectImage)
        viewImage.layer.addSublayer(iconSelectImage.layer)
        viewImage.layer.addSublayer(nLabelSelectImage.layer)
        self.previewLayer.addSublayer(viewImage.layer)
    }
    
    private func configDescription(_ yLabelDescription: CGFloat) {
        let frame = self.viewSquare.frame 
        let att: [NSAttributedStringKey: Any] = [
            .font: self.labelDescription.font
        ]
        let sT = ((self.labelDescription.text ?? "") as NSString).size(withAttributes: att)
        self.labelDescription.frame = { () -> CGRect in
            var f = self.viewBottom.frame
            f.size.height = 36
            f.size.width = sT.width + 20
            f.origin.y = frame.origin.y + frame.size.height + yLabelDescription
            f.origin.x = (UIScreen.main.bounds.width - f.size.width) / 2
            return f
            }()
    }
    
    func getPaddingYViewImageAndLblDes() -> (CGFloat,CGFloat) {
        
        //IP6
        var yViewImage:CGFloat = 80
        var yLblDes:CGFloat = 32
        if UIView.isIPhone4() {
            yViewImage = 45
            yLblDes = -5
        }
        else if UIView.isIPhone5()  {
            yViewImage = 65
            yLblDes = 15
        }
        else if (UIView.isIPhoneX()) {
            yViewImage += 20
            yLblDes += 10
        }
        else if (UIView.isHeightIP6PlusOrLarger()) {
            yViewImage += 20
        }
        
        return (yViewImage,yLblDes)
    }
}
