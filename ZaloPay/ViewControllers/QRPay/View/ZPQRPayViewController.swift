//
//  ZPQRPayViewController.swift
//  ZaloPay
//
//  Created by ThuChau on 3/24/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit
import RxSwift
import RxCocoa
import CoreGraphics
import ZaloPayCommonSwift
import ZaloPayAnalyticsSwift
import CocoaLumberjackSwift
import ZaloPayConfig

public let kHeightQRCodeBottom:CGFloat = 90
// TODO: remove or move to a common place
public let kQRCodeBottomSafeInset:CGFloat = UIView.safeAreaInsets().bottom > 0 ? 20 : 0
let kLoadingTimeDelay = 1.5

extension QRCodeReaderViewController {
    override open func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}

class ZPQRCodeReaderViewControlller: QRCodeReaderViewController {
    func setupAutoLayoutConstraints() {
        let cameraView = self.value(forKey: "cameraView")
        if let cameraView = cameraView as? QRCodeReaderView {
            cameraView.snp.makeConstraints { make in
                make.top.equalTo(0)
                make.left.equalTo(0)
                make.right.equalTo(0)
                make.bottom.equalTo(0)
            }
            let overlay = cameraView.value(forKey: "overlay")
            if let overlay = overlay as? CAShapeLayer {
                overlay.removeFromSuperlayer()
            }
        }
    }
}

class ZPQRPayViewController: BillHandleBaseViewController {
    var readerViewController: ZPQRCodeReaderViewControlller?
    var imageReader: ZPQRCodeImageReader?
    var presenter: ZPQRPayPresenterProtocol?
    var codeReader:ZPQRCodeReader?
    var isViewAppear = false

    var isLoading = false
    private let disposeBag = DisposeBag()
    fileprivate let enableQuickPay = ZPApplicationConfig.getQuickPayConfig()?.getEnable().toBool() ?? false

    weak var pageVCParent:ZPQRPayPageViewController?
    
    fileprivate lazy var viewCover: UIView = {
        let cover = UIView()
        cover.backgroundColor = UIColor(white: 0 , alpha: 0.5)
        cover.frame = UIScreen.main.bounds
        enableQuickPay ? pageVCParent?.view.addSubview(cover) : self.view.addSubview(cover)
        return cover
    }()
    
    fileprivate var isDisconnect: Bool = false {
        didSet{
            isDisconnect == true ? pauseHandleInputData() : startHandleInputData()
            self.viewCover.isHidden = !isDisconnect
            self.codeReader?.scanState = isDisconnect ? .Disable : .Scanning
            ZPAppFactory.sharedInstance().hideAllHUDs(for: self.view)
            isLoading = false
        }
    }
    
    fileprivate func addObserverHandleAppBecomeActive() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateAnimationScanSquare),
                                               name: .UIApplicationDidBecomeActive,
                                               object: nil)
    }
    
    fileprivate func removeObserverHandleAppBecomeActive() {
        NotificationCenter.default.removeObserver(self,
                                                  name: .UIApplicationDidBecomeActive,
                                                  object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = R.string_QRPay_Title()
        self.view.backgroundColor = enableQuickPay ? UIColor(red: 36 / 255, green: 39 / 255, blue: 43 / 255, alpha: 0.7) : UIColor.black

        if enableQuickPay.not {
            self.setupNavigationBar()
        }
        DispatchQueue.main.async {
            self.initReaderView()
            self.presenter?.observableNetworkStatus()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        ZPTrackingHelper.shared().trackScreen(with: self)
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.pay_launched)
        
        if enableQuickPay , let pageVCParentGet = pageVCParent {
            pageVCParentGet.navigationController?.setNavigationBarHidden(false, animated: false)
        }
        else {
            self.navigationController?.setNavigationBarHidden(false, animated: false)
        }
        
        self.presenter?.prepare()
        
        if isViewAppear.not {
            DispatchQueue.main.async {
                self.startScan()
            }
            isViewAppear = true
        }

        updateAnimationScanSquare()
        addObserverHandleAppBecomeActive()
        addTargetForHud()
    }
    
    @objc func updateAnimationScanSquare() {
        if let _ = self.readerViewController?.delegate,
            let reader = self.codeReader,
            reader.scanState == .Scanning {
            changeScanState(state: reader.scanState)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isViewAppear {
            DispatchQueue.main.async {
                self.startScan()
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        removeObserverHandleAppBecomeActive()
        removeTargetForHud()
        DispatchQueue.main.async {
            self.stopScan()
        }
    }
    
    override func backEventId() -> Int {
        return ZPAnalyticEventAction.pay_touch_back.rawValue
    }
    
    override func backButtonClicked(_ sender:Any) {
        super.backButtonClicked(self)
        stopShowLoadingView()
    }
    
    override func screenName() -> String {
        return ZPTrackerScreen.Screen_iOS_Pay
    }
    
    override func handleSetAppIdError(_ appId: Int) {
        var message:String! = R.string_PayBillSetAppId_ErrorMessage()
        var type = DialogTypeWarning
        if NetworkState.sharedInstance().isReachable {
            message = R.string_NetworkError_NoConnectionMessage()
            type = DialogTypeNoInternet
        }
        self.alertQRError(withType: type,
                          title:R.string_Dialog_Warning(),
                          message: message,
                          cancelButtonTitle: R.string_ButtonLabel_Close())
    }
    
    override func payBillDidCancel() {
        if enableQuickPay , let pageVCParentGet = pageVCParent {
            pageVCParentGet.navigationController?.popToViewController(pageVCParentGet, animated: true)
        }
        else {
            self.navigationController?.popToViewController(self, animated: true)
        }
        if let _ = self.readerViewController?.delegate {
            return
        }
        self.startHandleInputData()
    }
    
    override func toHomePage() {
        stopShowLoadingView()
        if enableQuickPay , let pageVCParentGet = pageVCParent {
            pageVCParentGet.navigationController?.popToRootViewController(animated: true)
        }
        else {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func initReaderView() {
        if QRCodeReader.supportsMetadataObjectTypes([AVMetadataObject.ObjectType.qr]) {
            let reader = ZPQRCodeReader(metadataObjectTypes: [AVMetadataObject.ObjectType.qr])
            self.readerViewController = ZPQRCodeReaderViewControlller.reader(withCancelButtonTitle: "",
                                                                                    codeReader: reader,
                                                                                    startScanningAtLoad: true,
                                                                                    showSwitchCameraButton: false,
                                                                                    showTorchButton: false)
            self.readerViewController?.delegate = self
            self.readerViewController?.setupAutoLayoutConstraints()
            codeReader = reader
        }

        if let readerViewController = self.readerViewController {
            self.view.addSubview(readerViewController.view)
            readerViewController.cancelButton.isHidden = true
            readerViewController.view.backgroundColor = UIColor.clear
            readerViewController.startScanning()
            readerViewController.view.snp.makeConstraints { make in
                make.top.equalTo(0)
                make.left.equalTo(0)
                make.right.equalTo(0)
                make.bottom.equalTo(0)
                //make.bottom.equalTo(self.enableQuickPay ? -kHeightQRCodeBottom : 0)
            }
        }
        self.checkCameraStatus()
    }
    
    func setupNavigationBar() {
        let button:UIButton = UIButton.init(type: UIButtonType.custom)
        button.titleLabel?.textColor = UIColor.white
        button.titleLabel?.font = UIFont.iconFont(withSize: 20)
        button.setIconFont("paymoney_qrcode", for: UIControlState.normal)
        button.addTarget(self, action: #selector(choosePhotoFromLibrary), for: UIControlEvents.touchUpInside)
        button.frame = CGRect(x:0, y:-10, width:30, height:40)
        let barButton:UIBarButtonItem = UIBarButtonItem.init(customView: button)
        let fixedSpace:UIBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = -10
        self.navigationItem.rightBarButtonItems = [fixedSpace, barButton]
    }
    
    override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if self.isDisconnect {
            return
        }
        guard let touch = touches.first, let codeReaderGet = self.codeReader else {
            return
        }
        let point = touch.location(in: self.view)
        guard codeReaderGet.viewImage.frame.contains(point) else {
            return
        }
        self.choosePhotoFromLibrary()
    }
    
    // MARK: - Action
    @objc func choosePhotoFromLibrary() {
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.pay_scanqr_touch_photo)
        if self.imageReader == nil {
            self.imageReader = ZPQRCodeImageReader()
            self.imageReader?.controller = self
            self.imageReader?.delegate = self
        }
        self.pauseHandleInputData()
        self.imageReader?.startSelectImage()
    }
    
    func checkCameraStatus() {
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        if authStatus == AVAuthorizationStatus.denied {
            self.alertSettingCamera()
        }
    }
    
    func startScan() {
        if !isLoading && self.codeReader?.scanState == .Scanning {
            startHandleInputData()
        }
        if (self.readerViewController?.codeReader.running() == true) {
            return
        }
        self.readerViewController?.codeReader.startScanning()
    }
    
    func stopScan() {
        if (self.readerViewController?.codeReader.running() == true) {
            self.readerViewController?.stopScanning()
        }
    }
    
    func startHandleInputData() {
        stopShowLoadingView()
        if self.isDisconnect {
            return
        }
        self.readerViewController?.delegate = self
    }
    
    func pauseHandleInputData() {
        self.codeReader?.stopAnimationLineSquare()
        self.readerViewController?.delegate = nil
    }
    
    func vibrateDevice() {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
    }

    func changeScanState(state: ZPQRScanState) {
        if !self.isDisconnect {
            self.codeReader?.scanState = state
        }
    }
}

extension ZPQRPayViewController {
    fileprivate func addTargetForHud() {
        ZPAppFactory.sharedInstance().setHUDWithTarget(self, sel:#selector(handleSingleTap(_:_:)))
    }

    @objc func handleSingleTap(_ sender: UIControl,_  event: UIEvent) {
        guard let navigation = self.navigationController,
            let leftBarItems = self.navigationItem.leftBarButtonItems,
            let touch = event.allTouches else {
                return
        }

        for item in leftBarItems {
            guard let backButton = item.customView as? UIButton else {
                return
            }
            for touches in touch {
                if !backButton.frame.contains(touches.location(in: navigation.view)) {
                    continue
                }
                backButtonClicked(self)
            }
        }
    }

    fileprivate func removeTargetForHud() {
        ZPAppFactory.sharedInstance().removeHUD(withTarget: self, sel:#selector(handleSingleTap(_:_:)))
    }
}

extension ZPQRPayViewController: ZPQRCodeImageReaderDelegate {
    func imageReaderDidCancel(_ reader: ZPQRCodeImageReader?) {
        self.startHandleInputData()
    }
    
    func imageReader(_ reader: ZPQRCodeImageReader?, selectImageWithResult json: String?) {
        if let json = json {
            DispatchQueue.main.async {
                self.reader(nil, didScanResult: json)
            }
        }
    }
}

extension ZPQRPayViewController: QRCodeReaderDelegate {
    func reader(_ reader: QRCodeReaderViewController?, didScanResult result: String) {
        self.pauseHandleInputData()
        self.startShowLoadingView()
        self.vibrateDevice()
        self.presenter?.handleQRCodeData(data: result)
    }
}

extension ZPQRPayViewController: ZPQRPayViewControllerProtocol {
    
    private func pauseScan() {
        stopShowLoadingView()
        self.pauseHandleInputData()
        changeScanState(state: .Pause)
    }
    
    func updateScreen(networkStatus: Bool) {
        self.isDisconnect = !networkStatus
    }
    
    func startShowLoadingView() {
        DDLogInfo("start show loading")
        _ = Observable.just(!isLoading)
            .filter({ return $0 })
            .delay(RxTimeInterval(kLoadingTimeDelay), scheduler: MainScheduler.instance)
            .takeUntil(self.rx.methodInvoked(#selector(self.stopShowLoadingView)))
            .subscribe(onNext: { [weak self] (result) in
                guard let strongSelf = self else {
                    return
                }
                ZPAppFactory.sharedInstance().showHUDAdded(to: strongSelf.view)
                strongSelf.changeScanState(state: .Processing)
            }).disposed(by: disposeBag)
        isLoading = true
    }
    
    func startProcessingShowBillDetail() {
        DDLogInfo("start show processing")
        removeTargetForHud()
        _ = Observable.just(!isLoading)
            .filter({ return $0 })
            .delay(RxTimeInterval(kLoadingTimeDelay), scheduler: MainScheduler.instance)
            .takeUntil(self.rx.methodInvoked(#selector(self.stopShowLoadingView)))
            .subscribe(onNext: { [weak self] (result) in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.changeScanState(state: .Processing)
            }).disposed(by: disposeBag)
        isLoading = true
    }
    
    @objc func stopShowLoadingView() {
        DDLogInfo("stop loading")
        ZPAppFactory.sharedInstance().hideAllHUDs(for: self.view)
        isLoading = false
        changeScanState(state: .Scanning)
    }
    
    func alertSettingCamera() {
        ZPDialogView.showDialog(with: DialogTypeWarning,
                                message: R.string_QRPay_AskPermissionMessage(),
                                cancelButtonTitle: R.string_ButtonLabel_Close(),
                                otherButtonTitle: [R.string_QRPay_AskPermissionEnable()],
                                completeHandle: {buttonIndex, cancelButtonIndex in
                                    if buttonIndex != cancelButtonIndex {
                                        guard let url = URL.init(string: UIApplicationOpenSettingsURLString) else {
                                            return
                                        }
                                        UIApplication.shared.openURL(url)
                                    }
        })
    }
    
    private func alertQRError(withType type: DialogType,
                              title: String,
                              message: String,
                              cancelButtonTitle cancelTitle: String) {
        pauseScan()
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.pay_scanqr_result_fail)
        _ = ZPDialogView.showDialog(with: type,
                                    message: message,
                                    cancelButtonTitle: cancelTitle,
                                    otherButtonTitle: nil,
                                    completeHandle: { [weak self] buttonIndex, cancelButtonIndex in
                                        self?.startHandleInputData()
                                        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.pay_scanqr_result_fail_close)
        })
    }
    
    func alertError(error: NSError) {
        pauseScan()
        ZPDialogView.showDialogWithError(error, handle: { [weak self] in
            self?.startHandleInputData()
        })
    }
    
    func alertCurrentUser() {
        pauseScan()
        ZPDialogView.showDialog(with: DialogTypeNotification,
                                message: R.string_TransferToAccount_CurrentUserAccountMessage(),
                                cancelButtonTitle: R.string_ButtonLabel_Close(),
                                otherButtonTitle: nil,
                                completeHandle: { [weak self] buttonIndex, cancelButtonIndex in
                                    self?.startHandleInputData()
        })
    }
    
    func alertQRError() {
        self.alertQRError(withType: DialogTypeWarning,
                          title: R.string_QRPay_ErrorTitle(),
                          message: R.string_QRPay_ErrorMessage(),
                          cancelButtonTitle: R.string_QRPay_ScanOrther())
    }
    
    func alertNetworkErrorNoConnectionMessage() {
        self.alertQRError(withType: DialogTypeNoInternet,
                          title: R.string_QRPay_ErrorTitle(),
                          message: R.string_NetworkError_NoConnectionMessage(),
                          cancelButtonTitle: R.string_ButtonLabel_OK())
    }
    
    
    func alertUpdateNewVersion() {
        self.alertQRError(withType: DialogTypeInfo,
                          title: R.string_QRPay_ErrorTitle(),
                          message: R.string_QRPay_ErrorUnsupport(),
                          cancelButtonTitle: R.string_ButtonLabel_OK())
    }

}
