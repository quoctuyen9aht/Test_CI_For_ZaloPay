//
//  QRCodeReaderViewController+ZaloPay.h
//  ZaloPay
//
//  Created by Bon Bon on 5/18/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//


#import <QRCodeReaderViewController/QRCodeReaderViewController.h>
@interface QRCodeReaderViewController (ZaloPay)
@property (strong, nonatomic) UIButton *cancelButton;
@end
