//
//  ZPQRPayPageViewController.swift
//  ZaloPay
//
//  Created by vuongvv on 5/16/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import Foundation
import SnapKit
import RxSwift
import RxCocoa

fileprivate enum ZPPageType:Int {
    case indexQrCode = 0 ,indexQuickPay
}

class ZPQRPayPageViewController: BasePageViewController
{
    fileprivate lazy var pages: [UIViewController] = {
        let VCQR = ZPQRPayRouter.createQRPayViewController(pageVCParent: self)
        let VCQuickPay = ZPPaymentCodeSecurityMixQRVC(nibName: String(describing: ZPPaymentCodeSecurityMixQRVC.self), bundle: nil)
        VCQuickPay.delegate = self
        return [
            VCQR,
            VCQuickPay
        ]
    }()
    fileprivate var currentIndex = -1
    var bottomView = ZPQRCodeBottomView()
    private let disposeBag = DisposeBag()
    private let activity: ActivityIndicator = ActivityIndicator()
    private var canMovePage: Bool = true
    var myScrollView:UIScrollView?
    
    override init(transitionStyle style: UIPageViewControllerTransitionStyle, navigationOrientation: UIPageViewControllerNavigationOrientation, options: [String : Any]? = nil) {
        super.init(transitionStyle: UIPageViewControllerTransitionStyle.scroll, navigationOrientation: UIPageViewControllerNavigationOrientation.horizontal, options: options)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.title = R.string_QRPay_Title()
        self.dataSource = self
        self.delegate   = self
        self.setupEvent()
        self.movePageToIndex(pageIndex: .indexQrCode).subscribe(onNext: { [weak self](idx) in
            self?.currentIndex = idx.rawValue
        }).disposed(by: disposeBag)
        
        setupBottomView()
        
        myScrollView = self.view.subviews.first(where: { $0 is UIScrollView }) as? UIScrollView
        myScrollView?.delegate = self
    }
    
    private func setupEvent() {
        self.activity.asDriver().drive(onNext: { [weak self] in
            self?.canMovePage = $0.not
        }).disposed(by: disposeBag)
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        //setupSubLayout()

    }
    override func  viewWillLayoutSubviews(){
        super.viewWillLayoutSubviews()
        //setupSubLayout()
    }
    
    override func backButtonClicked(_ sender:Any) {
        super.backButtonClicked(self)
        guard let currentPage = pages[currentIndex] as? ZPQRPayViewController else {
            return
        }
        currentPage.stopShowLoadingView()
    }
    
    func setupSubLayout() {
        for subView in view.subviews {
            if subView.frame.size.height == view.frame.size.height {
                subView.frame = CGRect(x:subView.frame.origin.x, y:subView.frame.origin.y, width:subView.frame.size.width,height: self.view.frame.size.height - kHeightQRCodeBottom)
            }
        }
    }
    func setupBottomView() {
        bottomView = ZPQRCodeBottomView()
        self.view.addSubview(bottomView)
        bottomView.delegate = self
        bottomView.snp.makeConstraints { make in
            make.height.equalTo(kHeightQRCodeBottom)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.bottom.equalTo(-kQRCodeBottomSafeInset)
        }
        
        
        if kQRCodeBottomSafeInset > 0 {
            let bottomViewSafe = UIView()
            bottomViewSafe.backgroundColor = bottomView.backgroundColor
            self.view.addSubview(bottomViewSafe)
            bottomViewSafe.snp.makeConstraints { make in
                make.height.equalTo(kQRCodeBottomSafeInset)
                make.left.equalTo(0)
                make.right.equalTo(0)
                make.bottom.equalTo(0)
            }
        }
    }
}

extension ZPQRPayPageViewController: UIPageViewControllerDataSource
{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = pages.index(of: viewController) else {
            return nil
        }
        
        if viewControllerIndex == 0 {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1

        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        guard let viewControllerIndex = pages.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        
        if nextIndex == pages.count  {
            return nil
        }

        return pages[nextIndex]
    }

    private func movePageToIndex(pageIndex : ZPPageType, animated: Bool = true) -> Observable<ZPPageType> {
        guard currentIndex != pageIndex.rawValue, canMovePage else {
            return Observable.empty()
        }

        guard let viewController = self.pages[safe:pageIndex.rawValue] else {
            return Observable.empty()
        }
        
        let direction : UIPageViewControllerNavigationDirection = pageIndex.rawValue < currentIndex ? .reverse : .forward
        let viewControllers = [viewController]
        
        return Observable.create({ [weak self](s) -> Disposable in
            DispatchQueue.main.async {
                self?.setViewControllers(viewControllers, direction: direction, animated: animated, completion: { [weak self] finished in
                    self?.myScrollView?.bounces = true
                    defer {
                        s.onCompleted()
                    }
                    guard finished else {
                        self?.canMovePage = true
                        return
                    }
                    s.onNext(pageIndex)
                })
            }
            return Disposables.create()
        }).trackActivity(self.activity)
    }
}

extension ZPQRPayPageViewController: UIPageViewControllerDelegate {
    public func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if finished.not || completed.not {
            return
        }
        guard let firstVC = self.viewControllers?.first else {
            return
        }
       
        currentIndex = pages.index(of: firstVC) ?? 0
        bottomView.setSelectedButton(index: currentIndex)
    }
}
extension ZPQRPayPageViewController : ZPQRCodeBottomViewDelegate {
    func didTouchScanQR() {
        self.movePageToIndex(pageIndex: .indexQrCode).subscribe(onNext: { [weak self](idx) in
            self?.currentIndex = idx.rawValue
            self?.bottomView.setSelectedButton(index:idx.rawValue)
        }).disposed(by: disposeBag)
    }
    
    func didTouchQuickPay() {
        guard let QRPayVC = self.viewControllers?.first as? ZPQRPayViewController else {
            return
        }
        QRPayVC.stopScan()
        self.movePageToIndex(pageIndex: .indexQuickPay).subscribe(onNext: { [weak self](idx) in
            self?.currentIndex = idx.rawValue
            self?.bottomView.setSelectedButton(index:idx.rawValue)
        }).disposed(by: disposeBag)
    }
    
}
extension ZPQRPayPageViewController : ZPPaymentCodeSecurityMixQRVCDelegate {
    func OKClick() {
        let options = ZPCenterRouterOptions()
        options.QuickPayForceToQRCode = true
        ZPCenterRouter.launch(routerId: .paymentCode, from: self,animated : true, options:options)
    }
}
extension ZPQRPayPageViewController : UIScrollViewDelegate {
    
    func checkDisableScrollOnEdge(_ scrollView: UIScrollView) {
        let widthScreen = UIScreen.main.bounds.size.width
        let leftOrRight:Bool = currentIndex == 0 ? scrollView.contentOffset.x <= widthScreen : scrollView.contentOffset.x >= widthScreen
        scrollView.bounces = leftOrRight.not
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        checkDisableScrollOnEdge(scrollView)
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        checkDisableScrollOnEdge(scrollView)
    }
}
