//
//  ZPQRPayProtocols.swift
//  ZaloPay
//
//  Created by ThuChau on 3/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import UIKit

protocol ZPQRPayPresenterProtocol: class {
    var interactor: ZPQRPayInteractorProtocol? { get set }
    var router: ZPQRPayRouterProtocol? { get set }
    var view: ZPQRPayViewControllerProtocol? { get set }
    
    // View -> Presenter
    func prepare()
    func handleQRCodeData(data: String)
    func observableNetworkStatus()
    
    // Interactor -> Presenter
    func updateScreen(networkStatus: Bool)
    func showQuickPay()
    
    func onAction(action: QRActionType)
}

protocol ZPQRPayInteractorProtocol: class {
    var presenter: ZPQRPayPresenterProtocol? { get set }
    
    // Presenter -> Interactor
    func handleQRCodeData(data: String)
//    func observableNetworkStatus()
}

protocol ZPQRPayRouterProtocol: class {

    // Presenter -> Router
    func showTransferReceiver(transferData: UserTransferData)
    func willPushFrom(_ viewController: BillHandleBaseViewController)
    func showBillViewController(bill: ZPBill)
    func showWebApp(withUrl url: String)
    func showQuickPay()
}

protocol ZPQRPayViewControllerProtocol: class {
    var presenter: ZPQRPayPresenterProtocol? { get set }
    
    // Presenter -> View
    func startProcessingShowBillDetail()
    func updateScreen(networkStatus: Bool)
    func stopShowLoadingView()
    func alertCurrentUser()
    func alertQRError()
    func alertNetworkErrorNoConnectionMessage()
    func alertUpdateNewVersion()
    func alertError(error: NSError)
}
