//
//  BankIntegrationViewController.swift
//  ZaloPay
//
//  Created by Dung Vu on 10/9/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit
import CocoaLumberjackSwift
import MessageUI
import ZaloPayProfile

protocol ParseActionProtocol {
    associatedtype E
    static func parseAction(from json: JSON) -> E
}

extension ParseActionProtocol where E: RawRepresentable, E.RawValue == String {
    static func parseAction(from json: JSON) -> E {
        let v = json.value(forKey: "action", defaultValue: "")
        guard let action = E(rawValue: v) else {
            fatalError("No Implement For this key : \(v)")
        }
        return action
    }
}

enum ReactNextAction: String, ParseActionProtocol {
    typealias E = ReactNextAction
    case otp = "otp"
    case retryOtp = "retryotp"
    case refreshOtp = "refreshotp"
}

enum ReactProcessResult: String, ParseActionProtocol {
    typealias E = ReactProcessResult
    case success = "success"
    case error = "exiterror"
    case unknown = "back"
}

final class BankIntegrationViewController: UIViewController {
    fileprivate var transId: String?
    fileprivate var info: NSDictionary?
    var isProcess: Bool = true
    var identify: Int {
        get {
            return kBankIntegration
        }
        
        set{
            DDLogInfo("Don't set with value : \(newValue)")
        }
    }
    
    fileprivate let trackLoading: Variable<Bool> = Variable(false)
    fileprivate let disposeBag = DisposeBag()
    fileprivate var isAppear: Bool = true
    fileprivate var currentAction: ReactNativeAction?
    // Result
    fileprivate let eResult: PublishSubject<Any?> = PublishSubject()
    fileprivate weak var otpVC: BankIntegrationInputOTPViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
        setupReact()
        setupAction()
        setupEvent()
    }
    
    fileprivate func setupReact() {
        let user = ZPProfileManager.shareInstance.userLoginData
        let phone = user?.phoneNumber ?? ""
        
        let properties: JSON = ["data": info as? JSON ?? [:], "userphonenumber": phone]
        
        guard let reactVC = ZPDownloadingViewController(properties: properties) else {
            fatalError("Error Module")
        }
        reactVC.moduleName = "RegisterOnlineBanking"
        reactVC.willMove(toParentViewController: self)
        self.view.addSubview(reactVC.view)
        // Set Layout
        reactVC.view.snp.makeConstraints({
            $0.edges.equalTo(UIEdgeInsetsMake(0, 0, 0, 0))
        })
        reactVC.didMove(toParentViewController: self)
    }
    
    fileprivate func setupAction() {
        ZPBankIntegrationApi.register(handler: self)
    }
    
    fileprivate func setupEvent() {
        trackLoading.asDriver().filter({ [weak self] _ in self?.isAppear ?? false }).drive(onNext: {
            $0 ? ZPAppFactory.sharedInstance().showHUDAdded(to: nil) : ZPAppFactory.sharedInstance().hideAllHUDs(for: nil)
        }).disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let otpVC = segue.destination as? BankIntegrationInputOTPViewController else {
            return
        }
        let phoneSupport = sender as? String ?? ""
        otpVC.phoneSupport = phoneSupport
        self.otpVC = otpVC
        self.isAppear = false
        self.trackLoading.asObservable().takeUntil(otpVC.rx.deallocating).bind { [weak otpVC] in
            otpVC?.activityTracking.value = $0
            }.disposed(by: disposeBag)
        
        otpVC.e.subscribe(onCompleted: { [weak self, unowned otpVC] in
            self?.isAppear = true
            // Track value
            let result = otpVC.result
            let transId = otpVC.transId
            let error = otpVC.errorResponse
            
            self?.dismiss(animated: true, completion: {
                if result {
                    self?.eResult.onNext(transId)
                    self?.eResult.onCompleted()
                } else if let e = error {
                    self?.eResult.onError(e)
                } else {
                    let stepInfor: JSON = ["action": "resetcountotp"]
                    self?.currentAction?.blockSuccess?(stepInfor)
                }
            })
        }).disposed(by: disposeBag)
        
        Observable.merge([otpVC.textTracking, otpVC.eRefreshOTP])
            .takeUntil(otpVC.rx.deallocating)
            .bind { [weak self] in self?.currentAction?.blockSuccess?($0)}
            .disposed(by: disposeBag)
    }
    
    fileprivate func openSMS(from json: JSON) {
        let phone: String? = json.value(forKey: "phone", defaultValue: nil)
        let body: String? = json.value(forKey: "body", defaultValue: nil)
        guard MFMessageComposeViewController.canSendText(),
            let p = phone,
            let b = body else {
            return
        }
        
        let smsController = MFMessageComposeViewController()
        smsController.messageComposeDelegate = self
        smsController.recipients = ["\(p)"]
        smsController.body = b
        self.present(smsController, animated: true, completion: nil)
    }
}
// MARK: - SMS
extension BankIntegrationViewController: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
}

// MARK: - Register Action
extension BankIntegrationViewController: ZPReactNativeHandlerProtocol {
    func next(_ action: ReactNativeAction?, step: ReactActionType) {
        let json: JSON = action?.json as? JSON ?? [:]
        switch step {
        case .next:
            self.currentAction = action
            self.processNextAction(from: json)
        case .showLoading:
            self.trackLoading.value = true
        case .hideLoading:
            self.trackLoading.value = false
        case .process:
            self.processResult(from: json)
        case .showSMS:
            runOnMain {[unowned self] in
                self.openSMS(from: json)
            }
            
        }
    }
}

// MARK: - Helper
extension BankIntegrationViewController {
    fileprivate func exit(with transId: String?, moduleId: Int) {
        runOnMain { [unowned self] in
            ReactFactory.sharedInstance().closeModuleInstance(moduleId)
            self.eResult.onNext(transId)
            self.eResult.onCompleted()
        }
    }
    
    fileprivate func runOnMain(_ block: @escaping () -> () ) {
        OperationQueue.main.addOperation(block)
    }
}


// MARK: - Process Handler
extension BankIntegrationViewController {
    /// Using for handler next step in React
    ///
    /// - Parameter json: Dictionary that include information from React
    fileprivate func processNextAction(from json: JSON) {
        let action = ReactNextAction.parseAction(from: json)
        switch action {
        case .otp:
            let phone = json.value(forKey: "phoneSupport", defaultValue: "1900 5588 68")
            runOnMain { [unowned self] in
                if self.isAppear {
                    self.performSegue(withIdentifier: "showOTP", sender: phone)
                } else {
                    self.otpVC?.hBtnRefresh?.constant = 0
                }
            }
        case .retryOtp:
            let eInfor: JSON = json.value(forKey: "data", defaultValue: [:])
            let message = eInfor.value(forKey: "returnmessage", defaultValue: "")
            self.otpVC?.error.value = message
        case .refreshOtp:
            self.otpVC?.error.value = R.string_ViettinBank_Register_Online_Wrong_OTP()
            runOnMain { [unowned self] in
                self.otpVC?.hBtnRefresh?.constant = 40
            }
        }
    }
    
    
    /// Using for return result to SDK
    ///
    /// - Parameter json: Dictionary that include information from React
    fileprivate func processResult(from json: JSON) {
        let action = ReactProcessResult.parseAction(from: json)
        let moduleId = json["moduleId"] as? Int ?? 0
        switch action {
        case .success:
            let infor: JSON = json.value(forKey: "data", defaultValue: [:])
            let transId = infor["zptransid"] as? NSNumber ?? 0
            self.otpVC?.transId = transId.stringValue
            self.otpVC?.result = true
            runOnMain { [unowned self] in
                self.otpVC?.e.onCompleted()
                ReactFactory.sharedInstance().closeModuleInstance(moduleId)
            }
        case .error:
            let eInfor: JSON = json.value(forKey: "data", defaultValue: [:])
            let error = NSError(domain: NSURLErrorDomain, code: NSURLErrorUnknown, userInfo: eInfor)
            guard isAppear.not else {
                runOnMain { [unowned self] in
                    self.eResult.onError(error)
                }
                return
            }
            
            self.otpVC?.result = false
            self.otpVC?.errorResponse = error
            runOnMain { [unowned self] in
                self.otpVC?.e.onCompleted()
                ReactFactory.sharedInstance().closeModuleInstance(moduleId)
            }
        default:
            self.exit(with: nil, moduleId: moduleId)
        }
    }
}

// MARK: - Public Call
extension BankIntegrationViewController {
    fileprivate static func show(on controller: UIViewController?,
                                 from transId: String?,
                                 using info: NSDictionary?) -> Observable<Any?> {
        guard let vc = controller else {
            return Observable.empty()
        }
        return Observable.create({(s) -> Disposable in
            let mController = BankIntegrationViewController.loadFromStoryBoard(with: "BankIntegration")
            mController.transId = transId
            mController.info = info
            _ = mController.eResult.subscribe(s)
            vc.navigationController?.pushViewController(mController, animated: true)
            return Disposables.create ()
        })
    }
    
    @objc static func show(_ controller: UIViewController?,
                     using info: NSDictionary?,
                     from transId: String?,
                     with completion:((Any?) -> ())?,
                     error eHandler:((NSError?) -> ())?) {
        _ = BankIntegrationViewController.show(on: controller, from: transId, using: info).subscribe(onNext: {
            completion?($0)
        }, onError: { eHandler?($0 as NSError) })
    }
}

