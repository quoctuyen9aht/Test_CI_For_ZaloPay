//
//  BankIntegrationInputOTPViewController.swift
//  ZaloPay
//
//  Created by Dung Vu on 10/11/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

fileprivate let kMaxCharacter = 6
final class BankIntegrationInputOTPViewController: UIViewController {
    
    @IBOutlet var tapGesture: UITapGestureRecognizer!
    @IBOutlet weak var overLayView: UIView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnCallSupport: UIButton!
    @IBOutlet weak var lblErrorMessage: UILabel!
    @IBOutlet weak var vIndicator: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var btnRefresh: UIButton!
    @IBOutlet weak var hBtnRefresh: NSLayoutConstraint?
    
    // Public handler
    let e: PublishSubject<Bool> = PublishSubject()
    let activityTracking: Variable<Bool> = Variable(false)
    let error: Variable<String> = Variable("")
    var errorResponse: Error?
    var result: Bool = false
    let textTracking: PublishSubject<JSON> = PublishSubject()
    let eRefreshOTP: PublishSubject<JSON> = PublishSubject()
    var transId: String? = ""
    var phoneSupport: String = ""
    
    // Private Handler
    fileprivate let currentInput: Variable<String> = Variable("")
    fileprivate let eResponse: Variable<Error?> = Variable(nil)
    fileprivate lazy var spinnerView: DGActivityIndicatorView = {
        let v: DGActivityIndicatorView = DGActivityIndicatorView(type:.ballPulse, tintColor: UIColor.subText())
        self.vIndicator.addSubview(v)
        return v
    }()
    fileprivate var canEditText: Bool = true
    fileprivate let disposeBag = DisposeBag()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDisplay()
        setupEvent()
    }
    
    fileprivate func setupDisplay() {
        spinnerView.snp.makeConstraints({ (make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.size.equalTo(self.vIndicator.bounds.size)
        })
        btnCallSupport.setTitle(phoneSupport, for: .normal)
        btnClose.titleLabel?.font = .iconFont(withSize: 16)
        btnClose.setIconFont("red_delete", for: .normal)
        btnClose.titleEdgeInsets = UIEdgeInsetsMake(5, 12, 0, 0)
        btnClose.setTitleColor(.subText(), for: .normal)
        
        // Create
        let icon: String = UILabel.iconCode(withName: "onboarding_refresh") ?? ""
        let titleNormal = String(format: R.string_Onboard_Input_OTP_Refresh(), "\(icon)")
        let atts = NSMutableAttributedString(string: titleNormal, attributes: [NSAttributedStringKey.foregroundColor : UIColor(hexValue:0x008fe5)])
        atts.addAttributes([NSAttributedStringKey.font : UIFont.zaloPay(withSize: 15)], range: (titleNormal as NSString).range(of: icon))
        
        btnRefresh.setAttributedTitle(atts, for: .normal)
    }
    
    fileprivate func setupEvent() {
        self.textField.delegate = self
        keyboardAction()
        errorAction()
        processActionResult()
        
        // Loading
        activityTracking.asDriver().drive(onNext: { [weak self](isLoading) in
            self?.canEditText = isLoading.not
            if isLoading {
                self?.lblErrorMessage.isHidden = true
            }
            guard let wSelf = self else {
                return
            }
            
            isLoading ? wSelf.spinnerView.startAnimating() : wSelf.spinnerView.stopAnimating()
        }).disposed(by: disposeBag)
        
        // Text
        self.textField.rx.text.map ({ $0 ?? "" }).bind(to: currentInput).disposed(by: disposeBag)
        currentInput.asObservable().filter({ $0.count == kMaxCharacter }).bind { [weak self](_) in
            guard self?.textField.isFirstResponder == true else {
                return
            }
            self?.trackOTP()
            }.disposed(by: disposeBag)
        
        // Refresh
        self.btnRefresh.rx.tap.filter({ [weak self] _ in self?.canEditText ?? false }).bind {[weak self] in
            self?.hBtnRefresh?.constant = 40
            self?.error.value = ""
            self?.refreshOTP()
            }.disposed(by: disposeBag)
        
        
        // Support
        self.btnCallSupport.rx.tap.bind { [unowned self] _ in
            self.callSupport()
            }.disposed(by: disposeBag)
        
        // Track event app move on from background
        NotificationCenter.default.rx
            .notification(Notification.Name.UIApplicationDidBecomeActive)
            .takeUntil(self.rx.methodInvoked(#selector(UIViewController.viewWillDisappear(_:))))
            .filter({[weak self] _ in  self?.textField.isFirstResponder.not ?? false })
            .bind
            { [weak self](_) in
                self?.textField.becomeFirstResponder()
            }.disposed(by: disposeBag)
        
    }
    
    fileprivate func callSupport() {
        let number = "tel://\(self.phoneSupport.replacingOccurrences(of: " ", with: ""))"
        (number~>UIApplication.shared.bindingUrl).disposed(by: disposeBag)
    }
    
    fileprivate func errorAction() {
        error.asDriver().drive(onNext: { [weak self] in
            self?.textField.text = ""
            self?.textField.sendActions(for: .valueChanged)
            self?.lblErrorMessage.isHidden = $0.count == 0
            self?.lblErrorMessage.text = $0
        }).disposed(by: disposeBag)
        
        eResponse.asDriver().filter({ $0 != nil }).drive(onNext: { [weak self] in
            self?.errorResponse = $0
            self?.e.onNext(false)
            self?.e.onCompleted()
        }).disposed(by: disposeBag)
    }
    
    fileprivate func processActionResult() {
        let eClose = Observable.merge([self.btnClose.rx.tap.asObservable(), tapGesture.rx.event.map({ _ in})])
        
        eClose.filter({ [weak self] _ in self?.canEditText ?? false }).bind { [weak self](_) in
            self?.e.onCompleted()
            }.disposed(by: disposeBag)
        
        self.e.subscribe(onNext: { [weak self] in
            self?.result = $0
        }).disposed(by: disposeBag)
        
        self.e.subscribe(onCompleted: { [weak self] in
            self?.textField.resignFirstResponder()
        }).disposed(by: disposeBag)
    }
    
    fileprivate func keyboardAction() {
        self.rx
            .methodInvoked(#selector(viewWillAppear(_:)))
            .bind { [weak self](_) in
                self?.textField.becomeFirstResponder()
            }.disposed(by: disposeBag)
        
        let eventKeyboards = [NotificationCenter.default.rx
            .notification(Notification.Name.UIKeyboardWillShow),
                              NotificationCenter.default.rx
                                .notification(Notification.Name.UIKeyboardWillHide)]
        
        Observable.merge(eventKeyboards)
            .takeUntil(self.rx.methodInvoked(#selector(UIViewController.viewWillDisappear(_:))))
            .subscribe(onNext: { [weak self](n) in
                guard let info = n.userInfo , let wSelf = self else {
                    return
                }
                let isHidden = n.name == Notification.Name.UIKeyboardWillHide
                
                let duration: TimeInterval = info[UIKeyboardAnimationDurationUserInfoKey] as? TimeInterval ?? 0
                let f = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue ?? .zero
                
                let nextAlpha: CGFloat = isHidden ? 0 : 0.6
                let nextHeight: CGFloat = isHidden.not ? f.size.height : -wSelf.containerView.bounds.height
                UIView.animate(withDuration: duration, animations: {
                    wSelf.containerView.transform = CGAffineTransform(translationX: 0, y: -nextHeight)
                    wSelf.overLayView.alpha = nextAlpha
                }, completion: nil)
                
            }).disposed(by: disposeBag)
    }
    
    fileprivate func trackOTP() {
        // Will call api from React
        textTracking.onNext(["action": "checkotp", "data": currentInput.value])
    }
    
    fileprivate func refreshOTP() {
        // Will call api register again
        eRefreshOTP.onNext(["action": "nativerefreshotp"])
    }
}

// MARK: - TextField Delegate
extension BankIntegrationInputOTPViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard canEditText else {
            return false
        }
        guard let t = textField.text else {
            return true
        }
        
        let nStr = (t as NSString).replacingCharacters(in: range, with: string)
        let next = nStr.count <= kMaxCharacter
        return next
    }
}

