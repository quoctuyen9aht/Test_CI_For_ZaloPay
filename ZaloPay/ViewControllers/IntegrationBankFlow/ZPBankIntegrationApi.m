//
//  ZPReactNativeHandler.m
//  ZaloPay
//
//  Created by Dung Vu on 10/6/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import "ZPBankIntegrationApi.h"
#import <React/RCTBridgeModule.h>
#import <ZaloPayNetwork/NetworkManager.h>
#import <ZaloPayNetwork/ZPReactNetwork.h>
//#import "ApplicationState.h"
#import <ZaloPayAppManager/PaymentReactNativeApp/ReactNativeAppManager.h>
#import <ZaloPayProfile/ZaloPayProfile-Swift.h>
#import <ZaloPayCommonSwift/ZaloPayCommonSwift-Swift.h>
#import <ZaloPayConfig/ZaloPayConfig-Swift.h>
#import ZALOPAY_MODULE_SWIFT

NSInteger kBankIntegration = 5673;
static NSPointerArray *pointers;
@protocol RACSubscriber;

@interface NetworkManager (TrackResponse)
- (NSError *)trackResult:(NSDictionary *)responseData
                   error:(NSError *)error
                    path:(NSString *)path;
@end

@interface ZPBankIntegrationApi()<RCTBridgeModule>
@end
@implementation ZPBankIntegrationApi
RCT_EXPORT_MODULE()
- (instancetype)init {
    self = [super init];
    if (!pointers) {
        pointers = [NSPointerArray weakObjectsPointerArray];
    }
    
    return self;
}
#pragma mark - add handler
+ (void)registerHandler:(id<ZPReactNativeHandlerProtocol>)handler {
    [pointers addPointer:(__bridge void * _Nullable)(handler)];
}

#pragma mark - handler
RCT_EXPORT_METHOD(processBankIntegration:(NSDictionary *)json
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
    handlerAction(json, ReactActionTypeNext, resolve, reject);
}

RCT_EXPORT_METHOD(showResultBankIntegration:(NSDictionary *)json) {
    handlerAction(json, ReactActionTypeProcess, nil, nil);
}

RCT_EXPORT_METHOD(showLoadingBI) {
    handlerAction(nil, ReactActionTypeShowLoading, nil, nil);
}

RCT_EXPORT_METHOD(hideLoadingBI) {
    handlerAction(nil, ReactActionTypeHideLoading, nil, nil);
}

RCT_EXPORT_METHOD(showSMS:(NSDictionary *)json) {
    handlerAction(json, ReactActionTypeShowSMS, nil, nil);
}

#pragma mark - load BI config
RCT_EXPORT_METHOD(loadBIConfig:(RCTPromiseResolveBlock)resolve
                  rejecter: (RCTPromiseRejectBlock)reject) {
    NSDictionary *config = [ApplicationState sharedInstance].zalopayConfig ?: @{};
    NSDictionary *bankConfig = [config dictionaryForKey:@"bankIntegration" defaultValue:@{}];
    resolve(@{@"code": @(1),
              @"data": bankConfig });
}

#pragma mark - load user infor
RCT_EXPORT_METHOD(loadBIUserInfo:(RCTPromiseResolveBlock)resolve
                  rejecter: (RCTPromiseRejectBlock)reject) {
    NSMutableDictionary *info = [NSMutableDictionary new];
    ZPUserLoginDataSwift *user = [[ZPProfileManager shareInstance] userLoginData];
    NSString *pUserId = [user paymentUserId];
    NSString *key = [NSString stringWithFormat:@"KYC_identifyNumber_%@",pUserId];
    NSString *identifyNumber = [KeyChainStore stringForKey:key service:nil accessGroup:nil];
    NSString *phoneNumber = [user phoneNumber];
    [info setObjectCheckNil:identifyNumber forKey:@"identifyNumber"];
    [info setObjectCheckNil:phoneNumber forKey:@"phoneNumber"];
    resolve(info);
}


#pragma mark - Check use API
bool canUseAPI() {
    if (pointers.count == 0) {
        return false;
    }
    
    id<ZPReactNativeHandlerProtocol> obj = [pointers pointerAtIndex:pointers.count - 1];
    if ([obj identify] != kBankIntegration) {
        return false;
    }
    
    return true;
}

#pragma mark - Request
RCT_EXPORT_METHOD(tpeRequest:(NSString *)path
                  :(NSDictionary *)requestOption
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
  
    if (!canUseAPI()) {
        if (reject) {
            NSError *e = [NSError errorWithDomain:NSURLErrorDomain
                                             code:NSURLErrorCancelled
                                         userInfo:@{ NSLocalizedDescriptionKey : @"This AppId Can't Use This API"}];
            reject(@"-1", @"This AppId Can't Use This API", e);
        }
        return;
    }
    
    DDLogDebug(@"path = %@",path);
    DDLogDebug(@"request option = %@",requestOption);
    NSString *url = [NSString stringWithFormat:@"%@%@",kBaseUrl, path];
    NSString *method;
    NSDictionary *headers;
    NSString *body;
    NSDictionary *preQuery;
    
    if ([requestOption isKindOfClass:[NSDictionary class]]) {
        method = [requestOption stringForKey:@"method" defaultValue:@"GET"];
        headers = [requestOption dictionaryForKey:@"headers" defaultValue:@{}];
        body = [requestOption stringForKey:@"body" defaultValue:@""];
        preQuery = [requestOption dictionaryForKey:@"query" defaultValue:@{}];
    }
    NSMutableDictionary *query = [NSMutableDictionary dictionaryWithDictionary:preQuery];
    [query setObjectCheckNil:[[NetworkManager sharedInstance] accesstoken] forKey:@"accesstoken"];
    [query setObjectCheckNil:[[NetworkManager sharedInstance] paymentUserId] forKey:@"userid"];
    
    ZPReactNetwork *client = [[ZPReactNetwork alloc] init];
    [[client requestWithMethod:method
                        header:headers
                     urlString:url
                        params:query
                          body:body] subscribeNext:^(NSString *result) {
        NSError *e = nil;
        NSDictionary *response = nil;
        if ([result isKindOfClass:[NSString class]]) {
            response = [result JSONValue];
            e = [[NetworkManager sharedInstance] trackResult:response error:nil path:path];
        }else {
            e = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorCannotParseResponse userInfo:@{NSLocalizedDescriptionKey : @" No response!!!!"}];
        }
        if (e) {
            DDLogDebug(@"Error = %@",e);
        }
        
        if (resolve) {
            resolve(response);
        }
        
    } error:^(NSError *error) {
        if (reject) {
            NSDictionary *data = error.userInfo;
            NSString *code = [[data numericForKey:@"error_code"] stringValue];
            NSString *message = [data stringForKey:@"error_message"];
            reject(code, message, error);
        }
    }];
}

void handlerAction(NSDictionary *json,
                   ReactActionType step,
                   SuccessHandler resolve,
                   ErrorHandler reject)
{
    for (NSInteger idx = 0; idx < pointers.count; idx++) {
        id<ZPReactNativeHandlerProtocol> obj = [pointers pointerAtIndex:idx];
        if (obj && [obj isProcess]) {
            ReactNativeAction *action = [[ReactNativeAction alloc] init:json
                                                                   with:resolve
                                                                  using:reject];
            [obj next:action step:step];
        }
    }
}

@end



@implementation ReactNativeAction
- (instancetype)init:(NSDictionary *)json
                with:(SuccessHandler)success
               using:(ErrorHandler)error {
    self = [super init];
    self.json = json;
    self.blockSuccess = success;
    self.blockError = error;
    return self;
}
@end

