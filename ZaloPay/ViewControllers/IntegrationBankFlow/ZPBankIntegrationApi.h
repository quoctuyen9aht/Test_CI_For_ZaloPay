//
//  ZPReactNativeHandler.h
//  ZaloPay
//
//  Created by Dung Vu on 10/6/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^ErrorHandler)(NSString  * _Nullable code, NSString * _Nullable message, NSError  * _Nullable error);
typedef void (^SuccessHandler)(id _Nullable result);
typedef NS_ENUM(NSInteger,ReactActionType) {
    ReactActionTypeNext = 0,
    ReactActionTypeProcess,
    ReactActionTypeShowLoading,
    ReactActionTypeHideLoading,
    ReactActionTypeShowSMS
};

extern NSInteger kBankIntegration;
@class ReactNativeAction;
@protocol ZPReactNativeHandlerProtocol
@property (nonatomic) NSInteger identify;
@property (nonatomic) BOOL isProcess;
- (void)next:(ReactNativeAction *_Nullable)action step:(ReactActionType)step;
@end

@interface ReactNativeAction: NSObject
@property (nonatomic, strong, nullable) NSDictionary *json;
@property (nonatomic, copy, nullable) SuccessHandler blockSuccess;
@property (nonatomic, copy, nullable) ErrorHandler blockError;
- (instancetype _Nonnull)init:(NSDictionary * _Nullable)json
                with:(SuccessHandler _Nullable)success
               using:(ErrorHandler _Nullable)error;
@end

@interface ZPBankIntegrationApi : NSObject
+ (void) registerHandler: (id<ZPReactNativeHandlerProtocol> _Nullable)handler NS_SWIFT_NAME(register(handler:));
@end
