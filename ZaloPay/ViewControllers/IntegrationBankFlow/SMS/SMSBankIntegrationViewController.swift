//
//  SacombankViewController.swift
//  ZaloPay
//
//  Created by Dung Vu on 9/6/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import MessageUI
import ZaloPayConfig

enum SacombankBoldTextType: Int {
    case step1
    case step2
    case step3
    
    var bold: [String] {
        switch self {
        case .step1:
            return [R.string_UpdateAccountName_Register()!.uppercased()]
        case .step2:
            return []
        case .step3:
            return ["ZaloPay"]
        }
    }
    
    var title: String {
        switch self {
        case .step1:
            return R.string_Sacombank_Register_Online_Step1()
        case .step2:
            return R.string_Sacombank_Register_Online_Step2()
        default:
            return ""
        }
    }
}

struct BankRegisterConfig {
    let numberLastDigit: Int
    let phone: String
    let sms: String
    let price: String
    
    init(_ json: JSON) {
        self.numberLastDigit = json.value(forKey: "numberLastDigit", defaultValue: 1)
        self.phone = json.value(forKey: "phone", defaultValue: "")
        self.sms = json.value(forKey: "sms", defaultValue: "")
        self.price = json.value(forKey: "price", defaultValue: "")
    }
    
    init(_ config: ZPEnableOBConfigProtocol) {
        self.numberLastDigit = config.getNumberLastDigit()
        self.phone = config.getPhone()
        self.sms = config.getSMS()
        self.price = config.getPrice()
    }
}

// MARK: Container controller
final class SMSContainerViewController: UITableViewController {
    @IBOutlet var noteLabels: [UILabel]!
    fileprivate var cardNumber: String?
    fileprivate var config: BankRegisterConfig!
    fileprivate var lastDigit: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupDisplay()
    }
    
    fileprivate func setupDisplay() {
        self.tableView.estimatedRowHeight = 54
        self.tableView.rowHeight = UITableViewAutomaticDimension
        // label
        let fourDigit = self.lastDigit ?? "****"
        noteLabels.enumerated().forEach { (v) in
            guard let step = SacombankBoldTextType(rawValue: v.offset) else {
                return
            }
            
            var boldText = step.bold
            if step.title.count > 0 {
                switch step {
                case .step1:
                    let body = String(format: config.sms, fourDigit)
                    boldText.append(body)
                    let textDisplay = String(format: step.title, body)
                    v.element.text = textDisplay
                case .step2:
                    boldText.append(config.phone)
                    boldText.append(config.price)
                    let textDisplay = String(format: step.title, config.phone, config.price)
                    v.element.text = textDisplay
                default:
                    break
                }
            }
            
            let text = v.element.text ?? ""
            let atts = NSMutableAttributedString(attributedString: v.element.attributedText ?? NSAttributedString(string: text))
            let font = UIFont.sfuiTextSemibold(withSize: 16) ?? UIFont.boldSystemFont(ofSize: 16)
            
            boldText.forEach({
                let r = (text as NSString).range(of: $0)
                guard r.location != NSNotFound else {
                    return
                }
                atts.addAttributes([NSAttributedStringKey.font : font], range: r)
            })
            v.element.attributedText = atts
        }
    }
}

// MARK: - Main Controller
final class SMSBankIntegrationViewController: UIViewController {
    @IBOutlet weak var btnRegister: UIButton!
    fileprivate var keyBank: String?
    fileprivate var cardNumber: String?
    fileprivate let eResult: PublishSubject<Any?> = PublishSubject()
    fileprivate let disposeBag = DisposeBag()
    fileprivate lazy var config: BankRegisterConfig = {
        return self.loadConfig()
    }()
    
    fileprivate lazy var lastDigit: String? = {
        return self.findLastDigit()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDisplay()
        setupEvent()
    }
    
    fileprivate func setupEvent() {
        self.btnRegister.rx.tap.bind { [weak self] in
            self?.openSMS()
            }.disposed(by: disposeBag)
    }
    
    fileprivate func loadConfig() -> BankRegisterConfig {
        let key = keyBank ?? ""
//        guard let appConfig = ZPSDKHelper.bankBIConfig.first(where: { $0.value(forKey: "bankCode", defaultValue: "") == key }) else {
//            fatalError("Not implement \(key)")
//        }
        guard let appConfig = ZPSDKHelper.bankBIConfig.first(where: { $0.getBankCode() == key }) else {
            fatalError("Not implement \(key)")
        }
        return BankRegisterConfig(appConfig)
    }
    
    fileprivate func findLastDigit() -> String?{
        guard let card = self.cardNumber, card.count >= self.config.numberLastDigit else {
            return nil
        }
        
        let location = card.count - config.numberLastDigit
        let startIndex = card.index(card.startIndex, offsetBy: location)
        let result = card[startIndex...]
        return String(result)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let containerVC = segue.destination as? SMSContainerViewController else {
            return
        }
        containerVC.cardNumber = self.cardNumber
        containerVC.config = self.config
        containerVC.lastDigit = self.lastDigit
    }
    
    fileprivate func setupDisplay() {
        self.btnRegister.setBackgroundColor(.zaloBase(), for: .normal)
        self.btnRegister.setBackgroundColor(#colorLiteral(red: 0.2862745098, green: 0.7333333333, blue: 1, alpha: 1), for: .highlighted)
        self.btnRegister.layer.cornerRadius = 3
        self.btnRegister.clipsToBounds = true
        
        self.title = R.string_Dialog_Title_Notification()
        // Add back Button
        let btnBack = UIButton(type: .custom)
        btnBack.frame = CGRect(origin: .zero, size: CGSize(width: 20, height: 20))
        btnBack.setIconFont("red_delete", for: .normal)
        btnBack.titleLabel?.font = UIFont.iconFont(withSize: 16)
        btnBack.setTitleColor(.white, for: .normal)
        btnBack.rx.tap.bind {[weak self] in
            self?.eResult.onNext(nil)
            }.disposed(by: disposeBag)
        
        let flexiable = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        flexiable.width = -12
        let barButton = UIBarButtonItem(customView: btnBack)
        self.navigationItem.leftBarButtonItems = [flexiable, barButton]
    }
    
    fileprivate func openSMS() {
        guard MFMessageComposeViewController.canSendText() else {
            return
        }
        let smsController = MFMessageComposeViewController()
        smsController.messageComposeDelegate = self
        smsController.recipients = ["\(self.config.phone)"]
        smsController.body = String(format: self.config.sms, "\(self.lastDigit ?? "****")")
        self.present(smsController, animated: true, completion: nil)
    }
}

// MARK: SMS Delegate
extension SMSBankIntegrationViewController: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
}

// MARK: Public Call
extension SMSBankIntegrationViewController {
    fileprivate static func show(on controller: UIViewController?,
                                 from transId: String?,
                                 using info: NSDictionary?) -> Observable<Any?> {
        guard let vc = controller else {
            return Observable.empty()
        }
        
        return Observable.create({(s) -> Disposable in
            let mController = SMSBankIntegrationViewController.loadFromStoryBoard(with: "SMS")
            let dict = info as? JSON
            mController.cardNumber = dict?.value(forKey: "cardnumber", defaultValue: "")
            let code = dict?.value(forKey: "bankcode", defaultValue: "") ?? ""
            mController.keyBank = code
            _ = mController.eResult.subscribe(s)
            vc.navigationController?.pushViewController(mController, animated: true)
            return Disposables.create ()
        })
    }
    
    static func show(_ controller: UIViewController?,
                     using info: NSDictionary?,
                     from transId: String?,
                     with completion:((Any?) -> ())?,
                     error eHandler:((NSError?) -> ())?) {
        _ = SMSBankIntegrationViewController.show(on: controller, from: transId, using: info).subscribe(onNext: {
            completion?($0)
        }, onError: { eHandler?($0 as NSError) })
    }
}


