//
//  AccountClearingApi.h
//  ZaloPay
//
//  Created by Phuoc Huynh on 10/20/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface AccountClearingApi : NSObject<RCTBridgeModule>

@end
