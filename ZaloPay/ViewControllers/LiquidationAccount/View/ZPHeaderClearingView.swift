//
//  ZPHeaderClearingView.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 10/12/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

final class ZPHeaderClearingView: UIView {
    
    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
   
    
    var backBtn: UIButton? {
        return self.viewWithTag(291) as? UIButton
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        backBtn?.tintColor = UIColor.zaloBase()
        backBtn?.titleLabel?.font = UIFont.zaloPay(withSize: 18)
        backBtn?.setIconFont("general_backios", for: .normal)
        backBtn?.titleEdgeInsets = UIEdgeInsetsMake(0, 12, 0, 0)
        roundAvatar()
        
    }
    
    fileprivate func roundAvatar() {
        avatarImg.layer.cornerRadius = avatarImg.bounds.width / 2
        avatarImg.clipsToBounds = true
        avatarImg.layer.borderWidth = 1.5
        avatarImg.layer.borderColor = UIColor.zaloBase().cgColor
    }
    
    
}



extension UIImage {
    static func drawIcon(with text: String,
                         using font: UIFont,
                         with color: UIColor,
                         at size: CGSize) -> UIImage?
    {
        let att = [NSAttributedStringKey.font : font, NSAttributedStringKey.foregroundColor : color]
        let sT = (text as NSString).size(withAttributes: att)
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        context.setFillColor(UIColor.clear.cgColor)
        let rect = CGRect(x:(size.width - sT.width) / 2, y:(size.height - sT.height) / 2, width: size.width, height: size.height)
        color.setFill()
        (text as NSString).draw(in: rect, withAttributes: att)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result
    }
}

extension UIImageView {
    func load(with iconFont: String, at size: CGFloat, using color: UIColor = .zaloBase()) {
        guard let text = UILabel.iconCode(withName: iconFont) else {
            return
        }
        
        let img = UIImage.drawIcon(with: text, using: UIFont.zaloPay(withSize: size), with: color, at: self.bounds.size)
        self.image = img?.withRenderingMode(.alwaysOriginal)
    }
}
