//
//  AccountClearingApi.m
//  ZaloPay
//
//  Created by Phuoc Huynh on 10/20/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import "AccountClearingApi.h"
#import "NetworkManager+Wallet.h"
#import "NetworkManager+Profile.h"
#import ZALOPAY_MODULE_SWIFT
#import "BillDetailViewControllerDelegate.h"
#import "ZPBankMapping.h"

#import <ZaloPayNetwork/NSError+ZaloPayAPI.h>
#import <ZaloPayWalletSDK/ZPPaymentChannel.h>
#import <ZaloPayWalletSDK/Internal/NetworkManager+ZaloPayWalletSDK.h>
#import <ZaloPayProfile/ZaloPayProfile-Swift.h>
#import <ZaloPayCommonSwift/ZaloPayCommonSwift-Swift.h>


@class ZaloPayWalletSDKPayment;
@class ZPSavedBankAccount;
@implementation AccountClearingApi
RCT_EXPORT_MODULE()

- (instancetype)init {
    self = [super init];
    return self;
}
- (NSMutableDictionary *) dictReturnWithData:(id)data errCode:(int)code message:(NSString *)message  {
    NSMutableDictionary *dicReturn = [NSMutableDictionary new];
    [dicReturn setObjectCheckNil:[NSNumber numberWithInt:code] forKey:@"code"];
    [dicReturn setObjectCheckNil:message forKey:@"message"];
    [dicReturn setObjectCheckNil:data forKey:@"data"];
    return dicReturn;
}

- (NSMutableDictionary *) dictReturnSuccessWithData:(id)data {
    return [self dictReturnWithData:data errCode:1 message:@"success"];
}


RCT_EXPORT_METHOD(getBalance:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
    
    [[[NetworkManager sharedInstance] getBalance] subscribeNext:^(NSDictionary* dic) {
        NSDictionary *data = [NSDictionary dictionaryWithObject:[dic stringForKey:@"zpwbalance"] forKey:@"balance"];
        resolve([self dictReturnSuccessWithData:data]);
    } error:^(NSError *error) {
        resolve([self dictReturnWithData:[NSDictionary new] errCode:-1 message:[error apiErrorMessage]]);
    }];
    
}
RCT_EXPORT_METHOD(getBankCards:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[ZaloPayWalletSDKPayment sharedInstance] getPaymentGatewayInfoCompleteHandle:^(int errorCode, NSString *mesage) {
            if (errorCode != 0) {
                resolve([self dictReturnWithData:[NSDictionary new] errCode:-1 message:mesage]);
                return ;
            }
            [[[ZPWalletManagerSwift sharedInstance] getListBankSupport] subscribeCompleted:^{
                NSDictionary *data = [NSDictionary dictionaryWithObject:[self arrListBank] forKey:@"cards"];
                resolve([self dictReturnSuccessWithData:data]);
            }];
        }];
    });
}
- (NSMutableArray *)arrListBank {
    NSMutableArray *arrCards = [[NSMutableArray alloc] init];
    for (ZPSavedCard *card in [[ZaloPayWalletSDKPayment sharedInstance] paymentSavedCard]) {
        NSString *bankCode = [self getFinalBankCode:card.bankCode andFirst:card.first6CardNo];
        [arrCards addObject:[ZPSavedCard jsonFromObject:card andName:[ZPBankMapping bankName:bankCode] andFinalBankcode:bankCode]];
    }
    for (ZPSavedBankAccount *card in [[ZaloPayWalletSDKPayment sharedInstance] paymentSavedBankAccount]) {
        NSString *bankCode = [self getFinalBankCode:card.bankCode andFirst:card.firstAccountNo];
        [arrCards addObject:[ZPSavedBankAccount jsonFromObject:card andName:[ZPBankMapping bankName:bankCode] andFinalBankcode:bankCode]];
    }
    return arrCards;
}
- (NSString *)getFinalBankCode:(NSString *)bankCode andFirst:(NSString *)first6CardNo {
    NSString *bankCodeFinal = bankCode;
    if ([bankCode.lowercaseString isEqualToString:CC.lowercaseString]) {
        bankCodeFinal = [[ZaloPayWalletSDKPayment sharedInstance] getCcBankCodeFromConfig:first6CardNo];
    }
    return bankCodeFinal;
}
RCT_EXPORT_METHOD(logout) {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[LoginManagerSwift sharedInstance] logout];
    });
}
RCT_EXPORT_METHOD(removeCard:(NSDictionary *)card
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
    
    if ([ZPBankMapping bankType:[card stringForKey:@"bankcode"] first6CardNo:[card stringForKey:@"first6cardno"]] == ZPVietcombank) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSMutableDictionary *dict = [NSMutableDictionary new];
            [dict setObjectCheckNil:[card stringForKey:@"first6cardno"] forKey:@"firstaccountno"];
            [dict setObjectCheckNil:[card stringForKey:@"last4cardno"] forKey:@"lastaccountno"];
            [dict setObjectCheckNil:[card stringForKey:@"bankcode"] forKey:@"tpebankcode"];
            ZPSavedBankAccount *account = [ZPSavedBankAccount fromJson:dict];
            NSString *name = [ZPBankMapping bankName:account.bankCode];
            NSString *message = [NSString stringWithFormat:[R string_LinkBank_Delete_Notification_Message], name];
            UINavigationController *navigationController = [[ZPAppFactory sharedInstance] rootNavigation];
            [navigationController defaultNavigationBarStyle];
            UIViewController *viewController = navigationController.topViewController;
            
            @weakify(viewController);
            [ZPDialogView showDialogWithType:DialogTypeNotification
                                     message:message
                           cancelButtonTitle:[R string_ButtonLabel_Skip]
                            otherButtonTitle:@[[R string_ButtonLabel_OK]]
                              completeHandle:^(int buttonIndex, int cancelButtonIndex) {
                                  if (buttonIndex != cancelButtonIndex) {
                                      BillDetailViewController *billDetailViewController = [[BillDetailViewController alloc] init:^(NSDictionary *data) {
                                          @strongify(viewController);
                                          resolve([self dictReturnSuccessWithData:[NSDictionary new]]);
                                          [viewController.navigationController popToViewController:viewController animated:YES];
                                      }                                                                                    cancel:^(NSDictionary *data) {
                                          @strongify(viewController);

                                          [viewController.navigationController popToViewController:viewController animated:YES];
                                          resolve([self dictReturnWithData:[NSDictionary new] errCode:-1 message:[R string_Clearing_reject_transaction]]);
                                      }                                                                                     error:^(NSDictionary *data) {
                                          @strongify(viewController);

                                          [viewController.navigationController popToViewController:viewController animated:YES];
                                          resolve([self dictReturnWithData:[NSDictionary new] errCode:-1 message:[R string_LinkCard_Remove_Error]]);
                                      }];
                                      [billDetailViewController startProcessBill:[ZPBill getRemovedBankAccountWithBankCode:account.bankCode] from:viewController];
                                  } else {
                                      [[ZPAppFactory sharedInstance] hideAllHUDsForView:viewController.view];
                                  }

                              }];
            
        });
        return;
    }

    NSString *originalBankCode = [card stringForKey:@"bankcode"];
    if (   [originalBankCode isEqualToString:VISA]
        || [originalBankCode isEqualToString:MASTER]
        || [originalBankCode isEqualToString:JCB]) {
        originalBankCode = CC;
    }
    
    if ([[ZPSDKHelper new] isBankAccountExist:originalBankCode]) {
        ZPSavedBankAccount *bankAccount = [[ZPSavedBankAccount alloc] init];
        bankAccount.bankCode = originalBankCode;
        bankAccount.firstAccountNo = [card stringForKey:@"first6cardno"];
        bankAccount.lastAccountNo = [card stringForKey:@"last4cardno"];
        NSString *bankCustomerId = [ZPProfileManager shareInstance].userLoginData.paymentUserId;
        [[[ZaloPayWalletSDKPayment sharedInstance] removeBankAccount:bankCustomerId bankAccount:bankAccount] subscribeNext:^(NSDictionary *dictionary) {
            resolve([self dictReturnSuccessWithData:[NSDictionary new]]);
        }];
        return;
    }
    [[[ZaloPayWalletSDKPayment sharedInstance] removeCard:originalBankCode
                                             first6cardno:[card stringForKey:@"first6cardno"]
                                              last4cardno:[card stringForKey:@"last4cardno"]]
     subscribeNext:^(NSDictionary *dictionary) {
         NSArray *cardinfos = [dictionary arrayForKey:@"cardinfos"];
         if (cardinfos) {
             NSMutableArray *allAccount = [NSMutableArray array];
             for (NSDictionary *oneDic in cardinfos) {
                 ZPSavedCard *account = [ZPSavedCard fromJson:oneDic];
                 [allAccount addObject:account];
             }
             [[ZaloPayWalletSDKPayment sharedInstance] updatepaymentSavedCard:allAccount];
             resolve([self dictReturnSuccessWithData:[NSDictionary new]]);
         }
     } error:^(NSError *error) {
         resolve([self dictReturnWithData:[NSDictionary new] errCode:(int)error.code message:[error apiErrorMessage]]);
     }];
}
RCT_EXPORT_METHOD(launchTransferMoney:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
    dispatch_async(dispatch_get_main_queue(), ^{
        UINavigationController *navigationController = [[ZPAppFactory sharedInstance] rootNavigation];
        [navigationController defaultNavigationBarStyle];
        TransferHomeViewController *transfer = [TransferHomeRouter assembleModule];
        @weakify(self);
        [[transfer rac_willDeallocSignal] subscribeCompleted:^{
            @strongify(self);
            [[[NetworkManager sharedInstance] getBalance] subscribeNext:^(NSDictionary* dic) {
                NSDictionary *data = [NSDictionary dictionaryWithObject:[dic stringForKey:@"zpwbalance"] forKey:@"balance"];
                resolve([self dictReturnSuccessWithData:data]);
            } error:^(NSError *error) {
                resolve([self dictReturnWithData:[NSDictionary new] errCode:-1 message:[error apiErrorMessage]]);
            }];
        }];
        [navigationController pushViewController:transfer animated:YES];
    });
    
    
}
RCT_EXPORT_METHOD(launchWithDraw:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        UINavigationController *navigationController = [[ZPAppFactory sharedInstance] rootNavigation];
        [navigationController defaultNavigationBarStyle];
        UIViewController *controller;
        if (![ZPWithdrawHelperSwift validWithdrawCondition]) {
            controller = [WithdrawConditionWireFrame createWithDrawConditionModule];
        }else {
            controller = [ZPWithdrawRouter assembleModule];
        }
        [navigationController pushViewController: controller animated:YES];
        @weakify(self);
        [[controller rac_willDeallocSignal] subscribeCompleted:^{
            @strongify(self);
            [[[NetworkManager sharedInstance] getBalance] subscribeNext:^(NSDictionary* dic) {
                NSDictionary *data = [NSDictionary dictionaryWithObject:[dic stringForKey:@"zpwbalance"] forKey:@"balance"];
                resolve([self dictReturnSuccessWithData:data]);
            } error:^(NSError *error) {
                resolve([self dictReturnWithData:[NSDictionary new] errCode:-1 message:[error apiErrorMessage]]);
            }];
        }];
        
        
    });
    
}
RCT_EXPORT_METHOD(clearAccount:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
    
        [[[NetworkManager sharedInstance] lockUserForClient] subscribeNext:^(id x) {
            
            resolve([self dictReturnSuccessWithData:[NSDictionary new]]);
            
        }error:^(NSError *error) {
            resolve([self dictReturnWithData:[NSDictionary new] errCode:-1 message:[error apiErrorMessage]]);
        }];
    
}
RCT_EXPORT_METHOD(promptPIN:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
    dispatch_async(dispatch_get_main_queue(), ^{

        [[ZPAppFactory sharedInstance] promptPIN:4 callback:^(id result) {
            if ([result intValue] != 1) {
                resolve([self dictReturnWithData:[NSDictionary new] errCode:-1 message:@""]);
                return ;
            }
            resolve([self dictReturnSuccessWithData:[NSDictionary new]]);
        } error:^(NSError *error) {
            if (error.code == -161) {
                [ZPCheckPinViewController dissmiss:NO];
                resolve([self dictReturnWithData:[NSDictionary new] errCode:(int)error.code message:[error apiErrorMessage]]);
            }
            
        }];
    });
}
RCT_EXPORT_METHOD(getBankCardsInCache:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSDictionary *data = [NSDictionary dictionaryWithObject:[self arrListBank] forKey:@"cards"];
        resolve([self dictReturnSuccessWithData:data]);
        
    });
    
}
@end
