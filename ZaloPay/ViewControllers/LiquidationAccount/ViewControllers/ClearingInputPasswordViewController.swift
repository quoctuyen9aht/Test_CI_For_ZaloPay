//
//  ClearingInputPasswordViewController.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 10/11/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import CocoaLumberjackSwift
import ZaloPayAnalyticsSwift


final class ClearingInputPasswordViewController: BaseInputViewController{
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var topContainView: NSLayoutConstraint!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var heightBtn: NSLayoutConstraint!
    
    @IBOutlet var tapGesture: UITapGestureRecognizer!
    fileprivate var _password: String = ""
    fileprivate var currentInput: Variable<String> = Variable("")
    fileprivate let state: Variable<PasswordState> = Variable(.identify)
    
    var phoneNumber:String?
    
    lazy var textField: UITextField = {
        let tField = UITextField(frame: .zero)
        self.view.addSubview(tField)
        tField.delegate = self
        tField.keyboardType = .numberPad
        return tField
    }()
    
    override func viewDidLoad() {
        currentDate = Date()
        super.viewDidLoad()
        self.input = textField
        collectionView.allowsMultipleSelection = true
        let isSmallView = UIScreen.main.bounds.height < 603
        self.topContainView.constant = isSmallView ? 25 : 35

        self.configShowKeyboardForClearing()
        //let notificationCenter = NotificationCenter.default
        NotificationCenter.default.addObserver(self, selector: #selector(appBecomeActive), name: Notification.Name.UIApplicationDidBecomeActive, object: nil)

    }
    @objc func appBecomeActive() {
        self.textField.becomeFirstResponder()
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIApplicationDidBecomeActive, object: nil)
    }
    
//    override func screenName() -> String! {
//        return ZPTrackerScreen.Screen_Onboarding
//    }
   
    override func setupButton() {
        self.roundButton(self.btnBack)
        self.showBack(will: false)
    }
    
    func showBack(will show:Bool) {
        self.btnBack.isHidden = show.not
        let nextH = show ? hBackBtn : 0
        let title = show ? "Quay lại" : ""
        heightBtn.constant = nextH
        self.btnBack.isEnabled = show
        self.btnBack.setTitle(title, for: .normal)
    }
    
    override func setupEvent() {
        super.setupEvent()
        tapGesture.rx
            .event
            .filter({[weak self] _ in self?.textField.isFirstResponder == false })
            .subscribe(onNext: { [weak self](_) in
                self?.textField.becomeFirstResponder()
            }).disposed(by: disposeBag)
        // same collection datasource
        Observable.just(0..<6)
            .bind(to: self.collectionView.rx.items(cellIdentifier: "OnboardInputPasswordCell", cellType: OnboardInputPasswordCell.self))
            {
                $2.isSelected = $0 < self.currentInput.value.count
            }
            .disposed(by: disposeBag)
        
        //gan text vaof current input
        textField.rx.text.map { $0 ?? ""}.bind(to: currentInput).disposed(by: disposeBag)
        
        // same should change character
        currentInput.asDriver().drive(onNext: { [weak self] in
            let total = $0.count
            (0..<6).map({ IndexPath(item: $0, section: 0) }).forEach({
                guard let cell = self?.collectionView.cellForItem(at: $0) as? OnboardInputPasswordCell else {
                    return
                }
                cell.isSelected = $0.row < total
            })
        }).disposed(by: disposeBag)
        
        
        // Track UI from state
        state.asDriver().drive(onNext: { [weak self] _ in
            self?.updateStateUI()
        }).disposed(by: disposeBag)
        
        // Next state
        currentInput.asDriver().filter({
            return $0.count == 6
        }).debounce(0.2).drive(onNext: { [weak self](s) in
            self?._password = s
            if NetworkState.sharedInstance().isReachable == false {
                ZPDialogView.showDialog(with: DialogTypeNoInternet, title: R.string_Dialog_Warning(), message: R.string_NetworkError_NoConnectionMessage(), buttonTitles:[R.string_ButtonLabel_Close()], handler: nil)
                return
            }
            ZPAppFactory.sharedInstance().showHUDAdded(to: self?.view)
            NetworkManager.sharedInstance().login(withPhone: self?.phoneNumber, pin: self?._password).subscribeNext({(_) in
                
                self?.resetPasswordInput()
                self?.state.value = .end
            }, error:({(error) in
                DispatchQueue.main.async {
                    ZPAppFactory.sharedInstance().hideAllHUDs(for: self?.view)
                    self?.resetPasswordInput()
                    if (error?.errorCode())! == ZALOPAY_ERRORCODE_USER_IS_LOCKED.rawValue
                    || (error?.errorCode())! == ZALOPAY_ERRORCODE_OVERTIME_CLEARING.rawValue {
                        let message = (error?.errorCode())! == ZALOPAY_ERRORCODE_USER_IS_LOCKED.rawValue ? R.string_Clearing_account_locked() : R.string_Clearing_overtime()
                        ZPDialogView.showDialog(with: DialogTypeNotification, title: nil , message: message , buttonTitles: [R.string_ButtonLabel_Close()]) { (idx) in
                            if (idx == 0) {
                                LoginManagerSwift.sharedInstance.logout()
                            }
                        }
                        return
                    }
                    if (error?.errorCode())! == ZALOPAY_ERRORCODE_OVER_INPUT_CHANGE_PASSWORD.rawValue
                    || (error?.errorCode())! == ZALOPAY_ERRORCODE_OVER_OTP.rawValue {
                        let clearingMaxErrorPinViewController = self?.storyboard?.instantiateViewController(withIdentifier: "ClearingMaxErrorPinViewController") as! ClearingMaxErrorPinViewController
                        clearingMaxErrorPinViewController.maxErrorType = (error?.errorCode())! == ZALOPAY_ERRORCODE_OVER_INPUT_CHANGE_PASSWORD.rawValue ? .MaxErrorTypePin : .MaxErrorTypeOTP; self?.navigationController?.pushViewController(clearingMaxErrorPinViewController, animated: true)
                        return
                    }
                    if (error?.errorCode())! == ZALOPAY_ERRORCODE_PIN_NOT_MATCH.rawValue {
                        let message = error?.userInfoData()["returnmessage"] as? String ?? ""
                        self?.lblWarning.text = message
                        self?.lblWarning.textColor = UIColor.zp_red()
                        return
                    }
                    
                    let message = R.string_Clearing_login_undefine_error()
                    ZPDialogView.showDialog(with: DialogTypeNotification, title: nil , message: message , buttonTitles: [R.string_ButtonLabel_Close()]) { (idx) in
                        if (idx == 0) {
                            LoginManagerSwift.sharedInstance.logout()
                        }
                    }
                    
                }
            }))
        }).disposed(by: disposeBag)
        
        state.asDriver().filter({ $0 == .end }).drive(onNext: { [weak self](_) in
            ZPAppFactory.sharedInstance().hideAllHUDs(for: self?.view)
            self?.performSegue(withIdentifier: "ShowSMS", sender: nil)
        }).disposed(by: disposeBag)
    }
    
    func resetPasswordInput() {
        _ = Observable.just("").bind(to: self.textField.rx.text)
        self.currentInput.value = ""
    }
    
    func updateStateUI() {
        self.backTop?.isHidden = false
        lblTitle.text = R.string_Clearing_input_pin()
        lblNote.text = R.string_Clearing_login_text()
        lblWarning.text = ""
        isCorrect = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        guard let vc = segue.destination as? ClearingInputSMSViewController else {
            return
        }
        vc.password = _password
        vc.phone = self.phoneNumber
    }
}

extension ClearingInputPasswordViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let t = textField.text else {
            return true
        }
        let nStr = (t as NSString).replacingCharacters(in: range, with: string)
        let next = nStr.count <= 6
        return next
    }
}
