//
//  ClearingInputSMSViewController.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 10/11/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Foundation
import ZaloPayProfile
import ZaloPayAnalyticsSwift

fileprivate let timeResend: TimeInterval = 60
class ClearingInputSMSViewController: BaseInputViewController {
    
    var reactAppRouter = ZPReactNativeAppRouter()
    var phone: String!
    var password: String!
    @IBOutlet weak var textField: UITextField! {
        didSet{
            self.input = textField
        }
    }
    @IBOutlet weak var topContainView: NSLayoutConstraint!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnResend: UIButton!
    @IBOutlet weak var lblTimer: UILabel!
    fileprivate var timer: Disposable?
    fileprivate let numberResend: Variable<Int> = Variable(1)
    fileprivate let trackErrorCode: Variable<Int> = Variable(0)
    fileprivate var timeRefresh: TimeInterval = timeResend
    fileprivate lazy var calendar: Calendar = Calendar.current
    fileprivate var dateCheck: Date!
    
    var isEnableButton: Bool = false {
        didSet{
            btnNext.isEnabled = isEnableButton
            setBorderColor(isEnableButton)
        }
    }
    
    fileprivate lazy var popUpResend: UIView = {
        return self.setupPopUpResend()
    }()
    
    @objc func appBecomeActive() {
        self.textField.becomeFirstResponder()
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIApplicationDidBecomeActive, object: nil)
    }
    
    override func setupDisplay() {
        super.setupDisplay()
        self.textField.delegate = self
        self.lblTitle.text = R.string_Clearing_verify_OTP()
        let s = String(format: R.string_Onboard_Input_OTP_Note() , "\((phone as NSString).removePhoneCountryCode() ?? "")")
        self.lblNote.text = s
        self.lblWarning.text = ""
        isCorrect = true
        let isSmallView = UIScreen.main.bounds.height < 603
        self.topContainView.constant = isSmallView ? 25 : 35
        self.configShowKeyboardForClearing()
        NotificationCenter.default.addObserver(self, selector: #selector(appBecomeActive), name: Notification.Name.UIApplicationDidBecomeActive, object: nil)
    }
    
    override func setupButton() {
        self.backTop?.isHidden = false
        self.roundButton(btnNext)
        self.lblTimer.textColor = UIColor.zp_gray()
        btnNext.setTitle(R.string_Onboard_Input_OTP_Confirm(), for: .normal)
        btnNext.setTitle(R.string_Onboard_Input_OTP_Confirm(), for: .disabled)
        btnNext.setTitleColor(.white, for: .normal)
        btnNext.setBackgroundColor(UIColor.zaloBase(), for: UIControlState.normal)
        btnNext.setBackgroundColor(UIColor(hexValue: 0xc7cad3), for: UIControlState.disabled)
        btnNext.setTitleColor(.white, for: .disabled)
        isEnableButton = false
        btnResend.setTitleColor(UIColor.zaloBase(), for: .normal)
        // Create
        let icon: String = UILabel.iconCode(withName: "onboarding_refresh") ?? ""
        let titleNormal = String(format: R.string_Onboard_Input_OTP_Refresh(), "\(icon)")
        let atts = NSMutableAttributedString(string: titleNormal, attributes: [NSAttributedStringKey.foregroundColor : UIColor(hexValue:0x008fe5)])
        atts.addAttributes([NSAttributedStringKey.font : UIFont.zaloPay(withSize: 15)], range: (titleNormal as NSString).range(of: icon))
        
        btnResend.setAttributedTitle(atts, for: .normal)
    }
    
    func setBorderColor(_ enabled: Bool) {
        btnNext.layer.borderColor = (enabled ? UIColor.zaloBase() : UIColor(hexValue: 0xc7cad3)).cgColor
        
    }
    
    override func setupEvent() {
        super.setupEvent()
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.onboarding_input_otp_success)
        
        let eventText = textField.rx.text.map({ $0 ?? ""})
        eventText.bind { [weak self] in
            self?.isEnableButton = $0.count == 6
            }.disposed(by: disposeBag)
        
        eventText.filter({ $0.count < 6 }).subscribe(onNext: { [weak self](_) in
            self?.lblWarning.text = ""
            self?.isCorrect = true
        }).disposed(by: disposeBag)
        
        
        btnResend.rx.controlEvent(.touchUpInside).subscribe(onNext: { [weak self](_) in
            self?.resendPhone()
        }).disposed(by: disposeBag)
        
        btnNext.rx.controlEvent(.touchUpInside).subscribe(onNext: { [weak self](_) in
            self?.checkOTP()
        }).disposed(by: disposeBag)
        
        self.eventBack?.bind(onNext: { [weak self](_) in
            // Event Alert
            self?.showAlertBack()
        }).disposed(by: disposeBag)
        
        // Track number resend
        numberResend.asDriver().drive(onNext: { [weak self] in
            self?.timeRefresh = $0 % 3 == 0 ? timeResend * 15 : timeResend
            self?.createTimer()
        }).disposed(by: disposeBag)
        
        
        // Track Error Code Over Limit Send Identify OTP
        trackErrorCode.asDriver().filter({ $0 == -2002 }).drive(onNext: { (_) in
            // Show Alert -> move to login
            //-----self?.showAlertMoveToLogin()
            
        }).disposed(by: disposeBag)
    }
    
    fileprivate func setupPopUpResend() -> UIView {
        let v = UIView(frame: .zero)
        v.layer.cornerRadius = 10
        v.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.54)
        self.view.addSubview(v)
        let top: CGFloat = isIphone4 ? 50 : 114
        v.snp.makeConstraints { (make) in
            make.top.equalTo(top)
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.equalTo(228)
            make.height.equalTo(100)
        }
        
        let icon = UILabel(frame: .zero)
        icon.font = UIFont.zaloPay(withSize: 27)
        icon.textColor = .white
        v.addSubview(icon)
        icon.snp.makeConstraints { (make) in
            make.top.equalTo(27)
            make.centerX.equalTo(v.snp.centerX)
            make.width.greaterThanOrEqualTo(0)
            make.height.greaterThanOrEqualTo(0)
        }
        icon.text = UILabel.iconCode(withName: "general_check")
        
        let lblDescription = UILabel(frame: .zero)
        lblDescription.font = UIFont.sfuiTextRegular(withSize: 15)
        lblDescription.textColor = .white
        v.addSubview(lblDescription)
        lblDescription.snp.makeConstraints { (make) in
            make.top.equalTo(icon.snp.bottomMargin).offset(10)
            make.centerX.equalTo(v.snp.centerX)
            make.width.greaterThanOrEqualTo(0)
            make.height.greaterThanOrEqualTo(0)
        }
        
        lblDescription.text = R.string_Onboard_Input_OTP_Resend_Success()
        v.isHidden = true
        v.alpha = 0
        return v
    }
    
    fileprivate func showPopupResend() {
        popUpResend.isHidden = false
        UIView.animateKeyframes(withDuration: 1.4, delay: 0, options: .calculationModeLinear, animations: {[weak self] in
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.3, animations: {
                self?.popUpResend.alpha = 1
            })
            
            UIView.addKeyframe(withRelativeStartTime: 1, relativeDuration: 0.4, animations: {
                self?.popUpResend.alpha = 0
            })
        }) { [weak self](_) in
            self?.popUpResend.isHidden = true
        }
        
    }
    
    func time(from interval: TimeInterval) -> Time{
        let nDate = Date(timeInterval: interval, since: dateCheck)
        let components = calendar.dateComponents([.minute, .second], from: dateCheck, to: nDate)
        
        let m = components.minute ?? 0
        let s = components.second ?? 0
        return Time(m, s)
    }
    
    
    fileprivate func createTimer() {
        self.btnResend.isHidden = true
        self.lblTimer.isHidden = false
        dateCheck = Date()
        let nT = self.time(from: timeRefresh)
        self.lblTimer.text = String(format: R.string_Onboard_Input_OTP_Refresh_Counting(), nT.minute, nT.seconds)
        self.timer = Observable<Int>.interval(1, scheduler: MainScheduler.instance).subscribe(onNext: {[weak self] (idx) in
            guard let wSelf = self else {
                return
            }
            guard idx < Int(wSelf.timeRefresh) else {
                wSelf.resetTimer()
                return
            }
            let t = wSelf.time(from: wSelf.timeRefresh - TimeInterval(idx))
            let text = String(format: R.string_Onboard_Input_OTP_Refresh_Counting(), t.minute, t.seconds)
            wSelf.lblTimer.text = text
            
            }, onDisposed: {[weak self] in
                self?.btnResend.isHidden = false
                self?.lblTimer.isHidden = true
        })
        
    }
    
    fileprivate func resetTimer() {
        self.timer?.dispose()
        self.timer = nil
    }
    
    fileprivate func showAlertMoveToLogin() {
        ZPDialogView.showDialog(with: DialogTypeWarning, title: R.string_Dialog_Title_Notification() , message: R.string_Onboard_Input_OTP_Over_Limit(), buttonTitles: [R.string_ButtonLabel_OK()]) { [weak self](idx) in
            
            // Go to Login
            // Copy handler using for next login
            guard let navi = self?.navigationController as? ZPNavigationCustomStatusViewController, let handler = navi.completionHandler else {
                return
            }
            let login = ZPLoginViewController.loginViewWihtCompleteHande(handler)
            UIApplication.shared.delegate?.window??.rootViewController = login
        }
    }
    
    
    fileprivate func showAlertBack() {
        let message = String(format: R.string_Onboard_Input_OTP_Dialog_Message(),phone ?? "" )
        ZPDialogView.showDialog(with: DialogTypeWarning, title: R.string_Dialog_Confirm() , message: message , buttonTitles: [R.string_ButtonLabel_OK(), R.string_ButtonLabel_Cancel()]) { [weak self](idx) in
            if (idx == 0) {
                self?.navigationController?.popViewController(animated: false)
            }else {
                self?.input?.becomeFirstResponder()
            }
        }
    }
    
    fileprivate func checkOTP() {
        if NetworkState.sharedInstance().isReachable == false {
            ZPDialogView.showDialog(with: DialogTypeNoInternet, title: R.string_Dialog_Warning(), message: R.string_NetworkError_NoConnectionMessage(), buttonTitles:[R.string_ButtonLabel_Close()], handler: nil)
            return
        }
        ZPAppFactory.sharedInstance().showHUDAdded(to: self.view)
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.onboarding_submit_otp)
        //let navi = self.navigationController as? ZPNavigationCustomStatusViewController
         self.authenPhone(self.textField.text).trackActivity(activityTracking).subscribe(onNext: {[weak self] r in
            // go to React Native
            DispatchQueue.main.async {
                guard let wSelf = self else {
                    return
                }
                ZPAppFactory.sharedInstance().hideAllHUDs(for: wSelf.view)
                wSelf.textField.resignFirstResponder()
                if let response = r as? [String : Any], let userLoginData = ZPUserLoginDataSwift.fromDic(response) {
                    userLoginData.isClearing = true
                    LoginManagerSwift.sharedInstance.handleUserLoginData(userLoginData)
                    wSelf.reactAppRouter.showAccountClearing(from: wSelf)
                }
            }
            }, onError: { [weak self](e) in
                DispatchQueue.main.async {
                    ZPAppFactory.sharedInstance().hideAllHUDs(for: self?.view)
                    self?.textField.text = ""
                    if (e.errorCode()) == ZALOPAY_ERRORCODE_OVER_OTP.rawValue {
                        let clearingMaxErrorPinViewController = self?.storyboard?.instantiateViewController(withIdentifier: "ClearingMaxErrorPinViewController") as! ClearingMaxErrorPinViewController
                        
                        clearingMaxErrorPinViewController.maxErrorType = .MaxErrorTypeOTP; self?.navigationController?.pushViewController(clearingMaxErrorPinViewController, animated: true)
                        return
                    }
                    
                    self?.trackErrorCode.value = e.errorCode()
                    self?.isCorrect = false
                    var message: String
                    if e.errorCode() == NSURLErrorNotConnectedToInternet {
                        message = R.string_Home_InternetConnectionError()
                    }else {
                        message = e.userInfoData()["returnmessage"] as? String ?? ""
                    }
                    self?.lblWarning.text = message
                }
                
                
        }).disposed(by: disposeBag)
    }
    
    func resendPhone() {
        if NetworkState.sharedInstance().isReachable == false {
            ZPDialogView.showDialog(with: DialogTypeNoInternet, title: R.string_Dialog_Warning(), message: R.string_NetworkError_NoConnectionMessage(), buttonTitles:[R.string_ButtonLabel_Close()], handler: nil)
            return
        }
        ZPAppFactory.sharedInstance().showHUDAdded(to: self.view)
        NetworkManager.sharedInstance().login(withPhone: self.phone, pin: self.password).subscribeNext({[weak self](_) in
            DispatchQueue.main.async {
                ZPAppFactory.sharedInstance().hideAllHUDs(for: self?.view)
                self?.showPopupResend()
                self?.numberResend.value += 1
            }
            
        }, error:({[weak self](error) in
            DispatchQueue.main.async {
                ZPAppFactory.sharedInstance().hideAllHUDs(for: self?.view)
                if (error?.errorCode())! == ZALOPAY_ERRORCODE_OVER_OTP.rawValue {
                    let clearingMaxErrorPinViewController = self?.storyboard?.instantiateViewController(withIdentifier: "ClearingMaxErrorPinViewController") as! ClearingMaxErrorPinViewController
                    
                    clearingMaxErrorPinViewController.maxErrorType = .MaxErrorTypeChangeOTP; self?.navigationController?.pushViewController(clearingMaxErrorPinViewController, animated: true)
                    return
                }
                self?.isCorrect = false
                var message: String
                if error?.errorCode() == NSURLErrorNotConnectedToInternet {
                    message = R.string_Home_InternetConnectionError()
                }else {
                    message = error?.userInfoData()["returnmessage"] as? String ?? ""
                }
                self?.lblWarning.text = message
            }
            
        }))
    }
    
}

extension ClearingInputSMSViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard canInput.value else {
            return false
        }
        
        guard let t = textField.text else {
            return true
        }
        
        let nStr = (t as NSString).replacingCharacters(in: range, with: string)
        let next = nStr.count <= 6
        return next
    }
}

// MARK: - request
extension ClearingInputSMSViewController: SendPhoneProtocol {
    func authenPhone(_ otp: String?) -> Observable<NSDictionary?> {
        guard let otp = otp else {
            return Observable.empty()
        }
        let signal = NetworkManager.sharedInstance().authenLogin(byPhone: self.phone, andOTP: otp)
        return Observable.eventLoading(with: event(from: signal) { $0 as? NSDictionary })
    }
}
