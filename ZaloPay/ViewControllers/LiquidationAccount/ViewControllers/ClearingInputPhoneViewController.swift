//
//  ClearingInputPhoneViewController.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 10/11/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import ZaloPayAnalyticsSwift
import ZaloPayConfig

final class ClearingInputPhoneViewController: BaseInputViewController {
    @IBOutlet weak var textField: UITextField! {
        didSet{
            self.input = textField
            guard let t = textField else {
                return
            }
            t.isUserInteractionEnabled = false
        }
    }
    
    @IBOutlet weak var btnContinue: UIButton!
    fileprivate var patterns: [String] = []
    @objc var phoneNumber:String = ""
    @IBOutlet var naviView: UIView!
    @IBOutlet var naviViewSmall: UIView!
    override func viewDidLoad() {
        //LoginManager.sharedInstance().clearLoginData()
        self.loadConfigPhone()
        let isSmallView = UIScreen.main.bounds.height < 603
        let vNavi: UIView = isSmallView.not ? naviView : naviViewSmall
        
        vNavi.frame = {
            var f = vNavi.frame
            f.size.width = UIScreen.main.bounds.width
            return f
        }()
        
        vNavi.tag = 472
        self.navigationController?.navigationBar.addSubview(vNavi)
        super.viewDidLoad()
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.onboarding_submit_phone_number)
    }
    
    override func setupButton() {
        //nextios
        let icon: String = UILabel.iconCode(withName: "general_arrowright") ?? ""
        let titleNormal = String(format: "%@ %@", R.string_Onboard_Input_Phone_Next(), "\(icon)")
        let r = (titleNormal as NSString).range(of: icon)
        let attN = NSMutableAttributedString(string: titleNormal, attributes: [NSAttributedStringKey.foregroundColor : UIColor.zaloBase()])
        let attD = NSMutableAttributedString(string: titleNormal, attributes: [NSAttributedStringKey.foregroundColor :  UIColor(hexValue: 0xc7cad3)])
        
        attN.addAttributes([NSAttributedStringKey.font : UIFont.zaloPay(withSize: 15)], range: r)
        attD.addAttributes([NSAttributedStringKey.font : UIFont.zaloPay(withSize: 15)], range: r)
        
        let paragraph = NSMutableParagraphStyle()
        paragraph.lineHeightMultiple = 20
        paragraph.alignment = .justified
        
        attN.addAttributes([NSAttributedStringKey.paragraphStyle : paragraph], range: r)
        attD.addAttributes([NSAttributedStringKey.paragraphStyle : paragraph], range: r)
        
        btnContinue.setAttributedTitle(attN, for: .normal)
        btnContinue.setAttributedTitle(attD, for: .disabled)
        
    }
    
    func setBorderColor(_ enabled: Bool) {
        btnContinue.layer.borderColor = (enabled ? UIColor.zaloBase() : UIColor.zp_gray()).cgColor
    }
    
    override func setupDisplay() {
        super.setupDisplay()
        self.lblTitle.text = R.string_Onboard_Input_Phone_Title()
        self.lblNote.text = R.string_Clearing_login_text()
        self.lblWarning.text = ""
        self.isCorrect = true
    }
    
    func loadConfigPhone() {
//        let dic_phone_format = ApplicationState.getDictionaryConfig(fromDic: "general", andKey: "phone_format", andDefault: [:])
//        let phonePatterns = dic_phone_format?["patterns"] as? [String]
//        patterns = phonePatterns ?? []
        
        let dic_phone_format = ZPApplicationConfig.getGeneralConfig()?.getPhoneFormat()
        let phonePatterns = dic_phone_format?.getPatterns()
        patterns = phonePatterns ?? []
    }
    
    override func setupEvent() {
        super.setupEvent()
        self.btnContinue.rx.controlEvent(.touchUpInside).subscribe(onNext: { [weak self](_) in
            self?.register()
        }).disposed(by: disposeBag)
    }
    
    func register() {
       self.performSegue(withIdentifier: "ShowPassword", sender: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let phoneString = self.phoneNumber as NSString
        self.textField.text = phoneString.removePhoneCountryCode()
        self.lblWarning.text = ""
        self.backTop?.isHidden = true
        super.viewWillAppear(animated)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let vc = segue.destination as? ClearingInputPasswordViewController else {
            return
        }
        vc.phoneNumber = self.textField.text ?? ""
    }
}

