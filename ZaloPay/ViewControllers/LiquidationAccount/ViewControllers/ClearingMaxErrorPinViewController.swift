//
//  ClearingMaxErrorPinViewController.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 10/13/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
enum MaxErrorType: Int {
    case MaxErrorTypePin = 0
    case MaxErrorTypeOTP = 1
    case MaxErrorTypeChangeOTP = 2
}
class ClearingMaxErrorPinViewController: UIViewController {

    @IBOutlet weak var imageViewFaild: ZPIconFontImageView!
    @IBOutlet weak var lblContact: UILabel!
    @IBOutlet weak var lblMaxError: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    @IBAction func btnCloseTouch(_ sender: Any) {
        exit(0)
    }
    var maxErrorType:MaxErrorType!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.navigationItem.leftBarButtonItem = nil
        self.setupLblContact()
        self.setupBtnClose()
        self.setupImageFail()
        self.setupLabelMaxError()
    }
    func setupLabelMaxError() {
        var msg = ""
        switch self.maxErrorType {
        case .MaxErrorTypePin:
            msg = R.string_Clearing_over_wrong_pin()
            break
        case .MaxErrorTypeOTP:
            msg = R.string_Clearing_over_wrong_otp()
            break
        case .MaxErrorTypeChangeOTP:
            msg = R.string_Clearing_over_change_otp()
            break
        default: break
        }
        self.lblMaxError.text = msg
    }
    func setupLblContact() {
        let attributedString = NSMutableAttributedString(string: R.string_Clearing_call_CS_text())
        attributedString.addAttributes([
            NSAttributedStringKey.foregroundColor: UIColor(red: 15.0 / 255.0, green: 121.0 / 255.0, blue: 222.0 / 255.0, alpha: 1.0),
            NSAttributedStringKey.kern: -0.2
            ], range: NSRange(location: 41, length: 13))
        self.lblContact.attributedText = attributedString;
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ClearingMaxErrorPinViewController.callPhone))
        self.lblContact.addGestureRecognizer(tapGesture)
    }
    func setupBtnClose() {
        self.btnClose.roundRect(4, borderColor: UIColor.zaloBase(), borderWidth: 1)
    }
    func setupImageFail()  {
        self.imageViewFaild.setIconFont("pay_fail")
        self.imageViewFaild.setIconColor(UIColor.error())
    }
    @objc func callPhone() {
        if let url = URL(string: "tel://1900545436"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
