//
//  LiquidationInputPasswordViewController.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 10/11/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import CocoaLumberjackSwift



final class ClearingInputPasswordViewController: BaseInputViewController{
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var heightBtn: NSLayoutConstraint!
    
    @IBOutlet var tapGesture: UITapGestureRecognizer!
    fileprivate var _password: String = ""
    fileprivate var currentInput: Variable<String> = Variable("")
    fileprivate let state: Variable<PasswordState> = Variable(.begin)
    
    var phoneNumber:String?
    
    lazy var textField: UITextField = {
        let tField = UITextField(frame: .zero)
        self.view.addSubview(tField)
        tField.delegate = self
        tField.keyboardType = .numberPad
        return tField
    }()
    
    override func viewDidLoad() {
        
        currentDate = Date()
        super.viewDidLoad()
        self.input = textField
        collectionView.allowsMultipleSelection = true
        trackScreen(Screen_Onboarding)
        trackEvent(EventAction.onboarding_input_password.rawValue, value: nil)
    }
    
    override func setupButton() {
        self.roundButton(self.btnBack)
        self.showBack(will: false)
    }
    
    func showBack(will show:Bool) {
        self.btnBack.isHidden = show.not
        let nextH = show ? hBackBtn : 0
        let title = show ? "Quay lại" : ""
        heightBtn.constant = nextH
        self.btnBack.isEnabled = show
        self.btnBack.setTitle(title, for: .normal)
    }
    
    override func setupEvent() {
        super.setupEvent()
        tapGesture.rx
            .event
            .filter({[weak self] _ in self?.textField.isFirstResponder == false })
            .subscribe(onNext: { [weak self](_) in
                self?.textField.becomeFirstResponder()
            }).disposed(by: disposeBag)
        // same collection datasource
        Observable.just(0..<6)
            .bind(to: self.collectionView.rx.items(cellIdentifier: "OnboardInputPasswordCell", cellType: OnboardInputPasswordCell.self))
            {
                $0.2.isSelected = $0.0 < self.currentInput.value.characters.count
            }
            .disposed(by: disposeBag)
        
        //gan text vaof current input
        textField.rx.text.map { $0 ?? ""}.bind(to: currentInput).disposed(by: disposeBag)
        
        // same should change character
        currentInput.asDriver().drive(onNext: { [weak self] in
            let total = $0.characters.count
            (0..<6).map({ IndexPath(item: $0, section: 0) }).forEach({
                guard let cell = self?.collectionView.cellForItem(at: $0) as? OnboardInputPasswordCell else {
                    return
                }
                cell.isSelected = $0.row < total
            })
        }).disposed(by: disposeBag)
        
        
        // Track UI from state
        state.asDriver().drive(onNext: { [weak self] _ in
            self?.updateStateUI()
        }).disposed(by: disposeBag)
        
        // Next state
        currentInput.asDriver().filter({
            return $0.characters.count == 6
        }).debounce(0.2).drive(onNext: { [weak self](s) in
            guard let wSelf = self else { return }
            let currentState = wSelf.state.value
            switch currentState {
            case .begin:
                wSelf.resetPasswordInput()
                // save new pass
                wSelf._password = s
                // set next state
                wSelf.state.value = .identify
            case .identify:
                // Vertify pass
                let isCorrect = s == wSelf._password
                // Correc -> end move to input phone
                if isCorrect {
                    NetworkManager.sharedInstance().login(withPhone: wSelf.phoneNumber, pin: wSelf._password).subscribeNext({(_) in
                        wSelf.state.value = .end
                    }, error:({(error) in
                        wSelf.resetPasswordInput()
                        
                        wSelf.state.value = .begin
                        
                        var message: String
                        if error?.errorCode() == NSURLErrorNotConnectedToInternet {
                            message = R.string_Home_InternetConnectionError()
                        }else {
                            message = error?.userInfoData()["returnmessage"] as? String ?? ""
                        }
                        
                        self?.lblWarning.text = message
                        if error?.errorCode() == -117 {
                            
                            let liquidationMaxErrorPinViewController = wSelf.storyboard?.instantiateViewController(withIdentifier: "LiquidationMaxErrorPinViewController") as! LiquidationMaxErrorPinViewController
                            wSelf.navigationController?.pushViewController(liquidationMaxErrorPinViewController, animated: true)
                        }
                        
                    }))
                    
                }else {
                    wSelf.lblWarning.text = R.string_Onboard_Confirm_Password_Error()
                    wSelf.lblWarning.textColor = UIColor.zp_red()
                    wSelf.resetPasswordInput()
                }
                
            default:
                break
            }
        }).disposed(by: disposeBag)
        
        // end
        state.asDriver().filter({ $0 == .end }).drive(onNext: { [weak self](_) in
            self?.performSegue(withIdentifier: "ShowSMS", sender: self!)
            
        }).disposed(by: disposeBag)
        
        // reset state
        self.eventBack?.bind { [weak self](_) in
            self?._password = ""
            self?.resetPasswordInput()
            self?.state.value = .begin
            }.disposed(by: disposeBag)
    }
    
    func resetPasswordInput() {
        _ = Observable.just("").bind(to: self.textField.rx.text)
        self.currentInput.value = ""
    }
    
    func updateStateUI() {
        let s = state.value
        guard s != .end else {
            return
        }
        self.backTop?.isHidden = (s == .begin)
        lblTitle.text = s.title
        lblNote.text = "Sau khi đăng nhập, vui lòng thanh lý toàn bộ số dư và huỷ liên kết ngân hàng"
        lblWarning.text = s.warning
        isCorrect = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        guard let vc = segue.destination as? LiquidationInputSMSViewController else {
            return
        }
        vc.password = _password
        vc.phone = self.phoneNumber
        
    }
}

extension ClearingInputPasswordViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let t = textField.text else {
            return true
        }
        
        let nStr = (t as NSString).replacingCharacters(in: range, with: string)
        let next = nStr.characters.count <= 6
        return next
    }
}
