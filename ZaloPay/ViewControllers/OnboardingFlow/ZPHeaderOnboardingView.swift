//
//  ZPHeaderOnboardingView.swift
//  ZaloPay
//
//  Created by Dung Vu on 6/12/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayProfile
final class ZPHeaderOnboardingView: UIView {

    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var genderImg: UIImageView!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var calendarImg: UIImageView!
    @IBOutlet weak var lblBirthDay: UILabel!
    
    var backBtn: UIButton? {
        return self.viewWithTag(291) as? UIButton
    }
        
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        self.lblGender.textColor = UIColor.zp_gray()
        self.lblBirthDay.textColor = UIColor.zp_gray()
        
        backBtn?.tintColor = UIColor.zaloBase()
        backBtn?.titleLabel?.font = UIFont.zaloPay(withSize: 18)
        backBtn?.setIconFont("general_backios", for: .normal)
        backBtn?.titleEdgeInsets = UIEdgeInsetsMake(0, 12, 0, 0)
        calendarImg.load(with: "onboarding_birthday", at: 15, using: .zp_gray())
        roundAvatar()
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        ZaloSDKApiWrapper.sharedInstance.loadZaloUserProfile().subscribeNext { [weak self](user) in
            guard let u = user as? ZaloUserSwift else { return }
            self?.fetchAvatar(from: u)
            let gender = u.gender
            self?.lblGender.text = gender.text
            self?.genderImg.load(with: gender.iconFont, at: 15, using: .zp_gray())
            self?.lblBirthDay.text = u.birthDay.string(using: "dd/MM/yyyy")
            self?.lblUserName.text = u.displayName
        }
    }
    
    fileprivate func roundAvatar() {
        avatarImg.layer.cornerRadius = avatarImg.bounds.width / 2
        avatarImg.clipsToBounds = true
        avatarImg.layer.borderWidth = 1.5
        avatarImg.layer.borderColor = UIColor(hexValue: 0x94d7d7).cgColor
    }
    
    fileprivate func fetchAvatar(from user:ZaloUserSwift) {
        let url = URL(string: user.avatar)
        avatarImg.sd_setImage(with: url, placeholderImage: UIImage.defaultAvatar())
    }

}

extension Gender {
    var text: String {
        switch self.rawValue {
        case 1:
            return R.string_MainProfile_Male()
        case 2:
            return R.string_MainProfile_Female()
        default:
            return ""
        }
    }
    
    var iconFont: String {
        switch self.rawValue {
        case 1:
            return "onboarding_male"
        case 2:
            return "onboarding_female"
        default:
            return ""
        }
    }
}
//
//extension UIImage {
//    static func drawIcon(with text: String,
//                         using font: UIFont,
//                         with color: UIColor,
//                         at size: CGSize) -> UIImage?
//    {
//        let att = [NSFontAttributeName : font, NSForegroundColorAttributeName : color]
//        let sT = (text as NSString).size(attributes: att)
//        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
//        guard let context = UIGraphicsGetCurrentContext() else {
//            return nil
//        }
//        context.setFillColor(UIColor.clear.cgColor)
//        let rect = CGRect(x:(size.width - sT.width) / 2, y:(size.height - sT.height) / 2, width: size.width, height: size.height)
//        color.setFill()
//        (text as NSString).draw(in: rect, withAttributes: att)
//        let result = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        return result
//    }
//}
//
//extension UIImageView {
//    func load(with iconFont: String, at size: CGFloat, using color: UIColor = .zaloBase()) {
//        guard let text = UILabel.iconCode(withName: iconFont) else {
//            return
//        }
//        
//        let img = UIImage.drawIcon(with: text, using: UIFont.zaloPay(withSize: size), with: color, at: self.bounds.size)
//        self.image = img?.withRenderingMode(.alwaysOriginal)
//    }
//}

//extension UIImage {
//    static func drawIcon(with text: String,
//                         using font: UIFont,
//                         with color: UIColor,
//                         at size: CGSize) -> UIImage?
//    {
//        let att = [NSAttributedStringKey.font : font, NSAttributedStringKey.foregroundColor : color]
//        let sT = (text as NSString).size(withAttributes: att)
//        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
//        guard let context = UIGraphicsGetCurrentContext() else {
//            return nil
//        }
//        context.setFillColor(UIColor.clear.cgColor)
//        let rect = CGRect(x:(size.width - sT.width) / 2, y:(size.height - sT.height) / 2, width: size.width, height: size.height)
//        color.setFill()
//        (text as NSString).draw(in: rect, withAttributes: att)
//        let result = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        return result
//    }
//}

//extension UIImageView {
//    func load(with iconFont: String, at size: CGFloat, using color: UIColor = .zaloBase()) {
//        guard let text = UILabel.iconCode(withName: iconFont) else {
//            return
//        }
//        
//        let img = UIImage.drawIcon(with: text, using: UIFont.zaloPay(withSize: size), with: color, at: self.bounds.size)
//        self.image = img?.withRenderingMode(.alwaysOriginal)
//    }
//}

