//
//  BaseInputViewController.swift
//  ZaloPay
//
//  Created by Dung Vu on 6/30/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import CocoaLumberjackSwift

let hBackBtn: CGFloat = 44

// Using this to check to return date
var currentDate: Date?
fileprivate let timeReturnLogin: TimeInterval = 3600

class BaseInputViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNote: UILabel!
    @IBOutlet weak var lblWarning: UILabel!
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView?
    @IBOutlet weak var bottomConstrainScrollView: NSLayoutConstraint?
    lazy var activityTracking: ActivityIndicator = ActivityIndicator()
    let canInput: Variable<Bool> = Variable(true)
    
    weak var input: UIResponder?
    
    var isAppear: Bool = true
    var isCorrect: Bool = true {
        didSet{
            lblWarning.textColor = isCorrect ? UIColor.zaloBase() : UIColor.zp_red()
        }
    }
    lazy var isIphone4: Bool = UIScreen.main.bounds.height < 568
    
    var backTop: UIButton? {
        return self.navigationController?.navigationBar.viewWithTag(291) as? UIButton
    }
    
    var eventBack: Observable<Void>? {
        return self.backTop?.rx.controlEvent(.touchUpInside).filter({ [weak self] in self?.isAppear ?? false })
    }
    let eError: PublishSubject<Error> = PublishSubject()
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupEvent()
        setupDisplay()
        setupButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isAppear = true
        self.input?.becomeFirstResponder()
        DDLogDebug("-------\(type(of: self)) viewWillAppear !!!!--------")
        //trackEvent()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.isAppear = false
        input?.resignFirstResponder()
    }
    
    func moveToLogin() {
        // Go to Login
        // Copy handler using for next login
        guard let navi = self.navigationController as? ZPNavigationCustomStatusViewController, let handler = navi.completionHandler else {
            return
        }
        let login = ZPLoginViewController.loginViewWihtCompleteHande(handler)
        UIApplication.shared.delegate?.window??.rootViewController = login
    }
    
    func setupEvent() {
        // Tracking loading input
        activityTracking.asDriver().map({ $0.not }).asDriver().drive(canInput).disposed(by: disposeBag)
//        // Tracking App Open from Multitask
        NotificationCenter.default.rx.notification(Notification.Name.UIApplicationDidBecomeActive).filter({[weak self] _ in self?.isAppear ?? false }).observeOn(MainScheduler.instance).subscribe(onNext: { [weak self](_) in
            if self?.input?.isFirstResponder == false {
                self?.input?.becomeFirstResponder()
            }
        }).disposed(by: disposeBag)
        
        // Track Error Code
        eError.observeOn(MainScheduler.asyncInstance)
            .filter({ $0.errorCode() == Int(ZALOPAY_ERRORCODE_ZALO_LOGIN_FAIL.rawValue) })
            .subscribe(onNext: { [weak self](e) in
            self?.moveToLogin()
        }).disposed(by: disposeBag)
        
        // Tracking app active and check time -> Login controller
        NotificationCenter.default.rx.notification(Notification.Name.UIApplicationDidBecomeActive).filter({[weak self] _ in
            guard let wSelf = self , wSelf.isAppear else {
                return false
            }
            // Tracking if have currentDate
            guard let date = currentDate else {
                return false
            }
            // Check
            let time = Date().timeIntervalSince(date)
            
            return time >= timeReturnLogin
        }).subscribe(onNext: { [weak self](_) in
            self?.moveToLogin()
        }).disposed(by: disposeBag)
        
        // Handler event hide keyboard
        NotificationCenter.default.rx.notification(Notification.Name.UIKeyboardWillHide).filter({ [weak self] _ in return self?.isAppear ?? false }).subscribe(onNext: { [weak self](n) in
            
            guard let info = n.userInfo , let wSelf = self else {
                return
            }
            
            let duration = info[UIKeyboardAnimationDurationUserInfoKey] as? TimeInterval ?? 0
            wSelf.handlerShowKeyboard(duration: duration, height: 0)
            UIView.animate(withDuration: duration, animations: {
                wSelf.bottomConstrainScrollView?.constant = 0
                wSelf.view.layoutIfNeeded()
            }, completion: { (completion) in })
            
        }).disposed(by: disposeBag)
        
        // Show keyboard
        NotificationCenter.default.rx.notification(Notification.Name.UIKeyboardWillShow).filter({ [weak self] _ in return self?.isAppear ?? false }).subscribe(onNext: { [weak self](n) in
            
            guard let info = n.userInfo , let wSelf = self else {
                return
            }
            
            let duration = info[UIKeyboardAnimationDurationUserInfoKey] as? TimeInterval ?? 0
            let f = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue ?? .zero
            wSelf.bottomConstrainScrollView?.constant = f.height
            wSelf.view.layoutIfNeeded()
            wSelf.handlerShowKeyboard(duration: duration, height: f.height)
            wSelf.scroll(to: wSelf.containerView, using: duration)
        }).disposed(by: disposeBag)
    }
    
    func handlerShowKeyboard(duration t:TimeInterval,height h:CGFloat) {}
    
    func scroll(to view: UIView, using duration: TimeInterval) {
        var c = view.convert(view.bounds, to: self.scrollView)
        
        if self.isIphone4 {
            c.origin.y -= 25
            c.size.height -= 25
        }
        UIView.animate(withDuration: duration, animations: {
            self.scrollView?.scrollRectToVisible(c, animated: false)
        }, completion: { (completion) in })
    }
    
    func configShowKeyboardForClearing() {
        NotificationCenter.default.rx.notification(Notification.Name.UIKeyboardWillShow).filter({ [weak self] _ in return self?.isAppear ?? false }).subscribe(onNext: { [weak self](n) in
            
            guard let info = n.userInfo , let wSelf = self else {
                return
            }
            
            let duration = info[UIKeyboardAnimationDurationUserInfoKey] as? TimeInterval ?? 0
            let f = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue ?? .zero
            wSelf.bottomConstrainScrollView?.constant = f.height
            wSelf.view.layoutIfNeeded()
            var c = wSelf.containerView.bounds
            if wSelf.isIphone4 {
                c.origin.y -= 25
                c.size.height -= 25
            }
            UIView.animate(withDuration: duration, animations: {
                wSelf.scrollView?.scrollRectToVisible(c, animated: false)
            }, completion: { (completion) in })
            
        }).disposed(by: disposeBag)
    }
    
    func roundButton(_ button: UIButton) {
        button.layer.cornerRadius = hBackBtn / 2
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.zaloBase().cgColor
        button.clipsToBounds = true
        button.setTitleColor(UIColor.zaloBase(), for: .normal)
    }
    
    func setupButton() {}
    func setupDisplay() {
        if #available(iOS 11, *) {
            let isSmallView = UIScreen.main.bounds.height < 603
            let h: CGFloat = isSmallView.not ? 159 : 67
            self.additionalSafeAreaInsets.top = h - 44
        }
        
        self.lblNote.textColor = UIColor.zp_gray()
        self.adjustConstraints()
    }
    
    func isAdjustedConstraintsIndentify(_ identify: String?) -> Bool {
        return identify == "topTitle" ||
            identify == "topNote" ||
            identify == "topWarning" ||
            identify == "topButton"
    }
    
    func adjustConstraints() {
        // Iphone 4 -> force size
        if isIphone4 {
            self.containerView.constraints.filter({
                let identify = $0.identifier
                return isAdjustedConstraintsIndentify(identify)
            }).forEach({ $0.constant = 2})
        }
    }
    
    deinit {
        DDLogDebug("-----------\(type(of: self)) dealloc !!!!-------------")
    }
}
