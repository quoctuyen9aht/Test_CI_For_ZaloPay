//
//  OnboardInputPasswordViewController.swift
//  ZaloPay
//
//  Created by Dung Vu on 6/12/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import CocoaLumberjackSwift
import ZaloPayAnalyticsSwift

fileprivate let kNextScreenName = "showPhoneInput"

class OnboardInputPasswordCell: UICollectionViewCell {
    lazy var vDisplay: UIView = {
        let v = UIView(frame: self.contentView.bounds)
        self.contentView.addSubview(v)
        return v
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
        vDisplay.layer.cornerRadius = vDisplay.bounds.width / 2
        vDisplay.clipsToBounds = true
        vDisplay.layer.borderWidth = 1;
        vDisplay.layer.borderColor = UIColor.zaloBase().cgColor
    }
    
    override var isSelected: Bool {
        didSet{
            updateSelect()
        }
    }
    
    func updateSelect() {
        vDisplay.backgroundColor = isSelected.not ? .white : .zaloBase()
    }
}


enum PasswordState: Int {
    case begin
    case identify
    case end
    
    var title: String {
        switch self {
        case .begin:
            return R.string_Onboard_Create_Password_Title()
        case .identify:
            return R.string_Onboard_Confirm_Password_Title()
        default:
            return ""
        }
    }
    
    var note: String {
        switch self {
        case .begin:
            return R.string_Onboard_Create_Password_Note()
        case .identify:
            return R.string_Onboard_Confirm_Password_Note()
        default:
            return ""
        }
    }
    
    var warning: String {
        switch self {
        case .begin:
            return ""
        case .identify:
            return ""
        default:
            return ""
        }
    }
    var trackScreenName: String {
        switch self {
        case .begin:
            return ZPTrackerScreen.Screen_iOS_Onboarding_InputPaymentPasscode
        case .identify:
            return ZPTrackerScreen.Screen_iOS_Onboarding_ConfirmPaymentPasscode
        default:
            return ""
        }
    }
}

class OnboardInputPasswordViewController: BaseInputViewController{

    @IBOutlet var naviView: UIView!
    @IBOutlet var naviViewSmall: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var heightBtn: NSLayoutConstraint!
    
    @IBOutlet var tapGesture: UITapGestureRecognizer!
    var _password: String = ""
    var currentInput: Variable<String> = Variable("")
    fileprivate let state: Variable<PasswordState> = Variable(.begin)
    
    lazy var textField: UITextField = {
       let tField = UITextField(frame: .zero)
        self.view.addSubview(tField)
        tField.delegate = self
        tField.keyboardType = .numberPad
        return tField
    }()
    
    override func viewDidLoad() {
        prepareNaviView()
        currentDate = Date()
        super.viewDidLoad()
        self.input = textField
        collectionView.allowsMultipleSelection = true
    }
    
    func prepareNaviView() {
        let isSmallView = UIScreen.main.bounds.height < 603
        let vNavi: UIView = isSmallView.not ? naviView : naviViewSmall
        
        vNavi.frame = {
            var f = vNavi.frame
            f.size.width = UIScreen.main.bounds.width
            return f
        }()
        
        vNavi.tag = 472
        self.navigationController?.navigationBar.addSubview(vNavi)
    }
    
    override func setupButton() {
        self.roundButton(self.btnBack)
        self.showBack(will: false)
    }
    
    func showBack(will show:Bool) {
        self.btnBack.isHidden = show.not
        let nextH = show ? hBackBtn : 0
        let title = show ? "Quay lại" : ""
        heightBtn.constant = nextH
        self.btnBack.isEnabled = show
        self.btnBack.setTitle(title, for: .normal)
    }
    
    func changedPasswordInput(_ pswd: String) {
        let total = pswd.count
        (0..<6).map({ IndexPath(item: $0, section: 0) }).forEach({
            guard let cell = self.collectionView.cellForItem(at: $0) as? OnboardInputPasswordCell else {
                return
            }
            cell.isSelected = $0.row < total
        })
    }
    
    func completedInputPassword() {
        self.performSegue(withIdentifier: kNextScreenName, sender: nil)
    }
    
    func trackEventAnalytic(_ eventId: ZPAnalyticEventAction) {
        ZPTrackingHelper.shared().trackEvent(eventId)
    }
    
    func trackScreenAnalytic(_ nameScreen: String) {
        ZPTrackingHelper.shared().trackScreen(withName: nameScreen)
    }
    
    override func setupEvent() {
        super.setupEvent()
        tapGesture.rx
            .event
            .filter({[weak self] _ in self?.textField.isFirstResponder == false })
            .subscribe(onNext: { [weak self](_) in
            self?.textField.becomeFirstResponder()
        }).disposed(by: disposeBag)
        
        Observable.just(0..<6)
            .bind(to: self.collectionView.rx.items(cellIdentifier: "OnboardInputPasswordCell", cellType: OnboardInputPasswordCell.self))
            {
                $2.isSelected = $0 < self.currentInput.value.count
            }
            .disposed(by: disposeBag)
        
        textField.rx.text.map { $0 ?? ""}.bind(to: currentInput).disposed(by: disposeBag)
        currentInput.asDriver().drive(onNext: { [weak self] (x) in
            self?.changedPasswordInput(x)
        }).disposed(by: disposeBag)
        
        // Track UI from state
        state.asDriver().drive(onNext: { [weak self]  in
            self?.updateStateUI()
            let name = $0.trackScreenName
            guard name.count > 0 else {
                return
            }
            self?.trackScreenAnalytic(name)
            self?.trackEventAnalytic(.onboarding_input_payment_passcode)
        }).disposed(by: disposeBag)
        
        // Next state
        currentInput.asDriver().filter({
            return $0.count == 6
        }).debounce(0.2).drive(onNext: { [weak self](s) in
            guard let wSelf = self else { return }
            let currentState = wSelf.state.value
            switch currentState {
            case .begin:
                 wSelf.resetPasswordInput()
                // save new pass
                wSelf._password = s
                // set next state
                wSelf.state.value = .identify
            case .identify:
                // Vertify pass
                let isCorrect = s == wSelf._password
                // Correc -> end move to input phone
                if isCorrect {
                    wSelf.state.value = .end
                    wSelf.trackEventAnalytic(.onboarding_confirm_payment_passcode_success)
                }else {
                    wSelf.trackEventAnalytic(.onboarding_confirm_payment_passcode_fail)
                    wSelf.lblWarning.text = R.string_Onboard_Confirm_Password_Error()
                    wSelf.lblWarning.textColor = UIColor.zp_red()
                    wSelf.resetPasswordInput()
                }
                
            default:
                break
            }
        }).disposed(by: disposeBag)
        
        // end
        state.asDriver().filter({ $0 == .end }).drive(onNext: { [weak self](_) in
            self?.completedInputPassword()
        }).disposed(by: disposeBag)
        
        // reset state
        self.eventBack?.bind { [weak self](_) in
            self?.trackEventAnalytic(.onboarding_confirm_payment_passcode_back)
            self?.resetAll()
        }.disposed(by: disposeBag)
    }
    
    func resetAll() {
        self._password = ""
        self.resetPasswordInput()
        self.state.value = .begin
    }
    
    func resetPasswordInput() {
        _ = Observable.just("").bind(to: self.textField.rx.text)
        self.currentInput.value = ""
    }
    
    func updateUI(with pswdState: PasswordState) {
        self.backTop?.isHidden = (pswdState == .begin)
        lblTitle.text = pswdState.title
        lblNote.text = pswdState.note
        lblWarning.text = pswdState.warning
    }
    
    func updateStateUI() {
        let s = state.value
        guard s != .end else {
            return
        }
        updateUI(with: s)
        isCorrect = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let vc = segue.destination as? OnboardInputPhoneViewController else {
            return
        }
        vc.password = _password
    }
}

extension OnboardInputPasswordViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let t = textField.text else {
            return true
        }

        let nStr = (t as NSString).replacingCharacters(in: range, with: string)
        let next = nStr.count <= 6
        return next
    }
}



