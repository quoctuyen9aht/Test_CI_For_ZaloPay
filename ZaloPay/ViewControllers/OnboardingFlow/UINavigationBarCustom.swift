//
//  UINavigationBarCustom.swift
//  ZaloPay
//
//  Created by Dung Vu on 6/12/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit

final class UINavigationBarCustom: UINavigationBar {
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let isSmallView = UIScreen.main.bounds.height < 603
        let h: CGFloat = isSmallView.not ? 159 : 67
        return CGSize(width: UIScreen.main.bounds.width, height: h)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        guard let v = self.viewWithTag(472) else {
            return
        }
        
        self.subviews.forEach({
            guard $0 !== v else {
                return
            }
            self.insertSubview($0, belowSubview: v)
            
        })
        
    }
}


