//
//  ZPKYCOTPCollectionViewCell.swift
//  ZaloPay
//
//  Created by Dung Vu on 4/19/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit

final class ZPKYCOTPCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblCharacter: UILabel?
    @IBOutlet weak var vLine: UIView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblCharacter?.textColor = UIColor.defaultText()
        vLine?.backgroundColor = UIColor.subText()
    }
}
