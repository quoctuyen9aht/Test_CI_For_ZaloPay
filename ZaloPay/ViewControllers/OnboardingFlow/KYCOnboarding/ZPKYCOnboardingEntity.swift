//
//  ZPKYCOnboardingEntity.swift
//  ZaloPay
//
//  Created by Dung Vu on 4/18/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ObjectMapper

struct ZPKYCOnboardingEntity: Mappable {
    var phone: String?
    var gender: Int?
    var fullName: String?
    var dob: Date?
    var idType: Int?
    var idValue: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        phone <- map["zaloPhone"]
        gender <- map["gender"]
        fullName <- map["fullName"]
        dob <- (map["dob"], DateTransform())
        idType <- map["idType"]
        idValue <- map["idValue"]
    }
}
