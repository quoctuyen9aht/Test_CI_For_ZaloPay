//
//  ZPKYCSMSOnboardingViewController.swift
//  ZaloPay
//
//  Created by Dung Vu on 4/18/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import ZaloPayAnalyticsSwift

fileprivate let kNextScreenName = "inputPassword"

class ZPKYCSMSOnboardingViewController: OnboardInputSMSViewController, SendUserInfoProtocol {
    var entity: ZPKYCOnboardingEntity?
    @IBOutlet weak var collectionView: UICollectionView?
    @IBOutlet weak var gesture: UIGestureRecognizer?
    private lazy var strings: Variable<[String?]> = Variable([String?].init(repeating: nil, count: 6))
    
    override func viewDidLoad() {
        self.phone = entity?.phone ?? ""
        assert(entity?.phone != nil, "Wrong")
        self.password = ""
        currentDate = Date()
        super.viewDidLoad()
        setupNavigation()
    }
    
    override func screenName() -> String! {
        return ZPTrackerScreen.Screen_iOS_OnboardingKYC_OTP
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ZPTrackingHelper.shared().trackScreen(with: self)
    }
    
    func setupNavigation() {
        let backButton = UIButton(type: .custom)
        backButton.isMultipleTouchEnabled = false
        backButton.tintColor = .white
        backButton.setTitle(UILabel.iconCode(withName: "general_backios"), for: .normal)
        backButton.titleLabel?.font = UIFont.iconFont(withSize: 20)
        if #available(iOS 11, *) {
            backButton.contentEdgeInsets = UIEdgeInsetsMake(0, -12, 0, 0)
        }
        backButton.frame = CGRect(origin: .zero, size: CGSize(width: 44, height: 44))
        backButton.contentHorizontalAlignment = .left
        
        backButton.rx.tap.bind { [weak self](_) in
            // Show alert
            self?.navigationController?.popViewController(animated: true)
            }.disposed(by: disposeBag)
        
        let barButton = UIBarButtonItem(customView: backButton)
        let negativeSeparator = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        negativeSeparator.width = -12
        self.navigationItem.leftBarButtonItems = [negativeSeparator, barButton]
        self.navigationItem.title = R.string_Onboard_Input_OTP_Title().capitalizingFirstLetter()
    }
    
    override func isAdjustedConstraintsIndentify(_ identify: String?) -> Bool {
        return super.isAdjustedConstraintsIndentify(identify) ||
            identify == "topTimer"
    }
    
    override func setupDisplay() {
        self.adjustConstraints()
        self.textField?.delegate = self
        self.lblTitle.textColor = UIColor.subText()
        let  phonenumber = (self.phone ?? "") as NSString
        let formatedPhone = phonenumber.formatPhoneNumber() as String
        self.lblTitle.text = String(format: R.string_Onboard_KYC_OTP_Send_To_Phone(), formatedPhone)
        self.isCorrect = true
    }
    
    override func setupButton() {
        self.btnNext.setBackgroundColor(UIColor.zaloBase(), for: .normal)
        self.btnNext.setBackgroundColor(UIColor.disableButton(), for: .disabled)
        self.btnNext.layer.cornerRadius = 3
        self.btnNext.clipsToBounds = true
        
        self.lblTimer.textColor = UIColor.zaloBase()
        setupBtnResend()
        
        if isIphone4 {
            self.btnNext.snp.updateConstraints {
                $0.height.equalTo(44)
            }
        }
    }
    
    override func trackEventAnalytic(_ eventId: ZPAnalyticEventAction) {
        var eventId = eventId
        switch eventId {
        case .onboarding_resend_otp:
            eventId = .onboardingkyc_resend_otp
        case .onboarding_input_otp_success:
            eventId = .onboardingkyc_input_otp_success
        case .onboarding_input_otp_fail:
            eventId = .onboardingkyc_input_otp_fail
        case .onboarding_submit_otp:
            eventId = .onboardingkyc_submit_otp
        default:
            break
        }
        super.trackEventAnalytic(eventId)
    }

    override func handlerShowKeyboard(duration t: TimeInterval, height h: CGFloat) {
        let translation = CGAffineTransform.init(translationX: 0, y: -h)
        UIView.animate(withDuration: t, delay: 0, options: [.curveEaseInOut, .beginFromCurrentState], animations: {
            self.btnNext.transform = translation
        }, completion: nil)
    }

    override func setupEvent() {
        super.setupEvent()
        gesture?.rx.event.bind(onNext: { [weak self](_) in
            guard self?.textField?.isFirstResponder == false else {
                return
            }
            self?.textField?.becomeFirstResponder()
        }).disposed(by: disposeBag)
        
        guard let collectionView = collectionView else {
            return
        }

        self.textField.rx.text.asObservable().map { (v) -> [String?] in
            var values = [String?].init(repeating: nil, count: 6)
            guard let v = v , !v.isEmpty else {
                return values
            }
            v.enumerated().filter({$0.offset < 6}).forEach({
                let str = String($0.element)
                values[$0.offset] = str
            })
            return values
        }.bind(to: strings).disposed(by: disposeBag)
        
        
        strings.asObservable().bind(to: collectionView.rx.items(cellIdentifier: "ZPKYCOTPCollectionViewCell", cellType: ZPKYCOTPCollectionViewCell.self))
        {
            $2.lblCharacter?.text = $1
        }
        .disposed(by: disposeBag)
    }
    
    // MARK: - API
    override func authenPhone(_ otp: String?) -> Observable<NSDictionary?> {
        guard let otp = otp else {
            return Observable.empty()
        }
        let signal = NetworkManager.sharedInstance().authenPhoneV2(withOTP: otp)
        return Observable.eventLoading(with: event(from: signal) { $0 as? NSDictionary })
    }
    
    // MARK: - Handler response
    override func handlerResult(from result: NSDictionary?) {
        self.performSegue(withIdentifier: kNextScreenName, sender: result)
    }
    
    override func resendPhone() {
        guard let userInfo = self.entity else {
            return
        }
        
        // Register user's information
        self.register(userInfo: userInfo).subscribe(onNext: { [weak self] _ in
            self?.showPopupResend()
            self?.numberResend.value += 1
        }, onError: { [weak self] (e) in
            self?.isCorrect = false
            var message: String
            if e.errorCode() == NSURLErrorNotConnectedToInternet {
                message = R.string_Home_InternetConnectionError()
            }else {
                message = e.userInfoData()["returnmessage"] as? String ?? ""
            }
            self?.lblWarning.text = message
            self?.eError.onNext(e)
        }).disposed(by: disposeBag)
    }
}
