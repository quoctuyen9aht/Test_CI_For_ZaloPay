//
//  ZPKYCPasswordOnboardingViewController.swift
//  ZaloPay
//
//  Created by tridm2 on 4/24/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import ZaloPayAnalyticsSwift

enum PasswordShowingState: Int {
    case hide
    case show
    
    var isHidden: Bool {
        return self == .hide
    }
    
    var not: PasswordShowingState {
        return isHidden ? .show : .hide
    }
    
    var nextTitle: String {
        return isHidden ? R.string_ButtonLabel_Show() : R.string_ButtonLabel_Hide()
    }
}

class ZPKYCOnboardInputPasswordCell: OnboardInputPasswordCell {
    private lazy var vCharacterDisplay: UILabel = {
        let v = UILabel(frame: self.contentView.bounds)
        v.font = v.font.withSize(26)
        v.textColor = UIColor.defaultText()
        self.contentView.addSubview(v)
        return v
    }()
    
    var character: String? {
        get {
            return vCharacterDisplay.text
        }
        set {
            vCharacterDisplay.text = newValue
            vDisplay.backgroundColor = (newValue == nil) ? .white : .zaloBase()
        }
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        character = nil
    }
    
    func setPasswordShowingState(_ state: PasswordShowingState) {
        let isHiddenPassword = state.isHidden
        vDisplay.isHidden = isHiddenPassword.not
        vCharacterDisplay.isHidden = isHiddenPassword
    }
    
    override func updateSelect() {
    }
}

class ZPKYCPasswordOnboardingViewController : OnboardInputPasswordViewController {
    let backButton = UIButton(type: .custom)
    private lazy var passwordShowingState: Variable<PasswordShowingState> = Variable(.hide)
    private lazy var strings: Variable<[String?]> = Variable([String?].init(repeating: nil, count: 6))
    
    @IBOutlet weak var lblFooter: UILabel!
    @IBOutlet weak var btnChangePswdState: UIButton!
    @IBOutlet weak var bottomFooterLabelConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
        setupEvents()
    }
    
    override func screenName() -> String! {
        return ZPTrackerScreen.Screen_iOS_OnboardingKYC_InputPaymentPasscode
    }
    
    override func isAdjustedConstraintsIndentify(_ identify: String?) -> Bool {
        return super.isAdjustedConstraintsIndentify(identify) ||
            identify == "topHideBtn"
    }
    
    override func setupDisplay() {
        self.adjustConstraints()
        self.lblNote.textColor = UIColor.zp_gray()
        self.lblFooter.textColor = UIColor.zp_gray()
        self.lblFooter.font = UIFont.sfuiTextRegularItalic(withSize: 13.0)
        self.isCorrect = true
    }
    
    private func setupNavigation() {
        backButton.isMultipleTouchEnabled = false
        backButton.tintColor = .white
        backButton.setTitle(UILabel.iconCode(withName: "general_backios"), for: .normal)
        backButton.titleLabel?.font = UIFont.iconFont(withSize: 20)
        if #available(iOS 11, *) {
            backButton.contentEdgeInsets = UIEdgeInsetsMake(0, -12, 0, 0)
        }
        backButton.frame = CGRect(origin: .zero, size: CGSize(width: 20, height: 44))
        backButton.contentHorizontalAlignment = .left
        
        backButton.rx.tap.bind { [weak self](_) in
            self?.trackEventAnalytic(.onboardingkyc_confirm_payment_passcode_back)
            self?.resetAll()
        }.disposed(by: disposeBag)
        
        let barButton = UIBarButtonItem(customView: backButton)
        let negativeSeparator = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        negativeSeparator.width = -12
        self.navigationItem.leftBarButtonItems = [negativeSeparator, barButton]
    }
    
    private func setupEvents() {
        passwordShowingState.asDriver().drive(onNext: { [weak self] state in
            guard let wSelf = self else {
                return
            }
            wSelf.btnChangePswdState.setTitle(state.nextTitle, for: .normal)
            
            // Trick: run (1)
            let oldValue = wSelf.strings.value
            wSelf.strings.value = oldValue
        }).disposed(by: disposeBag)
        
        // (1)
        self.strings.asObservable()
            .bind(to: collectionView.rx.items(cellIdentifier: "ZPKYCOnboardInputPasswordCell", cellType: ZPKYCOnboardInputPasswordCell.self)) { [unowned self] in
                $2.setPasswordShowingState(self.passwordShowingState.value)
                $2.character = $1
            }
            .disposed(by: disposeBag)
    }
    
    override func handlerShowKeyboard(duration t: TimeInterval, height h: CGFloat) {
        super.handlerShowKeyboard(duration: t, height: h)
        bottomFooterLabelConstraint.constant = h + 10
    }
    
    @IBAction func touchShowPswdButton(_ sender: Any) {
        passwordShowingState.value = passwordShowingState.value.not
    }
    
    override func updateUI(with pswdState: PasswordState) {
        passwordShowingState.value = .hide
        
        if pswdState == .begin {
            lblNote.text = R.string_Onboard_KYC_Create_Password_Note()
            lblFooter.text = R.string_Onboard_KYC_Create_Password_Footer()
            backButton.isHidden = true
        } else {
            lblNote.text = R.string_Onboard_KYC_Confirm_Password_Note()
            lblFooter.text = R.string_Onboard_Confirm_Password_Note()
            backButton.isHidden = false
        }
        
        lblWarning.text = pswdState.warning
        
        let title = pswdState.title.capitalizingFirstLetter()
        self.navigationItem.title = title
    }
    
    override func prepareNaviView() {
    }
    
    override func setupButton() {
        btnChangePswdState.isHidden = false
        let title = R.string_ButtonLabel_Show()
        heightBtn.constant = hBackBtn
        btnChangePswdState.isEnabled = true
        btnChangePswdState.setTitleColor(.zaloBase(), for: .normal)
        btnChangePswdState.setTitle(title, for: .normal)
    }
    
    override func changedPasswordInput(_ pswd: String) {
        super.changedPasswordInput(pswd)
        var values = [String?].init(repeating: nil, count: 6)
        pswd.enumerated().filter({$0.offset < 6}).forEach({
            let str = String($0.element)
            values[$0.offset] = str
        })
        
        btnChangePswdState.isHidden = pswd.isEmpty
        strings.value = values
    }
    
    override func trackEventAnalytic(_ eventId: ZPAnalyticEventAction) {
        var eventId = eventId
        switch eventId {
        case .onboarding_input_payment_passcode:
            eventId = .onboardingkyc_input_payment_passcode
        case .onboarding_confirm_payment_passcode_success:
            eventId = .onboardingkyc_confirm_payment_passcode_success
        case .onboarding_confirm_payment_passcode_fail:
            eventId = .onboardingkyc_confirm_payment_passcode_fail
        default:
            break
        }
        super.trackEventAnalytic(eventId)
    }
    
    override func trackScreenAnalytic(_ nameScreen: String) {
        var nameScreen = nameScreen
        switch nameScreen {
        case ZPTrackerScreen.Screen_iOS_Onboarding_InputPaymentPasscode:
            nameScreen = ZPTrackerScreen.Screen_iOS_OnboardingKYC_InputPaymentPasscode
        case ZPTrackerScreen.Screen_iOS_Onboarding_ConfirmPaymentPasscode:
            nameScreen = ZPTrackerScreen.Screen_iOS_OnboardingKYC_ConfirmPaymentPasscode
        default:
            break
        }
        super.trackScreenAnalytic(nameScreen)
    }
    
    override func completedInputPassword() {
        self.register(pin: self._password).trackActivity(activityTracking).subscribe(onNext: { [weak self] in
            self?.textField.resignFirstResponder()
            self?.handleFinalResult($0)
        }, onError: { [weak self](e) in
            self?.isCorrect = false
            var message: String
            if e.errorCode() == NSURLErrorNotConnectedToInternet {
                message = R.string_Home_InternetConnectionError()
            }else {
                message = e.userInfoData()["returnmessage"] as? String ?? ""
            }
            self?.lblWarning.text = message
            self?.eError.onNext(e)
        }).disposed(by: disposeBag)
    }
    
    private func register(pin: String) -> Observable<NSDictionary?> {
        let signal = NetworkManager.sharedInstance().registerPin(pin)
        return Observable.eventLoading(with: event(from: signal) { $0 as? NSDictionary })
    }
    
    private func handleFinalResult(_ result: NSDictionary?) {
        let navi = self.navigationController as? ZPNavigationCustomStatusViewController
        guard let handler = navi?.completionHandler, let response = result else {
            return
        }
        handler(response)
    }
}
