//
//  ZPKYCNoticeOnboardingViewController.swift
//  ZaloPay
//
//  Created by Dung Vu on 4/17/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import ObjectMapper
import ZaloPayAnalyticsSwift

fileprivate let kNextScreenName = "inputSMS"

// Using this to check for returning to login screen
fileprivate let timeReturnLogin: TimeInterval = 3600

class ZPKYCNoticeOnboardingViewController: UIViewController, SendUserInfoProtocol {
    @IBOutlet weak var lblInformation: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint?
    @IBOutlet weak var btnNext: UIButton?
    
    private var inittedTime: Date!
    private let eventErrorMessage: PublishSubject<String> = PublishSubject()
    private let disposeBag = DisposeBag()
    private lazy var helper = ZPSDKHelper()
    private lazy var isIphone4: Bool = UIScreen.main.bounds.height < 568
    
    override func viewDidLoad() {
        super.viewDidLoad()
        inittedTime = Date()
        ZaloPayWalletSDKPayment.sharedInstance().appDependence = helper
        self.setNavigationBarStyle()
        self.addLayout()
        self.setupEvent()
        self.adjustConstraints()
    }
    
    override func screenName() -> String! {
        return ZPTrackerScreen.Screen_iOS_OnboardingKYC_Inform
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ZPTrackingHelper.shared().trackScreen(with: self)
    }
    
    private func isAdjustedConstraintsIndentify(_ identify: String?) -> Bool {
        return identify == "topImage" ||
            identify == "topInformation"
    }
    
    private func adjustConstraints() {
        // Iphone 4 -> force size
        if isIphone4 {
            self.view.constraints.filter({
                let identify = $0.identifier
                return isAdjustedConstraintsIndentify(identify)
            }).forEach({ $0.constant = 2})
            
            self.btnNext?.snp.updateConstraints {
                $0.height.equalTo(44)
            }
            
            self.image.snp.updateConstraints {
                $0.height.equalTo(200)
            }
            
            self.lblInformation.font = UIFont.sfuiTextRegular(withSize: 14.0)
        }
    }
    
    private func setNavigationBarStyle() {
        UIApplication.shared.statusBarStyle = .lightContent
        self.navigationController?.defaultNavigationBarStyle()
    }
    
    private func addLayout() {
        self.lblInformation.font = UIFont.sfuiTextRegular(withSize: 15.0)
        
        self.title = R.string_Dialog_Title_Notification()
        self.btnNext?.setBackgroundColor(UIColor.zaloBase(), for: .normal)
        self.btnNext?.setBackgroundColor(UIColor.highlightButton(), for: .selected)
        self.btnNext?.layer.cornerRadius = 3
        self.btnNext?.clipsToBounds = true
        
        // adjust layout for small view
        guard UIScreen.main.bounds.width == 320 else {
            return
        }
        self.topConstraint?.constant = 10
        self.view.layoutIfNeeded()
    }
    
    private func setupEvent() {
        self.btnNext?.rx.controlEvent(UIControlEvents.touchUpInside).bind { [weak self] in
            ZPTrackingHelper.shared().trackEvent(.onboardingkyc_view_inform)
            self?.runKYCFlow()
        }.disposed(by: disposeBag)
        
        self.eventErrorMessage
            .asObserver()
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { message in
            ZPDialogView.showDialog(with: DialogTypeInfo, title: nil, message: message, buttonTitles:[R.string_ButtonLabel_Close()], handler: nil)
        }).disposed(by: disposeBag)
        
        // Tracking app active and check time -> Login controller
        NotificationCenter.default.rx.notification(Notification.Name.UIApplicationDidBecomeActive).filter({[weak self] _ in
            // Notice viewcontroller + KYC viewcontroller => 2 viewcontrollers
            let isAppear = (self?.navigationController?.viewControllers.count ?? 0) <= 2
            guard let wSelf = self, isAppear else {
                return false
            }
            
            // Check
            let time = Date().timeIntervalSince(wSelf.inittedTime)
            return time >= timeReturnLogin
        }).subscribe(onNext: { [weak self](_) in
            self?.moveToLogin()
        }).disposed(by: disposeBag)
    }
    
    private func runKYCFlow() {
        let kycFlow = ~ZaloPayWalletSDKPayment.sharedInstance().runKYCFlow(self, title: R.string_UpdateProfileLevel2_Title(), infor: nil, type: ZPKYCType.onboarding)
        
        kycFlow.subscribe(onNext: { [weak self](result) in
            #if DEBUG
            print(result ?? [:])
            #endif
            
            let dict = result as? JSON
            self?.performSMSScreen(data: dict ?? [:])
        }, onError: { [weak self](e) in
            self?.handler(error: e)
        }, onCompleted: {
            // Move to OTP
        }).disposed(by: disposeBag)
    }
    
    private func handler(error e: Error) {
        self.popMe()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let destinationVC = segue.destination as? ZPKYCSMSOnboardingViewController,
            let kycDic = sender as? [String : Any],
            let kycObj = kycFromDic(kycDic) else {
            return
        }
        destinationVC.entity = kycObj
    }
    
    private func kycFromDic(_ kycDic: JSON) -> ZPKYCOnboardingEntity? {
        guard let infor = kycDic["kyc"] as? String else {
            return nil
        }
        return Mapper<ZPKYCOnboardingEntity>().map(JSONString: infor)
    }
    
    private func performSMSScreen(data: JSON) {
        guard let userInfo = kycFromDic(data) else {
            return
        }
        
        // Register user's information
        self.register(userInfo: userInfo).subscribe(onNext: { [weak self] _ in
            self?.performSegue(withIdentifier: kNextScreenName, sender: data)
        }, onError: { [weak self] (e) in
            self?.handleError(e)
        }).disposed(by: disposeBag)
    }
    
    private func handleError(_ e: Error) {
        let errCode = e.errorCode()
        guard errCode != ZALOPAY_ERRORCODE_ZALO_LOGIN_FAIL.rawValue else {
            self.moveToLogin()
            return
        }
        
        var message: String
        if errCode == ZALOPAY_ERRORCODE_DUPLICATE_PHONE.rawValue {
            ZPTrackingHelper.shared().trackEvent(.onboardingkyc_input_phone_number_duplicate)
        }
        if errCode == NSURLErrorNotConnectedToInternet {
            message = R.string_Home_InternetConnectionError()
        } else {
            message = e.userInfoData()["returnmessage"] as? String ?? ""
        }
        
        self.eventErrorMessage.onNext(message)
    }
    
    private func moveToLogin() {
        // Go to Login
        // Copy handler using for next login
        guard let navi = self.navigationController as? ZPNavigationCustomStatusViewController, let handler = navi.completionHandler else {
            return
        }
        let login = ZPLoginViewController.loginViewWihtCompleteHande(handler)
        UIApplication.shared.delegate?.window??.rootViewController = login
    }
}

protocol SendUserInfoProtocol : class {
    func register(userInfo: ZPKYCOnboardingEntity) -> Observable<Void>
}

extension SendUserInfoProtocol {
    func register(userInfo: ZPKYCOnboardingEntity) -> Observable<Void> {
        let signal = NetworkManager.sharedInstance().registerUserInfo(userInfo.phone,
                                                                      gender: userInfo.gender as NSNumber?,
                                                                      fullName: userInfo.fullName,
                                                                      dob:userInfo.dob,
                                                                      idType:userInfo.idType as NSNumber?,
                                                                      idValue:userInfo.idValue)
        return Observable.eventLoading(with: eventIgnoreValue(from: signal))
    }
}
