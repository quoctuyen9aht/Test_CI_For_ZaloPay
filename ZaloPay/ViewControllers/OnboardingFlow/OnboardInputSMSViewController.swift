//
//  OnboardInputSMSViewController.swift
//  ZaloPay
//
//  Created by Dung Vu on 6/13/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Foundation
import ZaloPayAnalyticsSwift

fileprivate let timeResend: TimeInterval = 60
typealias Time = (minute: Int, seconds: Int)
class OnboardInputSMSViewController: BaseInputViewController, ZPOTPProtocol, SendPhoneProtocol {

    var phone: String!
    var password: String!
    @IBOutlet weak var textField: UITextField! {
        didSet{
            self.input = textField
        }
    }
    
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnResend: UIButton!
    @IBOutlet weak var lblTimer: UILabel!
    fileprivate var timer: Disposable?
    let numberResend: Variable<Int> = Variable(1)
    fileprivate let trackErrorCode: Variable<Int> = Variable(0)
    fileprivate var timeRefresh: TimeInterval = timeResend
    fileprivate lazy var calendar: Calendar = Calendar.current
    fileprivate var dateCheck: Date!
    
    var isEnableButton: Bool = false {
        didSet{
            btnNext.isEnabled = isEnableButton
            setBorderColor(isEnableButton)
        }
    }
    
    fileprivate lazy var popUpResend: UIView = {
        return self.setupPopUpResend()
    }()
    
    override func setupDisplay() {
        super.setupDisplay()
        self.textField.delegate = self
        self.lblTitle.text = R.string_Onboard_Input_OTP_Title()
        let s = String(format: R.string_Onboard_Input_OTP_Note() , "\(phone ?? "")")
        self.lblNote.text = s
        self.lblWarning.text = ""
        isCorrect = true
        
    }
    
    func setupBtnResend() {
        isEnableButton = false
        btnResend.setTitleColor(UIColor.zaloBase(), for: .normal)
        // Create
        let icon: String = UILabel.iconCode(withName: "onboarding_refresh") ?? ""
        let titleNormal = String(format: R.string_Onboard_Input_OTP_Refresh(), "\(icon)")
        let atts = NSMutableAttributedString(string: titleNormal, attributes: [NSAttributedStringKey.foregroundColor : UIColor(hexValue:0x008fe5)])
        atts.addAttributes([NSAttributedStringKey.font : UIFont.zaloPay(withSize: 15)], range: (titleNormal as NSString).range(of: icon))
        
        btnResend.setAttributedTitle(atts, for: .normal)
    }
    
    override func setupButton() {
        self.backTop?.isHidden = false
        self.roundButton(btnNext)
        self.lblTimer.textColor = UIColor.zp_gray()
        btnNext.setTitle(R.string_Onboard_Input_OTP_Confirm(), for: .normal)
        btnNext.setTitle(R.string_Onboard_Input_OTP_Confirm(), for: .disabled)
        btnNext.setTitleColor(.white, for: .normal)
        btnNext.setBackgroundColor(UIColor.zaloBase(), for: UIControlState.normal)
        btnNext.setBackgroundColor(UIColor(hexValue: 0xc7cad3), for: UIControlState.disabled)
        btnNext.setTitleColor(.white, for: .disabled)
        self.setupBtnResend()
    }
    
    func setBorderColor(_ enabled: Bool) {
        btnNext.layer.borderColor = (enabled ? UIColor.zaloBase() : UIColor(hexValue: 0xc7cad3)).cgColor
        
    }
    override func screenName() -> String! {
        return ZPTrackerScreen.Screen_iOS_Onboarding_OTP
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ZPTrackingHelper.shared().trackScreen(with: self)
        self.trackEventAnalytic(.onboarding_input_otp_success)
    }
    
    override func setupEvent() {
        super.setupEvent()
        
        let eventText = textField.rx.text.map({ $0 ?? ""})
        eventText.bind { [weak self] in
            self?.isEnableButton = $0.count == 6
            }.disposed(by: disposeBag)
        
        eventText.filter({ $0.count < 6 }).subscribe(onNext: { [weak self](_) in
            self?.lblWarning.text = ""
            self?.isCorrect = true
        }).disposed(by: disposeBag)
        
        
        btnResend.rx.controlEvent(.touchUpInside).subscribe(onNext: { [weak self](_) in
            self?.resendPhone()
        }).disposed(by: disposeBag)
        
        btnNext.rx.controlEvent(.touchUpInside).subscribe(onNext: { [weak self](_) in
            self?.checkOTP()
        }).disposed(by: disposeBag)
        
        self.eventBack?.bind(onNext: { [weak self](_) in
            // Event Alert
            self?.showAlertBack()
        }).disposed(by: disposeBag)
        
        // Track number resend
        numberResend.asDriver().drive(onNext: { [weak self] in
            self?.timeRefresh = $0 % 3 == 0 ? timeResend * 15 : timeResend
            self?.createTimer()
        }).disposed(by: disposeBag)
        
        
        // Track Error Code Over Limit Send Identify OTP
        trackErrorCode.asDriver().filter({ $0 == -2002 }).drive(onNext: { [weak self](_) in
            // Show Alert -> move to login
            self?.showAlertMoveToLogin()
            
        }).disposed(by: disposeBag)
    }
    
    fileprivate func setupPopUpResend() -> UIView {
        let v = UIView(frame: .zero)
        v.layer.cornerRadius = 10
        v.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.54)
        self.view.addSubview(v)
        let top: CGFloat = isIphone4 ? 50 : 114
        v.snp.makeConstraints { (make) in
            make.top.equalTo(top)
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.equalTo(228)
            make.height.equalTo(100)
        }
        
        let icon = UILabel(frame: .zero)
        icon.font = UIFont.zaloPay(withSize: 27)
        icon.textColor = .white
        v.addSubview(icon)
        icon.snp.makeConstraints { (make) in
            make.top.equalTo(27)
            make.centerX.equalTo(v.snp.centerX)
            make.width.greaterThanOrEqualTo(0)
            make.height.greaterThanOrEqualTo(0)
        }
        icon.text = UILabel.iconCode(withName: "general_check")
        
        let lblDescription = UILabel(frame: .zero)
        lblDescription.font = UIFont.sfuiTextRegular(withSize: 15)
        lblDescription.textColor = .white
        v.addSubview(lblDescription)
        lblDescription.snp.makeConstraints { (make) in
            make.top.equalTo(icon.snp.bottomMargin).offset(10)
            make.centerX.equalTo(v.snp.centerX)
            make.width.greaterThanOrEqualTo(0)
            make.height.greaterThanOrEqualTo(0)
        }
        
        lblDescription.text = R.string_Onboard_Input_OTP_Resend_Success()
        v.isHidden = true
        v.alpha = 0
        return v
    }
    
    func showPopupResend() {
        popUpResend.isHidden = false
        UIView.animateKeyframes(withDuration: 1.4, delay: 0, options: .calculationModeLinear, animations: {[weak self] in
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.3, animations: { 
                self?.popUpResend.alpha = 1
            })
            
            UIView.addKeyframe(withRelativeStartTime: 1, relativeDuration: 0.4, animations: {
                self?.popUpResend.alpha = 0
            })
        }) { [weak self](_) in
            self?.popUpResend.isHidden = true
        }
        
    }
    
    func time(from interval: TimeInterval) -> Time{
        let nDate = Date(timeInterval: interval, since: dateCheck)
        let components = calendar.dateComponents([.minute, .second], from: dateCheck, to: nDate)
        
        let m = components.minute ?? 0
        let s = components.second ?? 0
        return Time(m, s)
    }
    
    
    fileprivate func createTimer() {
        self.btnResend.isHidden = true
        self.lblTimer.isHidden = false
        dateCheck = Date()
        let nT = self.time(from: timeRefresh)
        self.lblTimer.text = String(format: R.string_Onboard_Input_OTP_Refresh_Counting(), nT.minute, nT.seconds)
        self.timer = Observable<Int>.interval(1, scheduler: MainScheduler.instance).subscribe(onNext: {[weak self] (idx) in
            guard let wSelf = self else {
                return
            }
            guard idx < Int(wSelf.timeRefresh) else {
                wSelf.resetTimer()
                return
            }
            let t = wSelf.time(from: wSelf.timeRefresh - TimeInterval(idx))
            let text = String(format: R.string_Onboard_Input_OTP_Refresh_Counting(), t.minute, t.seconds)
            wSelf.lblTimer.text = text
            
        }, onDisposed: {[weak self] in
            self?.btnResend.isHidden = false
            self?.lblTimer.isHidden = true
        })
        
    }
    
    fileprivate func resetTimer() {
        self.timer?.dispose()
        self.timer = nil
    }
    
    fileprivate func showAlertMoveToLogin() {
        ZPDialogView.showDialog(with: DialogTypeWarning, title: R.string_Dialog_Title_Notification() , message: R.string_Onboard_Input_OTP_Over_Limit(), buttonTitles: [R.string_ButtonLabel_OK()]) { [weak self](idx) in
            
            // Go to Login
            // Copy handler using for next login
            guard let navi = self?.navigationController as? ZPNavigationCustomStatusViewController, let handler = navi.completionHandler else {
                return
            }
            let login = ZPLoginViewController.loginViewWihtCompleteHande(handler)
            UIApplication.shared.delegate?.window??.rootViewController = login
        }
    }
    
    
    fileprivate func showAlertBack() {
        let message = String(format: R.string_Onboard_Input_OTP_Dialog_Message(),phone ?? "" )
        ZPDialogView.showDialog(with: DialogTypeWarning, title: R.string_Dialog_Confirm() , message: message , buttonTitles: [R.string_ButtonLabel_OK(), R.string_ButtonLabel_Cancel()]) { [weak self](idx) in
            if (idx == 0) {
                self?.navigationController?.popViewController(animated: false)
            }else {
                self?.input?.becomeFirstResponder()
            }
        }
    }
    
    func checkOTP() {
        self.trackEventAnalytic(.onboarding_submit_otp)
        self.authenPhone(self.textField.text).trackActivity(activityTracking).subscribe(onNext: {[weak self] r in
            // go to home
            self?.trackEventAnalytic(.onboarding_input_otp_success)
            self?.textField.resignFirstResponder()
            self?.handlerResult(from: r)
        }, onError: { [weak self](e) in
            self?.trackErrorCode.value = e.errorCode()
            self?.isCorrect = false
            self?.trackEventAnalytic(.onboarding_input_otp_fail)
            var message: String
            if e.errorCode() == NSURLErrorNotConnectedToInternet {
                message = R.string_Home_InternetConnectionError()
            }else {
                message = e.userInfoData()["returnmessage"] as? String ?? ""
            }
            print(message)
            self?.lblWarning.text = message
            self?.eError.onNext(e)
        }).disposed(by: disposeBag)
    }
    
    func trackEventAnalytic(_ eventId: ZPAnalyticEventAction) {
        ZPTrackingHelper.shared().trackEvent(eventId)
    }
    
    func resendPhone() {
        self.trackEventAnalytic(.onboarding_resend_otp)
        self.registerPhone(self.phone).trackActivity(activityTracking).subscribe(onNext: { [weak self](_) in
            self?.showPopupResend()
            self?.numberResend.value += 1
        }, onError: { [weak self](e) in
            self?.isCorrect = false
            var message: String
            if e.errorCode() == NSURLErrorNotConnectedToInternet {
                message = R.string_Home_InternetConnectionError()
            }else {
                message = e.userInfoData()["returnmessage"] as? String ?? ""
            }
            self?.lblWarning.text = message
            self?.eError.onNext(e)
        }).disposed(by: disposeBag)
    }
    
    /// Handler response success
    ///
    /// - Parameter result: json from api check otp
    func handlerResult(from result: NSDictionary?) {
        let navi = self.navigationController as? ZPNavigationCustomStatusViewController
        guard let handler = navi?.completionHandler, let response = result else {
            return
        }
        handler(response)
    }

    func didReceiveOptMessage(_ opt: String) {
        self.textField.text = opt
        self.textField.sendActions(for: .valueChanged)
    }
    
    func authenPhone(_ otp: String?) -> Observable<NSDictionary?> {
        guard let otp = otp else {
            return Observable.empty()
        }
        let signal = NetworkManager.sharedInstance().authenPhone(withOTP: otp)
        return Observable.eventLoading(with: event(from: signal) { $0 as? NSDictionary })
    }
}

extension OnboardInputSMSViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard canInput.value else {
            return false
        }
        
        guard let t = textField.text else {
            return true
        }
        do {
            var nStr: String = ""
            try catchException {
                nStr = (t as NSString).replacingCharacters(in: range, with: string)
            }
            let next = nStr.count <= 6
            return next
        } catch {
            #if DEBUG
                print(error.localizedDescription)
                assert(false, "Need check this case!!!!")
            #endif
            return false
        }
    }
}
