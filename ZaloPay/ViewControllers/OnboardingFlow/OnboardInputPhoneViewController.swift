//
//  OnboardInputPhoneViewController.swift
//  ZaloPay
//
//  Created by Dung Vu on 6/12/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import ZaloPayAnalyticsSwift
import ZaloPayConfig

fileprivate let kNextScreenName = "showSMS"

final class OnboardInputPhoneViewController: BaseInputViewController {
    
    var password: String!
    var isEnableButton: Bool = false {
        didSet{
            btnContinue.isEnabled = isEnableButton
        }

    }
    @IBOutlet weak var textField: UITextField! {
        didSet{
            self.input = textField
            guard let t = textField else {
                return
            }
            t.delegate = self
        }
    }
    
    @IBOutlet weak var btnContinue: UIButton!
    fileprivate var patterns: [String] = []
    
    override func viewDidLoad() {
        self.loadConfigPhone()
        super.viewDidLoad()
    }
    
    override func screenName() -> String! {
        return ZPTrackerScreen.Screen_iOS_Onboarding_Phone;
    }
    override func setupButton() {
        //nextios
        let icon: String = UILabel.iconCode(withName: "general_arrowright") ?? ""
        let titleNormal = String(format: "%@ %@", R.string_Onboard_Input_Phone_Next(), "\(icon)")
        let r = (titleNormal as NSString).range(of: icon)
        let attN = NSMutableAttributedString(string: titleNormal, attributes: [NSAttributedStringKey.foregroundColor : UIColor.zaloBase()])
        let attD = NSMutableAttributedString(string: titleNormal, attributes: [NSAttributedStringKey.foregroundColor :  UIColor(hexValue: 0xc7cad3)])

        attN.addAttributes([NSAttributedStringKey.font : UIFont.zaloPay(withSize: 15)], range: r)
        attD.addAttributes([NSAttributedStringKey.font : UIFont.zaloPay(withSize: 15)], range: r)

        let paragraph = NSMutableParagraphStyle()
        paragraph.lineHeightMultiple = 20
        paragraph.alignment = .justified
        
        attN.addAttributes([NSAttributedStringKey.paragraphStyle : paragraph], range: r)
        attD.addAttributes([NSAttributedStringKey.paragraphStyle : paragraph], range: r)
        
        btnContinue.setAttributedTitle(attN, for: .normal)
        btnContinue.setAttributedTitle(attD, for: .disabled)
        
        isEnableButton = false
    }
    
    func setBorderColor(_ enabled: Bool) {
        btnContinue.layer.borderColor = (enabled ? UIColor.zaloBase() : UIColor.zp_gray()).cgColor
    }
    
    override func setupDisplay() {
        super.setupDisplay()
        self.lblTitle.text = R.string_Onboard_Input_Phone_Title()
        self.lblNote.text = R.string_Onboard_Input_Phone_Note()
        self.lblWarning.text = ""
        self.isCorrect = true
    }
    
    func loadConfigPhone() {
//        let dic_phone_format = ApplicationState.getDictionaryConfig(fromDic: "general", andKey: "phone_format", andDefault: [:])
//        let phonePatterns = dic_phone_format?["patterns"] as? [String]
//        patterns = phonePatterns ?? []
        
        let dic_phone_format = ZPApplicationConfig.getGeneralConfig()?.getPhoneFormat()
        let phonePatterns = dic_phone_format?.getPatterns()
        patterns = phonePatterns ?? []
    }
    
    override func setupEvent() {
        super.setupEvent()
        self.btnContinue.rx.controlEvent(.touchUpInside).subscribe(onNext: { [weak self](_) in
            self?.register()
        }).disposed(by: disposeBag)
     
        let eventText = textField.rx.text.map{ $0 ?? ""}.asObservable()
        
        eventText.filter({ $0.count >= 10 }).subscribe(onNext: { [weak self](s) in
            guard let wSelf = self else {
                return
            }
            func check() -> Bool{
                for regex in wSelf.patterns {
                    let p = NSPredicate(format: "SELF MATCHES %@", regex)
                    
                    if p.evaluate(with: s) {
                        return true
                    }
                    continue
                }
                
                return false
            }
            
            let isOke = s.count <= 11 ? check() : false
            if !isOke {
                ZPTrackingHelper.shared().trackEvent(.onboarding_input_phone_number_invalid)
            }
            wSelf.isEnableButton = isOke
            wSelf.isCorrect = isOke
            wSelf.lblWarning.text = isOke.not ? R.string_Onboard_Input_Phone_Error() : ""
            
        }).disposed(by: disposeBag)
        
        eventText.filter({ $0.count < 10}).subscribe(onNext: { [weak self](s) in
            self?.isEnableButton = false
            self?.isCorrect = true
            self?.lblWarning.text = ""
        }).disposed(by: disposeBag)
    }
    
    func register() {
        self.registerPhone(self.textField.text).trackActivity(activityTracking).subscribe(onNext: { [weak self](_) in
            self?.performSegue(withIdentifier: kNextScreenName, sender: nil)
        }, onError: { [weak self](e) in
                self?.isCorrect = false
                var message: String
                if e.errorCode() == ZALOPAY_ERRORCODE_DUPLICATE_PHONE.rawValue {
                    ZPTrackingHelper.shared().trackEvent(.onboarding_input_phone_number_duplicate)
                }
                if e.errorCode() == NSURLErrorNotConnectedToInternet {
                    message = R.string_Home_InternetConnectionError()
                }else {
                    message = e.userInfoData()["returnmessage"] as? String ?? ""
                }
                self?.lblWarning.text = message
                self?.eError.onNext(e)
        }).disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.isEnableButton = false
        self.textField.text = ""
        self.lblWarning.text = ""
        self.backTop?.isHidden = true
        super.viewWillAppear(animated)
        
        ZPTrackingHelper.shared().trackScreen(with: self)
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.onboarding_submit_phone_number)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let vc = segue.destination as? OnboardInputSMSViewController else {
            return
        }
        vc.phone = self.textField.text ?? ""
        vc.password = password
    }
}

extension OnboardInputPhoneViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard canInput.value else {
            return false
        }
        
        guard let t = textField.text else {
            return true
        }
        
        let nStr = (t as NSString).replacingCharacters(in: range, with: string)
        let next = nStr.count <= 11
        return next
    }
}

// MARK: - request
extension OnboardInputPhoneViewController: SendPhoneProtocol {}

protocol SendPhoneProtocol: class{
    var password: String! { get }
    func registerPhone(_ phone: String?) -> Observable<Void>
}

extension SendPhoneProtocol {
    func registerPhone(_ phone: String?) -> Observable<Void> {
        guard let p = phone else {
            return Observable.empty()
        }
        let pin = self.password
        let signal = NetworkManager.sharedInstance().register(withPhone: p, pin: pin)
        return  Observable.eventLoading(with: eventIgnoreValue(from: signal))
    }
}

