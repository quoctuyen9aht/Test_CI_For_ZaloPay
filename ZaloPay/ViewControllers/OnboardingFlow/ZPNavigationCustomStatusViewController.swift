//
//  ZPNavigationCustomStatusViewController.swift
//  ZaloPay
//
//  Created by Dung Vu on 6/12/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit

final class ZPNavigationCustomStatusViewController: UINavigationController {
    var completionHandler: ((NSDictionary)->Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarStyle = .default
        self.interactivePopGestureRecognizer?.isEnabled = false
        
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    

}


