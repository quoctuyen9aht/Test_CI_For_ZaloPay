//
//  ZPSettingViewController.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/13/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import ZaloPayAnalyticsSwift

@objc class ZPSettingViewController: BaseViewController {
    
    var presenter: ZPSettingPresenterDelegate!
    var dataSources: [ZPSettingEntity] = []
    var tableView: UITableView!
    private let disposeBag = DisposeBag()
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureTableView()
        
        self.baseViewControllerViewDidLoad()
        self.title = R.string_Settings_Protect_Account()
        
        let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        space.width = 20
        self.navigationItem.rightBarButtonItems = [space]
        presenter.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ZPTrackingHelper.shared().trackScreen(with: self)
    }
    
    override func screenName() -> String! {
        return ZPTrackerScreen.Screen_iOS_Me_Security
    }
    //ZPAnalyticEventActionMe_security_touch_back
    override func backEventId() -> Int {
        return ZPAnalyticEventAction.me_security_touch_back.rawValue
    }
    
    override func backButtonClicked(_ sender: Any!) {
        super.backButtonClicked(sender)
    }
    private func configureTableView() {
        self.tableView = UITableView(frame: .zero, style: .grouped)
        self.view.addSubview(tableView)
        self.tableView.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.bottom.equalTo(0)
            make.left.equalTo(0)
            make.right.equalTo(0)
        }
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.backgroundColor = .defaultBackground()
        self.tableView.rowHeight = 60
        let nib = UINib(nibName: "ZPProfileLogoutCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "ZPProfileLogoutCell")
    }
    
    func getCellSettingOfSection(withType typeSection: ZPSettingtType) -> ZPSettingCell? {
        guard let item = dataSources.first(where: { $0.type == typeSection}),
            let index = self.dataSources.index(of: item),
            let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: index)),
            let settingCell = cell as? ZPSettingCell else {
                return nil
        }
        
        return settingCell
    }
}

extension ZPSettingViewController: ZPSettingViewControllerInput {

    func reloadData() {
        tableView.reloadData()
        tableView.layoutIfNeeded()
    }
    
    func setValueForSection(typeItem: ZPSettingtType, value: Bool) {
        guard let item = dataSources.first(where: { $0.type == typeItem}),
            let settingCell = self.getCellSettingOfSection(withType: typeItem) else {
            return
        }
        settingCell.valueSwitch.isOn = value
        self.presenter.changeSwitchValue(item, value: value)
    }
    
    func setDataSourcesSetting(_ dataSources: [ZPSettingEntity]) {
        self.dataSources = dataSources
        self.tableView.reloadData()
    }
    
    func showAutoLockMenu() {
        let optionMenu = UIAlertController(title: nil, message: R.string_Setting_Auto_Lock_Title(), preferredStyle: .actionSheet)
        let cancelActionButton = UIAlertAction(title: "Hủy Bỏ", style: .cancel)
        optionMenu.addAction(cancelActionButton)
        
        let options = presenter.getAutoLockOptions()
        for option in options {
            var title = String()
            if option == 0 {
                let item = UIAlertAction(title: R.string_Setting_Auto_Lock_Description(), style: .default)
                { _ in
                    self.selectedTimeAutoLock(seconds: option)
                }
                optionMenu.addAction(item)
                continue
            }
            if option % 60 != 0 {
                title = String(format: R.string_Setting_Auto_Lock_After_Second_Title(), option.toString())
            } else {
                title = String(format: R.string_Setting_Auto_Lock_After_Minute_Title(), (option / 60).toString())
            }
            let item = UIAlertAction(title: title, style: .default) { _ in
                self.selectedTimeAutoLock(seconds: option)
            }
            optionMenu.addAction(item)
        }
        optionMenu.rx.deallocated.bind(onNext: { (_) in
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.me_security_touch_pass_time_close)
        }).disposed(by: disposeBag)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func showManageLoginDevices() {
        let listVC = ListDeviceViewController.loadFromStoryBoard(with: "ListDevice")
        self.navigationController?.pushViewController(listVC, animated: true)
    }
}

// MARK: UITableViewDelegate, UITableViewDataSource
extension ZPSettingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSources.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = dataSources[indexPath.section]
        
        if (item.type == .logout) {
            let cellLogOut = tableView.dequeueReusableCell(withIdentifier: "ZPProfileLogoutCell", for: indexPath) as? ZPProfileLogoutCell
            let dict: [String: Any] = ["Content": R.string_LeftMenu_SigOut(),
                                       "icon": ["name": "personal_logout","color" : UIColor.zp_gray()],
                                       "color": UIColor.zp_gray()]
            cellLogOut?.setItem(dict: dict)
            
            return cellLogOut!
        }
        
        let cell = ZPSettingCell.zp_defaultCell(for: tableView)
        
        cell?.delegate = self
        cell?.bindingUI(item)
    
        if item.type == .autoLock {
            let autoLockTime = ZPSettingsConfig.sharedInstance().autoLockTime.toInt()
            cell?.titleLabel.attributedText = self.presenter.selectTimeAutoLock(seconds: autoLockTime)
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if tableView.cellForRow(at: indexPath) is ZPProfileLogoutCell  {
            ZPDialogView.showDialog(with: DialogTypeNotification, title: R.string_Dialog_Title_Notification() , message: R.string_Message_Signout_Confirmation() , buttonTitles: [R.string_LeftMenu_SigOut(), R.string_ButtonLabel_Cancel()]) { (idx) in
                if (idx == 0) {
                    LoginManagerSwift.sharedInstance.logout()
                }
            }
        }else {
             let item = self.dataSources[indexPath.section]
             ZPTrackingHelper.shared().trackEvent(item.eventID)
             presenter.selectEnitity(at: indexPath)
        }
        
        return nil
    }
    
    // MARK: Footer
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let item = self.dataSources[section]
        if item.type == .autoLock {
            return 33
        }
        return item.height + 33
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let item = self.dataSources[section]
        if item.type == .autoLock {
            return nil
        }
        
        let hFooter = item.height + 33
        let footer = UIView(frame: CGRect(origin: .zero, size: CGSize(width: tableView.bounds.width, height: hFooter))).then {
            $0.backgroundColor = .clear
            let lblTitle = UILabel(frame: .zero)
            lblTitle.numberOfLines = 0
            lblTitle.zpSubTextGrayRegular()
            $0.addSubview(lblTitle)
            lblTitle.snp.makeConstraints({ (make) in
                make.top.equalTo(8)
                make.left.equalTo(10)
                make.right.equalTo(-10)
            })
            lblTitle.text = item.sectionDescription
        }
        return footer
    }
    
    // MARK: Header
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 30 : 0.01
    }
    
    //Set items on Cell
    func setSwitchValue(at item: ZPSettingEntity, value: Bool) {
        guard let settingCell = self.getCellSettingOfSection(withType: item.type) else { return }
        settingCell.valueSwitch.isOn = value
    }

    func setDisableCell(of settingType: ZPSettingtType, value: Bool) {
        guard let settingCell = self.getCellSettingOfSection(withType: settingType) else { return }
        
        if value {
            settingCell.titleLabel.alpha = 0.5
            settingCell.valueSwitch.alpha = 0.5
            settingCell.valueSwitch.isUserInteractionEnabled = false
        } else {
            settingCell.titleLabel.alpha = 1
            settingCell.valueSwitch.alpha = 1
            settingCell.valueSwitch.isUserInteractionEnabled = true
        }
    }
    
    func selectedTimeAutoLock(seconds: Int) {
        guard let cell = self.getCellSettingOfSection(withType: .autoLock) else { return }
        let titleString = self.presenter.selectTimeAutoLock(seconds: seconds)
        cell.titleLabel.attributedText = titleString
    }
}

extension ZPSettingViewController: ZPSettingCellDelegate {
    
    func changeValueSwitch(_ item: ZPSettingEntity, value: Bool) {
        self.presenter.changeSwitchValue(item, value: value)
    }
    
}



