//
//  ZPSplashScreenView.swift
//  ZaloPay
//
//  Created by nhatnt on 14/04/2018.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift
import ZaloPayAnalyticsSwift

class ZPSplashScreenView: UIView {
    weak var currentWindow: UIView?
    private let disposeBag = DisposeBag()
    
    @discardableResult
    init(appear window: UIView?) {
        super.init(frame: .zero)
        currentWindow = window
        addlayout()
        setupEvent()
        ZPTrackingHelper.shared().trackScreen(withName:self.screenName())
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addlayout() {
        self.backgroundColor = .zaloBase()
        let logoImageView = UIImageView(image: #imageLiteral(resourceName: "logo_splashscreen"))
        logoImageView.contentMode = .scaleAspectFit
        
        //If keyboard is being shown
        guard let currentNaviController = UINavigationController.currentActiveNavigationController() else {
            return
        }
        currentNaviController.view.endEditing(true)
        if let presentedViewController = currentNaviController.presentedViewController {
            presentedViewController.view.endEditing(true)
        }
        
        currentWindow?.addSubview(self)
        self.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        self.addSubview(logoImageView)
        logoImageView.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview().offset(-30)
        }
    }
    
    func setupEvent() {
        NotificationCenter.default.rx.notification(Notification.Name.UIApplicationDidBecomeActive).bind { [weak self](_) in
            self?.removeFromSuperview()
            NotificationCenter.default.post(name: NSNotification.Name.ZPDialogViewHide, object: nil)
        }.disposed(by: disposeBag)
    }
    
    func screenName() -> String {
        return ZPTrackerScreen.Screen_iOS_Splash
    }
}
