//
//  File.swift
//  ZaloPay
//
//  Created by nhatnt on 10/04/2018.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit

class AuthenticationRouter: AuthenticationRouterProtocol {
    
    class func assembleModule() -> AuthenticationViewController {
        let view = AuthenticationViewController.init(nibName: String(describing: AuthenticationViewController.self), bundle: nil)
        let router = AuthenticationRouter()
        let interactor = AuthenticationInteractor()
        let presenter = AuthenticationPresenter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router        
        interactor.presenter = presenter

        return view
    }
}
