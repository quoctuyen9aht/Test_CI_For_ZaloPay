//
//  ZPSettingPresenter.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/13/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import ZaloPayCommonSwift
import ZaloPayAnalyticsSwift

class ZPSettingPresenter: ZPSettingPresenterDelegate {
    
    weak var controller: ZPSettingViewControllerInput?
    var interactor: ZPSettingInteractorInputDelegate!
    var router: ZPSettingRouterDelegate!
    
    func viewDidLoad() {
        interactor.configureDataSource()
    }
    
    func alertSuccess() {
        let dialog = ZPDialogView(type: DialogTypeSuccess, message: R.string_ChangePIN_Message_Success(), items: nil)
        dialog?.show()
    }
    
    func getAutoLockOptions() -> [Int] {
        return interactor.getAutoLockOptions()
    }
    
    func selectTimeAutoLock(seconds: Int) -> NSAttributedString {
        //Track events 
        switch seconds {
        case 0:
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.me_security_touch_pass_time_now)
        case 30:
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.me_security_touch_pass_time_30)
        case 60:
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.me_security_touch_pass_time_60)
        case 180:
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.me_security_touch_pass_time_180)
        case 300:
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.me_security_touch_pass_time_300)
        default:
            break
        }
        return self.interactor.selectTimeAutoLock(seconds: seconds)
    }
}

// MARK: ZPSettingInteractorOutputDelegate
extension ZPSettingPresenter: ZPSettingInteractorOutputDelegate {
    
    func setDataSources(_ dataSources: [ZPSettingEntity]) {
        self.controller?.setDataSourcesSetting(dataSources)
    }
    
    func showAutoLockMenu() {
        self.controller?.showAutoLockMenu()
    }
    
    func showManageLoginDevices() {
        self.controller?.showManageLoginDevices()
    }
    
    func updatePassword() {
        guard let controller = controller as? ZPSettingViewController else {
            return
        }
        _ = ZPChangePasswordViewController.showChangePassword(on: controller).subscribe(onNext: { [unowned controller, unowned self](_) in
            self.interactor.setUsingTouchID(false)
            if let entity = controller.dataSources.first, entity.type == .touchID {
                entity.value = false
            }
            controller.reloadData()
            ZPToastNotifyView.showToastDefault(on: controller.view, delay: 0.3, with: R.string_Change_Password_Successful(), completion: {
                ZPTrackingHelper.shared().trackEvent(.me_security_changepassword_result)
            })
            
            }, onError: {
                let message = ($0.userInfoData() as? JSON ?? [:]).value(forKey: "returnmessage", defaultValue: "")
                ZPDialogView.showDialog(with: DialogTypeNotification,
                                        message: message,
                                        cancelButtonTitle: R.string_ButtonLabel_Close(),
                                        otherButtonTitle: nil,
                                        completeHandle: nil)
            
        })
        
    }
    
    func selectEnitity(at indexPath: IndexPath) {
        interactor.selectEnitity(at: indexPath)
    }

    func changeSwitchValue(_ item: ZPSettingEntity, value: Bool) {
        switch item.type {
        case .splashScreen:
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.me_security_touch_cover)
            self.interactor.setAddSplashScreen(value)
        case .password:
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.me_security_touch_privacy)
            if value {
                self.interactor.setAskForPassword(value)
                break
            }
            
            ZPCheckPinViewController.show(withMessage: R.string_Setting_Remove_Ask_Password(),
                                          handleSuccess: { [weak self] (sha) in
                                            self?.interactor.setAskForPassword(false)
                                            self?.setSwitchValue(at: item, value: false)
                },
                                          handleError: {
                                            [weak self] in
                                            self?.interactor.setAskForPassword(true)
                                            self?.setSwitchValue(at: item, value: true)
            })
        case .authenticationOpen:
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.me_security_touch_pass_open)
                        
            if value {
                self.setAuthenticationView(isEnable: value)
                break
            }
            
            ZPCheckPinViewController.show(withMessage: R.string_Setting_Remove_Ask_Password(),
                                          handleSuccess: { [weak self] (sha) in
                                            self?.setAuthenticationView(isEnable: false)
                                            self?.setSwitchValue(at: item, value: false)
                },
                                          handleError: {
                                            [weak self] in
                                            self?.setAuthenticationView(isEnable: true)
                                            self?.setSwitchValue(at: item, value: true)
            })
        case .touchID:
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.me_security_touch_touchid)
            // Add Check Have Touch Id first
            let service = ZPTouchIDService.sharedInstance
            let isEnableTouchId = service.isEnableTouchID() as Bool
            if !isEnableTouchId {
                self.setSwitchValue(at: item, value: false)
                let message = service.checkAndReplaceFaceIdMessage(R.string_TouchID_Enable_System_Setting())
                ZPDialogView.showDialog(with: DialogTypeNotification,
                                        message: message,
                                        cancelButtonTitle: R.string_ButtonLabel_Close(),
                                        otherButtonTitle: [R.string_ButtonLabel_Setting()],
                                        completeHandle: { [weak self](buttonIndex, cancelButtonIndex) in
                                            self?.setSwitchValue(at: item, value: !value)
                                            if buttonIndex == cancelButtonIndex {
                                                return
                                            }
                                            if let url = URL.init(string: UIApplicationOpenSettingsURLString) {
                                                UIApplication.shared.openURL(url)
                                            }
                })
                break
            }
            
            ZPCheckPinViewController.show(withMessage: R.string_Setting_Remove_Ask_Password(),
                                          handleSuccess: { [weak self]  (sha) in
                                            self?.interactor.setUsingTouchID(value)
                                            
                                            if value == false {
                                                self?.interactor.setTurnOffTouchIDManual(true)
                                            }
                                            
                                            self?.setSwitchValue(at: item, value: value)
                                            
                                            if let sha = sha , value {
                                                self?.interactor.saveZaloPayPassword(sha)
                                            }
                },
                                          handleError: {
                                            [weak self] in
                                            self?.interactor.setUsingTouchID(!value)
                                            self?.setSwitchValue(at: item, value: !value)
            })
        default:
            break
        }
    }
    
    func setAuthenticationView(isEnable: Bool) {
        self.interactor.setAuthenticationOpen(isEnable)
        self.setDisableCell(of: .splashScreen, value: isEnable)
        self.setDisableCell(of: .autoLock, value: isEnable.not)
        
        //Nếu tùy chọn "Yêu cầu xác thực khi mở ứng dụng" được bật, cài đặt Multitask Cover không cho phép tắt
        if isEnable {
            self.controller?.setValueForSection(typeItem: ZPSettingtType.splashScreen, value: true)
        }
    }
    
    func setSwitchValue(at item: ZPSettingEntity, value: Bool) {
        self.controller?.setSwitchValue(at: item, value: value)
    }
    
    func setDisableCell(of typeSetting: ZPSettingtType, value: Bool) {
        self.controller?.setDisableCell(of: typeSetting, value: value)
    }
}


