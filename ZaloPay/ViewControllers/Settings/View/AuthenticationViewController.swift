//
//  AuthenticationViewController.swift
//  ZaloPay
//
//  Created by nhatnt on 06/03/2018.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import CocoaLumberjackSwift
import ZaloPayAnalyticsSwift
import ZaloPayProfile
import ZaloPayConfig

class AuthenticationViewController: BaseInputViewController {
    
    // MARK: UI Elements
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var passWordCollectionView: UICollectionView!
    @IBOutlet weak var btnForgetPassword: UIButton!
    @IBOutlet weak var tapGesture: UITapGestureRecognizer!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblNoteTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblForgetPassWordTopConstrain: NSLayoutConstraint!
    @IBOutlet weak var heightContainerView: NSLayoutConstraint!
    @IBOutlet weak var topImageViewConstrain: NSLayoutConstraint!
    @IBOutlet weak var topPassWordCellConstrain: NSLayoutConstraint!
    
    // MARK: Local variables
    var numberPad: ZPAuthNumberPad?
    var presenter: AuthenticationPresenterProtocol?
    let subColor = UIColor(red: 166.0/255, green: 221.0/255, blue: 255.0/255, alpha: 1.0)
    fileprivate var _password: String = ""
    fileprivate var currentInput: Variable<String> = Variable("")
    fileprivate let state: Variable<PasswordState> = Variable(.identify)

    lazy var isIphone5: Bool = UIScreen.main.bounds.height < 667
    lazy var textField: UITextField = {
        let tField = UITextField(frame: .zero)
        self.view.addSubview(tField)
        tField.delegate = self
        tField.keyboardType = .numberPad
        return tField
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupBasicView()
        self.setupFullNameAndAvatar()
        self.setupPhoneNumber()
        self.setupNoteAndDescriptionPassword()
        self.setupForgetPasswordButton()
        self.setupPasswordCollectionView()
    }
    
    override func screenName() -> String! {
        return ZPTrackerScreen.Screen_iOS_PassOpen
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.textField.resignFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.textField.becomeFirstResponder()
        self.setupTouchID()
        ZPTrackingHelper.shared().trackScreen(with: self)
    }
    
    // MARK: Setup UI
    func setupBasicView() {
        self.automaticallyAdjustsScrollViewInsets = false
        self.passWordCollectionView.backgroundColor = .clear
        self.view.backgroundColor = .zaloBase()
        if UIView.isIPhoneX() {
            self.heightContainerView.constant = 440
        }
        
        self.navigationItem.hidesBackButton = true
        self.configShowKeyboardForClearing()
        NotificationCenter.default.addObserver(self, selector: #selector(appBecomeActive), name: Notification.Name.UIApplicationDidBecomeActive, object: nil)
    }
    
    func setupFullNameAndAvatar() {
        //Get Info of user
        guard let currentUser = ZPProfileManager.shareInstance.currentZaloUser else {
            return
        }
        
        //Load avatar
        let width: CGFloat = avatarImageView.frame.width
        self.avatarImageView.zp_roundRect(Float(width / 2))
        self.avatarImageView.layer.borderColor = UIColor.white.cgColor
        self.avatarImageView.layer.borderWidth = 0
        let path = currentUser.avatar 
        let url = URL.init(string: path)
        self.avatarImageView.sd_setImage(with: url, placeholderImage: UIImage.defaultAvatar())
        
        //Load full name
        let name = currentUser.displayName 
        self.lblTitle.text = name
        self.lblTitle.font = UIFont.sfuiTextRegular(withSize: 16)
        self.lblTitle.textColor = UIColor.white
    }
    
    func setupPhoneNumber() {
        var acccountPhone = ZPProfileManager.shareInstance.userLoginData?.phoneNumber.formatPhoneNumber() ?? ""
        acccountPhone = acccountPhone.count > 0 ? acccountPhone : R.string_LeftMenu_ZaloPayIdEmpty()
        
        self.lblPhoneNumber.text = acccountPhone
        self.lblPhoneNumber.font = UIFont.sfuiTextRegular(withSize: 13)
        self.lblPhoneNumber.textColor = subColor
    }
    
    func setupNoteAndDescriptionPassword() {
        //Set other label
        if UIView.isScreen2x() {
            lblNoteTopConstraint.constant = 25
        }
        if isIphone5 {
            topImageViewConstrain.constant = 75
            lblNoteTopConstraint.constant = 20
            lblForgetPassWordTopConstrain.constant = 10
            topPassWordCellConstrain.constant = 15
        }
        if UIView.isIPhone4() {
            topImageViewConstrain.constant = 20
        }
        
        //Setup note input password
        self.lblNote.text = R.string_Setting_Authentication_Enter_Password()
        self.lblNote.font = UIFont.sfuiTextRegular(withSize: 16)
        self.lblNote.textColor = UIColor.white
        
        //Setup description
        self.lblDescription.text = R.string_Setting_Authentication_Enter_Password_Description()
        self.lblDescription.font = UIFont.sfuiTextRegular(withSize: 13)
        self.lblDescription.textColor = subColor
    }
    
    func setupForgetPasswordButton() {
        self.btnForgetPassword.setTitle(R.string_Setting_Authentication_Forget_Password(), for: .normal)
        self.btnForgetPassword.setTitleColor(subColor, for: .normal)
        self.btnForgetPassword.titleLabel?.font = UIFont.sfuiTextSemibold(withSize: 15)
        let textRange = NSMakeRange(0, R.string_Setting_Authentication_Forget_Password().count)
        let attributedText = NSMutableAttributedString(string: R.string_Setting_Authentication_Forget_Password())
        attributedText.addAttribute(NSAttributedStringKey.underlineStyle , value: NSUnderlineStyle.styleSingle.rawValue, range: textRange)
        self.btnForgetPassword.titleLabel?.attributedText = attributedText
    }
    
    func setupPasswordCollectionView() {
        //Register file xib
        self.passWordCollectionView.register(UINib(nibName: "PassWordCell", bundle: nil), forCellWithReuseIdentifier: "passWordCELL")
        
        //Setup numberPad
        numberPad = ZPAuthNumberPad(delegate: self)
        numberPad?.clearButton.setImage(UIImage(named: "ZPAuthNumberPad.bundle/ic_del"), for: .normal)
        numberPad?.backgroundColor = .zaloBase()
        self.textField.inputView = numberPad!
    }
    
    func showTouchIdDialog( cancel: @escaping (() -> Void), error: @escaping ((Error?) -> Void), complete: @escaping ((String?) -> Void)) {
        self.textField.resignFirstResponder()
        let touchIds = ZPTouchIDService.sharedInstance
        let pass = touchIds.currentUserPassword()
        
        if ZPSettingsConfig.sharedInstance().usingTouchId == false || pass.isEmpty {
            cancel()
            return;
        }
        
        touchIds.authenFromTouchIDWithCompleteHandle({ (result) in
            if result == TouchIDResult.userCancel || result == TouchIDResult.activated {
                cancel()
                return
            }
            if result == TouchIDResult.success {
                complete(pass);
                return
            }
            error(nil)
        }, messsage: R.string_Setting_Authentication_Login_By_TouchID())
    }
    
    // MARK: Setup Events
    func isEnableFaceID() -> Bool {
        guard let appConfigs = ApplicationState.sharedInstance.zalopayConfig() else {
            return false
        }
        return appConfigs.value(forPath: "setting.is_enable_face_id", defaultValue: false)
        
    }
    func setupTouchID() {
        if UIView.isIPhoneX() && self.isEnableFaceID().not {
            return
        }
        let isEnableTouchId = ZPTouchIDService.sharedInstance.isEnableTouchID()
        if isEnableTouchId.not {
            return
        }
        self.authenByTouchId().subscribe(onNext: {[weak self] (text) in
            self?.presenter?.scanedTouchId(encodeString: text)
            }, onCompleted: {[weak self] in
                self?.textField.becomeFirstResponder()
        }).disposed(by: disposeBag)
    }
    
    func authenByTouchId() -> Observable<String> {
        return Observable.create({ (s) -> Disposable in
            self.showTouchIdDialog(cancel: {
                s.onCompleted()
            }, error: {(e) in
                let error = NSError(domain: NSURLErrorDomain, code: NSURLErrorDataNotAllowed, userInfo: nil)
                s.onError(error)
            }, complete: {
                s.onNext($0 ?? "")
                s.onCompleted()
            })
            return Disposables.create()
        })
    }
    
    @objc func appBecomeActive() {
        self.textField.becomeFirstResponder()
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIApplicationDidBecomeActive, object: nil)
    }
    
    override func scroll(to view: UIView, using duration: TimeInterval) {
        if isIphone5.not {
            super.scroll(to: view, using: duration)
            return
        }
        DispatchQueue.main.async {
            let c = self.btnForgetPassword.convert(self.btnForgetPassword.bounds, to: self.scrollView)
            let delta = UIScreen.main.bounds.height - (self.bottomConstrainScrollView?.constant ?? 0)
            
            UIView.animate(withDuration: duration, animations: {
                self.heightContainerView.constant = delta + 55
                self.scrollView?.scrollRectToVisible(c, animated: false)
            }, completion: { (completion) in })
        }
    }
    
    override func setupEvent() {
        super.setupEvent()
        
        self.textField.becomeFirstResponder()
        //Tracking action input password
        self.textField.rx.controlEvent(.editingDidBegin).bind {(_) in
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.passopen_input_touch)
        }.disposed(by: disposeBag)
        
        self.btnForgetPassword.rx.tap.bind {
            ZPTrackingHelper.shared().trackEvent(.passopen_forget_touch)
            ZPDialogView.showDialog(with: DialogTypeNotification, title: "",
                                    message: R.string_Setting_Authentication_Forget_Password_Content(),
                                    buttonTitles: [R.string_ButtonLabel_OK(), R.string_ButtonLabel_Cancel()],
                                    handler: {[weak self](idx) in
                                        if idx == 0 {
                                            self?.callSupport()
                                        }
                                        if idx == 1 {
                                            ZPTrackingHelper.shared().trackEvent(.passopen_forget_close)
                                            self?.textField.becomeFirstResponder()
                                        }
                                        self?.input?.becomeFirstResponder()
                                        return
                                    })
            
        }.disposed(by: disposeBag)
        
        tapGesture.rx
            .event
            .filter({[weak self] _ in self?.textField.isFirstResponder == false })
            .subscribe(onNext: { [weak self](_) in
                self?.textField.becomeFirstResponder()
            }).disposed(by: disposeBag)
        
        //collection datasource
        Observable.just(0..<6)
            .bind(to: self.passWordCollectionView.rx.items(cellIdentifier: "passWordCELL", cellType: PassWordCell.self))
            {
                $2.isSelected = $0 < self.currentInput.value.count
            }
            .disposed(by: disposeBag)
        
        //set current input
        textField.rx.text.map { $0 ?? ""}.bind(to: currentInput).disposed(by: disposeBag)
        
        //catch event shouldChangeCharacter
        currentInput.asDriver().drive(onNext: { [weak self] in
            let total = $0.count
            (0..<6).map({ IndexPath(item: $0, section: 0) }).forEach({
                guard let cell = self?.passWordCollectionView.cellForItem(at: $0) as? PassWordCell else {
                    return
                }
                cell.isSelected = $0.row < total
            })
        }).disposed(by: disposeBag)
        
        //Track UI from state
        state.asDriver().drive(onNext: { [weak self] _ in
            self?.updateStateUI()
        }).disposed(by: disposeBag)
        
        // Next state
        currentInput.asDriver().filter({
            return $0.count == 6
        }).debounce(0.2).drive(onNext: { [weak self](s) in
            guard let wSelf = self else { return }
            wSelf.presenter?.didInputPassword(input: s)
        }).disposed(by: disposeBag)
        
        // end
        state.asDriver().filter({ $0 == .end }).drive(onNext: {[weak self] _ in
            self?.inputCorrectPassword()
        }).disposed(by: disposeBag)
    }
    
    
    func inputCorrectPassword() {
        ZPTrackingHelper.shared().trackEvent(.passopen_input_success)
        guard let currentNaviController = UINavigationController.currentActiveNavigationController() else {
            return
        }

        if let currentViewController = currentNaviController.presentedViewController as? AuthenticationViewController {
            currentViewController.dismiss(animated: true, completion: {
                NotificationCenter.default.post(name: Notification.Name.ZPDialogViewHide, object: nil)
                NotificationCenter.default.post(name: Notification.Name.CloseAuthenScreen, object: nil)
            })
            return
        }
    }

    fileprivate func callSupport() {
        ("tel://1900545436"~>UIApplication.shared.bindingUrl).disposed(by: disposeBag)
    }
    
    func resetPasswordInput() {
        _ = Observable.just("").bind(to: self.textField.rx.text)
        self.currentInput.value = ""
    }
    
    func updateStateUI() {
        isCorrect = true
    }
}

extension AuthenticationViewController: AuthenticationViewProtocol {
    func changeToEndState() {
        self.state.value = .end
    }
    
    func showWarningLabel(with message: String) {
        ZPTrackingHelper.shared().trackEvent(.passopen_input_fail)
        self.resetPasswordInput()
        self.lblDescription.text = message
        self.lblDescription.textColor = UIColor.zp_warning_yellow()
    }
}

extension AuthenticationViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let t = textField.text else {
            return true
        }
        let nStr = (t as NSString).replacingCharacters(in: range, with: string)
        let next = nStr.count <= 6
        return next
    }
}

extension AuthenticationViewController: APNumberPadDelegate {
    typealias TypeInput = UIResponder & UITextInput
    func numberPad(_ numberPad: APNumberPad!, functionButtonAction functionButton: UIButton!, textInput: TypeInput!) {
        guard let numberPad = numberPad as? ZPAuthNumberPad else {
            return
        }
        let service = ZPTouchIDService.sharedInstance
        let isEnableTouchId = service.isEnableTouchID()
        if isEnableTouchId.not {
            let message = service.checkAndReplaceFaceIdMessage(R.string_TouchID_Enable_System_Setting())
            ZPDialogView.showDialog(with: DialogTypeNotification,
                                    message: message,
                                    cancelButtonTitle: R.string_ButtonLabel_Close(),
                                    otherButtonTitle: [R.string_ButtonLabel_Setting()],
                                    completeHandle: {(buttonIndex, cancelButtonIndex) in
                                        if buttonIndex == cancelButtonIndex {
                                            self.textField.becomeFirstResponder()
                                            return
                                        }
                                        if let url = URL.init(string: UIApplicationOpenSettingsURLString) {
                                            UIApplication.shared.openURL(url)
                                        }
            })
            return
        }
        if functionButton == numberPad.leftFunctionButton {
            self.setupTouchID()
            return
        }
    }
}

