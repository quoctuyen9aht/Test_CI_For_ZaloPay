//
//  PassWordCell.swift
//  ZaloPay
//
//  Created by nhatnt on 07/03/2018.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit

final class PassWordCell: UICollectionViewCell {
    
    private lazy var vDisplay: UIView = {
        let v = UIView(frame: self.contentView.bounds)
        self.contentView.addSubview(v)
        return v
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
        vDisplay.layer.cornerRadius = vDisplay.bounds.width / 2
        vDisplay.clipsToBounds = true
        vDisplay.layer.borderWidth = 1;
        vDisplay.layer.borderColor = UIColor.white.cgColor
    }
    
    override var isSelected: Bool {
        didSet{
            updateSelect()
        }
    }
    
    func updateSelect() {
        vDisplay.backgroundColor = isSelected.not ? .zaloBase() : .white
    }
}
