//
//  ZPSettingProtocol.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/14/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit

// ZPSettingPresenter -> ZPSettingController
protocol ZPSettingViewControllerInput: class {
    
    var presenter: ZPSettingPresenterDelegate! {get set}
    
    func reloadData()
    
    func showManageLoginDevices()
    
    func showAutoLockMenu()

    func setSwitchValue(at item: ZPSettingEntity, value: Bool)
    
    func setDisableCell(of settingType: ZPSettingtType, value: Bool)
    
    func setValueForSection(typeItem: ZPSettingtType, value: Bool)
    
    func setDataSourcesSetting(_ dataSources: [ZPSettingEntity])
}

// ZPSettingController -> ZPSettingPresenter
protocol ZPSettingPresenterDelegate: class {
    
    var controller: ZPSettingViewControllerInput? {get set}
    
    var interactor: ZPSettingInteractorInputDelegate! {get set}
    
    var router: ZPSettingRouterDelegate! {get set}
    
    func viewDidLoad()
    
    func changeSwitchValue(_ item: ZPSettingEntity, value: Bool)
    
    func selectEnitity(at indexPath: IndexPath)

    func selectTimeAutoLock(seconds: Int) -> NSAttributedString
    
    func getAutoLockOptions() -> [Int]
}

// ZPSettingPresenter -> ZPSettingInteractor
protocol ZPSettingInteractorInputDelegate: class {
    
    var presenter: ZPSettingInteractorOutputDelegate? {get set}
    
    func configureDataSource()
    
    func getAutoLockOptions() -> [Int]
    
    func selectTimeAutoLock(seconds: Int) -> NSAttributedString
    
    func selectEnitity(at indexPath: IndexPath)
    
    func setUsingTouchID(_ value: Bool)
    
    func setAskForPassword(_ value: Bool)
    
    func saveZaloPayPassword(_ sha: String)
    
    func setTurnOffTouchIDManual(_ value: Bool)
    
    func setAuthenticationOpen(_ value: Bool)
    
    func setAddSplashScreen(_ value: Bool)
}

// ZPSettingInteractor -> ZPSettingPresenter
protocol ZPSettingInteractorOutputDelegate: class {
    
    func showManageLoginDevices()
    
    func setDataSources(_ dataSources: [ZPSettingEntity])
    
    func updatePassword()
    
    func showAutoLockMenu()
    
    func setSwitchValue(at item: ZPSettingEntity, value: Bool)
}

protocol ZPSettingRouterDelegate: class {
    
    var rootViewController: UIViewController? {get set}
    
    func popToRootViewController()
        
}
