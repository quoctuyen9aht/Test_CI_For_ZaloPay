//
//  ZPSettingInteractor.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/13/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayConfig

enum contentType : String {
    case title
    case description
    case askPasswordDescription
}

class ZPSettingInteractor: NSObject {
    
    weak var presenter: ZPSettingInteractorOutputDelegate?
    var dataSources: [ZPSettingEntity] = []
    let timesAutoLockArray = [0, 30, 60, 180, 300]
    
    override init() {
        super.init()
        self.observerResetPinNotification()
    }
    func configureDataSource() {
        self.createDataSources()
        presenter?.setDataSources(dataSources)
    }
    
    func observerResetPinNotification () {
        let until = self.rac_willDeallocSignal()
        ZPConnectionManager.sharedInstance().messageSignal.take(until: until).filter { (value) -> Bool in
            if let item = value as? ZPNotifyMessage{
                return item.notificationType == NotificationType.resetPin
            }
            return false
            }.deliverOnMainThread().subscribeNext({ [weak self] (msg) in
                self?.configureDataSource()
            })
    }
    
    func createDataSources() {
        let service = ZPTouchIDService.sharedInstance
        let hasTouchID = service.hasTouchID()
        let contents = self.loadContentWithTouchIDService()
        let isEnableManageLoginDevices = ZPApplicationConfig.getProfileConfig()?.getManageLoginDevices() ?? false
        var dataSources = [ZPSettingEntity]()

        if hasTouchID {
            let touchIdEnable = self.touchIdEnable()
            let item = ZPSettingEntity.item(description: contents[.description] ?? "",
                                            title: contents[.title] ?? "",
                                            value: touchIdEnable, isDisable: false,
                                            type: .touchID)
            dataSources.append(item)
        }

        let item2 = ZPSettingEntity.item(description: contents[.askPasswordDescription] ?? "",
                                         title: R.string_Setting_Ask_Password_Title(),
                                         value: ZPSettingsConfig.sharedInstance().askForPassword, isDisable: false,
                                         type: .password)
//        let item3 = ZPSettingEntity.item(description: R.string_Setting_Authentication_Open_Description(),
//                                         title: R.string_Setting_Authentication_Open_Title(),
//                                         value: ZPSettingsConfig.sharedInstance().authenticationOpen, isDisable: false,
//                                         type: .authenticationOpen)
//        //Nếu tùy chọn "Yêu cầu xác thực khi mở ứng dụng" được bật, cài đặt Multitask Cover không cho phép tắt
//        let item4 = ZPSettingEntity.item(description: R.string_Setting_Splash_Screen_Description(),
//                                         title: R.string_Setting_Splash_Screen_Title(),
//                                         value: ZPSettingsConfig.sharedInstance().addSplashScreen,
//                                         isDisable: ZPSettingsConfig.sharedInstance().authenticationOpen,
//                                         type: .splashScreen)
//
//        let item5 = ZPSettingEntity.item(description: "", title: R.string_Setting_Auto_Lock_Title(),
//                                         value: false,
//                                         isDisable: ZPSettingsConfig.sharedInstance().authenticationOpen.not,
//                                         type: .autoLock)

        let item6 = ZPSettingEntity.item(description: R.string_Setting_Update_Password_Description(),
                                         title: R.string_Setting_Update_Password_Title(),
                                         value: false, isDisable: false,
                                         type: .updatePassword)
        dataSources.append(item2)
//        dataSources.append(item3)
//        dataSources.append(item4)
//        dataSources.append(item5)
        dataSources.append(item6)
        
        if isEnableManageLoginDevices == true {
            let itemSession = ZPSettingEntity.item(description: R.string_Setting_Manage_Login_Device_Description(),
                                                   title: R.string_Setting_Manage_Login_Device_Title(),
                                                   value: false, isDisable: false,
                                                   type: .listSession)
            dataSources.append(itemSession)
        }
            
        let itemLogout = ZPSettingEntity.item(description: "",
                                         title: R.string_LeftMenu_SigOut(),
                                         value: false, isDisable: false,
                                         type: .logout)
        dataSources.append(itemLogout)
    
        self.dataSources = dataSources
    }
    
    func getAutoLockOptions() -> [Int] {
        return timesAutoLockArray
    }
    
    func touchIdEnable() -> Bool {
        let usingTouchId = ZPSettingsConfig.sharedInstance().usingTouchId as Bool;
        let touchIdExist = ZPTouchIDService.sharedInstance.isTouchIDExist() as Bool;
        let isEnableTouchId = ZPTouchIDService.sharedInstance.isEnableTouchID() as Bool;
        let pass = ZPTouchIDService.sharedInstance.currentUserPassword()
        return usingTouchId && touchIdExist && isEnableTouchId && pass.isEmpty.not;
    }

    func setValueDataSources(type: ZPSettingtType, value: Bool) {
        if let item = dataSources.first(where: { $0.type == type}) {
            item.value = value
        }
    }
    
    func loadContentWithTouchIDService() -> [contentType: String]{
        let service = ZPTouchIDService.sharedInstance
        let hasFaceID = service.hasFaceID()
        let hasTouchID = service.hasTouchID()
        var contents: [contentType: String] = [:]
        
        if hasFaceID {
            contents[.title] = R.string_Setting_FaceId_Title()
            contents[.description] = R.string_Setting_FaceId_Description()
            contents[.askPasswordDescription] = R.string_Setting_Ask_Password_FaceID_Description()
            return contents
        }
        
        contents[.title] = R.string_Setting_TouchId_Title()
        contents[.description] = hasTouchID == false ? R.string_Setting_Ask_Password_Description_WithOutTouchID() : R.string_Setting_TouchId_Description()
        contents[.askPasswordDescription] = R.string_Setting_Ask_Password_Description()
        return contents
    }
}

extension ZPSettingInteractor: ZPSettingInteractorInputDelegate {
    public func selectTimeAutoLock(seconds: Int) -> NSAttributedString {
        ZPSettingsConfig.sharedInstance().autoLockTime = seconds.toDouble()
        var detailString : String

        if seconds % 60 != 0 {
            detailString = "\nSau " + String(format: R.string_Setting_Auto_Lock_After_Second_Title(), seconds.toString())
        } else {
            detailString = "\nSau " + String(format: R.string_Setting_Auto_Lock_After_Minute_Title(), (seconds / 60).toString())
        }
        if seconds == 0 {
            detailString = "\n" + R.string_Setting_Auto_Lock_Description()
        }

        let longString = R.string_Setting_Auto_Lock_Title() + detailString
        
        let attributesRegular : [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : UIFont.sfuiTextRegular(withSize: UILabel.mainTextSize()), NSAttributedStringKey.foregroundColor : UIColor.defaultText()]
        let detailAttributes : [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : UIFont.sfuiTextRegular(withSize: UILabel.zpDetailGraySize()), NSAttributedStringKey.foregroundColor : UIColor.subText()]

        let attributedString = NSMutableAttributedString.init(string: longString, attributes: attributesRegular)
        let detailStringRange = (longString as NSString).range(of: detailString)
        attributedString.setAttributes(detailAttributes, range: detailStringRange)
        return attributedString
    }
    
    func selectEnitity(at indexPath: IndexPath) {
        let item = self.dataSources[indexPath.section]

        switch item.type {
        case .listSession:
            presenter?.showManageLoginDevices()
        case .updatePassword:
            presenter?.updatePassword()
        case .autoLock:
            if ZPSettingsConfig.sharedInstance().authenticationOpen {
                presenter?.showAutoLockMenu()
            }
        default:
            return
        }
    }
    
    func setUsingTouchID(_ value: Bool) {
        ZPSettingsConfig.sharedInstance().usingTouchId = value
        self.setValueDataSources(type: ZPSettingtType.touchID, value: value)
    }
    
    func setTurnOffTouchIDManual(_ value: Bool) {
        if !ZPSettingsConfig.sharedInstance().turnOffTouchIdManual {
            ZPSettingsConfig.sharedInstance().turnOffTouchIdManual = value
        }
    }
    
    func setAskForPassword(_ value: Bool) {
        ZPSettingsConfig.sharedInstance().askForPassword = value
        self.setValueDataSources(type: ZPSettingtType.password, value: value)
    }
    
    func setAuthenticationOpen(_ value: Bool) {
        ZPSettingsConfig.sharedInstance().authenticationOpen = value
        //Nếu tùy chọn "Yêu cầu xác thực khi mở ứng dụng" được bật, cài đặt Multitask Cover luôn bật
        if value {
           self.setValueDataSources(type: ZPSettingtType.splashScreen, value: true)
        }
        self.setValueDataSources(type: ZPSettingtType.autoLock, value: value)
        self.setValueDataSources(type: ZPSettingtType.authenticationOpen, value: value)
    }
    
    func setAddSplashScreen(_ value: Bool) {        
        ZPSettingsConfig.sharedInstance().addSplashScreen = value
        self.setValueDataSources(type: ZPSettingtType.splashScreen, value: value)
    }
    
    func saveZaloPayPassword(_ sha: String) {
        ZPTouchIDService.sharedInstance.saveZaloPayPassword(sha)
    }
}
