//
//  AuthenticationPresenter.swift
//  ZaloPay
//
//  Created by nhatnt on 10/04/2018.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import UIKit

class AuthenticationPresenter: AuthenticationPresenterProtocol {
    weak var view: AuthenticationViewProtocol?
    var interactor: AuthenticationInteractorProtocol?
    var router: AuthenticationRouterProtocol?
 
    func didInputPassword(input: String) {
        self.interactor?.authenticatePassword(input)
    }
    
    func scanedTouchId(encodeString: String) {
        self.interactor?.authenticateEncodeString(encodeString)
    }
}

extension AuthenticationPresenter: AuthenticationInteractorCallBackProtocol {
    func authenticatedPassword() {
        self.view?.changeToEndState()
    }
    
    func warningNotice(with message: String) {
        self.view?.showWarningLabel(with: message)
    }
}
