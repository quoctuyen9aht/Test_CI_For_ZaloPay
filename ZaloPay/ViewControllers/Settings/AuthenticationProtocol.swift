//
//  AuthenticationProtocol.swift
//  ZaloPay
//
//  Created by nhatnt on 10/04/2018.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation

protocol AuthenticationViewProtocol: class {
    var presenter: AuthenticationPresenterProtocol? {get set}
    
    func changeToEndState()
    func showWarningLabel(with message: String)
}

protocol AuthenticationPresenterProtocol: class {
    var view: AuthenticationViewProtocol? {get set}
    var interactor: AuthenticationInteractorProtocol? {get set}
    var router: AuthenticationRouterProtocol? {get set}
    
    func didInputPassword(input: String)
    func scanedTouchId(encodeString: String)
}

protocol AuthenticationRouterProtocol: class {

}

protocol AuthenticationInteractorProtocol: class {
    var presenter: AuthenticationInteractorCallBackProtocol? {get set}

    func authenticatePassword(_ passwordString: String)
    func authenticateEncodeString(_ inputString: String)
}

protocol AuthenticationInteractorCallBackProtocol: class {
    func authenticatedPassword()
    func warningNotice(with message: String)
}

