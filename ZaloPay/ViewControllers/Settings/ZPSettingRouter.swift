//
//  ZPSettingRouter.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/13/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
@objcMembers
class ZPSettingRouter: NSObject, ZPSettingRouterDelegate {

    weak var rootViewController: UIViewController?
    
    class func assemblyModule() -> ZPSettingViewController {
        let settingController: ZPSettingViewController = ZPSettingViewController()
        
        let presenter: ZPSettingPresenter = ZPSettingPresenter()
        let interactor: ZPSettingInteractor = ZPSettingInteractor()
        let router: ZPSettingRouter = ZPSettingRouter()
        
        presenter.controller = settingController
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.rootViewController = settingController
        
        settingController.presenter = presenter
        
        return settingController
    }
    
    func popToRootViewController() {
        guard let controller = rootViewController else {
            return
        }
        
        controller.navigationController?.popToViewController(controller, animated: true)
    }
    
}
