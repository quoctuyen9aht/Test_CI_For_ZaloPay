//
//  AuthenticationInteractor.swift
//  ZaloPay
//
//  Created by nhatnt on 10/04/2018.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class AuthenticationInteractor: AuthenticationInteractorProtocol {
    weak var presenter: AuthenticationInteractorCallBackProtocol?
    let disposeBag = DisposeBag()
    
    func authenticatePassword(_ passwordString: String) {
        guard let encrypt = (passwordString as NSString).sha256() else {
            return
        }
        self.authenticateEncodeString(encrypt)
    }
    
    func authenticateEncodeString(_ inputString: String) {
        let signal = NetworkManager.sharedInstance().validatePin(inputString)
        
        eventIgnoreValue(from: signal).observeOn(MainScheduler.instance).observeOn(MainScheduler.instance).subscribe(onNext: { [weak self](_) in
                self?.presenter?.authenticatedPassword()
            }, onError: { [weak self](e) in
                var message: String
                message = e.userInfoData()["returnmessage"] as? String ?? ""
                if e.errorCode() == NSURLErrorNotConnectedToInternet {
                    message = R.string_NetworkError_NoConnectionMessage()
                }
                self?.presenter?.warningNotice(with: message)
        }).disposed(by: disposeBag)
    }
}



