//
//  ZPSettingCell.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/13/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit

public protocol ZPSettingCellDelegate: class {
    func changeValueSwitch(_ item: ZPSettingEntity, value: Bool)
}

public class ZPSettingCell: UITableViewCell {
    
    public var delegate: ZPSettingCellDelegate?
    weak var item: ZPSettingEntity?
    
    public lazy var titleLabel: UILabel! = {
        let titleLabel = UILabel().build({
            $0.zpMainBlackRegular()
            self.contentView.addSubview($0)
            $0.numberOfLines = 0
            $0.snp.makeConstraints { (make) in
                make.left.equalTo(10)
                make.right.equalTo(-50)
                make.top.bottom.equalTo(0)
            }
        })
        
        return titleLabel
    }()
    
    public lazy var valueSwitch: UISwitch! = {
        let valueSwitch = UISwitch().build({
            self.contentView.addSubview($0)
            $0.addTarget(self, action: #selector(swithchDidChange), for: .valueChanged)
        })
        valueSwitch.snp.makeConstraints { (make) in
            make.right.equalTo(-10)
            make.centerY.equalTo(self.contentView.snp.centerY)
            make.size.equalTo(valueSwitch.frame.size)
        }
        
        return valueSwitch
    }()
    
    public lazy var arrowImageView: UIImageView! = {
        let arrowImageView = UIImageView(image: UIImage(named: "right_arrow"))
        self.contentView.addSubview(arrowImageView)
        arrowImageView.snp.makeConstraints { (make) in
            make.right.equalTo(-10);
            make.centerY.equalTo(self.contentView.snp.centerY)
            make.size.equalTo(arrowImageView.frame.size)
        }
        
        return arrowImageView
    }()
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        
        self.item = nil
        self.titleLabel.text = nil
        self.valueSwitch.isOn = false
        self.valueSwitch.isHidden = false
        self.arrowImageView.isHidden = false
    }
    
    func bindingUI(_ item: ZPSettingEntity) {
        self.item = item
        let isSelection = item.type == .updatePassword || item.type == .listSession || item.type == .autoLock
        let isHiddenImage = isSelection ? false : true
        let isHiddenSwitch = isSelection ? true : false
        let selectionType: UITableViewCellSelectionStyle = isSelection ? .default : .none

        setView(item.title,
                value: item.value,
                isHiddenImage: isHiddenImage,
                isHiddenSwitch: isHiddenSwitch,
                isDisable: item.isDisable,
                selectionType: selectionType)
    }
    
    public func setView(_ title: String, value: Bool, isHiddenImage: Bool, isHiddenSwitch: Bool, isDisable: Bool, selectionType: UITableViewCellSelectionStyle) {
        self.titleLabel.text = title
        self.valueSwitch.isOn = value
        
        self.valueSwitch.isHidden = isHiddenSwitch
        self.arrowImageView.isHidden = isHiddenImage
        self.selectionStyle = selectionType
        
        if isDisable {
            self.titleLabel.alpha = 0.5
            self.valueSwitch.alpha = 0.5
            self.valueSwitch.isUserInteractionEnabled = false
        } else {
            self.titleLabel.alpha = 1
            self.valueSwitch.alpha = 1
            self.valueSwitch.isUserInteractionEnabled = true
        }
    }
    
    // MARK: Action's
    @objc func swithchDidChange() {
        guard let item = self.item else {
            return
        }
        delegate?.changeValueSwitch(item, value: self.valueSwitch.isOn)
    }

}
