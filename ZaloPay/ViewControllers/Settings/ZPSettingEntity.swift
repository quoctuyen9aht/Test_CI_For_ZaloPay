//
//  ZPSettingModel.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/13/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayAnalyticsSwift

public enum ZPSettingtType {
    case touchID
    case password
    case updatePassword
    case listSession
    case logout
    case splashScreen
    case authenticationOpen
    case autoLock
    
    var eventID: ZPAnalyticEventAction {
        switch self{
        case .touchID:
            return .me_security_touch_touchid
        case .password:
            return .me_security_touch_privacy
        case .updatePassword:
            return .me_security_touch_changepassword
        case .listSession:
            return .me_security_touch_changepassword
        case .logout:
            return .me_security_touch_logout
        case .splashScreen:
            return .me_security_touch_cover
        case .authenticationOpen:
            return .me_security_touch_pass_open
        case .autoLock:
            return .me_security_touch_pass_time_set
        }
    }
}

public class ZPSettingEntity: NSObject {

    public var type: ZPSettingtType = .touchID
    public var sectionDescription: String = ""
    public var title: String = ""
    public var value: Bool = false
    public var height: CGFloat = 0
    public var isDisable: Bool = false
    public var eventID: ZPAnalyticEventAction = .me_security_touch_touchid

    
    public static func item(description: String, title: String, value: Bool, isDisable: Bool, type: ZPSettingtType) -> ZPSettingEntity {
        let font: UIFont = UIFont.sfuiTextRegular(withSize: UILabel.zpDetailGraySize()) ?? UIFont.systemFont(ofSize: 15)
        let w = UIScreen.main.bounds.width - 20
        let allStrings = description.components(separatedBy: "\n")
        var height = description.size(w,using: font).height
        if allStrings.count > 1 {
            // Tracking if first line is fit screen
            let fString = allStrings.first ?? ""
            let sString = fString.size(maxHeight: font.lineHeight, using: font)
            let isFitScreen = sString.width <= w
            
            // Case fit screen , don't need \n
            if isFitScreen {
                height = allStrings.joined().size(w,using: font).height
            }
        }
        
        return ZPSettingEntity().build {
            $0.type = type
            $0.sectionDescription = description
            $0.title = title
            $0.value = value
            $0.height = height
            $0.isDisable = isDisable
            $0.eventID = type.eventID
        }
    }

}
