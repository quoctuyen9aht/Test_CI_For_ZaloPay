//
//  TabbarViewController.swift
//  ZaloPay
//
//  Created by Dung Vu on 3/23/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift
import CocoaLumberjackSwift
import ZaloPayAnalyticsSwift
import ZaloPayWeb

enum TabIndex: Int {
    case Home = 0, Transaction, Promotion, Personal
}
protocol TrackEventProtocol: class {
    var eventTracking: ZPAnalyticEventAction? { get set }
}

@objc protocol TabbarViewControllerDelegate: class {
    func shouldChange(to controller: UIViewController) -> Bool
}

extension UIViewController: TrackEventProtocol {
    fileprivate struct EventTrackingController {
        static var Name = "EventTrackingControllerName"
    }
    
    var eventTracking: ZPAnalyticEventAction? {
        get{
            return objc_getAssociatedObject(self, &EventTrackingController.Name) as? ZPAnalyticEventAction
        }
        set{
            objc_setAssociatedObject(self, &EventTrackingController.Name, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}

final class TabbarViewController: UITabBarController {

    fileprivate var isAtHome: Bool = false
    weak var delegateChange: TabbarViewControllerDelegate?
    @objc dynamic fileprivate(set) var idxSelected: Int = TabIndex.Home.rawValue
    
    private let cacheDataTable: ZPCacheDataTable = ZPCacheDataTable(db: PerUserDataBase.sharedInstance())
    
    
    override var selectedIndex: Int {
        didSet{
            idxSelected = selectedIndex
        }
    }
    
    fileprivate var bgViewTabbar: UIView? {
        didSet{
            guard bgViewTabbar != nil else {
                return
            }
            setupBgTabbar()
            checkBadgePromotion()
        }
    }
    fileprivate let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTabbar()
        observerMessageSignal()
        
        NotificationCenter.default.rx
            .notification(Notification.Name.UIApplicationWillChangeStatusBarFrame)
            .delay(0.15, scheduler: MainScheduler.instance)
            .bind
            { [weak self](n) in
            let userInfo = n.userInfo
            let rect = (userInfo?[UIApplicationStatusBarFrameUserInfoKey] as? NSValue)?.cgRectValue ?? .zero
            let h = rect.height
            let allBar = self?.viewControllers?.compactMap({ $0 as? UINavigationController }).map({ $0.navigationBar })
            let offSetY = h - 20
                func calculateNewFrame(from f:CGRect) -> CGRect {
                    var f = f
                    f.origin.y = 0
                    f.size.height = 64 - offSetY
                    return f
                }
            
            allBar?.forEach({
                $0.frame = calculateNewFrame(from: $0.frame)
            })
            
            }.disposed(by: disposeBag)
    }
    
    fileprivate func setupBgTabbar() {
        guard let bg = bgViewTabbar else { return }
        bg.backgroundColor = .clear
        let subs = bg.subviews
        
        let effectView = subs.compactMap({ $0 as? UIVisualEffectView }).first
        effectView?.backgroundColor = UIColor(hexValue: 0xF8F8F8)
        effectView?.effect = nil//UIBlurEffect(style: .light)
        effectView?.removeFromSuperview()
        
        let v = UIView(frame: self.tabBar.bounds)
        v.alpha = 0
        self.tabBar.insertSubview(v, at: 1)
        
    }
    
    fileprivate func setupTabbar() {
        self.delegate = self
        self.view.backgroundColor = UIColor.white
        
        // Handler change app state
        NotificationCenter.default.rac_addObserver(forName: NSNotification.Name.UIApplicationDidBecomeActive.rawValue, object: nil).subscribeNext { [weak self](_) in
            guard let wSelf = self, wSelf.isAtHome else { return }
            wSelf.changeFrameSubviews()
        }
        
        _ = self.rx.methodInvoked(#selector(viewDidLayoutSubviews)).takeUntil(self.rx.deallocating).filter({[weak self] _ in
            guard let wSelf = self else {
                return false
            }
            return wSelf.bgViewTabbar == nil
        }).bind { [weak self](_) in
            self?.tabBar.subviews.forEach({
                print("\(type(of: $0))")
            })
            self?.bgViewTabbar = self?.tabBar.subviews.first(where: { "\(type(of: $0))" == "_UIBarBackground" })
        }
        
        self.view.clipsToBounds = false
        
        let vLine = UIView(frame: CGRect(origin: CGPoint(x: 0, y: -1), size: CGSize(width: self.tabBar.bounds.width, height: 1)))
        vLine.autoresizingMask = .flexibleWidth
        vLine.backgroundColor = UIColor.line()
        self.tabBar.addSubview(vLine)
        
        self.tabBar.backgroundColor = .clear
//        self.tabBar.barStyle = .black
    }
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        changeFrameSubviews(isShowToolbar: !self.tabBar.isHidden)
        
    }
    
    func checkBadgePromotion()
    {
        if let _ = cacheDataTable.cacheDataValue(forKey: Key_Store_Database_Promotion) {
            self.tabBar.items?[TabIndex.Promotion.rawValue].badgeValue = "N"
        }
        else{
            self.tabBar.items?[TabIndex.Promotion.rawValue].badgeValue = nil
        }
    }
    
    func clearBadge()
    {
        self.tabBar.items?[TabIndex.Promotion.rawValue].badgeValue = nil
        cacheDataTable.cacheDataUpdateValue(nil, forKey:Key_Store_Database_Promotion)
        guard let tabPromotion = self.viewControllers?[safe: TabIndex.Promotion.rawValue] as? UINavigationController else {
            return
        }
        if let homeWebPromotion = tabPromotion.viewControllers[0] as? ZPPromotionTabWebApp {
            homeWebPromotion.reloadWebview()
        }
    }
    
    func showBadge()
    {
        self.tabBar.items?[TabIndex.Promotion.rawValue].badgeValue = "N"
        cacheDataTable.cacheDataUpdateValue("1", forKey:Key_Store_Database_Promotion)
    }
    
    func observerMessageSignal()
    {
        ZPConnectionManager.sharedInstance().messageSignal.filter { (value) -> Bool in
            if let item = value as? ZPNotifyMessage{
                return item.notificationType == NotificationType.promotion
            }
            return false
        }.deliverOnMainThread().subscribeNext({ (promotion) in
            if self.idxSelected == TabIndex.Promotion.rawValue
            {
                self.clearBadge()
            }
            else
            {
                self.showBadge()
            }
        })
    }
}

// MARK: - Public function
extension TabbarViewController {
    public func toolbar(willHidden hidden:Bool) {
        isAtHome = hidden.not
        showToolbar(isShow: hidden.not)
        changeFrameSubviews(isShowToolbar: hidden.not)
    }
}

// MARK: - Private function
extension TabbarViewController {
    fileprivate func showToolbar(isShow: Bool) {
        self.tabBar.isHidden = !isShow
    }
    
    fileprivate func changeFrameSubviews(isShowToolbar: Bool = true) {
        let nHeight = self.view.bounds.size.height - (isShowToolbar ? self.tabBar.frame.size.height : 0)
        self.childViewControllers.forEach({
            if $0.isViewLoaded {
                $0.view.clipsToBounds = false
                $0.view.frame = { () -> CGRect in
                    var f = self.view.frame
                    f.size.height = nHeight
                    return f;
                }()
            }
            $0.view.subviews.first?.clipsToBounds = false
            guard let nav = $0 as? UINavigationController else { return }
            nav.delegate = self
        })
    }
}

extension TabbarViewController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        self.updateTabbarUI(navigationController: navigationController, willShow: viewController)
        let tc = navigationController.topViewController?.transitionCoordinator
        tc?.notifyWhenInteractionEnds({ [weak self] (context) in
            guard let fromViewController = context.viewController(forKey: .from) as? BaseViewController else {
                return
            }
            
            if !context.isCancelled {
                let eventId = fromViewController.backEventId()
                if let eventValue = ZPAnalyticEventAction(rawValue: Int(eventId)) {
                    ZPTrackingHelper.shared().trackEvent(eventValue)
                }
                fromViewController.viewWillSwipeBack()
                return
            }
            self?.updateTabbarUI(navigationController: navigationController, willShow: fromViewController)
        });
    }
    
    func updateTabbarUI(navigationController: UINavigationController, willShow viewController: UIViewController) {
        let idx = navigationController.viewControllers.index(of: viewController)
        self.isAtHome = (idx == TabIndex.Home.rawValue)
        
        navigationController.view.frame = { () -> CGRect in
            var f = self.view.frame
            if (isAtHome) { f.size.height -= self.tabBar.frame.size.height }
            return f
        }()
        if isAtHome {
            let v = navigationController.visibleViewController?.view.subviews.first
            v?.clipsToBounds = false
        }
        showToolbar(isShow: isAtHome)
        
    }
}
extension TabbarViewController: UITabBarControllerDelegate {    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        changeFrameSubviews()
        self.idxSelected = self.selectedIndex
        
        if self.idxSelected == TabIndex.Promotion.rawValue && self.tabBar.items?[self.idxSelected].badgeValue != nil  {
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.home_touch_promotion_new)
            clearBadge()
            return
        }
        if let eventTracking = viewController.eventTracking {
            ZPTrackingHelper.shared().trackEvent(eventTracking)
        }
    }
}
