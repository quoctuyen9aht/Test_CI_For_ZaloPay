//
//  ZPRechargeItem.h
//  ZaloPay
//
//  Created by bonnpv on 5/7/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZPRechargeBaseItem : NSObject
@property (nonatomic, readonly) Class cellClass;
@end

@interface ZPRecharMinvalueItem : ZPRechargeBaseItem
@property (nonatomic, strong) NSString *title;
@end

@interface ZPRechargeInputItem : ZPRechargeBaseItem
@end

@interface ZPRechargeHeaderItem : ZPRechargeBaseItem
@end
@interface ZPRechargeButtonBottomItem : ZPRechargeBaseItem
@end
