//
//  ZPRechargeItem.m
//  ZaloPay
//
//  Created by bonnpv on 5/7/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPRechargeItem.h"
#import "ZPRechargeCell.h"

@implementation ZPRechargeBaseItem
- (Class)cellClass {
    return [ZPRechargeBaseCell class];
}
@end

@implementation ZPRecharMinvalueItem
- (Class)cellClass {
    return [ZPRechargeMinValueTitleCell class];
}
@end

@implementation ZPRechargeInputItem
- (Class)cellClass {
    return [ZPRechargeInputCell class];
}
@end

@implementation ZPRechargeHeaderItem
- (Class)cellClass {
    return [ZPRechargeHeaderCell class];
}
@end
@implementation ZPRechargeButtonBottomItem
- (Class)cellClass {
    return [ZPRechargeButtonBottomCell class];
}
@end

