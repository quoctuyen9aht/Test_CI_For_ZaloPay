//
//  ZPRechargeInteractor.swift
//  ZaloPay
//
//  Created by nhatnt on 25/09/2017.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import UIKit

class ZPRechargeInteractor: ZPRechargeInteractorDelegate {    
    weak var presenter: ZPRechargePresenterDelegate?
    var dataSources: [ZPRechargeEntity] = []
    var min: UInt64 = 0
    var max: UInt64 = 0
    
    func setupMinMaxValue() {
        self.max = UInt64(ZPWalletManagerSwift.sharedInstance.config.maxRechargeValue )
        self.min = UInt64(ZPWalletManagerSwift.sharedInstance.config.minRechargeValue )
    }
    
    func createDateSources() {
        setupMinMaxValue()
        var dataSources = [ZPRechargeEntity]()
        let textMinValue: String = NSNumber(value: self.min).formatMoneyValue()
        let itemMinValue = ZPRechargeEntity.init(title: String(format: R.string_Recharge_Min_Value(), textMinValue), sectionDescription: "", value: false, type: RechargeType.MinValueItem)
        dataSources.append(itemMinValue)
        
        let itemInput = ZPRechargeEntity.init(title: String(format: R.string_AddCash_Title()), sectionDescription: "", value: false, type: RechargeType.InputItem)
        dataSources.append(itemInput)
        
        self.dataSources = dataSources
    }
    
    func configureDataSource() -> [ZPRechargeEntity] {
        self.createDateSources()
        return self.dataSources
    }
    
    func validInputData(totalValue: Int64?) -> Bool{
        if totalValue == nil || totalValue! < self.min {
            let money = NSNumber(value: self.min).formatCurrency()
            let message = NSString.localizedStringWithFormat(R.string_InputMoney_SmallAmount_Error_Message()! as NSString, money!)
            presenter?.showMessageInputData(message: message)
            return false
        }
        if totalValue! % 10000 != 0 {
            presenter?.showInvalidInputData()
            return false
        }
        if self.isGreaterMaxValue(totalValue: totalValue!) {
            return false
        }
        
        return true
    }
    
    func isGreaterMaxValue(totalValue: Int64) -> Bool{
        if totalValue > self.max {
            let money = NSNumber(value: self.max).formatCurrency()
            let message = NSString.localizedStringWithFormat(R.string_InputMoney_BigAmount_Error_Message()! as NSString, money!)
            presenter?.showMessageInputData(message: message)
            return true
        }
        return false
    }
    
    func requestCreateOrder(totalMoney: Int) {
        let appUser = NetworkManager.sharedInstance().paymentUserId
        let description = R.string_AddCash_Description_Message()
        let item = [ZPCreateWalletOrderTranstypeKey: ZPTransType.walletTopup.rawValue] as NSDictionary
        let itemStr = item.jsonRepresentation()
        
        NetworkManager.sharedInstance().getWalletOrdeder(withAmount: totalMoney, andTransactionType: Int32(ZPTransType.withDraw.rawValue), appUser: appUser, description: description, appId: kZaloPayClientAppId , embeddata: "", item: itemStr)
            .deliverOnMainThread()
            .subscribeNext({[weak self] json in
                guard let dict = json as? [String : Any] else {
                    return
                }

                let billInfo = ZPBill(data: dict,
                                      transType: ZPTransType.walletTopup,
                                      orderSource: Int32(OrderSource_None.rawValue),
                                      appId:kZaloPayClientAppId)
                self?.processBill(billInfo: billInfo, appId: kZaloPayClientAppId)
                }, error: {[weak self] error in
                    self?.presenter?.rechargeMoneyFailWithError(error: error!)
            })
    }
    
    func processBill(billInfo: ZPBill, appId: Int) {
        self.presenter?.processBillSuccessfully(appId: appId, billInfo: billInfo)
    }
}

