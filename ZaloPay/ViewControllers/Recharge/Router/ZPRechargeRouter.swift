//
//  ZPRechargeRouter.swift
//  ZaloPay
//
//  Created by nhatnt on 25/09/2017.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

@objcMembers
class ZPRechargeRouter: NSObject, ZPRechargeRouterDelegate {
    weak var view: UIViewController?
    
    class func assemblyModule() -> ZPRechargeViewController {
        let viewController = ZPRechargeViewController()
        let interactor = ZPRechargeInteractor()
        let router = ZPRechargeRouter()
        let presenter = ZPRechargePresenter(interface: viewController, interactor: interactor, router: router)
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        router.view = viewController
        viewController.presenter = presenter
        
        return viewController;
    }
    
    func popToRootViewController() {
        self.view?.navigationController?.popToRootViewController(animated: true)
    }
    
    func popToViewController(view: ZPRechargeViewDelegate, animated: Bool) {
        guard let vc = view as? UIViewController else {
            return
        }
        self.view?.navigationController?.popToViewController(vc, animated: animated)
    }
}


