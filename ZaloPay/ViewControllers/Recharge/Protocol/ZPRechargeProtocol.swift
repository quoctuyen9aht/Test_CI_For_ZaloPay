//
//  ZPRechargeProtocol.swift
//  ZaloPay
//
//  Created by nhatnt on 25/09/2017.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation

//MARK: ZPRechargeInteractor
protocol ZPRechargeInteractorDelegate: class {
    var presenter: ZPRechargePresenterDelegate? {get set}
    func configureDataSource() -> [ZPRechargeEntity]
    func processBill(billInfo: ZPBill, appId: Int)
    func requestCreateOrder(totalMoney: Int)
    func validInputData(totalValue: Int64?) -> Bool
    func isGreaterMaxValue(totalValue: Int64) -> Bool
}

//MARK: ZPRechargeView
protocol ZPRechargeViewDelegate: class {
    var presenter: ZPRechargePresenterDelegate? {get set}
    
    func alertRechargeFail(error: Error?)
    func showInvalidRechargeMoney()
    func showMessageInputData(message: NSString)
    func showLoadingView()
    func hideLoadingView()
}

//MARK: ZPRechargePresenter
protocol ZPRechargePresenterDelegate: class {
    var view: ZPRechargeViewDelegate? {get set}
    var interactor: ZPRechargeInteractorDelegate? { get set }
    var router: ZPRechargeRouterDelegate? { get set }
    
    func requestDataSource() -> [ZPRechargeEntity]?
    func processBillSuccessfully(appId: Int, billInfo: ZPBill)
    func rechargeMoneyFailWithError(error: Error?)
    func didSelectMoneyNumber(totalMoney: Int)
    func isValidInputData(totalValue: Int64?) -> Bool
    func isGreaterMaxValue(totalValue: Int64) -> Bool
    func showInvalidInputData()
    func showMessageInputData(message: NSString)
}

//MARK: Router
protocol ZPRechargeRouterDelegate: class {
    var view: UIViewController? {get set}
    func popToRootViewController()
    func popToViewController(view: ZPRechargeViewDelegate, animated: Bool)
}

