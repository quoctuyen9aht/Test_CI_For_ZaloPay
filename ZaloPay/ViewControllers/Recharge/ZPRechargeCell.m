//
//  ZPRechargeCell.m
//  ZaloPay
//
//  Created by bonnpv on 5/7/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPRechargeCell.h"
#import "ZPRechargeItem.h"
#import "PMNumberPad.h"
#import <ZaloPayCommon/UILabel+ZaloPayStyle.h>
#import ZALOPAY_MODULE_SWIFT
@interface ZPRechargeBaseCell ()

@end

@implementation ZPRechargeBaseCell
- (void)setItem:(ZPRechargeBaseItem *)item {

}
- (UIView*)seperater {
    if (!_seperater) {
        float startX = 0;
        _seperater = [[UIView alloc] initWithFrame:CGRectMake(startX, [[self class] height] -1, self.frame.size.width - startX, 1)];
        _seperater.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin;
        _seperater.backgroundColor = [UIColor lineColor];
        [self addSubview:_seperater];
    }
    return _seperater;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self seperater];
}

@end

#pragma mark - ZPRechargeMinValueTitleCell

@interface ZPRechargeMinValueTitleCell ()

@end

@implementation ZPRechargeMinValueTitleCell

+ (float)height {
    return 40.0;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    UIColor *textColor =  [UIColor subText];
    self.labelMinvalue.textColor =  textColor;
    self.labelvnd.textColor =  textColor;
    
    self.labelvnd.font = [UIFont SFUITextRegularWithSize:9];
    [self.labelMinvalue zpSubTextGrayRegular];
}

- (void)setItem:(ZPRechargeBaseItem *)item {
    ZPRecharMinvalueItem *minItem = (ZPRecharMinvalueItem *)item;
    self.labelMinvalue.text = minItem.title;
}

@end

#pragma mark - ZPRechargeInputCell

@interface ZPRechargeInputCell ()<APNumberPadDelegate, UITextFieldDelegate>
@end

@implementation ZPRechargeInputCell
+ (float)height {
    return 60.0;
}

+ (instancetype)cellFromSameNibForTableView:(UITableView *)tableView {
    return [self defaultCellForTableView:tableView];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupCell];
    }
    return self;
}

- (void)resignTextField {
    [self.textFieldMoney resignFirstResponder];
}

- (void)setupCell {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.textFieldMoney = [[ZPFloatTextInputView alloc] init];
    [self addSubview:self.textFieldMoney];
    self.textFieldMoney.textField.placeholder = [R string_AddCash_TextPlaceHolder];
    
    float w = [UIScreen mainScreen].applicationFrame.size.width;
    [self.textFieldMoney mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.left.equalTo(0);
        make.width.equalTo(w);
        make.height.equalTo(60);
    }];
    
    self.textFieldMoney.textField.delegate = self;
    @weakify(self);
    self.textFieldMoney.textField.inputView = ({
        @strongify(self);
        PMNumberPad *numberPad = [PMNumberPad numberPadWithDelegate:self];
        numberPad;
    });
    
    [[self.textFieldMoney.textField rac_textSignal] subscribeNext:^(NSString *text) {
        @strongify(self);
        [self updateText:text];
    }];
}

- (void)updateText:(NSString *)text {
    DDLogInfo(@"text :%@",text);
    if ([text length] == 0) {
        return;
    }
    UITextField *tField = self.textFieldMoney.textField;
    NSString *textFormat = [[text onlyDigit] formatMoneyValue];
    tField.text = textFormat;
    UITextPosition *nPostion = [tField positionFromPosition:[tField beginningOfDocument] offset:textFormat.length];
    UITextPosition *ePostion = [tField endOfDocument];
    dispatch_async(dispatch_get_main_queue(), ^{
        [tField setSelectedTextRange:[tField textRangeFromPosition:ePostion toPosition:nPostion]];
    });
    
}

#pragma mark - APNumberPadDelegate

- (void)numberPad:(PMNumberPad *)numberPad functionButtonAction:(UIButton *)functionButton textInput:(UIResponder<UITextInput> *)textInput {
    if (functionButton == numberPad.leftFunctionButton) {
        if ([self.inputDelegate textFieldShouldChange:self text:@"000"]) {
            UITextField *textField = (UITextField *)textInput;
            [self updateText:[NSString stringWithFormat:@"%@000",textField.text]];
        }
    } else if (functionButton == numberPad.doneButton) {
        [self.inputDelegate inputCellDidDoneInput];
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    return [self.inputDelegate textFieldShouldChange:self text:string];
}


@end

@implementation ZPRechargeHeaderCell
+ (float)height {
        return 167.0;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    
    _TopView.backgroundColor = [UIColor zaloBaseColor];
    
    NSNumber *balance = [ZPWalletManagerSwift sharedInstance].currentBalance;
    _balanceLabel.text = balance ? [[NSString stringWithFormat:@"%ld",balance.longValue] formatMoneyValue] : @"";
    [_BalanceTitle zpMainBlackRegular];
    _BalanceTitle.textColor = [UIColor whiteColor];
    _BalanceTitle.text = [R string_Withdraw_Free_Charge_Title];
    [_BalanceImgView setIconFont:@"header_overbalance"];
    [_BalanceImgView setIconColor:[UIColor whiteColor]];
    
    if (UIScreen.mainScreen.bounds.size.height <= 568.0 ) {
        // IPhone 5
        _balanceLabel.font = [_balanceLabel.font fontWithSize:26.0];
        _BalanceTitle.font = [_balanceLabel.font fontWithSize:13];
    }
    
}

@end
@implementation ZPRechargeButtonBottomCell
+ (float)height {
    return kZaloButtonHeight + kZaloButtonToScrollOffset;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.buttonConfirm setupZaloPayButton];
    self.buttonConfirm.enabled = false;
    [self.buttonConfirm setTitle:[R string_Withdraw_Button_Title] forState:UIControlStateNormal];
}

@end
