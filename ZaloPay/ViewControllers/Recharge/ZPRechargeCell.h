//
//  ZPRechargeCell.h
//  ZaloPay
//
//  Created by bonnpv on 5/7/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ZaloPayCommon/ZPFloatTextInputView.h>

@class ZPRechargeBaseItem;
@class ZPRechargeInputCell;

@interface ZPRechargeBaseCell : UITableViewCell
- (void)setItem:(ZPRechargeBaseItem *)item;
@property (nonatomic, strong) UIView *seperater;
@end

@interface ZPRechargeMinValueTitleCell : ZPRechargeBaseCell
@property (weak, nonatomic) IBOutlet UILabel *labelMinvalue;
@property (weak, nonatomic) IBOutlet UILabel *labelvnd;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *m_trailling_contraint;

@end

@protocol ZPRechargeInputCellDelegate<NSObject>
- (void)inputCellDidDoneInput;
- (BOOL)textFieldShouldChange:(ZPRechargeInputCell *)sender text:(NSString *)text;
@end

@interface ZPRechargeInputCell : ZPRechargeBaseCell
@property (nonatomic, weak) id <ZPRechargeInputCellDelegate>inputDelegate;
@property (nonatomic, strong) ZPFloatTextInputView *textFieldMoney;
+ (instancetype)cellFromSameNibForTableView:(UITableView *)tableView;
@end

@interface ZPRechargeHeaderCell : ZPRechargeBaseCell
    @property (weak, nonatomic) IBOutlet UIView *TopView;
    @property (weak, nonatomic) IBOutlet ZPIconFontImageView *BalanceImgView;
    @property (weak, nonatomic) IBOutlet UILabel *balanceLabel;
    @property (weak, nonatomic) IBOutlet UILabel *BalanceTitle;

@end
@interface ZPRechargeButtonBottomCell : ZPRechargeBaseCell
@property (weak, nonatomic) IBOutlet UIButton *buttonConfirm;

@end
