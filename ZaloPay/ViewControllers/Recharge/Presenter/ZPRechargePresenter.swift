//
//  ZPRechargePresenter.swift
//  ZaloPay
//
//  Created by nhatnt on 25/09/2017.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

class ZPRechargePresenter: ZPRechargePresenterDelegate {
    
    weak var view: ZPRechargeViewDelegate?
    var interactor: ZPRechargeInteractorDelegate?
    var router: ZPRechargeRouterDelegate?
    
    init(interface: ZPRechargeViewDelegate, interactor: ZPRechargeInteractorDelegate, router: ZPRechargeRouterDelegate) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }
    
    func requestDataSource() -> [ZPRechargeEntity]? {
        return self.interactor?.configureDataSource()
    }
    
    func didSelectMoneyNumber(totalMoney: Int) {
        self.interactor?.requestCreateOrder(totalMoney: totalMoney)
    }
    
    func processBillSuccessfully(appId: Int, billInfo: ZPBill) {
        guard let viewController = self.view as? ZPRechargeViewController else{
            return
        }
        viewController.view.endEditing(true)
        self.view?.hideLoadingView()
        
        ZPAppFactory.sharedInstance().showZaloPayGateway(viewController,
                                                         appId: appId,
                                                         with: billInfo,
                                                         complete:
            { [weak viewController](_) in
                viewController?.payBillDidCompleteSuccess()
            }, cancel: { [weak viewController](_) in
                viewController?.payBillDidCancel()
        }) { [weak viewController](_) in
            viewController?.payBillDidError()
        }
    }
    
    func rechargeMoneyFailWithError(error: Error?) {
        self.view?.alertRechargeFail(error: error)
    }
    
    func isValidInputData(totalValue: Int64?) -> Bool {
        return interactor?.validInputData(totalValue: totalValue) ?? false
    }
    
    func isGreaterMaxValue(totalValue: Int64) -> Bool {
        return interactor?.isGreaterMaxValue(totalValue: totalValue) ?? false
    }
    
    func showInvalidInputData() {
        self.view?.showInvalidRechargeMoney()
    }
    
    func showMessageInputData(message: NSString) {
        self.view?.showMessageInputData(message: message)
    }
}



