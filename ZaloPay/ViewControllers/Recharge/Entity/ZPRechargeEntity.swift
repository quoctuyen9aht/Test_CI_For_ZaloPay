//
//  ZPRechargeEntity.swift
//  ZaloPay
//
//  Created by Nguyễn Nhật on 25/09/2017.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import UIKit

enum RechargeType: Int {
    case MinValueItem  = 0
    case InputItem  = 1
    case HeaderItem = 2
    case ButtonItem = 3
}

class ZPRechargeEntity: NSObject {
    var title: String = ""
    var sectionDescription: String = ""
    var value: Bool = false
    var type: RechargeType!
    
    init(title: String, sectionDescription: String, value : Bool, type: RechargeType ) {
        self.title = title
        self.sectionDescription = sectionDescription
        self.value = value
        self.type = type
    }
    
    func getCellClass() -> AnyClass {
        return ZPTransferBaseCell.self
    }
}

