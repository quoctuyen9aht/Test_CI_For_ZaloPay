//
//  ZPRechargeInputTextCell.swift
//  ZaloPay
//
//  Created by nhatnt on 02/10/2017.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayCommonSwift
import RxSwift

protocol ZPRechargeInputTextCellDelegate: class {
    func inputCellDidDoneInput()
}

class ZPRechargeInputTextCell: UITableViewCell {
    
    //MARK: Local variables
    var inputDelegate: ZPRechargeInputTextCellDelegate?
    var textFieldMoney = ZPCalFloatTextInputView()

    //MARK: Initialize
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupCell() {
        self.selectionStyle = UITableViewCellSelectionStyle.none
        self.textFieldMoney.backgroundColor = UIColor.white
        
        self.addSubview(self.textFieldMoney)
        self.textFieldMoney.textField.placeholder = R.string_AddCash_TextPlaceHolder()
        
        let w = UIScreen.main.applicationFrame.size.width
        self.textFieldMoney.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.width.equalTo(w)
            make.height.equalTo(60);
        }
    }
        
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    //MARK: get height
    func height() -> Float {
        return 60.0
    }
}
