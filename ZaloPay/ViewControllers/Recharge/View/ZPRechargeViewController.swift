//
//  ZPRechargeViewController.swift
//  ZaloPay
//
//  Created by nhatnt on 25/09/2017.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa
import CocoaLumberjackSwift
import ZaloPayAnalyticsSwift

@objc class ZPRechargeViewController: BillHandleBaseViewController {
    
    // MARK: Local variables
    var presenter: ZPRechargePresenterDelegate?
    var dataSources: [ZPRechargeEntity]?
    var tableView: UITableView!
    var buttonConfirm = UIButton()
    var inputTextCell = ZPRechargeInputTextCell()

    // MARK:  UI Controller
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.dataSources = self.presenter?.requestDataSource()
        self.title = R.string_AddCash_Title()
        self.inputTextCell.textFieldMoney.textField.delegate = self
        
        if ZPKeyboardConfig.enableCalculateKeyboard(.recharge) {
            let numberPad: ZPCalculateNumberPad = ZPCalculateNumberPad(delegate: self)
            self.inputTextCell.textFieldMoney.textField.inputView = numberPad
            self.buttonConfirm.addTarget(self, action: #selector(self.pmcContinueButtonClicked), for: UIControlEvents.touchUpInside)
        } else {
            let numberPad: PMNumberPad = PMNumberPad(delegate: self)
            self.inputTextCell.textFieldMoney.textField.inputView = numberPad
            self.buttonConfirm.addTarget(self, action: #selector(self.pmContinueButtonClicked), for: UIControlEvents.touchUpInside)
        }
        
        NotificationCenter.default.addObserver(forName: Notification.Name.UITextFieldTextDidChange, object: nil, queue: OperationQueue.main) { [weak self](_) in
            guard let wSelf = self else {
                return
            }
            guard let textField = findingFirstResponder(from: wSelf.view) as? UITextField, let text = textField.text, text.isEmpty.not else {
                return
            }
            wSelf.textField(textField, updateText: text)
            let nText = textField.text ?? ""
            if let nPosition = textField.position(from: textField.beginningOfDocument, offset: nText.count) {
                let range = textField.textRange(from: textField.endOfDocument, to: nPosition)
                DispatchQueue.main.async {
                    textField.selectedTextRange = range
                }
            }
        }
        
        self.addTableView()
        self.addButtonConfirm()
        
        self.registerHandleKeyboardNotification()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ZPTrackingHelper.shared().trackScreen(with: self)
    }
    
    override func backEventId() -> Int {
        return ZPAnalyticEventAction.balance_addcash_touch_back.rawValue
    }
    
    deinit {
        DDLogInfo("Deinit : ZPRechargeViewController")
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if ZPDialogView.isDialogShowing() {
            return
        }
        self.inputTextCell.textFieldMoney.textField.becomeFirstResponder()
    }
    
    override func payBillDidCancel() {
        super.payBillDidCancel()
        self.buttonConfirm.isUserInteractionEnabled = true
        
        DispatchQueue.main.asyncAfter(wallDeadline: .now() + 0.8) {
            self.inputTextCell.textFieldMoney.textField.isUserInteractionEnabled = true
            self.inputTextCell.textFieldMoney.textField.becomeFirstResponder()
        }
    }
    
    override func screenName() -> String! {
        return ZPTrackerScreen.Screen_iOS_Balance_AddCash
    }
    
    override func viewWillSwipeBack() {
    }
    
    //MARK: UI Events
    @IBAction override func backButtonClicked(_ sender: Any) {
        super.backButtonClicked(sender)
    }
    
    // MARK: setup View
    
    func addButtonConfirm() {
        self.buttonConfirm.setupZaloPay()
        self.buttonConfirm.isEnabled = false
        if let numberPad = self.inputTextCell.textFieldMoney.textField.inputView as? ZPCalculateNumberPad {
            numberPad.doneButton.isEnabled = false
        }
        self.view.addSubview(self.buttonConfirm)
        self.buttonConfirm.setTitle(R.string_Recharge_Button_Title(), for: UIControlState.normal)
        
        if self.inputTextCell.textFieldMoney.textField.inputView is ZPCalculateNumberPad {
            self.buttonConfirm.addTarget(self, action: #selector(self.pmcContinueButtonClicked), for: UIControlEvents.touchUpInside)
        } else {
            self.buttonConfirm.addTarget(self, action: #selector(self.pmContinueButtonClicked), for: UIControlEvents.touchUpInside)
        }
        
        self.buttonConfirm.snp.makeConstraints { (make) in
            make.top.equalTo(self.tableView.snp.bottom).offset(buttonToScrollOffset())
            make.left.equalTo(10)
            make.right.equalTo(-10)
            make.height.equalTo(kZaloButtonHeight);
        }
    }
    
    func addTableView() {
        self.tableView = UITableView.init(frame: CGRect.zero, style: UITableViewStyle.plain)
        self.tableView.backgroundColor = UIColor.init(red: 0.94, green: 0.96, blue: 0.97, alpha: 1)
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.view.addSubview(self.tableView)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.isScrollEnabled = true;
        
        self.tableView.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.top.equalTo(0)
            make.height.equalTo(self.scrollHeight())
        }
    }
    
    override func scrollHeight() -> Float {
        return 101
    }
    
    func setNextButtonEnable(isEnable: Bool) {
        if let numberPad = self.inputTextCell.textFieldMoney.textField.inputView as? ZPCalculateNumberPad {
            numberPad.doneButton.isEnabled = isEnable
        }
        if let numberPad = self.inputTextCell.textFieldMoney.textField.inputView as? PMNumberPad {
            numberPad.doneButton.isEnabled = isEnable
        }
        self.buttonConfirm.isEnabled = isEnable
    }
    
    // MARK: control UI Elements
    func configTextFormat() {
       _ = self.inputTextCell.textFieldMoney.textField.rx.text.subscribe(onNext: {
            [unowned self] (text) in
            self.inputTextCell.textFieldMoney.clearErrorText()
            var enableButton = false
            if (text?.count)! > 0 {
                enableButton = true
                self.buttonConfirm.setBackgroundColor(UIColor.zaloBase(), for: UIControlState.normal)
            } else {
                enableButton = false
                self.buttonConfirm.setBackgroundColor(UIColor.subText(), for: UIControlState.normal)
            }

            self.buttonConfirm.isEnabled = enableButton
            if let numberPad = self.inputTextCell.textFieldMoney.textField.inputView as? ZPCalculateNumberPad {
                numberPad.doneButton.isEnabled = enableButton
            }
            if let numberPad = self.inputTextCell.textFieldMoney.textField.inputView as? PMNumberPad {
                numberPad.doneButton.isEnabled = enableButton
            }
            self.textField(self.inputTextCell.textFieldMoney.textField, updateText: text)
        })
    }

    func showInvalidRechargeMoney() {
        self.inputTextCell.textFieldMoney.showError(withText: R.string_Recharge_Condition())
    }
    
    func alertRechargeFail(error: Error?) {
        ZPDialogView.showDialogWithError(error, handle: nil)
        self.hideLoadingView()
        self.inputTextCell.textFieldMoney.textField.isUserInteractionEnabled = true
        self.buttonConfirm.isUserInteractionEnabled = true
        self.inputTextCell.textFieldMoney.textField.resignFirstResponder()
    }
    
    func showMessageInputData(message: NSString){
        self.inputTextCell.textFieldMoney.showError(withText: message as String)
    }
}

extension ZPRechargeViewController: UITableViewDelegate, UITableViewDataSource {
    // MARK: UITableViewController
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSources!.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = dataSources![indexPath.row]
        
        if item.type == RechargeType.MinValueItem {
            let cell = ZPRechargeMinValueCell.zp_cellFromSameNib(for: tableView)
            cell.setItem(item: item)
            return cell
        }
        if item.type == RechargeType.InputItem {
            self.inputTextCell.inputDelegate = self
            if ZPDialogView.isDialogShowing() == false {
                self.inputTextCell.textFieldMoney.becomeFirstResponder()
            }
            self.configTextFormat()
            return inputTextCell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = dataSources![indexPath.row]
        if item.type == RechargeType.MinValueItem {
            return CGFloat(40)
        }
        if item.type == RechargeType.InputItem {
            return CGFloat(60)
        }
        return CGFloat(40)
    }
}

extension ZPRechargeViewController: ZPRechargeInputTextCellDelegate {
    func inputCellDidDoneInput() {
        if self.inputTextCell.textFieldMoney.textField.inputView is ZPCalculateNumberPad {
            self.pmcContinueButtonClicked(nil)
        } else {
            self.pmContinueButtonClicked(nil)
        }
        
    }
}

extension ZPRechargeViewController: ZPRechargeViewDelegate {
    override func showLoadingView() {
        ZPAppFactory.sharedInstance().showHUDAdded(to: self.view)
    }
    
    override func hideLoadingView() {
        ZPAppFactory.sharedInstance().hideAllHUDs(for: self.view)
    }
}

extension ZPRechargeViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField?, updateText text: String?) {
        guard   let textField = textField,
            let text = text
            else {
                return
        }
        if textField == self.inputTextCell.textFieldMoney.textField {
            let stringMoneyValue = (self.inputTextCell.textFieldMoney.textField.text ?? "").onlyDigit() ?? ""
            if stringMoneyValue == "" {
                setNextButtonEnable(isEnable: false)
            }
            self.inputTextCell.textFieldMoney.updateTextField(textField, updateText: text)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField != self.inputTextCell.textFieldMoney.textField {
            return true
        }
        
        self.inputTextCell.textFieldMoney.clearErrorText()
        var newString = (textField.text ?? "") as NSString
        newString = newString.replacingCharacters(in: range, with: string) as NSString
        
        if self.inputTextCell.textFieldMoney.textField.inputView is PMNumberPad {
            let totalValue =  Int64(newString.onlyDigit()) ?? 0
            return (presenter?.isGreaterMaxValue(totalValue: totalValue).not)!
        }
        
        if self.inputTextCell.textFieldMoney.isExpression() {
            if self.inputTextCell.textFieldMoney.isMaxCharactor(newString as String) {
                self.inputTextCell.textFieldMoney.showErrorAuto(R.string_InputMoney_Max_Character_Error_Message())
                return false
            }
        }
        return true
    }
}

extension ZPRechargeViewController: PMCNumberPadControl {
    
    func pmcKeyboard(_ keyboard: ZPCalculateNumberPad!, checkMinvalue value: Int64, fromFloatTextField textInput: ZPFloatTextInputView!) {
        let min = ZPWalletManagerSwift.sharedInstance.config.minRechargeValue;
        if (value < min) {
            let moneyString = (min as NSNumber).formatCurrency() ?? ""
            let message = String(format:R.string_InputMoney_SmallAmount_Error_Message(), moneyString)
            textInput.showError(withText: message);
        }
    }
    
    func pmcContinueButtonClicked(_ sender: Any?) {
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.balance_addcash_input)
        guard let numberPad = self.inputTextCell.textFieldMoney.textField.inputView as? ZPCalculateNumberPad else {
            return
        }
        if numberPad.isExpression(self.inputTextCell.textFieldMoney.textField.text) {
            let result = numberPad.calc(self.inputTextCell.textFieldMoney)
            if numberPad.wrongSyntax(self.inputTextCell.textFieldMoney) || !(self.presenter?.isValidInputData(totalValue: Int64(result)))! {
                numberPad.doneButton.isEnabled = false
                self.buttonConfirm.isEnabled = false
                return
            }
            self.inputTextCell.textFieldMoney.textField.text = String(result).onlyDigit().formatMoneyValue()
        }
        
        let totalValue = self.inputTextCell.textFieldMoney.textField.text?.onlyDigit().toInt64()
        if (self.presenter?.isValidInputData(totalValue: totalValue))! {
            self.inputTextCell.textFieldMoney.resignFirstResponder()
            self.showLoadingView()
            self.buttonConfirm.isUserInteractionEnabled = false;
            self.inputTextCell.textFieldMoney.textField.isUserInteractionEnabled = false
            self.presenter?.didSelectMoneyNumber(totalMoney: Int(totalValue!))
        } else {
            self.buttonConfirm.isEnabled = false
            numberPad.doneButton.isEnabled = false
        }
    }
}

extension ZPRechargeViewController: PMNumberPadControl {
    func pmContinueButtonClicked(_ sender: Any!) {
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.balance_addcash_input)
        let totalValue = Int64((self.inputTextCell.textFieldMoney.textField.text?.onlyDigit())!)
        if (self.presenter?.isValidInputData(totalValue: totalValue))! {
            self.inputTextCell.textFieldMoney.resignFirstResponder()
            self.showLoadingView()
            self.buttonConfirm.isUserInteractionEnabled = false;
            self.inputTextCell.textFieldMoney.textField.isUserInteractionEnabled = false
            self.presenter?.didSelectMoneyNumber(totalMoney: Int(totalValue!))
        }
    }
}

//MARK: NumberPad
extension ZPRechargeViewController: APNumberPadDelegate {
    
    func numberPad(_ numberPad: APNumberPad!, functionButtonAction functionButton: UIButton!, textInput: (UIResponder & UITextInput)!) {
        
        if let numberPad = numberPad as? ZPCalculateNumberPad  {
            numberPad.control(self.inputTextCell.textFieldMoney, with: functionButton, viewController: self)
            return
        }
        
        guard let numberPad = numberPad as? PMNumberPad else {
            return
        }
        
        if functionButton == numberPad.doneButton {
            pmContinueButtonClicked(UIButton())
            return
        }
        
        if functionButton == numberPad.leftFunctionButton {
            let textToReplace: String = "000"
            let txtField = self.inputTextCell.textFieldMoney.textField!;
            let count = (txtField.text ?? "").count
            let range = NSRange(location: count, length: 0)
            if self.textField(txtField, shouldChangeCharactersIn: range, replacementString: textToReplace) {
                self.inputTextCell.textFieldMoney.textField.insertText(textToReplace)
            }
        }
    }
}
