//
//  ZPRechargeHeaderViewCell.swift
//  ZaloPay
//
//  Created by nhatnt on 03/10/2017.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

class ZPRechargeHeaderViewCell: UITableViewCell {

    //MARK: UI Elements
    @IBOutlet weak var TopView: UIView!
    @IBOutlet weak var BalanceImgView: ZPIconFontImageView!
    @IBOutlet weak var BalanceLabel: UILabel!
    @IBOutlet weak var BalanceTitle: UILabel!
    
    //MARK: Initialize
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        TopView.backgroundColor = UIColor.zaloBase()
        BalanceLabel.text = NSString.init(format: "%ld", ZPWalletManagerSwift.sharedInstance.currentBalance?.intValue ?? 0).formatMoneyValue()
        BalanceTitle.zpMainBlackRegular()
        BalanceTitle.textColor = UIColor.white
        BalanceTitle.text = R.string_Withdraw_Free_Charge_Title()
        BalanceImgView.setIconFont("header_overbalance")
        BalanceImgView.setIconColor(UIColor.white)
        
        if UIScreen.main.bounds.size.height <= 568.0 {
            // Iphone 5
            BalanceLabel.font = BalanceLabel.font.withSize(26.0)
            BalanceTitle.font = BalanceLabel.font.withSize(13)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: get height
    func height() -> Float {
        return 167.0
    }
}
