//
//  ZPRechargePayBillSupportSwift.swift
//  ZaloPay
//
//  Created by nhatnt on 16/10/2017.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation

typealias RechargeHandle = () -> Void
@objcMembers
class ZPRechargePayBillSupportSwift : ZPRechargeViewController {
    var complete: RechargeHandle?
    var cancel: RechargeHandle?
    var error: RechargeHandle?
    
    convenience init(complete: @escaping RechargeHandle, cancel: @escaping RechargeHandle, error: @escaping RechargeHandle) {
        self.init()
        
        let interactor = ZPRechargeInteractor()
        let router = ZPRechargeRouter()
        let presenter = ZPRechargePresenter(interface: self, interactor: interactor, router: router)
        
        presenter.view = self
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        router.view = self
        self.presenter = presenter
        
        self.complete = complete
        self.cancel = cancel
        self.error = error
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if self.navigationController?.topViewController == self {
            self.navigationController?.isNavigationBarHidden = false
        }
    }
    
    override func backButtonClicked(_ sender: Any) {
       viewWillSwipeBack()
    }
    
    override func viewWillSwipeBack() {
        if self.cancel != nil {
            self.cancel!()
        }
        self.clearAllHandle()
    }
    
    //pragma mark - Override Bill Handle
    override func handleSetAppIdError(_ appId: Int) {
        self.payBillDidError()
    }
    
    override func payBillDidCompleteSuccess() {
        if self.complete != nil {
            self.complete!()
        }
        self.clearAllHandle()
    }
    
    override func payBillDidCancel() {
        self.navigationController?.popToViewController(self, animated: true)
    }
    
    override func payBillDidError() {
        if self.error != nil {
            self.error!()
        }
        self.clearAllHandle()
    }
    
    func clearAllHandle() {
        self.error = nil
        self.cancel = nil
        self.complete = nil
    }
}





