//
//  ZPRechargeBottomButtonCell.swift
//  ZaloPay
//
//  Created by nhatnt on 02/10/2017.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

class ZPRechargeBottomButtonCell: UITableViewCell {
    // MARK: UI Elements
    @IBOutlet weak var buttonConfirm: UIButton!    
    @IBAction func clicked(_ sender: Any) {
    }
    // MARK: Initialize
    override func awakeFromNib() {
        super.awakeFromNib()
        self.buttonConfirm.setupZaloPay()
        self.buttonConfirm.isEnabled = false
        self.buttonConfirm.setTitle(R.string_Withdraw_Button_Title(), for: UIControlState.normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    // MARK: Get height
    func height() -> Float {
        return Float(kZaloButtonHeight) + kZaloButtonToScrollOffset
    }
}
