//
//  ZPRechargeMinValueCell.swift
//  ZaloPay
//
//  Created by nhatnt on 29/09/2017.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

class ZPRechargeMinValueCell: UITableViewCell {

    //MARK: UI Elements
    @IBOutlet weak var labelvnd: UILabel!
    @IBOutlet weak var labelMinvalue: UILabel!
    
    //MARK: Initialize
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = UITableViewCellSelectionStyle.none
        self.contentView.backgroundColor = UIColor.init(red: 0.94, green: 0.96, blue: 0.97, alpha: 1)
        
        let textColor = UIColor.subText();
        self.labelvnd.textColor = textColor
        self.labelMinvalue.textColor = textColor
        
        self.labelvnd.font = UIFont.sfuiTextRegular(withSize: 9)
        self.labelMinvalue.zpSubTextGrayRegular()
    }
    
    func setItem(item: ZPRechargeEntity) {
        self.labelMinvalue.text = item.title
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: get height
    func height() -> Float {
        return 40.0
    }
}
