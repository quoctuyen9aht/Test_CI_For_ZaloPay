//
//  ZPPaymentCodeListLimitCashCell.swift
//  ZaloPay
//
//  Created by vuongvv on 11/10/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

class ZPPaymentCodeListLimitCashCell: UITableViewCell {

    @IBOutlet weak var imgViewCheck: ZPIconFontImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblVND: UILabel!
    @IBOutlet weak var bottomView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgViewCheck.setIconFont("general_check")
        imgViewCheck.setIconColor(UIColor.zaloBase())
        imgViewCheck.defaultView.font = UIFont.iconFont(withSize: 20)
        
        let fontSize: Int = 16
        self.lblTitle.font
            = UIFont.sfuiTextRegular(withSize: CGFloat(fontSize))
        self.lblTitle.textColor = UIColor.defaultText()
        
        let fontSizeVND: Int = 12
        self.lblVND.font
            = UIFont.sfuiTextRegular(withSize: CGFloat(fontSizeVND))
        self.lblVND.textColor = UIColor.defaultText()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
