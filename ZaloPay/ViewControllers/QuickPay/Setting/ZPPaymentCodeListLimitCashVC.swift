//
//  ZPPaymentCodeListLimitCashVC.swift
//  ZaloPay
//
//  Created by vuongvv on 11/10/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayAnalyticsSwift
import ZaloPayConfig

typealias ZPPaymentCodeListLimitCashVCFinishedBlock = ((_ amount_NotNeedPass: Int64,_ amount_Max: Int64,_ channel: Int64) -> Void)
let kZPPaymentCodeListLimitCashCell = "ZPPaymentCodeListLimitCashCell"
let kZPPaymentCodeListLimitCashTitleCell = "ZPPaymentCodeListLimitCashTitleCell"
let kZPPaymentCodeListLimitCashChannelCell = "ZPPaymentCodeListLimitCashChannelCell"
let kZPPaymentCodeListLimitCashLockCell = "ZPPaymentCodeListLimitCashLockCell"
enum TypeCellPaymentCodeLimit {
    case title
    case price
    case channel
    case timeLock
}
struct ItemCellPaymentCodeLimit {
    var type = TypeCellPaymentCodeLimit.title
    var value:String = ""
    var id :Int = 0
    var icon :UIImage = UIImage()
    init(type:TypeCellPaymentCodeLimit,value:String) {
        self.type = type
        self.value = value
    }
    init(type:TypeCellPaymentCodeLimit,value:String,id:Int) {
        self.init(type: type, value: value)
        self.id = id
    }
    init(type:TypeCellPaymentCodeLimit,value:String,id:Int,icon:UIImage) {
        self.init(type: type, value: value,id:id)
        self.icon = icon
    }
}
struct ItemChannelPaymentCodeLimit {
    var id :Int = 0
    var name : String = ""
    var icon : UIImage = UIImage()
    
    init(id:Int,name:String,icon:UIImage) {
        self.id = id
        self.name = name
        self.icon = icon
    }
    
    init(_ dic: Dictionary<AnyHashable,Any>) {
        self.id = dic.value(forKey: "pmcid", defaultValue: 38)
        self.name = dic.value(forKey: "name", defaultValue: "")
        let nameImage = dic.value(forKey: "logo", defaultValue: "")
        self.icon = ZaloPayWalletSDKPayment.sharedInstance().getImageWithName(nameImage) ?? UIImage()
    }
    
    init(_ config: ZPPaymentMethodQuickPayConfigProtocol) {
        self.id = config.getPmcid()
        self.name = config.getName()
        let nameImage = config.getLogo()
        self.icon = ZaloPayWalletSDKPayment.sharedInstance().getImageWithName(nameImage) ?? UIImage()
    }
}
@objcMembers
class ZPPaymentCodeConfig : NSObject {

    static let channelZaloPayDefault = ItemChannelPaymentCodeLimit(id: 38, name: "TK Zalo Pay",icon:ZaloPayWalletSDKPayment.sharedInstance().getImageWithName("ic_vi_zalo.png") ?? UIImage())
    override init() {
        super.init()
    }
    class func getArrChannelPayment() -> [ItemChannelPaymentCodeLimit]{
//        guard let arrChannel = ApplicationState.getArrayConfig(fromDic: "quickpay", andKey: "payment_method", andDefault: []) as? [Dictionary<AnyHashable, Any>] else {
//            return [channelZaloPayDefault]
//        }
        let arrChannel = ZPApplicationConfig.getQuickPayConfig()?.getPaymentMethod() ?? []
        var arrGet = [ItemChannelPaymentCodeLimit]()
        for channel in arrChannel {
            arrGet.append(ItemChannelPaymentCodeLimit(channel))
        }
        return arrGet
    }
    class func getpaymentLimitAmount() -> Int{
//        return Int(ApplicationState.getIntConfig(fromDic: "quickpay", andKey: "payment_limit", andDefault: 200000))
        return ZPApplicationConfig.getQuickPayConfig()?.getPaymentLimit() ?? 200000
    }
    class func getChannelSelected() -> ItemChannelPaymentCodeLimit{
        let channelSelected = ZPSettingsConfig.sharedInstance().paymentCodeChannelSelect
        let arrChannel = getArrChannelPayment()
        let arrChannelSelected = arrChannel.filter{$0.id == channelSelected}
        if arrChannelSelected.count > 0 {
            return arrChannelSelected.first!
        }
        if arrChannel.count > 0 {
            ZPSettingsConfig.sharedInstance().paymentCodeChannelSelect = Int32(arrChannel.first!.id)
        }
        return channelZaloPayDefault
    }
}
class ZPPaymentCodeListLimitCashVC: BaseViewController {
    
    var tableView = UITableView()
    var btnOK = UIButton()
    var finishedBlock: ZPPaymentCodeListLimitCashVCFinishedBlock?
    
    var arrLimit_Max:[Int] = [ZPPaymentCodeConfig.getpaymentLimitAmount()]
    var arrLimit_Channel:[ItemChannelPaymentCodeLimit] = ZPPaymentCodeConfig.getArrChannelPayment()
    var arrLimit = [Any]()
    var arrLimit_TimeLock:[Int] = [5]
    
    var arrSelected = [ZPPaymentCodeConfig.getpaymentLimitAmount(),ZPPaymentCodeConfig.getChannelSelected().id]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
        setupData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ZPTrackingHelper.shared().trackScreen(with: self)
    }
    
    override func screenName() -> String! {
        return ZPTrackerScreen.Screen_iOS_Quickpay_Setupinfor
    }
    
    func initView(){
        self.view.backgroundColor = .white
        self.title = R.string_QuickPay_Setting_Title()
        //setupRightBarButton()
        
        self.view.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            make.left.top.equalTo(0)
            make.right.equalTo(0)
            make.top.equalTo(0)
            make.bottom.equalTo(0)
        }
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        tableView.bounces = true
    }
    func setupData(){

        var arrLimitMax = [ItemCellPaymentCodeLimit]()
        var arrChannel = [ItemCellPaymentCodeLimit]()
        //var arrTimeLock = [ItemCellPaymentCodeLimit]()
        
        arrLimitMax.append(ItemCellPaymentCodeLimit(type: .title, value: R.string_QuickPay_Setting_MaxAmount()))
        for amount in arrLimit_Max {
            arrLimitMax.append(ItemCellPaymentCodeLimit(type: .price, value: "\(amount)"))
        }
        arrChannel.append(ItemCellPaymentCodeLimit(type: .title, value: R.string_QuickPay_Setting_ListChannel()))
        for channel in arrLimit_Channel {
            arrChannel.append(ItemCellPaymentCodeLimit(type: .channel, value: channel.name,id:channel
                .id,icon:channel.icon))
        }
//        arrTimeLock.append(ItemCellPaymentCodeLimit(type: .title, value: R.string_QuickPay_Setting_LockScreen()))
//        for timeGet in arrLimit_TimeLock {
//            arrTimeLock.append(ItemCellPaymentCodeLimit(type: .timeLock, value: "\(timeGet) phút"))
//        }
        
        arrLimit = [arrLimitMax,arrChannel]
        tableView.reloadData()
    }
    func rightMenuBars() -> [UIBarButtonItem] {

        let button = UIButton(type: UIButtonType.custom)
        button.frame = CGRect(x: 0, y: 0, width: 60, height: 26)
        button.setTitle("Áp dụng", for: .normal)
        button.addTarget(self, action: #selector(self.applyClick), for: .touchUpInside)
        button.titleLabel?.font = UIFont.sfuiTextRegular(withSize: 15)
        button.titleLabel?.textAlignment = .right
        let negativeSeparator = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        negativeSeparator.width = -8
        let rightBar = UIBarButtonItem(customView: button)
        return [negativeSeparator, rightBar]
    }
    func setupRightBarButton() {
        self.navigationItem.rightBarButtonItems = rightMenuBars()
    }
    @objc func applyClick(){
    }
}
extension ZPPaymentCodeListLimitCashVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrLimit.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (arrLimit[section] as! [ItemCellPaymentCodeLimit]).count;
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let item = (arrLimit[indexPath.section] as! [ItemCellPaymentCodeLimit])[safe:indexPath.row] else {
            return UITableViewCell()
        }
        
        if item.type == .title {
            var cell = tableView.dequeueReusableCell(withIdentifier: kZPPaymentCodeListLimitCashTitleCell) as? ZPPaymentCodeListLimitCashTitleCell
            if cell == nil {
                cell = UINib(nibName: kZPPaymentCodeListLimitCashTitleCell, bundle: nil).instantiate(withOwner: self,
                                                                                                options: nil)[0] as? ZPPaymentCodeListLimitCashTitleCell
            }
            cell?.lblTitle.text = item.value
            return cell ?? UITableViewCell()
        }
        else if item.type == .price {
            
            var cell = tableView.dequeueReusableCell(withIdentifier: kZPPaymentCodeListLimitCashCell) as? ZPPaymentCodeListLimitCashCell
            if cell == nil {
                cell = UINib(nibName: kZPPaymentCodeListLimitCashCell, bundle: nil).instantiate(withOwner: self,
                                                                                                options: nil)[0] as? ZPPaymentCodeListLimitCashCell
            }
            cell?.lblTitle.text = ("\(item.value)" as NSString ).formatMoneyValue()
            cell?.imgViewCheck.isHidden = Int(item.value) != arrSelected[indexPath.section]
            cell?.bottomView.isHidden = (arrLimit[indexPath.section] as! [ItemCellPaymentCodeLimit]).count - 1 == indexPath.row

            return cell ?? UITableViewCell()
        }
        else if item.type == .channel {
            var cell = tableView.dequeueReusableCell(withIdentifier: kZPPaymentCodeListLimitCashChannelCell) as? ZPPaymentCodeListLimitCashChannelCell
            if cell == nil {
                cell = UINib(nibName: kZPPaymentCodeListLimitCashChannelCell, bundle: nil).instantiate(withOwner: self,
                                                                                                options: nil)[0] as? ZPPaymentCodeListLimitCashChannelCell
            }
            cell?.lblTitle.text = "\(item.value)"
            cell?.imgViewCheck.isHidden = Int(item.id) != arrSelected[indexPath.section]
            cell?.imgViewChannel.image = item.icon
            cell?.bottomView.isHidden = (arrLimit[indexPath.section] as! [ItemCellPaymentCodeLimit]).count - 1 == indexPath.row

            return cell ?? UITableViewCell()
        }
        else if item.type == .timeLock {
            var cell = tableView.dequeueReusableCell(withIdentifier: kZPPaymentCodeListLimitCashLockCell) as? ZPPaymentCodeListLimitCashLockCell
            if cell == nil {
                cell = UINib(nibName: kZPPaymentCodeListLimitCashLockCell, bundle: nil).instantiate(withOwner: self,
                                                                                                       options: nil)[0] as? ZPPaymentCodeListLimitCashLockCell
            }
            cell?.lblTime.text = "\(item.value)"
            cell?.bottomView.isHidden = (arrLimit[indexPath.section] as! [ItemCellPaymentCodeLimit]).count - 1 == indexPath.row

            return cell ?? UITableViewCell()
        }
        return UITableViewCell()
    }
}

extension ZPPaymentCodeListLimitCashVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let item = (arrLimit[indexPath.section] as! [ItemCellPaymentCodeLimit])[safe:indexPath.row] else {
            return
        }
        if item.type == .price {
            arrSelected[indexPath.section] = Int(item.value) ?? 0
        }
        else if item.type == .channel {
            arrSelected[indexPath.section] = item.id
            ZPSettingsConfig.sharedInstance().paymentCodeChannelSelect = Int32(item.id)
        }
        self.tableView.reloadData()
    }
}
