//
//  ZPPaymentCodeListLimitCashTitleCell.swift
//  ZaloPay
//
//  Created by GonyMac on 11/15/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

class ZPPaymentCodeListLimitCashTitleCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let fontSize: Int = 14
        self.lblTitle.font
            = UIFont.sfuiTextRegular(withSize: CGFloat(fontSize))
        self.lblTitle.textColor = UIColor.subText()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
