//
//  ZPPaymentCodeListLimitCashLockCell.swift
//  ZaloPay
//
//  Created by vuongvv on 11/29/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

class ZPPaymentCodeListLimitCashLockCell: UITableViewCell {
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblNotice: UILabel!
    @IBOutlet weak var imgViewCheck: ZPIconFontImageView!
    @IBOutlet weak var bottomView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgViewCheck.setIconFont("general_check")
        imgViewCheck.setIconColor(UIColor.zaloBase())
        imgViewCheck.defaultView.font = UIFont.iconFont(withSize: 20)
        
        let fontSize: Int = 16
        self.lblTime.font
            = UIFont.sfuiTextRegular(withSize: CGFloat(fontSize))
        self.lblTime.textColor = UIColor.defaultText()
        
        let fontSizeVND: Int = 12
        self.lblNotice.font
            = UIFont.sfuiTextRegular(withSize: CGFloat(fontSizeVND))
        self.lblNotice.textColor = UIColor.subText()
        self.lblNotice.text = R.string_QuickPay_Setting_WhenDisconnect()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
