//
//  ZPPaymentCodeListLimitCashChannelCell.swift
//  ZaloPay
//
//  Created by vuongvv on 11/15/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

class ZPPaymentCodeListLimitCashChannelCell: UITableViewCell {
    @IBOutlet weak var imgViewChannel: ZPIconFontImageView!
    @IBOutlet weak var imgViewCheck: ZPIconFontImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var bottomView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgViewCheck.setIconFont("general_check")
        imgViewCheck.setIconColor(UIColor.zaloBase())
        imgViewCheck.defaultView.font = UIFont.iconFont(withSize: 20)
        
        imgViewChannel.defaultView.font = UIFont.iconFont(withSize: 35)
    }
    
}
