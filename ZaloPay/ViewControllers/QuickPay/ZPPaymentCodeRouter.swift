//
//  ZPPaymentCodeRouter.swift
//  ZaloPay
//
//  Created by vuongvv on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayAnalyticsSwift
import ZaloPayWeb

@objc class ZPPaymentCodeRouter: NSObject, ZPPaymentCodeRouterDelegate {
    weak var rootViewController: UIViewController?
    
    func backViewController() {
        rootViewController?.navigationController?.popViewController(animated: true)
    }
    
    class func assemblyModule(forceGoToGenQRCode:Bool = false) -> ZPPaymentCodeViewController {
        let controller: ZPPaymentCodeViewController = ZPPaymentCodeViewController()
        
        let presenter: ZPPaymentCodePresenter = ZPPaymentCodePresenter()
        let interactor: ZPPaymentCodeInteractor = ZPPaymentCodeInteractor()
        let router: ZPPaymentCodeRouter = ZPPaymentCodeRouter()
        
        presenter.controller = controller
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.rootViewController = controller
        
        controller.presenter = presenter
        controller.forceGoToGenQRCode = forceGoToGenQRCode
        
        return controller
    }
    func gotoListLimitCash(_ finishedBlock: @escaping ZPPaymentCodeListLimitCashVCFinishedBlock){
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.quickpay_customize_touch_setupinfor)
        let paymentCodeListLimitCashVC = ZPPaymentCodeListLimitCashVC()
        paymentCodeListLimitCashVC.finishedBlock = finishedBlock
        rootViewController?.navigationController?.pushViewController(paymentCodeListLimitCashVC, animated: true)
        
    }
    func gotoDisconnectVC(){
        let VC = ZPInternetConnectionViewController()
        rootViewController?.navigationController?.pushViewController(VC, animated: true)
    }
    func gotoSupportCenterVC(){
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.quickpay_customize_touch_setupinfor)
        let urlSupport = kUrlQuickPaySupport //ZPApplicationConfig.getQuickPayConfig()?.getUserManualUrl() ?? "https://zalopay.com.vn/trung-tam-ho-tro/tratien.html"
        let viewController = ZPWWebViewController(url: urlSupport, webHandle: ZPWebHandle())
        rootViewController?.navigationController?.pushViewController(viewController, animated: true)
    }
    func presentSlideIntroVC(){
        let intro = IntroViewController()
        intro.delegate = rootViewController as? IntroViewControllerDelegate
        rootViewController?.present(intro, animated: true, completion: nil)
    }
    func gotoWithdrawVC() {
        ZPCenterRouter.launch(routerId: .withdraw, from: rootViewController!)
    }
    func gotoRechargeVC(){
        let VC = ZPRechargeRouter.assemblyModule()
        rootViewController?.navigationController?.pushViewController(VC, animated: true)
    }
}
