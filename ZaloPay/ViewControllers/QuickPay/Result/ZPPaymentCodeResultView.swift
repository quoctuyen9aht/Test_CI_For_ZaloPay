//
//  ZPPaymentCodeResultView.swift
//  ZaloPay
//
//  Created by vuongvv on 11/14/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

fileprivate let kZPPaymentCodeResultDetailCell = "ZPPaymentCodeResultDetailCell"
fileprivate let kZPPaymentCodeResultAmountCell = "ZPPaymentCodeResultAmountCell"
fileprivate let kZPPaymentCodeResultLineCell = "ZPPaymentCodeResultLineCell"

protocol ZPPaymentCodeResultViewDelegate : class {
    func btnGoHomeClick()
}

class ZPPaymentCodeResultView: UIView {
    @IBOutlet weak var mTableView: UITableView!
    @IBOutlet weak var mBtnOk: UIButton!
    weak var delegate:ZPPaymentCodeResultViewDelegate?
    let disposeBag = DisposeBag()
    var resultGet = ZPPaymentCodeResult()
    
    override func awakeFromNib() {
        
        mTableView.backgroundColor = .clear
        mTableView.separatorStyle = .none
        mTableView.delegate = self
        mTableView.dataSource = self
        mTableView.bounces = true
        mTableView.rowHeight = UITableViewAutomaticDimension
        mTableView.estimatedRowHeight = 60
        
        mBtnOk.setTitle(R.string_LeftMenu_Home(), for: .normal)
        mBtnOk.backgroundColor = UIColor.hex_0xe6f6ff()
        mBtnOk.layer.borderColor = UIColor.hex_0x4abbff().cgColor
        mBtnOk.layer.borderWidth = 1.0
        mBtnOk.setTitleColor(UIColor.subText(), for: .normal)
        mBtnOk.titleLabel?.zpMainBlackRegular()
        mBtnOk.titleLabel?.textAlignment = .center
        mBtnOk.layer.cornerRadius = 3
        mBtnOk.rx.tap.bind {[weak self] _ in
               self?.delegate?.btnGoHomeClick()
            }.disposed(by: disposeBag)
        //setupData(result: ZPPaymentCodeResult())
    }
    func setupData(result:ZPPaymentCodeResult){
        resultGet = result
        //testData()
        mTableView.reloadData()
    }
    func testData(){
        resultGet.returncode = 1
        resultGet.amount = 40000
        resultGet.returnmessage = "thanh toan tien tra sua"
        resultGet.discountamount = 10000
    }
}
extension ZPPaymentCodeResultView: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // resultGet.discountamount > 0 ? 4 : 1;
        return 1;
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            var cell = tableView.dequeueReusableCell(withIdentifier: kZPPaymentCodeResultDetailCell) as? ZPPaymentCodeResultDetailCell
            if cell == nil {
                cell = UINib(nibName: kZPPaymentCodeResultDetailCell, bundle: nil).instantiate(withOwner: self,
                                                                                               options: nil)[0] as? ZPPaymentCodeResultDetailCell
            }
            cell?.setupData(result: resultGet)
            return cell ?? UITableViewCell()
        }
        else if indexPath.row == 1 {
            var cell = tableView.dequeueReusableCell(withIdentifier: kZPPaymentCodeResultLineCell) as? ZPPaymentCodeResultLineCell
            if cell == nil {
                cell = UINib(nibName: kZPPaymentCodeResultLineCell, bundle: nil).instantiate(withOwner: self,
                                                                                               options: nil)[0] as? ZPPaymentCodeResultLineCell
            }
            return cell ?? UITableViewCell()
        }
        else {
            var cell = tableView.dequeueReusableCell(withIdentifier: kZPPaymentCodeResultAmountCell) as? ZPPaymentCodeResultAmountCell
            if cell == nil {
                cell = UINib(nibName: kZPPaymentCodeResultAmountCell, bundle: nil).instantiate(withOwner: self,
                                                                                               options: nil)[0] as? ZPPaymentCodeResultAmountCell
            }
            if indexPath.row == tableView.numberOfRows(inSection: 0) - 1 {
                cell?.isShowDiscount = true
            }
            cell?.setupData(result: resultGet)
            return cell ?? UITableViewCell()
        }
    }
}

extension ZPPaymentCodeResultView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
