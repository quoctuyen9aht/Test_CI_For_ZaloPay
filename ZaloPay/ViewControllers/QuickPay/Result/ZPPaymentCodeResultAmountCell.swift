//
//  ZPPaymentCodeResultAmountCell.swift
//  ZaloPay
//
//  Created by vuongvv on 11/21/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

class ZPPaymentCodeResultAmountCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var lblVND: UILabel!
    var isShowDiscount = false
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblTitle.font = UIFont.sfuiTextRegular(withSize: 16)
        lblTitle.text = ""
        lblTitle.textAlignment = .left

        
        lblValue.text = ""
        lblValue.font = UIFont.sfuiTextRegular(withSize: 16)
        lblValue.textAlignment = .right
        
        lblVND.font = UIFont.sfuiTextRegular(withSize: 10)
        lblVND.textColor = UIColor.subText()
    }
    func setupData(result:ZPPaymentCodeResult){
        lblTitle.textColor = !isShowDiscount ?  UIColor.subText() : UIColor.init(hexString: "F0A62B")
        lblValue.textColor = !isShowDiscount ?  UIColor.subText() : UIColor.init(hexString: "F0A62B")
        lblVND.textColor = !isShowDiscount ?  UIColor.subText() : UIColor.init(hexString: "F0A62B")
        
        lblTitle.text = !isShowDiscount ? R.string_QuickPay_Result_Original_Amount() : "Thanh toán qua ZaloPay"
        lblValue.text = !isShowDiscount ? "\(result.amount + result.discountamount)" : "-\(result.discountamount)"
    }
}
