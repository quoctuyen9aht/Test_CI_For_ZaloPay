//
//  ZPPaymentCodeResultDetailCell.swift
//  ZaloPay
//
//  Created by vuongvv on 11/14/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

class ZPPaymentCodeResultDetailCell: UITableViewCell {
    @IBOutlet weak var imgView: ZPIconFontImageView!
    @IBOutlet weak var lblDes: UILabel!
    
    @IBOutlet weak var viewPrice: UIView!
    @IBOutlet weak var lblPrice: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblDes.font = UIFont.defaultSFUITextRegular()
        lblDes.textColor = UIColor.subText()
        lblDes.text = ""
        
        lblPrice.text = ""
        lblPrice.font = UIFont.sfuiTextMedium(withSize: 48)
    }
    func setupData(result:ZPPaymentCodeResult){
        let iconColor = result.isSuccess ? UIColor.pay() : UIColor.error()
        let iconSuccess = result.isSuccess ? "pay_success" : "pay_fail"

        imgView.setIconFont(iconSuccess)
        imgView.setIconColor(iconColor)
        
        viewPrice.isHidden = result.isSuccess ? false : true
        lblDes.text = result.isSuccess ? result.merchantName : result.returnmessage
        lblPrice.text = NSNumber(value: result.amount).formatMoneyValue()
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
