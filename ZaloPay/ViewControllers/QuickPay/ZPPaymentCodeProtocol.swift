//
//  ZPPaymentCodeContract.swift
//  ZaloPay
//
//  Created by vuongvv on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit

enum PaymentCodeTypePopup {
    case qrcode
    case barcode
}
// ZPPaymentCodePresenter -> ZPPaymentCodeController
protocol ZPPaymentCodeViewControllerInput: class {
    
    var presenter: ZPPaymentCodePresenterDelegate? { get set }
    
    func bindingQRImage(params: String)
        
    func showResultViewWithMessage(message: String)
    
    func updateView()
    
    func reloadDataAnScrollTo(index indexPath: IndexPath)
    
    func setAvatar(with url: URL)
    
    func showLoadingHud()
    
    func hideLoadingHud()
    
    func startTimerOverlapPmtCode(time:TimeInterval)
    func stopTimerOverlapPmtCode()
    func refreshPmtCode()
    func createQRImage()
    
    func verfyWithPin(model : ZPQRToPayMessage)
    func showError(error: NSError?, handle: (() -> Void)?)
    
    func checkShowSecurityIntro(isShow:Bool)
    func showResultView(result:ZPPaymentCodeResult)
    func showDisconnectView()
    func showTimeRetainDisconnectView(timeRetain:Int64)
    func hideTimeRetainDisconnectView()
    func verifyPinForHideSecurityView(hideNext:Bool)
    func showSlideIntro()
    func checkDisconnectWithEmptyAndStillRetainTimeCode()
    func stopAllTimerOverlap()
    func getForceGoToGenQRCode() -> Bool
}

// ZPPaymentCodeController -> ZPPaymentCodePresenter
protocol ZPPaymentCodePresenterDelegate: class {
    
    var controller: ZPPaymentCodeViewControllerInput? { get set }
    
    var interactor: ZPPaymentCodeInteractorInputDelegate? { get set }
    
    var router: ZPPaymentCodeRouterDelegate? { get set }
    
    func viewDidLoad()
    
    func showDialogQRCodeError()
    
    func createQRCodeImage()
    
    func backClicked()
    
    func setAvatar(with url: URL)
    
    func refreshPaymentCode()
    
    func verifyPin(model : ZPQRToPayMessage,password:String)
    
    func showError(error: NSError?, handle: (() -> Void)?)
    
    func gotoListLimitCash(_ finishedBlock: @escaping ZPPaymentCodeListLimitCashVCFinishedBlock)
    func gotoDisconnectVC()
    func gotoSupportCenterVC()
    func setpaymentCodeShowSecurityIntro(_ value: Bool)
    func presentSlideIntroVC()
    
    func loadUserInfo()
    func refreshAllPaymentCode()
    func handleObserverConnectVC(_ status: Bool)
    func verifyPinForHideSecurityView(hideNext:Bool)
    func gotoWithDrawVC()
    func gotoRechargeVC()
}

// ZPPaymentCodePresenter -> ZPPaymentCodeInteractor
protocol ZPPaymentCodeInteractorInputDelegate: class {
    
    var presenter: ZPPaymentCodeInteractorOutputDelegate? {get set}

    func setupInteractor()
    
    func createQRImage()

    func backAction()
    
    func verifyPin(model : ZPQRToPayMessage,password:String)
    
    func refreshPaymentCode()
    
    func paymentCodeShowSecurityIntro() -> Bool
    func setpaymentCodeShowSecurityIntro(_ value: Bool)
    
    func loadUserInfo()
    func refreshAllPaymentCode()
    func getAllRetainSecondPaymentCode()->Int64
    func getFirstShowSlideIntro() -> Bool?
    func setFirstShowSlideIntro()
}

// ZPPaymentCodeInteractor -> ZPPaymentCodePresenter
protocol ZPPaymentCodeInteractorOutputDelegate: class {
    
    func bindingQRImage(params: String)
    
    func showResultViewWithMessage(message: String)
    
    func updateView()
    
    func reloadDataAnScrollTo(index indexPath: IndexPath)
    
    func showDialogError(_ error: NSError?, handle: (() -> Void)?)
    
    func setAvatar(with url: URL)
    
    func showLoading()
    
    func hideLoading()
    
    func verfyWithPin(model : ZPQRToPayMessage)
    
    func startTimerOverlapPmtCode(time:TimeInterval)
    func stopTimerOverlapPmtCode()
    func refreshPmtCode()
    func createQRImage()
    func showResultView(result:ZPPaymentCodeResult)
    func showDisconnectView()
    func notiHidePopupZoomBarQrCode()
    func checkDisconnectWithEmptyAndStillRetainTimeCode()
    func backToHome()
}

protocol ZPPaymentCodeRouterDelegate: class {
    
    var rootViewController: UIViewController? { get set }
    
    func backViewController()

    func gotoListLimitCash(_ finishedBlock: @escaping ZPPaymentCodeListLimitCashVCFinishedBlock)
    func gotoDisconnectVC()
    func gotoSupportCenterVC()
    func presentSlideIntroVC()
    func gotoWithdrawVC()
    func gotoRechargeVC()
}
