//
//  ZPReceiveMoneyInteractor.swift
//  ZaloPay
//
//  Created by vuongvv on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import ZaloPayCommonSwift
import CocoaLumberjackSwift
import ZaloPayProfile
import ZaloPayConfig

let KQuickPayFirstShowSlideIntro = "QuickPayFirstShowSlideIntro"

class ZPPaymentCodeInteractor: NSObject, ZPPaymentCodeInteractorInputDelegate {
    
    weak var presenter: ZPPaymentCodeInteractorOutputDelegate?
    
    let timeMax = Int64((ZPApplicationConfig.getQuickPayConfig()?.getTimeRefresh() ?? 60000) / 1000)
    let kMaxPaymentCodeGet = Int(ZPApplicationConfig.getQuickPayConfig()?.getMaxPaymentCode() ?? 5)
    
    var arrPaymentCode = [ZPPaymentCodeModel]()
    var isFirstStartOpenGet = true
    let kAppIdGetListPaymentCode:Int32 = Int32(ZPPaymentCodeConfig.getChannelSelected().id)
    var isUserForceRefreshCode = false
    
    var lastCodeDidShow = ZPPaymentCodeModel()
    
    func setupInteractor() {
        self.handleNotificationQRToPay()
        arrPaymentCode = ZPPaymentCodeModel.getPaymentCodes() as! [ZPPaymentCodeModel]
    }
    
    func handleNotificationQRToPay(){
        let until = self.rac_willDeallocSignal()
        ZPConnectionManager.sharedInstance().messageSignal.take(until: until).filter({ value in
            if let qrMessage = value as? ZPQRToPayMessage,
                qrMessage.notificationType == NotificationType.qrToPay {
                return true
            }
            return false
        }).deliverOnMainThread().subscribeNext({ [weak self](notify) in
            guard let strongSelf = self else {
                return
            }
            let result = notify as! ZPQRToPayMessage
            let codeToRemove = (result.paymentcode ?? "").toUInt64()
            strongSelf.removePaymentCodeAndUpdateCache(codeToRemove)
            strongSelf.presenter?.startTimerOverlapPmtCode(time: 0)
            DDLogInfo("payment code notify: \(codeToRemove)")
            strongSelf.presenter?.notiHidePopupZoomBarQrCode()            
            let success = (result.isrequiredpin.not && result.resultStatus == ZALOPAY_ERRORCODE_SUCCESSFUL.rawValue)
            let response = strongSelf.resultObject(result, success: success)
            strongSelf.presenter?.showResultView(result: response)
            
            
            //                if result.isrequiredpin {
            //                    self?.presenter?.verfyWithPin(model: result)
            //                } else {
            //                    self?.getStatusByTransId(transId: "\(result.transid)")
            //                }
        })
    }
    
    func resultObject(_ message: ZPQRToPayMessage, success: Bool) -> ZPPaymentCodeResult{
        let result = ZPPaymentCodeResult()
        result.amount = message.amount.toUInt64()
        result.returncode = message.resultStatus
        result.merchantName = message.merchantName
        result.returnmessage = message.resultMessage ?? (success ? R.string_Dialog_SuccessTitle() : R.string_Dialog_FailTitle())
        result.isSuccess = success;
        return result
    }
    
    func createQRImage() {
        let (arrRetain,countGet) = getInfoPaymentCanUse()
        self.arrPaymentCode = arrRetain
        if arrRetain.count == kMaxPaymentCodeGet && isFirstStartOpenGet {
            isFirstStartOpenGet = false
            self.presenter?.startTimerOverlapPmtCode(time: 0)
            return
        }
        isFirstStartOpenGet = false
        
        self.presenter?.stopTimerOverlapPmtCode()
        
        //Check connect
        if self.arrPaymentCode.count == 0 && !NetworkState.sharedInstance().isReachable && !isFirstStartOpenGet {
            self.presenter?.checkDisconnectWithEmptyAndStillRetainTimeCode()
            if self.isUserForceRefreshCode {
                self.isUserForceRefreshCode = false
            }
            self.presenter?.notiHidePopupZoomBarQrCode()
            return
        }
        
        if !self.isUserForceRefreshCode {
            DispatchQueue.main.async {
                self.presenter?.showLoading()
            }
        }
        NetworkManager.sharedInstance().getPaymentCode(kAppIdGetListPaymentCode, chargeinfo: "",count:Int32(countGet),remaincode:Int32(arrRetain.count)).delay(0.1).deliverOnMainThread().subscribeNext({ [weak self](result) in
            
            DispatchQueue.main.async {
                self?.presenter?.hideLoading()
            }
            
            guard let resultGet = result as? NSDictionary else { return }
                let arrGet = ZPPaymentCodeModel().parse(withDic: resultGet as! [AnyHashable : Any]) as! [ZPPaymentCodeModel]
    
                //Cache
                ZPPaymentCodeModel.addPaymentCodes(arrGet)
                self?.arrPaymentCode = ZPPaymentCodeModel.getPaymentCodes() as! [ZPPaymentCodeModel]
            
                self?.presenter?.startTimerOverlapPmtCode(time: 0)
            
            }, error:({[weak self] (error) in
                //Check if empty array and disconnect when maybe have retain time of last code
                //=>Force show disconnect
                if self?.arrPaymentCode.count == 0{
                    self?.presenter?.checkDisconnectWithEmptyAndStillRetainTimeCode()
                }
                
                if (self?.isUserForceRefreshCode)! {
                    self?.isUserForceRefreshCode = false
                }
                self?.presenter?.notiHidePopupZoomBarQrCode()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self?.presenter?.showDialogError(error as NSError?, handle: nil)
                    self?.presenter?.hideLoading()
                }
            }))
    }
    
    func refreshPaymentCode() {
        
        //Check delete previous code
        if self.lastCodeDidShow.code > 0 {
            self.removePaymentCodeAndUpdateCache(self.lastCodeDidShow.code)
        }
        
        let arrTempt:[ZPPaymentCodeModel] = NSArray(array: self.arrPaymentCode
            ) as! [ZPPaymentCodeModel]
        guard arrTempt.count > 0,isFirstStartOpenGet == false else {
            self.presenter?.createQRImage()
            return
        }
        
        guard let model = arrTempt.first else {
            return
        }
        DDLogInfo("payment code show: \(model.code)")
        let timeToLiveNow =  Int64(model.expire) - Int64(NSDate().timeIntervalSince1970)
        if timeToLiveNow > 0 {
            lastCodeDidShow = model
            self.presenter?.bindingQRImage(params: "\(model.code)")
            if self.isUserForceRefreshCode {
                self.isUserForceRefreshCode = false
                self.presenter?.showResultViewWithMessage(message: R.string_QuickPay_UpdatePin_Success())
            }
            let timeNext = min(timeToLiveNow, Int64(self.timeMax))
            self.presenter?.startTimerOverlapPmtCode(time: TimeInterval(timeNext))
            
        } else {
            self.removePaymentCodeAndUpdateCache(model.code)
            self.presenter?.startTimerOverlapPmtCode(time: 0)
        }
    }
    
    func verifyPin(model : ZPQRToPayMessage,password:String){
        if password.count  == 0 {
            self.presenter?.showDialogError(NSError(domain: "", code: -1, userInfo: ["returnmessage":R.string_QuickPay_Error_Input_Password()]), handle: nil)
            return
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.presenter?.showLoading()
        }
        NetworkManager.sharedInstance().verifyPinQrcodePay("\(model.transid)", pin: password, time: NSDate().timeIntervalSince1970)
            .delay(1).deliverOnMainThread().subscribeNext({ [weak self](result) in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self?.presenter?.hideLoading()
                }
                guard let resultget = ZPPaymentCodeResult(result as! [AnyHashable : Any]) else {
                    let error = ZPPaymentCodeResult()
                    error.returncode = -1
                    error.returnmessage = R.string_QuickPay_Error_VerifyPin()
                    self?.presenter?.showResultView(result: error)
                    return
                }
                self?.presenter?.showResultView(result: resultget)
                
                }, error:({[weak self] (error) in
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        self?.presenter?.hideLoading()
                    }
                    guard let dicUserInfo = error?.userInfoData() else {
                        self?.presenter?.showDialogError(error as NSError?, handle: nil)
                        return
                    }
                    guard let resultget = ZPPaymentCodeResult(dicUserInfo as! [AnyHashable : Any]) else {
                        let error = ZPPaymentCodeResult()
                        error.returncode = -1
                        error.returnmessage = R.string_QuickPay_Error_VerifyPin()
                        self?.presenter?.showResultView(result: error)
                        return
                    }
                    if resultget.isprocessing {
                        self?.getStatusByTransId(transId: "\(resultget.zptransid)")
                        return
                    }
                    self?.presenter?.showResultView(result: resultget)
                    
                }))
        
    }
    func getStatusByTransId(transId:String){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.presenter?.showLoading()
        }
        NetworkManager.sharedInstance().getStatusTransId(transId, appid: Int32(ZPConstants.kAppIdGetStatus), start: Date()
            )
            .delay(1).deliverOnMainThread().subscribeNext({ [weak self](result) in
                //Show success
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self?.presenter?.hideLoading()
                }
                guard let resultget = result as? ZPPaymentCodeResult else {
                    let error = ZPPaymentCodeResult()
                    error.returncode = -1
                    error.returnmessage = R.string_QuickPay_Error_GetStatus()
                    self?.presenter?.showResultView(result: error)
                    return
                }
                self?.presenter?.showResultView(result: resultget)
                }, error:({[weak self] (error) in
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        self?.presenter?.hideLoading()
                    }
                    
                    guard let dicUserInfo = error?.userInfoData() else {
                        self?.presenter?.showDialogError(error as NSError?, handle: nil)
                        return
                    }
                    
                    //Case timeout get status
                    if error?.errorCode() == NSURLErrorTimedOut {
                        self?.presenter?.showDialogError(error as NSError?, handle: {
                            //Back to home
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                self?.presenter?.backToHome()
                            }
                        })
                        return
                    }
                    guard let resultget = ZPPaymentCodeResult(dicUserInfo as! [AnyHashable : Any]) else {
                        return
                    }
                    self?.presenter?.showResultView(result: resultget)
                }))
    }
    
    func removePaymentCodeAndUpdateCache(_ codeToRemove: UInt64){
        //Check delete expire/used
        if let index = self.arrPaymentCode.index(where: {$0.code == codeToRemove}) {
            self.arrPaymentCode.remove(at: index)
        }
        ZPPaymentCodeModel.savePaymentCodes(self.arrPaymentCode)
    }
    
    func refreshAllPaymentCode(){
        //Change next on local
        removePaymentCodeAndUpdateCache(lastCodeDidShow.code)
        isUserForceRefreshCode = true
        self.presenter?.startTimerOverlapPmtCode(time: 0)
    }
    
    func loadUserInfo() {
        
        ZPProfileManager.shareInstance.loadZaloUserProfile()
            .deliverOnMainThread()
            .subscribeNext({
                [weak self] user in
                if let user = user as? ZaloUserSwift {
                    let avatarURL = user.avatar
                    if let url = URL(string: avatarURL) {
                        self?.presenter?.setAvatar(with: url)
                    }
                }
                }, error: {
                    [weak self] error in
                    self?.presenter?.showDialogError(error as NSError?, handle: nil)
            })
    }
    
    func getInfoPaymentCanUse() ->([ZPPaymentCodeModel],Int){
        if !isFirstStartOpenGet {
            return ([ZPPaymentCodeModel](),kMaxPaymentCodeGet)
        }
        var arrPaymentCodeRetain = [ZPPaymentCodeModel]()
        let arrTempt:[ZPPaymentCodeModel] = NSArray(array: self.arrPaymentCode
            ) as! [ZPPaymentCodeModel]
        for paymentCode in arrTempt {
            let timeToLiveNow =  Int64(paymentCode.expire) - Int64(NSDate().timeIntervalSince1970)
            if timeToLiveNow > 0 {
                arrPaymentCodeRetain.append(paymentCode)
            }
            else {
                ZPPaymentCodeModel.removePaymentCode(paymentCode)
            }
        }
        guard arrPaymentCodeRetain.count > 0 else {
            return ([ZPPaymentCodeModel](),kMaxPaymentCodeGet)
        }
        
        return (arrPaymentCodeRetain,max(0,kMaxPaymentCodeGet - arrPaymentCodeRetain.count))
    }
    func paymentCodeShowSecurityIntro() -> Bool{
        return ZPSettingsConfig.sharedInstance().paymentCodeShowSecurityIntro
    }
    
    func setpaymentCodeShowSecurityIntro(_ value: Bool){
        ZPSettingsConfig.sharedInstance().paymentCodeShowSecurityIntro = value
    }
    func getAllRetainSecondPaymentCode()->Int64{
        var allRetain:Int64 = 0
        let arrTempt:[ZPPaymentCodeModel] = NSArray(array: self.arrPaymentCode) as! [ZPPaymentCodeModel]
        for model in arrTempt {
            let timeToLiveNow =  Int64(model.expire) - Int64(NSDate().timeIntervalSince1970)
            if timeToLiveNow > 0 && timeToLiveNow > allRetain {
                allRetain = timeToLiveNow
            }
        }
        return allRetain
    }
    func backAction() {
    }
    func getFirstShowSlideIntro() -> Bool?{
        let firstShowSlideIntro:Bool? = UserDefaults.standard.object(forKey: KQuickPayFirstShowSlideIntro) as? Bool
        return firstShowSlideIntro
    }
    func setFirstShowSlideIntro(){
        UserDefaults.standard.set(true, forKey: KQuickPayFirstShowSlideIntro)
        UserDefaults.standard.synchronize()
    }
}
