//
//  ZPPaymentCodeChannelView.swift
//  ZaloPay
//
//  Created by vuongvv on 5/3/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit

class ZPPaymentCodeChannelView: UIView {
    
    @IBOutlet weak var imgViewChannel: ZPIconFontImageView!
    @IBOutlet weak var imgViewCheck: ZPIconFontImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var bottomView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgViewCheck.setIconFont("general_check")
        imgViewCheck.setIconColor(UIColor.zaloBase())
        imgViewCheck.defaultView.font = UIFont.iconFont(withSize: 20)
        
        imgViewChannel.defaultView.font = UIFont.iconFont(withSize: 20)
        
        lblTitle.textColor = UIColor.subText()
        lblTitle.font
            = UIFont.sfuiTextRegular(withSize: 16)
        setupData()
    }
    func setupData(){
        guard let channelSelected = ZPPaymentCodeConfig.getArrChannelPayment().first  else {
            return
        }
        
        self.lblTitle.text = "\(channelSelected.name)"
        self.imgViewCheck.isHidden = false
        self.imgViewChannel.image = channelSelected.icon
    }
    
}
