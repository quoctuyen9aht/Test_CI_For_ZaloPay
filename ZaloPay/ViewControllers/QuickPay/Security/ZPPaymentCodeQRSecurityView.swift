//
//  ZPPaymentCodeQRIntroView.swift
//  ZaloPay
//
//  Created by vuongvv on 11/6/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
protocol ZPPaymentCodeQRSecurityViewDelegate : class {
    func OKClick(hideNext:Bool)
}
class ZPPaymentCodeQRSecurityView: UIView {
    
    @IBOutlet weak var viewBody: UIView!
    @IBOutlet weak var mImgView: UIImageView!
    @IBOutlet weak var lblDetail: UILabel!
    //@IBOutlet weak var lblMaxMoney: UILabel!
    @IBOutlet weak var btnOk: UIButton!
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var lblBalance: UILabel!
    @IBOutlet weak var lblVND: UILabel!
    
    //@IBOutlet weak var lblNote: UILabel!
    @IBOutlet weak var lblDontShare: UILabel!
    
    @IBOutlet weak var iconTopContraint: NSLayoutConstraint!
    @IBOutlet weak var iconWidthContraint: NSLayoutConstraint!
    @IBOutlet weak var iconHightContraint: NSLayoutConstraint!
    @IBOutlet weak var detailTopContraint: NSLayoutConstraint!
    @IBOutlet weak var buttonOkTopContraint: NSLayoutConstraint!
    weak var delegate:ZPPaymentCodeQRSecurityViewDelegate?
    let disposeBag = DisposeBag()
    var viewBg = UIView()
    func contentViewQR() -> UIView {
        
        if UIScreen.main.bounds.size.height > 568 {
            iconTopContraint.constant = 78
        }
        
        let view = ZPPaymentCodeQRView.zp_viewFromSameNib() ?? ZPPaymentCodeQRView()
        self.addSubview(view)
        view.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.width.equalToSuperview()
            make.height.equalToSuperview()
        }
        view.backgroundColor = .clear
        
        let imageSize = CGSize(width: 300, height: 300)
        let data = "11111111111111111".data(using: String.Encoding.isoLatin1)
        let imageQR = UIImage.createQRImage(with: imageSize, from: data)
        let barcodeQR = UIImage.createBarCodeImage(with: imageSize, from: data)
        view.qrImageView.image = imageQR
        view.barCodeImageView.image = barcodeQR
        self.sendSubview(toBack: view)
        return view
    }
    
    override func awakeFromNib() {
        
        //Fake below view
        self.backgroundColor = UIColor.clear
        self.addSubview(viewBg)
        self.sendSubview(toBack: viewBg)
        self.viewBg.backgroundColor = UIColor.clear //UIColor(white: 1, alpha: 0.985)
        self.viewBg.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.top.equalTo(0)
            make.bottom.equalTo(0)
        }
        //_ = self.contentViewQR()
        setupViewChannel()
        
        let fontSize: Int = (UIView.isIPhone4() || UIView.isIPhone5()) ? 14 : 15
        self.lblDetail.font
            = UIFont.sfuiTextRegular(withSize: CGFloat(fontSize))
        self.lblDetail.text = R.string_QuickPay_Secuirty_Warning()
        self.lblDetail.textColor = UIColor.subText()
        self.lblDetail.lineBreakMode = .byWordWrapping
        self.lblDetail.setLineSpacing(lineSpacing: 6.0)
        self.lblDetail.textAlignment = .center
//        let moneyGet = ("\(ZPPaymentCodeConfig.getpaymentLimitAmount())" as NSString).formatMoneyValue() ?? ""
//        let stringMaxMoney = String(format: R.string_QuickPay_Secuirty_Warning_Max(), moneyGet)
//        self.lblMaxMoney.font
//            = UIFont.sfuiTextRegular(withSize: 14)
//        self.lblMaxMoney.attributedText = editTextMaxMoney(str: stringMaxMoney, money: moneyGet)
//        self.lblMaxMoney.textColor = UIColor.subText()
        
        self.viewHeader.backgroundColor = .clear
        self.viewBody.roundRect(3)
        self.lblBalance.textColor = .white
        self.lblBalance.font
            = UIFont.sfuiTextRegular(withSize: 38)
        self.lblVND.textColor = .white
        self.lblVND.font
            = UIFont.sfuiTextRegular(withSize: 17)
        self.registerUpdateBalance()
        
        //self.lblNote.font
        // = UIFont.sfuiTextRegular(withSize: 14)
        //self.lblNote.textColor = UIColor.subText()
        
        self.lblDontShare.font
            = UIFont.sfuiTextRegularItalic(withSize: 14)
        self.lblDontShare.textColor = UIColor.subText()
        self.lblDontShare.text = R.string_QuickPay_Secuirty_Warning_DontShare()
        //Must set button to custom
        btnOk.setupZaloPay()
        btnOk.setTitle(R.string_QuickPay_Button_OK(), for: UIControlState.normal)
        btnOk.roundRect(Float(btnOk.frame.size.height/2))
        
        if UIView.isIPhone4() || UIView.isIPhone5() {
            iconTopContraint.constant -= 10
            buttonOkTopContraint.constant -= 20
            iconWidthContraint.constant = iconWidthContraint.constant * 0.9
            iconHightContraint.constant = iconHightContraint.constant * 0.9
        }
        
        let touchIdInfo = UIControl()
        touchIdInfo.tag = 301
        addSubview(touchIdInfo)
        let checkButton = UIButton(type: .custom)
        checkButton.roundRect(10)
        checkButton.layer.borderColor = UIColor.subText().cgColor
        checkButton.layer.borderWidth = 1.2
        checkButton.titleLabel?.font = UIFont.zaloPay(withSize: 12)
        checkButton.setIconFont("general_check", for: .selected)
        checkButton.setBackgroundColor(UIColor.white, for: .normal)
        checkButton.setBackgroundColor(UIColor.pay(), for: .selected)
        checkButton.setTitleColor(UIColor.subText(), for: .normal)
        checkButton.setTitleColor(UIColor.white, for: .selected)
        checkButton.isUserInteractionEnabled = false
        checkButton.isSelected = false
        touchIdInfo.isHidden = true
        
        let labelDescription = UILabel()
        labelDescription.isUserInteractionEnabled = false
       
        labelDescription.font = UIFont.sfuiTextRegular(withSize: CGFloat(fontSize))
        labelDescription.textColor = UIColor.subText()
        labelDescription.text = R.string_QuickPay_Secuirty_Warning_DontShow()
        let size = labelDescription.sizeThatFits(CGSize(width: 300, height: 20))
        touchIdInfo.addSubview(checkButton)
        touchIdInfo.addSubview(labelDescription)
        
        //float heightReduced = image == nil ? 40.0 : 0.0
        touchIdInfo.snp.makeConstraints { (make) in
            make.height.equalTo(0)
            make.bottom.equalTo(0)
            make.left.equalTo(0)
            make.right.equalTo(0)
        }
        labelDescription.snp.makeConstraints { (make) in
            make.right.equalTo(0)
            make.left.equalTo(touchIdInfo.snp.centerX).offset(-(size.width / 2 - 20))
            make.top.equalTo(0)
            make.bottom.equalTo(0)
        }
        checkButton.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.size.equalTo(CGSize(width:20,height:20))
            make.right.equalTo(labelDescription.snp.left).offset(-5)
        }
        touchIdInfo.rx.controlEvent(UIControlEvents.touchDown)
            .bind
            {
                checkButton.isSelected = !checkButton.isSelected
                checkButton.layer.borderColor = checkButton.isSelected
 ? UIColor.pay().cgColor : UIColor.subText().cgColor
            }.disposed(by: disposeBag)
        
        btnOk.rx.tap.bind {[weak self] _ in
            ZPTrackingHelper.shared().trackEvent(.quickpay_touch_cont)
            if NetworkState.sharedInstance().isReachable == false {
                ZPDialogView.showDialog(with: DialogTypeNoInternet, title: R.string_Dialog_Warning(), message: R.string_NetworkError_NoConnectionMessage(), buttonTitles:[R.string_ButtonLabel_Close()], handler: nil)
                return
            }
            self?.delegate?.OKClick(hideNext: checkButton.isSelected)
        }.disposed(by: disposeBag)
    }
    func editTextMaxMoney(str:String,money:String) -> NSMutableAttributedString{
        let att = NSMutableAttributedString(string: str)
        let rangeMoney = (str as NSString).range(of: money)
        if rangeMoney.location != NSNotFound {
            att.addAttribute(NSAttributedStringKey.font, value: UIFont.sfuiTextSemibold(withSize: 14), range: rangeMoney)
            att.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.zaloBase(), range: rangeMoney)
        }
        let rangeVND = (str as NSString).range(of: "VND")
        if rangeMoney.location != NSNotFound {
            att.addAttribute(NSAttributedStringKey.font, value: UIFont.sfuiTextRegular(withSize: 10), range: rangeVND)
            att.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.zaloBase(), range: rangeVND)
        }
        return att
    }

    func registerUpdateBalance() {
        self.updateBalanceView()
        ZMEventManager.registerHandler(self, eventType: EventTypeUpdateBalance.rawValue, with: {[weak self] (event: ZMEvent?) in
            DispatchQueue.main.async {
                self?.updateBalanceView()
            }
        })
    }
    
    func updateBalanceView() {
        self.lblVND.isHidden = false
        guard let balanceValue = ZPWalletManagerSwift.sharedInstance.currentBalance else {
            self.lblBalance.text = ""
            self.lblVND.isHidden = true
            return
        }
        self.showBalanceWithValue(balanceValue.int64Value)
    }
    
    func showBalanceWithValue(_ balanceValue: Int64) {
        if (balanceValue < 0) {
            // don't show balance value if
            // + balance is invalid
            // + or don't have correct value
            self.lblBalance.text = ""
            self.lblVND.isHidden = true
            return
        }
        self.lblBalance.text = NSNumber.init(value: balanceValue).formatMoneyValue()
    }
    func setupViewChannel() {
        let view = ZPPaymentCodeChannelView.zp_viewFromSameNib() ?? ZPPaymentCodeChannelView()
        self.addSubview(view)
        view.snp.makeConstraints { (make) in
            make.top.equalTo(self.btnOk.snp.bottom).offset(30)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(60)
        }
    }
}
