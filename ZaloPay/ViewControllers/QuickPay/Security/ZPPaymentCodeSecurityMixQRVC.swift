//
//  ZPPaymentCodeSecurityMixQRVC.swift
//  ZaloPay
//
//  Created by vuongvv on 5/16/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol ZPPaymentCodeSecurityMixQRVCDelegate : class {
    func OKClick()
}
class ZPPaymentCodeSecurityMixQRVC: BaseViewController {
    
    @IBOutlet weak var viewBody: UIView!
    @IBOutlet weak var mImgView: UIImageView!
    @IBOutlet weak var lblDetail: UILabel!
    //@IBOutlet weak var lblMaxMoney: UILabel!
    @IBOutlet weak var btnOk: UIButton!

    //@IBOutlet weak var lblNote: UILabel!
    @IBOutlet weak var lblDontShare: UILabel!
    
    @IBOutlet weak var iconTopContraint: NSLayoutConstraint!
    @IBOutlet weak var iconWidthContraint: NSLayoutConstraint!
    @IBOutlet weak var iconHightContraint: NSLayoutConstraint!
    @IBOutlet weak var detailTopContraint: NSLayoutConstraint!
    @IBOutlet weak var buttonOkTopContraint: NSLayoutConstraint!
    @IBOutlet weak var bottomViewBodyContraint: NSLayoutConstraint!
    @IBOutlet weak var topViewBodyContraint: NSLayoutConstraint!
    weak var delegate:ZPPaymentCodeSecurityMixQRVCDelegate?
    let disposeBag = DisposeBag()
    var viewBg = UIView()
    
    override func isAllowSwipeBack() -> Bool {
        return false
    }
    override func viewDidLoad() {
        super.viewDidLoad()
         
        //Fake below view
        self.view.backgroundColor = UIColor.clear
        self.view.addSubview(viewBg)
        self.view.sendSubview(toBack: viewBg)
        self.viewBg.backgroundColor = UIColor(red: 36 / 255, green: 39 / 255, blue: 43 / 255, alpha: 0.7)
        self.viewBg.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.top.equalTo(0)
            make.bottom.equalTo(0)
        }
        viewBody.layer.masksToBounds = true
        viewBody.layer.cornerRadius = 3
        
        setupViewChannel()
        
        let fontSize: Int = (UIView.isIPhone4() || UIView.isIPhone5()) ? 14 : 15
        self.lblDetail.font
            = UIFont.sfuiTextRegular(withSize: CGFloat(fontSize))
        self.lblDetail.text = R.string_QuickPay_Secuirty_Warning()
        self.lblDetail.textColor = UIColor.subText()
        self.lblDetail.lineBreakMode = .byWordWrapping
        self.lblDetail.setLineSpacing(lineSpacing: 6.0)
        self.lblDetail.textAlignment = .center
        
        self.lblDontShare.font
            = UIFont.sfuiTextRegularItalic(withSize: 14)
        self.lblDontShare.textColor = UIColor.subText()
        self.lblDontShare.text = R.string_QuickPay_Secuirty_Warning_DontShare()
        //Must set button to custom
        btnOk.setupZaloPay()
        btnOk.setTitle(R.string_QuickPay_Button_OK(), for: UIControlState.normal)
        btnOk.roundRect(Float(btnOk.frame.size.height/2))
        
        adjustUIByVariousDevice()
        
        btnOk.rx.tap.bind {[weak self] _ in
            ZPTrackingHelper.shared().trackEvent(.quickpay_touch_cont)
            if NetworkState.sharedInstance().isReachable == false {
                ZPDialogView.showDialog(with: DialogTypeNoInternet, title: R.string_Dialog_Warning(), message: R.string_NetworkError_NoConnectionMessage(), buttonTitles:[R.string_ButtonLabel_Close()], handler: nil)
                return
            }
            self?.verifyPinSecurityView()
        }.disposed(by: disposeBag)
    }
    func adjustUIByVariousDevice() {
        
        let scaleHeight = UIScreen.main.bounds.height / 763
        var kiconTopContraint = 36 * scaleHeight
        var kdetailTopContraint = 30 * scaleHeight
        var kbuttonOkTopContraint = 46 * scaleHeight + (UIView.isHeightIP6PlusOrLarger() ? 20 : 0)
        var ktopViewBodyContraint  = 56 * scaleHeight
        var kbottomViewBodyContraint = 56 * scaleHeight
        
        if UIView.isIPhone4() {
            kiconTopContraint = 10
            kdetailTopContraint  = 15
            kbuttonOkTopContraint = 15
            ktopViewBodyContraint = 12
            kbottomViewBodyContraint = 12
            iconWidthContraint.constant = iconWidthContraint.constant *  0.7
            iconHightContraint.constant = iconHightContraint.constant *  0.7
        }
        if UIView.isIPhone5() {
            kiconTopContraint = 20
            kdetailTopContraint  = 25
            kbuttonOkTopContraint = 25
            ktopViewBodyContraint = 12
            kbottomViewBodyContraint = 12
            iconWidthContraint.constant = iconWidthContraint.constant * 0.9
            iconHightContraint.constant = iconHightContraint.constant * 0.9
        }
        
        iconTopContraint.constant = kiconTopContraint
        detailTopContraint.constant = kdetailTopContraint
        buttonOkTopContraint.constant = kbuttonOkTopContraint
        topViewBodyContraint.constant = ktopViewBodyContraint
        bottomViewBodyContraint.constant = kbottomViewBodyContraint + kHeightQRCodeBottom + kQRCodeBottomSafeInset
    }
    func setupViewChannel() {
        let view = ZPPaymentCodeChannelView.zp_viewFromSameNib() ?? ZPPaymentCodeChannelView()
        self.viewBody.addSubview(view)
        view.snp.makeConstraints { (make) in
            //make.top.equalTo(self.btnOk.snp.bottom).offset(30)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(60)
            make.bottom.equalTo(0)
        }
    }
    func verifyPinSecurityView() {
        ZPAppFactory.sharedInstance().showPinDialogMessage(R.string_QuickPay_Input_Password_Function(),
                                                           isForceInput: true,
                                                           success: { [weak self] in
                                                            self?.delegate?.OKClick()
            }, error: nil)
    }
}
