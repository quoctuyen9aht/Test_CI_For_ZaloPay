//
//  ZPPaymentCodeCache.h
//  ZaloPay
//
//  Created by vuongvv on 11/8/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ZPPaymentCodeModel : NSObject

@property (nonatomic, assign) uint64_t code;
@property (nonatomic, assign) uint64_t timetolive;
@property (nonatomic, assign) uint64_t expire;

- (instancetype)initWith:(uint64_t)code timetolive:(uint64_t)timetolive expire:(uint64_t)expire;
-(NSMutableArray*)parseWithDic:(NSDictionary *)dict;
+(NSArray*)getPaymentCodes;
+(void)savePaymentCodes:(NSArray*)arrPaymentCode;
+(void)addPaymentCodes:(NSArray*)arrPaymentCode;
+(void)removeAllPaymentCodes;
+(void)removePaymentCode:(ZPPaymentCodeModel*)paymentCode;
@end
