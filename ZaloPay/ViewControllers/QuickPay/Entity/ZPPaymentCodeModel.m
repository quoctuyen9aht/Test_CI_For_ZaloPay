//
//  ZPPaymentCodeCache.m
//  ZaloPay
//
//  Created by vuongvv on 11/8/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import "ZPPaymentCodeModel.h"
#import <ZaloPayCommonSwift/ZaloPayCommonSwift-Swift.h>
#import <ZaloPayConfig/ZaloPayConfig-Swift.h>

#define kPaymentCodeCache  @"PaymentCodeCache_asdf"
#define kPaymentCodeCacheService  @"PaymentCodeCache_3245234"
#import ZALOPAY_MODULE_SWIFT
//#import "ApplicationState.h"
//#import "ApplicationState+GetConfig.h"
@implementation ZPPaymentCodeModel
- (instancetype)initWith:(uint64_t)code timetolive:(uint64_t)timetolive expire:(uint64_t)expire {
    self = [super init];
    if (self) {
        self.code = code;
        self.timetolive = timetolive;
        self.expire = expire;
    }
    return self;
}
-(NSMutableArray*)parseWithDic:(NSDictionary *)dict {
    NSMutableArray *paymentcodesGet = [NSMutableArray new];
    NSArray *paymentcodes = [dict arrayForKey:@"paymentcodes" defaultValue:[NSArray new]];
    for (NSDictionary *dicItem in paymentcodes) {
        uint64_t code = [dicItem uint64ForKey:@"code" defaultValue:0];
        uint64_t timetolive = [dicItem uint64ForKey:@"timetolive" defaultValue:0];
        uint64_t expire = [[NSDate date] timeIntervalSince1970] + timetolive / 1000;
        ZPPaymentCodeModel* model = [[ZPPaymentCodeModel alloc] initWith:code timetolive:timetolive expire:expire];
        [paymentcodesGet addObject:model];
    }
    return paymentcodesGet;
}
+(NSArray*)getPaymentCodes{
    NSMutableArray *arrCodesGet = [NSMutableArray new];
    NSString *value = [KeyChainStore stringForKey:kPaymentCodeCache service:kPaymentCodeCacheService accessGroup:nil];
    if ([value length] > 0) {
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[value componentsSeparatedByString:@"|"] ];
        [arr removeLastObject];
        for (NSString *strCode in arr) {
            NSArray *arrItem = [strCode componentsSeparatedByString:@"-"];
            if ([arrItem count] == 3){
                ZPPaymentCodeModel *model = [ZPPaymentCodeModel new];
                model.code = [[NSString stringWithFormat:@"%@",arrItem[0]] longLongValue];
                model.timetolive = [[NSString stringWithFormat:@"%@",arrItem[1]] longLongValue];
                model.expire = [[NSString stringWithFormat:@"%@",arrItem[2]] longLongValue];
                [arrCodesGet addObject:model];
            }
        }
    }
    return arrCodesGet;
}
+(void)savePaymentCodes:(NSArray*)arrPaymentCode{
    NSMutableArray *arrPaymentCodeCache = [NSMutableArray arrayWithArray:arrPaymentCode];
//    int maxCodeGet = [ApplicationState getIntConfigFromDic:@"quickpay" andKey:@"max_payment_code" andDefault:5];
    
    id<ZPQuickPayConfigProtocol> quickPayConfigProtocol = [ZPApplicationConfig getQuickPayConfig];
    NSInteger maxCodeGet = quickPayConfigProtocol ? [quickPayConfigProtocol getMaxPaymentCode] : 5;
    
    while (arrPaymentCodeCache.count > maxCodeGet) {
        [arrPaymentCodeCache removeObjectAtIndex:0];
    }
    
    NSMutableString *strStore = [NSMutableString stringWithString:@""];
    for (ZPPaymentCodeModel *model in arrPaymentCodeCache) {
        [strStore appendFormat:@"%llu-%llu-%llu|",model.code,model.timetolive,model.expire];
    }
    if ([strStore length] == 0){
        return;
    }
    [KeyChainStore setWithString:strStore forKey:kPaymentCodeCache service:kPaymentCodeCacheService accessGroup:nil];
}
+(void)addPaymentCodes:(NSArray*)arrPaymentCode{
    NSMutableArray *arrPaymentCodeCache = [NSMutableArray arrayWithArray:[ZPPaymentCodeModel getPaymentCodes]];
    [arrPaymentCodeCache addObjectsFromArray:arrPaymentCode];
    [ZPPaymentCodeModel savePaymentCodes:arrPaymentCodeCache];
}
+(void)removeAllPaymentCodes{
    [KeyChainStore setWithString:@"" forKey:kPaymentCodeCache service:kPaymentCodeCacheService accessGroup:nil];
}
+(void)removePaymentCode:(ZPPaymentCodeModel*)paymentCode{
    NSMutableArray *arrPaymentCode = [NSMutableArray arrayWithArray:[ZPPaymentCodeModel getPaymentCodes]];
    if ([arrPaymentCode count] == 0){
        return;
    }
    NSMutableArray * tempArray = [arrPaymentCode mutableCopy];
    for (ZPPaymentCodeModel * model in arrPaymentCode){
        if (model.code == paymentCode.code){
            [tempArray removeObject: model];
            break;
        }
    }
    arrPaymentCode = tempArray;
    [ZPPaymentCodeModel savePaymentCodes:arrPaymentCode];
}
@end

