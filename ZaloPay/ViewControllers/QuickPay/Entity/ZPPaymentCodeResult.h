//
//  ZPPaymentCodeResult.h
//  ZaloPay
//
//  Created by vuongvv on 11/22/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZPPaymentCodeResult : NSObject
@property (nonatomic) BOOL isSuccess;
@property (nonatomic) uint64_t amount;
@property (nonatomic) uint64_t discountamount;
@property (nonatomic) uint64_t zptransid;
@property (nonatomic) BOOL isprocessing;
@property (nonatomic) int returncode;
@property (nonatomic) NSString* returnmessage;
@property (nonatomic) NSString* merchantName;

- (instancetype)initWith:(NSDictionary *)dict;

@end
