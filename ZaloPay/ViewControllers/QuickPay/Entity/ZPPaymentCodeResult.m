//
//  ZPPaymentCodeResult.m
//  ZaloPay
//
//  Created by vuongvv on 11/22/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import "ZPPaymentCodeResult.h"

@implementation ZPPaymentCodeResult

- (instancetype)initWith:(NSDictionary *)dict{
    self = [super init];
    if (self) {
        self.amount = [dict uint64ForKey:@"amount" defaultValue:0];
        self.discountamount = [dict uint64ForKey:@"discountamount" defaultValue:0];
        self.zptransid = [dict uint64ForKey:@"zptransid" defaultValue:0];
        self.returncode = [dict intForKey:@"returncode" defaultValue:0];
        self.returnmessage = [dict stringForKey:@"returnmessage" defaultValue:0];
        self.isprocessing = [dict boolForKey:@"isprocessing" defaultValue:false];
    }
    return self;
}
@end
