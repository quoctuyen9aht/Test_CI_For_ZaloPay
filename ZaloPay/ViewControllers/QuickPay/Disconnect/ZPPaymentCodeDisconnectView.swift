//
//  ZPPaymentCodeDisconnectView.swift
//  ZaloPay
//
//  Created by vuongvv on 11/15/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

class ZPPaymentCodeDisconnectView: UIView {
    @IBOutlet weak var imgView: ZPIconFontImageView!
    @IBOutlet weak var lblDes: MDHTMLLabel!
    @IBOutlet weak var btnRefresh: UIButton!
    override func awakeFromNib() {
        let fontSize: Int = UIView.isScreen2x() ? 15 : 17
        self.lblDes.font
            = UIFont.sfuiTextRegular(withSize: CGFloat(fontSize))
        self.lblDes.text = R.string_QuickPay_Disconnect()
        self.lblDes.textColor = UIColor.subText()
        
        self.btnRefresh.setupZaloPay()
        self.btnRefresh.layer.cornerRadius = btnRefresh.frame.size.height/2
    }
}
