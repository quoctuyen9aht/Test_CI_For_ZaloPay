//
//  IntroViewController.swift
//  ZaloPay
//
//  Created by vuongvv on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
protocol IntroViewControllerDelegate : class {
    func hideClick()
}
class IntroViewController: UIViewController {
    weak var delegate:IntroViewControllerDelegate?
    private (set) lazy var model: IntroModel! = {
        return IntroModel()
    }()
    
    private (set) lazy var introView: IntroView! = {
        return IntroView()
    }()
    
    override func loadView() {
        view = self.introView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = UIColor.zaloBase()
        setUpOnboard()
    }
    private func setUpOnboard() {
        let setUpActionButtonBlock: OnboardViewConfigureActionButtonBlock = {
            (button) in
            
            button.titleLabel?.font = UIFont.sfuiTextRegular(withSize: 16)
            button.setTitleColor(UIColor.white, for: .normal)
            button.setTitleColor(UIColor.white, for: .highlighted)
        }
        
        introView.onboardView.delegate = self
        introView.onboardView.dataSource = self
        
        introView.onboardView.dotImage = UIImage(named: "IntroDotImage")
        introView.onboardView.currentDotImage = UIImage(named: "IntroCurrentDotImage")
        
        introView.onboardView.setUpLeftActionButtonWith(setUpActionButtonBlock)
        introView.onboardView.setUpRightActionButtonWith(setUpActionButtonBlock)
        
        introView.onboardView.reloadData()
    }
    func setDidShowSlideIntro(){
        UserDefaults.standard.set(true, forKey: KQuickPayFirstShowSlideIntro)
        UserDefaults.standard.synchronize()
        
        self.delegate?.hideClick()
    }
}

extension IntroViewController: OnboardViewDelegate {
    func onboardView(_ onboardView: OnboardView, didTouchOnLeftActionButtonAt index: Int) {
        self.dismiss(animated: true, completion: nil)
        if index < model.numberOfPages() - 1 {
            setDidShowSlideIntro()
        }
    }
    
    func onboardView(_ onboardView: OnboardView, didTouchOnRightActionButtonAt index: Int) {
        
        if index < 2 {
            onboardView.scrollToNextPage(animated: true)
        }
        if index == model.numberOfPages() - 1 {
            self.dismiss(animated: true, completion: nil)
            setDidShowSlideIntro()
        }
        
    }
}

extension IntroViewController: OnboardViewDataSource {
    func numberOfPages(in onboardView: OnboardView) -> Int {
        return model.numberOfPages()
    }

    func onboardView(_ onboardView: OnboardView, viewForPageAtIndex index: Int) -> UIView {
        let pageModel = model.pageModel(forPageAtIndex: index)
        
        let page = IntroPage()
        page.title = pageModel.title
        page.subtitle = pageModel.subtitle
        page.image = UIImage(named: pageModel.imageName)
        page.type = pageModel.type
        return page
    }
    
    public func onboardView(_ onboardView: OnboardView, shouldHideRightActionButtonForPageAt index: Int) -> Bool {
        return model.shouldHideRightActionButton(forPageAtIndex: index)
    }
    
    public func onboardView(_ onboardView: OnboardView, titleForRightActionButtonAt index: Int) -> String?  {
        return model.rightActionButtonTitle(atIndex: index)
    }
    
    public func onboardView(_ onboardView: OnboardView, shouldHideLeftActionButtonForPageAt index: Int) -> Bool {
        return model.shouldHideLeftActionButton(forPageAtIndex: index)
    }
    
    public func onboardView(_ onboardView: OnboardView, titleForLeftActionButtonAt index: Int) -> String? {
        return model.leftActionButtonTitle(atIndex: index)
    }
}
