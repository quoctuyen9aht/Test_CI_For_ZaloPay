//
//  IntroView.swift
//  ZaloPay
//
//  Created by vuongvv on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit

class IntroView: UIView {
    private (set) var onboardView: OnboardView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.zaloBase()
        
        onboardView = OnboardView()
        onboardView.backgroundColor = UIColor.zaloBase()
        self.addSubview(onboardView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let onboardViewX = CGFloat(0.0)
        var onboardViewY = CGFloat(0.0)
        var onboardViewWidth = self.bounds.size.width
        var onboardViewHeight = self.bounds.size.height
        if #available(iOS 11, *) {
            onboardViewY = self.safeAreaInsets.top
            onboardViewWidth = self.bounds.size.width - self.safeAreaInsets.left - self.safeAreaInsets.right
            onboardViewHeight = self.bounds.size.height - self.safeAreaInsets.top - self.safeAreaInsets.bottom
        }
        
        onboardView.frame = CGRect(
            x: onboardViewX,
            y: onboardViewY,
            width: onboardViewWidth,
            height: onboardViewHeight
        )
    }
}
