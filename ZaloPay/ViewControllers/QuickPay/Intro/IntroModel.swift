//
//  IntroModel.swift
//  ZaloPay
//
//  Created by vuongvv on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit

class IntroModel {
    public lazy var pages: [IntroPageModel] = {
        var pages = [IntroPageModel]()
        pages.append(IntroPageModel.pageModel(
            title: R.string_QuickPay_Slide_Intro_Title_1(),
            subtitle: R.string_QuickPay_Slide_Intro_SubText_1(),
            imageName: "quickpay_intro_1", type: IntroPageType.Normal
        ))
        pages.append(IntroPageModel.pageModel(
            title: R.string_QuickPay_Slide_Intro_Title_2(),
            subtitle: R.string_QuickPay_Slide_Intro_SubText_2(),
            imageName: "quickpay_intro_2", type: IntroPageType.Normal
        ))
        pages.append(IntroPageModel.pageModel(
            title: R.string_QuickPay_Slide_Intro_Title_3(),
            subtitle: R.string_QuickPay_Slide_Intro_SubText_3(),
            imageName: "quickpay_intro_3", type: IntroPageType.Column
        ))
        return pages
    }()
    
    func numberOfPages() -> Int {
        return pages.count
    }
    
    func pageModel(forPageAtIndex index: Int) -> IntroPageModel {
        return pages[index]
    }
    
    func shouldHideLeftActionButton(forPageAtIndex index: Int) -> Bool {
        return false
    }
    
    func leftActionButtonTitle(atIndex index: Int) -> String {
        return index < 2 ? R.string_ButtonLabel_Skip() : ""
    }
    
    func shouldHideRightActionButton(forPageAtIndex index: Int) -> Bool {
        return false
    }
    
    func rightActionButtonTitle(atIndex index: Int) -> String {
        return index < 2 ? R.string_ButtonLabel_Next() : R.string_ButtonLabel_Begin()
    }
}
enum IntroPageType {
    case Normal
    case Column
}
class IntroPageModel {
    private (set) var title: String!
    private (set) var subtitle: String!
    private (set) var imageName: String!
    private (set) var type: IntroPageType!
    public class func pageModel(title: String!, subtitle: String!, imageName: String!,type:IntroPageType) -> IntroPageModel {
        let pageModel = IntroPageModel()
        pageModel.title = title
        pageModel.subtitle = subtitle
        pageModel.imageName = imageName
        pageModel.type = type
        return pageModel
    }
    
}
