//
//  IntroPage.swift
//  ZaloPay
//
//  Created by vuongvv on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//


import UIKit
import SnapKit
import ZaloPayCommonSwift

class IntroPage: UIView {
    public var title: String!
    public var subtitle: String!
    public var image: UIImage! {
        get {
            return imageView.image
        }
        
        set {
            imageView.image = newValue
        }
    }
    public var type: IntroPageType!
    
    private var titleLabel: UILabel = UILabel()
    private var subtitleLabel: UILabel?
    private var imageView: UIImageView = UIImageView()
    private var subtitleView: UIView?
    let heightSubtitleView:CGFloat = 60
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.zaloBase()
        
        imageView = UIImageView()
        self.addSubview(imageView)
        
        titleLabel = UILabel()
        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.font = UIFont.sfuiTextMedium(withSize: 20)
        titleLabel.textColor = UIColor.white
        self.addSubview(titleLabel)
        
        let bottomView = UIView()
        bottomView.backgroundColor = UIColor(white: 1, alpha: 0.3)
        self.addSubview(bottomView)
        bottomView.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.width.equalToSuperview()
            make.height.equalTo(1)
            make.bottom.equalToSuperview().offset(-PVOnboardFooterViewHeight - 1)
        }

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        titleLabel.text = title
        
        if (type == IntroPageType.Normal) {
            if subtitleLabel != nil {
                return
            }
            subtitleLabel = UILabel()
            subtitleLabel?.numberOfLines = 0
            subtitleLabel?.textAlignment = .center
            subtitleLabel?.lineBreakMode = .byWordWrapping
            subtitleLabel?.font = UIFont.sfuiTextRegular(withSize: 16)
            subtitleLabel?.textColor = UIColor.white
            self.addSubview(subtitleLabel!)
            subtitleLabel?.text = subtitle
        }
        else {
            if subtitleView != nil {
                return
            }
            subtitleView = UIView()
            subtitleView?.backgroundColor = UIColor.clear
            self.addSubview(subtitleView!)
        }
        
        let titleLabelWidthInsets = CGFloat(48.0)
        let titleLabelSize = titleLabel.sizeThatFits(CGSize(
            width: bounds.size.width - titleLabelWidthInsets,
            height: CGFloat.greatestFiniteMagnitude
        ))
        
        let subtitleLabelWidthInsets = CGFloat(48.0)
        // Size frame sublbl with max length
        let arrSubLbl:[String] = [R.string_QuickPay_Slide_Intro_SubText_1(),R.string_QuickPay_Slide_Intro_SubText_2()]
        var rectMax:CGSize = CGSize.zero
        for textSubLbl in arrSubLbl {
            subtitleLabel?.text = textSubLbl
            let subtitleLabelGet = subtitleLabel?.sizeThatFits(CGSize(
                width: bounds.size.width - subtitleLabelWidthInsets,
                height: CGFloat.greatestFiniteMagnitude
            )) ?? CGSize.zero
            if rectMax.height < subtitleLabelGet.height {
                rectMax.height = subtitleLabelGet.height
            }
        }
        subtitleLabel?.text = subtitle
        let subtitleLabelSize = subtitleLabel?.sizeThatFits(CGSize(
            width: bounds.size.width - subtitleLabelWidthInsets,
            height: CGFloat.greatestFiniteMagnitude
        )) ?? CGSize.zero
        let diffSize = rectMax.height - subtitleLabelSize.height
        
        
        //let titleLabelHeightMultiplier = CGFloat(0.14)
        var offSetY:CGFloat = 15
        var marginImgView:CGFloat = 180
        var marginImgViewLeftRight:CGFloat = 80
        var marginBottomSubLabel:CGFloat = 50
        if UIScreen.main.bounds.size.height == 568 {
            marginImgView = 120
            marginImgViewLeftRight = 40
            marginBottomSubLabel = 10
        }
        else if UIScreen.main.bounds.size.height == 480 {
            marginImgView = 80
            marginImgViewLeftRight = 40
            marginBottomSubLabel = 0
        }
        
        if (type == IntroPageType.Normal) {
            offSetY = bounds.size.height - offSetY - subtitleLabelSize.height - PVOnboardFooterViewHeight - marginBottomSubLabel - diffSize
            self.subtitleLabel?.frame = CGRect(
                x: (bounds.size.width - subtitleLabelSize.width) / 2.0,
                y: offSetY,
                width: subtitleLabelSize.width,
                height: subtitleLabelSize.height);
        }
        else {
            
            offSetY = bounds.size.height - offSetY - heightSubtitleView - PVOnboardFooterViewHeight - marginBottomSubLabel
            self.subtitleView?.frame = CGRect(
                x: 20,
                y: offSetY,
                width: bounds.size.width - 48,
                height: heightSubtitleView);
            self.view4column()
        }
        
        offSetY = offSetY - titleLabelSize.height - 16
        titleLabel.frame = CGRect(
            x: (bounds.size.width - titleLabelSize.width) / 2.0,
            y: offSetY,
            width: titleLabelSize.width,
            height: titleLabelSize.height
        )
        
        if (imageView.image?.size) != nil {
            let heightRetain = min(offSetY - marginImgView, bounds.size.width - marginImgViewLeftRight)
            offSetY = offSetY - marginImgView / 2 - heightRetain
            imageView.contentMode = .scaleAspectFit
            imageView.frame = CGRect(
                x: (bounds.size.width - heightRetain) / 2.0,
                y: offSetY,
                width: heightRetain,
                height: heightRetain
            )
        }
    }
    func view4column(){
        
        let arrIntro = R.string_QuickPay_Slide_Intro_SubText_3().components(separatedBy: "-")
        let numberRow = arrIntro.count / 2
        for i in 0..<numberRow {
            let viewRow = UIView()
            self.subtitleView?.addSubview(viewRow)
            viewRow.snp.makeConstraints { (make) in
                make.width.equalToSuperview()
                make.height.equalTo(heightSubtitleView / 2)
                make.left.equalTo(0)
                make.top.equalTo(CGFloat(i) * heightSubtitleView / 2)
            }
            viewRow.backgroundColor = .clear
            let numberCol = 2
            for k in 0..<numberCol {
                let title = arrIntro[numberCol * i + k];
                let viewSub = subview1column(title:title)
                viewRow.addSubview(viewSub)
                viewSub.snp.makeConstraints { (make) in
                    make.width.equalTo((self.subtitleView?.frame.width)! / 2)
                    make.height.equalToSuperview()
                    make.left.equalTo((self.subtitleView?.frame.width)! / 2 * CGFloat(k))
                    make.top.equalTo(0)
                }
                viewSub.backgroundColor = .clear
            }
            
        }

    }
    func subview1column(title:String)->UIView{
        let view = UIView()
        let imgCheck = ZPIconFontImageView()
        let lblTitle = UILabel()
        view.addSubview(imgCheck)
        view.addSubview(lblTitle)
        
        imgCheck.snp.makeConstraints { (make) in
            make.width.equalTo(20)
            make.height.equalTo(20)
            make.centerX.equalToSuperview().offset(-40)
            make.centerY.equalToSuperview()
        }
        
        imgCheck.setIconFont("quickpay_check")
        imgCheck.setIconColor(UIColor.white)
        imgCheck.backgroundColor = .clear
        imgCheck.defaultView.font = UIFont.iconFont(withSize: 16)
        
        lblTitle.snp.makeConstraints { (make) in
            make.left.equalTo(imgCheck.snp.right).offset(5)
            make.right.equalTo(0)
            make.height.equalToSuperview()
            make.top.equalTo(0)
        }
        lblTitle.textAlignment = .left
        lblTitle.font = UIFont.sfuiTextRegular(withSize: 17)
        lblTitle.textColor = UIColor.white
        lblTitle.text = title
        
        return view
    }
}
