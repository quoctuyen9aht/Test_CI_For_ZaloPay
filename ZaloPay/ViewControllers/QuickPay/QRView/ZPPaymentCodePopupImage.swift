//
//  ZPPaymentCodePopupImage.swift
//  ZaloPay
//
//  Created by vuongvv on 10/18/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

let kZPPaymentCodePopupImageNotiHide = "ZPPaymentCodePopupImageNotiHide"
class ZPPaymentCodePopupImage: UIView {
    var container = UIView()
    var imageView = UIImageView()
    var lblBarCode = UILabel()
    var imageAvatar = UIImage()
    var rectOld = CGRect()
    var viewBg = UIView()
    var viewAvatarSmall = UIView()
    var imgAvatarSmall = UIImageView()
    var typePopup = PaymentCodeTypePopup.barcode
    func showWithImage(frameCurrent:CGRect,image:UIImage,imageAvatar:UIImage,code:String,type:PaymentCodeTypePopup) {
        typePopup = type
        self.backgroundColor = UIColor.clear
        self.addSubview(viewBg)
        self.viewBg.backgroundColor = UIColor(white: 1, alpha: 0.0)
        self.viewBg.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.top.equalTo(0)
            make.bottom.equalTo(0)
        }
        
        var frameGet = frameCurrent
        frameGet.origin.y += CGFloat(UINavigationController.navigationBarHeight())
        frameGet.origin.x += 10
        self.container.frame = frameGet
        rectOld = frameGet
        
        lblBarCode.text = editCodeDisplay(code: code)
        lblBarCode.font = UIFont.sfuiTextMedium(withSize: 20)
        self.imageAvatar = imageAvatar
        setupView(image:image,type:type)
        
        guard let window = UIApplication.shared.keyWindow else { return }
        self.frame = window.frame
        
        window.rootViewController?.view.addSubview(self)
        
        self.container.alpha = 0
        UIView.animate(withDuration: 0.5,delay:0.0,options:UIViewAnimationOptions.curveEaseIn, animations: {
            type == PaymentCodeTypePopup.qrcode ? self.animationQR() :  self.animationBarCode()
            self.viewBg.backgroundColor = UIColor(white: 1, alpha: 1.0)
            self.container.alpha = 1
        }) { (finished) in
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(hide))
        self.addGestureRecognizer(tap)
        
        NotificationCenter.default.rac_addObserver(forName: kZPPaymentCodePopupImageNotiHide, object: nil).subscribeNext { [weak self](_) in
            guard let wSelf = self else { return }
            wSelf.hide()
        }
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    func editCodeDisplay(code:String) -> String{
        if code.count <= 8 {
            return code
        }
        let codeEdit  =  NSMutableString(string: code)
        let arrSpaceCharater = Array(repeating: " ", count: code.count - 1)
        var index = 1
        for space in arrSpaceCharater {
            codeEdit.insert(space, at: index)
            index += 2
        }
        
        index = 7
        let arrSpace = Array(repeating: "  ", count: 2)
        for space in arrSpace {
            codeEdit.insert(space, at: index)
            index += 10
        }
       
        return codeEdit as String
    }
    @objc func hide(){
        UIView.animate(withDuration: 0.5, animations: {
            if self.typePopup == .barcode {
                self.lblBarCode.snp.updateConstraints({ (make) in
                    make.height.equalTo(0)
                })
                self.imageView.snp.updateConstraints ({ (make) in
                    make.bottom.equalTo(0)
                })
                self.container.layer.anchorPoint = self.layer.anchorPoint
                self.container.transform = self.transform
                self.container.frame = self.rectOld
            }
            else {
                self.container.frame = self.rectOld
                self.updateFrame(type: self.typePopup)
            }

        }) { (finished) in
            UIView .animate(withDuration: 0.1, animations: {
                self.viewBg.alpha = 0
            })
            { (finished) in
                self.removeFromSuperview()
            }
        }
    }
    
    func updateQuickPayImage(imageQR:UIImage, barcodeQR: UIImage, codeString: String) {
        if typePopup == .qrcode {
            imageView.image = imageQR
        } else {
            imageView.image = barcodeQR
            lblBarCode.text = editCodeDisplay(code: codeString)
        }
    }
    
    func setupView(image:UIImage,type:PaymentCodeTypePopup){
        self.addSubview(container)
        container.addSubview(imageView)
        imageView.image = image
        if type == .qrcode {
            viewAvatarSmall.addSubview(imgAvatarSmall)
            container.addSubview(viewAvatarSmall)
            viewAvatarSmall.backgroundColor = .white
            imgAvatarSmall.image = imageAvatar
        }
        else {
            container.addSubview(lblBarCode)
            lblBarCode.textAlignment = .center
            lblBarCode.adjustsFontSizeToFitWidth = true
            
            imageView.snp.makeConstraints { (make) in
                make.left.equalTo(0)
                make.right.equalTo(0)
                make.top.equalTo(0)
                make.bottom.equalTo(-50)
            }
            lblBarCode.snp.makeConstraints({ (make) in
                make.left.equalTo(0)
                make.right.equalTo(0)
                make.height.equalTo(50)
                make.bottom.equalToSuperview()
            })
            self.container.layoutIfNeeded()
        }
        updateFrame(type: type)
    }
    func updateFrame(type:PaymentCodeTypePopup){
        if type == .qrcode {
            let heightViewSmall:CGFloat = 46
            var heightImvSmall:CGFloat = 40
            
            imageView.frame = CGRect(x: 0, y: 0, width: container.frame.width, height: container.frame.height)
            viewAvatarSmall.frame = CGRect(x: (container.frame.width - heightViewSmall ) / 2, y: (container.frame.height - heightViewSmall ) / 2, width: heightViewSmall, height: heightViewSmall)
            imgAvatarSmall.frame = CGRect(x: (viewAvatarSmall.frame.width - heightImvSmall ) / 2, y: (viewAvatarSmall.frame.height - heightImvSmall ) / 2, width: heightImvSmall, height: heightImvSmall)
            
            imgAvatarSmall.zp_roundRect(Float(heightImvSmall / 2))
            heightImvSmall = viewAvatarSmall.frame.height
            viewAvatarSmall.zp_roundRect(Float(heightImvSmall / 2))
        }
    }
    func animationQR(){
        let width = self.frame.size.width - 72 * 2
        let frameNew = CGRect(x: 72, y: self.frame.size.height / 2 - width / 2, width: width, height:width)
        container.frame = frameNew
        updateFrame(type:PaymentCodeTypePopup.qrcode)
    }
    func animationBarCode(){
        var width:CGFloat = 400
        let height:CGFloat = 200
        
        let att: [NSAttributedStringKey: Any] = [
            .font: self.lblBarCode.font,
            .foregroundColor: self.lblBarCode.textColor
        ]
        
        let sizeCode = ((self.lblBarCode.text ?? "") as NSString).size(withAttributes: att)
        width = floor(min(width,sizeCode.width + 2))
        
        let frameNew = CGRect(x: floor(self.frame.size.width / 2 - width / 2), y: self.frame.size.height / 2 - height / 2, width: width, height: height)
        container.frame = frameNew
        container.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        container.transform = CGAffineTransform(rotationAngle: (.pi / 2))
    }
}
extension UIView {
    func rotate(angle: CGFloat) {
        let degrees: CGFloat = angle
        let radians = CGFloat(__sinpi(Double(degrees.native/180.0)))
        self.transform = CGAffineTransform(rotationAngle: radians)
    }
    
}
