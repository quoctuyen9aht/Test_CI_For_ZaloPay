//
//  ZPReceiveQRView.swift
//  ZaloPay
//
//  Created by vuongvv on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit
import ZaloPayCommonSwift

protocol ZPPaymentCodeQRViewDelegate {
    func showPopup(frameCurrent:CGRect,image: UIImage, type: PaymentCodeTypePopup)
}
class ZPPaymentCodeQRView: UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var qrImageView: UIImageView!
    @IBOutlet weak var barCodeImageView: UIImageView!
    @IBOutlet weak var avatarView: UIView!
    @IBOutlet weak var lblMaxMoney: UILabel!
    var delegate:ZPPaymentCodeQRViewDelegate?
    
    @IBOutlet weak var titleDetailTopContraint: NSLayoutConstraint!
    @IBOutlet weak var barcodeTopContraint: NSLayoutConstraint!
    @IBOutlet weak var lblMaxTopContraint: NSLayoutConstraint!
    @IBOutlet weak var leadingQRContraint: NSLayoutConstraint!
    @IBOutlet weak var qrcodeTopContraint: NSLayoutConstraint!
    @IBOutlet weak var trailingQRContraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        var height = Float(avatarImageView.frame.height)
        avatarImageView.zp_roundRect(height / 2)
        height = Float(avatarView.frame.height)
        avatarView.zp_roundRect(height / 2)
        avatarView.backgroundColor = UIColor.white
        
        titleLabel?.text = R.string_QuickPay_Detail_BarCode()
        titleLabel?.textColor = UIColor.subText()
        avatarImageView.image = UIImage(named: "profile_avatar")
        let fontSize: Int = UIView.isScreen2x() ? 14 : 16
        titleLabel?.font
            = UIFont.sfuiTextRegular(withSize: CGFloat(fontSize))
        titleLabel?.isHidden = true
        backgroundColor = UIColor.white
        
        if UIScreen.main.bounds.size.height == 568 {
            barcodeTopContraint.constant -= 20
            qrcodeTopContraint.constant -= 30
            leadingQRContraint.constant -= 20
            trailingQRContraint.constant -= 20
            lblMaxTopContraint.constant -= 10
        }
        else if UIScreen.main.bounds.size.height == 480 {
            barcodeTopContraint.constant -= 40
            qrcodeTopContraint.constant -= 40
            leadingQRContraint.constant -= 20
            trailingQRContraint.constant -= 20
            lblMaxTopContraint.constant -= 20
        }
        
        // Show max money
        let moneyGet = ("\(ZPPaymentCodeConfig.getpaymentLimitAmount())" as NSString).formatMoneyValue() ?? ""
        let stringMaxMoney = String(format: R.string_QuickPay_Secuirty_Warning_Max(), moneyGet)
        self.lblMaxMoney.font
            = UIFont.sfuiTextRegularItalic(withSize: 14)
        //self.lblMaxMoney.attributedText = editTextMaxMoney(str: stringMaxMoney, money: moneyGet)
        self.lblMaxMoney.text = stringMaxMoney
        self.lblMaxMoney.textAlignment = .center
        self.lblMaxMoney.textColor = UIColor.subText()
        
        setupTap()
    }
    func setupTap(){
        let tapQR = UITapGestureRecognizer(target: self, action: #selector(ZPPaymentCodeQRView.tapImage(tap:)))
        qrImageView.tag = 1
        qrImageView.addGestureRecognizer(tapQR)
        qrImageView.isUserInteractionEnabled = true
        let barcode = UITapGestureRecognizer(target: self, action: #selector(ZPPaymentCodeQRView.tapImage(tap:)))
        barCodeImageView.tag = 2
        barCodeImageView.addGestureRecognizer(barcode)
        barCodeImageView.isUserInteractionEnabled = true
        
        let avatar = UITapGestureRecognizer(target: self, action: #selector(ZPPaymentCodeQRView.tapImage(tap:)))
        avatarView.tag = 1
        avatarView.addGestureRecognizer(avatar)
        avatarView.isUserInteractionEnabled = true
        
        let lblBarCodetap = UITapGestureRecognizer(target: self, action: #selector(ZPPaymentCodeQRView.tapImage(tap:)))
        titleLabel.tag = 2
        titleLabel.addGestureRecognizer(lblBarCodetap)
        titleLabel.isUserInteractionEnabled = true
    }
    @objc func tapImage(tap:UITapGestureRecognizer){
        if tap.view?.tag == 1 {
            guard let imageGet = qrImageView.image else {
                return
            }
            self.delegate?.showPopup(frameCurrent:qrImageView.frame, image: imageGet, type: PaymentCodeTypePopup.qrcode)
        }
        else {
            guard let imageGet = barCodeImageView.image else {
                return
            }
            self.delegate?.showPopup(frameCurrent:barCodeImageView.frame,image: imageGet, type: PaymentCodeTypePopup.barcode)
        }
    }
    func editTextMaxMoney(str:String,money:String) -> NSMutableAttributedString{
        let att = NSMutableAttributedString(string: str)
        let rangeMoney = (str as NSString).range(of: money)
        if rangeMoney.location != NSNotFound {
            att.addAttribute(NSAttributedStringKey.font, value: UIFont.sfuiTextSemibold(withSize: 14), range: rangeMoney)
            att.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.zaloBase(), range: rangeMoney)
        }
        let rangeVND = (str as NSString).range(of: "VND")
        if rangeMoney.location != NSNotFound {
            att.addAttribute(NSAttributedStringKey.font, value: UIFont.sfuiTextRegular(withSize: 10), range: rangeVND)
            att.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.zaloBase(), range: rangeVND)
        }
        return att
    }
}

