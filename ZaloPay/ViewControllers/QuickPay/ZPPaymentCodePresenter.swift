//
//  ZPPaymentCodePresenter.swift
//  ZaloPay
//
//  Created by vuongvv on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit

class ZPPaymentCodePresenter: ZPPaymentCodePresenterDelegate {

    weak var controller: ZPPaymentCodeViewControllerInput?
    
    var interactor: ZPPaymentCodeInteractorInputDelegate?
    
    var router: ZPPaymentCodeRouterDelegate?
    
    func viewDidLoad() {
        interactor?.setupInteractor()
        
        if (self.controller?.getForceGoToGenQRCode() ?? false) && interactor?.getFirstShowSlideIntro() == nil {
            controller?.showSlideIntro()
            return
        }
        //Check show security first
        controller?.checkShowSecurityIntro(isShow: interactor?.paymentCodeShowSecurityIntro() ?? false)
    }

    func showDialogQRCodeError() {
        ZPDialogView.showDialog(with: DialogTypeError,
                                message: R.string_TransferQRCode_ErrorMessage(),
                                cancelButtonTitle: R.string_ButtonLabel_Close(),
                                otherButtonTitle: nil,
                                completeHandle: nil)
    }
    func showError(error: NSError?, handle: (() -> Void)? = nil) {
        ZPDialogView.showDialogWithError(error, handle: handle)
    }
    func createQRCodeImage() {
        interactor?.createQRImage()
    }
    
    func backClicked() {
        controller?.stopAllTimerOverlap()
        interactor?.backAction()
        router?.backViewController()
    }
    
    func updateView() {
        controller?.updateView()
    }
    
    func refreshPaymentCode(){
        interactor?.refreshPaymentCode()
    }
    func verifyPin(model : ZPQRToPayMessage,password:String) {
        interactor?.verifyPin(model: model, password: password)
    }
    func gotoListLimitCash(_ finishedBlock: @escaping ZPPaymentCodeListLimitCashVCFinishedBlock){
        router?.gotoListLimitCash(finishedBlock)
    }
    func gotoDisconnectVC(){
        router?.gotoDisconnectVC()
    }
    func gotoSupportCenterVC(){
        router?.gotoSupportCenterVC()
    }
    func gotoWithDrawVC() {
        router?.gotoWithdrawVC()
    }
    func gotoRechargeVC() {
        router?.gotoRechargeVC()
    }
    func presentSlideIntroVC(){
        router?.presentSlideIntroVC()
    }
    func setpaymentCodeShowSecurityIntro(_ value: Bool){
        interactor?.setpaymentCodeShowSecurityIntro(value)
        let firstShowSlideIntro = interactor?.getFirstShowSlideIntro()
        if firstShowSlideIntro == nil {
           controller?.showSlideIntro()
        }
        else {
           controller?.checkShowSecurityIntro(isShow: false)
        }
    }
    func loadUserInfo(){
        interactor?.loadUserInfo()
    }
    func refreshAllPaymentCode(){
        interactor?.refreshAllPaymentCode()
    }
    func handleObserverConnectVC(_ status: Bool){
        if status {
            controller?.hideTimeRetainDisconnectView()
        }
        else {
             let secondRetain = interactor?.getAllRetainSecondPaymentCode()
            controller?.showTimeRetainDisconnectView(timeRetain: secondRetain ?? 0)
        }
    }
    func verifyPinForHideSecurityView(hideNext:Bool){
        controller?.verifyPinForHideSecurityView(hideNext:hideNext)
    }
}

extension ZPPaymentCodePresenter: ZPPaymentCodeInteractorOutputDelegate {
    
    func bindingQRImage(params: String) {
        controller?.bindingQRImage(params: params)
    }
    
    func showResultViewWithMessage(message: String) {
        controller?.showResultViewWithMessage(message: message)
    }
    
    func reloadDataAnScrollTo(index indexPath: IndexPath) {
        controller?.reloadDataAnScrollTo(index: indexPath)
    }
    
    func showDialogError(_ error: NSError?, handle: (() -> Void)? = nil) {
        controller?.showError(error: error, handle: handle)
    }
    
    func setAvatar(with url: URL) {
        self.controller?.setAvatar(with: url)
    }
    func showLoading() {
        controller?.showLoadingHud()
    }
    
    func hideLoading() {
        controller?.hideLoadingHud()
    }
    func verfyWithPin(model : ZPQRToPayMessage){
        controller?.verfyWithPin(model:model)
    }
    func startTimerOverlapPmtCode(time:TimeInterval){
        controller?.startTimerOverlapPmtCode(time: time)
    }
    func stopTimerOverlapPmtCode(){
        controller?.stopTimerOverlapPmtCode()
    }
    func refreshPmtCode(){
        controller?.refreshPmtCode()
    }
    func createQRImage(){
        controller?.createQRImage()
    }
    func showResultView(result:ZPPaymentCodeResult){
        controller?.stopAllTimerOverlap()
        controller?.showResultView(result: result)
    }
    func showDisconnectView(){
        controller?.showDisconnectView()
    }
    func notiHidePopupZoomBarQrCode(){
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kZPPaymentCodePopupImageNotiHide), object: nil)
    }
    func checkDisconnectWithEmptyAndStillRetainTimeCode(){
        controller?.checkDisconnectWithEmptyAndStillRetainTimeCode()
    }
    func backToHome() {
        controller?.stopAllTimerOverlap()
        router?.backViewController()
    }
}
