//
//  ZPPaymentCodeCell.swift
//  ZaloPay
//
//  Created by vuongvv on 1/22/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit

class ZPPaymentCodeCell: UITableViewCell {
    
    @IBOutlet weak var mImgView: ZPIconFontImageView!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewBottom: UIView!
    var mImgViewArrow: ZPIconFontImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let colorMain = UIColor(hexString: "#BEE1F7")
        self.mImgView.setIconColor(UIColor.white)
        
        self.lblTitle.zpMainBlackRegular()
        self.lblDetail.zpDetailGrayStyle()
        self.lblTitle.textColor = .white
        self.lblDetail.textColor = colorMain
        
        mImgViewArrow = ZPIconFontImageView(frame: CGRect(x: 0, y: 0, width: 14, height: 14))
        mImgViewArrow.setIconFont("general_arrowright")
        mImgViewArrow.setIconColor(colorMain)
        mImgViewArrow.tag = 99
        self.addSubview(mImgViewArrow)
        mImgViewArrow.snp.makeConstraints { (make) in
            make.right.equalTo(-10)
            make.size.equalTo(mImgViewArrow.frame.size)
            make.centerY.equalToSuperview()
        }
        self.viewBottom.backgroundColor = colorMain
        self.contentView.backgroundColor = .clear
    }
    func setupData(model:ZPPaymentCodeModelCell) {
        self.lblTitle.text = model.title
        self.lblDetail.text = model.detail
        self.mImgView.setIconFont(model.icon)
    }
    func hideAll(isHide:Bool) {
        self.lblTitle.isHidden = isHide
        self.lblDetail.isHidden = isHide
        self.mImgView.isHidden = isHide
        self.mImgViewArrow.isHidden = isHide
    }
    
}
