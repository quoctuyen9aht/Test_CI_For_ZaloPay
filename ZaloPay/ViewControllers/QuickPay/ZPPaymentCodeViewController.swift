//
//  ZPPaymentCodeViewController.swift
//  ZaloPay
//
//  Created by vuongvv on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa
import ZaloPayCommonSwift
import ZaloPayAnalyticsSwift

enum  StateViewHeader {
    case showSecurity
    case showQRCode
    case showResult
    case showDisconnect
}

struct ZPPaymentCodeModelCell {
    var icon = ""
    var title = ""
    var detail = ""
    var type:WalletActionType? = WalletActionType.WalletActionTypeRecharge
}

let kZPPaymentCodeCell = "ZPPaymentCodeCell"

enum ZPPaymentCodeTracking {
    case lauched
    case contentViewQR
    case settingClick
    case qrcodePopupView
    case barcodePopupView
}

class ZPPaymentCodeViewController: BaseViewController {
    
    var presenter: ZPPaymentCodePresenterDelegate?
    
    var contentView = ZPPaymentCodeQRView.zp_viewFromSameNib() ?? ZPPaymentCodeQRView()
    var securityView = ZPPaymentCodeQRSecurityView.zp_viewFromSameNib() ?? ZPPaymentCodeQRSecurityView()
    var resultView = ZPPaymentCodeResultView.zp_viewFromSameNib() ?? ZPPaymentCodeResultView()
    var disconnectView = ZPPaymentCodeDisconnectView.zp_viewFromSameNib() ?? ZPPaymentCodeDisconnectView()
    var popupView: ZPPaymentCodePopupImage?
    
    var arrDatasouce:[ZPPaymentCodeModelCell] = [ZPPaymentCodeModelCell(icon: "", title: "", detail: "",type:nil),
                                                 ZPPaymentCodeModelCell(icon: "notify_recharge", title: R.string_WalletAction_Rechager(), detail: R.string_WalletAction_Rechager_Description(), type:WalletActionType.WalletActionTypeRecharge)
                                                ,ZPPaymentCodeModelCell(icon: "notify_1_withdraw", title: R.string_WalletAction_Withdraw(), detail: R.string_WalletAction_Withdraw_Description(),type:WalletActionType.WalletActionTypeWithdraw)
        ]
    
    lazy var tableView: UITableView! = {
        return UITableView(frame: .zero, style: .plain).build {
            self.view.addSubview($0)
            self.view.bringSubview(toFront: connectionView)
            $0.snp.makeConstraints { (make) in
                make.left.top.equalTo(0)
                make.right.equalTo(0)
                make.top.equalTo(10)
                make.bottom.equalTo(-10)
            }
            $0.roundRect(3)
            $0.backgroundColor = .clear
            $0.separatorStyle = .none
            $0.delegate = self
            $0.dataSource = self

        }
    }()
    let imageSize = CGSize(width: 300, height: 300)
    var resultMessage: ZPQRTransferAppMessage!
    var codeString = ""
    var timerOverlayPaymentCode:Timer?
    var timerOverlayDisconnect:Timer?
    
    var connectionView = UIView()
    var connectionLbl = MDHTMLLabel()
    var timeRetainWhenDisconnect:Int64 = 0
    
    var forceGoToGenQRCode = false
    
    let disposeBag = DisposeBag()
    let brightness = UIScreen.main.brightness
    
    fileprivate var kViewTopConstaint:CGFloat {
        let scaleHeight = UIScreen.main.bounds.height / 763
        var ktopViewBodyContraint  = 56 * scaleHeight
        if UIView.isIPhone4() || UIView.isIPhone5() {
            ktopViewBodyContraint = 12
        }
        return ktopViewBodyContraint
    }
    fileprivate var kTableBottomConstaint:CGFloat {
        var bottom = -100 + kViewTopConstaint
        if UIView.isIPhone5() {
            bottom = -54
        }
        else if UIView.isIPhone4() {
            bottom = -10
        }
        return bottom
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
        self.setupView()
        observerViewState()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ZPTrackingHelper.shared().trackScreen(with: self)
        self.analytic(from: .lauched)
    }
    
    override func screenName() -> String! {
        return ZPTrackerScreen.Screen_iOS_Quickpay_Balance
    }
    
    override func backEventId() -> Int {
        let eventId: ZPAnalyticEventAction = self.stateView == .showQRCode ? .quickpay_codeview_touch_back : .quickpay_touch_back
        return eventId.rawValue
    }
    
    override func isEnableAuthenticationView() -> Bool {
        return self.stateView != .showQRCode
    }
    
    var stateView:StateViewHeader! {
        didSet {
            self.tableView.tableHeaderView = nil
            if self.stateView == .showSecurity {
                self.tableView.tableHeaderView = introViewSecurity()
            }
            else if self.stateView == .showQRCode {
                self.tableView.tableHeaderView = contentViewQR()
                self.startTimerOverlapPmtCode(time: 0)
            }
            else if self.stateView == .showResult {
                self.tableView.tableHeaderView = resultViewTB()
            }
            else if self.stateView == .showDisconnect {
                self.tableView.tableHeaderView = disconnectExpireView()
            }
            if self.stateView == .showSecurity || self.stateView == .showResult {
                self.navigationItem.rightBarButtonItems = nil
            } else {
                self.navigationItem.rightBarButtonItems = rightMenuBars()
            }
            if self.stateView == .showResult {
                self.navigationItem.leftBarButtonItems = nil
                self.navigationItem.hidesBackButton = true
            }
            
            if self.stateView == .showSecurity {
                self.tableView.bounces = true
                self.tableView.isScrollEnabled = true
                self.tableView.snp.remakeConstraints { (make) in
                    make.left.top.equalTo(0)
                    make.right.equalTo(0)
                    make.top.equalTo(10)
                    make.bottom.equalTo(0)
                }
                self.tableView.layoutIfNeeded()
            }
            else {
                self.tableView.bounces = false
                self.tableView.isScrollEnabled = false
                
                self.tableView.snp.remakeConstraints { (make) in
                    make.left.top.equalTo(0)
                    make.right.equalTo(0)
                    make.top.equalTo(10)
                    make.bottom.equalTo(kTableBottomConstaint)
                }
                self.tableView.layoutIfNeeded()
            }
            self.setupTitle(stateView: self.stateView, result: nil)
            self.tableView.reloadData()
            
             UIScreen.main.brightness = self.stateView == .showQRCode ? 1.0 : self.brightness
        }
    }
    
    func contentViewQR() -> ZPPaymentCodeQRView {
        self.analytic(from: .contentViewQR)
        let view = ZPPaymentCodeQRView.zp_viewFromSameNib() ?? ZPPaymentCodeQRView()
        self.tableView.tableHeaderView = view
        
        view.snp.makeConstraints { (make) in
            make.top.equalTo(kViewTopConstaint)
            make.left.equalTo(10)
            make.width.equalToSuperview().offset(-20)
            make.height.equalToSuperview().offset( -10 - kViewTopConstaint)
        }
        view.roundRect(3)
        view.backgroundColor = .white
        view.delegate = self
        contentView = view
        contentView.avatarView.isHidden = true
        return view
    }
    
    func introViewSecurity() -> ZPPaymentCodeQRSecurityView {
        let view = ZPPaymentCodeQRSecurityView.zp_viewFromSameNib() ?? ZPPaymentCodeQRSecurityView()
        self.tableView.tableHeaderView = view
        let heightIntro:CGFloat =  (UIView.isIPhone4() || UIView.isIPhone5()) ? 440 : 480
        
        view.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(10)
            make.width.equalToSuperview().offset(-20)
            make.height.equalTo(heightIntro)
        }
        view.frame = CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: heightIntro)
        view.delegate = self
        securityView = view
        return view
    }
    
    func resultViewTB() -> ZPPaymentCodeResultView {
        let view = ZPPaymentCodeResultView.zp_viewFromSameNib() ?? ZPPaymentCodeResultView()
        self.tableView.tableHeaderView = view
        view.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(10)
            make.width.equalToSuperview().offset(-20)
            make.height.equalToSuperview()
        }
        view.backgroundColor = .white
        view.roundRect(3)
        view.delegate = self
        resultView = view
        return view
    }
    
    func disconnectExpireView() -> ZPPaymentCodeDisconnectView {
        let view = ZPPaymentCodeDisconnectView.zp_viewFromSameNib() ?? ZPPaymentCodeDisconnectView()
        self.tableView.tableHeaderView = view
        view.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(10)
            make.width.equalToSuperview().offset(-20)
            make.height.equalToSuperview()
        }
        view.backgroundColor = .white
        view.roundRect(3)
        disconnectView = view
        view.btnRefresh.rx.tap.bind { [weak self] in
            self?.presenter?.refreshAllPaymentCode()
        }.disposed(by: self.disposeBag)
        return view
    }
    
    func observerViewState() {
        let until = self.rx.deallocated
        let willAppear = self.rx.methodInvoked(#selector(viewWillAppear(_:)))
        let disAppear = self.rx.methodInvoked(#selector(viewWillDisappear(_:)))
        
        _ = willAppear.takeUntil(until).subscribe(onNext: { [weak self](_) in
            UIScreen.main.brightness = self?.stateView == .showQRCode ? 1.0 : self?.brightness ?? 1.0
        })
        _ = disAppear.takeUntil(until).subscribe(onNext: {  [weak self](_) in
            UIScreen.main.brightness = self?.brightness ?? 1.0
        })
        
        let enterBG = NotificationCenter.default.rx.notification(Notification.Name.UIApplicationDidEnterBackground, object: nil)
        let willResignActive = NotificationCenter.default.rx.notification(Notification.Name.UIApplicationWillResignActive, object: nil)
        let enterFG = NotificationCenter.default.rx.notification(Notification.Name.UIApplicationWillEnterForeground, object: nil)
        _ = Observable.merge([enterBG,willResignActive]).takeUntil(until).observeOn(MainScheduler.instance).subscribe ( onNext: {[weak self] (_) in
            UIScreen.main.brightness = self?.brightness ?? 1.0
        })
        
        _ = Observable.merge([enterFG]).takeUntil(until).observeOn(MainScheduler.instance).subscribe(onNext: {[weak self](_) in
            UIScreen.main.brightness = self?.stateView == .showQRCode ? 1.0 : self?.brightness ?? 1.0
        })
        
    }
    
    deinit {
        self.stopAllTimer()
    }
    
    func setupView() {
        self.view.backgroundColor = UIColor.zaloBase()
        self.createConnectionStatusView()
        self.setupNotiHandleConnection()
    }
    
    func setupTitle(stateView: StateViewHeader, result: ZPPaymentCodeResult?) {
        if stateView == StateViewHeader.showResult {
            self.title = result?.isSuccess == true ? R.string_QuickPay_Result_Success() : R.string_QuickPay_Result_Fail()
        }
        else if (stateView == StateViewHeader.showSecurity){
           self.title =  R.string_WalletAction_Title()
        }
        else {
            self.title = R.string_QuickPay_Title()
        }
    }
    
    @objc func settingButtonClicked() {
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.quickpay_codeview_touch_customize)
        let data = [R.string_QuickPay_ChangePaymentCode(),
                    R.string_QuickPay_Setting(),
                    R.string_QuickPay_Support()]
        let view = HAActionSheet(fromView: (self.navigationController?.view)!, sourceData: data as! [String])
        view.show { [weak self] (canceled, index) in
            if canceled {
                return
            }
            if index == 0 {
                self?.presenter?.refreshAllPaymentCode()
            } else if index == 1 {
                self?.presenter?.gotoListLimitCash({ [weak self] (amount_NotNeedPass,amount_Max,channel) in
                    self?.presenter?.refreshAllPaymentCode()
                })
            } else if index == 2 {
                self?.presenter?.gotoSupportCenterVC()
            }
        }
    }
    
    override func backButtonClicked(_ sender: Any!) {
        if stateView != .showSecurity {
            stateView = .showSecurity
        }
        else {
            presenter?.backClicked()
        }
        stopAllTimer()
    }
    
    override func viewWillSwipeBack() {
        stopAllTimer()
    }
    
    func rightMenuBars() -> [UIBarButtonItem] {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 26, height: 26))
        button.titleLabel?.textColor = UIColor.white
        button.titleLabel?.font = UIFont.iconFont(withSize: 20)
        button.setIconFont("webapp_3point_ios", for: .normal)
        button.addTarget(self, action: #selector(self.settingButtonClicked), for: .touchUpInside)
        let negativeSeparator = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        negativeSeparator.width = -8
        let rightBar = UIBarButtonItem(customView: button)
        return [negativeSeparator, rightBar]
    }
    
    func updateView() {
        self.tableView.layoutSubviews()
        self.tableView.layoutIfNeeded()
    }
    
    func showLoading() {
        ZPAppFactory.sharedInstance().showHUDAdded(to: self.view)
    }
    
    func hideLoading() {
        ZPAppFactory.sharedInstance().hideAllHUDs(for: self.view)
    }
    
    func startTimerOverlapPaymentCode(time:TimeInterval){
        self.stopTimerOverlapPaymentCode()
        self.timerOverlayPaymentCode = Timer.scheduledTimer(timeInterval: time, target: self, selector: #selector(self.refreshPaymentCode), userInfo: nil, repeats: false)
    }
    func stopTimerOverlapPaymentCode(){
        self.timerOverlayPaymentCode?.invalidate()
        self.timerOverlayPaymentCode = nil
    }
    @objc func refreshPaymentCode(){
        self.presenter?.refreshPaymentCode()
    }
    
    func startTimerOverlapDisconnect(time:TimeInterval){
        self.stopTimerOverlapDisconnect()
        if self.stateView == StateViewHeader.showSecurity {
            return
        }
        if (self.stateView != StateViewHeader.showDisconnect) {
            //self.connectionView.isHidden = false
        }
        self.timerOverlayDisconnect = Timer.scheduledTimer(timeInterval: time, target: self, selector: #selector(self.refreshDisconnect), userInfo: nil, repeats: false)
    }
    func stopTimerOverlapDisconnect(){
        self.timerOverlayDisconnect?.invalidate()
        self.timerOverlayDisconnect = nil
    }
    @objc func refreshDisconnect(){
        if timeRetainWhenDisconnect > 1 {
            timeRetainWhenDisconnect -= 1
            //let time = getAllRetainSecondDisplay(secondRetain: timeRetainWhenDisconnect)
            connectionLbl.attributedText = NSMutableAttributedString(string: R.string_QuickPay_Disconnect_TopView())
            self.startTimerOverlapDisconnect(time: 1)
        }
        else {
            self.hideTimeRetainDisconnect()
            self.showDisconnectExpireView()
        }
    }
    func createQRBarCodeImage() {
        self.presenter?.createQRCodeImage()
    }
    func verifyPinVC(model : ZPQRToPayMessage) {
        guard let VC = UIApplication.shared.delegate?.window??.rootViewController else {
            return
        }
        
        ZPCheckPinViewController.show(on: VC, using: R.string_Title_PIN(), methodName: ZPPaymentCodeConfig.getChannelSelected().name, methodImage: ZPPaymentCodeConfig.getChannelSelected().icon, suggestTouchId: true).subscribeNext({ [weak self] (result) in
            let resultStr = (result as? NSString ?? "").sha256() ?? ""
            self?.presenter?.verifyPin(model:model, password: resultStr)
        }) { [weak self] (error) in
            self?.presenter?.verifyPin(model:model, password: "")
        }
    }
    func verifyPinForHideIntroSecurityView(hideNext:Bool) {
        ZPAppFactory.sharedInstance().showPinDialogMessage(R.string_QuickPay_Input_Password_Function(),
                                                           isForceInput: true,
                                                           success: { [weak self] in
                                                            self?.presenter?.setpaymentCodeShowSecurityIntro(hideNext)
        }, error: nil)
    }
    func showDialogError(error: NSError?, handle: (() -> Void)?){
        ZPDialogView.showDialogWithError(error, handle: handle)
    }
    func checkShowSecurityIntro(isShow:Bool){
        if isShow && self.forceGoToGenQRCode.not {
            self.stateView = StateViewHeader.showSecurity
        }
        else {
            self.stateView = StateViewHeader.showQRCode
            self.forceGoToGenQRCode = false
        }
    }
    func showResult(result:ZPPaymentCodeResult){
        self.stateView = StateViewHeader.showResult
        self.setupTitle(stateView: self.stateView, result: result)
        resultView.setupData(result: result)
        self.stopTimerOverlapPaymentCode()
        self.stopTimerOverlapDisconnect()
    }
    func showDisconnectExpireView(){
        self.stateView = StateViewHeader.showDisconnect
    }

    func showTimeRetainDisconnect(timeRetain:Int64){
        if timeRetain > 0 {
            timeRetainWhenDisconnect = timeRetain
            startTimerOverlapDisconnect(time: 0)
        }
        else {
            self.hideTimeRetainDisconnect()
            //self.connectionView.isHidden = true
        }
    }
    func hideTimeRetainDisconnect(){
        self.stopTimerOverlapDisconnect()
        //self.connectionView.isHidden = true
        if (self.stateView == StateViewHeader.showDisconnect) {
            self.stateView = StateViewHeader.showQRCode
        }
    }
    func showSlideIntroView(){
        self.presenter?.presentSlideIntroVC()
    }
    
    func stopAllTimer(){
        stopTimerOverlapPaymentCode()
        stopTimerOverlapDisconnect()
    }
    
    func checkDisconnectWithEmptyAndStillRetainTime(){
        if !NetworkState.sharedInstance().isReachable {
            self.stopTimerOverlapDisconnect()
            self.showDisconnectExpireView()
            //self.connectionView.isHidden = true
        }
    }
}

// MARK: ZPPaymentCodeViewControllerInput
extension ZPPaymentCodeViewController: ZPPaymentCodeViewControllerInput {
    
    func bindingQRImage(params: String) {
        if self.contentView.avatarView.isHidden {
            self.presenter?.loadUserInfo()
        }
        if self.stateView != StateViewHeader.showQRCode {
            self.stateView = StateViewHeader.showQRCode
        }
        guard
            let data = params.data(using: String.Encoding.isoLatin1),
            let imageQR = UIImage.createQRImage(with: imageSize, from: data),
            let barcodeQR = UIImage.createBarCodeImage(with: imageSize, from: data)
            else {
                presenter?.showDialogQRCodeError()
                return
        }
        codeString = params
        self.contentView.qrImageView.image = imageQR
        self.contentView.barCodeImageView.image = barcodeQR
        self.contentView.avatarView.isHidden = false
        self.contentView.titleLabel?.isHidden = false
        popupView?.updateQuickPayImage(imageQR: imageQR, barcodeQR: barcodeQR, codeString: codeString)
    }      
    
    func showResultViewWithMessage(message: String) {
        showToast(with: message, delay: 0.1)
    }
    
    func reloadDataAnScrollTo(index indexPath: IndexPath) {
        self.tableView.reloadData()
        self.tableView.scrollToRow(at: indexPath, at: .none, animated: true)
    }
    
    func setAvatar(with url: URL) {
        self.contentView.avatarImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "profile_avatar"))
    }
    func showLoadingHud() {
        self.showLoading()
    }
    
    func hideLoadingHud() {
        self.hideLoading()
    }
    
    func startTimerOverlapPmtCode(time:TimeInterval){
        self.startTimerOverlapPaymentCode(time: time)
    }
    func stopTimerOverlapPmtCode(){
        self.stopTimerOverlapPaymentCode()
    }
    func refreshPmtCode(){
        self.refreshPaymentCode()
    }
    func createQRImage() {
        self.createQRBarCodeImage()
    }
    func verfyWithPin(model : ZPQRToPayMessage){
       self.verifyPinVC(model: model)
    }
    func showError(error: NSError?, handle: (() -> Void)?) {
        self.showDialogError(error: error, handle: handle)
    }
    func showResultView(result:ZPPaymentCodeResult){
        self.showResult(result: result)
    }
    func showDisconnectView(){
        self.showDisconnectExpireView()
    }
    func showTimeRetainDisconnectView(timeRetain:Int64){
        self.showTimeRetainDisconnect(timeRetain: timeRetain)
    }
    func hideTimeRetainDisconnectView(){
        self.hideTimeRetainDisconnect()
    }
    func verifyPinForHideSecurityView(hideNext:Bool){
        self.verifyPinForHideIntroSecurityView(hideNext:hideNext)
    }
    func showSlideIntro(){
        self.showSlideIntroView()
    }
    func checkDisconnectWithEmptyAndStillRetainTimeCode(){
        self.checkDisconnectWithEmptyAndStillRetainTime()
    }
    func stopAllTimerOverlap() {
        self.stopAllTimer()
    }
    func getForceGoToGenQRCode() -> Bool {
        return self.forceGoToGenQRCode
    }
}

extension ZPPaymentCodeViewController : ZPPaymentCodeQRViewDelegate {
    func showPopup(frameCurrent:CGRect,image: UIImage, type: PaymentCodeTypePopup) {
        let typeTrack: ZPPaymentCodeTracking = type == .barcode ? .qrcodePopupView : .barcodePopupView
        self.analytic(from: typeTrack)
        self.showPopup(frameCurrent:frameCurrent,image: image,imageAvatar:self.contentView.avatarImageView.image ?? UIImage(),code:codeString, type: type)
    }
    
    func showPopup(frameCurrent:CGRect,image: UIImage,imageAvatar:UIImage,code:String, type: PaymentCodeTypePopup) {
        popupView = ZPPaymentCodePopupImage()
        popupView?.showWithImage(frameCurrent:frameCurrent,image: image,imageAvatar:imageAvatar,code:code,type:type)
    }
    
}

extension ZPPaymentCodeViewController : ZPPaymentCodeQRSecurityViewDelegate {
    func OKClick(hideNext:Bool){
       self.verifyPinForHideSecurityView(hideNext:!hideNext)
    }
}

extension ZPPaymentCodeViewController : ZPPaymentCodeResultViewDelegate {
    func btnGoHomeClick() {
        self.presenter?.backClicked()
    }
}

extension ZPPaymentCodeViewController : IntroViewControllerDelegate {
    func hideClick() {
        self.checkShowSecurityIntro(isShow: false)
    }
}

extension ZPPaymentCodeViewController {
    func createConnectionStatusView() {
        connectionView = UIView()
        connectionView.backgroundColor = UIColor(hexValue: 0xffffc1)
        self.view.addSubview(connectionView)
        self.view.bringSubview(toFront: connectionView)
        connectionView.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.right.equalTo(0);
            make.height.greaterThanOrEqualTo(50)
        }
        
        let label = MDHTMLLabel()
        connectionView.addSubview(label)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = UIFont.sfuiTextRegular(withSize: 15)
        label.textColor = UIColor.subText()
        label.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            //make.right.equalTo(connectionView.snp.centerX).offset(20)
            make.right.equalTo(0)
            make.top.equalTo(5)
            make.height.greaterThanOrEqualTo(50)
        }
        label.backgroundColor = UIColor(hexValue: 0xffffc1)
        connectionLbl = label
        
        let button = UIButton(type: .custom)
        connectionView.addSubview(button)
        button.titleLabel?.font = UIFont.sfuiTextRegular(withSize: 15)
        button.setTitle(R.string_Home_CheckInternetConnection(), for: .normal)
        button.contentHorizontalAlignment = .left
        button.addTarget(self, action: #selector(self.showInternetConnection), for: .touchUpInside)
        button.setTitleColor(UIColor(hexValue: 0xcd7a04), for: .normal)
        button.snp.makeConstraints { (make) in
            //make.left.equalTo(label.snp.right).offset(0)
            make.top.equalTo(0)
            make.bottom.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(0)
            make.width.equalTo(0)
        }
        let line = ZPLineView()
        label.addSubview(line)
        line.alignToBottom()
        
        connectionView.isHidden = true
        handleConnectionStatus(NetworkState.sharedInstance().isReachable)
    }
    
    @objc func showInternetConnection(){
        self.presenter?.gotoDisconnectVC()
    }
    
    func setupNotiHandleConnection(){
        let until = self.rac_willDeallocSignal()
        NetworkState.sharedInstance().networkSignal.take(until: until).deliverOnMainThread().subscribeNext({ [weak self](result) in
            let isConnected = result as? Bool ?? true
            self?.handleConnectionStatus(isConnected)
        })
    }
    
    func handleConnectionStatus(_ status: Bool) {
//        if self.stateView == StateViewHeader.showQRCode {
//            connectionView.isHidden = status
//        }
        self.presenter?.handleObserverConnectVC(status)
    }
}

//MARK: - Utils
extension ZPPaymentCodeViewController {
    func addSeeMoreToCodeText(code:String) -> NSMutableAttributedString{
        if code.count <= 4 {
            return NSMutableAttributedString()
        }
        let indexStart = code.index(code.startIndex, offsetBy: 4)
        let first4 = code[indexStart...]
        let seeMore = "Xem số"
        let stringAttribute = "\(first4)****** \(seeMore)"
        let att = NSMutableAttributedString(string: stringAttribute)
        
        let rangeSeeMore = NSMakeRange(stringAttribute.count - seeMore.count, seeMore.count)
        att.addAttribute(NSAttributedStringKey.font, value: UIFont.sfuiTextRegular(withSize: 16), range: rangeSeeMore)
        att.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.zaloBase(), range: rangeSeeMore)
        
        return att
    }
    
    func addDisconnectText(text:String,time:String) -> NSMutableAttributedString{
        let rangeTime = (text as NSString).range(of: time)
        let att = NSMutableAttributedString(string: text)
        att.addAttribute(NSAttributedStringKey.font, value: UIFont.sfuiTextMedium(withSize: 15), range: rangeTime)
        att.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.defaultText(), range: rangeTime)
        return att
    }
    
    func getAllRetainSecondDisplay(secondRetain:Int64) ->  String{
        var timeStr = ""
        let hourStr = "giờ"
        let minutesStr = "phút"
        let secondStr = "giây"
        
        var seconds = Double(secondRetain)
        let hours = Int64(floor(seconds / 3600))
        if hours > 0 {
            seconds -= Double(hours) * 3600
            timeStr = "\(timeStr) \(hours) \(hourStr)"
        }
        let minutes = Int64(floor(seconds / 60))
        if minutes > 0 {
            seconds -= Double(minutes) * 60
            timeStr = "\(timeStr) \(minutes) \(minutesStr)"
        }
        if seconds >= 0 {
            timeStr = "\(timeStr) \(Int64(seconds)) \(secondStr)"
        }
        return timeStr
        
    }
}
extension ZPPaymentCodeViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.stateView == .showSecurity ? self.arrDatasouce.count : 0;
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = arrDatasouce[indexPath.row]
        var cell = tableView.dequeueReusableCell(withIdentifier: kZPPaymentCodeCell) as? ZPPaymentCodeCell
        if cell == nil {
            cell = UINib(nibName: kZPPaymentCodeCell, bundle: nil).instantiate(withOwner: self,
                                                                                                 options: nil)[0] as? ZPPaymentCodeCell
        }
        cell?.setupData(model: item)
        cell?.backgroundColor = .clear
        cell?.viewBottom.isHidden = ((indexPath.row + 1) % 2 ) != 0
        item.type == nil ? cell?.hideAll(isHide: true) : cell?.hideAll(isHide: false)
        return cell ?? UITableViewCell()
    }
}

extension ZPPaymentCodeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = arrDatasouce[indexPath.row]
        if item.type == .WalletActionTypeRecharge || item.type == .WalletActionTypeWithdraw {
            return 60
        }
        return 10
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = arrDatasouce[indexPath.row]
        if item.type == .WalletActionTypeRecharge {
            ZPBankApiWrapperSwift.sharedInstance.hasBankToRecharge() ? moveToRecharge() : moveToMapCard()
        }
        else if item.type == .WalletActionTypeWithdraw {
            openWithdraw()
        }
    }
}
extension ZPPaymentCodeViewController {

    private func moveToMapCard() {
        
        let navi = self.navigationController
        (~ZPBankApiWrapperSwift.sharedInstance.mapBank(with: self, appId: kZaloPayClientAppId, transType: .atmMapCard))
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak navi,weak self](_) in
                    guard let myViewController = navi?.viewControllers.first(where: { $0 is ZPPaymentCodeViewController }) as? ZPPaymentCodeViewController else {
                         navi?.popToRootViewController(animated: false)
                         self?.moveToRecharge()
                         return
                    }
                    navi?.popToViewController(myViewController, animated: false)
                    self?.moveToRecharge()
                }, onError: { [weak navi] (e) in
                    guard let myViewController = navi?.viewControllers.first(where: { $0 is ZPPaymentCodeViewController }) as? ZPPaymentCodeViewController else {
                        navi?.popToRootViewController(animated: true)
                        return
                    }
                    navi?.popToViewController(myViewController, animated: true)
            }).disposed(by: disposeBag)
    }
    private func moveToRecharge() {
        self.presenter?.gotoRechargeVC()
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.quickpay_touch_addcash)
    }
    func openWithdraw() {
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.quickpay_touch_withdraw)
        if (ZPWalletManagerSwift.sharedInstance.isMaintainWithdraw()) {
            ZPWalletManagerSwift.sharedInstance.alertMaintainWithdraw()
            return;
        }
        self.presenter?.gotoWithDrawVC()
    }
    
    private func analytic(from type: ZPPaymentCodeTracking) {
        var idEvent: ZPAnalyticEventAction?
        switch type {
        case .lauched:
            idEvent = .quickpay_launched
        case .settingClick:
            idEvent = .quickpay_codeview_touch_customize
        case .qrcodePopupView:
            idEvent = .quickpay_codeview_touch_barcode
        case .contentViewQR:
            fallthrough
        case .barcodePopupView:
            idEvent = .quickpay_codeview
        }
        
        guard let idTrack = idEvent else {
            return
        }
        ZPTrackingHelper.shared().trackEvent(idTrack)
    }
}
