//
//  WithdrawConditionView.swift
//  ZaloPay
//
//  Created by Duy Dao Pham Hoang on 6/16/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

private let screenSize: CGRect = UIScreen.main.bounds
private let pading:CGFloat = 11
private let padingCenterButton:CGFloat = 10
private let buttonFrameWith = (screenSize.width - pading*2 - padingCenterButton*2)/3
private let buttonFrameHeight = buttonFrameWith/3.157

@objcMembers
class WithdrawConditionView: BaseViewController {
    
    var presenter: WithdrawConditionPresenterProtocol?
    var imageHolder: UIImageView?
    var lblTitleHolder: UILabel?
    var lblContentHolder: UILabel?
    var btnConnectNow: UIButton?
    var lblConnectNow: UILabel?
    var lastRow: CGRect?
    var scrollView: UIScrollView?
    fileprivate var eResult: PublishSubject<AnyObject?>?
    fileprivate let disposeBag = DisposeBag()
    
    var arrayBank: NSMutableArray? {
        didSet {
            self.collectionView.reloadData()
            self.drawButtonConnectNow()
        }
    }
    
    var heightNavigation: CGFloat?
    lazy var collectionView: UICollectionView = {
        return self.setupCollectionView()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let status_height = UIApplication.shared.statusBarFrame.size.height
        heightNavigation = (self.navigationController?.navigationBar.frame.height)! + status_height
        self.title = R.string_WithdrawCondition_Title()
        self.baseViewControllerViewDidLoad()
        self.view.backgroundColor = UIColor.white
        arrayBank = requestGetListBank()
        observerResource()
    }
    
    func observerResource() {
        ReactNativeAppManager.sharedInstance().observe(self, fromAppId: showshowAppId, complete: { [weak self] in
            self?.collectionView.reloadData()
        })
    }
    
    func setupCollectionView() -> UICollectionView
    {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.headerReferenceSize = CGSize(width: screenSize.size.width, height: HeaderWithDrawCondition.heightWidthDraw());
        
        let collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height - heightNavigation!), collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.register(BankWithDrawConditionCell.self, forCellWithReuseIdentifier: BankWithDrawConditionCell.description())
        collectionView.register(HeaderWithDrawCondition.self, forSupplementaryViewOfKind:UICollectionElementKindSectionHeader, withReuseIdentifier: HeaderWithDrawCondition.description())
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.contentInset = UIEdgeInsetsMake(0, 0, 85, 0)
        self.view.addSubview(collectionView)
        return collectionView
    }
    
    func requestGetListBank() -> NSMutableArray{
        let allBank = ZPWalletManagerSwift.sharedInstance.bankSupports
        if allBank.isEmpty  {
            return NSMutableArray()
        }
        var arrayBankAllowWithDraw = allBank.sorted(by: { ($0.displayOrder ?? 0).compare(($1.displayOrder ?? 0)) == .orderedAscending})
        // remove visa and master card
        arrayBankAllowWithDraw = arrayBankAllowWithDraw.filter() { $0.enableWithdraw == true }
        return NSMutableArray(array: arrayBankAllowWithDraw)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func drawButtonConnectNow()
    {
        if btnConnectNow == nil
        {
            btnConnectNow = UIButton()
            btnConnectNow?.setupZaloPay()
            var heightBtnConnectNow:CGFloat = kZaloButtonHeight
            let plus = UILabel.iconCode(withName: "linkaccount_add")
            let buttonTitle = String(format: "%@  %@", plus!, R.string_WithdrawCondition_Connect_Now())
            
            if screenSize.size.height <= 568
            {
                heightBtnConnectNow = kZaloButtonHeight
                let att = NSMutableAttributedString(string: buttonTitle, attributes: [NSAttributedStringKey.font : UIFont.sfuiTextRegular(withSize: 16), NSAttributedStringKey.foregroundColor : UIColor.white])
                att.addAttribute(NSAttributedStringKey.font, value: UIFont.zaloPay(withSize: 16), range: NSMakeRange(0, (plus?.count)!))
                btnConnectNow?.setAttributedTitle(att, for: .normal)
            }
            else
            {
                let att = NSMutableAttributedString(string: buttonTitle, attributes: [NSAttributedStringKey.font : UIFont.sfuiTextRegular(withSize: 18), NSAttributedStringKey.foregroundColor : UIColor.white])
                att.addAttribute(NSAttributedStringKey.font, value: UIFont.zaloPay(withSize: 18), range: NSMakeRange(0, (plus?.count)!))
                btnConnectNow?.setAttributedTitle(att, for: .normal)
            }
           
            btnConnectNow?.addTarget(self, action: #selector(processPushConnectNow), for: UIControlEvents.touchUpInside)
            self.view.addSubview(btnConnectNow!)
            
            var heightSurplus:CGFloat = 0
            if ((arrayBank?.count)! > 3 && ((arrayBank?.count)! % 3) > 0)
            {
                heightSurplus = buttonFrameHeight
            }
            
            let heightContentSize = (padingCenterButton*CGFloat(((arrayBank?.count)!/3))) + (buttonFrameHeight*CGFloat(((arrayBank?.count)!/3))) + 46 + HeaderWithDrawCondition.heightWidthDraw() + heightSurplus
            
            if (Int(heightContentSize) > Int((collectionView.frame.size.height)) - (Int(heightBtnConnectNow) + 30))
            {
                self.btnConnectNow?.frame = CGRect(x: 10, y: self.view.frame.size.height - heightBtnConnectNow - 10 - heightNavigation!, width: self.view.frame.size.width - 20, height: heightBtnConnectNow)
            }
            else
            {
                self.btnConnectNow?.frame = CGRect(x: 10, y: heightContentSize , width: self.view.frame.size.width - 20, height: heightBtnConnectNow);
            }
        }
    }
    
    override func backButtonClicked(_ sender: Any!) {
        guard let _ = self.eResult else {
            self.navigationController?.popViewController(animated: true)
            return
        }        
        viewWillSwipeBack()
    }
    
    override func viewWillSwipeBack() {
        if let e = self.eResult  {
            let error = NSError(domain: NSURLErrorDomain, code: NSURLErrorCancelled, userInfo: [NSLocalizedDescriptionKey : "Cancel"])
            e.onError(error)
        }
    }
    
    @objc func processPushConnectNow()
    {
        (~ZPBankApiWrapperSwift.sharedInstance.mapBank(with: self,
                                                  appId: withdrawAppId,
                                                  transType: .withDraw)).subscribe(onNext: { [weak self] in
            guard let observer = self?.eResult else { // case thanh lý tài khoản
                self?.navigationController?.popToRootViewController(animated: false)
                return
            }
            observer.onNext($0)
            observer.onCompleted()
        }, onError: { [weak self](e) in
                // Return this
            self?.popMe()
        }).disposed(by: disposeBag)
    }
}

// Using function event
extension WithdrawConditionView {
    static func showCondition(on vc: UIViewController?) -> Observable<AnyObject?>{
        guard let vc = vc else {
            return Observable.empty()
        }
        return Observable.create({ [weak vc](s) -> Disposable in
            let vcCondition = WithdrawConditionWireFrame.createWithDrawConditionModule()
            vcCondition.eResult = PublishSubject()
            _ = vcCondition.eResult?.subscribe(s)
            vc?.navigationController?.pushViewController(vcCondition, animated: true)
            return Disposables.create()
        })
        
    }
}

extension WithdrawConditionView: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return (arrayBank?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BankWithDrawConditionCell.description(),for: indexPath) as! BankWithDrawConditionCell
        
        if let item = arrayBank?.object(at: indexPath.row) as? ZPBankSupport
        {
            if item.iconNameHorizontal != nil
            {
                cell.setData(item.iconNameHorizontal, bankName: item.name)
            }
        }
        return cell
    }
}

extension WithdrawConditionView : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: buttonFrameWith, height: buttonFrameHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, pading, 0, pading)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return (padingCenterButton)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if (kind == UICollectionElementKindSectionHeader) {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                             withReuseIdentifier: HeaderWithDrawCondition.description(),
                                                                             for: indexPath) as! HeaderWithDrawCondition
            //headerView.label.text = searches[(indexPath as NSIndexPath).section].searchTerm
            return headerView
        }
        return UICollectionReusableView()
    }
    
}
