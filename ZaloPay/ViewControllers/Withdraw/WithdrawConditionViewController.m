//
//  WithdrawConditionViewController.m
//  ZaloPay
//
//  Created by bonnpv on 8/17/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "WithdrawConditionViewController.h"
#import "WithdrawConditionCell.h"
#import "LoginManager.h"
#import "ZPSavedCard+ZaloPay.h"
#import "ZPWithdrawHelper.h"
#import "ZPSavedBankAccount+ZaloPay.h"
#import "ZPBankSupport.h"
#import "ZPSelectBankView.h"
#import "ZPBill+ZaloPay.h"
#import "BillDetailViewController.h"
#import "ZPBankApiWrapper.h"
#import <ZaloPayCommon/NSArray+Safe.h>
#import ZALOPAY_MODULE_SWIFT

@interface WithdrawConditionViewController () <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *dataSource;
@end

@interface BankCardStatus : NSObject
@property (nonatomic, strong) NSString *cardTitle;
@property (nonatomic) BOOL hasCard;
@property (nonatomic) ZPBankType cardType;
@end

@implementation BankCardStatus

@end

@implementation WithdrawConditionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = [R string_WithdrawCondition_Title];
    self.tableView = [[UITableView alloc] init];
    self.tableView.allowsSelection = NO;
    self.tableView.backgroundColor =[UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(0);
        make.right.equalTo(0);
        make.top.equalTo(0);
        make.bottom.equalTo(0);
    }];
    
    self.dataSource = [self createDataSource];
    self.tableView.tableHeaderView = [self headerView];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.navigationController.navigationBarHidden) {
        [self.navigationController setNavigationBarHidden:NO];
    }
}

- (NSDictionary *)getAcceptedBank {
    // Default list of accepted cards
    NSDictionary *dict = [ZPWithdrawHelper enableBank];
    // Initialize list of accept cards with default values
    NSMutableDictionary *acceptCards = [NSMutableDictionary new];
    
    NSMutableArray *allCard = [NSMutableArray array];
    NSMutableArray *allBank = [NSMutableArray array];
    
    for (NSNumber *cardType in dict.allKeys) {
        ZPBankSupport *bankSupport = [dict objectForKey:cardType];
        BankCardStatus* bankCard = [BankCardStatus new];
        bankCard.cardTitle = bankSupport.name;
        bankCard.cardType = [cardType intValue];
        bankCard.hasCard = NO;
        [acceptCards setObject:bankCard forKey:cardType];
        if (bankSupport.type == ZPBSupportType_Card) {
            [allCard addObject:bankCard];
        }else {
            [allBank addObject:bankCard];
        }
    }
    
    // verify if User has any accepted cards
    NSArray *saveCard = [ZaloPayWalletSDKPayment sharedInstance].paymentSavedCard;
    for (ZPSavedCard *card in saveCard) {
        BankCardStatus *bankCard = [acceptCards objectForKey:@(card.bankType)];
        if (bankCard != nil) {
            bankCard.hasCard = YES;
        }
    }
    NSArray *saveBanks = [ZaloPayWalletSDKPayment sharedInstance].paymentSavedBankAccount;
    for (ZPSavedBankAccount *account in saveBanks) {
        BankCardStatus *accountStatus = [acceptCards objectForKey:@(account.bankType)];
        if (accountStatus != nil) {
            accountStatus.hasCard = YES;
        }
    }
    return @{@"card": allCard,
             @"bank": allBank};
}

- (NSArray *)createDataSource {
    NSMutableArray *array = [NSMutableArray array];
    BOOL isLevel2 = [ZPProfileManager shareInstance].userLoginData.profilelevel >=2;
    WithdrawCategoryItem *categoryItem = [self categoryWithTitle:[R string_WithdrawCondition_Info]
                                                     buttonTitle:[R string_WithdrawCondition_Update]
                                                            type:WithdrawCategoryItemTypeLevel];
    WithdrawDetailItem *item =[self itemWithTitle1:[R string_WithdrawCondition_Phone]
                                           option1:isLevel2
                                            title2:[R string_WithdrawCondition_Password]
                                           option2:isLevel2];
    
    [array addObject:categoryItem];
    
    if (isLevel2) {
        item.hideTopLine = FALSE;
        categoryItem.hideButton = YES;
    } else {
        item.hideTopLine = TRUE;
        categoryItem.hideButton = NO;
        WithdrawTitleItem *title = [[WithdrawTitleItem alloc] init];
        title.title = [R string_WithdrawCondition_UpdateInfo];
        [array addObject:title];
    }
    
    [array addObject:item];
    categoryItem = [self categoryWithTitle:[R string_WithdrawCondition_LinkCard]
                               buttonTitle:[R string_WithdrawCondition_AddCard]
                                      type:WithdrawCategoryItemTypeAtmCard];
    [array addObject:categoryItem];
    
    WithdrawTitleItem *title = [[WithdrawTitleItem alloc] init];
    title.title = [R string_WithdrawCondition_LinkCardTitle];
    [array addObject:title];
    
    NSDictionary *acceptedCards = [self getAcceptedBank];
    NSArray *allSupportBank = [acceptedCards arrayForKey:@"card"];
        
    for (int i = 0; i < [allSupportBank subArrayCount:2]; i++) {
        NSArray *twoItems = [allSupportBank subArrayAtIndex:i withTotalItem:2];
        BankCardStatus *bank1 = [twoItems safeObjectAtIndex:0];
        BankCardStatus *bank2 = [twoItems safeObjectAtIndex:1];
        
        item = [self itemWithTitle1:bank1.cardTitle
                            option1:bank1.hasCard
                             title2:bank2.cardTitle
                            option2:bank2.hasCard];
        item.hideTopLine = true;
        item.hideBottomLine = true;
        [array addObject:item];
    }
    item = array.lastObject;
    if ([item isKindOfClass:[WithdrawDetailItem class]]) {
        item.hideBottomLine = false;
    }
    allSupportBank = [acceptedCards arrayForKey:@"bank"];
    if (allSupportBank.count == 0) {
        return array;
    }
    categoryItem = [self categoryWithTitle:[R string_LinkBank_Title]
                               buttonTitle:[R string_WithdrawCondition_AddCard]
                                      type:WithdrawCategoryItemTypeAccount];
    [array addObject:categoryItem];
    
    title = [[WithdrawTitleItem alloc] init];
    title.title = [R string_WithdrawCondition_LinkAccountTitle];
    
    [array addObject:title];
    for (int i = 0; i < [allSupportBank subArrayCount:2]; i++) {
        NSArray *twoItems = [allSupportBank subArrayAtIndex:i withTotalItem:2];
        BankCardStatus *bank1 = [twoItems safeObjectAtIndex:0];
        BankCardStatus *bank2 = [twoItems safeObjectAtIndex:1];
        
        item = [self itemWithTitle1:bank1.cardTitle
                            option1:bank1.hasCard
                             title2:bank2.cardTitle
                            option2:bank2.hasCard];
        item.hideTopLine = true;
        item.hideBottomLine = true;
        [array addObject:item];
    }
    item = array.lastObject;
    if ([item isKindOfClass:[WithdrawDetailItem class]]) {
        item.hideBottomLine = false;
    }
    return array;
}

- (WithdrawCategoryItem *)categoryWithTitle:(NSString *)title
                                buttonTitle:(NSString *)buttonTitle
                                       type:(WithdrawCategoryItemType)type{
    WithdrawCategoryItem *item = [[WithdrawCategoryItem alloc] init];
    item.title = title;
    item.buttonTitle = buttonTitle;
    item.type = type;
    return item;
}

- (WithdrawDetailItem *)itemWithTitle1:(NSString *)title1
                               option1:(BOOL)option1
                                title2:(NSString *)title2
                               option2:(BOOL)option2 {
    WithdrawDetailItem *item = [[WithdrawDetailItem alloc] init];
    item.title1 = title1;
    item.title2 = title2;
    item.option1 = option1;
    item.option2 = option2;
    return  item;
}

- (UIView *)headerView {
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 80)];
    UILabel *labelTitle = [[UILabel alloc] init];
    labelTitle.text = [R string_WithdrawCondition_NeedMoreCondition];
    labelTitle.textColor = [UIColor defaultText];
    labelTitle.font = [UIFont SFUITextSemiboldWithSize:15];
    [header addSubview:labelTitle];
    
    [labelTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(10);
        make.left.equalTo(10);
        make.right.equalTo(-10);
        make.height.equalTo(20);
    }];
    
    UILabel *title = [[UILabel alloc] init];
    title.numberOfLines = 0;
    title.font = [UIFont SFUITextRegularWithSize:14];
    title.textColor = [UIColor defaultText];
    title.text = [R string_WithdrawCondition_NeedUpdateMoreInfo];
    [header addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(labelTitle.mas_bottom).with.offset(3);
        make.left.equalTo(10);
        make.right.equalTo(-10);
        make.height.equalTo(40);
    }];
    header.backgroundColor = [UIColor colorWithHexValue:0xd5efff];
    return header;
}

- (void)mapCardClick {
    [[ZPBankApiWrapper sharedInstance] mapCardInWithViewController:self];
}

- (void)mapAccountClick {
    @weakify(self);
    [ZPSelectBankView showWithType:ZPBSupportType_Account withComplete:^(ZPBankSupport *bank) {
        @strongify(self);
        [self linkAccount:bank.code];
    }];
}


- (void)linkAccount:(NSString *)bankCode {
    @weakify(self);
    BillDetailViewController *billDetailViewController = [[BillDetailViewController alloc] initWithComplete:^(NSDictionary *data) {
        @strongify(self);
        NSArray *allController = self.navigationController.viewControllers;
        UIViewController *toViewController = self;
        for (UIViewController *oneController in allController) {
            if ([oneController isKindOfClass:[ZPWalletActionViewController class]]) {
                toViewController = oneController;
                break;
            }
        }
        [self.navigationController popToViewController:toViewController animated:NO];
    } cancel:^(NSDictionary *data) {
        @strongify(self);
        [self.navigationController popToViewController:self animated:YES];
    } error:^(NSDictionary *data) {
        @strongify(self);
        [self.navigationController popToViewController:self animated:YES];
    }];
    [billDetailViewController startProcessBill:[ZPBill addBankAcount:bankCode] from:self];
}

- (IBAction)actionButtonClick:(id)sender {
    UIButton *button = (UIButton *)sender;
    NSInteger index = button.tag;
    WithdrawCategoryItem *category = [self.dataSource objectAtIndex:index];
    if (category.type == WithdrawCategoryItemTypeLevel) {

    } else if (category.type == WithdrawCategoryItemTypeAtmCard) {
        [self mapCardClick];
    } else if (category.type == WithdrawCategoryItemTypeAccount) {
        [self mapAccountClick];
    }
}

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    WithdrawCategoryItem *category = [self.dataSource objectAtIndex:indexPath.row];
    if ([category isKindOfClass:[WithdrawCategoryItem class]]) {
        return 44.;
    }
    if ([category isKindOfClass:[WithdrawTitleItem class]]) {
        return 25;
    }
    return 60.;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WithdrawCategoryItem *category = [self.dataSource objectAtIndex:indexPath.row];
    if ([category isKindOfClass:[WithdrawCategoryItem class]]) {
        WithdrawCategoryCell *cell = [WithdrawCategoryCell cellFromSameNibForTableView:tableView];
        cell.btAction.tag = indexPath.row;
        [cell.btAction addTarget:self action:@selector(actionButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell setData:category];
        return cell;
    }
    
    if ([category isKindOfClass:[WithdrawTitleItem class]]) {
        WithdrawTitleCell *cell = [WithdrawTitleCell defaultCellForTableView:tableView];
        [cell setData:(WithdrawTitleItem *)category];
        return cell;
    }
    
    WithdrawDetailItem *detail = (WithdrawDetailItem *)category;
    WithdrawDetailCell *cell = [WithdrawDetailCell cellFromSameNibForTableView:tableView];
    [cell setData:detail];
    return cell;
}

@end
