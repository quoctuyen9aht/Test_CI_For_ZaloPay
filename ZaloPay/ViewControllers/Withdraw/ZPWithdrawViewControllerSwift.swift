//
//  ZPWithdrawViewController.swift
//  ZaloPay
//
//  Created by Quang Huy on 6/17/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import ZaloPayProfile
import ZaloPayAnalyticsSwift

enum WithdrawMoneyValue: Int {
    case withdraw100    = 100000
    case withdraw200    = 200000
    case withdraw500    = 500000
    case withdraw1000   = 1000000
    case withdraw2000   = 2000000
}

class ZPWithdrawViewControllerSwift: BaseViewController {
    let ZPWithdrawCollectionViewCellIdentifier = "ZPWithdrawCollectionViewCell"
    let ZPWithdrawBalanceCollectionViewCellIdentifier = "ZPWithdrawBalanceCollectionViewCell"
    let headerTitle = R.string_Withdraw_MoneyNumber_Title()
    let remainTitle = R.string_Withdraw_Balance()
    let notEnoughMoneyViewHeight: CGFloat = 38.0
    
//MARK: Outlets

    @IBOutlet weak var collectionViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var notEnoughMoneyLabel: MDHTMLLabel!
    @IBOutlet weak var notEnoughMoneyView: UIView!
    @IBOutlet weak var bufferView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var balanceTitle: UILabel!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var balanceImgView: ZPIconFontImageView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var topViewHeightConstraint: NSLayoutConstraint!
//MARK: Custom variables
    var presenter: WithdrawPresenterInterface?
    var flowLayout: ZPWithdrawCollectionViewLayout!
    var withdrawMoneyArray = [Int]()
    var isValidBallance = false
    var isLoading = false
    var isNotEnoughMoney = false
    var isProcessing = false
    var balance = 0;
//MARK: Life cycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
        setupView()
        presenter?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ZPTrackingHelper.shared().trackScreen(with: self)
    }
    
    override func backButtonClicked(_ sender: Any!) {
        super.backButtonClicked(sender)
    }
    
    override func backEventId() -> Int {
        return ZPAnalyticEventAction.balance_withdraw_touch_back.rawValue;
    }
    
    override func screenName() -> String! {
        return "[iOS][Withdraw]"
    }
    
//MARK: Custom functions
    private func setupView() {
        let cellNib = UINib(nibName: ZPWithdrawCollectionViewCellIdentifier, bundle: nil)
        collectionView.register(cellNib, forCellWithReuseIdentifier: ZPWithdrawCollectionViewCellIdentifier)
        let headerNib = UINib(nibName: ZPWithdrawBalanceCollectionViewCellIdentifier, bundle: nil)
        collectionView.register(headerNib, forCellWithReuseIdentifier: ZPWithdrawBalanceCollectionViewCellIdentifier)
        collectionView.alwaysBounceVertical = true
        
        flowLayout = ZPWithdrawCollectionViewLayout(topViewHeight: self.view.bounds.height / 4, bufferHeight: 18.0)
        collectionView.collectionViewLayout = flowLayout
        balanceImgView.setIconFont("header_overbalance")
        balanceImgView.setIconColor(UIColor.white)
        bufferView.backgroundColor = UIColor.zaloBase()
        topViewHeightConstraint.constant = self.view.bounds.height / 4
        self.view.backgroundColor = UIColor.white
    }
    
    private func setupNavigation() {
        self.baseViewControllerViewDidLoad()
        self.navigationItem.title = R.string_Withdraw_Title()
    }
    
    func setupNotEnoughMoneyView() {
        if isValidBallance {
            notEnoughMoneyView.isHidden = true
            return
        }
        self.isNotEnoughMoney = true
        notEnoughMoneyView.isHidden = false
        if (ZPProfileManager.shareInstance.userLoginData?.isClearing ?? false) {
            notEnoughMoneyLabel.delegate = self
            notEnoughMoneyLabel.htmlText = R.string_Clearing_not_enough_money_withdraw()
        }else {
            notEnoughMoneyLabel.text = R.string_Withdraw_Not_Enough_Money_Info()
            if UIScreen.main.bounds.height <= 568.0 {
                notEnoughMoneyLabel.font = notEnoughMoneyLabel.font.withSize(15.0)
            }
        }
        heightConstraint.constant = notEnoughMoneyViewHeight
        collectionViewTopConstraint.constant = notEnoughMoneyViewHeight
        self.view.layoutIfNeeded()
    }
}

//MARK: WithdrawViewInterface
extension ZPWithdrawViewControllerSwift: WithdrawViewInterface {
    func hideLoadingView() {
        guard let navView = self.navigationController?.view else {
            return
        }
        ZPAppFactory.sharedInstance().hideAllHUDs(for: navView)
        self.isLoading = false
        self.isProcessing = false
    }

    func showLoadingView() {
        if !self.isLoading {
            guard let navView = self.navigationController?.view else {
                return
            }
            ZPAppFactory.sharedInstance().showHUDAdded(to: navView)
            self.isLoading = true
        }
    }

    func updateViewAfterGettingWithdrawMoney(withdrawMoney: [Int]?) {
        self.withdrawMoneyArray = withdrawMoney ?? []
        self.collectionView.reloadData()
    }
    
    func updateViewAfterGettingRemainMoneyNumber(remainNumber: Int?,isValidBallance: Bool) {
        balance = remainNumber ?? 0
        self.isValidBallance = isValidBallance
        // text
        let text = ZPWalletManagerSwift.sharedInstance.currentBalance != nil ? NSNumber(integerLiteral: balance).formatMoneyValue() ?? "" : ""
        self.balanceLabel.text = text
        self.balanceTitle.text = R.string_Withdraw_Free_Charge_Title()
        self.topView.backgroundColor = UIColor.zaloBase()
        
        if UIScreen.main.bounds.height <= 568.0 {
            // IPhone 5
            balanceLabel.font = balanceLabel.font.withSize(26.0)
            balanceTitle.font = balanceTitle.font.withSize(13.0)
        }
        self.setupNotEnoughMoneyView()
    }
    
    func isValidMoneyNumber(_ number: Int) -> Bool {
        if number == Int(WithDrawNumberOther) {            
            return self.isValidBallance;
        }
        return (number > 0) && (balance >= number)
    }
}

//MARK: CollectionViewDelegate
extension ZPWithdrawViewControllerSwift: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let withdrawMoney = withdrawMoneyArray[indexPath.row]
        
        if (ZPProfileManager.shareInstance.userLoginData?.isClearing ?? false){
            if balance - withdrawMoney < 1000 && balance - withdrawMoney > 0 {
                
                let min = ZPWalletManagerSwift.sharedInstance.config.minTransfer
                let stringMoney: String = "\(min)".formatCurrency()
                let message = R.string_Clearing_withdraw_faild_min() + "\(stringMoney)"
                
                ZPDialogView.showDialog(with: DialogTypeError, title: nil, message: message, buttonTitles: [R.string_ButtonLabel_Close()], handler: nil)
                return ;
            }
        }
        
        let isValid = isValidMoneyNumber(withdrawMoney)
        if isValid && !isProcessing {
            if withdrawMoney != Int(WithDrawNumberOther) {
                self.showLoadingView()
                self.isProcessing = true
                switch withdrawMoney {
                case WithdrawMoneyValue.withdraw100.rawValue:
                    ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.balance_withdraw_touch_100)
                case WithdrawMoneyValue.withdraw200.rawValue:
                    ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.balance_withdraw_touch_200)
                case WithdrawMoneyValue.withdraw500.rawValue:
                    ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.balance_withdraw_touch_500)
                case WithdrawMoneyValue.withdraw1000.rawValue:
                    ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.balance_withdraw_touch_1000)
                case WithdrawMoneyValue.withdraw2000.rawValue:
                    ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.balance_withdraw_touch_2000)
                default:
                    ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.balance_withdraw_touch_other)
                }
            } else {
                ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.balance_withdraw_touch_other)
            }
            
            self.presenter?.didSelectMoneyNumber(moneyNumber: withdrawMoneyArray[indexPath.row])
        }
    }
}

//MARK: CollectionViewDatasource

extension ZPWithdrawViewControllerSwift: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.withdrawMoneyArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ZPWithdrawCollectionViewCellIdentifier, for: indexPath) as? ZPWithdrawCollectionViewCell else {
            return UICollectionViewCell()
        }
        let moneyValue = withdrawMoneyArray[indexPath.row]
        let isValid = isValidMoneyNumber(moneyValue)
        cell.configureCell(withdrawNumber: withdrawMoneyArray[indexPath.row], isValidBallance: isValid)
        return cell
    }
}

extension ZPWithdrawViewControllerSwift: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffset = scrollView.contentOffset
        if contentOffset.y < 0.0 {
            topConstraint.constant = 0.0
            heightConstraint.constant = -contentOffset.y + (isNotEnoughMoney ? notEnoughMoneyViewHeight : 0.0)
        } else {
            topConstraint.constant = -contentOffset.y * 1.4
            heightConstraint.constant = isNotEnoughMoney ? notEnoughMoneyViewHeight : 0.0
        }
        self.view.layoutIfNeeded()
    }
}
extension ZPWithdrawViewControllerSwift: MDHTMLLabelDelegate {
    func htmlLabel(_ label: MDHTMLLabel!, didSelectLinkWith URL: URL!) {
        if URL.absoluteString == "transfer" {
            ZPCenterRouter.launch(routerId: .transferMoney, from: self)
//            let vc = TransferHomeRouter.assembleModule()
//            vc.title = R.string_TransferHome_Title()
//            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
