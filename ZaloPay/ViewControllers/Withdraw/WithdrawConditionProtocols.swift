//
//  WithdrawConditionProtocols.swift
//  ZaloPay
//
//  Created by Duy Dao Pham Hoang on 6/16/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit

protocol WithdrawConditionPresenterProtocol: class {
    var view: WithdrawConditionViewProtocol? { get set }
    var interactor: WithdrawConditionInteractorInputProtocol? { get set }
    var wireFrame: WithdrawConditionWireFrameProtocol? { get set }
    
    // VIEW -> PRESENTER
    func viewDidLoad()
//    func getDataListBank()
}

protocol WithdrawConditionWireFrameProtocol: class {
    // PRESENTER -> WIREFRAME
    //func presentPostDetailScreen(from view: PostListViewProtocol, forPost post: PostModel)
}


protocol WithdrawConditionViewProtocol: class {
    var presenter: WithdrawConditionPresenterProtocol? { get set }
//    func resultListBank(_ listBank:NSMutableArray)
    // PRESENTER -> VIEW
}

protocol WithdrawConditionInteractorInputProtocol: class {
    var presenter: WithdrawConditionInteractorOutputProtocol? { get set }
//    func requestGetListBank()
    // PRESENTER -> INTERACTOR
}

protocol WithdrawConditionInteractorOutputProtocol: class {
    // INTERACTOR -> PRESENTER
//    func resultListBank(_ listBank:NSMutableArray)
}




