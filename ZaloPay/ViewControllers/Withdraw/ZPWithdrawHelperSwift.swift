//
//  ZPWithdrawHelperSwift.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 4/13/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayCommonSwift

@objcMembers
public class ZPWithdrawHelperSwift: NSObject {
    public class func validWithdrawCondition() -> Bool {
        let enableBank = self.enableBank()
        return self.validWithdrawConditionOfSaveCard(enableBank: enableBank) || self.validWithdrawConditionOfSaveAccount(enableBank: enableBank)
    }
    class func validWithdrawConditionOfSaveCard(enableBank:[AnyHashable:Any]) -> Bool {
        if let saveCard = ZaloPayWalletSDKPayment.sharedInstance().paymentSavedCard() as? [ZPSavedCard] {
            if saveCard.count == 0 {
                return false
            }
            for card in saveCard {
                let type = card.bankType.rawValue
                if let _ = enableBank[type] as? ZPBankSupport {
                    return true
                }
            }
        }
        return false
    }
    class func validWithdrawConditionOfSaveAccount(enableBank:[AnyHashable:Any]) -> Bool {
        if let saveAccount = ZaloPayWalletSDKPayment.sharedInstance().paymentSavedBankAccount() as? [ZPSavedBankAccount] {
            if saveAccount.count == 0 {
                return false
            }
            for account in saveAccount {
                let type = account.bankType.rawValue
                if let _ = enableBank[type] as? ZPBankSupport {
                    return true
                }
            }
        }
        return false
    }

    public class func enableBank() -> [AnyHashable:Any] {
        var banDic = [AnyHashable:Any]()
        let allBank = ZPWalletManagerSwift.sharedInstance.bankSupports
        for oneBank in allBank {
            if oneBank.enableWithdraw.not {
                continue
            }
            let type = ZPBankMapping.bankType(oneBank.code, first6CardNo: nil).rawValue
            banDic[type] = oneBank
        }
        return banDic
    }
    public class func requestUserCard() -> RACSignal<AnyObject> {
        
        return RACSignal.createSignal({ (subscriber) -> RACDisposable? in
            ZaloPayWalletSDKPayment.sharedInstance().getGatewayInfoCompleteHandle { (errorCode, mesage) in
                if errorCode < 0 {
                    subscriber.sendError(nil)
                    return
                }
                subscriber.sendNext(nil)
                subscriber.sendCompleted()
            }
            return nil
        })
    }
    
    public class func isShowWithDraw() -> RACSignal<NSNumber> {
        let bankSignal = ZPWalletManagerSwift.sharedInstance.getListBankSupport()
        let userCard = self.requestUserCard()
        ZPAppFactory.sharedInstance().showHUDAdded(to: nil)
        return bankSignal.zip(with: userCard).catch({ (error) -> RACSignal<AnyObject> in
            ZPAppFactory.sharedInstance().hideAllHUDs(for: nil)
            ZPDialogView.showDialog(with: DialogTypeWarning, message: R.string_NetworkError_NoConnectionMessage(), cancelButtonTitle: R.string_ButtonLabel_Close(), otherButtonTitle: nil, completeHandle: nil)
            return RACSignal.empty()
        }).flattenMap({ (value) -> RACSignal<AnyObject>? in
            ZPAppFactory.sharedInstance().hideAllHUDs(for: nil)
            return RACSignal<AnyObject>.`return`(self.validWithdrawCondition() as AnyObject)
        }).deliverOnMainThread() as! RACSignal<NSNumber>
        
    }
    
    
}
