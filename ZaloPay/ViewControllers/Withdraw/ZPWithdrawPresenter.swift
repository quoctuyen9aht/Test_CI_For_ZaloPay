//
//  ZPWithdrawPresenter.swift
//  ZaloPay
//
//  Created by Quang Huy on 6/18/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit

class ZPWithdrawPresenter: NSObject {
    fileprivate weak var view: WithdrawViewInterface!
    fileprivate var interactor: WithdrawInteractorInterface
    fileprivate var router: WithdrawRouterInterface

//MARK: Init
    init(view: WithdrawViewInterface, interactor: WithdrawInteractorInterface, router: WithdrawRouterInterface) {
        self.view = view
        self.interactor = interactor
        self.router = router
        super.init()
    }
}

//MARK: WithdrawPresenterInterface
extension ZPWithdrawPresenter: WithdrawPresenterInterface {
    func viewDidLoad() {
        self.interactor.getRemainMoneyNumber()
        self.interactor.getWithdrawMoney()
        
        // add track
        ZMEventManager.registerHandler(self, eventType: 0) { [weak self](e) in
            // refresh
            // change to main
            DispatchQueue.main.async {
                self?.interactor.getRemainMoneyNumber()
            }
        }
    }
    
    func didSelectMoneyNumber(moneyNumber: Int) {
        if moneyNumber == Int(WithDrawNumberOther) {
            let vc = WithdrawOtherNumberRouter.setupModule()
            self.router.pushToViewController(view: vc, animated: true)
        }
        else {
            self.interactor.withdrawMoneyNumber(moneyNumber: moneyNumber)
        }

    }
}

extension ZPWithdrawPresenter: WithdrawInteractorOutput {
    func didGetRemainMoneyNumber(moneyNumber: Int?,isValidBallance: Bool) {
        view.updateViewAfterGettingRemainMoneyNumber(remainNumber: moneyNumber, isValidBallance: isValidBallance)
    }
    
    func didGetWithdrawMoney(withdrawMoney: [Int]?) {
        view.updateViewAfterGettingWithdrawMoney(withdrawMoney: withdrawMoney)
    }
    
    func withdrawMoneySuccessfully(dict: NSDictionary) {
        let billInfo = ZPBill(data: dict as? [String : Any],
                              transType: ZPTransType.withDraw,
                              orderSource: Int32(OrderSource_None.rawValue),
                              appId:withdrawAppId)
        interactor.processBill(billInfo: billInfo, appId: withdrawAppId)
    }
    
    func withdrawMoneyFailWithError(error: Error?) {
        ZPDialogView.showDialogWithError(error) {
            //TODO
        }
        self.view.hideLoadingView()
    }
        
    func processBillSuccessfully(appId: Int, billInfo: ZPBill) {
        guard let viewController = self.view as? UIViewController else{
            return
        }
        self.view.hideLoadingView()
        ZPAppFactory.sharedInstance().showZaloPayGateway(viewController,
                                                         appId: appId,
                                                         with: billInfo,
                                                         complete:
        { [weak self](_) in
            self?.router.popToRootViewController()
        }, cancel: { [weak self](_) in
            guard let this = self else {
                return
            }
            this.router.popToViewController(view: this.view, animated: true)
        }) { [weak self](_) in
            self?.router.popToRootViewController()
        }
    }
}
