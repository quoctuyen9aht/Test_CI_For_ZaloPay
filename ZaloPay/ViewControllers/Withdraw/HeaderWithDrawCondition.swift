//
//  HeaderWithDrawCondition.swift
//  ZaloPay
//
//  Created by Duy Dao Pham Hoang on 6/20/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit

private let heightCell:CGFloat = 287
private let heightCellSmall:CGFloat = 256
private let imageHeightWithdrawCondition:CGFloat = 112
private let imageWidthWithdrawCondition:CGFloat = 130

class HeaderWithDrawCondition: UICollectionReusableView {
    
    var imageHolder:UIImageView?
    var lblTitleHolder:UILabel?
    var lblContentHolder:UILabel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup()
    {
        if (UIScreen.main.applicationFrame.size.height < 500){
            imageHolder = UIImageView(frame: CGRect(x: (self.frame.width - imageWidthWithdrawCondition)/2, y: 15, width: imageWidthWithdrawCondition, height: imageHeightWithdrawCondition))
        }
        else
        {
            imageHolder = UIImageView(frame: CGRect(x: (self.frame.width - imageWidthWithdrawCondition)/2, y: 46, width: imageWidthWithdrawCondition, height: imageHeightWithdrawCondition))
        }

        imageHolder?.image = UIImage(named: "ico_DKRT")
        self.addSubview(imageHolder!)

        lblTitleHolder = UILabel(frame: CGRect(x: 20, y: (imageHolder?.frame.size.height)! + (imageHolder?.frame.origin.y)! + 14, width: (self.frame.width) - 40, height: 25))
        lblTitleHolder?.font = UIFont.sfuiTextSemibold(withSize: 17)
        lblTitleHolder?.text = R.string_WithdrawCondition_NeedMoreCondition()
        lblTitleHolder?.textAlignment = .center
        lblTitleHolder?.textColor = UIColor.subText()
        self.addSubview(lblTitleHolder!)

        lblContentHolder = UILabel(frame: CGRect(x: 20, y: (lblTitleHolder?.frame.size.height)! + (lblTitleHolder?.frame.origin.y)! + 26, width: (self.frame.width) - 40, height: 0))
        lblContentHolder?.font = UIFont.sfuiTextRegular(withSize: 14)
        lblContentHolder?.numberOfLines = 0
        lblContentHolder?.textColor = UIColor.subText()
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5.0
        let attrString = NSMutableAttributedString(string: R.string_WithdrawCondition_Title_Requiment())
        attrString.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString.length))
        lblContentHolder?.attributedText = attrString
        lblContentHolder?.textAlignment = NSTextAlignment.center
        lblContentHolder?.sizeToFit()
        self.addSubview(lblContentHolder!)
    }
    
    class func heightWidthDraw() -> CGFloat
    {
        if (UIScreen.main.applicationFrame.size.height < 500){
            return heightCellSmall
        }
        return heightCell
    }
}
