//
//  ZPWithdrawCollectionViewLayout.swift
//  ZaloPay
//
//  Created by Quang Huy on 6/18/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit

class ZPWithdrawCollectionViewLayout: UICollectionViewLayout {
    private let margin: CGFloat = 12.0
    private let spaceBetweenItems: CGFloat = 12.0
    private let numberColumn = 2
    private let ratio: CGFloat = 1.8
    private var yOffSet: CGFloat = 0.0
    private var contentWidth: CGFloat!
    private var contentHeight: CGFloat!
    private var attributes = [IndexPath: UICollectionViewLayoutAttributes]()
    var topViewHeight: CGFloat
    var bufferHeight: CGFloat
    
    init(topViewHeight: CGFloat, bufferHeight: CGFloat) {
        self.topViewHeight = topViewHeight
        self.bufferHeight = bufferHeight
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepare() {
        guard let numberSection = collectionView?.numberOfSections else {
            return
        }
        guard numberSection > 0 else {
            return
        }

        guard attributes.isEmpty else {
            return
        }
        prepareforWithdrawMoneySection()
    }
    
    private func prepareforWithdrawMoneySection() {
        // Cell
        yOffSet = topViewHeight - bufferHeight
        let numberItems = collectionView?.numberOfItems(inSection: 0) ?? 0
        let itemWidth: CGFloat = (UIScreen.main.bounds.width - (CGFloat(numberColumn - 1) * spaceBetweenItems + 2 * margin)) / CGFloat(numberColumn)
        let itemHeight = itemWidth / ratio
        var maxY = yOffSet
        for index in 0..<numberItems {
            let indexPath = IndexPath(item: index, section: 0)
            let withdrawAttribute = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            let itemIndex = self.getColumnIndexFromIndex(index: index)
            let x = margin + spaceBetweenItems * CGFloat(itemIndex.column - 1) + CGFloat(itemIndex.column - 1) * itemWidth
            let y = yOffSet + margin * CGFloat(itemIndex.row - 1) + CGFloat(itemIndex.row - 1) * itemHeight
            maxY = max(maxY, y)
            withdrawAttribute.frame = CGRect(x: x, y: y, width: itemWidth, height: itemHeight)
            attributes[indexPath] = withdrawAttribute
        }
        contentWidth = UIScreen.main.bounds.width
        contentHeight = maxY + itemWidth + margin
    }
    
    private func getColumnIndexFromIndex(index: Int) -> (row: Int, column: Int) {
        let realIndex = index + 1
        var column = realIndex % numberColumn
        if column == 0 {
            column = index == 0 ? 0 : numberColumn
        }
        let row = index/numberColumn + 1
        return (row, column)
    }
    
    override var collectionViewContentSize: CGSize {
        if contentWidth != nil && contentHeight != nil {
            return CGSize(width: contentWidth, height: contentHeight)
        }
        return CGSize.zero
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return attributes[indexPath]
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var allAttributes = [UICollectionViewLayoutAttributes]()
        for attribute in attributes.values {
            if attribute.frame.intersects(rect) {
                allAttributes.append(attribute)
            }
        }
        return allAttributes
    }
    
    override func invalidateLayout() {
        yOffSet = 0.0
        contentHeight = nil
        contentWidth = nil
        attributes.removeAll()
        super.invalidateLayout()
    }
}
