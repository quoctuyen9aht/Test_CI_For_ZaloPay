//
//  ZPWithdrawControllerInterface.swift
//  ZaloPay
//
//  Created by Quang Huy on 6/17/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit

protocol WithdrawRouterInterface: class {
    func popToRootViewController()
    func popToViewController(view: WithdrawViewInterface, animated: Bool)
    func pushToViewController(view: UIViewController, animated: Bool)
}

protocol WithdrawInteractorInterface {
    func getRemainMoneyNumber()
    func getWithdrawMoney()
    func withdrawMoneyNumber(moneyNumber: Int)
    func processBill(billInfo: ZPBill, appId: Int)
}

protocol WithdrawPresenterInterface: class {
    func viewDidLoad()
    func didSelectMoneyNumber(moneyNumber: Int)
}

protocol WithdrawInteractorOutput: class {
    func didGetRemainMoneyNumber(moneyNumber: Int?, isValidBallance: Bool)
    func didGetWithdrawMoney(withdrawMoney: [Int]?)
    func withdrawMoneySuccessfully(dict: NSDictionary)
    func withdrawMoneyFailWithError(error: Error?)
    func processBillSuccessfully(appId: Int, billInfo: ZPBill)
}

protocol WithdrawViewInterface: class {
    func updateViewAfterGettingRemainMoneyNumber(remainNumber: Int?, isValidBallance: Bool)
    func updateViewAfterGettingWithdrawMoney(withdrawMoney: [Int]?)
    func showLoadingView()
    func hideLoadingView()
}
