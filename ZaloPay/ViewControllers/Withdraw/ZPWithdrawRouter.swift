//
//  ZPWithdrawRouter.swift
//  ZaloPay
//
//  Created by Quang Huy on 6/19/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit

class ZPWithdrawRouter: NSObject {
    weak var view: UIViewController?
    
    @objc class func assembleModule() -> ZPWithdrawViewControllerSwift {
        let viewController = ZPWithdrawViewControllerSwift(nibName: "ZPWithdrawViewControllerSwift", bundle: nil)
        let interactor = ZPWithdrawInteractor()
        let router = ZPWithdrawRouter()
        let presenter = ZPWithdrawPresenter(view: viewController, interactor: interactor, router: router)
        
        interactor.presenter = presenter
        router.view = viewController
        viewController.presenter = presenter
        
        return viewController
    }
}

extension ZPWithdrawRouter: WithdrawRouterInterface {
    func popToRootViewController() {
        self.view?.navigationController?.popToRootViewController(animated: true)
    }
    
    func popToViewController(view: WithdrawViewInterface, animated: Bool) {
        guard let vc = view as? UIViewController else {
            return
        }
        self.view?.navigationController?.popToViewController(vc, animated: animated)
    }
    
    func pushToViewController(view: UIViewController, animated: Bool) {
        self.view?.navigationController?.pushViewController(view, animated: animated)
    }
    
}
