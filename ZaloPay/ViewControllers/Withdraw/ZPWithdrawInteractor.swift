//
//  ZPWithdrawInteractor.swift
//  ZaloPay
//
//  Created by Quang Huy on 6/18/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayConfig

class ZPWithdrawInteractor {
    weak var presenter: WithdrawInteractorOutput?
}

//MARK: WithdrawInteractorInterface
extension ZPWithdrawInteractor: WithdrawInteractorInterface {    
    func getWithdrawMoney() {
        let config = ZPApplicationConfig.getWithdrawConfig()
        var withdrawMoneyArray = config?.getWithdrawMoney()
        if  (withdrawMoneyArray?.count ?? 0) > 0{
            withdrawMoneyArray?.append(Int(WithDrawNumberOther))
        }
        presenter?.didGetWithdrawMoney(withdrawMoney: withdrawMoneyArray)
    }
    
    func getRemainMoneyNumber() {
        let config = ZPApplicationConfig.getWithdrawConfig()
        var min = ZPWalletManagerSwift.sharedInstance.config.minWithdraw 
        min = min > 0 ? min : (config?.getMinWithdrawMoney() ?? WithDrawMinDefault.toInt())
        
        let remainNumber = (ZPWalletManagerSwift.sharedInstance.currentBalance ?? 0).intValue
        let isValidBallance =  remainNumber >= min
        presenter?.didGetRemainMoneyNumber(moneyNumber: remainNumber,isValidBallance: isValidBallance)
    }
    
    func withdrawMoneyNumber(moneyNumber: Int) {
        let appUser = NetworkManager.sharedInstance().paymentUserId
        let description = R.string_Withdraw_Description_Message()
        let item = [ZPCreateWalletOrderTranstypeKey: ZPTransType.withDraw.rawValue] as NSDictionary
        let itemStr = item.jsonRepresentation()
    
        NetworkManager.sharedInstance().getWalletOrdeder(withAmount: moneyNumber, andTransactionType: Int32(ZPTransType.withDraw.rawValue), appUser: appUser, description: description, appId: withdrawAppId , embeddata: "", item: itemStr)
            .deliverOnMainThread()
            .subscribeNext({[weak self] json in
                guard let dict = json as? NSDictionary else {
                    return
                }
                self?.presenter?.withdrawMoneySuccessfully(dict: dict)
            }, error: {[weak self] error in
                self?.presenter?.withdrawMoneyFailWithError(error: error)
            })
    }
    
    func processBill(billInfo: ZPBill, appId: Int) {
        self.presenter?.processBillSuccessfully(appId: appId, billInfo: billInfo)
    }
}
