//
//  WithdrawPresenter.swift
//  ZaloPay
//
//  Created by Duy Dao Pham Hoang on 6/16/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//


class WithdrawConditionPresenter: WithdrawConditionPresenterProtocol {
    weak var view: WithdrawConditionViewProtocol?
    var interactor: WithdrawConditionInteractorInputProtocol?
    var wireFrame: WithdrawConditionWireFrameProtocol?
    
    func viewDidLoad() {
    }
}

extension WithdrawConditionPresenter: WithdrawConditionInteractorOutputProtocol {

//    func resultListBank(_ listBank: NSMutableArray) {
//        view?.resultListBank(listBank)
//    }
}
