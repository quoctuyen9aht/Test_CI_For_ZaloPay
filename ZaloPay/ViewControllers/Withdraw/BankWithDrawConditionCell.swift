//
//  BankWithDrawConditionCell.swift
//  ZaloPay
//
//  Created by Duy Dao Pham Hoang on 6/20/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit

class BankWithDrawConditionCell: UICollectionViewCell {
    
    var viewLayout:UIView?
    var imageLogoBank:UIInappResourceImageView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        viewLayout = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height))
        viewLayout?.layer.borderWidth = 1
        viewLayout?.layer.cornerRadius = 3
        viewLayout?.layer.borderColor = UIColor.line().cgColor
        self.addSubview(viewLayout!)
        
        imageLogoBank = UIInappResourceImageView(frame: CGRect(x: (self.frame.size.width - (self.frame.size.width - 2))/2, y: (self.frame.size.height - (self.frame.size.height - 2))/2, width: (self.frame.size.width - 2), height: (self.frame.size.height - 2)))
        imageLogoBank?.contentMode = .scaleAspectFit
        imageLogoBank?.clipsToBounds = true
        self.addSubview(imageLogoBank!)
    }
    
    func setData(_ icon:String, bankName: String)
    {
        imageLogoBank?.setImageFromResource(imagName: icon, title: bankName, titleColor: UIColor.zp_gray())
    }
}
