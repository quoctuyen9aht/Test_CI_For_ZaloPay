//
//  WithDrawWireFrame.swift
//  ZaloPay
//
//  Created by Duy Dao Pham Hoang on 6/19/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
@objcMembers
class WithdrawConditionWireFrame: NSObject, WithdrawConditionWireFrameProtocol {
    
    class func createWithDrawConditionModule() -> WithdrawConditionView {
        
        let withdrawConditionView = WithdrawConditionView()
        
        let presenter: WithdrawConditionPresenterProtocol & WithdrawConditionInteractorOutputProtocol = WithdrawConditionPresenter()
        let interactor: WithdrawConditionInteractorInputProtocol =
            WithdrawConditionInteractor()
        let wireFrame: WithdrawConditionWireFrameProtocol = WithdrawConditionWireFrame()
        
        withdrawConditionView.presenter = presenter
        presenter.view = withdrawConditionView as? WithdrawConditionViewProtocol
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        return withdrawConditionView
    }
}
