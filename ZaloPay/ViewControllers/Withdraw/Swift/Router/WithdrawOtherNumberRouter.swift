//
//  WithdrawOtherNumberRouter.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 10/13/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation

class WithdrawOtherNumberRouter {
    
    weak var view: UIViewController?
    
    static func setupModule() -> ZPWithdrawViewController {
        let viewController = ZPWithdrawViewController()        
        let presenter  = WithdrawOtherNumberPresenter()
        let interactor  = WithdrawOtherNumberInteractor()
        let router  = WithdrawOtherNumberRouter()
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        router.view = viewController
        viewController.presenter = presenter
        
        return viewController
    }
    
}

extension WithdrawOtherNumberRouter: WithdrawOrtherNumberRouterProtocol {
    // TODO: Implement wireframe methods
    func popToRootViewController() {
        guard let controller = view else {
            return
        }
        controller.navigationController?.popToViewController(controller, animated: true)
    }
}
