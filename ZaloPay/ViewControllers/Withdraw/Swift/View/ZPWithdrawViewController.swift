//
//  ZPWithdrawViewController.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 10/18/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import ZaloPayAnalyticsSwift
import ZaloPayProfile

class ZPWithdrawViewController: BillHandleBaseViewController {
    
    // MARK: Properties
    // Controller -> Presenter
    var presenter : WithdrawOrtherNumberPresenterProtocol?
    var tableView: UITableView!
    var dataSources : [Any]?
    var inputCell: ZPRechargeInputCell?
    var buttonContinue = UIButton()
    private let disposeBag = DisposeBag()
    
    // MARK: Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = R.string_Withdraw_Title()
        configDataSources()
        addTableView()
        addButtonContinue()
    }
    
    override func screenName() -> String! {
        return ZPTrackerScreen.Screen_iOS_Balance_Withdraw
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.observerKeyboard()        
        ZPTrackingHelper.shared().trackScreen(with: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.configUI()
    }
    // MARK: Private methods
    private func configDataSources() {
        presenter?.requestDataSourceForWithdraw()
    }
    
    private func configUI() {
        if ZPDialogView.isDialogShowing() {
            return
        }
        self.buttonContinue.isUserInteractionEnabled = true
        self.inputCell?.textFieldMoney.textField.isUserInteractionEnabled = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.inputCell?.textFieldMoney.textField.becomeFirstResponder()
        }
    }
    
    override func viewWillSwipeBack() {
    }
        
    func addTableView() {
        self.tableView = UITableView()
        self.view.addSubview(self.tableView)
        tableView.backgroundColor = UIColor(red: 0.94, green: 0.96, blue: 0.97, alpha: 1)
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView.isScrollEnabled = true
        tableView.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.top.equalTo(0)
            make.bottom.equalTo(0)
        }
        self.registerCell()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    func registerCell() {
        let nibMinValueTitle = UINib(nibName: "ZPRechargeMinValueTitleCell", bundle: nil)
        self.tableView.register(nibMinValueTitle, forCellReuseIdentifier: "ZPRechargeMinValueTitleCell")
        let nibButtonBottom = UINib(nibName: "ZPRechargeButtonBottomCell", bundle: nil)
        self.tableView.register(nibButtonBottom, forCellReuseIdentifier: "ZPRechargeButtonBottomCell")
        let nibHeader = UINib(nibName: "ZPRechargeHeaderCell", bundle: nil)
        self.tableView.register(nibHeader, forCellReuseIdentifier: "ZPRechargeHeaderCell")
    }
    
    func observerKeyboard() {
        keyboardHeight().observeOn(MainScheduler.instance).subscribe(onNext: { [weak self] (height) in
            if height == 0 {
                self?.keyBoardWillHide(keyboardHeight: height)
                return
            }
            self?.keyBoardWillShow(keyboardHeight: height)
        }).disposed(by: disposeBag)
    }
    
    func keyboardHeight() -> Observable<CGFloat> {
        return Observable
            .from([
                NotificationCenter.default.rx.notification(NSNotification.Name.UIKeyboardWillShow)
                    .map { notification -> CGFloat in
                        (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.height ?? 0
                },
                NotificationCenter.default.rx.notification(NSNotification.Name.UIKeyboardWillHide)
                    .map { _ -> CGFloat in
                        0
                }
                ])
            .merge()
    }

    func keyBoardWillShow(keyboardHeight: CGFloat) {
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardHeight + 5, right: 0.0)
        self.tableView?.contentInset = contentInsets
        self.tableView?.scrollIndicatorInsets = contentInsets

        if let row = self.tableView?.numberOfRows(inSection: 0) {
            let indexPathLast = IndexPath(row: row - 1, section: 0)
            self.tableView?.scrollToRow(at: indexPathLast, at: .top, animated: true)
        }
    }

    func keyBoardWillHide(keyboardHeight: CGFloat) {
        self.tableView?.contentInset = UIEdgeInsets.zero
        self.tableView?.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
    func addButtonContinue() {
        self.buttonContinue = UIButton(type: UIButtonType.custom)
        self.buttonContinue.setupZaloPay()
        self.buttonContinue.isEnabled = false
        self.view.addSubview(self.buttonContinue)
        self.buttonContinue.setTitle(R.string_ButtonLabel_Next(), for: UIControlState.normal)
        self.buttonContinue.addTarget(self, action: #selector(self.confirmButtonClick), for: UIControlEvents.touchUpInside)
        
        self.buttonContinue.snp.makeConstraints { (make) in
            make.top.equalTo(self.tableView.snp.bottom).offset(buttonToScrollOffset())
            make.left.equalTo(10)
            make.right.equalTo(-10)
            make.height.equalTo(kZaloButtonHeight)
        }
        self.buttonContinue.isHidden = true
    }
    

    //UI EVENT
    override func backButtonClicked(_ sender: Any) {
        super.backButtonClicked(sender)
    }
    
    override func backEventId() -> Int {
        return ZPAnalyticEventAction.balance_addcash_touch_back.rawValue
    }
    
    @objc func confirmButtonClick(_ sender: Any?) {
        let money = self.inputCell?.textFieldMoney.textField.text ?? ""
        let currentBalance = (ZPWalletManagerSwift.sharedInstance.currentBalance ?? 0).intValue
        if (ZPProfileManager.shareInstance.userLoginData?.isClearing ?? false) {
            let moneyValue = money.onlyDigit().toInt()
            if moneyValue > currentBalance {
                inputCell?.textFieldMoney.showError(withText: R.string_Withdraw_NotEnoughMoney())
                return
            }
            if currentBalance - moneyValue > 0 && currentBalance - moneyValue < 1000 {
                let min = ZPWalletManagerSwift.sharedInstance.config.minTransfer 
                let stringMoney: String = "\(min)".formatCurrency()
                inputCell?.textFieldMoney.showError(withText: R.string_Clearing_withdraw_faild_min() + "\(stringMoney)")
                return
            }
            
        }
        presenter?.validWithdrawMoneyValue(value: money)
    }
    
    func requestCreateOrder() {
        guard let money = self.inputCell?.textFieldMoney.textField.text,
            let totalValue = Int(money.onlyDigit()) else {
                return
        }
        self.showLoadingView()
        self.buttonContinue.isUserInteractionEnabled = false
        self.inputCell?.textFieldMoney.textField.resignFirstResponder()
        self.inputCell?.textFieldMoney.textField.isUserInteractionEnabled = false
        self.updateConfirmButton()
        let item = [ZPCreateWalletOrderTranstypeKey: ZPTransType.withDraw.rawValue] as NSDictionary
        let itemStr = item.jsonRepresentation()
        
        NetworkManager.sharedInstance().getWalletOrdeder(withAmount: totalValue,
                                                         andTransactionType: Int32(ZPTransType.withDraw.rawValue),
                                                         appUser: NetworkManager.sharedInstance().paymentUserId,
                                                         description: R.string_Withdraw_Description_Message(),
                                                         appId: withdrawAppId,
                                                         embeddata: "",
                                                         item: itemStr)
            .deliverOnMainThread()
            .subscribeNext({ [weak self] (json) in
                let billInfo : ZPBill = ZPBill(data: json as? [String : Any],
                                               transType: ZPTransType.withDraw,
                                               orderSource: Int32(OrderSource_None.rawValue),
                                               appId: withdrawAppId)
                self?.showBillViewController(billInfo)
                self?.buttonContinue.isUserInteractionEnabled = true
                self?.updateConfirmButton()
                self?.inputCell?.textFieldMoney.textField.isUserInteractionEnabled = true
                }, error:({ [weak self] (error) in
                    ZPDialogView.showDialogWithError(error, handle: {
                        self?.inputCell?.textFieldMoney.textField.becomeFirstResponder()
                    })
                    self?.hideLoadingView()
                    self?.buttonContinue.isUserInteractionEnabled = true
                    self?.updateConfirmButton()
                    self?.inputCell?.textFieldMoney.textField.isUserInteractionEnabled = true
                }))
    }
    
    func updateConfirmButton() {
        guard let tbView = self.tableView else {
            return
        }
        let indexPathLast = IndexPath(row: tbView.numberOfRows(inSection: 0) - 1, section: 0)
        if let cell = tbView.cellForRow(at: indexPathLast) {
            if cell.isKind(of: ZPRechargeButtonBottomCell.self) {
                tbView.reloadRows(at: [indexPathLast], with: .none)
            }
        }
    }
    func configTextFormat() {
        _ = self.inputCell?.textFieldMoney.textField.rx.text.subscribe(onNext: { [unowned self] (text) in
            self.inputCell?.textFieldMoney.clearErrorText()
            let textString = text ?? ""
            if textString.count > 0 {
                if self.buttonContinue.isEnabled == false {
                    self.buttonContinue.isEnabled = true
                    self.buttonContinue.setBackgroundColor(UIColor.zaloBase(), for: UIControlState.normal)
                    self.updateConfirmButton()
                }
                return
            }
            if self.buttonContinue.isEnabled {
                self.buttonContinue.isEnabled = false
                self.buttonContinue.setBackgroundColor(UIColor.subText(), for: UIControlState.normal)
                self.updateConfirmButton()
            }
        })
    }
}

extension ZPWithdrawViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSources?.count ?? 0
    }
    
    func setupRechargeMinValueTitleCell(item: ZPRechargeBaseItem, for indexPath: IndexPath) -> ZPRechargeMinValueTitleCell? {
        guard let titleCell = tableView.dequeueReusableCell(withIdentifier: "ZPRechargeMinValueTitleCell", for: indexPath) as? ZPRechargeMinValueTitleCell else {
            return nil
        }
        titleCell.setItem(item)
        titleCell.labelvnd.text = ""
        titleCell.labelMinvalue.textAlignment = .left
        let config = presenter?.getWithdrawConfig()
        let minNumber = (config?.min ?? 0) as NSNumber
        let min: String = minNumber.formatCurrency()
        let multipleStr = (config?.multiple ?? 0)as NSNumber
        let multiple: String = multipleStr.formatCurrency()
        
        titleCell.labelMinvalue.adjustsFontSizeToFitWidth = false
        titleCell.labelMinvalue.lineBreakMode = .byTruncatingTail
        titleCell.labelMinvalue.text = String(format: R.string_Recharge_Min_Multiple_Value(), min, multiple)
        titleCell.m_trailling_contraint.constant = 10
        titleCell.seperater.isHidden = true
        return titleCell
    }
    
    func setupRechargeButtonBottomCell(item: ZPRechargeBaseItem, for indexPath: IndexPath)  -> ZPRechargeButtonBottomCell? {
        guard let buttonCell = tableView.dequeueReusableCell(withIdentifier: "ZPRechargeButtonBottomCell", for: indexPath) as? ZPRechargeButtonBottomCell else {
            return nil
        }
        
        buttonCell.setItem(item)
        buttonCell.buttonConfirm.addTarget(self, action: #selector(confirmButtonClick), for: .touchUpInside)
        buttonCell.seperater.isHidden = true
        buttonCell.buttonConfirm.setTitle(R.string_ButtonLabel_Next(), for: UIControlState.normal)
        if self.buttonContinue.isEnabled {
            buttonCell.buttonConfirm.isUserInteractionEnabled = true
            buttonCell.buttonConfirm.isEnabled = true
            buttonCell.buttonConfirm.setBackgroundColor(UIColor.zaloBase(), for: .normal)
        } else {
            buttonCell.buttonConfirm.isUserInteractionEnabled = false
            buttonCell.buttonConfirm.isEnabled = false
            buttonCell.buttonConfirm.setBackgroundColor(UIColor.subText(), for: .normal)
        }
        return buttonCell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let item = self.dataSources?[indexPath.row] as? ZPRechargeBaseItem,
            let cell = item.cellClass.defaultCell(for: tableView) as? ZPRechargeBaseCell else {
                return UITableViewCell()
        }
        if cell.isKind(of: ZPRechargeInputCell.self) {
            cell.setItem(item)
            self.inputCell = cell as? ZPRechargeInputCell
            self.inputCell?.inputDelegate = self
            self.inputCell?.textFieldMoney.becomeFirstResponder()
            self.configTextFormat()
        }
        
        if cell.isKind(of: ZPRechargeMinValueTitleCell.self) {
            return self.setupRechargeMinValueTitleCell(item: item, for: indexPath) ?? cell
        }
        
        if cell.isKind(of: ZPRechargeButtonBottomCell.self) {
            return self.setupRechargeButtonBottomCell(item: item, for: indexPath) ?? cell
        }
        return cell
    }
}

extension ZPWithdrawViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let item = self.dataSources?[indexPath.row] as? ZPRechargeBaseItem else {
            return 0
        }
        
        return CGFloat(item.cellClass.height() + ((UIScreen.main.bounds.size.width <= 320 && item.isKind(of: ZPRecharMinvalueItem.self)) ? 20 : 0))
    }
}

extension ZPWithdrawViewController : ZPRechargeInputCellDelegate {
    func inputCellDidDoneInput() {
        self.confirmButtonClick(nil)
    }
    
    func textFieldShouldChange(_ sender: ZPRechargeInputCell!, text: String!) -> Bool {
        guard let money = inputCell?.textFieldMoney.textField?.text,
            let totalValue = Int("\(money)\(text)".onlyDigit()) else {
                return false
        }
        return presenter?.checkMaxWithdrawInput(value:totalValue) == false
    }
}

extension ZPWithdrawViewController : WithdrawOrtherNumberViewProtocol {
    //TODO: Implement WithdrawOrtherNumberView methods here

    func showErrorInValid(text: String) {
        self.inputCell?.textFieldMoney.showError(withText: text)
    }
    
    func updateSourceForWithdraw(data: [Any]) {
        self.dataSources = data
    }
    
    func processWithdraw() {
        self.inputCell?.textFieldMoney.resignFirstResponder()
        self.requestCreateOrder()
    }
}
