//
//  WithdrawOtherNumberInteractor.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 10/13/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import CocoaLumberjackSwift
import ZaloPayCommonSwift
import ZaloPayConfig

class WithdrawOtherNumberInteractor {
    // MARK: Properties
    weak var presenter: WithdrawOrtherNumberPresenterProtocol?
    var min: Int = 0
    var max: Int = 0
    var multiple : Int = 0
    init () {
        setupMinMaxValue()
    }
    
    func createDataSource() ->  [Any]{
        var dataSources = [Any]()
        let minvalue = ZPRecharMinvalueItem()
        let input = ZPRechargeInputItem()
        dataSources.append(ZPRechargeHeaderItem())
        dataSources.append(input)
        dataSources.append(minvalue)
        dataSources.append(ZPRechargeButtonBottomItem())
        return dataSources
    }
    
    func validMoney(value: Int) -> Bool {
        return value % multiple == 0
    }
    
    func isGreaterMaxValue(_ totalValue: Int) -> Bool {
        return totalValue > max
    }
    
    func setupMinMaxValue() {
        let config = ZPApplicationConfig.getWithdrawConfig()
        let min = ZPWalletManagerSwift.sharedInstance.config.minWithdraw 
        self.max = ZPWalletManagerSwift.sharedInstance.config.maxWithdraw 
        self.min = min > 0 ? min : (config?.getMinWithdrawMoney() ?? WithDrawMinDefault.toInt())
        self.multiple = config?.getMultipleWithdrawMoney() ??  WithDrawMultipleDefault.toInt()
    }
}

extension WithdrawOtherNumberInteractor: WithdrawOrtherNumberInteractorProtocol {

    func createDataSourceForWithdraw(){
        let data = createDataSource()
        presenter?.dataSourceForWithdraw(data: data)
    }
    
    func validWithdrawMoney(value: String?) {
        guard let money = value,
            let totalValue = Int(money.onlyDigit()) else {
                return
        }
        if totalValue > (ZPWalletManagerSwift.sharedInstance.currentBalance ?? 0).intValue {
            presenter?.showError(text: R.string_Withdraw_NotEnoughMoney())
            return
        }
        if totalValue < min {
            presenter?.showErrorWith(format: R.string_InputMoney_SmallAmount_Error_Message(), value: min)
            return
        }
        
        if self.isGreaterMaxValue(totalValue) {
            presenter?.showErrorWith(format: R.string_InputMoney_BigAmount_Error_Message(), value: max)
            return
        }
        if !self.validMoney(value: totalValue){
            presenter?.showError(text: R.string_Recharge_Condition())
            return
        }
        
        presenter?.validateWithdrawSuccess()
    }
    
    func checkMaxWithdraw(_ totalValue: Int) -> Bool {
        let isGreaterThan = self.isGreaterMaxValue(totalValue)
        if isGreaterThan {
            presenter?.showErrorWith(format: R.string_InputMoney_BigAmount_Error_Message(), value: max)
        }
        return isGreaterThan
    }
    
    func getWithdrawConfig() -> (min: Int, max: Int, multiple: Int) {
        return (min, max, multiple)
    }
}
