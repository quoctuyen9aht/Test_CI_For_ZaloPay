//
//  WithdrawOrtherNumberProtocol.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 10/13/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation

protocol WithdrawOrtherNumberPresenterProtocol : class {
    func requestDataSourceForWithdraw()
    func validWithdrawMoneyValue(value: String?)
    func checkMaxWithdrawInput(value: Int) -> Bool
    func showError(text: String)
    func showErrorWith(format: String, value: Int)
    func dataSourceForWithdraw(data: [Any])
    func validateWithdrawSuccess()
    func getWithdrawConfig() -> (min: Int, max: Int, multiple: Int)?
}

protocol WithdrawOrtherNumberViewProtocol : class {
    func showErrorInValid(text: String)
    func updateSourceForWithdraw(data: [Any])
    func processWithdraw()
}

protocol WithdrawOrtherNumberInteractorProtocol : class {
    func createDataSourceForWithdraw()
    func validWithdrawMoney(value: String?)
    func checkMaxWithdraw(_ totalValue: Int) -> Bool
    func getWithdrawConfig() -> (min: Int, max: Int, multiple: Int)
}

protocol WithdrawOrtherNumberRouterProtocol : class {
    func popToRootViewController()
}

