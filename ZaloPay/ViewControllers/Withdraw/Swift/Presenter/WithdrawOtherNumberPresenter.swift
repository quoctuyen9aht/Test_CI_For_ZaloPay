//
//  WithdrawOtherNumberPresenter.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 10/13/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation

class WithdrawOtherNumberPresenter {
    weak var view: WithdrawOrtherNumberViewProtocol?
    var interactor: WithdrawOrtherNumberInteractorProtocol?
    var router: WithdrawOrtherNumberRouterProtocol?
}

extension WithdrawOtherNumberPresenter: WithdrawOrtherNumberPresenterProtocol {
    //TODO: Implement other methods from presenter->view here

    func requestDataSourceForWithdraw() {
        interactor?.createDataSourceForWithdraw()
    }
    
    func validWithdrawMoneyValue(value: String?) {
        interactor?.validWithdrawMoney(value: value)
    }
    
    func checkMaxWithdrawInput(value: Int) -> Bool {
        return interactor!.checkMaxWithdraw(value)
    }

    //TODO: Implement other methods from interactor->presenter here
    func showError(text: String) {
        view?.showErrorInValid(text: text)
    }
    
    func showErrorWith(format: String, value: Int) {
        let min = value as NSNumber
        let money = min.formatCurrency()!
        let message = String(format: format, money)
        view?.showErrorInValid(text: message)
    }
    
    func dataSourceForWithdraw(data: [Any]) {
        view?.updateSourceForWithdraw(data: data)
    }
    func validateWithdrawSuccess() {
        view?.processWithdraw()
    }
    
    func getWithdrawConfig() -> (min: Int, max: Int, multiple: Int)? {
        return interactor?.getWithdrawConfig()
    }
}
