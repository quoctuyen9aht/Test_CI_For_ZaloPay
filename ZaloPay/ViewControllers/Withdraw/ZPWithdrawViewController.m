////
////  ZPWithdrawViewController.m
////  ZaloPay
////
////  Created by bonnpv on 8/12/16.
////  Copyright © 2016-present VNG Corporation. All rights reserved.
////
//
//#import "ZPWithdrawViewController.h"
//#import "NetworkManager+Order.h"
//#import "ZPRechargeCell.h"
//#import "ZPWalletManager.h"
//#import "ApplicationState.h"
//#import "ZPRechargeItem.h"
//#import "NetworkManager+Order.h"
//#import "BillDetailViewController.h"
//#import "ZPWalletManager.h"
//#import "UIScrollView+ScrollToBottom.h"
//#import <ZaloPayCommon/BaseViewController+Keyboard.h>
//#import "ZPRechargeMainViewController+EventLog.h"
//#import <ZaloPayCommon/BaseViewController+ZPScrollViewController.h>
//#import <ZaloPayCommon/ZPConstants.h>
//#import <ZaloPayCommon/TrackerEvents.h>
//#import <ZaloPayAnalytics/TrackerScreen.h>
//@interface ZPRechargeMainViewController (Withdraw) <UITableViewDelegate, UITableViewDataSource, ZPRechargeInputCellDelegate>
//@property (nonatomic, strong) ZPRechargeInputCell *inputCell;
//@property (nonatomic, strong) UIButton *buttonConfirm;
//@property (nonatomic, strong) UITableView *tableView;
//@property (nonatomic, strong) NSArray *dataSource;
//@property (nonatomic) long min;
//@property (nonatomic) long max;
//- (BOOL)validInputData;
//@end
//
//@interface ZPWithdrawViewController ()
//@property (nonatomic) long multiple;
//@end
//
//@implementation ZPWithdrawViewController
//
//- (void)viewDidLoad {
//    [super viewDidLoad];
//     self.title = [R string_Withdraw_Title];
//    [self.buttonConfirm setTitle:[R string_ButtonLabel_Next] forState:UIControlStateNormal];
//    self.buttonConfirm.hidden = true;
//    [self updateConfirmButton];
//    
//    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(0);
//        make.left.equalTo(0);
//        make.right.equalTo(0);
//        make.bottom.equalTo(0);
//    }];
//
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillShow:)
//                                                 name:UIKeyboardWillShowNotification
//                                               object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillHide:)
//                                                 name:UIKeyboardWillHideNotification
//                                               object:nil];
//    
//    [[ZPAppFactory sharedInstance].eventTracker trackScreen:Screen_Balance_Withdraw];
//}
//
//- (void)dealloc {
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
//}
//
//- (void)loadDataSource {
//    NSMutableArray *dataSource = [NSMutableArray array];
//    ZPRecharMinvalueItem *minvalue = [[ZPRecharMinvalueItem alloc] init];
//    minvalue.title = [NSString stringWithFormat:[R string_Recharge_Min_Value],[@(self.min) formatMoneyValue]];
//    
//    ZPRechargeInputItem *input = [[ZPRechargeInputItem alloc] init];
//    [dataSource addObject:[[ZPRechargeHeaderItem alloc] init]];
//    [dataSource addObject:input];
//    [dataSource addObject:minvalue];
//    [dataSource addObject:[[ZPRechargeButtonBottomItem alloc] init]];
//    
//    self.dataSource = dataSource;
//}
//
//- (float)scrollHeight {
//    return  101 + (( UIScreen.mainScreen.bounds.size.width <= 320) ? 20  : 0) + 167 + kZaloButtonHeight + kZaloButtonToScrollOffset;
//}
//
//- (void)setupMinMaxValue {
//    self.max = [ZPWalletManager sharedInstance].config.maxWithdraw;
//    self.min = [ZPWalletManager sharedInstance].config.minWithdraw;
//    self.multiple = [ApplicationState getIntConfigFromDic:@"withdraw"
//                                                   andKey:@"multiple_withdraw_money"
//                                               andDefault:WithDrawMultipleDefault];
//}
//
//- (BOOL)validInputData {
//    int totalValue = [[self.inputCell.textFieldMoney.textField.text onlyDigit] intValue];
//    if (totalValue > [ZPWalletManager sharedInstance].currentBalance) {
//        [self.inputCell.textFieldMoney showErrorWithText:[R string_Withdraw_NotEnoughMoney]];
//        return FALSE;
//    }
//    return [super validInputData];
//}
//
//- (BOOL)isGreaterMaxValue:(int)totalValue {
//    if (totalValue > self.max) {
//        NSString *money = [@(self.max) formatCurrency];
//        NSString *message = [NSString stringWithFormat:[R string_InputMoney_BigAmount_Error_Message], money];
//        [self.inputCell.textFieldMoney showErrorWithText:message];
//        //[self logInputBigValue:@(totalValue)];
//        return true;
//    }
//    return false;
//}
//
//- (BOOL)validRechagerMoneyValue:(int)totalValue {
//    if (totalValue % self.multiple != 0) {
//        [self.inputCell.textFieldMoney showErrorWithText:[R string_Recharge_Condition]];
//        return FALSE;
//    }
//    return TRUE;
//}
//
//- (void)requestCreateOrder {
//    int totalValue = [[self.inputCell.textFieldMoney.textField.text onlyDigit] intValue];
//    [self showLoadingView];
//    self.buttonConfirm.userInteractionEnabled = NO;
//    [self.inputCell.textFieldMoney.textField resignFirstResponder];
//    self.inputCell.textFieldMoney.textField.userInteractionEnabled = NO;
//    
//    [self updateConfirmButton];
//    
//    @weakify(self);
//    NSDictionary *item = @{ZPCreateWalletOrderTranstypeKey: [NSNumber numberWithInteger: ZPTransTypeWithDraw]};
//    NSString *itemStr = [item JSONRepresentation];
//    [[[[NetworkManager sharedInstance] getWalletOrdederWithAmount:totalValue
//                                               andTransactionType:ZPTransTypeWithDraw
//                                                          appUser:[NetworkManager sharedInstance].paymentUserId
//                                                      description:[R string_Withdraw_Description_Message]
//                                                            appId:withdrawAppId
//                                                        embeddata:@""
//                                                             item:itemStr]
//      deliverOnMainThread] subscribeNext:^(NSDictionary *json) {
//        @strongify(self);
//        ZPBill *billInfo = [ZPBill billWithData:json
//                                      transType:ZPTransTypeWithDraw
//                                    orderSource:OrderSource_None
//                                          appId:withdrawAppId];
//        [self showBillViewController:billInfo];
//        self.buttonConfirm.userInteractionEnabled = YES;
//        [self updateConfirmButton];
//        self.inputCell.textFieldMoney.textField.userInteractionEnabled = YES;
//    } error:^(NSError *error) {
//        @strongify(self);
//        [ZPDialogView showDialogWithError:error handle:^{
//            @strongify(self);
//            [self.inputCell.textFieldMoney.textField becomeFirstResponder];
//        }];
//        [self hideLoadingView];
//        self.buttonConfirm.userInteractionEnabled = YES;
//        [self updateConfirmButton];
//        self.inputCell.textFieldMoney.textField.userInteractionEnabled = YES;
//    }];
//}
//
//- (void)updateConfirmButton {
//    NSIndexPath *indexPathLast = [NSIndexPath indexPathForRow:[self.tableView numberOfRowsInSection:0] - 1 inSection:0];
//    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPathLast];
//    if ([cell isKindOfClass:[ZPRechargeButtonBottomCell class]]){
//        [self.tableView reloadRowsAtIndexPaths:@[indexPathLast] withRowAnimation:UITableViewRowAnimationNone];
//    }
//}
//
//- (void)keyboardWillShow:(NSNotification *)notification {
//    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.height) + 5 , 0.0);
//    self.tableView.contentInset = contentInsets;
//    NSIndexPath *indexPathLast = [NSIndexPath indexPathForRow:[self.tableView numberOfRowsInSection:0] - 1 inSection:0];
//    [self.tableView scrollToRowAtIndexPath:indexPathLast atScrollPosition:UITableViewScrollPositionTop animated:YES];
//}
//
//- (void)keyboardWillHide:(NSNotification *)notification{
//    self.tableView.contentInset = UIEdgeInsetsZero;
//    self.tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
//}
//
//#pragma mark - TableView
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return self.dataSource.count;
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    ZPRechargeBaseItem *item = [self.dataSource safeObjectAtIndex:indexPath.row];
//    ZPRechargeBaseCell *cell = [item.cellClass cellFromSameNibForTableView:tableView];
//    [cell setItem:item];
//    
//    if ([cell isKindOfClass:[ZPRechargeInputCell class]]) {
//        self.inputCell = (ZPRechargeInputCell *)cell;
//        self.inputCell.inputDelegate = self;
//        [self.inputCell.textFieldMoney becomeFirstResponder];
//        [super configTextFormat];
//    }
//    
//    if ([cell isKindOfClass:[ZPRechargeMinValueTitleCell class]]) {
//        ZPRechargeMinValueTitleCell *titleCell = (ZPRechargeMinValueTitleCell *)cell;
//        titleCell.labelvnd.text = @"";
//        titleCell.labelMinvalue.textAlignment = NSTextAlignmentLeft;
//        
//        NSString *min = [[@(self.min) stringValue] formatCurrency];
//        NSString *multiple = [[@(self.multiple) stringValue] formatCurrency];
//        titleCell.labelMinvalue.text = [NSString stringWithFormat:[R string_Recharge_Min_Multiple_Value],min,multiple];
//        titleCell.m_trailling_contraint.constant = 10;
//        titleCell.seperater.hidden = true;
//    }
//    
//    if ([cell isKindOfClass:[ZPRechargeButtonBottomCell class]]) {
//        ZPRechargeButtonBottomCell *buttonCell = (ZPRechargeButtonBottomCell *)cell;
//        [buttonCell.buttonConfirm addTarget:self action:@selector(confirmButtonClick:) forControlEvents:UIControlEventTouchUpInside];
//        buttonCell.seperater.hidden = true;
//        
//        if (self.buttonConfirm.isEnabled) {
//            buttonCell.buttonConfirm.userInteractionEnabled = true;
//            buttonCell.buttonConfirm.enabled = true;
//            [buttonCell.buttonConfirm setBackgroundColor:[UIColor zaloBaseColor] forState:UIControlStateNormal];
//        } else {
//            buttonCell.buttonConfirm.userInteractionEnabled = false;
//            buttonCell.buttonConfirm.enabled = false;
//            [buttonCell.buttonConfirm setBackgroundColor:[UIColor subText] forState:UIControlStateNormal];
//        }
//    }
//    return cell;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    ZPRechargeBaseItem *item = [self.dataSource safeObjectAtIndex:indexPath.row];
//    return [item.cellClass height] + (( UIScreen.mainScreen.bounds.size.width <= 320 && [item isKindOfClass:[ZPRecharMinvalueItem class]] ) ? 20 : 0);
//}
//
//#pragma mark - InputCellDelegate
//- (void)inputCellDidDoneInput {
//    [super confirmButtonClick:nil];
//}
//
//- (BOOL)textFieldShouldChange:(ZPRechargeInputCell *)sender text:(NSString *)text {
//    int totalValue =  [[[NSString stringWithFormat:@"%@%@",self.inputCell.textFieldMoney.textField.text,text] onlyDigit] intValue];
//    return ![self isGreaterMaxValue:totalValue];
//}
//@end

