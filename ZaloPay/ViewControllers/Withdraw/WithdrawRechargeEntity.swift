////
////  WithdrawRechargeEntity.swift
////  ZaloPay
////
////  Created by Nguyen Tuong Vi on 10/18/17.
////  Copyright © 2017 VNG Corporation. All rights reserved.
////
//
//import Foundation
//import UIKit
//
//enum WithdrawRechargeType: Int {
//    case MinValueItem  = 0
//    case InputItem  = 1
//    case HeaderItem = 2
//    case ButtonItem = 3
//}
//
//class ZPWithdrawRechargeEntity: NSObject {
//    var title: String = ""
//    var sectionDescription: String = ""
//    var value: Bool = false
//    var type: WithdrawRechargeType!
//    
//    init(title: String, sectionDescription: String, value : Bool, type: WithdrawRechargeType ) {
//        self.title = title
//        self.sectionDescription = sectionDescription
//        self.value = value
//        self.type = type
//    }
//    
//    func getCellClass() -> AnyClass {
//        return ZPTransferBaseCell.self
//    }
//}

