//
//  ZPWithdrawCollectionViewCell.swift
//  ZaloPay
//
//  Created by Quang Huy on 6/19/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit

class ZPWithdrawCollectionViewCell: UICollectionViewCell {
//MARK: Custom variables
    private let borderWidth: CGFloat = 0.5
    private let cornerRadius: CGFloat = 5.0
    private let arrowIconFont = "general_arrowright"
    private let textColorNotEnoughHexString = "#C0C3C5"
    private let bgColorNotEnoughHexString = "#F7F9FA"
    private let borderColorNotEnoughHexString = "#77C2EF"
    private let arrowColorNotEnoughHexString = "#B7DDF5"
//MARK: Outlets
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var withdrawMoneyNumberLabel: UILabel!
    @IBOutlet weak var arrowRightImgView: ZPIconFontImageView!
    @IBOutlet weak var centerLabelConstraint: NSLayoutConstraint!
    @IBOutlet weak var arrowImgViewWidthConstraint: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(withdrawNumber: Int, isValidBallance: Bool) {
        
        if withdrawNumber == Int(WithDrawNumberOther) {
            self.withdrawMoneyNumberLabel.text = R.string_Withdraw_Other_Number()
        }
        else {
            self.withdrawMoneyNumberLabel.text = NSNumber(integerLiteral: withdrawNumber).formatMoneyValue()
        }
        self.arrowRightImgView.setIconFont(arrowIconFont)
        self.withdrawMoneyNumberLabel.textAlignment = .center
        self.contentView.layer.borderWidth = borderWidth
        self.contentView.layer.cornerRadius = cornerRadius
        self.contentView.clipsToBounds = true

        if isValidBallance == false {
            self.contentView.backgroundColor = UIColor(fromHexString: bgColorNotEnoughHexString)
            self.arrowRightImgView.setIconColor(UIColor(fromHexString: arrowColorNotEnoughHexString))
            self.withdrawMoneyNumberLabel.textColor = UIColor(fromHexString: textColorNotEnoughHexString)
            self.contentView.layer.borderColor = UIColor(fromHexString: borderColorNotEnoughHexString)?.cgColor
        } else {
            self.arrowRightImgView.setIconColor(UIColor.zaloBase())
            self.contentView.layer.borderColor = UIColor.zaloBase().cgColor
            self.contentView.backgroundColor = UIColor.white
            self.withdrawMoneyNumberLabel.textColor = UIColor.black
        }
        
        if UIScreen.main.bounds.height <= 568.0 {
            // IPhone 5 or smaller
            withdrawMoneyNumberLabel.font = withdrawMoneyNumberLabel.font.withSize(20.0)
        } else {
            self.arrowImgViewWidthConstraint.constant = 14.0
            self.layoutIfNeeded()
        }
    }
}
