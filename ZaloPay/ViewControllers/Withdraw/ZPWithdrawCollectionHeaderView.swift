//
//  ZPWithdrawCollectionHeaderView.swift
//  ZaloPay
//
//  Created by Quang Huy on 6/19/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

class ZPWithdrawCollectionHeaderView: UICollectionReusableView {

//MARK: Outlets
    @IBOutlet weak var headerLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
