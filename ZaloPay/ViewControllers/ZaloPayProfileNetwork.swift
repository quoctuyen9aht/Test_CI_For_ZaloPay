//
//  ZaloPayProfileNetwork.swift
//  ZaloPay
//
//  Created by Bon Bon on 6/14/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayProfile

class ZaloPayProfileNetwork: ZPProfileNetworkProtocol {
    
    func getCurrenUserProfile() -> RACSignal<AnyObject> {
        return NetworkManager.sharedInstance().getCurrenUserProfile()
    }
    
    func getListZaloPayId(_ uids: Array<String>) -> RACSignal<AnyObject> {
        return NetworkManager.sharedInstance().getListZaloPayId(uids)
    }
    
    func getListZaloPayInfo(fromPhones: Array<String>) -> RACSignal<AnyObject> {
        return NetworkManager.sharedInstance().getListZaloPayInfo(fromPhones: fromPhones)
    }

}
