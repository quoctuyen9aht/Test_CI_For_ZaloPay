//
//  ZPLoginInteractor.swift
//  ZaloPay
//
//  Created by bonnpv on 4/8/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayAnalyticsSwift
import RxCocoa
import RxSwift
import ZaloPayProfile

class ZPLoginInteractor: ZPLoginInteractorProtocol {
    weak var presenter: ZPLoginInteractorCallBackProtocol?
    private let disposeBag = DisposeBag()
    
    func validateLoginState() -> Bool {
        if LoginManagerSwift.sharedInstance.isForceUpdateApp() {
            LoginManagerSwift.sharedInstance.alertForceUpdateApp()
            return false
        }
        if NetworkState.sharedInstance().isReachable == false {
            self.presenter?.networkNotConnected()
            return false
        }
        return true
    }
    
    func startLogin(from: UIViewController) {
        if !validateLoginState() {
            return
        }
        ZaloSDKApiWrapper.sharedInstance.login(withZalo: from, type: typeLogin).subscribe(onNext: { [weak self] (result) in
            let json = result as NSDictionary
            self?.loginWithZaloSession(json: json, from: from)
            }, onError: { [weak self] (error) in
                ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.login_fail)
                if (error.errorCode() == Int(kZaloSDKErrorCodeRequestCanceled.rawValue) ||
                    error.errorCode() == Int(kZaloSDKErrorCodeUserCancel.rawValue)) {
                    return;
                }
                self?.presenter?.loginZaloFail()
        }).disposed(by: disposeBag)
    }
    
    func loginWithZaloSession(json: NSDictionary, from: UIViewController)  {
        let zaloId = json.string(forKey: "uid")
        let oauth = json.string(forKey: "oauthCode")
        self.presenter?.startRequestAccessToken()
        let delay = from.presentedViewController != nil ? 0.7 : 0.0;
        // login web -> delay 0.7s
        NetworkManager.sharedInstance().createAccesssZaloOauthCode(oauth, andZaloUserId: zaloId).delay(delay).deliverOnMainThread().subscribeNext({ [weak self](result) in
            self?.presenter?.doneRequestAccessToken()
            let json = result as! NSDictionary
            self?.presenter?.loginSuccess(json: json)
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.login_success)
        }, error:({[weak self] (error) in
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.login_fail)
            DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                self?.handleErrorResponse(error: error as NSError?)
            }
        }))
    }
    
    func handleErrorResponse(error: NSError?) {
        self.presenter?.doneRequestAccessToken()
        let code = error?.errorCode() ?? 0
        if code == Int(ZALOPAY_ERRORCODE_UPDATE_PHONE.rawValue) {
            self.clearCache()
            self.presenter?.showInputPhone()
            return
        }
        self.presenter?.loginFail(error: error ?? NSError())
    }
    
    //note phần clear cache fix cho case:
    //user level 2 nâng cấp level 3 lúc đó trong DB đã đánh dấu là done upload dữ liệu.
    //sau khi QC dùng tool reset để đi lại flow onboading -> không thể nâng cấp level 3 được nữa do đã đánh dấu cờ trong DB.
    //Xoá DB user cũ
    
    func clearCache() {
        if let cacheTable = ZPCacheDataTable(db: GlobalDatabase.sharedInstance()),
           let oldUid = cacheTable.cacheDataValue(forKey: kLastLoginUser),
           let path = PerUserDataBase.sharedInstance().databasePath(withUid: oldUid) {
            try? FileManager.default.removeItem(atPath: path)
        }
    }
   
    func startRegister(from: UIViewController) {
        if !validateLoginState() {
            return
        }
        ZaloSDKApiWrapper.sharedInstance.register(withZalo: from).subscribe(onNext: { [weak self] (result) in
            let json = result as NSDictionary
            self?.loginWithZaloSession(json: json, from: from)
            }, onError: { [weak self] (error) in
                ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.login_fail)
                if (error.errorCode() == Int(kZaloSDKErrorCodeRequestCanceled.rawValue) ||
                    error.errorCode() == Int(kZaloSDKErrorCodeUserCancel.rawValue)) {
                    return;
                }
                self?.presenter?.loginZaloFail()
        }).disposed(by: disposeBag)
    }
}
