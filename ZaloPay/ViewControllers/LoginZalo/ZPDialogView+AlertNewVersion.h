//
//  ZPDialogView+AlertNewVersion.h
//  ZaloPay
//
//  Created by bonnpv on 9/26/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

//#import "ZPDialogView.h"

@interface ZPDialogView (AlertNewVersion)

- (id)initWithNewVersion:(NSString *)version
                 message:(NSString *)message
                  handle:(void (^)(void)) handle;
@end
