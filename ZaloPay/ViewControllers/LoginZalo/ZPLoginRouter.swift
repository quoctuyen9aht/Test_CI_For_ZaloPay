//
//  ZPLoginRouter.swift
//  ZaloPay
//
//  Created by bonnpv on 4/8/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import Foundation
import UIKit
import FirebaseRemoteConfig



class ZPLoginRouter: ZPLoginRouterProtocol {
    
    var completionHandler: ((NSDictionary)->Void)?
    
    private lazy var remoteConfig: RemoteConfig = {
       let nConfig = RemoteConfig.remoteConfig()
        let developerModeEnabled: Bool
        #if DEBUG
            developerModeEnabled = true
        #else
            developerModeEnabled = false
        #endif
        let remoteConfigSettings = RemoteConfigSettings(developerModeEnabled: developerModeEnabled)
        nConfig.configSettings = remoteConfigSettings
        return nConfig
    }()
    
    private lazy var expriationDuration: TimeInterval = {
        let time: TimeInterval
        #if DEBUG
            time = 0
        #else
            time = 600
        #endif
        return time
    }()
    
    static let experimentOnboardingConfigKey = "experiment_onboarding"
    
    func showInputPhone() {
        remoteConfig.fetch(withExpirationDuration: TimeInterval(expriationDuration)) { [weak self] (status, error) in
            if let error = error {
                print("Error fetching config: \(error)")
            }
           
            var isRunKYC: Bool = false
            defer {
                self?.setAppearance(isRunKYC)
            }
            
            guard error == nil else {
                return
            }
            self?.remoteConfig.activateFetched()
            let configValue = self?.remoteConfig[ZPLoginRouter.experimentOnboardingConfigKey]
            isRunKYC = configValue?.stringValue == "0" ? false : true
        }
 
    }
    
    private func setAppearance(_ isRunKYC : Bool) {
        let storyboard = UIStoryboard(name: "Onboarding", bundle: nil)
        let navVC = isRunKYC ? storyboard.instantiateViewController(withIdentifier: "navKYCOnboarding") :
            storyboard.instantiateInitialViewController()
        guard let navi = navVC as? ZPNavigationCustomStatusViewController else {
            return
        }
        navi.completionHandler = self.completionHandler
        UIApplication.shared.delegate?.window??.rootViewController = navi
    }
    

}
 

