//
//  NetworkManager+Login.h
//  ZaloPay
//
//  Created by bonnpv on 4/25/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//


@interface NetworkManager (Accesstoken)
- (RACSignal *)createAccesssZaloOauthCode:(NSString *)zaloAuthenCode andZaloUserId:(NSString *)zaloUserId;
- (RACSignal *)removeAccesssToken;
- (RACSignal *)verifyAccesssToken;
- (RACSignal *)submitInviteCode:(NSString *)code;
@end
