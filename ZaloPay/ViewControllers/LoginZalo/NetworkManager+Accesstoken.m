//
//  NetworkManager+Login.m
//  ZaloPay
//
//  Created by bonnpv on 4/25/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "NetworkManager+Accesstoken.h"
#import <ZaloPayNetwork/ZPNetWorkApi.h>
@implementation NetworkManager (Accesstoken)

- (RACSignal *)createAccesssZaloOauthCode:(NSString *)zaloAuthenCode andZaloUserId:(NSString *)zaloUserId {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:zaloAuthenCode forKey:@"oauthcode"];
    [params setObjectCheckNil:zaloUserId forKey:@"zaloid"];//@"loginuid"];
    return [self postRequestWithPath:api_um_loginviazalo parameters:params];
}

- (RACSignal *)removeAccesssToken{
    return [self requestWithPath:api_um_removeaccesstoken parameters:nil];
}

- (RACSignal *)verifyAccesssToken {
    return [self requestWithPath:api_um_verifyaccesstoken parameters:nil];
}

- (RACSignal *)submitInviteCode:(NSString *)code {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:code forKey:@"codetest"];
    return [self requestWithPath:api_um_verifycodetest parameters:params];
}

@end
