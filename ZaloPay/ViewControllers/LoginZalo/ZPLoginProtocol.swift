//
//  ZPLoginProtocol.swift
//  ZaloPay
//
//  Created by bonnpv on 4/8/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import Foundation
import UIKit

protocol ZPLoginViewProtocol: class {
    var presenter: ZPLoginPresenterProtocol? {get set}
    
    func startLoading()
    func endLoading()
    
    func alertNoNetwork()
    func alertLoginZaloFail()
    func alertLoginFail(error: NSError)
}

protocol ZPLoginPresenterProtocol: class {
    var view: ZPLoginViewProtocol? {get set}
    var interactor: ZPLoginInteractorProtocol? {get set}
    var router: ZPLoginRouterProtocol? {get set}
    
    func startLogin()
    func startRegister()
}

protocol ZPLoginRouterProtocol: class {
    var completionHandler: ((NSDictionary)->Void)? {get set}
    func showInputPhone()
}

protocol ZPLoginInteractorProtocol: class {
    var presenter: ZPLoginInteractorCallBackProtocol? {get set}
    func startLogin(from: UIViewController);
    func startRegister(from: UIViewController);
}

protocol ZPLoginInteractorCallBackProtocol: class {
    func networkNotConnected()
    func loginZaloFail()
    
    func startRequestAccessToken()
    func doneRequestAccessToken()
    func loginSuccess(json: NSDictionary)
    func loginFail(error: NSError)
    func showInputPhone()
}

