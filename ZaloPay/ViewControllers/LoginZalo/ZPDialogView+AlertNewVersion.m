//
//  ZPDialogView+AlertNewVersion.m
//  ZaloPay
//
//  Created by bonnpv on 9/26/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPDialogView+AlertNewVersion.h"

@interface ZPDialogView(AlertNewVersionInternal)
@property (nonatomic, strong) UILabel     *titleLabel;
@property (nonatomic, strong) UILabel     *detailLabel;
@property (nonatomic, strong) UITextField *inputView;
@property (nonatomic, strong) UIView      *buttonView;
@property (nonatomic, strong) NSArray     *actionItems;
@property (nonatomic, strong) UILabel     *describeLabel;
@property (nonatomic, strong) ZPIconFontImageView *imageView;
@property (nonatomic, assign) DialogType dialogType;
@end

@implementation ZPDialogView (AlertNewVersion)

- (id)initWithNewVersion:(NSString *)version
                 message:(NSString *)message
                  handle:(void (^)(void)) handle {
    
    NSMutableArray *items = [NSMutableArray array];
    [items addObjectNotNil:MMNormalItemMake([R string_ButtonLabel_Close], nil)];
    [items addObjectNotNil:MMBoldItemMake([R string_ButtonLabel_Upgrade], ^(NSInteger index) {
        handle();
    })];
    self = [self initWithType:DialogTypeNotification
                      message:message
                        items:items];
    
    if (self) {
        [self.imageView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(30);
        }];
        UILabel *label = [[UILabel alloc] init];
        [self addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.titleLabel);
            make.width.greaterThanOrEqualTo(50);
            make.height.equalTo(self.titleLabel);
            make.top.equalTo(self.titleLabel);
        }];
        label.text = [NSString stringWithFormat:@"V%@",version];
        [label setFont:[UIFont SFUITextMediumWithSize:16]];
        [label setTextColor:[UIColor zaloBaseColor]];
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.describeLabel.textAlignment = NSTextAlignmentLeft;
    }
    return self;
}

@end
