//
//  ZPLoginViewController.swift
//  ZaloPay
//
//  Created by bonnpv on 4/8/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit
import ZaloPayAnalyticsSwift

class ZPConstraints{
    public static var backgroundImageName: String {
        return UIView.isIPhoneX() ? "launchScreenBackgroundForIPhoneX" : "launchScreenBackground"
    }
    public static var backgroundTopOffset: CGFloat {
        return UIView.isIPhoneX() ? 33 : 0
    }
    //-----------------------------------------------------
    public static let logoRatioSize: CGSize = CGSize(width: 0.6281, height: 0.2873)
    public static var logoCenterYOffset: CGFloat {
        return UIView.isIPhoneX() ? -10 : -30
    }
    //-----------------------------------------------------
    public static let splashTextRatioSize : CGSize = CGSize(width: 0.48, height: 0.0981)
    public static let splashTextTopOffset: CGFloat = 23
    //-----------------------------------------------------
    public static var loginButtonRoundRect: Float {
        return (UIScreen.main.scale == 2) ? 5.0 : 7.0
    }
    public static var loginButtonSize: CGSize {
        let is2x = UIScreen.main.scale == 2
        return CGSize(width: (is2x ? 226 : 269), height: (is2x ? 44 : 54))
    }
    public static var loginButtonBottomOffset: CGFloat {
        return UIView.isIPhoneX() ? -113 : -93
    }
    public static var loginButtonEdgeInset: UIEdgeInsets{
        return UIView.isScreen2x() ? UIEdgeInsetsMake(2, -25, 0, 0) : UIEdgeInsetsMake(2, -58, 0, 0)
    }
    
    public static var textBottomBottomOffset: CGFloat {
        return UIView.isIPhoneX() ? -30 : -40
    }
   
}

@objcMembers
class ZPLoginViewController: BaseViewController {
    var presenter: ZPLoginPresenterProtocol?
    var imageBackground: UIImageView?
    var imageLogo: UIImageView?
    var imageSplashText: UIImageView?
    var buttonLogin: UIButton?
    var lblDescription = MDHTMLLabel()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.addBackground()
        self.addLogo()
        self.addSplashText()
        self.addTextBottom()
        self.addLoginButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ZPTrackingHelper.shared().trackScreen(with: self)
    }
    
    override func screenName() -> String! {
        return ZPTrackerScreen.Screen_iOS_LoginZalo
    }
    
    deinit {
        self.lblDescription.delegate = nil
    }
    
    func addTextBottom() {
        lblDescription.textColor = UIColor(fromHexString: "#727f8c")
        lblDescription.font = UIFont.sfuiTextRegular(withSize: 14)
        lblDescription.numberOfLines = 0
        lblDescription.textAlignment = .center
        
        self.view.addSubview(lblDescription)
        lblDescription.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.view.snp.centerX)
            make.bottom.equalTo(self.view.snp.bottomMargin).offset(ZPConstraints.textBottomBottomOffset)
        }
        
        lblDescription.delegate = self
        lblDescription.htmlText = R.string_Login_Description()
    }
    
    func addBackground () {
        let img = UIImage(named: ZPConstraints.backgroundImageName)
        let imageBackground = UIImageView.init(image:img)
        self.view.addSubview(imageBackground)
        
        let widthBackground = UIScreen.main.bounds.width
        let heightBackground = widthBackground * (imageBackground.frame.size.height/imageBackground.frame.size.width)

        imageBackground.snp.makeConstraints({ (make) in
            make.centerX.equalTo(self.view.snp.centerX)
            make.top.equalTo(self.view.snp.top).offset(ZPConstraints.backgroundTopOffset)
            make.width.equalTo(widthBackground)
            make.height.equalTo(heightBackground)
        })
        self.imageBackground = imageBackground
    }
    
    func addLogo() {
        let imageLogo = UIImageView.init(image: UIImage.init(named: "loginLogo"))
        self.view.addSubview(imageLogo)
        
        let logoWidth = UIScreen.main.bounds.width * ZPConstraints.logoRatioSize.width
        let logoHeight = logoWidth * ZPConstraints.logoRatioSize.height
        let centerYOffset = ZPConstraints.logoCenterYOffset
        
        imageLogo.snp.makeConstraints { (make) in
            make.width.equalTo(logoWidth)
            make.height.equalTo(logoHeight)
            make.centerX.equalTo(self.imageBackground!.snp.centerX)
            make.centerY.equalTo(self.imageBackground!.snp.centerY).offset(centerYOffset)
        }
        self.imageLogo = imageLogo
    }
    
    func addSplashText()
    {
        let imageSplashText = UIImageView.init(image: UIImage.init(named: "splash_text"))
        
        let widthSplashText = UIScreen.main.bounds.width * ZPConstraints.splashTextRatioSize.width
        let heightSplashText = widthSplashText * ZPConstraints.splashTextRatioSize.height
        
        
        self.view.addSubview(imageSplashText)
        imageSplashText.snp.makeConstraints { (make) in
            make.width.equalTo(widthSplashText)
            make.height.equalTo(heightSplashText)
            make.centerX.equalTo(self.imageBackground!.snp.centerX)
            make.top.equalTo(self.imageLogo!.snp.bottom).offset(ZPConstraints.splashTextTopOffset)
        }
        self.imageSplashText = imageSplashText
    }
    
    func addLoginButton() {
        let buttonLogin = UIButton.init()
        self.view.addSubview(buttonLogin);
        buttonLogin.setTitle(R.string_Login_Button_Title(), for: .normal)
        
        let roundRect = ZPConstraints.loginButtonRoundRect
        buttonLogin.zp_roundRect(roundRect)
        buttonLogin.setBackgroundColor(UIColor.zaloBase(), for: .normal)
        buttonLogin.setImage(UIImage.init(named:"loginButtonIcon"), for: .normal)
        buttonLogin.titleLabel?.font = UIFont.sfuiTextRegular(withSize: 17.0)
        buttonLogin.imageEdgeInsets = ZPConstraints.loginButtonEdgeInset;
        
        let bottomOffset = ZPConstraints.loginButtonBottomOffset
        
        buttonLogin.snp.makeConstraints { (make) in
            make.size.equalTo(ZPConstraints.loginButtonSize)
            make.centerX.equalTo(self.view.snp.centerX)
            make.bottom.equalTo(lblDescription.snp.top).offset(bottomOffset)
        }
        
        buttonLogin.addTarget(self, action: #selector(loginButtonClick), for: .touchUpInside)
        self.buttonLogin = buttonLogin
    }
    
    @objc func loginButtonClick()  {
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.login_touch)
        self.presenter?.startLogin()
    }
}

extension ZPLoginViewController: MDHTMLLabelDelegate {
    func htmlLabel(_ label: MDHTMLLabel!, didSelectLinkWith URL: URL!) {
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.login_touch_register)
        self.presenter?.startRegister()
    }
}

extension ZPLoginViewController {
    class func loginViewWihtCompleteHande(_ handle:@escaping (_ json: NSDictionary) -> ()) -> ZPLoginViewController {
        
        let view = ZPLoginViewController()
        let router = ZPLoginRouter()
        let interactor = ZPLoginInteractor()
        let presenter = ZPLoginPresenter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        router.completionHandler = handle
        return view
    }
}

extension ZPLoginViewController: ZPLoginViewProtocol {
    
    func startLoading() {
        ZPAppFactory.sharedInstance().showHUDAdded(to: self.view)
    }
    
    func endLoading() {
        ZPAppFactory.sharedInstance().hideAllHUDs(for: self.view)
    }
    
    func alertNoNetwork() {
        ZPDialogView.showDialog(with:DialogTypeNoInternet,
                                title: R.string_Dialog_Warning(),
                                message: R.string_NetworkError_NoConnectionMessage() ,
                                buttonTitles: [R.string_ButtonLabel_Close()],
                                handler: nil)
    }
    
    func alertLoginZaloFail() {        
        ZPDialogView.showDialog(with: DialogTypeError,
                                message: R.string_Login_Zalo_Error(),
                                cancelButtonTitle: R.string_ButtonLabel_Close(),
                                otherButtonTitle: nil,
                                completeHandle: nil)
        
    }
    
    func alertLoginFail(error: NSError) {
        if error.errorCode() == ZALOPAY_ERRORCODE_USER_IS_LOCKED.rawValue {
            let message = R.string_Clearing_account_locked()
            ZPDialogView.showDialog(with: DialogTypeNotification, title: nil , message: message , buttonTitles: [R.string_ButtonLabel_Close()]) { (idx) in
                if (idx == 0) {
                    LoginManagerSwift.sharedInstance.logout()
                }
            }
            return
        }
        ZPDialogView.showDialogWithError(error, handle: nil)
    }
}
