//
//  LoginManager.swift
//  ZaloPay
//
//  Created by nhatnt on 12/01/2018.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import CocoaLumberjackSwift
import NetworkExtension
import RxSwift
import SDWebImage
import StoreKit
import ZaloPayAnalyticsSwift
import ZaloPayProfile
import ZaloPayConfig

@objcMembers
public class LoginManagerSwift: NSObject {
    static let sharedInstance = LoginManagerSwift()
    private lazy var cacheDataTable: ZPCacheDataTable = ZPCacheDataTable(db: GlobalDatabase.sharedInstance())

    func appDelegate() -> ZPUIAppDelegate {
        return ZPUIAppDelegate.sharedInstance
    }
    
    func handleLoginData(loginDic: Dictionary<AnyHashable, Any>) {
        DDLogInfo("login Dic :\(loginDic)")
        if loginDic.count > 0, let loginDic = loginDic as? [String:Any] {
            let user = ZPUserLoginDataSwift.fromDic(loginDic)
            ZPProfileManager.shareInstance.userLoginData = user
            ZPProfileManager.shareInstance.saveUserLoginData()
            if let user = user {
                self.initializeWithValidSessionFromLoginView(fromLogin: true, user)
            }
            
        }
    }
    
    func initializeWithValidSessionFromLoginView(fromLogin: Bool,_ userLoginData: ZPUserLoginDataSwift) {
    
        ApplicationState.sharedInstance.state = .authenticated
        AppsFlyerTracker.shared().customerUserID = userLoginData.paymentUserId
        NetworkManager.sharedInstance().accesstoken = userLoginData.accesstoken
        NetworkManager.sharedInstance().paymentUserId = userLoginData.paymentUserId
        ZPAnalyticSwiftConfig.setUserId(with: userLoginData.paymentUserId)
        ZPAnalyticSwiftConfig.setAccessToken(with: userLoginData.accesstoken)
        let eName = ZPTrackerEvents.eventActionFromId(ZPAnalyticEventAction.login_success)
        ZPSDKHelper().trackAppsflyer(eName, eventValues: [:])
        // Track open app
        let trackerOpenApp = (UIApplication.shared.delegate as? ZPAppDelegate)?.trackerOpenApp
        trackerOpenApp?.trackAppIcon()
        
        self.appDelegate().startBackgroudProcessWhenLoginSuccessful()
        let oldUid = getLastLoginUserId()
        let uid = userLoginData.paymentUserId
        let isNewDB = PerUserDataBase.sharedInstance().createDB(withUser: uid, oldUser: oldUid)
        cacheDataTable.cacheDataUpdateValue(uid, forKey: kLastLoginUser)

        if fromLogin || isNewDB {
            ZPNotificationService.sharedInstance().checkToLoadRecoveryMessage()
            ReactNativeAppManager.sharedInstance().clearMerchantUserInfo()
        }

        if uid != oldUid {
            ZPTouchIDService.sharedInstance.removePassword(oldUid)
        }
        
        ZPAnalyticAppdelegate.sharedInstance.setLoginUserOnCrashlytics(["uid": userLoginData.paymentUserId])
        self.configZaloPayWalletSDK()
        ZPWalletManagerSwift.sharedInstance.configDefaultValue()
        ZPWalletManagerSwift.sharedInstance.updateBalanceValue()
        
        self.appDelegate().showMainViewController()
        
        ZPSettingsConfig.loadUserSetting(uid)
        ZPSchemeUrlHandle.sharedInstance.handleMissingScheme()
        DispatchQueue.global(qos: .background).async {
            self.connectToWebsocket()
            // TODO: - Upload log session
            ZPTrackingHelper.shared().eventTracker?.uploadLogSession()
            ZPProfileManager.shareInstance.loadZaloUserProfile(usingCache: false).subscribeCompleted{}
        }
        self.delayLoadData();
        ZPLocationManagerSwift.share.startUpdating()
        ZPNotificationAppDelegate.sharedInstance.registerPushNotify()
        
        if fromLogin.not {
            ZPSecurityAppDelegate.sharedInstance.showAuthentication()
        }        
    }
    
    func getLastLoginUserId() -> String {
        return cacheDataTable.cacheDataValue(forKey: kLastLoginUser) ?? ""
    }
    
    func configZaloPayProfile() {
        ZPProfileManager.shareInstance.enableMergeName = ZPApplicationConfig.getZPCAppConfig()?.getEnableMergeContactName().toBool(defaultValue: false)
        ZPProfileManager.shareInstance.phonePattern = ZPApplicationConfig.getGeneralConfig()?.getPhoneFormat()?.getPatterns() ?? [String]()
        ZPProfileManager.shareInstance.network = ZaloPayProfileNetwork()
    }
    
    func handleUserLoginData(_ userLoginData: ZPUserLoginDataSwift) {
        ZPProfileManager.shareInstance.userLoginData = userLoginData
        ZPProfileManager.shareInstance.saveUserLoginData()
        ApplicationState.sharedInstance.state = .authenticated
        NetworkManager.sharedInstance().accesstoken = userLoginData.accesstoken
        NetworkManager.sharedInstance().paymentUserId = userLoginData.paymentUserId
        
        self.appDelegate().startBackgroudProcessWhenLoginSuccessful()
        let oldUid = getLastLoginUserId()
        let uid = userLoginData.paymentUserId
        let isNewDB = PerUserDataBase.sharedInstance().createDB(withUser: uid, oldUser: oldUid)
        cacheDataTable.cacheDataUpdateValue(uid, forKey: kLastLoginUser)
        
        if isNewDB {
            ZPNotificationService.sharedInstance().checkToLoadRecoveryMessage()
            ReactNativeAppManager.sharedInstance().clearMerchantUserInfo()
        }
        
        ZPAnalyticAppdelegate.sharedInstance.setLoginUserOnCrashlytics(["uid": userLoginData.paymentUserId])
        self.configZaloPayWalletSDK()
        ZPWalletManagerSwift.sharedInstance.configDefaultValue()
        ZPWalletManagerSwift.sharedInstance.updateBalanceValue()
        ZPSchemeUrlHandle.sharedInstance.handleMissingScheme()
        _ = SerialDispatchQueueScheduler(qos: .background).schedule(true) { (_) -> Disposable in
            self.connectToWebsocket()
            return Disposables.create()
        }
        
        ZPProfileManager.shareInstance.loadZaloUserProfile(usingCache: false).subscribeCompleted{}
        self.delayLoadData();
        ZPLocationManagerSwift.share.startUpdating()
        ZPNotificationAppDelegate.sharedInstance.registerPushNotify()
    }
    
    func start() {
        self.configZaloPayProfile()
        let oldUid = getLastLoginUserId()
        
        if oldUid.isEmpty {
            // case user xoá app cài lại hoặc lần đầu run app.
            // xoá logindata do được lưu vào keychain.
            self.moveToLoginState()
            return
        }
        
        guard let user = ZPProfileManager.shareInstance.loginDataFromCache() else {
            self.moveToLoginState()
            return
        }
        
        if oldUid != user.paymentUserId {
            // case QC test 3.7.0 có data trong keychain sau đó xoá app chuyển 3.6.0 login account khác rồi update lên 3.7.0
            self.moveToLoginState()
            return
        }
        
        if user.isExpire() || (user.profilelevel) <= 1 {
            self.moveToLoginState()
            return
        }
        ZPProfileManager.shareInstance.userLoginData = user
        self.initializeWithValidSessionFromLoginView(fromLogin: false, user)
    }
    
    func delayLoadData() {
        _ = Observable<Int>.just(5).delay(5, scheduler: SerialDispatchQueueScheduler(qos: .background)).subscribe(onNext: { (_) in
            ZPProfileManager.shareInstance.loadZaloUserFriendList(forceSyncContact: false).subscribeNext({ (friends) in })
            ZPWalletManagerSwift.sharedInstance.loadAppInfo().subscribeCompleted{}
            ZPWalletManagerSwift.sharedInstance.writeAllAppTransIdLog()
        })
    }

    func configZaloPayWalletSDK() {
        let key = self.zaloPayWalletSDKCacheKey()
        let hasValidResource = cacheDataTable.cacheDataValue(forKey: key)?.toBool() ?? false
        if !hasValidResource {
            ZaloPayWalletSDKPayment.sharedInstance().clearPlatformInfoAndResource()
            cacheDataTable.cacheDataUpdateValue(true.toString(), forKey: key)
        }

        if let userLoginData = ZPProfileManager.shareInstance.userLoginData {
            let zaloPaySDK = ZaloPayWalletSDKPayment.sharedInstance() ?? ZaloPayWalletSDKPayment()
            
            zaloPaySDK.setUserProfileLevel(Int32(userLoginData.profilelevel)) //check nil profileLevel?
            zaloPaySDK.setUserPLP(userLoginData.profilelevelpermisssion)
            zaloPaySDK.network = NetworkManager.sharedInstance()
            zaloPaySDK.initializeSDK()
            
            zaloPaySDK.appDependence = ZPWalletManagerSwift.sharedInstance.sdkHelper
        }
        
    }

    func zaloPayWalletSDKCacheKey() -> String {
        guard let build = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String else {
            return  ""
        }
        guard let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String else {
            return  ""
        }
        return "zaloPayWalletSDKCacheKey_\(version)_\(build)"
    }

    func connectToWebsocket() {
        guard let userLoginData = ZPProfileManager.shareInstance.userLoginData else {
            return
        }
        let appleToken = cacheDataTable.cacheDataValue(forKey: kApplePushNotifyToken)
        
        let accessToken = userLoginData.accesstoken
        let paymentUserId = userLoginData.paymentUserId
        if accessToken.count > 0 && paymentUserId.count > 0 {
            let connectionManager = ZPConnectionManager.sharedInstance()
            connectionManager?.login(withId: paymentUserId, accesstoken: accessToken, applenotifyToken: appleToken)
        }
    }
    
    func moveToLoginState() {
        self.clearLoginData()
        if self.appDelegate().window?.rootViewController is ZPLoginViewController {
            return
        }
        self.showLoginViewController()
    }
    
    func clearLoginData() {
        ZPConnectionManager.sharedInstance().sendLogoutRequest()
        if ZaloPayWalletSDKPayment.sharedInstance().checkIsCurrentPaying() {
            ZaloPayWalletSDKPayment.sharedInstance().forceCloseSDK()
        }
        ZPAnalyticSwiftConfig.setUserId(with: nil)
        ApplicationState.sharedInstance.state = .anonymous
        ZPSecurityAppDelegate.sharedInstance.dimissAuthenticationView()
        ZPProfileManager.shareInstance.logout()
        ZaloSDKApiWrapper.sharedInstance.logout()
        NetworkManager.sharedInstance().cancelAllRequest()
        NetworkManager.sharedInstance().accesstoken = nil
        NetworkManager.sharedInstance().paymentUserId = nil
        ZPConnectionManager.sharedInstance().logout()
        ReactNativeAppManager.sharedInstance().clearMerchantUserInfo()
        ZPNotificationService.sharedInstance().logout()
        PerUserDataBase.sharedInstance().closeDataBase()
        ZPLocationManagerSwift.share.stopUpdating()
        ZaloPayWalletSDKPayment.sharedInstance().logout()
        ZPPaymentCodeModel.removeAllPaymentCodes()
        ZPWalletManagerSwift.sharedInstance.logout()
        ZPTouchIDService.sharedInstance.forceClose()
        ZPCheckPinViewController.forceClose()
        ZPGiftPopupManager.sharedInstance.logout()
    }
    
    func showLoginViewController() {
        let login = ZPLoginViewController.loginViewWihtCompleteHande { [weak self] (loginData) in
            let loginData = loginData as! Dictionary<String,Any>
            self?.handleLoginData(loginDic: loginData)
        }
        self.appDelegate().window?.rootViewController = login
    }
    
    func showDialogLogout(message: String) {
        var message = message
        if ZPProfileManager.shareInstance.userLoginData == nil {
            return
        }
        
        if message.count == 0 {
            message = R.string_Message_Session_Expire()
        }
        
        DispatchQueue.main.async { [message] in
            ZPAppFactory.sharedInstance().hideAllHUDs(for: nil)
            LoginManagerSwift.sharedInstance.moveToLoginState()
            ZPDialogView.dismissAllDialog()
            
            ZPDialogView.showDialog(with: DialogTypeNotification, message: message, cancelButtonTitle: R.string_ButtonLabel_OK(), otherButtonTitle: nil, completeHandle: nil)
        }
    }
    
    func logout() {
        if !NetworkState.sharedInstance().isReachable {
            ZPDialogView.showDialog(with: DialogTypeNoInternet,
                                    message: R.string_Home_InternetConnectionError(),
                                    cancelButtonTitle: R.string_ButtonLabel_OK(),
                                    otherButtonTitle: nil,
                                    completeHandle: nil)
            return
        }
        
        ZPAppFactory.sharedInstance().showHUDAdded(to: nil)
        NetworkManager.sharedInstance().removeAccesssToken()
            .deliverOnMainThread().subscribeError({ (error) in
            ZPAppFactory.sharedInstance().hideAllHUDs(for: nil)
            self.showAlertError(err: error)
        }) {
            ZPAppFactory.sharedInstance().hideAllHUDs(for: nil)
            SDWebImageDownloader.shared().cancelAllDownloads()
            UIApplication.shared.unregisterForRemoteNotifications()
            self.moveToLoginState()
        }
    }
    
    func showAlertError(err: Error?) {
        guard let error = err else {
            return
        }
        ZPDialogView.showDialogWithError(error, handle: nil)
    }
    
    //MARK: - Update Date
    func updateAccountNameToLoginData(accountName: String) {
        ZPProfileManager.shareInstance.userLoginData?.accountName = accountName
        ZPProfileManager.shareInstance.saveUserLoginData()
    }
    
    func updateAccessToken(accessToken: String) {
        ZPProfileManager.shareInstance.userLoginData?.accesstoken = accessToken
        ZPProfileManager.shareInstance.saveUserLoginData()
        self.connectToWebsocket()
    }
    
    func updateProfileLevel(profileLevel: Int32, permission: Array<Any>, accountName: String, phone: String) {
        guard let userLoginData = ZPProfileManager.shareInstance.userLoginData else {
            return
        }
        userLoginData.profilelevel = Int(profileLevel)
        userLoginData.profilelevelpermisssion = permission
        userLoginData.phoneNumber = phone
        userLoginData.accountName = accountName
        ZPProfileManager.shareInstance.saveUserLoginData()
        ZaloPayWalletSDKPayment.sharedInstance()
            .setUserProfileLevel(profileLevel)
        ZaloPayWalletSDKPayment.sharedInstance().setUserPLP(permission)
    }
    
    //MARK: - Force Update
    func alertForceUpdateApp() {
        let version = UserDefaults.standard.string(forKey: Force_Update_Appversion_Key) ?? ""
        self.alertForceUpdateApp(version: version)
    }
    
    func alertForceUpdateApp(version: String) {
        self.logout()
        let dialogView = ZPDialogView.init(upgradeVersion: version, message: R.string_Force_Update_App_Message()) {
            self.navigateToAppStore()
        }
        dialogView?.show()
    }

    
    func navigateToAppStore() {
        UIApplication.shared.openURL(URL.init(string: ZaloPayAppStoreUrl)!)
    }
    
    func currentVersion() ->String {
        guard let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String else {
            return  ""
        }
        return version
    }
    
    func forceUpdateApp(version: String) -> Bool {
        let currentVersion = self.currentVersion()
        
        if currentVersion.count == 0 || version.count == 0 {
            DDLogInfo("current version = \(currentVersion)")
            DDLogInfo("force version = \(currentVersion)")
            return false
        }
        
        if NSString.isEqualOrNewerThanVersion(currentVersion: currentVersion, checkVersion: version) {
            return false
        }
        
        UserDefaults.standard.set(version, forKey: Force_Update_Appversion_Key)
        DispatchQueue.main.async {
            self.alertForceUpdateApp(version: version)
        }
        return true
    }
    
    func productViewControllerDidFinish(viewController: SKStoreProductViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func isForceUpdateApp() -> Bool {
        let currentVersion = self.currentVersion()
        let versionToUpdate = UserDefaults.standard.string(forKey: Force_Update_Appversion_Key) ?? ""
        
        DDLogInfo("current version = \(currentVersion)")
        DDLogInfo("force update to version = \(versionToUpdate)")
        return NSString.isEqualOrNewerThanVersion(currentVersion: currentVersion, checkVersion: versionToUpdate).not
    }
    
    func alertAppHasNewVersion(version: String) {
        let currentVersion = self.currentVersion()
        let key = String(format: "done_alert_new_version_%@", version)
        if UserDefaults.standard.bool(forKey: key) {
            return
        }
        if NSString.isEqualOrNewerThanVersion(currentVersion: currentVersion, checkVersion: version) {
            return
        }
        
        UserDefaults.standard.set(true, forKey: key)
        UserDefaults.standard.synchronize()
        let dialog = ZPDialogView.init(newVersion: version, message: R.string_Notice_Update_App_Message()) {
            self.navigateToAppStore()
        }
        dialog?.show()
    }
}

