//
//  ZPLoginPresenter.swift
//  ZaloPay
//
//  Created by bonnpv on 4/8/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import Foundation

class ZPLoginPresenter: ZPLoginPresenterProtocol {
    weak var view: ZPLoginViewProtocol?
    var interactor: ZPLoginInteractorProtocol?
    var router: ZPLoginRouterProtocol?
    
    func startLogin() {
        if let controller = self.view as? UIViewController {
            self.interactor?.startLogin(from: controller)
        }
        
    }
    
    func startRegister() {
        if let controller = self.view as? UIViewController {
            self.interactor?.startRegister(from: controller)
        }        
    }
}

extension ZPLoginPresenter: ZPLoginInteractorCallBackProtocol {
    func networkNotConnected() {
        self.view?.alertNoNetwork()
    }
    
    func loginZaloFail() {
        self.view?.alertLoginZaloFail()
    }
    
    func startRequestAccessToken() {
        self.view?.startLoading()
    }
    
    func doneRequestAccessToken() {
        self.view?.endLoading()
    }
    
    func loginSuccess(json: NSDictionary) {
        if let handle = self.router?.completionHandler {
            handle(json)
        }
    }
        
    func loginFail(error: NSError){
        self.view?.alertLoginFail(error: error)
    }
    
    func showInputPhone() {
        self.router?.showInputPhone()
    }
}
