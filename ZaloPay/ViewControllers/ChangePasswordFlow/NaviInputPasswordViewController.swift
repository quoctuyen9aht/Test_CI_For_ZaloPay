//
//  NaviInputPasswordViewController.swift
//  ZaloPay
//
//  Created by Dung Vu on 7/21/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class NaviInputPasswordViewController: UINavigationController {

    let publish: PublishSubject<Bool> = PublishSubject()
    let isLoading: Variable<Bool> = Variable(false)
    let isShowAlert: PublishSubject<Bool> = PublishSubject()
}
