//
//  ZPChangePasswordViewController.swift
//  ZaloPay
//
//  Created by Dung Vu on 7/21/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit
import ZaloPayAnalyticsSwift

@objc class ZPChangePasswordViewController: UIViewController {

    @IBOutlet weak var overLayView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet var tapGesture: UITapGestureRecognizer!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var hContainer: NSLayoutConstraint!
    fileprivate lazy var disposeBag = DisposeBag()
    let canExit: Variable<Bool> = Variable(true)
    let publish: PublishSubject<Bool> = PublishSubject()
    let isShowAlert: Variable<Bool> = Variable(false)
    
    weak var naviInput: NaviInputPasswordViewController? {
        didSet{
            naviInput?.publish.subscribe(self.publish).disposed(by: disposeBag)
            naviInput?.isLoading.asDriver().map({ $0.not }).drive(canExit).disposed(by: disposeBag)
            naviInput?.isShowAlert.asObserver().bind(to: self.isShowAlert).disposed(by: disposeBag)
        }
    }
    
    fileprivate lazy var btnCallSupport: UIButton = {
        let button = UIButton(frame: .zero)
        button.setTitleColor(UIColor.zaloBase(), for: .normal)
        button.titleLabel?.font = UIFont.sfuiTextRegular(withSize: 14)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupView()
        setupEvent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ZPTrackingHelper.shared().trackScreen(with: self)
    }
    
    override func screenName() -> String! {
        return ZPTrackerScreen.Screen_iOS_Me_ChangePassword
    }
    
    func setupView() {
        // attach label
        let lblNotice: UILabel = UILabel(frame: .zero)
        lblNotice.font = UIFont.sfuiTextRegular(withSize: 14)
        lblNotice.text = R.string_Change_Password_Call_Support()
        lblNotice.textColor = UIColor(hexValue: 0x5f6c79)
        lblNotice.sizeToFit()
        containerView.addSubview(lblNotice)
        
        // attach button
        btnCallSupport.setTitle(R.string_Change_Password_Phone_Support(), for: .normal)
        btnCallSupport.sizeToFit()
        containerView.addSubview(btnCallSupport)
        
        var totalHeight: CGFloat
        
        // Calculate size
        let wScreen = UIScreen.main.bounds.width
        let isOverScreen = (lblNotice.frame.width + btnCallSupport.frame.width + 56) > wScreen
        if isOverScreen {
            // Vertical
            // get height button
            let hButton = btnCallSupport.frame.height + 8
            
            // add label
            lblNotice.snp.makeConstraints({ (make) in
                make.centerX.equalTo(self.containerView.snp.centerX)
                make.bottom.equalTo(-hButton)
            })
            
            btnCallSupport.snp.makeConstraints({ (make) in
                make.centerX.equalTo(self.containerView.snp.centerX)
                make.bottom.equalTo(-8)
            })
            
            totalHeight = hButton + lblNotice.frame.height
            
        }else {
            // Horizonal
            totalHeight = btnCallSupport.frame.height + 10
            
            // add label
            lblNotice.snp.makeConstraints({ (make) in
                make.left.equalTo(28)
                make.bottom.equalTo(-8)
            })
            
            btnCallSupport.snp.makeConstraints({ (make) in
                make.right.equalTo(-28)
                make.centerY.equalTo(lblNotice.snp.centerY)
            })
            
        }
        hContainer.constant += totalHeight

        btnClose.titleLabel?.font = .iconFont(withSize: 16)
        btnClose.setIconFont("red_delete", for: .normal)
        btnClose.titleEdgeInsets = UIEdgeInsetsMake(5, 12, 0, 0)
        btnClose.setTitleColor(.subText(), for: .normal)
    }
    
    func setupEvent() {
        NotificationCenter.default.rx
            .notification(ZPPayAppToAppUrlHandler.notification)
            .takeUntil(self.rx.deallocating).bind { [weak self](_) in
            self?.publish.onCompleted()
        }.disposed(by: disposeBag)
        
        tapGesture.rx.event.filter({[weak self] _ in self?.canExit.value ?? false }).subscribe(onNext: { [weak self](g) in
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.me_security_changepassword_back)
            guard self?.isShowAlert.value == true else {
                self?.publish.onCompleted()
                return
            }
             self?.showAlert()
        }).disposed(by: disposeBag)
        
        self.rx
            .methodInvoked(#selector(viewWillDisappear(_:)))
            .bind { [weak self](_) in
                self?.view.endEditing(true)
            }.disposed(by: disposeBag)

        
        btnClose.rx.tap.filter({[weak self] _ in  self?.canExit.value ?? false }).bind { [weak self](_) in
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.me_security_changepassword_back)
            guard self?.isShowAlert.value == true else {
                self?.publish.onCompleted()
                return
            }
            self?.showAlert()
        }.disposed(by: disposeBag)
        
        let eventKeyboards = [NotificationCenter.default.rx
            .notification(Notification.Name.UIKeyboardWillShow),
                              NotificationCenter.default.rx
                                .notification(Notification.Name.UIKeyboardWillHide)]
        
        Observable.merge(eventKeyboards)
            .subscribe(onNext: { [weak self](n) in
            guard let info = n.userInfo , let wSelf = self else {
                return
            }
            let isHidden = n.name == Notification.Name.UIKeyboardWillHide
            
            let duration: TimeInterval = info[UIKeyboardAnimationDurationUserInfoKey] as? TimeInterval ?? 0
            let f = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue ?? .zero
        
            let nextAlpha: CGFloat = isHidden ? 0 : 0.6
            let nextHeight: CGFloat = isHidden.not ? f.size.height : -wSelf.containerView.bounds.height
            UIView.animate(withDuration: duration, animations: {
                wSelf.containerView.transform = CGAffineTransform(translationX: 0, y: -nextHeight)
                wSelf.overLayView.alpha = nextAlpha
            }, completion: nil)
            
        }).disposed(by: disposeBag)
        
        NotificationCenter.default.rx.notification(.excutePaymentAppURL).bind { [weak self](_) in
            self?.publish.onCompleted()
        }.disposed(by: disposeBag)
        
        btnCallSupport.rx.tap.filter({[weak self] _ in self?.canExit.value ?? false }).subscribe(onNext:{[weak self] in
            self?.callSupport()
        }).disposed(by: disposeBag)
    }
    
    fileprivate func callSupport() {
        ("tel://1900545436"~>UIApplication.shared.bindingUrl).disposed(by: disposeBag)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "initInputPassword" {
            self.naviInput = segue.destination as? NaviInputPasswordViewController
        }
        
        
    }
    
}

extension ZPChangePasswordViewController {
    static func showChangePassword(on controller: UIViewController?) -> Observable<Bool> {
        guard let c = controller else {
            return Observable.empty()
        }
        return Observable.create({ (s) -> Disposable in
            guard let p = UIStoryboard(name: "ChangePasswordFlow", bundle: nil).instantiateInitialViewController() as? ZPChangePasswordViewController else {
                s.onCompleted()
                return Disposables.create()
            }
            p.modalPresentationStyle = .overCurrentContext
            p.modalTransitionStyle = .crossDissolve
            _ = p.publish.subscribe(s)
            c.present(p, animated: true, completion: nil)
            
            return Disposables.create {
                p.dismiss(animated: true, completion: nil)
            }
        })
    }
}

extension  ZPChangePasswordViewController {
    static func showChangePass(_ from: UIViewController?, completeHandle:@escaping (Error?) -> ()) {
        _ = showChangePassword(on: from).subscribe(onNext: { (_) in
            completeHandle(nil)
        }, onError: { completeHandle($0) })
    }
}


extension ZPChangePasswordViewController  {
    fileprivate func showAlert() {
        ZPDialogView.showDialog(with: DialogTypeNotification, title: R.string_Dialog_Title_Notification() , message: R.string_Change_Password_Cancel_Message() , buttonTitles: [R.string_ButtonLabel_Later(), R.string_ButtonLabel_Next()]) { [weak self](idx) in
            if (idx != 1) {
                self?.publish.onCompleted()
            }
        }
    }
}

extension ZPChangePasswordViewController: ZPOTPProtocol {
    func didReceiveOptMessage(_ opt: String) {
        guard let otpVC = self.naviInput?.viewControllers.last as? ZPChangePasswordOTPViewController else {
            return
        }
        otpVC.didReceiveOptMessage(opt)
    }
}

