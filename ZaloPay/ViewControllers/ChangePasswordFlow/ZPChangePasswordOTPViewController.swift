//
//  ZPChangePasswordOTPViewController.swift
//  ZaloPay
//
//  Created by Dung Vu on 7/21/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

fileprivate let kMaxInputOTP = 3
class ZPChangePasswordOTPViewController: UIViewController, ShowKeyBoardProtocol {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    @IBOutlet weak var lblMessageError: UILabel!
    fileprivate var currentInput: Variable<String> = Variable("")
    fileprivate let error: Variable<String> = Variable("")
    fileprivate let disposeBag = DisposeBag()
    lazy var activityTracking: ActivityIndicator = ActivityIndicator()
    
    fileprivate var publishRoot: PublishSubject<Bool>? {
        return (self.navigationController as? NaviInputPasswordViewController)?.publish
    }
    
    @IBOutlet weak var indicatorView: UIView!
    fileprivate lazy var spinnerView: DGActivityIndicatorView = {
        let v: DGActivityIndicatorView = DGActivityIndicatorView(type:.ballPulse, tintColor: UIColor.subText())
        self.indicatorView.addSubview(v)
        return v
    }()
    fileprivate var isAppear: Bool = true
    fileprivate var canEditText: Bool = true
    fileprivate var currentInputOTP: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupView()
        setupEvent()
    }

    func setupView() {
        lblTitle.text = ZPChangePasswordType.InputOTPPassword.title
        spinnerView.snp.makeConstraints({ (make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.size.equalTo(self.indicatorView.bounds.size)
        })
    }
    
    func setupEvent() {
        activityTracking.asDriver().drive(onNext: { [weak self](isLoading) in
            self?.canEditText = isLoading.not
            if isLoading {
                self?.lblMessageError.isHidden = true
            }
            guard let wSelf = self else {
                return
            }
            
            isLoading ? wSelf.spinnerView.startAnimating() : wSelf.spinnerView.stopAnimating()
        }).disposed(by: disposeBag)
        
        self.rx
            .methodInvoked(#selector(viewWillAppear(_:)))
            .bind { [weak self](_) in
                self?.isAppear = true
                self?.showKeyBoard()
            }.disposed(by: disposeBag)
        
        self.rx
            .methodInvoked(#selector(viewWillDisappear(_:)))
            .bind { [weak self](_) in
                self?.isAppear = false
            }.disposed(by: disposeBag)
        
        textField.rx.text.map({ $0 ?? ""}).bind(to: currentInput).disposed(by: disposeBag)
        
        NotificationCenter.default.rx
            .notification(Notification.Name.ZPDialogViewHide)
            .filter({[weak self] _ in self?.isAppear ?? false})
            .bind
            { [weak self](_) in
            self?.textField.becomeFirstResponder()
        }.disposed(by: disposeBag)

        
        error.asDriver().drive(onNext: { [weak self] in
            self?.lblMessageError.isHidden = $0.count == 0
            self?.lblMessageError.text = $0
        }).disposed(by: disposeBag)
        
        currentInput.asDriver().filter({
            return $0.count == 6
        }).drive(onNext: { [weak self] s in
            self?.checkOTP(s)
        }).disposed(by: disposeBag)
        
        // Track event app move on from background
        NotificationCenter.default.rx
            .notification(Notification.Name.UIApplicationDidBecomeActive)
            .takeUntil(self.rx.methodInvoked(#selector(UIViewController.viewWillDisappear(_:))))
            .filter({[weak self] _ in  self?.textField.isFirstResponder.not ?? false })
            .bind
            { [weak self](_) in
                self?.showKeyBoard()
            }.disposed(by: disposeBag)
        
        // Add tracking to root
        guard let root = self.navigationController as? NaviInputPasswordViewController else {
            return
        }
        
        self.activityTracking.drive(root.isLoading).disposed(by: disposeBag)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func resetValue() {
        _ = Observable.just("").bind(to: self.textField.rx.text)
        self.currentInput.value = ""
    }

}

extension ZPChangePasswordOTPViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard canEditText else { return false }
        guard let t = textField.text else {
            return true
        }
        
        let nStr = (t as NSString).replacingCharacters(in: range, with: string)
        let next = nStr.count <= 6
        return next
    }
}

extension ZPChangePasswordOTPViewController {
    func checkOTP(_ otp: String) {
        let signal = NetworkManager.sharedInstance().recoveryPin(withOtp:otp)
        
        eventIgnoreValue(from: signal).observeOn(MainScheduler.instance).trackActivity(self.activityTracking).subscribe(onNext: { [weak self](_) in
            self?.publishRoot?.onNext(true)
            self?.publishRoot?.onCompleted()
            }, onError: { [weak self](e) in
                guard let wSelf = self else { return }
                var message: String
                if e.errorCode() == NSURLErrorNotConnectedToInternet {
                    message = R.string_Home_InternetConnectionError()
                }else {
                    wSelf.currentInputOTP += 1
                    guard wSelf.currentInputOTP < kMaxInputOTP else {
                        let error = NSError(domain: NSURLErrorDomain, code: NSURLErrorUnknown, userInfo: ["returnmessage": "Quá số lần nhập OTP, vui lòng thử lại."])
                        wSelf.publishRoot?.onError(error)
                        return
                    }
                    message = e.userInfoData()["returnmessage"] as? String ?? ""
                }
                wSelf.error.value = message
                wSelf.resetValue()
        }).disposed(by: disposeBag)
    }
}

extension ZPChangePasswordOTPViewController: ZPOTPProtocol {
    func didReceiveOptMessage(_ opt: String) {
        self.textField.text = opt
        self.textField.sendActions(for: .valueChanged)
    }
}

