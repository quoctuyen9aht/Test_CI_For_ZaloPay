//
//  ZPChangePasswordInputViewController.swift
//  ZaloPay
//
//  Created by Dung Vu on 7/21/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import ZaloPayAnalyticsSwift

enum ZPChangePasswordType: Int {
    case InputLastPassword
    case InputNewPassword
    case InputConfirmNewPassword
    case InputOTPPassword
    
    var title: String {
        switch self {
        case .InputLastPassword:
            return R.string_Change_Password_Input_Current()
        case .InputNewPassword:
            return R.string_Change_Password_Input_New()
        case .InputConfirmNewPassword:
            return R.string_Change_Password_Confirm()
        case .InputOTPPassword:
            return R.string_Change_Password_Input_OTP()
        }
    }
}

protocol ShowKeyBoardProtocol {
    var textField: UITextField! { get }
    func showKeyBoard()
}
extension ShowKeyBoardProtocol {
    func showKeyBoard() {
        guard (UIApplication.shared.keyWindow is MMPopupWindow).not else {
            return
        }
        self.textField.becomeFirstResponder()
    }
}


class ZPChangePasswordInputViewController: UIViewController, ShowKeyBoardProtocol {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblMessageError: UILabel!
    
    @IBOutlet weak var indicatorView: UIView!
    fileprivate var currentInput: Variable<String> = Variable("")
    internal lazy var textField: UITextField! = {
        let t = UITextField(frame: .zero)
        t.keyboardType = .numberPad
        t.delegate = self
        self.view.addSubview(t)
        return t
    }()
    fileprivate let state: Variable<ZPChangePasswordType> = Variable(.InputLastPassword)
    fileprivate let error: Variable<String> = Variable("")
    fileprivate let disposeBag = DisposeBag()
    lazy var activityTracking: ActivityIndicator = ActivityIndicator()
    fileprivate var currentPass: String = ""
    fileprivate var oldPass: String = ""
    fileprivate var canEditText: Bool = true
    fileprivate lazy var spinnerView: DGActivityIndicatorView = {
        let v: DGActivityIndicatorView = DGActivityIndicatorView(type:.ballPulse, tintColor: UIColor.subText())
        self.indicatorView.addSubview(v)
        return v
    }()
    fileprivate var root: NaviInputPasswordViewController? {
        return self.navigationController as? NaviInputPasswordViewController
    }
    
    fileprivate var isAppear: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupView()
        setupEvent()
    }
    
    func setupView() {
        spinnerView.snp.makeConstraints({ (make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.size.equalTo(self.indicatorView.bounds.size)
        })
    }

    func setupEvent() {
        // Track event app move on from background
        NotificationCenter.default.rx
            .notification(Notification.Name.UIApplicationDidBecomeActive)
            .takeUntil(self.rx.methodInvoked(#selector(UIViewController.viewWillDisappear(_:))))
            .filter({[weak self] _ in  self?.textField.isFirstResponder.not ?? false })
            .bind
            { [weak self](_) in
                self?.showKeyBoard()
            }.disposed(by: disposeBag)
        
        activityTracking.asDriver().drive(onNext: { [weak self](isLoading) in
            self?.canEditText = isLoading.not
            if isLoading {
                self?.lblMessageError.isHidden = true
            }
            guard let wSelf = self else {
                return
            }
            
            isLoading ? wSelf.spinnerView.startAnimating() : wSelf.spinnerView.stopAnimating()
        }).disposed(by: disposeBag)
        
        // Error Message
        error.asDriver().drive(onNext: { [weak self] in
            guard let wSelf = self else { return }
            wSelf.textField.text = ""
            wSelf.textField.sendActions(for: .valueChanged)
            wSelf.lblMessageError.isHidden = $0.count == 0
            wSelf.lblMessageError.text = $0
        }).disposed(by: disposeBag)
        
        state.asDriver().drive(onNext: { [weak self]  in
            if ($0 != .InputLastPassword) {
                self?.root?.isShowAlert.onNext(true)
            }
            
            self?.lblTitle.text = $0.title
        }).disposed(by: disposeBag)
        self.rx
            .methodInvoked(#selector(viewWillAppear(_:)))
            .bind { [weak self](_) in
                self?.isAppear = true
                self?.showKeyBoard()
                ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.me_security_changepassword_input)
            }.disposed(by: disposeBag)
        
        self.rx
            .methodInvoked(#selector(viewWillDisappear(_:)))
            .bind { [weak self](_) in
                self?.isAppear = false
            }.disposed(by: disposeBag)
        
        NotificationCenter.default.rx
            .notification(Notification.Name.ZPDialogViewHide)
            .filter({[weak self] _ in self?.isAppear ?? false})
            .bind
            { [weak self](_) in
            self?.textField.becomeFirstResponder()
            }.disposed(by: disposeBag)
        
        Observable.just(0..<6)
            .bind(to: self.collectionView.rx.items(cellIdentifier: "OnboardInputPasswordCell", cellType: OnboardInputPasswordCell.self))
            {
                $2.isSelected = $0 < self.currentInput.value.count
            }
            .disposed(by: disposeBag)
        
        textField.rx.text.map({ $0 ?? ""}).bind(to: currentInput).disposed(by: disposeBag)
        
        currentInput.asDriver().drive(onNext: { [weak self] in
            let total = $0.count
            (0..<6).map({ IndexPath(item: $0, section: 0) }).forEach({
                guard let cell = self?.collectionView.cellForItem(at: $0) as? OnboardInputPasswordCell else {
                    return
                }
                cell.isSelected = $0.row < total
            })
        }).disposed(by: disposeBag)

        // Handler state
        currentInput.asDriver().filter({[weak self] in
            return $0.count == 6 && self?.canEditText == true && self?.isAppear == true
        }).debounce(0.2).drive(onNext: { [weak self] (s) in
            guard let wSelf = self else {
                return
            }
            switch (wSelf.state.value) {
            case .InputLastPassword:
                wSelf.vertifyPass(with: s)
            case .InputNewPassword:
                wSelf.currentPass = s
                wSelf.state.value = .InputConfirmNewPassword
                wSelf.resetValue()
            case .InputConfirmNewPassword:
                //Validate same pass
                if wSelf.currentPass == s {
                    // send api get OTP
                    wSelf.getOTP()
                }else {
                    wSelf.error.value = R.string_Change_Password_Input_Wrong()
                    wSelf.resetValue()
                }
                
            default:
                break
            }
        }).disposed(by: disposeBag)
        
        // Add tracking to root
        guard let root = self.root else {
            return
        }
        
        self.activityTracking.drive(root.isLoading).disposed(by: disposeBag)
    }
    
    func resetValue() {
        _ = Observable.just("").bind(to: self.textField.rx.text)
        self.currentInput.value = ""
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
    }
}

extension ZPChangePasswordInputViewController:UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard canEditText else { return false }
        guard let t = textField.text else {
            return true
        }
        
        let nStr = (t as NSString).replacingCharacters(in: range, with: string)
        let next = nStr.count <= 6
        return next
    }
}
// MARK: - Vertify Pass
extension ZPChangePasswordInputViewController {
    func vertifyPass(with pass: String) {
        let encrypt = (pass as NSString).sha256()
        let signal = NetworkManager.sharedInstance().validatePin(encrypt)
        
        eventIgnoreValue(from: signal).observeOn(MainScheduler.instance).trackActivity(self.activityTracking).observeOn(MainScheduler.instance).subscribe(onNext: { [weak self](_) in
            
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.me_security_changepassword_continue)
            
            self?.state.value = .InputNewPassword
            self?.oldPass = pass
            self?.resetValue()
            self?.error.value = ""
            }, onError: { [weak self](e) in
                var message: String
                if e.errorCode() == NSURLErrorNotConnectedToInternet {
                    message = R.string_Home_InternetConnectionError()
                }else {
                    guard Int32(e.errorCode()) != ZALOPAY_ERRORCODE_OVER_INPUT_CHANGE_PASSWORD.rawValue else {
                        self?.root?.publish.onError(e)
                        return
                    }
                    message = e.userInfoData()["returnmessage"] as? String ?? ""
                }
                self?.error.value = message
                self?.resetValue()
        }).disposed(by: disposeBag)
    }
    
    
    func getOTP() {
        let signal = NetworkManager.sharedInstance().recoveryPin(withNewPin: currentPass, oldPin: oldPass)

        eventIgnoreValue(from: signal).observeOn(MainScheduler.instance).trackActivity(self.activityTracking).observeOn(MainScheduler.instance).subscribe(onNext: { [weak self](_) in
            self?.performSegue(withIdentifier: "showOTP", sender: nil)
            }, onError: { [weak self](e) in
                
                ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.me_security_changepassword_result)
                
                var message: String
                if e.errorCode() == NSURLErrorNotConnectedToInternet {
                    message = R.string_Home_InternetConnectionError()
                }else {
                    message = e.userInfoData()["returnmessage"] as? String ?? ""
                }
                self?.error.value = message
                self?.resetValue()
        }).disposed(by: disposeBag)
    }
}

