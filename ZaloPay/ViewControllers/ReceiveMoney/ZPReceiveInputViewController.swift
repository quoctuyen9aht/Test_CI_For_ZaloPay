//
//  ZPReceiveInputViewController.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/27/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import ZaloPayAnalyticsSwift

typealias ZPReceiveInputFinishedBlock = ((_ amount: Int64,_ message: String) -> Void)


enum InvalidMessageError : Error {
    case failedJSON
    case invalidMessage(message: String, modified: String)
}

@objcMembers
class ZPReceiveInputViewController: ZPBaseScrollViewController {

    var amountTextField: ZPCalFloatTextInputView!
    var messageTextField: ZPFloatTextInputView!
    var minTransfer: Int64 = 0
    var maxTransfer: Int64 = 0
    var messageLength: Int = 0
    private let disposeBag = DisposeBag()
    
    var finishedBlock: ZPReceiveInputFinishedBlock?
    
    override func scrollHeight() -> Float {
        return 122
    }
    
    override func buttonText() -> String {
        return R.string_ReceiveInput_Confirm()
    }
    
    override func backButtonClicked(_ sender: Any!) {
        super.backButtonClicked(sender)
    }
    
    override func backEventId() -> Int {
        return ZPAnalyticEventAction.moneyreceive_input_touch_back.rawValue
    }
    
    override func screenName() -> String! {
        return ZPTrackerScreen.Screen_iOS_ReceiveMoney_Input
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupView()
        self.handleTextFieldEvent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ZPTrackingHelper.shared().trackScreen(with: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.amountTextField.textField.becomeFirstResponder()
    }
    
    func setupView() {
        self.title = R.string_TransferQRCode_Title()
        self.getTransferAppInfo()
        self.amountTextField = ZPCalFloatTextInputView()
        self.amountTextField.backgroundColor = .white
        self.contentView.addSubview(self.amountTextField)
        
        self.messageTextField = ZPFloatTextInputView()
        self.messageTextField.backgroundColor = .white
        self.contentView.addSubview(self.messageTextField)
        
        self.amountTextField.snp.makeConstraints {
            make in
            make.left.right.top.equalTo(0)
        }
        
        self.messageTextField.snp.makeConstraints {
            make in
            make.left.right.equalTo(0)
            make.top.equalTo(self.amountTextField.snp.bottom)
        }
        
        self.contentView.snp.updateConstraints {
            make in
            make.bottom.equalTo(self.messageTextField.snp.bottom)
        }
        
        self.amountTextField.lineInsets = UIEdgeInsetsMake(0, 10, 0, 0)
        self.amountTextField.textField.delegate = self
        self.amountTextField.textField.placeholder = R.string_TransferQRCode_AmountPlaceHolder()
        
        self.messageTextField.lineInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        self.messageTextField.textField.delegate = self
        self.messageTextField.textField.placeholder = R.string_TransferQRCode_MessagePlaceHolder()
        

        if ZPKeyboardConfig.enableCalculateKeyboard(.receive) {
            let numberPad: ZPCalculateNumberPad = ZPCalculateNumberPad(delegate: self)
            numberPad.doneButton.isEnabled = false
            self.amountTextField.textField.inputView = numberPad
            self.bottomButton.addTarget(self, action: #selector(self.pmcContinueButtonClicked), for: UIControlEvents.touchUpInside)
        } else {
            let numberPad: PMNumberPad = PMNumberPad(delegate: self)
            numberPad.doneButton.isEnabled = false
            self.amountTextField.textField.inputView = numberPad
            self.bottomButton.addTarget(self, action: #selector(self.pmContinueButtonClicked), for: UIControlEvents.touchUpInside)
        }
        
        _ = self.amountTextField.textField.rx.text.subscribe(onNext: {
            [weak self] (text) in
            self?.textField(self?.amountTextField.textField, updateText: text)
        })
        
        _ = self.messageTextField.textField.rx.text.subscribe(onNext: {
            [weak self] (text) in
            self?.textField(self?.messageTextField.textField, updateText: text)
        })
        
        self.bottomButton.titleLabel?.zpMainBlackRegular()
    }
    
    func trackTextInput(textField: UITextField, actionInput: ZPAnalyticEventAction, actionClear: ZPAnalyticEventAction) {
        textField.rx.controlEvent(.editingDidBegin).bind {(_) in
            ZPTrackingHelper.shared().trackEvent(actionInput)
        }.disposed(by: disposeBag)
        textField.rx.controlEvent(.editingChanged).bind {(_) in
            //Track clear amountTextField
            if let text = textField.text, text.isEmpty {
                ZPTrackingHelper.shared().trackEvent(actionClear)
            }
        }.disposed(by: disposeBag)
    }
    
    func handleTextFieldEvent() {
        self.trackTextInput(textField: messageTextField.textField, actionInput: .moneyreceive_input_message, actionClear: .moneyreceive_input_message_touch_clear)
        self.trackTextInput(textField: amountTextField.textField, actionInput: .moneyreceive_input_amount, actionClear: .moneyreceive_input_amount_touch_clear)
    }
    
    func getTransferAppInfo() {
        self.minTransfer = ZPWalletManagerSwift.sharedInstance.config.minTransfer.toInt64()
        self.maxTransfer = ZPWalletManagerSwift.sharedInstance.config.maxTransfer.toInt64()
    }
    
    func textField(_ textField: UITextField?, updateText text: String?) {
        guard   let textField = textField,
            let text = text
            else {
                return
        }
        var enableButton = true
        
        if textField == self.amountTextField.textField {
            if text == "" {
                self.amountTextField.clearErrorText()
            }
            self.amountTextField.updateTextField(textField, updateText: text)
        }
        
        if !TransferAppInfo
            .validMessage(self.messageTextField.textField.text ?? "") {
            enableButton = false
        }
        
        self.bottomButton.isEnabled = enableButton
        if let numberPad = self.amountTextField.textField.inputView as? ZPCalculateNumberPad {
            numberPad.doneButton.isEnabled = enableButton
        }
        if let numberPad = self.amountTextField.textField.inputView as? PMNumberPad {
            numberPad.doneButton.isEnabled = enableButton
        }
    }
}

extension ZPReceiveInputViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField != self.amountTextField.textField {
            return
        }
        guard let numberPad = self.amountTextField.textField.inputView as? ZPCalculateNumberPad else {
            return
        }
        if !numberPad.isExpression(self.amountTextField.textField.text) {
            return
        }
        let result = numberPad.calc(self.amountTextField)
        if numberPad.calcError == ZPCalculationError.calc_Divide_By_Zero
            || numberPad.calcError == ZPCalculationError.calc_Wrong_Syntax {
            return
        }
        TransferAppInfo.isLessThan(Int64(result), textInput: self.amountTextField)
        TransferAppInfo.isGreaterThanMax(Int64(result), textInput:self.amountTextField)
        if result < 0 {
            return
        }
        self.amountTextField.textField.text = String(result).onlyDigit().formatMoneyValue()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var newString = (textField.text ?? "") as NSString
        newString = newString.replacingCharacters(in: range, with: string) as NSString
        if textField == self.messageTextField.textField {
            return TransferAppInfo.validMessage(newString as String, textInput: self.messageTextField)
        }
        
        self.amountTextField.clearErrorText()
        if  self.amountTextField.textField.inputView is PMNumberPad {
            let totalValue: Int64 = Int64(newString.onlyDigit()) ?? 0
            return !TransferAppInfo.isGreaterThanMax(totalValue, textInput:self.amountTextField)
        }
        
        if self.amountTextField.textField.inputView is ZPCalculateNumberPad &&
           self.amountTextField.isExpression() &&
            self.amountTextField.isMaxCharactor(newString as String?){
                self.amountTextField.showErrorAuto(R.string_InputMoney_Max_Character_Error_Message())
                return false
        }
        return true
    }
}

extension ZPReceiveInputViewController: PMCNumberPadControl {
    func pmcKeyboard(_ keyboard: ZPCalculateNumberPad!, checkMinvalue value: Int64, fromFloatTextField textInput: ZPFloatTextInputView!) {
        TransferAppInfo.isLessThan(value, textInput: textInput)
    }
    
    func pmcContinueButtonClicked(_ sender: Any?) {
        let message = self.messageTextField.textField.text!
        
        guard let numberPad = self.amountTextField.textField.inputView as? ZPCalculateNumberPad else {
            return
        }
        
        if numberPad.isExpression(self.amountTextField.textField.text) {
            let result = numberPad.calc(self.amountTextField)
            if numberPad.calcError == ZPCalculationError.calc_Divide_By_Zero
                || numberPad.calcError == ZPCalculationError.calc_Wrong_Syntax {
                return
            }
            if result < 0 {
                TransferAppInfo.isLessThan(Int64(result), textInput: self.amountTextField)
                numberPad.doneButton.isEnabled = false
                self.bottomButton.isEnabled = false
                return
            }
            self.amountTextField.textField.text = String(result).onlyDigit().formatMoneyValue()
        }
        
        let totalValue = Int64(self.amountTextField.textField.text!.onlyDigit()) ?? 0
        if TransferAppInfo.isLessThan(totalValue, textInput: self.amountTextField) ||
            TransferAppInfo.isGreaterThanMax(totalValue, textInput:self.amountTextField) ||
            !TransferAppInfo.validMessage(message) {
            numberPad.doneButton.isEnabled = false
            self.bottomButton.isEnabled = false
            return
        }
        
        if let finishBlock = self.finishedBlock {
            finishBlock(totalValue,message)
        }
        
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.moneyreceive_input_touch_confirm)
        self.navigationController?.popViewController(animated: true)
    }
}

extension ZPReceiveInputViewController: PMNumberPadControl {
    func pmContinueButtonClicked(_ sender: Any!) {
        let totalValue: Int64 = Int64(self.amountTextField.textField.text!.onlyDigit()) ?? 0
        let message = self.messageTextField.textField.text!
        
        if TransferAppInfo.isLessThan(totalValue, textInput: self.amountTextField) ||
            TransferAppInfo.isGreaterThanMax(totalValue) ||
            !TransferAppInfo.validMessage(message) {
            self.bottomButton.isEnabled = false
            if let numberPad = self.amountTextField.textField.inputView as? PMNumberPad {
                numberPad.doneButton.isEnabled = false
            }
            return
        }
        if let finishBlock = self.finishedBlock {
            finishBlock(totalValue,message)
        }
        requestVerifyReceiveMessage(message: message)
    }
}

extension ZPReceiveInputViewController {
    func requestVerifyReceiveMessage(message: String) {
        showLoadingView()

        let signal = NetworkManager.sharedInstance().filterTextContent(message)
        signal?.flattenMap({ (json) -> RACSignal<AnyObject>? in
            guard let json = json as? JSON else {
                return RACSignal.error(InvalidMessageError.failedJSON)
            }
            let status = json.value(forPath:"data.status", defaultValue: false)
            let returnMessage = json.value(forPath:"returnmessage", defaultValue: "")
            let modified = json.value(forPath:"data.modified", defaultValue: message)
            
            if status {
                return RACSignal.return(json as AnyObject)
            }
            let error = InvalidMessageError.invalidMessage(message: returnMessage, modified: modified)
            return RACSignal.error(error)
            
        }).deliverOnMainThread().subscribeNext({ [weak self] _ in
            self?.popViewController()
        }, error: { [weak self] (error) in
            self?.handleInvalidMessageError(error)
        })
    }
    
    func handleInvalidMessageError(_ error: Error?) {
        if let err = error as? InvalidMessageError {
            switch err {
            case .failedJSON:
                self.popViewController()
            case .invalidMessage (let message, let modified):
                self.invalidReceiveMessage(errorMessage: message, modified: modified)
            }
            return
        }
        if let err = error as NSError?, err.isApiError().not {
            self.popViewController()
            return
        }
        self.showErrorPopup(error: error)
    }
    
    func showErrorPopup(error: Error?) {
        ZPDialogView.showDialogWithError(error, handle: {
            self.amountTextField.textField.becomeFirstResponder()
        })
        self.hideLoadingView()
    }
    
    func popViewController() {
        self.hideLoadingView()
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.moneyreceive_input_touch_confirm)
        self.navigationController?.popViewController(animated: true)
    }
    
    func invalidReceiveMessage(errorMessage: String, modified: String) {
        self.hideLoadingView()
        self.messageTextField.showError(withText: errorMessage)
        self.messageTextField.textField.text = modified
    }
    
    func showLoadingView() {
        ZPAppFactory.sharedInstance().showHUDAdded(to: self.view)
    }
    
    func hideLoadingView() {
        ZPAppFactory.sharedInstance().hideAllHUDs(for: self.view)
    }
}

extension ZPReceiveInputViewController: APNumberPadDelegate {
    typealias TypeInput = UIResponder & UITextInput
    func numberPad(_ numberPad: APNumberPad!, functionButtonAction functionButton: UIButton!, textInput: TypeInput!) {
        if let numberPad = numberPad as? ZPCalculateNumberPad  {
            numberPad.control(self.amountTextField, with: functionButton, viewController: self)
            return
        }
        guard let numberPad = numberPad as? PMNumberPad  else {
            return
        }
        if functionButton == numberPad.doneButton {
            pmContinueButtonClicked(nil)
            return
        }
        if functionButton == numberPad.leftFunctionButton {
            let txtField = amountTextField.textField!
            let count = (txtField.text ?? "").count
            let range = NSRange(location: count, length: 0)
            let text: String = "000"
            if self.textField(txtField, shouldChangeCharactersIn: range, replacementString: text) {
                self.amountTextField.textField.insertText(text)
            }
        }
    }
}
