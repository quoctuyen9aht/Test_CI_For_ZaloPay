//
//  ZPReceiveMoneyPresenter.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit

class ZPReceiveMoneyPresenter: ZPReceiveMoneyPresenterDelegate {

    weak var controller: ZPReceiveMoneyViewControllerInput?
    
    var interactor: ZPReceiveMoneyInteractorInputDelegate?
    
    var router: ZPReceiveMoneyRouterDelegate?
    
    func viewDidLoad() {
        interactor?.setupInteractor()
    }
    
    func showDialogQRCodeError() {
        ZPDialogView.showDialog(with: DialogTypeError,
                                message: R.string_TransferQRCode_ErrorMessage(),
                                cancelButtonTitle: R.string_ButtonLabel_Close(),
                                otherButtonTitle: nil,
                                completeHandle: nil)
    }
    
    func nextResult() {
        interactor?.nextResult()
    }
    
    func getInfo() -> (amount: Int64, message: String) {
        return (amount: interactor?.amount ?? 0, message: interactor?.message ?? "")
    }
    
    func createQRCodeImage() {
        interactor?.createQRImage()
    }
    
    func backClicked() {
        interactor?.backAction()
    }
    
    func editClicked() {
        self.interactor?.editAction()
    }
    
    func updateView() {
        controller?.updateView()
    }
    
    func getDataArray() -> [ZPQRTransferAppMessage] {
        return interactor?.dataArray ?? [ZPQRTransferAppMessage]()
    }
    
    func finishHideResult() {
        interactor?.finishHideResult()
    }
}

extension ZPReceiveMoneyPresenter: ZPReceiveMoneyInteractorOutputDelegate {
    
    func bindingQRImage(params: NSDictionary) {
        controller?.bindingQRImage(params: params)
    }
    
    func showResultViewWithMessage(_ message: ZPQRTransferAppMessage) {
        controller?.showResultViewWithMessage(message)
    }
    
    func reloadDataAnScrollTo(index indexPath: IndexPath) {
        controller?.reloadDataAnScrollTo(index: indexPath)
    }
    
    func showDialogError(_ error: NSError?, handle: (() -> Void)? = nil) {
        ZPDialogView.showDialogWithError(error, handle: handle)
    }
    
    func setAvatar(with url: URL) {
        self.controller?.setAvatar(with: url)
    }
    
    func openInputViewController(_ finishedBlock: @escaping ZPReceiveInputFinishedBlock) {
        router?.openInputViewController(finishedBlock)
    }
}
