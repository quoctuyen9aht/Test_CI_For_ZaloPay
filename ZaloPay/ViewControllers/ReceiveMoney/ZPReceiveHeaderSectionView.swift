//
//  ZPReceiveHeaderSectionView.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit
import ZaloPayCommonSwift

class ZPReceiveHeaderSectionView: UIView {
    
    var amountLabel: UILabel!
    
    convenience init() {
        self.init(frame: .zero)
        setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView() {
        self.backgroundColor = .blue
        let title = UILabel()
        title.font = UIFont.sfuiTextRegular(withSize: 16)
        title.zpMainBlackRegular()
        title.text = R.string_TransferQRCode_Total()
        title.textColor = UIColor.defaultText()
        self.addSubview(title)
        
        amountLabel = UILabel()
        amountLabel.zpMainBlackRegular()
        self.addSubview(amountLabel)
        
        title.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.snp.bottom).offset(-10)
            make.left.equalTo(UIView.isScreen2x() ? 18 : 22)
            make.right.equalTo(amountLabel.snp.left).offset(20)
            make.height.equalTo(title.font.lineHeight)
        }
        
        amountLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.snp.bottom).offset(-10)
            make.right.equalTo(UIView.isScreen2x() ? -18 : -22)
            make.width.equalTo(100).priority(.low)
            make.height.equalTo(amountLabel.font.lineHeight)
        }
        
        let line = ZPLineView()
        self.addSubview(line)
        line.alignToBottom()
        self.backgroundColor = .white
    }
    
    func setAmount(_ amount: Int64) {
        self.amountLabel.attributedText = NSAttributedString.attributedStringWith(amount: amount, font: amountLabel.font, subFont: UIFont.sfuiTextRegular(withSize: 12), color: UIColor.defaultText(), textAlignment: .right)
    }
}

