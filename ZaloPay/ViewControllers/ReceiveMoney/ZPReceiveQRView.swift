//
//  ZPReceiveQRView.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit
import ZaloPayCommonSwift

class ZPReceiveQRView: UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var qrImageView: UIImageView!
    @IBOutlet weak var avatarView: UIView!
    @IBOutlet var descView: UIView!
    @IBOutlet var amountTitleLable: UILabel!
    @IBOutlet var messageTitleLable: UILabel!
    @IBOutlet var amountLabel: UILabel!
    @IBOutlet var messageLabel: UILabel!
    
    var bottomConstraint: ConstraintMakerEditable?
    var imageTopConstraint: ConstraintMakerEditable?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        var height = Float(avatarImageView.frame.height)
        avatarImageView.zp_roundRect(height / 2)
        height = Float(avatarView.frame.height)
        avatarView.zp_roundRect(height / 2)
        avatarView.backgroundColor = UIColor.white
        
        //titleLabel?.font = UIFont.sfuiTextRegular(withSize: 16)
        titleLabel.zpMainBlackRegular()
        titleLabel?.textColor = UIColor.subText()
        avatarImageView.image = UIImage(named: "profile_avatar")
        self.addSubview(descView)
        
        //messageLabel.font = UIFont.sfuiTextRegular(withSize: 16)
        messageLabel.zpMainBlackRegular()
        messageLabel.textColor = UIColor.defaultText()
        
        let line = ZPLineView()
        self.descView.addSubview(line)
        line.alignToBottom()
        setAmount(0, message: "")
        backgroundColor = UIColor.white
        
        self.amountLabel.zpMainBlackRegular()
    }
    
    func setAmount(_ amount: Int64, message: String) {
        if let imageTopConstraint = imageTopConstraint {
            imageTopConstraint.constraint.deactivate()
        }
        
        if amount > 0 || message.count > 0 {
            self.descView.snp.updateConstraints({ (make) in
                make.left.right.equalTo(0)
                make.top.equalTo(self.titleLabel.snp.bottom)
            })
            
            if let _ = self.bottomConstraint {
                self.bottomConstraint?.constraint.deactivate()
                self.bottomConstraint = nil
            }
            
            if message.count == 0 {
                self.descView.snp.makeConstraints({ (make) in
                    self.bottomConstraint = make.bottom.equalTo(self.amountLabel.snp.bottom).offset(10)
                })
            } else {
                self.descView.snp.makeConstraints({ (make) in
                    self.bottomConstraint = make.bottom.equalTo(self.messageLabel.snp.bottom).offset(10)
                })
            }
            
            self.qrImageView.snp.remakeConstraints({ (make) in
                self.imageTopConstraint = make.top.equalTo(self.descView.snp.bottom)
                // UIScreen.main.bounds.width == 414 -> IPhone Plus
                let widthConst = self.bounds.size.width * (3/5)
                make.width.equalTo(UIScreen.main.bounds.width == 414 ? (widthConst + 50) : widthConst)
            })
            
            self.descView.isHidden = false
        } else {
            self.descView.isHidden = true
            self.qrImageView.snp.remakeConstraints({ (make) in
                self.imageTopConstraint = make.top.equalTo(self.titleLabel.snp.bottom).offset(self.bounds.size.width * (1/5))
                // UIScreen.main.bounds.width == 414 -> IPhone Plus
                let widthConst = self.bounds.size.width * (3/5)
                make.width.equalTo(UIScreen.main.bounds.width == 414 ? (widthConst + 50) : widthConst)
            })
        }
        
        self.setAmount(amount)
        self.messageLabel.text = message
    }
    
    func setAmount(_ amount: Int64) {
        self.amountLabel.attributedText = NSAttributedString.attributedStringWith(amount: amount, font: UIFont.sfuiTextRegular(withSize: 30), subFont: self.amountLabel.font, color: UIColor.defaultText(), textAlignment: .center)
    }
    
    func setupDescView() {
        
    }
    
}

