//
//  ZPReceiveMoneyRouter.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
@objcMembers
class ZPReceiveMoneyRouter: NSObject, ZPReceiveMoneyRouterDelegate {

    weak var rootViewController: UIViewController?
    
    func backViewController() {
        rootViewController?.navigationController?.popViewController(animated: true)
    }
    
    @objc class func assemblyModule() -> ZPReceiveMoneyViewController {
        let settingController: ZPReceiveMoneyViewController = ZPReceiveMoneyViewController()
        
        let presenter: ZPReceiveMoneyPresenter = ZPReceiveMoneyPresenter()
        let interactor: ZPReceiveMoneyInteractor = ZPReceiveMoneyInteractor()
        let router: ZPReceiveMoneyRouter = ZPReceiveMoneyRouter()
        
        presenter.controller = settingController
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.rootViewController = settingController
        
        settingController.presenter = presenter
        
        return settingController
    }
    
    func openInputViewController(_ finishedBlock: @escaping ZPReceiveInputFinishedBlock) {
        
        let inputViewController = ZPReceiveInputViewController()
        inputViewController.finishedBlock = finishedBlock
        rootViewController?.navigationController?.pushViewController(inputViewController, animated: true)
    }
    
    
}
