//
//  ZPReceiveMoneyContract.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit

// ZPReceiveMoneyPresenter -> ZPReceiveMoneyController
protocol ZPReceiveMoneyViewControllerInput: class {
    
    var presenter: ZPReceiveMoneyPresenterDelegate? { get set }
    
    func bindingQRImage(params: NSDictionary)
    
    func hiddenResultView()
    
    func showResultViewWithMessage(_ message: ZPQRTransferAppMessage)
    
    func updateView()
    
    func reloadDataAnScrollTo(index indexPath: IndexPath)
    
    func setAvatar(with url: URL)
}

// ZPReceiveMoneyController -> ZPReceiveMoneyPresenter
protocol ZPReceiveMoneyPresenterDelegate: class {
    
    var controller: ZPReceiveMoneyViewControllerInput? { get set }
    
    var interactor: ZPReceiveMoneyInteractorInputDelegate? { get set }
    
    var router: ZPReceiveMoneyRouterDelegate? { get set }
    
    func viewDidLoad()
    
    func showDialogQRCodeError()
    
    func nextResult()
    
    func getInfo() -> (amount: Int64, message: String)
    
    func getDataArray() -> [ZPQRTransferAppMessage]
    
    func createQRCodeImage()
    
    func backClicked()
    
    func editClicked()
    
    func setAvatar(with url: URL)
    
    func finishHideResult()
}

// ZPReceiveMoneyPresenter -> ZPReceiveMoneyInteractor
protocol ZPReceiveMoneyInteractorInputDelegate: class {
    
    var presenter: ZPReceiveMoneyInteractorOutputDelegate? { get set }

    var amount: Int64 {get}
    var message: String {get}
    var dataArray: [ZPQRTransferAppMessage] {get}
    
    func setupInteractor()
    
    func createQRImage()
    
    func nextResult()
    
    func editAction()
    
    func backAction()
    
    func finishHideResult()
}

// ZPReceiveMoneyInteractor -> ZPReceiveMoneyPresenter
protocol ZPReceiveMoneyInteractorOutputDelegate: class {
    
    func bindingQRImage(params: NSDictionary)
    
    func showResultViewWithMessage(_ message: ZPQRTransferAppMessage)
    
    func updateView()
    
    func reloadDataAnScrollTo(index indexPath: IndexPath)
    
    func showDialogError(_ error: NSError?, handle: (() -> Void)?)
    
    func setAvatar(with url: URL)
    
    func openInputViewController(_ finishedBlock: @escaping ZPReceiveInputFinishedBlock)
}

protocol ZPReceiveMoneyRouterDelegate: class {
    
    var rootViewController: UIViewController? { get set }
    
    func backViewController()
 
    func openInputViewController(_ finishedBlock: @escaping ZPReceiveInputFinishedBlock)
}
