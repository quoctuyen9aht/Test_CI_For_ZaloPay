//
//  ZPSenderStatusView.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit

class ZPSenderStatusView: UIView {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupView() {
        let height = Float(self.image.frame.height)
        self.image.zp_roundRect(height/2)
        self.image.layer.borderWidth = 0
        //self.label.font = UIFont.sfuiTextRegular(withSize: 16)
        self.label.zpMainBlackRegular()
        self.label.textColor = .subText()
        let size = self.label.sizeThatFits(self.label.frame.size)
        self.label.snp.updateConstraints { (make) in
            make.height.equalTo(size.height)
        }
    }
    
    func setDisplayName(_ displayName: String, progress: MoneyTransferProgress) {
        var text = ""
        var name = displayName
        if displayName.count > 10 {
            name = name.substring(from: name.index(name.startIndex, offsetBy: 7))
            name = name.appending("...")
        }
        
        switch progress {
        case MoneyTransferProgress.scan:
            text = String(format: R.string_QRReceive_BeginScan(), name)
        case MoneyTransferProgress.success:
            text = String(format: R.string_QRReceive_SendSuccess(), name)
        case MoneyTransferProgress.fail:
            text = String(format: R.string_QRReceive_SendFail(), name)
        case MoneyTransferProgress.cancel:
            text = String(format: R.string_QRReceive_Cancel(), name)
        default:
            break
        }
    
        self.label.text = text
    }
}
