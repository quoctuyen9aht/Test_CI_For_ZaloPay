//
//  ZPReceiveMoneyViewController.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa
import CocoaLumberjackSwift
import ZaloPayAnalyticsSwift

class ZPReceiveMoneyViewController: BaseViewController {
    
    var presenter: ZPReceiveMoneyPresenterDelegate?
    
    var brightness: CGFloat = 0
    lazy var contentView: ZPReceiveQRView! = {
        return ZPReceiveQRView.zp_viewFromSameNib()!.build {
            let size = $0.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
            $0.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
            $0.autoresizingMask = .flexibleWidth
            self.tableView.tableHeaderView = $0
            $0.snp.makeConstraints { (make) in
                make.width.equalTo(self.tableView);
                make.edges.equalTo(self.tableView)
            }
        }
    }()
    
    
    lazy var resultView: ZPReceiveResultView! = {
        return ZPReceiveResultView.zp_viewFromSameNib()!.build {
            self.view.addSubview($0)
            $0.backgroundColor = .white
            $0.snp.makeConstraints {
                make in
                make.left.top.equalTo(10)
                make.right.equalTo(-10);
                make.height.equalTo(self.contentView.snp.height).offset(25)
            }
        }
    }()
    
    var senderStatusView: ZPSenderStatusView!
    var headerSectionView: ZPReceiveHeaderSectionView!
    lazy var tableView: UITableView! = {
        return UITableView(frame: .zero, style: .plain).build {
            self.view.addSubview($0)
            
            $0.snp.makeConstraints { (make) in
                make.left.top.equalTo(10)
                make.right.equalTo(-10)
                make.bottom.equalTo(-50)
            }
            
            $0.backgroundColor = .clear
            $0.separatorStyle = .none
            $0.delegate = self
            $0.dataSource = self
            $0.bounces = false
            $0.separatorColor = UIColor.clear
        }
    }()
    let imageSize = CGSize(width: 300, height: 300)
    var totalAmount: Int64 = 0
    var dataArray : [ZPQRTransferAppMessage] {
        get {
            return self.presenter?.getDataArray() ?? [ZPQRTransferAppMessage]()
        }
    }
    var resultMessage: ZPQRTransferAppMessage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter?.viewDidLoad()
        self.setupView()
    }
    deinit {
        DDLogInfo("Deinit : ZPReceiveMoneyViewController")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.brightness = UIScreen.main.brightness
        UIScreen.main.brightness = 1.0
        
        ZPTrackingHelper.shared().trackScreen(with: self)
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.moneyreceive_launched)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UIScreen.main.brightness = self.brightness
    }
    override func screenName() -> String! {
        return ZPTrackerScreen.Screen_iOS_ReceiveMoney
    }
    
    override func backEventId() -> Int {
        return ZPAnalyticEventAction.moneyreceive_input_touch_back.rawValue
    }
    
    func setupView() {
        self.title = R.string_TransferQRCode_ReceiveMoneTitle()
        self.view.backgroundColor = UIColor.zaloBase()
        self.setupRightBarButton()
        self.presenter?.createQRCodeImage()
    }
    
    @objc func editButtonClicked() {
        presenter?.editClicked()
    }
    
//    override func backButtonClicked(_ sender: Any!) {
//        presenter?.backClicked()
//    }
//    
    override func viewWillSwipeBack() {
        presenter?.backClicked()
    }
    
    func setupRightBarButton() {
        let title = presenter!.getInfo().amount > 0 ?
            R.string_TransferQRCode_Delete() :
            R.string_TransferQRCode_Money()
        
        let rightBtn = UIButton(type: .custom)
        rightBtn.setTitle(title, for: .normal)
        rightBtn.frame = CGRect(x: 0, y: 0, width: 80, height: 50)
        rightBtn.addTarget(self, action: #selector(self.editButtonClicked), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightBtn)
    }
    
    func updateView() {
        let receiveInfo = presenter!.getInfo()
        self.contentView.setAmount(receiveInfo.amount, message: receiveInfo.message)
        self.tableView.layoutSubviews()
        self.tableView.layoutIfNeeded()
        
        self.tableView.tableHeaderView = self.contentView
        self.setupRightBarButton()
        
        self.presenter?.createQRCodeImage()
    }
}

// MARK: ZPReceiveMoneyViewControllerInput
extension ZPReceiveMoneyViewController: ZPReceiveMoneyViewControllerInput {
    func bindingQRImage(params: NSDictionary) {
        guard
            let dataString = params.jsonRepresentation(),
            let data = dataString.data(using: String.Encoding.isoLatin1),
            let imageQR = UIImage.createQRImage(with: imageSize, from: data)
            else {
                presenter?.showDialogQRCodeError()
                return
        }
        self.contentView.qrImageView.image = imageQR
    }
    
    func hiddenResultView() {
        //        self.contentView.addSubview(self.senderStatusView)
        self.setupRightBarButton()
        
        guard let index = self.dataArray.index(of: self.resultMessage) else {
            return
        }
        
        let indexPath = IndexPath(row: index, section: 0)
        let rect = self.tableView.rectForRow(at: indexPath)
        var point = CGPoint(x: rect.midX, y: rect.midY)
        point.y -= self.tableView.contentOffset.y
        let _point = self.view.convert(point, to: self.tableView)
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations: {
            let scale = CGAffineTransform(scaleX: 0.1, y: 0.1)
            let move = CGAffineTransform(translationX: 0, y: _point.y - self.resultView.frame.midY)
            self.resultView.transform = scale.concatenating(move)
        }, completion: {
            finish in
            
            self.tableView.scrollToRow(at: indexPath, at: .none, animated: true)
            self.resultView.transform = CGAffineTransform.identity
            self.resultView.isHidden = true
            self.resultMessage.progress = MoneyTransferProgress.finish.rawValue
            self.totalAmount = self.totalAmount + self.resultMessage.amount
            
            self.tableView.reloadData()
            self.resultMessage = nil
            self.presenter?.finishHideResult()
            self.presenter?.nextResult()
        })
    }
    
    func showResultViewWithMessage(_ message: ZPQRTransferAppMessage) {
        _ = Observable<Any>.empty()
            .delay(2.0, scheduler: MainScheduler.instance)
            .subscribe(onCompleted: {
                [weak self] in
                self?.hiddenResultView()
            })
        
        self.resultMessage = message
        self.resultView.setResult(true, amount: message.amount, avatar: message.senderAvatar, displayName: message.senderDisplayName)
        self.resultView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        self.resultView.isHidden = false
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations: {
            self.resultView.transform = CGAffineTransform(scaleX: 1, y: 1)
        }, completion: nil)
    }
    
    func reloadDataAnScrollTo(index indexPath: IndexPath) {
        self.tableView.reloadData()
        self.tableView.scrollToRow(at: indexPath, at: .none, animated: true)
    }
    
    func setAvatar(with url: URL) {
        self.contentView.avatarImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "profile_avatar"))
    }
}

extension ZPReceiveMoneyViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ZPReceiveMoneySenderCell.zp_cellFromSameNib(for: tableView)
        cell.selectionStyle = .none
        cell.setMessage(self.dataArray[indexPath.row])
        
        return cell
    }
}

extension ZPReceiveMoneyViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var view: UIView? = nil
        if totalAmount > 0 && self.dataArray.count > 1 {
            if self.headerSectionView == nil {
                self.headerSectionView = ZPReceiveHeaderSectionView(frame: CGRect(x: 0, y: 0, width: 0, height: self.contentView.frame.width * (1/5)))
            }
            
            tableView.separatorStyle = .singleLine
            self.headerSectionView.setAmount(totalAmount)
            view = self.headerSectionView
        } else {
            view = UIView()
            view?.backgroundColor = .white
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.dataArray.count > 0 ? (self.view.bounds.size.width * (1/5)) : (self.view.bounds.size.width * (1/5) + 25)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ZPReceiveMoneySenderCell.heightCell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        cell.layoutMargins = UIEdgeInsetsMake(0, 0, 0, 0)
    }
}

