//
//  ZPReceiveMoneyInteractor.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import ZaloPayAnalyticsSwift
import ZaloPayProfile

class ZPReceiveMoneyInteractor: NSObject, ZPReceiveMoneyInteractorInputDelegate {

    weak var presenter: ZPReceiveMoneyInteractorOutputDelegate?
    
    var amount: Int64 = 0
    var message: String = ""
    var user: ZaloUserSwift?
    var resultMessage: ZPQRTransferAppMessage?
    var resultMessageQueue = [ZPQRTransferAppMessage]()
    var dataArray = [ZPQRTransferAppMessage]()
    
    func setupInteractor() {
        self.loadUserInfo()
        self.handleNotificationTransferRx()
    }
    
    func handleNotificationTransferRx() {
        _ = self.handleNotificationTransfer()
            .subscribe(onNext: {
                [weak self] (message) in
                self?.handleNotification(message)
            })
    }
    
    func handleNotification(_ message: ZPQRTransferAppMessage) {
        for i in 0..<self.dataArray.count {
            let _message = self.dataArray[i]
            if message.senderUserId == _message.senderUserId {
                
                if  _message.transId == message.transId,
                    _message.progress == message.progress {
                    return
                }
                
                if _message.progress != MoneyTransferProgress.finish.rawValue {
                    self.dataArray[i] = message
                    break
                }
            }
        }
        
        if !self.dataArray.contains(message) {
            for i in 0..<self.dataArray.count {
                let _message = self.dataArray[i]
                if  _message.progress == MoneyTransferProgress.fail.rawValue ||
                    _message.progress == MoneyTransferProgress.cancel.rawValue {
                    self.dataArray[i] = message
                    break
                }
            }
        }
        self.logReceiveMoney()
        DispatchQueue.main.async {
            if !self.dataArray.contains(message) {
                self.dataArray.insert(message, at: 0)
            }
            if let index = self.dataArray.index(of: message) {
                let indexPath = IndexPath(row: index, section: 0)
                self.presenter?.reloadDataAnScrollTo(index: indexPath)
            }
            
            if message.progress == MoneyTransferProgress.success.rawValue {
                self.processMessageSuccess(message)
                ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.moneyreceive_result)
            }
        }
    }
    
    func handleNotificationTransfer() -> Observable<ZPQRTransferAppMessage> {
        
        return Observable.create({ (observer) -> Disposable in
            ZPConnectionManager.sharedInstance()
                .messageSignal
                .take(until: self.rac_willDeallocSignal())
                .filter({
                    value in
                    return value is ZPQRTransferAppMessage
                })
                .subscribeNext({
                    notification in
                    observer.onNext(notification as! ZPQRTransferAppMessage)
                })
            
            return Disposables.create()
        })
    }
    
    func processMessageSuccess(_ message: ZPQRTransferAppMessage) {
        if !self.resultMessageQueue.contains(message) {
            self.resultMessageQueue.append(message)
        }
        self.nextResult()
    }
    
    func logReceiveMoney() {
        var totalSuccess = 0
        for i in 0..<self.dataArray.count {
            let message = self.dataArray[i]
            if  message.progress == MoneyTransferProgress.success.rawValue ||
                message.progress == MoneyTransferProgress.finish.rawValue {
                totalSuccess += 1
            }
        }
        
//        var event: EventAction?
//        switch totalSuccess {
//        case 1:
//            event = ZPAnalyticEventAction.receiveMoney_Received_1
//            break
//        case 2:
//            event = ZPAnalyticEventAction.receiveMoney_Received_2
//            break
//        case 3:
//            event = ZPAnalyticEventAction.receiveMoney_Received_3
//            break
//        case 4:
//            event = ZPAnalyticEventAction.receiveMoney_Received_4
//            break
//        case 5:
//            event = ZPAnalyticEventAction.receiveMoney_Received_5
//            break
//        case 6:
//            event = ZPAnalyticEventAction.receiveMoney_Received_6
//            break
//        case 7:
//            event = ZPAnalyticEventAction.receiveMoney_Received_7
//            break
//        case 8:
//            event = ZPAnalyticEventAction.receiveMoney_Received_8
//            break
//        case 9:
//            event = ZPAnalyticEventAction.receiveMoney_Received_9
//            break
//        default:
//            break
//        }
//        
//        if totalSuccess > 9 {
//            event = ZPAnalyticEventAction.receiveMoney_Received_More
//        }
//        
//        if let event = event {
//            ZPAppFactory.sharedInstance().eventTracker.trackEvent(event.rawValue)
//        }
    }
    
    func createQRImage() {
        let type = 1
        let zaloPayId: Int64 = Int64(NetworkManager.sharedInstance().paymentUserId)!
        let amount: Int64? = self.amount > 0 ? self.amount : nil
        let messageBase64: String? = self.message.count > 0 ? self.message.base64() : nil
        
        let array = NSMutableArray()
        array.addObjectNotNil(type)
        array.addObjectNotNil(zaloPayId)
        array.addObjectNotNil(amount)
        array.addObjectNotNil(messageBase64)
        
        let checksum = (array.componentsJoined(by: "|").sha256() as NSString).substring(with: NSRange(location: 0, length: 8))
        let params = NSMutableDictionary()
        params.setObjectCheckNil(type, forKey: "type" as NSCopying)
        params.setObjectCheckNil(zaloPayId, forKey: "uid" as NSCopying)
        params.setObjectCheckNil(checksum, forKey: "checksum" as NSCopying)
        params.setObjectCheckNil(amount, forKey: "amount" as NSCopying)
        params.setObjectCheckNil(messageBase64, forKey: "message" as NSCopying)
        
        presenter?.bindingQRImage(params: params)
    }
    
    func loadUserInfo() {

        ZPProfileManager.shareInstance.loadZaloUserProfile()
            .deliverOnMainThread()
            .subscribeNext({
                [weak self] user in
                if let user = user as? ZaloUserSwift {
                    self?.user = user
                    let avatarURL = user.avatar

                    if let url = URL(string: avatarURL) {
                        self?.presenter?.setAvatar(with: url)
                    }
                }
            }, error: {
                [weak self] error in
                self?.presenter?.showDialogError(error as NSError?, handle: nil)
            })
    }
    
    func finishHideResult() {
        resultMessage = nil
    }
    
    func nextResult() {
        if resultMessage == nil,
            self.resultMessageQueue.count != 0 {
            self.resultMessage = self.resultMessageQueue.last
            self.resultMessageQueue.removeLast()
            
            if let resultMessage = resultMessage {
                presenter?.showResultViewWithMessage(resultMessage)
            }
        }
    }
    
    func editAction() {
        
        if self.amount > 0 {
            self.amount = 0
            self.message = ""
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.moneyreceive_touch_clear)
            presenter?.updateView()
            return
        }
        
        presenter?.openInputViewController { [weak self] (amount: Int64, message: String) in
            self?.amount = amount
            self?.message = message
            if amount > 0 {
                ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.moneyreceive_input_amount)
            }
            self?.presenter?.updateView()
        }
    }
    
    func backAction() {

    }
}
