//
//  ZPReceiveMoneySenderCell.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit

class ZPReceiveMoneySenderCell: UITableViewCell {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var amountLabel: UILabel!
    
    lazy var displayNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.sfuiTextRegular(withSize: 16)
        label.zpMainBlackRegular()
        label.textColor = .defaultText()
        self.labelsView.addSubview(label)
        
        label.snp.makeConstraints({ (make) in
            make.leading.trailing.top.equalTo(0)
            make.height.equalTo(label.font.lineHeight)
        })
        
        return label
    }()
    
    lazy var descLabel: UILabel = { // description label
        let label = UILabel()
        label.font = UIFont.sfuiTextRegular(withSize: 14)
        label.zpMainBlackRegular()
        label.textColor = .subText()
        self.labelsView.addSubview(label)
        
        label.snp.makeConstraints({ (make) in
            make.leading.trailing.equalTo(0)
            make.height.equalTo(label.font.lineHeight)
            make.top.equalTo(self.displayNameLabel.snp.bottom).offset(2).priority(.high)
        })
        
        return label
    }()
    
    lazy var labelsView: UIView = { // contains displayNameLabel and descLabel
        let view = UIView()
        self.contentView.addSubview(view)
        view.snp.makeConstraints({ (make) in
            make.left.equalTo(self.avatarImageView.snp.right).offset(10)
            make.right.equalTo(self.amountLabel.snp.left).offset(-10)
            make.top.equalTo(self.avatarImageView.snp.top)
            make.bottom.equalTo(self.avatarImageView.snp.bottom)
        })
        return view
    }()
    
    fileprivate var labelsBottomConstraint: ConstraintMakerEditable?
    
    static var heightCell: CGFloat {
        get {
            return 60
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        avatarImageView.zp_roundRect(Float(avatarImageView.frame.width / 2))
    }
    
    func setDescText(_ text: String) {
        labelsBottomConstraint?.constraint.deactivate()
        let descLabel = self.descLabel
        
        if text.count  > 0 {
            descLabel.text = text
        } else {
            descLabel.snp.removeConstraints()
            descLabel.removeFromSuperview()
            displayNameLabel.snp.removeConstraints()
            displayNameLabel.snp.makeConstraints { (make) in
                make.leading.trailing.equalTo(0)
                make.centerY.equalTo(self.labelsView.snp.centerY)
                make.height.equalTo(displayNameLabel.font.lineHeight)
            }
            displayNameLabel.textColor = UIColor.subText()
        }
    }
    
    func setMessage(_ message: ZPQRTransferAppMessage) {
        self.displayNameLabel.text = message.senderDisplayName
        var descText = ""
        
        switch message.progress {
        case MoneyTransferProgress.scan.rawValue:
            descText = R.string_TransferQRCode_TransferLoading()
        case MoneyTransferProgress.success.rawValue:
            descText = R.string_TransferQRCode_TransferSuccess()
        case MoneyTransferProgress.fail.rawValue:
            descText = R.string_TransferQRCode_TransferFail()
        case MoneyTransferProgress.cancel.rawValue:
            descText = R.string_TransferQRCode_TransferCancel()
        case MoneyTransferProgress.finish.rawValue:
            descText = ""
        default:
            break
        }
        
        self.setDescText(descText)
        if message.progress == MoneyTransferProgress.finish.rawValue {
            self.setAmount(message.amount)
            self.amountLabel.isHidden = false
        } else {
            self.amountLabel.isHidden = true
        }
        
        guard let avatarUrlString = message.senderAvatar else {
            return;
        }
        
        avatarImageView.sd_setImage(with: URL(string: avatarUrlString),
                                    placeholderImage: UIImage.defaultAvatar())
    }
    
    func setAmount(_ amount: Int64) {
        amountLabel.attributedText = NSAttributedString.attributedStringWith(amount: amount, font: UIFont.sfuiTextRegular(withSize: 16), subFont: UIFont.sfuiTextRegular(withSize: 12), color: UIColor.subText(), textAlignment: .right)
        let size = amountLabel.sizeThatFits(CGSize.zero)
        
        amountLabel.snp.updateConstraints({ (make) in
            make.width.equalTo(size.width)
        })
    }
}

