//
//  ZPReceiveResultView.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit

class ZPReceiveResultView: UIView {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var iconImageView: ZPIconFontImageView!
    @IBOutlet weak var balanceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let height: CGFloat = avatarImageView.frame.height
        avatarImageView.zp_roundRect(Float(height / 2))
        avatarImageView.layer.borderColor = UIColor.white.cgColor
        avatarImageView.layer.borderWidth = 0
        //nameLabel.font = UIFont.sfuiTextRegular(withSize: 16)
        nameLabel.zpMainBlackRegular()
        nameLabel.textColor = UIColor.defaultText()
        
        iconImageView.defaultView.font = UIFont.iconFont(withSize: 26)
    }
    
    func setResult(_ success: Bool, amount: Int64, avatar avatarUrl: String, displayName: String) {
        self.setAmount(amount)
        
        avatarImageView.sd_setImage(with: URL(string: avatarUrl), placeholderImage: UIImage(named: "profile_avatar"))
        
        nameLabel.text = displayName
        
        let imageName: String = success ? "general_success" : "general_failed"
        let color = success ? UIColor(hexValue: 0x04b303) : UIColor.red
        iconImageView.setIconFont(imageName)
        iconImageView.setIconColor(color)
    }
    
    func setAmount(_ amount: Int64) {
        
        let text: String = NSNumber(value: amount).formatCurrency()
        let range = text.count > 3 ?
            NSRange(location: text.count - 3, length: 3) :
            NSRange(location: 0, length: 0)
        
        balanceLabel.attributedText = attributedString(text,
                                                       font: UIFont.sfuiTextRegular(withSize: 22), color: UIColor.pay(), linkRange: range)
    }
    
    func attributedString(_ string: String, font: UIFont, color: UIColor, linkRange range: NSRange) -> NSMutableAttributedString {
        
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 1.0
        style.alignment = .center
        
        let attributtes: [NSAttributedStringKey: Any] = [
            NSAttributedStringKey.paragraphStyle: style,
            NSAttributedStringKey.font: font,
            NSAttributedStringKey.foregroundColor: color
        ]
        let _return = NSMutableAttributedString(string: string, attributes: attributtes)
        
        let length: Int = range.length
        if length > 0 {
            _return.addAttribute(NSAttributedStringKey.font, value: UIFont.sfuiTextRegular(withSize: 15), range: range)
        }
        
        return _return
    }
}
