//
//  ZPGiftCodeViewController.swift
//  ZaloPay
//
//  Created by thi la on 2/5/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit

class ZPGiftCodeViewController: BaseViewController {
    let cellIdentifier = "GiftCodeCollectionViewCell"
    var collectionView: UICollectionView?
    private var dataSources = [GiftCodeCellModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = R.string_Voucher_gift_text()
        setupCollectionView()
    }
    
    override func backButtonIcon() -> String {
        return "red_delete"
    }
    
    func setListNotification(_ datas: [ZPGiftPopupModel]) {
        var dataSource = [GiftCodeCellModel]()
        datas.forEach { (model) in
            if let model = modelFromNotification(message: model.message)  {
                dataSource.append(model)
            }
        }
        self.dataSources = dataSource
        self.collectionView?.reloadData()
    }
    
    func modelFromNotification(message: ZPNotifyMessage) -> GiftCodeCellModel? {
    
        if let voucher = message as? ZPVoucherMessage {
            return cellModelForVoucher(voucher)
        }
        
        if let lixi = message as? ZPLiXiWishesMessage {
            return cellModelForLixi(lixi)
        }
        if let cashback = message as? ZPCashbackMessage {
            return cellModelForCashback(cashback)
        }
        
        return nil;
    }
    
    func valueDisplay(_ value: UInt64) -> Float {
        return value >= 1000 ? value.toFloat()/1000 : value.toFloat();
    }
    
    func cellModelForCashback(_ cashback: ZPCashbackMessage) -> GiftCodeCellModel {
        let valueDisplay = self.valueDisplay(cashback.data.amount)
        let format = cashback.data.amount > 1000 ? "%.3f" : "%.0f"
        let info = R.string_Giftcode_Cashback_Title() + " " + String(format: format, valueDisplay)
        return GiftCodeCellModel(imageName: "lixi_vnd", info: info, message: cashback)
    }
    
    func cellModelForLixi(_ lixi: ZPLiXiWishesMessage) -> GiftCodeCellModel {
        return GiftCodeCellModel(imageName: "cauchuc", info: lixi.data.message, message: lixi)
    }
    
    func cellModelForVoucher(_ voucher: ZPVoucherMessage) -> GiftCodeCellModel {
        if voucher.data.valuetype == VoucherType.percent.rawValue {
            let info = R.string_Giftcode_Voucher_Title() + " " + String(format: "%lld%%", voucher.data.value)
            return GiftCodeCellModel(imageName: "quatang", info: info, message: voucher)
        }
        
        let valueDisplay = self.valueDisplay(voucher.data.value.toUInt64())
        let info = R.string_Giftcode_Voucher_Title() + " " + String(format: "%.0fk", valueDisplay)
        return GiftCodeCellModel(imageName: "quatang", info: info,  message: voucher)
    }
    
    
    func setupCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = UICollectionViewScrollDirection.vertical
        layout.sectionInset = UIEdgeInsets(top: 12, left: 12, bottom: 0, right: 12)
        let cellWidth = (self.view.frame.width - 36) / 2
        layout.itemSize = CGSize(width: cellWidth, height:188)
        layout.minimumInteritemSpacing = 12
        layout.minimumLineSpacing = 12
        
        let collectionView = UICollectionView.init(frame: self.view.bounds, collectionViewLayout: layout)
        collectionView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = UIColor.defaultBackground()
        collectionView.dataSource = self
        collectionView.delegate = self
        self.collectionView = collectionView
        self.view.addSubview(collectionView)
        
        collectionView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
            make.left.equalToSuperview()
        }
    }
}

extension ZPGiftCodeViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? GiftCodeCollectionViewCell {
            let data = self.dataSources[indexPath.row]
            cell.setupData(data)
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSources.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
}

extension ZPGiftCodeViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        let notify = dataSources[indexPath.item]
        if notify.message.isUnread {
            notify.message.isUnread = false
            collectionView.reloadItems(at: [indexPath])
        }
        ZPGiftPopupManager.sharedInstance.showPopup(notify.message)
    }
}

enum VoucherType: Int {
    case percent = 1
    case value   = 2
}
