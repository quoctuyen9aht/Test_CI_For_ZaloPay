//
//  ZPRecentTransferMoneyTable.h
//  ZaloPay
//
//  Created by bonnpv on 7/6/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <ZaloPayDatabase/ZPBaseTable.h>

@class UserTransferData;

@interface ZPRecentTransferMoneyTable : ZPBaseTable
- (void)addRecentTransferFriend:(UserTransferData *)user;
- (NSArray *)recentTransferFriends;
- (UserTransferData *)getRecentUserTransfer:(NSString *)paymentUserId;
@end
