//
//  PerUserDataBase+RecentTransferMoney.m
//  ZaloPay
//
//  Created by bonnpv on 7/6/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPRecentTransferMoneyTable.h"
#import ZALOPAY_MODULE_SWIFT

#import <FMDB/FMDatabase.h>
#import <FMDB/FMDatabaseQueue.h>


@implementation ZPRecentTransferMoneyTable

+ (NSArray<NSString*>*) creatingTableQuery {
    NSString* tableRecentTransferFriend = @"CREATE TABLE IF NOT EXISTS RecentTransferFriend(zaloId text, avatar text, name text, phone text,transferMoney text,message text,paymentUserId text primary key, timeupdate long, acountname text, type int);";
    return @[tableRecentTransferFriend];
}

- (void)addRecentTransferFriend:(UserTransferData *)user {
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *query =@"INSERT OR REPLACE INTO RecentTransferFriend(zaloId, avatar, name, phone, transferMoney, message, paymentUserId, timeupdate, acountname, type) VALUES (?, ?, ?, ?, ?, ?, ?, ? , ?, ?)";
        [db executeUpdate:query,user.zaloId, user.avatar, user.displayName, user.phone, user.transferMoney, user.message, user.paymentUserId, @([[NSDate date] timeIntervalSince1970]),user.accountName, @(user.mode)];
    }];
}


- (NSArray *)recentTransferFriends {
    int count = 20;
    int index = 0;
    NSString * query = [NSString stringWithFormat:@"SELECT * FROM RecentTransferFriend ORDER BY timeupdate COLLATE NOCASE DESC  LIMIT %d OFFSET %d", count,index];
    
    NSMutableArray *_return = [NSMutableArray array];
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:query];
        while ([rs next]) {
            UserTransferData *user = [self recentTransferFriendWithResult:rs];
            [_return addObject:user];
        }
        [rs close];
    }];
    return _return;
}

- (UserTransferData *)recentTransferFriendWithResult:(FMResultSet *)rs {
    UserTransferData *user = [[UserTransferData alloc] init];
    user.zaloId = [rs stringForColumn:@"zaloId"];
    user.avatar = [rs stringForColumn:@"avatar"];
    user.displayName = [rs stringForColumn:@"name"];
    user.phone = [rs stringForColumn:@"phone"];
    user.transferMoney = [rs stringForColumn:@"transferMoney"];
    user.message = [rs stringForColumn:@"message"];
    user.paymentUserId = [rs stringForColumn:@"paymentUserId"];
    user.accountName = [rs stringForColumn:@"acountname"];
    user.mode = [rs intForColumn:@"type"];
    return user;
}

- (UserTransferData *)getRecentUserTransfer:(NSString *)paymentUserId {
    __block UserTransferData *_return = nil;
    [self.db.dbQueue inDatabase:^(FMDatabase * _Nonnull db) {
        NSString * query = @"SELECT * FROM RecentTransferFriend Where paymentUserId = ?";
        FMResultSet *rs = [db executeQuery:query, paymentUserId];
        if ([rs next]) {
            _return = [self recentTransferFriendWithResult:rs];
        }
    }];
    return _return;
}

@end
