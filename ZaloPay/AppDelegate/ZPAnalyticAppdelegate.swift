//
//  ZPAnalyticService.swift
//  ZaloPay
//
//  Created by tridm2 on 10/4/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import GoogleReporter
//import GoogleMaps
import Fabric
import Crashlytics
import AdSupport
import ZaloPayAnalyticsSwift
import ZaloPayModuleProtocolsObjc
import Firebase
import ZaloPayConfig

enum ZPAnalyticTrackOpenAppType: Int {
    case icon = 1
    case otherApp
}

protocol ZPAnalyticTrackOpenAppProtocol {
    func trackAppIcon()
    func trackOtherApp(from source: String?)
}

@objcMembers
class ZPAnalyticAppdelegate: ZPBaseAppDelegate {
    static let sharedInstance = ZPAnalyticAppdelegate()
    private var isOpenFromIcon: Bool = false
    let dbInstance = ZPTrackingAppTransidTable(db: PerUserDataBase.sharedInstance())
    
    override func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [AnyHashable : Any]?) -> Bool {
        self.trackOpenAppIcon(from: launchOptions)
//        self.configLogLocal()
        self.configGoogleAnalytic()
        self.configAppsFlyer()
        self.configServerLog()
        self.registerAllTrackings()
        self.initCrashlytics()
        
        // intialize analytic module
        self.initializeAnalyticModule()
        
        // init firebase
        self.setupFirebase()
        
        return true;
    }
    
    private func setupFirebase() {
        let configFileName = kRemoteConfigFirebase
       
        guard let filePath = Bundle.main.path(forResource: configFileName, ofType: "plist"),
            let fileopts = FirebaseOptions(contentsOfFile: filePath) else {
            return FirebaseApp.configure()
        }
        
        FirebaseApp.configure(options: fileopts)
    }
    
    private func trackOpenAppIcon(from options:[AnyHashable : Any]?) {
        // check options
        guard options == nil else {
            return
        }
        
        // App open directly
        isOpenFromIcon = true
    }
//
//    func configLogLocal() {
//        ZPTrackingEvent.instance.logEvent = ZPAppFactory.sharedInstance()
//    }
    
    private func configServerLog() {
        let durationSendLog = TimeInterval(ZPApplicationConfig.getServerLogConfig()?.getTimeSendLog() ?? 120)
        let network = NetworkManager.sharedInstance()
        ZPServerEventLog.durationWriteLogs = durationSendLog
        let appsflyerId = AppsFlyerTracker.shared().getAppsFlyerUID()
        let idfaString = ASIdentifierManager.shared().advertisingIdentifier.uuidString
        ZPAnalyticSwiftConfig.setAppflyerId(with: appsflyerId)
        ZPAnalyticSwiftConfig.setAdvertisingId(with: idfaString)
        ZPAnalyticSwiftConfig.setDeviceId(with: UIDevice.zmDeviceUUID().lowercased())
        ZPAnalyticSwiftConfig.setNetworkDependence(network)
        ZPAnalyticSwiftConfig.setEnvironment(use: kAnalyticEnvironment.rawValue)
        let canUseSilos = ZPApplicationConfig.silosConfig()?.enable ?? false
        ZPAnalyticSwiftConfig.canUseGRPC(canUseSilos, path: kURLNavigatorLog)
    }
    
    private func configAppsFlyer() {
        AppsFlyerTracker.shared().appsFlyerDevKey = "k5nfUwyL6J6Fe6jvBbPcKG"
        AppsFlyerTracker.shared().appleAppID = "1112407590"
        #if DEBUG
            AppsFlyerTracker.shared().isDebug = true
        #endif
    }

    func registerAllTrackings() {
        let eventTrack = ZPTrackerManager()
        eventTrack.registerTracker(ZPServerEventLog())
        eventTrack.registerTracker(ZPAnswersLog())
        eventTrack.registerTracker(ZPGoogleAnalytic())
        #if DEBUG
            eventTrack.registerTracker(ZPDefaultEventLog())
        #endif
        if ZPAnalyticSwiftConfig.isUsingGRPC() {
            let analyticGRPC = ZPLogGRPC()
            eventTrack.registerTracker(analyticGRPC)
        }
        ZPTrackingHelper.shared().eventTracker = eventTrack
    }
    
    func configGoogleAnalytic() {
//        GMSServices.provideAPIKey(GMSService_API)
        var trakeId: String = kTrackerId
        #if DEBUG || TARGET_IPHONE_SIMULATOR
            // hard code dùng trackingid của sandbox cho debug mode + simulator
            trakeId = "UA-79910400-2"
        #endif
        GoogleReporter.shared.configure(withTrackerId: trakeId)
    }
    
    func initCrashlytics() {
        Fabric.with([Crashlytics.self(), Answers.self()])
    }
    
    func setLoginUserOnCrashlytics(_ data: [String: Any]) {
        if data.count == 0 {
            return
        }
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        let uid = data.value(forKey: "uid", defaultValue: "")
        if uid.isEmpty.not {
            Crashlytics.sharedInstance().setUserIdentifier(uid)
        }
        
        let email = data.value(forKey: "email", defaultValue: "")
        if email.isEmpty.not {
            Crashlytics.sharedInstance().setUserEmail(email)
        }
        
        let name = data.value(forKey: "name", defaultValue: "")
        if name.isEmpty.not {
            Crashlytics.sharedInstance().setUserName(name)
        }
    }
    
    func initializeAnalyticModule() {
        let orderTracking = ZPAOrderTracking(database: dbInstance)
        ZPAppFactory.sharedInstance().registerOrderTracking(orderTracking)
        //[ZPAppFactory sharedInstance].orderTracking
    }
    
    override func applicationDidBecomeActive(_ application: UIApplication) {
        AppsFlyerTracker.shared().trackAppLaunch()
    }
    
    override func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        AppsFlyerTracker.shared().registerUninstall(deviceToken)
    }

}

extension ZPAnalyticAppdelegate: ZPAnalyticTrackOpenAppProtocol {
    
    /// Using track app from icon to home
    func trackAppIcon() {
        guard isOpenFromIcon else {
            return
        }
        
        defer {
            isOpenFromIcon = false
        }
        
        var infor: JSON = [:]
        infor["opentype"] = ZPAnalyticTrackOpenAppType.icon.rawValue
        infor["userid"] = NetworkManager.sharedInstance().paymentUserId
        infor["source"] = "openFromIcon"
    
        ZPTrackingHelper.shared().eventTracker?.writeLogOpenApp(log: infor as NSDictionary)
    }
    
    /// Open app from other apps
    ///
    /// - Parameter source: scheme url open app
    func trackOtherApp(from source: String?) {
        var infor: JSON = [:]
        infor["opentype"] = ZPAnalyticTrackOpenAppType.otherApp.rawValue
        infor["userid"] = NetworkManager.sharedInstance().paymentUserId
        infor["source"] = source
        
        ZPTrackingHelper.shared().eventTracker?.writeLogOpenApp(log: infor as NSDictionary)
    }
}
