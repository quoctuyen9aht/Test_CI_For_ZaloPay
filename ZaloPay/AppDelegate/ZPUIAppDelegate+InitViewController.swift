//
//  AppDelegateService+InitViewController.swift
//  ZaloPay
//
//  Created by tridm2 on 10/4/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayAnalyticsSwift
import ZaloPayWeb

fileprivate enum BarType: Int {
    case home = 0,
    transaction,
    promotion,
    profile
    
    var iconName: String {
        switch self {
        case .home:
            return "tab_home"
        case .transaction:
            return "tab_history"
        case .profile:
            return "tab_personal"
        case .promotion:
            return "tab_promotion"
        }
    }
    
    var name: String {
        switch self {
        case .home:
            return R.string_Tabbar_Home()
        case .transaction:
            return R.string_Tabbar_Transaction()
        case .profile:
            return R.string_Tabbar_Profile()
        case .promotion:
            return R.string_Tabbar_Promotion()
        }
    }
    
    var title: BarButtonIconTitle {
        let n = UILabel.iconCode(withName: iconName) ?? ""
        let s = UILabel.iconCode(withName: "\(iconName)_active") ?? ""
        return BarButtonIconTitle(n, s)
    }
}
fileprivate typealias BarButtonIconTitle = (normal: String, selected: String)
extension ZPUIAppDelegate {
    fileprivate func createBarItem(with type: BarType) -> UITabBarItem {
        let img = self.loadImage(with: type.title.normal)
        let imgs = self.loadImage(with: type.title.selected)
        let item = UITabBarItem(title: type.name, image: img, selectedImage: imgs?.withRenderingMode(.alwaysOriginal))
        item.badgeValue = nil
        item.titlePositionAdjustment = UIOffset(horizontal: 0.0, vertical: -2.0)
        item.setTitleTextAttributes([.foregroundColor: UIColor.subText()], for: .normal)
        item.setTitleTextAttributes([.foregroundColor: UIColor.zaloBase()], for: .selected)
        return item;
    }
    
    func showMainViewController() {
        let navHome = self.createNavi(vc: ZPHomeRouter.createHomeViewController())
        navHome.eventTracking = ZPAnalyticEventAction.home_touch_home
        navHome.tabBarItem = self.createBarItem(with: .home)
        
        let navProfile = self.createNavi(vc: ZPProfileViewController())
        navProfile.eventTracking = ZPAnalyticEventAction.home_touch_profile
        navProfile.tabBarItem = self.createBarItem(with: .profile)
        
        guard let listTransactionHistory = ZPReactNativeAppRouter().listTransactionHistoryViewController() else{
            return
        }
        _ = listTransactionHistory.rx.methodInvoked(#selector(UIViewController.viewWillAppear(_:))).takeUntil(self.rx.deallocated).bind {_ in
            ZPTrackingHelper.shared().trackScreen(withName: ZPTrackerScreen.Screen_iOS_TransactionLog)
        }
        let promotion = self.createNavi(vc: listTransactionHistory)
        promotion.eventTracking = ZPAnalyticEventAction.home_touch_transactionlog
        promotion.tabBarItem = self.createBarItem(with: .transaction)
        
        let url = String(format: "%@?userid=%@", kUrlPromotion, NetworkManager.sharedInstance().paymentUserId)
        let navWeb = self.createNavi(vc: ZPPromotionTabWebApp(url: url, webHandle: ZPWebHandle()))
        navWeb.eventTracking = ZPAnalyticEventAction.home_touch_promotion
        navWeb.tabBarItem = self.createBarItem(with: .promotion)
        
        let tabbarController = TabbarViewController()
        tabbarController.setViewControllers([navHome,promotion,navWeb,navProfile], animated: true)
        tabbarController.selectedIndex = 0
        self.window?.rootViewController = tabbarController;
        
        DispatchQueue.main.async { () in
            ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.home_launched)
            //            ZPTrackingEvent.instance.trackEvent(ZPAnalyticEventAction.home_launched)
        }
        
    }
    
    fileprivate func createNavi(vc: UIViewController) -> UINavigationController {
        let navi = UINavigationController.init(navigationBarClass: ZPNavigationBar.self, toolbarClass: nil)
        navi.viewControllers = [vc]
        navi.defaultNavigationBarStyle()
        return navi
    }
    
    // MARK: helper
    fileprivate func loadImage(with text: String) -> UIImage? {
        let u = UserDefaults.standard
        let defaultSize = CGSize(width: 25, height: 25)
        let appversion = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String) ?? ""
        let key = "\(appversion)_\(text)"
        
        func createNew() -> UIImage? {
            guard let font = UIFont.zaloPay(withSize: 25) else {
                return nil
            }
            guard let result = UIImage.drawText(text, font: font, size: defaultSize, color: .zaloBase()) else {
                return nil
            }
            DispatchQueue.global(qos: .background).async {
                let d = UIImagePNGRepresentation(result)
                u.set(d, forKey: key)
                u.synchronize()
            }
            return result
        }
        guard let dU = u.data(forKey: key) else { return createNew() }
        return UIImage.render(using: dU, by: defaultSize) //UIImage(data: dU)?.scaled(to: defaultSize)
    }
}

