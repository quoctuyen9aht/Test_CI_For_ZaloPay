//
//  ZPAppDelegateConstants.h
//  ZaloPay
//
//  Created by tridm2 on 10/4/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ZaloPayAppManager/ReactConfig.h>
#import <ZaloSDK/ZaloSDK.h>


#ifndef ZPAppDelegateConstants_h
#define ZPAppDelegateConstants_h

extern int platformInfoInterval;

extern NSString *const kPerUserDataBaseScheme;
extern NSString *const kGlobalDataBaseScheme;

extern NSInteger appDelegateServiceReactConfig;
//extern BOOL disableDownloadReactNativeApp;
//extern BOOL disablePaymentConnector;
extern NSString *kBaseUrl;
extern NSString *kUploadUrl;
extern NSString *kUploadClientLog;
extern NSString *kUrlPromotion;
extern NSString *kUrlVoucher;
extern NSString *kUrlTopUp;
extern NSString *kUrlGateway;
extern NSString *kLixiApiUrl;
extern NSString *termsOfUseUrl;
extern NSString *faqUrl;
extern NSString *kUrlQuickPaySupport;
extern NSString *kTrackerId;

extern NSString *kSchemePayment;
extern NSString *kBIDVHelpUrl;
extern NSString *supportCenterUrl;
extern NSString *kURLNavigatorLog;

extern float socketPingInterval;
extern float socketPingTimout;
extern NSString *GMSService_API;
extern double delayUpTransacLogServer;
extern enum ZAZaloSDKAuthenType typeLogin;
extern BOOL usingLocalConfig;
extern NSString *kUrlFilter;

typedef NS_ENUM(NSInteger, ZaloPayEnvironment) {
    ZaloPayEnvironmentDevelopment = 0,
    ZaloPayEnvironmentSandbox = 1,
    ZaloPayEnvironmentStaging = 2,
    ZaloPayEnvironmentProduction = 3
};

extern enum ZaloPayEnvironment kAnalyticEnvironment;
extern NSString *kRemoteConfigFirebase;
//AppEvironment *environment;

#endif /* ZPAppDelegateConstants_h */

