//
//  ZPNetworkService.swift
//  ZaloPay
//
//  Created by tridm2 on 10/3/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayProfile
import ZaloPayProfilePrivate
import ZaloPayAnalyticsSwift

class ZPNetworkAppdelegate: ZPBaseAppDelegate {
    override func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [AnyHashable : Any]?) -> Bool {
        NetworkState.sharedInstance()
        // start monitor network.
        // intialize red packet module
        initializeRedPacketModule()
        registerAllMessageHandler()
        registerHandleNetworkError()
        return true
    }
    
    func registerAllMessageHandler() {
//        ApplicationState.sharedInstance.pingModul.registerMessageSignal()
        ZPPingServerModule().registerMessageSignal()
        ZPNotificationManager.sharedInstance().registerMessageSignal()
        ZPNotificationService.sharedInstance().registerNotificationVibrate()
        ZPNotificationService.sharedInstance().registerMessageSignal()
        ZPGiftPopupManager.sharedInstance.observerMessageSignal()
    }
    
    func isSessionExpire(_ errorCode: Int) -> Bool {
        return errorCode == ZALOPAY_ERRORCODE_ZALO_LOGIN_FAIL.rawValue ||
            errorCode == ZALOPAY_ERRORCODE_ZALO_LOGIN_EXPIRE.rawValue ||
            errorCode == ZALOPAY_ERRORCODE_TOKEN_INVALID.rawValue ||
            errorCode == ZALOPAY_ERRORCODE_UM_TOKEN_NOT_FOUND.rawValue ||
            errorCode == ZALOPAY_ERRORCODE_UM_TOKEN_EXPIRE.rawValue;
    }
    
    func isAccountSuppend(_ errorCode: Int) -> Bool {
        return errorCode == ZALOPAY_ERRORCODE_ZPW_ACCOUNT_SUSPENDED.rawValue || errorCode == ZALOPAY_ERRORCODE_USER_IS_LOCKED.rawValue
    }
    
    func registerHandleNetworkError() {
        NetworkManager.sharedInstance().prehandleError {
            [unowned self] (errorCode: Int32, message: String?) in
            self.handleErrorCode(Int(errorCode), errorMessage: message)
        }
        
        NetworkManager.sharedInstance().accessTokenChangeHandle { (accesstoken) in
            ZPAnalyticSwiftConfig.setAccessToken(with: accesstoken)
            LoginManagerSwift.sharedInstance.updateAccessToken(accessToken: accesstoken!)
            //LoginManager.sharedInstance().updateAccessToken(accesstoken)
        }
    }
    
    func handleErrorCode(_ errorCode: Int, errorMessage message: String?) {
        if isSessionExpire(errorCode) {
            handleSessionExpire(errorCode)
        } else if errorCode == ZALOPAY_ERRORCODE_MAINTAIN.rawValue {
            handleServerMaintain(message)
        }
        
        if isAccountSuppend(errorCode) {
            handleAccountSuppend(message)
        }
    }
    
    func handleSessionExpire(_ errorCode: Int) {
        LoginManagerSwift.sharedInstance.showDialogLogout(message: R.string_Message_Session_Expire())
    }
    
    func handleServerMaintain(_ message: String?) {
        var msg = message ?? ""
        if msg.count == 0 {
            msg = R.string_Message_Server_Maintain()
        }
        LoginManagerSwift.sharedInstance.showDialogLogout(message: msg)
    }
    
    func handleAccountSuppend(_ message: String?) {
        let phone = ZPProfileManager.shareInstance.userLoginData?.phoneNumber ?? ""
        let zaloPayId = ZPProfileManager.shareInstance.userLoginData?.accountName ?? ""
        
        var msg: String
        switch (zaloPayId.count > 0, phone.count > 0) {
        case (true, _):
            msg = String(format: R.string_Message_Lock_Account_Format(), zaloPayId)
        case (_, true):
            msg = String(format: R.string_Message_Lock_Account_Format(), phone)
        default:
            msg = message ?? R.string_Message_Lock_Account()
        }
        
        LoginManagerSwift.sharedInstance.showDialogLogout(message: msg)
    }
    
    func initializeRedPacketModule() {
        let rpNetworkApi = ZPRPNetwork(baseUrl: kLixiApiUrl)
        let db = ZPLixiNotificationTable.init(db: PerUserDataBase.sharedInstance())
        let rpManager = ZPRPManager(network: rpNetworkApi, db: db)
        ZPAppFactory.sharedInstance().registerRedPacketManager(rpManager)        
    }
}
