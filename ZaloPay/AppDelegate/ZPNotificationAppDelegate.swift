//
//  ZPPushNotificationService.swift
//  ZaloPay
//
//  Created by tridm2 on 10/3/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import CocoaLumberjackSwift
@objcMembers
class ZPNotificationAppDelegate: ZPBaseAppDelegate {
    static let sharedInstance = ZPNotificationAppDelegate()
    
    private lazy var cacheDataTable: ZPCacheDataTable = ZPCacheDataTable(db: GlobalDatabase.sharedInstance())
    
    override func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [AnyHashable : Any]?) -> Bool {
        handleLauchOptions(launchOptions)
        return true
    }
    
    override func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        let token: String = string(withHexBytes: deviceToken)
        let token: String = deviceToken.hexEncodedString()
        DDLogInfo("deviceToken data : \(deviceToken)")
        DDLogInfo("deviceToken string : \(token)")
        cacheDataTable.cacheDataUpdateValue(token, forKey: kApplePushNotifyToken)
        LoginManagerSwift.sharedInstance.connectToWebsocket()
    }
    
//    func string(withHexBytes data: Data) -> String {
//        var stringBuffer = ""
//        stringBuffer.reserveCapacity(data.count * 2)
//        let dataBuffer = [UInt8](data)
//        for i in 0..<data.count {
//            stringBuffer.append(String(format: "%02X", UInt(dataBuffer[i])))
//        }
//        return stringBuffer
//    }
    
    override func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        ZPAppleNotificationServices.sharedInstance.handleRemoveNotification(userInfo as NSDictionary, fromLaunch: false)
    }
    
    func handleLauchOptions(_ launchOptions: [AnyHashable: Any]?) {
        let notifyInfo = launchOptions?.value(forKey: UIApplicationLaunchOptionsKey.remoteNotification, defaultValue: [AnyHashable : Any]())
        if notifyInfo?.count == 0 {
            return
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { // 1 second
            () in
            
            if let info = notifyInfo as NSDictionary? {
                ZPAppleNotificationServices.sharedInstance.handleRemoveNotification(info, fromLaunch: true)
            }

        }
    }
    
    func registerPushNotify() {
        let application = UIApplication.shared
        application.registerUserNotificationSettings(UIUserNotificationSettings(types: ([.sound, .alert, .badge]), categories: nil))
        application.registerForRemoteNotifications()
    }
}
