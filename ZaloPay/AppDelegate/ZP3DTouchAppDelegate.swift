//
//  ZP3DTouchAppDelegate.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 4/18/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayProfile
import RxSwift

enum TouchActions: String {
    case empty = ""
    case scanQRCode = "scanQRCode"
    case historyTransactions = "historyTransactions"
}

class ZP3DTouchAppDelegate: ZPBaseAppDelegate {
    private let disposeBag = DisposeBag()
    private var touchActionsType:TouchActions = .empty
    override func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [AnyHashable : Any]?) -> Bool {
        NotificationCenter.default.rx
            .notification(Notification.Name.CloseAuthenScreen)
            .bind
            { [weak self](_) in
                if self?.touchActionsType != .empty {
                    self?.handle3DTouchAction(type: self?.touchActionsType ?? .empty)
                    self?.touchActionsType = .empty
                }
            }.disposed(by: disposeBag)
        return true;
    }
    @available(iOS 9.0, *)
    override func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        ZPProfileManager.shareInstance.userLoginData?.isFrom3DTouch = true
        guard let type = TouchActions(rawValue: shortcutItem.type) else {
            completionHandler(false)
            return
        }
        touchActionsType = type
        if ZPSettingsConfig.sharedInstance().authenticationOpen.not {
            handle3DTouchAction(type: type)
        }
        completionHandler(true)
    }
    
    func handle3DTouchAction(type:TouchActions) {
        if (ZPProfileManager.shareInstance.userLoginData?.isFrom3DTouch ?? false).not {
            return
        }
        let userid = ZPProfileManager.shareInstance.userLoginData?.paymentUserId ?? ""
        if userid.isEmpty {
            return
        }
        guard let tabbar = UIApplication.shared.keyWindow?.rootViewController as? TabbarViewController else {
            return
        }
        UINavigationController.currentActiveNavigationController()?.topViewController?.dismiss(animated: true, completion: nil)
        
        if ZaloPayWalletSDKPayment.sharedInstance().checkIsCurrentPaying() {
            ZaloPayWalletSDKPayment.sharedInstance().forceCloseSDK()
        }
        ZPAppFactory.sharedInstance().hideAllHUDs(for: tabbar.view)
        self.trackAction3DTouch(actionType: type)
        switch type {
        case .scanQRCode:
            if let qrCodeCurrent = getQRCodeVCCurrent(tabbar: tabbar) {
                qrCodeCurrent.navigationController?.popToViewController(qrCodeCurrent, animated: true)
            } else {
                if let navs = tabbar.viewControllers as? [UINavigationController] {
                    for nav in navs {
                        nav.popToRootViewController(animated: true)
                    }
                }
                tabbar.toolbar(willHidden: false)
                tabbar.selectedIndex = TabIndex.Home.rawValue
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                  ZPCenterRouter.launch(routerId: .qrScan, from: self.getRootVC(tabbar: tabbar))
                }
            }
        case .historyTransactions:
            if let navs = tabbar.viewControllers as? [UINavigationController] {
                for nav in navs {
                    nav.popToRootViewController(animated: true)
                }
            }
            tabbar.toolbar(willHidden: false)
            tabbar.selectedIndex = TabIndex.Transaction.rawValue

        case .empty: break
            
        }
    }
    func getQRCodeVCCurrent(tabbar:TabbarViewController) -> UIViewController? {
        if let navHome = tabbar.viewControllers?.first as? UINavigationController, tabbar.selectedIndex == 0 {
            for vc in navHome.viewControllers {
                if vc.isKind(of: ZPQRPayViewController.self) {
                    return vc
                }
            }
        }
        return nil
    }
    func getRootVC(tabbar:TabbarViewController) -> ZPHomeViewController {
        if let navHome = tabbar.viewControllers?.first as? UINavigationController, tabbar.selectedIndex == 0 {
            for vc in navHome.viewControllers {
                if let vc = vc as? ZPHomeViewController {
                    return vc
                }
            }
        }
        return ZPHomeViewController()
    }
    
    func trackAction3DTouch(actionType:TouchActions) {
        switch actionType {
        case .scanQRCode:
            ZPTrackingHelper.shared().trackEvent(.touch3d_qr)
        case .historyTransactions:
            ZPTrackingHelper.shared().trackEvent(.touch3d_transactionlogs)
        default:
            break
        }
    }
}
