//
//  ZPAppDelegate.swift
//  ZaloPay
//
//  Created by thi la on 9/28/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import UIKit

@UIApplicationMain
class ZPAppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    let services = ZPAppDelegateDispatcher()
    
    private (set) lazy var trackerOpenApp: ZPAnalyticTrackOpenAppProtocol? = {
        return services.allService.compactMap({$0 as? ZPAnalyticTrackOpenAppProtocol}).first
    }()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        _ = services.application(application, didFinishLaunchingWithOptions: launchOptions)
        return true
    }
    
    // annotation is nil -> use Any? (ref: https://stackoverflow.com/questions/26178324/application-openurl-in-swift)
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any?) -> Bool {
        return services.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation ?? [:])
    }
  
    @available(iOS 9.0, *)
    public func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return services.application(app, open: url, sourceApplication: "", annotation: [:])
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        services.applicationDidEnterBackground(application)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
      services.applicationWillResignActive(application)
    }
   
    func applicationDidBecomeActive(_ application: UIApplication) {
        services.applicationDidBecomeActive(application)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        services.application(application, didRegisterForRemoteNotificationsWithDeviceToken: deviceToken)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        services.application(application, didReceiveRemoteNotification: userInfo)
    }
    
    func application(_ application: UIApplication, shouldAllowExtensionPointIdentifier extensionPointIdentifier: UIApplicationExtensionPointIdentifier) -> Bool {
        if (extensionPointIdentifier == .keyboard) {
            return false
        }
        return true
    }
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        return services.application(application,continue : userActivity,restorationHandler : restorationHandler)
    }
    @available(iOS 9.0, *)
    public func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        services.application(application, performActionFor: shortcutItem, completionHandler: completionHandler)
    }
}
