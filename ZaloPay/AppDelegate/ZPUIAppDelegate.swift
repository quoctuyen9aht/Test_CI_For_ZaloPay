//
//  AppDelegateService.swift
//  ZaloPay
//
//  Created by tridm2 on 10/4/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import CocoaLumberjackSwift
import ZaloPayAnalyticsSwift
import ZaloPayCommonSwift
import ZaloPayProfile
import ZaloPayModuleProtocols
import ZaloPayConfig
import ZaloPayWeb

@objcMembers
class ZPUIAppDelegate: ZPBaseAppDelegate {
    var window: UIWindow? {
        let appdelegate: ZPAppDelegate? = appDelegate()
        return appdelegate?.window
    }
    
    static let sharedInstance = ZPUIAppDelegate()
    fileprivate let disposeBag = DisposeBag()
    fileprivate let transactionHistoryManager = TransactionHistoryManager()
    
    override func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [AnyHashable : Any]?) -> Bool {
        ZPWUtils.configZPUserAgent()
        ApplicationState.sharedInstance.state = .anonymous
        UIApplication.shared.setStatusBarStyle(.lightContent, animated: false)
        ReactConfig.setConfigType(ReactConfigType(rawValue: Int(appDelegateServiceReactConfig))!)
        self.observeResourceState()
        if let appdelegate = appDelegate() {
            appdelegate.window = UIWindow(frame: UIScreen.main.bounds)
            if LoginManagerSwift.sharedInstance.shouldShowJailbrokenWarning() {
                LoginManagerSwift.sharedInstance.showJailbreakWarning()
            } else {
                LoginManagerSwift.sharedInstance.start()
            }

            appdelegate.window?.makeKeyAndVisible()
        }
        ZPWalletManagerSwift.sharedInstance.preloadAppDataAtLaunchTime()
        ZPTrackingHelper.shared().trackEvent(.app_launch)
        return true
    }
    
    fileprivate func appDidEnterBackground() {
        //Tracking time close noti
        let log = ZPAppleNotificationServices.sharedInstance.notiReceiveLast?.getInfor(from: .Terminate)
        ZPTrackingHelper.shared().eventTracker?.writeLogNotification(log: log as NSDictionary?)
        ZPAppleNotificationServices.sharedInstance.notiReceiveLast = nil
    }

    func startBackgroudProcessWhenLoginSuccessful() {
        let historyManager = self.transactionHistoryManager
        NSObject.cancelPreviousPerformRequests(withTarget: historyManager, selector: #selector(historyManager.updateTransactionLogFromServer), object: nil)
        historyManager.perform(#selector(historyManager.updateTransactionLogFromServer), with: nil, afterDelay: 10.0)
    }

    override func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return ZPSchemeUrlHandle.sharedInstance.handleUrlSchemes(schemesUrl: url, sourceApplication: sourceApplication ?? "")
    }

    override func applicationWillResignActive(_ application: UIApplication) {
        let historyManager = self.transactionHistoryManager
        if ApplicationState.sharedInstance.state == .authenticated {
            NSObject.cancelPreviousPerformRequests(withTarget: historyManager, selector: #selector(historyManager.updateTransactionLogFromServer), object: nil)
        }
    }

    override func applicationDidBecomeActive(_ application: UIApplication) {
        if ApplicationState.sharedInstance.state == .authenticated {
            self.startBackgroudProcessWhenLoginSuccessful()
        }
    }
    
    override func application(_ application: UIApplication,continue userActivity: NSUserActivity) -> Bool {
        guard userActivity.activityType == NSUserActivityTypeBrowsingWeb,
            let url = userActivity.webpageURL else {
                return false
        }
        
        ZPUniversalLinkHandler.sharedInstance.handleUniversalLink(url.absoluteString)
        return true
    }
    // MARK: Internal
    func appDelegate() -> ZPAppDelegate? {
        return UIApplication.shared.delegate as? ZPAppDelegate
    }

    func setHiddenTabbar(_ hiddenTabbar: Bool) {
        let tabbarController = self.window?.rootViewController as? TabbarViewController
        tabbarController?.toolbar(willHidden: hiddenTabbar)
    }
    
    func loadConfig() {
        updateConfig()
        observerConfigChange()
    }
    
    func updateConfig() {
        let manager = ReactNativeAppManager.sharedInstance()
        let configWithdrawApp = manager.getApp(ZPConstants.withdrawAppId)
        let intenalPath = manager.getRunFolder(configWithdrawApp) ?? ""
        let applicationState = ApplicationState.sharedInstance
        let usingLocal = usingLocalConfig.boolValue
        
        applicationState.loadConfig(usingLocal: usingLocal, path: intenalPath).subscribe(onError: { error in
            let errorCode: Int
            defer {
               ZPTrackingHelper.shared().eventTracker?.trackParseOrVerifyConfigError(errorCode)
            }
            guard let e = error as? ParseError else {
                errorCode = ParseError.invalidContent.code
                return
            }
            errorCode = e.code
        }, onDisposed: {
            NetworkManager.sharedInstance().configConnectorApi(applicationState.zalopayConfig())
        }).disposed(by: self.disposeBag)
    }
    
    func observerConfigChange() {
        let operationQueue = OperationQueue()
        operationQueue.qualityOfService = .userInitiated
        let bgScheduler = OperationQueueScheduler(operationQueue: operationQueue)
        let appModel = ReactNativeAppManager.sharedInstance().getApp(withdrawAppId)
        let toSkip_withdrawApp = appModel.appState == ReactNativeAppStateReadyToUse ? 1 : 0
        
        appModel.rx.observeWeakly(ReactNativeAppState.self, "appState")
            .observeOn(bgScheduler)
            .skip(toSkip_withdrawApp)
            .filter( { state in return state == ReactNativeAppStateReadyToUse} )
            .take(1)
            .subscribe(onCompleted: { [weak self] in
                self?.updateConfig()
            }).disposed(by: self.disposeBag)
    }
    
    func observeResourceState() {
        loadConfig()
        loadZaloPayFont()
        loadResourceString()
        let appModel = ReactNativeAppManager.sharedInstance().getApp(reactInternalAppId)
        let toSkip = appModel.appState == ReactNativeAppStateReadyToUse ? 1 : 0
        appModel.rx.observe(ReactNativeAppState.self, "appState")
            .skip(toSkip)
            .filter( { state in return state == ReactNativeAppStateReadyToUse} )
            .throttle(1.0, scheduler: MainScheduler.instance)
            .bind(onNext: { [weak self] _ in
                self?.loadZaloPayFont()
                ReactFactory.sharedInstance().reloadAllView(withAppId: reactInternalAppId)
            }).disposed(by: disposeBag)
    }
    
    func loadZaloPayFont() {
        let appModel = ReactNativeAppManager.sharedInstance().getApp(reactInternalAppId)
        let intenalPath = ReactNativeAppManager.sharedInstance().getRunFolder(appModel) ?? ""
        var fontPath = "\(intenalPath)/fonts/zalopay.ttf"
        var jsonPath = "\(intenalPath)/fonts/zalopay.json"
        
        let fileManager = FileManager.default;
        var shouldLoadFromLocal = !fileManager.fileExists(atPath: fontPath) ||  !fileManager.fileExists(atPath: jsonPath)
        if shouldLoadFromLocal || usingLocalConfig.boolValue {
            fontPath = Bundle.main.path(forResource: "zalopay", ofType: "ttf") ?? ""
            jsonPath = Bundle.main.path(forResource: "zalopay", ofType: "json") ?? ""
        }
        let urlFont = URL(fileURLWithPath: fontPath)
        var data = try? Data(contentsOf: urlFont)
        defer {
            data = nil
        }
        UIFont.register(with: data)
        guard let jsonString = try? String(contentsOfFile: jsonPath, encoding: String.Encoding.utf8) else {
            DDLogError("load json string error: /fonts/zalopay.json")
            return
        }
        guard let jsonData = jsonString.data(using: .utf8) else {
            return
        }
        guard let json = try? JSONSerialization.jsonObject(with: jsonData, options: []) else {
            DDLogError("not json file: /fonts/zalopay.json")
            return
        }
        UILabel.loadIconName(fromDic: json as! [AnyHashable : Any])
    }
    
    func loadResourceString() {
        if let fromJson = loadResourceFromJson("resource_string", fileType: "json") {
            R.loadText(fromDict: fromJson)
        }
    }

//    func loadResourceFromPlist(_ fileName: String, fileType: String) -> [AnyHashable : Any]? {
//        let resource_string = filePathFrom(fileName, fileType:fileType)
//        return (NSDictionary(contentsOfFile: resource_string) as? [AnyHashable : Any])
//    }
    
    func loadResourceFromJson(_ fileName: String, fileType: String)-> [AnyHashable : Any]? {
        let resource_string = filePathFrom(fileName, fileType:fileType)
        guard let jsonString = (try? NSString.init(contentsOfFile: resource_string, encoding: String.Encoding.utf8.rawValue)),
            let json = jsonString.jsonValue()  as? [AnyHashable : Any] else {
                DDLogError("load json string error: \(resource_string)")
                return nil
        }
        return json
    }
    
    func filePathFrom(_ fileName: String, fileType: String) -> String {
        let appModel: ReactAppModel? = ReactNativeAppManager.sharedInstance().getApp(showshowAppId)
        let intenalPath = ReactNativeAppManager.sharedInstance().getRunFolder(appModel) ?? ""
        var resourcePath = "\(intenalPath)/assets/\(fileName).\(fileType)"
            
        let shouldLoadFromLocal = !FileManager.default.fileExists(atPath: resourcePath)
        if shouldLoadFromLocal || usingLocalConfig.boolValue {
            resourcePath = Bundle.main.path(forResource: fileName, ofType: fileType) ?? ""
        }
        return resourcePath
    }
}

