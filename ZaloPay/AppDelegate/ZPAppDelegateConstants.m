//
//  ZPAppDelegateConstants.m
//  ZaloPay
//
//  Created by tridm2 on 10/4/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import "ZPAppDelegateConstants.h"
#import ZALOPAY_MODULE_SWIFT

int platformInfoInterval            = 300;

NSString *const kPerUserDataBaseScheme  = @"54";
NSString *const kGlobalDataBaseScheme   = @"9";

NSInteger appDelegateServiceReactConfig =  ZALOPAY_REACT_CONFIG_TYPE;
//BOOL disableDownloadReactNativeApp  =  ZALOPAY_DISABLE_DOWNLOAD_REACNATIVE;
//BOOL disablePaymentConnector        =  ZALOPAY_DISABLE_PAYMENT_CONNECTOR;
NSString *kBaseUrl                  =  ZALOPAY_API_URL;
NSString *kUploadUrl                =  ZALOPAY_UPLOAD_URL;
NSString *kUploadClientLog          =  ZALOPAY_UPLOAD_CLIENT_LOG;
NSString *kUrlPromotion             =  ZALOPAY_PROMOTION_URL;
NSString *kUrlVoucher               =  ZALOPAY_VOUCHER_URL;
// Direct discount
NSString *kUrlDirectDiscount        =  ZALOPAY_ENDOW_URL;

NSString *kUrlTopUp                 =  ZALOPAY_TOPUP_URL;
NSString *kUrlGateway               =  ZALOPAY_GATEWAY_URL;
NSString *kLixiApiUrl               =  ZALOPAY_LIXI_API_URL;
NSString *termsOfUseUrl             =  ZALOPAY_TERMSOFUSE_URL;
NSString *faqUrl                    =  ZALOPAY_FAQ_URL;
NSString *kUrlQuickPaySupport       =  ZALOPAY_QUICKPAY_SUPPORT_URL;
NSString *kTrackerId                =  GA_TRACKING_ID;

NSInteger kAnalyticEnvironment      = ZALOPAY_ANALYTIC_ENVIRONMENT;
NSString *kURLNavigatorLog          = ZALOPAY_NAVIGATOR_LOG_URL;

NSString *kSchemePayment            =  ZALOPAY_SCHEME_PAYMENT;

NSString *kBIDVHelpUrl              =  ZALOPAY_BIDV_HELP_URL;

NSString *supportCenterUrl          =  ZALOPAY_SUPPORT_CENTER_URL;

NSString *kRemoteConfigFirebase     =  ZALOPAY_REMOTE_CONFIG_FIREBASE;

float socketPingInterval            = 30.; // 30s
float socketPingTimout              = 60.;
AppEvironment environment           = MERCHANT_APP_ENVIRONMENT;
enum ZAZaloSDKAuthenType typeLogin  = ZALO_AUTHEN_TYPE;
BOOL usingLocalConfig               = ZALOPAY_USING_LOCAL_CONFIG;
NSString *kUrlFilter                = ZALOPAY_FILTER_URL;
