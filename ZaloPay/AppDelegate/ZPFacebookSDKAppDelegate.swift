//
//  ZPFacebookSDKAppDelegate.swift
//  ZaloPay
//
//  Created by tridm2 on 1/30/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import FBSDKCoreKit

class ZPFacebookSDKAppDelegate: ZPBaseAppDelegate {
    override func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [AnyHashable: Any]?) -> Bool {
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        return true
    }
    
    override func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    override func applicationDidBecomeActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
    }
}
