//
//  ZPDatabaseAppDelegate.swift
//  ZaloPay
//
//  Created by Bon Bon on 6/26/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit

class ZPDatabaseAppDelegate: ZPBaseAppDelegate {
    override func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [AnyHashable : Any]?) -> Bool {
        self.initDataBase()
        return true
    }
    
    func initDataBase() {
        let tables: [ZPTableProtocol.Type] = [
            ZPCacheDataTable.self, ZPDataManifestTable.self, ZPTransactionLogTable.self, ZPNotifyTable.self, ZPNotifyPopupTable.self,
            ZPProfileTable.self, ZPZaloFriendTable.self, ZPRecentTransferMoneyTable.self, ZPLixiNotificationTable.self, ZPMerchantTable.self, ZPTrackingAppTransidTable.self
        ]
        PerUserDataBase.sharedInstance().registerTables(tables)
        
        let globalTables: [ZPTableProtocol.Type] = [
            ZPCacheDataTable.self, ZPMarkDownloadAppTable.self, ZPReactNativeTable.self
        ]
        GlobalDatabase.sharedInstance().registerTables(globalTables)
        GlobalDatabase.sharedInstance().createDBWithConfigVersion()
    }
}
