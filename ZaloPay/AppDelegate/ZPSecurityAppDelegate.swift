//
//  ZPSecurityAppDelegate.swift
//  ZaloPay
//
//  Created by nhatnt on 10/04/2018.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift
import ZaloPayConfig

class ZPSecurityAppDelegate: ZPBaseAppDelegate {
    static let sharedInstance = ZPSecurityAppDelegate()
    var window: UIWindow? {
        let appdelegate: ZPAppDelegate? = appDelegate()
        return appdelegate?.window
    }
    var enterBackgroundTime: Date?
    
    override func applicationDidEnterBackground(_ application: UIApplication) {
        if ApplicationState.sharedInstance.state == .anonymous {
            return            
        }
        
        //save time when app enter background to check timer for auto lock authenticaition
        if ZPSettingsConfig.sharedInstance().authenticationOpen {
            self.enterBackgroundTime = Date()
        }
        
        //If AuthenticationView wasn't shown, check to show splash Screen
        if ZPSettingsConfig.sharedInstance().addSplashScreen && self.isShowingAuthenticationView().not {
            ZPSplashScreenView.init(appear: self.window)
        }
    }
    
    override func applicationDidBecomeActive(_ application: UIApplication) {
        guard let enterBackgroundTime = self.enterBackgroundTime else {
            return
        }
        if ApplicationState.sharedInstance.state == .anonymous {
            return
        }
        let elapsed = Date().timeIntervalSince(enterBackgroundTime)
        let autoLockTime = ZPSettingsConfig.sharedInstance().autoLockTime
        if elapsed >= autoLockTime {
            self.showAuthentication()
        }
    }
    
    func showAuthentication() {
        self.enterBackgroundTime = nil;
        if ZPSettingsConfig.sharedInstance().authenticationOpen.not || self.isShowingAuthenticationView() {
            return
        }
        DispatchQueue.main.async {
            let authenticationViewController = AuthenticationRouter.assembleModule()
            guard let currentNaviController = UINavigationController.currentActiveNavigationController(),
            let topViewController = currentNaviController.topViewController else {
                return
            }
            if topViewController.isEnableAuthenticationView().not {
                return
            }
            if let presentedViewController = currentNaviController.presentedViewController {
                presentedViewController.dismiss(animated: true, completion: nil)
            }
            currentNaviController.present(authenticationViewController, animated: true, completion: nil)
        }
    }
    
    func dimissAuthenticationView() {
        if isShowingAuthenticationView() {
            guard let naviController = UINavigationController.currentActiveNavigationController() else {
                return
            }
            naviController.dismiss(animated: true, completion: nil)
        }
    }
    
    
    func appDelegate() -> ZPAppDelegate? {
        return UIApplication.shared.delegate as? ZPAppDelegate
    }

    func isShowingAuthenticationView() -> Bool {
        let root = UINavigationController.currentActiveNavigationController()
        return root?.presentedViewController is AuthenticationViewController
    }
}
