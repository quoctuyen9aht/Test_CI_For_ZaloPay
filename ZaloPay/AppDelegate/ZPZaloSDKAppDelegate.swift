//
//  ZPZaloSDKService.swift
//  ZaloPay
//
//  Created by tridm2 on 10/3/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

class ZPZaloSDKAppDelegate: ZPBaseAppDelegate {
    override func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [AnyHashable: Any]?) -> Bool {
        ZaloSDK.sharedInstance().initialize(withAppId: "19240466928203470")
        return true
    }
    
    override func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return ZDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
}
