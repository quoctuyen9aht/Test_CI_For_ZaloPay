//
//  ZPLoggerService.swift
//  ZaloPay
//
//  Created by tridm2 on 10/3/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import Crashlytics
import CocoaLumberjack.DDDispatchQueueLogFormatter

class ZaloPayLogFormat: DDDispatchQueueLogFormatter {
    static var processName = ProcessInfo.processInfo.processName
    
    override func format(message logMessage: DDLogMessage) -> String {
        // One-charecter log level
        var logLevel: String
        switch logMessage.flag {
        case .error:
            logLevel = "E"
        case .warning:
            logLevel = "W"
        case .info:
            logLevel = "I"
        case .debug:
            logLevel = "D"
        default:
            logLevel = "V"
        }
        return String(format: "%@ %@[%@] %@ %@:%lu %@",
                      self.string(from: logMessage.timestamp),
                      ZaloPayLogFormat.processName,
                      self.queueThreadLabel(for: logMessage),
                      logLevel,
                      (logMessage.file as NSString).lastPathComponent,
                      UInt(logMessage.line),
                      logMessage.message
        )
    }
}

class CrashlyticsLogger: DDAbstractLogger {
    override func log(message logMessage: DDLogMessage) {
        let logMsg: String = self.format(message: logMessage)
        if logMessage.flag == .warning || logMessage.flag == .error {
            CLSLogv("%@", getVaList([logMsg]))
            let error = NSError(domain: "ZaloPay", code: 0, userInfo: [NSLocalizedDescriptionKey: logMsg])
            Crashlytics.sharedInstance().recordError(error)
        }
    }
    
    func format(message logMessage: DDLogMessage) -> String {
        // One-charecter log level
        var logLevel: String
        switch logMessage.flag {
        case .error:
            logLevel = "E"
        case .warning:
            logLevel = "W"
        case .info:
            logLevel = "I"
        case .debug:
            logLevel = "D"
        default:
            logLevel = "V"
        }
        
        return String(format: "[%@] %@:%lu %@",
                      logLevel,
                      (logMessage.file as NSString).lastPathComponent,
                      UInt(logMessage.line),
                      logMessage.message
        )
    }
}

class ZPLoggerAppdelegate: ZPBaseAppDelegate {
    override func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [AnyHashable : Any]?) -> Bool {
        self.configLog()
        return true
    }
    
    func configLog() {
        #if DEBUG
            if let ttyLogger = DDTTYLogger.sharedInstance {
                ttyLogger.logFormatter = ZaloPayLogFormat()
                DDLog.add(ttyLogger)
            }
        #endif
        DDLog.add(CrashlyticsLogger())
    }
}
