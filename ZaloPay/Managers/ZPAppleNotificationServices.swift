//
//  ZPAppleNotificationServices.swift
//  ZaloPay
//
//  Created by vuongvv on 10/31/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import CocoaLumberjackSwift
import ZaloPayAnalyticsSwift
import ZaloPayConfig

fileprivate let k120Handler = 120

@objcMembers
public class ZPAppleNotificationServices: NSObject {
    static let sharedInstance = ZPAppleNotificationServices()
    
    var notificationHandlers: [Int : ZPNotificationAppleHandler] = [:]

    var notiReceiveLast:ZPAppleNotificationModel?
    
    override public init() {
        super.init()
        registerAllHandlers()
    }
    
    private func registerAllHandlers() {
        notificationHandlers[k120Handler] = ZPNotiApple120Handler()
    }
    
    @objc func handleRemoveNotification(_ userInfo: NSDictionary, fromLaunch: Bool) {
        if UIApplication.shared.applicationState == .active && !fromLaunch {
            DDLogInfo("app active and recieve remove notification")
            return
        }
        
        handleNotificationData(userInfo)
    }
    
    private func handleNotificationData(_ dic: NSDictionary) {
        let model = ZPAppleNotificationModel(dic as! Dictionary<AnyHashable, Any>)
        guard let handler = getHandler(model: model) else {
            return
        }
        
        //Log open notification
        let log = model.getInfor(from: .Open)
        ZPTrackingHelper.shared().eventTracker?.writeLogNotification(log: log as NSDictionary?)
        ZPAppleNotificationServices.sharedInstance.notiReceiveLast = model
        
        if ApplicationState.sharedInstance.state != .authenticated  {
            DDLogInfo("app not authenticated")
            return
        }
        handler.process(model: model)
    }
    
    func getHandler (model:ZPAppleNotificationModel) -> ZPNotificationAppleHandler? {
        return notificationHandlers[model.notificationType]
    }
}
