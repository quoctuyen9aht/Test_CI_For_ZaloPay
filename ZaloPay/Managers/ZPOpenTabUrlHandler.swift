//
//  ZPOpenTabSchemeHandler.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 10/12/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation

class ZPOpenTabUrlHandler: UrlHandler {
    func canHandle(scheme: ZPScheme, authenticated: Bool) -> Bool {
        return authenticated && scheme.domain.hasPrefix("zalopay://launch/tab/")
    }

    func handleUrl(scheme: ZPScheme) {
        let tab = scheme.uriPath.components(separatedBy: "/").last ?? ""
        let tabWillSelect = tabIndexFromName(tab: tab)
        if tabWillSelect == -1 {
            return
        }
        activateTabWithIndex(tabWillSelect: tabWillSelect)
    }
    
    func tabIndexFromName(tab: String) -> Int {
        switch tab {
        case "home":
            return 0
        case "transaction":
            return 1
        case "promotion":
            return 2
        case "me":
            return 3
        default:
            return -1
        }
    }
    
    func activateTabWithIndex(tabWillSelect: Int) {
        guard let tabbarController = ZPUIAppDelegate.sharedInstance.window?.rootViewController as? TabbarViewController else {
            return
        }
        if tabbarController.selectedIndex == tabWillSelect {
            return
        }
        guard let nav = tabbarController.selectedViewController as? UINavigationController else {
            return
        }
        if nav.viewControllers.count > 1 {
            nav.popToRootViewController(animated: true)
        }
        tabbarController.selectedIndex = tabWillSelect
    }
}

