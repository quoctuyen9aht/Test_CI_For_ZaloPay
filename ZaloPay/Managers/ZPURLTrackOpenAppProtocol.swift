//
//  ZPURLTrackOpenAppProtocol.swift
//  ZaloPay
//
//  Created by Dung Vu on 2/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import UIKit
protocol ZPURLTrackOpenAppProtocol {
    func trackOpenApp(from scheme: ZPScheme)
}

extension ZPURLTrackOpenAppProtocol {
    func trackOpenApp(from scheme: ZPScheme) {
        let source = scheme.domain
        (UIApplication.shared.delegate as? ZPAppDelegate)?.trackerOpenApp?.trackOtherApp(from: source)
    }
}
