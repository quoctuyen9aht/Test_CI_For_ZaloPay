//
//  AppleNotificationItem.swift
//  ZaloPay
//
//  Created by vuongvv on 10/31/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayProfile
@objc public enum ZPNotiSource: Int {
    case Backend = 0
    case Marketing = 1
}

@objc public enum ZPUserNotiAction: Int {
    case Open = 1
    case Terminate = 2
}

@objcMembers
class ZPAppleNotificationModel: NSObject {
    var notificationType:Int = 0
    var action:String = ""
    var actionData:String = ""
    
    //Log
    var source:Int = 0
    var pushId:String = ""
    var timeStamp:UInt64 = 0
    
     convenience init(_ dic: Dictionary<AnyHashable,Any>) {
        self.init()
        let embeddata = dic.value(forKey: "embebdata",defaultValue:[:])
        let dataStr = embeddata.value(forKey: "data", defaultValue: "")
        let data = dataStr.data(using: String.Encoding.utf8) ?? NSData() as Data
        guard  let dataDic = try? JSONSerialization.jsonObject(with: data, options: []) else  {
            return
        }
        guard let dataDicGet = dataDic as? Dictionary<AnyHashable,Any> else {
            return
        }
        self.notificationType = dataDicGet.value(forKey: "notificationType", defaultValue: -1)
        self.action = dataDicGet.value(forKey: "action", defaultValue: "")
        self.actionData = dataDicGet.value(forKey: "data", defaultValue: "")
        
        self.source = dataDicGet.value(forKey: "source", defaultValue: 0)
        self.pushId = dataDicGet.value(forKey: "pushId", defaultValue: "")
    }
    
    
    func getInfor(from action: ZPUserNotiAction) -> JSON? {
        if self.source != ZPNotiSource.Marketing.rawValue || self.pushId.count == 0 {
            return nil
        }
        
        var dicLog = JSON()
        dicLog["userId"] = ZPProfileManager.shareInstance.userLoginData?.paymentUserId
        dicLog["deviceId"] = UIDevice.zmDeviceUUID()
        dicLog["timestamp"] = self.timeStamp > 0 ? self.timeStamp :  NSDate().timeIntervalSince1970 * 1000
        dicLog["action"] = action.rawValue
        dicLog["frontid"] = "main"
        dicLog["pushId"] = self.pushId
        dicLog["source"] = self.source
        return dicLog
    }
    
    
    
    
}
