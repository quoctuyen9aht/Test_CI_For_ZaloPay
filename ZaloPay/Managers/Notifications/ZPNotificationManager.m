//
//  ZPNotificationManager.m
//  ZaloPay
//
//  Created by bonnpv on 6/14/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPNotificationManager.h"
#import "ZPMessage.h"
#import "ZPConnectionManager+SendMessage.h"
#import "ZPConnectionManager.h"
#import "ZPNotificationManager.h"

#import <ZaloPayAppManager/TransactionHistoryManager.h>
#import <ZaloPayDatabase/ZPNotifyTable.h>
#import <ZaloPayDatabase/PerUserDataBase.h>
#import <ZaloPayDatabase/ZPTransactionLogTable.h>
#import <ZaloPayAppManager/ZaloPayModuleManager.h>
#import <ZaloPayRedPacket/ZPRPManager.h>
#import <ZaloPayRedPacket/ZPRPExternalProtocols.h>
#import <ZaloPayRedPacket/ZPRPCommon.h>

@interface ZPNotificationManager()
@property (nonatomic, strong, nonnull) ZPNotifyTable* notifyTable;
@property (nonatomic, strong, nonnull) ZPTransactionLogTable* transactionLogTable;
@end

@implementation ZPNotificationManager
+ (instancetype)sharedInstance {
    static ZPNotificationManager *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[ZPNotificationManager alloc] init];
    });
    return instance;
}

- (instancetype)init
{
    if (self = [super init]) {
        self.notifyTable = [[ZPNotifyTable alloc] initWithDB:[PerUserDataBase sharedInstance]];
        self.transactionLogTable = [[ZPTransactionLogTable alloc] initWithDB:[PerUserDataBase sharedInstance]];
    }
    return self;
}

- (void)registerMessageSignal {
    [[ZPConnectionManager sharedInstance].messageSignal subscribeNext:^(ZPMessage *message) {
        [self handleMessage:message];
    }];
}

- (void)handleMessage:(ZPMessage *)message {
    if ([message isKindOfClass:[ZPLixiMessage class]]) {
        [self handleLixiMessage:(ZPLixiMessage *)message];
    } else if ([message isKindOfClass:[ZPNotifyMessage class]]) {
        if (((ZPNotifyMessage*)message).notificationType == Notification_Promotion)
        {
            return;
        }
        [self handleNotificationMessage:(ZPNotifyMessage *)message];
    } else if ([message isKindOfClass:[ZPKickOutMessage class]]) {
        DDLogInfo(@"receive kickout message");
    } else if ([message isKindOfClass:[ZPAuthenMessage class]]) {
        
    } else if ([message isKindOfClass:[ZPRecoveryMessage class]]) {
        [self handleRecoveryMessage:(ZPRecoveryMessage *)message];
    }
}

- (void)handleRecoveryMessage:(ZPRecoveryMessage *)message {
    for (ZPMessage *recoveryMsg in message.recoveryMessage) {
        [self handleMessage:recoveryMsg];
    }
}

- (void)handleLixiMessage:(ZPLixiMessage *)lixiMessage {
    
    [[[ZPAppFactory sharedInstance] rpManager].dbApi saveLixiNotificationDataWithPackageId:lixiMessage.packageId
                                                                   bundleId:lixiMessage.bundleId
                                                               senderAvatar:lixiMessage.avatarUrl
                                                                 senderName:lixiMessage.name
                                                                    message:lixiMessage.lixiMessage
                                                                     amount:0
                                                                     status:PacketStatusCanOpen
                                                               overrideData:lixiMessage.isUnread
                                                                mtaid:lixiMessage.mtaid
                                                                mtuid:lixiMessage.mtuid];
    
    // support react native parse data bị lỗi (ví dụ : 1611180000000010006 convert thành 1611180000000010000)
    NSDictionary *embededData = lixiMessage.embeded;
    NSNumber *packageid = [embededData objectForKey:@"packageid"];
    if (packageid && [packageid isKindOfClass:[NSNumber class]]) {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:embededData];
        [dic setObject:[packageid stringValue] forKey:@"packageid"];
        embededData = dic;
    }
    [self.notifyTable addNotifyWithTranId:lixiMessage.transid
                                                timestamp:lixiMessage.timestamp
                                                    appid:lixiMessage.appId
                                                  message:lixiMessage.message
                                                transtype:0
                                                   userid:lixiMessage.senderUserId
                                               destuserid:lixiMessage.destuserid
                                                 isUnread:lixiMessage.isUnread
                                               notifyType:lixiMessage.notificationType
                                                embeddata:embededData
                                                    mtaid:lixiMessage.mtaid
                                                    mtuid:lixiMessage.mtuid
                                                     area:lixiMessage.area];
    
    
}

- (void)updateThankMessage:(ZPNotifyMessage *)notify {
    NSDictionary *embeded = notify.embeded;
    NSString *transid = [embeded objectForKey:@"transid"];
    NSString *message = [embeded objectForKey:@"message"];
    [self.transactionLogTable updateThankMessageWithTransId:transid encodeMessage:message];
    [[NSNotificationCenter defaultCenter] postNotificationName:ZPTransactionDetailUpdated object:embeded userInfo:@{}];
}

- (BOOL)skipStorage:(ZPNotifyMessage *)notify {
    if ([notify isKindOfClass:[ZPThankMessage class]]) {
        return  false;
    }
    return notify.area == 6;
}

- (void)handleNotificationMessage:(ZPNotifyMessage *)notify {      
    if ([self skipStorage:notify]) {
        return;
    }
    if ([notify isKindOfClass:[ZPThankMessage class]]) {
        [self updateThankMessage:notify];
    }
    
    [self.notifyTable addNotifyWithTranId:notify.transid
                                                timestamp:notify.timestamp
                                                    appid:notify.appId
                                                  message:notify.message
                                                transtype:0
                                                   userid:notify.senderUserId
                                               destuserid:notify.destuserid
                                                 isUnread:notify.isUnread
                                               notifyType:notify.notificationType
                                                embeddata:notify.embeded
                                                    mtaid:notify.mtaid
                                                    mtuid:notify.mtuid
                                                     area:notify.area];
}


@end
