//
//  ZPConnectionManager.h
//  ZaloPay
//
//  Created by bonnpv on 6/14/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RACSignal;
@class RACBehaviorSubject<ValueType>;

@interface ZPConnectionManager : NSObject
@property (nonatomic, readonly) RACSignal *messageSignal;
@property (nonatomic, readonly) uint64_t zaloPayId;
@property (nonatomic) BOOL isAuthenSuccess;
@property (nonatomic, readonly) RACBehaviorSubject <NSNumber *>*eAuthenSuccess;

+ (instancetype)sharedInstance;
- (void)loginWithId:(NSString *)zaloPayId
        accesstoken:(NSString *)token
   applenotifyToken:(NSString *)appleNotifyToken;

- (void)logout;

- (void)reconnect;

- (BOOL)isConnected;

- (BOOL)isAuthenticated;

- (void)sendMessageData:(NSData *)strData type:(uint8_t)nType;

- (RACSignal *)requestWithDomain:(NSString *)domain
                            path:(NSString *)path
                            data:(NSDictionary*)data;
@end

