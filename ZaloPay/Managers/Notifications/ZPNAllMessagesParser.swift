//
//  ZPNAllMessagesParser.swift
//  ZaloPay
//
//  Created by thi la on 3/20/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayAnalyticsSwift
import CocoaLumberjackSwift
import ZaloPayCommonSwift

typealias ZPNotifyData = (message: ZPMessage?, isCorrectUser: Bool)

protocol ZPNMessageParser {
    func parse(fromEmbeded embeded: JSON) -> ZPNotifyMessage?
}

protocol ZPNServerParser {
    func parse(fromData data: Data) -> ZPNotifyData?
}

fileprivate func checkNotificationWrongUser(usrid: UInt64) -> Bool {
    // Ignore 0
    if (usrid == 0) {
        return true
    }
    guard let userId = NetworkManager.sharedInstance().paymentUserId  else {
        return false
    }
    return userId.toUInt64() == usrid
}

fileprivate func getEmbeddata(from json: JSON) -> JSON {
    let embedData = json["embeddata"]
    
    // handle embedData type json
    if let embedJson = embedData as? JSON {
        return embedJson
    }
    // handle embedData type string : case thank message and tranfer QR
    guard let embedString = embedData as? String else {
        return JSON()
    }
    
    if let json = (embedString as NSString).jsonValue() as? JSON {
        return json
    }
    
    if let data = NSString(fromBase64String: embedString), let newJson = data.jsonValue() as? JSON  {
        return newJson
    }
    
    return JSON()
}

fileprivate func writeExceptionFrom(usrid: UInt64, mtuid: UInt64 ,sourceid: UInt32, isRecovery: Bool){
    guard let userId = NetworkManager.sharedInstance().paymentUserId else {
        return
    }
    var infos = [String: Any]()
    infos["currentId"] = userId
    infos["usrid"] = usrid
    infos["mtuid"] = mtuid
    infos["sourceid"] = isRecovery ? SourceType.recover : sourceid
    infos["timestamp"] = NSDate().timeIntervalSince1970
    ZPTrackingHelper.shared().eventTracker?.writeLogErrorNotification(error: infos as NSDictionary?)
}

@objcMembers
class ZPNAllMessagesParser: NSObject {
    private lazy var serverParser: [ServerMessageType: ZPNServerParser] = [.pushNotification : ZPPushNotificationParser(),
                                                                           .kickOutUser: ZPKickOutUserParser(),
                                                                           .authenLoginResult: ZPAuthenResultParser(),
                                                                           .pongClient: ZPPongClientParser(),
                                                                           .recoveryResponse: ZPRevoveryParser(),
                                                                           .paymentResponse: ZPPaymentResponseParser()]
    
    // MARK: public methods using for both objective c and swift
    @objc public func message(fromData data: DataResponseUser, receiveTime: Double) -> ZPMessage? {
        
        DDLogInfo("data = \(data)")
        guard let notifyData = data.data_p else {
            DDLogInfo("data.data_p == nil")
            return nil
        }
        
        if checkNotificationWrongUser(usrid: data.usrid) == false {
            DDLogInfo("invalid userid")
            writeExceptionFrom(usrid: data.usrid, mtuid: data.mtuid, sourceid: data.sourceid, isRecovery: false)
            return nil
        }
        
        guard let serverType = ServerMessageType(rawValue: data.msgtype),
              let parser = serverParser[serverType] else {
              DDLogInfo("invalid message type")
              return nil
        }
        
        guard let notify = parser.parse(fromData: notifyData) else {
            DDLogInfo("parse message error")
            return nil
        }
        
        if notify.isCorrectUser.not {
            DDLogInfo("invalid userid 2")
            let isRecovery = serverType == .recoveryResponse
            writeExceptionFrom(usrid: data.usrid, mtuid: data.mtuid, sourceid: data.sourceid, isRecovery: isRecovery)
            return nil
        }
        
        let message = notify.message
        message?.mstatus = data.status
        message?.mtuid = data.mtuid
        message?.mtaid = data.mtaid
        message?.receiveTime = receiveTime
        return message
    }
}

// MARK: Server Message
class ZPPushNotificationParser: ZPNServerParser {
    
    private lazy var allParser: [NotificationType: ZPNMessageParser] = {
        let profileParser = ZPProfileMessageParser()
        let _return: [NotificationType: ZPNMessageParser] = [.fromApp: ZPFromAppMessageParser(),
                                                            .lixi: ZPLixiMessageParser(),
                                                            .updateProfileSuccess: profileParser,
                                                            .updateProfileFail: profileParser,
                                                            .upateProfileMessage: profileParser,
                                                            .removeExpireCard: ZPRemoveExpireCardMessageParser(),
                                                            .cashback: ZPCashbackMessageParser(),
                                                            .voucher: ZPVoucherMessageParser(),
                                                            .liXiWishes: ZPLiXiWishesMessageParser(),
                                                            .qrToPay: ZPQRToPayMessageParser(),
                                                            .gateway: ZPGatewayMessageParser()]
        
        return _return
    }()
    
    func parse(fromJSON json: JSON) -> ZPNotifyMessage? {
        let type:Int = json.int(forKey: "notificationtype")
        let ntype = NotificationType(rawValue: type).or(.none)
        let embedData = getEmbeddata(from: json)
        let message = (allParser[ntype]?.parse(fromEmbeded: embedData)).or(else:ZPNotifyMessage())
        message.notificationType = ntype
        message.embeded = message.embeded == nil ? embedData : message.embeded
        message.transid = json.uInt64(forKey: "transid")
        message.appId = json.int32(forKey: "appid")
        message.timestamp = json.uInt64(forKey: "timestamp")
        message.message = json.string(forKey: "message")
        message.senderUserId = json.string(forKey: "userid")
        message.destuserid = json.string(forKey: "destuserid")
        message.area = json.int32(forKey: "area")
        message.isUnread = self.isUnreadNotify(message)
        return message
    }
    
    func parse(fromData data: Data) -> ZPNotifyData? {
        var notify: ZPNotifyData = (message: ZPNotifyMessage(), isCorrectUser: true)
        guard let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue),
            let json = jsonString.jsonValue() as? JSON else {
                return notify
        }
        let message = parse(fromJSON: json)
        message?.isUnread = self.isUnreadNotify(message)
        notify.message = message
        return notify
    }
    
    func isUnreadNotify(_ message: ZPNotifyMessage?) -> Bool {
        guard let message = message else {
            return false
        }
        if message.notificationType == .payBill ||
           message.notificationType == .gift {
            return false
        }
        if message.notificationType == .transfer {
            let userId = NetworkManager.sharedInstance().paymentUserId ?? ""
            if userId == message.senderUserId {
                return false
            }
        }
        return true
    }
}

class ZPKickOutUserParser: ZPNServerParser {
    func parse(fromData data: Data) -> ZPNotifyData? {
        return (message: ZPKickOutMessage(), isCorrectUser: true)
    }
}

class ZPAuthenResultParser: ZPNServerParser {
    func parse(fromData data: Data) -> ZPNotifyData? {
        guard let authen = try? ResultAuth(data: data) else {
            DDLogInfo("could not parse ResultAuthMessage")
            return nil
        }
        let authenMessage = ZPAuthenMessage()
        authenMessage.result = authen.result
        authenMessage.code = authen.code
        authenMessage.message = authen.msg
        return (message: authenMessage, isCorrectUser: true)
    }
}

class ZPPongClientParser: ZPNServerParser {
    func parse(fromData data: Data) -> ZPNotifyData? {
        guard let message = try? MessageConnectionInfo(data: data) else {
            DDLogInfo("could not parse PongMessage")
            return nil
        }
        let pongClient = ZPPongMessage()
        pongClient.embedata = message.embeddata
        return (message: pongClient, isCorrectUser: true)
    }
}

class ZPRevoveryParser: ZPPushNotificationParser {
    override func parse(fromData data: Data) -> ZPNotifyData? {
        
        guard let response = try? DataRecoveryResponse(data: data),
              let messages = response.messagesArray as? [RecoveryMessage] else {
              DDLogInfo("could not parse ZPRecoveryMessage")
              return nil
        }
        
        let recoveryMessage = messages.filter {
                return $0.servermessagetype == ServerMessageType.pushNotification.rawValue &&
                       $0.status != MessageStatus.deleted.rawValue
            }.map { recovery -> ZPMessage? in
                guard let data = recovery.data_p,
                      let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue),
                      let json = jsonString.jsonValue() as? JSON else {
                        DDLogInfo("could not parse RecoveryMessage")
                        return nil
                }
                let notifyMessage = super.parse(fromJSON: json)
                notifyMessage?.mtaid = recovery.mtaid
                notifyMessage?.mtuid = recovery.mtuid
                notifyMessage?.isUnread = false
                return notifyMessage
            }.compactMap { $0 }
        
        let message = ZPRecoveryMessage()
        message.recoveryMessage = recoveryMessage
        message.timestamp = response.starttime
        let isCorrectUser = checkNotificationWrongUser(usrid: response.usrid)
        return (message: message, isCorrectUser: isCorrectUser)
    }
}

class ZPPaymentResponseParser: ZPNServerParser {

    func parse(fromData data: Data) -> ZPNotifyData? {
        guard let response = try? PaymentResponseMessage(data: data as Data) else {
            DDLogInfo("could not parse PaymentResponseMessage")
            return nil
        }
        let message = ZPPaymentResponseMessage()
        message.resultData = response.resultdata
        message.errorcode = response.resultcode
        message.requestId = response.requestid.toInt64()
        let isCorrectUser = checkNotificationWrongUser(usrid: response.usrid)
        return (message: message, isCorrectUser: isCorrectUser)
    }
}

// MARK: Notify Message
class ZPLixiMessageParser: ZPNMessageParser {
    func parse(fromEmbeded embeded: JSON) -> ZPNotifyMessage? {
        let lixi = ZPLixiMessage()
        lixi.packageId = embeded.uInt64(forKey: "packageid")
        lixi.bundleId = embeded.uInt64(forKey: "bundleid")
        lixi.avatarUrl = embeded.string(forKey: "avatar")
        lixi.name = embeded.string(forKey: "name")
        lixi.lixiMessage = embeded.string(forKey: "liximessage")
        return lixi
    }
}

class ZPProfileMessageParser: ZPNMessageParser {
    func parse(fromEmbeded embeded: JSON) -> ZPNotifyMessage? {
        let profile = ZPProfileMessage()
        profile.profileLevel = embeded.int32(forKey: "profilelevel")
        profile.status = embeded.int32(forKey: "status")
        return profile
    }
}

class ZPRemoveExpireCardMessageParser: ZPNMessageParser {
    func parse(fromEmbeded embeded: JSON) -> ZPNotifyMessage? {
        let removeCard = ZPRemoveExpireCardMessage()
        removeCard.firstSixNumber = embeded.string(forKey: "first6cardno")
        removeCard.lastFourNumber = embeded.string(forKey: "last4cardno")
        return removeCard
    }
}

class ZPCashbackMessageParser: ZPNMessageParser {
    func parse(fromEmbeded embeded: JSON) -> ZPNotifyMessage? {
        let cachback = ZPCashbackMessage()
        cachback.data = ZPCashbackData(embeded)
        return cachback
    }
}

class ZPVoucherMessageParser: ZPNMessageParser {
    func parse(fromEmbeded embeded: JSON) -> ZPNotifyMessage? {
        let voucher = ZPVoucherMessage()
        voucher.data = ZPVoucherData(embeded)
        return voucher
    }
}

class ZPLiXiWishesMessageParser: ZPNMessageParser {
    func parse(fromEmbeded embeded: JSON) -> ZPNotifyMessage? {
        let lixiWishes = ZPLiXiWishesMessage()
        lixiWishes.data = ZPLiXiWishesPopupData(embeded)
        return lixiWishes
    }
}

class ZPQRToPayMessageParser: ZPNMessageParser {
    func parse(fromEmbeded embeded: JSON) -> ZPNotifyMessage? {
        let mess = ZPQRToPayMessage()
        mess.paymentcode = embeded.string(forKey: "paymentcode")
        mess.isrequiredpin = embeded.bool(forKey: "isrequiredpin")
        mess.resultStatus = embeded.int32(forKey: "status")
        mess.resultMessage = embeded.string(forKey: "message")
        mess.amount = embeded.int32(forKey: "amount")
        mess.merchantName = embeded.string(forKey: "appname")
        return mess
    }
}

class ZPGatewayMessageParser: ZPNMessageParser {
    func parse(fromEmbeded embeded: JSON) -> ZPNotifyMessage? {
        let mess = ZPGatewayMessage()
        return mess
    }
}


class ZPFromAppMessageParser: ZPNMessageParser {
    
    class ZPQRTransferAppMessageParser: ZPNMessageParser {
        func parse(fromEmbeded embeded: JSON) -> ZPNotifyMessage? {
            let qrTransfer = ZPQRTransferAppMessage()
            qrTransfer.progress = embeded.int32(forKey: "mt_progress")
            qrTransfer.senderAvatar = embeded.string(forKey: "avatar")
            qrTransfer.amount = embeded.int64(forKey: "amount")
            qrTransfer.transId = embeded.string(forKey: "transid")
            let displayNameV2 = embeded.string(forKey: "displaynamev2")
            qrTransfer.senderDisplayName = displayNameV2.count > 0 ? NSString.decodeP2PMessage(displayNameV2) : embeded.string(forKey: "displayname")
            return qrTransfer
        }
    }
    
    class ZPThankMessageParser: ZPNMessageParser {
        func parse(fromEmbeded embeded: JSON) -> ZPNotifyMessage? {
            var nEmbeded  = embeded
            let thankMessage = ZPThankMessage()
            let displayName = nEmbeded.string(forKey: "displayname")
            nEmbeded["displayname"] = NSString.decodeP2PMessage(displayName)
            thankMessage.embeded = nEmbeded
            return thankMessage
        }
    }
    
    private lazy var notifyAppParser: [NotfFromAppType: ZPNMessageParser] = [.qrTransfer: ZPQRTransferAppMessageParser(),
                                                                             .sendThankMessage: ZPThankMessageParser()]
    
    func parse(fromEmbeded embeded: JSON) -> ZPNotifyMessage? {
        let type = embeded.int(forKey: "type")
        guard let appType = NotfFromAppType(rawValue: type) else {
            return nil
        }
        return self.notifyAppParser[appType]?.parse(fromEmbeded: embeded)
    }
}




