//
//  NotifyMessage.h
//  ZaloPay
//
//  Created by bonnpv on 6/14/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, NotificationType){
    Notification_None                 = 0,
    Notification_PayBill              = 1,
    Notification_Recharge             = 2,
    Notification_MapCard              = 3,
    Notification_Transfer             = 4,
    Notification_Withdraw             = 5,
    Notification_OpenLixiSuccess      = 6,
    Notification_RefundLixi           = 7,
    Notification_Gift                 = 9,
    Notification_RevertMoney          = 10,
    Notification_OfflineTransaction   = 11,
    Notification_ZPBankTopup          = 12,
    Notification_QRToPay              = 14,
    Notification_Gateway              = 15,
    
    Notification_UpdateProfileSuccess = 100,
    Notification_Merchant             = 101,
    Notification_UpdateProfileFail    = 102,
    Notification_Lixi                 = 103,
    Notification_UpateProfileMessage  = 104,
    Notification_RefundTransationWallet     = 105,
    Notification_RefundTransationBank       = 106,
    Notification_RetryTransaction           = 107,
    Notification_MerchantApp                = 108,
    Notification_FromApp                    = 109,
    Notification_ResetPin                   = 110,
    Notification_ReceiveLixi                = 111,
    Notification_RemoveExpireCard           = 112,
    Notification_MerchantBill               = 113,
    Notification_AllUser                    = 114,
    Notification_RemoveBankAccount          = 115,
    Notification_MapBankAccount             = 116,
    Notification_Cashback                   = 117,
    Notification_Promotion                  = 118,
    Notification_Voucher                    = 119,
    Notification_OpenWebAndApp              = 120,
    Notification_LiXiWishes                 = 122
};

typedef NS_ENUM(NSInteger, NotfFromAppType){
    NotfFromAppTypeQRTransfer = 1,
    NotfFromAppTypeSendThankMessage = 2,
};


@interface ZPMessage : NSObject
@property (nonatomic) uint64_t mtaid;
@property (nonatomic) uint64_t mtuid;
@property (nonatomic) int mstatus;
@property (nonatomic) BOOL isUnread;
@property (nonatomic) double receiveTime;

@end

@interface ZPDisconnectionMessage : ZPMessage
@end

@interface ZPAuthenMessage : ZPMessage
@property (nonatomic) int result;
@property (nonatomic) int code;
@property (nonatomic, strong) NSString *message;
@end

@interface ZPKickOutMessage : ZPMessage
@end

@interface ZPPongMessage : ZPMessage;
@property (nonatomic) uint64_t embedata;
@end

@interface ZPRecoveryMessage : ZPMessage
@property (nonatomic) uint64_t timestamp;
@property (nonatomic, strong) NSArray *recoveryMessage;
@end

@interface ZPNotifyMessage : ZPMessage
@property (nonatomic) int appId;
@property (nonatomic) u_int64_t timestamp;
@property (nonatomic) u_int64_t transid;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSString *senderUserId;
@property (nonatomic, strong) NSString *destuserid;
@property (nonatomic) NotificationType notificationType;
@property (nonatomic) int area;
@property (nonatomic, strong) NSDictionary *embeded;
@end


@interface ZPProfileMessage : ZPNotifyMessage
@property (nonatomic) int profileLevel;
@property (nonatomic) int status;
@end

@interface ZPLixiMessage : ZPNotifyMessage
@property (nonatomic) uint64_t bundleId;
@property (nonatomic) uint64_t packageId;
@property (nonatomic, strong) NSString *avatarUrl;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *lixiMessage;
@end
@interface ZPFromAppMessage: ZPNotifyMessage
@property (nonatomic, assign) NotfFromAppType type;
@end

@interface ZPThankMessage: ZPFromAppMessage
@end

@interface ZPQRTransferAppMessage : ZPFromAppMessage
@property (nonatomic, strong) NSString *embedData;
@property (nonatomic, strong) NSString *senderDisplayName;
@property (nonatomic, strong) NSString *senderAvatar;
@property (nonatomic, assign) int64_t amount;
@property (nonatomic, assign) int progress; // MoneyTransferProgress
@property (nonatomic, strong) NSString *transId;

@end

@interface ZPRemoveExpireCardMessage : ZPNotifyMessage
@property (nonatomic, strong) NSString *firstSixNumber;
@property (nonatomic, strong) NSString *lastFourNumber;

@end

@interface ZPPaymentResponseMessage : ZPMessage
@property (nonatomic) long long requestId;
@property (nonatomic) int errorcode;
@property (nonatomic, strong) NSString *resultData;
@end

@class ZPCashbackData;
@interface ZPCashbackMessage : ZPNotifyMessage
@property (nonatomic, strong) ZPCashbackData *data;
@end

@class ZPVoucherData;
@interface ZPVoucherMessage : ZPNotifyMessage
@property (nonatomic, strong) ZPVoucherData *data;
@end

@class ZPLiXiWishesPopupData;
@interface ZPLiXiWishesMessage : ZPNotifyMessage
@property (nonatomic, strong) ZPLiXiWishesPopupData *data;
@end

@interface ZPQRToPayMessage : ZPNotifyMessage
@property (nonatomic, strong) NSString *paymentcode;
@property (nonatomic) BOOL isrequiredpin;
@property (nonatomic) int resultStatus;
@property (nonatomic, strong) NSString *resultMessage;
@property (nonatomic) int amount;
@property (nonatomic, strong) NSString *merchantName;
@end

@interface ZPGatewayMessage : ZPNotifyMessage
@end

