//
//  ZPConnectionManager.m
//  ZaloPay
//
//  Created by bonnpv on 6/14/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPConnectionManager.h"
#import "GCDAsyncSocket.h"
#import "NSNotificationCenter+ZaloPay.h"
#import "ZPMessage.h"
#import "ZPNotificationManager.h"
#import "ZPPingServerModule.h"
#import "ZPConnectionManager+SendMessage.h"
#import "Zpmsguser.pbobjc.h"
#import <ZaloPayNetwork/ZaloPayErrorCode.h>
#import <ZaloPayNetwork/ZPNetWorkApi.h>
#import ZALOPAY_MODULE_SWIFT
//#import <ZaloPayCommon/ZaloPayCommon-Swift.h>

#define SOCKET_DATA_STAGE_READ_HEADER   0
#define SOCKET_DATA_STAGE_READ_BODY   1
#define SOCKET_TIMEOUT_INFINITE (-1.0)
#define SOCKET_BODY_LENGTH      65000

@interface ZPUserConnectionData : NSObject
@property (nonatomic, copy) NSString *token;
@property (nonatomic, copy) NSString *appleNotifyToken;
@property (nonatomic) u_int64_t zaloPayId;
@end

@implementation ZPUserConnectionData
- (NSString *)accessToken {
    @synchronized(self) {
        return _token;
    }
}

- (void)setAccessToken:(NSString *)token {
    @synchronized(self) {
        self.token = token;
    }
}

- (id)copy {
    ZPUserConnectionData *temp = [ZPUserConnectionData new];
    temp.token = self.token;
    temp.appleNotifyToken = self.appleNotifyToken;
    temp.zaloPayId = self.zaloPayId;
    return temp;
}
@end

@interface ZPConnectionManager()
@property (atomic, strong) ZPUserConnectionData *userData;
@property (nonatomic) BOOL isBackground;
@property (nonatomic, strong) GCDAsyncSocket *socket;
@property (nonatomic) int retryCount;
@property (nonatomic, strong) RACSubject *subject;
@property (nonatomic, strong) RACTargetQueueScheduler *scheduler;
@property (nonatomic, strong) RACScheduler *messageScheduler;
@property (nonatomic, strong) RACSubject *connectionSignal;
@property (nonatomic, readwrite) RACBehaviorSubject <NSNumber *>*eAuthenSuccess;
@property (nonatomic, strong) ZPNAllMessagesParser *parsers;
@end

@implementation ZPConnectionManager

+ (instancetype)sharedInstance {
    static ZPConnectionManager *instance ;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[ZPConnectionManager alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
        NSString *queueName = @"com.zalopay.socket";
        RACSchedulerPriority priority = RACSchedulerPriorityBackground;
        self.scheduler = (RACTargetQueueScheduler *)[RACScheduler schedulerWithPriority:priority name:queueName];
        self.messageScheduler = [RACScheduler scheduler];
        self.connectionSignal = [RACSubject subject];
        self.subject = [RACSubject subject];
        self.eAuthenSuccess = [RACBehaviorSubject behaviorSubjectWithDefaultValue:@(NO)];
        [self monitorApplicationState];
        [self observerConnectionSignal];
        self.parsers = [ZPNAllMessagesParser new];
    }
    return self;
}

- (RACSignal *)messageSignal {
    return self.subject;
}

- (uint64_t)zaloPayId {
    return self.userData.zaloPayId;
}

- (BOOL)isConnected {
    return self.socket.isConnected;
}

- (BOOL)isAuthenticated {
    return self.socket.isConnected && self.isAuthenSuccess;
}

- (void) setIsAuthenSuccess:(BOOL)isAuthenSuccess {
    _isAuthenSuccess = isAuthenSuccess;
    [_eAuthenSuccess sendNext:@(isAuthenSuccess)];
}

- (void)loginWithId:(NSString *)zaloPayId
        accesstoken:(NSString *)token
   applenotifyToken:(NSString *)appleNotifyToken {
    
    ZPUserConnectionData *data = [[ZPUserConnectionData alloc] init];
    data.zaloPayId = zaloPayId ? [zaloPayId longLongValue] : 0;
    data.appleNotifyToken = appleNotifyToken;
    data.accessToken = token;
    [self.connectionSignal sendNext:data];
}

- (void)logout {
    [self.scheduler afterDelay:2.0 schedule:^{
        self.userData = nil;
        [self stopConnect];
    }];
}

- (void)reconnect {
    [self.scheduler schedule:^{
        [self stopConnect];
        [self.connectionSignal sendNext:[self.userData copy]];
    }];
}

#pragma mark - fucntion

- (void)observerConnectionSignal {
    [[[self.connectionSignal throttle:1.0] deliverOn:self.scheduler] subscribeNext:^(ZPUserConnectionData *value) {
        @try {
            [self handleConnectionData:value];
        } @catch (NSException *exception) {
            DDLogError(@"handleConnectionData exception: %@",exception);
        }
    }];
}

- (void)handleConnectionData:(ZPUserConnectionData *)value {
#if DEBUG
    NSAssert(![self.userData isEqual:value], @"Wrong set!!!");
#endif
    if (value.accessToken.length == 0 || value.zaloPayId == 0) {
        return;
    }
    if (self.isConnected && self.userData) {
        BOOL sameId = value.zaloPayId == self.userData.zaloPayId;
        BOOL sameAppleToken = (value.appleNotifyToken == nil && self.userData.appleNotifyToken == nil) ||
                              [value.appleNotifyToken isEqualToString:self.userData.appleNotifyToken];
        
        if (sameId && sameAppleToken){
            //              case cập nhật accesstoken mới khi đã có connect -> bỏ qua chỉ update dữ liệu mà không cần reconnect
            //              nếu socket đá ra lần sau sẽ tự connect với accesstoken mới được cập nhật.
            @synchronized (self.userData) {
                self.userData.accessToken = value.accessToken;
            }
            return;
        }
    }
    self.userData = value;
    [self stopConnect];
    [self startConnect];
}

- (void)monitorApplicationState {
    [[[[NSNotificationCenter defaultCenter] signalAppEnterForceground] deliverOn:self.scheduler] subscribeNext:^(id x) {
        self.isBackground = false;
        self.retryCount = 0;
        [self startConnect];
    }];
    [[[[NSNotificationCenter defaultCenter] signalAppEnterBackground] deliverOn:self.scheduler] subscribeNext:^(id x) {
        self.isBackground = true;
        [self stopConnect];
    }];
}

- (void)startConnect {
    DDLogInfo(@"start connect");
    if (self.userData == nil || self.isBackground || self.socket) {
        DDLogInfo(@"in background || socket exist");
        return;
    }
    
    DDLogInfo(@"host = %@ port = %d",ZALOPAY_SOCKET_URL, ZALOPAY_SOCKET_PORT);
    self.socket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:self.scheduler.queue];
    NSError *error = nil;
    if (![self.socket connectToHost:ZALOPAY_SOCKET_URL onPort:ZALOPAY_SOCKET_PORT error:&error]) {
        DDLogInfo(@"Error connecting: %@", error);
    }
}

- (void)stopConnect {
    DDLogInfo(@"stop connect");
    self.isAuthenSuccess = false;
    self.socket.delegate = nil;
    [self.socket disconnect];
    self.socket = nil;
    [self sendDisconnectMessage];
}

- (void)sendDisconnectMessage {
    ZPDisconnectionMessage *disconnectMessage = [[ZPDisconnectionMessage alloc] init];
    [self.subject sendNext:disconnectMessage];
}

#pragma mark - Delegate

- (void)setUpSSL:(GCDAsyncSocket *)sock {
    NSMutableDictionary *sslSetting = [[NSMutableDictionary alloc] init];
    [sslSetting setObject:[NSNumber numberWithInteger:kTLSProtocol12]forKey:GCDAsyncSocketSSLProtocolVersionMax];
    [sslSetting setObject:[NSNumber numberWithInteger:kTLSProtocol1]forKey:GCDAsyncSocketSSLProtocolVersionMin];
    [sock startTLS:sslSetting];
}

- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port {
    DDLogInfo(@"socket did connect");
    if (ZALOPAY_ENABLE_SSL_SOCKET) {
        [self setUpSSL:sock];
    }
    self.retryCount = 0;
    [self.socket readDataToLength:sizeof(uint32_t) withTimeout:SOCKET_TIMEOUT_INFINITE tag:SOCKET_DATA_STAGE_READ_HEADER];
    [self sendLoginUser];
}

- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag {
    @try {
        [self handleData:data withTag:tag];
    } @catch (NSException *exception) {
        DDLogInfo(@"socket data ex : %@",exception);
    } @finally {
        
    }
}

- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err {
    DDLogInfo(@"socketDidDisconnect : %@", err);
    [self stopConnect];
    if (self.isBackground) {
        return;
    }
    
    self.retryCount ++;
    int delayInSecond = [self delayTimeFromRetryCount:self.retryCount];
    [self.scheduler afterDelay:delayInSecond schedule:^{
        [self startConnect];
    }];
}

- (void)handleData:(NSData *)data withTag:(long)tag {
    if (tag == SOCKET_DATA_STAGE_READ_HEADER) {
        uint32_t headerLength = 0;
        memcpy(&headerLength, [data bytes], sizeof(uint32_t));
        headerLength = ntohl(headerLength);
        NSInteger bodyLength = (NSInteger)headerLength;
        if (bodyLength > 0 && bodyLength < SOCKET_BODY_LENGTH) {
            [self.socket readDataToLength:bodyLength withTimeout:SOCKET_TIMEOUT_INFINITE tag:SOCKET_DATA_STAGE_READ_BODY];
        } else {
            DDLogInfo(@"body length = %lld",(long long) bodyLength);
        }
        return;
    }
    
    if (tag == SOCKET_DATA_STAGE_READ_BODY) {
        double receiveTime = [[NSDate date] timeIntervalSince1970] * 1000;
        [self.messageScheduler schedule:^{
            NSError *error;
            DataResponseUser *response = [[DataResponseUser alloc] initWithData:data error:&error];
            if (response) {
                ZPMessage *msg = [self.parsers messageFromData:response receiveTime:receiveTime];
                [self handleMessage:msg];
            } else {
                DDLogInfo(@"error = %@",error);
            }
        }];
        [self.socket readDataToLength:sizeof(uint32_t) withTimeout:SOCKET_TIMEOUT_INFINITE tag:SOCKET_DATA_STAGE_READ_HEADER];
    }
}

- (void)handleMessage:(ZPMessage *)msg {
    [self sendReceiveStatus:msg];
    if ([msg isKindOfClass:[ZPAuthenMessage class]]) {
        ZPAuthenMessage *authen = (ZPAuthenMessage *)msg;
        DDLogInfo(@"authen result = %d code = %d",authen.result, authen.code);
        self.isAuthenSuccess = authen.result == ZALOPAY_ERRORCODE_SUCCESSFUL;
    }
    [self.subject sendNext:msg];
}

- (int)delayTimeFromRetryCount:(int)retryCount {
    if (retryCount < 3) { // 3 lần đầu delay 5s
        return 5;
    }
    return  retryCount * 10;
}

- (void)sendLoginUser {
    [self sendLoginData:self.userData.accessToken
             appleToken:self.userData.appleNotifyToken
              zaloPayId:self.userData.zaloPayId];
}

- (void)sendMessageData:(NSData *)strData type:(uint8_t)nType {
    NSMutableData *buffer = [[NSMutableData alloc] init];
    uint32_t headerLength = (uint32_t)[strData length] + sizeof(uint8_t);
    headerLength = htonl(headerLength);
    [buffer appendBytes:&headerLength length:sizeof(uint32_t)];
    [buffer appendBytes:&nType length:sizeof(uint8_t)];
    [buffer appendBytes:[strData bytes] length:[strData length]];
    [self.socket writeData:buffer withTimeout:SOCKET_TIMEOUT_INFINITE tag:0];
}

- (RACSignal *)requestWithDomain:(NSString *)domain
                            path:(NSString *)path
                            data:(NSDictionary*)data {
    
    uint64_t tempId = [self requesId];
    RACSignal *signal = [[[self.messageSignal filter:^BOOL(ZPPaymentResponseMessage *value) {
        bool isDisconnect = [value isKindOfClass:[ZPDisconnectionMessage class]];
        bool isResponse = [value isKindOfClass:[ZPPaymentResponseMessage class]] && value.requestId == tempId;
        return isDisconnect || isResponse;
    }] take:1] timeout:timeoutIntervalForConnectorRequest onScheduler:[RACScheduler scheduler]];
    
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        double startTime = [[NSDate date] timeIntervalSince1970] * 1000;
        [signal subscribeNext:^(id value) {
            if ([value isKindOfClass:[ZPPaymentResponseMessage class]]) {
                ZPPaymentResponseMessage *response = (ZPPaymentResponseMessage *)value;
                double timimg = response.receiveTime - startTime;
                [[NetworkManager sharedInstance] trackRequestWithEventId:connectorEventIdFromApi(path) withTiming:timimg];
                NSDictionary *json = [response.resultData JSONValue];
                [subscriber sendNext:json];
                [subscriber sendCompleted];
                return;
            }
            DDLogInfo(@"connector request did disconnected");
            NSError *err = [NSError errorWithDomain:@"Socket Did Disconnected" code:NSURLErrorTimedOut userInfo:nil];
            [subscriber sendError:err];
        } error:^(NSError *error) {
            DDLogInfo(@"connector request did timeout");
            NSError *err = [NSError errorWithDomain:@"Socket Time Out" code:NSURLErrorTimedOut userInfo:nil];
            [subscriber sendError:err];
        }];
        [self sendPaymentRequestWithRequestId:tempId domain:domain path:path data:data];
        return nil;
    }];
}

@end

