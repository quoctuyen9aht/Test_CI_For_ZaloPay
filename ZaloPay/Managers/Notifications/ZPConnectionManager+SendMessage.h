//
//  ZPConnectionManager+SendMessage.h
//  ZaloPay
//
//  Created by bonnpv on 3/21/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPConnectionManager.h"
@class ZPMessage;

@interface ZPConnectionManager (SendMessage)
- (void)sendReceiveStatus:(ZPMessage *)message;
- (void)sendReadStatus:(ZPMessage *)message;
- (void)sendDeleteStatus:(ZPMessage *)message;

- (void)sendReceiveStatusWithMtaid:(uint64_t)mtaid mtuid:(uint64_t)mtuid;
- (void)sendReadStatusWithMtaid:(uint64_t)mtaid mtuid:(uint64_t)mtuid;
- (void)sendDeleteStatusWithMtaid:(uint64_t)mtaid mtuid:(uint64_t)mtuid;

- (void)sendRecoveryRequest:(uint64_t)minTime count:(int)count;

- (void)sendLoginData:(NSString *)token appleToken:(NSString *)appleToken zaloPayId:(uint64_t)zaloPayId;

- (void)sendPaymentRequestWithRequestId:(long long)requestId
                                 domain: (NSString *)domain
                                   path:(NSString *)path
                                   data:(NSDictionary *)param;

- (uint64_t)requesId;

- (void)sendLogoutRequest;
@end
