//
//  ZPPingServeModul.m
//  ZaloPay
//
//  Created by bonnpv on 8/24/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPPingServerModule.h"
#import "ZPConnectionManager.h"
#import "ZPMessage.h"
#import "Zpmsguser.pbobjc.h"
#import <ZaloPayAnalyticsSwift/ZaloPayAnalyticsSwift-Swift.h>

extern float socketPingInterval;
extern float socketPingTimout;

@implementation ZPPingServerModule

- (void)registerMessageSignal {
    [[ZPConnectionManager sharedInstance].messageSignal subscribeNext:^(ZPMessage *message) {
        if ([message isKindOfClass:[ZPAuthenMessage class]]) {
            ZPAuthenMessage *authen = (ZPAuthenMessage *)message;
            if (authen.result == 1) {
                [self startPingServer];
            }
            return;
        }
        
        if ([message isKindOfClass:[ZPDisconnectionMessage class]]) {
            [self stopPingServer];
            return;
        }
        
        if ([message isKindOfClass:[ZPPongMessage class]]) {
            [self handlePongMessage:(ZPPongMessage *)message];
        }
    }];
}

- (void)sendPing {
    RACSignal *untilSignal = [RACSignal merge:@[[self rac_signalForSelector:@selector(handlePongMessage:)],
                                                [self rac_signalForSelector:@selector(stopPingServer)]]];
    @weakify(self);
    [[[[RACSignal return:nil] delay:socketPingTimout] takeUntil:untilSignal] subscribeNext:^(id x) {
        @strongify(self);
        [self handlePingTimeout];
    }];
    
    [self sendPingMessage:[[NSDate date] timeIntervalSince1970] * 1000];
}

- (void)handlePingTimeout {
    DDLogInfo(@"handle ping timout");
    [self stopPingServer];
    [[ZPConnectionManager sharedInstance] reconnect];
}


- (void)sendPingMessage:(uint64_t)embeddata  {
    MessageConnectionInfo *ping = [[MessageConnectionInfo alloc] init];
    ping.userid = [[ZPConnectionManager sharedInstance] zaloPayId];
    ping.embeddata = embeddata;
    [[ZPConnectionManager sharedInstance] sendMessageData:[ping data] type:MessageType_PingServer];
}

- (void)handlePongMessage:(ZPPongMessage *)pongMessage {
    NSTimeInterval ellapsedTime = pongMessage.receiveTime - pongMessage.embedata;
    [[ZPTrackingHelper shared].eventTracker trackTiming:ZPAnalyticEventActionApi_notification_ping_pong value:@(ellapsedTime)];
}

- (void)startPingServer {
    @weakify(self);
    [[[RACSignal interval:socketPingInterval onScheduler:[RACScheduler scheduler]] takeUntil:[self rac_signalForSelector:@selector(stopPingServer)]] subscribeNext:^(id x) {
        @strongify(self);
        [self sendPing];
    }];
}

- (void)stopPingServer {
    DDLogInfo(@"stop ping server");
}


@end
