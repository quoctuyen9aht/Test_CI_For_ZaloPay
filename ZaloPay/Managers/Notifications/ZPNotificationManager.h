//
//  ZPNotificationManager.h
//  ZaloPay
//
//  Created by bonnpv on 6/14/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface ZPNotificationManager : NSObject
+ (instancetype)sharedInstance;
- (void)registerMessageSignal;
@end
