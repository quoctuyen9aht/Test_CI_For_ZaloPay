//
//  ZPPingServeModul.h
//  ZaloPay
//
//  Created by bonnpv on 8/24/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZPPingServerModule : NSObject
- (void)registerMessageSignal;
@end
