//
//  NotifyMessage.m
//  ZaloPay
//
//  Created by bonnpv on 6/14/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPMessage.h"

@implementation ZPMessage
@end

@implementation ZPDisconnectionMessage
@end

@implementation ZPAuthenMessage
@end

@implementation ZPKickOutMessage
@end

@implementation ZPPongMessage
@end

@implementation ZPRecoveryMessage
@end

@implementation ZPNotifyMessage
@end

@implementation ZPProfileMessage
@end

@implementation ZPLixiMessage
@end

@implementation ZPQRTransferAppMessage
@end
@implementation ZPFromAppMessage
@end
@implementation ZPThankMessage
@end
@implementation ZPRemoveExpireCardMessage
@end

@implementation ZPPaymentResponseMessage
@end
@implementation ZPCashbackMessage
@end

@implementation ZPVoucherMessage
@end
@implementation ZPLiXiWishesMessage
@end
@implementation ZPQRToPayMessage
@end

@implementation ZPGatewayMessage
@end
