//
//  ZPConnectionManager+SendMessage.m
//  ZaloPay
//
//  Created by bonnpv on 3/21/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPConnectionManager+SendMessage.h"
#import "Zpmsguser.pbobjc.h"
#import "ZPMessage.h"

@implementation ZPConnectionManager (SendMessage)

- (void)sendReceiveStatus:(ZPMessage *)message {
    [self sendReceiveStatusWithMtaid:message.mtaid mtuid:message.mtuid];
}

- (void)sendReadStatus:(ZPMessage *)message {
    [self sendReadStatusWithMtaid:message.mtaid mtuid:message.mtuid];
}

- (void)sendDeleteStatus:(ZPMessage *)message {
    [self sendDeleteStatusWithMtaid:message.mtaid mtuid:message.mtuid];
}

- (void)sendReceiveStatusWithMtaid:(uint64_t)mtaid mtuid:(uint64_t)mtuid {
    [self sendStatus:MessageStatus_Received mtaid:mtaid mtuid:mtuid];
}

- (void)sendReadStatusWithMtaid:(uint64_t)mtaid mtuid:(uint64_t)mtuid {
    [self sendStatus:MessageStatus_Read mtaid:mtaid mtuid:mtuid];
}

- (void)sendDeleteStatusWithMtaid:(uint64_t)mtaid mtuid:(uint64_t)mtuid {
    [self sendStatus:MessageStatus_Deleted mtaid:mtaid mtuid:mtuid];
}

- (void)sendStatus:(int)status mtaid:(uint64_t)mtaid mtuid:(uint64_t)mtuid {
    if ([self validMtaid:mtaid mtuid:mtuid]) {
        [self sendMessageStatus:status mtaid:mtaid mtuid:mtuid];
    }
}

- (BOOL)validMtaid:(uint64_t)mtaid mtuid:(uint64_t)mtuid {
    if (mtuid == 0 && mtaid == 0) {
        return FALSE;
    }
    return TRUE;
}

- (void)sendRecoveryRequest:(uint64_t)minTime count:(int)count {
    DDLogInfo(@"send recovery with timestamp : %lld",minTime);
    [self requestRecoveryMessageFrom:minTime order:2 count:count];
}

#pragma mark - Internal

- (void)sendLoginData:(NSString *)token appleToken:(NSString *)appleToken zaloPayId:(uint64_t)zaloPayId{
//    MessageLoginBuilder *builder = [MessageLogin builder];
//    [builder  setToken:token];
//    [builder setUsrid:zaloPayId];
//    [builder setOstype:OSTypeIos];
//    if (appleToken) {
//        [builder setDevicetoken:appleToken];
//    }
    MessageLogin *message = [[MessageLogin alloc] init];
    message.token = token;
    message.usrid = zaloPayId;
    message.ostype = OSType_Enum_Ios;
    message.devicetoken = appleToken;
    NSData* strData = [message data];
    [self sendMessageData:strData type:MessageType_AuthenLogin];
}

- (void)sendMessageStatus:(int)status
                    mtaid:(uint64_t)mtaid
                    mtuid:(uint64_t)mtuid {
    StatusMessageClient *message = [[StatusMessageClient alloc] init];
    message.mtaid = mtaid;
    message.mtuid = mtuid;
    message.status = status;
    message.userid = self.zaloPayId;
    [self sendMessageData:[message data] type:MessageType_Feedback];
}

- (void)requestRecoveryMessageFrom:(uint64_t)startTime order:(int)order count:(int)count{
    MessageRecoveryRequest *request = [[MessageRecoveryRequest alloc] init];
    request.count = count;
    request.starttime = startTime;
    request.order = order;
    [self sendMessageData:[request data] type:MessageType_RecoveryRequest];
}

- (void)sendPaymentRequestWithRequestId:(long long)requestId
                                 domain: (NSString *)domain
                                   path:(NSString *)path
                                   data:(NSDictionary *)param {
    DDLogInfo(@"\n requestId : %lld \n doimain: %@ \n path: %@ \n params: %@",requestId, domain, path, param);
    
    PaymentRequestMessage *message = [[PaymentRequestMessage alloc] init];
    message.requestid = requestId;
    message.domain = domain;
    message.path = path;
    [param enumerateKeysAndObjectsUsingBlock: ^(id name, id obj, BOOL *stop) {
        NameValuePair *value = [[NameValuePair alloc] init];
        [value setName:name];
        [value setValue:[NSString stringWithFormat:@"%@",obj]];
        [message.paramsArray addObject:value];        
    }];
    
    NSData* strData = [message data];
    [self sendMessageData:strData type:MessageType_PaymentRequest];
}

- (uint64_t)requesId {
    static uint64_t requestId;
    if (requestId == 0) {
        requestId = [[NSDate date] timeIntervalSince1970] * 1000;
#if  DEBUG
        requestId = 1;
#endif
        return requestId;
    }
    return ++requestId;
}

- (void)sendLogoutRequest {
    if (self.zaloPayId <= 0) {
        return;
    }
    MessageLogout *logout = [[MessageLogout alloc] init];
    logout.userid = self.zaloPayId;
    NSData *data = [logout data];
    [self sendMessageData:data type:MessageType_Logout];
}

@end
