//
//  NSNotificationCenter+ZaloPay.h
//  ZaloPay
//
//  Created by bonnpv on 7/7/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNotificationCenter (ZaloPay)
- (RACSignal *)signalAppEnterForceground;
- (RACSignal *)signalAppEnterBackground;
- (RACSignal *)enterBackground;
@end
