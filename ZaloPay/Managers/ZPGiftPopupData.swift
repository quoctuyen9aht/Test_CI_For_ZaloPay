//
//  ZPGiftPopupData.swift
//  ZaloPay
//
//  Created by Bon Bon on 2/5/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit

class ZPGiftPopupData: ZPGiftPopupModel {
//    var isUnread = true
    var message: ZPNotifyMessage
    var info = ""
    
    init(message: ZPNotifyMessage) {
        self.message = message
    }
}
