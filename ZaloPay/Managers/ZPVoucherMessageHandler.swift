//
//  ZPVoucherMessageHandler.swift
//  ZaloPay
//
//  Created by Bon Bon on 2/5/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation

class ZPVoucherMessageHandler: NSObject, ZPGiftPopupHandler {
    weak var popup: ZPVoucherPopup?
    
    func canHandleMessage(_ message: ZPNotifyMessage) -> Bool {
        return message is ZPVoucherMessage
    }
    
    func handleMessage(_ message: ZPNotifyMessage) {
        guard let msg = message as? ZPVoucherMessage else {
            return
        }
        if msg.data.type != kVoucherDataType {
            return
        }
        self.popup = ZPVoucherPopup.showWithData(data: msg.data) {
            if msg.data.action == GiftPopupAction.showListGiftCode.rawValue {
                ZPCenterRouter.launch(routerId: .voucher, from: nil)
            } else {
                self.popup?.hide()
            }
            } as? ZPVoucherPopup
    }
    
    func isPopupShowing() -> Bool {
        return self.popup != nil
    }
}
