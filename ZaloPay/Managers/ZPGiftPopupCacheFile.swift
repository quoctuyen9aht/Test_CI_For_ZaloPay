//
//  ZPGiftPopupCacheFile.swift
//  ZaloPay
//
//  Created by Bon Bon on 2/9/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation

class ZPGiftPopupCacheFile: ZPGiftPopupCacheProtocol {
    
    public func isPopupMessage(_ message: ZPNotifyMessage) -> Bool {
        return message.notificationType == NotificationType.cashback ||
            message.notificationType == NotificationType.voucher ||
            message.notificationType == NotificationType.liXiWishes
    }
    
    public func  save(_ message: ZPNotifyMessage) {
        
    }
    
    public func allMessage() -> [ZPNotifyMessage] {
        return [ZPNotifyMessage]()
    }
    
    public func clearData() {
        
    }
    
    public func updateState(with message: ZPNotifyMessage) {
        
    }
}

