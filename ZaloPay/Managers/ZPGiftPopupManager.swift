//
//  ZPGiftPopupManager.swift
//  ZaloPay
//
//  Created by Bon Bon on 2/3/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

let kCashbackDataType = 1
let kVoucherDataType = 2
let kLixiWishesDataType = 3

let kMaxUnreadMessage = 99

@objcMembers
class ZPGiftPopupManager: NSObject {
    static let sharedInstance: ZPGiftPopupManager = ZPGiftPopupManager()
    var allMessage = [ZPGiftPopupModel]()
    var handlers = [ZPGiftPopupHandler]()
    weak var delegate: ZPGiftPopupShowHandlerProtocol?
    public let messageCache: ZPGiftPopupCacheProtocol = ZPGiftPopupCacheDB()
    let notifyTable: ZPNotifyTable = ZPNotifyTable(db: PerUserDataBase.sharedInstance())
    
    override init() {
        super.init()
        self.registerAllHandler()
        loadCacheGift()
    }
    
    private func loadCacheGift() {
        let list = messageCache.allMessage().map({ ZPGiftPopupData.init(message: $0) })
        self.allMessage = list
    }
    
    func registerAllHandler() {
        handlers.append(ZPVoucherMessageHandler())
        handlers.append(ZPCashbackMessageHandler())
        handlers.append(ZPLixiWishesMessageHandler())
    }
    
    func observerMessageSignal() {
        _ = (~ZPConnectionManager.sharedInstance().messageSignal.filter({ [weak self] (msg) -> Bool in
            if let message = msg as? ZPNotifyMessage, let _ = self?.findHandler(message) {
                return true
            }
            return false
        })).observeOn(MainScheduler.instance).do(onNext: { [weak self](msg) in
            if let message = msg as? ZPNotifyMessage, self?.isListPopupShowing() == false {
              self?.messageCache.save(message)
            }            
        }).map({ return ZPGiftPopupData(message: $0 as! ZPNotifyMessage)}).buffer(timeSpan: 1.5, count: 100, scheduler: MainScheduler.instance).filter({ $0.isEmpty.not}).subscribe(onNext:{ [weak self] (messages) in
            self?.runWhenResourceReady {
                self?.handleGifMessages(messages)
            }
        })
    }
    
    func findHandler(_ message: ZPNotifyMessage) -> ZPGiftPopupHandler? {
        return handlers.first(where: { $0.canHandleMessage(message)})
    }
    
    private func runWhenResourceReady( _ completeHandle:@escaping () -> Void) {
        let appModel = ReactNativeAppManager.sharedInstance().getApp(reactInternalAppId)
        if appModel.appState ==  ReactNativeAppStateReadyToUse {
            completeHandle()
            return
        }
        ReactNativeAppManager.sharedInstance().observe(self, fromAppId: showshowAppId, complete: {
            completeHandle()
        })
    }
    
    private func getGiftPopupType() -> ZPGiftPopupShow {
        let totalGift: Int = self.totalUnredMessage()
        if totalGift <= 0  {
            return .none
        }
        let type: ZPGiftPopupShow = (totalGift > 1) ? .list : .popup
        return type
    }
    
    private func showGiftPopup(type: ZPGiftPopupShow) {
        if type == .popup {
            self.delegate?.handlerShowPopup(with: .popup)
            return
        }
        self.delegate?.handlerShowPopup(with: .list)
    }
    
    func handleUnreadMessages() {
        let giftPopupType = getGiftPopupType()
        if giftPopupType == .none {
            return
        }
        runWhenResourceReady {[weak self]  in
            self?.showGiftPopup(type: giftPopupType)
        }
    }
    
    func handleGifMessages(_ messages: [ZPGiftPopupModel]) {
        if messages.isEmpty {
            return
        }
        self.allMessage.append(contentsOf: messages)
        
        // case đang show list -> reload lại danh sách.
        if isListPopupShowing() {
            updateListPopup()
            return
        }
        
        // case đang show popup -> hiển thị toast.
        if isPopupShowing(), let model = messages.last {
            let unread = self.totalUnredMessage()
            let total = unread > kMaxUnreadMessage ? "\(kMaxUnreadMessage)+" : "\(unread)"
            let infor: JSON = ["model": model, "total": total]
            NotificationCenter.default.post(name: ZPGiftPopupToastUpdateNotification, object: infor)
            return
        }
        
        // case chỉ có một message -> show popup
        if messages.count == 1 {
            self.delegate?.handlerShowPopup(with: .popup)
            return
        }
        // case nhiều message -> show danh sách.
        self.delegate?.handlerShowPopup(with: .list)
    }
    
    func showPopup(_ message: ZPNotifyMessage) {
        if let handler = findHandler(message) {
            handler.handleMessage(message)
            message.isUnread = false
            // Update temp
            messageCache.updateState(with: message)
            
//            mark notify as read and update total unread message
            self.notifyTable.updateStateRead(withMtaid: message.mtaid, mtuid: message.mtuid)
            ZPNotificationService.sharedInstance().updateTotalUnreadNotify()
        }
    }
    
    func addListPopup() {
        let navi =  UINavigationController.currentActiveNavigationController()
        let viewcontroller = ZPGiftCodeViewController()
        navi?.pushViewController(viewcontroller, animated: true)
        viewcontroller.setListNotification(self.allGiftDataSource())
        self.messageCache.clearData()
    }
    
    func updateListPopup() {
        let currentMessageVC = getCurrentPopupListController();
        currentMessageVC?.popMe()
        currentMessageVC?.setListNotification(self.allGiftDataSource())
        self.messageCache.clearData()
    }
    
    func showListPopup()  {
        isListPopupShowing() ? updateListPopup() : addListPopup()
    }
    
    func isPopupShowing() -> Bool {
        for handler in self.handlers {
            if handler.isPopupShowing() {
                return true
            }
        }
        return false
    }
    
    func isListPopupShowing() -> Bool {
        return getCurrentPopupListController() != nil
    }
    
    func getCurrentPopupListController() -> ZPGiftCodeViewController?  {
        let navi =  UINavigationController.currentActiveNavigationController()
        return navi?.viewControllers.first(where:{ $0 is ZPGiftCodeViewController }) as? ZPGiftCodeViewController
    }
    
    func logout() {
        self.allMessage.removeAll()
    }
}

extension ZPGiftPopupManager {
    
    func totalUnredMessage() -> Int {
        return self.allMessage.filter({ $0.message.isUnread }).count
    }
    
    func allGiftDataSource() -> [ZPGiftPopupModel] {
        return self.allMessage.sorted(by: { (obj1, obj2) -> Bool in
            return obj1.message.timestamp > obj2.message.timestamp
        })
    }
}
