//
//  ZPGiftPopupCache.swift
//  ZaloPay
//
//  Created by Bon Bon on 2/9/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit

class ZPGiftPopupCacheDB: ZPGiftPopupCacheProtocol {
    private var notifyPopupTable: ZPNotifyPopupTable!
    
    init() {
        notifyPopupTable = ZPNotifyPopupTable(db: PerUserDataBase.sharedInstance())
    }
    
    lazy var parsers = ZPPushNotificationParser()
    
    public func isPopupMessage(_ message: ZPNotifyMessage) -> Bool {
        return message.notificationType == NotificationType.cashback ||
               message.notificationType == NotificationType.voucher ||
               message.notificationType == NotificationType.liXiWishes
    }
    
    public func  save(_ message: ZPNotifyMessage) {
        notifyPopupTable.addNotifyPopup(withTranId: message.transid,
                                                         timestamp: message.timestamp,
                                                         appid: message.appId,
                                                         message: message.message,
                                                         transtype: 0,
                                                         userid: message.senderUserId,
                                                         destuserid: message.destuserid,
                                                         isUnread: message.isUnread,
                                                         notifyType: message.notificationType.rawValue.toInt32(),
                                                         embeddata: message.embeded,
                                                         mtaid: message.mtaid,
                                                         mtuid: message.mtuid,
                                                         area: message.area)
    }
    
    public func allMessage() -> [ZPNotifyMessage] {
        let listGift = notifyPopupTable.getListNotifyPopupMessageUnRead() ?? []
        var result  = [ZPNotifyMessage]()
        for messageInfo in listGift {
            if let info = messageInfo as? JSON,
                let message = self.parsers.parse(fromJSON: info) {
                message.mtaid = info.uInt64(forKey: "mtaid")
                message.mtuid = info.uInt64(forKey: "mtuid")
                message.isUnread = info.bool(forKey: "unread")
                result.add(element: message)
            }
        }
        return result
    }
    
    public func clearData() {
        notifyPopupTable.deleteAlllNotificationPopup()
    }
    
    public func updateState(with message: ZPNotifyMessage) {
        let mtaid = message.mtaid;
        let mtuid = message.mtuid
        notifyPopupTable.updateStateReadNotifyPopUp(withMtaid: mtaid, mtuid: mtuid)
    }
}

