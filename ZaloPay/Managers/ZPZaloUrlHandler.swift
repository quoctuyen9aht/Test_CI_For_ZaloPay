//
//  ZPZaloSchemeHelper.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 10/9/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import ZaloPayCommonSwift
import ZaloPayProfile
import ZaloPayConfig

let transferScheme = "zalopay-zapi-28://app/transfer"
let mywalletScheme = "zalopay-zapi-29://app/mywallet"

enum ZaloTransferMode: String {
    case money =  "money"
    case redpacket = "redpacket"
}

// MARK: - UrlHandler

class ZPZaloUrlHandler : UrlHandler {
    func canHandle(scheme: ZPScheme, authenticated: Bool) -> Bool {
        return authenticated && (scheme.domain.hasPrefix(transferScheme) ||
                                 scheme.domain.hasPrefix(mywalletScheme))
    }
    
    func handleUrl(scheme: ZPScheme) {
        if scheme.domain.hasPrefix(mywalletScheme) {
            handleMyWallet(scheme: scheme)
            return
        }
        
        
        
        if scheme.domain.hasPrefix(transferScheme) {
            if ZPAppFactory.sharedInstance().isPayingBill() {
                ZPDialogView.showDialog(with: DialogTypeNotification,
                                        message: R.string_AppToApp_Popup_When_IsBilling(),
                                        cancelButtonTitle: R.string_ButtonLabel_Close(),
                                        otherButtonTitle: nil,
                                        completeHandle: nil)
                return
            }
            
            let mode = zaloTransferMode()
            if mode == .money {
                preHandleSendMoneyScheme(scheme: scheme)
            } else if mode == .redpacket {
                openLixiApp(scheme)
            }
            return
        }
    }
}

// MARK: - Internal

extension ZPZaloUrlHandler {
    func isSameZaloAccount(zaloId: String) -> Bool {
        let currentZaloId = ZaloSDKApiWrapper.sharedInstance.zaloUserId() 
        return  currentZaloId == zaloId
    }
    
    func showLoginViewWithScheme(scheme: ZPScheme) -> ZPLoginViewController {
        LoginManagerSwift.sharedInstance.clearLoginData()
        let login  = ZPLoginViewController.loginViewWihtCompleteHande { [weak self] (loginData) in
            let loginData = loginData as? Dictionary<String,Any> ?? [:]
            LoginManagerSwift.sharedInstance.handleLoginData(loginDic: loginData)
            self?.handleUrl(scheme: scheme)
        }
        ZPUIAppDelegate.sharedInstance.window?.rootViewController = login
        return login
    }
    
    func switchAccount(scheme: ZPScheme ) {
        ZPAppFactory.sharedInstance().showHUDAdded(to: nil)
        ZPProfileManager.shareInstance.loadZaloUserProfile().deliverOnMainThread().subscribeNext({ [weak self] (result) in
            ZPAppFactory.sharedInstance().hideAllHUDs(for: nil)
            self?.alertSwitchAccount(scheme: scheme)
            }, error:({ [weak self] (error) in
                ZPAppFactory.sharedInstance().hideAllHUDs(for: nil)
                self?.alertSwitchAccount(scheme: scheme)
            }))
    }
    
    func accountDisplayName() -> String {
        guard let loginAcount =  ZPProfileManager.shareInstance.userLoginData else {
            return ""
        }
        if let displayName = ZPProfileManager.shareInstance.currentZaloUser?.displayName, displayName.count != 0 {
            return displayName
        }
        if loginAcount.accountName.count != 0 {
            return loginAcount.accountName
        }
        if loginAcount.phoneNumber.count != 0 {
            return loginAcount.phoneNumber
        }
        if loginAcount.paymentUserId.count != 0 {
            return loginAcount.paymentUserId
        }
        return ""              
    }
    
    func alertSwitchAccount(scheme: ZPScheme) {
        let msg = String(format: R.string_Zalo_Scheme_SwitchAccountMessage(), accountDisplayName())
        ZPDialogView.showDialog(with: DialogTypeNotification,
                                message: msg,
                                cancelButtonTitle: R.string_ButtonLabel_Close(),
                                otherButtonTitle: [R.string_Zalo_Scheme_SwitchAccount()]) { (buttonIndex, cancelButtonIndex) in
                                    if buttonIndex == cancelButtonIndex {
                                        self.openZaloApp()
                                        return
                                    }
                                    let loginView : ZPLoginViewController = self.showLoginViewWithScheme(scheme: scheme)
                                    loginView.loginButtonClick()
        }
    }
    
    func openZaloApp() {
        UIApplication.shared.openURL(URL.zaloUrl())
    }
    
    func zaloTransferMode() -> ZaloTransferMode {
        let transferMode = ZPApplicationConfig.getZaloConfig()?.getTransferMode() ?? ""
        return ZaloTransferMode.init(rawValue: transferMode) ?? .redpacket
    }
}

// MARK: - My Wallet
extension ZPZaloUrlHandler {
    
    func handleMyWallet(scheme: ZPScheme) {
        let sender = "\(scheme.params["sender"] ?? "")"
        if sender.isEmpty {
            return
        }
        if self.isSameZaloAccount(zaloId: sender) {
            self.navigateWallet()
            return
        }
        self.switchAccount(scheme: scheme)
    }
    
    func navigateWallet() {
        if let tabbarController = ZPUIAppDelegate.sharedInstance.window?.rootViewController as? TabbarViewController,
            tabbarController.selectedIndex != 3 {
            //3: Profile
            tabbarController.selectedIndex = 3
        }
    }
}

// MARK: - Transfer Money To Friend

extension ZPZaloUrlHandler {
    
    func preHandleSendMoneyScheme(scheme: ZPScheme) {
        
        if ZPAppFactory.sharedInstance().isPayingBill() == false {
            self.handleSendMoneyScheme(scheme: scheme)
            return
        }
        
        if ZaloPayWalletSDKPayment.sharedInstance().checkIsCurrentPaymentFinished() == false {
            self.alertPayingBill()
            return
        }
        
        guard let viewControllers = UINavigationController.currentActiveNavigationController()?.viewControllers else {
            return
        }
        
        for controller in viewControllers {
            if let transfer = controller as? ZPTransferReceiverWrapper {
                transfer.preHandle = nil
                break
            }
        }
        ZaloPayWalletSDKPayment.sharedInstance().closeAndCleanSDK()
        ZPAppFactory.sharedInstance().showHUDAdded(to: nil)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.7 * Double(NSEC_PER_SEC)))) {
            ZPAppFactory.sharedInstance().hideAllHUDs(for: nil)
            self.handleSendMoneyScheme(scheme: scheme)
        }
    }
    
    func alertPayingBill() {
        ZaloPayWalletSDKPayment.sharedInstance().dismissSDKKeyboard()
        ZPDialogView.showDialog(with: DialogTypeNotification,
                                message: R.string_Zalo_Scheme_PayingBill(),
                                cancelButtonTitle: R.string_ButtonLabel_OK(),
                                otherButtonTitle: nil) { (buttonIndex, cancelButtonIndex) in
                                    ZaloPayWalletSDKPayment.sharedInstance().showSDKKeyboard()
                                    self.openZaloApp()
        }
    }
    
    func handleSendMoneyScheme(scheme: ZPScheme) {
        let receiver : String = "\(scheme.params["receiver"] ?? "")"
        let zaloId : String = "\(scheme.params["sender"] ?? "")"
        if receiver.count == 0 || zaloId.count == 0 || receiver == zaloId {
            return
        }
        
        if !(self.isSameZaloAccount(zaloId: zaloId)) {
            self.switchAccount(scheme: scheme)
            return
        }
        let friend = ZPProfileManager.shareInstance.getZaloUserProfile(zaloId: receiver)
        if friend.zaloPayId.isEmpty.not {
            self.sendMoney(to: friend)
            return
        }
        self.tranferMoney(to: receiver)
    }
    
    func tranferMoney(to friendId: String) {
        ZPProfileManager.shareInstance.requestUserProfile(userId: friendId).deliverOnMainThread().subscribeNext({ [weak self] (receiverUser) in
                ZPAppFactory.sharedInstance().hideAllHUDs(for: nil)
                guard let receiverUserGet = receiverUser as? ZaloUserSwift else {
                    self?.alertUserNotUsingZaloPay( R.string_Zalo_Scheme_FriendNotUsingZaloPay())
                    return
                }
                self?.sendMoney(to: receiverUserGet)
            }, error:({ [weak self] (error) in
                ZPAppFactory.sharedInstance().hideAllHUDs(for: nil)
                ZPDialogView.showDialogWithError(error, handle: {
                    self?.openZaloApp()
                })
            }))
    }
    
    func alertUserNotUsingZaloPay(_ message: String) {
        ZPDialogView.showDialog(with: DialogTypeNotification,
                                message:message,
                                cancelButtonTitle: R.string_ButtonLabel_Close(),
                                otherButtonTitle: nil) { (buttonIndex, cancelButtonIndex) in
                                    self.openZaloApp()
        }
    }
    
    func sendMoney(to user: ZaloUserSwift) {
        let navi = UINavigationController.currentActiveNavigationController()
        let transferUser : UserTransferData = UserTransferData.fromZaloUser(user)
        transferUser.source = .fromZalo
        transferUser.mode = .toZaloPayContact
        
        if let top = navi?.topViewController as? ZPTransferReceiverWrapper {
            top.reloadUser(transferData: transferUser)
            return
        }
        let viewController = ZPTransferReceiverWrapper(transferData: transferUser,
                                                       orderSource: OrderSource_Zalo,
                                                       preHandle: {self.openZaloApp()},
                                                       cancelHandle: {},
                                                       errorHandle: {})
        {}
        navi?.pushViewController(viewController, animated: true)
    }
}

// MARK: - Send Lixi

extension ZPZaloUrlHandler {
    func openLixiApp(_ scheme: ZPScheme) {
        let sender  = scheme.params.string(forKey:"sender")
        let receiver = scheme.params.string(forKey: "receiver")
        if receiver.isEmpty || receiver.isEmpty || sender == receiver {
            return
        }
        
        if !(self.isSameZaloAccount(zaloId: sender)) {
            self.switchAccount(scheme: scheme)
            return
        }
        
        let friend = ZPProfileManager.shareInstance.getZaloUserProfile(zaloId: receiver)
        if friend.zaloPayId.isEmpty.not  {
            self.openLixi(user: friend)
            return
        }
        requestUserProfile(userId: receiver)
    }
    
    func requestUserProfile(userId: String) {
        ZPAppFactory.sharedInstance().showHUDAdded(to: nil)
        ZPProfileManager.shareInstance.requestUserProfile(userId: userId).deliverOnMainThread().flattenMap({ (value) -> RACSignal<AnyObject>? in
            if let user = value as? ZaloUserSwift {
                return RACSignal.return(user)
            }
            return RACSignal.error(nil)
        }).subscribeNext({(receiverUser) in
            ZPAppFactory.sharedInstance().hideAllHUDs(for: nil)
            
            self.openLixi(user: (receiverUser as? ZaloUserSwift) ?? ZaloUserSwift())
        }, error:({ [weak self] (error) in
            ZPAppFactory.sharedInstance().hideAllHUDs(for: nil)
            self?.requestUpdateZaloPayContact(userId: userId)
        }))
    }
    
    func requestUpdateZaloPayContact(userId: String) {
        let manager = ZPProfileManager.shareInstance;
        if manager.isDoneLoadContact() {
            let friend = ZPProfileManager.shareInstance.getZaloUserProfile(zaloId: userId)
            self.lixiAlertInviteFriend(friend)
            return
        }
        
        ZPAppFactory.sharedInstance().showHUDAdded(to: nil)
        manager.loadZaloUserFriendList(forceSyncContact: false).deliverOnMainThread().subscribeCompleted {
            ZPAppFactory.sharedInstance().hideAllHUDs(for: nil)
            let friend = ZPProfileManager.shareInstance.getZaloUserProfile(zaloId: userId)
            if friend.zaloPayId.isEmpty.not {
                self.openLixi(user: friend)
                return
            }
            self.lixiAlertInviteFriend(friend)
        }
    }
    
    func lixiAlertInviteFriend(_ friend: ZaloUserSwift?) {
        let name = friend?.displayName ?? R.string_Zalo_Scheme_Lixi_Default_DisplayName()!
        let message = String.init(format: R.string_Zalo_Scheme_Lixi_FriendNotUsingZaloPay(), name, name)
        self.alertUserNotUsingZaloPay(message)
    }
    
    func openLixi(user: ZaloUserSwift) {
        let controller =  ZPReactNativeAppRouter().openLixiFromZalo(with: user)
        controller?.rac_willDeallocSignal().subscribeCompleted { [weak self] in
            self?.openZaloApp()
        }
    }
}



