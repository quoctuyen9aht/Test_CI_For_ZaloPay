//
//  ZPNotificationAppleHandler.swift
//  ZaloPay
//
//  Created by vuongvv on 10/31/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

protocol ZPNotificationAppleAction {
    func handleOpenWith(model: ZPAppleNotificationModel)
}

