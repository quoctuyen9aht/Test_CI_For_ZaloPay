//
//  ZPInternalWebAppHelper.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 10/6/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import UIKit
import ZaloPayCommonSwift
import ZaloPayAnalyticsSwift
import ZaloPayWeb

let kWebUrlPath = "%@?muid=%@&maccesstoken=%@&appid=%d&isroot=%@"
let kWebResultPath = "%@result/?apptransid=%@&muid=%@&maccesstoken=%@&appid=%d&isroot=%@"
let kWebHistoryPath = "%@history/?muid=%@&maccesstoken=%@&appid=%@&isroot=%@"

@objcMembers
class ZPWebAppRouter : NSObject {
    static func activeWebView() -> ZPWWeDichVu? {
        if let web = UINavigationController.currentActiveNavigationController()?.topViewController as? ZPWWeDichVu {
            return web
        }
        return nil
    }
    
    static func webUrl(withAppId appId: Int, mUid: String, mAccessToken: String) -> String {
        let baseUrl = self.webUrl(withAppId: appId)
        return String(format: kWebUrlPath, baseUrl, mUid, mAccessToken, appId, isRootParams())
    }
    
    static func resultUrl(withAppId appId: Int,
                   mUid: String,
                   mAccessToken: String,
                   zpTransId: String) -> String {
        
        var baseUrl : String = self.webUrl(withAppId: appId)
        if !baseUrl.hasSuffix("/") {
            baseUrl = "\(baseUrl)/"
        }
        return String(format: kWebResultPath, baseUrl, zpTransId, mUid, mAccessToken, Int(appId), isRootParams())
        
    }
    
    static func webUrl(withAppId appId: Int) -> String {
        let app = ReactNativeAppManager.sharedInstance().getApp(appId)
        if app.appType == ReactAppTypeWeb && app.webUrl.count > 0 {
            return app.webUrl
        }
        return ""
    }
    
    static func historyUrl(withAppId appId: Int) -> String {
        let info : ZPMerchantUserInfo? = ReactNativeAppManager.sharedInstance().merchantUserInfo(withAppId: appId)
        let muid = info?.muid ?? ""
        let maccesstoken = info?.maccesstoken ?? ""        
        return self.historyUrl(withAppId: String(appId), mUid: muid, mAccessToken: maccesstoken)
    }
    
   static func historyUrl(withAppId appId: String, mUid muid: String, mAccessToken: String) -> String {
        var baseUrl = self.webUrl(withAppId: Int(appId)!) 
        if !baseUrl.hasSuffix("/") {
            baseUrl = "\(baseUrl)/"
        }
        return String(format: kWebHistoryPath, baseUrl, muid, mAccessToken, appId, isRootParams())
    }
    
    static private func isRootParams() -> String {
        return UIDevice.current.isJailBroken() == true ? "true" : "false"
    }
    
    
    static func openWeb(withAppId appId: Int, viewController controller: UIViewController) {
        ReactNativeAppManager.sharedInstance().getMerchantUserInfo(withAppId: appId).deliverOnMainThread().subscribeNext({ (minfo) in
            guard let info = minfo as? ZPMerchantUserInfo else {
                return
            }
            let muid = info.muid ?? ""
            let maccesstoken = info.maccesstoken ?? ""
            let webUrl  = self.webUrl(withAppId: appId, mUid: muid, mAccessToken: maccesstoken)
            if appId == appDichVuId {
                //Mark flow xử lý cũ của app dịch vụ
                let webview = ZPWWeDichVu(webUrl, appId: appId, webHandle: ZPWebHandle())
                controller.navigationController?.pushViewController(webview, animated: true)
//                ZPAppFactory.sharedInstance().trackScreen(ZPTrackerScreen.Screen_Service)
                return
            }
            let webview = ZPWMerchantWebApp(url: webUrl, webHandle: ZPWebHandle())
            webview.firstUrl = webUrl
            webview.appId = appId
            controller.navigationController?.pushViewController(webview, animated: true)
            
        }, error:({ (error) in
            ZPAppFactory.sharedInstance().hideAllHUDs(for: controller.view)
            ZPDialogView.showDialogWithError(error, handle: nil)
        }))
    }
    
}

