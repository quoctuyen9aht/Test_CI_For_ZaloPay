//
//  ZPNotificationService.m
//  ZaloPay
//
//  Created by bonnpv on 7/13/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <AudioToolbox/AudioToolbox.h>
#import ZALOPAY_MODULE_SWIFT

#import "ZPNotificationService.h"
#import "ZPNotificationManager.h"
#import "NetworkManager+Wallet.h"
#import "MMPopupItem.h"
#import "NetworkManager+Order.h"
#import "BillDetailViewControllerDelegate.h"
#import "ZPConnectionManager.h"
#import "ZPMessage.h"
#import "ZPConnectionManager+SendMessage.h"
#import <ZaloPayDatabase/ZPNotifyTable.h>
#import <ZaloPayDatabase/ZPTransactionLogTable.h>
#import <ZaloPayAppManager/ZaloPayModuleManager.h>
#import <ZaloPayAppManager/RCTBridge+Payment.h>
#import <ZaloPayAppManager/TransactionHistoryManager.h>
#import <ZaloPayRedPacket/ZPRPManager.h>
#import <ZaloPayAnalytics/ZPAOrderTracking.h>
#import "ZPPaymentCodeModel.h"
#import <ZaloPayCommonSwift/ZaloPayCommonSwift-Swift.h>
#import <ZaloPayConfig/ZaloPayConfig-Swift.h>

@interface ZPNotificationService ()
@property (nonatomic, strong) ZPPayBillModel *payBillModel;
@property (nonatomic, strong) RACSubject *signalLoadData;
@property (nonatomic, strong) ZPReactNativeAppRouter *reactRouter;
@property (nonatomic, strong) RACSubject* subjectTotalUnreadMessage;
@property (nonatomic, strong) ZPCacheDataTable* cacheDataTable;
@property (nonatomic, strong) ZPNotifyTable* notifyTable;
@property (nonatomic, strong) ZPTransactionLogTable* transactionLogTable;
@property (nonatomic, strong) TransactionHistoryManager* transactionHistoryManager;
@end

@implementation ZPNotificationService

+ (instancetype)sharedInstance {
    static ZPNotificationService *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[ZPNotificationService alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
        self.transactionHistoryManager = [[TransactionHistoryManager alloc] init];
        
        self.cacheDataTable = [[ZPCacheDataTable alloc] initWithDB:[PerUserDataBase sharedInstance]];
        self.notifyTable = [[ZPNotifyTable alloc] initWithDB:[PerUserDataBase sharedInstance]];
        self.transactionLogTable = [[ZPTransactionLogTable alloc] initWithDB:[PerUserDataBase sharedInstance]];
        
        self.signalLoadData = [RACSubject subject];
        self.subjectTotalUnreadMessage = [RACSubject subject];
        [self handleReloadDataSignal];
        [self observerLixiMessage];
    }
    return self;
}

- (void)dealloc {
    [self.subjectTotalUnreadMessage sendCompleted];
}

- (ZPPayBillModel *)payBillModel {
    if (!_payBillModel) {
        _payBillModel = [[ZPPayBillModel alloc] init];
    }
    return _payBillModel;
}

- (ZPReactNativeAppRouter *)reactRouter {
    if (!_reactRouter) {
        _reactRouter = [[ZPReactNativeAppRouter alloc] init];
    }
    return _reactRouter;
}

#pragma mark - Signals
- (RACSignal *)signalTotalUnreadMessage {
    return self.subjectTotalUnreadMessage;
}

#pragma mark - Recovery

- (void)checkToLoadRecoveryMessage {
    
    LoginManagerSwift *login = [LoginManagerSwift sharedInstance];
    RACSignal *logoutSignal = [login rac_signalForSelector:@selector(logout)];
    RACSignal *doneSignal = [self rac_signalForSelector:@selector(doneLoadRecoveryMessage:)];
    RACSignal *untilSignal = [RACSignal merge:@[logoutSignal, doneSignal]];
    
    BOOL isEmptyNotification = [self.notifyTable notificationMinTimestamp] == 0;
    int pageCount = 30;
    __block uint64_t recoveryTimestamp = 1;
    
    [[[[ZPConnectionManager sharedInstance].messageSignal takeUntil:untilSignal]
      filter:^BOOL(ZPMessage *message) {
          if ([message isKindOfClass:[ZPAuthenMessage class]]) {
              ZPAuthenMessage *authen = (ZPAuthenMessage *)message;
              return authen.result == ZALOPAY_ERRORCODE_SUCCESSFUL;
          }
          return [message isKindOfClass:[ZPRecoveryMessage class]];
      }] subscribeNext:^(ZPMessage *message) {
          
          if ([message isKindOfClass:[ZPRecoveryMessage class]]) {
              ZPRecoveryMessage *recovery = (ZPRecoveryMessage *)message;
              [self reloadNotificationList];
              if (recovery.recoveryMessage.count < pageCount) {
                  [self doneLoadRecoveryMessage:isEmptyNotification];
                  return;
              }
              ZPNotifyMessage *lastMessage = [self minTimestampMessage:recovery.recoveryMessage];
              if (lastMessage.timestamp == 0 || (recoveryTimestamp > 100 && lastMessage.timestamp >= recoveryTimestamp)) {
                  DDLogInfo(@"receive invalid recovery message");
                  DDLogInfo(@"timestamp : %llu lastMessage timestamp: %llu",recoveryTimestamp, lastMessage.timestamp);
                  [self doneLoadRecoveryMessage:isEmptyNotification];
                  return;
              }
              recoveryTimestamp = lastMessage.timestamp;
          }
          [[ZPConnectionManager sharedInstance] sendRecoveryRequest:recoveryTimestamp count:pageCount];
      }];
}
- (ZPNotifyMessage *)minTimestampMessage:(NSArray *)allMessage {
    ZPNotifyMessage *min = allMessage.firstObject;
    for (ZPNotifyMessage *message in allMessage) {
        if (min.timestamp > message.timestamp) {
            min = message;
        }
    }
    return min;
}


- (void)doneLoadRecoveryMessage:(BOOL)isEmptyNotification {
    DDLogInfo(@"done load recovery message");
    if (isEmptyNotification) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10.0 * NSEC_PER_SEC)), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            uint64_t minTime = [self.notifyTable notificationMinTimestamp];
            [self.transactionHistoryManager updateTransctionLogWithNotificationTimestamp:minTime];
        });
    }
    [[ZPAppFactory sharedInstance].rpManager updateLixiPacketStatus];
}

#pragma mark - Notification

- (void)registerMessageSignal {
    [[[[ZPConnectionManager sharedInstance].messageSignal filter:^BOOL(ZPNotifyMessage *notify) {
        if (![notify isKindOfClass:[ZPNotifyMessage class]]) {
            return false;
        }
        if (notify.notificationType == Notification_Promotion ||
            (notify.notificationType == Notification_FromApp && ![notify isKindOfClass:[ZPThankMessage class]]) ) {
            return false;
        }
        return true;
    }]delay:.2] subscribeNext:^(ZPNotifyMessage *notify) {
        
        [self reloadNotificationList];
        [self forwardInAppNotificationToSDK:notify];
        
        if ([self shouldNotifyToUser:(ZPNotifyMessage *)notify]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self updateTotalUnreadNotify];
            });
        }
        
        switch (notify.notificationType) {
            case Notification_PayBill: // đã update balance sau khi paybill
            case Notification_Recharge: // đã update balance sau khi paybill
            case Notification_MapCard: // đã update balance sau khi paybill
            case Notification_Withdraw: // đã update balance sau khi paybill
            case Notification_MerchantApp:
            case Notification_AllUser:
                break;
            case Notification_UpdateProfileFail:
                [self handleUpdateProfileFail];
                break;
                
            case Notification_RefundLixi:
            case Notification_RefundTransationWallet:
            case Notification_RefundTransationBank:
            case Notification_OfflineTransaction:
            case Notification_RevertMoney:
            case Notification_ZPBankTopup:
            case Notification_QRToPay:
            case Notification_Gateway:
            case Notification_Cashback:
            case Notification_OpenLixiSuccess:
            {
                [self.signalLoadData sendNext:nil];
            }
                break;
            case Notification_Gift:
            {
                [self.signalLoadData sendNext:nil];
                [self alertToUser:notify];
            }
                break;
            case Notification_RetryTransaction:
            {
                [self handleRetryTransaction:notify];
            }
                break;
            case Notification_Transfer:
            {
                if ([notify.destuserid isEqualToString:[NetworkManager sharedInstance].paymentUserId]) {
                    // user khác gửi tiền đến mình
                    [self.signalLoadData sendNext:nil];
                }
            }
                break;
            case Notification_UpdateProfileSuccess:
            case Notification_UpateProfileMessage: {
                [self updateProfileInfo];
            }
                break;
            case Notification_ResetPin: {
                [self handleResetPinMessage:notify];
            }
                break;
            case Notification_RemoveExpireCard:
                [self updateSavedCardList];
                break;
            case Notification_MerchantBill:
                [self handleMerchantNotification:notify];
                break;
            case Notification_RemoveBankAccount:
            case Notification_MapBankAccount:
                [self reloadListBankAccount];
                break;
//            case Notification_Cashback:
//                [self.signalLoadData sendNext:nil];
//                [self handleCashbackMessage:(ZPCashbackMessage *)notify];
//                break;
//            case Notification_Voucher:
//                [self handleVoucherMessage:(ZPVoucherMessage *)notify];
//                break;
            default:
                break;
        }
    }];
}

- (void)observerLixiMessage {
    __block ZPNotifyMessage *lixiMessage = nil;
    [[[[[ZPConnectionManager sharedInstance].messageSignal delay:0.2] filter:^BOOL(ZPNotifyMessage *value) {
        return [value isKindOfClass:[ZPNotifyMessage class]] && value.notificationType == Notification_Lixi;
    }] deliverOnMainThread] subscribeNext:^(ZPNotifyMessage *message) {
        ReactAppModel *model = [[ReactNativeAppManager sharedInstance] getApp:lixiAppId];
        if (model.appState == ReactNativeAppStateReadyToUse) {
            lixiMessage = nil;
            [self showLixiPopup:message];
            return;
        }
        lixiMessage = message;
    }];

    ReactAppModel *model = [[ReactNativeAppManager sharedInstance] getApp:lixiAppId];
    if (model.appState == ReactNativeAppStateReadyToUse) {
        return;
    }
    [[[[RACObserve(model, appState) filter:^BOOL(NSNumber *value) {
        return [value integerValue] == ReactNativeAppStateReadyToUse;
    }] take:1] deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        if (lixiMessage) {
            [self showLixiPopup:lixiMessage];
            lixiMessage  = nil;
        }
    }];
}

- (void)showLixiPopup:(ZPNotifyMessage *)notify {
    ZPReactNativeAppRouter *reactAppRouter = [[ZPReactNativeAppRouter alloc] init];
    UIViewController *controller = [[ZPAppFactory sharedInstance] rootNavigation].topViewController;
    if (![reactAppRouter canPresentLixiPopup:controller]) {
        return;
    }
    
    NSDictionary *data = [self.notifyTable getNotificationWithMtaid:notify.mtaid mtuid:notify.mtuid];
    if ([data intForKey:@"unread"] == NotificationStateRead) {
        return;
    }

    [self.notifyTable updateStateReadWithMtaid:notify.mtaid mtuid:notify.mtuid];
    [self updateTotalUnreadNotify];

    NSMutableDictionary *lixiDataFinal = [NSMutableDictionary new];
    [lixiDataFinal setObjectCheckNil:[NSNumber numberWithInteger:lixiAppId] forKey:@"appid"];
    [lixiDataFinal setObjectCheckNil:data forKey:@"data"];
    [lixiDataFinal setObjectCheckNil:ReactModule_RedPacket forKey:@"moduleName"];
    [lixiDataFinal setObjectCheckNil:@"present" forKey:@"launchmode"];
    [lixiDataFinal setObjectCheckNil:@"popupredpacket" forKey:@"view"];
    ReactAppModel *app = [[ReactNativeAppManager sharedInstance] getApp:lixiAppId];
    [reactAppRouter openApp:app from:controller moduleName:ReactModule_RedPacket withProps:lixiDataFinal];
}

- (void) registerNotificationVibrate {
    [[[[[ZPConnectionManager sharedInstance].messageSignal filter:^BOOL(id value) {
        return [value isKindOfClass:[ZPNotifyMessage class]];
    }] flattenMap:^__kindof RACSignal * _Nullable(ZPNotifyMessage * _Nullable value) {
        if ([value isKindOfClass:[ZPThankMessage class]]) {
            return [RACSignal return:@(YES)];
        }
        
//        NSArray *notifiesVibrate = [ApplicationState getArrayConfigFromDic:@"notification" andKey:@"vibrate" andDefault:[NSArray new]];
        NSArray *notifiesVibrate = [[ZPApplicationConfig getNotificationConfig] getVibrate];
        if (notifiesVibrate == nil || [notifiesVibrate count] == 0) {
            return [RACSignal return:@(NO)];
        }
        
        return [RACSignal return:@([notifiesVibrate containsObject:@(value.notificationType)])];
    }] filter:^BOOL(NSNumber *result) {
        if (![result isKindOfClass:[NSNumber class]]) {
            return NO;
        }
        return [result boolValue];
    }] subscribeNext:^(id x) {
         AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    }];
}


// thanh toán hoặc nạp tiền vào ví không show notify
// chuyển tiền mà người chuyển là mình không show notify.
// mặc định show notify

- (BOOL)shouldNotifyToUser:(ZPNotifyMessage *)notify {
    if (![notify isKindOfClass:[ZPNotifyMessage class]]) {
        return FALSE;
    }
    
    if (notify.notificationType == Notification_PayBill) {
        return FALSE;
    }
    if (notify.notificationType == Notification_Transfer) {
        NSString *userId = [NetworkManager sharedInstance].paymentUserId;
        if ([userId isEqualToString:notify.senderUserId]) {
            return FALSE;
        }
    }
    return TRUE;
}

#pragma mark - ResetPin

- (void)handleResetPinMessage:(ZPNotifyMessage *)msg {
    [self updateSavedCardList];
    [[ZPTouchIDService sharedInstance] removeZaloPayPassword];
    [ZPSettingsConfig sharedInstance].usingTouchId = false;
}
//
//#pragma mark - Voucher
//
//- (void)handleVoucherMessage:(ZPVoucherMessage *)msg {
//    // Valid
//    if (![msg isKindOfClass:[ZPVoucherMessage class]]) {
//        DDLogInfo(@"Not Correct Data");
//        return;
//    }
//    
//    //Check type
//    if (msg.data.type == 2)
//    {
//        ReactAppModel *model = [[ReactNativeAppManager sharedInstance] getApp:reactInternalAppId];
//        if (model.appState == ReactNativeAppStateReadyToUse) {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [self showVoucherPopup:msg];
//            });
//            return;
//        }
//        
//        [[[[RACObserve(model, appState) filter:^BOOL(NSNumber *value) {
//            return [value integerValue] == ReactNativeAppStateReadyToUse;
//        }] take:1] deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
//               [self showVoucherPopup:msg];
//        }];
//    }
//    
//}
//
//- (void)showVoucherPopup:(ZPVoucherMessage *)msg {
//    [ZPVoucherPopup showWithDataWithData:msg.data detailAction:^{
//        // Will show list Voucher
//        UIViewController *controller = [ZPVoucherHistoryRouter createZPVoucherHistoryModule];
//        [[UINavigationController currentActiveNavigationController] pushViewController:controller animated:TRUE];
//    }];
//}
//
//#pragma mark - Cashback
//
//
//- (void)handleCashbackMessage:(ZPCashbackMessage *)msg {
//    
//    ReactAppModel *model = [[ReactNativeAppManager sharedInstance] getApp:reactInternalAppId];
//    if (model.appState == ReactNativeAppStateReadyToUse) {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self showCashbackPopup:msg];
//        });
//        return;
//    }
//    
//    [[[[RACObserve(model, appState) filter:^BOOL(NSNumber *value) {
//        return [value integerValue] == ReactNativeAppStateReadyToUse;
//    }] take:1] deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
//        [self showCashbackPopup:msg];
//    }];
//}
//
//- (void)showCashbackPopup:(ZPCashbackMessage *)msg {
//    [ZPCashBackPopup showWithDataWithData:msg.data detailAction:^{
//        [self showTransactionDetailWithMessage:msg];
//    }];
//}
//#pragma mark - Reload List BankAccount

- (void)reloadListBankAccount {
    [[ZaloPayWalletSDKPayment sharedInstance] updateSavedAccountList:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:Reload_AccountList_Notification object:nil];    
    }];
}

#pragma mark - Support ZaloPayWalletSDK

- (void)forwardInAppNotificationToSDK:(ZPNotifyMessage *)notification {
    NSDictionary *data = @{@"type": @(notification.notificationType),
                           @"transid":[@(notification.transid) stringValue]};
    [[NSNotificationCenter  defaultCenter] postNotificationName:ZPInApp_Notification_Key object:data];
}

#pragma mark - Reload Notifcation List

- (void)reloadNotificationList {
    [[NSNotificationCenter defaultCenter] postNotificationName:ZPNotificationsAddedNotification object:nil userInfo:@{}];
}


#pragma mark - Update Profile Level3

- (void)handleUpdateProfileFail { // update level3 fail -> cho phép update level3.
    [self.cacheDataTable cacheDataRemoveDataForKey:kDisableUpdateProfileLevel3];
}

#pragma mark - Handle Gift, Revert Money Message

- (void)alertToUser:(ZPNotifyMessage *)message{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSString *title = [R string_Dialog_Title_Notification];
        if (message.notificationType == Notification_RevertMoney) {
            title = [R string_Dialog_Title_RevertMoney];
        }
        
        [ZPDialogView showDialogWithType:DialogTypeNotification
                                   title:title
                                 message:message.message
                            buttonTitles:@[[R string_ButtonLabel_Detail], [R string_ButtonLabel_Close]]
                                 handler:^(NSInteger index) {
                                     if (index == 0) {
                                         [self showTransactionDetailWithMessage:message];
                                     }
                                 }];
    });
}


- (void)showTransactionDetailWithMessage:(ZPNotifyMessage *)message {
    [[ZPAppFactory sharedInstance] showHUDAddedTo:nil];
    [[[[self ensureTransationWithId:message.transid] catch:^RACSignal * _Nonnull(NSError * _Nonnull error) {
        //Case : can't get info transaction => retry get api
        TransactionHistoryManager *manager = self.transactionHistoryManager;
        RACSignal * merge = [RACSignal merge:@[[manager reloadOldestTransaction:StatusTypeSuccess timeStamp:message.timestamp],
                                               [manager reloadOldestTransaction:StatusTypeFail timeStamp:message.timestamp]]];
        return merge;
    }] deliverOnMainThread] subscribeError:^(NSError * _Nullable error) {
        [self showTransactionDetailWithTranId:message.transid existInDb:NO];
        [[ZPAppFactory sharedInstance] hideAllHUDsForView:nil];
    } completed:^{
        [[ZPAppFactory sharedInstance] hideAllHUDsForView:nil];
        [self showTransactionDetailWithTranId:message.transid existInDb:YES];
    }];
}

- (void)showTransactionDetailWithTranId:(uint64_t)transid existInDb:(BOOL)exist {
    if (!exist) {
        UIViewController *controller = [UINavigationController currentActiveNavigationController].topViewController;
        [self.reactRouter showNotificationViewFrom:controller];
        return;
    }
    UITabBarController *tabbar = [self tabbarController];
    if (tabbar) {
        UIViewController *view = [self.reactRouter transactionHistoryWithTransId:transid];
        [tabbar presentViewController:view animated:YES completion:nil];
    }
}

- (RACSignal *)ensureTransationWithId:(uint64_t)tranid {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [self checkTransation:tranid subscriber:subscriber fromDate:[NSDate date] timout:5.0];
        return nil;
    }];
}

- (void)checkTransation:(uint64_t)tranId
             subscriber:(id<RACSubscriber>)subscriber
               fromDate:(NSDate *)requestTime
                 timout:(float)timout {
    NSArray *data = [self.transactionLogTable loadTransactionLogWithId:[@(tranId) stringValue]];
    DDLogInfo(@"transaction from db: %@",data);
    if (data.count > 0) {
        [subscriber sendCompleted];
        return;
    }
    if ([requestTime timeIntervalSinceNow] < -timout) {
        [subscriber sendError:nil];
        return;
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 1), ^{
        [self checkTransation:tranId subscriber:subscriber fromDate:requestTime timout:timout];
    });
}

#pragma mark - Handle Retry Transaction

- (void)handleRetryTransaction:(ZPNotifyMessage *)notify {
    [self.transactionLogTable updateRetrySuccessTransaction:notify.transid];
    [self.signalLoadData sendNext:nil];
}

#pragma mark - Signal Load data

- (void)handleReloadDataSignal {
    [[self.signalLoadData throttle:1.0] subscribeNext:^(id x) {
        [[ZPWalletManagerSwift sharedInstance] updateBalanceAndTransactionLogWithBalanceValue:0];
    }];
}

#pragma mark - Pay Bill

- (void)handleMerchantNotification:(ZPNotifyMessage *)notification {
    
    if ([[ZPAppFactory sharedInstance] isPayingBill]) {
        DDLogInfo(@"paying bill -> return");
        return;
    }
    
    NSString *notificationid = [self.notifyTable notificaionIdFromMtaid:notification.mtaid
                                                                                  mtuid:notification.mtuid];
    dispatch_main_async_safe((^{
        
        [ZPDialogView showDialogWithType:DialogTypeNotification
                                   title:[R string_PayMerchantBill_Title]
                                 message:notification.message
                            buttonTitles:@[[R string_PayMerchantBill_Pay_Button], [R string_ButtonLabel_Close]]
                                 handler:^(NSInteger index) {
                                     
                                     if (index == 0) {
                                         [self.notifyTable updateStateReadWithNotificationId:notificationid];
                                         [self payBillWithParams:notification.embeded
                                                  notificationId:notificationid];
                                     }
                                 }];
        
    }));
    
}

- (void)payMerchantBill:(NSString *)notificationId {
    NSDictionary *params = [self.notifyTable merchantBill:notificationId];
    DDLogInfo(@" pay bill param = %@",params);
    if (params == nil) {
        return;
    }
    dispatch_main_async_safe(^{
        [self.notifyTable updateStateReadWithNotificationId:notificationId];
        [self payBillWithParams:params notificationId:notificationId];
    });
}

- (void)payBillWithParams:(NSDictionary *)params
           notificationId:(NSString *)notificationId {
    self.payBillModel.orderSource = OrderSource_NotifyInApp;
    [self.payBillModel payBillWithParams:params complete:^(ZPSchemePayBillReponse * result) {
        if (result) {
            [self removeNotification:notificationId];
        }
    }];
}

- (void)removeNotification:(NSString *)notificationId {
    [self.notifyTable deleteNotificationWithId:notificationId];
    [self reloadNotificationList];
}

#pragma mark - Function

- (void)updateTotalUnreadNotify {
    int totalUnread = [self.notifyTable countUnreadNotify];
    [UIApplication sharedApplication].applicationIconBadgeNumber = totalUnread;
    [self.subjectTotalUnreadMessage sendNext:@(totalUnread)];
}

- (void)logout {
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (void)clearUnreadNotification {
    [self.notifyTable clearNotificationCount];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (void)updateProfileInfo {
    [[[ZPWalletManagerSwift sharedInstance] updateUserInfo] subscribeCompleted:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:Reload_Profile_Notification object:nil];
    }];
}

- (void)updateSavedCardList {
    [[ZaloPayWalletSDKPayment sharedInstance] updateSavedCardList:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:Reload_CardList_Notification object:nil];
    }];
}

- (UITabBarController *)tabbarController {
    NSArray *windows = [[UIApplication sharedApplication] windows];
    for (UIWindow *oneWindow in windows) {
        UITabBarController *tabbar = (UITabBarController *)oneWindow.rootViewController;
        if ([tabbar isKindOfClass:[UITabBarController class]]) {
            return tabbar;
        }
    }
    return nil;
}
@end
