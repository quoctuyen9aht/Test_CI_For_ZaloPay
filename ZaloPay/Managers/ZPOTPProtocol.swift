//
//  ZPOTPProtocol.swift
//  ZaloPay
//
//  Created by Dung Vu on 10/26/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
protocol ZPOTPProtocol: NSObjectProtocol {
    func didReceiveOptMessage(_ opt: String)
}
