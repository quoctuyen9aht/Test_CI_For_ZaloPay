//
//  ZPSettingsConfig.swift
//  ZaloPay
//
//  Created by Nguyen Minh Tri on 12/28/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayCommonSwift
@objcMembers
public class ZPSettingsConfig: NSObject, Codable {
    static private var currentUserId: String?
    static private var instance: ZPSettingsConfig?
    public class func sharedInstance() -> ZPSettingsConfig {
        #if DEBUG
            assert(instance != nil, "Need to load setting after login")
        #endif
        return instance ?? ZPSettingsConfig()
    }
    
    private override init() {
        super.init()
        
        guard let cacheDataTable = ZPCacheDataTable(db: PerUserDataBase.sharedInstance()) else {
            return
        }

        self.askForPassword = (cacheDataTable.cacheDataValue(forKey: kAskForPassword) ?? "").toBool(defaultValue: true)
        self.usingTouchId = (cacheDataTable.cacheDataValue(forKey: kUsingTouchID) ?? "").toBool(defaultValue: false)
        self.turnOffTouchIdManual = (cacheDataTable.cacheDataValue(forKey: kTurnOffTouchIdManual) ?? "").toBool(defaultValue: false)
        self.authenticationOpen = (cacheDataTable.cacheDataValue(forKey: kAuthenticationOpen) ?? "").toBool(defaultValue: false)
        self.autoLockTime = (cacheDataTable.cacheDataValue(forKey: kAutoLockTime) ?? "").toDouble(defaultValue: 0)
        self.addSplashScreen = (cacheDataTable.cacheDataValue(forKey: kAddSplashScreen) ?? "").toBool(defaultValue: true)
        self.paymentCodeShowSecurityIntro = (cacheDataTable.cacheDataValue(forKey: kPaymentCodeShowSecurityIntro) ?? "").toBool(defaultValue: true)
        self.paymentCodeChannelSelect =  (cacheDataTable.cacheDataValue(forKey: kPaymentCodeChannelSelect) ?? "").toInt32(defaultValue: -1)
    }
    
    var askForPassword: Bool = true {
        didSet {
            guard askForPassword != oldValue else {
                return
            }
            updateConfig()
        }
    }
    
    var addSplashScreen: Bool = true {
        didSet {
            guard addSplashScreen != oldValue else {
                return
            }
            updateConfig()
        }
    }
    
    var authenticationOpen: Bool = true {
        didSet {
            guard authenticationOpen != oldValue else {
                return
            }
            updateConfig()
        }
    }
    
    var autoLockTime: TimeInterval = 0 {
        didSet {
            guard autoLockTime != oldValue else {
                return
            }
            updateConfig()
        }
    }
    
    var usingTouchId: Bool = false {
        didSet {
            guard usingTouchId != oldValue else {
                return
            }
            updateConfig()
        }
    }
    
    var turnOffTouchIdManual: Bool = false {
        didSet {
            guard turnOffTouchIdManual != oldValue else {
                return
            }
            updateConfig()
        }
    }
    
    var paymentCodeShowSecurityIntro: Bool = true {
        didSet {
            guard paymentCodeShowSecurityIntro != oldValue else {
                return
            }
            updateConfig()
        }
    }
    
    var paymentCodeChannelSelect: Int32 = -1 {
        didSet {
            guard paymentCodeChannelSelect != oldValue else {
                return
            }
            updateConfig()
        }
    }
    private lazy var update: NSLock = NSLock()
    
    // Update config
    private func updateConfig() {
        update.lock()
        defer {
            update.unlock()
        }
        ZPSettingsConfig.saveSetting(self)
    }
    
    private class func saveSetting(_ setting: ZPSettingsConfig) {
        let uid = self.currentUserId ?? ""
        let key = settingKey(from: uid)
        let result = KeyChainStore.set(object: setting, forKey: key)
        #if DEBUG
            assert(result, "Save unsuccessful!!!")
        #endif
    }
 
    private class func settingKey(from userId: String) -> String {
        return "user_setting" + userId
    }
    
    // Chỉ xoá setting khi có user mới login vào && user mới != user cũ.
    private class func removeSetting(_ userId: String) {
        let key = ZPSettingsConfig.settingKey(from: userId)
        let result = KeyChainStore.remove(forKey: key, service: nil)
        #if DEBUG
            assert(result, "Can't remove unsuccessful!!!")
        #endif
    }
    
    private class func updateUserSetting(_ userId: String) {
        self.currentUserId = userId
        let key = ZPSettingsConfig.settingKey(from: userId)
        guard let setting = KeyChainStore.get(withType: ZPSettingsConfig.self, forKey: key) else {
            self.instance = ZPSettingsConfig()
            self.instance?.updateConfig()
            return
        }
        self.instance = setting
    }
    
    public class func loadUserSetting(_ userId: String?) {
        guard let newUserId = userId else {
            return
        }
        let oldUserId = self.currentUserId ?? ""
        if userId != oldUserId {
            removeSetting(oldUserId)
        }
        updateUserSetting(newUserId)
    }
}





