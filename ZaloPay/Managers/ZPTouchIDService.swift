//
//  ZPTouchIDService.swift
//  ZaloPay
//
//  Created by Nguyen Minh Tri on 12/28/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import CocoaLumberjackSwift
import LocalAuthentication
import ZaloPayCommonSwift

@objc public enum TouchIDResult : Int {
    case fail
    case success
    case userCancel
    case noTouchID
    case activated
}


@objcMembers
public class ZPTouchIDService: NSObject {
    
    static let sharedInstance = ZPTouchIDService()
    var isTouchIDActive: Bool = false
    weak var currentContext: LAContext?
    
    lazy var deviceModelsWithoutTouchID = [
        "iPhone1,1",     //iPhone
        "iPhone1,2",     //iPhone 3G
        "iPhone2,1",     //iPhone 3GS
        "iPhone3,1",     //iPhone 4
        "iPhone3,2", "iPhone3,3", "iPhone4,1",     //iPhone 4S
        "iPhone5,1",     //iPhone 5
        "iPhone5,2", "iPhone5,3",     //iPhone 5C
        "iPhone5,4", "iPod1,1",     //iPod
        "iPod2,1", "iPod3,1", "iPod4,1", "iPod5,1", "iPod7,1", "iPad1,1",     //iPad
        "iPad2,1",     //iPad 2
        "iPad2,2", "iPad2,3", "iPad2,4",     // iPad mini 1G
        "iPad2,5", "iPad2,5", "iPad2,7", "iPad3,1",     //iPad 3
        "iPad3,2", "iPad3,3", "iPad3,4",     //iPad 4
        "iPad3,5", "iPad3,6", "iPad4,1",     //iPad Air
        "iPad4,2", "iPad4,3", "iPad4,4",     //iPad mini 2
        "iPad4,5", "iPad4,6"     //                                           @"iPad4,7", // iPad mini 3
    ]
    
    
    lazy var identifier: String = UIDevice.current.modelName

    func forceClose() {
        if (currentContext != nil) {
            if #available(iOS 9.0, *) {
                self.currentContext?.invalidate()
            }
        }
    }

    func isTouchIDExist() -> Bool{
        return !deviceModelsWithoutTouchID.lazy.contains(identifier)
    }

    func hasTouchID() -> Bool {
        return self.isTouchIDExist();
    }

    func isEnableTouchID() -> Bool {
        if self.hasTouchID(){
            let context = LAContext()
            return context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
        }
        return false
    }

    func requestPassFromTouchIdWithCompleteHandle(_ handle: @escaping (_ password: String) -> Void, messsage message: String) {
        authenFromTouchIDWithCompleteHandle({(_ result: TouchIDResult) -> Void in
            var password: String = ""
            if result == TouchIDResult.success {
                password = self.currentUserPassword()
            }
            handle(password)
        }, messsage: message)
        
    }
    
    func authenFromTouchIDWithCompleteHandle(_ handle: @escaping (_ result: TouchIDResult) -> Void, messsage message: String){
        
        if(self.isTouchIDActive){
            handle(.activated)
            return;
        }
        self.isTouchIDActive = true;
        let myContext = LAContext()
        let authError: Error? = nil

        if !(myContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: authError as? AutoreleasingUnsafeMutablePointer<NSError?>)) {
            DDLogInfo("no touch id")
            DispatchQueue.main.async {
                handle(TouchIDResult.noTouchID)
                self.isTouchIDActive = false
            }
            return
        }
        myContext.localizedFallbackTitle = R.string_TouchID_Input_Password()
        
        if #available(iOS 10.0, *) {
            myContext.localizedCancelTitle = R.string_ButtonLabel_Close()
            myContext.maxBiometryFailures = 5
        }
        
        myContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: message) { (success, error) in
            DDLogInfo("touch id error = \(String(describing: error))")
            var result: TouchIDResult
            if success {
                result = TouchIDResult.success
            }
            else if Int32(error?.errorCode() ?? 0) == kLAErrorUserCancel {
                result = TouchIDResult.userCancel
            }
            else {
                result = TouchIDResult.fail
            }
            
            DispatchQueue.main.async {
                handle(result)
                self.isTouchIDActive = false
            }
            
        }
        self.currentContext = myContext
        
    }
    
    func hasPassword() -> Bool {
        return self.currentUserPassword().count > 0;
    }

    func currentUserPassword() -> String {
        let uid = NetworkManager.sharedInstance().paymentUserId ?? ""
        let shaUid: String = uid.sha256()
        return loadPasswordFromKeyChain(fromKeyChain: shaUid)
    }

    func loadPasswordFromKeyChain(fromKeyChain shaUid: String) -> String {
        return KeyChainStore.string(forKey: shaUid, service: nil, accessGroup: nil) ?? ""
    }

    func saveZaloPayPassword(_ password: String) {
        let uid: String = NetworkManager.sharedInstance().paymentUserId.sha256()
        if !password.isEmpty && !uid.isEmpty {
            KeyChainStore.set(string: password, forKey: uid, service: nil, accessGroup: nil)
        }
    }

    func removeZaloPayPassword() {
        let uid: String = NetworkManager.sharedInstance().paymentUserId ?? ""
        self.removePassword(uid)
    }

    func removePassword(_ uid: String) {
        if !uid.isEmpty {
            KeyChainStore.remove(forKey: uid.sha256(), service: nil, accessGroup: nil)
        }
    }
    
    func hasFaceID() -> Bool {
        if #available(iOS 11, *) {
            let authContext = LAContext()
            let _ = authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
//           return authContext.biometryType == LABiometryType.faceID
//          using raw value beacause of Xcode 9.1 using enum LABiometryType.typeFaceID
            var result: Bool = false
            do {
                try catchException {
                    result = authContext.biometryType.rawValue == 2
                }
            } catch {
                #if DEBUG
                    print(error.localizedDescription)
                #endif
            }
            return result
        }
        return false
    }
}

public extension UIDevice {
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        return identifier
    }
}

public extension ZPTouchIDService {
    public func checkAndReplaceFaceIdMessage(_ message: String) -> String {
        guard hasFaceID() else {
            return message
        }
        
        var str = message        
        str = str.replacingOccurrences(of: "vân tay", with: "Face ID")
        str = str.replacingOccurrences(of: "TouchID", with: "Face ID")
        return str
    }
}


