//
//  ZPPayAppToAppSchemeHandler.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 10/12/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import CocoaLumberjackSwift

enum ZPErrorCode : Int {
    case success            = 1
    case notInstall         = -1
    case invalidResponse    = -2
    case invalidOrder       = -3
    case userCancel         = -4
    case fail               = -5
}

let kZaloPayRedirectPath    = "zp-redirect-%d://result?code=%d&tranid=%@&transid=%@&zptranstoken=%@"
let kZaloPayWebRedirectUrl  = "%@?appid=%d&zptransid=%@&apptransid=%@"

class ZPEmptyViewController: BaseViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Đang tải"
        let imageBackground : UIImageView = UIImageView(image: UIImage(named: "loadingdata"))
        self.view.addSubview(imageBackground)
        let size : CGSize = imageBackground.frame.size
        imageBackground.snp.makeConstraints { (make) in
            make.centerX.equalTo(0)
            make.centerY.equalTo(0)
            make.size.equalTo(size)
        }
    }
}


class ZPPayAppToAppUrlHandler: UrlHandler, ZPURLTrackOpenAppProtocol {
    lazy var payBillModel: ZPPayBillModel = {
        let model = ZPPayBillModel()
        model.shouldSkipSuccessView = true
        return model
    }()
    static let notification = Notification.Name(rawValue: "ZPPayAppToApNotification")
    func canHandle(scheme: ZPScheme, authenticated: Bool) -> Bool {
        return authenticated && scheme.domain.hasPrefix(kSchemePayment + "://pay")
    }    
    
    func handleUrl(scheme: ZPScheme) {
        if ZPAppFactory.sharedInstance().isPayingBill() {
                ZPDialogView.showDialog(with: DialogTypeNotification,
                                        message: R.string_AppToApp_Popup_When_IsBilling(),
                                        cancelButtonTitle: R.string_ButtonLabel_Close(),
                                        otherButtonTitle: nil,
                                        completeHandle: nil)
            return
        }
        NotificationCenter.default.post(name: .excutePaymentAppURL, object: nil)
        self.payBillModel.orderSource = Int(OrderSource_AppToApp.rawValue)
        self.payBillModel.payBill(withParams: (scheme.params), complete: { (result) in
            self.handlePayAppToApp(scheme: scheme, result: result)
        })
    }
    
    
    func handlePayAppToApp(scheme: ZPScheme, result: ZPSchemePayBillReponse) {
        var code : Int = 1
        let zptransid : String = result.data.string(forKey: "zptransid")
        if result.orderResult == ZPOrderResult.cancel {
            code = ZPErrorCode.userCancel.rawValue
        } else if result.orderResult == ZPOrderResult.fail {
            code = ZPErrorCode.fail.rawValue
        } else if result.orderResult == ZPOrderResult.success {
            code = ZPErrorCode.success.rawValue
        }
        
        let source = scheme.params.string(forKey: "source").lowercased()
        if source != "web" {
            let zptranstoken = scheme.params.string(forKey: "zptranstoken")
            let redictPath = String(format: kZaloPayRedirectPath, Int(result.appId), code, zptransid, zptransid, zptranstoken)
            UIApplication.shared.openUrlString(redictPath)
            return
        }
        
        ZaloPayWalletSDKPayment.sharedInstance().requetAppInfo(result.appId, transtype: ZPTransType.billPay).subscribeNext({ [weak self] (app) in
            let appId : Int = result.appId
            let app = app as? ZPAppData
            let url : String = app?.redirect_url ?? ""
            let apptransid = result.bill.appTransID ?? ""
            let browser = scheme.params.string(forKey: "browser").lowercased()
            self?.open(urlString: url, browser: browser, appId: appId, apptransId: apptransid, zptransid: zptransid)
            }, error:({(error) in
            }))
    }
    
    func open(urlString url: String, browser: String, appId: Int, apptransId: String, zptransid: String) {
        DDLogInfo("url from appinfo = \(url)")
        var redirectUrl = String(format: kZaloPayWebRedirectUrl, url, Int(appId), zptransid, apptransId)
        DDLogInfo("redireactUrl = \(url)")
        if browser == "chrome" {
            redirectUrl = self.chrome(url: &redirectUrl)
        } else if browser == "firefox" {
            redirectUrl = self.fireFox(url: redirectUrl)
        }
        DDLogInfo("launchUrl = \(redirectUrl)")
        UIApplication.shared.openUrlString(redirectUrl)
    }
    
    func chrome( url: inout String) -> String {
        if url.hasPrefix("https") {
            url = url.replacingOccurrences(of: "https", with: "googlechromes")
        } else if url.hasPrefix("http") {
            url = url.replacingOccurrences(of: "http", with: "googlechrome")
        }
        return url
    }
    
    func fireFox(url: String) -> String {
        return "\("firefox://open-url?url=")\(url)"
    }
}

