//
//  ZMEventManager.m
//
//  Created by Bon on 10/2/15.
//  Copyright (c) 2015 Bon. All rights reserved.
//

#import "ZMEventManager.h"
#import <ReactiveObjC/ReactiveObjC.h>

@interface ZMEventManager ()
@property (nonatomic, strong) RACSubject *internalSignal;
@end

@interface ZMEventWrapper: NSObject
@property (nonatomic, weak, nullable) id object;
@property (nonatomic, copy, nullable) ZMEventHandler block;
- (instancetype _Nonnull)initWith:(id _Nullable)object using:(ZMEventHandler _Nullable)block;
- (void)excuteWith:(ZMEvent *_Nullable)e;
- (RACSignal *_Nonnull)eDealloc;

@end

@implementation ZMEventManager

+ (ZMEventManager *) sharedInstance {
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

- (id)init {
    self = [super init];
    if (self) {
        self.internalSignal = [RACSubject subject];
    }
    return self;
}

+ (void)registerHandler:(id)handler eventType:(uint32_t)eventType withBlock:(ZMEventHandler)blockHandle {
    __block ZMEventWrapper *wrapper = [[ZMEventWrapper alloc] initWith:handler using:blockHandle];
    [[[[[ZMEventManager sharedInstance].internalSignal takeUntil:[wrapper eDealloc]] deliverOnMainThread]
      filter:^BOOL(ZMEvent *event) {
        return event.eventType == eventType;
      }] subscribeNext:^(ZMEvent  *_Nullable event) {
          [wrapper excuteWith:event];
      }];
}

+ (void)bindEvent:(uint32_t)eventype
             data:(id)data {
    [[RACScheduler scheduler] schedule:^{
        ZMEvent *event = [ZMEvent eventWithType:eventype data:data];
        [[ZMEventManager sharedInstance].internalSignal sendNext:event];
    }];
}


@end

@implementation ZMEvent

+ (instancetype)eventWithType:(uint32_t)eventType data:(id)eventData {
    return [[self alloc] initWithType:eventType data:eventData];
}

- (instancetype)initWithType:(uint32_t)type data:(id)data {
    self = [super init];
    if (self) {
        _eventType = type;
        _eventData  = data;
    }
    return self;
}
@end

@implementation ZMEventWrapper
- (instancetype)initWith:(id)object using:(ZMEventHandler)block {
    self = [super init];
    self.object = object;
    self.block = block;
    return self;
}

- (void)excuteWith:(ZMEvent *)e {
    if (!self.object || !self.block) {
        return;
    }
    self.block(e);
}

- (RACSignal *)eDealloc {
    return self.object ? [self.object rac_willDeallocSignal] : [RACSignal empty];
}
@end
