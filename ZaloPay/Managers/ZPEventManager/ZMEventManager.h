//
//  ZMEventManager.h
//
//  Created by Bon on 10/2/15.
//  Copyright (c) 2015 Bon. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum{
    EventTypeUpdateBalance,
} EventType;

@class ZMEvent;
typedef  void (^ZMEventHandler)(ZMEvent *event);
@interface ZMEvent : NSObject
@property (nonatomic, readonly) uint32_t eventType;
@property (nonatomic, readonly) id eventData;
+ (instancetype)eventWithType:(uint32_t)eventType data:(id)eventData;
@end

@interface ZMEventManager : NSObject

+ (void)registerHandler:(id)handler
              eventType:(uint32_t)eventType
              withBlock:(ZMEventHandler)blockHandle;

+ (void)bindEvent:(uint32_t)eventype
             data:(id)data;
@end
