//
//  ZPClearingUrlHandler.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 11/5/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import UIKit

class ZPClearingUrlHandler: UrlHandler {
    
    func canHandle(scheme: ZPScheme, authenticated: Bool) -> Bool {
        return scheme.domain.hasPrefix("zalopay://clearing")     
    }
    
    func handleUrl(scheme: ZPScheme) {
        if let navi = UIStoryboard.init(name: "ClearingAccount", bundle: nil).instantiateInitialViewController() as? ZPNavigationCustomStatusViewController{
            if let clearingInputPhoneViewController = navi.viewControllers.first as? ClearingInputPhoneViewController {
                let phone = scheme.params.string(forKey: "p")
                clearingInputPhoneViewController.phoneNumber = phone
                LoginManagerSwift.sharedInstance.clearLoginData()
                UIApplication.shared.delegate?.window??.rootViewController = navi
            }
            
        }
    }
}
