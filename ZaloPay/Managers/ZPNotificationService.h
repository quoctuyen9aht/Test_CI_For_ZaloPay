//
//  ZPNotificationService.h
//  ZaloPay
//
//  Created by bonnpv on 7/13/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ZPNotifyMessage;

@interface ZPNotificationService : NSObject
+ (instancetype)sharedInstance;

- (RACSignal*)signalTotalUnreadMessage;
- (void)logout;
- (void)clearUnreadNotification;
- (void)registerMessageSignal;
- (void)registerNotificationVibrate;
- (void)updateTotalUnreadNotify;
- (void)checkToLoadRecoveryMessage;
- (void)payMerchantBill:(NSString *)notificationId;
- (void)showTransactionDetailWithMessage:(ZPNotifyMessage *)message;
@end
