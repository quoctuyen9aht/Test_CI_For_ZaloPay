//
//  NSNotificationCenter+ZaloPay.m
//  ZaloPay
//
//  Created by bonnpv on 7/7/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "NSNotificationCenter+ZaloPay.h"
#import <ZaloPayConfig/ZaloPayConfig-Swift.h>
#import ZALOPAY_MODULE_SWIFT
//#import "ApplicationState.h"

@implementation NSNotificationCenter (ZaloPay)

- (RACSignal *)signalAppEnterForceground {
    return [[self rac_addObserverForName:UIApplicationWillEnterForegroundNotification object:nil] filter:^BOOL(id value) {
        return [ApplicationState sharedInstance].state == ZPApplicationStateAuthenticated;
    }];
}

- (RACSignal *)signalAppEnterBackground {
    return [[self rac_addObserverForName:UIApplicationDidEnterBackgroundNotification object:nil] filter:^BOOL(id value) {
        return [ApplicationState sharedInstance].state == ZPApplicationStateAuthenticated;
    }];
}

- (RACSignal *)enterBackground {
    return [self rac_addObserverForName:UIApplicationWillEnterForegroundNotification object:nil];
}
@end
