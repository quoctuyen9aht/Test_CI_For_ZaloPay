//
//  ZPOpenInternalAppSchemeHandler.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 10/12/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayCommonSwift
import ZaloPayWeb

enum ZPOpenInternalRouter {
    case none
    case openByController(UIViewController?)
    case openByTag(Int)
}

class ZPOpenInternalAppUrlHandler: UrlHandler {
    
    let reactAppRouter = ZPReactNativeAppRouter()
    let kTagNotification = 100
    let kTagLinkCard = 102
    
    func canHandle(scheme: ZPScheme, authenticated: Bool) -> Bool {
        return authenticated && scheme.domain.hasPrefix("zalopay://launch/f/")
    }
    
    private static let FunctionNameMapping: [String: RouterId] = [
        "userprofile": .userProfile,
        "withdraw":    .withdraw,
        "qrcode":      .qrScan,
        "receive":     .receiveMoney,
        "transfer":    .transferMoney,
        "linkcard":    .linkCard,
        "wallettopup": .recharge,
        "listvoucher": .voucher,
    ]

    func handleUrl(scheme: ZPScheme) {
        let functionName = scheme.uriPath.components(separatedBy: "/").last ?? ""
        if let routerId = ZPOpenInternalAppUrlHandler.FunctionNameMapping[functionName] {
            ZPCenterRouter.launch(routerId: routerId, from: nil)
            return
        }
        let router = routerFromFunction(scheme:scheme,functionName: functionName)
        let navi = UINavigationController.currentActiveNavigationController()
        switch router {
        case .openByController(let vc):
            guard let controller = vc else {
                return
            }
            navi?.pushViewController(controller, animated: true)
        case .openByTag(let tag):
            openViewControllerOther(tag: tag, vc: navi)
        case .none:
            #if DEBUG
                assert(false, "Please check this case")
            #endif
        }
    }
    
    private func getDicHandler(scheme: ZPScheme) -> [String : ZPOpenInternalRouter] {
        let dicHandler:[String : ZPOpenInternalRouter] = [
            "linkcard"      : .openByTag(kTagLinkCard),
            "notification"  : .openByTag(kTagNotification),
            "web"           : .openByController(getVCWeb(scheme: scheme))
        ]
        return dicHandler
    }
    
    private func routerFromFunction(scheme: ZPScheme, functionName: String) -> ZPOpenInternalRouter {
        let dict = getDicHandler(scheme: scheme)
        let result = dict[functionName] ?? .none
        return result
    }
    
    private func openViewControllerOther(tag:Int,vc : UINavigationController?){
        guard let controllers = vc?.viewControllers, controllers.count > 0 else {
            return
        }
        guard let rootVC = controllers.last else {
            return
        }
        
        if tag == kTagNotification {
            self.reactAppRouter.showNotificationView(from: rootVC)
            ZPNotificationService.sharedInstance().clearUnreadNotification()
        } else if tag == kTagLinkCard {
            ZPBankApiWrapperSwift.sharedInstance.mapBank(with: rootVC,
                                                      appId: kZaloPayClientAppId,
                                                      transType: .atmMapCard).subscribeNext({ [weak rootVC] (data) in
                rootVC?.popMe(animated: false)
            }) { [weak rootVC] (error) in
                rootVC?.popMe(animated: false)
            }
        }
    }
    
    private func getVCWeb(scheme: ZPScheme) -> ZPWWebViewController?{
        let value : Dictionary<String,Any> = scheme.params
        let url = value.string(forKey: "url", defaultValue: "")
        return url.count > 0 ? ZPWWebViewController(url: url, webHandle: ZPWebHandle()) : nil
    }
}

