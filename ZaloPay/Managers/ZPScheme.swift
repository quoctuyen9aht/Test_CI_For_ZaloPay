//
//  ZPScheme.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 10/6/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation

let ktimeout: TimeInterval  = 120 // 2 phút

class ZPScheme : NSObject {
    
    var urlScheme : URL
    var sourceApplication : String
    lazy var params : JSON = {
        return self.params(fromUrl: self.urlScheme)
    }()
    lazy var uriPath : String = {
        return self.uriPath(fromUrl: self.urlScheme)
    }()
    lazy var domain : String = {
        return self.getDomain(fromUrl: self.urlScheme)
        
    }()
    private lazy var components = URLComponents(url: self.urlScheme, resolvingAgainstBaseURL: false)
    var receiveTime : Date
    
    init(schemeWith urlScheme: URL, sourceApplication: String) {
        self.urlScheme = urlScheme
        self.receiveTime = Date()
        self.sourceApplication = sourceApplication
        super.init()
    }
    
    func uriPath(fromUrl urlScheme: URL) -> String {
        let host = components?.host ?? ""
        let path = components?.path ?? ""
        return host + path
    }
    
    func getDomain(fromUrl urlScheme: URL) -> String {
        let relativePath = urlScheme.relativeString
        return relativePath.components(separatedBy: "?").first ?? ""
    }
    
    func params(fromUrl schemesUrl: URL) -> Dictionary<String,Any> {
        var _return = Dictionary<String,Any>()
        let query = urlScheme.relativeString.components(separatedBy: "?").last ?? ""
        let queryComponents = query.components(separatedBy: "&")
        
        for oneParam: String in queryComponents {
            let paramsArray : Array = oneParam.components(separatedBy: "=")
            if paramsArray.count != 2 {
                continue
            }
            let name = paramsArray[0].lowercased()
            let value = paramsArray[1]
            if name.count > 0 && value.count > 0 {
                _return[name] = value
            }
        }
        self.decodeURL(fromParam: &_return, withKey: "description")
        return _return
    }
    
    func decodeURL(fromParam params: inout JSON, withKey key: String) {
        var value = params.value(forKey: key, defaultValue: "")
        if value.count > 0 {
            value = self.urlDecode(string: value)
            params[key] = value
        }
    }
    
    func urlDecode(string: String) -> String {
        return string.removingPercentEncoding?.replacingOccurrences(of: "+", with: " ") ?? ""
    }
    
    func isTimeOut() -> Bool {
        return self.receiveTime.timeIntervalSinceNow < -ktimeout
    }
}

