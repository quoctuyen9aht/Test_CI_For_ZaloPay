//
//  ZPReactNativeAppRouter.swift
//  ZaloPay
//
//  Created by CPU11680 on 2/28/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import CocoaLumberjackSwift
import ZaloPayProfile
import ZaloPayWeb

@objcMembers
public class ZPReactNativeAppRouter: NSObject {

    var controller: UIViewController?
    
    func showAccountClearing(from controller: UIViewController) {
        let viewController: UIViewController = clearingAccount()
        let navClearingAccount = UINavigationController(rootViewController: viewController)
        ZPUIAppDelegate.sharedInstance.window?.rootViewController = navClearingAccount
    }

    func clearingAccount() -> UIViewController {
        let properties = NSMutableDictionary()
        properties["remain_time"] = ZPProfileManager.shareInstance.userLoginData?.hourSessionTime
        return internalModuleView("AccountClearing", properties: properties)
    }

    func showTransactionHistory(_ transId: UInt64, from controller: UIViewController) {
        let viewController: UIViewController = transactionHistory(withTransId: transId)
        controller.navigationController?.pushViewController(viewController, animated: true)
    }

    func transactionHistory(withTransId transId: UInt64) -> UIViewController {
        let properties = NSMutableDictionary()
        let zaloPayId: String = NetworkManager.sharedInstance().paymentUserId
        properties["zalopay_userid"] = zaloPayId
        properties["view"] = "history"
        properties["transid"] = transId
        return internalModuleView(ReactModule_TransactionLogs, properties: properties)
    }

    func listTransactionHistoryViewController() -> UIViewController? {
        guard let properties = ReactNativeAppManager.sharedInstance().reactProperties(reactInternalAppId) else{
            return nil
        }
        return internalModuleView(ReactModule_TransactionLogs, properties: properties)
    }

    func showNotificationView(from controller: UIViewController) {
        ZPTrackingHelper.shared().trackEvent(.notification_launched)
        if let viewController: UIViewController = notificationViewController(){
            controller.navigationController?.pushViewController(viewController, animated: true)
        }
    }

    func notificationViewController() -> UIViewController? {
        guard let properties = ReactNativeAppManager.sharedInstance().reactProperties(Int(reactInternalAppId)) else{
            return nil
        }
        let viewController: UIViewController = internalModuleView("Notifications", properties: properties)
        return viewController
    }

    func supportCenterViewController() -> UIViewController? {
        guard let properties = ReactNativeAppManager.sharedInstance().reactProperties(reactInternalAppId) else{
            return nil
        }
        let viewController: UIViewController = internalModuleView("SupportCenter", properties: properties)
        return viewController
    }

    func internalModuleView(_ moduleName: String, properties: NSDictionary) -> UIViewController {
        let viewController: ZPDownloadingViewController = ZPDownloadingViewController(properties: properties as? [AnyHashable : Any])
        viewController.moduleName = moduleName
        return viewController
    }

    func openExternalApp(_ appId: NSInteger, from controller: UIViewController) {
        guard let activesApp: NSArray = ReactNativeAppManager.sharedInstance().activeAppIds as NSArray?  else{
            return
        }
        if !activesApp.contains(appId) {
            DDLogInfo("error: open app with id = \(appId)")
            DDLogInfo("all active app = \(activesApp)")
            return
        }
        if appId == withdrawAppId {
            ZPCenterRouter.launch(routerId: .withdraw, from: controller)
            return
        }

        
        let app = ReactNativeAppManager.sharedInstance().getApp(appId)
        if app.appType == ReactAppTypeWeb {
            ZPWebAppRouter.openWeb(withAppId: appId, viewController: controller)
            return
        }
        let moduleName: String = ReactNativeAppManager.sharedInstance().defaultModuleName(appId)
        guard let properties = ReactNativeAppManager.sharedInstance().reactProperties(Int(appId)) else{
            return
        }
        openApp(app, from: controller, moduleName: moduleName, withProps: properties)
    }

    @discardableResult
    func openApp(_ appModel: ReactAppModel, from controller: UIViewController, moduleName: String, withProps props: NSDictionary) -> UIViewController? {
        self.controller = controller
        return launchApp(appModel, moduleName: moduleName, withProps: props)
    }

    func launchApp(_ appModel: ReactAppModel, moduleName: String, withProps props: NSDictionary) -> UIViewController? {

        ReactNativeAppManager.sharedInstance().runingAppId = appModel.appid
        let rnViewController: ZPDownloadingViewController = ZPDownloadingViewController(appId: appModel.appid, properties: props as? [AnyHashable : Any])
        rnViewController.enableSwipeBack = enableSwipeBack(appModel.appid)
        rnViewController.moduleName = moduleName
        let launchMode = props.string(forKey: "launchmode", defaultValue: "")
    
        if let moduleId = props.object(forKey: "moduleId") as? Int, launchMode == "replace"{
            // case app reactnative gọi mở một app reactnative khác -> clear app trước đó rồi push một viewcontroller mới vào.
            ReactFactory.sharedInstance().replaceModuleInstance(moduleId, with: rnViewController, from: self.controller)
            return rnViewController
        }

        if (launchMode == "present") {
            // case popup lixi: chỉ hiển thị ở trang home hoặc trang notification.
            if canPresentLixiPopup(self.controller) {
                guard let rootViewController: UIViewController = UIApplication.shared.keyWindow?.rootViewController else{
                    return nil
                }
                rnViewController.view.backgroundColor = UIColor.clear
                let navi = UINavigationController(rootViewController: rnViewController)
                navi.view.backgroundColor = UIColor.clear
                navi.modalPresentationStyle = .overCurrentContext
                navi.modalTransitionStyle = .crossDissolve
                navi.defaultNavigationBarStyle()
                rootViewController.present(navi, animated: true, completion: nil)
                return rnViewController
            }
            return nil
        }
        self.controller?.navigationController?.pushViewController(rnViewController, animated: true)
        return rnViewController
    }

    func enableSwipeBack(_ appId: Int) -> Bool {
        return appId != lixiAppId
    }

    func alertDownloading(_ appModel: ReactAppModel) {
        ZPDialogView.showDialog(with: DialogTypeNotification, message: R.string_HomeReactNative_WaitForDownloading(), cancelButtonTitle: R.string_ButtonLabel_Close(), otherButtonTitle: nil, completeHandle: nil)
    }

    func alertRetry(_ appModel: ReactAppModel) {
        ZPDialogView.showDialog(with: DialogTypeNotification, message: R.string_HomeReactNative_DownloadAgainMessage(), cancelButtonTitle: R.string_ButtonLabel_Close(), otherButtonTitle: [R.string_HomeReactNative_DownloadAgain()]) { [weak self] (buttonIndex, cancelButtonIndex) in
                if buttonIndex == cancelButtonIndex {
                    return
                }
                self?.downloadApp(appModel)
        }

    }

    func alertDownloading(with message: String) {
        ZPDialogView.showDialog(with: DialogTypeNotification, message: message, cancelButtonTitle: R.string_ButtonLabel_Close(), otherButtonTitle: nil, completeHandle: nil)
    }

    func downloadApp(_ app: ReactAppModel) {
        ZPAppFactory.sharedInstance().showHUDAdded(to: self.controller?.view)
        ReactNativeAppManager.sharedInstance().downloadApp(app, progress: nil).deliverOnMainThread().subscribeError({ [weak self] (error) in
            ZPAppFactory.sharedInstance().hideAllHUDs(for: self?.controller?.view)
            self?.alertDownloading(with: R.string_HomeReactNative_DownloadError())
        }) {
            ZPAppFactory.sharedInstance().hideAllHUDs(for: self.controller?.view)
            self.alertDownloading(with: R.string_HomeReactNative_DownloadSuccess())
        }
        
    }

    func canPresentLixiPopup(_ controller: UIViewController?) -> Bool {
        let rootViewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController
        if rootViewController?.presentedViewController != nil {
            return false
        }
        if controller is ZPHomeViewController {
            return true
        }
        let react = controller as? ReactModuleViewController
        if (react != nil && react?.moduleName == "Notifications") {
            return true
        }
        return false
    }

    func openLixiFromZalo(with user: ZaloUserSwift) -> UIViewController? {
        guard let properties = ReactNativeAppManager.sharedInstance().reactProperties(lixiAppId) else{
            return nil
        }
        let allProperties = properties
        allProperties["receiver"] = user.zaloPayId
        allProperties["receiver_name"] = user.displayName
        allProperties["receiver_avatar"] = user.avatar
        allProperties["receiver_zaloid"] = user.userId
        allProperties["phone"] = user.phone
        allProperties["view"] = "preset_zalo"
        DDLogInfo(String(describing: allProperties))
        guard let controller: UIViewController = UINavigationController.currentActiveNavigationController()?.topViewController else{
            return nil
        }
        let model = ReactNativeAppManager.sharedInstance().getApp(lixiAppId)
        return openApp(model, from: controller, moduleName: ReactModule_RedPacket, withProps: allProperties)
    }
    
    @objc public class func getInternalWebApp(url: String) -> UIViewController {
        let controller = ZPWInternalWebApp(url: url, webHandle: ZPWebHandle())
        controller.setUpWebView(false, showShareBtn: true)
        return controller
    }
    @objc public class func getUserInfosWith(appId: Int) -> NSDictionary {
        return ZPWWebAppFunctionWrapper.userInfos(appId: appId, webHandle: ZPWebHandle()) as NSDictionary
    }
}



