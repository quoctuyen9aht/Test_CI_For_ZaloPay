//
//  ZPShowTransactionHistorySchemeHandler.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 10/12/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayCommonSwift

class ZPShowTransactionHistoryUrlHandler: UrlHandler {
    lazy var reactAppRouter: ZPReactNativeAppRouter = ZPReactNativeAppRouter()
    lazy var transactionHistoryManager = TransactionHistoryManager()
    
    func canHandle(scheme: ZPScheme, authenticated: Bool) -> Bool {
        return authenticated && scheme.domain.hasPrefix("zalopay://zalopay.vn/view/transid/")
    }
    
    func handleUrl(scheme: ZPScheme) {
        let strTransId = scheme.uriPath.components(separatedBy: "/").last ?? ""
        let transid = UInt64(strTransId) ?? 0
        let value : Dictionary<String,Any> = scheme.params
        let reqTime = value.uInt64(forKey: "reqtime")
        if transid == 0 {
            self.alertTransactionNotFound()
            return
        }
        self.transactionHistoryManager.requestTransaction(transid, time: reqTime).deliverOnMainThread().subscribeError({ [weak self](error) in
            self?.alertTransactionNotFound()
        }, completed: { [weak self] in
            self?.startShowTransactionHistory(transid: transid, requestTime: reqTime)
        })
    }
    
    func startShowTransactionHistory(transid: UInt64, requestTime reqTime: UInt64) {
        guard let navi = UINavigationController.currentActiveNavigationController()  else {
            return
        }        
        if let top = navi.topViewController {
            self.reactAppRouter.showTransactionHistory(transid, from: top)
        }
    }
    
    func alertTransactionNotFound() {
        ZPDialogView.showDialog(with: DialogTypeNotification,
                                message: "Không tìm thấy giao dịch",
                                cancelButtonTitle: R.string_ButtonLabel_Close(),
                                otherButtonTitle: nil,
                                completeHandle: nil)
        
    }
}

