//
//  ZPBackSchemeHandler.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 10/12/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayCommonSwift
import ZaloPayWeb

class ZPBackUrlHandler: UrlHandler {
    func canHandle(scheme: ZPScheme, authenticated: Bool) -> Bool {
        return authenticated && scheme.domain.hasPrefix("zalopay-1://backtoapp")
    }
    // xử lý click back trong app dịch vụ (app dịch vụ dùng ZPWInternalWebController)
    func handleUrl(scheme: ZPScheme) {
        let navi = UINavigationController.currentActiveNavigationController()
        if (navi?.viewControllers.last as? ZPWWeDichVu) != nil {
            navi?.popViewController(animated: true)
        }
    }
}
