//
//  ZPWebHandle.swift
//  ZaloPay
//
//  Created by phuongnl on 6/13/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayCommonSwift
import ZaloPayWeb
import ZaloPayConfig

public class ZPWebHandle : ZPWebHelperProtocol  {
    private var model: ZPTransferModel?
    static let payBillModel = ZPPayBillModel()
    
    public func getMuid(appId: Int) -> String? {
        let merchantTable = ZPMerchantTable(db: PerUserDataBase.sharedInstance())
        let merchantInfo = merchantTable?.getMerchantUserInfo(appId)
        return merchantInfo?.muid
    }

    public func patternURLs() -> [String]? {
        return ZPApplicationConfig.getWebAppConfig()?.getAllowUrls()
    }

    public func historyUrl(withAppId: Int) -> String {
        return ZPWebAppRouter.historyUrl(withAppId: withAppId)
    }
    
    public func webUrl(withAppId: Int) -> String {
        return ZPWebAppRouter.webUrl(withAppId: withAppId)
    }
    
    public func handleUrlSchemes(schemesUrl: URL, sourceApplication: String) -> Bool {
        return ZPSchemeUrlHandle.sharedInstance.handleUrlSchemes(schemesUrl: schemesUrl, sourceApplication: sourceApplication)
    }
    
    public func userInfos(appId: Int) -> [String] {
        return appConfigs(appId: appId)?.getUserInfo() ?? []
    }
    
    public func merchantName(appId: Int) -> String {
        return appConfigs(appId: appId)?.getMerchantName() ?? ""
    }
    
    private func appConfigs(appId: Int) -> ZPExternalAppDisclaimerConfigProtocol? {
        let externalApps = ZPApplicationConfig.getDisclaimerConfig()?.getExternalApps()
        let appConfig = externalApps?.first(where: { $0.getAppId() == appId })
        return appConfig
    }
        
    public func getPayBillReturnCode(params: Dictionary<String,Any>?, completion: @escaping (_ returnCode: Int)->()) {
        ZPWebHandle.payBillModel.payBill(withParams: params, complete:{ (result: ZPSchemePayBillReponse) in
            completion(result.orderResult.rawValue)
        })
    }
    
    public func launchExternalApp(appId: Int, appType: Int, from: UIViewController?, moduleName: String?) {
        let appType = ReactAppType(rawValue: UInt32(appType))
        ZPCenterRouter.launchExternalApp(appId: appId, appType: appType, from: from, moduleName: moduleName)
    }
    
    public func transferModel(params: Dictionary<String,Any>, completion: @escaping (_ returnCode: Int)->()) {
        model = ZPTransferModel(params: params)
        model?.transferModel { (result: ZPTransferResult) in
            completion(result.rawValue)
        }
    }
    
    public func launchApp(_ appId:Int, from: UIViewController?) {
        if appId == lixiAppId {
            self.launchExternalApp(appId: lixiAppId, appType: AppType.native.rawValue, from: from, moduleName: ReactModule_RedPacket)
            return
        }
        
        let app = ReactNativeAppManager.sharedInstance().getApp(appId)
        if (app.appType == ReactAppTypeWeb) {
            self.launchExternalApp(appId: appId, appType: AppType.webview.rawValue, from: from, moduleName: nil)
            return
        }
        
        self.launchExternalApp(appId: appId, appType: AppType.native.rawValue, from: from, moduleName: "PaymentMain")
    }
    
}
