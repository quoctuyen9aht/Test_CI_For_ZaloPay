//
//  120Handler.swift
//  ZaloPay
//
//  Created by tridm2 on 5/7/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import CocoaLumberjackSwift
import ZaloPayCommonSwift
import ZaloPayWeb

fileprivate let kNotiOpenWeb = "openweb"
fileprivate let kNotiOpenApp = "openapp"
fileprivate let kNotiOpenVoucherList = "openvoucherlist"
fileprivate let kNotiOpenUserProfile = "openprofile"

class ZPNotiApple120Handler : ZPNotificationAppleHandler {
    private lazy var actions : [String : ZPNotificationAppleAction] = [
        kNotiOpenWeb : ZPNotiAppleOpenWebAction(),
        kNotiOpenApp : ZPNotiAppleOpenAppAction(),
        kNotiOpenVoucherList : ZPNotiAppleOpenVoucherListAction(),
        kNotiOpenUserProfile : ZPNotiAppleOpenUserProfileAction(),
    ]
    
    func process(model: ZPAppleNotificationModel) {
        let action = model.action
        guard let handler = actions[action] else {
            return
        }
        handler.handleOpenWith(model: model)
    }
}


fileprivate class ZPNotiAppleOpenWebAction: ZPNotificationAppleAction {
    
    func handleOpenWith(model: ZPAppleNotificationModel) {
        self.handleOpenWebView(model.actionData)
    }
    
    private func handleOpenWebView(_ url: String) {
        guard let navi = UINavigationController.currentActiveNavigationController() else {
            return
        }
        
        let viewController = ZPWInternalWebApp(url: url, webHandle: ZPWebHandle())
        viewController.setUpWebView(false, showShareBtn:true)
        navi.pushViewController(viewController, animated: true)
    }
    
}


fileprivate class ZPNotiAppleOpenAppAction: ZPNotificationAppleAction {
    func handleOpenWith(model: ZPAppleNotificationModel) {
        self.handleOpenMerchantApp(model.actionData)
    }
    
    private func handleOpenMerchantApp(_ appIdStr: String) {
        let appId = appIdStr.toInt()
        guard appId > 0,
            let navi = UINavigationController.currentActiveNavigationController() else {
            return
        }
        
        if let viewController: UIViewController = navi.topViewController {
            ZPReactNativeAppRouter().openExternalApp(appId, from: viewController)
        }
        
    }
}


fileprivate class ZPNotiAppleOpenVoucherListAction: ZPNotificationAppleAction {
    func handleOpenWith(model: ZPAppleNotificationModel) {
        self.handleOpenVoucherList()
    }
    
    private func handleOpenVoucherList() {
        guard let navi = UINavigationController.currentActiveNavigationController(),
            !(navi.viewControllers.last is ZPVoucherHistoryListViewController) else {
            return
        }
        
        if let viewController: UIViewController = navi.topViewController {
            ZPCenterRouter.launch(routerId: .voucher, from: viewController)
        }
    }
}

fileprivate class ZPNotiAppleOpenUserProfileAction: ZPNotificationAppleAction {
    func handleOpenWith(model: ZPAppleNotificationModel) {
        self.handleOpenUserProfile()
    }
    
    private func handleOpenUserProfile() {
        guard let navi = UINavigationController.currentActiveNavigationController(),
            !(navi.viewControllers.last is ZPMainProfileViewController) else {
                return
        }
        
        if let viewController: UIViewController = navi.topViewController {
            ZPCenterRouter.launch(routerId: .userProfile, from: viewController)
        }
    }
}
