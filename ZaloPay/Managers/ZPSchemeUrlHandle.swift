//
//  ZPSchemeUrlHandle.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 10/9/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import UIKit
import CocoaLumberjackSwift
import ZaloPayConfig


/*
 App Dịch Vụ:
 zalopay-1://backtoapp
 zalopay-1://post
 zalopay-1://backtologin
 
 zalopay://pay
 zalopay://otp
 zalopay://zalopay.vn/view/transid/
 zalopay://launch/app/
 zalopay://launch/tab/
 zalopay://launch/f/
 
 tel://
 mailto://
 
 App Zalo:
 zalopay-zapi-28://app/transfer
 zalopay-zapi-29://app/mywallet
*/

@objcMembers
class ZPSchemeUrlHandle: NSObject {
    private var scheme : ZPScheme?
    private var observers: [UrlHandler]  = [UrlHandler]()
    static let sharedInstance = ZPSchemeUrlHandle()
        
    override init() {
        super.init()
        registerAllHanlder()
    }
    
    private func registerAllHanlder(){
        observers.append(ZPBackUrlHandler())
        observers.append(ZPPayInternalAppUrlHandler())
        observers.append(ZPLoginUrlHandler())
        observers.append(ZPPayAppToAppUrlHandler())
        observers.append(ZPOtpUrlHandler())
        observers.append(ZPShowTransactionHistoryUrlHandler())
        observers.append(ZPOpenMerchantAppUrlHandler())
        observers.append(ZPOpenTabUrlHandler())
        observers.append(ZPOpenInternalAppUrlHandler())
        observers.append(ZPMailOrPhoneUrlHandler())
        observers.append(ZPZaloUrlHandler())
        observers.append(ZPClearingUrlHandler())
    }
    
    func handleUrlSchemes(schemesUrl: URL, sourceApplication: String) -> Bool {
        DDLogInfo("schemeUrl = \(schemesUrl)")
        let zpscheme = ZPScheme(schemeWith: schemesUrl, sourceApplication: sourceApplication)
        guard let handler = findHander(scheme: zpscheme) else {
            return false
        }
        self.scheme = handle(scheme: zpscheme, handler: handler)
        return true
    }
    
    func handleMissingScheme() {
        if let scheme = self.scheme  {
            handle(scheme: scheme, handler: findHander(scheme: scheme))
            self.scheme = nil
        }
    }
    
    //dùng urlhandler xử lý scheme.
    //thành công -> return nil,
    //không thành công (case scheme cần login data) -> return scheme để sau khi login xong gọi hàm handleMissingScheme() xử lí tiếp.
    @discardableResult
    private func handle(scheme zpscheme: ZPScheme?, handler urlhandler: UrlHandler?) -> ZPScheme? {
        guard let scheme = zpscheme, let handler = urlhandler else {
            return nil
        }
        if scheme.isTimeOut() {
            return nil
        }
        
        let authenticated = ApplicationState.sharedInstance.state == .authenticated
        let canHandle = handler.canHandle(scheme: scheme, authenticated: authenticated)
        if canHandle {
            let runTracker = {() in
                self.trackingTraffic(scheme: scheme)
                // Rule: Only track payment
                guard let tracker = handler as? ZPURLTrackOpenAppProtocol else {
                    return
                }
                tracker.trackOpenApp(from: scheme)
            }
            
            defer {
                runTracker()
            }
            handler.handleUrl(scheme: scheme)
            return nil
        }
        return scheme
    }
    
    private func trackingTraffic(scheme: ZPScheme) {
        let listUrl = ["zalopay://otp", "zalopay://zalopay.vn/view/transid"]
        let category = "Deep Link"
        for url in listUrl {
            if scheme.domain.hasPrefix(url){
                ZPTrackingHelper.shared().eventTracker?.trackCustomEvent(category, action: url, label: url, value: nil)
                return
            }
        }
        let action = scheme.domain
        let label = scheme.params["utm_source"] as? String ?? ""
        ZPTrackingHelper.shared().eventTracker?.trackCustomEvent(category, action: action, label: label, value: nil)
    }
    
    //hàm này dùng để lấy ra urlhandler có thể xử lý được scheme -> luôn truyền authenticated = true
     func findHander(scheme: ZPScheme) -> UrlHandler? {
        return observers.first(where: { $0.canHandle(scheme: scheme, authenticated: true) })
    }
}
