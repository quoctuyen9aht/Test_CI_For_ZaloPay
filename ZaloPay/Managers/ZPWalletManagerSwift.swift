//
//  ZPWalletManagerSwift.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 1/15/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import ZaloPayCommonSwift
import ZaloPayAnalyticsSwift
import Synchronized
import ZaloPayProfile
import ZaloPayConfig

enum IdentityType : Int32 {
    case cmnd = 1, passport, cc
}

@objcMembers
public class ZPWalletManagerSwift: NSObject {
    
    public var currentBalance:NSNumber?
    public lazy var config:ZPAppInfoConfig = {
         return (ZPCache.object(forKey: ZPConfigCacheKey) as? ZPAppInfoConfig) ?? ZPAppInfoConfig.default()
    }()
    
    public var enableRecharge:Bool = true
    public var bankSupports = [ZPBankSupport]()
    public var signalUpdatePlatform = RACSubject<AnyObject>()
    public var sdkHelper:ZPSDKHelper = ZPSDKHelper()
    
    var maintainWithdraw = false
    var maintainwithdrawto:UInt64 = 0
    var maintainwithdrawfrom:UInt64 = 0
    let transactionHistoryManager = TransactionHistoryManager()

    static let sharedInstance = ZPWalletManagerSwift()

    let formatter = DateFormatter()
    public func configDefaultValue() {
        if let enableRecharge = ZPCache.object(forKey: ZPEnableRechargeKey) as? NSNumber {
            self.enableRecharge = Bool(truncating: enableRecharge)
        }
//        if let config = ZPCache.object(forKey: ZPConfigCacheKey) as? ZPAppInfoConfig {
//            self.config = config
//        }
        self.autoUpdateBallance()

    }
    public func logout() {
        self.currentBalance = nil 
    }

    // MARK:------------------------update Balance---------------------------------
    public func updateBalanceAndTransactionLog(withBalanceValue balance:CLong) {
        self.transactionHistoryManager.loadSuccessTransaction()
        if balance > 0 {
            self.currentBalance = NSNumber(value: balance)
            ZMEventManager.bindEvent(EventTypeUpdateBalance.rawValue, data: balance)
            return
        }
        self.updateBalanceValue()
    }

    public func updateBalanceValue() {
        self.getBalance().subscribeNext { (x) in
            ZMEventManager.bindEvent(EventTypeUpdateBalance.rawValue, data: x)
        }
    }

    public func getBalance() -> RACSignal<AnyObject> {
        let accesstoken = NetworkManager.sharedInstance().accesstoken ?? ""
        if accesstoken.isEmpty {
            return RACSignal.empty()
        }
        return RACSignal.createSignal({ (subscriber) -> RACDisposable? in

            NetworkManager.sharedInstance().getBalance().subscribeNext({ (dict) in
                if let dic = dict as? NSDictionary,
                    let balance = dic.numeric(forKey: "zpwbalance") {
                    self.currentBalance = balance
                    subscriber.sendNext(balance.copy())
                }
                subscriber.sendCompleted()
            }, error: { (err) in
                if let balance = self.currentBalance {
                    subscriber.sendNext(balance)
                }
                subscriber.sendCompleted()
                if let error = err as NSError?, error.isApiError().not {
                    DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 1) {
                        self.reloadBalance()
                    }
                }
            })
            return nil
        })
    }
    
    private func reloadBalance() {
        if let signal = self.rac_signal(for:#selector(updateBalanceValue)) as? RACSignal<AnyObject> {
            NetworkState.sharedInstance().networkSignal.take(until: signal).throttle(2.0).filter({ (v) -> Bool in
                if let value = v as? NSNumber {
                    return value.boolValue
                }
                return false
            }).subscribeNext({ (x) in
                self.updateBalanceValue()
            })
        }
    }

    func autoUpdateBallance() {
        NotificationCenter.default.signalAppEnterBackground().subscribeNext { [weak self](x) in
            self?.updateBalanceValue()
            self?.autoLoadPlaformInfo()
        }
    }

    // MARK:---------------------------Get PlatformInfo---------------------------------

    func autoLoadPlaformInfo() {
        self.getPlatformInfo().subscribeCompleted {
            self.signalUpdatePlatform.sendNext(nil)
        }
    }
    public func getPlatformInfo() -> RACSignal<AnyObject> {
        return RACSignal.createSignal({ (subscriber) -> RACDisposable? in
            ZaloPayWalletSDKPayment.sharedInstance().getPlatformInfoConfig({ (dict) in
                if let dictionary = dict as? JSON {
                    self.handlePlatformInfoData(dictionary)
                    subscriber.sendNext(dictionary)
                    subscriber.sendCompleted()
                }
            })
            return nil;
        })
    }
    func handlePlatformInfoData(_ dic: JSON) {
        let forceUpdateApp = dic.bool(forKey: "forceappupdate")
        if forceUpdateApp {
            let resource = dic.value(forKey: "resource", defaultValue: JSON())
            let version = resource.string(forKey: "appversion", defaultValue: "")
            if LoginManagerSwift.sharedInstance.forceUpdateApp(version: version) {
                // for update app
                return
            }
        }
        let newestappversion = dic.string(forKey: "newestappversion")
        LoginManagerSwift.sharedInstance.alertAppHasNewVersion(version: newestappversion) //<-- kiểm tra và thông báo có version mới
        self.enableRecharge = dic.bool(forKey: "isenabledeposit")
        self.maintainWithdraw = dic.bool(forKey: "ismaintainwithdraw")
        self.maintainwithdrawfrom = dic.uInt64(forKey: "maintainwithdrawfrom", defaultValue: 0) / 1000
        self.maintainwithdrawto = dic.uInt64(forKey: "maintainwithdrawto", defaultValue: 0) / 1000
        var expiredtime = dic.double(forKey: "expiredtime", defaultValue: 0)/1000
        if (expiredtime <= 0) {
            expiredtime = platformInfoInterval.toDouble()
        }
        expiredtime = max(expiredtime, platformInfoInterval.toDouble())
        expiredtime = expiredtime + 10
        self.autoLoadPlaformInfoInterval(expiredtime.toInt())
        ZPCache.setObject(NSNumber(value: self.enableRecharge) , forKey: ZPEnableRechargeKey)
    }
    
    func autoLoadPlaformInfoInterval(_ interval:Int) {
        if interval <= 0 {
            return
        }
        
        if let signalBackground = NotificationCenter.default.enterBackground() {
            let signal = RACSignal<AnyObject>.`return`(nil)
            signal.delay(TimeInterval(interval)).take(until: signalBackground).filter({(_) -> Bool in
                return ApplicationState.sharedInstance.state == ZPApplicationState.authenticated
            }).subscribeNext({ [weak self](x) in
                self?.autoLoadPlaformInfo()
            })
        }
    }

    // MARK:------------------------Get AppInfo---------------------------------

    public func loadAppInfo() -> RACSignal<AnyObject> {
        return self.loadWithDrawConfig().then({
            return self.loadTransactionConfig()
        })
    }

    func loadTransactionConfig() -> RACSignal<AnyObject> {
        let type:[Int] = [ZPTransType.walletTopup.rawValue, ZPTransType.atmMapCard.rawValue, ZPTransType.tranfer.rawValue]
        return ZaloPayWalletSDKPayment.sharedInstance().getAppInfo(withId: kZaloPayClientAppId, transtypes: type).doNext({ (appdata) in
            if let appData = appdata as? ZPAppData {
                synchronized(self.config) { ()  in
                    self.config.update(appData)
                    ZPCache.setObject(self.config, forKey: ZPConfigCacheKey)
                }
            }
        })
    }
    func loadWithDrawConfig() -> RACSignal<AnyObject> {
        let type:[Int] = [(ZPTransType.withDraw).rawValue]
        return ZaloPayWalletSDKPayment.sharedInstance().getAppInfo(withId: withdrawAppId, transtypes: type).doNext({ (appdata) in
            if let appData = appdata as? ZPAppData {
                synchronized(self.config) { ()  in
                    self.config.updateWithdrawConfig(appData)
                    ZPCache.setObject(self.config, forKey: ZPConfigCacheKey)
                }
            }
        })

    }
    // MARK: -------------------update UserInfo---------------------------
    public func updateUserInfo() -> RACSignal<AnyObject> {
        // GetUserProfileSimpleInfo API data mapping is here
        return NetworkManager.sharedInstance().getProfileLevel().map({ (dict) -> Any? in
            if let dic = dict as? NSDictionary {
                let level = dic.int(forKey: "profilelevel", defaultValue: 0)
                let permission = dic.array(forKey: "profilelevelpermisssion") ?? [Any]()
                let email = dic.string(forKey: "email", defaultValue: "")
                // No more using this identity
//                let identity = dic.string(forKey: "identity", defaultValue: "")
                let phone = NSString(format: "%@", dic.string(forKey: "phonenumber", defaultValue: "")).normalizePhoneNumber() ?? ""
                let acountName = dic.string(forKey: "zalopayname", defaultValue: "") ?? ""
                
                // Parse list of 3 identity type(CMND, Passport, CC)
                if let kycInfo = dic.value(forKey: "kycInfo") as? NSDictionary {
                    // Get fullName,dob and gender
                    let fullName = kycInfo.string(forKey: "fullName")
                    let dob = kycInfo.int(forKey: "dob")
                    let gender = kycInfo.int(forKey: "gender")
                    
                    // Setting fullName,dob and gender
                    ZPProfileManager.shareInstance.fullName = fullName
                    ZPProfileManager.shareInstance.dob = Date(timeIntervalSince1970: TimeInterval(dob))
                    ZPProfileManager.shareInstance.gender = Gender(rawValue: Int(gender))
                    
                    // Process identity array
                    if let identityList = kycInfo.array(forKey: "ids") as? [NSDictionary] {
                        for identity in identityList {
                            let verified = identity.bool(forKey: "verified", defaultValue: false)
                            if verified {
                                let type = IdentityType(rawValue: identity.int(forKey: "type", defaultValue: 0)) ?? .cmnd
                                let value = identity.string(forKey: "value", defaultValue: "") ?? ""
                                self.setUserIdentityValue(with: type, value: value)
                            }
                        }
                    }
                }
                LoginManagerSwift.sharedInstance.updateProfileLevel(profileLevel: level, permission: permission, accountName: acountName, phone: phone)
                ZPProfileManager.shareInstance.email = email ?? ""
                return dic
            }
            return nil
        })
    }
    
    private func setUserIdentityValue(with type: IdentityType, value: String) {
        if type == .cmnd {
            ZPProfileManager.shareInstance.identifier = value
            return
        }
        if type == .passport {
            ZPProfileManager.shareInstance.passport = value
            return
        }
        ZPProfileManager.shareInstance.cc = value
    }
    
    // MARK:-------------------Bank Support--------------------------------------
    public func getListBankSupport() -> RACSignal<AnyObject> {
        return RACSignal.createSignal({ (subscriber) -> RACDisposable? in
            ZaloPayWalletSDKPayment.sharedInstance().getBankListEnabled({ (dict) in
                if dict == nil,let bankSupports = ZPCache.object(forKey: ZPBankSupportKey) as? [ZPBankSupport] {
                    self.bankSupports = bankSupports
                } else if let dictionary = dict as NSDictionary? {
                    self.bankSupports = self.parserBankData(dictionary)
                    ZPCache.setObject(self.bankSupports as NSCoding, forKey: ZPBankSupportKey)
                }
                subscriber.sendNext(self.bankSupports)
                subscriber.sendCompleted()
            })
            return nil
        }).deliverOnMainThread()
    }

    func parserBankData(_ data: NSDictionary) -> [ZPBankSupport] {
        guard let banklist = data.array(forKey: "banklist") as? [JSON],
            let imageMap = ZPBankSupport.imagesMapper(),
            let imageMapHorizontal = ZPBankSupport.imagesMapperHorizontal()  else {
                return [ZPBankSupport]()
        }
        
        var banks = [ZPBankSupport]()
        for oneDic in banklist {
            let code = oneDic.string(forKey: "tpebankcode")
            let bankFunctions: [JSON] = oneDic.value(forKey: "functions", defaultValue: [])
            let type = self.bankSupportType(bankFunctions: bankFunctions)
            let status = ZPBStatus(rawValue: ZPBStatus.RawValue(oneDic.int(forKey: "status")))
            let from = oneDic.uInt64(forKey: "maintenancefrom") / 1000
            let to = oneDic.uInt64(forKey: "maintenanceto") / 1000
            let msgMaintain = oneDic.string(forKey: "maintenancemsg")
            let minAppVersion = oneDic.string(forKey: "minappversion")
            let displayOrder = oneDic.int(forKey: "displayorder")
            let enableWithdraw = oneDic.bool(forKey: "allowwithdraw", defaultValue: true)
            let name = oneDic.string(forKey: "fullname")
            let icon = imageMap.string(forKey: code)
            let iconHorizontal = imageMapHorizontal.string(forKey: code)
            if let oneBank = ZPBankSupport.bank(withCode: code, name: name, image: icon, iconNameHorizontal: iconHorizontal) {
                oneBank.enableWithdraw = enableWithdraw
                oneBank.type = type
                oneBank.status = status
                oneBank.maintainFrom = from
                oneBank.maintainTo = to
                oneBank.messageMaintain = msgMaintain
                oneBank.minAppVersion = minAppVersion
                oneBank.displayOrder = NSNumber(value: displayOrder)
                if ZPBankSupport.isCCCard(code) {
                    banks.append(contentsOf:self.setupBankforCC(oneBank,false))
                    continue;
                }
                if ZPBankSupport.isCCDebitCard(code) {
                    banks.append(contentsOf:self.setupBankforCC(oneBank,true))
                    continue;
                }
                banks.append(oneBank)
            }
            
        }
        return banks
    }
    
    func bankSupportType(bankFunctions: [JSON]) -> ZPBSupportType {
        let supportType = ZaloPayWalletSDKPayment.sharedInstance().bankSupportType(bankFunctions)
        return supportType == .card ? ZPBSupportType_Card: ZPBSupportType_Account
    }
    
    func setupBankforCC(_ bank:ZPBankSupport,_ isCCDebit:Bool) -> [ZPBankSupport] {
        var banks = [ZPBankSupport]()
        if let imageMapHorizontal = ZPBankSupport.imagesMapperHorizontal() {
            // VisaCard
            if ZaloPayWalletSDKPayment.sharedInstance().isVisaCardEnable() {
                if let visa = isCCDebit ? ZPBankSupport.visaDebitCard() : ZPBankSupport.visaCard() {
                    let iconHorizontal = imageMapHorizontal.string(forKey: visa.code, defaultValue: "")
                    visa.iconNameHorizontal = iconHorizontal;
                    visa.type = ZPBSupportType_Card
                    visa.status = bank.status
                    visa.maintainFrom = bank.maintainFrom
                    visa.maintainTo = bank.maintainTo
                    visa.messageMaintain = bank.messageMaintain
                    visa.minAppVersion = bank.minAppVersion
                    visa.displayOrder = ZaloPayWalletSDKPayment.sharedInstance().ccDisplayOrder("visa")
                    visa.enableWithdraw = bank.enableWithdraw
                    banks.append(visa)
                }
            }
            // masterCard
            if ZaloPayWalletSDKPayment.sharedInstance().isMasterCardEnable() {
                if let master = isCCDebit ? ZPBankSupport.masterDebitCard() : ZPBankSupport.masterCard() {
                    let iconHorizontal = imageMapHorizontal.string(forKey: master.code, defaultValue: "")
                    master.iconNameHorizontal = iconHorizontal;
                    master.status = bank.status
                    master.maintainFrom = bank.maintainFrom
                    master.maintainTo = bank.maintainTo;
                    master.type = ZPBSupportType_Card
                    master.messageMaintain = bank.minAppVersion
                    master.minAppVersion = bank.minAppVersion
                    master.displayOrder = ZaloPayWalletSDKPayment.sharedInstance().ccDisplayOrder("masterCard")
                    master.enableWithdraw = bank.enableWithdraw
                    banks.append(master)
                }
            }
            // JCBCard
            if ZaloPayWalletSDKPayment.sharedInstance().isJCBCardEnable() {
                if let jcb = isCCDebit ? ZPBankSupport.jcbDebitCard() : ZPBankSupport.jcbCard() {
                    let iconHorizontal = imageMapHorizontal.string(forKey: jcb.code, defaultValue: "")
                    jcb.iconNameHorizontal = iconHorizontal;
                    jcb.status = bank.status
                    jcb.maintainFrom = bank.maintainFrom
                    jcb.maintainTo = bank.maintainTo;
                    jcb.type = ZPBSupportType_Card
                    jcb.messageMaintain = bank.minAppVersion
                    jcb.minAppVersion = bank.minAppVersion
                    jcb.displayOrder = ZaloPayWalletSDKPayment.sharedInstance().ccDisplayOrder("jcb")
                    jcb.enableWithdraw = bank.enableWithdraw
                    banks.append(jcb)
                }
            }
        }
        return banks

    }

    // MARK:-------------------Request React Native App-------------------------------------
    public func getlAllMerchantApp() -> RACSignal<AnyObject> {
        return ReactNativeAppManager.sharedInstance().loadAppFromServer()
    }

    public func downloadAppResource() -> RACSignal<AnyObject> {
        return ReactNativeAppManager.sharedInstance().downloadAllActiveAppIgnoreApp([])
    }

    public func preloadAppDataAtLaunchTime() {
        let manager = ReactNativeAppManager.sharedInstance()
        let internalApp = manager.getApp(reactInternalAppId)
        let runFolder = manager.getRunFolder(internalApp) ?? ""
        let hasCache = FileManager.default.fileExists(atPath: runFolder)
        
        let doneExtractLocal = manager.isDoneExtractLocalApp()
        if doneExtractLocal == false || hasCache == false {
            manager.clearAllCacheApp()
            self.extractLocalApp()
        }
        DispatchQueue.global(qos: .background).async {
            manager.loadAppFromServer().then({ () -> RACSignal<AnyObject> in
                return self.downloadAppResource()
            }).subscribeCompleted {}
        }
    }
    
    lazy var extractLocalApp:(() -> Void) = {
        let appinfos = [["appId":1,"zipFile":"ios.1"]]
        return ReactNativeAppManager.sharedInstance().loadApp(fromJson: appinfos)
    }
    // MARK:-------------------Maintain Withdraw-------------------------------------
    public func isMaintainWithdraw() -> Bool {
        return self.maintainWithdraw
    }
    public func alertMaintainWithdraw() {
        let fromDate = Date(timeIntervalSince1970: TimeInterval(self.maintainwithdrawfrom))
        let toDate = Date(timeIntervalSince1970: TimeInterval(self.maintainwithdrawto))

        let strFromHour = self.formatFrom(fromDate, "HH:mm")
        let strToHour = self.formatFrom(toDate, "HH:mm")
        let strFromDate = self.formatFrom(fromDate, "dd/MM/yyyy")
        let strToDate = self.formatFrom(toDate, "dd/MM/yyyy")
        let message = String(format: R.string_Withdraw_Maintain_Message(), strFromHour,strFromDate,strToHour,strToDate)
        ZPDialogView.showDialog(with: DialogTypeNotification, message: message, cancelButtonTitle: R.string_ButtonLabel_Close(), otherButtonTitle: nil, completeHandle: nil)

    }
    public func writeAppTransIdLog(_ bill:ZPBill) {
        if bill.transType == ZPTransType.atmMapCard {
            return
        }
        let apptransid = bill.appTransID ?? ""
        if apptransid.isEmpty {
            return
        }
        self.writeAllAppTransIdLog()
    }
    public func writeAllAppTransIdLog() {
        if let logs = ZPAppFactory.sharedInstance().orderTracking.allOrderLog() as? [NSDictionary] {
            if logs.count == 0 {
                return
            }
            ZPTrackingHelper.shared().eventTracker?.writeLogAll(transactions: logs)

        }

    }

    func formatFrom(_ date:Date,_ format:String) -> String {
        formatter.dateFormat = format
        return formatter.string(from: date)
    }

}


