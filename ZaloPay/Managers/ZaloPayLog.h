//
//  ZaloPayLog.h
//  ZaloPay
//
//  Created by bonnpv on 6/8/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CocoaLumberjack/CocoaLumberjack.h>
extern const DDLogLevel zaloPayLogLevel;
#define ddLogLevel  zaloPayLogLevel
