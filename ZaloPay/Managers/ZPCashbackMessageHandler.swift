//
//  ZPCashbackMessageHandler.swift
//  ZaloPay
//
//  Created by Bon Bon on 2/5/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation

enum GiftPopupAction: Int {
    case showTransaction = 1
    case showListGiftCode = 2
    case hide = 3
}

class ZPCashbackMessageHandler: NSObject, ZPGiftPopupHandler {
    
    weak var popup: ZPCashBackPopup?
    
    func canHandleMessage(_ message: ZPNotifyMessage) -> Bool {
        return message is ZPCashbackMessage
    }
    
    func handleMessage(_ message: ZPNotifyMessage) {
        guard let msg = message as? ZPCashbackMessage else {
            return
        }
        if msg.data.type != kCashbackDataType {
            return
        }
        self.popup =  ZPCashBackPopup.showWithData(data: msg.data) {
            if msg.data.action == GiftPopupAction.showTransaction.rawValue {
                ZPNotificationService.sharedInstance().showTransactionDetail(with: msg)
            } else {
                self.popup?.hide()
            }
            } as? ZPCashBackPopup
    }
    
    func isPopupShowing() -> Bool {
        return self.popup != nil
    }
}

