//
//  ZPLoginSchemeHandler.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 10/12/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation

class ZPLoginUrlHandler: UrlHandler {
    func canHandle(scheme: ZPScheme, authenticated: Bool) -> Bool {
        return authenticated && scheme.domain.hasPrefix("zalopay-1://backtologin")
    }
    
    func handleUrl(scheme: ZPScheme) {
        LoginManagerSwift.sharedInstance.logout()
    }
}
