//
//  ZPPayInternalAppSchemeHandler.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 10/12/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let excutePaymentAppURL = Notification.Name.init("excutePaymentApp")
}

class ZPPayInternalAppUrlHandler: UrlHandler, ZPURLTrackOpenAppProtocol {
    lazy var payBillModel: ZPPayBillModel = ZPPayBillModel()
    
    func canHandle(scheme: ZPScheme, authenticated: Bool) -> Bool {
        return authenticated && scheme.domain.hasPrefix("zalopay-1://post")
    }
    
    func handleUrl(scheme: ZPScheme) {
        if ZPAppFactory.sharedInstance().isPayingBill() {
            ZPDialogView.showDialog(with: DialogTypeNotification,
                                    message: R.string_AppToApp_Popup_When_IsBilling(),
                                    cancelButtonTitle: R.string_ButtonLabel_Close(),
                                    otherButtonTitle: nil,
                                    completeHandle: nil)
            return
        }
        NotificationCenter.default.post(name: .excutePaymentAppURL, object: nil)
        
        // case web gọi thanh toán -> hide trang kết quả.
        // app dịch vụ -> web != nil -> show trang kết quả.        
        self.payBillModel.shouldSkipSuccessView = ZPWebAppRouter.activeWebView() == nil
        self.payBillModel.orderSource = Int(OrderSource_WebToApp.rawValue)
        self.payBillModel.payBill(withParams: (scheme.params), complete:{(result: ZPSchemePayBillReponse) in
            if result.orderResult == ZPOrderResult.cancel {
                return
            }
            let params : Dictionary<String,Any> = scheme.params
            let muid = params.string(forKey: "muid")
            let maccesstoken = params.string(forKey: "maccesstoken")
            let transId = params.string(forKey: "apptransid")
            let urlString : String = ZPWebAppRouter.resultUrl(withAppId: result.appId, mUid: muid, mAccessToken: maccesstoken, zpTransId: transId)
            let web = ZPWebAppRouter.activeWebView()
            web?.loadUrlString(urlString)
        })
    }
}
