//
//  ZPSchemeTransferModel.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 10/6/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayCommonSwift

enum ZPTransferResult: Int {    
    case success = 1
    case params_error = 2
    case network_error = 3
    case cancel = 4
    case fail = 5
}

class ZPTransferModel : NSObject {
    
    var params = Dictionary<AnyHashable,Any>()
    var completitionBlock: ((_ result: ZPTransferResult) -> Void)?
    var amount : Int = 0
    weak var topViewController = UIViewController()
    
    init(params: Dictionary<AnyHashable,Any>) {
        super.init()
        self.params = params
    }
    
    func transferModel(withCompleteionHandle completeHandle: @escaping (_ result: ZPTransferResult) -> Void) {
        
        completitionBlock = completeHandle
        
        if !NetworkState.sharedInstance().isReachable {
            //3 lỗi kết nối mạng
            if (completitionBlock != nil) {
                ZPDialogView.showDialog(with: DialogTypeNoInternet, title: R.string_Dialog_Warning(), message: R.string_NetworkError_NoConnectionMessage(), buttonTitles: [R.string_ButtonLabel_Close()], handler: nil)
                completitionBlock!(ZPTransferResult.network_error)
            }
            return
        }
        
        let zpid : String = params.string(forKey: "zpid").lowercased()
        amount = params.int(forKey: "amount")
        if (zpid.count < 1 || amount < 1) {
            //2 Thiếu tham số, tham số không hợp lệ
            if (completitionBlock != nil) {
                completitionBlock!(ZPTransferResult.params_error)
            }
            return
        }
        
        let user = UserTransferData()
        user.accountName = zpid
        user.mode = .toZaloPayID
        user.source = .fromWebAppQRType2
        user.transferMoney = "\(amount)"
        user.message = params.string(forKey: "message")
        handleTransfer(with: user)
    }
    
    func handleTransfer(with user: UserTransferData) {
        
        let navi = UINavigationController.currentActiveNavigationController()
        topViewController = navi?.topViewController
        let transferViewController : ZPTransferReceiverWebAppWrapper = ZPTransferReceiverWebAppWrapper(transferData: user, preHandle: {}, cancelHandle: {
            self.completitionBlock!(ZPTransferResult.cancel)
            navi?.popToViewController(self.topViewController!, animated: true)
            //4 User huỷ bỏ giao dịch
            if self.completitionBlock != nil {
                self.completitionBlock!(ZPTransferResult.cancel)
            }
        }, errorHandle: {
            ZPAppFactory.sharedInstance().hideAllHUDs(for: nil)
            navi?.popToViewController(self.topViewController!, animated: true)
            //5 Giao dịch thất bại
            if self.completitionBlock != nil {
                self.completitionBlock!(ZPTransferResult.fail)
            }
        }) {
            //1 Chuyển tiền thành công
            navi?.popToViewController(self.topViewController!, animated: true)
            self.completitionBlock!(ZPTransferResult.success)
            if self.completitionBlock != nil {
                self.completitionBlock!(ZPTransferResult.success)
            }
        }
        
        navi?.pushViewController(transferViewController, animated: true)
    }
    
}


