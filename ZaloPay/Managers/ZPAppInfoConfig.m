//
//  ZPTransationConfig.m
//  ZaloPay
//
//  Created by bonnpv on 8/31/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPAppInfoConfig.h"
//#import "ApplicationState+GetConfig.h"
#import <ZaloPayWalletSDK/ZPPaymentChannel.h>
#import ZALOPAY_MODULE_SWIFT


@implementation ZPAppInfoConfig

+ (instancetype)defaultConfig {
    ZPAppInfoConfig *instance = [[ZPAppInfoConfig alloc] init];
    [instance setupDefaultConfig];
    return instance;
}

- (void)setupDefaultConfig {
    self.maxRechargeValue =  maxRechargeValue;
    self.minRechargeValue = minRechargeValue;
    self.minTransfer = defaultMinTransfer;
    self.maxWithdraw = defaultMaxWithdraw;
    self.maxTransfer = defaultMaxTransfer;
//    self.minWithdraw = [ApplicationState getIntConfigFromDic:@"withdraw"
//                                                      andKey:@"min_withdraw_money"
//                                                  andDefault:WithDrawMinDefault];
}

- (void)updateConfig:(ZPAppData *)appData{
    NSDictionary *map = appData.pmcTransTypeMap;
    ZPTransTypePmc *topup = [map objectForKey:@(ZPTransTypeWalletTopup)];
    ZPTransTypePmc *transfer = [map objectForKey:@(ZPTransTypeTranfer)];
    
    if (topup) {
        [self updateWalletTopUp:topup];
    }
    if (transfer) {
        [self updateTransfer:transfer];
    }
}

- (void)updateWithdrawConfig:(ZPAppData *)appData {
    ZPTransTypePmc *withdraw = [appData.pmcTransTypeMap objectForKey:@(ZPTransTypeWithDraw)];
    if (withdraw) {
        [self updateWithdraw:withdraw];
    }
}


- (void)updateWalletTopUp:(ZPTransTypePmc *)topup {
    NSDictionary *dic = [self minMaxFromPmc:topup defaultMax:maxRechargeValue defaultMin:minRechargeValue];
    self.maxRechargeValue = [[dic objectForKey:@"maxOfMax"] longValue];
    self.minRechargeValue = [[dic objectForKey:@"minOfMin"] longValue];
}

- (void)updateTransfer:(ZPTransTypePmc *)transfer {
    NSDictionary *dic = [self minMaxFromPmc:transfer defaultMax:0 defaultMin:0];
    self.minTransfer = [[dic objectForKey:@"minOfMin"] longValue];
    self.maxTransfer = [[dic objectForKey:@"maxOfMax"] longValue];
}

- (void)updateWithdraw:(ZPTransTypePmc *)withdraw {
    NSDictionary *dic = [self minMaxFromPmc:withdraw defaultMax:0 defaultMin:0];
    self.minWithdraw = [[dic objectForKey:@"minOfMin"] longValue];
    self.maxWithdraw = [[dic objectForKey:@"maxOfMax"] longValue];
}

- (NSDictionary *)minMaxFromPmc:(ZPTransTypePmc *)pmc
                     defaultMax:(long)dmax
                     defaultMin:(long)dmin {
    
    long maxOfMin = -1;
    long maxOfMax = -1;
    long minOfMax = NSIntegerMax;
    long minOfMin = NSIntegerMax;
    
    for (ZPChannel *oneChannel in pmc.pmcList) {
        if (oneChannel.status == ZPPMCDisable) {
            continue;
        }
        if (maxOfMin < oneChannel.minPPAmount) { // lấy min lớn nhất
            maxOfMin = oneChannel.minPPAmount;
        }
        if (minOfMin > oneChannel.minPPAmount) { // lấy min nhỏ nhất
            minOfMin = oneChannel.minPPAmount;
        }
        if (minOfMax > oneChannel.maxPPAmount) { // lấy max lớn nhất.
            minOfMax = oneChannel.maxPPAmount;
        }
        if (maxOfMax < oneChannel.maxPPAmount) { // lấy max lớn nhất
            maxOfMax = oneChannel.maxPPAmount;
        }
    }
    
    if (maxOfMin <= 0) {
        maxOfMin = dmin;
    }
    if (maxOfMax <= 0) {
        maxOfMax = dmax;
    }
    
    if (minOfMax == NSIntegerMax) {
        minOfMax = dmax;
    }
    if (minOfMin == NSIntegerMax) {
        minOfMin = dmin;
    }
    
    return @{@"maxOfMin": @(maxOfMin),
             @"minOfMax": @(minOfMax),
             @"minOfMin": @(minOfMin),
             @"maxOfMax": @(maxOfMax)};
}

@end
