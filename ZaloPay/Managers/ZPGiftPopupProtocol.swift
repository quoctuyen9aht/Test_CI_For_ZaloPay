//
//  ZPGiftPopupProtocol.swift
//  ZaloPay
//
//  Created by Bon Bon on 2/3/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import UIKit

protocol ZPGiftPopupHandler {
    func canHandleMessage(_ message: ZPNotifyMessage) -> Bool
    func handleMessage(_ message: ZPNotifyMessage)
    func isPopupShowing() -> Bool;
}

protocol ZPGiftPopupModel {
//    var isUnread: Bool {get set}
    var message: ZPNotifyMessage {get set}
    var info: String {get set}
}

let ZPGiftPopupToastUpdateNotification: Notification.Name = Notification.Name("ZPGiftPopupToastUpdateNotification")

protocol ZPGiftPopupToastProtocol: class {
    var toastView: ZPGiftToastView? { get set }
    var parentView: UIView { get }
    func configToast()
    func showMoreMessages()
}

protocol ZPPopupHideProtocol {
    func hide()
}

extension ZPGiftPopupToastProtocol {
    func configToast() {
        if self.toastView == nil {
            self.toastView = ZPGiftToastView(with: .default, on: self.parentView, tap: { [weak self] in
                self?.showMoreMessages()
            })
        }
        
        var totalUnread = ZPGiftPopupManager.sharedInstance.totalUnredMessage()
        if  ZPGiftPopupManager.sharedInstance.isListPopupShowing().not {
            totalUnread -= 1
        } else {
            totalUnread = 0
        }
        let giftMessage = totalUnread > 0 ? "\(totalUnread)" : ""
        self.toastView?.numberGift = giftMessage
        
        // Handler Event
        NotificationCenter.default.addObserver(forName: ZPGiftPopupToastUpdateNotification, object: nil, queue: OperationQueue.main) { [weak self](n) in
            guard let wSelf = self, let infor = n.object as? JSON else {
                return
            }
            let model: ZPGiftPopupModel? = infor.value(forKey: "model", defaultValue: nil)
            let total = infor.value(forKey: "total", defaultValue: "")
            let embeded = (model?.message.embeded as? JSON) ?? [:]
            let title: String? = embeded.value(forKey: "title", defaultValue: nil)
            let body: String? = embeded.value(forKey: "message", defaultValue: nil)
            let message = (body ?? title) ?? ""
            wSelf.toastView?.message = message
            wSelf.toastView?.numberGift = total
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                wSelf.toastView?.show()
            })
        }
    }
    
    func showMoreMessages() {
        // Track showing popup
        UIApplication.shared.keyWindow?
            .subviews
            .compactMap({ $0 as? ZPPopupHideProtocol })
            .forEach({
            $0.hide()
        })
    
        ZPGiftPopupManager.sharedInstance.showListPopup()
    }
}

enum ZPGiftPopupShow: Int {
    case none
    case popup
    case list
}

protocol ZPGiftPopupShowHandlerProtocol: class {
    func handlerShowPopup(with action: ZPGiftPopupShow)
}

protocol ZPGiftPopupCacheProtocol {
    func save(_ message: ZPNotifyMessage)
    func allMessage() -> [ZPNotifyMessage]
    func updateState(with message: ZPNotifyMessage)
    func clearData()
}

