//
//  ZPSDKHelper.swift
//  ZaloPay
//
//  Created by bonnpv on 10/3/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayCommonSwift
import ZaloPayAnalyticsSwift
import ZaloPayProfile
import ZaloPayConfig
import ZaloPayWeb
import ZaloPayShareControl

public class ZPSDKHelper: NSObject, ZPWalletDependenceProtocol {
    lazy var updateLevel3helper = ZPUploadProfileLevel3Helper()

    public func updateLevel3(from controller: UIViewController?) {
        if updateLevel3helper.isDoneUpload() {
            updateLevel3helper.alertDoneUpload()
            return
        }
        
        updateLevel3helper.uploadLevel(from: controller, cancel: { [weak controller] in
            controller?.popMe()
        }, error: {
            
        }, complete: {[weak controller] in
            controller?.popMe()
        })
//
//        let updateViewController = helper.updateLevel3ViewController({
//        }, error: {
//        }) { [weak controller] in
//            if let strongController = controller {
//                strongController.navigationController?.popToViewController(strongController, animated: true)
//            }
//        }
//        controller?.navigationController?.pushViewController(updateViewController, animated: true)
//
        
    }
    
    public func check(toReplaceFaceIDMessage messsage: String) -> String {
        return ZPTouchIDService.sharedInstance.checkAndReplaceFaceIdMessage(messsage)
    }
    
    public func authen(fromTouchIdCancel cancel: @escaping (() -> Void), error: @escaping ((Error?) -> Void), complete: @escaping ((String?) -> Void)) {
        let touchIds = ZPTouchIDService.sharedInstance
        let pass = touchIds.currentUserPassword()
        if (ZPSettingsConfig.sharedInstance().usingTouchId == false || pass.isEmpty) {
            cancel()
            return;
        }
        
        touchIds.authenFromTouchIDWithCompleteHandle({ (result) in
            if result == TouchIDResult.userCancel || result == TouchIDResult.activated {
                cancel()
                return
            }
            if result == TouchIDResult.success {
                complete(pass);
                return
            }
            error(nil)
        }, messsage:
            UIView.isIPhoneX() ? R.string_TouchID_Confirm_Pay_Password_Message() : R.string_TouchID_Confirm_Pay_Message()
        )
        
        
    }
    
    public func phoneIsSupportTouchId() -> Bool {
        let touchIds = ZPTouchIDService.sharedInstance
        if (touchIds.hasTouchID() == false) {
            return false;
        }
        return ZPSettingsConfig.sharedInstance().usingTouchId && !touchIds.currentUserPassword().isEmpty;
    }
    
    public func externalConfig() -> [String: Any]? {
        return ApplicationState.sharedInstance.getSDKConfig()
    }
    
    public func creditCardConfig() -> [String : Any]? {
        return externalConfig()?["creditCard"] as? JSON
    }
    
    public func checkCCBinIsSupport(_ ccBin: String) -> Bool {
        guard let configs = self.externalConfig() else {
            return false
        }
        let configCCLength = configs.value(forKey: "length_bin", defaultValue: 6)
        
        if ccBin.count < configCCLength {
            return true
        }
        let ccBinDict = configs.value(forKey: "bank_bin", defaultValue: [:])
        let key = String(ccBin.prefix(configCCLength))
        if ccBinDict.keys.contains(key) {
            return true;
        }
        self.writeLogBINNotAllow(key)
        return false
    }
    
    func writeLogBINNotAllow(_ number: String) {
        guard number.count > 0 else {
            return
        }
        let timestamp = Int64(NSDate().timeIntervalSince1970 * 1000)
        guard let uid = ZPProfileManager.shareInstance.userLoginData?.paymentUserId else {
            return
        }
        let data: [String: Any] = ["card_number": number,
                                   "uid":uid,
                                   "timestamp":timestamp]
        
        ZPTrackingHelper.shared().eventTracker?.writeLog(bin: NSDictionary(dictionary: data))
        
    }
    public func savePassword(_ pass: String?) {
        ZPTouchIDService.sharedInstance.saveZaloPayPassword(pass ?? "")
    }
    
    public func isTouchIDExist() -> Bool {
        return ZPTouchIDService.sharedInstance.isTouchIDExist()
    }
    
    public func isEnableTouchID() ->Bool {
        return ZPTouchIDService.sharedInstance.isEnableTouchID()
    }
    
    public func removeZaloPayPassword () {
        ZPTouchIDService.sharedInstance.removeZaloPayPassword()
    }
    
    //MARK: - SETTINGS
    
    public func usingTouchId() -> Bool {
        return ZPSettingsConfig.sharedInstance().usingTouchId
    }
    
    public func setUsingTouchId(_ value: Bool) {
        ZPSettingsConfig.sharedInstance().usingTouchId = value
    }
    
    public func turnOffTouchIdManual() -> Bool{
        return ZPSettingsConfig.sharedInstance().turnOffTouchIdManual
    }
    
    public func setTurnOffTouchIdManual(_ value: Bool){
        ZPSettingsConfig.sharedInstance().turnOffTouchIdManual = value
    }
    public func paymentCodeShowSecurityIntro() -> Bool{
        return ZPSettingsConfig.sharedInstance().paymentCodeShowSecurityIntro
    }
    
    public func setPaymentCodeShowSecurityIntro(_ value: Bool){
        ZPSettingsConfig.sharedInstance().paymentCodeShowSecurityIntro = value
    }
    public func paymentCodeChannelSelect() -> Int32{
        return Int32(ZPSettingsConfig.sharedInstance().paymentCodeChannelSelect)
    }
    
    public func setPaymentCodeChannelSelect(_ value: Int32){
        ZPSettingsConfig.sharedInstance().paymentCodeChannelSelect = Int32(value)
    }
    //MARK: - WalletManager
    
    public func currentBalance() -> Int {
        return (ZPWalletManagerSwift.sharedInstance.currentBalance ?? 0).intValue
    }
    
    public func update(withNewBalance balance: Int64) {
        ZPWalletManagerSwift.sharedInstance.updateBalanceAndTransactionLog(withBalanceValue: Int(balance))
    }
    
    //MARK: - Config
    
    public func maxCCCard() -> Int32 {
//        return Int32(ApplicationState.getIntConfig(fromDic: "general", andKey: "max_cc_links", andDefault: 3))
        return Int32(ZPApplicationConfig.getGeneralConfig()?.getMaxCCLinks() ?? 3)
    }
    
    public func bidvRegisterPayOnlineUrl() -> String? {
        // NSString *urlString = [self getStringFromConfigWithKey:@"bidv_register_link"];
        guard let configs = ApplicationState.sharedInstance.zalopayConfig() as NSDictionary? else {
            return nil;
        }
        return configs.string(forKey: "bidv_register_link", defaultValue: "")
    }
    
    public func bidvHelpUrl() -> String? {
        guard let configs = ApplicationState.sharedInstance.zalopayConfig() as NSDictionary? else {
            return nil;
        }
        return configs.string(forKey: "bidv_help_link", defaultValue: "")        
    }
    // MARK: - Placeholder
    public func createPlaceholderView() -> UIView {
//        guard let result = ZPPlaceHolderView.zp_viewFromSameNib() else {
//            fatalError("Error at nib ZPPlaceHolderView")
//        }
        let result = ZPPlaceHolderView(frame: .zero);
        return result
    }
    // MARK: - Notification
    public func isNotificationMapAccount(_ notificationType: Int32) -> Bool {
        return (notificationType == Int32(NotificationType.mapBankAccount.rawValue))
    }
    
    public func isNotificationRemoveAccount(_ notificationType: Int32) -> Bool {
        return (notificationType == Int32(NotificationType.removeBankAccount.rawValue))
    }
    
    public func isNotification(withUpdateBalance type: Int32) -> Bool {
        return (type == Int(NotificationType.payBill.rawValue)   ||
            type == Int(NotificationType.recharge.rawValue)  ||
            type == Int(NotificationType.mapCard.rawValue)   ||
            type == Int(NotificationType.transfer.rawValue)  ||
            type == Int(NotificationType.withdraw.rawValue))
    }
    
    //MARK: - Voucher
    public func goToVoucherHistoriesList() {
        ZPCenterRouter.launch(routerId: .voucher, from: nil)        
    }
    //MARK: RegisterBank
//    static var appConfig: [String: Any]? {
//        return ApplicationState.sharedInstance.zalopayConfig()
//    }
    
//    static var bankIntegrationInfo: [String: Any]? {
//        return appConfig?.value(forKey: "bankIntegration", defaultValue: nil)
//    }
    
    static var bankBIConfig: [ZPEnableOBConfigProtocol] = {
//        return bankIntegrationInfo?.value(forKey: "enableOB", defaultValue: nil) ?? []
        return ZPApplicationConfig.getBankIntegrationConfig()?.getEnableOB() ?? []
    }()
    
    public func registerBank(controller: UIViewController?,
                             info: [AnyHashable : Any]?,
                             transId: String?,
                             completion: ((Any?) -> Void)?,
                             errorHandler: ((Error?) -> Void)? = nil) {
        let card = info?.value(forKey: "cardinfo", defaultValue: "")
        let cardInfor = (card as NSString?)?.jsonValue() as? JSON
        let code = cardInfor?.value(forKey: "bankcode", defaultValue: "") ?? ""
//        let isSupported = ZPSDKHelper.bankBIConfig.first(where: { $0.value(forKey: "bankCode", defaultValue: "") == code }) != nil
        let isSupported = ZPSDKHelper.bankBIConfig.first(where: { $0.getBankCode() == code }) != nil
        guard isSupported else {
            var dict: [String: Any] = [:]
            dict["zptransid"] = transId
            dict["returnmessage"] = info?["message"]
            let error = NSError(domain: NSURLErrorDomain, code: NSURLErrorCannotParseResponse, userInfo: dict)
            errorHandler?(error)
            return
        }
        BankIntegrationViewController.show(controller, using: info as NSDictionary?, from: transId, with: completion, error: errorHandler)
    }
    
    public func showToast(from controller: UIViewController?, message: String?, delay: TimeInterval) {
        controller?.showToast(with: message, delay: delay)
    }
    //MARK: WriteLog
    
    public func writeAppTransIdLog(_ bill :ZPBill) {
        ZPWalletManagerSwift.sharedInstance.writeAppTransIdLog(bill)
    }
    
    //MARK: ZaloSK
    public func getZaloId() -> String? {
        return ZPProfileManager.shareInstance.currentZaloUser?.userId
    }
    
    public func userPhoneNumber() -> String? {
        return ZPProfileManager.shareInstance.userLoginData?.phoneNumber
    }
    
    public func zaloPayUserId() -> String? {
        return ZPProfileManager.shareInstance.userLoginData?.paymentUserId
    }
    
    public func getDeviceId() ->String? {
        return UIDevice.zmDeviceUUID()
    }
    
    public func isSupportCCDebit() -> Bool {
        guard let appConfigs = ApplicationState.sharedInstance.zalopayConfig() else {
            return false
        }
        return appConfigs.value(forPath: "bank.is_support_ccdebit", defaultValue: false)
    }
    
    public func isCCDebit(_ ccBin: String) -> Bool {
        
        if self.isSupportCCDebit().not {
            return false
        }
        guard let configs = self.externalConfig() else {
            return false
        }
        let configCCLength = configs.value(forKey: "length_bin", defaultValue: 6)
        
        if ccBin.count < configCCLength {
            return false
        }
        let bin = String(ccBin.prefix(configCCLength))
        let isDebit = configs.value(forPath: "bank_bin.\(bin).isdebit", defaultValue: false)
        return isDebit
    }

    public func mapBank(with vc: UIViewController?, for bill: ZPBill?) -> RACSignal<AnyObject> {
        guard let viewController = vc else {
            return RACSignal.empty()
        }
        let transtype = bill?.transType ?? .atmMapCard
        let appId = bill?.appId.toInt() ?? kZaloPayClientAppId
        return ZPBankApiWrapperSwift.sharedInstance.mapBank(with: viewController,
                                                        appId: appId,
                                                    transType: transtype)
    }

    public func showFAQ(_ fromVC: UIViewController) {
        guard let controllerSupport = ZPReactNativeAppRouter().supportCenterViewController() else {
            return
        }
        fromVC.navigationController?.pushViewController(controllerSupport, animated: true)
        
    }
    
    public func showNeedSupport(using data: [AnyHashable : Any], from viewController: UIViewController) {
        let model = ZPFeedbackModel()
        let category = data.value(forKey: "paytype", defaultValue: "")
        let image: UIImage? = data.value(forKey: "capturescreen", defaultValue: nil)
        let transaction: String? = data.value(forKey: "zptransid", defaultValue: nil)
        let errormessage: String? = data.value(forKey: "errormessage", defaultValue: nil)
        let errorCode: NSNumber = data.value(forKey: "errorcode", defaultValue: 0)
        
        model.configDataSource(withCategory: category,
                               transactionId: transaction,
                               image: image,
                               message: errormessage,
                               errorCode: errorCode)
        viewController.navigationController?.pushViewController(ZPFeedbackViewController(model: model), animated: true)
    }
    
    public func phonePattern() -> [String]? {
        let dic_phone_format = ZPApplicationConfig.getGeneralConfig()?.getPhoneFormat()
        let phonePatterns = dic_phone_format?.getPatterns()
        return phonePatterns
    }
    
    public func showTermView(from controller: UIViewController?) {
        
        guard let controller = controller else {
            return
        }
        //let termVC = ZPWWebViewController.init(url: termsOfUseUrl)
//        let params = ["view": "termsOfUse"]
//        guard let termVC = ZPDownloadingViewController(properties: params) else {
//            return
//        }
//        termVC.moduleName = "About"
        //controller.navigationController?.pushViewController(termVC, animated: true)
    }
    
    public func updateIdentifyNumber(_ number: String?) {
        let pUserID = ZPProfileManager.shareInstance.userLoginData?.paymentUserId ?? ""
        let key = "KYC_identifyNumber_\(pUserID)"
        let result = KeyChainStore.set(string: number, forKey: key)
        #if DEBUG
            guard number != nil else {
                return
            }
            assert(result, "Wrong save!!!")
        #endif
    }
    
    public func kycEnableMapCard() -> Bool {
        return ZPApplicationConfig.getKYCValue()?.mapCardEnable ?? false
    }
    
    public func kycEnableLimitTransaction() -> Bool {
        return ZPApplicationConfig.getKYCValue()?.transactionLimitEnable ?? false
    }
    
    public func kycLimitTransaction() -> Int {
        return ZPApplicationConfig.getKYCValue()?.limitTransaction ?? 500000
    }
    
    public func canUseGateway(_ appid: Int) -> Bool {
        // Will use config for this
        return ZaloPayWalletSDKPayment.sharedInstance()?.canPaymentGateway(appid) ?? false
    }
    
    public func trackAppsflyer(_ eventName: String, eventValues values: [AnyHashable : Any]) {
        guard kAnalyticEnvironment == .production else {
            return
        }
        
        AppsFlyerTracker.shared().trackEvent(eventName,withValues: values)
    }
    
    public func isBankAlreadyMapped(_ bankCode: String) -> Bool {
        return isBankAccountExist(bankCode) || isSavedCardExist(bankCode)
    }
    
    public func isBankAccountExist(_ bankCode: String) -> Bool {
        guard let allAccount = ZaloPayWalletSDKPayment.sharedInstance().paymentSavedBankAccount() as? [ZPSavedBankAccount], allAccount.count > 0  else {
            return false
        }
        
        let isExist = allAccount.filter {
            $0.bankCode == bankCode
        }.count > 0
        return isExist
    }
    
    public func isSavedCardExist(_ bankCode: String) -> Bool {
        guard let saveCards = ZaloPayWalletSDKPayment.sharedInstance().paymentSavedCard() as? [ZPSavedCard], saveCards.count > 0  else {
            return false
        }
        
        let isExist = saveCards.filter {
            $0.bankCode == bankCode
        }.count > 0
        
        return isExist
    }

    public func mapAccountBill(_ bankCode: String) -> ZPIBankingAccountBill {
        return ZPBill.getAddedBankAcount(bankCode: bankCode)
    }

    public func getMessageLinkCardResult(_ binCheck: String?) -> String? {
        guard let toCheck = binCheck?.toInt() else {
            // Not need to check
            return nil
        }
        // bins array
        let arraybins = ZPApplicationConfig.getSDKConfig()?.getBins()
        let bin = arraybins?.first(where: { $0.getBin().contains(toCheck) })
        return bin?.getMessageLinkcardResult()
    }
}

