//
//  ZPUniversalLinkHandler.swift
//  ZaloPay
//
//  Created by vuongvv on 2/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayCommonSwift
class ZPUniversalLinkHandler: NSObject {
    
    static let sharedInstance = ZPUniversalLinkHandler()
    
    func handleUniversalLink(_ url: String) {
        if url.contains("lbp").not {
            return
        }
        let arrParams = url.components(separatedBy: "=")
        if arrParams.count > 0  {
            handleLogin(byPhone: arrParams.last!)
        }
    }
    func handleLogin(byPhone phoneNumber: String) {
        guard let navi = UIStoryboard(name: "ClearingAccount", bundle: nil).instantiateInitialViewController() as? ZPNavigationCustomStatusViewController else {
            return
        }
        guard let clearingInputPhoneViewController = navi.viewControllers.first as? ClearingInputPhoneViewController else {
            return
        }
        clearingInputPhoneViewController.phoneNumber = phoneNumber
        LoginManagerSwift.sharedInstance.clearLoginData()
        UIApplication.shared.delegate?.window??.rootViewController = navi
    }
}
