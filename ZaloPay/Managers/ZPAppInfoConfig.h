//
//  ZPTransationConfig.h
//  ZaloPay
//
//  Created by bonnpv on 8/31/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ZaloPayCommon/CodableObject.h>
@class ZPAppData;

@interface ZPAppInfoConfig : CodableObject
@property (nonatomic) long maxRechargeValue;
@property (nonatomic) long minRechargeValue;
@property (nonatomic) long minTransfer;
@property (nonatomic) long maxTransfer;
@property (nonatomic) long minWithdraw;
@property (nonatomic) long maxWithdraw;
+ (instancetype)defaultConfig;
- (void)updateConfig:(ZPAppData *)appData;
- (void)updateWithdrawConfig:(ZPAppData *)appData;
@end
