//
//  ZPLocationManagerSwift.swift
//  ZaloPay
//
//  Created by thi la on 2/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import CocoaLumberjackSwift
import RxSwift

public class ZPLocationManagerSwift: NSObject {
    public static let share = ZPLocationManagerSwift()
    private lazy var cacheDataTable: ZPCacheDataTable = ZPCacheDataTable(db: GlobalDatabase.sharedInstance())
    let disposeBag = DisposeBag()
    
    var lastCoordinate = CLLocationCoordinate2D()
    
    let updateLocationTime = 300.0
    lazy var locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        manager.distanceFilter = CLLocationDistance(1)
        return manager
    }()
    
    public func getCoordinate() -> CLLocationCoordinate2D {
        return self.lastCoordinate
    }
    
    public func startUpdating() {
        DDLogInfo("start updating location")
        self.lastCoordinate = self.getCoordinateFromStorage()
        self.updateLocation()
        let event = self.rx.sentMessage(#selector(stopUpdating))
        Observable<Int>.interval(updateLocationTime, scheduler: MainScheduler.instance)
            .takeUntil(event)
            .subscribe(onNext: { [weak self](_) in
                self?.updateLocation()
            }).disposed(by: disposeBag)
    }
    
    @objc public func stopUpdating() {
        DDLogInfo("stop updating location")
    }
    
    private func updateLocation() {
        DDLogInfo("request update location")
        if CLLocationManager.locationServicesEnabled().not {
            DDLogInfo("Location services are not enabled")
            return
        }
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined, .restricted, .denied:
            DDLogInfo("could not access location")
        case .authorizedAlways, .authorizedWhenInUse:
            if #available(iOS 9.0, *) {
                self.locationManager.requestLocation()
            } else {
                self.locationManager.startUpdatingLocation()
            }
        }
    }
    
    func getCoordinateFromStorage() -> CLLocationCoordinate2D {
        var coordinate = CLLocationCoordinate2D()
        let locationString = cacheDataTable.cacheDataValue(forKey: kUserLocation) ?? ""
        let components = locationString.components(separatedBy: " - ")
        if components.count != 2 {
            return coordinate
        }
        let latitude = components[0].toDouble()
        let longitude = components[1].toDouble()
        coordinate.latitude = latitude
        coordinate.longitude = longitude
        return coordinate
    }
    
    private func storageLocation() {
        let latitute = self.lastCoordinate.latitude
        let longitude = self.lastCoordinate.longitude
        let locationString = "\(latitute) - \((longitude))"
        cacheDataTable.cacheDataUpdateValue(locationString, forKey: kUserLocation)
    }
}

extension ZPLocationManagerSwift: CLLocationManagerDelegate {
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        DDLogInfo("update to location : \(locations)")
        self.locationManager.stopUpdatingLocation()
        
        if let lastLocation = locations.last {
            self.lastCoordinate = lastLocation.coordinate
            self.storageLocation()
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        DDLogInfo("update location fail : \(error)")
        self.locationManager.stopUpdatingLocation()
    }
}

