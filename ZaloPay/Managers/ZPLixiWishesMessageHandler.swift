//
//  ZPLixiWishesMessageHandler.swift
//  ZaloPay
//
//  Created by vuongvv on 2/5/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
class ZPLixiWishesMessageHandler: NSObject, ZPGiftPopupHandler {
    
    weak var popup: ZPLiXiWishesPopup?

    func canHandleMessage(_ message: ZPNotifyMessage) -> Bool {
        return message is ZPLiXiWishesMessage
    }
    
    func handleMessage(_ message: ZPNotifyMessage) {
        guard let msg = message as? ZPLiXiWishesMessage else {
            return
        }
        if msg.data.type != kLixiWishesDataType {
            return
        }
        self.popup = ZPLiXiWishesPopup.showWithData(data: msg.data) {} as? ZPLiXiWishesPopup
    }
    func isPopupShowing() -> Bool {
        return self.popup != nil
    }

}
