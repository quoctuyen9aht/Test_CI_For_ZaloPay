//
//  ZPMailOrPhoneSchemeHandler.swift
//  ZaloPay
//
//  Created by bonnpv on 10/25/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

class ZPMailOrPhoneUrlHandler: UrlHandler {
    func canHandle(scheme: ZPScheme, authenticated: Bool) -> Bool {
        return scheme.domain.hasPrefix("tel://") || scheme.domain.hasPrefix("mailto://")
    }    
    func handleUrl(scheme: ZPScheme) {
        UIApplication.shared.openURL(scheme.urlScheme)
    }
}

