//
//  ZPOpenMerchantAppSchemeHandler.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 10/12/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayCommonSwift

class ZPOpenMerchantAppUrlHandler: UrlHandler {
    lazy var reactAppRouter: ZPReactNativeAppRouter = ZPReactNativeAppRouter()
    
    func canHandle(scheme: ZPScheme, authenticated: Bool) -> Bool {
        return authenticated && scheme.domain.hasPrefix("zalopay://launch/app/")
    }

    func handleUrl(scheme: ZPScheme) {
        let strAppId = scheme.uriPath.components(separatedBy: "/").last ?? ""
        let appId = Int(strAppId) ?? 0
        if appId <= 0 {
            return
        }
        guard let navi = UINavigationController.currentActiveNavigationController() else {
            return
        }
        if let viewController = navi.topViewController {
            self.reactAppRouter.openExternalApp(appId, from: viewController)
        }
    }
}

