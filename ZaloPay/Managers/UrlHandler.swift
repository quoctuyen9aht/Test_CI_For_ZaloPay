//
//  UrlHandler.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 10/12/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation

protocol UrlHandler {
    func canHandle(scheme: ZPScheme, authenticated: Bool) -> Bool
    func handleUrl(scheme: ZPScheme)
}

