//
//  ZPHandlerProtocol.swift
//  ZaloPay
//
//  Created by tridm2 on 5/7/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation

protocol ZPNotificationAppleHandler {
    func process(model: ZPAppleNotificationModel)
}
