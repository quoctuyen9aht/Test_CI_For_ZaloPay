//
//  ZPSchemePayBillModel.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 10/6/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayCommonSwift
typealias ZPSchemeBillHandle = (_ reponse: ZPSchemePayBillReponse) -> Void

enum ZPOrderResult: Int {
    case success       = 1
    case params_error  = 2
    case network_error = 3
    case cancel        = 4
    case fail          = 8
    case paying_bill   = 9
}

class ZPSchemePayBillReponse : NSObject {
    var appId : Int = 0
    var orderResult = ZPOrderResult.success
    var data = Dictionary<String,Any>()
    var bill = ZPBill()
}

@objcMembers
class ZPPayBillModel : NSObject {
    var completeHandle : ZPSchemeBillHandle?
    var orderSource : Int = 0
    var shouldSkipSuccessView = false
    
    func payBill(withParams params: Dictionary<String,Any>?, complete handle: @escaping ZPSchemeBillHandle) {
        self.completeHandle = handle        
        guard let billParams = params  else {
            complete(withAppId: 0, result: ZPOrderResult.params_error, resultInfo: nil)
            return
        }
        
        if (ZPWalletManagerSwift.sharedInstance.currentBalance ?? 0).intValue > 0 {
            self.payBill(withScheme: billParams)
            return
        }
        ZPWalletManagerSwift.sharedInstance.getBalance().deliverOnMainThread().subscribeCompleted {
            self.payBill(withScheme: billParams)
        }
    }
    
    func payBill(withScheme params: Dictionary<String,Any>) {
        let appId = params.int32(forKey: "appid")
        let zptranstoken = params.string(forKey: "zptranstoken")
        let apptransid = params.string(forKey: "apptransid")
        
        if (appId > 0 && zptranstoken.count > 0) {
            self.handleTransaction(withAppId: appId, transtoken: zptranstoken, params: params)
        } else if (apptransid.count > 0 && appId > 0) {
            self.handleBill(data: params, appId: appId)
        } else {
            self.complete(withAppId: Int(appId), bill: nil, result: ZPOrderResult.fail, resultInfo: nil)
        }
    }
    
    func handleTransaction(withAppId appId: Int32, transtoken zptranstoken: String, params: Dictionary<String,Any>) {
        
        ZPAppFactory.sharedInstance().showHUDAdded(to: nil)
        NetworkManager.sharedInstance().getOrderFromAppId(Int(appId), transToken: zptranstoken).deliverOnMainThread().subscribeNext({ [weak self] (dic : Any) in
            let dic : Dictionary<String,Any> = dic as! Dictionary<String,Any>
            self?.handleBill(data: dic, appId: appId)
            }, error:({ [weak self] (error) in
                ZPAppFactory.sharedInstance().hideAllHUDs(for: nil)
                ZPDialogView.showDialogWithError(error, handle: {
                    self?.complete(withAppId: Int(appId), bill: nil, result: ZPOrderResult.fail, resultInfo: nil)
                })
            }))
    }
    
    func handleBill(data billDic: Dictionary<String,Any>, appId: Int32) {
        
        if ZPAppFactory.sharedInstance().isPayingBill() {
            return
        }
        
        guard let navi  = UINavigationController.currentActiveNavigationController() else {
            return
        }
        
        let billInfo : ZPBill = ZPBill(data: billDic, transType: ZPTransType.billPay, orderSource: Int32(orderSource), appId: Int(appId))
        // Skip màn hình thanh toán thành công trong flow mobil app-to-app
        billInfo.shouldSkipSuccessView = self.shouldSkipSuccessView
        let topViewController = navi.topViewController
        ZPAppFactory.sharedInstance().showZaloPayGateway(topViewController,
                                                         appId: Int(appId),
                                                         with: billInfo,
                                                         complete:
            { [weak self](data) in
                self?.complete(withAppId: Int(appId), bill: billInfo, result: ZPOrderResult.success, resultInfo: data as? Dictionary<String, Any>)
                self?.backToViewController(topViewController)
            }, cancel: { [weak self](data) in
                self?.complete(withAppId: Int(appId), bill: billInfo, result: ZPOrderResult.cancel, resultInfo: data as? Dictionary<String, Any>)
                self?.backToViewController(topViewController)
        }) { [weak self](data) in
            self?.complete(withAppId: Int(appId), bill: billInfo, result: ZPOrderResult.fail, resultInfo: data as? Dictionary<String, Any>)
            self?.backToViewController(topViewController)
        }
    }
    
    func backToViewController(_ topViewController: UIViewController?) {
        guard let top = topViewController else {
            return
        }
        top.navigationController?.popToViewController(top, animated: true)
    }
    
    func complete(withAppId appId: Int, bill: ZPBill? = nil, result: ZPOrderResult, resultInfo data: Dictionary<String,Any>?) {
        
        let response = ZPSchemePayBillReponse()
        response.appId = appId
        response.orderResult = result
        response.data = data ?? Dictionary()
        response.bill = bill ?? ZPBill()
        if (completeHandle != nil) {
            self.completeHandle!(response)
            self.completeHandle = nil
        }
    }
}


