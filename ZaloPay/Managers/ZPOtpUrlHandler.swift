//
//  ZPOtpSchemeHandler.swift
//  ZaloPay
//
//  Created by Nguyen Tuong Vi on 10/12/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import UIKit
import ZaloPayCommonSwift

class ZPOtpUrlHandler: UrlHandler {
    
    func canHandle(scheme: ZPScheme, authenticated: Bool) -> Bool {
        return scheme.domain.hasPrefix("zalopay://otp")
    }
    
    func handleUrl(scheme: ZPScheme) {
        guard let otp = scheme.uriPath.components(separatedBy: "/").last ,
            otp.count == NUMBER_OF_OTP,
            otp.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil else {
            return
        }
        
        var handler: ZPOTPProtocol?
        defer {
            DispatchQueue.main.async {
                handler?.didReceiveOptMessage(otp)
            }
        }
        
        // Track current navi
        let topVC =  UINavigationController.currentActiveNavigationController()?.topViewController
        handler = [topVC, topVC?.presentedViewController].compactMap({ $0 as? ZPOTPProtocol }).first
    }
}
