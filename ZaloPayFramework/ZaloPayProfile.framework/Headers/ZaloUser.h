////
////  ZaloLoginUser.h
////  ZaloPay
////
////  Created by bonnpv on 4/25/16.
////  Copyright © 2016-present VNG Corporation. All rights reserved.
////
//
//#import <Foundation/Foundation.h>
//
//typedef enum {
//    GenderMale = 1,
//    GenderFemale = 2
//} Gender;
//
//@interface ZaloUser : NSObject
//@property(nonatomic, strong) NSString *userId;
//@property(nonatomic, strong) NSString *displayName;
//@property(nonatomic, strong) NSString *avatar;
//@property(nonatomic, assign) Gender gender;
//@property(nonatomic, strong) NSDate *birthDay;
//@property(nonatomic) BOOL usedZaloPay;
//
//// zaloPayId: userid trong hệ thống của zalopay
//// zaloPayName: zalopayid (accountname) trong hệ thống của zalopay
//@property(nonatomic, strong) NSString *zaloPayId;
//@property(nonatomic, strong) NSString *phone;
//@property(nonatomic, strong) NSString *zaloPayName;
//
//@property(nonatomic, strong, readonly) NSString *firstCharacterInDisplayName;
//@property(nonatomic, strong, readonly) NSString *asciiDisplayName;
//
//- (NSComparisonResult)compareByDisplayValue:(ZaloUser *)other;
//
//+ (NSDictionary *)userDictionary:(ZaloUser *)user;
//
//+ (instancetype)userFromDictionary:(NSDictionary *)dictionary;
//@end
