//
//  ZPZaloFriendTable.h
//  ZaloPay
//
//  Created by bonnpv on 6/20/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <ZaloPayDatabase/ZPBaseTable.h>
#import "ZPContactProtocol.h"

@class ZaloUserSwift;
@class ZPContactSwift;

@interface ZPZaloFriendTable : ZPBaseTable
- (void)saveZaloFriends:(NSArray *)friends;

- (NSArray *)zaloFriendList;

- (int)countZaloFriendList;

- (NSArray *)zaloPayContactList:(ContactMode)mode;

- (NSArray *)zaloPayContactListFavorite:(ContactMode)kindShowContactList;

- (NSDictionary *)getUnsavePhoneContactWith:(NSString *)phone;

- (void)mergeAllContactDataToZPC;

- (NSDictionary *)getZaloUserWithId:(NSString *)zaloId;

- (NSInteger)countTotalContact;

- (NSDictionary *)getUserFromZaloPayContact:(NSString *)phone;

- (NSArray *)getListUserFromZaloPayContact:(NSArray *)phones;

- (NSMutableArray *)getAllFriendIdShouldRequestZaloPayInfo:(BOOL) forSycn;

- (NSMutableArray *)getAllPhonesShouldRequestInfo;
- (NSMutableArray *)getAllPhonesInZPCShouldRequestInfo;

- (NSArray *)lixiZaloFriendInfoWithIds:(NSArray *)ids;

#pragma mark - Phone Contact Table

- (void)saveListPhone:(NSMutableArray *)contacts;

- (void)saveUnSavePhoneContact:(ZPContactSwift *)contact;

// hàm này dùng khi search ra số điện thoại trong danh bạ chưa dùng zalopay,
// user click -> request lên server, server trả ra có info -> update vào DB.
- (void)updateFriendProfileToZPC:(ZPContactSwift *)contact phone:(NSString *)phone;

#pragma mark - ZaloPayInfo Table

- (void)saveZalopayInfoWithUserId:(NSString *)userid
                        zalopayId:(NSString *)zalopayid
                            phone:(NSString *)phone
                      zalopayname:(NSString *)zalopayname
                           status:(int)status
                      displayName:(NSString *)displayName;

- (void)saveZaloPayInfoData:(NSArray *)data;

- (void)setFavoriteOrIgnore:(NSDictionary *)data;

- (void)setStateKeyboardWithValue:(NSString *)value
                           forKey:(NSString *)key;

- (NSString *)getStateKeyboardForKey:(NSString *)key;

@end
