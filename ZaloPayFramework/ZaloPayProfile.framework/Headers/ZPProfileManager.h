////
////  ZaloPayProfileManager.h
////  ZaloPayProfileManager
////
////  Created by bonnpv on 12/16/16.
////  Copyright © 2016-present VNG. All rights reserved.
////
//
//#import <Foundation/Foundation.h>
////#import "ZaloUser.h"
////#import "ZPUserLoginData.h"
//@class ZPUserLoginDataSwift;
//@class ZaloSDKApiWrapper;
//@class ZaloUserSwift;
//@class ZPCacheDataTable;
//@class ZPZaloFriendTable;
//@class ZPProfileTable;
//
//@interface ZPProfileManager : NSObject
//@property (nonatomic, strong, nullable) ZPUserLoginDataSwift *userLoginData;
//@property (nonatomic, strong, nullable) ZaloUserSwift *currentZaloUser;
//@property (nonatomic, strong, nullable) NSString *email;
//@property (nonatomic, strong, nullable) NSString *identifier;
//@property (nonatomic, strong, nullable) NSString *address;
//@property (nonatomic) BOOL enableMergeName;
//@property (nonatomic, strong, nonnull) NSArray *phonePattern;
//@property (nonatomic, strong, nonnull) ZPCacheDataTable* cacheDataTable;
//@property (nonatomic, strong, nonnull) ZPZaloFriendTable* zaloFriendTable;
//@property (nonatomic, strong, nonnull) ZPProfileTable* profileTable;
//+ (instancetype _Nonnull )shareInstance;
//- (void)saveUserLoginData;
//- (ZPUserLoginDataSwift *_Nullable)loginDataFromCache;
//- (void)logout;
//
//- (BOOL)isPhoneNumber:(NSString *_Nonnull)phone;
//@end
