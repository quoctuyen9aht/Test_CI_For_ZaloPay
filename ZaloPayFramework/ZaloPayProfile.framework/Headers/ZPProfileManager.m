////
////  ZaloPayProfileManager.m
////  ZaloPayProfileManager
////
////  Created by bonnpv on 12/16/16.
////  Copyright © 2016-present VNG. All rights reserved.
////
//
//#import "ZPProfileManager.h"
//#import <ZaloPayCommon/ZPConstants.h>
//#import <ZaloPayDatabase/GlobalDatabase+CacheData.h>
//#import <ZaloPayCommon/NSCollection+JSON.h>
//
//#import "ZPProfileTable.h"
//#import <ZaloPayDatabase/PerUserDataBase.h>
//#import <ZaloPayDatabase/ZPCacheDataTable.h>
//#import "ZPProfileManager+ZaloSDK.h"
//#import <ZaloPayProfile/ZaloPayProfile-Swift.h>
//#import "ZPZaloFriendTable.h"
//
//@implementation ZPProfileManager
//
//+ (instancetype)shareInstance {
//    static ZPProfileManager *instance;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        instance = [[ZPProfileManager alloc] init];
//    });
//    return instance;
//}
//
//- (id)init {
//    self = [super init];
//    if (self) {
//        self.cacheDataTable = [[ZPCacheDataTable alloc] initWithDB:[PerUserDataBase sharedInstance]];
//        self.zaloFriendTable = [[ZPZaloFriendTable alloc] initWithDB:[PerUserDataBase sharedInstance]];
//        self.profileTable = [[ZPProfileTable alloc] initWithDB:[PerUserDataBase sharedInstance]];
//        
//        [self loadConfig];
//    }
//    return self;
//}
//
//- (void)saveUserLoginData {
//    if (self.userLoginData) {
//        [[GlobalDatabase sharedInstance] cacheDataUpdateValue:[self.userLoginData jsonString] forKey:kZPUserLoginDataKey];
//    } else {
//        [[GlobalDatabase sharedInstance] cacheDataRemoveDataForKey:kZPUserLoginDataKey];
//    }
//}
//
//- (ZPUserLoginDataSwift *)loginDataFromCache {
//    NSString *cache = [[GlobalDatabase sharedInstance] cacheDataValueForKey:kZPUserLoginDataKey];
//    return [ZPUserLoginDataSwift fromDic:[cache JSONValue]];
//}
//
//- (void)logout {
//    self.userLoginData = nil;
//    self.currentZaloUser = nil;
//    [self saveUserLoginData];
//}
//
//- (BOOL)isPhoneNumber:(NSString *)phone {
//
//    for (NSString *pattern in self.phonePattern) {
//        NSString *phoneRegex = pattern;
//        NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
//        if ([phoneTest evaluateWithObject:phone]) {
//            return YES;
//        }
//    }
//
//    return NO;
//}
//
//#pragma mark - Override
//
//- (NSString *)address {
//    return [self.profileTable getProfileValueForKey:kAddress];
//}
//
//- (void)setAddress:(NSString *)address {
//    [self.profileTable updateProfileData:address forKey:kAddress];
//}
//
//- (NSString *)email {
//    return [self.profileTable getProfileValueForKey:kEmail];
//}
//
//- (void)setEmail:(NSString *)email {
//    [self.profileTable updateProfileData:email forKey:kEmail];
//}
//
//- (NSString *)identifier {
//    return [self.profileTable getProfileValueForKey:kIdentifier];
//}
//
//- (void)setIdentifier:(NSString *)identifier {
//    [self.profileTable updateProfileData:identifier forKey:kIdentifier];
//}
//@end
