//
//  ZaloPayProfile.h
//  ZaloPayProfile
//
//  Created by Bon Bon on 4/23/18.
//  Copyright © 2018 VNG. All rights reserved.
//
#import <UIKit/UIKit.h>

//! Project version number for ZaloPayProfile.
FOUNDATION_EXPORT double ZaloPayProfileVersionNumber;

//! Project version string for ZaloPayProfile.
FOUNDATION_EXPORT const unsigned char ZaloPayProfileVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ZaloPayProfile/PublicHeader.h>

