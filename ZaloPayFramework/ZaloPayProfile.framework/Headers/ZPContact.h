//
//  ZPContact.h
//  ZaloPay
//
//  Created by Phuoc Huynh on 7/17/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ZaloPayCommon/ZaloPayCommon.h>
//#import "ZPContactSwift.h"
//@interface ZPContact : CodableObject <ZPContactProtocol>
//@property(nonatomic, strong) NSString *zaloPayId;
//@property(nonatomic, strong) NSString *zaloId;
//@property(nonatomic, strong) NSString *phoneNumber;
//@property(nonatomic, strong) NSString *zaloPayName;
//@property(nonatomic, strong) NSString *displayNameZLF;
//@property(nonatomic, strong) NSString *displayNameUCB;
//@property(nonatomic, strong) NSString *avatar;
//@property(nonatomic) UserStatus status;
//
//@property(nonatomic, strong) NSString *displayName;
//@property(nonatomic, strong) NSString *firstCharacterInDisplayName;
//@property(nonatomic, strong) NSString *asciiDisplayName;
//@property(nonatomic, assign) UserGender gender;
//@property(nonatomic, strong) NSDate *birthDay;
//@property(nonatomic) BOOL usedZaloPay;
//@property(nonatomic) BOOL isFavorite;
//@property(nonatomic) BOOL isRedPacket;
//@property(nonatomic, strong) NSString *zpContactID;
//@property(nonatomic, strong) NSString *firstName;
//@property(nonatomic, strong) NSString *lastName;
//@property(nonatomic) BOOL isSavePhoneNumber;
//
////+ (instancetype)fromPhone:(NSString *)phone;
////
////+ (instancetype)userFromServerDictionary:(NSDictionary *)dictionary;
////
////+ (instancetype)createContactFromReactDictionary:(NSDictionary *)dictionary;
//
//- (NSString *)createZaloPayContactId;
//@end
