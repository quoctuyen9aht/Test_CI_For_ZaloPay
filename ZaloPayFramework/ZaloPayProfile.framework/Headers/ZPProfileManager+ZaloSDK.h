////
////  ZPProfileManager+ZaloSDK.h
////  ZaloPayProfile
////
////  Created by bonnpv on 12/19/16.
////  Copyright © 2016-present VNG. All rights reserved.
////
//
//#import "ZPProfileManager.h"
//#import "ZPContactProtocol.h"
//#import <ReactiveObjC/ReactiveObjC.h>
//
//@interface ZPProfileManager (ZaloSDK)
//- (RACSignal *)loadZaloUserProfile;
//
//- (RACSignal *)loadZaloUserProfileUsingCache:(BOOL)usingCache;
//
//- (RACSignal *)loadZaloUserFriendList:(BOOL)forceSyncContact;
//
//- (BOOL)isDoneLoadContact;
//
//- (ZaloUserSwift *)getZaloUserProfile:(NSString *)zaloId;
//
//- (NSArray *)getZaloFriendListFromDB;
//
//- (NSArray *)lixiZaloFriendInfoWithIds:(NSArray *)ids;
//
//- (RACSignal *)requestUserProfile:(NSString *)userId;
//
//- (void)loadConfig;
//
//- (id <ZPContactProtocol>)getContactInfoByPhone:(NSString *)phone;
//
//- (NSArray<id <ZPContactProtocol>> *)getListContactInfoByPhone:(NSArray *)phones;
//
//- (NSArray *)getZaloPayContactListFromDB:(ContactMode)contactMode;
//
//- (NSArray<id <ZPContactProtocol>> *)getZaloPayContactListFavoriteFromDB:(ContactMode)contactMode;
//
//- (NSString *)createZPContactId:(id <ZPContactProtocol>)contact;
//
////- (NSArray *)getListUnsavePhoneContactFromDB;
//- (id <ZPContactProtocol>)getUnsavePhoneContactFromDBWithPhoneStringInput:(NSString *)phoneStringInput;
//@end
