//
//  ZPProfileTable.h
//  ZaloPayCommon
//
//  Created by PhucPv on 6/3/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <ZaloPayDatabase/ZPBaseTable.h>

#define kUserid             @"userId"
#define kDisplayName        @"displayName"
#define kAvatar             @"avatar"
#define kUserGender         @"userGender"
#define kBirthDate          @"birthDate"
#define kIdentifier         @"identifier"
#define kAddress            @"address"
#define kEmail              @"email"
#define kAddress            @"address"

@interface ZPProfileTable : ZPBaseTable
- (void)updateProfileData:(NSString *)value forKey:(NSString *)key;

- (NSString *)getProfileValueForKey:(NSString *)key;

- (void)saveCurrentUserInfo:(NSObject *)user;

- (NSObject *)getCurrentUserInfo;
@end
