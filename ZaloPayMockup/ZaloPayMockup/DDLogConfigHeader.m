//
//  DDLogConfigHeader.m
//  ZaloPayNetwork
//
//  Created by bonnpv on 6/7/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "DDLogConfigHeader.h"

const DDLogLevel zaloPayLogLevel = DDLogLevelInfo;
