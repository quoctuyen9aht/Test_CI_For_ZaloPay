//
//  NetworkProxyHandler.h
//  ZaloPayNetwork
//
//  Created by Huu Hoa Nguyen on 9/16/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Make global functions usable in C++
 */
#if defined(__cplusplus)
#define ZPN_EXTERN extern "C" __attribute__((visibility("default")))
#else
#define ZPN_EXTERN extern __attribute__((visibility("default")))
#endif

#define ZPN_EXPORT_PROXY() \
ZPN_EXTERN void ZPNRegisterProxyHandler(Class); \
+ (void)load { ZPNRegisterProxyHandler(self); }


// Network proxy to pre-handle request
@protocol NetworkProxyHandler <NSObject>

// Initialize the handler
- (void)initialize;

@optional
// handle network request
// return TRUE if can handle
//        FALSE if ignore
- (BOOL)handleRequestWithPath:(NSString *)path
                       params:(NSDictionary *)params
                    subscribe:(id<RACSubscriber>)subscriber;

// handle network response
- (void)preHandleResult:(NSDictionary *)responseData
                  error:(NSError *)error
                   path:(NSString *)path
                 params:(NSDictionary *)requestParams;
@end

