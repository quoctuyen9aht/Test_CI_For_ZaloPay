//
//  NetworkMockup.h
//  ZaloPayNetwork
//
//  Created by Huu Hoa Nguyen on 9/16/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import "NetworkProxyHandler.h"

@interface NetworkMockup: NSObject<NetworkProxyHandler>
@end


