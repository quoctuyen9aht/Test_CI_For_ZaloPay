//
//  NetworkMockup.m
//  ZaloPayNetwork
//
//  Created by Huu Hoa Nguyen on 9/16/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import <ReactiveObjC/ReactiveObjC.h>
#import <ZaloPayCommon/NSCollection+JSON.h>

#import "DDLogConfigHeader.h"
#import "NetworkMockup.h"


@interface NetworkMockup()
@property (nonatomic, strong) NSDictionary *networkMockup;
@property (nonatomic, strong) NSString *sessionPrefix;
@property (nonatomic) int64_t counter;
@end

@implementation NetworkMockup

#if ENABLE_NETWORK_MOCKUP

ZPN_EXPORT_PROXY()

#endif

- (void)initialize {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyyMMdd_HHmmss";
    self.sessionPrefix = [formatter stringFromDate:[NSDate date]];
    self.counter = 0;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateMockupData)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:[UIApplication sharedApplication]];

    self.networkMockup = nil;
    NSString *path = [[NSBundle mainBundle] pathForResource:@"network_mockup" ofType:@"json"];
    [self loadNetworkMockup:path];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL)handleRequestWithPath:(NSString *)path params:(NSDictionary *)params subscribe:(id<RACSubscriber>)subscriber {
    DDLogInfo(@"mockup path: %@", path);
    
    NSArray *allMockups = [self.networkMockup arrayForKey:path];
    if (!allMockups) {
        DDLogInfo(@"ignore path: %@", path);
        return false;
    }
    
    for (NSDictionary *mockupItem in allMockups) {
        NSDictionary *mockupOutput = [self findSuitablePathForMocking:mockupItem params:params];
        if (!mockupOutput) {
            continue;
        }
        
        DDLogInfo(@"Found mockup: %@", mockupOutput);
        [subscriber sendNext:mockupOutput];
        return true;
    }
    
    DDLogInfo(@"ignore path: %@", path);
    return false;
}

- (NSDictionary *)findSuitablePathForMocking:(NSDictionary *)mockupItem params:(NSDictionary *)params {
    NSDictionary* mockupParams = [mockupItem dictionaryForKey:@"params"];
    BOOL isMatching = YES;
    for (NSString *key in mockupParams.allKeys) {
        if (![[mockupParams stringForKey:key] isEqualToString:[params stringForKey:key]]) {
            isMatching = NO;
            continue;
        }
        if ([mockupParams intForKey:key] != [params intForKey:key]) {
            isMatching = NO;
            continue;
        }
    }

    if (!isMatching) {
        return nil;
    }

    NSDictionary *mockupOutput = [mockupItem dictionaryForKey:@"output"];
    return mockupOutput;
}

// record response data
- (void)preHandleResult:(NSDictionary *)responseData error:(NSError *)error
                   path:(NSString *)path
                 params:(NSDictionary *)requestParams {
    DDLogInfo(@"network response for path: %@", path);
    if (!responseData || !requestParams) {
        return;
    }
    @try {
        NSMutableDictionary *outputDict = [NSMutableDictionary dictionary];
        [outputDict setObjectCheckNil:path forKey:@"path"];
        NSMutableDictionary *requestParameters = [requestParams mutableCopy];
        
        NSArray* keysToRemove = @[@"osver", @"accesstoken", @"userid", @"appversion", @"mno",
                                  @"sdkversion", @"resourceversion", @"dscreentype", @"devicemodel",
                                  @"platformcode", @"sdkver", @"latitude", @"deviceid", @"longitude",
                                  @"conntype", @"apptime", @"platform", @"mac"];
        [requestParameters removeObjectsForKeys:keysToRemove];
        
        [outputDict setObjectCheckNil:@[
                                        @{@"params": requestParameters,
                                          @"output": responseData}]
                               forKey:@"mockup"];
        
        DDLogInfo(@"Response: %@", [outputDict JSONRepresentation]);
        NSFileManager *fileManager = [NSFileManager defaultManager];
        self.counter ++;
        NSURL *documentPath = [[fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
        NSURL *fileUrl = [documentPath URLByAppendingPathComponent:
                          [NSString stringWithFormat:@"%@_%lld.txt", self.sessionPrefix, self.counter]];
        NSString *filePath = [[fileUrl absoluteString] stringByReplacingOccurrencesOfString:@"file://" withString:@""];
        DDLogInfo(@"Write to file %@", filePath);
        [fileManager createFileAtPath:filePath
                             contents:[[outputDict JSONRepresentation] dataUsingEncoding:NSUTF8StringEncoding]
                           attributes:nil];
    } @catch (NSException *exception) {
        DDLogError(@"Exception: %@", exception);
    }
}

- (void)loadNetworkMockup:(NSString *)filePath {
    NSDictionary *mockupData = [self readJSONFromFile:filePath];
    if (![mockupData isKindOfClass:[NSDictionary class]]) {
        DDLogInfo(@"Network mockup is not valid JSON object");
        return;
    }
    
    NSMutableDictionary *networkMockup = [NSMutableDictionary dictionary];
    NSArray *apis = [mockupData arrayForKey:@"apis"];
    for (NSDictionary *item in apis) {
        NSString *keyPath = [item stringForKey:@"path"];
        NSArray *allMockups = [item arrayForKey:@"mockup"];
        
        [networkMockup setObject:allMockups forKey:keyPath];
    }
    DDLogInfo(@"Load network mocking: %@", networkMockup);
    self.networkMockup = networkMockup;
}

- (void)updateMockupData {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *documentPath = [[fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    NSURL *fileUrl = [documentPath URLByAppendingPathComponent:@"mockupdata.json"];
    NSString *filePath = [self filePathFromURL:fileUrl];
    NSDictionary *mockupData = [self readJSONFromFile:filePath];
    if (![mockupData isKindOfClass:[NSDictionary class]]) {
        DDLogInfo(@"Mockupdata is not valid JSON object");
        return;
    }
    
    NSString *networkFilePath = [mockupData stringForKey:@"network"];
    fileUrl = [documentPath URLByAppendingPathComponent:networkFilePath];
    [self loadNetworkMockup:[self filePathFromURL:fileUrl]];
}

- (NSString *)filePathFromURL:(NSURL *)url {
    return [[url absoluteString] stringByReplacingOccurrencesOfString:@"file://" withString:@""];
}

- (NSDictionary *)readJSONFromFile:(NSString *)filePath {
    NSString *string = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    NSData * data = [string dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *mockupData = [data JSONValue];
    return mockupData;
}

@end
