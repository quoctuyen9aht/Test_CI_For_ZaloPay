//
//  ZPPlaceholderView.swift
//  ZaloPay
//
//  Created by Huu Hoa Nguyen on 9/27/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import UIKit
import ZaloPayShareControlObjc

@objc public protocol ZPPlaceHolderViewProtocol {
    func setupViewErrorLoad()
    func setupViewNoNetwork()
    func addRefreshTarget(_ target: Any!, action: Selector!, for controlEvents: UIControlEvents)
}

public class ZPPlaceHolderView : UIView, ZPPlaceHolderViewProtocol {
    public var imageView: UIImageView!
    public var labelTitle: UILabel!
    public var buttonRetry: UIButton!
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initializeView() {
        setupImageView()
        setupLabelTitle()
        setupButtonRetry()
    }
    
    func setupImageView() {
        imageView = UIImageView()
        self.addSubview(imageView)
        imageView.snp.makeConstraints { (make) in
            make.width.equalTo(240)
            make.height.equalTo(128)
            make.top.equalTo(90)
            make.centerX.equalToSuperview()
        }
    }
    
    func setupLabelTitle() {
        labelTitle = UILabel().build {
            $0.textAlignment = .center
            $0.font = UIFont.sfuiTextRegular(withSize: 17)
            $0.textColor = UIColor.subText()
            self.addSubview($0)
        }
        labelTitle.snp.makeConstraints { (make) in
            make.height.equalTo(30)
            make.centerX.equalToSuperview()
            make.top.equalTo(self.imageView.snp.bottom).offset(16)
        }
    }
    
    func setupButtonRetry() {
        buttonRetry = UIButton().build {
            $0.setupZaloPay()
            self.addSubview($0)
        }
        buttonRetry.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.width.equalTo(180)
            make.height.equalTo(44)
            make.top.equalTo(labelTitle.snp.bottom).offset(46)
        }
    }
    
    public func setupViewNoNetwork() {
        imageView.image = UIImage(named: "ico_noconnect")
        buttonRetry.setTitle(R.string_HomeReactNative_DownloadAgain(), for: .normal)
        labelTitle.text = R.string_NetworkError_NoConnectionMessage()
        labelTitle.textColor = UIColor.hex_0xacb3ba()
        let size = imageView.sizeThatFits(CGSize.zero)
        imageView.snp.makeConstraints ({ (make) in
            make.width.equalTo(size.width)
            make.height.equalTo(size.height)
        })
    }
    
    public func setupViewErrorLoad() {
        imageView.stopAnimating()
        imageView.image = UIImage(named: "ico_noload")
        buttonRetry.setTitle(R.string_HomeReactNative_DownloadAgain(), for: .normal)
        labelTitle.text = R.string_FetchTransactionLogsFail()
        let size = imageView.sizeThatFits(CGSize.zero)
        imageView.snp.makeConstraints { (make) in
            make.width.equalTo(size.width)
            make.height.equalTo(size.height)
        }
    }
    
    public func addRefreshTarget(_ target: Any!, action: Selector!, for controlEvents: UIControlEvents) {
        buttonRetry.addTarget(target , action: action, for: controlEvents)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        buttonRetry.roundRect(Float(buttonRetry.bounds.size.height / 2))
    }
}
