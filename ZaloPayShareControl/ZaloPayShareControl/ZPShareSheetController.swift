//
//  ZPShareSheetController.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit
import ZaloPayAnalyticsSwift
import ZaloPayShareControlObjc
import ReactiveObjC

@objc public protocol ZPShareSheetControllerDelegate {
    @objc optional func processCloseShareSheet()
}

public class ZPShareSheetController: UIViewController, UIGestureRecognizerDelegate {
    public var items: [[ZPItemShareProtocol]] = []
    public var zpSheetConfigure: ZPShareSheetConfigure = ZPShareSheetConfigure.default()
    public var headerLabel: UILabel!
    public var cancelButton: UIButton!
    public var contentView: UIView!
    public var overlayBlurView: UIView!
    public var mainStackView: OAStackView!
    public var itemsStackView: OAStackView!
    public var lastPanGesturePoint = CGPoint.zero
    public var delegate: ZPShareSheetControllerDelegate?
    
    convenience public init(_ items: [[ZPItemShareProtocol]] = [],
                     configure: ZPShareSheetConfigure = ZPShareSheetConfigure.default()) {
        self.init(nibName: nil, bundle: nil)
        self.zpSheetConfigure = configure
        self.initializeConfigure()
    }
    
    public func initializeConfigure() {
        self.definesPresentationContext = true
        self.modalPresentationStyle = .overCurrentContext
    }
    
    public func initializeContentView() {
        contentView =  UIView()
        contentView.backgroundColor = self.zpSheetConfigure.actionSheetColor
        self.view.addSubview(contentView)
        
        
        contentView.snp.makeConstraints {(make) in
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(self.zpSheetConfigure.actionSheetHeight)
        }
        
        self.initialTitleHeaderLabel()
        self.initializeCancelButton()
        self.initializeMainStackView()
    }
    
    public func initialTitleHeaderLabel() {
        headerLabel = UILabel()
        headerLabel.text = zpSheetConfigure.titleHeader
        headerLabel.font = zpSheetConfigure.titleHeaderFont
        headerLabel.textColor = zpSheetConfigure.titleHeaderFontColor
        self.contentView.addSubview(headerLabel)
        
        headerLabel.snp.makeConstraints {(make) in
            make.top.equalToSuperview().offset(5)
            make.height.equalTo(zpSheetConfigure.headerHeight)
            make.centerX.equalToSuperview()
        }
        headerLabel.sizeToFit()
    }
    
    public func initializeCancelButton() {
        cancelButton = UIButton()
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        cancelButton.backgroundColor = zpSheetConfigure.cancelButtonColor
        cancelButton.setTitle(zpSheetConfigure.titleCancelButton, for: .normal)
        cancelButton.setTitleColor(zpSheetConfigure.titleCancelButtonFontColor, for: .normal)
        cancelButton.titleLabel?.font = zpSheetConfigure.titleCancelButtonFont
        self.contentView.addSubview(cancelButton)
        
        cancelButton.snp.makeConstraints {
            make in
            make.bottom.leading.trailing.equalToSuperview()
            make.height.equalTo(self.zpSheetConfigure.cancelButtonHeight)
        }
        cancelButton.addTarget(self, action: #selector(self.onTapCancelButton), for: .touchUpInside)
        
        let sepratorView: UIView = initializeSepatorView()
        cancelButton.addSubview(sepratorView)
        sepratorView.snp.makeConstraints({
            make in
            make.top.equalTo(0)
            make.width.equalTo(cancelButton.snp.width)
            make.height.equalTo(1.0)
        })
    }
    
    public func initializeMainStackView() {
        self.view.layoutIfNeeded()
        mainStackView = OAStackView()
        mainStackView.axis = .vertical
        self.contentView.addSubview(mainStackView)
        
        mainStackView.snp.makeConstraints {
            make in
            
            make.leading.trailing.equalTo(contentView)
            make.top.equalTo(headerLabel.snp.bottom).offset(5)
            make.bottom.equalTo(cancelButton.snp.top)
        }
        
        mainStackView.layoutIfNeeded()
        
        let heightNextElementScrollView: CGFloat = 118
        let heightFirstElementScrollView: CGFloat = mainStackView.frame.size.height - heightNextElementScrollView
        var scrollViewArray = [UIScrollView]()
        
        for i in 0..<items.count {
            
            let scrollViewItem: UIScrollView = initializeItemInScrollView(withData: items[i])
            scrollViewArray.append(scrollViewItem)
            mainStackView.addArrangedSubview(scrollViewItem)
            
            let heightElement: CGFloat = i == 0 ? heightFirstElementScrollView : heightNextElementScrollView
            
            scrollViewItem.snp.makeConstraints({
                make in
                make.width.equalTo(contentView.snp.width)
                make.height.equalTo(heightElement)
            })
            
            if i != items.count - 1 {
                let sepratorView: UIView = initializeSepatorView()
                mainStackView.addArrangedSubview(sepratorView)
                sepratorView.snp.makeConstraints({
                    make in
                    make.width.equalTo(contentView.snp.width)
                    make.height.equalTo(1.0)
                })
            }
        }
        
        self.view.layoutIfNeeded()
        for i in 0..<items.count {
            let scrollViewItem: UIScrollView = scrollViewArray[i]
            
            if scrollViewItem.subviews.count != 0 {
                scrollViewItem.layoutIfNeeded()
                let lastViewFrame: CGRect = scrollViewItem.subviews.last!.frame
                scrollViewItem.contentSize = CGSize(width: CGFloat((lastViewFrame.size.width + 18) * CGFloat(items[i].count)), height: 0)
            }
        }
    }
    
    func initializeItemInScrollView(withData dataInRow: [ZPItemShareProtocol]) -> UIScrollView {
        let scrollViewItem = UIScrollView()
        scrollViewItem.translatesAutoresizingMaskIntoConstraints = false
        scrollViewItem.showsHorizontalScrollIndicator = false
        var firstItem: UIView! = nil
        
        for i in 0..<dataInRow.count {
            let itemView: ZPItemSheetView = ZPItemSheetView()
            
            itemView.bindingUI(dataInRow[i])
            scrollViewItem.addSubview(itemView)
            if firstItem == nil {
                itemView.snp.makeConstraints {
                    make in
                    make.leading.equalToSuperview().offset(14)
                    make.height.centerY.equalToSuperview()
                    make.width.equalTo(64)
                }
            } else {
                itemView.snp.makeConstraints {
                    make in
                    make.leading.equalTo(firstItem.snp.trailing).offset(18)
                    make.top.bottom.width.equalTo(firstItem)
                }
            }
            
            firstItem = itemView
        }
        return scrollViewItem
    }
    
    public func initializeSepatorView() -> UIView {
        let sepratorView = UIView()
        sepratorView.backgroundColor = zpSheetConfigure.speratorColor
        return sepratorView
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        initializeContentView()
        animationShowColorEffect()
        view.backgroundColor = UIColor.clear
        let dismissTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissViewController))
        dismissTapGesture.delegate = self
        view.addGestureRecognizer(dismissTapGesture)
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.panGesture))
        panGesture.delegate = self
        view.addGestureRecognizer(panGesture)
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        animationHideColorEffect()
    }
    
    override public func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if let cancelBlock = zpSheetConfigure.cancelBlock {
            cancelBlock()
        }
    }
    
    @objc public func dismissViewController() {
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.promotion_detail_share_touch_close)
        dismiss(animated: true, completion: {
            //self.delegate?.processCloseShareSheet!()
        })
    }
    
    @objc public func onTapCancelButton() {
        ZPTrackingHelper.shared().trackEvent(ZPAnalyticEventAction.promotion_detail_share_touch_close)
        self.view.backgroundColor = UIColor.clear
        self.dismiss(animated: true, completion: {
            //self.delegate?.processCloseShareSheet!()
        })
    }
    
    public func animationShowColorEffect() {
        let presentingVC: UIViewController? = self.presentingViewController
        if let presentingVC = presentingVC {
            
            overlayBlurView = UIView()
            presentingVC.view.addSubview(overlayBlurView)
            overlayBlurView.backgroundColor = .clear
            
            overlayBlurView.snp.makeConstraints({
                make in
                make.edges.equalTo(presentingVC.view)
            })
            UIView.animate(withDuration: 0.3, animations: {() -> Void in
                self.overlayBlurView.backgroundColor = self.zpSheetConfigure.overlayColor
            })
        }
    }
    
    public func animationHideColorEffect() {
        if let overlayBlurView = overlayBlurView {
            UIView.animate(withDuration: 0.3, animations: {() -> Void in
                overlayBlurView.backgroundColor = UIColor.clear
            }, completion: {(_ finished: Bool) -> Void in
                overlayBlurView.removeFromSuperview()
            })
        }
    }
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        // Determine if the touch is inside the custom subview
        if touch.view == overlayBlurView || touch.view == view {
            return true
        }
        return false
    }
    
    @objc func panGesture(_ panGesture: UIPanGestureRecognizer) {
        let panGesturePoint: CGPoint = panGesture.translation(in: view)
        let offsetValue: CGFloat = panGesturePoint.y - lastPanGesturePoint.y
        if panGesture.state == .began {
            lastPanGesturePoint = panGesturePoint
        } else if panGesture.state == .changed {
            if offsetValue > 0.0 {
                contentView.snp.updateConstraints({ (make) in
                    make.bottom.equalToSuperview().offset(offsetValue)
                })
                self.view.layoutIfNeeded()
            }
        } else if offsetValue > contentView.frame.size.height / 2 {
            dismissViewController()
        } else {
            lastPanGesturePoint = CGPoint.zero
            contentView.snp.updateConstraints({ (make) in
                make.bottom.equalToSuperview()
            })
            UIView.animate(withDuration: 0.1, delay: 0.0, usingSpringWithDamping: 5.0, initialSpringVelocity: 5.0, options: .curveEaseInOut, animations: {() -> Void in
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
}
