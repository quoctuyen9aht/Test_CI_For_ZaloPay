//
//  ZPShareSheetItemView.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit
import ZaloPayShareControlObjc
import ZaloPayCommonSwift

protocol ZPItemShareViewProtocol: NSObjectProtocol {
    var itemProtcol: ZPItemShareProtocol? { get set }
    func bindingUI(_ item: ZPItemShareProtocol)
}

class ZPItemSheetView: UIView {
    var iconStringLabel: UILabel!
    var iconImageView: UIImageView!
    var titleLabel: UILabel!
    weak var itemProtcol: ZPItemShareProtocol?
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        intializeView()
    }
    
    func intializeView() {
        if let _ = iconStringLabel,
            let _ = iconImageView,
            let _ = titleLabel {
            return
        }
        
        // Create background white color
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.white
        backgroundView.layer.cornerRadius = 12
        backgroundView.clipsToBounds = true
        self.addSubview(backgroundView)
        self.layoutIfNeeded()
        
        backgroundView.snp.makeConstraints { (make) in
            make.trailing.equalToSuperview()
            make.width.equalTo(64)
            make.height.equalTo(60)
            make.top.equalToSuperview().offset(18)
        }
        
        self.setupIconString()
        self.iconStringLabel.snp.makeConstraints { (make) in
            make.top.bottom.equalTo(backgroundView)
            make.leading.equalTo(backgroundView).offset(2)
            make.trailing.equalTo(backgroundView).offset(-2)
        }
        
        self.setupImageView()
        self.iconImageView.snp.makeConstraints { (make) in
            make.top.leading.equalTo(backgroundView).offset(10)
            make.bottom.trailing.equalTo(backgroundView).offset(-10)
        }
        
        self.setupTitleLabel()
        titleLabel.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(backgroundView.snp.bottom).offset(8)
        }
        
        titleLabel.isHidden = true
        iconImageView.isHidden = true
        iconStringLabel.isHidden = true
        
        self.backgroundColor = .clear
        self.clipsToBounds = true
        
        self.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapItemAction))
        self.addGestureRecognizer(tapGesture)
    }
    
    func setupIconString() {
        if iconStringLabel != nil {
            return
        }

        iconStringLabel = UILabel().build {
            $0.textAlignment = .center
            $0.font = UIFont.zaloPay(withSize: 30)
            $0.textColor = UIColor.hex_0x686871()
            $0.adjustsFontSizeToFitWidth = true
            self.addSubview($0)
        }
    }
    
    func setupImageView() {
        if iconImageView != nil {
            return
        }
        
        iconImageView = UIImageView().build {
            $0.backgroundColor = UIColor.clear
            $0.contentMode = .scaleAspectFit
            $0.clipsToBounds = true
            self.addSubview($0)
        }
    }
    
    func setupTitleLabel() {
        if titleLabel != nil {
            return
        }
        
        titleLabel = UILabel().build({
            $0.textAlignment = .center
            $0.numberOfLines = 2
            $0.font = UIFont.sfuiTextRegular(withSize: 12)
            $0.textColor = UIColor.hex_0x686871()
            self.addSubview($0)
        })
    }
    
    @objc func onTapItemAction(_ tapGesture: UITapGestureRecognizer) {
        if let itemProtcol = itemProtcol {
            let action = itemProtcol.actionBlock()
            action?(itemProtcol)
        }
    }
}

extension ZPItemSheetView: ZPItemShareViewProtocol {
    func bindingUI(_ item: ZPItemShareProtocol) {
        self.itemProtcol = item
        
        titleLabel.isHidden = false
        titleLabel.text = item.titleItem()
        titleLabel.sizeToFit()
        
        iconStringLabel.isHidden = false
        if let font = item.iconFont() {
            iconStringLabel.setIconfont(font)
        } else {
            iconImageView.isHidden = false
            iconImageView.image = item.primaryIconImage()
        }
    }
}
