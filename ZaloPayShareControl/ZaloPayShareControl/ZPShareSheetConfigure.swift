//
//  ZPShareSheetConfigure.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayShareControlObjc

public typealias ShareActionSheetCancelBlock = () -> Void

public class ZPShareSheetConfigure: NSObject {
    
    public var cancelBlock: ShareActionSheetCancelBlock?
    public var actionSheetHeight: CGFloat = 0.0
    public var cancelButtonHeight: CGFloat = 0.0
    public var minimumInteritemSpacing: CGFloat = 0.0
    public var headerHeight: CGFloat = 0.0
    public var overlayColor: UIColor?
    public var actionSheetColor: UIColor?
    public var titleHeader: String = ""
    public var titleHeaderFont: UIFont?
    public var titleHeaderFontColor: UIColor?
    public var titleCancelButton: String = ""
    public var titleCancelButtonFontColor: UIColor?
    public var titleCancelButtonFont: UIFont?
    public var cancelButtonColor: UIColor?
    public var speratorColor: UIColor?
    
    public class func `default`() -> ZPShareSheetConfigure {
        let configure = ZPShareSheetConfigure()
        configure.cancelButtonHeight = 54
        configure.actionSheetHeight = 342
        configure.headerHeight = 40
        configure.minimumInteritemSpacing = 18
        configure.overlayColor = UIColor.black.withAlphaComponent(0.4)
        configure.actionSheetColor = UIColor(hexValue: 0xf2f2f2)
        configure.titleHeaderFont = UIFont.sfuiTextRegular(withSize: 14)
        configure.titleHeaderFontColor = UIColor.hex_0x686871()
        configure.titleCancelButton = "Hủy"
        configure.cancelButtonColor = .white
        configure.titleCancelButtonFontColor = UIColor.hex_0x686871()
        configure.titleCancelButtonFont = UIFont.sfuiTextRegular(withSize: 18)
        configure.speratorColor = UIColor(hexValue: 0xd7d7d7)
        return configure
    }
    
    public class func zalopay() -> ZPShareSheetConfigure {
        let configure = `default`()
        configure.actionSheetHeight = 250
        return configure
    }
}
