//
//  ZPShareSheetItemView.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/24/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit

protocol ZPShareItemViewProtocol: NSObjectProtocol {
    var itemProtcol: ZPShareItemProtocol? { get set }
    
    func bindingUI(_ item: ZPShareItemProtocol?)
}

class ZPShareSheetItemView: UIView {
    
    var iconStringLabel: UILabel!
    var iconImageView: UIImageView!
    var titleLabel: UILabel!

    weak var itemProtcol: ZPShareItemProtocol?
    
    convenience init() {
        self.init(frame: .zero)
        
        intializeView()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        intializeView()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        intializeView()
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        intializeView()
    }

    func intializeView() {

        if let iconStringLabel = iconStringLabel,
           let iconImageView = iconImageView,
           let titleLabel = titleLabel {
            return
        }
        
        // Create background white color
        var backgroundView = UIView()
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        backgroundView.backgroundColor = UIColor.white
        backgroundView.layer.cornerRadius = 12
        backgroundView.clipsToBounds = true
        addSubview(backgroundView)
    }
}

extension ZPShareSheetItemView: ZPShareItemViewProtocol {

    func bindingUI(_ item: ZPShareItemProtocol?) {
        
    }
    
}
