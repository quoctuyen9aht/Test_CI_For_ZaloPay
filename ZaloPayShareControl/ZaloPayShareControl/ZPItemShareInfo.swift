//
//  ZPItemShareInfo.swift
//  ZaloPay
//
//  Created by thanhqhc on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import UIKit


public typealias ShareItemActionBlock = ((_ item: ZPItemShareProtocol) -> Void)
public protocol ZPItemShareProtocol: NSObjectProtocol {
    
    func iconFont() -> String?

    func titleItem() -> String?
    func primaryIconImage() -> UIImage?
    
    func actionBlock() -> ShareItemActionBlock?
}

public class ZPItemShareModel: NSObject {
    
    public var titleShare: String!
    public var iconFontString: String?
    public var iconImage: UIImage?
    public var tapAction: ShareItemActionBlock?
    
    public init(title: String, iconString: String? = nil, iconImage: UIImage? = nil, tapAction: ShareItemActionBlock? = nil) {
        super.init()
        
        self.titleShare = title
        self.iconImage = iconImage
        self.iconFontString = iconString
        self.tapAction = tapAction
    }
}

extension ZPItemShareModel: ZPItemShareProtocol {
    
    public func iconFont() -> String? {
        return iconFontString
    }
    
    public func titleItem() -> String? {
        return titleShare
    }
    
    public func primaryIconImage() -> UIImage? {
        return iconImage
    }
    
    public func actionBlock() -> ShareItemActionBlock? {
        return tapAction
    }
}
