//
//  ReceiveLixiObject.h
//  ZaloPayRedPacket
//
//  Created by bonnpv on 7/23/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReceiveLixiObject : NSObject
@property (nonatomic) uint64_t packetId;
@property (nonatomic) uint64_t bundleId;
@property (nonatomic, strong) NSString *senderName;
@property (nonatomic, strong) NSString *senderAvatar;
@property (nonatomic, strong) NSString *message;
@property (nonatomic) int amount;
@property (nonatomic) int status;
@property (nonatomic) NSString *statusMessage;
@property (nonatomic) uint64_t mtaid;
@property (nonatomic) uint64_t mtuid;
@end
