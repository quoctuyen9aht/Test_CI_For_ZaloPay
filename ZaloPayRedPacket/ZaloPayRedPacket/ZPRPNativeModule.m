//
//  ZPRPNativeModule.m
//  ZaloPayAppManager
//
//  Created by bonnpv on 7/14/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPRPNativeModule.h"
#import "ZPRPLog.h"
#import "ZPRPManager.h"
#import "ZPRPExternalProtocols.h"
#import "PacketData.h"
#import "ZPRPCommon.h"
#import "ReceiveLixiObject.h"

#import <ZaloPayCommon/NSCollection+JSON.h>
#import <ZaloPayUI/ZPAppFactory.h>
#import <ZaloPayNetwork/NSError+ZaloPayAPI.h>
#import <ZaloPayProfile/ZaloPayProfile-Swift.h>
#import <ZaloPayProfile/ZPContact.h>
#import <ReactiveObjC/ReactiveObjC.h>
#import <ReactiveObjC/RACSignal.h>

@implementation ZPRPNativeModule
RCT_EXPORT_MODULE(ZaloPayRedPacketApi)

RCT_EXPORT_METHOD(createRedPacketBundleOrder:(int)quantity
                  totalLuck:(NSInteger)totalLuck
                  amountEach:(NSInteger)amountEach
                  type:(int)type
                  sendMessage:(NSString *)sendMessage
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
    
    @weakify(self);
    [[[[ZPAppFactory sharedInstance] rpManager].networkApi createRedPacketBundleOrder:(int)quantity
                                                    totalluck:(long)totalLuck
                                                   amounteach:(long)amountEach
                                                         type:(int)type
                                                  sendmessage:(NSString *)sendMessage] subscribeNext:^(NSDictionary* bill) {
        @strongify(self);
        [self payOrder:bill
              resolver:resolve
              rejecter:reject];
    } error:^(NSError *error) {
        NSDictionary *response = @{@"code":@(error.code), @"message":[error apiErrorMessage]};
        resolve(response);
    }];
}

RCT_EXPORT_METHOD(submitToSendBundle:(NSString *)bundleId
                  toFriends:(NSArray *)zaloIds
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
    
    NSArray *listUser = [[ZPProfileManager shareInstance] lixiZaloFriendInfoWithIds: zaloIds];
    ZaloUserSwift *userInfo = [ZPProfileManager shareInstance].currentZaloUser;
    NSString *zaloPayId = [ZPProfileManager shareInstance].userLoginData.paymentUserId;
    
    NSMutableDictionary *infoDic = [NSMutableDictionary dictionary];
    [infoDic setObjectCheckNil:zaloPayId forKey:@"zaloPayID"];
    [infoDic setObjectCheckNil:userInfo.userId forKey:@"zaloID"];
    [infoDic setObjectCheckNil:userInfo.displayName forKey:@"zaloName"];
    [infoDic setObjectCheckNil:userInfo.avatar forKey:@"avatar"];
    
    NSString *jsonString = [listUser JSONRepresentation];
    NSString *userInfoString = [infoDic JSONRepresentation];
    
    DDLogInfo(@"user info : %@",userInfoString);
    DDLogInfo(@"friend info : %@",jsonString);
    
    [[[[ZPAppFactory sharedInstance] rpManager].networkApi submitToSendBundle:bundleId
                                     toZaloPayFriends:jsonString
                                      currentUserInfo:userInfoString]
     subscribeNext:^(NSDictionary* result) {
         if (resolve) {
             NSDictionary *response = @{@"code":@(1), @"data":result};
             resolve(response);
         }
     } error:^(NSError *error) {
         if (resolve) {
             NSDictionary *response = @{@"code":@(-1), @"message":[error apiErrorMessage]};
             resolve(response);
         }
     }];
}

RCT_EXPORT_METHOD(openPacket:(NSString *)packageId
                  bundleId:(NSString *)bundleId
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
    
    RACSignal* signal = [[[ZPAppFactory sharedInstance] rpManager].networkApi openPacket:packageId
                                                        bundleId:bundleId];
    [[signal flattenMap:^__kindof RACSignal * _Nullable(NSDictionary  *_Nullable response) {
        uint64_t zptransid = [[response objectForKey:@"zptransid"] longLongValue];
        return [[ZPAppFactory sharedInstance].rpManager requestStatus:zptransid
                                                             packageid:[packageId longLongValue]
                                                               timeout:30.0];
        
    }] subscribeNext:^(NSDictionary *result) {
        if (resolve) {
            NSDictionary *response = @{@"code":@(1), @"data":result};
            resolve(response);
        }
    } error:^(NSError *error) {
        
        if ([error isInvalidPacketError]) {
            [[[ZPAppFactory sharedInstance] rpManager].dbApi openPacket:[packageId longLongValue]
                                                  status:(int)error.code
                                                 message:[error apiErrorMessage]];
        }
        
        if (resolve) {
            NSDictionary *response = @{@"code":@(error.code), @"message":[error apiErrorMessage]};
            resolve(response);
        }
    }];
}

RCT_REMAP_METHOD(getAllFriend,
                 resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject) {
    
    if ([self resolverZaloFriendsFomCache:resolve forceResolve:false]) {
        return;
    }
    [[[ZPProfileManager shareInstance] loadZaloUserFriendListWithForceSyncContact:false] subscribeCompleted:^{
        [self resolverZaloFriendsFomCache:resolve forceResolve:true];
    }];
}

RCT_EXPORT_METHOD(getPacketStatus:(NSString *)packetId
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
    if (resolve) {
        ReceiveLixiObject *lixi = [[[ZPAppFactory sharedInstance] rpManager].dbApi receiveLixiObjectWithPacketId:[packetId longLongValue]];
        NSMutableDictionary *response = [NSMutableDictionary dictionary];
        [response setObjectCheckNil:@(lixi.status) forKey:@"code"];
        [response setObjectCheckNil:lixi.statusMessage forKey:@"message"];
        resolve(response);
    }
}

RCT_EXPORT_METHOD(getReceivedPacket:(NSString *)packetId
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
    
    if (resolve) {
        ReceiveLixiObject *lixi = [[[ZPAppFactory sharedInstance] rpManager].dbApi receiveLixiObjectWithPacketId:[packetId longLongValue]];
        NSMutableDictionary *_return = [NSMutableDictionary dictionary];
        [_return setObjectCheckNil:lixi.senderName forKey:@"sendername"];
        [_return setObjectCheckNil:lixi.message forKey:@"message"];
        [_return setObjectCheckNil:lixi.senderAvatar forKey:@"senderavatar"];
        [_return setObjectCheckNil:@(lixi.amount) forKey:@"amount"];
        [_return setObject:@(lixi.bundleId) forKey:@"bundleid"];
        resolve(_return);
    }
}

RCT_REMAP_METHOD(getCurrentUserInfo,
                 cresolver:(RCTPromiseResolveBlock)resolve
                 crejecter:(RCTPromiseRejectBlock)reject) {
    
    NSString *zaloPayId = [ZPProfileManager shareInstance].userLoginData.paymentUserId;
    NSString *phone = [ZPProfileManager shareInstance].userLoginData.phoneNumber;
    ZaloUserSwift *user = [ZPProfileManager shareInstance].currentZaloUser;
    NSString *avatar = user.avatar;
    if (avatar.length == 0) {
        avatar = @"";
    }
    NSString *name = user.displayName;
    if (name.length == 0) {
        name = @"";
    }
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setObject:avatar forKey:@"avatar"];
    [data setObject:name forKey:@"displayname"];
    [data setObjectCheckNil:phone forKey:@"phonenumber"];
    [data setObjectCheckNil:user.userId forKey:@"zaloid"];
    [data setObjectCheckNil:zaloPayId forKey:@"zalopayid"];
    resolve(@{@"code":@(1),@"data":data});
}

    
RCT_EXPORT_METHOD(launchContactList:(NSString *)navigationTitle
                  backgroundColor:(NSString *)backgroundColor
                  selectedArray:(NSArray *)selectedArray
                  maxUsersSelected:(int)maxUsersSelected
                  mode:(int)mode
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[ZPAppFactory sharedInstance] launchContactList:navigationTitle backgroundColor:backgroundColor selectedArray:selectedArray maxUsersSelected:maxUsersSelected mode:mode completeHandle:^(NSDictionary *data) {
            if (resolve)
            {
                resolve(data);
            }
            
        }];
    });
}
RCT_EXPORT_METHOD(getContactInfo:(NSArray *)phoneNumbers
                 resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject) {
    [self resolverGetContactInfo:phoneNumbers resolve:resolve];
}
RCT_EXPORT_METHOD(getPacketFromNotifications:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject){
    [self resolverGetPacketFromNotifications:resolve];
}
#pragma mark - Friend

- (BOOL)resolverZaloFriendsFomCache:(RCTPromiseResolveBlock)resolve forceResolve:(BOOL)forceResolve{
    NSArray *friends = [[ZPProfileManager shareInstance] getZaloFriendListFromDB];
    if (friends.count == 0) {
        if (forceResolve && resolve != nil) {
            NSDictionary *response = @{@"code":@(1), @"data":@[]};
            resolve(response);
        }
        return  FALSE;
    }
    friends = [friends sortedArrayUsingSelector:@selector(compareByDisplayValue:)];
    NSMutableArray *result = [NSMutableArray array];
    for (int i = 0; i< friends.count; i++) {
        ZaloUserSwift *user = [friends objectAtIndex:i];
        NSDictionary *dict = [self userDictionary:user];
        [result addObject:dict];
    }
    if (resolve) {
        NSDictionary *response = @{@"code":@(1), @"data":result};
        resolve(response);
    }
    return TRUE;
}

- (NSDictionary *)userDictionary:(ZaloUserSwift *)user {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObjectCheckNil:user.displayName forKey:@"displayName"];
    [dict setObjectCheckNil:user.avatar forKey:@"avatar"];
    [dict setObjectCheckNil:user.userId forKey:@"userId"];
    [dict setObjectCheckNil:@(user.gender) forKey:@"userGender"];
    [dict setObjectCheckNil:@(user.usedZaloPay) forKey:@"usingApp"];
    [dict setObjectCheckNil:user.asciiDisplayName forKey:@"ascciDisplayName"];
    [dict setObjectCheckNil:user.zaloPayId forKey:@"zaloPayId"];
    return dict;
}
- (NSDictionary *)userDictionaryShort:(ZPContactSwift *)user {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObjectCheckNil:user.displayName forKey:@"contactname"];
    [dict setObjectCheckNil:user.phoneNumber forKey:@"phonenumber"];
    return dict;
}

- (void)resolverGetContactInfo:(NSArray*)phoneNumbers resolve:(RCTPromiseResolveBlock)resolve{
    NSArray *friends = [[ZPProfileManager shareInstance] getListContactInfoByPhone:phoneNumbers];
    if (friends.count == 0) {
        if (resolve) {
            NSDictionary *response = @{@"code":@(1), @"data":@[]};
            resolve(response);
        }
        return;
    }
    friends = [friends sortedArrayUsingSelector:@selector(compareByDisplayValue:)];
    NSMutableArray *result = [NSMutableArray array];
    for (int i = 0; i< friends.count; i++) {
        ZPContactSwift * user = [friends objectAtIndex:i];
        NSDictionary *dict = [self userDictionaryShort:user];
        [result addObject:dict];
    }
    if (resolve) {
        NSDictionary *response = @{@"code":@(1), @"data":result};
        resolve(response);
    }
}
- (void)resolverGetPacketFromNotifications:(RCTPromiseResolveBlock)resolve{
    NSArray *listLixi = [[[ZPAppFactory sharedInstance] rpManager].dbApi getListCanOpenPacket];
    if (listLixi.count == 0) {
        if (resolve) {
            NSDictionary *response = @{@"code":@(1), @"data":@[]};
            resolve(response);
        }
        return;
    }
    if (resolve) {
        NSMutableArray *arrMTaid = [NSMutableArray new];
        NSMutableArray *arrMTuid = [NSMutableArray new];
        for (ReceiveLixiObject *lixi in listLixi) {
            [arrMTaid addObject:[NSString stringWithFormat:@"%llu",lixi.mtaid]];
            [arrMTuid addObject:[NSString stringWithFormat:@"%llu",lixi.mtuid]];
        }
        NSDictionary *response = @{@"code":@(1), @"data":[[ZPAppFactory sharedInstance] getListNotificationWithLixi:arrMTaid arrMtuid:arrMTuid]};
        resolve(response);
    }
}
#pragma mark - Pay Order

- (void)payOrder:(NSDictionary *)param
        resolver:(RCTPromiseResolveBlock)resolve
        rejecter:(RCTPromiseRejectBlock)reject {
    
    DDLogInfo(@"about to process payment with payOrder :%@", param);
    
    NSInteger appId  = [param intForKey:@"appid"];
    if(param.count == 0 || appId == 0) {
        NSDictionary *response = @{@"code":@(2), @"data":@""};
        if (resolve) {
            resolve(response);
        }
        return;
    }
    
    UINavigationController *nav = [[ZPAppFactory sharedInstance] rootNavigation];
    if (![nav isKindOfClass:[UINavigationController class]]) {
        DDLogError(@"The rootViewController is not kind of UINavigationController. Something must be wrong");
        if (resolve) {
            NSDictionary *response = @{@"code":@(4), @"data":@""};
            resolve(response);
        }
        return;
    }
    __weak UIViewController *startViewController = nav.topViewController;
    __weak UINavigationController *weakNavi = nav;
    NSString *bundleId = [NSString stringWithFormat:@"%@",[param objectForKey:@"bundleid"]];
    
    @weakify(self);
    void (^successBlock)(NSDictionary *data) = ^(NSDictionary *data) {
        @strongify(self);
        NSDictionary *response = @{@"code":@(1), @"data":@{@"bundleid":bundleId}};
        if (resolve) {
            resolve(response);
        }
        [self navigationController:weakNavi popToViewController:startViewController];
    };
    
    void (^errorBlock)(NSDictionary *data) = ^(NSDictionary *data) {
        @strongify(self);
        if (resolve) {
            NSDictionary *response = @{@"code":@(-1), @"data":@""};
            resolve(response);
        }
        [self navigationController:weakNavi popToViewController:startViewController];
    };
    
    void (^cancelBlock)(NSDictionary *data) = ^(NSDictionary *data) {
        @strongify(self);
        if (resolve) {
            NSDictionary *response = @{@"code":@(4), @"data":@""};
            resolve(response);
        }
      [self navigationController:weakNavi popToViewController:startViewController];
    };
    
    [[ZPAppFactory sharedInstance] showZaloPayGateway:startViewController
                                        completeBlock:successBlock
                                          cancelBlock:cancelBlock
                                           errorBlock:errorBlock
                                                appId:appId
                                             billData:param];
}

- (void)navigationController:(UINavigationController *)navi popToViewController:(UIViewController *)viewController{
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([navi isKindOfClass:[UINavigationController class]]) {
            [navi popToViewController:viewController animated:YES];
        }
    });
}

//- (void)popToViewController:(UIViewController *)viewController {
//    dispatch_async(dispatch_get_main_queue(), ^{
//        UINavigationController *navi = [[ZPAppFactory sharedInstance] rootNavigation];
//        if ([navi isKindOfClass:[UINavigationController class]] == false) {
//            return;
//        }
//        [navi popToViewController:viewController animated:YES];
//    });
//}

@end
