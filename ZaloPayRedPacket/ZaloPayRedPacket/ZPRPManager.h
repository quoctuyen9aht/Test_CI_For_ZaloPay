//
//  ZPRPManager.h
//  ZaloPayRedPacket
//
//  Created by bonnpv on 7/22/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
@class RACSignal;
@protocol ZPRPNetwork;
@protocol ZPRPDatabase;

@interface ZPRPManager : NSObject

- (instancetype)initWithNetwork:(id<ZPRPNetwork>)networkApi db:(id<ZPRPDatabase>)dbApi;


@property (strong, nonatomic, readonly) id<ZPRPDatabase> dbApi;
@property (strong, nonatomic, readonly) id<ZPRPNetwork> networkApi;

- (RACSignal *)requestStatus:(uint64_t)transid packageid:(uint64_t)packageid timeout:(float)timout;
- (void)updateLixiPacketStatus;

@end
