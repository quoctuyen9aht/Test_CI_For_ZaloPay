//
//  ZPRPNetwork.h
//  ZaloPayRedPacket
//
//  Created by bonnpv on 7/13/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <ZaloPayNetwork/NetworkManager.h>
#import "ZPRPExternalProtocols.h"

@interface ZPRPNetwork : NetworkManager <ZPRPNetwork>
@end
