//
//  ZPRPNativeModule.h
//  ZaloPayAppManager
//
//  Created by bonnpv on 7/14/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface ZPRPNativeModule : NSObject<RCTBridgeModule>

@end

