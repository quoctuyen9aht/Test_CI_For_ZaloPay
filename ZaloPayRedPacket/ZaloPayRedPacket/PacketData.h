//
//  PacketData.h
//  ZaloPayRedPacket
//
//  Created by bonnpv on 7/23/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PacketData : NSObject
@property (nonatomic) int amount;
@property (nonatomic) BOOL isLuckiest;
@property (nonatomic, strong) NSString *revAvatar;
@property (nonatomic, strong) NSString *revName;
@property (nonatomic, strong) NSString* sendMessage;
@property (nonatomic) uint64_t openTime;
@end
