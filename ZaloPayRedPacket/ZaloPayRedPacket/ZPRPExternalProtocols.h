//
//  ZPRPExternalProtocols.h
//  ZaloPayRedPacket
//
//  Created by Huu Hoa Nguyen on 8/18/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#ifndef ZPRPExternalProtocols_h
#define ZPRPExternalProtocols_h

#import <Foundation/Foundation.h>

@class RACSignal;

/**
 * Define list of external protocols that need to provide for RedPacket to work
 */


@protocol ZPRPNetwork <NSObject>
- (void)registerPrehandleError:(void (^)(int errorCode, NSString *message))handle;
- (RACSignal *)getPacketStatusWithPackageId:(uint64_t)packageId
                                    transId:(uint64_t)transId;

- (RACSignal *)createRedPacketBundleOrder:(int)quantity
                                totalluck:(long)totalluck
                               amounteach:(long)amounteach
                                     type:(int)type
                              sendmessage:(NSString *)sendmessage;

//- (RACSignal *)submitToSendBundle:(NSString *)bundleId toFriends:(NSArray *)zaloIds;

- (RACSignal *)submitToSendBundle:(NSString *)bundleId
                 toZaloPayFriends:(NSString *)zaloFriendInfo
                  currentUserInfo:(NSString *)info;

- (RACSignal *)openPacket:(NSString *)packageId bundleId:(NSString *)bundleId;

//- (RACSignal *)getBundleStatus:(uint64_t)bundleId
//                       appUser:(NSString *)appUser;

- (RACSignal *)getSendHistoryWithCount:(int)count timestamp:(uint64_t)timestamp order:(int)order;

- (RACSignal *)getReceivePackageWithCount:(int)count timestamp:(uint64_t)timestamp order:(int)order;

- (RACSignal *)getPackageInBundle:(uint64_t )bundleid
                            count:(int)count
                        timestamp:(uint64_t)timestamp
                            order:(int)order;
//- (RACSignal *)getRedPacketAppInfo:(NSString *)checksum;

- (RACSignal *)getBundleStatus:(uint64_t)bundleId;
- (RACSignal *)getListLixiStatus:(NSArray *)listPacketIds;

@end


@class ReceiveLixiObject;
@class FMDatabase;

@protocol ZPRPDatabase <NSObject>
- (void)saveLixiNotificationDataWithPackageId:(uint64_t)packageid
                                     bundleId:(uint64_t)bundleId
                                 senderAvatar:(NSString *)avatar
                                   senderName:(NSString *)name
                                      message:(NSString *)messge
                                       amount:(int)amount
                                       status:(int)status
                                 overrideData:(BOOL)shouldOverride
                                        mtaid:(uint64_t)mtaid
                                        mtuid:(uint64_t)mtuid;
- (void)openPacket:(uint64_t)packetId amount:(int)amount status:(int)status message:(NSString *)message;
- (void)openPacket:(uint64_t)packetId status:(int)status message:(NSString *)message;
- (ReceiveLixiObject *)receiveLixiObjectWithPacketId:(uint64_t)packetId;
- (NSArray *)getAllAvailablePacket;
- (NSArray *)getListCanOpenPacket;
@end

#endif /* ZPRPExternalProtocols_h */
