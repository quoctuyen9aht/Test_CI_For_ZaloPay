//
//  ZPRPDatabase.m
//  ZaloPayRedPacket
//
//  Created by bonnpv on 7/23/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPRPDatabase.h"
#import <fmdb/FMDatabase.h>
#import <fmdb/FMDatabaseQueue.h>
#import <ZaloPayCommon/NSCollection+JSON.h>
#import "ZPRPCommon.h"

#define kLixiNotify         @"LixiNotify"

#define kPackageId          @"packageid"
#define kBundleId           @"bundleid"
#define kSenderAvatar       @"senderavatar"
#define kSenderName         @"sendername"
#define kMessage            @"message"
#define kAmount             @"amount"
#define kStatus             @"status"
#define kErrorMessage       @"errormessage"
#define kMtaid              @"mtaid"
#define kMtuid              @"mtuid"


@implementation ZPLixiNotificationTable

+ (NSArray<NSString*>*) creatingTableQuery {
    NSString* tableLixiNotify = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@(%@ long primary key, %@ long, %@ text, %@ text, %@ text, %@ int,%@ int, %@ text, %@ long, %@ long);",kLixiNotify,kPackageId,kBundleId,kSenderAvatar,kSenderName,kMessage, kAmount, kStatus, kErrorMessage,kMtaid, kMtuid];
    return @[tableLixiNotify];
}

+ (NSArray<NSString*>*) tableNamesCanBeDropped {
    return @[kLixiNotify];
}

- (void)saveLixiNotificationDataWithPackageId:(uint64_t)packageid
                                     bundleId:(uint64_t)bundleId
                                 senderAvatar:(NSString *)avatar
                                   senderName:(NSString *)name
                                      message:(NSString *)messge
                                       amount:(int)amount
                                       status:(int)status
                                 overrideData:(BOOL)shouldOverride
                                        mtaid:(uint64_t)mtaid
                                        mtuid:(uint64_t)mtuid
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@(packageid) forKey:kPackageId];
    [params setObject:@(bundleId) forKey:kBundleId];
    [params setObjectCheckNil:avatar forKey:kSenderAvatar];
    [params setObjectCheckNil:name forKey:kSenderName];
    [params setObjectCheckNil:messge forKey:kMessage];
    [params setObject:@(amount) forKey:kAmount];
    [params setObject:@(status) forKey:kStatus];
    [params setObject:@(mtaid) forKey:kMtaid];
    [params setObject:@(mtuid) forKey:kMtuid];
    if (shouldOverride) {
        [self.db replaceToTable:kLixiNotify params:params];
    }else {
        [self.db insertToTable:kLixiNotify params:params];
    }
    
}

- (void)openPacket:(uint64_t)packetId amount:(int)amount status:(int)status message:(NSString *)message{
    NSString * query = [NSString stringWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ? WHERE %@ = ?",kLixiNotify,kAmount,kStatus,kErrorMessage, kPackageId];
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        [db executeUpdate:query,@(amount),@(status), message,@(packetId)];
    }];
}

- (void)openPacket:(uint64_t)packetId status:(int)status message:(NSString *)message {
    NSString * query = [NSString stringWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ? WHERE %@ = ?",kLixiNotify,kStatus,kErrorMessage, kPackageId];
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        [db executeUpdate:query,@(status), message,@(packetId)];
    }];
}

- (ReceiveLixiObject *)receiveLixiObjectWithPacketId:(uint64_t)packetId {
    ReceiveLixiObject *_return = [[ReceiveLixiObject alloc] init];
    
    NSString * query = [NSString stringWithFormat:@"SELECT * FROM %@ where %@ = ? ",kLixiNotify, kPackageId];
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:query ,@(packetId)];
        while ([rs next]) {
            NSString *name = [rs stringForColumn:kSenderName];
            NSString *message = [rs stringForColumn:kMessage];
            NSString *avatar = [rs stringForColumn:kSenderAvatar];
            NSString *statusMessage = [rs stringForColumn:kErrorMessage];
            int amount = [rs intForColumn:kAmount];
            int status = [rs intForColumn:kStatus];
            uint64_t bundleId = [rs longLongIntForColumn:kBundleId];
            
            _return.packetId = packetId;
            _return.bundleId = bundleId;
            _return.senderName = name;
            _return.senderAvatar = avatar;
            _return.message = message;
            _return.status = status;
            _return.amount = amount;
            _return.statusMessage = statusMessage;
            _return.mtaid = [rs longLongIntForColumn:kMtaid];
            _return.mtuid = [rs longLongIntForColumn:kMtuid];
            break;
        }
        [rs close];
    }];
    return _return;
}

- (NSArray *)getAllAvailablePacket {
    NSMutableArray *_return = [NSMutableArray array];
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString * query = [NSString stringWithFormat:@"SELECT * FROM %@ where %@ = ? ", kLixiNotify, kStatus];
        FMResultSet *rs = [db executeQuery:query, @(PacketStatusCanOpen)];
        while ([rs next]) {
            uint64_t packetId = [rs longLongIntForColumn:kPackageId];
            [_return addObject:@(packetId)];
        }
    }];
    return _return;
}

- (NSArray *)getListCanOpenPacket {
    NSMutableArray *_return = [NSMutableArray array];
    [self.db.dbQueue inDatabase:^(FMDatabase *db) {
        NSString * query = [NSString stringWithFormat:@"SELECT * FROM %@ where %@ = ? ", kLixiNotify, kStatus];
        FMResultSet *rs = [db executeQuery:query, @(PacketStatusCanOpen)];
        while ([rs next]) {
            ReceiveLixiObject *lixi = [[ReceiveLixiObject alloc] init];
            uint64_t packetId = [rs longLongIntForColumn:kPackageId];
            
            NSString *name = [rs stringForColumn:kSenderName];
            NSString *message = [rs stringForColumn:kMessage];
            NSString *avatar = [rs stringForColumn:kSenderAvatar];
            NSString *statusMessage = [rs stringForColumn:kErrorMessage];
            int amount = [rs intForColumn:kAmount];
            int status = [rs intForColumn:kStatus];
            uint64_t bundleId = [rs longLongIntForColumn:kBundleId];
            
            lixi.packetId = packetId;
            lixi.bundleId = bundleId;
            lixi.senderName = name;
            lixi.senderAvatar = avatar;
            lixi.message = message;
            lixi.status = status;
            lixi.amount = amount;
            lixi.statusMessage = statusMessage;
            lixi.mtaid = [rs longLongIntForColumn:kMtaid];
            lixi.mtuid = [rs longLongIntForColumn:kMtuid];
            
            [_return addObject:lixi];
        }
    }];
    return _return;
}

@end
