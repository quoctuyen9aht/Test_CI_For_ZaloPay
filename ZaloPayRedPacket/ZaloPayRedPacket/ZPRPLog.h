//
//  ZPRPLog.h
//  ZaloPayRedPacket
//
//  Created by bonnpv on 7/19/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <CocoaLumberjack/CocoaLumberjack.h>
extern const DDLogLevel zaloRedPackageLogLevel;
#define ddLogLevel  zaloRedPackageLogLevel
