//
//  ZPRPLog.m
//  ZaloPayRedPacket
//
//  Created by bonnpv on 7/19/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPRPLog.h"

const DDLogLevel zaloRedPackageLogLevel = DDLogLevelInfo;
