//
//  ZPRPNetwork.m
//  ZaloPayRedPacket
//
//  Created by bonnpv on 7/13/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPRPNetwork.h"
#import <ZaloPayCommon/NSCollection+JSON.h>
#import <AFNetworking/AFNetworking.h>
#import <ZaloPayCommon/ZPConstants.h>
#import <ZaloPayNetwork/ZPNetWorkApi.h>
#import <ZaloPayNetwork/NSError+ZaloPayAPI.h>
#import <ZaloPayNetwork/ZaloPayErrorCode.h>
#import <ReactiveObjC/ReactiveObjC.h>
@interface UIDevice (ZPRPNativeModule)
+ (NSString *)zmDeviceUUID;
@end


@interface NetworkManager (ZPRPNetworkInternal)
- (NSDictionary *)requestParamWithParam:(NSDictionary *)params;
@end


@implementation NetworkManager (ZPRPNetwork)
- (RACSignal *)getPacketStatusWithPackageId:(uint64_t)packageId transId:(uint64_t)transId deviceId:(NSString *)deviceId {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:self.paymentUserId forKey:@"revzalopayid"];
    [params setObject:@(packageId) forKey:@"packageid"];
    [params setObject:@(transId) forKey:@"zptransid"];
    [params setObjectCheckNil:deviceId forKey:@"deviceid"];
    [params setObject:@(kZaloPayClientAppId) forKey:@"appid"];
    return [self requestWithPath:api_v001_tpe_gettransstatus parameters:params];
}
@end

@implementation ZPRPNetwork
- (void)registerPrehandleError:(void (^)(int, NSString *))handle {
    [self prehandleError:handle];
}

- (NSDictionary *)requestParamWithParam:(NSDictionary *)params {
    NSDictionary *requestParam = [super requestParamWithParam:params];
    NSMutableDictionary *lixiParam = [self lixiRequestParam];
    [lixiParam addEntriesFromDictionary:requestParam];
    return lixiParam;
}

- (RACSignal *)getPacketStatusWithPackageId:(uint64_t)packageId transId:(uint64_t)transId {
    NSString *deviceId = [UIDevice zmDeviceUUID];

    return [[NetworkManager sharedInstance] getPacketStatusWithPackageId:packageId transId:transId deviceId:deviceId];
}

- (NSMutableDictionary *)lixiRequestParam {
    NSMutableDictionary *_return = [NSMutableDictionary dictionary];
    [_return setObject:@"ios" forKey:@"platform"];
    NSString *deviceId = [UIDevice zmDeviceUUID];
    [_return setObjectCheckNil:deviceId forKey:@"deviceid"];
    NSString *model = [[UIDevice currentDevice] model];
    [_return setObjectCheckNil:model forKey:@"devicemodel"];
    
    NSString *version = [[UIDevice currentDevice] systemVersion];
    [_return setObjectCheckNil:version forKey:@"osver"];
    NSString *appVersion = [NSString stringWithFormat:@"%@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
    [_return setObjectCheckNil:appVersion forKey:@"appver"];
    [_return setObjectCheckNil:@"" forKey:@"sdkver"];
    [_return setObjectCheckNil:@"" forKey:@"mno"];
    [_return setObjectCheckNil:[self networkType] forKey:@"conntype"];
    return _return;
}

- (NSString *)networkType {
    if ([AFNetworkReachabilityManager sharedManager].networkReachabilityStatus == AFNetworkReachabilityStatusReachableViaWiFi) {
        return @"wifi";
    }
    
    if ([AFNetworkReachabilityManager sharedManager].networkReachabilityStatus == AFNetworkReachabilityStatusReachableViaWWAN) {
        return @"3g";
    }
    return @"unknown";
}

- (NSString *)paymentUserId {
    return [NetworkManager sharedInstance].paymentUserId;
}

- (NSError *)preHandleResult:(NSDictionary *)responseData
                       error:(NSError *)error
                  subscriber:(id<RACSubscriber>)subscriber
                        path:(NSString *)path
                      params:(NSDictionary *)params {
    
    if (error) {
        [subscriber sendError:error];
        return error;
    }
    
    if (![responseData isKindOfClass:[NSDictionary class]] || responseData.count == 0) {
        NSError *dataError = [NSError errorWithDomain:@"ResponseDataError" code:-1 userInfo:nil];
        [subscriber sendError:dataError];
        return dataError;
    }
    
    int errorCode = [responseData intForKey:@"returncode"];
    if (errorCode != ZALOPAY_ERRORCODE_SUCCESSFUL) {
        NSError *returnError = [NSError errorFromDic:responseData];
        [subscriber sendError:returnError];
        return returnError;
    }
    
    [subscriber sendNext:responseData];
    [subscriber sendCompleted];
    return nil;
}

- (RACSignal *)createRedPacketBundleOrder:(int)quantity
                                totalluck:(long)totalluck
                               amounteach:(long)amounteach
                                     type:(int)type
                              sendmessage:(NSString *)sendmessage {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@(quantity) forKey:@"quantity"];
    [params setObject:@(totalluck) forKey:@"totalluck"];
    [params setObject:@(amounteach) forKey:@"amounteach"];
    [params setObject:@(type) forKey:@"type"];
    [params setObjectCheckNil:self.paymentUserId forKey:@"sendzalopayid"];
    [params setObjectCheckNil:sendmessage forKey:@"sendmessage"];
    return [self requestWithPath:api_redpackage_createbundleorder parameters:params];
}

//- (RACSignal *)submitToSendBundle:(NSString *)bundleId toFriends:(NSArray *)zaloIds {
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setObjectCheckNil:self.paymentUserId forKey:@"sendzalopayid"];
//    [params setObject:bundleId forKey:@"bundleid"];
//    [params setObjectCheckNil:[zaloIds componentsJoinedByString:@"|"] forKey:@"friendlist"];
//    return [self requestWithPath:api_redpackage_submittosendbundle parameters:params];
//}

- (RACSignal *)submitToSendBundle:(NSString *)bundleId toZaloPayFriends:(NSString *)zaloFriendInfo currentUserInfo:(NSString *)info {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:info forKey:@"zalopayofsender"];
    [params setObject:bundleId forKey:@"bundleid"];
    [params setObjectCheckNil:zaloFriendInfo forKey:@"zalopayoffriendlist"];
    return [self postRequestWithPath:api_redpackage_submittosendbundlebyzalopayinfo parameters:params];
}

- (RACSignal *)openPacket:(NSString *)packageId bundleId:(NSString *)bundleId {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:self.paymentUserId forKey:@"revzalopayid"];
    [params setObject:bundleId forKey:@"bundleid"];
    [params setObject:packageId forKey:@"packageid"];
    return [self postRequestWithPath:api_redpackage_submitopenpackage parameters:params];
}

- (RACSignal *)getSendHistoryWithCount:(int)count timestamp:(uint64_t)timestamp order:(int)order{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@(timestamp) forKey:@"timestamp"];
    [params setObject:@(count) forKey:@"count"];
    [params setObject:@(order) forKey:@"order"];
    [params setObjectCheckNil:self.paymentUserId forKey:@"zalopayid"];
    return [self requestWithPath:api_redpackage_getsentbundlelist parameters:params];
}

- (RACSignal *)getReceivePackageWithCount:(int)count timestamp:(uint64_t)timestamp order:(int)order {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@(timestamp) forKey:@"timestamp"];
    [params setObject:@(count) forKey:@"count"];
    [params setObject:@(order) forKey:@"order"];
    [params setObjectCheckNil:self.paymentUserId forKey:@"zalopayid"];
    return [self requestWithPath:api_redpackage_getrevpackagelist parameters:params];
}

- (RACSignal *)getPackageInBundle:(uint64_t )bundleid
                            count:(int)count
                        timestamp:(uint64_t)timestamp
                            order:(int)order {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:@(count) forKey:@"count"];
    [params setObjectCheckNil:@(order) forKey:@"order"];
    [params setObjectCheckNil:@(bundleid) forKey:@"bundleid"];
    [params setObjectCheckNil:@(timestamp) forKey:@"timestamp"];
    [params setObjectCheckNil:self.paymentUserId forKey:@"zalopayid"];
    return [self requestWithPath:api_redpackage_getpackagesinbundle parameters:params];
}

//- (RACSignal *)getRedPacketAppInfo:(NSString *)checksum {
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setObjectCheckNil:checksum forKey:@"checksum"];
//    return [self requestWithPath:api_redpackage_getappinfo parameters:params];
//}

- (RACSignal *)getBundleStatus:(uint64_t)bundleId {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@(bundleId) forKey:@"bundleid"];
    return [self requestWithPath:api_redpackage_getbundlestatus parameters:params];
}

- (RACSignal *)getListLixiStatus:(NSArray *)listPacketIds {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[listPacketIds JSONRepresentation] forKey:@"listpackageid"];
    return [self requestWithPath:api_redpackage_getlistpackagestatus parameters:params];

}

@end
