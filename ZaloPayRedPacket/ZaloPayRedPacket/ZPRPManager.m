//
//  ZPRPManager.m
//  ZaloPayRedPacket
//
//  Created by bonnpv on 7/22/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPRPManager.h"
#import <ZaloPayCommon/NSCollection+JSON.h>
#import <ReactiveObjC/ReactiveObjC.h>
#import <ZaloPayUI/ZPAppFactory.h>
#import "ZPRPLog.h"
#import "ZPRPExternalProtocols.h"
#import "ZPRPCommon.h"
#import "ZPRPNetwork.h"

typedef enum {
    BundleStatusInit = 1,
    BundleStatusPacketGen = 2,
    BundleStatusPacketAvailable = 3,
    BundleStatusPacketRefund = 4,
}BundleStatus;

@interface UIDevice (ZPRPNativeModule)
+ (NSString *)zmDeviceUUID;
@end


@interface ZPRPManager ()
@property (strong, nonatomic, readwrite) id<ZPRPDatabase> dbApi;
@property (strong, nonatomic, readwrite) id<ZPRPNetwork> networkApi;
@end

@implementation ZPRPManager

//+ (instancetype)sharedInstance {
//    static ZPRPManager *instance;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        instance = [[ZPRPManager alloc] init];
//    });
//    return instance;
//}

- (instancetype)initWithNetwork:(id<ZPRPNetwork>)networkApi db:(id<ZPRPDatabase>)dbApi {
    self = [super init];
    if (self) {
        self.networkApi = networkApi;
        self.dbApi = dbApi;
        [self registerNetworkErrorCode];
    }
    return self;
}

- (id)init {
    self = [super init];
    if (self) {
        [self registerNetworkErrorCode];
    }
    return self;
}

- (void)updateLixiPacketStatus {
    NSArray *allPacket = [self.dbApi getAllAvailablePacket];
    if (allPacket.count == 0) {
        return;
    }
    
//    "-1: Không tìm thấy package
//    1. INIT (chưa mở)
//    2. OPENED (đã mở)
//    3. REFUNDED (đã hoàn tiền)"
    
    [[self.networkApi getListLixiStatus:allPacket] subscribeNext:^(NSDictionary *dic) {
        NSArray *listpackagestatus = [dic arrayForKey:@"listpackagestatus"];
        for (NSDictionary *oneDic in listpackagestatus) {
            uint64_t packetId = [oneDic uint64ForKey:@"packageID"];
            int status = [oneDic intForKey:@"status"];
            
            if (status == 1) { // trạng thái được phép mở -> bỏ qua vì dữ liệu trong db là trạng thái có thể mở.
                continue;
            }
            
            if (status == 2) { // đánh dấu đã mở
                int amount = [oneDic intForKey:@"amount"];
                [self.dbApi openPacket:packetId amount:amount status:LixiErrorCodePacketHasOpen message:@""];
            } else { // mặc định là bao đã được hoàn tiền.
                [self.dbApi openPacket:packetId  amount:0 status:LixiErrorCodeRefund message:@""];
            }
        }
    }];
}

- (void)registerNetworkErrorCode {
    [self.networkApi registerPrehandleError:^(int errorCode, NSString *message) {
        if (errorCode == LixiErrorCodeInvalidAccessToken) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[ZPAppFactory sharedInstance] logout];
            });
        }
    }];
}

- (NSError *)timoutError {
    return [[NSError alloc] initWithDomain:@"Time out"
                                      code:-1
                                  userInfo:@{@"returnmessage":@"Hết thời gian kết nối đến máy chủ"}];
    
}

#pragma mark - OpenLixi

- (RACSignal *)requestStatus:(uint64_t)transid packageid:(uint64_t)packageid timeout:(float)timout {
    return [[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [self requestStatus:transid
                  packageid:packageid
                requestTime:[NSDate date]
                     timout:timout
                 subscriber:subscriber];
        return nil;
    }] replayLazily];
}



- (void)requestStatus:(uint64_t)transid
            packageid:(uint64_t)packageid
          requestTime:(NSDate *)timerequest
               timout:(float)timeout
           subscriber:(id<RACSubscriber>)subcriber {
    [[self.networkApi getPacketStatusWithPackageId:packageid
                                           transId:transid]
     subscribeNext:^(NSDictionary *dic) {
        int amount = [dic intForKey:@"amount"];
        [self.dbApi openPacket:packageid amount:amount status:LixiErrorCodePacketHasOpen message:@""];                
        [subcriber sendNext:dic];
        [subcriber sendCompleted];
        
    } error:^(NSError *error) {
        NSDictionary *dic = error.userInfo;
        if (![dic objectForKey:@"isprocessing"]) {
            [subcriber sendError:error];
            return;
        }
        if ([timerequest timeIntervalSinceNow] < -timeout) {
            NSError *timeOutError = [self timoutError];
            [subcriber sendError:timeOutError];
            return;
        }
        BOOL isProccess = [[dic objectForKey:@"isprocessing"] boolValue];
        if (isProccess) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 1), ^{
                [self requestStatus:transid
                          packageid:packageid
                        requestTime:timerequest
                             timout:timeout
                         subscriber:subcriber];
            });
            return;
        }
        [subcriber sendError:error];
    }];
}


@end
