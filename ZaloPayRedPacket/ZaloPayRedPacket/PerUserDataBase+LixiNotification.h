//
//  PerUserDataBase+LixiNotification.h
//  ZaloPayRedPacket
//
//  Created by bonnpv on 7/23/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <ZaloPayDatabase/PerUserDataBase.h>
#import "ReceiveLixiObject.h"

@interface PerUserDataBase (LixiNotification)

- (void)createLixiNotificationTable:(FMDatabase *)db;

- (void)saveLixiNotificationDataWithPackageId:(uint64_t)packageid
                                     bundleId:(uint64_t)bundleId
                                 senderAvatar:(NSString *)avatar
                                   senderName:(NSString *)name
                                      message:(NSString *)messge
                                       amount:(int)amount
                                       status:(int)status
                                 overrideData:(BOOL)shouldOverride;

- (void)openPacket:(uint64_t)packetId amount:(int)amount status:(int)status message:(NSString *)message;

- (ReceiveLixiObject *)receiveLixiObjectWithPacketId:(uint64_t)packetId;

- (NSArray *)getAllAvailablePacket;

@end
