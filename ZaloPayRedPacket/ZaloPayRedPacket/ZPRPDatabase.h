//
//  ZPRPDatabase.h
//  ZaloPayRedPacket
//
//  Created by bonnpv on 7/23/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <ZaloPayDatabase/ZPBaseTable.h>
#import "ReceiveLixiObject.h"
#import "ZPRPExternalProtocols.h"

@interface ZPLixiNotificationTable : ZPBaseTable<ZPRPDatabase>

@end
