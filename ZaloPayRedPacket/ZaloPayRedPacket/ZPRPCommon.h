//
//  ZPRPCommon.h
//  ZaloPayRedPacket
//
//  Created by Huu Hoa Nguyen on 8/18/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    LixiErrorCodeInvalidAccessToken     = -2,
    LixiErrorCodeInvalidPacketId        = -22,
    LixiErrorCodePacketNotMacth         = -23,
    LixiErrorCodeRefund                 = -24,
    LixiErrorCodePacketExpire           = -25,
    LixiErrorCodePacketNotExist         = -26,
    LixiErrorCodePacketHasOpen          = -27,
    LixiErrorCodeBundleNotContainPacket = -28,
    LixiErrorCodeOpenFail               = -29,
    LixiErrorInvalidMac                 = -30,
    LixiErrorCodeInvalidPacketStatus    = -74,
} LixiErrorCode ;

@interface NSError (Lixi)
- (BOOL)isInvalidPacketError;
@end


typedef enum {
    //    PacketStatusUnknown  = 0,
    PacketStatusCanOpen  = 1,
    //    PacketStatusOpened   = 2,
    //    PacketStatusInvalid  = 3,
} PacketStatus;
