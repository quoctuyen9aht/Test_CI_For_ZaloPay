//
//  ZaloPayRedpacketManager.h
//  ZaloPayRedPacket
//
//  Created by bonnpv on 7/22/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
@class RACSignal;

@interface ZaloPayRedpacketManager : NSObject

+ (instancetype)sharedInstance;

- (RACSignal *)requestStatus:(uint64_t)transid packageid:(uint64_t)packageid timeout:(float)timout;

- (void)updateLixiPacketStatus;
@end
