//
//  ZPRPCommon.m
//  ZaloPayRedPacket
//
//  Created by Huu Hoa Nguyen on 8/18/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import "ZPRPCommon.h"

@implementation NSError(Lixi)

- (BOOL)isInvalidPacketError {
    if (self.code == LixiErrorCodeInvalidPacketId ||
        self.code == LixiErrorCodePacketNotMacth||
        self.code == LixiErrorCodeRefund||
        self.code == LixiErrorCodePacketExpire||
        self.code == LixiErrorCodePacketNotExist||
        self.code == LixiErrorCodePacketHasOpen||
        self.code == LixiErrorCodeBundleNotContainPacket ||
        self.code == LixiErrorCodeInvalidPacketStatus) {
        
        return TRUE;
    }
    return FALSE;
}

@end
