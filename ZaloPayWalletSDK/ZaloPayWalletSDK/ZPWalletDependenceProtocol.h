//
//  ZPWalletPinProtocol.h
//  ZaloPayWalletSDK
//
//  Created by thi la on 3/2/17.
//  Copyright © 2017-present VNG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class ZPBill;
@class RACSignal;
@class ZPIBankingAccountBill;

@protocol ZPWalletDependenceProtocol <NSObject>

// TouchID
- (void)authenFromTouchIdCancel:(void(^_Nonnull)(void))cancel
                          error:(void(^_Nonnull)(NSError *_Nullable error))error
                       complete:(void(^_Nonnull)(NSString *_Nullable password))complete;
- (void)savePassword:(NSString *_Nullable)password;
- (BOOL)phoneIsSupportTouchId;
- (BOOL)isTouchIDExist;
- (BOOL)isEnableTouchID;
- (void)removeZaloPayPassword;
- (NSString *_Nonnull)checkToReplaceFaceIDMessage:(NSString *_Nonnull)messsage;
// App Settings
- (BOOL)usingTouchId;
- (void)setUsingTouchId:(BOOL)value;
- (BOOL)turnOffTouchIdManual;
- (void)setTurnOffTouchIdManual:(BOOL)value;

//Quickpay
- (BOOL)paymentCodeShowSecurityIntro;
- (void)setPaymentCodeShowSecurityIntro:(BOOL)value;
- (int)paymentCodeChannelSelect;
- (void)setPaymentCodeChannelSelect:(int)pmcId;
// WalletManager
- (long)currentBalance;
- (void)updateWithNewBalance:(long long)balance;

// Config
- (NSDictionary <NSString *, id> *_Nullable)externalConfig;
- (NSDictionary <NSString *, id> *_Nullable)creditCardConfig;
- (BOOL)checkCCBinIsSupport:(NSString *_Nonnull)ccBin;
- (int)maxCCCard;
- (NSString *_Nullable)bidvRegisterPayOnlineUrl;
- (NSString *_Nullable)bidvHelpUrl;
// PlaceHolderView

- (UIView *_Nonnull)createPlaceholderView;

// Notification
- (BOOL)isNotificationMapAccount:(int)notificationType;
- (BOOL)isNotificationRemoveAccount:(int)notificationType;
- (BOOL)isNotificationWithUpdateBalance:(int)type;

// Voucher
- (void)goToVoucherHistoriesList;

// RegisterBank
- (void)registerBankFrom:(UIViewController *_Nullable)controller
                    with:(NSDictionary  * _Nullable)info
                    from:(NSString  * _Nullable)transId
                    with:(void(^_Nullable)(id _Nullable result))completion
                   error:(void(^_Nullable)(NSError * _Nullable error))errorHandler NS_SWIFT_NAME(registerBank(controller:info:transId:completion:errorHandler:));

- (void)showToastFrom:(UIViewController *_Nullable)controller message:(NSString *_Nullable)message delay:(NSTimeInterval)delay;

// WriteLog

- (void)writeAppTransIdLog:(ZPBill *_Nonnull)bill;

// Profile

- (NSString *_Nullable)getZaloId;
- (NSString *_Nullable)userPhoneNumber;
- (NSString *_Nullable)zaloPayUserId;
- (NSString *_Nullable)getDeviceId;

// CC Debit
- (BOOL)isSupportCCDebit;
- (BOOL)IsCCDebit:(NSString *_Nonnull)ccBin;

// Map card
- (RACSignal *_Nonnull)mapBankWith:(UIViewController *_Nullable)vc forBill:(ZPBill *_Nullable)bill;
- (void)showFAQ:(UIViewController *_Nonnull)fromVC;
- (void)showNeedSupportUsing:(NSDictionary *_Nonnull)data from:(UIViewController *_Nonnull)viewController;
- (void)showTermViewFrom:(UIViewController *_Nullable)controller;

// Update Level
- (void)updateLevel3From:(UIViewController *_Nullable)controller;

// phone
- (NSArray<NSString *> *_Nullable)phonePattern;

// Update identify number user
- (void)updateIdentifyNumber:(NSString *_Nullable)number;

- (BOOL)kycEnableMapCard;
- (BOOL)kycEnableLimitTransaction;

// Limit transaction
- (NSInteger)kycLimitTransaction;

// check bank account or saved card is already existed
- (BOOL)isBankAlreadyMapped:(NSString *_Nonnull)bankCode;

- (ZPIBankingAccountBill*_Nonnull)mapAccountBill:(NSString *_Nonnull)bankCode;

// Check use gateway
- (BOOL)canUseGateway:(NSInteger)appid;

// Custom result link card
- (NSString*_Nullable)getMessageLinkCardResult:(NSString*_Nullable)binCheck;

- (void)trackAppsflyer:(NSString *_Nonnull)eventName eventValues:(NSDictionary *_Nonnull)values;
- (BOOL)isBankAccountExist:(NSString *_Nonnull)bankCode;
@end


