//
//  ZPBill+ExtInfo.h
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/21/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPBill.h"

@interface ZPBill (ExtInfo)
- (NSArray<ZPBillExtraInfo*> *)extFromItem;
- (NSString *)appName;
@end
