//
//  ZPVoucherViewWithButton.h
//  ZaloPayWalletSDK
//
//  Created by Phuoc Huynh on 9/12/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZPTableViewVoucherHistories.h"
#import "ZPBill.h"
#import "ZPContainerViewController.h"

@interface ZPVoucherViewWithButton : UIView <ZPTableViewVoucherHistoriesDelegate>

@property (nonatomic,strong) ZPTableViewVoucherHistories *tbvVouchersList;
@property (nonatomic,strong) UIView *viewExpendVouchersList;
@property (nonatomic,strong) ZPIconFontImageView *imageViewGift;
@property (nonatomic,strong) UILabel *lableHaveGift;
@property (nonatomic,strong) UILabel *labelSelectVouchers;
@property (nonatomic,strong) ZPIconFontImageView *imageArrowUp;
@property (nonatomic,strong) UILabel *labelValueDiscount;
@property (nonatomic,strong) UILabel *labelDescriptionDiscount;
@property (nonatomic,strong) UIView *maskView;

@property (nonatomic, strong) UIButton *actionButton;
@property (nonatomic) ZPBill *bill;
@property (assign, nonatomic) BOOL isExpendVouchersList;
@property (weak, nonatomic) ZPContainerViewController *currentVC;
@property (strong,nonatomic) UILabel *labelError;
@property (strong,nonatomic) UIView *viewError;
@property (strong,nonatomic) UIView *lineGray;
@property (strong,nonatomic) UIView *lineViewGreen;
@property (nonatomic) int countSelectPaymentMethod;

- (instancetype)initWithBill:(ZPBill *)bill fromController:(ZPContainerViewController *)controller;
- (void) updateListVoucherWhenSwitchPaymentMethod;
@end
