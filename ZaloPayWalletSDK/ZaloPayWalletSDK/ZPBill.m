//
//  ZPBill.m
//  ZPSDK
//
//  Created by phungnx on 12/21/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import "ZPBill.h"
#import "ZaloPayWalletSDKPayment.h"
#import "ZPPaymentManager.h"
#import "ZaloPayWalletSDKLog.h"
#import <ZaloPayAnalytics/ZPAOrderData.h>
#import <ZaloPayWalletSDK/ZaloPayWalletSDK-Swift.h>
#import "ZPVoucherInfo.h"
#import <ZaloPayCommon/MF_Base64Additions.h>
#import "NSData+ZPExtension.h"
#import <ZaloPayCommon/NSCollection+JSON.h>

#define KEY_APP_TRANX_ID        @"app_tranx_id"
#define KEY_MAC                 @"mac"
#define KEY_ITEMS               @"items"
#define KEY_BILL_DESCRIPTION    @"bill_description"
#define KEY_EMBED_DATA          @"embed_data"
#define KEY_TIME                @"time"
#define KEY_IAP_PRODUCT_ID      @"iap_product_id"
#define KEY_APP_USER            @"app_user"


@implementation ZPBill

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.appTransID forKey:KEY_APP_TRANX_ID];
    [aCoder encodeObject:self.mac forKey:KEY_MAC];
    [aCoder encodeObject:self.items forKey:KEY_ITEMS];
    [aCoder encodeObject:self.billDescription forKey:KEY_BILL_DESCRIPTION];
    [aCoder encodeObject:self.embedData forKey:KEY_EMBED_DATA];
    [aCoder encodeObject:[NSNumber numberWithLongLong:self.time] forKey:KEY_TIME];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        self.appTransID = [aDecoder decodeObjectForKey:KEY_APP_TRANX_ID];
        self.mac = [aDecoder decodeObjectForKey:KEY_MAC];
        self.items = [aDecoder decodeObjectForKey:KEY_ITEMS];
        self.billDescription = [aDecoder decodeObjectForKey:KEY_BILL_DESCRIPTION];
        self.embedData = [aDecoder decodeObjectForKey:KEY_EMBED_DATA];
        self.time = [[aDecoder decodeObjectForKey:KEY_TIME] longLongValue];
    }
    return self;
}

- (void)setItems:(NSString *)items {
    _items = items;
}

- (long)finalAmount {
    if (!self.voucherInfo && !self.promotionInfo) {
        return self.amount;
    }
    long long discountAmount = [self finalDiscountAmount];
    return MAX(self.amount - discountAmount, 0);
}

- (long)finalDiscountAmount {
    if (!self.voucherInfo && !self.promotionInfo) {
        return 0;
    }
    long long discountAmount = self.voucherInfo == nil ? self.promotionInfo.discountamount : self.voucherInfo.discountAmount;
    return discountAmount;
}

- (id) initWithItems: (NSString*) items andAppTranxID:(NSString *) aTranxID andDescription:(NSString *) desc andEmbedData:(NSString *) eData{
    self = [super init];
    self.items = items;
    self.appTransID = aTranxID;
    self.billDescription = desc;
    self.embedData = eData;
    NSDictionary *eDataJson = [NSDictionary castFrom:[eData JSONValue]];
    NSDictionary *promotioninfo = [NSDictionary castFrom:[[eDataJson stringForKey:@"promotioninfo"] JSONValue]];
    self.campaigncode = [promotioninfo stringForKey:@"campaigncode" defaultValue:@""];
    return self;
}

+ (ZPBill *)coppyFrom:(ZPBill *)bill {
    ZPBill *_return = [ZPBill new];
    _return.appId = bill.appId;
    _return.appTransID = bill.appTransID;
    _return.mac = bill.mac;
    _return.items = bill.items;
    _return.billDescription = bill.billDescription;
    _return.embedData = bill.embedData;
    _return.time = bill.time;
    _return.amount = bill.amount;
    _return.appUser = bill.appUser;
    _return.data = bill.data;
    _return.voucherInfo = bill.voucherInfo;
    _return.promotionInfo = bill.promotionInfo;
    _return.appUserInfo = bill.appUserInfo;
    _return.transType = bill.transType;
    _return.currentPaymentMethod = bill.currentPaymentMethod;
    _return.displayInfo = bill.displayInfo;
    _return.bankCode = bill.bankCode;
    _return.ordersource = bill.ordersource;
    _return.doneShowPinViewOrTouchId = bill.doneShowPinViewOrTouchId;
    _return.shouldSkipSuccessView = bill.shouldSkipSuccessView;
    _return.errorCCCanNotMapWhenPay = bill.errorCCCanNotMapWhenPay;
    _return.bankCustomerId = bill.bankCustomerId;
    return _return;
}

- (NSString *)encodeBase64:(NSDictionary *)userInfor {
    NSMutableDictionary *result = [NSMutableDictionary new];
    NSString *muid = [userInfor stringForKey:@"muid"];
    NSString *maccesstoken = [userInfor stringForKey:@"maccesstoken"];
    [result addEntriesFromDictionary:@{ @"appid": @(self.appId)}];
    [result setObjectCheckNil:self.appTransID forKey:@"apptransid"];
    [result setObjectCheckNil:self.appUser forKey:@"appuser"];
    [result addEntriesFromDictionary:@{ @"apptime" : @(self.time)}];
    [result addEntriesFromDictionary:@{ @"item": self.items ?: @""} ];
    [result setObjectCheckNil:self.billDescription forKey:@"description"];
    [result setObjectCheckNil:self.embedData forKey:@"embeddata"];
    [result addEntriesFromDictionary:@{ @"amount": @(self.amount)}];
    [result setObjectCheckNil:self.mac forKey:@"mac"];
    
    [result setObjectCheckNil:_bankCode forKey:@"bankcode"];
    [result setObjectCheckNil:muid forKey:@"muid"];
    [result setObjectCheckNil:maccesstoken forKey:@"maccesstoken"];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:result options:0 error:nil];
    NSString *data = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSData *data2 = [data dataUsingEncoding:NSUTF8StringEncoding];

    NSString *rs = [[data2 base64EncodedStringWithOptions:0] stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
    rs = [rs stringByReplacingOccurrencesOfString:@"+" withString:@"-"];

    return rs;
}
@end

@implementation ZPIBankingAccountBill

@end

@implementation ZPBillExtraInfo

- (instancetype)initWithExtraKey:(NSString *)extraKey extValue:(NSString *)extraValue {
    self = [super init];
    self.extraKey = extraKey;
    self.extraValue = extraValue;
    return self;
}

@end
