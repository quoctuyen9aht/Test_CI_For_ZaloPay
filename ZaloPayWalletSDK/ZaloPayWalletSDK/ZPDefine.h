//
//  ZPDefine.h
//  ZPSDK
//
//  Created by phungnx on 12/21/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//
//mai a tai
#import <Foundation/Foundation.h>

// notifi key from in app
//#define ZPInApp_Notification_Key @"ZPInApp_Notification_Key"
extern NSString *const ZPInApp_Notification_Key;


#ifndef ZALOPAY_SDK_DEFINE1_H
#define ZALOPAY_SDK_DEFINE1_H

#define BIDV_URL_SUPPORT  @"EWALLET/pay.jsp"
#define BIDV_URL_REGISTER  @"DKNHDT"
#define BIDV_OPEN_URL_REGISTER  @"https://ebank.bidv.com.vn/DKNHDT/dk_bidvonline.htm"
#define BIDV_HELP_LINK @"http://html.zapps.vn/pay/FAQCenter/suport_bidv.html"

#define ZP_NOTIF_PAYMENT_CLOSE @"ZP_NOTIF_PAYMENT_CLOSE"
#define ZP_NOTIF_PAYMENT_TOKEN_EXPIRED @"ZP_NOTIF_PAYMENT_TOKEN_EXPIRED"
#define ZP_NOTIF_PAYMENT_IS_MAINTAINANCE @"ZP_NOTIF_PAYMENT_IS_MAINTAINANCE"
#define ZP_NOTIF_PAYMENT_MY_WALLET @"ZP_NOTIF_PAYMENT_MY_WALLET"
extern NSString * ZP_NOTIF_UPGRADE_LEVEL_3;
#define ZP_NOTIF_PAYMENT_GO_MAPCARD @"ZP_NOTIF_PAYMENT_GO_MAPCARD"
#define ZP_NOTIF_PAYMENT_GO_LISTBANK_MAPCARD @"ZP_NOTIF_PAYMENT_GO_LISTBANK_MAPCARD"
#define ZP_NOTIF_UPGRADE_LEVEL_AND_SAVE_CARD @"ZP_NOTIF_UPGRADE_LEVEL_AND_SAVE_CARD"
#define ZP_NOTIF_VIEW_BANK_ACCOUNTS @"ZP_NOTIF_VIEW_BANK_ACCOUNTS"

#define ZP_NOTIF_PAYMENT_GO_LINKACOUNT @"ZP_NOTIF_PAYMENT_GO_LINKACOUNT"
#define ZP_NOTIF_UPGRADE_LEVEL_AND_GO_LINKACOUNT @"ZP_NOTIF_UPGRADE_LEVEL_AND_GO_LINKACOUNT"
#define ZP_NOTIF_PAYMENT_GO_SELECTBANK @"ZP_NOTIF_PAYMENT_GO_SELECTBANK"
#define ZP_NOTIF_PAYMENT_CANCEL_REGISTER_BANK @"ZP_NOTIF_PAYMENT_CANCEL_REGISTER_BANK"

#define kZPCachePlatformInfo @"kZP_Cache_Platform_Info"
#define kZPCacheBankList @"kZP_Cache_Bank_List"

#define BIDV_Message_Username_invalidate @"tên khách hàng không đúng"
#define BIDV_Card_Not_Found @"không tồn tại"
#define BIDV_NotRegister @"chưa đăng ký"
#define BIDV_Not_Support_Pay_By_Card @"%@ chỉ hỗ trợ hình thức thanh toán qua thẻ liên kết. Bạn có muốn liên kết thẻ để tiếp tục thanh toán?"
#define VCB_Not_Support_Map_By_Card @"Ngân hàng Vietcombank chỉ hỗ trợ Liên kết tài khoản.\nHãy liên kết tài khoản để tiếp tục thanh toán"
//Add

#define WEB_USER_AGENT  @"Mozilla/5.0 (iPhone; CPU iPhone OS 9_3_2 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Mobile/13F69"

#define VCB_WEB_USER_AGENT @"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36"

typedef NS_ENUM(NSInteger, ZPATMCellType){
    ZPATMCellTypePinnedViewCell,
    ZPATMCellTypeCardWebView,
    ZPATMCellTypeOtp,
    ZPATMCellTypeAuthen,
    ZPATMCellTypeOption,
    ZPATMCellTypeDidChoosePaymentMethod,
    ZPATMCellTypeAutoDetect
};

typedef NS_ENUM(NSInteger, ZPPaymentStep) {
    ZPPaymentStepSubmitTrans        = 1,
    ZPPaymentStepSubmitAuthenPayer  = 2,
    ZPPaymentStepGetPaymentResult   = 3
};

typedef NS_ENUM(NSInteger, ZPBankFuntion) {
    ZPBankFuntionWithDraw               = 5,
    ZPBankFuntionBillPayByCard          = 103,
    ZPBankFuntionBillPayByAccount       = 104,
    ZPBankFuntionBillPayByCardToken     = 105,
    ZPBankFuntionBillPayByAccountToken  = 106,
    ZPBankFuntionMapCard                = 301,
    ZPBankFuntionMapAccount             = 302,
};
typedef NS_ENUM(NSInteger, ZPAPPStatus) {
    ZPAPPDisable        = 0,
    ZPAPPEnable         = 1,
    ZPAPPMaintenance    = 2
};

typedef NS_ENUM(NSInteger, ZPRequirePin) {
    ZPRequirePinEnableOption1   = 1,
    ZPRequirePinDisable         = 2,
    ZPRequirePinEnableOption3   = 3
};

typedef NS_ENUM(NSInteger, ZPTransType) {
    ZPTransTypeBillPay     = 1,
    ZPTransTypeWalletTopup = 2,
    ZPTransTypeAtmMapCard  = 3,
    ZPTransTypeTranfer     = 4,
    ZPTransTypeWithDraw    = 5,
};

typedef NS_ENUM(NSInteger, ZPPMCStatus) {
    ZPPMCDisable        = 0,
    ZPPMCEnable         = 1,
    ZPPMCMaintenance    = 2
};

typedef NS_ENUM(NSInteger, ZPBankInterfaceType) {
    ZPBankInterfaceTypeParseWeb     = 1,
    ZPBankInterfaceTypeShowWeb      = 2
};

typedef NS_ENUM(NSInteger, ZPBankActionType) {
    ZPBankActionTypeAPI     = 1,
    ZPBankActionTypeWeb     = 2
};


typedef NS_ENUM(NSInteger, ZPPaymentMethodType){
    ZPPaymentMethodTypeMethods          = 0,
    ZPPaymentMethodTypeAtm              = 39,
    ZPPaymentMethodTypeCreditCard       = 36,
    ZPPaymentMethodTypeZaloPayWallet    = 38,
    ZPPaymentMethodTypeIbanking         = 37,
    ZPPaymentMethodTypeCCDebit          = 41,
    ZPPaymentMethodTypeSavedCard        = 1002,
    ZPPaymentMethodTypeSavedAccount     = 1004
};

typedef NS_ENUM(NSInteger, ZPSecurityMode){
    ZPSecurityModeUnsupport     = 0,
    ZPSecurityModeHMACSHA256    = 1,
    ZPSecurityModeHMACMD5       = 2,
    ZPSecurityModeHMACSHA384    = 3,
    ZPSecurityModeHMACSHA1      = 4,
    ZPSecurityModeHMACSHA512    = 5,
};

typedef NS_ENUM(NSInteger, ZPVoucherHistoryStatus){
    
    ZPVoucherHistoryStatusInit       = 1,
    ZPVoucherHistoryStatusAvailabel  = 2,
    ZPVoucherHistoryStatusUsed       = 6,
};

typedef NS_ENUM(NSInteger, ZPVoucherValueType){
    ZPVoucherValueTypePercent  = 1,
    ZPVoucherValueTypeValue    = 2,
};

typedef NS_ENUM(NSInteger, ZPPromotionType){
    ZPPromotionTypePercent  = 1,
    ZPPromotionTypeValue    = 2,
};

typedef NS_ENUM(NSInteger, ZPEndowType){
    ZPEndowTypePromotion  = 1,
    ZPEndowTypeVoucher    = 2,
};

typedef NS_ENUM(NSInteger, ZPVoucherConditionPPAmount) {
    ZPVoucherConditionPPAmountCorrect                    = 1,
    ZPVoucherConditionPPAmountLessThanMinPPAmount        = 2,
    ZPVoucherConditionPPAmountGreaterThanMaxPPAmount     = 3,
};

typedef NS_ENUM(NSInteger, ZPZaloPayCreditStatus) {
    kZPZaloPayCreditStatusCodeSuccess              =   1,
    kZPZaloPayCreditStatusCodeFail                 =   -1,
    kZPZaloPayCreditStatusCodeUnidentified         =   -1000
};


typedef NS_ENUM(NSInteger, ZPErrorStep)  {
    ZPErrorStepAbleToRetry  = 2,
    ZPErrorStepNonRetry     = 3,
};

typedef NS_ENUM(NSInteger, ZPZaloPayCreditError) {
    kZPZaloPayCreditErrorCodeNoneError                         =   1,
    kZPZaloPayCreditAuthenErrorCodeNoneError1                  =   14,
    kZPZaloPayCreditAuthenErrorCodeNoneError2                  =   18,
    kZPZaloPayCreditErrorCodeMissParam                         = -5004,
    kZPZaloPayCreditErrorCodeInvalidParam                      = -5005,
    kZPZaloPayCreditErrorCodeFail                              = -5008,
    kZPZaloPayCreditErrorCodeServerMaintenance                 = -5505,
    kZPZaloPayCreditErrorCodeSDKTimeout                        = -5507,
    kZPZaloPayCreditErrorCodePinRetry                          = -5508,
    
    //for ATM
    kZPZaloPayCreditErrorCodeRequestTimeout                    = -5057,    
    kZPZaloPayCreditErrorCodeRetryOTP                          = -5059,
    kZPZaloPayCreditErrorCodeAtmIncorrectOTP                   = -5075,
    
    kZPZaloPayCreditErrorCodeCaptchaInvalid                    = -5076,
    ZPZPZaloPayCreditErrorCodeTransIdNotExist                  = -49,
    ZPZPZaloPayCreditErrorCodeBlockUser                        = -124,
    ZPZaloPayCreditErrorLoginFail                              = -71,
    ZPZaloPayCreditErrorLoginExpired                           = -72,
    ZPZaloPayCreditErrorTokenInvalid                           = -73,
    ZPZaloPayCreditErrorTokenNotFound                          = -77,
    ZPZaloPayCreditErrorTokenExpired                           = -78,
    ZPZaloPayCreditErrorAccountSuspended                       = -61,
    ZPZaloPayCreditErrorMaxBalancePerDay                       = -133,
    //for iap
    
    kZPZaloPayCreditErrorCodeUnknownException                  = -1000,
    kZPZaloPayCreditErrorCodeGettingResourceFailed             = -1001,
};

typedef NS_ENUM (NSInteger, ZPInterfaceType){
    ZPInterfaceTypeParseWeb = 1,
    ZPInterfaceTypeShowWebView
};

typedef NS_ENUM (NSInteger, ZPSDK_BankType){
    ZPSDK_BankType_Card = 1, // banh type cho thẻ atm của ngân hàng
    ZPSDK_BankType_Account = 2,  // banh type cho tài khoản ngân hàng
};

typedef NS_ENUM(NSInteger, ZPBankStatus) {
    ZPBankStatusDisable        = 0,
    ZPBankStatusEnable         = 1,
    ZPBankStatusMaintenance    = 2
};


#endif
