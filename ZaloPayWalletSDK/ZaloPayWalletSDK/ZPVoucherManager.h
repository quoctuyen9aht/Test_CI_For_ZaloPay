//
//  ZPVoucherManager.h
//  ZaloPayWalletSDK
//
//  Created by Phuoc Huynh on 8/25/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ZPBill;
@class RACSignal;
@class ZPVoucherInfo;
@class ZPVoucherHistory;

@interface ZPVoucherManager : NSObject
@property (nonatomic,strong) NSMutableArray <ZPVoucherInfo *>*arrVouchersWillRevert;

- (RACSignal <NSArray <ZPVoucherHistory *>*>*)loadAllAvailableVoucher;
- (RACSignal <NSArray <ZPVoucherHistory *>*>*)loadVoucherForBill:(ZPBill *)bill;

- (void)arrVouchersWillRevertAddVoucher:(ZPVoucherInfo *)voucherInfo NS_SWIFT_NAME(addRevert(voucher:));
- (void)revertArrVoucherAndUseVoucher:(ZPVoucherInfo *)voucherUsed NS_SWIFT_NAME(revertArrVoucherAndUse(voucher:));
- (void)revertVoucher:(ZPVoucherInfo *)voucher NS_SWIFT_NAME(revert(voucher:));
    
- (NSMutableArray *)voucherCachesForBill:(ZPBill *)bill;
- (void)saveVouchers:(NSArray *)vouchers forBill:(ZPBill *)bill;
- (NSString *)cacheKeyForBill:(ZPBill *)bill;
- (void)updateCacheAfterPaymentSuccessWithVoucher:(ZPVoucherHistory *)voucher forBill:(ZPBill *)bill;
- (RACSignal *)preloadVoucherForBill:(ZPBill *)bill;
- (void)clearVouchersCaches;


- (ZPVoucherHistory *) getBestVoucherFromArrayVoucher:(NSArray *)arrVouchers ofBill:(ZPBill *)bill;

- (NSMutableArray <ZPVoucherHistory *>*)filterAvailableVouchers:(NSArray *)allVoucher forBill:(ZPBill *)bill;
- (NSArray <ZPVoucherHistory *>*)filterAvailableVouchersWhenSelectedPaymentMethod:(NSArray *)allVoucher forBill:(ZPBill *)bill;
- (NSArray <ZPVoucherHistory *>*)filterAvailableVouchersToEnablePaymentMethod:(NSArray *)allVoucher forBill:(ZPBill *)bill;

- (long long)getValueCurrencyFromVoucher:(ZPVoucherHistory *)voucher ofBill:(ZPBill *)bill;
- (ZPVoucherInfo *)getVoucherUsedExistedInCache:(ZPVoucherHistory *)voucher;

@end
