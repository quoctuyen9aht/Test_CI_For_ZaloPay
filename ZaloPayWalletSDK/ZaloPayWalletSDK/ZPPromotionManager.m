//
//  ZPPromotionManager.m
//  ZaloPayWalletSDK
//
//  Created by Phuochh on 5/31/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import "ZPPromotionManager.h"

#import "NetworkManager+ZaloPayWalletSDK.h"
#import "ZPVoucherHistory.h"
#import "ZaloPayWalletSDKPayment.h"
#import "ZPBill.h"
#import "ZPPaymentManager.h"
#import <PINCache/PINMemoryCache.h>
#import <ZaloPayWalletSDK/ZaloPayWalletSDK-Swift.h>
#import "ZPVoucherInfo.h"
#import "ZaloPayWalletSDKLog.h"

#import "ZPPaymentMethod.h"
#import "ZPPaymentChannel.h"
#import "ZPDataManager.h"
#import "ZPResourceManager.h"

#import "ZPPromotion.h"
#import "ZPPromotionInfo.h"

#import <PINCache/PINMemoryCache.h>
@interface ZPPromotionManager ()
@property (nonatomic,strong) PINMemoryCache *memoryCache;
@end
@implementation ZPPromotionManager
- (id)init {
    self = [super init];
    if (self != NULL) {
        self.promotionsWillRevert = [NSMutableArray new];
        self.memoryCache = [[PINMemoryCache alloc] init];
    }
    return self;
}

#pragma mark - Cache
- (NSString *)cacheKeyForBill:(ZPBill *)bill {
    id<ZPWalletDependenceProtocol> helper = [ZaloPayWalletSDKPayment sharedInstance].appDependence;
    NSString *userId = [helper zaloPayUserId];
    long billAmount = bill.amount;
    uint32_t appId = bill.appId;
    return [NSString stringWithFormat:@"promotions%d%@%ld",appId,userId,billAmount];
}

- (void)savePromotions:(NSArray *)promotions forBill:(ZPBill *)bill {
    if ([promotions isKindOfClass:[NSArray class]]) {
        NSString *cacheKey = [self cacheKeyForBill:bill];
        [self.memoryCache setObject:promotions forKey:cacheKey];
    }
}

- (NSMutableArray *)promotionsCachesForBill:(ZPBill *)bill {
    NSString *cacheKey = [self cacheKeyForBill:bill];
    NSArray *data = [self.memoryCache objectForKey:cacheKey];
    NSMutableArray *_return = [NSMutableArray array];
    if (data.count > 0) {
        [_return addObjectsFromArray:data];
    }
    return  _return;
}
- (void)clearPromotionsCaches {
    [self.memoryCache removeAllObjects];
}

- (ZPPromotionInfo *) getPromotionUsedExistedInCache:(ZPPromotion *)promotion {
    return [self.promotionsWillRevert firstObjectUsing:^BOOL(ZPPromotionInfo *promotionInfo) {
        return promotion.campaignId == promotionInfo.promotionUsed.campaignId;
    }];
}

#pragma mark - Load promotions
- (RACSignal *)preloadPromotionsForBill:(ZPBill *)bill {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSString *key = [self cacheKeyForBill:bill];
        NSArray *data = [self.memoryCache objectForKey:key];
        if (data) {
            [subscriber sendCompleted];
            return nil;
        }
        [[self loadAllAvailablePromotionsOfBill:bill] subscribeNext:^(NSArray *promotions) {
            [self savePromotions:promotions forBill:bill];
            [subscriber sendCompleted];
        } error:^(NSError *error) {
            [subscriber sendCompleted];
        }];
        return nil;
    }];
}

- (RACSignal *)loadRemotePromotionsForBill:(ZPBill *)bill {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [[self loadAllAvailablePromotionsOfBill:bill] subscribeNext:^(NSArray *promotions) {
            [self savePromotions:promotions forBill:bill];
            [subscriber sendCompleted];
        } error:^(NSError *error) {
            [subscriber sendCompleted];
        }];
        return nil;
    }];
}

- (RACSignal <NSArray <ZPPromotion *>*>*)loadAllPromotionsOfBill:(ZPBill *)bill {
    return  [[[[ZaloPayWalletSDKPayment sharedInstance] getPromotionsWithAppID:bill.appId campaignCode:bill.campaigncode] map:^NSArray <ZPPromotion *>*(NSDictionary *data) {
        NSArray *allPromotions = [data arrayForKey:@"data"];
        NSArray *promotions = [self parsePromotionData:allPromotions];
        return promotions;
    }] deliverOnMainThread];
}
- (NSArray <ZPPromotion *>*)parsePromotionData:(NSArray *)promotionsData {
    if (promotionsData.count == 0) {
        return @[];
    }
    NSMutableArray <ZPPromotion *>*allPromotions = [NSMutableArray new];
    for (NSDictionary *oneDic in promotionsData) {
        ZPPromotion *promotion = [[ZPPromotion alloc] initDataWithDict:oneDic];
        [allPromotions addObject:promotion];
    }
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"useconditionexpireddate"
                                                                   ascending:YES];
    NSArray <ZPPromotion *> *sortedPromotions = [allPromotions sortedArrayUsingDescriptors:@[sortDescriptor]];
    return sortedPromotions;
}
- (RACSignal *)loadAllAvailablePromotionsOfBill:(ZPBill *)bill {
    return [[self loadAllPromotionsOfBill:bill] map:^NSArray <ZPPromotion *> * _Nullable (NSArray<ZPPromotion *> * _Nullable promotions) {
        if (!promotions) {
            return @[];
        }
        
        NSMutableArray *allPromotions = [self filterAvailablePromotions:promotions forBill:bill];
        // case user có nhiều promotions -> use danh sách promotions đó -> vào xem danh sách promotions đang có.
        @synchronized (self.promotionsWillRevert) {
            for (ZPPromotionInfo *promotionInfo in self.promotionsWillRevert) {
                if (![allPromotions containsObject:promotionInfo.promotionUsed]) {
                    [allPromotions addObjectNotNil:promotionInfo.promotionUsed];
                }

            }
        }
        return allPromotions;
    }];
}
- (NSMutableArray *)filterAvailablePromotions:(NSArray *)allPromotions forBill:(ZPBill *)bill {
    NSMutableArray *_return = [NSMutableArray array];
    for (ZPPromotion *promotion in allPromotions) {
        NSDate* expireddate = [NSDate dateWithTimeIntervalSince1970:promotion.useconditionexpireddate/1000];
        if ([expireddate timeIntervalSinceNow] < 0) {
            continue;
        }
        if (![self conditionMinMaxFor:promotion bill:bill]) {
            continue;
        }
        [_return addObject:promotion];
    }
    return _return;
}

- (BOOL)conditionMinMaxFor:(ZPPromotion *)promotion bill:(ZPBill *)bill {
    return bill.amount >= promotion.minamount;
}


#pragma mark - Payment Condition
- (BOOL) conditionForPaymentMethod:(ZPPromotion *)promotion forBill:(ZPBill *) bill {
    if ([promotion.paymentConditions.useconditionpmcs containsObject:@(-1)] || promotion.paymentConditions.useconditionpmcs.count == 0) {
        return true;
    }
    if (![self conditionPMCListOfPromotion:promotion forBill:bill]) {
        return false;
    }
    if (bill.currentPaymentMethod.methodType == ZPPaymentMethodTypeZaloPayWallet) {
        return true;
    }
    if ([bill.currentPaymentMethod isCCCard] || [bill.currentPaymentMethod isCCDebitCard]) {
        return [self conditionCCBankCodeListOfPromotion:promotion forBill:bill];
    }else {
        return [self conditionBankCodeListOfPromotion:promotion forBill:bill];
    }
}
// PMC Condition
- (BOOL) conditionPMCListOfPromotion:(ZPPromotion *)promotion forBill:(ZPBill *) bill {
    NSString *bankCode = bill.currentPaymentMethod.bankCode;
    if ([bankCode.uppercaseString containsString:@"VISA"]||
        [bankCode.uppercaseString containsString:@"MASTER"]||
        [bankCode.uppercaseString containsString:@"JCB"]) {
        bankCode = @"CC";
    }
    ZPChannel *channel = [[ZPDataManager sharedInstance] channelWithTypeAndBankCode:bill.currentPaymentMethod.methodType bankCode:bankCode];
    
    int pmcidOfBill = channel.channelID;
    for (NSString *pmcid in promotion.paymentConditions.useconditionpmcs) {
        if (pmcidOfBill == pmcid.intValue) {
            promotion.paymentConditions.currentPMCID = pmcidOfBill;
            return true;
        }
    }
    return false;
}

// CCBankcode Condition
- (BOOL) conditionCCBankCodeListOfPromotion:(ZPPromotion *)promotion forBill:(ZPBill *) bill {
    
    if (promotion.paymentConditions.useconditionccbankcodes.count == 0 ) {
        return true;
    }
    NSString *ccbankcodeOfBill = [ZPResourceManager getCcBankCodeFromConfig:bill.currentPaymentMethod.savedCard.first6CardNo];
    if ([ccbankcodeOfBill.uppercaseString containsString:@"VISA"]) {
        ccbankcodeOfBill = @"Visa";
    }
    if ([ccbankcodeOfBill.uppercaseString containsString:@"MASTER"]) {
        ccbankcodeOfBill = @"Master";
    }
    if ([ccbankcodeOfBill.uppercaseString containsString:@"JCB"]) {
        ccbankcodeOfBill = @"JCB";
    }
    for (NSString *ccbankcode in promotion.paymentConditions.useconditionccbankcodes) {
        if ([ccbankcode.uppercaseString isEqualToString:ccbankcodeOfBill.uppercaseString]) {
            promotion.paymentConditions.currentCCBankcode = ccbankcode;
            return true;
        }
    }
    return false;
}

// Bankcode Condition
- (BOOL) conditionBankCodeListOfPromotion:(ZPPromotion *)promotion forBill:(ZPBill *) bill {
    if (promotion.paymentConditions.useconditionbankcodes.count == 0 ) {
        return true;
    }
    NSString *bankcodeOfBill = bill.currentPaymentMethod.bankCode;
    for (NSString *bankcode in promotion.paymentConditions.useconditionbankcodes) {
        if ([bankcode.uppercaseString isEqualToString:bankcodeOfBill.uppercaseString]) {
            promotion.paymentConditions.currentBankcode = bankcode;
            return true;
        }
    }
    return false;
}

- (NSArray <ZPPromotion *>*)filterAvailablePromotionsToEnablePaymentMethod:(NSArray *)allPromotions forBill:(ZPBill *)bill {
    NSMutableArray <ZPPromotion *>*_return = [NSMutableArray array];
    for (ZPPromotion *promotion in allPromotions) {
        if ([self conditionForPaymentMethod:promotion forBill:bill] && [self conditionMinMaxPPAmount:promotion forBill:bill] == ZPVoucherConditionPPAmountCorrect) {
            [_return addObject:promotion];
        }
        
    }
    return _return;
}

- (NSArray <ZPPromotion *>*)filterAvailablePromotionsWhenSelectedPaymentMethod:(NSArray *)allPromotions forBill:(ZPBill *)bill {
    NSMutableArray <ZPPromotion *>*_return = [NSMutableArray array];
    for (ZPPromotion *promotion in allPromotions) {
        if ([self conditionForPaymentMethod:promotion forBill:bill]) {
            promotion.paymentConditions.voucherConditionPPAmount = [self conditionMinMaxPPAmount:promotion forBill:bill];
            promotion.isDisable = promotion.paymentConditions.voucherConditionPPAmount != ZPVoucherConditionPPAmountCorrect;
            NSString *bankCode = bill.currentPaymentMethod.bankCode;
            if ([bankCode.uppercaseString containsString:@"VISA"]||
                [bankCode.uppercaseString containsString:@"MASTER"]||
                [bankCode.uppercaseString containsString:@"JCB"]) {
                bankCode = @"CC";
            }
            ZPChannel *channel = [[ZPDataManager sharedInstance] channelWithTypeAndBankCode:bill.currentPaymentMethod.methodType bankCode:bankCode];
            
            int pmcidOfBill = channel.channelID;
            promotion.paymentConditions.currentPMCID = pmcidOfBill;
            [_return addObject:promotion];
        }
    }
    return _return;
}

- (ZPVoucherConditionPPAmount) conditionMinMaxPPAmount:(ZPPromotion *)promotion forBill:(ZPBill *) bill {
    NSString *bankCode = bill.currentPaymentMethod.bankCode;
    if ([bankCode containsString:stringCreditCardBankCode]
        ||  [bankCode containsString:VISA]
        ||  [bankCode containsString:MASTER]
        ||  [bankCode containsString:JCB]
        ) {
        bankCode = stringCreditCardBankCode;
    }
    ZPChannel *channel = [[ZPDataManager sharedInstance] channelWithTypeAndBankCode:bill.currentPaymentMethod.methodType bankCode:bankCode];
    long long promotionValue = [self getValueCurrencyFromPromotion:promotion ofBill:bill];
    long long totalCharge = bill.amount + bill.currentPaymentMethod.totalChargeFee - promotionValue;
    long maxAmount = bill.currentPaymentMethod.methodType == ZPPaymentMethodTypeZaloPayWallet ? MIN([ZaloPayWalletSDKPayment sharedInstance].appDependence.currentBalance, channel.maxPPAmount) : channel.maxPPAmount;
    if (totalCharge < channel.minPPAmount) {
        return ZPVoucherConditionPPAmountLessThanMinPPAmount;
    }
    if (totalCharge > maxAmount) {
        return ZPVoucherConditionPPAmountGreaterThanMaxPPAmount;
    }
    return ZPVoucherConditionPPAmountCorrect;
}


# pragma mark - Best Promotion
- (long long) getValueCurrencyFromPromotion:(ZPPromotion *)promotion ofBill:(ZPBill *)bill {
    long long valueCurrency = promotion.discounttype == ZPPromotionTypePercent ? promotion.discountpercent : promotion.discountamount;
    if (promotion.discounttype == ZPPromotionTypePercent) {
        valueCurrency *= bill.amount/100;
        if (promotion.cap > 0 && valueCurrency > promotion.cap) {
            valueCurrency = promotion.cap;
        }
    }
    return valueCurrency;
}

- (ZPPromotion *)getBestPromotionFromPromotions:(NSArray *)promotions ofBill:(ZPBill *)bill {
    ZPPromotion *bestPromotion = promotions.firstObject;
    long long bestPromotionValue = [self getValueCurrencyFromPromotion:bestPromotion ofBill:bill];
    for (ZPPromotion *promotion in promotions) {
        long long promotionValue = [self getValueCurrencyFromPromotion:promotion ofBill:bill];
        if (bestPromotionValue < promotionValue) {
            bestPromotion = promotion;
            bestPromotionValue = promotionValue;
        }
    }
    return bestPromotion;
}

# pragma mark - revert promotion
- (void)promotionsWillRevertAddPromotion:(ZPPromotionInfo *)promotionInfo {
    if (![promotionInfo isKindOfClass:[ZPPromotionInfo class]]) {
        return;
    }
    @synchronized (self.promotionsWillRevert) {
        if ([self.promotionsWillRevert containsObject:promotionInfo]) {
            return;
        }
        [self.promotionsWillRevert addObject:promotionInfo];
    }
}

- (void)revertPromotionsAndUsePromotion:(ZPPromotionInfo *)promotionUsed {
    @synchronized (self.promotionsWillRevert) {
        if ([promotionUsed isKindOfClass:[ZPPromotionInfo class]]) {
            [self.promotionsWillRevert removeObject:promotionUsed];
        }
        if ([self.promotionsWillRevert count] == 0) {
            return;
        }
        for (ZPPromotionInfo *promotionInfo in self.promotionsWillRevert) {
            [self revertPromotion:promotionInfo];
        }
        [self.promotionsWillRevert removeAllObjects];
    }
}
- (void)revertPromotion:(ZPPromotionInfo *)promotion {
    [[ZPPaymentManager revertPromotionInfo:promotion] subscribeNext:^(id  _Nullable x) {
        
    }];
}


@end
