//
//  ZaloPaySDKUtil.m
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 7/11/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZaloPaySDKUtil.h"
#import "ZPPaymentMethod.h"
#import "ZPBill.h"
#import "ZaloPayWalletSDKPayment.h"
#import "ZPBillExtraInfoData.h"

@implementation ZaloPaySDKUtil

+ (NSArray<ZPBillExtraInfo*> *)extInfoFromString:(NSString *)item {
    if (![item isKindOfClass:[NSString class]]) {
        return nil;
    }
    NSDictionary *data = [item JSONValue];
    if (![data isKindOfClass:[NSDictionary class]]) {
        return nil;
    }
    NSString *ext = [data stringForKey:@"ext"];
    if (ext.length == 0) {
        return nil;
    }
    NSArray *infos = [ext componentsSeparatedByString:@"\t"];
    if (infos.count == 0) {
        return nil;
    }
    NSArray *result = [infos map:^ZPBillExtraInfo*(NSString *aInfo) {
        NSRange range = [aInfo rangeOfString:@":"];
        if (range.length == 0 || range.location == aInfo.length - 1) {
            return nil;
        }
        NSString *key = [aInfo substringToIndex:range.location];
        NSString *value = [aInfo substringFromIndex:range.location + 1];
        ZPBillExtraInfo *oneInfo = [[ZPBillExtraInfo alloc] initWithExtraKey:key extValue:value];
        return oneInfo;
    }];
    
    return result;
}

+ (void)writeAppTransIdLog:(ZPBill *)bill {
    id<ZPWalletDependenceProtocol> helper = [ZaloPayWalletSDKPayment sharedInstance].appDependence;
    [helper writeAppTransIdLog:bill];
}

+ (BOOL)isRequirePin:(ZPBill *)bill method:(ZPPaymentMethod*)method {
    if ((bill.amount + method.totalChargeFee) <= method.amountRequireOTP) {
        return  (method.inamounttype == ZPRequirePinEnableOption1 || method.inamounttype == ZPRequirePinEnableOption3);
    }
    return  (method.overamounttype == ZPRequirePinEnableOption1 || method.overamounttype == ZPRequirePinEnableOption3);
}

@end
