//
//  ZPDataManager+ObserverNewCard.h
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/23/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPDataManager.h"

@interface ZPDataManager (ObserverNewCard)
- (void)startObserverNewCard;
@end
