//
//  ZPSmartLinkHelper.m
//  ZPSDK
//
//  Created by Nguyen Xuan Phung on 1/12/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPSmartLinkHelper.h"

#import "ZPConfig.h"
#import "ZPDataManager.h"
#import "NSString+ZPExtension.h"
#import "ZaloPayWalletSDKLog.h"

@implementation ZPSmartLinkHelper

#pragma mark - Privates
- (void)evaluateJs:(NSString *)js injectFile:(NSString *)file {
    NSString * jsPath = [[ZPDataManager sharedInstance].bundle pathForResource:file ofType:@".js"];
    
   // NSString * jsPath = [[NSBundle mainBundle] pathForResource:file ofType:@".js"];
    NSString * jsString = [NSString stringWithContentsOfFile:jsPath
                                                    encoding:NSUTF8StringEncoding
                                                       error:nil];
    jsString = [jsString stringByAppendingString:js];
    
    DDLogInfo(@"javascript - %@ : %@", file, jsString );
    
    [self.webview stringByEvaluatingJavaScriptFromString:jsString];
}

- (NSString*)getJs:(NSString*)js injectFile:(NSString*)file{
    NSString * jsPath = [[ZPDataManager sharedInstance].bundle pathForResource:file ofType:@".js"];
    
    //    NSString * jsPath = [[NSBundle mainBundle] pathForResource:file ofType:@".js"];
    
    NSString * jsString = [NSString stringWithContentsOfFile:jsPath
                                                    encoding:NSUTF8StringEncoding
                                                       error:nil];
    jsString = [jsString stringByAppendingString:js];
    DDLogInfo(@"javascript - %@ : %@", file, jsString );
    return jsString;
}

#pragma mark - Smartlink
- (BOOL)isSmartLinkSkippable:(NSString *) url{
    //https:://www.vietcombank.com.vn/VCBOnlineServices/VCB_Payment/_ScriptLibrary/TermsConditions.htm
    NSString *exAtmCallback = @"(.*vietcombank\\.com\\.vn.*VCBOnlineServices.*VCB_Payment.*_ScriptLibrary.*TermsConditions.*)";
    return [NSRegularExpression checkRegularExpression:exAtmCallback url:url];
}

- (BOOL)isMessageErrorUrl:(NSString *) url{
    //https:://payment.smartlink.com.vn/message.do
    NSString * regex = @"(.*payment\\.smartlink\\.com\\.vn.*message\\.do.*)";
    return [NSRegularExpression checkRegularExpression:regex url:url];
}
- (BOOL) isSmartLinkOTP:(NSString *) url{
    //https:://payment.smartlink.com.vn/otp.do?method=ws
    NSString *expOtp = @"(.*payment\\.smartlink\\.com\\.vn.*otpcode\\.do.*method.*)|(.*payment\\.smartlink\\.com\\.vn.*otp\\.do.*method.*)";
    return [NSRegularExpression checkRegularExpression:expOtp url:url];
}

- (BOOL) isBanketOTP:(NSString *) url{
    //https:://123pay.vn/bank/otp.BANKNET.123P1601284210631.MOBILEPAYMENT.3697036ae193990fe150a2e3ad6fce53.1453963904.html
    NSString *expOtp = @"^.+(123pay\\.vn\\/bank\\/otp).*$";
    return [NSRegularExpression checkRegularExpression:expOtp url:url];
}

- (BOOL) isVicomDirect:(NSString *) url{
    //https:://www.vietcombank.com.vn/VCBOnlineServices/VCBPaymentGateWayP1/SignIn.aspx
    NSString *expOtp = @"(.*vietcombank.*SignIn\\.aspx.*)";
    return [NSRegularExpression checkRegularExpression:expOtp url:url];
}

- (BOOL) isOnePayFailLoadURL:(NSString*) url{
    //https:://onepay.vn/onecomm-pay/vpcpost.op
    NSString *expOtp = @".*onepay\\.vn\\/onecomm-pay\\/vpcpost\\.op";
    return [NSRegularExpression checkRegularExpression:expOtp url:url];
}

- (BOOL)isAtmCallback:(NSString *) url{
    NSString *exAtmCallback = @"(.*zalopay.*atmfinish.*)";
    return [NSRegularExpression checkRegularExpression:exAtmCallback url:url];
}
- (BOOL)isOtpCallback:(NSString *)url{
    NSString *expOtp = @"(.*otpcallback.*)";
    return [NSRegularExpression checkRegularExpression:expOtp url:url];
}

- (BOOL)isCaptchaCallback:(NSString *)url {
    //https:://ebanking.dongabank.com.vn/epayment_info?
    NSString *exp = @"(.*captchacallback.*)";
    return [NSRegularExpression checkRegularExpression:exp url:url];
}

- (BOOL)isAtmError:(NSString *)url {
    //https:://atm-error.com
    NSString *exp = @"(.*sdk-atm-error\\.com.*)";
    return [NSRegularExpression checkRegularExpression:exp url:url];
}

- (BOOL) isOnePayURL:(NSString*) url{
    //https:://onepay.vn/
    NSString *expOtp = @".*onepay\\.vn\\/.*";
    return [NSRegularExpression checkRegularExpression:expOtp url:url];
}

- (void)sml_RequestCaptcha {
    [self evaluateJs:@"znp_sml_request_captcha();" injectFile:@"znp_sml_request_captcha"];
}


#pragma mark - VCBank
- (BOOL)vcb_IsOtpUrl:(NSString *)url{
    //https:://www.vietcombank.com.vn/VCBOnlineServices/VCB_Payment/VPSC.aspx
    NSString *exAtmCallback = @"(.*vietcombank\\.com\\.vn.*VCBOnlineServices.*VCB_Payment.*VPSC.*)";
    return [NSRegularExpression checkRegularExpression:exAtmCallback url:url];
}

- (BOOL)vcb_IsLoginUrl:(NSString *)url{
    //https:://www.vietcombank.com.vn/VCBOnlineServices/VCB_Payment/VPS.aspx?id=7b4783
    NSString *exAtmCallback = @"(.*vietcombank.com\\.vn.*VCBOnlineServices.*VCB_Payment.*VPS.*)";
    return [NSRegularExpression checkRegularExpression:exAtmCallback url:url];
}

- (BOOL)vcb_ErrorUrl:(NSString *) url{
    NSString *exAtmCallback = @"(.*vietcombank\\.com\\.vn.*VCBOnlineServices.*VCB_Payment.*Err.*)";
    return [NSRegularExpression checkRegularExpression:exAtmCallback url:url];
}

- (BOOL)vcb_DirectErrorUrl:(NSString *) url{
    NSString *exAtmCallback = @"(.*vietcombank\\.com\\.vn.*VCBOnlineServices.*VCBPayment.*Error.*)";
    return [NSRegularExpression checkRegularExpression:exAtmCallback url:url];
}

- (void)vcb_DirectRequestCaptcha {
    [self evaluateJs:@"znp_vcb_dr_request_captcha();" injectFile:@"znp_vcb_dr_request_captcha"];
}

- (void)vcb_DirectLoginWithUsername:(NSString *)username
                           password:(NSString *)password
                            captcha:(NSString *)captcha
{
    NSString * js = [NSString stringWithFormat:@"znp_vcb_dr_login('%@','%@','%@');"
                     , username, password, captcha];
    [self evaluateJs:js injectFile:@"znp_vcb_dr_login"];
}

- (NSString*)vcb_DirectGetErrorMessage{
    return [self getJs:@"znp_vcb_dr_get_error_msg();" injectFile:@"znp_vcb_dr_get_error_msg"];
}

- (BOOL)vcb_DirectIsCaptchaUrl:(NSString *)url {
    //https:://vcb-direct-captcha-step.com
    NSString *exp = @"(.*vcb-direct-captcha-step\\.com.*)";
    return [NSRegularExpression checkRegularExpression:exp url:url];
}

- (void)vcb_DirectCheckStep{
    [self evaluateJs:@"znp_vcb_dr_check_step();" injectFile:@"znp_vcb_dr_check_step"];
}

- (BOOL)vcb_DirectIsOtpUrl:(NSString *)url{
    //https:://www.vietcombank.com.vn/VCBOnlineServices/VCBPaymentGateWayP1/VPGA.aspx
    NSString *exAtmCallback = @"(.*VCBPayment.*VPGA.*)";
    return [NSRegularExpression checkRegularExpression:exAtmCallback url:url];
}

- (void)vcb_DirectConfirm:(NSString*)value {
    NSString * js = [NSString stringWithFormat:@"znp_vcb_dr_confirm('%@');",value];
    [self evaluateJs:js injectFile:@"znp_vcb_dr_confirm"];
}

- (NSString*)vcb_DirectConfirmChooseAccount {
    return [self getJs:@"znp_vcb_dr_confirm_choose_account();" injectFile:@"znp_vcb_dr_confirm_choose_account"];
    
}

- (void)vcb_DicrectSubmitOtp:(NSString *)otp {
    NSString * js = [NSString stringWithFormat:@"znp_vcb_dr_submit_otp('%@');",otp];
    [self evaluateJs:js injectFile:@"znp_vcb_dr_submit_otp"];
}



#pragma mark - Vietinbank
- (void)vtb_CheckStep {
    [self evaluateJs:@"znp_vtb_check_step();" injectFile:@"znp_vtb_check_step"];
}

- (void)vtb_RequestCaptcha {
    [self evaluateJs:@"znp_vtb_request_captcha();" injectFile:@"znp_vtb_request_captcha"];
}

- (void)vtb_SubmitCaptcha:(NSString *)captcha {
    NSString * js = [NSString stringWithFormat:@"znp_vtb_submit_captcha('%@');",captcha];
    [self evaluateJs:js injectFile:@"znp_vtb_submit_captcha"];
}

- (void)vtb_SubmitOtp:(NSString *)otp {
    NSString * js = [NSString stringWithFormat:@"znp_vtb_submit_otp('%@');",otp];
    [self evaluateJs:js injectFile:@"znp_vtb_submit_otp"];
}

- (BOOL)vtb_IsCaptchaUrl:(NSString *)url {
    return [url zpContainsString:@"vtb-captcha-step"];
}

- (BOOL)vtb_IsOtpUrl:(NSString *)url {
    return [url zpContainsString:@"vtb-otp-step"];
}

- (BOOL)vtb_IsUrl:(NSString *)url {
    NSString * regex = @"(.*ebanking\\.vietinbank\\.vn.*epayment.*)";
    return [NSRegularExpression checkRegularExpression:regex url:url];
}

#pragma mark BIDV bank
- (void)bidv_CheckStep {
    [self evaluateJs:@"znp_bidv_check_step();" injectFile:@"znp_bidv_check_step"];
}

- (void)bidv_GetCaptcha {
    [self evaluateJs:@"znp_bidv_getcaptchar();" injectFile:@"znp_bidv_getcaptchar"];
}

- (void)bidv_SubmitCaptcha:(NSString *)captcha andPassWord:(NSString *)password{
    NSString * js = [NSString stringWithFormat:@"znp_bidv_submit_captcha('%@', '%@');",password,captcha];
    [self evaluateJs:js injectFile:@"znp_bidv_submit_captcha"];
}

- (void)bidv_SubmitOtp:(NSString *)otp {
    NSString * js = [NSString stringWithFormat:@"znp_bidv_submit_otp('%@');",otp];
    [self evaluateJs:js injectFile:@"znp_bidv_submit_otp"];
}
- (BOOL)bidv_IsCaptchaUrl:(NSString *)url {
    return [url zpContainsString:@"bidv-captcha-step"];
}
- (BOOL)bidv_IsOtpUrl:(NSString *)url {
    return [url zpContainsString:@"bidv-otp-step"];
}


- (BOOL)bidv_IsUrl:(NSString *)url {
    //  NSString * regex = @"(.*ebanking\\.vietinbank\\.vn.*epayment.*)";
    NSString * regex = @"(.*bidv\\.net.*EWALLET.*)";
    BOOL ret = [NSRegularExpression checkRegularExpression:regex url:url];
    return ret;
}
-(NSString *)bidv_GetError{
    NSString *msg = @"";
    NSString *func = [self getJs:@"znp_bidv_geterror();" injectFile:@"znp_bidv_geterror"];
    msg = [self.webview stringByEvaluatingJavaScriptFromString:func];
    return msg;
}
-(BOOL)bidv_checkLoading{
    NSString *msg = nil;
    NSString *func = [self getJs:@"znp_bidv_checkLoading();" injectFile:@"znp_bidv_checkLoading"];
    msg = [self.webview stringByEvaluatingJavaScriptFromString:func];
    DDLogInfo(@"check loading %@",msg);
    return [msg boolValue];
}
- (void)bidv_ClosePopup{
    NSString *func = [self getJs:@"znp_bidv_close_popup();" injectFile:@"znp_bidv_close_popup"];
    [self.webview stringByEvaluatingJavaScriptFromString:func];
}
-(void)bidv_Cancel{
    NSString * jsPath = [[NSBundle mainBundle] pathForResource:@"znp_bidv_cancel" ofType:@".js"];
    
    NSString * jsString = [NSString stringWithContentsOfFile:jsPath
                                                    encoding:NSUTF8StringEncoding
                                                       error:nil];
    jsString = [jsString stringByAppendingString:@"znp_bidv_cancel();"];
    [self.webview stringByEvaluatingJavaScriptFromString:jsString];
}
-(void)bidv_Inject_Reloadcaptcha{
    NSString *func = [self getJs:@"" injectFile:@"znp_bidv_inject_reloadcaptcha"];
    
    [self.webview stringByEvaluatingJavaScriptFromString:func];
}
-(void)bidv_Inject_submitCaptchaSusscess{
    NSString *func = [self getJs:@"" injectFile:@"znp_bidv_inject_submitCaptchaSusscess"];
    
    [self.webview stringByEvaluatingJavaScriptFromString:func];
}

#pragma mark iBanking
- (BOOL)isIbankingError:(NSString *)url {
    //https:://atm-error.com
    NSString *exp = @"(.*sdk-ibanking-error\\.com.*)";
    return [NSRegularExpression checkRegularExpression:exp url:url];
}
- (NSString*)getJsFunc:(NSString*)funcName injectFile:(NSString*)fileName{
    return  [self getJs:funcName injectFile:fileName];
}
//VCB
- (void)iBanking_VCB_LoginWithUsername:(NSString *)username
                              password:(NSString *)password
                               captcha:(NSString *)captcha
{
    NSString * js = [NSString stringWithFormat:@"zp_ibanking_VCB_Login('%@','%@','%@');"
                     , username, password, captcha];
    [self evaluateJs:js injectFile:@"zp_ibanking_VCB_Login"];
}
-(NSString * )iBanking_VCB_Loading{
    NSString * func = [self getJsFunc:@"zp_ibanking_VCB_checkLoading();" injectFile:@"zp_ibanking_VCB_checkLoading"];
    NSString *checking = [self.webview stringByEvaluatingJavaScriptFromString:func];
    return checking;
}
-(NSString *)iBanking_VCB_Step{
    NSString * func = [self getJsFunc:@"zp_ibanking_VCB_check_step();" injectFile:@"zp_ibanking_VCB_check_step"];
    NSString *step = [self.webview stringByEvaluatingJavaScriptFromString:func];
    return step;
}
- (NSString*)iBanking_VCB_GetErrorMessage{
    NSString *errorString = @"";
    errorString = [self.webview stringByEvaluatingJavaScriptFromString:[self getJs:@"zp_ibanking_VCB_GetErrorMessage();" injectFile:@"zp_ibanking_VCB_GetErrorMessage"]];
    return errorString;
}
- (void)iBanking_VCB_GetCaptchar {
    [self evaluateJs:@"zp_ibanking_VCB_GetCaptchar();" injectFile:@"zp_ibanking_VCB_GetCaptchar"];
}
-(NSMutableArray *)iBanking_VCB_GetListAccountNumber{
    NSMutableArray *listAccountNumber = [NSMutableArray new];
    
    NSString *func = [self getJs:@"zp_ibanking_VCB_choose_account();" injectFile:@"zp_ibanking_VCB_choose_account"];
    NSString *temp = [self.webview stringByEvaluatingJavaScriptFromString:func];
    NSArray *optionArray = [temp componentsSeparatedByString:@","];
    for (NSString *value in optionArray) {
        if (value.length > 0) {
            if (![value containsString:@"Chọn"]) {
                [listAccountNumber addObject:value];
            }
        }
    }
    return listAccountNumber;
}
-(NSMutableArray *)iBanking_VCB_GetListZaloPayID{
    NSMutableArray *listAccountNumber = [NSMutableArray new];
    NSString *func = [self getJs:@"zp_ibanking_VCB_get_list_id();" injectFile:@"zp_ibanking_VCB_get_list_id"];
    NSString *temp = [self.webview stringByEvaluatingJavaScriptFromString:func];
    NSArray *optionArray = [temp componentsSeparatedByString:@","];
    for (NSString *value in optionArray) {
        if (value.length > 0) {
            if (![value containsString:@"Chọn"]) {
                [listAccountNumber addObject:value];
            }
        }
    }
    return listAccountNumber;
}
-(void)iBanking_VCB_LinkWalletSubmitInfo:(NSString *)accountNumber zaloPayId:(NSString *)zaloPayId captchar:(NSString *)capt{
    NSString * js = [NSString stringWithFormat:@"zp_ibanking_VCB_LinkWallet_submit_info('%@','%@','%@');"
                     ,accountNumber , zaloPayId, capt];
    [self evaluateJs:js injectFile:@"zp_ibanking_VCB_LinkWallet_submit_info"];
    
}
-(void)iBanking_VCB_LinkWalletSubmitOTP:(NSString *)otp{
    NSString * js = [NSString stringWithFormat:@"zp_ibanking_VCB_submit_otp('%@');"
                     ,otp];
    [self evaluateJs:js injectFile:@"zp_ibanking_VCB_submit_otp"];
    
}
-(void)iBanking_VCB_SelectZaloPay{
    [self evaluateJs:@"selectZaloPay();" injectFile:@"zp_ibanking_VCB_SelectZaloPay"];
}
-(void)iBanking_VCB_CancelWalletWithPassWord:(NSString *)passWord{
    NSString * js = [NSString stringWithFormat:@"zp_ibanking_VCB_cancelEWallet_confirm('%@');"
                     ,passWord];
    [self evaluateJs:js injectFile:@"zp_ibanking_VCB_cancelEWallet_confirm"];
 
}
-(NSString *)iBanking_VCB_UnlinkStep{
    NSString * func = [self getJsFunc:@"zp_ibanking_VCB_cancel_checkStep();" injectFile:@"zp_ibanking_VCB_cancel_checkStep"];
    NSString *step = [self.webview stringByEvaluatingJavaScriptFromString:func];
    return step;
}
-(void)iBanking_VCB_RefreshCaptCha{
     [self evaluateJs:@"zp_ibanking_VCB_RefreshCaptcha();" injectFile:@"zp_ibanking_VCB_RefreshCaptcha"];   
}
-(NSBundle *)getBundle{
    //return [NSBundle mainBundle];
    return [ZPDataManager sharedInstance].bundle;
}

@end
