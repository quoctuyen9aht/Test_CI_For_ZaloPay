//
//  ZPPaymentManager.m
//  ZPSDK
//
//  Created on 12/22/15.
//  Copyright © 2015-present VNG Corporation. All rights reserved.
//

#import "ZPPaymentManager.h"
#import "UIDevice+ZPExtension.h"
#import "ZaloPayWalletSDKPayment.h"
#import "ZPBill.h"
#import "ZPDefine.h"
#import "ZPPaymentChannel.h"
#import "ZPConfig.h"
#import "ZPPaymentInfo.h"
#import "ZPDataManager.h"
#import "NSString+ZPExtension.h"
#import "ZPPaymentResponse.h"
#import "ZPResourceManager.h"
#import "ZPOTPResponse.h"
#import "ZPAtmResponseObject.h"
#import "ZPCreditCardResponseObject.h"
#import "ZPZaloPayWalletResponseObject.h"
#import "NetworkManager+ZaloPayWalletSDK.h"
#import "NSData+ZPExtension.h"
#import "ZPSDKStringConstants.h"
#import "ZaloPayWalletSDKLog.h"
#import <ZaloPayWalletSDK/ZaloPayWalletSDK-Swift.h>
#import <ZaloPayNetwork/ZPNetWorkApi.h>
#import <ZaloPayNetwork/NSError+ZaloPayAPI.h>
#import "ZPMapCardLog.h"
#import "ZPVoucherInfo.h"
#import "ZPVoucherHistory.h"
#import "ZaloPayWalletSDKPayment.h"
#define kGetTranStatusTimeOut                30.0

extern NSString *kUrlVoucher;

@interface ZPPaymentManager()
@property(nonatomic) int paymentChannelId;
@property(nonatomic, strong) ZPPaymentInfo * sPaymentInfo;
@property (nonatomic, copy) void (^paymentCallback)(ZPPaymentResponse* responseObject);
@end


@implementation ZPPaymentManager

- (void)dealloc {
    DDLogInfo(@"-[%@ dealloc]", NSStringFromClass([self class]));
}

#pragma mark - Class methods

+ (void)paymentWithBill:(ZPBill *)bill
         andPaymentInfo:(ZPPaymentInfo *)paymentInfo
               andAppId:(NSInteger)appId
                channel:(ZPChannel*)channel
                 inMode:(ZPPaymentMethodType)mode
            andCallback:(void (^)(ZPPaymentResponse *responseObject))callback {
    ZPPaymentManager * handler = [[ZPPaymentManager alloc] init];
    [handler paymentWithBill:bill
                 paymentInfo:paymentInfo
                       appId:appId
                     channel:channel
                      inMode:mode
                 andCallback:callback];
}

- (void)paymentWithBill:(ZPBill*)bill
            paymentInfo:(ZPPaymentInfo*)paymentInfo
                  appId:(NSInteger)appId
                channel:(ZPChannel*)channel
                 inMode:(ZPPaymentMethodType)mode
            andCallback:(void (^)(ZPPaymentResponse* responseObject))callback {
    
    self.sPaymentInfo = paymentInfo;
    self.paymentChannelId = channel.channelID;
    NSMutableDictionary *params = [self paramsWithBill:bill andChannelId:channel.channelID];
    if (paymentInfo && [paymentInfo conformsToProtocol:@protocol(KYCGetInforProtocol)]) {
        NSDictionary *kycInfor = [NSDictionary castFrom:[paymentInfo performSelector:@selector(kycInfor)]];
        if (kycInfor) {
            [params addEntriesFromDictionary:kycInfor];
        }
    }
    
    
    switch (mode) {
        /*! ATM CARD */
        case ZPPaymentMethodTypeAtm:{
            [self paymentATMWithBill:bill
                             atmCard:(ZPAtmCard *)paymentInfo
                             channel:channel
                              params:params
                         andCallback:callback];
            break;
        }
        //Credit Card
        case ZPPaymentMethodTypeCreditCard: {
            [self paymentCreditCardWithBill:bill
                                 creditCard:(ZPCreditCard *)paymentInfo
                                    channel:channel
                                     params:params
                                andCallback:callback];
            break;
        }
        default:
            //Check for this because it doesn't complete block
#ifdef DEBUG
            NSAssert(NO, @"Check Logic!!!");
#endif
            break;
    }
}

- (NSMutableDictionary *)paramsWithBill:(ZPBill *)bill andChannelId:(int)channelID {
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    UIDevice * device = [UIDevice currentDevice];
    NSString *deviceId = [[ZaloPayWalletSDKPayment sharedInstance].appDependence getDeviceId];
    [params setObject:@(bill.ordersource) forKey:@"ordersource"];
    [params setObjectCheckNil:@(bill.appId) forKey:@"appid"];
    [params setObjectCheckNil:bill.appTransID forKey:@"apptransid"];
    [params setObjectCheckNil:bill.appUser forKey:@"appuser"];
    [params setObjectCheckNil:@(bill.time) forKey:@"apptime"];
    [params setObjectCheckNil:bill.items forKey:@"item"];
    [params setObjectCheckNil:bill.billDescription forKey:@"description"];
    [params setObjectCheckNil:bill.embedData forKey:@"embeddata"];
    [params setObjectCheckNil:@(bill.amount) forKey:@"amount"];
    [params setObjectCheckNil:@"ios" forKey:@"platform"];
    [params setObjectCheckNil:bill.mac forKey:@"mac"];
    [params setObjectCheckNil:@(channelID) forKey:@"pmcid"];
    [params setObjectCheckNil:deviceId forKey:@"deviceid"];
    [params setObjectCheckNil:[device zpMobileNetworkCode] forKey:@"mno"];
    [params setObjectCheckNil:SDK_BUILD_VERSION forKey:@"sdkver"];
    [params setObjectCheckNil:[device zpOsVersion] forKey:@"osver"];
    [params setObjectCheckNil:[device zpDeviceModel] forKey:@"devicemodel"];
    [params setObjectCheckNil:[ZaloPayWalletSDKPayment sharedInstance].network.connectionType forKey:@"conntype"];
    [params setObjectCheckNil:[NSNumber numberWithInt:(int)bill.transType] forKey:@"transtype"];
    [params setObjectCheckNil:[bill.appUserInfo stringForKey:@"zaloid"] forKey:@"zaloid"];
    [params setObjectCheckNil:@([[bill.appUserInfo objectForKey:@"longitude"] doubleValue]) forKey:@"longitude"];
    [params setObjectCheckNil:@([[bill.appUserInfo objectForKey:@"latitude"] doubleValue]) forKey:@"latitude"];
    if (bill.voucherInfo) {
        NSString *info = [self paramsFromVoucher:bill.voucherInfo];
        [params setObjectCheckNil:info forKey:@"voucherinfo"];
    }
    if (bill.promotionInfo) {
        NSString *info = [self paramsFromPromotionInfo:bill.promotionInfo orVoucherInfo:bill.voucherInfo];
        [params setObjectCheckNil:info forKey:@"promotioninfo"];
    }
    return params;
}

- (NSString *)paramsFromVoucher:(ZPVoucherInfo *)voucher {
    NSMutableDictionary* voucherInfo = [[NSMutableDictionary alloc] init];
    [voucherInfo setObjectCheckNil:voucher.voucherCode forKey:@"vouchercode"];
    [voucherInfo setObjectCheckNil:@(voucher.campaignId) forKey:@"campaignid"];
    [voucherInfo setObjectCheckNil:@(voucher.discountAmount) forKey:@"discountamount"];
    [voucherInfo setObjectCheckNil:@(voucher.useVoucheTime) forKey:@"usevouchertime"];
    [voucherInfo setObjectCheckNil:voucher.voucherSig forKey:@"vouchersig"];
    return [voucherInfo JSONRepresentation];
}
- (NSString *)paramsFromPromotionInfo:(ZPPromotionInfo *)promotionInfo orVoucherInfo:(ZPVoucherInfo *)voucherInfo {
    NSMutableDictionary* promotionInfoReturn = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* data = [[NSMutableDictionary alloc] init];
    
    if (promotionInfo) {
        [promotionInfoReturn setObjectCheckNil:@(ZPEndowTypePromotion) forKey:@"promotiontype"];
        [data setObjectCheckNil:promotionInfo.promotionUsed.campaigncode forKey:@"campaigncode"];
        [data setObjectCheckNil:@(promotionInfo.promotionUsed.campaignId) forKey:@"campaignid"];
        [data setObjectCheckNil:@(promotionInfo.discountamount) forKey:@"discountamount"];
        [data setObjectCheckNil:promotionInfo.promotionUsed.promotiontransid forKey:@"promotiontransid"];
        [data setObjectCheckNil:promotionInfo.promotionsig forKey:@"promotionsig"];
        [promotionInfoReturn setObjectCheckNil:[data JSONRepresentation] forKey:@"data"];
        return [promotionInfoReturn JSONRepresentation];
    }
    if (voucherInfo) {
        [promotionInfoReturn setObjectCheckNil:@(ZPEndowTypeVoucher) forKey:@"promotiontype"];
        data = [[self paramsFromVoucher:voucherInfo] JSONValue];
        [promotionInfoReturn setObjectCheckNil:data forKey:@"data"];
        return [promotionInfoReturn JSONRepresentation];
    }
    return [promotionInfoReturn JSONRepresentation];
}

#pragma mark - Actions based on payment methods
- (void)paymentATMWithBill:(ZPBill*)bill
                   atmCard:(ZPAtmCard*)atmCard
                   channel:(ZPChannel*)channel
                    params:(NSMutableDictionary *)params
               andCallback:(void (^)(ZPPaymentResponse* ZPPaymentResponse))callback {
    
    [params setObjectCheckNil:@(atmCard.amount) forKey:@"amount"];
    
    switch (atmCard.step) {
        case ZPAtmPaymentStepSavedCardConfirm:
        [params setObjectCheckNil:bill.data forKey:@"chargeinfo"];
        [self submitTransidWithParams:params bill:bill bankCode:atmCard.bankCode callback:callback];
        break;
        
        case ZPAtmPaymentStepSubmitTrans: {
            NSString *jsonStr = [self atmCardInfo:atmCard];
            if ([self isMapCard:bill]) {
                [params setObjectCheckNil:jsonStr forKey:@"cardinfo"];
                [self verifyCardWithParams:params bill:bill bankCode:atmCard.bankCode callback:callback];
            } else if ([self isMapBankAccount:bill]) {
                [params setObjectCheckNil:bill.bankCode forKey:@"bankcode"];
                [params setObjectCheckNil:bill.bankCustomerId forKey:@"bankcustomerid"];
                [self verifyBankAccountWithParams:params bill:bill bankCode:atmCard.bankCode callback:callback];
            } else {
                [params setObjectCheckNil:jsonStr forKey:@"chargeinfo"];
                [self submitTransidWithParams:params bill:bill bankCode:atmCard.bankCode callback:callback];
            }
        }
        break;
        case ZPAtmPaymentStepGetTransStatus:
        [self updatePaymentStatusWithTransID:atmCard.zpTransID
                                     andBill:bill
                                     andStep:ZPPaymentStepGetPaymentResult
                                 andBankCode:atmCard.bankCode
                                 andCallback:callback];
        break;
        
        case ZPAtmPaymentStepAtmAuthenPayer:
        {
            [params setObjectCheckNil:atmCard.zpTransID forKey:@"zptransid"];
            [params setObjectCheckNil:atmCard.otpType forKey:@"authentype"];
            [params setObjectCheckNil:atmCard.authenValue forKey:@"authenvalue"];
            if ([self isMapCard:bill]) {
                [self authCardHolderWithPmc:channel bill:bill andBankCode:atmCard.bankCode andParams:params andCallback:callback];
            }else {
                [self authenpayerWithPmc:channel bill:bill andBankCode:atmCard.bankCode andParams:params andCallback:callback];
            }
        }
        break;
        
        case ZPAtmPaymentStepSubmitPurchase:
        [params setObjectCheckNil:bill.data forKey:@"chargeinfo"];
        [params setObjectCheckNil:atmCard.pin forKey:@"pin"];
        [self submitTransidWithParams:params bill:bill bankCode:atmCard.bankCode callback:callback];
        break;
        
        case ZPAtmPaymentStepGetStatusByAppTransIdForClient: {
            [self getStatusByAppTransid:bill.appTransID appId:bill.appId bankCode:atmCard.bankCode  bill:bill callback:callback];
        }
        break;
        
        default:
            //Check for this because it doesn't complete block
#ifdef DEBUG
            NSAssert(NO, @"Check Logic!!!");
#endif
        break;
    }
}

- (void)paymentCreditCardWithBill:(ZPBill*)bill
                       creditCard:(ZPCreditCard*)creditCard
                          channel:(ZPChannel*)channel
                           params:(NSMutableDictionary *)params
                      andCallback:(void (^)(ZPPaymentResponse* responseObject))callback {
    
    switch (creditCard.step) {
        case ZPCcPaymentStepSavedCardConfirm: {
            [params setObjectCheckNil:bill.data forKey:@"chargeinfo"];
            [self submitTransidWithParams:params bill:bill bankCode:creditCard.ccBankCode callback:callback];
            break;
        }
        case ZPCcPaymentStepFillCardInfo:{
            NSString *jsonStr = [self creditCardInfo:creditCard];
            if ([self isMapCard:bill]) {
                [params setObjectCheckNil:jsonStr forKey:@"cardinfo"];
                [self verifyCardWithParams:params bill:bill bankCode:creditCard.ccBankCode callback:callback];
            } else {
                [params setObjectCheckNil:jsonStr forKey:@"chargeinfo"];
                [self submitTransidWithParams:params bill:bill bankCode:creditCard.ccBankCode callback:callback];
            }
            break;
        }
        case ZPCcPaymentStepUpdate:{
            [self updatePaymentStatusWithTransID:creditCard.zpTransID
                                         andBill:bill
                                         andStep:ZPPaymentStepGetPaymentResult
                                     andBankCode:creditCard.ccBankCode
                                     andCallback:callback];
            break;
            
        }
        case ZPCcPaymentStepSubmitPurchase: {
            [params setObjectCheckNil:bill.data forKey:@"chargeinfo"];
            [params setObjectCheckNil:creditCard.pin forKey:@"pin"];
            [self submitTransidWithParams:params bill:bill bankCode:creditCard.ccBankCode callback:callback];
            break;
        }
        case ZPCreditCardPaymentStepStepGetStatusByAppTransIdForClient: {
            [self getStatusByAppTransid:bill.appTransID appId:bill.appId bankCode:creditCard.ccBankCode bill:bill callback:callback];
            break;
        }
        default:
            //Check for this because it doesn't complete block
#ifdef DEBUG
            NSAssert(NO, @"Check Logic!!!");
#endif
        break;
    }
}

#pragma mark - BankAccount

- (void)bankAccountSubmitTransWithBill:(ZPBill *)bill
                                   pin:(NSString *)pin
                             channelId:(int)channelId
                              bankCode:(NSString *)bankCode
                        firstaccountno:(NSString *)firstaccountno
                         lastaccountno:(NSString *)lastaccountno
                              callback:(void (^)(ZPPaymentResponse* responseObject))callback {
    self.paymentChannelId = channelId;
    NSMutableDictionary *params = [self paramsWithBill:bill andChannelId:channelId];
    NSMutableDictionary* chargeinfo = [[NSMutableDictionary alloc] init];
    [chargeinfo setObjectCheckNil:firstaccountno forKey:@"firstaccountno"];
    [chargeinfo setObjectCheckNil:lastaccountno forKey:@"lastaccountno"];
    [chargeinfo setObjectCheckNil:bankCode forKey:@"bankcode"];
    [params setObjectCheckNil:[chargeinfo JSONRepresentation] forKey:@"chargeinfo"];
    [params setObjectCheckNil:pin forKey:@"pin"];
    [self submitTransidWithParams:params bill:bill bankCode:bankCode callback:callback];
}

#pragma mark - ZaloPayWallet

- (void)zaloPayWalletSubmitTransidWithBill:(ZPBill *) bill
                                       pin:(NSString *)pin
                                 channelId:(int)channelID
                                  callback:(void (^)(ZPPaymentResponse* responseObject)) callback {
    self.paymentChannelId = channelID;
    NSMutableDictionary *params = [self paramsWithBill:bill andChannelId:channelID];
    [params setObjectCheckNil:pin forKey:@"pin"];
    [self submitTransidWithParams:params bill:bill bankCode:@"" callback:callback];
}

- (void)getStatusByAppTransid:(NSString *)apptransid
                        appId:(NSInteger)appId
                     bankCode:(NSString *)bankCode
                         bill:(ZPBill *)bill
                     callback:(void (^)(ZPPaymentResponse* responseObject))callback {
    if (callback == nil) {
        return;
    }
    
    [[[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [self getStatusByAppTransid:apptransid appId:appId bankCode:bankCode bill:bill startDate:[NSDate date] subscriber:subscriber];
        return nil;
    }] deliverOnMainThread] subscribeNext:^(ZPPaymentResponse *response) {
        callback(response);
    }];
}

- (void)getStatusByAppTransid:(NSString *)apptransid
                        appId:(NSInteger)appId
                     bankCode:(NSString *)bankCode
                         bill:(ZPBill *)bill
                    startDate:(NSDate*)startDate
                   subscriber:(id<RACSubscriber>)subscriber {
    
    NSTimeInterval beginTime = [[NSDate date] timeIntervalSince1970] * 1000;
    [[[ZaloPayWalletSDKPayment sharedInstance] getStatusByaAppTransiId:apptransid
                                                                 appId:appId] subscribeNext:^(NSDictionary *dictionary)
     {
         [ZPPaymentManager trackingApptransId:api_v001_tpe_getstatusbyapptransidforclient
                                    beginTime:beginTime
                                   returnCode:kZPZaloPayCreditErrorCodeNoneError];
         
         ZPPaymentResponse* responseObject = [[ZPPaymentResponse alloc] init];
         responseObject.zpTransID = [dictionary stringForKey:@"zptransid"];
         responseObject.errorCode = kZPZaloPayCreditErrorCodeNoneError;
         responseObject.exchangeStatus = kZPZaloPayCreditStatusCodeSuccess;
         responseObject.message = nil;
         responseObject.errorStep = ZPErrorStepNonRetry;
         
         [subscriber sendNext:responseObject];
         [subscriber sendCompleted];
         
     } error:^(NSError *error) {
         
         BOOL apiError = [error isApiError];
         
         // error code từ BE hoặc INT32_MAX
         int errorCode = apiError ? (int)error.code : INT32_MAX;
         [ZPPaymentManager trackingApptransId:api_v001_tpe_getstatusbyapptransidforclient beginTime:beginTime returnCode:errorCode];
         
         // error từ server && mã lỗi < 1  && mã lỗi != -49 : giao dịch thất bại.
         if (apiError && (errorCode < ZALOPAY_ERRORCODE_SUCCESSFUL) && errorCode != ZALOPAY_ERRORCODE_TRANS_INFO_NOT_FOUND) {
             ZPPaymentResponse* resp = [self reponseObjectFromApiError:error];
             [subscriber sendNext:resp];
             [subscriber sendCompleted];
             return;
         }
         
         // error từ server && mã lỗi > 1 && có zpTransID : gettranstatus
         NSDictionary *data = error.userInfo;
         NSString *zpTransID = [data stringForKey:@"zptransid"];
         if (apiError && errorCode > ZALOPAY_ERRORCODE_SUCCESSFUL && zpTransID.length > 0) {
             [[ZPPaymentManager new] updatePaymentStatusWithTransID:zpTransID
                                                            andBill:bill
                                                            andStep:ZPPaymentStepSubmitTrans
                                                        andBankCode:bankCode
                                                        andCallback:^(ZPPaymentResponse *responseObject) {
                 [subscriber sendNext:responseObject];
                 [subscriber sendCompleted];
            }];
             return;
         }
         
         // hết 30s: hiển thị processing
         if ([ZPPaymentManager isGetBillStatusTimeout:startDate]) {
             [subscriber sendNext:[ZPPaymentResponse timeOutRequestObject]];
             [subscriber sendCompleted];
             return;
         }
         
         // chưa hết 30s tiếp tục gọi lại getStatusByAppTransid.
         [[RACScheduler scheduler] afterDelay:1.0 schedule:^{
             [self getStatusByAppTransid:apptransid appId:appId bankCode:bankCode bill:bill startDate:startDate subscriber:subscriber];
         }];
     }];
}


- (ZPPaymentResponse *)reponseObjectFromApiError:(NSError *)error {
    int errorCode = (int)error.code;
    NSDictionary *data = error.userInfo;
    ZPPaymentResponse* responseObject = [[ZPPaymentResponse alloc] init];
    responseObject.errorCode = [ZPResourceManager convertToFormalErrorCode:errorCode];
    responseObject.originalCode = errorCode;
    responseObject.zpTransID = [data stringForKey:@"zptransid"];
    responseObject.message = [data stringForKey:@"returnmessage"];;
    responseObject.suggestMessage = [data stringForKey:@"suggestmessage"];
    responseObject.suggestAction = [data arrayForKey:@"suggestaction"];
    return responseObject;
}


#pragma mark - Middle Request

- (void)authCardHolderWithPmc:(ZPChannel*)pmc
                         bill:(ZPBill*)bill
                  andBankCode:(NSString*)bankCode
                    andParams:(NSDictionary *) params
                  andCallback:(void (^)(ZPPaymentResponse* responseObject)) callback{
    [self processMiddleRequestWithUrl:api_v001_tpe_authcardholderformapping
                                  pmc:pmc
                                 bill:bill
                          andBankCode:bankCode
                            andParams:params
                          andCallback:callback];
}

- (void)authenpayerWithPmc:(ZPChannel*)pmc
                      bill:(ZPBill*)bill
               andBankCode:(NSString*)bankCode
                 andParams:(NSDictionary *) params
               andCallback:(void (^)(ZPPaymentResponse* responseObject)) callback{
    [self processMiddleRequestWithUrl:api_v001_tpe_atmauthenpayer
                                  pmc:pmc
                                 bill:bill
                          andBankCode:bankCode
                            andParams:params
                          andCallback:callback];
}

#pragma mark - Transaction

- (void)verifyCardWithParams:(NSDictionary *)params
                        bill:(ZPBill *) bill
                    bankCode:(NSString *)bankCode
                    callback:(void (^)(ZPPaymentResponse* responseObject)) callback {
    [self processPaymentRequestWithUrl:api_v001_tpe_verifycardformapping
                             andParams:params
                               andBill:bill
                           andBankCode:bankCode
                           andCallback:callback];
}

- (void)verifyBankAccountWithParams:(NSDictionary *)params
                        bill:(ZPBill *) bill
                    bankCode:(NSString *)bankCode
                    callback:(void (^)(ZPPaymentResponse* responseObject)) callback {
    [self processPaymentRequestWithUrl:api_v001_tpe_verifyaccountformapping
                             andParams:params
                               andBill:bill
                           andBankCode:bankCode
                           andCallback:callback];
}

- (void)submitTransidWithParams:(NSDictionary *)params
                           bill:(ZPBill *) bill
                       bankCode:(NSString *)bankCode
                       callback:(void (^)(ZPPaymentResponse* responseObject)) callback {
    [self processPaymentRequestWithUrl:api_v001_tpe_submittrans
                             andParams:params
                               andBill:bill
                           andBankCode:bankCode
                           andCallback:callback];
}


#pragma mark - Making APIs call

- (void)processMiddleRequestWithUrl:(NSString*) path
                                pmc:(ZPChannel*)pmc
                               bill: (ZPBill*)bill
                        andBankCode:(NSString*)bankCode
                          andParams:(NSDictionary *) params
                        andCallback:(void (^)(ZPPaymentResponse* responseObject)) callback
{
    DDLogInfo(@"processMiddleRequestWithUrl");
    self.paymentCallback = callback;
    NSTimeInterval beginTime = [[NSDate date] timeIntervalSince1970] * 1000;
    [[ZaloPayWalletSDKPayment sharedInstance] sendRequest:path params:params method:@"POST" handler:^(NSDictionary *dictionary, int errorCode) {
        if (dictionary) {
            int errorCode = [dictionary intForKey:@"returncode"];
            [ZPPaymentManager trackingApptransId:path beginTime:beginTime returnCode:errorCode];
            NSString *returnMessage = [dictionary stringForKey:@"returnmessage"];
            NSString *suggestMessage = [dictionary stringForKey:@"suggestmessage"];
            NSArray *suggestAction = [dictionary arrayForKey:@"suggestaction"];
            NSString * zpTransID = [NSString stringWithFormat:@"%llu", [dictionary uint64ForKey:@"zptransid"]];
            
            if(errorCode >= 1) {
            } else {

                [ZPPaymentManager sendRequestErrorWithData:zpTransID bankCode:bankCode exInfo:[NSString stringWithFormat:@"%ld",(long)errorCode] apiPath:path params:params response:dictionary appId:bill.appId];
                
            }
            
            if (errorCode == ZALOPAY_ERRORCODE_TIME_INVALID) {
                ZPPaymentResponse* responseObject =  [ZPPaymentResponse unknownExceptionResponseObject];
                if (returnMessage.length > 0) {
                    responseObject.message = returnMessage;
                    responseObject.suggestMessage = suggestMessage;
                    responseObject.suggestAction = suggestAction;
                }
                responseObject.originalCode = errorCode;
                responseObject.zpTransID = zpTransID;
                [self handlePaymentResponseObject:responseObject];
                return;
            }
            
            if (errorCode == ZALOPAY_ERRORCODE_DUPLICATE_APPTRANSID) {
                ZPPaymentResponse* responseObject =  [ZPPaymentResponse duplicateTransIDResponseObject];
                if (returnMessage.length > 0) {
                    responseObject.message = returnMessage;
                    responseObject.suggestMessage = suggestMessage;
                    responseObject.suggestAction = suggestAction;
                }
                responseObject.originalCode = errorCode;
                responseObject.errorCode = errorCode;
                responseObject.zpTransID = zpTransID;
                [self handlePaymentResponseObject:responseObject];
                return;
            }
            
            ZPPaymentMethodType payType = pmc.channelType;
            if(payType == ZPPaymentMethodTypeAtm|| payType == ZPPaymentMethodTypeIbanking){
                DDLogInfo(@"payType == ZPPaymentMethodTypeAtm");
                ZPAtmCard *cardInfo = (ZPAtmCard*)self.sPaymentInfo;
                if(cardInfo.step == ZPAtmPaymentStepAtmAuthenPayer){
                    
                    [[ZPAppFactory sharedInstance].orderTracking trackVerifyOtp:bill.appTransID transid:zpTransID serverResult:errorCode];
                    
                    DDLogInfo(@"step: atm authen payer");
                    ZPPaymentResponse* responseObject = [[ZPPaymentResponse alloc] init];
                    if (errorCode == kZPZaloPayCreditAuthenErrorCodeNoneError1 ||
                        errorCode == kZPZaloPayCreditAuthenErrorCodeNoneError2 ||
                        [[dictionary objectForKey:@"isprocessing"] boolValue] == YES) {
                        
                        [self updatePaymentStatusWithTransID:zpTransID andBill:bill andStep:ZPPaymentStepSubmitAuthenPayer andBankCode:bankCode andCallback:callback];
                        
                        
                    } else if (errorCode == ZPServerErrorCodeAtmRetryOtp) {
                        ZPPaymentResponse* responseObject =  [ZPPaymentResponse retryRequestObject];
                        responseObject.errorCode = errorCode;
                        responseObject.originalCode = errorCode;
                        if (returnMessage.length > 0) {
                            responseObject.message = returnMessage;
                            responseObject.suggestMessage = suggestMessage;
                            responseObject.suggestAction = suggestAction;
                        }
                        responseObject.zpTransID = zpTransID;
                        [self handlePaymentResponseObject:responseObject];
                        return;
                    } else {
                        if (errorCode == ZPServerErrorCodeNoneError) {
                            responseObject.exchangeStatus = kZPZaloPayCreditStatusCodeSuccess;
                        } else {
                            responseObject.exchangeStatus = kZPZaloPayCreditStatusCodeFail;
                        }
                        responseObject.errorCode = errorCode;
                        responseObject.originalCode = errorCode;
                        responseObject.message = returnMessage;
                        responseObject.suggestMessage = suggestMessage;
                        responseObject.suggestAction = suggestAction;
                        responseObject.errorStep = ZPErrorStepNonRetry;
                        responseObject.zpTransID = zpTransID;
                        responseObject.suggestMessage = suggestMessage;
                        [self handlePaymentResponseObject:responseObject];
                    }
                    
                }
                else{
                    ZPAtmResponseObject* atmResponse = [[ZPAtmResponseObject alloc] init];
                    if(cardInfo.step == ZPAtmPaymentStepSubmitTrans){
                        DDLogInfo(@"step: submit trans");
                        atmResponse.errorCode = [ZPResourceManager convertToFormalErrorCodeSuccessOrFail:errorCode];
                    }
                    atmResponse.originalCode = errorCode;
                    atmResponse.mac = [dictionary stringForKey:@"mac"];
                    atmResponse.zpTransID = zpTransID;
                    atmResponse.message = returnMessage;
                    atmResponse.suggestMessage = suggestMessage;
                    atmResponse.suggestAction = suggestAction;
                    int option = [dictionary intForKey:@"option"];
                    NSString * paySrc = [dictionary stringForKey:@"src"];
                    atmResponse.inputType = [self classifyAtmInputTypeWithOption:option andBankType:paySrc];
                    atmResponse.atmFlag = option;
                    atmResponse.payUrl = [dictionary stringForKey:@"redirecturl"];
                    atmResponse.statusParam =  [atmResponse.statusParam zpStringByAddingQueryDictionary:dictionary];
                    
                    [self handlePaymentResponseObject:(ZPPaymentResponse*)atmResponse];
                    
                }
                
            }
            else if(payType == ZPPaymentMethodTypeCreditCard){
                ZPCreditCardResponseObject * creditResponse = [[ZPCreditCardResponseObject alloc] init];
                
                creditResponse.errorCode = [ZPResourceManager convertToFormalErrorCodeSuccessOrFail:errorCode];
                creditResponse.originalCode = errorCode;
                creditResponse.zpTransID = zpTransID;
                creditResponse.message = returnMessage;
                creditResponse.suggestMessage = suggestMessage;
                creditResponse.suggestAction = suggestAction;
                creditResponse.payUrl = [[dictionary stringForKey:@"redirecturl"] zpRemovePercentEncoding];
                creditResponse.src = [dictionary stringForKey:@"src"];
                
                [self handlePaymentResponseObject:(ZPPaymentResponse*)creditResponse];
                
                
            }
            else if(payType == ZPPaymentMethodTypeZaloPayWallet){
                DDLogInfo(@"payType ZPPaymentMethodTypeZaloPayWallet");
                ZPZaloPayWalletResponseObject *zpwResponse = [[ZPZaloPayWalletResponseObject alloc] init];
                zpwResponse.errorCode = [ZPResourceManager convertToFormalErrorCodeSuccessOrFail:errorCode];
                zpwResponse.originalCode = errorCode;
                zpwResponse.balance = [dictionary intForKey:@"zpwbalance"];
                [self handlePaymentResponseObject:(ZPPaymentResponse*)zpwResponse];
            }
            else{
                DDLogInfo(@"payType ZPOTPResponse");
                ZPOTPResponse* middleResponse = [[ZPOTPResponse alloc] init];
                middleResponse.errorCode = [ZPResourceManager convertToFormalErrorCode:errorCode];
                middleResponse.originalCode = errorCode;
                middleResponse.mac = [dictionary stringForKey:@"receiptmac"];
                middleResponse.zpTransID = zpTransID;
                middleResponse.message = returnMessage;
                middleResponse.suggestMessage = suggestMessage;
                middleResponse.suggestAction = suggestAction;
                middleResponse.chargePhoneNum = [dictionary stringForKey:@"chargephone"];
                NSString * captchaBase64 = [dictionary objectForKey:@"captcha"];
                middleResponse.capcharImage = captchaBase64;
                [self handlePaymentResponseObject:(ZPPaymentResponse*)middleResponse];
            }
        }
        else{
            ZPPaymentResponse* responseObject =  [ZPPaymentResponse timeOutRequestObject];
            [self handlePaymentResponseObject:responseObject];
            
        }
    }];
    
}

- (void)processPaymentRequestWithUrl:(NSString*) path
                           andParams:(NSDictionary *) params
                             andBill:(ZPBill *) bill
                         andBankCode:(NSString *)bankCode
                         andCallback:(void (^)(ZPPaymentResponse* responseObject)) callback
{
    
    self.paymentCallback = callback;
    NSTimeInterval beginTime = [[NSDate date] timeIntervalSince1970] * 1000;
    [[ZaloPayWalletSDKPayment sharedInstance] sendRequest:path
                                          params:params
                                          method:@"POST"
                                         handler:^(NSDictionary *dictionary, int error)
     {
         if(!dictionary) {
             ZPPaymentResponse* responseObject =  [ZPPaymentResponse timeOutRequestObject];
             [self handlePaymentResponseObject:responseObject];
             return;
         }

         int errorCode = [dictionary intForKey:@"returncode"];
         [ZPPaymentManager trackingApptransId:path beginTime:beginTime returnCode:errorCode];
         NSString * message = [dictionary stringForKey:@"returnmessage"];
         NSString * suggestMessage = [dictionary stringForKey:@"suggestmessage"];
         NSArray *suggestAction = [dictionary arrayForKey:@"suggestaction"];
         int originalCode = errorCode;
         
         if(errorCode < 1) {
             [ZPPaymentManager sendRequestErrorWithData:@"" bankCode:bankCode exInfo:[NSString stringWithFormat:@"%ld",(long)errorCode] apiPath:path params:params response:dictionary appId:bill.appId];
         }
         
         NSString * zpTransID = [NSString stringWithFormat:@"%llu", [dictionary uint64ForKey:@"zptransid"]];
         DDLogInfo(@"zpTransID====%@", zpTransID);
         DDLogInfo(@"processPaymentRequestWithUrl returnCode: %d", errorCode);
         
         [[ZPAppFactory sharedInstance].orderTracking trackSubmitTrans:bill.appTransID transid:zpTransID pcmid:self.paymentChannelId serverResult:errorCode];

         if (errorCode == ZALOPAY_ERRORCODE_PIN_NOT_MATCH) {
             errorCode = ZPServerErrorCodeRetryPIN;
         }
         if (errorCode == ZALOPAY_ERRORCODE_TIME_INVALID) {
             ZPPaymentResponse* responseObject =  [ZPPaymentResponse unknownExceptionResponseObject];
             if (message.length > 0) {
                 responseObject.message = message;
                 responseObject.suggestMessage = suggestMessage;
                 responseObject.suggestAction = suggestAction;
             }
             responseObject.originalCode = errorCode;
             responseObject.zpTransID = zpTransID;
             [self handlePaymentResponseObject:responseObject];
             return;
         }
         
         ZPPaymentResponse* responseObject = [[ZPPaymentResponse alloc] init];
         responseObject.zpTransID = zpTransID;
         responseObject.originalCode = originalCode;
         
         if (errorCode > 1 &&
             errorCode != ZPServerErrorCodeAtmRetryOtp &&
             errorCode != ZPServerErrorCodeAtmRetryCaptcha &&
             errorCode != ZPServerErrorCodeRetryPIN) {
             [self updatePaymentStatusWithTransID:zpTransID andBill:bill andStep:ZPPaymentStepSubmitTrans andBankCode: bankCode andCallback:callback];
             return;
         }
         
         if(errorCode == ZALOPAY_ERRORCODE_SUCCESSFUL){
             responseObject.errorCode = kZPZaloPayCreditErrorCodeNoneError;
             responseObject.exchangeStatus = kZPZaloPayCreditStatusCodeSuccess;
             responseObject.message = nil;
             responseObject.errorStep = ZPErrorStepNonRetry;
             responseObject.zpTransID = zpTransID;
             responseObject.balance = [dictionary uint32ForKey:@"balance"];
             responseObject.reqdate = [dictionary uint64ForKey:@"reqdate"];
             [self handlePaymentResponseObject:responseObject];
             return;
         }
         
         if(errorCode == ZPServerErrorCodeAtmRetryOtp) {
             responseObject.errorCode = errorCode;
             responseObject.exchangeStatus = kZPZaloPayCreditStatusCodeFail;
             responseObject.message = nil;
             responseObject.errorStep = ZPErrorStepAbleToRetry;
             responseObject.zpTransID = zpTransID;
             [self handlePaymentResponseObject:responseObject];
             return;
         }
         
         
         responseObject.errorCode = [ZPResourceManager convertToFormalErrorCode:errorCode];
         responseObject.exchangeStatus = kZPZaloPayCreditStatusCodeFail;
         responseObject.message = message;
         responseObject.suggestMessage = suggestMessage;
         responseObject.suggestAction = suggestAction;
         responseObject.errorStep = ZPErrorStepNonRetry;
         responseObject.zpTransID = zpTransID;
         [self handlePaymentResponseObject:responseObject];
     }];
}

- (void)updatePaymentStatusWithTransID:(NSString *)zpTransID
                               andBill:(ZPBill*)bill
                               andStep:(ZPPaymentStep)step
                           andBankCode:(NSString*)bankCode
                           andCallback:(void (^)(ZPPaymentResponse * responseObject))callback {
    
    self.paymentCallback = callback;
    zpTransID = [NSString zpEmptyIfNilString:zpTransID];
    bankCode = [NSString zpEmptyIfNilString:bankCode];
    
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    NSString *deviceId = [[ZaloPayWalletSDKPayment sharedInstance].appDependence getDeviceId];
    [params setObjectCheckNil:zpTransID forKey:@"zptransid"];
    [params setObjectCheckNil:deviceId forKey:@"deviceid"];
    [params setObjectCheckNil:@(bill.appId) forKey:@"appid"];
    
    if ([self isMapCard:bill]) {
        [self getMapCardStatusWithParams:params transID:zpTransID step:step bankCode:bankCode];
        return;
    }
    if ([self isMapBankAccount:bill]) {
        [self getLinkAccountStatusWithParams:params transID:zpTransID step:step bankCode:bankCode];
        return;
    }
    [self getTransactionStatusWithParams:params transID:zpTransID step:step bankCode:bankCode];
}

- (void)getMapCardStatusWithParams:(NSDictionary *)params
                           transID:(NSString *)zpTransID
                              step:(ZPPaymentStep)step
                          bankCode:(NSString *)bankCode{
    
    [self getPaymentStatusWithParams:params
                             transID:zpTransID
                                path:api_v001_tpe_getstatusmapcard
                                step:step
                            bankCode:bankCode
                           startTime:[NSDate date]];
}

- (void)getLinkAccountStatusWithParams:(NSDictionary *)params
                           transID:(NSString *)zpTransID
                              step:(ZPPaymentStep)step
                          bankCode:(NSString *)bankCode{
    
    [self getPaymentStatusWithParams:params
                             transID:zpTransID
                                path:api_v001_tpe_getstatusmapaccount
                                step:step
                            bankCode:bankCode
                           startTime:[NSDate date]];
}

- (void)getTransactionStatusWithParams:(NSDictionary *)params
                               transID:(NSString *)zpTransID
                                  step:(ZPPaymentStep)step
                              bankCode:(NSString *)bankCode {
    [self getPaymentStatusWithParams:params
                             transID:zpTransID
                                path:api_v001_tpe_gettransstatus
                                step:step
                            bankCode:bankCode
                           startTime:[NSDate date]];
    
}
#pragma mark - update payment status

+ (BOOL)isGetBillStatusTimeout:(NSDate *)startTime {
    return [[NSDate date] timeIntervalSinceDate:startTime] > kGetTranStatusTimeOut;
}

- (void)getPaymentStatusWithParams:(NSDictionary *)params
                           transID:(NSString *)zpTransID
                              path:(NSString *)path
                              step:(ZPPaymentStep)step
                          bankCode:(NSString *)bankCode
                         startTime:(NSDate *)startTime {
    NSTimeInterval beginTime = [[NSDate date] timeIntervalSince1970] * 1000;
    [[ZaloPayWalletSDKPayment sharedInstance] sendRequest:path
                                          params:params
                                          method:@"POST"
                                         handler:^(NSDictionary *dictionary, int error)
     {
         if ([dictionary isKindOfClass:[NSDictionary class]]) {
             ZPPaymentResponse *responseObject = [ZPPaymentResponse paymentResponseFrom:dictionary
                                                                                   with:zpTransID
                                                                                     at:step
                                                                               bankCode:bankCode
                                                                                  path :path
                                                                                 params:params
                                                  ];
             
             [ZPPaymentManager trackingApptransId:path beginTime:beginTime returnCode:responseObject.errorCode];
             if (responseObject.shouldHandlePaymentResponse) {
                 [self handlePaymentResponseObject:responseObject];
                 return;
             }
         }
         
         if ([ZPPaymentManager isGetBillStatusTimeout:startTime]) {
             ZPPaymentResponse *response =  [ZPPaymentResponse unidentifiedStatusObject];
             response.zpTransID = zpTransID;
             [self handlePaymentResponseObject:response];
             return;
         }
         [[RACScheduler scheduler] afterDelay:1.0 schedule:^{
             [self getPaymentStatusWithParams:params
                                      transID:zpTransID
                                         path:path
                                         step:step
                                     bankCode:bankCode
                                    startTime:startTime];
         }];
     }];
}

- (void)handlePaymentResponseObject:(ZPPaymentResponse *)responseObject{
    if (self.paymentCallback) {
        self.paymentCallback(responseObject);
        self.paymentCallback = nil;
    }
}

+ (void)writeMapCardLog:(ZPMapCardLog *)log {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:log.transid forKey:@"transid"];
    [params setObjectCheckNil:log.pmcid forKey:@"pmcid"];
    [params setObjectCheckNil:log.atmcaptcha_begindate forKey:@"atmcaptcha_begindate"];
    [params setObjectCheckNil:log.atmcaptcha_enddate forKey:@"atmcaptcha_enddate"];
    [params setObjectCheckNil:log.atmotp_begindate forKey:@"atmotp_begindate"];
    [params setObjectCheckNil:log.atmotp_enddate forKey:@"atmotp_enddate"];
    NSTimeInterval beginTime = [[NSDate date] timeIntervalSince1970] * 1000;
    [[[ZaloPayWalletSDKPayment sharedInstance].network creatRequestWithPath:api_v001_tpe_sdkwriteatmtime
                                                    params:params
                                                     isGet:false] subscribeNext:^(NSDictionary *dictionary) {
        DDLogInfo(@"WRITE_LOG-RESPONSE: %@", dictionary);
        [ZPPaymentManager trackingApptransId:api_v001_tpe_sdkwriteatmtime beginTime:beginTime returnCode:kZPZaloPayCreditErrorCodeNoneError];
    } error:^(NSError *error) {
        DDLogInfo(@"WRITE_LOG error");
        NSDictionary *dictionary = error.userInfo;
        int errorCode = [dictionary intForKey:@"returncode"];
        [ZPPaymentManager trackingApptransId:api_v001_tpe_sdkwriteatmtime beginTime:beginTime returnCode:errorCode];
        
    }];
    
}


+ (void)sendRequestErrorWithData:(NSString *)zptransID
                        bankCode:(NSString*)bankCode
                          exInfo:(NSString *)exinfo
                         apiPath:(NSString *)apiPath
                          params:(NSDictionary *)requestParams
                        response:(NSDictionary *)reponses
                           appId:(NSInteger)appId {
    
    NSMutableDictionary *info = [NSMutableDictionary dictionary];
    [info setObjectCheckNil:apiPath forKey:@"api_path"];
    [info setObjectCheckNil:requestParams forKey:@"params"];
    [info setObjectCheckNil:reponses forKey:@"reponse"];
    
    NSString *strInfo = [info JSONRepresentation];
    [self sendRequestErrorWithData:zptransID bankCode:bankCode exInfo:exinfo exception:strInfo appId:appId];
}


+ (void)sendRequestErrorWithData:(NSString *)zptransID
                        bankCode:(NSString*)bankCode
                          exInfo:(NSString *)exinfo
                       exception:(NSString*)exception
                           appId:(NSInteger)appId{
    
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    NSString *deviceId = [[ZaloPayWalletSDKPayment sharedInstance].appDependence getDeviceId];
    [params setObjectCheckNil:zptransID forKey:@"transid"];
    [params setObjectCheckNil:bankCode forKey:@"bankcode"];
    [params setObjectCheckNil:exinfo forKey:@"exinfo"];
    [params setObjectCheckNil:exception forKey:@"exception"];
    [params setObjectCheckNil:deviceId forKey:@"deviceid"];
    [params setObjectCheckNil:@(appId) forKey:@"appid"];
    
    NSTimeInterval beginTime = [[NSDate date] timeIntervalSince1970] * 1000;
    [[[ZaloPayWalletSDKPayment sharedInstance] sdkReportErrorWithParams:params] subscribeError:^(NSError *error) {
        DDLogInfo(@"sdk error report fail: %@",error);
        NSDictionary *dictionary = error.userInfo;
        int errorCode = [dictionary intForKey:@"returncode"];
        [ZPPaymentManager trackingApptransId:api_v001_tpe_sdkerrorreport beginTime:beginTime returnCode:errorCode];
    } completed:^{
        DDLogInfo(@"sdk error report success");
        [ZPPaymentManager trackingApptransId:api_v001_tpe_sdkerrorreport beginTime:beginTime returnCode:kZPZaloPayCreditErrorCodeNoneError];
        
    }];
}

- (BOOL)isMapCard:(ZPBill*)bill {
    return bill.transType == ZPTransTypeAtmMapCard && ![self isMapBankAccount:bill];
}

- (BOOL)isMapBankAccount:(ZPBill*)bill {
    return [bill isKindOfClass:[ZPIBankingAccountBill class]];
}

#pragma mark - Private Methods

- (enum ZPAtmInputType) classifyAtmInputTypeWithOption:(int) opt andBankType:(NSString *) bankType{
    if (opt == 0) {
        return ZPAtmInputTypeApiBaseInfo;
    }
    if (1 == (1 & opt)) {
        return ZPAtmInputTypeApiPasswordInfo;
    }
    if (2 == (2 & opt)) {
        return ZPAtmInputTypeApiStartDateInfo;
    }
    if (4 == (4 & opt)) {
        return ZPAtmInputTypeApiEndDateInfo;
    }
    if (8 == (8 & opt)) {
        return ZPAtmInputTypeApiStartDateInfo;
    }
    return ZPAtmInputTypeApiBaseInfo;
}

#pragma mark - Card Info

- (NSString *)atmCardInfo:(ZPAtmCard *)atmCard {
    NSMutableDictionary* bankDict = [[NSMutableDictionary alloc] init];
    [bankDict setObjectCheckNil:atmCard.bankCode forKey:@"bankcode"];
    [bankDict setObjectCheckNil:atmCard.cardHolderName forKey:@"cardholdername"];
    [bankDict setObjectCheckNil:atmCard.cardNumber forKey:@"cardnumber"];
    [bankDict setObjectCheckNil:atmCard.type forKey:@"type"];
    [bankDict setObjectCheckNil:atmCard.otpType forKey:@"otptype"];
    [bankDict setObjectCheckNil:atmCard.validFrom forKey:@"cardvalidfrom"];
    [bankDict setObjectCheckNil:atmCard.validTo forKey:@"cardvalidto"];
    [bankDict setObjectCheckNil:atmCard.cardPassword forKey:@"pwd"];
    return [bankDict JSONRepresentation];
}

- (NSString *)creditCardInfo:(ZPCreditCard *)creditCard {
    NSMutableDictionary* ccDict = [[NSMutableDictionary alloc] init];
    [ccDict setObjectCheckNil:creditCard.ccBankCode forKey:@"bankcode"];
    [ccDict setObjectCheckNil:creditCard.cardNumber forKey:@"cardnumber"];
    [ccDict setObjectCheckNil:creditCard.cardName forKey:@"cardholdername"];
    [ccDict setObjectCheckNil:creditCard.cardValidFrom forKey:@"cardvalidfrom"];
    [ccDict setObjectCheckNil:creditCard.cardValidTo forKey:@"cardvalidto"];
    [ccDict setObjectCheckNil:creditCard.cardCVV forKey:@"cvv"];
    return [ccDict JSONRepresentation];
}

+ (void)trackingApptransId:(NSString*)path beginTime:(NSTimeInterval)beginTime returnCode:(NSInteger)returnCode {
    NSTimeInterval endTime = [[NSDate date] timeIntervalSince1970] * 1000;
    NSInteger apiId = requestEventIdFromApi(path);
    NSString *appTransId = [ZaloPayWalletSDKPayment sharedInstance].bill.appTransID;
    if (appTransId.length == 0 || apiId == 0 || beginTime <= 0 || endTime <= 0) {
        return;
    }
    [[ZPAppFactory sharedInstance].orderTracking trackingApptransId:appTransId apiId:@(apiId) timeBegin:@(beginTime) timeEnd:@(endTime) returnCode:@(returnCode)];
    
}

#pragma mark - direct discount
+ (RACSignal *)registerPromotionWith:(ZPBill *) bill
                  promotion:(ZPPromotion *)promotion

{
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSTimeInterval beginTime = [[NSDate date] timeIntervalSince1970] * 1000;
        [[[ZaloPayWalletSDKPayment sharedInstance] registerPromotionWithBill:bill promotionHistory:promotion hashCardNumber:@"" hashBankAccount:@""] subscribeNext:^(NSDictionary *dict){
            [ZPPaymentManager trackingApptransId:[kUrlPromotion stringByAppendingString: api_registerpromotion]
                                       beginTime:beginTime
                                      returnCode:ZALOPAY_ERRORCODE_SUCCESSFUL];
            
            [subscriber sendNext:[dict dictionaryForKey:@"data" defaultValue:@{}]];
            [subscriber sendCompleted];
        } error:^(NSError * _Nullable error) {
            [ZPPaymentManager trackingApptransId:[kUrlPromotion stringByAppendingString: api_registerpromotion]
                                       beginTime:beginTime
                                      returnCode:error.code];
            BOOL isNext = [self trackErrorVoucher:subscriber error:error];
            if (isNext) {
                NSDictionary *data = [error.userInfo dictionaryForKey:@"data"];
                NSString *promotiontransid = [data stringForKey:@"promotiontransid"];
                promotion.promotiontransid = promotiontransid;
                [self getRegisterPromotionStatusWith:[promotiontransid longLongValue]
                                   subscriber:subscriber
                                    startTime:[NSDate date]];
            }
        }];
        
        return nil;
    }];
}

+ (void)getRegisterPromotionStatusWith:(long)promotiontransid
                     subscriber:(id<RACSubscriber>)subscriber
                      startTime:(NSDate *)startTime {
    
    NSTimeInterval beginTime = [[NSDate date] timeIntervalSince1970] * 1000;
    // Currently set 0
    [[[ZaloPayWalletSDKPayment sharedInstance] getRegisterPromotionStatus:promotiontransid] subscribeNext:^(NSDictionary *dict) {
        [ZPPaymentManager trackingApptransId:[kUrlPromotion stringByAppendingString: api_getregisterpromotionstatus]
                                   beginTime:beginTime
                                  returnCode:ZALOPAY_ERRORCODE_SUCCESSFUL];
        [subscriber sendNext:[dict dictionaryForKey:@"data" defaultValue:@{}]];
        [subscriber sendCompleted];
        return ;
    } error:^(NSError * _Nullable error) {
        [ZPPaymentManager trackingApptransId:[kUrlVoucher stringByAppendingString: api_getregisterpromotionstatus]
                                   beginTime:beginTime
                                  returnCode:error.code];
        
        if ([self isGetBillStatusTimeout:startTime]) {
            [subscriber sendError:error];
            return;
        }
        
        BOOL isNext = [self trackErrorVoucher:subscriber error:error];
        
        if (isNext) {
            [[RACScheduler scheduler] afterDelay:1.0 schedule:^{
                [self getRegisterPromotionStatusWith:promotiontransid
                                   subscriber:subscriber
                                    startTime:startTime];
            }];
        }
    }];
    
}

#pragma mark - voucher
+ (RACSignal *)submitVoucherWith:(ZPBill *) bill
                     voucherHistory:(ZPVoucherHistory *)voucher

{
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSTimeInterval beginTime = [[NSDate date] timeIntervalSince1970] * 1000;
        [[[ZaloPayWalletSDKPayment sharedInstance] useVoucher:bill
                                         voucherHistory:voucher
                                               ] subscribeNext:^(NSDictionary *dict) {
            [ZPPaymentManager trackingApptransId:[kUrlVoucher stringByAppendingString: api_usevoucher]
                                       beginTime:beginTime
                                      returnCode:ZALOPAY_ERRORCODE_SUCCESSFUL];
            
            [subscriber sendNext:[dict dictionaryForKey:@"data" defaultValue:@{}]];
            [subscriber sendCompleted];
        } error:^(NSError *error) {
            [ZPPaymentManager trackingApptransId:[kUrlVoucher stringByAppendingString: api_usevoucher]
                                       beginTime:beginTime
                                      returnCode:error.code];
            BOOL isNext = [self trackErrorVoucher:subscriber error:error];
            if (isNext) {
                NSDictionary *data = error.userInfo;
                NSString *evTransID = [data stringForKey:@"evtransid"];
                [self getUseVoucherStatusWith:evTransID
                                   subscriber:subscriber
                                    startTime:[NSDate date]];
            }
        }];
        return nil;
    }];
}

+ (void)getUseVoucherStatusWith:(NSString *)evTransID
                     subscriber:(id<RACSubscriber>)subscriber
                      startTime:(NSDate *)startTime {
    
    NSTimeInterval beginTime = [[NSDate date] timeIntervalSince1970] * 1000;
    [[[ZaloPayWalletSDKPayment sharedInstance] getUseVoucherStatus:evTransID] subscribeNext:^(NSDictionary *dict) {
        [ZPPaymentManager trackingApptransId:[kUrlVoucher stringByAppendingString: api_getusevoucherstatus]
                                   beginTime:beginTime
                                  returnCode:ZALOPAY_ERRORCODE_SUCCESSFUL];
        [subscriber sendNext:[dict dictionaryForKey:@"data" defaultValue:@{}]];
        [subscriber sendCompleted];
        return ;
    } error:^(NSError *error) {
        [ZPPaymentManager trackingApptransId:[kUrlVoucher stringByAppendingString: api_getusevoucherstatus]
                                   beginTime:beginTime
                                  returnCode:error.code];
        
        if ([self isGetBillStatusTimeout:startTime]) {
            [subscriber sendError:error];
            return;
        }
        
        BOOL isNext = [self trackErrorVoucher:subscriber error:error];
        
        if (isNext) {
            [[RACScheduler scheduler] afterDelay:1.0 schedule:^{
                [self getUseVoucherStatusWith:evTransID
                                   subscriber:subscriber
                                    startTime:startTime];
            }];
        }
    }];
}

#pragma mark - Revert Promotion

+ (RACSignal *)revertPromotionInfo:(ZPPromotionInfo *)promotionInfo {
    
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSTimeInterval beginTime = [[NSDate date] timeIntervalSince1970] * 1000;
        [[[ZaloPayWalletSDKPayment sharedInstance] revertPromotion:promotionInfo] subscribeNext:^(NSDictionary *data) {
            [ZPPaymentManager trackingApptransId:[kUrlPromotion stringByAppendingString: api_revertpromotion]
                                       beginTime:beginTime
                                      returnCode:ZALOPAY_ERRORCODE_SUCCESSFUL];
            [subscriber sendNext:data];
            [subscriber sendCompleted];
        } error:^(NSError * _Nullable error) {
            [ZPPaymentManager trackingApptransId:[kUrlPromotion stringByAppendingString: api_revertpromotion]
                                       beginTime:beginTime
                                      returnCode:error.code];
            
            
            BOOL isNext = [self trackErrorVoucher:subscriber error:error];
            if (isNext) {
                NSDictionary *data = error.userInfo;
                NSString *reverttransid = [data stringForKey:@"reverttransid"];
                [self getRevertPromotionStatusWith:[reverttransid longLongValue] subscriber:subscriber startTime:[NSDate date]];
            }
        }];
        return nil;
    }];
    
}

+ (void)getRevertPromotionStatusWith:(long)reverttransid
                        subscriber:(id<RACSubscriber>)subscriber
                         startTime:(NSDate *)startTime
{
    NSTimeInterval beginTime = [[NSDate date] timeIntervalSince1970] * 1000;
    
    [[[ZaloPayWalletSDKPayment sharedInstance] getRevertPromotionStatus:reverttransid] subscribeNext:^(NSDictionary *dict) {
        [ZPPaymentManager trackingApptransId:[kUrlPromotion stringByAppendingString: api_getrevertpromotionstatus]
                                   beginTime:beginTime
                                  returnCode:ZALOPAY_ERRORCODE_SUCCESSFUL];
        [subscriber sendNext:dict];
        [subscriber sendCompleted];
    } error:^(NSError * _Nullable error) {
        [ZPPaymentManager trackingApptransId:[kUrlPromotion stringByAppendingString: api_getrevertpromotionstatus]
                                   beginTime:beginTime
                                  returnCode:error.code];
        if ([self isGetBillStatusTimeout:startTime]) {
            [subscriber sendError:error];
            return;
        }
        
        BOOL isNext = [self trackErrorVoucher:subscriber error:error];
        if (isNext) {
            [[RACScheduler scheduler] afterDelay:1.0 schedule:^{
                [self getRevertPromotionStatusWith:reverttransid
                                      subscriber:subscriber
                                       startTime:startTime];
            }];
        }
    }];
}

#pragma mark - Revert Voucher

+ (RACSignal *)revertVoucher:(NSString *)voucherSig {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSTimeInterval beginTime = [[NSDate date] timeIntervalSince1970] * 1000;
        [[[ZaloPayWalletSDKPayment sharedInstance] revertVoucher:voucherSig] subscribeNext:^(NSDictionary *data) {
            [ZPPaymentManager trackingApptransId:[kUrlVoucher stringByAppendingString: api_revertvoucher]
                                       beginTime:beginTime
                                      returnCode:ZALOPAY_ERRORCODE_SUCCESSFUL];
            [subscriber sendNext:data];
            [subscriber sendCompleted];
            
        } error:^(NSError *error) {
            [ZPPaymentManager trackingApptransId:[kUrlVoucher stringByAppendingString: api_revertvoucher]
                                       beginTime:beginTime
                                      returnCode:error.code];
            
            
            BOOL isNext = [self trackErrorVoucher:subscriber error:error];
            if (isNext) {
                NSDictionary *data = error.userInfo;
                NSString *evTransID = [data stringForKey:@"evtransid"];
                [self getRevertVoucherStatusWith:evTransID
                                      subscriber:subscriber
                                       startTime:[NSDate date]];
            }
        }];
        return nil;
    }];
}

+ (void)getRevertVoucherStatusWith:(NSString *)evTransID
                        subscriber:(id<RACSubscriber>)subscriber
                         startTime:(NSDate *)startTime
{
    NSTimeInterval beginTime = [[NSDate date] timeIntervalSince1970] * 1000;
    [[[ZaloPayWalletSDKPayment sharedInstance] getRevertVoucherStatus:evTransID] subscribeNext:^(NSDictionary *dict) {
        [ZPPaymentManager trackingApptransId:[kUrlVoucher stringByAppendingString: api_getrevertvoucherstatus]
                                   beginTime:beginTime
                                  returnCode:ZALOPAY_ERRORCODE_SUCCESSFUL];
        [subscriber sendNext:dict];
        [subscriber sendCompleted];
    } error:^(NSError *error) {
        [ZPPaymentManager trackingApptransId:[kUrlVoucher stringByAppendingString: api_getrevertvoucherstatus]
                                   beginTime:beginTime
                                  returnCode:error.code];
        if ([self isGetBillStatusTimeout:startTime]) {
            [subscriber sendError:error];
            return;
        }
        
        BOOL isNext = [self trackErrorVoucher:subscriber error:error];
        if (isNext) {
            [[RACScheduler scheduler] afterDelay:1.0 schedule:^{
                [self getRevertVoucherStatusWith:evTransID
                                      subscriber:subscriber
                                       startTime:startTime];
            }];
        }
    }];
}

#pragma mark - handler response voucher

+ (BOOL)trackErrorVoucher:(id<RACSubscriber>)subscriber error:(NSError *)e {
    if ([e isApiError] == NO) {
        [subscriber sendError:e];
        return NO;
    }
    
    NSDictionary *info = e.userInfo;
    int errorCode = [info intForKey:@"returncode"];
    if (errorCode != ZALOPAY_ERRORCODE_PROCESSING) {
        [subscriber sendError:e];
        return NO;
    }
    
    return YES;
}

@end
