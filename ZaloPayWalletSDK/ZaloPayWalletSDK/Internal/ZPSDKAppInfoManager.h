//
//  ZPSDKAppInfoCache.h
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 4/19/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ZPAppData;
@class RACSignal;

@interface ZPSDKAppInfoManager : NSObject
- (ZPAppData *)appWithId:(NSInteger)appId;
- (ZPAppData *)addAppFrom:(NSDictionary*) dictionary appId:(NSInteger)appId;
- (RACSignal *)getAppInfoWithId:(NSInteger)appID transtypes:(NSArray *)arrTranstype;
@end
