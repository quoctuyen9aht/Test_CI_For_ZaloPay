//
//  NetworkManager+RegisterBank.h
//  ZaloPay
//
//  Created by Dung Vu on 8/21/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import <ZaloPayNetwork/NetworkManager.h>
@class ZPBill;
@class ZPAtmCard;
@interface NetworkManager (RegisterBank)
- (RACSignal *)registerBankWith:(NSDictionary *)params;
- (NSDictionary *)paramsWithBill:(ZPBill *)bill;
- (NSString *)atmCardInfo:(ZPAtmCard *)atmCard;
@end
