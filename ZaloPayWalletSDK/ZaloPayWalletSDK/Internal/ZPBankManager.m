//
//  ZPBankManager.m
//  ZaloPayWalletSDK
//
//  Created by thi la on 12/6/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import "ZPBankManager.h"
#import "UIDevice+ZPExtension.h"
#import "ZPDataManager.h"
#import "NetworkManager+ZaloPayWalletSDK.h"
#import "ZaloPayWalletSDKLog.h"
#import "ZPResourceManager.h"
#import "ZPBank.h"
#import "ZPMiniBank.h"
#import <ZaloPayCommon/CodableObject.h>


#define kDataManagerGetBankList     @"zp_datamanager_get_banklist_key"

@interface ZPConfigBanks : CodableObject
@property (nonatomic) NSTimeInterval getBanklistTimeout;
@property (nonatomic, strong) NSString * checksum;
@property (nonatomic, strong) NSArray *jsonBankList;
@property (nonatomic, strong) NSDictionary *jsonTpeBankCardPrefixMap;
@property (nonatomic, strong) NSDictionary *jsonBankInfo;
@property (nonatomic, strong) NSDictionary *bankMap;
@property (nonatomic, strong) NSDictionary *jsonBankListGateway;

- (void)updateBankGateway:(NSInteger)appId with:(NSDictionary *)infor;
- (NSDictionary *)jsonBankGateway:(NSInteger)appid;
- (NSString *)checksumGateway:(NSDictionary *)json;
- (NSTimeInterval)getBankGatewayListTimeout:(NSDictionary *)json;
@end

@implementation ZPConfigBanks

+ (instancetype)config {
    ZPConfigBanks *config = [ZPCache objectForKey:kDataManagerGetBankList];
    return  config != nil ? config: [ZPConfigBanks new];
}

+ (void)saveToCache:(ZPConfigBanks *)config {
    [ZPCache setObject:config forKey:kDataManagerGetBankList];
}

+ (void)removeCache {
    [ZPCache removeObjectForKey:kDataManagerGetBankList];
}

- (void)updateFromDic:(NSDictionary *)dictionary {
    NSString *checksum = [dictionary stringForKey:@"checksum"];
    NSTimeInterval expiredtime = [[dictionary objectForKey:@"expiredtime"] doubleValue]/1000;
    NSTimeInterval timeout = [[NSDate date] timeIntervalSince1970] + expiredtime;
    NSDictionary *bankPrefix = [dictionary dictionaryForKey:@"tpebankcardprefixmap"];
    NSArray *banklist = [dictionary objectForKey:@"banklist"];
    
    if (checksum) {
        self.checksum = checksum;
    }
    if (timeout > 0) {
        self.getBanklistTimeout = timeout;
    }
    
    if (bankPrefix) {
        self.jsonTpeBankCardPrefixMap = bankPrefix;
    }
    if (banklist) {
        self.jsonBankList = banklist;
        self.jsonBankInfo = dictionary;
        self.bankMap = [self bankMapFromArray:banklist];
    }
}

- (NSDictionary *)bankMapFromArray:(NSArray *)banklist {
    if (!banklist) {
        return nil;
    }
    NSMutableDictionary *bankDic = [NSMutableDictionary dictionary];
    for (NSDictionary *dic in banklist) {
        id key = [dic objectForKey:stringKeyCode];
        if (key) {
            ZPBank * bank = [ZPBank initBankWithData:dic];
            [bankDic setObject:bank forKey:key];
        }
    }
    return bankDic;
}

- (NSDictionary *)jsonBankGateway:(NSInteger)appid {
    if (!self.jsonBankListGateway) {
        return nil;
    }
    NSDictionary *json = [self.jsonBankListGateway dictionaryForKey:@(appid)];
    return json;
}

- (NSString *)checksumGateway:(NSDictionary *)json {
    if (!json) {
        return @"";
    }
    return [json stringForKey:@"checksum" defaultValue:@""];
}

- (NSTimeInterval)getBankGatewayListTimeout:(NSDictionary *)json {
    if (!json) {
        return 0;
    }
    return [json doubleForKey:@"expiredtime"];
}


- (void)updateBankGateway:(NSInteger)appId with:(NSDictionary *)infor {
    // Valid
    if (!infor) {
        return;
    }
    
    if (!self.jsonBankListGateway) {
        // init json cache
        self.jsonBankListGateway = @{};
    }
    
    // Add expire time
    NSMutableDictionary *result = [NSMutableDictionary dictionaryWithDictionary:infor];
    NSTimeInterval expireTime = [infor doubleForKey:@"expiredtime"] / 1000 + [[NSDate date] timeIntervalSince1970];
    [result addEntriesFromDictionary:@{ @"expiredtime": @(expireTime) }];
    
    NSMutableDictionary *jsonTemp = [NSMutableDictionary dictionaryWithDictionary:self.jsonBankListGateway];
    [jsonTemp addEntriesFromDictionary:@{ @(appId) : result }];
    
    // Cache
    self.jsonBankListGateway = jsonTemp;
    
    // Save
    [[self class] saveToCache:self];
}

@end

@interface ZPBankManager()
@property (nonatomic, strong) ZPConfigBanks * cachedBanks;
@end

@implementation ZPBankManager

- (id)init {
    self = [super init];
    if (self) {
        self.cachedBanks = [ZPConfigBanks config];
    }
    return self;
}

- (void)getBanklistWithCallBack:(void (^)(NSDictionary *))callback {
    if (callback == nil) {
        return;
    }
    
    BOOL isExpire = ([[NSDate date] timeIntervalSince1970] >= self.cachedBanks.getBanklistTimeout);
    NSDictionary *json = self.cachedBanks.jsonBankInfo;
    
    if (!isExpire && json) {
        callback(json);
        return;
    }
    
    [[[ZaloPayWalletSDKPayment sharedInstance] getBankListWithCheckSum:self.cachedBanks.checksum
                                                       resourceVersion:[[ZPDataManager sharedInstance] resourceVersion3]] subscribeNext:^(NSDictionary* dictionary)
    {
        [self.cachedBanks updateFromDic:dictionary];
        [ZPConfigBanks saveToCache:self.cachedBanks];
        callback(self.cachedBanks.jsonBankInfo);
    } error:^(NSError *error) {
        callback(self.cachedBanks.jsonBankInfo);
    }];
}

- (RACSignal <NSDictionary *>*)getBankGatetway:(NSInteger)appId {
    NSDictionary *json = [[self.cachedBanks jsonBankGateway:appId] copy];
    NSTimeInterval expireTime = [self.cachedBanks getBankGatewayListTimeout:json];
    BOOL isExpire = [[NSDate date] timeIntervalSince1970] >= expireTime;
    
    // Return if has cache
    if (!isExpire && json) {
        return [RACSignal return:json];
    }
    
    NSString *currentCheckSum = [self.cachedBanks checksumGateway:json];
    
    @weakify(self);
    return [[[[[ZaloPayWalletSDKPayment sharedInstance] getBankGatewayListWithCheckSum:currentCheckSum appId:appId] doNext:^(id  _Nullable x) {
        NSDictionary *result = [NSDictionary castFrom:x];
#ifdef DEBUG
        NSAssert(result != nil, @"Check Result!!!!");
#endif
        @strongify(self);
        // check checksum
        NSString *checksum = [result stringForKey:@"checksum"];
        
        // Valid to update
        if (!checksum || ([currentCheckSum length] > 0 && [checksum length] > 0 && [currentCheckSum isEqualToString:checksum])) {
            return;
        }
        [self.cachedBanks updateBankGateway:appId with:result];
    }] map:^NSDictionary *_Nullable(id  _Nullable value) {
        @strongify(self);
        return [[self.cachedBanks jsonBankGateway:appId] copy];
    }] catch:^RACSignal * _Nonnull(NSError * _Nonnull error) {
        return [RACSignal return:json];
    }];
}

- (BOOL)canPaymentGatway:(NSInteger)appid {
    NSDictionary *json = [self.cachedBanks jsonBankGateway:appid];
    if (!json) {
        return NO;
    }
    NSArray *items = [json arrayForKey:@"banklist" defaultValue:@[]];
    return [items count] > 0 ;
}


- (NSArray <ZPBank*> *)allBanks {
    return [self.cachedBanks.bankMap allValues];
}

- (NSDictionary *)bankbins {
    return self.cachedBanks.jsonTpeBankCardPrefixMap;
}

- (void)clearCache {
    [ZPConfigBanks removeCache];
}

- (NSDictionary *)bankDisplayOrder {
    return [self bankDisplayOrder:YES];
}

- (NSDictionary *)bankDisplayOrderForPayment {
    return [self bankDisplayOrder:NO];
}
- (NSNumber*)ccDisplayOrder:(NSString*)key {
    return [self ccDisplayOrder:key isMapcard:YES];
}

- (NSDictionary *)bankDisplayOrder:(BOOL)isMapcard {
    NSMutableDictionary *_return = [NSMutableDictionary dictionary];
    for (ZPBank *bank in self.allBanks) {
        if ([bank.bankCode isEqualToString:stringCreditCardBankCode]) {
            [_return setObjectCheckNil:[self ccDisplayOrder:@"visa" isMapcard:isMapcard] forKey:VISA];
            [_return setObjectCheckNil:[self ccDisplayOrder:@"masterCard" isMapcard:isMapcard] forKey:MASTER];
            [_return setObjectCheckNil:[self ccDisplayOrder:@"jcb" isMapcard:isMapcard] forKey:JCB];
            continue;
        }
        if ([bank.bankCode isEqualToString:stringDebitCardBankCode]) {
            [_return setObjectCheckNil:[self ccDisplayOrder:@"visa" isMapcard:isMapcard] forKey:VISADEBIT];
            [_return setObjectCheckNil:[self ccDisplayOrder:@"masterCard" isMapcard:isMapcard] forKey:MASTERDEBIT];
            [_return setObjectCheckNil:[self ccDisplayOrder:@"jcb" isMapcard:isMapcard] forKey:JCBDEBIT];
            continue;
        }
        [_return setObject:@(bank.dispayOrder) forKey:bank.bankCode];
    }
    return _return;
}

- (NSNumber*)ccDisplayOrder:(NSString*)key isMapcard:(BOOL)isMapcard {
    NSDictionary *ccConfig = [self creditCardConfig];
    if (ccConfig) {
        NSDictionary *ccBank = [ccConfig dictionaryForKey:key defaultValue:@{}];
        NSString *displayOrder = isMapcard ? @"displayorder": @"pm_displayorder";
        return [ccBank numericForKey:displayOrder defaultValue:@(0)];
    }
    return @(0);
}
- (ZPMiniBank*)getBankFunctionIsSupportTranType:(ZPBank *)bank withTranType:(ZPTransType)trantype {
    if (bank == nil) {
        return nil;
    }
    DDLogInfo(@"bank %@",bank);
    NSArray *bankFunctions = bank.bankFunctions;
    DDLogInfo(@"Arr bankFuntion %@", bankFunctions);
    NSInteger bankType =  bank.supportType;
    ZPBankFuntion zpbankFuntion = [self bankFunctionFromTranstype:trantype bankType:bankType];
    DDLogInfo(@"zpbankFuntion %@", bankFunctions);
    for(NSDictionary * bankFunction in bankFunctions) {
        if ([bankFunction intForKey:@"bankfunction"] == zpbankFuntion) {
            ZPMiniBank *miniBank = [ZPMiniBank fromDictionary:bankFunction];
            return miniBank;
        }
    }
    return nil;
}

- (ZPMiniBank*)getBankFunctionIsSupportMethod:(ZPBank *)bank
                            withPaymentMethod:(ZPPaymentMethodType)method {
    if (bank == nil) {
        return nil;
    }
    NSArray *bankFunctions = bank.bankFunctions;
    DDLogInfo(@"Arr bankFuntion %@", bankFunctions);
    ZPBankFuntion zpbankFuntion = [self payTypeToBankfuntion:method];
    DDLogInfo(@"zpbankFuntion %ld", (long)zpbankFuntion);
    for(NSDictionary * bankFunction in bankFunctions) {
        if ([bankFunction intForKey:@"bankfunction"] == zpbankFuntion) {
            ZPMiniBank *miniBank = [ZPMiniBank fromDictionary:bankFunction];
            return miniBank;
        }
    }
    return nil;
}

#pragma mark - Mini Banks

- (ZPMiniBank *)findBankFunction:(ZPBankFuntion) type fromBank:(ZPBank *)bank {
    for (NSDictionary *bankFunction in bank.bankFunctions) {
        ZPBankFuntion currentType = [bankFunction intForKey:@"bankfunction"];
        if (currentType == type) {
            return [ZPMiniBank fromDictionary:bankFunction];
        }
    }
    return nil;
}

- (NSArray <ZPMiniBank *> *)getMiniBanksForMapping {
    NSMutableArray *_return  = [NSMutableArray array];
    for (ZPBank *bank in self.allBanks) {
        ZPBankFuntion funcType = bank.supportType == ZPSDK_BankType_Card ? ZPBankFuntionMapCard : ZPBankFuntionMapAccount;
        ZPMiniBank *miniBank = [self findBankFunction:funcType fromBank:bank];
        if (miniBank) {
            [_return addObject:miniBank];
        }
    }
    return _return;
}


- (ZPBankFuntion)bankFunctionFromTranstype:(ZPTransType)tranType
                                  bankType:(ZPSDK_BankType)bankType {
    if (tranType == ZPTransTypeWithDraw) {
        return ZPBankFuntionWithDraw;
    }
    if (tranType == ZPTransTypeAtmMapCard) {
        return bankType == ZPSDK_BankType_Card ? ZPBankFuntionMapCard : ZPBankFuntionMapAccount;
    }
    return ZPBankFuntionBillPayByCard;
}

- (ZPBankFuntion)payTypeToBankfuntion: (ZPPaymentMethodType)method {
    switch (method) {
        case ZPPaymentMethodTypeCCDebit:
        case ZPPaymentMethodTypeSavedCard:
            return ZPBankFuntionBillPayByCardToken;
        case ZPPaymentMethodTypeSavedAccount:
            return ZPBankFuntionBillPayByAccountToken;
        case ZPPaymentMethodTypeIbanking:
            return ZPBankFuntionMapAccount;
        default:
            break;
    }
    return ZPBankFuntionBillPayByCard;
}

- (NSArray *)listCCBankSupport {
    NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
    ZPBank *bank;
    if ([self isVisaCardEnable]) {
        bank = [[ZPBank alloc] init];
        bank.imageName = @"123pvisa";
        [tmpArray addObject:bank];
        DDLogInfo(@"isVisaCardEnable");
    }
    
    if ([self isMasterCardEnable]) {
        bank = [[ZPBank alloc] init];
        bank.imageName = @"123pmaster";
        [tmpArray addObject:bank];
        DDLogInfo(@"isVisaCardEnable");
        
    }
    
    if ([self isJCBCardEnable]) {
        bank = [[ZPBank alloc] init];
        bank.imageName = @"123pjcb";
        [tmpArray addObject:bank];
        DDLogInfo(@"isJCBCardEnable");
    }
    
    return tmpArray;
}

- (NSMutableArray *)listBankInPinView {
    return [[self.allBanks filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
        ZPBank *bank = [ZPBank castFrom:evaluatedObject];
        if (!bank) {
            return NO;
        }
        if ([bank.bankCode isEqualToString:stringDebitCardBankCode]) {
            return NO;
        }
        if ([bank.bankCode isEqualToString:stringCreditCardBankCode]) {
            return NO;
        }
        return YES;
    }]] mutableCopy];
}

- (NSArray*)getArrayBankInPinViewWithTranstype:(ZPTransType)transType{
    NSMutableArray *tmpArray = [self listBankInPinView];
    if (transType == ZPTransTypeAtmMapCard) {
        NSArray *listCCBank = [self listCCBankSupport];
        [tmpArray addObjectsFromArray:listCCBank];
    }
    return tmpArray;
}

- (NSDictionary *)ccBanksDict {
    if (_ccBanksDict || ![ZPDataManager sharedInstance].configData) return _ccBanksDict;
    _ccBanksDict = [[ZPDataManager sharedInstance].configData objectForKey:@"ccbanks_list"];
    return _ccBanksDict;
}
- (NSDictionary *)banksDictOnBundle {
    if (_banksDictOnBundle || ![ZPDataManager sharedInstance].configData) return _banksDictOnBundle;
    _banksDictOnBundle = [[ZPDataManager sharedInstance].configData objectForKey:@"banks_list"];
    return _banksDictOnBundle;
}

- (NSDictionary *)binsFrom:(NSDictionary *)config for:(NSString *)value {
    if (!config || [config allKeys].count == 0 || [value length] == 0) {
        return @{};
    }
    int minRange = [config intForKey:@"min_range"];
    int maxRange = [config intForKey:@"max_range"];
    NSMutableDictionary *result = [NSMutableDictionary new];
    if(minRange > 0) {
        for (int i = minRange; i <= maxRange; i++) {
            [result addEntriesFromDictionary:@{[NSString stringWithFormat:@"%d",i] : value}];
        }
    }
    return result;
}

- (NSDictionary *)ccBankbins {
    if (_ccBankbins || ![ZPDataManager sharedInstance].configData) return _ccBankbins;
    NSDictionary *bins = [[ZPDataManager sharedInstance].configData objectForKey:@"ccbankbins"];
    NSMutableDictionary *result = [NSMutableDictionary dictionaryWithDictionary:bins ?: @{}];
    // check master
    if ([self isMasterCardEnable]) {
        NSDictionary *config = [self getConfig:@"masterCard"];
        [result addEntriesFromDictionary: [self binsFrom:config for:@"123PMASTER"]];
    }
    
    // check jcb
    if ([self isJCBCardEnable]) {
        NSDictionary *config = [self getConfig:@"jcb"];
        [result addEntriesFromDictionary: [self binsFrom:config for:@"123PJCB"]];
    }
    _ccBankbins = result;
    return _ccBankbins;
}

- (NSDictionary *)creditCardConfig {
    return [[ZaloPayWalletSDKPayment sharedInstance].appDependence creditCardConfig];
}

- (NSDictionary *)getConfig:(NSString *)key {
    NSDictionary *config = [self creditCardConfig];
    if (!config) {
        return @{};
    }
    NSDictionary *result = [config dictionaryForKey:key defaultValue:@{}];
    return result;
}

- (BOOL)isMasterCardEnable {
    return [[self getConfig:@"masterCard"] boolForKey:@"enable"];
}

- (BOOL)isVisaCardEnable {
    return [[self getConfig:@"visa"] boolForKey:@"enable"];
}

- (BOOL)isJCBCardEnable {
    return [[self getConfig:@"jcb"] boolForKey:@"enable"];
}

- (ZPBank*)getBankInfoWith:(NSString*)bankCode {
    return [self.cachedBanks.bankMap objectForKey:bankCode];
}

@end



