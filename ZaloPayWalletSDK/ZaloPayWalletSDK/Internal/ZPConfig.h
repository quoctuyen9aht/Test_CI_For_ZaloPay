//
//  ZPConfig.h
//  ZPSDK
//
//  Created by phungnx on 12/22/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#ifndef ZPConfig_h
#define ZPConfig_h

///Configuration///////
#define ATM_SHOW_WEBVIEW 0                  //show webview to debug, 0 for LIVE
#define SDK_CONNECTION_TIMEOUT 10
#define MAX_CC_SAVED_CARD 3
#define SDK_BUILD_VERSION @"3.4.0"

//show webview to debug, 0 for LIVE
#define kATM_TRANSATION_TIMEOUT 7*60        //atm transaction timeout, default 7 minutes
#define kATM_LONG_PROGESSHUD_TIME  60
#define ATM_CHECK_CARD_IS_LUHN_VALID [[ZPDataManager sharedInstance] doubleForKey:@"atm_check_luhn_is_valid"]
#define CC_CHECK_CARD_IS_LUHN_VALID [[ZPDataManager sharedInstance] doubleForKey:@"cc_check_luhn_is_valid"]
#define TEXTFIELD_ERROR_COLOR [UIColor zpColorFromHexString:@"#ED1E31"]
#define TEXTFIELD_SUCCESS_COLOR [UIColor zpColorFromHexString:@"#008FE5"]

#define PADDING 10

#define ZPUserDefaults   [NSUserDefaults standardUserDefaults]

#define kLastChargedATMCardKey  @"com.vng.ZaloPaySDK.kLastChargedATMCardKey"

#define IS_IPAD ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
#define IS_IPHONE ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone )
#define IS_2X ( [[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2 )
#define IS_IOS_8_AND_GREATER() ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8)

#define zpRGB(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]
#define kPAYMENT_SDK_KEY_PREFIX @"k_zalopay_handle_sdk_"
#define kStringTypePrivateKeyForCookiesEncrypt  @"znp_private_key_abc_1234_#@!$$%%"
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width

#define ratioCardView 1.7
#define cardViewScrollHeight 60 + 16 + 10
#define kMarginCardView 70
#define kDefaultStepCardView 3

#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH >= 736.0)
#define IS_IPHONE_X (IS_IPHONE && SCREEN_MAX_LENGTH >= 812.0)

#define kLengthCheckExistMin 16
#define kLengthCheckExistMax 19

typedef NS_ENUM(NSInteger, ZPStringEnum) {
    ZPStringTypeErrorMessageNetworkError,
    ZPStringTypeErrorMessageMapcardNetworkError,
    ZPStringTypeErrorMessageUnsuportAmountError,
    ZPStringTypeAnnounceMessageIsInProcessing,
    ZPStringTypeAnnounceMessageMapcardIsInProcessing,
    ZPStringTypeAnnounceMessageExchangeSuccess,
    ZPStringTypeAnnounceMessageExchangeUnidentified,
    ZPStringTypeAnnounceMessageExchangeMapcardUnidentified,
    ZPStringTypeAnnounceMessageExchangeFail,
    ZPStringTypeAnnounceMessageNoInternetConnection,
    ZPStringTypePrivateKeyForCookieEncrypt
};

typedef NS_ENUM(NSInteger, ZPFontStyle) {
    ZPTextStyleBold = 0,
    ZPTextStyleNormal = 1,
    ZPTextStyleItalic = 2,
    ZPFontStyleMAX = 3
};

typedef NS_ENUM(NSInteger, ZPRecordType) {
    ZPRecordTypeTextField = 0,
    ZPRecordTypeCaptcha  = 1
};

typedef NS_ENUM(NSInteger, ZPCellType){
    ZPCellTypeCaptchaOnlyCell
};

typedef NS_ENUM(NSInteger, ZPAtmInputType)  {
    ZPAtmInputTypeApiBaseInfo = 0,
    ZPAtmInputTypeApiPasswordInfo = 1,
    ZPAtmInputTypeApiStartDateInfo = 2,
    ZPAtmInputTypeApiEndDateInfo = 4
};

typedef NS_ENUM(NSInteger, ZPBankCode) {
    ZPBankCodeSGCB,
    ZPBankCodeABB,
    ZPBankCodeACB,
    ZPBankCodeAGB,
    ZPBankCodeBAB,
    ZPBankCodeBIDV,
    ZPBankCodeDAIAB,
    ZPBankCodeEIB,
    ZPBankCodeGPB,
    ZPBankCodeHDB,
    ZPBankCodeMRTB,
    ZPBankCodeMB,
    ZPBankCodeNAB,
    ZPBankCodeNVB,
    ZPBankCodeOCB,
    ZPBankCodeOCEB,
    ZPBankCodePGB,
    ZPBankCodeSCB,
    ZPBankCodeSGB,
    ZPBankCodeTCB,
    ZPBankCodeTPB,
    ZPBankCodeZP,
    ZPBankCodeVAB,
    ZPBankCodeVTB,
    ZPBankCodeVPB,
    ZPBankCodeVIB,
    ZPBankCodeDAB,
    ZPBankCodeVCB,
    ZPBankCodeCC,
    ZPBankCodeCCDebit,
    ZPBankCodeVCCB
};

typedef NS_ENUM(NSInteger, ZPCCBankCode) {
    ZPCCBankCodeVISA,
    ZPCCBankCodeMASTER,
    ZPCCBankCodeJCB,
    ZPCCBankCodeVISADebit,
    ZPCCBankCodeMASTERDebit,
    ZPCCBankCodeJCBDebit
};

typedef NS_ENUM(NSInteger, ZPServerErrorCode) {
    ZPServerErrorCodeFail               = 0,
    ZPServerErrorCodeNoneError          = 1,
    ZPServerErrorCodeAtmPurchaseSuccess = 18,
    ZPServerErrorCodeAtmRetryOtp        = 17,
    ZPServerErrorCodeAtmRetryCaptcha    = 16,
    ZPServerErrorCodeRetryPIN           = 15,
    ZPServerErrorCodeFailInit           = 10,
    ZPServerErrorCodeFailDuplicate      = 2,
    ZPServerErrorCodeFailTimeout        = -54,
    ZPServerErrorCodeTransIdNotExist    = -49,
    ZPServerErrorCodeBlockUser          = -124,
    ZPServerErrorCodeMaintainance       = -999,
    ZPServerErrorCodeLoginFail          = -71,
    ZPServerErrorCodeLoginExpired       = -72,
    ZPServerErrorCodeTokenInvalid       = -73,
    ZPServerErrorCodeTokenNotFound      = -77,
    ZPServerErrorCodeTokenExpired       = -78,
    ZPServerErrorCodeAccountSuspended   = -61,
    ZPServerErrorCodeMaxBalancePerDay   = -133
};

typedef NS_ENUM(NSInteger, ZPAtmPaymentStep) {
    
    //1 Input card
    
    ZPAtmPaymentStepAutoDetectBank = 21,
    ZPAtmPaymentStepSelectBank = 31,
    ZPAtmPaymentStepChooseSMSToken = 41,
    ZPAtmPaymentStepLoginVicom = 51,
    ZPAtmPaymentStepChooseAccountVicom = 61,
    ZPAtmPaymentStepSubmitOtpVicom = 71,
    ZPAtmpStepSubmitCardInfo = 81,
    ZPAtmPaymentStepSubmitPurchase = 91,
    ZPAtmPaymentStepSubmitTrans = 101,
    ZPAtmPaymentStepGetTransStatus = 111,
    ZPAtmPaymentStepAtmAuthenPayer = 121,
    ZPAtmPaymentStepWaitingWebview = 131,
    ZPAtmPaymentStepSavedCardConfirm = 141,
    ZPAtmPaymentStepGetStatusByAppTransIdForClient = 161,
    
    //2 Captcha
    ZPAtmPaymentStepVietinBankCapCha = 12,
    ZPAtmPaymentStepBIDVCapChaPassword = 22,
    
    
    //3 OTP
    ZPAtmPaymentStepVietinBankOTP = 13,
    ZPAtmPaymentStepBIDVBankOTP = 23
    //4 Result
};

typedef NS_ENUM(NSInteger, ZPCreditCardPaymentStep) {
    
    //1 Input card
    ZPCcPaymentStepFillCardInfo = 11,
    ZPCcPaymentStepSavedCardConfirm = 21,
    ZPCcPaymentStepWaitingWebview = 31,
    ZPCcPaymentStepUpdate = 41,
    ZPCcPaymentStepSubmitPurchase = 61,
    ZPCreditCardPaymentStepStepGetStatusByAppTransIdForClient = 71
    
    //2 Captcha
    //3 OTP
    //4 Result
    
};
typedef NS_ENUM(NSInteger, ZPIBankingPaymentStep) {
    
    //1 Input card
    ZPIBankingPaymentStepLogin = 11,
    ZPIBankingPaymentStepLinkWalletSubmitInfo = 21,
    ZPIBankingPaymentStepCancelWalletSubmitInfo = 31,
    ZPIBankingPaymentStepWaitingWebview = 41,
    ZPIBankingPaymentStepSavedAccountConfirm = 51,
    ZPIBankingPaymentStepSubmitPurchase = 61,
    ZPIBankingPaymentStepGetStatusByAppTransIdForClient = 71,
    ZPIBankingPaymentStepAtmAuthenPayer = 91,
    
    ZPIBankingPaymentStepSubmitTrans = 101,
    //2 Captcha
    //3 OTP
    ZPIBankingPaymentStepOTP = 12
    //4 Result
};

#endif /* ZPConfig_h */
