//
//  ZPDataManager.h
//  ZPSDK
//
//  Created by phungnx on 12/22/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZPConfig.h"
#import "ZPDefine.h"
#import "ZPSDKAppInfoManager.h"
#import "ZPBankManager.h"

@class ZPChannel;
@class ZPBill;
@class ZPAtmCard;
@class ZPPaymentViewCell;
@class ZPSavedCard;
@class ZPSavedBankAccount;
@class ZPPaymentResponse;
@class ZPPaymentMethod;
@class ZPMiniBank;
@class RACSubject;
@class ZPSDKAppInfoManager;
@class RACSignal;

@interface ZPConfigData : NSObject <NSCoding>
@property (nonatomic, assign) NSTimeInterval resourceTimeout;
@property (nonatomic, strong) NSString * resourceVersion;
@property (nonatomic, strong) NSString * resourceLink;
@property (nonatomic, assign) NSTimeInterval gwInfoTimeout;
@property (nonatomic, assign) NSTimeInterval resourceLastGetTime;
@property (nonatomic, assign) NSTimeInterval gwInfoLastGetTime;
@property (nonatomic, strong) NSDictionary * gatewayInfo;
@property (nonatomic, strong) NSArray * cachedCardList;
@property (nonatomic, strong) NSString * checksum;
@property (nonatomic, assign) NSTimeInterval expiredTime;
@property (nonatomic, assign) BOOL isModify;
@property (nonatomic, assign) BOOL isModifyResource;

//Add
@property (nonatomic, strong) NSString * cardinfochecksum;
@property (nonatomic, assign) BOOL isenabledeposit;

@property (nonatomic, strong) NSDictionary * info;
@property (nonatomic, strong) NSDictionary * transtypepmcs;

@property (nonatomic, strong) NSString * bankaccountchecksum;
@property (nonatomic, strong) NSArray * cachedBankAccountList;

- (void)updateWithDict:(NSDictionary *)dictionary;
- (void)save;
- (void)addCardInfo:(NSDictionary *)cardInfo;
@end

//--------------------------------

@interface ZPDataManager : NSObject
@property (strong, nonatomic) NSDictionary* configData;
@property (nonatomic, strong) ZPSDKAppInfoManager *appInfoManager;
@property (nonatomic, strong) ZPBankManager *bankManager;
@property (nonatomic, assign) CGFloat heightRatio;
@property (nonatomic, assign) CGFloat widthRatio;
@property (nonatomic, strong) NSString *cachedCardNumber;

@property (nonatomic, strong) ZPBill * bill;
@property (nonatomic, strong) NSArray *paymentSavedCard;
@property (nonatomic, strong) NSArray *paymentChannel;
@property (nonatomic, strong) NSArray * paymentSavedBankAccount;
@property (nonatomic, strong) NSArray * paymentUserPLP;
@property (nonatomic, strong) ZPConfigData * cachedData;

@property (nonatomic, strong) NSString * userPIN;
@property (nonatomic, strong) id tempSavedCard;

@property (nonatomic, strong) NSString *savedCardNumber;
@property (nonatomic, assign) BOOL isExistNewBankAccount;

@property (nonatomic, strong) RACSubject *addNewCardSignal;

+ (instancetype)sharedInstance;
- (void)initDataWithCallback:(void (^)(int errorCode, NSString * message)) callback;
- (void)getPaymentGatewayInfoCompleteHandle:(void (^) (int errorCode, NSString *mesage))callBack;
- (void)loadData;
- (RACSignal *)removeSavedCard:(ZPSavedCard *)card;
- (RACSignal *)removeBankAccount:(NSString*)bankCustomerId bankAccount:(ZPSavedBankAccount*)bankAccount;
- (RACSignal *)addSavedCard:(NSDictionary *)bankInfo;
- (void)updateListBankAccount:(void (^)(NSArray* listAccount, NSError *error)) callback;

- (NSDictionary *)getPlatformInfoConfig;

- (NSDictionary *)viewsConfig;
- (NSDictionary *)stringConfig;
- (NSDictionary *)numberConfig;
- (NSString *)currencyUnit;
- (NSString *) bundlePath;
- (NSBundle *) bundle;
- (UIView *) viewWithNibName: (NSString *) name owner: (id) owner;
- (UINib *) nibNamed: (NSString *) name;
- (NSString *) messageForKey: (NSString *) key;
- (NSString *)stringByEnum:(ZPStringEnum) eString;
- (double)doubleForKey:(NSString *)key;

//Load,Save ATM Card
- (void)saveATMCard:(ZPAtmCard *)card;
- (void)deleteSavedAtmCard;
- (ZPAtmCard *)getCachedAtmCard;

//Cell Factory
- (ZPPaymentViewCell *)cellWithEnum:(ZPCellType) type;
- (float)heightCellWithEnum:(ZPCellType) type;
- (NSString *)reuseIdCellWithEnum:(ZPCellType) type;

- (void) clearPaymentCache;

- (NSString*) getTitleByTranstype:(ZPTransType)transType;

- (BOOL)isPLPEnableWithTranstype:(ZPTransType) transType pmcID:(ZPPaymentMethodType)pmcID;

- (NSString*)getButtonTitle:(NSString*)key;
- (BOOL)checkInternetConnect;
- (BOOL)checkSavedCardIsExist:(NSString*)cardNumber;
- (NSArray *)getSavedCardsForBank:(NSString *)bankCode;
- (NSArray *)getSavedAccountForBank:(NSString *)bankCode;

- (void)requestUpdateCardList:(dispatch_block_t)callback;
- (void)requestUpdateAccountList:(dispatch_block_t)callback;
- (RACSignal *)loadCardListAndBankAccount;
- (void)getPlatformInfoConfig:(void (^)(NSDictionary *))callback;
- (BOOL)chekMinAppVersion:(NSString *)minAppVersion;
- (NSString*)checkMaxCCSavedCard;
- (ZPChannel *)channelWithTypeAndBankCode:(ZPPaymentMethodType)type bankCode:(NSString*)bankCode;

- (NSArray *)allSaveCardsFromCache;

- (BOOL)isCardSaved:(NSString *)first6CardNo andLast4CardNo:(NSString *)last4CardNo;
- (void)clearLocalUserCache;

- (BOOL)shouldDownloadResource;

- (void)clearPlatformInfoAndResource;
- (BOOL)isMaxCCSavedCard;
- (NSString*)checkErrorMappCCCardWith:(ZPTransType)transType appId:(int32_t)appId;
- (NSString *)resourceVersion3;

- (BOOL)isCCCard:(NSString*)bankCode;
- (BOOL)isCCDebitCard:(NSString*)bankCode;

RACSignal *loadImageFromString(NSString *str);

- (void)showAlertMaxCCSavedCard;
@end
