//
//  ZPSDKStringConstants.m
//  ZaloPayWalletSDK
//
//  Created by thi la on 4/11/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPSDKStringConstants.h"

NSString *const stringKeyBankCode = @"bankcode";
NSString *const stringKeyBankCode2 = @"bankCode";
NSString *const stringKeyIconInfo = @"ic_alert_speaker";
NSString *const stringKeyMaintain = @"message_maintainance";
NSString *const stringKeyUnsupportAmount = @"message_amount_unsupported";
NSString *const stringKeyCardNotSupportWithdraw = @"zalo_pay_card_not_allow_withdraw";
NSString *const stringKeyCardSavedIsEmpty = @"zalo_pay_not_have_saved_card";
NSString *const stringKeyStatus = @"status";
NSString *const stringKeyFeecalType = @"feecaltype";
NSString *const stringKeyFeeRate = @"feerate";
NSString *const stringKeyMinFee = @"minfee";
NSString *const stringKeyCCBankName = @"bank_name";
NSString *const stringKeyATMBankName = @"name";
NSString *const stringKeyAllowWithdraw = @"allowwithdraw";
NSString *const stringKeyErrorCode = @"errorcode";
NSString *const stringKeyMessage = @"message";
NSString *const stringKeyBalance = @"balance";
NSString *const stringKeyTranType = @"trantype";
NSString *const stringKeyZPTranId = @"zptransid";
NSString *const stringKeyPaymentComplete = @"payment-complete";
NSString *const stringKeyPaymentCancel = @"payment-cancel";
NSString *const stringKeyTransactionId = @"transactionID";
NSString *const stringKeyPath = @"path";
NSString *const stringKeyStep = @"step";
NSString *const stringKeyMinAppVersion = @"minappversion";
NSString *const stringKeyMasterCardBinMinRange = @"master_card_bin_min_range";
NSString *const stringKeyJCBBinMinRange = @"jcb_card_bin_min_range";
NSString *const stringKeyVisa = @"4";
NSString *const stringKeyCode = @"tpebankcode";
NSString *const stringKeyChannelUnsupport = @"zalo_pay_channel_unsupport";
NSString *const stringKeyChannelIsMaintain = @"message_chanel_is_maintain";
NSString *const stringKeyChannelUnsupportAmount = @"zalo_pay_channel_unsupport_amount";
NSString *const stringKeyAnnounceSuccess = @"message_announce_success_payment";
NSString *const stringKeyAnnounceError = @"message_announce_error_payment";
NSString *const stringKeyBankNotSupportMapByCard = @"bank_not_support_mapbycard";
NSString *const stringKeyBankNotSupportPayByCard = @"bank_not_support_paybycard";
NSString *const stringKeyBankAccountPayByCard = @"popup_bankaccount_paybycard";
NSString *const stringKeySupportType = @"supporttype";
NSString *const stringNetworkError = @"Mạng không ổn định. Vui lòng thử lại sau.";
NSString *const stringInvalidAppId = @"Invalid AppID";
NSString *const stringInvalidAppUser = @"Invalid AppUser";
NSString *const stringInvalidAmount = @"Invalid Amount";
NSString *const stringAlertTitleInfo = @"Thông báo";
NSString *const stringAlertButtonClose = @"ĐÓNG";
NSString *const stringFeeCal = @"SUM";
NSString *const stringCreditCardBankCode = @"CC";
NSString *const stringDebitCardBankCode = @"CCDB";
NSString *const stringMethodNameFormat = @"%@ •••%@";
NSString *const stringBankPrefix = @"NH ";
NSString *const stringBankAccountFormat = @"Tài khoản %@";
NSString *const stringUpdateNewVersionFormat = @"Quý khách vui lòng nâng cấp phiên bản mới nhất để thực hiện %@ với %@";
NSString *const stringErrorMinVersion = @"Cập nhật phiên bản mới nhất để sử dụng";
NSString *const stringInputCardNumber = @"Nhập số thẻ";
NSString *const stringErrorOverAmountPerTrans = @"Vượt quá hạn mức một lần giao dịch.";
NSString *const stringVietcomBankCode = @"VCB";
NSString *const stringWarningCancelPayment = @"Bạn có chắc muốn hủy %@ này không ?";
NSString *const stringAlertTitleYes = @"Có";
NSString *const stringAlertTitleNo = @"Không";
NSString *const stringErrorCardIsExist = @"%@ đã được liên kết trước đó";
NSString *const stringResultNavTitle = @"Kết quả";
NSString *const stringAlertTitleCancel = @"Bỏ qua";
NSString *const stringAlertTitleAccept = @"Đồng ý";
NSString *const stringResultSuccess = @"thành công";
NSString *const stringResultFailed = @"thất bại";
NSString *const stringTitleMapcard = @"Liên kết";
NSString *const stringTitleRemoveMapcard = @"Hủy liên kết";
NSString *const stringSupportTitle = @"Yêu cầu hỗ trợ?";
NSString *const stringTypeErrorMessageNetworkError = @"Mạng không ổn định, vui lòng kiểm tra kết quả giao dịch trong phần Lịch sử giao dịch sau 20 phút";
NSString *const stringTypeErrorMessageUnidentified = @"Giao dịch đang xử lý, vui lòng kiểm tra kết quả giao dịch trong phần Lịch sử giao dịch sau 20 phút";
NSString *const stringTitleInputCaptcha = @"Nhập mã captcha";
NSString *const stringMethodBankAccountFormat = @"%@ %@•••%@";
NSString *const stringTitleUpdatelevel3 = @"Cập nhật thông tin";
NSString *const stringBankAccountPayByCardFormat = @"%@ chỉ hỗ trợ Liên kết tài khoản.\nVui lòng liên kết tài khoản để tiếp tục thanh toán.";
NSString *const stringBankSupportMapcardFormat = @"Ngân hàng %@ chỉ hỗ trợ Liên kết tài khoản.\nHãy liên kết tài khoản để tiếp tục.";
NSString *const stringRegisterBIDV = @"Đăng Ký BIDV Online";
NSString *const stringDefaultMaintainFormat = @"%@ bảo trì %@ tới %@, vui lòng chọn ngân hàng khác hoặc quay lại sau.";
NSString *const stringDefaultUnSupportFormat = @"%@ chưa hỗ trợ %@, vui lòng chọn ngân hàng khác hoặc quay lại sau.";
NSString *const stringAlertAlreadyMapBankAccount = @"Bạn đã liên kết tài khoản ngân hàng này. Hãy chọn tài khoản đã liên kết trước đó để thanh toán.";
NSString *const stringAlertViewBankAcocuntFormat = @"%@ chỉ hỗ trợ Liên kết tài khoản. Bạn có muốn hiển thị Tài khoản đã liên kết?";
NSString *const stringAlertMaxCCCardFormat = @"Bạn chỉ có thể liên kết tối đa %d thẻ Visa/Master/JCB. Vui lòng hủy liên kết thẻ cũ để có thể liên kết với thẻ mới.";
NSString *const stringAlertPreventCCCardWhenPay = @"Thẻ Visa/Master/JCB không hỗ trợ %@. Vui lòng chọn loại thẻ khác.";
NSString *const stringMapcardInfoFormat = @"Liên kết để tiếp tục %@";
NSString *const stringGettingPaymentStatus = @"Đang kiểm tra giao dịch";
NSString *const stringTitleOriginalAmount = @"Giá ban đầu";
NSString *const stringTitleDiscountAmount = @"Ưu đãi";
NSString *const stringMapcardSuccess = @"Liên kết thành công";
NSString *const stringMessageCCCardUnSupport = @"ZaloPay chỉ hỗ trợ liên kết các thẻ được phát hành bởi ngân hàng trong nước, vui lòng nhập thẻ khác";
NSString *const stringMessageBankSupportMapAccount = @"%@ chỉ hỗ trợ Liên kết tài khoản. Đồng ý để tiếp tục liên kết";
NSString *const stringMessageMinAmount = @"Giao dịch tối thiểu từ %@";
NSString *const stringMessageMaxAmount = @"Giao dịch tối đa %@";
NSString *const stringMessageVCBMinimumBanlance = @"Số dư trong TK sau giao dịch phải lớn hơn 50k";

#pragma mark -  Key Notification
NSString *const ZPUIKeyboardDidHideNotification = @"hideKeyboard";
NSString *const ZPUIKeyboardDidShowNotification = @"showKeyboard";
NSString *const ZPUIButtonPreNextDidHideNotification = @"hideBtnPreNext";
NSString *const ZPCurrentBalanceChange = @"currentBalanceChange";

NSString *const MASTER    = @"123PMASTER";
NSString *const JCB       = @"123PJCB";
NSString *const VISA      = @"123PVISA";
NSString *const MASTERDEBIT    = @"123PMASTERDEBIT";
NSString *const JCBDEBIT       = @"123PJCBDEBIT";
NSString *const VISADEBIT      = @"123PVISADEBIT";
NSString *const BIDV           = @"BIDV";
