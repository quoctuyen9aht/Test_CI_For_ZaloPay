//
//  ZaloMobilePaymentSDK.h
//  ZPSDK
//
//  Created by phungnx on 12/22/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZPDefine.h"

@class ZPBill;
@class ZPPaymentResponse;
@class ZPAppData;
@class ZPSavedCard;
@class ZPSavedBankAccount;
@class ZPPaymentMethod;
@class RACSignal;

@interface ZaloMobilePaymentSDK : NSObject
@property (strong, nonatomic) UIViewController *parentController;
@property (nonatomic, weak) UINavigationController *navigationController;

- (void)initPaymentSDK;

- (RACSignal *)removeSavedCard:(ZPSavedCard *)card;

- (RACSignal *)removeBankAccount:(NSString*)bankCustomerId bankAccount:(ZPSavedBankAccount*)bankAccount;

- (void)payWithBill:(ZPBill *)bill;

- (void)linkCardWithBill:(ZPBill *)bill;

- (void)processBankAccountWithBill:(ZPBill *)bill;

- (void)closePaymentAndCleanSDK;

- (BOOL)checkIsCurrentPaymentFinished;

- (BOOL)isPayingBill;

- (void)closePaymentAndCleanUp;

- (void)dismissSDKKeyboard;

- (void)showSDKKeyboard;
@end
