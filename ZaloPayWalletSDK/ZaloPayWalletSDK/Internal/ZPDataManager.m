//
//  ZPDataManager.m
//  ZPSDK
//
//  Created by phungnx on 12/22/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import "ZPDataManager.h"
#import "ZPPaymentChannel.h"
#import "ZPResourceManager.h"
#import "NSString+ZPExtension.h"
#import "UIDevice+ZPExtension.h"
#import "ZPCellObject.h"
#import "ZPPaymentInfo.h"
#import "ZPPaymentViewCell.h"
#import "ZPProgressHUD.h"
#import "ZPBank.h"
#import "ZPMiniBank.h"
#import "ZaloPayWalletSDKPayment.h"
#import "ZPPaymentResponse.h"
#import "ZPPaymentMethod.h"
#import "UIImage+ZPExtension.h"
#import "ZPPaymentManager.h"
#import "ZPBill.h"
#import "NetworkManager+ZaloPayWalletSDK.h"
#import <ZaloPayCommon/NSArray+Safe.h>
#import "NSData+ZPExtension.h"
#import "ZPBIDVHandler.h"
#import "ZPDataManager+ObserverNewCard.h"
#import "ZaloPayWalletSDKLog.h"
#import <ZaloPayCommon/FileUtils.h>
#import <ZaloPayCommonSwift/ZaloPayCommonSwift-Swift.h>
#import "ZPSDKAppInfoManager.h"

#define kDataManagerGetPlatforminfo        @"zp_datamanager_gw_info_key"
#define kChecksumVersion                   [NSString stringWithFormat:@"checksum-%@%@",[UIDevice currentDevice].zpAppVersion,[UIDevice currentDevice].zpAppBuildVersion]
#define kResourceVersion                   [NSString stringWithFormat:@"resourceVersion-%@%@",[UIDevice currentDevice].zpAppVersion,[UIDevice currentDevice].zpAppBuildVersion]


@implementation ZPConfigData

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        _resourceTimeout =[aDecoder decodeDoubleForKey:@"resourceTimeout"];
        _resourceVersion = [aDecoder decodeObjectForKey:kResourceVersion];
        _resourceLink = [aDecoder decodeObjectForKey:@"resourceLink"];
        _gwInfoTimeout =[aDecoder decodeDoubleForKey:@"gwInfoTimeout"];
        _resourceLastGetTime =[aDecoder decodeDoubleForKey:@"resourceLastGetTime"];
        _gwInfoLastGetTime =[aDecoder decodeDoubleForKey:@"gwInfoLastGetTime"];
        _gatewayInfo = [aDecoder decodeObjectForKey:@"gatewayInfo"];
        _checksum = [aDecoder decodeObjectForKey:kChecksumVersion];
        _expiredTime = [aDecoder decodeDoubleForKey:@"expiredTime"];
        _cachedCardList = [aDecoder decodeObjectForKey:@"cachedCardList"];
        _cardinfochecksum = [aDecoder decodeObjectForKey:@"cardinfochecksum"];
        _bankaccountchecksum = [aDecoder decodeObjectForKey:@"bankaccountchecksum"];
        _cachedBankAccountList = [aDecoder decodeObjectForKey:@"cachedBankAccountList"];
        
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeDouble:_resourceTimeout forKey:@"resourceTimeout"];
    [aCoder encodeObject:_resourceVersion forKey:kResourceVersion];
    [aCoder encodeObject:_resourceLink forKey:@"resourceLink"];
    [aCoder encodeDouble:_gwInfoTimeout forKey:@"gwInfoTimeout"];
    [aCoder encodeDouble:_resourceLastGetTime forKey:@"resourceLastGetTime"];
    [aCoder encodeDouble:_gwInfoLastGetTime forKey:@"gwInfoLastGetTime"];
    [aCoder encodeObject:_gatewayInfo forKey:@"gatewayInfo"];
    [aCoder encodeObject:_checksum forKey:kChecksumVersion];
    [aCoder encodeDouble:_expiredTime forKey:@"expiredTime"];
    [aCoder encodeObject:_cachedCardList forKey:@"cachedCardList"];
    [aCoder encodeObject:_cardinfochecksum forKey:@"cardinfochecksum"];
    [aCoder encodeObject:_bankaccountchecksum forKey:@"bankaccountchecksum"];
    [aCoder encodeObject:_cachedBankAccountList forKey:@"cachedBankAccountList"];
}

- (void)save {
    NSData * data = [NSKeyedArchiver archivedDataWithRootObject:self];
    [ZPUserDefaults setObject:data forKey:kDataManagerGetPlatforminfo];
    [ZPUserDefaults synchronize];
}

- (void)addCardInfo:(NSDictionary *)cardInfo {
    NSMutableArray *allCard = [NSMutableArray array];
    if (self.cachedCardList.count > 0) {
        [allCard addObjectsFromArray:self.cachedCardList];
    }
    [allCard addObjectNotNil:cardInfo];
    @synchronized (self) {
        self.cachedCardList = allCard;
        [self save];
    }
}

- (void)updateWithDict:(NSDictionary *)dictionary {
    
    NSString *platforminfochecksum = [dictionary objectForKey:@"platforminfochecksum"];
    self.isModify = ![platforminfochecksum isEqualToString:self.checksum];
    if(self.isModify){
        self.checksum = platforminfochecksum;
        NSMutableDictionary *gatewayInfo = [[NSMutableDictionary alloc] init];
        id info  = [dictionary objectForKey:@"info"];
        id transtypepmcs = [dictionary objectForKey:@"transtypepmcs"];
        [gatewayInfo setObjectCheckNil:info forKey:@"info"];
        [gatewayInfo setObjectCheckNil:transtypepmcs forKey:@"transtypepmcs"];
        self.gatewayInfo = gatewayInfo;
    }
    
    //Compare cardinfochecksum
    NSString *cardinfochecksum = [dictionary objectForKey:@"cardinfochecksum"];
    if(![cardinfochecksum isEqualToString:self.cardinfochecksum]){
        //Update cache
        self.cardinfochecksum =  cardinfochecksum;
        self.cachedCardList = [dictionary arrayForKey:@"cardinfos"];
    }
    
    //Compare bankaccountchecksum
    NSString *bankaccountchecksum = [dictionary objectForKey:@"bankaccountchecksum"];
    if(![bankaccountchecksum isEqualToString:self.bankaccountchecksum]){
        //Update cache
        self.bankaccountchecksum =  bankaccountchecksum;
        self.cachedBankAccountList = [dictionary arrayForKey:@"bankaccounts"];
    }
    
    NSArray *fieldToUpdate = [self fieldDataUpdateFromFlatformInfo:self.isModify && dictionary];
    NSDictionary *dicCache = [self getPlatformInfoConfigAPP];
    NSMutableDictionary *dicCacheNew = [[NSMutableDictionary alloc] init];
    if(dicCache) {
        [dicCacheNew addEntriesFromDictionary:dicCache];
    }
    for (NSString *key in fieldToUpdate) {
        id value = [dictionary objectForKey:key];
        [dicCacheNew setObjectCheckNil:value forKey:key];
    }
    [self setCachedPlatformInfo:dicCacheNew forKey:kZPCachePlatformInfo];
    
    if ([dictionary boolForKey:@"forceappupdate"]) {
        return;
    }
    
    // resource version
    self.isModifyResource = [dictionary boolForKey:@"isupdateresource"];
    NSDictionary * sdkResource = [dictionary dictionaryForKey:@"resource"];
    
    if (sdkResource && (self.isModifyResource || [[ZPDataManager sharedInstance] shouldDownloadResource])) {
        DDLogInfo(@"sdkResource %@",sdkResource);
        self.resourceLink = [sdkResource stringForKey:@"rsurl"];
        self.resourceVersion = [sdkResource stringForKey:@"rsversion"];
    }
    
    self.gwInfoTimeout = [[NSDate date] timeIntervalSince1970] + self.expiredTime;
    self.expiredTime = [[dictionary objectForKey:@"expiredtime"] doubleValue]/1000;
    self.gwInfoLastGetTime = [[NSDate date] timeIntervalSince1970];
    
    [self save];
}

- (NSArray *)fieldDataUpdateFromFlatformInfo:(BOOL)isUpdate {
    if (isUpdate) {
        return  @[@"approvedinsideappids",
                  @"bannerresources",
                  @"expiredtime",
                  @"newestappversion",
                  @"isenabledeposit",
                  @"forceappupdate",
                  @"forceupdatemessage",
                  @"returncode",
                  @"returnmessage",
                  @"resource",
                  @"ismaintainwithdraw",
                  @"maintainwithdrawfrom",
                  @"maintainwithdrawto"];
        
    }
    return  @[@"isenabledeposit",
              @"forceappupdate",
              @"forceupdatemessage",
              @"returncode",
              @"returnmessage",
              @"expiredtime",
              @"newestappversion",
              @"resource",
              @"ismaintainwithdraw",
              @"maintainwithdrawfrom",
              @"maintainwithdrawto"];
}

- (void)setCachedPlatformInfo: (NSDictionary*)dic forKey: (NSString *)key{
    if ([ZPUserDefaults objectForKey:key] != nil) {
        [ZPUserDefaults removeObjectForKey:key];
    }
    [ZPUserDefaults setObject:dic forKey:key];
    [ZPUserDefaults synchronize];
}
- (NSDictionary *) getPlatformInfoConfigAPP {
    return [ZPUserDefaults objectForKey:kZPCachePlatformInfo];
}
- (NSDictionary *) getPlatformInfoConfigSDK {
    return [ZPUserDefaults objectForKey:kDataManagerGetPlatforminfo];
}

@end

@interface ZPDataManager() {
    NSBundle * bundle;
    NSString * bundlePath;
}
@property (nonatomic, strong) RACSubject *platformSignal;
@property (nonatomic) BOOL gettingPlatformInfo;
@end

@implementation ZPDataManager

@synthesize configData;

+ (instancetype)sharedInstance {
    static ZPDataManager * _sharedInstance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        _sharedInstance = [[ZPDataManager alloc] init];
        [_sharedInstance startObserverNewCard];
    });
    
    return _sharedInstance;
}

- (id)init {
    self = [super init];
    if (self) {
        _addNewCardSignal = [RACSubject subject];
        _platformSignal = [RACSubject subject];
        _appInfoManager = [[ZPSDKAppInfoManager alloc] init];
        _bankManager = [[ZPBankManager alloc] init];
    }
    return self;
}

- (void)initDataWithCallback:(void (^)(int errorCode, NSString * message)) callback {
    [self loadData];
    [self getPaymentGatewayInfoCompleteHandle:callback];
}

- (void)requestUpdateCardList:(dispatch_block_t)callback {
    self.cachedData.gwInfoTimeout = [[NSDate date] timeIntervalSince1970];
    self.cachedData.cardinfochecksum = @"";
    [self.cachedData save];
    [self initDataWithCallback:^(int errorCode, NSString * message) {
        if (callback) {
            callback();
        }
    }];
}

- (void)requestUpdateAccountList:(dispatch_block_t)callback {
    self.cachedData.gwInfoTimeout = [[NSDate date] timeIntervalSince1970];
    self.cachedData.bankaccountchecksum = @"";
    [self.cachedData save];
    [self initDataWithCallback:^(int errorCode, NSString * message) {
        if (callback) {
            callback();
        }
    }];
}

- (RACSignal *)loadCardListAndBankAccount {
    self.cachedData.gwInfoTimeout = [[NSDate date] timeIntervalSince1970];
    self.cachedData.cardinfochecksum = @"";
    self.cachedData.bankaccountchecksum = @"";
    [self.cachedData save];
    @weakify(self);
    return [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
        @strongify(self);
        [self initDataWithCallback:^(int errorCode, NSString * message) {
            [subscriber sendNext:@(errorCode)];
            [subscriber sendCompleted];
        }];
        return nil;
    }];
}

- (void)getPlatformInfoConfig:(void (^)(NSDictionary *))callback {
    if ([self shouldGetGatewayInfo]){
        [self initDataWithCallback:^(int errorCode, NSString * message) {
            DDLogInfo(@"paymentMethods with error %@", message);
            callback(self.getPlatformInfoConfig);
        }];
        return;
    }
    
    if(self.getPlatformInfoConfig != nil) {
        callback(self.getPlatformInfoConfig);
        return;
    }
    self.cachedData.gwInfoTimeout = [[NSDate date] timeIntervalSince1970];
    [self.cachedData save];
    [self initDataWithCallback:^(int errorCode, NSString * message) {
        DDLogInfo(@"paymentMethods with error %@", message);
        callback(self.getPlatformInfoConfig);
    }];
}

- (BOOL)shouldDownloadResource {
    if(configData == nil ) {
        DDLogInfo(@"configData is nil, call loadData");
        [self loadData];
    }
    return configData == nil;
}

- (BOOL) shouldGetGatewayInfo {
    return ([[NSDate date] timeIntervalSince1970] >= self.cachedData.gwInfoTimeout)
    ||([self shouldDownloadResource]);
}

- (void)configPaymentApp{
    _paymentSavedCard = [self parseSaveCard:self.cachedData.cachedCardList];
    _paymentSavedBankAccount = [self parseSaveBankAccount];
}

- (NSArray *)allSaveCardsFromCache {
    return [self parseSaveCard:self.cachedData.cachedCardList];
}

- (NSArray *)parseSaveCard:(NSArray *)cachedCardList {
    NSMutableArray *allAccount = [NSMutableArray array];
    for (NSDictionary *oneDic in cachedCardList) {
        ZPSavedCard *account = [ZPSavedCard fromJson:oneDic];
        [allAccount addObject:account];
    }
    return allAccount;
}

- (BOOL)isCardSaved:(NSString *)first6CardNo andLast4CardNo:(NSString *)last4CardNo {
    for (ZPSavedCard * saveCard in self.paymentSavedCard){
        if ([saveCard.first6CardNo isEqualToString:first6CardNo] &&
            [saveCard.last4CardNo isEqualToString:last4CardNo]) {
            return YES;
        }
    }
    return NO;
}

- (NSArray *)parseSaveBankAccount {
    NSMutableArray *allAccount = [NSMutableArray array];
    for (NSDictionary *oneDic in self.cachedData.cachedBankAccountList) {
        ZPSavedBankAccount *account = [ZPSavedBankAccount fromJson:oneDic];
        [allAccount addObject:account];
    }
    return allAccount;
}

#pragma mark - Get Payment Info

- (void)getPaymentGatewayInfoCompleteHandle:(void (^) (int errorCode, NSString *mesage))callBack {
    
    if (!callBack) {
        DDLogInfo(@"callback = nil");
        return;
    }
    [[self.platformSignal take:1] subscribeNext:^(NSNumber *code) {
        int errorCode = [code intValue];
        NSString *message = errorCode < 0 ? [self messageForKey:@"message_maintainance"] : nil;
        callBack(errorCode, message);
    }];
    
    [self getPlatformInfo];
}

- (void)getPlatformInfo {
    
    if ([self shouldGetGatewayInfo] == FALSE) {
        DDLogInfo(@"shouldGetGatewayInfo = false");
        [self doneGetPlatformInfo:0];
        return;
    }
    
    if (self.gettingPlatformInfo) {
        DDLogInfo(@"gettingPlatformInfo");
        return;
    }
    
    self.gettingPlatformInfo = true;
    
    NSString *checksum = self.cachedData.checksum;
    NSString *resourceVersion = [self resourceVersion3];
    NSString *cardinfoCheckSum = self.cachedData.cardinfochecksum;
    NSString *bankaccountchecksum = self.cachedData.bankaccountchecksum;
    
    [[[[ZaloPayWalletSDKPayment sharedInstance] getPlatformInfo:checksum
                                                resourceVersion:resourceVersion
                                               cardinfochecksum:cardinfoCheckSum
                                            bankaccountchecksum:bankaccountchecksum] deliverOnMainThread] subscribeNext:^(NSDictionary *dictionary) {
        
        [self.cachedData updateWithDict:dictionary];
        [self configPaymentApp];
        if ([dictionary boolForKey:@"forceappupdate"]) {
            [self doneGetPlatformInfo:0];
            return;
        }
        
        BOOL needDownload = [self shouldDownloadResource] || self.cachedData.isModifyResource;
        if (!needDownload) {
            [self doneGetPlatformInfo:0];
            return;
        }
        
        if (self.cachedData.resourceLink.length <= 0) {
            [self doneGetPlatformInfo:kZPZaloPayCreditErrorCodeGettingResourceFailed];
            return;
        }
        
        [self downloadResourceFromLink:self.cachedData.resourceLink completeHandle:^(int errorCode) {
            [self doneGetPlatformInfo:errorCode];
        }];
        
    } error:^(NSError *error) {
        [self configPaymentApp];
        self.cachedData.gwInfoTimeout = [[NSDate date] timeIntervalSince1970];
        [self.cachedData save];
        [self doneGetPlatformInfo:kZPZaloPayCreditErrorCodeGettingResourceFailed];
    }];
}

- (void)doneGetPlatformInfo:(int)errorCode {
    self.gettingPlatformInfo = false;
    [self.platformSignal sendNext:@(errorCode)];
}


- (NSDictionary *) getPlatformInfoConfig {
    return [ZPUserDefaults objectForKey:kZPCachePlatformInfo];
}

- (RACSignal *)removeSavedCard:(ZPSavedCard *)card {
    DDLogInfo(@"removeSavedCard: %@", card);
    return [[[ZaloPayWalletSDKPayment sharedInstance] removeCard:card.bankCode
                                                    first6cardno:card.first6CardNo
                                                     last4cardno:card.last4CardNo] doNext:^(NSDictionary *dictionary)
            {
                NSArray *cardinfos = [dictionary arrayForKey:@"cardinfos"];
                if (cardinfos) {
                    self.cachedData.cachedCardList = cardinfos;
                    self.cachedData.cardinfochecksum = @"";
                    [self.cachedData save];
                    self.paymentSavedCard = [self parseSaveCard:cardinfos];
                }
            }];
}

- (RACSignal *)removeBankAccount:(NSString*)bankCustomerId bankAccount:(ZPSavedBankAccount*)bankAccount {
    DDLogInfo(@"removeBankAccount with bankcode: %@", bankAccount.bankCode);
    return [[[ZaloPayWalletSDKPayment sharedInstance] removeBankAccountWith:bankCustomerId bankCode:bankAccount.bankCode firstaccountno:bankAccount.firstAccountNo lastaccountno:bankAccount.lastAccountNo] doNext:^(NSDictionary *dictionary)
            {
                NSArray *bankaccounts = [dictionary arrayForKey:@"bankaccounts"];
                if (bankaccounts) {
                    self.cachedData.cachedBankAccountList  = bankaccounts;
                    [self.cachedData save];
                    self.paymentSavedBankAccount = [self parseSaveBankAccount];
                }
        }];
}

- (RACSignal *)addSavedCard:(NSDictionary *)bankInfo {
    DDLogInfo(@"addSavedCard with transid: %@", bankInfo);
    NSString *zpTransID = [bankInfo stringForKey:@"zptransid"];
    NSString *checkSum = self.cachedData.cardinfochecksum;
    
    return [[[ZaloPayWalletSDKPayment sharedInstance] getListCardInfo:zpTransID cardInforCheckSum:checkSum] doNext:^(NSDictionary *dictionary) {
        NSArray *cardinfos = [dictionary objectForKey:@"cardinfos"];
        NSString *cardinfochecksum = [dictionary stringForKey:@"cardinfochecksum"];
        if (cardinfos && cardinfochecksum) {
            self.cachedData.cachedCardList  = [cardinfos mutableCopy];
            self.cachedData.cardinfochecksum = cardinfochecksum;
            [self.cachedData save];
            self.paymentSavedCard =  [self parseSaveCard:self.cachedData.cachedCardList];
            
            NSInteger appId = self.bill.appId;
            // map thẻ mới khi lixi thì không show popup
            if(appId == lixiAppId) {
                return;
            }
            if([ZaloPayWalletSDKPayment sharedInstance].handleMapcard && cardinfos.count > 0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSDictionary *mapcardInfo = [self getSaveCardTitleFromConfig:bankInfo];
                    [ZaloPayWalletSDKPayment sharedInstance].handleMapcard(mapcardInfo);
                });
            }
        }
    }];
}

- (void)updateListBankAccount:(void (^)(NSArray* listAccount, NSError *error)) callback {
    
    [[[[ZaloPayWalletSDKPayment sharedInstance] getListBankAccount:self.cachedData.bankaccountchecksum] deliverOnMainThread] subscribeNext:^(NSDictionary *dictionary) {
        
        NSString *checksum = [dictionary stringForKey:@"bankaccountchecksum"];
        if(![checksum isEqualToString:self.cachedData.bankaccountchecksum]){
            self.cachedData.cachedBankAccountList  = [dictionary arrayForKey:@"bankaccounts"];
            self.cachedData.bankaccountchecksum = checksum;
            [self.cachedData save];
            self.paymentSavedBankAccount = [self parseSaveBankAccount];
        }
        callback(self.paymentSavedBankAccount, nil);
    } error:^(NSError *error) {
        callback(self.paymentSavedBankAccount, error);
    }];
}

-(NSDictionary *)getSaveCardTitleFromConfig:(NSDictionary *)bankInfo{
    
    NSString *bankCode = [bankInfo stringForKey:@"tpebankcode"];
    NSString *first6CardNo = [bankInfo stringForKey:@"first6cardno"];
    NSString *last4CardNo = [bankInfo stringForKey:@"last4cardno" defaultValue:@""];
    NSString *titleStr = @"";
    NSString *bankIcon = @"";
    NSString *bankName = @"";
    
    if ([bankCode isEqualToString:stringCreditCardBankCode] || [bankCode isEqualToString:stringDebitCardBankCode]) {
        bankCode = [ZPResourceManager getCcBankCodeFromConfig:first6CardNo];
        NSDictionary *bankDict = [[ZPDataManager sharedInstance].bankManager.ccBanksDict objectForKey:bankCode];
        bankName = [bankDict stringForKey:@"bank_name" defaultValue:@""];
    } else {
        ZPBank *bank = [self.bankManager getBankInfoWith:bankCode];
        bankName = [bank.bankName removePrefix:@"NH "];
    }
    
    bankIcon = [ZPResourceManager bankIconFromBankCode:bankCode];
    titleStr =  [NSString stringWithFormat:@"Thẻ %@ •••%@",bankName ?: @"", last4CardNo];
    
    return @{@"last4cardno":last4CardNo,
             @"bankicon": bankIcon,
             @"bankname":titleStr
             };;
    
}

- (void)downloadResourceFromLink:(NSString *)link completeHandle:(void (^) (int errorCode))callBack{
    NSString *zipPath = [self tempZipPath];
    NSString *dataPath = [self dataPath];
    NSString *attractPath = [self tempAttractPath];
    
    [FileUtils removeFolderAtPath:zipPath];
    [FileUtils removeFolderAtPath:dataPath];
    [FileUtils removeFolderAtPath:attractPath];
    
    @weakify(self);
    [[[[ZaloPayWalletSDKPayment sharedInstance].network downloadZipFileWithUrl:link
                                                                        toPath:zipPath
                                                                   attractPath:attractPath
                                                                      progress:nil] deliverOnMainThread]  subscribeError:^(NSError *error) {
        @strongify(self);
        [self removeBundleResouce];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (callBack) {
                callBack(kZPZaloPayCreditErrorCodeGettingResourceFailed);
            }
        });
        
    } completed:^{
        @strongify(self);
        [FileUtils createFolderAtPath:dataPath];
        [FileUtils copyFolderAtPath:attractPath toPath:dataPath overrideOldFile:YES];
        self->bundle = nil;
        self->bundlePath = nil;
        [self loadConfigData];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (callBack) {
                callBack(0);
            }
        });
    }];
}

- (void)removeBundleResouce {
    if (bundlePath) {
        [[NSFileManager defaultManager] removeItemAtPath:bundlePath error:nil];
    }
    bundle = nil;
    bundlePath = nil;
    self.configData = nil;
    self.cachedData = nil;
    [self clearPaymentCache];
}

- (ZPConfigData *)cachedData {
    if (_cachedData) {
        return _cachedData;
    }
    NSData * data = [ZPUserDefaults objectForKey:kDataManagerGetPlatforminfo];
    _cachedData = data ? [NSKeyedUnarchiver unarchiveObjectWithData:data] : [[ZPConfigData alloc] init];
    return _cachedData;
}

- (void) loadData{
    [self loadConfigData];    
    [self configPaymentApp];
}

- (void) loadConfigData {
    configData = [NSMutableDictionary dictionaryWithContentsOfFile:[self dictFilePath]];
}

- (NSString *)dataPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * dataPath = [basePath stringByAppendingPathComponent:@"zp"];
    return dataPath;
}

- (NSString *)tempAttractPath {
    NSString * attractPath = [[self cachePath] stringByAppendingPathComponent:@"zalopayattract"];
    return attractPath;
}

- (NSString *)cachePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    return paths.firstObject;
}

- (NSString *)tempZipPath {
    return [[self cachePath] stringByAppendingPathComponent:@"PaymentBundle.zip"];
}

- (NSString *)zipfilePath{
    return [[self dataPath] stringByAppendingPathComponent:@"PaymentBundle.zip"];
}

- (NSString *)dictFilePath{
    return [[self bundlePath] stringByAppendingPathComponent:@"views.plist"];
}

- (NSString *) bundlePath {
    if(bundlePath.length > 0) return bundlePath;
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSString *dataPath = [self dataPath];
    NSDirectoryEnumerator *enumerator = [fileMgr enumeratorAtPath:dataPath];
    NSString * path = nil;
    while((path = enumerator.nextObject) != nil) {
        if([path hasSuffix:@"bundle"] && [path hasPrefix:@"ZaloPayWalletSDK.PaymentBundle"]) {
            path = [dataPath stringByAppendingPathComponent:path];
            break;
        }
    }
    
    bundlePath = path;
    return bundlePath;
}

- (NSBundle *) bundle {
    if(bundle || bundlePath.length <= 0) return bundle;
    
    bundle = [NSBundle bundleWithPath:[self bundlePath]];
    return bundle;
}

- (UIView *) viewWithNibName: (NSString *) name owner: (id) owner {
    NSBundle * bdl = [self bundle];
    NSArray * views = [bdl loadNibNamed:name owner:owner options:nil];
    return views.count > 0 ? views[0] : [[UIView alloc] init];
}

- (UINib *) nibNamed: (NSString *) name {
    return [UINib nibWithNibName:name bundle:[self bundle]];
}

- (NSDictionary *)viewsConfig{
    return [configData objectForKey:@"views"];
}

- (NSDictionary *)stringConfig{
    return [configData objectForKey:@"strings"];
}

- (NSDictionary *)numberConfig {
    return [configData objectForKey:@"numbers"];
}

- (NSString *) messageForKey: (NSString *) key {
    return [[ZPResourceManager sharedInstance] messageForKey:key stringConfig:[self stringConfig]];
}

- (NSString *)stringByEnum:(ZPStringEnum)eString {
    return [[ZPResourceManager sharedInstance] stringByEnum:eString];
}

- (CGFloat)heightRatio {
    if (!IS_IPAD) return 1.0;
    NSNumber * number = [configData objectForKey:@"ipad_height_ratio"];
    return number ? [number floatValue] : 1.1f;
}

- (CGFloat)widthRatio {
    if (!IS_IPAD) return 1.0;
    NSNumber * number = [configData objectForKey:@"ipad_width_ratio"];
    return number ? [number floatValue] : 1.3f;
}

- (NSString *)currencyUnit{
    NSString * unit = [configData objectForKey:@"currency_unit"];
    return unit ? unit : @"VND";
}

- (double)doubleForKey:(NSString *)key {
    NSNumber * number = [[self numberConfig] objectForKey:key];
    if (number) return [number doubleValue];
    
    if ([key isEqualToString:@"atm_card_number_max_length"]){
        return 19;
    }
    else if ([key isEqualToString:@"atm_card_holder_max_length"]){
        return 30;
    }
    return 0;
}

- (NSString *)resourceVersion3 {
    DDLogInfo(@"self.cachedData.resourceVersion %@",self.cachedData.resourceVersion);
    return [self shouldDownloadResource] ? nil : self.cachedData.resourceVersion;
}

#pragma mark - Load save atm card

- (ZPAtmCard *)getCachedAtmCard {
    NSData *data = [ZPResourceManager objectForKey:kLastChargedATMCardKey];
    if(!data || ![data isKindOfClass:[NSData class]]) return nil;
    ZPAtmCard *card = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    if(!card || ![card isKindOfClass:[ZPAtmCard class]] || card.cardNumber.length <= 0 || card.cardNumberHash.length <= 0 || card.cardHolderName.length <= 0) return nil;
    
    return card;
}

- (void)saveATMCard:(ZPAtmCard *) card {
    if(!card || card.cardHolderName.length <= 0 || card.bankCode.length <= 0) return;
    NSData * data = [NSKeyedArchiver archivedDataWithRootObject:card];
    [ZPResourceManager saveObject:data forKey:kLastChargedATMCardKey];
}

- (void) deleteSavedAtmCard {
    [ZPResourceManager saveObject:nil forKey:kLastChargedATMCardKey];
}


#pragma mark - Cell Factory
- (NSString *)reuseIdCellWithEnum:(ZPCellType) type{
    return [[ZPResourceManager sharedInstance] reuseIdCellWithEnum:type];
}

- (float)heightCellWithEnum:(ZPCellType) type{
    DDLogInfo(@"heightCellWithEnum, type: %ld", (long)type);
    NSDictionary * dict = [self.viewsConfig objectForKey:[self reuseIdCellWithEnum:type]];
    DDLogInfo(@"heightCellWithEnum, dict: %@", dict);
    ZPCellObject* cellObject = [ZPCellObject initWithDictionary:dict];
    return [ZPPaymentViewCell caculateHeightCellWithGuiObject:cellObject];
}

- (ZPPaymentViewCell *)cellWithEnum:(ZPCellType) type{
    NSDictionary * dict = [self.viewsConfig objectForKey:[self reuseIdCellWithEnum:type]];
    DDLogInfo(@"---cellWithEnum");
    ZPCellObject * cellObject = [ZPCellObject initWithDictionary:dict];
    DDLogInfo(@"---init cell object");
    cellObject.type = type;
    if (cellObject) {
        return [self createCellWithCellObject:cellObject];
    } else{
        return nil;
    }
}

- (ZPPaymentViewCell *)createCellWithCellObject:(ZPCellObject *) object{
    DDLogInfo(@"---createCellWithCellObject");
    ZPPaymentViewCell* paymentViewCell = [[ZPPaymentViewCell alloc] initWithGuiObject:object];
    DDLogInfo(@"---initWithGuiObject");
    if ( !paymentViewCell) {
        return [[ZPPaymentViewCell alloc] init];
    } else{
        return paymentViewCell;
    }
}

- (NSString*)getTitleByTranstype:(ZPTransType)transType{
    switch (transType) {
        case ZPTransTypeBillPay:
            return [self messageForKey:@"title_bill_pay"];
            break;
        case ZPTransTypeWalletTopup:
            return [self messageForKey:@"title_wallet_topup"];
            break;
        case ZPTransTypeAtmMapCard:
            return [self messageForKey:@"title_map_card"];
            break;
        case ZPTransTypeTranfer:
            return [self messageForKey:@"title_tranfer_balance"];
            break;
        case ZPTransTypeWithDraw:
            return [self messageForKey:@"title_withdraw"] ;
            break;
        default:
            return [self messageForKey:@"title_bill_pay"];
            break;
    }
}

- (BOOL)isPLPEnableWithTranstype:(ZPTransType) transType pmcID:(ZPPaymentMethodType)pmcID{
    for(ZPUserPLP* userPLP in self.paymentUserPLP){
        if (userPLP.transType == transType && userPLP.pmcID == pmcID) {
            return YES;
        }
    }
    return NO;
}

- (NSString*)getButtonTitle:(NSString*)key{
    return [self messageForKey:[NSString stringWithFormat:@"btn_%@", key]];
}

- (void) clearPaymentCache {
    [self clearLocalUserCache];
    [self clearConfigCache];
}

-(void)clearConfigCache {
    [_bankManager clearCache];
    [self deleteSavedAtmCard];
}
- (BOOL)checkInternetConnect{
    return [ZaloPayWalletSDKPayment sharedInstance].network.isReachable;
}

#pragma mark - More

-(BOOL)checkSavedCardIsExist:(NSString *)cardNumber{
    for(ZPSavedCard* savedCard in self.paymentSavedCard ){
        if ([cardNumber hasPrefix:savedCard.first6CardNo] && [cardNumber hasSuffix:savedCard.last4CardNo]) {
            return YES;
        }
    }
    return NO;
}

-(NSArray *)getSavedCardsForBank:(NSString *)bankCode{
    NSMutableArray *result = [[NSMutableArray alloc]init];
    for(ZPSavedCard* savedCard in self.paymentSavedCard ){
        if ([bankCode containsString:savedCard.first6CardNo]) {
            [result addObject:savedCard];
        }
    }
    return [result copy];
}

-(NSArray *)getSavedAccountForBank:(NSString *)bankCode{
    NSMutableArray *result = [[NSMutableArray alloc]init];
    for(ZPSavedBankAccount* savedCard in self.paymentSavedBankAccount){
        if ([bankCode containsString:savedCard.bankCode]) {
            [result addObject:savedCard];
        }
    }
    return [result copy];
}

- (BOOL)chekMinAppVersion:(NSString *)minAppVersion{
    NSString *currentVersion = [[UIDevice currentDevice] zpAppVersion];
    return ![NSString isEqualOrNewerThanVersionWithCurrentVersion:currentVersion checkVersion:minAppVersion];
}

- (BOOL)isMaxCCSavedCard {
    NSString *errorMessage = [self checkMaxCCSavedCard];
    if (errorMessage.length > 0) {
        return YES;
    }
    return NO;
}

- (void)showAlertMaxCCSavedCard {
    NSString *errorMessage = [self checkMaxCCSavedCard];
    if (errorMessage.length > 0) {
        [ZPDialogView showDialogWithType:DialogTypeNotification
                                 message:errorMessage
                       cancelButtonTitle:[R string_ButtonLabel_Close]
                        otherButtonTitle:nil
                          completeHandle:nil];
    }
}

- (NSString*)checkMaxCCSavedCard {
    int count = 0;
    for (ZPSavedCard * saveCard in self.paymentSavedCard){
        if ([saveCard.bankCode isEqualToString:stringCreditCardBankCode]) {
            count += 1;
        }
    }
    id <ZPWalletDependenceProtocol>helper = [ZaloPayWalletSDKPayment sharedInstance].appDependence;
    
    int config = helper.maxCCCard;
    if (config <= 0) {
        config = MAX_CC_SAVED_CARD;
    }
    
    if (count >= config) {
        NSString *alertMessage = [NSString stringWithFormat:stringAlertMaxCCCardFormat,config];
        return alertMessage;
    }
    return @"";
}

- (NSString*)checkErrorMappCCCardWith:(ZPTransType)transType appId:(int32_t)appId {
    if (transType == ZPTransTypeWithDraw) {
        return [NSString stringWithFormat:stringAlertPreventCCCardWhenPay,R.string_Withdraw_Title];
    }
    BOOL supportCCDebit = [[ZaloPayWalletSDKPayment sharedInstance].appDependence isSupportCCDebit];
    if (!supportCCDebit && (transType == ZPTransTypeWalletTopup || transType == ZPTransTypeTranfer)) {
        return [NSString stringWithFormat:stringAlertPreventCCCardWhenPay,R.string_AddCash_Title];
    }
    if (appId == lixiAppId && !supportCCDebit) {
        return [NSString stringWithFormat:stringAlertPreventCCCardWhenPay,R.string_Home_Lixi];
    }
    return @"";
}

- (ZPChannel *)channelWithTypeAndBankCode:(ZPPaymentMethodType)type bankCode:(NSString*)bankCode {
    ZPPaymentMethodType correctMethodType = [self convertMethodType:type bankCode:bankCode];
    for (ZPChannel * channel in self.paymentChannel){
        if (channel.channelType == correctMethodType && [channel.bankCode isEqualToString:bankCode]){
            return channel;
        }
    }
    return nil;
}

- (ZPPaymentMethodType)convertMethodType:(ZPPaymentMethodType)type bankCode:(NSString*)bankCode {
    if (type == ZPPaymentMethodTypeZaloPayWallet) {
        return ZPPaymentMethodTypeZaloPayWallet;
    }
    if (type == ZPPaymentMethodTypeSavedAccount) {
        return ZPPaymentMethodTypeIbanking;
    }
    if ([self isCCCard:bankCode]) {
        return ZPPaymentMethodTypeCreditCard;
    }
    
    if ([self isCCDebitCard:bankCode]) {
        return ZPPaymentMethodTypeCCDebit;
    }
    
    if (type == ZPPaymentMethodTypeSavedCard) {
        return ZPPaymentMethodTypeAtm;
    }
    return type;
    
}

- (BOOL)isCCCard:(NSString*)bankCode {
    if (bankCode.length == 0) {
        return false;
    }
    return  ([bankCode isEqualToString:stringCreditCardBankCode]
             ||  [bankCode isEqualToString:VISA]
             ||  [bankCode isEqualToString:MASTER]
             ||  [bankCode isEqualToString:JCB]
             );
}

- (BOOL)isCCDebitCard:(NSString*)bankCode {
    if (bankCode.length == 0) {
        return false;
    }
    return  ([bankCode isEqualToString:stringDebitCardBankCode]
             ||  [bankCode isEqualToString:VISADEBIT]
             ||  [bankCode isEqualToString:MASTERDEBIT]
             ||  [bankCode isEqualToString:JCBDEBIT]
             );
}

- (void)clearLocalUserCache {
    _cachedData = nil;
    _paymentSavedCard = nil;
    _paymentSavedBankAccount = nil;
    [ZPUserDefaults setObject:nil forKey:kDataManagerGetPlatforminfo];
}

- (void)clearPlatformInfoAndResource {
    NSString *dataPath = [self dataPath];
    NSString *zipPath = [self tempZipPath];
    NSString *attractPath = [self tempAttractPath];
    
    [FileUtils removeFolderAtPath:zipPath];
    [FileUtils removeFolderAtPath:dataPath];
    [FileUtils removeFolderAtPath:attractPath];
    
    [ZPUserDefaults setObject:nil forKey:kDataManagerGetPlatforminfo];
    [self clearPaymentCache];
    
}

RACSignal *loadImageFromString(NSString *str) {
    if (!str || [str length] == 0){
        return [[RACSignal return:nil] deliverOn:[RACScheduler mainThreadScheduler]];
    }
    return [[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        dispatch_async(dispatch_get_global_queue(0, DISPATCH_TIME_NOW), ^{
            NSData* imageData = [NSData zpBase64DecodeString:str];
            UIImage *image = imageData ? [UIImage imageWithData:imageData] : nil;
            [subscriber sendNext:image];
            [subscriber sendCompleted];
        });
        return nil;
    }] deliverOn:[RACScheduler mainThreadScheduler]];
}
@end
