//
//  ZPSDKStringConstants.h
//  ZaloPayWalletSDK
//
//  Created by thi la on 4/11/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
extern NSString *const ZaloPayAppStoreUrl;

extern NSString *const stringKeyBankCode;
extern NSString *const stringKeyBankCode2;
extern NSString *const stringKeyIconInfo;
extern NSString *const stringKeyMaintain;
extern NSString *const stringKeyUnsupportAmount;
extern NSString *const stringKeyCardNotSupportWithdraw;
extern NSString *const stringKeyCardSavedIsEmpty;
extern NSString *const stringKeyStatus;
extern NSString *const stringKeyFeecalType;
extern NSString *const stringKeyFeeRate;
extern NSString *const stringKeyMinFee;
extern NSString *const stringKeyCCBankName;
extern NSString *const stringKeyATMBankName;
extern NSString *const stringKeyAllowWithdraw;
extern NSString *const stringKeyErrorCode;
extern NSString *const stringKeyMessage;
extern NSString *const stringKeyBalance;
extern NSString *const stringKeyTranType;
extern NSString *const stringKeyZPTranId;
extern NSString *const stringKeyPaymentComplete;
extern NSString *const stringKeyPaymentCancel;
extern NSString *const stringKeyTransactionId;
extern NSString *const stringKeyPath;
extern NSString *const stringKeyStep;
extern NSString *const stringKeyMinAppVersion;
extern NSString *const stringKeyMasterCardBinMinRange;
extern NSString *const stringKeyJCBBinMinRange;
extern NSString *const stringKeyVisa;
extern NSString *const stringKeyCode;
extern NSString *const stringKeyChannelUnsupport;
extern NSString *const stringKeyChannelIsMaintain;
extern NSString *const stringKeyChannelUnsupportAmount;
extern NSString *const stringKeyAnnounceSuccess;
extern NSString *const stringKeyAnnounceError;
extern NSString *const stringKeyBankNotSupportMapByCard;
extern NSString *const stringKeyBankNotSupportPayByCard;
extern NSString *const stringKeyBankAccountPayByCard;
extern NSString *const stringKeySupportType;

extern NSString *const stringNetworkError;
extern NSString *const stringInvalidAppId;
extern NSString *const stringInvalidAppUser;
extern NSString *const stringInvalidAmount;
extern NSString *const stringAlertTitleInfo;
extern NSString *const stringAlertButtonClose;
extern NSString *const stringFeeCal;
extern NSString *const stringCreditCardBankCode;
extern NSString *const stringDebitCardBankCode;
extern NSString *const stringMethodNameFormat;
extern NSString *const stringBankPrefix;
extern NSString *const stringBankAccountFormat;
extern NSString *const stringUpdateNewVersionFormat;
extern NSString *const stringErrorMinVersion;
extern NSString *const stringInputCardNumber;
extern NSString *const stringErrorOverAmountPerTrans;
extern NSString *const stringVietcomBankCode;
extern NSString *const stringWarningCancelPayment;
extern NSString *const stringAlertTitleYes;
extern NSString *const stringAlertTitleNo;
extern NSString *const stringErrorCardIsExist;
extern NSString *const stringResultNavTitle;
extern NSString *const stringAlertTitleCancel;
extern NSString *const stringAlertTitleAccept;
extern NSString *const stringResultSuccess;
extern NSString *const stringResultFailed;
extern NSString *const stringTitleMapcard;
extern NSString *const stringSupportTitle;
extern NSString *const stringTitleRemoveMapcard;
extern NSString *const stringTypeErrorMessageNetworkError;
extern NSString *const stringTypeErrorMessageUnidentified;
extern NSString *const stringTitleInputCaptcha;
extern NSString *const stringMethodBankAccountFormat;
extern NSString *const stringTitleUpdatelevel3;
extern NSString *const stringBankAccountPayByCardFormat;
extern NSString *const stringBankSupportMapcardFormat;
extern NSString *const stringRegisterBIDV;
extern NSString *const stringDefaultMaintainFormat;
extern NSString *const stringDefaultUnSupportFormat;
extern NSString *const stringAlertAlreadyMapBankAccount;
extern NSString *const stringAlertViewBankAcocuntFormat;
extern NSString *const stringAlertMaxCCCardFormat;
extern NSString *const stringAlertPreventCCCardWhenPay;
extern NSString *const stringMapcardInfoFormat;
extern NSString *const stringGettingPaymentStatus;
extern NSString *const stringTitleOriginalAmount;
extern NSString *const stringTitleDiscountAmount;
extern NSString *const stringMapcardSuccess;
extern NSString *const stringMessageCCCardUnSupport;
extern NSString *const stringMessageBankSupportMapAccount;
extern NSString *const stringMessageMinAmount;
extern NSString *const stringMessageMaxAmount;
extern NSString *const stringMessageVCBMinimumBanlance;

#pragma mark -  Key Notification
extern NSString *const ZPUIKeyboardDidHideNotification;
extern NSString *const ZPUIKeyboardDidShowNotification;
extern NSString *const ZPUIButtonPreNextDidHideNotification;

extern NSString *const MASTER;
extern NSString *const JCB;
extern NSString *const VISA;
extern NSString *const MASTERDEBIT;
extern NSString *const JCBDEBIT;
extern NSString *const VISADEBIT;
extern NSString *const BIDV;

