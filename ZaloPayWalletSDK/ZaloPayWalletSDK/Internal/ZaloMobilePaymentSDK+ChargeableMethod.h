//
//  ZaloMobilePaymentSDK+ChargeableMethod.h
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 4/18/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZaloMobilePaymentSDK.h"

@class ZPBill;
@class ZPChannel;
@class ZPPaymentMethod;
@class ZPMiniBank;
@class ZPSavedBankAccount;
@class ZPBank;
@interface ZaloMobilePaymentSDK(ChargeableMethod)
- (NSArray *)chargebleMethods:(ZPBill *)bill methodType:(ZPPaymentMethodType)methodType;
- (NSString *)getSaveCardTitleFromConfig:(NSString *)bankName andLast4CardNo: (NSString *)last4CardNo;
@end

typedef void(^ZPPaymentMethodConfigUsingSavedCard)(ZPSavedCard *card, ZPPaymentMethod *nMethod);
typedef void(^ZPPaymentMethodConfigUsingAccount)(ZPSavedBankAccount *card, ZPPaymentMethod *nMethod);
typedef void(^ZPPaymentMethodConfig)(ZPPaymentMethod *m);

@interface ZaloMobileProcessPaymentModel : NSObject
@property (nonatomic, strong) ZPBill *bill;
@property (assign, nonatomic) long balance;
@property (nonatomic) ZPPaymentMethodType paymentOption;
@property (strong, nonatomic) ZPChannel *channel;
@property (readonly, nonatomic) BOOL isDraw;

- (instancetype)initWithBill:(ZPBill *)bill
                     balance:(long)balance
               paymentOption:(ZPPaymentMethodType)option
                     channel:(ZPChannel *)channel;

- (void)updateInfoForPaymentMethod:(ZPPaymentMethod *)method
                              icon:(NSString *)methodIcon
                        methodType:(ZPPaymentMethodType)type
                          miniBank:(ZPMiniBank *)bank
                 usingCustomStatus:(int)nStatus;
- (void)updateForPaymentMethod:(ZPPaymentMethod *)m usingBankInfo:(ZPBank *)bank;

- (ZPMiniBank *)miniBankInfo:(ZPBank *)bank with:(ZPPaymentMethodType)type;

- (BOOL)validIbanking;
- (BOOL)validPaymentWallet;
- (BOOL)validATM;
- (BOOL)validCreditCard;

@end
