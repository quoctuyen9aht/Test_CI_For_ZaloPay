//
//  ZPResourceManager.m
//  ZPSDK
//
//  Created by ZaloPayTeam on 12/22/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import "ZPResourceManager.h"
#import "ZPDataManager.h"
#import "UIColor+ZPExtension.h"
#import "ZaloPayWalletSDKLog.h"
#import <ZaloPayCommon/NSCollection+JSON.h>
#import "ZaloPayWalletSDKPayment.h"
@implementation ZPResourceManager

+ (instancetype)sharedInstance {
    static ZPResourceManager * _sharedInstance = nil;
    static dispatch_once_t predicate;
    
    dispatch_once(&predicate, ^{
        _sharedInstance = [[ZPResourceManager alloc] init];
    });
    
    return _sharedInstance;
}

+ (UIImage *)getImageWithName:(NSString *)name {
    if ([name hasSuffix:@".png"]){
        return [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",
                                                 [ZPDataManager sharedInstance].bundlePath, name]];
    }else{
        return [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@.png",
                                                 [ZPDataManager sharedInstance].bundlePath, name]];
    }
    
}

+ (CGFloat)getContainterWidth {
    return [UIScreen mainScreen].bounds.size.width;
}

+ (CGFloat)getTextFieldHeight {
    return 60 ;//* [ZPDataManager sharedInstance].heightRatio;
}

+ (CGFloat)buttonOkHeight {
    return 40 * [ZPDataManager sharedInstance].heightRatio;
}

+ (CGFloat)otpImageViewHeight {
    return 50 * [ZPDataManager sharedInstance].heightRatio;
}
+ (CGFloat)otpTextFieldHeight {
    return 45 * [ZPDataManager sharedInstance].heightRatio;
}

+ (UIColor *)textFieldHighlightBgColor {
    return [UIColor colorWithRed:255/255.0f green:254/255.0f blue:190/255.0f alpha:1.0f];
}

+ (UIColor *)textFieldDisabledBgColor {
    return [UIColor colorWithWhite:0.88 alpha:1];
}

+ (NSString *)formatDecimalStyle:(id)object {
    NSString * str = (NSString *)object;
    
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important!
    
    NSString *formatted = [formatter stringFromNumber:[NSNumber numberWithInt:str.intValue]];
    
    return formatted;
}

+ (NSString*)getCcBankCodeFromConfig:(NSString*)ccBankNumber{
    NSString * detectedBankCode = @"";
    NSDictionary * ccBankbins = [ZPDataManager sharedInstance].bankManager.ccBankbins;
    if (ccBankbins == nil) {
        return detectedBankCode;
    }
    
    if (ccBankNumber.length >= 4 && detectedBankCode.length == 0){
        NSString * first4Chars = [ccBankNumber substringToIndex:4];
        detectedBankCode = [ccBankbins objectForKey:first4Chars];
    }
    
    if (ccBankNumber.length >= 2 && detectedBankCode.length == 0){
        NSString * first2Chars = [ccBankNumber substringToIndex:2];
        detectedBankCode = [ccBankbins objectForKey:first2Chars];
    }
    
    if (ccBankNumber.length >= 1 && detectedBankCode.length == 0){
        NSString * first1Chars = [ccBankNumber substringToIndex:1];
        detectedBankCode = [ccBankbins objectForKey:first1Chars];
    }
    
    if ([[ZaloPayWalletSDKPayment sharedInstance].appDependence IsCCDebit:ccBankNumber]) {
        detectedBankCode = [NSString stringWithFormat:@"%@DEBIT", detectedBankCode];
    }
    
    return detectedBankCode;
}

+ (NSString*)getAtmBankCodeFromConfig:(NSString*)atmBankNumber {
    NSString * detectedBankCode = @"";
    if (atmBankNumber.length >= 6 && detectedBankCode.length == 0){
        NSString * first6Chars = [atmBankNumber substringToIndex:6];
        DDLogInfo(@"bankbins %@",[ZPDataManager sharedInstance].bankManager.bankbins);
        detectedBankCode = [[ZPDataManager sharedInstance].bankManager.bankbins objectForKey:first6Chars];
    }
    return detectedBankCode;
}

+ (UIImage *)nextIcon {
    return [ZPResourceManager getImageWithName:@"i_back.png"];
}
+ (UIImage *)backIcon {
    return [ZPResourceManager getImageWithName:@"i_next.png"];
}

+ (UIImage *)nextIconDisabled {
    return [ZPResourceManager getImageWithName:@"i_back_disable.png"];
}
+ (UIImage *)backIconDisabled {
    return [ZPResourceManager getImageWithName:@"i_next_disable.png"];
}

+ (UIImage *)atmBackButtonIcon {
    return [ZPResourceManager getImageWithName:@"i_edit.png"];
}

+ (UIFont *) boldFont {
    return IS_IPAD ? [UIFont boldSystemFontOfSize:[UIFont systemFontSize]*1.5f]: [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
}

+ (ZPZaloPayCreditError) convertToFormalErrorCodeSuccessOrFail:(ZPServerErrorCode) serErrorCode{
    if (serErrorCode >= 1 && serErrorCode != ZPServerErrorCodeFailInit && serErrorCode != ZPServerErrorCodeFailDuplicate) {
        return kZPZaloPayCreditErrorCodeNoneError;
    }
    return kZPZaloPayCreditErrorCodeFail;
}

+ (ZPZaloPayCreditError) convertToFormalErrorCode:(ZPServerErrorCode) serErrorCode{
    switch (serErrorCode) {
        case ZPServerErrorCodeNoneError:
            return kZPZaloPayCreditErrorCodeNoneError;
        case ZPServerErrorCodeFail:
        case ZPServerErrorCodeFailInit:
        case ZPServerErrorCodeFailDuplicate:
            return kZPZaloPayCreditErrorCodeFail;
            
            // for ATM
        case ZPServerErrorCodeAtmPurchaseSuccess:
            return kZPZaloPayCreditErrorCodeNoneError;
        case ZPServerErrorCodeAtmRetryOtp:
            return kZPZaloPayCreditErrorCodeAtmIncorrectOTP;
        case ZPServerErrorCodeAtmRetryCaptcha:
            return kZPZaloPayCreditErrorCodeCaptchaInvalid;
            
        case ZPServerErrorCodeFailTimeout:
            return kZPZaloPayCreditErrorCodeSDKTimeout;
        case ZPServerErrorCodeRetryPIN:
            return kZPZaloPayCreditErrorCodePinRetry;
        case ZPServerErrorCodeMaintainance:
            return kZPZaloPayCreditErrorCodeServerMaintenance;
        case ZPServerErrorCodeTransIdNotExist:
            return ZPZPZaloPayCreditErrorCodeTransIdNotExist;
        case ZPServerErrorCodeBlockUser:
            return ZPZPZaloPayCreditErrorCodeBlockUser;
        case ZPServerErrorCodeLoginFail:
            return ZPZaloPayCreditErrorLoginFail;
        case ZPServerErrorCodeLoginExpired:
            return ZPZaloPayCreditErrorLoginExpired;
        case ZPServerErrorCodeTokenInvalid:
            return ZPZaloPayCreditErrorTokenInvalid;
        case ZPServerErrorCodeTokenNotFound:
            return ZPZaloPayCreditErrorTokenNotFound;
        case ZPServerErrorCodeTokenExpired:
            return ZPZaloPayCreditErrorTokenExpired;
        case ZPServerErrorCodeAccountSuspended:
            return ZPZaloPayCreditErrorAccountSuspended;
        case ZPServerErrorCodeMaxBalancePerDay:
            return ZPZaloPayCreditErrorMaxBalancePerDay;
            
    }
    return kZPZaloPayCreditErrorCodeFail;
}

#pragma mark - Save Context
+ (void)saveObject:(id)object forKey:(NSString *)key {
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:object forKey:key];
    [userDefaults synchronize];
}

+ (id)objectForKey:(NSString *)key {
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults objectForKey:key];
}
//
//+ (ZPScreenOrientation) screenOrientation {
//    UIInterfaceOrientation orientation =
//    [[UIApplication sharedApplication] statusBarOrientation];
//
//    switch (orientation)
//    {
//        case UIInterfaceOrientationPortrait:
//        case UIInterfaceOrientationPortraitUpsideDown:
//        {
//            return ZPScreenOrientationPortrait;
//        }
//
//            break;
//        case UIInterfaceOrientationLandscapeLeft:
//        case UIInterfaceOrientationLandscapeRight:
//        {
//            return ZPScreenOrientationLandscape;
//        }
//            break;
//        default:
//            return ZPScreenOrientationPortrait;
//    }
//
//
//}

- (NSString *) messageForKey: (NSString *) key stringConfig:(NSDictionary *)stringConfig {
    NSString * message = [stringConfig stringForKey:key];
    return [message stringByReplacingOccurrencesOfString:@"|n|" withString:@"\n"];
}

#pragma mark - Url Strings
- (NSString *)stringByEnum:(ZPStringEnum)eString {
    NSDictionary * strDict = [[ZPDataManager sharedInstance] stringConfig];
    NSString * string = @"";
    switch (eString) {
        // message
        case ZPStringTypeAnnounceMessageExchangeFail:
            string =  [strDict objectForKey:@"message_announce_fail"];
            break;
        case ZPStringTypeAnnounceMessageExchangeSuccess:
            string =  [strDict objectForKey:@"message_announce_success"];
            break;
        case ZPStringTypeAnnounceMessageExchangeUnidentified:
            string =  stringTypeErrorMessageUnidentified;
            break;
        case ZPStringTypeAnnounceMessageExchangeMapcardUnidentified:
            string =  [strDict objectForKey:@"message_announce_mapcard_unidentified"];
            break;
        case ZPStringTypeAnnounceMessageIsInProcessing:
            string = [strDict objectForKey:@"message_announce_in_processing"];
            break;
        case ZPStringTypeAnnounceMessageMapcardIsInProcessing:
            string = [strDict objectForKey:@"message_announce_mapcard_processing"];
            break;
        case ZPStringTypeAnnounceMessageNoInternetConnection:
            string =  stringTypeErrorMessageNetworkError;
            break;
        case ZPStringTypeErrorMessageNetworkError:
            string =  stringTypeErrorMessageNetworkError;
            break;
        case ZPStringTypeErrorMessageMapcardNetworkError:
            string =  [strDict objectForKey:@"message_announce_mapcard_no_internet"];
            break;
        case ZPStringTypeErrorMessageUnsuportAmountError:
            string =  [strDict objectForKey:@"message_error_amount_unsuported"];
            break;
        case ZPStringTypePrivateKeyForCookieEncrypt:
            string = kStringTypePrivateKeyForCookiesEncrypt;
            break;
        default:
            break;
    }
    string = [self normalizeString:string];
    return string;
}

- (NSString *) normalizeString:(NSString *) str{
    return [str stringByReplacingOccurrencesOfString:@"%n%" withString:@"\n"];
}

- (NSString *)reuseIdCellWithEnum:(ZPCellType) type{
    switch (type) {
        case ZPCellTypeCaptchaOnlyCell: {
            return @"atm_captcha_only_cell";
        }
        default:
            break;
    }
    
    return @"";
}

+ (ZPAtmInputType) classifyAtmInputTypeWithOption:(int) opt andBankType:(NSString *) bankType {
    //DDLogInfo(@"opt: %d", opt);
    if (opt == 0) {
        return ZPAtmInputTypeApiBaseInfo;
    }
    if (opt == 1) {
        return ZPAtmInputTypeApiPasswordInfo;
    }
    if (opt == 2) {
        return ZPAtmInputTypeApiStartDateInfo;
    }
    if (opt == 4) {
        return ZPAtmInputTypeApiEndDateInfo;
    }
    return ZPAtmInputTypeApiBaseInfo;
}

+ (CGFloat)getLeftContainerWidth {
    return 150;
}

+ (ZPBankCode)convertBankCodeToEnum:(NSString *)tpeBankCode
{
    NSDictionary *bankMappingEnum = @{@"VCB": @(ZPBankCodeVCB),
                                      @"VTB": @(ZPBankCodeVTB),
                                      @"BIDV": @(ZPBankCodeBIDV),
                                      @"SCB": @(ZPBankCodeSCB),
                                      @"EIB": @(ZPBankCodeEIB),
                                      @"SGCB": @(ZPBankCodeSGCB),
                                      @"CC": @(ZPBankCodeCC),
                                      @"CCDB": @(ZPBankCodeCCDebit),
                                      @"VCCB": @(ZPBankCodeVCCB)
                                      };
    
    
    return [bankMappingEnum intForKey:tpeBankCode defaultValue:ZPBankCodeZP];
}

+ (NSString*)convertTPEBankCodeToBankcode:(NSString*)tpeBankCode {
    
    NSDictionary *bankMapping = @{@"VCB": @"ZPVCB",
                                  @"VTB": @"123PVTB",
                                  @"BIDV": @"123PBIDV",
                                  @"SCB": @"123PSCB",
                                  @"EIB": @"123PEIB",
                                  @"SGCB": @"123PSGCB",
                                  @"CC": @"123PCC"
                                  };
    
    return [bankMapping stringForKey:tpeBankCode defaultValue:tpeBankCode];
}

+ (ZPCCBankCode)convertCCBankCodeToEnum:(NSString *)ccBankCode
{
    NSDictionary *ccBankMappingEnum = @{@"123PVISA": @(ZPCCBankCodeVISA),
                                        @"123PMASTER": @(ZPCCBankCodeMASTER),
                                        @"123PJCB": @(ZPCCBankCodeJCB),
                                        @"123PVISADEBIT": @(ZPCCBankCodeVISADebit),
                                        @"123PMASTERDEBIT": @(ZPCCBankCodeMASTERDebit),
                                        @"123PJCBDEBIT": @(ZPCCBankCodeJCBDebit)
                                      };
    return [ccBankMappingEnum intForKey:ccBankCode defaultValue:ZPCCBankCodeVISA];
    
}


+ (NSString *)bankImageFromBankCode:(NSString *)bankCode
{
    return [[self convertTPEBankCodeToBankcode: bankCode] lowercaseString];
}
+ (NSURL *) urlFromImageName:(NSString *)imageName {
    NSString *stringPath;
    if ([imageName hasSuffix:@".png"]){
        stringPath = [NSString stringWithFormat:@"%@/%@",
                      [ZPDataManager sharedInstance].bundlePath, imageName];
    }else{
        stringPath = [NSString stringWithFormat:@"%@/%@.png",
                      [ZPDataManager sharedInstance].bundlePath, imageName];
    }
    NSURL *url = [NSURL fileURLWithPath:stringPath];
    return url;
}

+ (int)otpKeyboardType:(NSString *)bankCode {
    NSDictionary *config = [ZPDataManager sharedInstance].bankManager.banksDictOnBundle;
    if(config) {
        NSDictionary *bankDict = [config objectForKey:[ZPResourceManager convertTPEBankCodeToBankcode: bankCode]];
        if (bankDict) {
            return [bankDict intForKey:@"bank_otp_keyboard_type" defaultValue:0];
        }
    }
    return 0;
}

+ (NSString *)bankIconFromBankCode:(NSString *)bankCode
{
    return [NSString stringWithFormat:@"icon_%@",[[self convertTPEBankCodeToBankcode: bankCode] lowercaseString]];
    
}

+ (NSString *)bankValueFromBankCodeBundle:(NSString*)key andBankCode:(NSString *)tpeBankCode andIsBankOrCC:(BOOL)isBankOrCC{
    
    NSDictionary *dicBanks;
    
    if(isBankOrCC){
        dicBanks = [ZPDataManager sharedInstance].bankManager.banksDictOnBundle;
    }
    else {
        dicBanks = [ZPDataManager sharedInstance].bankManager.ccBanksDict;
    }
    
    
    NSString *bankCode = [ZPResourceManager convertTPEBankCodeToBankcode: tpeBankCode];

    if(dicBanks == nil) return @"";
    else {
        
        if([dicBanks objectForKey:bankCode] == nil){
            return @"";
        }
        else {
            NSDictionary *bankCodeGet = [dicBanks objectForKey:bankCode];
            if([bankCodeGet objectForKey:key] == nil){
                return @"";
            }
            else {
                return [bankCodeGet objectForKey:key];
            }
        }
    }
    return @"";
}
+ (NSArray *)bankColorGradientFromBankCodeBundle:(NSString *)tpeBankCode andIsBankOrCC:(BOOL)isBankOrCC{
    
    NSDictionary *dicBanks;
    
    if(isBankOrCC){
        dicBanks = [ZPDataManager sharedInstance].bankManager.banksDictOnBundle;
    }
    else {
        dicBanks = [ZPDataManager sharedInstance].bankManager.ccBanksDict;
    }
    
    NSString *bankCode = [ZPResourceManager convertTPEBankCodeToBankcode: tpeBankCode];

    if(dicBanks == nil) return @[[UIColor clearColor],[UIColor clearColor]];
    else {
        
        if([dicBanks objectForKey:bankCode] == nil){
            return @[[UIColor clearColor],[UIColor clearColor]];
        }
        else {
            NSDictionary *bankCodeGet = [dicBanks objectForKey:bankCode];
            if([bankCodeGet objectForKey:@"bank_color_start"] == nil){
                return @[[UIColor clearColor],[UIColor clearColor]];
            }
            else {
                @try {
                    NSString *strStartColor = [bankCodeGet objectForKey:@"bank_color_start"];
                    NSString *strEndColor = [bankCodeGet objectForKey:@"bank_color_end"];
                    
                    return  @[[UIColor zpColorFromHexString:strStartColor],[UIColor zpColorFromHexString:strEndColor]];
                } @catch (NSException *exception) {
                    
                } @finally {
                    
                }
                
            }
        }
    }
    
    return @[[UIColor clearColor],[UIColor clearColor]];
}

+(NSString*)covertEnumStepToString:(NSInteger)intEnum{
    NSInteger result = intEnum % 10;
    return [NSString stringWithFormat:@"%ld",(long)result];
}



@end
