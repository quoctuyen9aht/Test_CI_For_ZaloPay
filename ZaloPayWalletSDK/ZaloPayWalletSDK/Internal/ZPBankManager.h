//
//  ZPBankManager.h
//  ZaloPayWalletSDK
//
//  Created by thi la on 12/6/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZPDefine.h"

@class ZPBank;
@class ZPMiniBank;
@class RACSignal<Type>;

@interface ZPBankManager : NSObject
@property (nonatomic, strong, readonly) NSDictionary * bankbins;
@property (strong, nonatomic, readonly) NSArray<ZPBank *> *allBanks;
@property (nonatomic, strong) NSDictionary * ccBanksDict;
@property (nonatomic, strong) NSDictionary * ccBankbins;
@property (nonatomic, strong) NSDictionary * ccBanksEnabled;
@property (nonatomic, strong) NSDictionary * banksDictOnBundle;

- (void)getBanklistWithCallBack:(void (^)(NSDictionary *dictionary))callback;
- (RACSignal <NSDictionary *>*)getBankGatetway:(NSInteger)appId;
/**
 Using this method to get banks display order on maping card flow

 @return static collection of key-value pairs, key : bankcode , value : displayorder
 */
- (NSDictionary *)bankDisplayOrder;
- (void)clearCache;
- (ZPMiniBank*)getBankFunctionIsSupportTranType:(ZPBank *)bank withTranType:(ZPTransType)trantype;
- (ZPMiniBank*)getBankFunctionIsSupportMethod:(ZPBank *)bank withPaymentMethod:(ZPPaymentMethodType)method;
- (NSArray <ZPMiniBank *> *)getMiniBanksForMapping;
- (NSArray *)listCCBankSupport;
- (NSArray*)getArrayBankInPinViewWithTranstype:(ZPTransType)transType;
- (BOOL)isMasterCardEnable;
- (BOOL)isVisaCardEnable;
- (BOOL)isJCBCardEnable;
- (ZPBank*)getBankInfoWith:(NSString*)bankCode;

/**
 Using this method to get display order of cc card on flow maping card
 
 @param key : type of cc card ( visa, masterCard, jcb)
 @return displayorder: NSnumber value
 */
- (NSNumber*)ccDisplayOrder:(NSString*)key;
/**
 Using this method to get banks display order on payment flow, display saved card, saved bank account by channel display order
 
 @return static collection of key-value pairs, key : bankcode , value : displayorder
 */
- (NSDictionary *)bankDisplayOrderForPayment;
- (BOOL)canPaymentGatway:(NSInteger)appid;
@end



