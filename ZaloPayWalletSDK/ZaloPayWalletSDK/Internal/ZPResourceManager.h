//
//  ZPResourceManager.h
//  ZPSDK
//
//  Created by ZaloPayTeam on 12/22/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ZPDefine.h"
#import "ZPConfig.h"

@interface ZPResourceManager : NSObject

+ (instancetype)sharedInstance;

- (NSString *) messageForKey: (NSString *) key stringConfig:(NSDictionary *)stringConfig;
- (NSString *)stringByEnum:(ZPStringEnum)eString;
- (NSString *)reuseIdCellWithEnum:(ZPCellType)type;
+ (NSString *)formatDecimalStyle:(id)object;
+ (NSString*)getCcBankCodeFromConfig:(NSString*)ccBankNumber;
+ (NSString*)getAtmBankCodeFromConfig:(NSString*)atmBankNumber;
+ (UIImage *)getImageWithName:(NSString *)name;
+ (UIImage *)nextIcon;
+ (UIImage *)nextIconDisabled;
+ (UIImage *)backIcon;
+ (UIImage *)backIconDisabled;
+ (UIImage *)atmBackButtonIcon;
+ (UIFont *)boldFont;
+ (CGFloat)getContainterWidth;
+ (CGFloat)getTextFieldHeight;
+ (CGFloat)buttonOkHeight;
+ (CGFloat)otpImageViewHeight;
+ (CGFloat)otpTextFieldHeight;
+ (CGFloat)getLeftContainerWidth;

+ (UIColor *)textFieldHighlightBgColor;
+ (UIColor *)textFieldDisabledBgColor;

+ (ZPAtmInputType)classifyAtmInputTypeWithOption:(int) opt andBankType:(NSString *) bankType;
+ (ZPBankCode)convertBankCodeToEnum:(NSString *)bankCode;
+ (ZPCCBankCode)convertCCBankCodeToEnum:(NSString *)ccBankCode;
+ (ZPZaloPayCreditError)convertToFormalErrorCode:(ZPServerErrorCode) serErrorCode;
+ (ZPZaloPayCreditError)convertToFormalErrorCodeSuccessOrFail:(ZPServerErrorCode) serErrorCode;

+ (NSString *)bankImageFromBankCode:(NSString *)bankCode;
+ (NSString *)bankIconFromBankCode:(NSString *)bankCode;
+ (void)saveObject:(id)object forKey:(NSString *)key;
+ (id)objectForKey:(NSString *)key;
+ (NSURL *)urlFromImageName:(NSString *)imageName;
+ (int)otpKeyboardType:(NSString*)bankCode;
//need
+ (NSString *)bankValueFromBankCodeBundle:(NSString*)key andBankCode:(NSString *)bankCode andIsBankOrCC:(BOOL)isBankOrCC;
+ (NSArray *)bankColorGradientFromBankCodeBundle:(NSString *)bankCode andIsBankOrCC:(BOOL)isBankOrCC;
+(NSString*)covertEnumStepToString:(NSInteger)intEnum;
@end
