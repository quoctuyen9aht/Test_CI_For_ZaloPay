//
//  ZPSmartLinkHelper.h
//  ZPSDK
//
//  Created by Nguyen Xuan Phung on 1/12/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZPConfig.h"


@interface ZPSmartLinkHelper : NSObject

@property (strong, nonatomic) UIWebView * webview;


#pragma mark - Smartlink
- (BOOL)isMessageErrorUrl:(NSString *)url;
- (BOOL)isSmartLinkOTP:(NSString *)url;
- (BOOL)isBanketOTP:(NSString *)url;
- (BOOL)isOtpCallback:(NSString *)url;
- (BOOL)isAtmCallback:(NSString *)url;
- (BOOL)isSmartLinkSkippable:(NSString *)url;
- (BOOL)isCaptchaCallback:(NSString *)url;
- (BOOL)isAtmError:(NSString *)url;
- (BOOL) isOnePayFailLoadURL:(NSString*) url;
- (BOOL) isOnePayURL:(NSString*) url;
- (void)sml_RequestCaptcha;
- (BOOL) isVicomDirect:(NSString *) url;

- (BOOL)isIbankingError:(NSString *)url;
#pragma mark - VCBank
- (BOOL)vcb_IsLoginUrl:(NSString *)url;
- (BOOL)vcb_IsOtpUrl:(NSString *)url;
- (BOOL)vcb_ErrorUrl:(NSString *)url;
- (BOOL)vcb_DirectErrorUrl:(NSString *) url;
- (void)vcb_DirectRequestCaptcha;
- (void)vcb_DirectLoginWithUsername:(NSString *)username
                           password:(NSString *)password
                            captcha:(NSString *)captcha;
- (NSString*)vcb_DirectGetErrorMessage;
- (BOOL)vcb_DirectIsCaptchaUrl:(NSString *)url;
- (void)vcb_DirectCheckStep;
- (BOOL)vcb_DirectIsOtpUrl:(NSString *)url;
- (void)vcb_DirectConfirm:(NSString*)value;
- (NSString *)vcb_DirectConfirmChooseAccount;
- (void)vcb_DicrectSubmitOtp:(NSString *)otp;

#pragma mark - VTBank
- (void)vtb_CheckStep;
- (void)vtb_RequestCaptcha;
- (void)vtb_SubmitCaptcha:(NSString *)captcha;
- (void)vtb_SubmitOtp:(NSString *)otp;
- (BOOL)vtb_IsCaptchaUrl:(NSString *)url;
- (BOOL)vtb_IsOtpUrl:(NSString *)url;
- (BOOL)vtb_IsUrl:(NSString *)url;

#pragma BIDV bank
- (BOOL)bidv_IsUrl:(NSString *)url;
- (void)bidv_CheckStep;
- (void)bidv_GetCaptcha;
- (void)bidv_SubmitCaptcha:(NSString *)captcha andPassWord:(NSString *)password;
- (void)bidv_SubmitOtp:(NSString *)otp;
- (BOOL)bidv_IsCaptchaUrl:(NSString *)url;
- (BOOL)bidv_IsOtpUrl:(NSString *)url;
- (NSString *)bidv_GetError;
- (BOOL)bidv_checkLoading;
- (void)bidv_ClosePopup;
- (void)bidv_Cancel;
-(void)bidv_Inject_Reloadcaptcha;
-(void)bidv_Inject_submitCaptchaSusscess;
#pragma mark - iBanking
- (NSString*)getJsFunc:(NSString*)funcName injectFile:(NSString*)fileName;

//VCB
- (void)iBanking_VCB_LoginWithUsername:(NSString *)username
                              password:(NSString *)password
                               captcha:(NSString *)captcha;
- (NSString*)iBanking_VCB_GetErrorMessage;
- (void)iBanking_VCB_GetCaptchar;
-(NSMutableArray *)iBanking_VCB_GetListAccountNumber;
-(NSMutableArray *)iBanking_VCB_GetListZaloPayID;
-(void)iBanking_VCB_LinkWalletSubmitInfo:(NSString *)accountNumber zaloPayId:(NSString *)zaloPayId captchar:(NSString *)capt;
-(void)iBanking_VCB_LinkWalletSubmitOTP:(NSString *)otp;
-(NSString * )iBanking_VCB_Loading;
-(NSString *)iBanking_VCB_Step;
-(void)iBanking_VCB_SelectZaloPay;
-(void)iBanking_VCB_CancelWalletWithPassWord:(NSString *)passWord;
-(NSString *)iBanking_VCB_UnlinkStep;
-(void)iBanking_VCB_RefreshCaptCha;
@end
