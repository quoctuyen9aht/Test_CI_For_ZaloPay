//
//  ZaloMobilePaymentSDK.m
//  ZPSDK
//
//  Created by phungnx on 12/22/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import "ZaloMobilePaymentSDK.h"
#import "ZPPaymentManager.h"
#import "ZPDataManager.h"
#import <UIKit/UIKit.h>
#import "ZPPaymentHandler.h"
#import "ZPPaymentInfo.h"
#import "ZPResponseObject.h"
#import "ZPDefine.h"
#import "ZPPaymentResponse.h"
#import "ZPBill.h"
#import "ZPProgressHUD.h"
#import "ZPPaymentChannel.h"
#import "ZPPaymentMethod.h"
#import "NSString+ZPExtension.h"
#import "NSData+ZPExtension.h"
#import "ZaloPayWalletSDKPayment.h"
#import "ZPCreditCardHandler.h"
#import "ZPContainerViewController.h"
#import "ZPResourceManager.h"
#import "ZPDataManager.h"
#import "ZaloPayWalletSDKPayment.h"
#import "ZPPaymentChannel.h"
#import "ZPResourceManager.h"
#import "ZPMiniBank.h"
#import "ZPSDKStringConstants.h"
#import "ZaloMobilePaymentSDK+ChargeableMethod.h"
#import "ZaloPayWalletSDKLog.h"
#import "ZPResultViewController.h"
#import "ZPResultHandler.h"
#import <ZaloPayWalletSDK/ZaloPayWalletSDK-Swift.h>


@interface ZaloMobilePaymentSDK() <ZPPaymentViewControllerDelegate>
@property (strong, nonatomic) ZPContainerViewController *containerController;
@property (strong, nonatomic) ZPBill *bill;
@end

@implementation ZaloMobilePaymentSDK

- (void)initPaymentSDK {
    [[ZPDataManager sharedInstance] getPaymentGatewayInfoCompleteHandle:^(int errorCode, NSString *mesage) {}];
    [[ZPDataManager sharedInstance].bankManager getBanklistWithCallBack:^(NSDictionary *dictionary){}];
}

- (RACSignal *)removeSavedCard:(ZPSavedCard *)card {
    return [[ZPDataManager sharedInstance] removeSavedCard:card];
}
- (RACSignal *)removeBankAccount:(NSString*)bankCustomerId bankAccount:(ZPSavedBankAccount*)bankAccount {
    return [[ZPDataManager sharedInstance] removeBankAccount:bankCustomerId bankAccount:bankAccount];
}
#pragma mark - public apis

- (void)linkCardWithBill:(ZPBill *)bill {
    DDLogInfo(@"linkCardInParentViewController zpbill: %@", bill);
    if ([self handlePayBillWithoutNetwork]) {
        return;
    }
    if ([self handleInvalidAppId:bill]) {
        return;
    }
    self.bill = bill;
    [[ZPDataManager sharedInstance] initDataWithCallback:^(int errorCode, NSString * message) {
         [self configCompleteWithCode:errorCode message:message methodType:ZPPaymentMethodTypeAtm];
     }];
}

- (void)processBankAccountWithBill:(ZPBill *)bill {
    DDLogInfo(@"linkCardInParentViewController zpbill: %@", bill);
    if ([self handlePayBillWithoutNetwork]) {
        return;
    }
    if ([self handleInvalidAppId:bill]) {
        return;
    }
    self.bill = bill;
    [[ZPDataManager sharedInstance] initDataWithCallback: ^(int errorCode, NSString * message) {
        if([self handleConfigResource]) {
            return;
        }
        if ([self handleInitConfigError:errorCode message:message]) {
            return;
        }
        [self showContainerView:nil];
     }];
}

- (void)payWithBill:(ZPBill *)bill {

    if ([self handleInvalidBillAmount:bill]) {
        return;
    }
    if ([self handlePayBillWithoutNetwork]) {
        return;
    }
    if ([self handleInvalidAppId:bill]) {
        return;
    }    
    self.bill = bill;
    
    [[ZPDataManager sharedInstance] initDataWithCallback:^(int errorCode, NSString * message) {
        [self configCompleteWithCode:errorCode message:message methodType:ZPPaymentMethodTypeMethods];
     }];
}

#pragma mark - Private Methods

- (BOOL)handleInvalidBillAmount:(ZPBill *)bill {
    if (bill.amount <= 0) {
        ZPPaymentResponse* pResponse = [[ZPPaymentResponse alloc] init];
        pResponse.errorCode = kZPZaloPayCreditErrorCodeInvalidParam;
        pResponse.exchangeStatus = kZPZaloPayCreditStatusCodeFail;
        pResponse.message = stringInvalidAmount;
        [self handleReturnResponseWithAlert:pResponse];
        return true;
    }
    return false;
}

- (BOOL)handlePayBillWithoutNetwork {
    BOOL shouldDownloadResource = [[ZPDataManager sharedInstance] shouldDownloadResource];
    if ([ZaloPayWalletSDKPayment sharedInstance].network.isReachable == false && shouldDownloadResource) {
        ZPPaymentResponse* pResponse = [[ZPPaymentResponse alloc] init];
        pResponse.errorCode = kZPZaloPayCreditErrorCodeInvalidParam;
        pResponse.exchangeStatus = kZPZaloPayCreditStatusCodeFail;
        pResponse.message = stringNetworkError;
        [self handleReturnResponseWithAlert:pResponse];
        return true;
    }
    return false;
}

- (BOOL)handleInitConfigError:(int)errorCode message:(NSString *)message {
    if (errorCode < 0) {
        ZPPaymentResponse* pResponse = [[ZPPaymentResponse alloc] init];
        pResponse.errorCode = errorCode;
        pResponse.exchangeStatus = kZPZaloPayCreditStatusCodeFail;
        pResponse.message = message;
        [self handleReturnResponseWithAlert:pResponse];
        return true;
    }
    return false;
}

- (BOOL)handleConfigResource {
    BOOL shouldDownloadResource = [[ZPDataManager sharedInstance] shouldDownloadResource];
    BOOL hasResources = [[ZPDataManager sharedInstance] viewsConfig] ? true : false;
    if (shouldDownloadResource || hasResources == false) {
        ZPPaymentResponse* response = [[ZPPaymentResponse alloc] init];
        response.errorCode = kZPZaloPayCreditErrorCodeGettingResourceFailed;
        response.errorStep = ZPErrorStepNonRetry;
        response.exchangeStatus = kZPZaloPayCreditStatusCodeFail;
        response.message = stringNetworkError;
        [self handleReturnResponseWithAlert:response];
        return true;
    }
    return false;
}

- (BOOL)handleMaintain:(NSArray *)paymentMethods {
    ZPDataManager * dataMgr = [ZPDataManager sharedInstance];
    DDLogInfo(@"paymentMethods count: %lu", (unsigned long)paymentMethods.count);
    if(paymentMethods.count == 0) {
        //server maintenance
        ZPPaymentResponse* response = [[ZPPaymentResponse alloc] init];
        response.errorCode = kZPZaloPayCreditErrorCodeServerMaintenance;
        response.message = [dataMgr messageForKey:stringKeyUnsupportAmount];
        if (self.bill.transType == ZPTransTypeWithDraw) {
            if([[ZPDataManager sharedInstance].paymentSavedCard count] > 0) {
                response.message = [dataMgr messageForKey:stringKeyCardNotSupportWithdraw];
            } else {
                response.message = [dataMgr messageForKey:stringKeyCardSavedIsEmpty];
            }
        }
        response.errorStep = ZPErrorStepNonRetry;
        response.exchangeStatus = kZPZaloPayCreditStatusCodeFail;
        [self handleReturnResponseWithAlert:response];
        return true;
    }
    return false;
}

- (BOOL)handleInvalidAppId:(ZPBill *)bill {
    if (bill.appId <= 0) {
        ZPPaymentResponse* pResponse = [[ZPPaymentResponse alloc] init];
        pResponse.errorCode = kZPZaloPayCreditErrorCodeInvalidParam;
        pResponse.exchangeStatus = kZPZaloPayCreditStatusCodeFail;
        pResponse.message = [R string_PayBillSetAppId_ErrorMessage];
        [self handleReturnResponseWithAlert:pResponse];
        return true;
    }
    return false;
}

- (void)showContainerView:(NSArray *)paymentMethods {
    self.containerController = [[ZPContainerViewController alloc] init];
    self.containerController.delegate = self;
    self.containerController.dataManager =  [ZPDataManager sharedInstance];
    self.containerController.paymentMethods = [paymentMethods mutableCopy];
    self.containerController.bill = self.bill;
    [[ZPAppFactory sharedInstance].orderTracking trackSDKInit:self.bill.appTransID result:OrderStepResult_Success];
    
    if (!self.navigationController) {
        return;
    }
    NSMutableArray *controllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    BOOL animation = FALSE;
    if (![controllers containsObject:self.parentController]) {
        // trường hợp user vào nạp tiền + nhập thẻ VCB rồi chọn liên kết tài khoản.
        [controllers addObject:self.parentController];
        animation = TRUE;
    }
    [controllers addObject:self.containerController];
    [self.navigationController setViewControllers:controllers animated:animation];
}

- (void)showAlertWithMessage:(NSString *)errorMessage {
    DDLogInfo(@"SDK : showAlertWithMessage");
    [ZPDialogView showDialogWithType:DialogTypeNotification
                             message:errorMessage
                   cancelButtonTitle:[R string_ButtonLabel_Close]
                    otherButtonTitle:nil
                      completeHandle:nil];
}

- (void)configCompleteWithCode:(int)code message:(NSString *)message methodType:(ZPPaymentMethodType)method {
    
    if([self handleConfigResource]) {
        return;
    }
    
    if ([self handleInitConfigError:code message:message]) {
        return;
    }
    NSArray *paymentMethods = [self chargebleMethods:self.bill methodType:method];
    [self showContainerView:paymentMethods];
}

- (void)handleReturnResponseWithAlert:(ZPPaymentResponse*) response{
    NSString * announceMessage = response.message;
    if (announceMessage == NULL) {
        announceMessage = [[ZPDataManager sharedInstance] stringByEnum:ZPStringTypeAnnounceMessageExchangeFail];
    }
    //[ZPProgressHUD showWarningWithStatus:announceMessage];
    
    // Hide show popup in specific case
    if(response.errorCode != ZPServerErrorCodeBlockUser &&
       response.errorCode != ZPServerErrorCodeLoginFail &&
       response.errorCode != ZPServerErrorCodeLoginExpired &&
       response.errorCode != ZPServerErrorCodeTokenInvalid &&
       response.errorCode != ZPServerErrorCodeTokenNotFound &&
       response.errorCode != ZPServerErrorCodeTokenExpired &&
       response.errorCode != ZPServerErrorCodeAccountSuspended) {
        
        [self showAlertWithMessage:announceMessage];
    }
    NSDictionary *returnDict = @{stringKeyErrorCode:@(response.originalCode),
                                 stringKeyMessage : [NSString zpEmptyIfNilString:response.message]};
    [[ZPAppFactory sharedInstance].orderTracking trackSDKInit:self.bill.appTransID result:OrderStepResult_Fail];
    [self closePaymentAndCleanUp];
    [[ZaloPayWalletSDKPayment sharedInstance].paymentDelegate paymentReponseFail:returnDict];    
}

- (void)handleReturnResponse:(ZPPaymentResponse*) response{
    DDLogInfo(@"// final: handleReturnResponse with errorCode: %d", response.originalCode);
    dispatch_async(dispatch_get_main_queue(), ^{
        NSDictionary *returnDict = @{stringKeyBalance:[NSString zpEmptyIfNilString:[NSString stringWithFormat:@"%lld",response.balance]],
                                    stringKeyTranType: [NSString stringWithFormat:@"%ld",(long)self.bill.transType],
                                    stringKeyErrorCode : @(response.originalCode),
                                    stringKeyMessage : [NSString zpEmptyIfNilString:response.message],
                                     stringKeyZPTranId:[NSString zpEmptyIfNilString:response.zpTransID]};
        DDLogInfo(@"notify data response%@",returnDict);
        [self closePaymentAndCleanUp];
        if (response.errorCode >= 1) {
            switch (response.exchangeStatus) {
                case kZPZaloPayCreditStatusCodeFail:
                    [[ZaloPayWalletSDKPayment sharedInstance].paymentDelegate paymentReponseFail:returnDict];
                    break;
                case kZPZaloPayCreditStatusCodeSuccess:
                    [[ZaloPayWalletSDKPayment sharedInstance].paymentDelegate paymentReponseSuccess:returnDict];
                    break;
                case kZPZaloPayCreditStatusCodeUnidentified:
                    [[ZaloPayWalletSDKPayment sharedInstance].paymentDelegate paymentReponseUnidentified];
                    break;
                default:
                    break;
            }
        }
        else{
            [[ZaloPayWalletSDKPayment sharedInstance].paymentDelegate paymentReponseFail: returnDict];
        }
    });
}

- (void)closePaymentAndCleanUp {
    DDLogInfo(@"closePaymentAndCleanUp");
    self.containerController = nil;
    self.parentController = nil;
    self.bill = nil;
    self.navigationController = nil;
}

#pragma mark - ZPPaymentViewControllerDelegate

- (void)paymentControllerDidFinishWithResponse:(ZPPaymentResponse *)response {
    [self handleReturnResponse:response];
}

- (void)paymentControllerDidFinish:(ZPPaymentResponse*)response{
    [self handleReturnResponse:response];
}

- (void)paymentControllerDidClose:(NSString*)key data:(NSString*)data {
    DDLogInfo(@"data %@",data);
    [[ZaloPayWalletSDKPayment sharedInstance].zpVoucherManager clearVouchersCaches];
    [[ZaloPayWalletSDKPayment sharedInstance].zpVoucherManager revertArrVoucherAndUseVoucher:nil];
    
    [[ZaloPayWalletSDKPayment sharedInstance].zpPromotionManager revertPromotionsAndUsePromotion:nil];
    
    [self closePaymentAndCleanUp];
    [[ZaloPayWalletSDKPayment sharedInstance].paymentDelegate paymentDidCloseWithKey:key andData:[data JSONValue]];
    
}

- (void)paymentControllerDidChooseFAQ {
    DDLogInfo(@"paymentControllerDidChooseFAQ ");
    if([[ZaloPayWalletSDKPayment sharedInstance].paymentDelegate respondsToSelector:@selector(paymentNotifyShowFAQ)]) {
        [[ZaloPayWalletSDKPayment sharedInstance].paymentDelegate paymentNotifyShowFAQ];
    }
}

- (void)paymentControllerDidChooseSupportWithData:(NSDictionary *)data {
    DDLogInfo(@"paymentControllerDidChooseSupportWithData_____ %@", data);
    if([[ZaloPayWalletSDKPayment sharedInstance].paymentDelegate respondsToSelector:@selector(paymentNotifyShowSupportPage:)]) {
        [[ZaloPayWalletSDKPayment sharedInstance].paymentDelegate paymentNotifyShowSupportPage:data];
    }
}

- (void)paymentControllerNotifyResultWithResponse:(ZPPaymentResponse *)response {
    [self notifyCloseWithResponse:response];
}

- (void)paymentControllerNotifyCloseWithKey:(NSString *)key data:(NSString *)data {
    DDLogInfo(@"notify data %@",data);
    [[ZaloPayWalletSDKPayment sharedInstance].paymentDelegate paymentNotifyCloseWithKey:key andData:[data JSONValue]];
}

- (void)notifyCloseWithResponse:(ZPPaymentResponse *)response {
    DDLogInfo(@"// final: notify handleReturnResponse with errorCode: %d", response.errorCode);
    dispatch_async(dispatch_get_main_queue(), ^{
        NSDictionary *returnDict = @{stringKeyBalance:@(response.balance),
                                     stringKeyTranType: @(self.bill.transType),
                                     stringKeyErrorCode : @(response.originalCode),
                                     stringKeyMessage : [NSString zpEmptyIfNilString:response.message],
                                     stringKeyZPTranId:[NSString zpEmptyIfNilString:response.zpTransID]};
        DDLogInfo(@"notify data %@",returnDict);
        if (response.errorCode >= 1) {
            switch (response.exchangeStatus) {
                case kZPZaloPayCreditStatusCodeFail:
                    [[ZaloPayWalletSDKPayment sharedInstance].paymentDelegate paymentNotifyFail:returnDict];
                    break;
                case kZPZaloPayCreditStatusCodeSuccess:
                    [[ZaloPayWalletSDKPayment sharedInstance].paymentDelegate paymentNotifySuccess:returnDict];
                    break;
                case kZPZaloPayCreditStatusCodeUnidentified:
                    [[ZaloPayWalletSDKPayment sharedInstance].paymentDelegate paymentNotifyUnidentified];
                    break;
                default:
                    break;
            }
        }
        else{
            [[ZaloPayWalletSDKPayment sharedInstance].paymentDelegate paymentNotifyFail: returnDict];
        }
    });
}

- (void)clearPaymentCache {
    [[ZPDataManager sharedInstance] clearPaymentCache];
}

#pragma mark - UIApplicationDelegate

- (void)closePaymentAndCleanSDK {
    ZPResultViewController *result = (ZPResultViewController *) self.containerController.navigationController.topViewController;
    if ([result isKindOfClass:[ZPResultViewController class]]) {
        [result.resultHandler processPayment];
    }
}

- (BOOL)isPayingBill {
    UIViewController *currentVC = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    UINavigationController *presentedVC = [UINavigationController castFrom:[currentVC presentedViewController]];
    
    // Case use for popup
    if (presentedVC && [[presentedVC topViewController] isKindOfClass:[ZPPaymentPopupRootController class]]) {
        ZPPaymentPopupRootController *popup = [ZPPaymentPopupRootController castFrom:[presentedVC topViewController]];
        if (!popup.isMappingCard) {
            return YES;
        }
    }
    
    // Case normal
    if (self.containerController != nil || self.parentController != nil) {
        return YES;
    }
    
    // Case use for gateway
    UINavigationController *navi;
    if ([currentVC isKindOfClass:[UITabBarController class]]) {
        navi = [UINavigationController castFrom:[(UITabBarController *)currentVC selectedViewController]];
    } else {
        navi = [UINavigationController castFrom:currentVC];
    }
    
    if (navi) {
        if ([navi.viewControllers firstObjectUsing:^BOOL(__kindof UIViewController *vc) {
            return [vc isKindOfClass:[ZPPaymentGatewayViewController class]];
        }] != nil) {
            return YES;
        }
    }
    
    return NO;
}

- (BOOL)checkIsCurrentPaymentFinished {
    ZPResultViewController *result = (ZPResultViewController *) self.containerController.navigationController.topViewController;
    return [result isKindOfClass:[ZPResultViewController class]];
}

- (void)dismissSDKKeyboard {
    [self.containerController dismissSDKKeyboard];
}

- (void)showSDKKeyboard {
    [self.containerController showSDKKeyboard];
}
@end
