//
//  NetworkManager+RegisterBank.m
//  ZaloPay
//
//  Created by Dung Vu on 8/21/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import "NetworkManager+RegisterBank.h"
#import "UIDevice+ZPExtension.h"
#import "ZPConfig.h"
#import "ZPBill.h"
#import "ZPPaymentInfo.h"

@implementation NetworkManager (RegisterBank)
- (NSDictionary *)paramsWithBill:(ZPBill *)bill {
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    UIDevice * device = [UIDevice currentDevice];
    [params setObjectCheckNil:@(bill.appId) forKey:@"appid"];
    [params setObjectCheckNil:bill.appTransID forKey:@"apptransid"];
    [params setObjectCheckNil:bill.appUser forKey:@"appuser"];
    [params setObjectCheckNil:@(bill.time) forKey:@"apptime"];
    [params setObjectCheckNil:bill.embedData forKey:@"embeddata"];
    [params setObjectCheckNil:@"ios" forKey:@"platform"];
    [params setObjectCheckNil:bill.mac forKey:@"mac"];
    [params setObjectCheckNil:[device zpDeviceId:YES] forKey:@"deviceid"];
    [params setObjectCheckNil:[device zpMobileNetworkCode] forKey:@"mno"];
    [params setObjectCheckNil:SDK_BUILD_VERSION forKey:@"sdkver"];
    [params setObjectCheckNil:[device zpOsVersion] forKey:@"osver"];
    [params setObjectCheckNil:[device zpDeviceModel] forKey:@"devicemodel"];
    [params setObjectCheckNil:[NetworkState sharedInstance].connectionType forKey:@"conntype"];
    [params setObjectCheckNil:[bill.appUserInfo stringForKey:@"zaloid"] forKey:@"zaloid"];
    return params;
}

- (NSString *)atmCardInfo:(ZPAtmCard *)atmCard {
    NSMutableDictionary* bankDict = [[NSMutableDictionary alloc] init];
    [bankDict setObjectCheckNil:atmCard.bankCode forKey:@"bankcode"];
    [bankDict setObjectCheckNil:atmCard.cardHolderName forKey:@"cardholdername"];
    [bankDict setObjectCheckNil:atmCard.cardNumber forKey:@"cardnumber"];
    [bankDict setObjectCheckNil:atmCard.type forKey:@"type"];
    [bankDict setObjectCheckNil:atmCard.otpType forKey:@"otptype"];
    [bankDict setObjectCheckNil:atmCard.validFrom forKey:@"cardvalidfrom"];
    [bankDict setObjectCheckNil:atmCard.validTo forKey:@"cardvalidto"];
    [bankDict setObjectCheckNil:atmCard.cardPassword forKey:@"pwd"];
    return [bankDict JSONRepresentation];
}


- (RACSignal *)registerBankWith:(NSDictionary *)params {
    return [self creatRequestWithPath:api_v001_tpe_registeropandlink params:params isGet:false];
}
@end
