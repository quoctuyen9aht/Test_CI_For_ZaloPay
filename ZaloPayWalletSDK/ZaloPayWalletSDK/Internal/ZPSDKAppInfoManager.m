//
//  ZPSDKAppInfoCache.m
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 4/19/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPSDKAppInfoManager.h"
#import "UIDevice+ZPExtension.h"
#import "NetworkManager+ZaloPayWalletSDK.h"
#import "ZPPaymentChannel.h"
@implementation ZPSDKAppInfoManager

- (NSString *)keyWithAppId:(NSInteger)appId {
    NSString *appversion = [[UIDevice currentDevice] zpAppVersion];
    NSString *buildversion = [[UIDevice currentDevice] zpAppBuildVersion];
    return [NSString stringWithFormat:@"ZPSDK_AppInfo_%@_%@_%d", appversion, buildversion, (int)appId];
}

- (NSArray *)transtypeCheckSums:(NSArray *)arrayTranstype appData:(ZPAppData*)appData{
    if (appData.pmcTransTypeMap.count == 0) {
        return @[];
    }
    return [arrayTranstype map:^id(NSNumber *transtype) {
        ZPTransTypePmc *pmc = [appData.pmcTransTypeMap objectForKey:transtype];
        return pmc.checksum.length > 0 ? pmc.checksum : @"";
    }];
}

- (RACSignal *)getAppInfoWithId:(NSInteger)appID transtypes:(NSArray *)arrTranstype {
    ZPAppData *appInfo = [self appWithId:appID];
    if ([self shouldRequestWithApp:appInfo transtypes:arrTranstype] == false) {
        return [RACSignal return:appInfo];
    }
    NSArray *transtypechecksums = [self transtypeCheckSums:arrTranstype appData:appInfo];
    return [[[ZaloPayWalletSDKPayment sharedInstance] getAppInfo:appID
                                        appInfoChecksum:appInfo.checkSum
                                     transtypechecksums:transtypechecksums
                                             transtypes:arrTranstype] flattenMap:^__kindof RACSignal * _Nullable(NSDictionary  *_Nullable dictionary)
            {
                ZPAppData *app = [self addAppFrom:dictionary appId:appID];
                if (app == nil || app.status == ZPAPPDisable) {
                    return [RACSignal error:nil];
                }
                return  [RACSignal return:app];
            }];
}

- (BOOL)shouldRequestWithApp:(ZPAppData *)appInfo transtypes:(NSArray *)transtypes {
    if ([[NSDate date] timeIntervalSince1970] >= appInfo.expiredTime) {
        return YES;
    }
    for (NSNumber *oneType in transtypes) {
        if ([appInfo.pmcTransTypeMap objectForKey:oneType] == nil) {
            return YES;
        }
    }
    return NO;
}


- (ZPAppData *)appWithId:(NSInteger)appId {
    NSString *key  = [self keyWithAppId:appId];
    return [ZPCache objectForKey:key];
}

- (ZPAppData *)defaultWithId:(NSInteger)appId {
    ZPAppData *app = [[ZPAppData alloc] init];
    app.appID = appId;
    return app;
}

- (void)updateApp:(ZPAppData *)app withInfos:(NSDictionary *)json {
    
    int defaultValue = -100;
    NSDictionary *info = [json dictionaryForKey:@"info"];
    if (info.count > 0) {
        NSString *applogourl = [info stringForKey:@"applogourl"];
        NSString *appname = [info stringForKey:@"appname"];
        NSString *redirect_url = [info stringForKey:@"webredirecturl"];
        int viewresulttype = [info intForKey:@"viewresulttype" defaultValue:defaultValue];
        int status = [info intForKey:@"status" defaultValue:defaultValue];
        
        if (applogourl.length > 0) {
            app.appLogo = applogourl;
        }
        if (appname.length > 0) {
            app.appName = appname;
        }
        if (redirect_url.length > 0) {
            app.redirect_url = redirect_url;
        }
        if (status != defaultValue) {
            app.status = status;
        }
        if (viewresulttype != defaultValue) {
            app.viewResultType = viewresulttype;
        }
    }
    NSString *checkSum = [json stringForKey:@"appinfochecksum"];
    int expiredtime = [json uint32ForKey:@"expiredtime" defaultValue:defaultValue];
    if (expiredtime > 0) {
        app.expiredTime = [[NSDate date] timeIntervalSince1970] + expiredtime / 1000;
    }
    if (checkSum.length > 0) {
        app.checkSum = checkSum;
    }
}

- (void)updateApp:(ZPAppData *)app pmctranstypes:(NSArray *)pmctranstypes {
    if (pmctranstypes.count == 0) {
        return;
    }
    for (NSDictionary * dict in pmctranstypes){
        ZPTransTypePmc *ttPmc = [[ZPTransTypePmc alloc] initWithDictionary:dict];
        [app.pmcTransTypeMap setObject:ttPmc forKey:@(ttPmc.transType)];
    }
}

- (ZPAppData *)addAppFrom:(NSDictionary*) dictionary appId:(NSInteger)appId {
    if (dictionary.count == 0) {
        return nil;
    }
    NSString *key = [self keyWithAppId:appId];
    ZPAppData *app = [self appWithId:appId];
    if (!app) {
        app = [self defaultWithId:appId];
    }
    NSArray *pmctranstypes = [dictionary arrayForKey:@"pmctranstypes"];
    [self updateApp:app withInfos:dictionary];
    [self updateApp:app pmctranstypes:pmctranstypes];
    NSArray *pmcs = [dictionary arrayForKey:@"pmcs"];
    [self updatePmcsDisplayOrder:app withPmcs:pmcs];
    [ZPCache setObject:app forKey:key];
    return app;
}

- (void)updatePmcsDisplayOrder:(ZPAppData *)app withPmcs:(NSArray *)pmcs {
    if (pmcs.count == 0) {
        return;
    }
    NSMutableDictionary *pmcsDisplayOrder = [[NSMutableDictionary alloc] init];
    for (NSDictionary * dict in pmcs) {
        int channelID = [dict intForKey:@"pmcid"];
        int displayOrder = [dict intForKey:@"orderindex"];
        [pmcsDisplayOrder setObject:@(displayOrder) forKey:@(channelID)];
    }
    app.pmcsDisplayOrderMap = pmcsDisplayOrder;
}

@end
