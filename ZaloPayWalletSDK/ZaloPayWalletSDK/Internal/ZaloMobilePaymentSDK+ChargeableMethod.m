//
//  ZaloMobilePaymentSDK+ChargeableMethod.m
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 4/18/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZaloMobilePaymentSDK+ChargeableMethod.h"
#import "ZPPaymentMethod.h"
#import "ZPDataManager.h"
#import "ZPBill.h"
#import "ZPPaymentChannel.h"
#import "ZaloPayWalletSDKPayment.h"
#import "ZPMiniBank.h"
#import "ZPSDKStringConstants.h"
#import "ZPResourceManager.h"
#import "ZaloPayWalletSDKLog.h"
//#import <ZaloPayWalletSDK/ZaloPayWalletSDK-Swift.h>
#import "ZPBank.h"
#import "ZPVoucherInfo.h"
#import "ZPSDKAppInfoManager.h"
@implementation ZaloMobilePaymentSDK(ChargeableMethod)

#pragma mark - Payment ATM

- (long)billAmount:(ZPBill *)bill method:(ZPPaymentMethod*)method {
    long amount = [method calculateChargeWith:bill.finalAmount];
    if (bill.voucherInfo == nil && bill.promotionInfo == nil) {
        return amount;
    }
    amount = amount - [bill finalDiscountAmount];
    return amount;
}

- (void) processPaymentATMWithModel:(ZaloMobileProcessPaymentModel *)model
                      allAtmMethods:(NSMutableArray *)allAtmMethods
                     atmMinPPAmount:(long)minAmount
                     atmMaxPPAmount:(long)maxAmount{
    ZPDataManager *dataManager = [ZPDataManager sharedInstance];
    if (![model validATM]) {
        return;
    }
    
    ZPPaymentMethodType methodType = model.channel.channelType;
    BOOL allowPLP = [dataManager isPLPEnableWithTranstype:model.bill.transType pmcID:methodType];
    
    ZPPaymentMethod *method = [ZPNewAtmCardMethod new];
    method.userPLPAllow =  allowPLP;
    [model updateInfoForPaymentMethod:method
                                 icon:@"pay_ATM"
                           methodType:ZPPaymentMethodTypeAtm
                             miniBank:[ZPMiniBank new]
                    usingCustomStatus:-1];    
    
    if (![allAtmMethods containsObject:method]) {
        method.defaultChannel = YES;
        method.isTotalChargeGreaterThanBalance = NO;
        method.isTotalChargeGreaterThanMaxAmount = NO;
        long amount = [self billAmount:model.bill method:method];
        method.isSupportAmount =  (amount >= minAmount && amount <= maxAmount);
        [allAtmMethods addObjectNotNil:method];
    }
    
    NSArray *allCard = [ZPDataManager sharedInstance].paymentSavedCard;
    if (allCard.count == 0 || model.bill.transType == ZPTransTypeAtmMapCard) {
        return;
    }
    
    for (ZPSavedCard* savedCard in allCard) {
        if (![savedCard.bankCode isEqualToString:model.channel.bankCode]) {
            continue;
        }
        if ([savedCard.bankCode isEqualToString:stringCreditCardBankCode]) {
            continue;
        }
        ZPPaymentMethod *atmMethod = [ZPUserAtmCardMethod new];
        atmMethod.userPLPAllow = allowPLP;
        NSString *bankImage = [ZPResourceManager bankIconFromBankCode:savedCard.bankCode];
        ZPBank *bank = [dataManager.bankManager getBankInfoWith:savedCard.bankCode];

        [model updateInfoForPaymentMethod:atmMethod
                                     icon:bankImage
                               methodType:ZPPaymentMethodTypeSavedCard
                                 miniBank:nil
                        usingCustomStatus:bank.status];
        [model updateForPaymentMethod:atmMethod usingBankInfo:bank];
        atmMethod.savedCard = savedCard;
        atmMethod.methodName = [self getSaveCardTitleFromConfig:bank.bankName andLast4CardNo:savedCard.last4CardNo];
        [allAtmMethods addObjectNotNil:atmMethod];
    }
}

#pragma mark - Payment Credit Card
- (void) processPaymentWithCCDebitCardWithModel:(ZaloMobileProcessPaymentModel *)model
                                       methods:(NSMutableArray *)methods
{
    if (![model validCreditCard]) {
        return;
    }
    
    ZPDataManager *dataManager = [ZPDataManager sharedInstance];
    ZPPaymentMethod *method = [ZPNewAtmCardMethod new];
    method.userPLPAllow =  [dataManager isPLPEnableWithTranstype:model.bill.transType pmcID:model.channel.channelType];
    ZPBank *ccBank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:stringDebitCardBankCode];

    [model updateInfoForPaymentMethod:method
                                 icon:@"pay_CC"
                           methodType:ZPPaymentMethodTypeCCDebit
                             miniBank:[ZPMiniBank new]
                    usingCustomStatus:ccBank.status];
    
    if (model.bill.transType != ZPTransTypeWithDraw && ![methods containsObject:method]) {
        method.defaultChannel = YES;
        [methods addObjectNotNil:method];
    }
    
    NSArray *allCards = [ZPDataManager sharedInstance].paymentSavedCard;
    if (allCards.count == 0 || model.bill.transType == ZPTransTypeAtmMapCard) {
        return;
    }
    
    for (ZPSavedCard* savedCard in allCards) {
        if (![savedCard isCCDeBit]) {
            continue;
        }
        ZPPaymentMethod *ccMethod = [ZPUserCreditCardMethod new];
        ccMethod.userPLPAllow = [dataManager isPLPEnableWithTranstype:model.bill.transType pmcID:model.channel.channelType];
        DDLogInfo(@"ccsavedcardmethod.userPLPAllow: %d", method.userPLPAllow);
        NSString *detectedBankCode = [ZPResourceManager getCcBankCodeFromConfig:savedCard.first6CardNo];
        DDLogInfo(@"detectedBankCode: %@", detectedBankCode);
        NSString *bankImage = [ZPResourceManager bankIconFromBankCode:detectedBankCode];
        NSDictionary *bankDict = [[ZPDataManager sharedInstance].bankManager.ccBanksDict objectForKey:detectedBankCode];
        [model updateInfoForPaymentMethod:ccMethod
                                     icon:bankImage
                               methodType:ZPPaymentMethodTypeCCDebit
                                 miniBank:nil
                        usingCustomStatus:ccBank.status];
        ccMethod.miniBank = [model miniBankInfo:ccBank with:ccMethod.methodType];
        ccMethod.savedCard = savedCard;
        ccMethod.bankCode = detectedBankCode;
        NSString * ccBankName = [bankDict stringForKey:stringKeyCCBankName] ?: @"";
        ccMethod.methodName = [NSString stringWithFormat:stringMethodNameFormat,ccBankName, savedCard.last4CardNo];
        [methods addObjectNotNil:ccMethod];
    }
}
#pragma mark - Payment Credit Card
- (void) processPaymentWithCreditCardWithModel:(ZaloMobileProcessPaymentModel *)model
                                       methods:(NSMutableArray *)methods
{
    if (![model validCreditCard]) {
        return;
    }
    
    ZPDataManager *dataManager = [ZPDataManager sharedInstance];
    ZPPaymentMethod *method = [ZPNewAtmCardMethod new];
    method.userPLPAllow =  [dataManager isPLPEnableWithTranstype:model.bill.transType pmcID:model.channel.channelType];
    ZPBank *ccBank = [dataManager.bankManager getBankInfoWith:stringCreditCardBankCode];

    [model updateInfoForPaymentMethod:method
                                 icon:@"pay_CC"
                           methodType:ZPPaymentMethodTypeCreditCard
                             miniBank:[ZPMiniBank new]
                    usingCustomStatus:ccBank.status];
    
    if (model.bill.transType != ZPTransTypeWithDraw && ![methods containsObject:method]) {
        method.defaultChannel = YES;
        [methods addObjectNotNil:method];
    }
    
    NSArray *allCards = [ZPDataManager sharedInstance].paymentSavedCard;
    if (allCards.count == 0 || model.bill.transType == ZPTransTypeAtmMapCard) {
        return;
    }
    
    for (ZPSavedCard* savedCard in allCards) {
        if (![savedCard.bankCode isEqualToString: stringCreditCardBankCode]) {
            continue;
        }
        if (![savedCard.bankCode isEqualToString:model.channel.bankCode]) {
            continue;
        }
        ZPPaymentMethod *ccMethod = [ZPUserCreditCardMethod new];
        ccMethod.userPLPAllow = [dataManager isPLPEnableWithTranstype:model.bill.transType pmcID:model.channel.channelType];
        DDLogInfo(@"ccsavedcardmethod.userPLPAllow: %d", method.userPLPAllow);
        NSString *detectedBankCode = [ZPResourceManager getCcBankCodeFromConfig:savedCard.first6CardNo];
        DDLogInfo(@"detectedBankCode: %@", detectedBankCode);
        NSString *bankImage = [ZPResourceManager bankIconFromBankCode:detectedBankCode];
        NSDictionary *bankDict = [[ZPDataManager sharedInstance].bankManager.ccBanksDict objectForKey:detectedBankCode];
        [model updateInfoForPaymentMethod:ccMethod
                                     icon:bankImage
                               methodType:ZPPaymentMethodTypeSavedCard
                                 miniBank:nil
                        usingCustomStatus:ccBank.status];
        ccMethod.miniBank = [model miniBankInfo:ccBank with:ccMethod.methodType];
        ccMethod.savedCard = savedCard;
        ccMethod.bankCode = detectedBankCode;
        NSString * ccBankName = [bankDict stringForKey:stringKeyCCBankName] ?: @"";
        ccMethod.methodName = [NSString stringWithFormat:stringMethodNameFormat,ccBankName, savedCard.last4CardNo];
        [methods addObjectNotNil:ccMethod];
    }
}

#pragma mark - Payment PayWallet

- (void) processPaymentWithPayWalletWithModel:(ZaloMobileProcessPaymentModel *)model
                                      methods:(NSMutableArray *)methods {
    if (![model validPaymentWallet]) {
        return;
    }

    ZPDataManager *dataManager = [ZPDataManager sharedInstance];
    ZPPaymentMethod *method = [ZPZaloPayWalletMethod new];
    method.userPLPAllow =  [dataManager isPLPEnableWithTranstype:model.bill.transType pmcID:model.channel.channelType];
    [model updateInfoForPaymentMethod:method
                                 icon:@"ic_vi_zalo.png"
                           methodType:ZPPaymentMethodTypeZaloPayWallet
                             miniBank:[ZPMiniBank new]
                    usingCustomStatus:-1];
    
    if (model.bill.transType != ZPTransTypeWithDraw) {
        if (![methods containsObject:method]) {
            long amount = [self billAmount:model.bill method:method];
            method.isTotalChargeGreaterThanBalance = amount > model.balance;
            method.channel = model.channel;
            [methods addObjectNotNil:method];
        }
        return;
    }
    
    // Check Saved Card
    NSArray *allCard = [ZPDataManager sharedInstance].paymentSavedCard;
    if (allCard.count > 0) {
        for (ZPSavedCard *savedCard in allCard) {
            if (![savedCard.bankCode isEqualToString:model.channel.bankCode]) {
                continue;
            }
            ZPPaymentMethod *atmMethod = [savedCard isCCCard] ? [ZPUserCreditCardMethod new] : [ZPUserAtmCardMethod new];
            atmMethod.userPLPAllow = [dataManager isPLPEnableWithTranstype:model.bill.transType pmcID:model.channel.channelType];
            DDLogInfo(@"withdrawuserPLPAllow: %d",method.userPLPAllow);
            NSString *bankImage = [ZPResourceManager bankIconFromBankCode:savedCard.bankCode];
            ZPBank *bank = [dataManager.bankManager getBankInfoWith:savedCard.bankCode];
            [model updateInfoForPaymentMethod:atmMethod
                                         icon:bankImage
                                   methodType:ZPPaymentMethodTypeSavedCard
                                     miniBank:nil
                            usingCustomStatus:-1];
            
            if ([savedCard isCCCard]) {
                NSString * detectedBankCode = [ZPResourceManager getCcBankCodeFromConfig:savedCard.first6CardNo];
                ZPBank *ccBank = [dataManager.bankManager getBankInfoWith:stringCreditCardBankCode];
                NSDictionary *bankDict = [[ZPDataManager sharedInstance].bankManager.ccBanksDict objectForKey:detectedBankCode];
                NSString *ccBankName = [bankDict stringForKey:stringKeyCCBankName];
                NSString *bankImg = [ZPResourceManager bankIconFromBankCode:detectedBankCode];
                
                atmMethod.methodName = [NSString stringWithFormat:stringMethodNameFormat,ccBankName, savedCard.last4CardNo];
                atmMethod.methodIcon = bankImg;
                atmMethod.status = (model.channel.status == ZPAPPEnable ? ccBank.status: model.channel.status);
            }else {
                atmMethod.status = model.channel.status == ZPAPPEnable ? bank.status : model.channel.status;
                atmMethod.methodName = [self getSaveCardTitleFromConfig:bank.bankName andLast4CardNo:savedCard.last4CardNo];
            }
            
            [model updateForPaymentMethod:atmMethod usingBankInfo:bank];
            atmMethod.savedCard = savedCard;
            long amount = [self billAmount:model.bill method:atmMethod];
            atmMethod.isTotalChargeGreaterThanBalance = amount > model.balance;
            [methods addObjectNotNil:atmMethod];
        }
    }
    
    // Check Account
    
    NSArray *bankAccount = [ZPDataManager sharedInstance].paymentSavedBankAccount;
    if (bankAccount.count == 0) {
        return;
    }
    for (ZPSavedBankAccount *savedCard in bankAccount) {
        if (![savedCard.bankCode isEqualToString:model.channel.bankCode]){
            continue;
        }
        ZPPaymentMethod *iBankingMethod = [ZPUserIBankingAccount new];
        iBankingMethod.userPLPAllow = [dataManager isPLPEnableWithTranstype:model.bill.transType pmcID:model.channel.channelType];
            DDLogInfo(@"ibankingsavedcardmethod.userPLPAllow: %d", method.userPLPAllow);
            NSString *bankImage = [ZPResourceManager bankIconFromBankCode:savedCard.bankCode];
            ZPBank *bank = [dataManager.bankManager getBankInfoWith:savedCard.bankCode];
            [model updateInfoForPaymentMethod:iBankingMethod
                                         icon:bankImage
                                   methodType:ZPPaymentMethodTypeSavedAccount
                                     miniBank:nil
                            usingCustomStatus:bank.status];
            
            [model updateForPaymentMethod:iBankingMethod usingBankInfo:bank];
            iBankingMethod.savedBankAccount = savedCard;
            iBankingMethod.methodName = [self getSaveAccountTitleFromConfig:bank.bankName andLast4CardNo:savedCard.lastAccountNo bankCode:savedCard.bankCode];
            [methods addObjectNotNil:iBankingMethod];
    }
    
}

#pragma mark - Payment Ibanking

- (void) processPaymentWithIbankingWithModel:(ZaloMobileProcessPaymentModel *)model
                                     methods:(NSMutableArray *)methods {
    ZPDataManager *dataManager = [ZPDataManager sharedInstance];
    if (![model validIbanking]) {
        return;
    }
    
//    ZPPaymentMethod *method = [ZPNewIBankingAccount new];
//    method.userPLPAllow =  [dataManager isPLPEnableWithTranstype:model.bill.transType pmcID:model.channel.channelType];
//    [model updateInfoForPaymentMethod:method
//                                 icon:@"pay_ATM"
//                           methodType:ZPPaymentMethodTypeIbanking
//                             miniBank:[ZPMiniBank new] usingCustomStatus:-1];
//    
//    if (![methods containsObject:method]) {
//        method.defaultChannel = YES;
//        [methods addObjectNotNil:method];
//    }
    
    NSArray *allAccount = [ZPDataManager sharedInstance].paymentSavedBankAccount;
    
    if (allAccount.count == 0 || [model.bill isKindOfClass:[ZPIBankingAccountBill class]]) {
        return;
    }
    
    for (ZPSavedBankAccount *savedCard in allAccount) {
        if (![savedCard.bankCode isEqualToString:model.channel.bankCode]) {
            continue;
        }
        ZPPaymentMethod *iBankingMethod = [ZPUserIBankingAccount new];
        iBankingMethod.userPLPAllow = [dataManager isPLPEnableWithTranstype:model.bill.transType pmcID:model.channel.channelType];
        NSString *bankImage = [ZPResourceManager bankIconFromBankCode:savedCard.bankCode];
        ZPBank *bank = [dataManager.bankManager getBankInfoWith:savedCard.bankCode];
        [model updateInfoForPaymentMethod:iBankingMethod
                                     icon:bankImage
                               methodType:ZPPaymentMethodTypeSavedAccount
                                 miniBank:nil
                        usingCustomStatus:bank.status];
        [model updateForPaymentMethod:iBankingMethod usingBankInfo:bank];
        iBankingMethod.savedBankAccount = savedCard;
        iBankingMethod.methodName = [self getSaveAccountTitleFromConfig:bank.bankName andLast4CardNo:savedCard.lastAccountNo bankCode:savedCard.bankCode];
        [methods addObjectNotNil:iBankingMethod];
    }
}

#pragma mark - ChargebleMethods Implement

- (NSArray *)chargebleMethods:(ZPBill *)bill methodType:(ZPPaymentMethodType)methodType{
    ZPAppData *appData = [[ZPDataManager sharedInstance].appInfoManager appWithId:bill.appId];
    if (appData == nil) {
        return @[];
    }
    ZPTransTypePmc *ttPmc = [appData.pmcTransTypeMap objectForKey:@(bill.transType)];
    if (!ttPmc) {
        return @[];
    }
    [ZPDataManager sharedInstance].paymentChannel = ttPmc.pmcList;
    long balance = [ZaloPayWalletSDKPayment sharedInstance].appDependence.currentBalance;
    ZaloMobileProcessPaymentModel *dataModel = [[ZaloMobileProcessPaymentModel alloc] initWithBill:bill
                                                                                           balance:balance
                                                                                     paymentOption:methodType
                                                                                           channel:nil];
    NSDictionary *minMax = [self findMinMaxATMPPAmount:ttPmc.pmcList];
    long min = [minMax uint32ForKey:@"min"];
    long max = [minMax uint32ForKey:@"max"];
    
    NSMutableArray * methods = [NSMutableArray new];
    for (ZPChannel *channel in ttPmc.pmcList) {
        dataModel.channel = channel;
        
        dataModel.channel.displayOrder = [appData.pmcsDisplayOrderMap intForKey:@(channel.channelID) defaultValue:0];
        switch (channel.channelType) {
            case ZPPaymentMethodTypeAtm: {
                [self processPaymentATMWithModel:dataModel allAtmMethods:methods atmMinPPAmount:min atmMaxPPAmount:max];
                break;
            }
            case ZPPaymentMethodTypeCCDebit: {
                [self processPaymentWithCCDebitCardWithModel:dataModel methods:methods];
                break;
            }
            case ZPPaymentMethodTypeCreditCard: {
                [self processPaymentWithCreditCardWithModel:dataModel methods:methods];
                break;
            }
            case ZPPaymentMethodTypeZaloPayWallet: {
                [self processPaymentWithPayWalletWithModel:dataModel methods:methods];
                break;
            }
            case ZPPaymentMethodTypeIbanking: {
                [self processPaymentWithIbankingWithModel:dataModel methods:methods];
                break;
            }
            default:
                break;
        }
    }
    return methods;
}

- (NSDictionary *)findMinMaxATMPPAmount:(NSArray *)allChannel {
    NSInteger min = NSIntegerMax;
    NSInteger max = 0;
    for (ZPChannel *channel in allChannel) {
        if (channel.channelType != ZPPaymentMethodTypeAtm) {
            continue;
        }
        if (min > channel.minPPAmount) {
            min = channel.minPPAmount;
        }
        if (max < channel.maxPPAmount) {
            max = channel.maxPPAmount;
        }
    }
    return @{@"min": @(min),
             @"max": @(max)};
}

#pragma mark - ChargebleMethods End Implement

- (NSString *)getSaveCardTitleFromConfig:(NSString *)bankName andLast4CardNo: (NSString *)last4CardNo {
    NSString * title = [bankName removePrefix:@"NH "];
    if (title.length == 0) {
        return @"";
    }    
    return [NSString stringWithFormat:stringMethodNameFormat,title, last4CardNo];
}

- (NSString *)getSaveAccountTitleFromConfig:(NSString *)bankName andLast4CardNo: (NSString *)last4CardNo bankCode:(NSString *)bankCode {
    NSString * title = [bankName removePrefix:@"NH "];
    if ([bankCode isEqualToString: stringVietcomBankCode]) {
        id<ZPWalletDependenceProtocol> helper = [ZaloPayWalletSDKPayment sharedInstance].appDependence;
        
        NSString * phoneNumber = [helper userPhoneNumber];
        if (phoneNumber.length > 3) {
            NSString * first3PhoneNumber = [phoneNumber substringToIndex:3];
            NSString * last3PhoneNumber = [phoneNumber substringFromIndex:MAX((int)[phoneNumber length]-3, 0)];
            return [NSString stringWithFormat:stringMethodBankAccountFormat,title, first3PhoneNumber,last3PhoneNumber];
        }
        
        return [NSString stringWithFormat:stringBankAccountFormat,title];
    }
    return [NSString stringWithFormat:stringMethodNameFormat,title, last4CardNo];
    
}

@end

#pragma mark - Process Payment Model
@implementation ZaloMobileProcessPaymentModel

- (instancetype)initWithBill:(ZPBill *)bill
                     balance:(long)balance
               paymentOption:(ZPPaymentMethodType)option
                     channel:(ZPChannel *)channel {
    self = [super init];
    self.bill = bill;
    self.channel = channel;
    self.paymentOption = option;
    self.balance = balance;
    return self;
}

- (void)updateInfoForPaymentMethod:(ZPPaymentMethod *)method
                              icon:(NSString *)methodIcon
                        methodType:(ZPPaymentMethodType)type
                          miniBank:(ZPMiniBank *)bank
                 usingCustomStatus:(int)nStatus{
    
    method.status = self.channel.status;
    if (nStatus >= 0 && self.channel.status == 1) {
        method.status = nStatus;
    }
    method.methodIcon = methodIcon;
    method.support = self.channel.status;
    method.methodName = self.channel.channelName;
    method.feeCalType = self.channel.feeCalType;
    method.feeRate = self.channel.feeRate;
    method.minFee = self.channel.minFee;
    method.methodType = type;
    method.requireOTP = self.channel.requireOTP;
    method.amountRequireOTP = self.channel.amountRequireOTP;
    method.miniBank = bank;
    ZaloMobilePaymentSDK *sdk = [ZaloPayWalletSDKPayment sharedInstance].zaloPaySDK;
    long amount = [sdk billAmount:self.bill method:method];
    method.isSupportAmount = [self.channel shouldSupportAmount:amount];
    method.isTotalChargeGreaterThanMaxAmount = ![self.channel shouldSupportAmount:amount];
    method.minAppVersion = self.channel.minAppVersion;
    method.inamounttype = self.channel.inamounttype;
    method.overamounttype = self.channel.overamounttype;
    method.bankCode = self.channel.bankCode;
    method.errorMessage = [self.channel getErrorWithAmount:amount];
    method.displayOrder = self.channel.displayOrder;
}

#pragma mark - miniBank
- (BOOL)isDraw {
    return self.bill.transType == ZPTransTypeWithDraw;
}

- (ZPMiniBank *)transType:(ZPBank *)bank with:(ZPTransType)type {
    return [[ZPDataManager sharedInstance].bankManager getBankFunctionIsSupportTranType:bank
                                                               withTranType:type];
}

- (ZPMiniBank *)method:(ZPBank *)bank with:(ZPPaymentMethodType)method {
    return [[ZPDataManager sharedInstance].bankManager getBankFunctionIsSupportMethod:bank withPaymentMethod:method];
}

- (ZPMiniBank *)miniBankWithInfo:(ZPBank *)bank is:(BOOL)draw using:(ZPPaymentMethodType)m{
    return draw ?
    [self transType:bank with:ZPTransTypeWithDraw] :
    [self method:bank with:m];
}

- (ZPMiniBank *)miniBankInfo:(ZPBank *)bank with:(ZPPaymentMethodType)type {
    return [self miniBankWithInfo:bank is:self.isDraw using:type];
}

#pragma mark - update method with bank info

- (void)updateForPaymentMethod:(ZPPaymentMethod *)m
                 usingBankInfo:(ZPBank *)bank {
    m.miniBank = [self miniBankInfo:bank with:m.methodType];
}

#pragma mark - validate
- (BOOL)validIbanking {
    return (self.bill.transType != ZPTransTypeWithDraw) &&
    (self.paymentOption == ZPPaymentMethodTypeIbanking || self.paymentOption == ZPPaymentMethodTypeMethods);
}

- (BOOL)validPaymentWallet {
    return (self.paymentOption == ZPPaymentMethodTypeZaloPayWallet ||
            self.paymentOption == ZPPaymentMethodTypeMethods);
}

- (BOOL)validATM {
    return (self.bill.transType != ZPTransTypeWithDraw) &&
    (self.paymentOption == ZPPaymentMethodTypeAtm ||
     self.paymentOption == ZPPaymentMethodTypeMethods);
}

- (BOOL)validCreditCard {
    return (self.paymentOption == ZPPaymentMethodTypeCreditCard ||
            self.paymentOption == ZPPaymentMethodTypeMethods);
}

@end
