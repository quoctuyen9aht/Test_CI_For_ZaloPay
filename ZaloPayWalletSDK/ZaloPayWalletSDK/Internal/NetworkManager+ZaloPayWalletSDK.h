//
//  NetworkManager+ZaloPayWalletSDK.h
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 4/9/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//
//#import "ZaloPayWalletSDKPayment.h"
#import "../ZaloPayWalletSDKPayment.h"
@interface ZaloPayWalletSDKPayment (Network)
- (void)sendRequest:(NSString *)path
             params:(NSDictionary *)p
             method:(NSString *)m
            handler:(ZPSDKNetworkManagerCallback)callback;
- (RACSignal *)getAppInfo:(NSInteger)appId
          appInfoChecksum:(NSString *)appInfoChecksum
       transtypechecksums:(NSArray *)transtypechecksums
               transtypes:(NSArray *)transtypes;

- (RACSignal *)getBankListWithCheckSum:(NSString *)checksum
                       resourceVersion:(NSString *)resouceVersion;

- (RACSignal *)getBankGatewayListWithCheckSum:(NSString *)checksum
                                        appId:(NSInteger)appId;

- (RACSignal *)getPlatformInfo:(NSString *)checksum
               resourceVersion:(NSString *)resouceVersion
              cardinfochecksum:(NSString *)cardinfochecksum
           bankaccountchecksum:(NSString *)bankaccountchecksum;

- (RACSignal *)getListBankAccount:(NSString *)checksum;

- (RACSignal *)sdkReportErrorWithParams:(NSDictionary *)params;

- (RACSignal *)removeCard:(NSString *)bankCode first6cardno:(NSString *)first6cardno last4cardno:(NSString *)last4cardno;

- (RACSignal *)removeBankAccountWith:(NSString *)bankCustomerId bankCode:(NSString *)bankCode firstaccountno:(NSString *)firstaccountno lastaccountno:(NSString *)lastaccountno;

- (RACSignal *)getListCardInfo:(NSString *)zptransid cardInforCheckSum:(NSString *)cardinfochecksum;

- (RACSignal *)submitMapAccount:(NSString *)accouninfo;

- (RACSignal *)getStatusByaAppTransiId:(NSString *)apptransid appId:(NSInteger)appId;

- (RACSignal *)useVoucher:(ZPBill *)bill voucherHistory:(ZPVoucherHistory *)voucher;
- (RACSignal *)getUseVoucherStatus:(NSString*)appTransId;
- (RACSignal *)getVoucherStatus:(NSString *)voucherSig;
- (RACSignal *)getVoucherHistory;
- (RACSignal *)revertVoucher:(NSString *)voucherSig;
- (RACSignal *)getRevertVoucherStatus:(NSString*)appTransId;

// Direct discount
- (RACSignal *)getPromotionsWithAppID:(int32_t)appId campaignCode:(NSString *)campaignCode;
- (RACSignal *)registerPromotionWithBill:(ZPBill *)bill promotionHistory:(ZPPromotion *)promotion hashCardNumber:(NSString *)hashCardNumber hashBankAccount:(NSString *)hashBankAccount;
- (RACSignal *)getRegisterPromotionStatus:(long)promotiontransid;
- (RACSignal *)revertPromotion:(ZPPromotionInfo *)promotionInfo;
- (RACSignal *)getRevertPromotionStatus:(long)revertTransid;

- (RACSignal *)sdkRegisterBankWith:(NSDictionary *)params using:(NSString *)zpTransID;
- (RACSignal *)sdkAuthCardholderWith:(NSDictionary *)params using:(NSDictionary *)paramsNext;
- (RACSignal *)getUserProfile;
- (RACSignal *)getMerchantUserInforFrom:(NSInteger)appId;
- (RACSignal *)getStatusTransId:(NSString *)transid
                          appid:(int)appid
                      startDate:(NSDate *)startDate;
- (RACSignal *)updateKYC:(NSString*)zaloPhone
             fullName:(NSString*)fullName
               idType:(NSNumber*)idType
              idValue:(NSString*)idValue
               gender:(NSNumber *)gender
                  dob:(NSNumber *)dob;
- (RACSignal *)updateKYCInfor:(NSDictionary <NSString *, id> *)json;
@end

