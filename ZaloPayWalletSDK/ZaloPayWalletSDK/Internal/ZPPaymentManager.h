//
//  ZPPaymentManager.h
//  ZPSDK
//
//  Created by ZaloPayTeam on 12/22/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZPDefine.h"
#import "ZaloPayWalletSDKPayment.h"

@class ZPBill;
@class ZPPaymentInfo;
@class ZPPaymentResponse;
@class ZPChannel;
@class RACSignal;
@class ZPMapCardLog;
@class ZPVoucherHistory;

typedef NS_ENUM(NSInteger, VoucherStatusUse) {
    VoucherStatusUseSuccess = 1,
    VoucherStatusUseSuccessful = 4
};

typedef NS_ENUM(NSInteger, VoucherStatusRevert) {
    VoucherStatusRevertSuccess = 1,
    VoucherStatusRevertSuccessful = 2
};

@interface ZPPaymentManager : NSObject

+ (void)paymentWithBill:(ZPBill *)bill
         andPaymentInfo:(ZPPaymentInfo *)paymentInfo
               andAppId:(NSInteger)appID
                channel:(ZPChannel*)channel
                 inMode:(ZPPaymentMethodType)mode
            andCallback:(void (^)(ZPPaymentResponse *responseObject))callback;

+ (void)sendRequestErrorWithData:(NSString *)zptransID
                        bankCode:(NSString*)bankCode
                          exInfo:(NSString *)exinfo
                       exception:(NSString*)exception
                           appId:(NSInteger)appId;

+ (void)sendRequestErrorWithData:(NSString *)zptransID
                        bankCode:(NSString*)bankCode
                          exInfo:(NSString *)exinfo
                         apiPath:(NSString *)apiPath
                          params:(NSDictionary *)requestParams
                        response:(NSDictionary *)reponses
                           appId:(NSInteger)appId;

+ (void)writeMapCardLog:(ZPMapCardLog *)log;

- (void)zaloPayWalletSubmitTransidWithBill:(ZPBill *) bill
                                       pin:(NSString *)pin
                                 channelId:(int)channelID
                                  callback:(void (^)(ZPPaymentResponse* responseObject))callback;

- (void)getStatusByAppTransid:(NSString *)apptransid
                        appId:(NSInteger)appId
                     bankCode:(NSString *)bankCode
                         bill:(ZPBill *)bill
                     callback:(void (^)(ZPPaymentResponse* responseObject))callback;

- (void)bankAccountSubmitTransWithBill:(ZPBill *)bill
                                   pin:(NSString *)pin
                             channelId:(int)channelId
                              bankCode:(NSString *)bankCode
                        firstaccountno:(NSString *)firstaccountno
                         lastaccountno:(NSString *)lastaccountno
                              callback:(void (^)(ZPPaymentResponse* responseObject))callback;
/// Track Status
- (void)updatePaymentStatusWithTransID:(NSString *)zpTransID
                               andBill:(ZPBill*)bill
                               andStep:(ZPPaymentStep)step
                           andBankCode:(NSString*)bankCode
                           andCallback:(void (^)(ZPPaymentResponse * responseObject))callback;

+ (RACSignal *)submitVoucherWith:(ZPBill *) bill
                     voucherHistory:(ZPVoucherHistory *)voucher;

+ (RACSignal *)revertVoucher:(NSString *)voucherSig;



// Direct discount
+ (void)getRegisterPromotionStatusWith:(long)promotiontransid
                            subscriber:(id<RACSubscriber>)subscriber
                             startTime:(NSDate *)startTime;

+ (RACSignal *)registerPromotionWith:(ZPBill *) bill
                           promotion:(ZPPromotion *)promotion;

+ (void)getRevertPromotionStatusWith:(long)reverttransid
                          subscriber:(id<RACSubscriber>)subscriber
                           startTime:(NSDate *)startTime;

+ (RACSignal *)revertPromotionInfo:(ZPPromotionInfo *)promotionInfo;

@end
