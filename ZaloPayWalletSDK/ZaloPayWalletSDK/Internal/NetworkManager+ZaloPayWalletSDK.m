//
//  NetworkManager+ZaloPayWalletSDK.m
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 4/9/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "NetworkManager+ZaloPayWalletSDK.h"
#import "ZPConfig.h"

#import "ZPDataManager.h"
#import "UIDevice+ZPExtension.h"
#import "ZPPaymentManager.h"
#import "NSString+ZPExtension.h"
#import <ZaloPayCommon/NSCollection+JSON.h>
#import <ZaloPayNetwork/ZPNetWorkApi.h>
#import <ZaloPayNetwork/NSError+ZaloPayAPI.h>
#import "ZPBill.h"
#import "ZPVoucherHistory.h"
#import "ZPPaymentResponse.h"
#import <ZaloPayProfile/ZaloPayProfile-Swift.h>

extern NSString *kUrlVoucher;
extern NSString *kUrlDirectDiscount;
static NSDate *currentDate;
@implementation ZaloPayWalletSDKPayment (Network)

- (void)sendRequest:(NSString *)path
             params:(NSDictionary *)p
             method:(NSString *)m
            handler:(ZPSDKNetworkManagerCallback)callback {
    BOOL isGet = [[m lowercaseString] isEqualToString:@"get"];
    [self callBack:callback withNetworkSignal:[self.network creatRequestWithPath:path
                                                                                             params:p
                                                                                              isGet:isGet]];
}
- (void)callBack:(ZPSDKNetworkManagerCallback)callback withNetworkSignal:(RACSignal *)signal {
    [[signal deliverOnMainThread]  subscribeNext:^(NSDictionary *response) {
        int errorCode = [response intForKey:@"returncode"];
        if (callback) {
            callback(response, errorCode);
        }
    } error:^(NSError *error) {
        NSDictionary *dic = error.userInfo;
        int errorCode = (int)error.code;
        NSDictionary *data = nil;
        if ([error isApiError]) {
            errorCode = [dic intForKey:@"returncode"];
            data = dic;
        }
        if (callback) {
            callback(data, errorCode);
        }
    }];
}

- (RACSignal *)getAppInfo:(NSInteger)appId
          appInfoChecksum:(NSString *)appInfoChecksum
       transtypechecksums:(NSArray *)transtypechecksums
               transtypes:(NSArray *)transtypes {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:@(appId) forKey:@"appid"];
    [params setObjectCheckNil:appInfoChecksum forKey:@"appinfochecksum"];
    [params setObject:@"ios" forKey:@"platform"];
    [params setObjectCheckNil:[transtypechecksums JSONRepresentation] forKey:@"transtypechecksums"];
    [params setObjectCheckNil:[transtypes JSONRepresentation] forKey:@"transtypes"];
    return [[self.network requestWithPath:api_v001_tpe_getappinfo parameters:params] doError:^(NSError *error) {
        if ([error isApiError] == false) {
            return;
        }
        
        [ZPPaymentManager sendRequestErrorWithData:@"" bankCode:@"" exInfo:@"getappinfo" apiPath:api_v001_tpe_getappinfo params:params response:error.userInfo appId:appId];
        
    }];
}

- (RACSignal *)getBankListWithCheckSum:(NSString *)checksum
                       resourceVersion:(NSString *)resouceVersion{
    
    NSString *screen = [[UIDevice currentDevice] screenTypeString];
    NSString *mno = [[UIDevice currentDevice] zpMobileNetworkCode];
    NSString *devicemodel = [[UIDevice currentDevice] zpDeviceModel];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:@(kZaloPayClientAppId) forKey:@"appid"];
    [params setObject:@"ios" forKey:@"platform"];
    [params setObjectCheckNil:screen forKey:@"dscreentype"];
    [params setObjectCheckNil:checksum forKey:@"checksum"];
    [params setObjectCheckNil:resouceVersion forKey:@"resourceversion"];
    [params setObjectCheckNil:SDK_BUILD_VERSION forKey:@"sdkversion"];
    [params setObjectCheckNil:mno forKey:@"mno"];
    [params setObjectCheckNil:devicemodel forKey:@"devicemodel"];
    
    return [[self.network requestWithPath:api_v001_tpe_getbanklist parameters:params] doError:^(NSError *error) {
        if ([error isApiError] == false) {
            return;
        }

        [ZPPaymentManager sendRequestErrorWithData:@"" bankCode:@"" exInfo:@"getBanklist" apiPath:api_v001_tpe_getbanklist params:params response:error.userInfo appId:kZaloPayClientAppId];

    }];
}

- (RACSignal *)getBankGatewayListWithCheckSum:(NSString *)checksum appId:(NSInteger)appId {
    NSString *appVersion = [NSString castFrom:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params addEntriesFromDictionary:@{@"appid" : @(appId)}];
    [params setObject:@"ios" forKey:@"platform"];
    [params setObjectCheckNil:checksum forKey:@"checksum"];
    [params setObjectCheckNil:appVersion forKey:@"appversion"];
    
    return [[self.network requestWithPath:api_tpe_getbanklistgateway parameters:params] doError:^(NSError *error) {
        if ([error isApiError] == false) {
            return;
        }
        
        [ZPPaymentManager sendRequestErrorWithData:@"" bankCode:@"" exInfo:@"getBanklistGateway" apiPath:api_v001_tpe_getbanklist params:params response:error.userInfo appId:kZaloPayClientAppId];
    }];
}

- (RACSignal *)getPlatformInfo:(NSString *)checksum
               resourceVersion:(NSString *)resouceVersion
              cardinfochecksum:(NSString *)cardinfochecksum
           bankaccountchecksum:(NSString *)bankaccountchecksum{
    
    NSString *dscreentype = [[UIDevice currentDevice] screenTypeString];
    NSString *mno = [[UIDevice currentDevice] zpMobileNetworkCode];
    NSString *devicemodel = [[UIDevice currentDevice] zpDeviceModel];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:@(kZaloPayClientAppId) forKey:@"appid"];
    [params setObject:@"ios" forKey:@"platformcode"];
    [params setObjectCheckNil:dscreentype forKey:@"dscreentype"];
    [params setObjectCheckNil:checksum forKey:@"platforminfochecksum"];
    [params setObjectCheckNil:SDK_BUILD_VERSION forKey:@"sdkversion"];
    [params setObjectCheckNil:mno forKey:@"mno"];
    [params setObjectCheckNil:devicemodel forKey:@"devicemodel"];
    [params setObjectCheckNil:resouceVersion forKey:@"resourceversion"];
    [params setObjectCheckNil:cardinfochecksum forKey:@"cardinfochecksum"];
    [params setObjectCheckNil:bankaccountchecksum forKey:@"bankaccountchecksum"];
    
    return [[self.network requestWithPath:api_v001_tpe_v001getplatforminfo parameters:params] doError:^(NSError *error) {
        if ([error isApiError] == false) {
            return;
        }

        [ZPPaymentManager sendRequestErrorWithData:@"" bankCode:@"" exInfo:@"v001getplatforminfo" apiPath:api_v001_tpe_v001getplatforminfo params:params response:error.userInfo appId:kZaloPayClientAppId];
        
    }];
}

- (RACSignal *)getListBankAccount:(NSString *)checksum {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:checksum forKey:@"bankaccountchecksum"];
    return [[self.network creatRequestWithPath:api_um_listbankaccountforclient params:params isGet:false] doError:^(NSError *error) {
        if ([error isApiError]) {

            [ZPPaymentManager sendRequestErrorWithData:@"" bankCode:@"" exInfo:@"listbankaccountforclient" apiPath:api_um_listbankaccountforclient params:params response:error.userInfo appId:kZaloPayClientAppId];
        }
    }];
}

- (RACSignal *)sdkReportErrorWithParams:(NSDictionary *)params {
    return [self.network creatRequestWithPath:api_v001_tpe_sdkerrorreport params:params isGet:true];
}

- (RACSignal *)removeCard:(NSString *)bankCode first6cardno:(NSString *)first6cardno last4cardno:(NSString *)last4cardno {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:bankCode forKey:@"bankcode"];
    [params setObjectCheckNil:first6cardno forKey:@"first6cardno"];
    [params setObjectCheckNil:last4cardno forKey:@"last4cardno"];
    return [[self.network creatRequestWithPath:api_v001_tpe_removemapcard params:params isGet:false] doError:^(NSError *error) {
        if ([error isApiError] == false) {
            return;
        }

        [ZPPaymentManager sendRequestErrorWithData:@"" bankCode:@"" exInfo:@"removemapcard" apiPath:api_v001_tpe_removemapcard params:params response:error.userInfo appId:kZaloPayClientAppId];
        
    }];
}

- (RACSignal *)removeBankAccountWith:(NSString *)bankCustomerId bankCode:(NSString *)bankCode firstaccountno:(NSString *)firstaccountno lastaccountno:(NSString *)lastaccountno {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:bankCode forKey:@"bankcode"];
    [params setObjectCheckNil:firstaccountno forKey:@"firstaccountno"];
    [params setObjectCheckNil:lastaccountno forKey:@"lastaccountno"];
    [params setObjectCheckNil:bankCustomerId forKey:@"bankcustomerid"];
    return [[self.network creatRequestWithPath:api_v001_tpe_removemapaccount params:params isGet:false] doError:^(NSError *error) {
        if ([error isApiError] == false) {
            return;
        }
        
        [ZPPaymentManager sendRequestErrorWithData:@"" bankCode:@"" exInfo:@"removebankaccount" apiPath:api_v001_tpe_removemapaccount params:params response:error.userInfo appId:kZaloPayClientAppId];
        
    }];
}

- (RACSignal *)getListCardInfo:(NSString *)zptransid cardInforCheckSum:(NSString *)cardinfochecksum {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:zptransid forKey:@"zptransid"];
    [params setObjectCheckNil:cardinfochecksum forKey:@"cardinfochecksum"];
    return [[self.network creatRequestWithPath:api_um_listcardinfoforclient params:params isGet:false] doError:^(NSError *error) {
        if ([error isApiError] == false) {
            return;
        }

        [ZPPaymentManager sendRequestErrorWithData:@"" bankCode:@"" exInfo:@"listcardinfoforclient" apiPath:api_um_listcardinfoforclient params:params response:error.userInfo appId:kZaloPayClientAppId];
    }];
}


- (RACSignal *)submitMapAccount:(NSString *)accouninfo {
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    UIDevice * device = [UIDevice currentDevice];
    id<ZPWalletDependenceProtocol> helper = [ZaloPayWalletSDKPayment sharedInstance].appDependence;
    NSString *deviceId = [helper getDeviceId];
    NSString *zaloId = [helper getZaloId];
    [params setObjectCheckNil:zaloId forKey:@"zaloid"];
    [params setObjectCheckNil:accouninfo  forKey:@"bankaccountinfo"];
    [params setObjectCheckNil:@"ios" forKey:@"platform"];
    [params setObjectCheckNil:deviceId forKey:@"deviceid"];
    [params setObjectCheckNil:[device zpMobileNetworkCode] forKey:@"mno"];
    [params setObjectCheckNil:SDK_BUILD_VERSION forKey:@"sdkver"];
    [params setObjectCheckNil:[device zpOsVersion] forKey:@"osver"];
    [params setObjectCheckNil:[device zpDeviceModel] forKey:@"devicemodel"];
    [params setObjectCheckNil:self.network.connectionType forKey:@"conntype"];
    return [self.network creatRequestWithPath:api_v001_tpe_submitmapaccount params:params isGet:false];
}

- (RACSignal *)getStatusByaAppTransiId:(NSString *)apptransid appId:(NSInteger)appId {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:@(appId) forKey:@"appid"];
    [params setObjectCheckNil:apptransid forKey:@"apptransid"];
    return [[self.network creatRequestWithPath:api_v001_tpe_getstatusbyapptransidforclient params:params isGet:YES] doError:^(NSError * _Nonnull error) {
        if ([error isApiError] == false) {
            return;
        }
        [ZPPaymentManager sendRequestErrorWithData:@""
                                          bankCode:@""
                                            exInfo:@"getstatusbyapptransidforclient"
                                           apiPath:api_v001_tpe_getstatusbyapptransidforclient
                                            params:params
                                          response:error.userInfo
                                             appId:kZaloPayClientAppId];
    }];
}

// ========================================================================================================
// PROMOTION API
- (RACSignal *)getPromotionsWithAppID:(int32_t)appId campaignCode:(NSString *)campaignCode {
    long long requestDate = [[NSDate date] timeIntervalSince1970] * 1000;
 
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:@{@"appid":@(appId),
                                                                                    @"reqdate":@(requestDate)
                                                                                    }];
    [params setObjectCheckNil:campaignCode forKey:@"campaigncode"];
    [params setObjectCheckNil:[ZPProfileManager shareInstance].userLoginData.phoneNumber forKey:@"phonenumber"];
    NSString *path = [kUrlDirectDiscount stringByAppendingString:api_getpromotioncampaigns];
    return [self.network creatRequestWithPath:path params:params isGet:true];
}

- (RACSignal *)registerPromotionWithBill:(ZPBill *)bill promotionHistory:(ZPPromotion *)promotion hashCardNumber:(NSString *)hashCardNumber hashBankAccount:(NSString *)hashBankAccount {
    long long requestDate = [[NSDate date] timeIntervalSince1970] * 1000;
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:@{@"appid":@(bill.appId),
                                                                                    @"reqdate":@(requestDate),
                                                                                    @"amount":@(bill.amount)
                                                                                    }];
    
    [params setObjectCheckNil:bill.appTransID forKey:@"apptransid"];
    [params setObjectCheckNil:promotion.paymentConditions.currentBankcode forKey:@"bankcode"];
    [params setObjectCheckNil:promotion.paymentConditions.currentCCBankcode forKey:@"ccbankcode"];
    long long discountamount = promotion.discounttype == ZPPromotionTypePercent ? MIN(self.bill.amount * promotion.discountAmountFinal/100, promotion.cap)  : promotion.discountAmountFinal;
    [params setObjectCheckNil:@(discountamount) forKey:@"discountamount"];
    [params setObjectCheckNil:bill.embedData forKey:@"embeddata"];
    [params setObjectCheckNil:promotion.campaigncode forKey:@"campaigncode"];
    [params setObjectCheckNil:bill.appUser forKey:@"appuser"];
    [params setObjectCheckNil:@(promotion.paymentConditions.currentPMCID) forKey:@"pmcid"];
    [params setObjectCheckNil:hashCardNumber forKey:@"hashcardnumber"];
    [params setObjectCheckNil:hashBankAccount forKey:@"hashbankaccount"];
    
    NSString *path = [kUrlDirectDiscount stringByAppendingString:api_registerpromotion];
    return [self.network creatRequestWithPath:path params:params isGet:false];
}

- (RACSignal *)getRegisterPromotionStatus:(long)promotiontransid {
    long long requestDate = [[NSDate date] timeIntervalSince1970] * 1000;
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary: @{@"reqdate":@(requestDate),
                                                                                      @"promotiontransid":@(promotiontransid)
                                                                                      }];
    NSString *path = [kUrlDirectDiscount stringByAppendingString:api_getregisterpromotionstatus];
    return [self.network creatRequestWithPath:path params:params isGet:true];
}
                                   
- (RACSignal *)revertPromotion:(ZPPromotionInfo *)promotionInfo {
    long long requestDate = [[NSDate date] timeIntervalSince1970] * 1000;
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary: @{@"reqdate":@(requestDate)
                                                                                     }];
    
     [params setObjectCheckNil:promotionInfo.promotionsig forKey:@"promotionsig"];
     [params setObjectCheckNil:promotionInfo.promotionUsed.campaigncode forKey:@"campaigncode"];
    
    NSString *path = [kUrlDirectDiscount stringByAppendingString:api_revertpromotion];
    return [self.network creatRequestWithPath:path params:params isGet:false];
}

- (RACSignal *)getRevertPromotionStatus:(long)revertTransid {
    long long requestDate = [[NSDate date] timeIntervalSince1970] * 1000;
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary: @{@"reqdate":@(requestDate),
                                                                                      @"reverttransid":@(revertTransid)
                                                                                      }];
    NSString *path = [kUrlDirectDiscount stringByAppendingString:api_getrevertpromotionstatus];
    return [self.network creatRequestWithPath:path params:params isGet:true];
}

// ========================================================================================================

- (RACSignal *)useVoucher:(ZPBill *)bill voucherHistory:(ZPVoucherHistory *)voucher {
    long long timestamp = [[NSDate date] timeIntervalSince1970] * 1000;
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:@{@"appid":@(bill.appId),
                                                                                    @"amount":@(bill.amount),
                                                                                    @"pmc":@(voucher.paymentConditions.currentPMCID),
                                                                                    @"timestamp":@(timestamp)
                                                                                    }];
    [params setObjectCheckNil:bill.appTransID forKey:@"apptransid"];
    [params setObjectCheckNil:voucher.vouchercode forKey:@"vouchercode"];
    [params setObjectCheckNil:voucher.paymentConditions.currentBankcode forKey:@"bankcode"];
    [params setObjectCheckNil:voucher.paymentConditions.currentCCBankcode forKey:@"ccbankcode"];
    [params setObjectCheckNil:bill.embedData forKey:@"embeddata"];
    NSString *path = [kUrlVoucher stringByAppendingString:api_usevoucher];
    return [self.network creatRequestWithPath:path params:params isGet:false];
}
- (RACSignal *)getUseVoucherStatus:(NSString*)appTransId {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObjectCheckNil:appTransId forKey:@"transid"];
    NSString *path = [kUrlVoucher stringByAppendingPathComponent:api_getusevoucherstatus];
    return [self.network creatRequestWithPath:path params:params isGet:true];
}

- (RACSignal *)getVoucherStatus:(NSString *)voucherSig {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObjectCheckNil:voucherSig forKey:@"vouchersig"];
    NSString *path = [kUrlVoucher stringByAppendingString:api_getvoucherstatus];
    return [self.network creatRequestWithPath:path params:params isGet:false];
}
    
- (RACSignal *)getVoucherHistory {
    NSString *path = [kUrlVoucher stringByAppendingString:api_getvoucherhistory];
    return [self.network creatRequestWithPath:path params:nil isGet:true];
}

- (RACSignal *)revertVoucher:(NSString *)voucherSig {
    if ([voucherSig length] == 0) {
        return [RACSignal error:[NSError new]];
    }
    NSTimeInterval timestamp = [[NSDate date] timeIntervalSince1970] * 1000;
    NSDictionary *params = @{ @"timestamp": @((long long)timestamp),
                             @"vouchersig": voucherSig };
    NSString *path = [kUrlVoucher stringByAppendingString:api_revertvoucher];
    return [self.network creatRequestWithPath:path params:params isGet:false];
}

- (RACSignal *)getRevertVoucherStatus:(NSString*)appTransId {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObjectCheckNil:appTransId forKey:@"transid"];
    NSString *path = [kUrlVoucher stringByAppendingString:api_getrevertvoucherstatus];
    return [self.network creatRequestWithPath:path params:params isGet:true];
}

// ========================================================================================================

- (RACSignal *)sdkRegisterBankWith:(NSDictionary *)params using:(NSString *)zpTransID {
    // Using to get trans status
    __block NSMutableDictionary *nextParams = [NSMutableDictionary dictionary];
    NSInteger appId = [params[@"appid"] integerValue];
    NSString *deviceId = params[@"deviceid"];
    [nextParams setObjectCheckNil:@(appId) forKey:@"appid"];
    [nextParams setObjectCheckNil:deviceId forKey:@"deviceid"];
    [nextParams setObjectCheckNil:zpTransID forKey:@"zptransid"];
    currentDate = [NSDate date];
    return [[self.network creatRequestWithPath:api_v001_tpe_registeropandlink params:params isGet:NO] catch:^RACSignal *(NSError *error) {
        NSDictionary *response = error.userInfo;
        if (![response isKindOfClass:[NSDictionary class]]) {
            return [RACSignal error:error];
        }
        
        // Track response
        BOOL isProcessing = [response[@"isprocessing"] boolValue];
        if (!isProcessing) {
            // Error
            return [RACSignal error:error];
        }
        NSString *nextTrans = [response stringForKey:@"zptransid" defaultValue:@""];
        [nextParams setObjectCheckNil:nextTrans forKey:@"zptransid"];
        return [self trackStatus:nextParams authenCard:NO];
    }];
}

- (RACSignal *)sdkAuthCardholderWith:(NSDictionary *)params using:(NSDictionary *)paramsNext {
    currentDate = [NSDate date];
    return [[[self.network creatRequestWithPath:api_v001_tpe_authcardholderformapping params:params isGet:NO] flattenMap:^__kindof RACSignal * _Nullable(NSDictionary  *_Nullable response) {
        BOOL isProccess = [[response objectForKey:@"isprocessing"] boolValue];
        if (isProccess) {
//            DDLogInfo(@"Response : %@", response);
            return [RACSignal error:[NSError errorWithDomain:NSCocoaErrorDomain code:NSURLErrorBadServerResponse userInfo:response]];
        }
        return [RACSignal return:response];
    }] catch:^RACSignal *(NSError *error) {
        NSDictionary *response = error.userInfo;
        if (![response isKindOfClass:[NSDictionary class]]) {
            return [RACSignal error:error];
        }
        // Track response
        BOOL isProcessing = [response[@"isprocessing"] boolValue];
        if (!isProcessing) {
            // Error
            return [RACSignal error:error];
        }
        return [self trackStatus:paramsNext authenCard:YES];
    }];
}

#pragma mark track status
- (RACSignal *)trackStatus:(NSDictionary *)params
                authenCard:(BOOL)isCheking {
    return [[[self.network creatRequestWithPath:api_v001_tpe_getstatusmapcard params:params isGet:NO] flattenMap:^__kindof RACSignal * _Nullable(NSDictionary  *_Nullable response) {
        BOOL isProccess = [[response objectForKey:@"isprocessing"] boolValue];
        if (isProccess) {
            return [RACSignal error:[NSError errorWithDomain:NSCocoaErrorDomain code:NSURLErrorBadServerResponse userInfo:response]];
        }
        NSString *result = [response stringForKey:@"data" defaultValue:@""];
        NSDictionary *nextAction = [result JSONValue];
        return [RACSignal return:nextAction];
    }] catch:^RACSignal *(NSError *error) {
        NSDictionary *response = error.userInfo;
        if (![response objectForKey:@"isprocessing"]) {
            return [RACSignal error:error];
        }
        NSDate *newDate = [NSDate date];
        NSTimeInterval t = [newDate timeIntervalSinceDate:currentDate];
        if (t > 30) {
            return [RACSignal error:[[NSError alloc] initWithDomain:@"Time out"
                                                               code:-1
                                                           userInfo:@{@"returnmessage":@"Hết thời gian kết nối đến máy chủ"}]];
        }
        NSInteger returnCode = [response intForKey:@"returncode" defaultValue:0];
        // It's retry
        if (returnCode == ZALOPAY_ERRORCODE_ATM_RETRY_OTP ||
            returnCode == ZALOPAY_ERRORCODE_REGISTER_BANK_WRONG_IDENTIFY) {
            return [RACSignal error:error];
        }
        
        BOOL isProccess = [[response objectForKey:@"isprocessing"] boolValue];
        NSString *result = [response stringForKey:@"data" defaultValue:@""];
        NSDictionary *nextAction = [result JSONValue];
        
        BOOL isOke = [nextAction allKeys].count == 2;
        if (isProccess) {
            NSString *nextTrans = [response stringForKey:@"zptransid" defaultValue:@""];
            if (isOke) {
                NSMutableDictionary *nDict = [NSMutableDictionary dictionaryWithDictionary:nextAction];
                [nDict addEntriesFromDictionary:@{@"zptransid": nextTrans}];
                nextAction = nDict;
            }
            
            if (isOke && !isCheking) {
                return [RACSignal return: nextAction];
            }
            
            RACSignal *retry = [[RACSignal interval:1 onScheduler:[RACScheduler scheduler]] flattenMap:^__kindof RACSignal * _Nullable(NSDate * _Nullable value) {
                // Next Check
                return [self trackStatus:params authenCard:isCheking];
            }];
            return retry;
            
        }else {
            // Finish
            return returnCode == 1 ? [RACSignal return:response] : [RACSignal error:error];
            
        }
    }];
}

- (RACSignal *)getUserProfile {
    return [self.network creatRequestWithPath:api_um_getuserprofilelevel params:nil isGet:YES];
}

- (RACSignal *)getMerchantUserInforFrom:(NSInteger)appId {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@(appId) forKey:@"appid"];
    return [self.network requestWithPath:api_ummerchant_getmerchantuserinfo parameters:params];
}

- (RACSignal *)getStatusTransId:(NSString *)transid
                          appid:(int)appid
                      startDate:(NSDate*)startDate {

    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSString *deviceid = [[ZaloPayWalletSDKPayment sharedInstance].appDependence getDeviceId];
    [params setObjectCheckNil:transid forKey:@"zptransid"];
    [params setObjectCheckNil:@(appid) forKey:@"appid"];
    [params setObjectCheckNil:deviceid forKey:@"deviceid"];
    return [[[self.network creatRequestWithPath:api_v001_tpe_gettransstatus params:params isGet:NO] flattenMap:^__kindof RACSignal * _Nullable(NSDictionary  *_Nullable response) {
        BOOL isProcessing = [response boolForKey:@"isprocessing"];
        //Still processing
        if (isProcessing) {
            return [RACSignal error:[NSError errorWithDomain:NSCocoaErrorDomain code:NSURLErrorBadServerResponse userInfo:response]];
        }
        ZPPaymentResponse *rsp = [self responseFrom:response];
        return [RACSignal return:rsp];
    }] catch:^RACSignal *(NSError *error) {
        NSDictionary *response = error.userInfo;
        BOOL isProcessing = [response boolForKey:@"isprocessing"];
        NSDate *newDate = [NSDate date];
        NSTimeInterval t = [newDate timeIntervalSinceDate:startDate];
        NSInteger timeout = 30.0;
        ZPPaymentResponse *rsp = [self responseFrom:response];
        BOOL expireTime = t >= timeout;
        if (!isProcessing || expireTime) {
            // Change status to show correct result
            rsp.exchangeStatus = expireTime ? kZPZaloPayCreditStatusCodeUnidentified : rsp.exchangeStatus;
            return [RACSignal return:rsp];
        }
        
        RACSignal *retry = [[[RACSignal interval:1 onScheduler:[RACScheduler scheduler]] take:1] flattenMap:^__kindof RACStream *_Nullable(id value) {
            // Retry
            return [self getStatusTransId:transid appid:appid startDate:startDate];
        }];
        return retry;
        
    }];
}

- (ZPPaymentResponse *_Nullable) responseFrom:(NSDictionary *_Nullable)response {
    // Make a copy avoid release instance
    NSDictionary *temp;
    @try {
        // Use for make sure it is natural NSDictionary this is conform copy protocol
        temp = [response copy];
    } @catch (NSException * e) {}
    
    // Valid object
    if (![temp isKindOfClass:[NSDictionary class]] || [temp allKeys] == 0) {
        return nil;
    }
    
    int rtcode = [temp intForKey:@"returncode"];
    NSString *returnmessage = [temp stringForKey:@"returnmessage"];
    NSString *suggestMessage = [temp stringForKey:@"suggestmessage"];
    NSArray *suggestAction = [temp arrayForKey:@"suggestaction"];
    
    ZPPaymentResponse* responseObject = [[ZPPaymentResponse alloc] init];
    responseObject.errorCode = rtcode;
    responseObject.originalCode = rtcode;
    responseObject.message = returnmessage;
    responseObject.suggestMessage = suggestMessage;
    responseObject.suggestAction = suggestAction;
    responseObject.isProcessing = [[temp objectForKey:@"isprocessing"] boolValue];
    responseObject.data = [temp stringForKey:@"data"];
    responseObject.reqdate = [temp uint64ForKey:@"reqdate"];
    responseObject.zpTransID = [temp stringForKey:@"zptransid"];
    responseObject.userFeeAmount = [temp intForKey:@"userfeeamount"];
    return responseObject;
}


- (RACSignal *)updateKYC:(NSString*)zaloPhone
                fullName:(NSString*)fullName
                  idType:(NSNumber*)idType
                 idValue:(NSString*)idValue
                  gender:(NSNumber *)gender
                     dob:(NSNumber *)dob {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObjectCheckNil:zaloPhone forKey:@"zalophone"];
    [params setObjectCheckNil:gender forKey:@"gender"];
    [params setObjectCheckNil:fullName forKey:@"fullname"];
    [params setObjectCheckNil:dob forKey:@"birthday"];
    [params setObjectCheckNil:idType forKey:@"idtype"];
    [params setObjectCheckNil:idValue forKey:@"idvalue"];
    return [self updateKYCInfor:params];
}

- (RACSignal *)updateKYCInfor:(NSDictionary <NSString *, id> *)json {
    NSDictionary *params = json ?: @{};
    return [self.network postRequestWithPath:api_um_setkyc parameters:params];
}

@end
