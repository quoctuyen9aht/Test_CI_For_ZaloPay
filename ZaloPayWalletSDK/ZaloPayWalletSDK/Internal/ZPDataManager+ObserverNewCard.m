//
//  ZPDataManager+ObserverNewCard.m
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/23/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPDataManager+ObserverNewCard.h"
#import "ZPPaymentChannel.h"
#import "ZPPaymentInfo.h"
#import "ZPBill.h"
#import "ZaloPayWalletSDKLog.h"
#import "ZPBank.h"

#define kMapCardNotification  @"kMapCardNotification"
#define cardLength 16


@implementation ZPDataManager (ObserverNewCard)

- (void)startObserverNewCard {
    [[self.addNewCardSignal filter:^BOOL(id value) {
        return [value isKindOfClass:[ZPCreditCard class]] || [value isKindOfClass:[ZPAtmCard class]];
    }] subscribeNext:^(id x) {
        if ([x isKindOfClass:[ZPCreditCard class]]) {
            [self handleCreditCard:(ZPCreditCard *)x];
        } else if ([x isKindOfClass:[ZPAtmCard class]]) {
            [self handleAtmCard:(ZPAtmCard *)x];
        }
    }];
}


- (void)handleAtmCard:(ZPAtmCard *)atmCard {
    
    ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:atmCard.bankCode];
    if (bank.supportType == ZPSDK_BankType_Account) {
        [[ZPDataManager sharedInstance] updateListBankAccount:^(NSArray *listAccount, NSError *error) {}];
        return;
    }
    
    if (atmCard.cardNumber.length < cardLength) {
        return;
    }
    if([self checkSavedCardIsExist:atmCard.cardNumber]) {
        return;
    }
    NSString *first6No = atmCard.cardNumber.length > 6 ? [atmCard.cardNumber substringToIndex:6]: @"";
    NSString *last4No = atmCard.cardNumber.length > 4 ? [atmCard.cardNumber substringFromIndex:[atmCard.cardNumber length] - 4] : @"";
    if (atmCard.isMapcard) {
        NSDictionary * bankInfo = @{@"zptransid": atmCard.zpTransID,
                                    @"tpebankcode": atmCard.bankCode,
                                    @"first6cardno":first6No,
                                    @"last4cardno":last4No
                                    };
        
        [self updateSavedCardWithDictionary:bankInfo];
    }
}


- (void)handleCreditCard:(ZPCreditCard *)creditCard {
    if (creditCard.cardNumber.length < cardLength) {
        return;
    }
    
    if([self checkSavedCardIsExist:creditCard.cardNumber]) {
        return;
    }
    NSString *first6No = creditCard.cardNumber.length > 6 ? [creditCard.cardNumber substringToIndex:6]: @"";
    NSString *last4No = creditCard.cardNumber.length > 4 ? [creditCard.cardNumber substringFromIndex:[creditCard.cardNumber length] - 4] : @"";
    if (creditCard.isMapcard) {
        NSDictionary * bankInfo = @{@"zptransid": creditCard.zpTransID,
                                    @"tpebankcode": stringCreditCardBankCode,
                                    @"first6cardno":first6No,
                                    @"last4cardno":last4No
                                    };
        
        [self updateSavedCardWithDictionary:bankInfo];
    }
}


//
- (void)updateSavedCardWithDictionary:(NSDictionary*)bankInfo {
    // new version
    DDLogInfo(@"bankInfo %@", bankInfo);
    [[[ZPDataManager sharedInstance] addSavedCard:bankInfo] subscribeCompleted:^{}];
}

@end
