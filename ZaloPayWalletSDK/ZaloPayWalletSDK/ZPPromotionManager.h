//
//  ZPPromotionManager.h
//  ZaloPayWalletSDK
//
//  Created by Phuochh on 5/31/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZPPromotionInfo.h"

@class ZPBill;
@class RACSignal;

@interface ZPPromotionManager : NSObject
@property (nonatomic,strong) NSMutableArray <ZPPromotionInfo *>*promotionsWillRevert;
- (RACSignal *)loadAllAvailablePromotionsOfBill:(ZPBill *)bill;
- (BOOL)conditionForPaymentMethod:(ZPPromotion *)promotion forBill:(ZPBill *) bill;
- (long long)getValueCurrencyFromPromotion:(ZPPromotion *)promotion ofBill:(ZPBill *)bill;
- (ZPPromotion *)getBestPromotionFromPromotions:(NSArray *)promotions ofBill:(ZPBill *)bill;
- (void)promotionsWillRevertAddPromotion:(ZPPromotionInfo *)promotionInfo NS_SWIFT_NAME(addRevert(promotion:));
- (void)revertPromotionsAndUsePromotion:(ZPPromotionInfo *)promotionUsed NS_SWIFT_NAME(revertPromotionsAndUse(promotion:));

- (NSMutableArray *)promotionsCachesForBill:(ZPBill *)bill;
- (void)savePromotions:(NSArray *)promotions forBill:(ZPBill *)bill;
- (NSString *)cacheKeyForBill:(ZPBill *)bill;
- (RACSignal *)preloadPromotionsForBill:(ZPBill *)bill;
- (RACSignal *)loadRemotePromotionsForBill:(ZPBill *)bill;
- (void)clearPromotionsCaches;
- (ZPPromotionInfo *)getPromotionUsedExistedInCache:(ZPPromotion *)promotion;

//- (NSMutableArray <ZPPromotion *>*)filterAvailablePromotions:(NSArray *)allVoucher forBill:(ZPBill *)bill;
- (NSArray <ZPPromotion *>*)filterAvailablePromotionsWhenSelectedPaymentMethod:(NSArray *)allPromotions forBill:(ZPBill *)bill;
- (NSArray <ZPPromotion *>*)filterAvailablePromotionsToEnablePaymentMethod:(NSArray *)allPromotions forBill:(ZPBill *)bill;
@end
