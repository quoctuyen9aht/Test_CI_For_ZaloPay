//
//  ZPBill+ExtInfo.m
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/21/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPBill+ExtInfo.h"
#import "ZPDataManager.h"
#import "ZaloPaySDKUtil.h"
#import "ZPSDKAppInfoManager.h"
#import "ZPPaymentChannel.h"
@implementation ZPBill (ExtInfo)

- (NSArray<ZPBillExtraInfo*> *)extFromItem {
    return [ZaloPaySDKUtil extInfoFromString:self.items];
}

- (NSString *)appName {
    if (self.appId == appDichVuId) {
        return [R string_App_DichVu];
    }    
    ZPAppData *currentApp = [[ZPDataManager sharedInstance].appInfoManager appWithId:self.appId];
    if (self.appId == kZaloPayClientAppId ||
        self.appId == withdrawAppId) {
        if (self.transType == ZPTransTypeWalletTopup) {
            return [R string_AddCash_Title];
        }
        if (self.transType == ZPTransTypeWithDraw) {
            return [R string_Withdraw_Title];
        }
        if (self.transType == ZPTransTypeTranfer) {
            return [R string_TransferHome_Title];
        }
        
    }
    return currentApp.appName;
}


@end
