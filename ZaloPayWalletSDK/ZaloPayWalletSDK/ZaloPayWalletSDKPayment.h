//
//  ZPSDK+Payment.h
//  ZPSDK
//
//  Created by ZaloPayTeam on 12/24/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZPDefine.h"
#import "ZPWalletDependenceProtocol.h"
#import "ZaloPayWalletSDKCallBackDelegate.h"
#import "ZPVoucherManager.h"

#import "ZPPromotion.h"
#import "ZPPromotionInfo.h"
#import "ZPPromotionManager.h"

@protocol ZaloPayNetworkProtocol;
@class ZPBill;
@class ZPSavedCard;
@class ZPSavedBankAccount;
@class ZPMiniBank;
@class ZaloMobilePaymentSDK;

typedef void (^MapCardCompletedHanlde)(NSDictionary *result);
typedef void (^ZPSDKNetworkManagerCallback)(NSDictionary * dictionary, int errorCode);

typedef NS_ENUM (NSInteger, ZPKYCType){
    ZPKYCTypeMapCard = 0,
    ZPKYCTypeTransaction,
    ZPKYCTypeOnboarding,
    ZPKYCTypeFullInformation,
    ZPKYCTypeProfile
};

typedef NS_ENUM (NSInteger, ZPGatewayType){
    ZPGatewayTypeNone = 0,
    ZPGatewayTypePayment = 1,
    ZPGatewayTypeLinkCard
};

@protocol KYCFlowDelegateProtocol<NSObject>
- (void)KYCFlowDidCancel;
@end

@interface ZaloPayWalletSDKPayment : NSObject

@property(nonatomic, weak, readwrite) id<ZaloPayWalletSDKCallBackDelegate> paymentDelegate;
@property (nonatomic, copy)  MapCardCompletedHanlde handleMapcard;
@property (nonatomic, strong) ZPVoucherManager *zpVoucherManager;
@property (nonatomic, strong) ZPPromotionManager *zpPromotionManager;
@property (nonatomic, strong) ZaloMobilePaymentSDK *zaloPaySDK;
@property (nonatomic, weak) id<ZPWalletDependenceProtocol> appDependence;
@property (nonatomic, assign) id<ZaloPayNetworkProtocol> network;

+ (instancetype) sharedInstance;

- (ZPBill*)bill;
- (int)userProfileLevel;
//support CC
- (BOOL)isMasterCardEnable;
- (BOOL)isVisaCardEnable;
- (BOOL)isJCBCardEnable;
- (BOOL)isMaxCCSavedCard;

- (NSString*)checkErrorMappCCCardWith:(ZPTransType)transType appId:(int32_t)appId;
- (RACSignal *)getAppInfoWithId:(NSInteger)appID transtypes:(NSArray *)arrTranstype;

- (void)setUserProfileLevel:(int)level;
- (void)setUserPLP:(NSArray*)array;

- (NSArray *) paymentSavedCard;
- (NSArray *) paymentSavedBankAccount;
- (NSArray *)updatepaymentSavedCard:(NSArray *)paymentSavedCard;

- (RACSignal *)removeSavedCard:(ZPSavedCard *)card;
- (RACSignal *)removeBankAccount:(NSString*)bankCustomerId bankAccount:(ZPSavedBankAccount *)bankAccount;
//! Get Image with name
- (UIImage*)getImageWithName:(NSString*)imageName;

//! Get config in platforminfo
- (void)getPlatformInfoConfig:(void (^)(NSDictionary* dictionary))callback;

- (void)getPaymentGatewayInfoCompleteHandle:(void (^) (int errorCode, NSString *mesage))callBack;

- (RACSignal *)getPaymentGatewayInfo;

//! Get config banklist
- (void)getBankListEnabled:(void (^)(NSDictionary* dictionary))callback;

//! Get config banklist gateway
- (RACSignal <NSDictionary *>*)getBankPaymentGateway:(NSInteger)appid;

//! Call this method when received notify reset level from server.
- (void)updateSavedCardList: (dispatch_block_t)callback;

//! Call this method when received notify link account from server.
- (void)updateSavedAccountList:(dispatch_block_t)callback;

//! get bank code from config
- (NSString*)getCcBankCodeFromConfig:(NSString*)ccBankNumber;

//! get cc display order
- (NSNumber*)ccDisplayOrder:(NSString*)key;

//! TCZ10: Handle specific case while lauch ZaloPay from Zalo app but user is curently paying, finished.

- (void)closePaymentAndCleanSDK;
- (BOOL)checkIsCurrentPaymentFinished;
- (BOOL)checkIsCurrentPaying;
- (void)forceClosePaymentSDK;
- (void)dismissSDKKeyboard;
- (void)showSDKKeyboard;
//! Call this method to remove user cache
- (void)logout;

/**
 Initialize SDK with specific app id
 */
- (void)initializeSDK;

- (NSArray <ZPMiniBank *> *)getMiniBanksForMapping;

- (BOOL)chekMinAppVersion:(NSString *)minAppVersion;

- (RACSignal *)requetAppInfo:(NSInteger)appId transtype:(ZPTransType)transtype;

- (void)startHandleBill:(ZPBill *)bill
           withDelegate:(UIViewController<ZaloPayWalletSDKCallBackDelegate> *)delegate
    navigaionController:(UINavigationController *)navi;

- (NSDictionary *)bankDisplayOrder;

- (void)clearPlatformInfoAndResource;
- (void)clearPaymentCache;

- (RACSignal *)registerBankWith:(NSDictionary *)params using:(NSString *)zpTransID;
- (RACSignal *)authCardholderWith:(NSDictionary *)params using:(NSDictionary *)paramsNext;
- (void)showPopupPayment:(UIViewController *)controller
                    with:(ZPBill *)bill
                complete:(void(^)(id object))completionHandler
                   error:(void(^)(NSError *error))errorHandler;

- (RACSignal*)loadCardListAndBankAccount;
- (RACSignal <NSDictionary *>*)runKYCFlow:(UIViewController *)controller title:(NSString *)title infor:(NSDictionary *)infor type:(ZPKYCType)type;

/**
 Run Flow check status by web

 @param bill bill using for transaction
 @param controller using for displa flow
 @param url url 'll  check result
 @param type kind flow web (note: type none doesn't run anything)
 @return signal with json
 */
- (RACSignal <NSDictionary *>*)runGateway:(ZPBill *)bill on:(UIViewController *)controller url:(NSURL *)url type:(ZPGatewayType)type;

- (void)showAlertMaxCCSavedCard;
- (BOOL)canPaymentGateway:(NSInteger)appid;
- (ZPSDK_BankType)bankSupportType:(NSArray*)bankFunctions;
@end



