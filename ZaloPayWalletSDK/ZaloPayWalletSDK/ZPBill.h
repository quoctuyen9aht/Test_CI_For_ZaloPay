//
//  ZPBill.h
//  ZPSDK
//
//  Created by phungnx on 12/21/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZPDefine.h"

@class ZPVoucherInfo;
@class ZPPromotionInfo;
@class ZPPaymentMethod;
/**
 ZPBillDetail is a class that describes bill information,
 include: item list, app tranx ID, mac, bill decription, embeded data.
 */
@interface ZPBill : NSObject <NSCoding>

@property (nonatomic) int32_t appId;
/**
 id of bill
 */
@property (nonatomic, strong) NSString * appTransID;
/**
 signature of bill
 */
@property (nonatomic, strong) NSString * mac;
/**
 list of ZPPaymentID object
 */
@property (nonatomic, strong) NSString* items;
/**
 bill's description.
 */
@property (nonatomic, strong) NSString * billDescription;
/**
 embeded data
 */
@property (nonatomic, strong) NSString * embedData;
/**
 bill's creation time.
 */
@property (assign, nonatomic)  long long time;
/**
 pre - filled amount
 */
@property (assign, nonatomic) long amount;

/**
 username of zalopay wallet
 */
@property (strong, nonatomic) NSString * appUser;

/**
 data of zalopay wallet, use to pay via credit saved card
 */
@property (strong, nonatomic) NSString * data;

/**
 voucher info
 */
@property (strong, nonatomic) ZPVoucherInfo *voucherInfo;
/**
 promotion info
 */
@property (strong, nonatomic) ZPPromotionInfo *promotionInfo;
/**
 campaigncode
 */
@property (strong, nonatomic) NSString * campaigncode;
/**
 appUserInfo
 */
@property (nonatomic, strong) NSDictionary* appUserInfo;

/**
 transtype
 */
@property (assign, nonatomic) ZPTransType transType;

/**
 paymentMethod
 */
@property (strong, nonatomic) ZPPaymentMethod * currentPaymentMethod;

@property (strong, nonatomic) NSString * displayInfo;

@property (strong,nonatomic) NSString *bankCode;

@property (nonatomic) int ordersource;

@property (nonatomic) BOOL doneShowPinViewOrTouchId;

@property (nonatomic) BOOL unsuportCCCredit;
@property (nonatomic) BOOL shouldSkipSuccessView;

@property (nonatomic) NSString * errorCCCanNotMapWhenPay;

@property (assign, nonatomic) NSInteger feeCharge;

/// Using use later
@property (strong, nonatomic) NSDictionary *rawData;

// params is required in flow map or payment using bank account
@property (strong, nonatomic) NSString * bankCustomerId;

/**
 init the bill.
 @param itemList: the list of ZPPayItem objects.
 @param appTransID: id of bill.
 @param time: bill creation time.
 @param description: discribe the bill
 @param embededData: data embedded to bill.
 */
- (id) initWithItems: (NSString*) items andAppTranxID:(NSString *) aTranxID andDescription:(NSString *) description andEmbedData:(NSString *) embededData;
/**
 calculate amount of bill
 */
- (long)finalAmount;

- (long) finalDiscountAmount;

+ (ZPBill *)coppyFrom:(ZPBill *)bill;

- (NSString *)encodeBase64:(NSDictionary *)userInfor;

@end

//! bill dùng cho liên kết tài khoản + xoá tài khoản ngân hàng.
@interface ZPIBankingAccountBill : ZPBill
@property (nonatomic) BOOL isMapAccount;
@end

//! bill extra info
@interface ZPBillExtraInfo: NSObject
@property (nonatomic, strong) NSString *extraKey;
@property (nonatomic, strong) NSString *extraValue;
- (instancetype)initWithExtraKey: (NSString*)extraKey extValue:(NSString *)extraValue;
@end
