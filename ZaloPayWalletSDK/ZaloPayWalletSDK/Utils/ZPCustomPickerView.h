//
//  ZPCustomPickerView.h
//  ibankingHandle
//
//  Created by CPU11695 on 11/8/16.
//  Copyright © 2016 CPU11695. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ZPCustomPickerView;
@protocol ZPCustomPickerViewDelegate <NSObject>
@optional
- (void)pickerView:(ZPCustomPickerView *)pickerView didSelectValue:(NSString *)value;
- (void)pickerView:(ZPCustomPickerView *)pickerView willCloseWithValue:(NSString *)value;
@end
@interface ZPCustomPickerView : UIView
-(void)pickerViewSetUIWithTitle:(NSString *)title andData:(NSArray *)data;
@property (nonatomic, weak) id<ZPCustomPickerViewDelegate> delegate;

@end
