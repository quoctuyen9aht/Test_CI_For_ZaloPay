//
//  ZPProgressHUD.m
//  ZPSDK
//
//  Created by ZaloPayTeam on 12/23/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//
#import "ZPProgressHUD.h"
#import <UIKit/UIKit.h>
#import "ZPResourceManager.h"
#import "UIColor+ZPExtension.h"
#import "DGActivityIndicatorView.h"


enum {
    ZPProgressHUDMaskTypeNone = 1, // allow user interactions while HUD is displayed
    ZPProgressHUDMaskTypeClear, // don't allow
    ZPProgressHUDMaskTypeBlack, // don't allow and dim the UI in the back of the HUD
    ZPProgressHUDMaskTypeGradient // don't allow and dim the UI with a a-la-alert-view bg gradient
};
typedef NSUInteger ZPProgressHUDMaskType;

@interface ZPProgressHUD ()@property (nonatomic, readwrite) ZPProgressHUDMaskType maskType;
@property (nonatomic, assign) BOOL isShow;
@property (nonatomic, strong, readonly) UIView *hudView;
@property (nonatomic, strong, readonly) UILabel *stringLabel;
@property (nonatomic, strong, readonly) UIImageView *imageView;
@property (nonatomic, strong, readonly) DGActivityIndicatorView *spinnerView;
@property (nonatomic, readonly) CGFloat visibleKeyboardHeight;
@end


@implementation ZPProgressHUD

@synthesize hudView, maskType, stringLabel, imageView, spinnerView, visibleKeyboardHeight, isShow;

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (ZPProgressHUD*)sharedView {
    static dispatch_once_t once;
    static ZPProgressHUD *sharedView;
    dispatch_once(&once, ^ { sharedView = [[ZPProgressHUD alloc] initWithFrame:[[UIScreen mainScreen] bounds]]; });
    return sharedView;
}

+ (void)setStatus:(NSString *)string {
    [[ZPProgressHUD sharedView] setStatus:string];
}

#pragma mark - Show Methods

+ (void)show {
    [[ZPProgressHUD sharedView] showWithStatus:nil maskType:ZPProgressHUDMaskTypeNone networkIndicator:NO];
}

+ (void)showWithStatus:(NSString *)status {
    [[ZPProgressHUD sharedView] showWithStatus:status maskType:ZPProgressHUDMaskTypeBlack networkIndicator:NO];
}

+ (void)showWithMaskType:(ZPProgressHUDMaskType)maskType {
    [[ZPProgressHUD sharedView] showWithStatus:nil maskType:maskType networkIndicator:NO];
}

+ (void)showWithStatus:(NSString*)status maskType:(ZPProgressHUDMaskType)maskType {
    [[ZPProgressHUD sharedView] showWithStatus:status maskType:maskType networkIndicator:NO];
}


#pragma mark - Dismiss Methods

+ (void)dismiss {
    [[ZPProgressHUD sharedView] dismiss];
}

#pragma mark - Instance Methods

- (id)initWithFrame:(CGRect)frame {
    
    if ((self = [super initWithFrame:frame])) {
        self.userInteractionEnabled = YES;
        self.backgroundColor = [UIColor clearColor];
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    
    return self;
}

- (void)drawRect:(CGRect)rect {
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    switch (self.maskType) {
            
        case ZPProgressHUDMaskTypeBlack: {
            [[UIColor colorWithWhite:0 alpha:0.01] set];
            CGContextFillRect(context, self.bounds);
            break;
        }
            
        case ZPProgressHUDMaskTypeGradient: {
            
            size_t locationsCount = 2;
            CGFloat locations[2] = {0.0f, 1.0f};
            CGFloat colors[8] = {0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.75f};
            CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
            CGGradientRef gradient = CGGradientCreateWithColorComponents(colorSpace, colors, locations, locationsCount);
            CGColorSpaceRelease(colorSpace);
            
            CGPoint center = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
            float radius = MIN(self.bounds.size.width , self.bounds.size.height) ;
            CGContextDrawRadialGradient (context, gradient, center, 0, center, radius, kCGGradientDrawsAfterEndLocation);
            CGGradientRelease(gradient);
            
            break;
        }
    }
}

- (void)setStatus:(NSString *)string {
    if([string isEqualToString:@""]) {
        CGFloat hudWidth = SCREEN_WIDTH/2.5;
        CGFloat hudHeight = SCREEN_WIDTH/3;
        self.hudView.bounds = CGRectMake(0, 0, hudWidth, hudHeight);
        self.imageView.center = CGPointMake(CGRectGetWidth(self.hudView.bounds)/2, ceil(self.hudView.bounds.size.height/2) -12);
        self.spinnerView.center = CGPointMake(ceil(CGRectGetWidth(self.hudView.bounds)/2)+0.5, ceil(self.hudView.bounds.size.height/2) + 25);
    } else {
        CGFloat hudWidth = 100;
        CGFloat hudHeight = 100;
        CGFloat stringWidth = 0;
        CGFloat stringHeight = 0;
        CGRect labelRect = CGRectZero;
        
        if(string) {
            CGRect textRect = [string boundingRectWithSize:CGSizeMake(200, 300)
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                              attributes:@{NSFontAttributeName:self.stringLabel.font}
                                                 context:nil];
            CGSize stringSize = textRect.size;
            
            stringWidth = stringSize.width;
            stringHeight = stringSize.height;
            hudHeight = 80+stringHeight;
            
            if(stringWidth > hudWidth)
                hudWidth = ceil(stringWidth/2)*2;
            
            if(hudHeight > 100) {
                labelRect = CGRectMake(12, 66, hudWidth, stringHeight);
                hudWidth+=24;
            } else {
                hudWidth+=24;
                labelRect = CGRectMake(0, 66, hudWidth, stringHeight);
            }
        }
        
        self.hudView.bounds = CGRectMake(0, 0, hudWidth, hudHeight);
        
        if(string)
            self.imageView.center = CGPointMake(CGRectGetWidth(self.hudView.bounds)/2, 36);
        else
            self.imageView.center = CGPointMake(CGRectGetWidth(self.hudView.bounds)/2, CGRectGetHeight(self.hudView.bounds)/2);
        
        self.stringLabel.hidden = NO;
        self.stringLabel.text = string;
        self.stringLabel.frame = labelRect;
        
        if(string)
            self.spinnerView.center = CGPointMake(ceil(CGRectGetWidth(self.hudView.bounds)/2)+0.5, 40.5);
        else
            self.spinnerView.center = CGPointMake(ceil(CGRectGetWidth(self.hudView.bounds)/2)+0.5, ceil(self.hudView.bounds.size.height/2)+0.5);
    }
}

- (void)registerNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(positionHUD:)
                                                 name:UIApplicationDidChangeStatusBarOrientationNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(positionHUD:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(positionHUD:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(positionHUD:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(positionHUD:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
}

- (void)positionHUD:(NSNotification*)notification {
    
    CGFloat keyboardHeight;
    double animationDuration = 0.0;
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    if(notification) {
        NSDictionary* keyboardInfo = [notification userInfo];
        CGRect keyboardFrame = [[keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
        animationDuration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
        
        if(notification.name == UIKeyboardWillShowNotification || notification.name == UIKeyboardDidShowNotification) {
            if(UIInterfaceOrientationIsPortrait(orientation))
                keyboardHeight = keyboardFrame.size.height;
            else
                keyboardHeight = keyboardFrame.size.width;
        } else
            keyboardHeight = 0;
    } else {
        keyboardHeight = self.visibleKeyboardHeight;
    }
    
    CGRect orientationFrame = [UIScreen mainScreen].bounds;
    CGRect statusBarFrame = [UIApplication sharedApplication].statusBarFrame;
    CGFloat activeHeight = orientationFrame.size.height;
    
    if(keyboardHeight > 0)
        activeHeight += statusBarFrame.size.height*2;
    
    activeHeight -= keyboardHeight;
    CGFloat posY = floor(activeHeight*0.5);
    CGFloat posX = orientationFrame.size.width/2;
    
    CGPoint newCenter;
    CGFloat rotateAngle = 0.0;
    
    switch (orientation) {
        case UIInterfaceOrientationPortraitUpsideDown:
            rotateAngle = M_PI;
            newCenter = CGPointMake(posX, posY);
            break;
        case UIInterfaceOrientationLandscapeLeft:
            rotateAngle = -M_PI/2.0f;
            newCenter = CGPointMake(posY, posX);
            [[ZPProgressHUD sharedView] setFrame:CGRectMake(0, 0, orientationFrame.size.height, orientationFrame.size.width)];
            break;
        case UIInterfaceOrientationLandscapeRight:
            rotateAngle = M_PI/2.0f;
            newCenter = CGPointMake(posY, posX);
            [[ZPProgressHUD sharedView] setFrame:CGRectMake(0, 0, orientationFrame.size.height, orientationFrame.size.width)];
            break;
        default: // as UIInterfaceOrientationPortrait
            rotateAngle = 0.0;
            newCenter = CGPointMake(posX, posY);
            break;
    }
    
    if(notification) {
        [UIView animateWithDuration:animationDuration
                              delay:0
                            options:UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             [self moveToPoint:newCenter rotateAngle:rotateAngle];
                         } completion:NULL];
    }
    
    else {
        [self moveToPoint:newCenter rotateAngle:rotateAngle];
    }
}

- (void)moveToPoint:(CGPoint)newCenter rotateAngle:(CGFloat)angle {
    self.hudView.center = newCenter;
}

#pragma mark - Master show/dismiss methods

- (void)showWithStatus:(NSString*)string maskType:(ZPProgressHUDMaskType)hudMaskType networkIndicator:(BOOL)show {
    id<UIApplicationDelegate>app = [[UIApplication sharedApplication] delegate];    
    if (app.window == nil || app.window.hidden || app.window.rootViewController == nil) {
        return;
    }
    if ([self isVisible] || self.superview) {
        return;
    }
    [app.window addSubview:self];
    self.imageView.hidden = NO;
    self.maskType = hudMaskType;
    self.imageView.image = [UIImage imageNamed:@"ico_logozalopay"] ? [UIImage imageNamed:@"ico_logozalopay"] : [ZPResourceManager getImageWithName:@"ico_logozalopay"];
    [self setStatus:@""];
    [self.spinnerView startAnimating];
    [self positionHUD:nil];
    isShow = YES;
    if(self.alpha != 1) {
        [self registerNotifications];
        [UIView animateWithDuration:0.15
                              delay:0
                            options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationCurveEaseOut | UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             self.alpha = 1;
                         }
                         completion:NULL];
    }
    
    [self setNeedsDisplay];
}

- (void)dismiss {
    if ([self isVisible] == false) {
        return;
    }
    [self.spinnerView stopAnimating];
    self.alpha = 0;
    isShow = NO;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self removeFromSuperview];
}

#pragma mark - Utilities

+ (BOOL)isVisible {
    return ([[ZPProgressHUD sharedView] isVisible]);
}

#pragma mark - Getters

- (BOOL)isVisible {
    return isShow;
}

- (UIView *)hudView {
    if(!hudView) {
        hudView = [[UIView alloc] initWithFrame:CGRectZero];
        hudView.layer.cornerRadius = 8;
        hudView.backgroundColor = [UIColor zpColorFromHexString:@"#77777A"];
        hudView.alpha = 0.9;
        hudView.autoresizingMask = (UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin |
                                    UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin);
        [self addSubview:hudView];
    }
    return hudView;
}

- (UILabel *)stringLabel {
    if (stringLabel == nil) {
        stringLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        stringLabel.textColor = [UIColor whiteColor];
        stringLabel.backgroundColor = [UIColor clearColor];
        stringLabel.adjustsFontSizeToFitWidth = YES;
        stringLabel.textAlignment = NSTextAlignmentCenter;
        stringLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
        stringLabel.font = [UIFont boldSystemFontOfSize:16];
        stringLabel.shadowColor = [UIColor blackColor];
        stringLabel.shadowOffset = CGSizeMake(0, -1);
        stringLabel.numberOfLines = 0;
    }
    if(!stringLabel.superview)
        [self.hudView addSubview:stringLabel];
    
    return stringLabel;
}

- (UIImageView *)imageView {
    if (imageView == nil) {
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 84, 24)];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.hudView addSubview:imageView];
    }
    return imageView;
}

- (DGActivityIndicatorView *)spinnerView {
    if (spinnerView == nil) {
        spinnerView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor whiteColor]];
        spinnerView.bounds = CGRectMake(0, 0, 36, 10);
        [self.hudView addSubview:spinnerView];
    }
    return spinnerView;
}

- (CGFloat)visibleKeyboardHeight {
    
    UIWindow *keyboardWindow = nil;
    for (UIWindow *testWindow in [[UIApplication sharedApplication] windows]) {
        if(![[testWindow class] isEqual:[UIWindow class]]) {
            keyboardWindow = testWindow;
            break;
        }
    }
    
    // Locate UIKeyboard.
    UIView *foundKeyboard = nil;
    for (__strong UIView *possibleKeyboard in [keyboardWindow subviews]) {
        
        // iOS 4 sticks the UIKeyboard inside a UIPeripheralHostView.
        if ([[possibleKeyboard description] hasPrefix:@"<UIPeripheralHostView"]) {
            possibleKeyboard = [[possibleKeyboard subviews] objectAtIndex:0];
        }
        
        if ([[possibleKeyboard description] hasPrefix:@"<UIKeyboard"]) {
            foundKeyboard = possibleKeyboard;
            break;
        }
    }
    
    if(foundKeyboard && foundKeyboard.bounds.size.height > 100)
        return foundKeyboard.bounds.size.height;
    
    return 0;
}

@end
