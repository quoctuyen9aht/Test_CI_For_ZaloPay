//
//  ZPCustomPickerView.m
//  ibankingHandle
//
//  Created by CPU11695 on 11/8/16.
//  Copyright © 2016 CPU11695. All rights reserved.
//

#import "ZPCustomPickerView.h"
#import "UIColor+ZPExtension.h"
#define PICKER_VIEW [ZPCustomPickerView sharedPickerView]
@interface ZPCustomPickerView()<UIPickerViewDelegate, UIPickerViewDataSource>{
    NSMutableArray *listItem;
    NSInteger currentIndex;
    NSInteger oldIndex;
}
@property (retain, nonatomic) UILabel *lblTitle;
@property (retain, nonatomic) UIPickerView *myPickerView;
@end

@implementation ZPCustomPickerView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubsView];
    }
    return self;
}
-(void)layoutSubviews{
  //  DDLogInfo(@"");
    self.myPickerView.frame = CGRectMake(0, 40, self.frame.size.width, self.frame.size.height-40);
}
-(void)initSubsView{
    //
    CGFloat topHeight = 40;
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    UIFont *btfont = [UIFont systemFontOfSize:15];
    UIColor *btColor = [UIColor blueColor];
    UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenSize.width, topHeight)];
    topView.backgroundColor = [UIColor zpColorFromHexString:@"#EFEFEF"];
    UIButton *btAccept = [[UIButton alloc]initWithFrame:CGRectMake(screenSize.width - 50, 0, topHeight, topHeight)];
    [btAccept setTitle:@"Xong" forState:UIControlStateNormal];
    [btAccept addTarget:self action:@selector(didTouchDone:) forControlEvents:UIControlEventTouchUpInside];
    [btAccept setTitleColor:btColor forState:UIControlStateNormal];
     btAccept.titleLabel.font = btfont;
    [topView addSubview:btAccept];
    UIButton *btCancel = [[UIButton alloc]initWithFrame:CGRectMake(10, 0, topHeight, topHeight)];
    [btCancel setTitle:@"Huỷ" forState:UIControlStateNormal];
    [btCancel addTarget:self action:@selector(didTouchCancel:) forControlEvents:UIControlEventTouchUpInside];
    [btCancel setTitleColor:btColor forState:UIControlStateNormal];
    btCancel.titleLabel.font = btfont;
    [topView addSubview:btCancel];
    self.lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(50, 0, screenSize.width - 100, topHeight)];
    self.lblTitle.textAlignment = NSTextAlignmentCenter;
    [topView addSubview:self.lblTitle];
    [self addSubview:topView];
    self.myPickerView = [[UIPickerView alloc]init];
    self.myPickerView.frame = CGRectMake(0, 40, self.frame.size.width, self.frame.size.height-40);
    self.myPickerView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.myPickerView];
}
-(void)setFrame:(CGRect)frame{
    
}
-(void)pickerViewSetUIWithTitle:(NSString *)title andData:(NSArray *)data{
    currentIndex = 0;
    oldIndex = 0;
    self.lblTitle.text = title;
 //   [self.lblTitle sizeToFit];
    listItem = [[NSMutableArray alloc]initWithArray:data];
    // test data
//    [listItem addObject:@"0251001810964"];
//    [listItem addObject:@"0251001810965"];
//    [listItem addObject:@"0251001810966"];
//    [listItem addObject:@"0251001810967"];
//    [listItem addObject:@"0251001810968"];
//    [listItem addObject:@"0251001810969"];
    
    self.myPickerView.dataSource = self;
    self.myPickerView.delegate = self;
    [self.myPickerView reloadAllComponents];
}
- (IBAction)didTouchDone:(id)sender {
    oldIndex = currentIndex;
    if (self.delegate &&[self.delegate respondsToSelector:@selector(pickerView:didSelectValue:)]) {
        [self.delegate pickerView:self willCloseWithValue:listItem[currentIndex]];
    }
}
- (IBAction)didTouchCancel:(id)sender {

    if (self.delegate &&[self.delegate respondsToSelector:@selector(pickerView:didSelectValue:)]) {
        [self.delegate pickerView:self willCloseWithValue:listItem[oldIndex]];
        [self.myPickerView selectRow:oldIndex inComponent:0 animated:YES];
    }
}
#pragma mark - UIPickerViewDelegate
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{

    return listItem.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return listItem[row];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    currentIndex = row;
    if (self.delegate &&[self.delegate respondsToSelector:@selector(pickerView:didSelectValue:)]) {
        [self.delegate pickerView:self didSelectValue:listItem[row]];
    }
}
-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 40;
}
@end
