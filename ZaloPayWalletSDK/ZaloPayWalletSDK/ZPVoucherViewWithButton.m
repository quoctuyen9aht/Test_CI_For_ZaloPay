//
//  ZPVoucherViewWithButton.m
//  ZaloPayWalletSDK
//
//  Created by Phuoc Huynh on 9/12/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//
#define heightViewPayWithVouchersListBase [UIView isIPhoneX] ? 78 : 68
#define heightViewPayWithVouchersListHaveVoucher 130
#define heightViewPayWithVouchersListExpendMax 400
#define heightViewExpendVoucherList       64
#define delayIntervalForAnimationExpendVoucherView      0.3
#import "ZPVoucherViewWithButton.h"
#import "ZPPaymentHandler.h"
#import "ZPPaymentManager.h"
#import "ZPContainerViewController+Voucher.h"
#import "ZPTableViewVoucherHistories.h"
#import "ZaloPayWalletSDKPayment.h"
#import "ZPDefine.h"
#import "ZPMethodsHandler.h"
#import <ZaloPayWalletSDK/ZaloPayWalletSDK-Swift.h>
#import <ZaloPayNetwork/NSError+ZaloPayAPI.h>
#import "ZPVoucherInfo.h"
#import "ZPPaymentChannel.h"
#import "ZPPaymentMethod.h"
#import <ZaloPayAnalyticsSwift/ZaloPayAnalyticsSwift-Swift.h>
#import "ZPEndowUtils.h"

@interface ZPVoucherViewWithButton ()
@property (nonatomic,strong) NSAttributedString *attributedStringValueDiscount;
@property (nonatomic,strong) NSString *stringDescriptionDiscount;

@end

@implementation ZPVoucherViewWithButton
- (instancetype)initWithBill:(ZPBill *)bill fromController:(ZPContainerViewController *)controller{
    self = [super init];
    if (self) {
        self.bill = bill;
        self.currentVC = controller;
        [self setupView];
        self.countSelectPaymentMethod = 0;
    }
    return self;
}
- (void)addMaskView {
    self.maskView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [[UIApplication sharedApplication].delegate.window addSubview:self.maskView];
    [self.maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(0);
        make.right.equalTo(0);
        make.top.equalTo(0);
        make.bottom.equalTo(0);
    }];
    self.maskView.backgroundColor = [UIColor darkGrayColor];
    self.maskView.alpha = 0.0;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(tapViewExpendVoucherList)];
    [self.maskView addGestureRecognizer:singleFingerTap];
    
}
-(void)setupView {
    self.isExpendVouchersList = NO;
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:button];
    [button setupZaloPayButton];
    int bottom = [UIView isIPhoneX] ? 20 : 10;
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(-bottom);
        make.left.equalTo(10);
        make.right.equalTo(-10);
        make.height.equalTo(kZaloButtonHeight);
    }];
    [button addTarget:self action:@selector(touchActionButton) forControlEvents:UIControlEventTouchUpInside];
    self.actionButton = button;
    
    self.viewExpendVouchersList = [[UIView alloc] init];
    [self addSubview:self.viewExpendVouchersList];
    self.viewExpendVouchersList.backgroundColor = [UIColor whiteColor];
    [self.viewExpendVouchersList roundRect:3 borderColor:[UIColor lineColor] borderWidth:1];
    
    [self.viewExpendVouchersList mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.width.equalTo(UIScreen.mainScreen.bounds.size.width);
        make.centerX.equalTo(0);
        make.height.equalTo(heightViewExpendVoucherList);
    }];
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(tapViewExpendVoucherList)];
    [self.viewExpendVouchersList addGestureRecognizer:singleFingerTap];
    
    
    self.viewError = [[UIView alloc] init];
    [self addSubview:self.viewError];
    self.viewError.backgroundColor = [UIColor hex_0xffffc1];
    
    self.labelError = [[UILabel alloc] init];
    [self.viewError addSubview:self.labelError];
    self.labelError.font = [UIFont SFUITextRegularMedium];
    self.labelError.textColor = [UIColor blackColor];
    
    [self.viewError mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.viewExpendVouchersList.mas_bottom);
        make.left.equalTo(0);
        make.right.equalTo(0);
        make.height.equalTo(0);
    }];
    
    [self.viewError addSubview:self.lineGray];
    self.lineGray.backgroundColor = [UIColor lineColor];
    [self.lineGray mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.left.equalTo(0);
        make.right.equalTo(0);
        make.height.equalTo(self.viewError.mas_height).multipliedBy(0.025);
    }];
    
    
    [self.labelError mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(self.viewError.mas_height).multipliedBy(0.5);
        make.centerY.equalTo(self.viewError);
        make.left.equalTo(12);
        make.right.equalTo(-12);
    }];
    
    
    self.imageViewGift = [[ZPIconFontImageView alloc] init];
    [self.viewExpendVouchersList addSubview:self.imageViewGift];
    [self.imageViewGift setIconFont:@"gift_box"];
    [self.imageViewGift setIconColor:[UIColor zp_red]];
    self.imageViewGift.defaultView.font = [UIFont iconFontWithSize:30];
    
    self.lableHaveGift = [[UILabel alloc] init];
    [self.viewExpendVouchersList addSubview:self.lableHaveGift];
    self.lableHaveGift.text = [R string_Voucher_have_gift_text];
    self.lableHaveGift.font = [UIFont SFUITextRegularMedium];
    self.lableHaveGift.textColor = [UIColor subText];
    self.lableHaveGift.backgroundColor = [UIColor whiteColor];
    
    self.imageArrowUp = [[ZPIconFontImageView alloc] init];
    [self.viewExpendVouchersList addSubview:self.imageArrowUp];
    
    [self.imageArrowUp setIconFont:@"gift_arrowup"];
    [self.imageArrowUp setIconColor:[UIColor hex_0xc7c7cc]];
    self.imageArrowUp.defaultView.font = [UIFont iconFontWithSize:15];
    
    self.labelSelectVouchers = [[UILabel alloc] init];
    [self.viewExpendVouchersList addSubview:self.labelSelectVouchers];
    self.labelSelectVouchers.text = [R string_Voucher_select_text];
    self.labelSelectVouchers.textColor = [UIColor zaloBaseColor];
    self.labelSelectVouchers.font = [UIFont SFUITextRegularMedium];
    
    self.tbvVouchersList = [[ZPTableViewVoucherHistories alloc] init];
    [self.tbvVouchersList setupBill:self.bill];
    self.tbvVouchersList.zpTableViewVoucherHistoriesDelegate = self;
    [self addSubview:self.tbvVouchersList];
    
    self.labelValueDiscount = [[UILabel alloc] init];
    [self.viewExpendVouchersList addSubview:self.labelValueDiscount];
    self.labelValueDiscount.font = [UIFont SFUITextMediumWithSize:16];
    self.labelValueDiscount.textAlignment = NSTextAlignmentLeft;
    
    self.labelDescriptionDiscount = [[UILabel alloc] init];
    [self.viewExpendVouchersList addSubview:self.labelDescriptionDiscount];
    self.labelDescriptionDiscount.font = [UIFont SFUITextRegularWithSize:12];
    self.labelDescriptionDiscount.textColor = [UIColor subText];
    self.labelDescriptionDiscount.textAlignment = NSTextAlignmentLeft;
    
    self.lineGray = [[UIView alloc] init];
    self.lineViewGreen = [[UIView alloc] init];
    
    [self addMaskView];
    [self updateUIPayWithVoucherResult:self.bill.voucherInfo andVoucherUsed:self.bill.voucherInfo.voucherUsed];
    [self updateUIPayWithPromotionResult:self.bill.promotionInfo andPromotionUsed:self.bill.promotionInfo.promotionUsed];
}
- (void) updateUIPayWithVoucherResult:(ZPVoucherInfo *)result andVoucherUsed:(ZPVoucherHistory *)voucher  {
    if (voucher) {
        voucher.isSelected = YES;
    }

    self.lableHaveGift.hidden = self.isExpendVouchersList || result != nil;
    self.labelValueDiscount.hidden = !self.lableHaveGift.hidden;
    self.labelDescriptionDiscount.hidden = !self.lableHaveGift.hidden;
    
    if (result != nil) {
        NSString *moneyTitle = [@(result.discountAmount) formatCurrency];
        NSString *mixValueString = [NSString stringWithFormat:@"%@ - %@",voucher.valueDisplayTableViewCell, moneyTitle];
        
        NSString *finalString = voucher.valuetype != ZPVoucherValueTypePercent ? moneyTitle : mixValueString;
        NSAttributedString *attStringDisplay = [finalString formatCurrencyFont:[UIFont SFUITextMediumWithSize:16]
                                                                         color:[UIColor defaultText]
                                                                       vndFont:[UIFont SFUITextRegularWithSize:12]
                                                                      vndcolor:[UIColor blackColor]
                                                                     alignment:NSTextAlignmentLeft];
        self.attributedStringValueDiscount = attStringDisplay;
        self.stringDescriptionDiscount = voucher.voucherHistoryDescription;
        self.labelValueDiscount.attributedText = attStringDisplay;
        self.labelDescriptionDiscount.text = voucher.voucherHistoryDescription;
        [self.currentVC updateZPOrderHeaderViewWithVoucherInfo:result];
    }else {
        self.labelValueDiscount.text = [R string_Voucher_select_gift_text];
        self.labelDescriptionDiscount.text = [R string_Endow_Situation_Text];
    }
}
- (void) updateUIPayWithPromotionResult:(ZPPromotionInfo *)promotionInfo andPromotionUsed:(ZPPromotion *)promotion  {
    if (promotion) {
        promotion.isSelected = YES;
    }
    self.lableHaveGift.hidden = self.isExpendVouchersList || promotionInfo != nil;
    self.labelValueDiscount.hidden = !self.lableHaveGift.hidden;
    self.labelDescriptionDiscount.hidden = !self.lableHaveGift.hidden;
    
    if (promotionInfo != nil) {
        NSString *moneyTitle = [@(promotionInfo.discountamount) formatCurrency];
        NSString *mixValueString = [NSString stringWithFormat:@"%@ - %@",promotion.valueDisplayTableViewCell, moneyTitle];
        
        NSString *finalString = promotion.discounttype != ZPVoucherValueTypePercent ? moneyTitle : mixValueString;
        NSAttributedString *attStringDisplay = [finalString formatCurrencyFont:[UIFont SFUITextMediumWithSize:16]
                                                                         color:[UIColor defaultText]
                                                                       vndFont:[UIFont SFUITextRegularWithSize:12]
                                                                      vndcolor:[UIColor blackColor]
                                                                     alignment:NSTextAlignmentLeft];
        
        self.attributedStringValueDiscount = attStringDisplay;
        self.stringDescriptionDiscount = promotion.promotionDescription;
        self.labelValueDiscount.attributedText = attStringDisplay;
        self.labelDescriptionDiscount.text = promotion.promotionDescription;
        [self.currentVC updateZPOrderHeaderViewWithPromotionInfo:promotionInfo];
    }else {
        self.labelValueDiscount.text = [R string_Voucher_select_gift_text];
        self.labelDescriptionDiscount.text = [R string_Endow_Situation_Text];
    }
}


- (void) resetMaskView {
    if (self.isExpendVouchersList) {
        [self tapViewExpendVoucherList];
    }
}

- (void)touchActionButton {
    [self resetMaskView];
}

- (void)tapViewExpendVoucherList {
    self.tbvVouchersList.layer.borderWidth = 1;
    float heightActualViewPayWithVouchersList = 12 * 2 + kZaloButtonHeight + self.tbvVouchersList.contentSize.height + heightViewExpendVoucherList;
    float heightViewPayWithVouchersList = MIN(heightActualViewPayWithVouchersList, heightViewPayWithVouchersListExpendMax) ;
    
    if (self.isExpendVouchersList) {
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(heightViewPayWithVouchersListHaveVoucher);
        }];
        [self.viewExpendVouchersList mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(10);
            make.width.equalTo(UIScreen.mainScreen.bounds.size.width - 20);
        }];
        [self.tbvVouchersList mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.actionButton.mas_top).offset(0);
            if (heightActualViewPayWithVouchersList < heightViewPayWithVouchersListExpendMax) {
                make.height.equalTo(0);
                
            }
        }];
        [self.maskView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(0);
        }];
        [self.viewError mas_updateConstraints:^(MASConstraintMaker *make) {

            make.height.equalTo(0);
        }];
        [self.viewExpendVouchersList roundRect:3 borderColor:[UIColor lineColor] borderWidth:1];
        self.lableHaveGift.hidden = !self.isExpendVouchersList || (self.bill.promotionInfo || self.bill.voucherInfo);
        self.labelValueDiscount.hidden = !self.lableHaveGift.hidden;
        self.labelDescriptionDiscount.hidden = !self.lableHaveGift.hidden;
        if (!self.bill.promotionInfo && !self.bill.voucherInfo) {
            self.lableHaveGift.text = [R string_Voucher_have_gift_text];
        }else {
            self.labelValueDiscount.attributedText = self.attributedStringValueDiscount;
            self.labelDescriptionDiscount.text = self.stringDescriptionDiscount;
        }
    }else {
        [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionSdk_voucher];
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(heightViewPayWithVouchersList);
        }];
        [self.maskView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(-heightViewPayWithVouchersList);
        }];
        [self.tbvVouchersList mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.actionButton.mas_top).offset(-10);
            if (heightActualViewPayWithVouchersList < heightViewPayWithVouchersListExpendMax) {
                make.height.equalTo(self.tbvVouchersList.contentSize.height);
            }
        }];
        [self.viewExpendVouchersList mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(0);
            make.width.equalTo(UIScreen.mainScreen.bounds.size.width);
        }];
        self.lineViewGreen.hidden = !self.isExpendVouchersList;
        self.lableHaveGift.hidden = !self.isExpendVouchersList;
        self.labelValueDiscount.hidden = !self.lableHaveGift.hidden;
        self.labelDescriptionDiscount.hidden = !self.lableHaveGift.hidden;
        if (!self.bill.promotionInfo && !self.bill.voucherInfo) {
            self.labelValueDiscount.text = [R string_Voucher_select_gift_text];
            self.labelDescriptionDiscount.text = [R string_Endow_Situation_Text];
        }else {
            self.labelValueDiscount.attributedText = self.attributedStringValueDiscount;
            self.labelDescriptionDiscount.text = self.stringDescriptionDiscount;
        }
        //self.lableHaveGift.text = [R string_Voucher_select_gift_text];
        
        [self.viewExpendVouchersList roundRect:0 borderColor:[UIColor whiteColor] borderWidth:0];
    }
    
    self.labelSelectVouchers.hidden = !self.isExpendVouchersList;
    float angel = self.isExpendVouchersList ? 2 * M_PI : -M_PI;
    
    [[UIApplication sharedApplication].delegate.window  needsUpdateConstraints];
    [UIWindow animateWithDuration:delayIntervalForAnimationExpendVoucherView animations:^{
        [[UIApplication sharedApplication].delegate.window layoutIfNeeded];
        self.imageArrowUp.transform = CGAffineTransformMakeRotation(angel);
        self.maskView.alpha = self.isExpendVouchersList ? 0.0 : 0.7;
        self.actionButton.enabled = self.isExpendVouchersList;
    }completion:^(BOOL finished) {
        if (finished) {
            self.lineViewGreen.backgroundColor = self.currentVC.controller.selectedIndex < 0 ? [UIColor disableButtonColor] : [UIColor colorWithHexValue:0x06be04];
            self.lineViewGreen.hidden = !self.isExpendVouchersList;
            self.isExpendVouchersList = !self.isExpendVouchersList;
        }
    }];
}

- (void)updateUIWhenHaveVouchersOrPromotions {
    
    if (self.bill.transType != ZPTransTypeBillPay) {
        return;
    }
    
    if (self.currentVC.paymentMethods.count == 1) {
        return;
    }
    
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(heightViewPayWithVouchersListHaveVoucher);
    }];
//    [self.viewExpendVouchersList mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(0);
//        make.width.equalTo(UIScreen.mainScreen.bounds.size.width);
//        make.centerX.equalTo(0);
//        make.height.equalTo(heightViewExpendVoucherList);
//    }];
    
//    [self.viewError mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.viewExpendVouchersList.mas_bottom);
//        make.left.equalTo(0);
//        make.right.equalTo(0);
//        make.height.equalTo(0);
//    }];
//
//    [self.viewError addSubview:self.lineGray];
//    self.lineGray.backgroundColor = [UIColor lineColor];
//    [self.lineGray mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(0);
//        make.left.equalTo(0);
//        make.right.equalTo(0);
//        make.height.equalTo(self.viewError.mas_height).multipliedBy(0.025);
//    }];
//
//
//    [self.labelError mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.height.equalTo(self.viewError.mas_height).multipliedBy(0.5);
//        make.centerY.equalTo(self.viewError);
//        make.left.equalTo(12);
//        make.right.equalTo(-12);
//    }];
    
    [self.imageViewGift mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.viewExpendVouchersList);
        make.left.equalTo(12);
        make.width.equalTo(30);
        make.height.equalTo(30);
    }];
    [self.lableHaveGift mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.viewExpendVouchersList);
        make.left.equalTo(self.imageViewGift.mas_right).offset(8);
        make.height.equalTo(21);
    }];
    [self.labelSelectVouchers mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.viewExpendVouchersList);
        make.right.equalTo(self.imageArrowUp.mas_left).offset(-13);
        make.height.equalTo(21);
    }];
    [self.imageArrowUp mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(0);
        make.right.equalTo(-15);
        make.height.equalTo(8);
        make.width.equalTo(15);
    }];
    [self.tbvVouchersList mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(0);
        make.right.equalTo(0);
        make.top.equalTo(self.viewError.mas_bottom);
        make.bottom.equalTo(self.actionButton.mas_top).offset(0);
    }];
    
    [self.labelValueDiscount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(10);
        make.left.equalTo(self.imageViewGift.mas_right).offset(10);
        make.right.equalTo(-84);
        make.height.equalTo(20);
    }];
    [self.labelDescriptionDiscount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(-10);
        make.left.equalTo(self.imageViewGift.mas_right).offset(10);
        make.right.equalTo(self.labelValueDiscount.mas_right);
        make.height.equalTo(14);
    }];
    self.currentVC.zpw_tableMain.contentInset = UIEdgeInsetsMake(0, 0, heightViewExpendVoucherList, 0);
    
    
    self.lineViewGreen.backgroundColor = self.currentVC.controller.selectedIndex < 0 ? [UIColor disableButtonColor] : [UIColor colorWithHexValue:0x06be04];
    [self addSubview:self.lineViewGreen];
    [self.lineViewGreen mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.viewExpendVouchersList.mas_bottom).offset(-2);
        make.left.equalTo(self.viewExpendVouchersList.mas_left).offset(0);
        make.right.equalTo(self.viewExpendVouchersList.mas_right).offset(0);
        make.height.equalTo(4);
    }];
    

}

- (RACSignal *)didSelectRowVoucherSignal:(ZPVoucherHistory *)voucher fromAutoSelect:(BOOL)isAutoSelect {
    if (voucher.isDisable) {
        return nil;
    }
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        ZPVoucherInfo *voucherInfoExisted = [[ZaloPayWalletSDKPayment sharedInstance].zpVoucherManager getVoucherUsedExistedInCache:voucher];
        if (voucherInfoExisted) {
            self.bill.promotionInfo = nil;
            [self updateUIPayWithVoucherResult:voucherInfoExisted andVoucherUsed:voucher];
            if (!isAutoSelect) {
                [self tapViewExpendVoucherList];
            }
            [self.tbvVouchersList reloadData];
            self.labelError.text = @"";
            self.bill.voucherInfo.voucherUsed = voucher;
            [subscriber sendCompleted];
            return nil;
        }
        
        [[ZPAppFactory sharedInstance] showHUDAddedTo:self.currentVC.view];
        @weakify(self);
        [[[ZPPaymentManager submitVoucherWith:self.bill
                               voucherHistory:voucher
           ] deliverOnMainThread]  subscribeNext:^(NSDictionary *result) {
            @strongify(self);
            [[ZPAppFactory sharedInstance] hideAllHUDsForView:self.currentVC.view];
            self.bill.promotionInfo = nil;
            ZPVoucherInfo *info = [ZPVoucherInfo fromDic:result];
            info.voucherCode = voucher.vouchercode;
            [self updateUIPayWithVoucherResult:info andVoucherUsed:voucher];
            if (!isAutoSelect) {
                [self tapViewExpendVoucherList];
            }
            [self.viewExpendVouchersList mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(10);
                make.width.equalTo(UIScreen.mainScreen.bounds.size.width - 20);
            }];
            self.lineViewGreen.hidden = !self.isExpendVouchersList;
            [self.tbvVouchersList reloadData];
            self.labelError.text = @"";
            voucher.uid = [NSString stringWithFormat:@"%@%@",voucher.uid,self.bill.currentPaymentMethod.paymenMethodId];
            self.bill.voucherInfo.voucherUsed = voucher;
            [[ZaloPayWalletSDKPayment sharedInstance].zpVoucherManager arrVouchersWillRevertAddVoucher:info];
            [subscriber sendCompleted];
            
        } error:^(NSError *error) {
            @strongify(self);
            [[ZPAppFactory sharedInstance] hideAllHUDsForView:self.currentVC.view];
            if (self.viewError.frame.size.height == 0 && !isAutoSelect) {
                [self.viewError mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.bottom.equalTo(self.tbvVouchersList.mas_top);
                    make.height.equalTo(40);
                }];
                [self mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.height.equalTo(self.frame.size.height + 40);
                }];
                [self.maskView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.bottom.equalTo(-self.frame.size.height - 40);
                }];
            }
            self.tbvVouchersList.layer.borderWidth = 0;
            self.labelError.text = [error apiErrorMessage];
            [self updateUIPayWithVoucherResult:self.bill.voucherInfo andVoucherUsed:self.bill.voucherInfo.voucherUsed];
            voucher.isDisable = YES;
            [self.tbvVouchersList reloadData];
            [subscriber sendCompleted];
        }];
        return nil;
    }];
}

- (void)didSelectRowVoucher:(ZPVoucherHistory *)voucher atIndexPath:(NSIndexPath *)indexPath {
    if (voucher.isSelected) {
        voucher.isSelected = NO;
        [self.tbvVouchersList reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.currentVC deleteCurrentVoucherAndPromotionWith:self.bill];
        [self updateUIPayWithVoucherResult:self.bill.voucherInfo andVoucherUsed:self.bill.voucherInfo.voucherUsed];
    } else {
        [self.tbvVouchersList resetSelectedStateAllVouchersAndPromotions];
        [[self didSelectRowVoucherSignal:voucher fromAutoSelect:NO] subscribeCompleted:^{}];
    }
    
}
- (void)didSelectRowPromotion:(ZPPromotion *)promotion atIndexPath:(NSIndexPath *)indexPath {
    if (promotion.isSelected) {
        promotion.isSelected = NO;
        [self.tbvVouchersList reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.currentVC deleteCurrentVoucherAndPromotionWith:self.bill];
        [self updateUIPayWithPromotionResult:self.bill.promotionInfo andPromotionUsed:self.bill.promotionInfo.promotionUsed];
        
        
    } else {
        [self.tbvVouchersList resetSelectedStateAllVouchersAndPromotions];
        [[self didSelectRowPromotionSignal:promotion fromAutoSelect:NO] subscribeCompleted:^{}];
    }
    
}
- (RACSignal *)didSelectRowPromotionSignal:(ZPPromotion *)promotion fromAutoSelect:(BOOL)isAutoSelect {
    if (promotion.isDisable) {
        return nil;
    }
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        ZPPromotionInfo *promotionInfoExisted = [[ZaloPayWalletSDKPayment sharedInstance].zpPromotionManager getPromotionUsedExistedInCache:promotion];
        if (promotionInfoExisted) {
            self.bill.voucherInfo = nil;
            [self updateUIPayWithPromotionResult:promotionInfoExisted andPromotionUsed:promotion];
            if (!isAutoSelect) {
                [self tapViewExpendVoucherList];
            }
            [self.tbvVouchersList reloadData];
            self.labelError.text = @"";
            self.bill.promotionInfo.promotionUsed = promotion;
            [self.viewExpendVouchersList mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(10);
                make.width.equalTo(UIScreen.mainScreen.bounds.size.width - 20);
            }];
            self.lineViewGreen.hidden = NO;
            [subscriber sendCompleted];
            return nil;
        }
        
        [[ZPAppFactory sharedInstance] showHUDAddedTo:self.currentVC.view];
        @weakify(self);
        [[[ZPPaymentManager registerPromotionWith:self.bill promotion:promotion] deliverOnMainThread] subscribeNext:^(NSDictionary *result) {
            @strongify(self);
            self.bill.voucherInfo = nil;
            [[ZPAppFactory sharedInstance] hideAllHUDsForView:self.currentVC.view];
            ZPPromotionInfo *info = [[ZPPromotionInfo alloc] initDataWithDict:result];
            [self updateUIPayWithPromotionResult:info andPromotionUsed:promotion];
            if (!isAutoSelect) {
                [self tapViewExpendVoucherList];
            }
            [self.tbvVouchersList reloadData];
            self.labelError.text = @"";
            
            promotion.uid = [NSString stringWithFormat:@"%@%@",promotion.campaigncode,self.bill.currentPaymentMethod.paymenMethodId];
            self.bill.promotionInfo.promotionUsed = promotion;
            [self.viewExpendVouchersList mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(10);
                make.width.equalTo(UIScreen.mainScreen.bounds.size.width - 20);
            }];
            self.lineViewGreen.hidden = NO;
            [[ZaloPayWalletSDKPayment sharedInstance].zpPromotionManager promotionsWillRevertAddPromotion:info];
            [subscriber sendCompleted];
        } error:^(NSError * _Nullable error) {
            @strongify(self);
            [[ZPAppFactory sharedInstance] hideAllHUDsForView:self.currentVC.view];
            if (self.viewError.frame.size.height == 0 && !isAutoSelect) {
                [self.viewError mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.bottom.equalTo(self.tbvVouchersList.mas_top);
                    make.height.equalTo(40);
                }];
                [self mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.height.equalTo(self.frame.size.height + 40);
                }];
                [self.maskView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.bottom.equalTo(-self.frame.size.height - 40);
                }];
            }
            self.tbvVouchersList.layer.borderWidth = 0;
            self.labelError.text = [error apiErrorMessage];
            [self updateUIPayWithPromotionResult:self.bill.promotionInfo andPromotionUsed:self.bill.promotionInfo.promotionUsed];
            promotion.isDisable = YES;
            [self.tbvVouchersList reloadData];
            [subscriber sendCompleted];
        }];
        return nil;
    }];
}


- (void)tapFooterView {
    [self tapViewExpendVoucherList];
    id<ZPWalletDependenceProtocol> helper = [ZaloPayWalletSDKPayment sharedInstance].appDependence;
    [helper goToVoucherHistoriesList];
}

- (void) updateListVoucherWhenSwitchPaymentMethod {
    
     ZPPromotionManager *promotionManager = [ZaloPayWalletSDKPayment sharedInstance].zpPromotionManager;
     ZPVoucherManager *voucherManager = [ZaloPayWalletSDKPayment sharedInstance].zpVoucherManager;
     @weakify(self)
    [[promotionManager preloadPromotionsForBill:self.bill] subscribeCompleted:^{
        @strongify(self);
        
        if (self.bill.transType != ZPTransTypeBillPay) {
            [self.viewExpendVouchersList setHidden:YES];
            [self mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(heightViewPayWithVouchersListBase);
            }];
            return;
        }
        self.countSelectPaymentMethod++;
        NSObject *oldEndow = self.bill.voucherInfo == nil ? self.bill.promotionInfo.promotionUsed : self.bill.voucherInfo.voucherUsed;
        if (self.bill.voucherInfo || self.bill.promotionInfo) {
            [self.currentVC deleteCurrentVoucherAndPromotionWith:self.bill];
            [self updateUIPayWithVoucherResult:self.bill.voucherInfo andVoucherUsed:self.bill.voucherInfo.voucherUsed];
        }
        self.tbvVouchersList.arrVoucherHistories = nil;
        self.tbvVouchersList.arrAllVouchersAndPromotions = nil;
        
        [[voucherManager preloadVoucherForBill:self.bill] subscribeCompleted:^{
            [self.tbvVouchersList resetStateAllVouchersAndPromotions];
            NSArray *promotions = [[NSArray alloc] initWithArray:[promotionManager promotionsCachesForBill:self.bill]];
            self.tbvVouchersList.arrPromotions = [[NSArray alloc] initWithArray:(NSMutableArray<ZPPromotion *> *)[promotionManager filterAvailablePromotionsWhenSelectedPaymentMethod:promotions forBill:self.bill]];
            NSArray *vouchers = [[NSArray alloc] initWithArray:[voucherManager voucherCachesForBill:self.bill]];
            self.tbvVouchersList.arrVoucherHistories = [[NSMutableArray alloc] initWithArray:(NSMutableArray<ZPVoucherHistory *> *)[voucherManager filterAvailableVouchersWhenSelectedPaymentMethod:vouchers forBill:self.bill]];
             self.tbvVouchersList.tableFooterView = self.tbvVouchersList.arrVoucherHistories.count > 0 ? self.tbvVouchersList.footerView : nil;
            [self.tbvVouchersList setupArrAllVouchersAndPromotions];
            int heightView = self.tbvVouchersList.arrAllVouchersAndPromotions.count > 0 ? heightViewPayWithVouchersListHaveVoucher : heightViewPayWithVouchersListBase;
            [self mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(heightView);
            }];
            
            [self.viewExpendVouchersList setHidden:self.tbvVouchersList.arrAllVouchersAndPromotions.count == 0];
            self.currentVC.zpw_tableMain.contentInset = UIEdgeInsetsMake(0, 0, heightView, 0);
            if (self.tbvVouchersList.arrAllVouchersAndPromotions.count > 0) {
                [self.tbvVouchersList resetSelectedStateAllVouchersAndPromotions];
                [self updateUIWhenHaveVouchersOrPromotions];
                [self.tbvVouchersList reloadData];
                //[self tapViewExpendVoucherList];
                if (self.countSelectPaymentMethod == 1) {
                    [self mas_updateConstraints:^(MASConstraintMaker *make) {
                        make.height.equalTo(heightViewPayWithVouchersListHaveVoucher);
                    }];
                    [self.viewExpendVouchersList mas_updateConstraints:^(MASConstraintMaker *make) {
                        make.top.equalTo(10);
                        make.width.equalTo(UIScreen.mainScreen.bounds.size.width - 20);
                    }];
                    float heightActualViewPayWithVouchersList = 12 * 2 + kZaloButtonHeight + self.tbvVouchersList.contentSize.height + heightViewExpendVoucherList;
                    [self.tbvVouchersList mas_updateConstraints:^(MASConstraintMaker *make) {
                        make.bottom.equalTo(self.actionButton.mas_top).offset(0);
                        if (heightActualViewPayWithVouchersList < heightViewPayWithVouchersListExpendMax) {
                            make.height.equalTo(0);
                            
                        }
                    }];
                    [self.maskView mas_updateConstraints:^(MASConstraintMaker *make) {
                        make.bottom.equalTo(0);
                    }];
                    [self.viewError mas_updateConstraints:^(MASConstraintMaker *make) {
                        make.height.equalTo(0);
                    }];
                    self.lineViewGreen.hidden = YES;
                    [self.viewExpendVouchersList roundRect:3 borderColor:[UIColor lineColor] borderWidth:1];
                }
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayIntervalForAnimationExpendVoucherView * NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    ZPVoucherHistory *bestVoucher = [[ZaloPayWalletSDKPayment sharedInstance].zpVoucherManager getBestVoucherFromArrayVoucher:self.tbvVouchersList.arrVoucherHistories ofBill:self.bill];
                    long long bestVoucherValue = bestVoucher.valuetype == ZPVoucherValueTypePercent ? bestVoucher.value * self.bill.amount/ 100 : bestVoucher.value;
                    
                    ZPPromotion *bestPromotion = [[ZaloPayWalletSDKPayment sharedInstance].zpPromotionManager getBestPromotionFromPromotions:self.tbvVouchersList.arrPromotions ofBill:self.bill];
                    long long bestPromotionValue = bestPromotion.discounttype == ZPVoucherValueTypePercent ? bestPromotion.discountpercent * self.bill.amount / 100 : bestPromotion.discountamount;
                    NSObject *bestEndow = bestVoucherValue > bestPromotionValue ? bestVoucher : bestPromotion;
                    if ([bestEndow isKindOfClass:[ZPVoucherHistory class]]) {
                        [[self didSelectRowVoucherSignal:bestVoucher fromAutoSelect:YES] subscribeCompleted:^{
                            [self showToastWithOldVoucher:oldEndow andNewVoucher:self.bill.voucherInfo.voucherUsed];
                        }];
                    }else {
                        [[self didSelectRowPromotionSignal:bestPromotion fromAutoSelect:YES] subscribeCompleted:^{
                            [self showToastWithOldVoucher:oldEndow andNewVoucher:self.bill.promotionInfo.promotionUsed];
                        }];
                    }
                    
                });
            }else {
                [self showToastWithOldVoucher:oldEndow andNewVoucher:nil];
            }
        }];
    }];
     
    
    
}

- (void) showToastWithOldVoucher:(NSObject *)oldEndow andNewVoucher:(NSObject *)newEndow {
    NSString *msg = @"";
    if (self.countSelectPaymentMethod == 1) {
        return;
    }
    if (oldEndow == nil && newEndow == nil) {
        return;
    }
    ZPPromotion *oldPromotion;
    ZPPromotion *newPromotion;
    ZPVoucherHistory *oldVoucher;
    ZPVoucherHistory *newVoucher;
    
    if ([oldEndow isKindOfClass:[ZPVoucherHistory class]]) {
        oldVoucher = (ZPVoucherHistory *)oldEndow;
    }else {
        oldPromotion = (ZPPromotion *)oldEndow;
    }
    
    if ([newEndow isKindOfClass:[ZPVoucherHistory class]]) {
        newVoucher = (ZPVoucherHistory *)newEndow;
    }else {
        newPromotion = (ZPPromotion *)newEndow;
    }
    
    if ([oldVoucher.paymentConditions.useconditionpmcs containsObject:@(-1)] || [oldPromotion.paymentConditions.useconditionpmcs containsObject:@(-1)]) {
        return;
    }
    if (newEndow) {
        msg = [NSString stringWithFormat:[R string_Voucher_Use_For_New_PaymentMethod],self.bill.currentPaymentMethod.methodName];
    }else if (oldEndow) {
        msg = [NSString stringWithFormat:[R string_Voucher_Not_Use_For_New_PaymentMethod],self.bill.currentPaymentMethod.methodName];
    }
    UIViewController *top = [[ZPAppFactory sharedInstance] rootNavigation].viewControllers.lastObject;
    id <ZPWalletDependenceProtocol>helper = [ZaloPayWalletSDKPayment sharedInstance].appDependence;
    [helper showToastFrom:top message:msg delay:0.5];
}

@end
