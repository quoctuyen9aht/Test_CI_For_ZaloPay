//
//  ZPBillExtraInfoCell.m
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/12/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPBillExtraInfoCell.h"
#import <ZaloPayWalletSDK/ZaloPayWalletSDK-Swift.h>
#import "ZPVoucherInfo.h"
#import "ZPPromotionInfo.h"
@interface ZPBillExtraInfoCell ()
@property (nonatomic, strong) UILabel *labelTitle;
@property (nonatomic, strong) UILabel *labelValue;
@end

@implementation ZPBillExtraInfoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle =  UITableViewCellSelectionStyleNone;
        [self setupCell];
    }
    return self;
}

- (void)setData:(ZPBillExtraInfoData *)data {
    self.labelValue.text = data.value;
    self.labelTitle.text = data.title;
}
- (void)setBasePriceFrom:(ZPBill *)bill {
    self.labelValue.text = [[NSString stringWithFormat:@"%ld",bill.amount] formatCurrency];
    self.labelTitle.text = [R string_Voucher_before_input_title];
}
- (void)setVoucherFrom:(ZPVoucherInfo *)voucher {
    self.labelValue.text = [[NSString stringWithFormat:@"-%ld",(long)voucher.discountAmount] formatCurrency];
    self.labelTitle.text = [R string_Voucher_gift_text];
}
- (void)setPromotionFrom:(ZPPromotionInfo *)promotionInfo {
    self.labelValue.text = [[NSString stringWithFormat:@"-%lld",promotionInfo.discountamount] formatCurrency];
    self.labelTitle.text = [R string_Voucher_gift_text];
}

- (void)setupCell {
    self.labelTitle = [[UILabel alloc] init];
    [self.contentView addSubview:self.labelTitle];
    [self.labelTitle zpMainGrayRegular];
    
    [self.labelTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(12);
        make.right.equalTo(self.contentView.mas_centerX).offset(-20);
        make.height.equalTo(20);
        make.bottom.equalTo(0);
    }];
    
    self.labelValue = [[UILabel alloc] init];
    [self.contentView addSubview:self.labelValue];
    [self.labelValue zpMainGrayRegular];
    self.labelValue.textAlignment = NSTextAlignmentRight;
    [self.labelValue mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(-12);
        make.left.equalTo(self.contentView.mas_centerX).offset(-20);
        make.height.equalTo(20);
        make.bottom.equalTo(0);
    }];
}

+ (float)height {
    return 32;
}

@end
