//
//  ZPAddNewMethodCell.h
//  ZaloPayWalletSDK
//
//  Created by vuongvv on 8/1/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZPMethodBaseCell.h"
#import "ZPDataManager.h"

@interface ZPAddNewMethodData : NSObject
@end

@interface ZPAddNewMethodCell : ZPMethodBaseCell
- (void)setData;
- (void)setDataWithTransType:(ZPTransType)transType;
- (void)canUsePayment:(NSInteger)appid;
@end
