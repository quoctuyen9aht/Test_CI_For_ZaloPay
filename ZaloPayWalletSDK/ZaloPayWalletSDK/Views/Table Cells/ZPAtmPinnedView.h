//
//  ZPAtmPinnedView.h
//  ZaloSDK.Payment
//
//  Created by Hoang Nguyen on 6/22/15.
//  Copyright (c) 2015-present VNG Corporation All rights reserved.
//

#import "ZPPaymentViewCell.h"

@interface ZPAtmPinnedView : ZPPaymentViewCell
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintCollectionViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintCollectionViewBottom;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintCollectionWidth;
@property (weak, nonatomic) IBOutlet UIButton *okButton;

@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *collectionViewFlow;
@property (weak, nonatomic) IBOutlet UIView *leftLayerView;
@property (weak, nonatomic) IBOutlet UIView *rightLayerView;
@property (weak, nonatomic) IBOutlet UIView *topLayerView;
@property (weak, nonatomic) IBOutlet UIView *botLayerView;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *layerViews;
@property (weak, nonatomic) IBOutlet UIImageView *imageHeaderBd;

- (void)updateLayoutsWithAnimation:(BOOL)animation;

- (void)showInView:(UIView *)view withData:(NSArray*)arrData;
- (void)dismiss;

@end
