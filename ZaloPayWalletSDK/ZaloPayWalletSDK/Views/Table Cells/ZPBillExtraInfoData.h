//
//  ZPBillExtraInfoData.h
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/12/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum {
    ZPBillExtraInfoType_Info,
    ZPBillExtraInfoType_Fee,
} ZPBillExtraInfoType;


@interface ZPBillExtraInfoData : NSObject
@property (nonatomic) ZPBillExtraInfoType type;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *value;
- (id)initWithTitle:(NSString *)title value:(NSString *)value;
- (id)initWithTitle:(NSString *)title value:(NSString *)value type:(ZPBillExtraInfoType)type;
@end
