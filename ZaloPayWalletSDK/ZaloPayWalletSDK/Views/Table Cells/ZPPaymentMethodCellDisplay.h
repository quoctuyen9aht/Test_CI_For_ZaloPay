//
//  ZPPaymentMethodCellDisplay.h
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/12/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class ZPPaymentMethod;
typedef NS_ENUM(NSInteger,ZPPaymentMethodCellType){
    ZPPaymentMethodCellTypeChannelUnsupport,
    ZPPaymentMethodCellTypeMaintain,
    ZPPaymentMethodCellTypeUnsupportAmmount,
    ZPPaymentMethodCellTypeNotEnoughMoney,
    ZPPaymentMethodCellTypeNotOverTransaction,
    ZPPaymentMethodCellTypeMinAppVersion,
    ZPPaymentMethodCellTypeAvailable,
};

@interface ZPPaymentMethodCellDisplay : NSObject
@property (nonatomic, strong) ZPPaymentMethod *method;
@property (nonatomic, strong) NSString *methodName;
@property (nonatomic, strong) UIImage *image;
@property (assign, nonatomic) CGFloat alpha;
@property (copy, nonatomic) NSString *feeText;
@property (strong, nonatomic) UIColor *feeTextColor;
@property (strong, nonatomic) UIColor *backgroundColor;
@property (nonatomic, strong) NSString *iconfontName;
@property (assign, nonatomic) BOOL isHiddenArrow;
@property (assign, nonatomic) BOOL isNewMethod;

@property (nonatomic) BOOL isLastCell;
@property (nonatomic) long fee;
@property (nonatomic) ZPPaymentMethodCellType type;
- (instancetype)initWith:(ZPPaymentMethod *)method withAmount:(long)amount;
+ (BOOL)isAvailable:(ZPPaymentMethod *)method;
@end
