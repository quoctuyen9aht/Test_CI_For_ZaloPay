//
//  ZPResultRowTitleValueView.m
//  ZaloPayWalletSDK
//
//  Created by thi la on 6/15/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPResultRowTitleValueView.h"
#import "ZPResultViewModel.h"

@interface ZPResultRowTitleValueView()
@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UILabel *value;
@end

@implementation ZPResultRowTitleValueView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        _title = [[UILabel alloc] init];
        [_title zpMainGrayRegular];
        [self addSubview:_title];
        
        _value = [[UILabel alloc] init];
        [_value zpMainGrayRegular];
        _value.textAlignment = NSTextAlignmentRight;
        [self addSubview:_value];
        [_title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@10);
            make.centerY.equalTo(@0);
            make.width.greaterThanOrEqualTo(@0);
        }];
        [_value mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(@-10);
            make.centerY.equalTo(@0);
            make.width.greaterThanOrEqualTo(0);
            make.left.equalTo(self.title.mas_right).offset(40);
        }];
    }
    return self;
}

- (void)setModel:(ZPResultViewModel *)model {
    [super setModel:model];

    ZPResultRowViewModel *viewModel = (ZPResultRowViewModel *)model;

    self.title.text = viewModel.title;
    self.value.text = viewModel.value;
}

@end
