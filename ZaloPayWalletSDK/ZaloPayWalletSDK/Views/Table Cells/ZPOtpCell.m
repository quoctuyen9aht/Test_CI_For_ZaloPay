//
//  ZPOtpCell.m
//  ZaloPayWalletSDK
//
//  Created by Nguyen Xuan Phung on 5/27/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPOtpCell.h"
#import "UIView+ZPBorders.h"
#import "UIColor+ZPExtension.h"
#import "ZPResourceManager.h"
#import "NSString+ZPExtension.h"
#import "ZPDataManager.h"
#import <ZaloPayAnalyticsSwift/ZaloPayAnalyticsSwift-Swift.h>

@interface ZPOtpCell()

@property (strong, nonatomic) UIView *line;
@property (strong, nonatomic) ZPKeyboardToolbar * toolbar;
@end
@implementation ZPOtpCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundView.backgroundColor = [UIColor whiteColor];
    [self.backgroundView addBottomBorderWithHeight:1.0 andColor:[UIColor lineColor]];
    self.txtOtpValue.autocorrectionType = UITextAutocorrectionTypeNo;
    self.txtOtpValue.spellCheckingType = UITextSpellCheckingTypeNo;
    self.txtOtpValue.delegate = self;
    [self.txtOtpValue addTarget:self action:@selector(textFieldDidChanged:) forControlEvents:UIControlEventEditingChanged];
    if (!_line) {
        _line = [[UIView alloc] init];
        [_line setBackgroundColor:TEXTFIELD_SUCCESS_COLOR];
        _line.frame = CGRectMake(0, 59, self.frame.size.width, 1);
        [self.contentView addSubview:_line];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideKB:) name:ZPUIKeyboardDidHideNotification object:nil];
}

- (void)setupKeyboardType:(NSString*)bankCode {
    int keyboardType = [ZPResourceManager otpKeyboardType:bankCode];
    [self.txtOtpValue setKeyboardType:keyboardType];
}

- (NSDictionary *)outputParams {
    NSMutableDictionary * params = [[NSMutableDictionary alloc] init];
    if (self.txtOtpValue.text.length > 0)
        [params setObject:self.self.txtOtpValue.text forKey:@"otp"];
    return params;
}

- (BOOL)checkValidate {
    NSString * errorMessage = @"";
    if (self.txtOtpValue.text.length <= 0) {
        errorMessage = @"Vui lòng nhập mã OTP";
    }
    
    if (errorMessage.length > 0) {
        return NO;
    }
    
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionSdk_otp_input];
    _line.hidden = NO;
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    _line.hidden = YES;
    [self validateInputData];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [self validateInputData];
    return YES;
}
- (void)validateInputData {
    if (self.txtOtpValue.text.length > 0) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(validateInputCompleted:)]) {
            [self.delegate validateInputCompleted:YES];
            
        }
    } else {
        if (self.delegate && [self.delegate respondsToSelector:@selector(validateInputCompleted:)]) {
            [self.delegate validateInputCompleted:NO];
        }
    }
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // This allows numeric text only, but also backspace for deletes
    NSString * textFieldString = textField.text;
    textFieldString = [textFieldString stringByReplacingCharactersInRange:range withString:string];
    NSString * detectString = [textFieldString zpTrimSpace];
    int maxLength = [[ZPDataManager sharedInstance] doubleForKey:@"otp_max_length"];
    if(maxLength == 0) maxLength = 16;
    return detectString.length <= maxLength;

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.txtOtpValue resignFirstResponder];
    return YES;
}

- (void)onNextButtonClicked:(id)sender {
    
}

- (void)onPreviousButtonClicked:(id)sender {
    
}

- (void)textFieldDidChanged:(UITextField*)textField {
    [self validateInputData];
}
-(void)onDoneButtonClicked:(id)sender {
    if (self.txtOtpValue.text.length > 0) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(validateInputDoneAndCallProcessPayment)]) {
            [self.delegate validateInputDoneAndCallProcessPayment];
        }
    }
    [self endEditing:YES];
    
}
-(void)hideKB:(NSNotification *)ntf{
    [self.txtOtpValue resignFirstResponder];
}
@end
