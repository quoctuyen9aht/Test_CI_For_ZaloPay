//
//  ZPBankCardMethodCell.m
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/12/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPBankCardMethodCell.h"

@implementation ZPBankCardMethodCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
