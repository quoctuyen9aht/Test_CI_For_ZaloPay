//
//  ZPCZCardViewCell.h
//  ZaloPaymentSDK
//
//  Created by hoangnx2 on 1/14/14.
//  Copyright (c) 2014 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZPCellObject.h"
#import "ZPRecordView.h"
#import "ZPKeyboardToolbar.h"
#define ZPShowMapCardIntro @"ZP_Show_Map_Card_Intro"
@class ZPAtmCard;
@class ZPBill;
@class ZPBank;
@class ZPRecordView;
@class ZPCell;
@class ZPTextFieldRecordView;
@class ZPPaymentViewCell;
@class ZPCreditCard;

@protocol ZPPaymentCellDelegate <ZPRecordViewDelegate>
@optional
- (void)detectCell:(ZPPaymentViewCell *)cell didDetectBank:(ZPBank *)bank;
- (void)detectCell:(ZPPaymentViewCell *)cell didEditCardNumberWithDetectedBank:(ZPBank *)bank;
- (void)pinnedCell:(ZPPaymentViewCell *)cell didSelectBank:(ZPBank *)bank;
- (void)detectCellNeedToSkipAccountInfo:(ZPPaymentViewCell *)cell;
- (void)paymentCell:(ZPPaymentViewCell *)cell didReturnData:(id)data;
- (void)detectCreditCard:(ZPPaymentViewCell *)cell didDetectBank:(NSDictionary *)bank;
- (void)detectAtmCard:(ZPPaymentViewCell *)cell didDetectBank:(NSDictionary *)ccbank;
- (BOOL)isAtmMapcard;
- (void)validateInputCompleted:(BOOL)isValid;
- (void)validateInputDoneAndCallProcessPayment;

//- Add
- (void)btnOkClicked;

- (void)showAlertGuideUserChooseSavedCard:(NSArray *)savedCardList;
- (void)autoChooseMethodWithCardNumber:(NSString *)cardNumber;
-(void)showAlertGuideUserLinkToMapAccount;
//Tai do
-(void)updateLayoutWhenInputText;
-(void)paymentCellDidTouchAccept:(ZPPaymentViewCell *)cell;
-(void)refreshCaptCha:(ZPPaymentViewCell *)cell;
@end

@interface ZPPaymentViewCell : UITableViewCell <ZPKeyboardToolbarDelegate, UITextFieldDelegate>
@property (strong, nonatomic) NSString * errorElementIdInWebView;
@property (strong, nonatomic) NSString * smlId;
@property (strong, nonatomic) NSString * banknetId;
@property (strong, nonatomic) NSString * submitButtonId;
@property (weak, nonatomic) id<ZPPaymentCellDelegate> delegate;
@property (strong, nonatomic) NSMutableArray * mySubViews;
@property (strong, nonatomic) UIView * whiteView;
@property (assign, nonatomic) ZPCellType type;
@property (assign, nonatomic) NSString* jsSubmitForm;


- (BOOL)checkValidate;
- (NSDictionary *)outputParams;
- (NSString *)jsSubmitFormSml;
- (NSString *)jsSubmitFormBanknet;
- (NSString *)jsSubmitFormSmlWithAtmCard:(ZPAtmCard *)atmCard;
- (NSString *)jsClickSubmitButtonSml;
- (void)setCaptchaImage:(UIImage*)img;
- (void)setAppendString:(NSString *) str;
- (void)setStaticValue:(NSString *) value andDisplayText:(NSString *) name;
- (void)setValue:(NSString *) value viewById:(NSString *)viewID;
- (void)setEnable:(bool)enable viewById:(NSString *)viewID;
- (void)setFormatIndex:(int)index viewId:(NSString *)viewID;
- (void)setHighlighted:(BOOL)highlighted viewId:(NSString *)viewID;
- (void)setBankName:(NSString *)value viewId:(NSString *)viewId;
- (void)setATMCardInfo:(ZPAtmCard *)cachedCard fromCC:(BOOL)backFromCC;
- (void)setCreditCardInfo:(ZPCreditCard *)cachedCard;
- (void)setSMPinnedViewInfo:(ZPBill *)bill;
- (void)setObject:(id)object viewId:(NSString *)viewId;
- (void)setKeyboardType:(UIKeyboardType)keyboardType viewId:(NSString *)viewId;
- (void)setOCRVisibility:(BOOL)flag viewId:(NSString *)viewId;
- (void)updateSmPinnedViewWithAmount:(long)amount;

- (instancetype)initWithGuiObject:(ZPCellObject *)object;
+ (float)caculateHeightCellWithGuiObject:(ZPCellObject *)object;
- (void)cacheInfo;
- (void)loadCachedInfo;
- (void)setCaptureButtonEnabled: (BOOL) enabled;
//- (void)setOkButtonEnabled:(BOOL)enabled;
- (ZPRecordView *)viewwithId:(NSString *)viewID;
- (void)updateLayouts;
- (UIView *)findFirstResponderBeneathView:(UIView *)view;

//Add
- (void)setOTP;

+(CGFloat)cellHeightForData:(NSObject*)data;
-(void)setupUIWithData:(NSObject*)data;
- (void)loadMyCustomFont;
@end
