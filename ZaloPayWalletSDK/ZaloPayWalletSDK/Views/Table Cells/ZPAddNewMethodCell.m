//
//  ZPAddNewMethodCell.m
//  ZaloPayWalletSDK
//
//  Created by vuongvv on 8/1/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPAddNewMethodCell.h"
#import <ZaloPayCommonSwift/ZaloPayCommonSwift-Swift.h>
#import "ZaloPayWalletSDKPayment.h"
@implementation ZPAddNewMethodData
@end

@implementation ZPAddNewMethodCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return self;
}

- (void)setData{
    [self.methodImageView setIconFont:@"pay_addcard"];
    [self.methodImageView setIconColor:[UIColor zaloBaseColor]];
    self.lbMethodName.text = [R string_Method_AddNewCardButton];
    [self.lbMethodName mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(20);
    }];

    [self.line mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(0);
    }];
    
    self.checkedImageView.hidden = true;
    self.lbMethodInfo.hidden = true;
    
}

- (void)setDataWithTransType:(ZPTransType)transType {
    [self.methodImageView setIconFont:@"pay_addcard"];
    [self.methodImageView setIconColor:[UIColor zaloBaseColor]];
    self.lbMethodInfo.text = [NSString stringWithFormat:stringMapcardInfoFormat,[[ZPDataManager sharedInstance] getTitleByTranstype:transType].lowercaseString];
    self.lbMethodName.text = [R string_Method_AddNewCardButton];
    [self.lbMethodName mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(10);
    }];
    
    [self.line mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(0);
    }];
    
    self.checkedImageView.hidden = true;
    self.lbMethodInfo.hidden = false;
    
}

- (void)canUsePayment:(NSInteger)appid {
    id<ZPWalletDependenceProtocol> dependency = [ZaloPayWalletSDKPayment sharedInstance].appDependence;
    if (!dependency) {
        return;
    }
    
    BOOL useGateway = [dependency canUseGateway:appid];
    self.lbMethodName.text = useGateway ? [R string_Gateway_Title_Source] : [R string_Method_AddNewCardButton];
    self.lbMethodInfo.text = @"";
    [self.lbMethodName mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(20);
    }];
}
@end
