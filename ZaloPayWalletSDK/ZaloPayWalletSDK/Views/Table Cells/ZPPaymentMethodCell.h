//
//  ZPPaymentMethodCell.h
//  ZaloPayWalletSDK
//
//  Created by Thi La on 4/23/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ZPPaymentMethod;
@interface ZPPaymentMethodCell : UITableViewCell

@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *methodIcon;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *methodName;
@property (weak, nonatomic) IBOutlet UILabel *feeLabel;

@property (weak, nonatomic) IBOutlet UIImageView *arrowIcon;

- (void)setupDisplayWith:(ZPPaymentMethod *)method withAmount:(long)amount;
@end
