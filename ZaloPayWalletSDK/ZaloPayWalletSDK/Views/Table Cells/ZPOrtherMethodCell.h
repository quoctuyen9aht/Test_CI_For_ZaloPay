//
//  ZPOrtherMethodCell.h
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/14/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ZPLineView;

@interface ZPOrtherMethodData : NSObject
@property (nonatomic, strong) NSString *title;
@end

@interface ZPOrtherMethodCell : UITableViewCell
@property (nonatomic, strong) ZPLineView *line;
- (void)setData:(ZPOrtherMethodData *)item;
@end

