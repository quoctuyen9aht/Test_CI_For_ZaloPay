//
//  ZPOrtherMethodCell.m
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/14/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPOrtherMethodCell.h"
#import <ZaloPayCommonSwift/ZaloPayCommonSwift-Swift.h>

@interface ZPOrtherMethodCell ()
@property (nonatomic, strong) UILabel *labelTitle;
@end

@implementation ZPOrtherMethodCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupCell];
    }
    return self;
}

- (UILabel *)labelTitle {
    if (!_labelTitle) {
        _labelTitle= [[UILabel alloc] initWithFrame:CGRectMake(10, 20, self.frame.size.width, 30)];
        _labelTitle.font = [UIFont SFUITextRegularWithSize:13];
        _labelTitle.backgroundColor = [UIColor clearColor];
        _labelTitle.textColor = [UIColor subText];
        [self.contentView addSubview:_labelTitle];
        
    }
    return _labelTitle;
}

- (void)setupCell {
    self.backgroundColor = [UIColor colorWithHexValue:0xF0F5F8];
    self.contentView.backgroundColor =  [UIColor colorWithHexValue:0xF0F5F8];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self labelTitle];
    [self addLine];
}

+ (float)height {
    return 50;
}

- (void)setData:(ZPOrtherMethodData *)item {
    self.labelTitle.text = item.title;
}
- (void)addLine {
    self.line = [[ZPLineView alloc] init];
    [self.contentView addSubview:self.line];
    [self.line alignToBottom];
}

@end

@implementation ZPOrtherMethodData
@end
