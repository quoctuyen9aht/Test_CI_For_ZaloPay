//
//  ZPCreditCardWebviewCell.m
//  ZaloPayWalletSDK
//
//  Created by Nguyen Xuan Phung on 5/7/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPCreditCardWebviewCell.h"

@interface ZPCreditCardWebviewCell()

@end
@implementation ZPCreditCardWebviewCell

-(void)dealloc{
    self.ccWebview.delegate = nil;
}

@end
