//
//  VoucherHistoryCell.h
//  ZaloPayWalletSDK
//
//  Created by Phuoc Huynh on 8/25/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZPVoucherHistory.h"
#import "ZPPromotion.h"

@interface ZPVoucherHistoryCell : UITableViewCell

@property (nonatomic,strong) ZPIconFontImageView *imgCheck;
@property (nonatomic,strong) UILabel  *lblVoucherDescription;
@property (nonatomic,strong) UILabel  *lblVoucherValue;
- (void) setVoucherHistoryData:(ZPVoucherHistory *)voucherHistory withBill:(ZPBill *)bill;
- (void) setPromotionData:(ZPPromotion *)promotion withBill:(ZPBill *)bill;
@end

