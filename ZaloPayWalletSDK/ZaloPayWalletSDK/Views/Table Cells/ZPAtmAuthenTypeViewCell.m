//
//  ZPAtmAuthenTypeViewCell.m
//  ZaloPayWalletSDK
//
//  Created by Thi La on 6/2/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPDataManager.h"
#import "ZPAtmAuthenTypeViewCell.h"
#import "UIColor+ZPExtension.h"
#import "ZPResourceManager.h"
#import "ZaloPayWalletSDKLog.h"
#import "ZPBank.h"

@interface ZPAtmAuthenTypeViewCell()

@property (strong, nonatomic) NSString *choseAuthMethod;
@property (strong, nonatomic) NSString *choseFirstMethod;
@property (strong, nonatomic) NSString *choseSecondMethod;

@end

@implementation ZPAtmAuthenTypeViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

    [self.btnSMS setImage:[ZPResourceManager getImageWithName:@"ico_radioactive"] forState:UIControlStateSelected];
    [self.btnSMS setImage:[ZPResourceManager getImageWithName:@"ic_unchecked-mark"] forState:UIControlStateNormal];
    [self.btnToken setImage:[ZPResourceManager getImageWithName:@"ico_radioactive"] forState:UIControlStateSelected];
    [self.btnToken setImage:[ZPResourceManager getImageWithName:@"ic_unchecked-mark"] forState:UIControlStateNormal];
    self.btnSMS.selected = YES;
    self.choseAuthMethod = @"otp";
    [self updateLayouts];
}

- (void)updateLayouts {
    DDLogInfo(@"self.bankcode %@", self.bankCode);
    ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:self.bankCode];
    if (bank.otpType.length > 0) {
        if ([[bank.otpType componentsSeparatedByString:@"|"] count] >= 2) {
            self.choseAuthMethod = [[[[bank.otpType componentsSeparatedByString:@"|"] firstObject] componentsSeparatedByString:@":"] firstObject];
            self.choseFirstMethod = _choseAuthMethod;
            DDLogInfo(@"choseFirstMethod %@", _choseFirstMethod);
            self.choseSecondMethod = [[[[bank.otpType componentsSeparatedByString:@"|"] objectAtIndex:1] componentsSeparatedByString:@":"] firstObject];
            DDLogInfo(@"choseSecondMethod %@", _choseSecondMethod);
        } else {
            self.choseAuthMethod = [[bank.otpType componentsSeparatedByString:@":"] firstObject];
            self.choseSecondMethod = [[bank.otpType componentsSeparatedByString:@":"] firstObject];
        }
    } else {
        self.choseAuthMethod = @"otp";
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (NSDictionary *)outputParams {
    NSMutableDictionary * params = [[NSMutableDictionary alloc] init];
    if (self.choseAuthMethod.length > 0) {
        [params setObject:self.choseAuthMethod forKey:@"otptype"];
    }
    return params;
    
}
- (IBAction)btnSMSClick:(UIButton *)sender {
    if ([self.choseAuthMethod isEqualToString:_choseSecondMethod]) {
        self.choseAuthMethod = _choseFirstMethod;
        self.btnSMS.selected = YES;
        self.btnToken.selected = NO;
    }
}

- (IBAction)btnTokenClick:(UIButton *)sender {
    if ([self.choseAuthMethod isEqualToString:_choseFirstMethod]) {
        self.choseAuthMethod = _choseSecondMethod;
        self.btnSMS.selected = NO;
        self.btnToken.selected = YES;
    }
}

@end
