//
//  ZPCreditSaveCardCell.m
//  ZaloPayWalletSDK
//
//  Created by CPU11689 on 9/29/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPCreditSaveCardCell.h"
#import <QuartzCore/QuartzCore.h>
#import "ZPDataManager.h"
#import "NSString+ZPExtension.h"
#import "ZPResourceManager.h"
#import "UIColor+ZPExtension.h"
#import "ZPPaymentInfo.h"
#import "UIImage+ZPExtension.h"
#import "ZPManagerSaveCardCell.h"
#import "ZaloPayWalletSDKLog.h"
#import <CoreText/CoreText.h>
#import "ZPBank.h"
#import "ZPBill.h"
#import "ZaloPayWalletSDKPayment.h"
#import <ZaloPayCommon/UIView+Extention.h>
#import <ZaloPayAnalyticsSwift/ZaloPayAnalyticsSwift-Swift.h>

#define kWidthScreen [UIScreen mainScreen].bounds.size.width
#define kHeightScreen [UIScreen mainScreen].bounds.size.height
//#define kHeightInputView 88 //IS_IPAD ? 60 : 38
#define KMarginViewOnSV 0
#define kMarginTextField 10
#define kHeightCell 300
#define kHeightBtnAccept 44

#define kTitleBtnPre @"Previous"
#define kTitleBtnNext @"Next"
#define kTitleBtnDone @"Xong"

#define kTitleBtnSMS @"SMS"
#define kTitleBtnToken @"Token"

#define kTitleConfirmTransition @"Hình thức xác thực giao dịch"

#define kIndexBtnSMS 0
#define kIndexBtnToken 1

#define kFormatDefaultNumber @"XXXX XXXX XXXX XXXX"
#define kFormatDefaultDate @"MM/YY"

#define kColorDefaultGradientStart @"#767A7B"
#define kColorDefaultGradientEnd @"#5C5D5F"

#define kFontNumberCard @"OCRATributeW01-RegularMono"
#define kFontNumberCardFile @"OCRA"

#define kCocorHightLightLabel @""
#define kCocorNormalLabel @""

#define kColorHightLightLabel @"#ffffff"
#define kColorNormalLabel @"#c1c1c1"
#define kColorTextChange @"#4387f6"

#define kColorLineBottom @"#E3E6E7"

#define binLength 6

@interface ZPCreditSaveCardCell()<UITextFieldDelegate,ZPMonthYearPickerDelegate,ZPPickerListDelegate,UIScrollViewDelegate>
{
    NSString *titleAmount;
    NSString *titleNumber;
    NSString *titleName;
    NSString *titlePass;
    NSString *titleExpire;
    NSString *titleCVV;
    
    NSString *titleNameDefault;
    CGFloat kHeightInputView;
    
    CGFloat kHSizeNumberFontBig;
    CGFloat kHSizeNumberFontSmall;
    
    int lastIndexSV;
    BOOL isCurrentOrientationAniFlip;
    BOOL LastOrientationAniFlip;
    BOOL isHaveNumberField;
    
    ZPMonthYearPicker * datePickerView;
    ZPPickerList * listConfirmTransitionPickerView;
    
    UIButton *mBtnSMS;
    UIButton *mBtnToken;
    
    NSArray *formats;
    NSArray *formatsDate;
    
    UIColor *lineColor;
    BOOL validFromDate;
    BOOL isCardDateValid;
    UIView *line;
    NSString *choseAuthMethod;
    NSString *choseFirstMethod;
    NSString *choseSecondMethod;
    
    NSString *TxtNumberLast;
    
    BOOL isNeedReload;
    CAGradientLayer *gradient;
    CAGradientLayer *gradientBack;

    UIPageControl *pageControlCur;
    
    NSTimer *timerDelayNextPreClicked;
    NSTimer *timerEnableScroll;
    NSTimer *timerFirstBecome;
    
    UIView *viewPreNext;
    CGFloat heightKeyboard;
    
    BOOL isViewBackClicked;
    
    BOOL isScrollingToError;
    
    BOOL isHideKeyBoard;
    
    CGFloat lastContentOffset;
    
}
@property (nonatomic) int indexCurrentSV;
@property (nonatomic) BOOL isCheckValidScroll;
@property (nonatomic) BOOL isEndDecelerating;
@property (nonatomic) BOOL isScrolling;
@property (nonatomic) BOOL isFlippingViewCard;
@property (nonatomic, strong) NSString *titleExpireDefault;
@property (nonatomic, strong) NSMutableArray *arrTF;

@end

@implementation ZPCreditSaveCardCell

#pragma mark - LIFE CYCLE
- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setupDefaultData];
    
    [self initView];
    [self excuteView];
}


#pragma mark - Init
-(void) initView{
    
    //Variables
    formats = @[@" ", @(4), @(4), @(4), @(4)];
    formatsDate = @[@"/",@(2),@(2)];
    timerDelayNextPreClicked = [[NSTimer alloc] init];
    timerEnableScroll = [[NSTimer alloc] init];
    timerFirstBecome = [[NSTimer alloc] init];
    
    [self loadMyCustomFont];
    [self setSizeWithOtherDevices];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideKeyBoard:) name:ZPUIKeyboardDidHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideBtnPreNext:) name:ZPUIButtonPreNextDidHideNotification object:nil];
    
    //viewPreNext = nil;
    line = [[UIView alloc] init];
    choseAuthMethod = @"otp";
    self.isEndDecelerating = TRUE;
    LastOrientationAniFlip = FALSE;
    self.isScrolling = NO;
    
    self.mLblTitleCardName.text = titleName;
    self.mLblTitleCardExpire.text = titleExpire;
    
    self.mLblCardName.text = titleNameDefault;
    self.mLblCardExpire.text = @"";
    
    self.mImgBG.image =  [ZPResourceManager getImageWithName:@"ico_map"];
    
    self.mLblCardNumber.text = kFormatDefaultNumber;
    self.mLblCardExpire.text = kFormatDefaultDate;
    
    CGFloat offsetShadow = 0.1;
    self.mLblCardNumber.layer.shadowColor = [UIColor zpColorFromHexString:@"#7F000000"].CGColor;
    self.mLblCardNumber.layer.shadowOffset = CGSizeMake(offsetShadow,offsetShadow);
    self.mLblCardNumber.layer.shadowOpacity = 0.5;
    self.mLblCardNumber.layer.shadowRadius = 0.5;
    
    self.mLblCardName.layer.shadowColor = [UIColor zpColorFromHexString:@"#7F000000"].CGColor;
    self.mLblCardName.layer.shadowOffset = CGSizeMake(offsetShadow,offsetShadow);
    self.mLblCardName.layer.shadowOpacity = 0.5;
    self.mLblCardName.layer.shadowRadius = 0.5;
    
    self.mLblCardExpire.layer.shadowColor = [UIColor zpColorFromHexString:@"#7F000000"].CGColor;
    self.mLblCardExpire.layer.shadowOffset = CGSizeMake(offsetShadow,offsetShadow);
    self.mLblCardExpire.layer.shadowOpacity = 0.5;
    self.mLblCardExpire.layer.shadowRadius = 0.5;
    
    [self getFontSizeNumber];
    
    
    
    [self setGradientColorForCardView:self.bankCode];
    self.mViewCardBG.backgroundColor = [UIColor whiteColor];
    
    self.mLblCardCVV.text = @"";
    
    [self setupScrollViewTF];
    
}
-(void) excuteView{
    
    if(IS_IPHONE_4_OR_LESS){
        double delayInSeconds = .1;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self firstResponderKeyboardTextField:0];
        });
    }
    else {
        ZPATMSaveCardObject *mdGet = self.arrTF[0];
        [mdGet.mTextField becomeFirstResponder];
        self.isScrolling = NO;
        
        double delayInSeconds = .1;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self scrollToIndex:self.indexCurrentSV];
        });
        delayInSeconds = .4;
        popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            self.isCheckValidScroll = true;
        });
        
    }
}

-(void) setupDefaultData{
    
    
    //Title
    titleAmount = @"Nhập số tiền";
    titleNumber = @"Nhập số thẻ";
    titleName = @"Nhập tên chủ thẻ";
    titlePass = @"Nhập mật khẩu DV thanh toán online";
    titleExpire = @"Nhập ngày hết hạn (mm/yy)";
    titleCVV = @"CVV";
    
    titleNameDefault = @""; //@"TÊN CHỦ THẺ";
    self.titleExpireDefault = @""; //@"NGÀY HẾT HẠN";
    
    self.bankCode = @"";
    self.lastBankCode = @"";
    
    self.arrTF = [NSMutableArray array];
    [self.arrTF addObject:[[ZPATMSaveCardObject alloc] initWithTitle:titleNumber andValue:@"" andPlaceHolder:@"" andType:ZPATMSaveCardObjectType_CardNumber]];
    [self.arrTF addObject:[[ZPATMSaveCardObject alloc] initWithTitle:titleExpire andValue:@"" andPlaceHolder:kFormatDefaultDate andType:ZPATMSaveCardObjectType_CardExpire]];
    [self.arrTF addObject:[[ZPATMSaveCardObject alloc] initWithTitle:titleCVV andValue:@"" andPlaceHolder:@"" andType:ZPATMSaveCardObjectType_CardCVV]];
    [self.arrTF addObject:[[ZPATMSaveCardObject alloc] initWithTitle:titleName andValue:@"" andPlaceHolder:@"" andType:ZPATMSaveCardObjectType_CardName]];
    
}



-(void) setupScrollViewTF{
    
    self.indexCurrentSV = 0;
    lastIndexSV = self.indexCurrentSV;
    isCurrentOrientationAniFlip = false;
    
    //self.mViewCardBackCGV.hidden = true;
    self.mViewCardBackCVV.alpha = 0;
    [self registerForKeyboardNotifications];
    
    self.mScrollTextfeild.pagingEnabled = true;
    self.mScrollTextfeild.showsVerticalScrollIndicator = false;
    self.mScrollTextfeild.showsHorizontalScrollIndicator = false;
    self.mScrollTextfeild.backgroundColor = [UIColor whiteColor];
    self.mViewCardBG.layer.cornerRadius = 3;
    self.mViewCardBG.clipsToBounds = true;
    self.mScrollTextfeild.delegate = self;
    
    
    CGRect rectSV = self.mScrollTextfeild.frame;
    rectSV.size.width = kWidthScreen ;//- rectSV.origin.x * 2;
    
    
    [self.mScrollTextfeild setContentSize:CGSizeMake((rectSV.size.width * self.arrTF.count) , rectSV.size.height)];
    
    int index = 0;
    if(isHaveNumberField) index = 1;
    for (int i = index ; i < self.arrTF.count; i++) {
        
        ZPATMSaveCardObject *mdGet = self.arrTF[i];
        
        CGRect rectTF = CGRectMake(rectSV.size.width * i + KMarginViewOnSV, KMarginViewOnSV, rectSV.size.width, rectSV.size.height - KMarginViewOnSV * 2);
        
        
        //UI Container
        UIView *view = [[UIView alloc] initWithFrame:rectTF];
        view.backgroundColor = [UIColor whiteColor];
        view.layer.cornerRadius = 0;
        view.clipsToBounds = true;
        view.translatesAutoresizingMaskIntoConstraints = true;
        view.tag = 10 + i;
        
        if(mdGet.mType  != ZPATMSaveCardObjectType_OptionSMSToken){
            
            //TF
            JVFloatLabeledTextField *titleField = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectZero];
            
            if([mdGet.mPlaceHolder isEqualToString:@""]){
                [titleField setPlaceholder:mdGet.mTitle floatingTitle:mdGet.mTitle];
            }
            else {
                [titleField setPlaceholder:mdGet.mPlaceHolder floatingTitle:mdGet.mTitle];
            }
            //[titleField setPlaceholder:@"" floatingTitle:mdGet.mTitle];
            titleField.keepBaseline = YES;
            titleField.alwaysShowFloatingLabel = YES;
            titleField.clearButtonMode = UITextFieldViewModeWhileEditing;
            titleField.isShowTopBottomLine = YES;
            titleField.isAlwayActiveTextColorFloating = YES;
            
            mdGet.mTextField = titleField;
            mdGet.mTextField.delegate = self;
            mdGet.mTextField.tag = i;
            [mdGet.mTextField addTarget:self action:@selector(changeTextOnCardView:) forControlEvents:UIControlEventEditingChanged];
            mdGet.mTextField.text = mdGet.mValue;
            mdGet.mTextField.autocorrectionType = UITextAutocorrectionTypeNo;
            mdGet.mTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
            mdGet.mTextField.spellCheckingType= UITextSpellCheckingTypeNo;
            
            if(mdGet.mType == ZPATMSaveCardObjectType_CardNumber || mdGet.mType == ZPATMSaveCardObjectType_CardCVV){
                mdGet.mTextField.keyboardType = UIKeyboardTypeNumberPad;
                [self setupInputViewForTFLineTopNumber:mdGet.mTextField];
            }
            
            else if(mdGet.mType == ZPATMSaveCardObjectType_CardExpire){
                //[mdGet.mTextField setInputView:[self getDatePicker]];
                mdGet.mTextField.keyboardType = UIKeyboardTypeNumberPad;
                [self setupInputViewForTFLineTopNumber:mdGet.mTextField];
            }
            
            if(mdGet.mType == ZPATMSaveCardObjectType_CardAmount){
                UILabel * rightView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
                rightView.font = [UIFont boldSystemFontOfSize:15];
                rightView.text = @"VND";
                rightView.textColor = zpRGB(200, 200, 200);
                rightView.backgroundColor = [UIColor clearColor];
                mdGet.mTextField.rightViewMode = UITextFieldViewModeAlways;
                mdGet.mTextField.rightView = rightView;
                
            }
            if(mdGet.mType == ZPATMSaveCardObjectType_CardPassword){
                mdGet.mTextField.secureTextEntry = true;
            }
            if(i == self.arrTF.count - 1) {
                mdGet.mTextField.returnKeyType = UIReturnKeyNext;
            }
            //[self setupInputViewForTF:titleField];
            
            [view addSubview:titleField];
            [self.mScrollTextfeild addSubview:view];
            
            //Constaint
            [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(xMargin)-[titleField]-(1)-|" options:NSLayoutFormatAlignAllCenterY metrics:@{@"xMargin": @(kMarginTextField)} views:NSDictionaryOfVariableBindings(titleField)]];
            
            [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(1)-[titleField]-(1)-|" options:NSLayoutFormatAlignAllCenterX metrics:@{@"xMargin": @(kMarginTextField)} views:NSDictionaryOfVariableBindings(titleField)]];
            
        }
        else {
            JVFloatLabeledTextField *titleFieldFake = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectZero];
            
            mdGet.mTextField = titleFieldFake;
            mdGet.mTextField.delegate = self;
            mdGet.mTextField.hidden = true;
            mdGet.mTextField.inputView = [self getListConfirmTransitionPicker];
            mdGet.mTextField.autocorrectionType = UITextAutocorrectionTypeNo;
            mdGet.mTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
            mdGet.mTextField.spellCheckingType= UITextSpellCheckingTypeNo;
            
            //[self setupInputViewForTF:titleFieldFake];
            
            [view addSubview:titleFieldFake];
            
            //Constaint
            [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(xMargin)-[titleFieldFake]-(1)-|" options:NSLayoutFormatAlignAllCenterY metrics:@{@"xMargin": @(kMarginTextField)} views:NSDictionaryOfVariableBindings(titleFieldFake)]];
            
            [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(1)-[titleFieldFake]-(1)-|" options:NSLayoutFormatAlignAllCenterX metrics:@{@"xMargin": @(kMarginTextField)} views:NSDictionaryOfVariableBindings(titleFieldFake)]];
            
            [self.mScrollTextfeild addSubview:view];
            [self setupBtnSMS_Token:view];
            
        }
        
        
    }
    
    for(int i = 0 ; i < self.arrTF.count ; i++){
        ZPATMSaveCardObject *mdGet = self.arrTF[i];
        [mdGet.mTextField addTarget:self action:@selector(tapTF) forControlEvents:UIControlEventTouchDown];
    }
    
    [self.mScrollTextfeild setContentOffset:CGPointMake(0, 0)];
    isHaveNumberField = true;
}
-(void)setupInputViewForTFLineTopNumber:(UITextField*)tf{
    CGRect rectInput = CGRectMake(0, 0, kWidthScreen, 1);
    UIView *viewInput = [[UIView alloc] initWithFrame:rectInput];
    viewInput.translatesAutoresizingMaskIntoConstraints = false;
    viewInput.backgroundColor = [UIColor zpColorFromHexString:@"#E3E6E7"];
    tf.inputAccessoryView = viewInput;
}

-(void)setupInputViewForTFFRame{
    if(viewPreNext) return;
    
    CGFloat cellHeight = (SCREEN_WIDTH - kMarginCardView) * (1 / ratioCardView) + cardViewScrollHeight;
    
    CGFloat bottomContraintCell = 15;
    if(IS_IPHONE_4_OR_LESS){
        cellHeight = (SCREEN_WIDTH - kMarginCardView) * (1 / (ratioCardView + 1)) + cardViewScrollHeight;
    }
    CGFloat heightSafeArea = [UIView safeAreaInsets].top;
    CGRect rectInput = CGRectMake(0, cellHeight  - bottomContraintCell + 4 + heightSafeArea / 2, kWidthScreen, kHeightInputView + bottomContraintCell - heightSafeArea);
    viewPreNext = [[UIView alloc] initWithFrame:rectInput];
    viewPreNext.tag = 100;
    viewPreNext.backgroundColor = [UIColor clearColor];
    [self.zpParentController.view addSubview:viewPreNext];
    CGFloat heightViewContainer = kHeightBtnAccept;
    if(IS_IPHONE_4_OR_LESS){
        heightViewContainer = 30;
    }
    
    
    CGRect rectContainer = CGRectMake(0, rectInput.size.height / 2.0 - heightViewContainer / 2.0 +3 ,kWidthScreen, heightViewContainer + 1);
    if(IS_IPHONE_4_OR_LESS){
        rectContainer = CGRectMake(0, rectInput.size.height / 2.0 - heightViewContainer / 2.0,kWidthScreen, heightViewContainer + 1);
    }
    UIView *viewContainer = [[UIView alloc] initWithFrame:rectContainer];
    
    [viewPreNext addSubview:viewContainer];
    
    //BtnPre
    UIButton *btnPre = [[UIButton alloc] initWithFrame:CGRectMake(kWidthScreen /2.0 - 80 /2.0 - heightViewContainer, 0, heightViewContainer, heightViewContainer)];
    //Need
    [btnPre setImage:[UIImage zpImageResize:[ZPResourceManager getImageWithName:@"ico_pre"]  andResizeTo:CGSizeMake(10, 17) andOffset:CGPointZero] forState:UIControlStateNormal];
    [btnPre setImageEdgeInsets:UIEdgeInsetsMake(0, -3, 0, 0)];
    [btnPre setBackgroundImage:[UIImage zpImageWithColor:[UIColor zpColorFromHexString:@"#008fe4"] withSize:CGSizeMake(heightViewContainer, heightViewContainer)] forState:UIControlStateNormal];
    [btnPre setBackgroundImage:[UIImage zpImageWithColor:[UIColor zpColorFromHexString:@"#78c1ec"] withSize:CGSizeMake(heightViewContainer, heightViewContainer)] forState:UIControlStateDisabled];
    [btnPre addTarget:self action:@selector(btnPreOnAccessoryViewClick) forControlEvents:UIControlEventTouchUpInside];
    btnPre.tag = 1;
    if(self.indexCurrentSV == 0)
        btnPre.enabled = false;
    //BtnNext
    UIButton *btnNext = [[UIButton alloc] initWithFrame:CGRectMake(kWidthScreen /2.0 + 80 /2.0, 0,heightViewContainer, heightViewContainer)];
    //[btnNext setTitle:kTitleBtnNext forState:UIControlStateNormal];
    [btnNext addTarget:self action:@selector(btnNextOnAccessoryViewClick) forControlEvents:UIControlEventTouchUpInside];
    [btnNext setImage:[UIImage zpImageResize:[ZPResourceManager getImageWithName:@"ico_next"]  andResizeTo:CGSizeMake(10, 17) andOffset:CGPointZero] forState:UIControlStateNormal];
    [btnNext setImageEdgeInsets:UIEdgeInsetsMake(0, 3, 0, 0)];
    [btnNext setBackgroundImage:[UIImage zpImageWithColor:[UIColor zpColorFromHexString:@"#008fe4"] withSize:CGSizeMake(heightViewContainer, heightViewContainer)] forState:UIControlStateNormal];
    [btnNext setBackgroundImage:[UIImage zpImageWithColor:[UIColor zpColorFromHexString:@"#78c1ec"] withSize:CGSizeMake(heightViewContainer, heightViewContainer)] forState:UIControlStateDisabled];
    btnNext.tag = 2;
    
    //Page control
    pageControlCur = [[UIPageControl alloc] initWithFrame:CGRectMake(kWidthScreen /2.0 - 80 /2.0, 0, 80, heightViewContainer)];
    pageControlCur.pageIndicatorTintColor = [UIColor lightGrayColor];
    pageControlCur.currentPageIndicatorTintColor = [UIColor grayColor];
    if(self.bankCode.length == 0 || self.bankCode == nil)
        pageControlCur.numberOfPages = kDefaultStepCardView;
    else {
        pageControlCur.numberOfPages = self.arrTF.count;
    }
    pageControlCur.currentPage = self.indexCurrentSV;
    pageControlCur.tag = 10;
    pageControlCur.userInteractionEnabled = false;

    [viewContainer addSubview:btnPre];
    [viewContainer addSubview:btnNext];
    [viewContainer addSubview:pageControlCur];
    
    viewContainer.backgroundColor =[UIColor clearColor];

    btnPre.backgroundColor = [UIColor clearColor];
    btnNext.backgroundColor = [UIColor clearColor];
    pageControlCur.backgroundColor = [UIColor clearColor];
    
    btnPre.clipsToBounds = true;
    btnPre.layer.cornerRadius = heightViewContainer / 2;
    
    btnNext.clipsToBounds = true;
    btnNext.layer.cornerRadius = heightViewContainer / 2;
    
    
}

-(void)setupInputViewForTF:(UITextField*)tf{
    
    CGRect rectInput = CGRectMake(0, 0, kWidthScreen, kHeightInputView);
    UIView *viewInput = [[UIView alloc] initWithFrame:rectInput];
    viewInput.translatesAutoresizingMaskIntoConstraints = false;
    
    
    CGRect rectContainer = CGRectMake(0, 0 ,kWidthScreen, 44);
    UIView *viewContainer = [[UIView alloc] initWithFrame:rectContainer];
    viewContainer.translatesAutoresizingMaskIntoConstraints = false;
    
    //Line
    UIView *lineBottom = [[UIView alloc] initWithFrame:CGRectZero];
    lineBottom.backgroundColor = [UIColor lightGrayColor];
    lineBottom.translatesAutoresizingMaskIntoConstraints = false;
    
    [viewInput addSubview:viewContainer];
    [viewInput addSubview:lineBottom];
    
    [viewInput addConstraint:[NSLayoutConstraint constraintWithItem:viewContainer
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.0
                                                           constant:SCREEN_WIDTH]];
    [viewInput addConstraint:[NSLayoutConstraint constraintWithItem:viewInput
                                                          attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:viewContainer
                                                          attribute:NSLayoutAttributeCenterY
                                                         multiplier:1.0
                                                           constant:0]];

    [viewInput addConstraint:[NSLayoutConstraint constraintWithItem:viewInput
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:lineBottom
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.0
                                                           constant:0.5]];
    [viewInput addConstraint:[NSLayoutConstraint constraintWithItem:lineBottom
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.0
                                                           constant:SCREEN_WIDTH]];
    [viewInput addConstraint:[NSLayoutConstraint constraintWithItem:lineBottom
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.0
                                                           constant:0.5]];
    
    //BtnPre
    UIButton *btnPre = [[UIButton alloc] initWithFrame:CGRectZero];
    //Need
    [btnPre setImage:[UIImage zpImageResize:[ZPResourceManager getImageWithName:@"ico_pre"]  andResizeTo:CGSizeMake(10, 13) andOffset:CGPointZero] forState:UIControlStateNormal];
    [btnPre setBackgroundImage:[UIImage zpImageWithColor:[UIColor zpColorFromHexString:@"#008fe4"] withSize:CGSizeMake(80, 80)] forState:UIControlStateNormal];
    [btnPre setBackgroundImage:[UIImage zpImageWithColor:[UIColor zpColorFromHexString:@"#78c1ec"] withSize:CGSizeMake(80, 80)] forState:UIControlStateDisabled];
    
    
    
    [btnPre addTarget:self action:@selector(btnPreOnAccessoryViewClick) forControlEvents:UIControlEventTouchUpInside];
    //BtnNext
    UIButton *btnNext = [[UIButton alloc] initWithFrame:CGRectZero];
    //[btnNext setTitle:kTitleBtnNext forState:UIControlStateNormal];
    [btnNext addTarget:self action:@selector(btnNextOnAccessoryViewClick) forControlEvents:UIControlEventTouchUpInside];
    [btnNext setImage:[UIImage zpImageResize:[ZPResourceManager getImageWithName:@"ico_next"]  andResizeTo:CGSizeMake(10, 13) andOffset:CGPointZero] forState:UIControlStateNormal];
    
    [btnNext setBackgroundImage:[UIImage zpImageWithColor:[UIColor zpColorFromHexString:@"#008fe4"] withSize:CGSizeMake(80, 80)] forState:UIControlStateNormal];
    [btnNext setBackgroundImage:[UIImage zpImageWithColor:[UIColor zpColorFromHexString:@"#78c1ec"] withSize:CGSizeMake(80, 80)] forState:UIControlStateDisabled];
    
    //Page control
    UIPageControl *pageControl = [[UIPageControl alloc] initWithFrame:CGRectZero];
    pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    pageControl.currentPageIndicatorTintColor = [UIColor grayColor];
    if(self.bankCode.length == 0 || self.bankCode == nil)
        pageControl.numberOfPages = kDefaultStepCardView;
    else {
        pageControl.numberOfPages = self.arrTF.count;
    }
    pageControl.currentPage = self.indexCurrentSV;
    pageControl.tag = 10;
    pageControl.userInteractionEnabled = false;
    pageControlCur  = pageControl;
    [self updatePageControl:tf];
    
    
    [viewContainer addSubview:btnPre];
    [viewContainer addSubview:btnNext];
    [viewContainer addSubview:pageControl];
    
    viewContainer.backgroundColor =[UIColor clearColor];
    viewInput.backgroundColor =[UIColor clearColor];
    
    btnPre.backgroundColor = [UIColor clearColor];
    btnNext.backgroundColor = [UIColor clearColor];
    pageControl.backgroundColor = [UIColor clearColor];
    
    btnPre.clipsToBounds = true;
    btnPre.layer.cornerRadius = 44 / 2;
    
    btnNext.clipsToBounds = true;
    btnNext.layer.cornerRadius = 44 / 2;
    
    
    btnPre.translatesAutoresizingMaskIntoConstraints = false;
    btnNext.translatesAutoresizingMaskIntoConstraints = false;
    pageControl.translatesAutoresizingMaskIntoConstraints = false;
    
    
    [viewContainer addConstraint:[NSLayoutConstraint constraintWithItem:btnPre
                                                              attribute:NSLayoutAttributeWidth
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeNotAnAttribute
                                                             multiplier:1.0
                                                               constant:44]];
    [viewContainer addConstraint:[NSLayoutConstraint constraintWithItem:btnNext
                                                              attribute:NSLayoutAttributeWidth
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeNotAnAttribute
                                                             multiplier:1.0
                                                               constant:44]];
    [viewContainer addConstraint:[NSLayoutConstraint constraintWithItem:pageControl
                                                              attribute:NSLayoutAttributeWidth
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeNotAnAttribute
                                                             multiplier:1.0
                                                               constant:100]];
    
    
    
    [viewContainer addConstraint:[NSLayoutConstraint
                                  constraintWithItem:btnPre
                                  attribute:NSLayoutAttributeWidth
                                  relatedBy:0
                                  toItem:btnPre
                                  attribute:NSLayoutAttributeHeight
                                  multiplier:1.0
                                  constant:0]];
    
    [viewContainer addConstraint:[NSLayoutConstraint
                                  constraintWithItem:btnNext
                                  attribute:NSLayoutAttributeWidth
                                  relatedBy:0
                                  toItem:btnNext
                                  attribute:NSLayoutAttributeHeight
                                  multiplier:1.0
                                  constant:0]];
    [viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(xMargin)-[btnPre]-(xMargin)-|" options:NSLayoutFormatAlignAllCenterY metrics:@{@"xMargin": @(1)} views:NSDictionaryOfVariableBindings(btnPre)]];
    
    [viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(xMargin)-[btnNext]-(xMargin)-|" options:NSLayoutFormatAlignAllCenterY metrics:@{@"xMargin": @(1)} views:NSDictionaryOfVariableBindings(btnNext)]];
    
    [viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(xMargin)-[pageControl]-(xMargin)-|" options:NSLayoutFormatAlignAllCenterY metrics:@{@"xMargin": @(1)} views:NSDictionaryOfVariableBindings(pageControl)]];
    
    
    [viewContainer addConstraint:[NSLayoutConstraint constraintWithItem:btnNext
                                                              attribute:NSLayoutAttributeTrailing
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:viewContainer
                                                              attribute:NSLayoutAttributeTrailing
                                                             multiplier:1.0
                                                               constant:-5 ]];
    
    [viewContainer addConstraint:[NSLayoutConstraint constraintWithItem:btnPre
                                                              attribute:NSLayoutAttributeLeading
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:viewContainer
                                                              attribute:NSLayoutAttributeLeading
                                                             multiplier:1.0
                                                               constant:5 ]];
    
    [viewContainer addConstraint:[NSLayoutConstraint constraintWithItem:pageControl
                                                              attribute:NSLayoutAttributeCenterX
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:viewContainer
                                                              attribute:NSLayoutAttributeCenterX
                                                             multiplier:1.0
                                                               constant:0]];
    
    tf.inputAccessoryView = viewInput;
    
    //set enable and disable
    if(self.indexCurrentSV == 0){
        btnPre.enabled = false;
    }
    else {
        btnPre.enabled = true;
        btnNext.enabled = true;
    }
    
    
}

-(void)setupBtnSMS_Token:(UIView*)viewContainer{
    
    
    //View Left
    UIView *viewLeft = [[UIView alloc] initWithFrame:CGRectZero];
    
    //View Right
    UIView *viewRight = [[UIView alloc] initWithFrame:CGRectZero];
    
    //BtnPre
    UIButton *btnSMS = [[UIButton alloc] initWithFrame:CGRectZero];
    [btnSMS addTarget:self action:@selector(btnSMSClick) forControlEvents:UIControlEventTouchUpInside];
    mBtnSMS = btnSMS;
    //BtnNext
    UIButton *btnToken = [[UIButton alloc] initWithFrame:CGRectZero];
    [btnToken addTarget:self action:@selector(btnTokenClick) forControlEvents:UIControlEventTouchUpInside];
    mBtnToken = btnToken;
    
    UILabel *lblSmS = [[UILabel alloc] initWithFrame:CGRectZero];
    lblSmS.text = kTitleBtnSMS;
    
    UILabel *lblToken = [[UILabel alloc] initWithFrame:CGRectZero];
    lblToken.text = kTitleBtnToken;
    
    //Title confirm
    UILabel *lblConfirm = [[UILabel alloc] initWithFrame:CGRectZero];
    lblConfirm.text = kTitleConfirmTransition;

    [viewContainer addSubview:viewLeft];
    [viewContainer addSubview:viewRight];
    [viewContainer addSubview:lblConfirm];
    
    
    [viewLeft addSubview:btnSMS];
    [viewLeft addSubview:lblSmS];
    
    [viewRight addSubview:btnToken];
    [viewRight addSubview:lblToken];
    
    
    btnSMS.backgroundColor = [UIColor clearColor];
    btnToken.backgroundColor = [UIColor clearColor];
    btnSMS.translatesAutoresizingMaskIntoConstraints = false;
    btnToken.translatesAutoresizingMaskIntoConstraints = false;
    
    [btnSMS setImage:[ZPResourceManager getImageWithName:@"ico_radioactive"] forState:UIControlStateSelected];
    [btnSMS setImage:[ZPResourceManager getImageWithName:@"ic_unchecked-mark"] forState:UIControlStateNormal];
    [btnToken setImage:[ZPResourceManager getImageWithName:@"ico_radioactive"] forState:UIControlStateSelected];
    [btnToken setImage:[ZPResourceManager getImageWithName:@"ic_unchecked-mark"] forState:UIControlStateNormal];
    btnSMS.selected = YES;
    
    lblSmS.translatesAutoresizingMaskIntoConstraints = false;
    lblToken.translatesAutoresizingMaskIntoConstraints = false;
    
    viewLeft.translatesAutoresizingMaskIntoConstraints = false;
    viewRight.translatesAutoresizingMaskIntoConstraints = false;
    lblConfirm.translatesAutoresizingMaskIntoConstraints = false;
    
    
    //View left
    
    [viewLeft addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(xMargin)-[btnSMS]-(xMargin)-|" options:NSLayoutFormatAlignAllCenterX metrics:@{@"xMargin": @(1)} views:NSDictionaryOfVariableBindings(btnSMS)]];
    [viewLeft addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(xMargin)-[lblSmS]-(xMargin)-|" options:NSLayoutFormatAlignAllCenterX metrics:@{@"xMargin": @(1)} views:NSDictionaryOfVariableBindings(lblSmS)]];
    
    [viewLeft addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(xMargin)-[btnSMS(==52)]-(-10)-[lblSmS]-(xMargin)-|" options:NSLayoutFormatAlignAllCenterY metrics:@{@"xMargin": @(1)} views:NSDictionaryOfVariableBindings(btnSMS,lblSmS)]];
    
    //View right
    [viewRight addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(xMargin)-[btnToken]-(xMargin)-|" options:NSLayoutFormatAlignAllCenterX metrics:@{@"xMargin": @(1)} views:NSDictionaryOfVariableBindings(btnToken)]];
    [viewRight addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(xMargin)-[lblToken]-(xMargin)-|" options:NSLayoutFormatAlignAllCenterX metrics:@{@"xMargin": @(1)} views:NSDictionaryOfVariableBindings(lblToken)]];
    
    [viewRight addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(xMargin)-[btnToken(==52)]-(-10)-[lblToken]-(xMargin)-|" options:NSLayoutFormatAlignAllCenterY metrics:@{@"xMargin": @(1)} views:NSDictionaryOfVariableBindings(btnToken,lblToken)]];
    
    //View left and right for view parent
    [viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(20)-[viewLeft]-(xMargin)-|" options:NSLayoutFormatAlignAllCenterX metrics:@{@"xMargin": @(1)} views:NSDictionaryOfVariableBindings(viewLeft)]];
    [viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(20)-[viewRight]-(xMargin)-|" options:NSLayoutFormatAlignAllCenterX metrics:@{@"xMargin": @(1)} views:NSDictionaryOfVariableBindings(viewRight)]];
    [viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(xMargin)-[viewLeft]-[viewRight]-(xMargin)-|" options:NSLayoutFormatAlignAllCenterY metrics:@{@"xMargin": @(1)} views:NSDictionaryOfVariableBindings(viewLeft,viewRight)]];
    [viewContainer addConstraint:[NSLayoutConstraint
                                  constraintWithItem:viewLeft
                                  attribute:NSLayoutAttributeWidth
                                  relatedBy:0
                                  toItem:viewRight
                                  attribute:NSLayoutAttributeWidth
                                  multiplier:1.0
                                  constant:0]];
    
    
    //Lbl Confirm
    [viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(10)-[lblConfirm]-(50)-|" options:NSLayoutFormatAlignAllCenterX metrics:@{@"xMargin": @(1)} views:NSDictionaryOfVariableBindings(lblConfirm)]];
    [viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(10)-[lblConfirm]-(xMargin)-|" options:NSLayoutFormatAlignAllCenterY metrics:@{@"xMargin": @(1)} views:NSDictionaryOfVariableBindings(lblConfirm)]];
    
    
}

-(void)setSizeWithOtherDevices{
    
    //Remove bottom constrain scrollview
    for (NSLayoutConstraint *con in self.contentView.constraints) {
        if (con.firstItem == self.contentView || con.secondItem == self.mScrollTextfeild) {
            if (con.constant == 8) {
                con.constant = 0;
            }
        }
    }
    
    if(IS_IPHONE_4_OR_LESS || IS_IPHONE_5){
        
        self.constraint_1_icon_number.constant = 5;
        self.constraint_1_number_date.constant = 20;
        self.constraint_1_date_name.constant = 0;
        kHeightInputView = 50;
        self.constraint_top_cardview.constant = 10;
        self.constraint_bottom_cardview.constant = 10;

        self.constraint_height_numberlbl.constant = 30;
        
        
        self.constraint_width_icon.constant = 60;
        self.constraint_height_icon.constant = 20;
        
        self.constraint_width_icon_back.constant =  60;
        self.constraint_height_icon_back.constant =  20;
    }
    else {
        self.constraint_1_icon_number.constant = 10;
        self.constraint_1_number_date.constant = 30;
        self.constraint_1_date_name.constant = 5;
        self.constraint_top_cardview.constant = 15;
        self.constraint_bottom_cardview.constant = 15;
        
        kHeightInputView = 120;

        self.constraint_height_numberlbl.constant = 30;
        
        self.constraint_width_icon.constant = 75;
        self.constraint_height_icon.constant = 25;
        
        self.constraint_width_icon_back.constant =  75;
        self.constraint_height_icon_back.constant =  25;
        
    }
    if(IS_IPHONE_4_OR_LESS){
        self.constraint_top_cardview.constant = 0;
        self.constraint_bottom_cardview.constant = 5;
        
        self.constraint_height_icon.constant = 0;
        self.constraint_width_icon.constant = 0;
        self.constraint_1_icon_number.constant = 0;
        self.constraint_1_number_date.constant = 0;
        
        self.mImgIconBank.hidden = true;
        
    }
    if(IS_IPHONE_6){
        self.constraint_width_icon.constant = 90;
        self.constraint_height_icon.constant = 30;
        
        self.constraint_1_icon_number.constant = 15;
        self.constraint_1_number_date.constant = 20;
        
        
    }
    if(IS_IPHONE_6P){
        self.constraint_width_icon.constant = 105;
        self.constraint_height_icon.constant = 35;
        
        self.constraint_1_icon_number.constant = 20;
        self.constraint_1_number_date.constant = 30;
    }
    
    
    [self setNeedsLayout];
    
}
#pragma mark - Others

-(void)scrollToIndex:(int)index{
    [self.mScrollTextfeild setContentOffset: CGPointMake(self.mScrollTextfeild.frame.size.width * index, 0) animated:true];
    
}
-(void)firstResponderKeyboardTextField:(int)index{
    lastIndexSV = index;
    [self setTimerFirstBecome];
    
}
-(void)firstResponderKeyboardTextFieldByTF{
    //Hide KB Bank is maintance
    if ([ZPDialogView isDialogShowing]) {
        return;
    }
    
    ZPATMSaveCardObject *mdGet = self.arrTF[lastIndexSV];
    if(self.isEndDecelerating) {
        [mdGet.mTextField becomeFirstResponder];
        self.isScrolling = NO;
    }
}
-(void)firstResponderKeyboardTextField_Old:(int)index{
    ZPATMSaveCardObject *mdGet = self.arrTF[index];
    lastIndexSV = index;
    double delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        if(self.isEndDecelerating) {
            [mdGet.mTextField becomeFirstResponder];
            self.isScrolling = NO;
        }
    });
    
}
-(void)firstResponderKeyboardTextFieldCurrent{
    [self firstResponderKeyboardTextField:self.indexCurrentSV];
}
-(void)resignKeyboardTextField:(int)index{
    ZPATMSaveCardObject *mdGet = self.arrTF[index];
    [mdGet.mTextField resignFirstResponder];
    
}

-(void)changeTextOnCardView:(UITextField*)tf{
    
    [self hightLightLabelOnCard_NotUse:tf];
    
    int indexChange = (int)tf.tag;
    
    //for(int i = 0 ; i < arrTF.count ; i++){
    
    ZPATMSaveCardObject *mdGet = self.arrTF[indexChange];
    if(mdGet.mType == ZPATMSaveCardObjectType_CardNumber){
        [self checkAutocomplete:mdGet.mTextField.text andType:mdGet.mType];
        
        //Check
        if ([self.bankCode length] <= 0 && [mdGet.mTextField.text length] >= 6 + 1) {
            if (self.bankCode != nil && [self.bankCode length] > 0) {
            } else {
                
                [(JVFloatLabeledTextField*)mdGet.mTextField setFloatingTitleErrorWithImage:@"Chưa hỗ trợ ngân hàng này" andImage:[ZPResourceManager getImageWithName:@"question"]];
                [self setupFloatingLabelShowBankSupport:(JVFloatLabeledTextField*)mdGet.mTextField];
            }
        }
        
        double delayInSeconds = 0.2;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self setFontSizeByLength:mdGet.mTextField];
        });
        
        if(mdGet.mTextField.text.length == 0){
            [self resetAll];
        }
        if(self.bankCode == nil || self.bankCode.length == 0){
            self.mLblCardExpire.text = @"";
            [self resetAll];
        }
        else {
            JVFloatLabeledTextField *tfDate = [self getTextFieldByType:ZPATMSaveCardObjectType_CardExpire];
            if(tfDate.text.length == 0){
                self.mLblCardExpire.text = self.titleExpireDefault;
            }
        }
        [self getFormatXHighLight:mdGet.mTextField andDefault:false];
        return;
    }
    if(mdGet.mType == ZPATMSaveCardObjectType_CardName){
        self.mLblCardName.text =  mdGet.mTextField.text;
        [self getFormatXHighLight:mdGet.mTextField andDefault:false];
        return;
    }
    if(mdGet.mType == ZPATMSaveCardObjectType_CardExpire){
        self.mLblCardExpire.text =  [self getFormatXXXXDefaultDate:mdGet.mTextField.text] ;
        if(mdGet.mTextField.text.length == 0){
            self.mLblCardExpire.text = kFormatDefaultDate;
        }
        
        //Add slash
        NSString *string = mdGet.mTextField.text;
        if (string.length == 2) {
            
            mdGet.mTextField.text = [string stringByAppendingString:@"/"];
            
        }
        
        [self checkValidateDate:self.mLblCardExpire.text];
        
        NSString *errorMessage = @"";
        if (!isCardDateValid && mdGet.mTextField.text.length == kFormatDefaultDate.length) {
            errorMessage = @"Ngày hết hạn không hợp lệ";
            [self textFieldShowError:(JVFloatLabeledTextField*)mdGet.mTextField withErrorText:errorMessage];
        }
        [self getFormatXHighLight:mdGet.mTextField andDefault:false];
        [self checkAutocomplete:mdGet.mTextField.text andType:mdGet.mType];
        
        return;
    }
    if(mdGet.mType == ZPATMSaveCardObjectType_CardCVV){
        self.mLblCardCVV.text =  mdGet.mTextField.text;
        [self getFormatXHighLight:mdGet.mTextField andDefault:false];
        [self checkAutocomplete:mdGet.mTextField.text andType:mdGet.mType];
        
        return;
    }
    
    
    //}
}

-(void)updateTextForTfByType:(enum ZPATMSaveCardObjectType)type withText:(NSString*)text{
    
    for(int i = 0 ; i < self.arrTF.count ; i++){
        
        ZPATMSaveCardObject *mdGet = self.arrTF[i];
        
        if(mdGet.mType == type){
            mdGet.mTextField.text =  text;
            break;
        }
        
    }
    
}
-(JVFloatLabeledTextField*)getTextFieldByType:(enum ZPATMSaveCardObjectType)type{
    
    for(int i = 0 ; i < self.arrTF.count ; i++){
        
        ZPATMSaveCardObject *mdGet = self.arrTF[i];
        
        if(mdGet.mType == type){
            return (JVFloatLabeledTextField*)mdGet.mTextField;
        }
        
    }
    JVFloatLabeledTextField *tf = [[JVFloatLabeledTextField alloc] init];
    if(type == ZPATMSaveCardObjectType_CardAmount) tf.text = [NSString stringWithFormat:@"%ld",self.suggestAmount];
    return tf;
}
-(enum ZPATMSaveCardObjectType)getTypeByTextField:(UITextField*)tf{
    
    for(int i = 0 ; i < self.arrTF.count ; i++){
        
        ZPATMSaveCardObject *mdGet = self.arrTF[i];
        
        if(mdGet.mTextField == tf){
            return mdGet.mType;
        }
        
    }
    return -1;
}
-(void)SetBecomeFirstScrollTF:(UITextField*)tf{
    
    
    for(int i = 0 ; i < self.arrTF.count ; i++){
        
        ZPATMSaveCardObject *mdGet = self.arrTF[i];
        
        if(mdGet.mTextField == tf){
            
            self.indexCurrentSV = i;
            [self scrollToIndex:i];
            
            double delayInSeconds = .5;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [mdGet.mTextField becomeFirstResponder];
            });
            
            break;
        }
        
    }
}
-(void)scrollToCell:(CGFloat)heightKeyboard{
}
-(void)textFieldShowError:(JVFloatLabeledTextField *)textField withErrorText:(NSString *)errorText{
    [textField setAlwaysShowFloatingLabel:YES];
    [textField setFloatingTitleWithError:errorText];
    textField.isShowTailImage = FALSE;
}
-(void)textFieldNonError:(JVFloatLabeledTextField *)textField{
    [textField setAlwaysShowFloatingLabel:YES];
    textField.isShowError = NO;
    textField.isShowTailImage = FALSE;
    //Reset floating label
    [self resetFloatingLabelPlaceHolder:textField];
    
}
- (void) formatTextField: (UITextField *) textField  {
    NSString * text = textField.text;
    NSString * textWithoutFormat = [[textField.text zpRemoveFormat:formats] zpTrimSpace];
    UITextRange *selRange = textField.selectedTextRange;
    UITextPosition *selStartPos = selRange.start;
    NSInteger cursorOffset = [textField offsetFromPosition:textField.beginningOfDocument toPosition:selStartPos];
    
    NSString * leftString = [text substringToIndex:cursorOffset];
    leftString = [leftString zpFormatWithFormat:formats separator:nil];
    cursorOffset = leftString.length;
    
    textField.text = [textWithoutFormat zpFormatWithFormat:formats separator:nil];
    
    UITextPosition *newCursorPosition = [textField positionFromPosition:textField.beginningOfDocument offset:cursorOffset];
    if(newCursorPosition) {
        UITextRange *newSelectedRange = [textField textRangeFromPosition:newCursorPosition toPosition:newCursorPosition];
        [textField setSelectedTextRange:newSelectedRange];
    }
}
- (void) formatTextFieldDate: (UITextField *) textField  {
    NSString * text = textField.text;
    NSString * textWithoutFormat = [[textField.text zpRemoveFormat:formatsDate] zpTrimSpace];
    UITextRange *selRange = textField.selectedTextRange;
    UITextPosition *selStartPos = selRange.start;
    NSInteger cursorOffset = [textField offsetFromPosition:textField.beginningOfDocument toPosition:selStartPos];
    
    NSString * leftString = [text substringToIndex:cursorOffset];
    leftString = [leftString zpFormatWithFormat:formatsDate separator:nil];
    cursorOffset = leftString.length;
    
    textField.text = [textWithoutFormat zpFormatWithFormat:formatsDate separator:nil];
    
    UITextPosition *newCursorPosition = [textField positionFromPosition:textField.beginningOfDocument offset:cursorOffset];
    if(newCursorPosition) {
        UITextRange *newSelectedRange = [textField textRangeFromPosition:newCursorPosition toPosition:newCursorPosition];
        [textField setSelectedTextRange:newSelectedRange];
    }
}
-(void)resetFloatingLabelPlaceHolder:(UITextField *) textField{
    
    for (int i = 0 ; i< self.arrTF.count ; i++) {
        ZPATMSaveCardObject *mdGet = self.arrTF[i];
        if(mdGet.mTextField == textField){
            
            JVFloatLabeledTextField *titleField = (JVFloatLabeledTextField*)textField;
            if([mdGet.mPlaceHolder isEqualToString:@""]){
                [titleField setPlaceholder:mdGet.mTitle floatingTitle:mdGet.mTitle];
            }
            else {
                [titleField setPlaceholder:mdGet.mPlaceHolder floatingTitle:mdGet.mTitle];
            }
            [titleField setPlaceholderOnly:@""];
        }
    }
}
-(NSString*)getFormatXXXXDefaultNumber:(NSString*)strTF{
    
    NSString *strTFGet = [strTF zpFormatWithFormat:formats separator:nil];
    
    NSString *strDefault = kFormatDefaultNumber;
    if(strTFGet.length >= strDefault.length) return strTFGet;
    NSRange range = NSMakeRange(0,strTFGet.length);
    NSString *newText = [strDefault stringByReplacingCharactersInRange:range withString:strTFGet];
    return newText;
    
}
-(void)getFormatXHighLight:(UITextField*)tf andDefault:(BOOL)isDefault{
    enum ZPATMSaveCardObjectType type =  [self getTypeByTextField:tf];
    NSString *strTF = tf.text;
    NSString *strTFGet = @"";
    NSString *strTFGetStart = @"";
    if(type == ZPATMSaveCardObjectType_CardNumber){
        strTFGet = [self getFormatXXXXDefaultNumber:strTF];
        strTFGetStart = [strTF zpFormatWithFormat:formats separator:nil];
    }
    else if (type == ZPATMSaveCardObjectType_CardExpire){
        strTFGet = [self getFormatXXXXDefaultDate:strTF];
        strTFGetStart = [strTF zpFormatWithFormat:formatsDate separator:nil];
        
        if(strTF.length == 0){
            strTFGet = self.titleExpireDefault;
        }
    }
    else if (type == ZPATMSaveCardObjectType_CardName){
        strTFGet = strTF;
        strTFGetStart = strTF;
        if(strTF.length == 0){
            strTFGet = titleNameDefault;
        }
    }
    else if (type == ZPATMSaveCardObjectType_CardCVV){
        strTFGet = strTF;
        strTFGetStart = strTF;
    }
    int lengthCheck = (int)strTFGetStart.length;
    if(strTF.length == 0){
        lengthCheck = 0;
    }
    
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:strTFGet];
    int checkLength = lengthCheck-1;
    if(checkLength < 0)
        lengthCheck = 0;
    else {
        lengthCheck --;
    }
    
    NSRange foundRange = NSMakeRange(lengthCheck, 1);
    if(foundRange.location >= attr.length || strTF.length == 0)
    {
        //return attr;
        [self setAttributeTextForLabel:type andAttr:attr];
        return;
    }
    NSString *strColor = kColorTextChange;
    if(self.bankCode.length > 0){
        strColor = [ZPResourceManager bankValueFromBankCodeBundle:@"bank_texttyping" andBankCode:self.bankCode andIsBankOrCC:false];
    }
    if (foundRange.location != NSNotFound)
    {
        [attr beginEditing];
        if(!isDefault)
            [attr addAttribute:NSForegroundColorAttributeName value:[UIColor zpColorFromHexString:strColor] range:foundRange];
        [attr endEditing];
    }
    
    [self setAttributeTextForLabel:type andAttr:attr];
    
}
-(void)setAttributeTextForLabel:(enum ZPATMSaveCardObjectType)type andAttr:(NSMutableAttributedString*)attr{
    if(type == ZPATMSaveCardObjectType_CardNumber){
        self.mLblCardNumber.attributedText = attr;
    }
    else if (type == ZPATMSaveCardObjectType_CardExpire){
        self.mLblCardExpire.attributedText = attr;
    }
    else if (type == ZPATMSaveCardObjectType_CardName ){
        self.mLblCardName.attributedText = attr;
    }
    else if (type == ZPATMSaveCardObjectType_CardCVV){
        self.mLblCardCVV.attributedText = attr;
    }
}
-(NSString*)getFormatXXXXDefaultDate:(NSString*)strTF{
    
    NSString *strTFGet = [strTF zpFormatWithFormat:formatsDate separator:nil];
    
    NSString *strDefault = kFormatDefaultDate;
    if(strTFGet.length >= strDefault.length) return strTFGet;
    NSRange range = NSMakeRange(0,strTFGet.length);
    NSString *newText = [strDefault stringByReplacingCharactersInRange:range withString:strTFGet];
    return newText;
}
-(void)checkAutocomplete:(NSString*)strTF andType:(enum ZPATMSaveCardObjectType)type{
    
    if(self.bankCode.length > 0){
        
        int maxlengthNumber = 0;
        
        if(type == ZPATMSaveCardObjectType_CardNumber){
            maxlengthNumber = [[ZPResourceManager bankValueFromBankCodeBundle:@"bank_completelength" andBankCode:self.bankCode andIsBankOrCC:false] intValue];
            
            //Check Save Card and Link Card
            if([self isSaveCardBefore:[[strTF zpRemoveFormat:formats] zpTrimSpace] andTf:[self getTextFieldByType:type]])
                return;
            
            if([strTF zpRemoveFormat:formats].length == maxlengthNumber){
                [self btnNextOnAccessoryViewClick];
            }
            
            if (![self checkInPutCardIsLuhnValid:[[strTF zpRemoveFormat:formats] zpTrimSpace]]) {
                return;
            }
            else {
                int lengthMax = [[ZPResourceManager bankValueFromBankCodeBundle:@"bank_maxlength" andBankCode:self.bankCode andIsBankOrCC:false] intValue];
                
                //Show error save card before
                int lengthMin = [[ZPResourceManager bankValueFromBankCodeBundle:@"bank_minlength" andBankCode:self.bankCode andIsBankOrCC:false] intValue];
                if([strTF zpRemoveFormat:formats].length == lengthMax || [strTF zpRemoveFormat:formats].length == lengthMin){
                    [self btnNextOnAccessoryViewClick];
                }
            }
            
        }
        else if (type == ZPATMSaveCardObjectType_CardCVV){
            maxlengthNumber = 3;
            
            if(strTF.length == maxlengthNumber){
                
                [self btnNextOnAccessoryViewClick];
            }
        }
        else if (type == ZPATMSaveCardObjectType_CardExpire ){
            maxlengthNumber = 4;
            
            if([strTF zpRemoveFormat:formatsDate].length == maxlengthNumber && isCardDateValid){
                [self btnNextOnAccessoryViewClick];
            }
            
        }
        
        if(maxlengthNumber == 0) return;
        
        
        
    }
}
-(void)updatePageControl:(UITextField*)tf{
    [self updatePageControl];
    
}
-(void)updatePageControl{
    
    if(pageControlCur != nil){
        pageControlCur.numberOfPages = self.arrTF.count;
        [pageControlCur setCurrentPage:self.indexCurrentSV];
    }
    
}
-(void)updatePageControlByIndex:(int)index{
    
    if(pageControlCur != nil){
        pageControlCur.numberOfPages = self.arrTF.count;
        [pageControlCur setCurrentPage:index];
    }
    
}
-(void) setTimerDecelerating{
    self.isEndDecelerating = FALSE;
    [timerDelayNextPreClicked invalidate];
    timerDelayNextPreClicked  = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(enableDecelerating) userInfo:nil repeats:NO];
}
-(void) setTimerEnableScroll{
    self.mScrollTextfeild.scrollEnabled = NO;
    [timerEnableScroll invalidate];
    timerEnableScroll  = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(tapTF) userInfo:nil repeats:NO];
}
-(void) setTimerFirstBecome{
    [timerFirstBecome invalidate];
    timerFirstBecome  = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(firstResponderKeyboardTextFieldByTF) userInfo:nil repeats:NO];//[NSTimer
}

-(void) enableDecelerating {
    self.isEndDecelerating = TRUE;
}

-(void) scrollToIndexByType:(enum ZPATMSaveCardObjectType )type{
    self.isEndDecelerating = FALSE;
    for (int i = 0 ; i< self.arrTF.count ; i++) {
        ZPATMSaveCardObject *mdGet = self.arrTF[i];
        if(mdGet.mType == type){
            
            self.indexCurrentSV = i;
            [self scrollToIndex:self.indexCurrentSV];
            [self firstResponderKeyboardTextField:self.indexCurrentSV];
            
            [self setTimerDecelerating];
            break;
        }
    }
}
-(void)getHeightInputView{
    // height = screen_height - (height_keyboard + statusbar + navbar + cellHeight)
    CGFloat cellHeight = (SCREEN_WIDTH - kMarginCardView) * (1 / ratioCardView) + cardViewScrollHeight;
    if(IS_IPHONE_4_OR_LESS){
        cellHeight = (SCREEN_WIDTH - kMarginCardView) * (1 / (ratioCardView + 1)) + cardViewScrollHeight;
        
    }
    CGFloat height = SCREEN_HEIGHT - (heightKeyboard + cellHeight + 66);
    kHeightInputView = height;
    if(!isViewBackClicked)
        [self setupInputViewForTFFRame];
}
-(void)setFontSizeByLength:(UITextField*)tf{
    [self getFontSizeNumber];
}
-(void)hightLightLabelOnCardDefault{
    
    //NSString *strColorNormal = kColorNormalLabel;
    NSString *strColorHighLight = kColorHightLightLabel;
    
    self.mLblCardNumber.textColor = [UIColor zpColorFromHexString:strColorHighLight];
    self.mLblCardName.textColor = [UIColor zpColorFromHexString:strColorHighLight];
    self.mLblCardExpire.textColor = [UIColor zpColorFromHexString:strColorHighLight];
    
    
}
-(void)hightLightLabelOnCard_NotUse:(UITextField*)tf{
    
    NSString *strColorNormal = kColorNormalLabel;
    NSString *strColorHighLight = kColorHightLightLabel;
    
    self.mLblCardNumber.textColor = [UIColor zpColorFromHexString:strColorHighLight];
    self.mLblCardName.textColor = [UIColor zpColorFromHexString:strColorHighLight];
    self.mLblCardExpire.textColor = [UIColor zpColorFromHexString:strColorHighLight];
    
    
    if(self.bankCode.length > 0){
        strColorNormal = [ZPResourceManager bankValueFromBankCodeBundle:@"bank_textunhighlight" andBankCode:self.bankCode andIsBankOrCC:false];
        strColorHighLight = [ZPResourceManager bankValueFromBankCodeBundle:@"bank_texthighlight" andBankCode:self.bankCode andIsBankOrCC:false];
        
    }
    self.mLblCardName.textColor = [UIColor zpColorFromHexString:strColorNormal];
    self.mLblCardExpire.textColor = [UIColor zpColorFromHexString:strColorNormal];
    
    for (int i = 0 ; i< self.arrTF.count ; i++) {
        ZPATMSaveCardObject *mdGet = self.arrTF[i];
        if(mdGet.mTextField == tf){
            
            switch (mdGet.mType) {
                case ZPATMSaveCardObjectType_CardNumber:
                    self.mLblCardNumber.textColor = [UIColor zpColorFromHexString:strColorHighLight];
                    break;
                case ZPATMSaveCardObjectType_CardName:
                    self.mLblCardName.textColor = [UIColor zpColorFromHexString:strColorHighLight];
                    break;
                case ZPATMSaveCardObjectType_CardExpire:
                    self.mLblCardExpire.textColor = [UIColor zpColorFromHexString:strColorHighLight];
                    break;
                case ZPATMSaveCardObjectType_CardCVV:
                    break;
                    
                default:
                    break;
            }
            
            break;
        }
    }
    
    
    
}

-(void)updatePreNextEnable{
    
    UIButton *btnPre = [viewPreNext viewWithTag:1];
    UIButton *btnNext = [viewPreNext viewWithTag:2];
    
    //set enable and disable
    if(self.indexCurrentSV == 0){
        btnPre.enabled = false;
        
    }
    else {
        
        btnPre.enabled = true;
        btnNext.enabled = true;
    }
    
    
}
-(void)getFontSizeNumber{
    
    float largestFontSize  = 50;
    NSString *text = self.mLblCardNumber.text;
    if(text.length == 0) return;
    
    self.mLblCardNumber.backgroundColor = [UIColor clearColor];
    
    CGFloat width = self.mLblCardNumber.frame.size.width;
    if(!kHSizeNumberFontBig) width = SCREEN_WIDTH - (20 + 35) * 2;
    
    NSString *fontPath = [[ZPDataManager sharedInstance].bundle pathForResource:kFontNumberCardFile ofType:@"ttf"];
    if(fontPath.length == 0){
        while ([text sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:largestFontSize]}].width > width)
        {
            largestFontSize -= 0.1;
        }
        self.mLblCardNumber.font = [UIFont systemFontOfSize:largestFontSize];
    }
    else {
        while ([text sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:kFontNumberCard size:largestFontSize]}].width > width)
        {
            largestFontSize -= 0.1;
        }
        self.mLblCardNumber.font = [UIFont fontWithName:kFontNumberCard size:largestFontSize];
    }
    kHSizeNumberFontBig = largestFontSize;
    kHSizeNumberFontSmall = kHSizeNumberFontBig - 2;
    
}
-(void)resetAll{
    self.bankCode = @"";
    //Card View
    self.mImgIconBank.image = [UIImage imageNamed:@""];
    self.mImgIconBank.backgroundColor = [UIColor lightGrayColor];
    [self setGradientColorForCardView:self.bankCode];
    //self.mLblCardNumber.text = kFormatDefaultNumber;
    self.mLblCardExpire.text = @"";
    self.mLblCardName.text = titleNameDefault;
    
    //Color
    NSString *strColorNormal = kColorNormalLabel;
    NSString *strColorHighLight = kColorHightLightLabel;
    self.mLblCardNumber.textColor = [UIColor zpColorFromHexString:strColorHighLight];
    self.mLblCardName.textColor = [UIColor zpColorFromHexString:strColorNormal];
    self.mLblCardExpire.textColor = [UIColor zpColorFromHexString:strColorNormal];
    
    //Input
    for (int i = 1; i < self.arrTF.count; i++) {
        ZPATMSaveCardObject *mdGet = self.arrTF[i];
        mdGet.mTextField.text= @"";
    }
}
- (void) hideKeyBoard:(NSNotification *)note{
    [self resignKeyboardTextField:self.indexCurrentSV];
    isHideKeyBoard = YES;
}
- (void) hideBtnPreNext:(NSNotification *)note{
    [self removeBtnPreNext];
}
-(BOOL)isSaveCardBefore:(NSString*)detectString andTf:(JVFloatLabeledTextField*)tfNumber{
    
    //Check Save Card and Link Card
    if (self.delegate && [self.delegate respondsToSelector:@selector(isAtmMapcard)]) {
        if ([self.delegate isAtmMapcard]) {
            
            if(self.bankCode.length > 0){
                if (detectString.length == kLengthCheckExistMin || detectString.length == kLengthCheckExistMax) {
                    if([[ZPDataManager sharedInstance] checkSavedCardIsExist:detectString]){
                        NSDictionary * bank = [[ZPDataManager sharedInstance].bankManager.ccBanksDict objectForKey:self.bankCode];
                        NSString * ccBankName = [bank stringForKey:stringKeyCCBankName];
                        NSString *last4No = [detectString substringFromIndex:MAX((int)[detectString length]-4, 0)];
                        
                        NSString *title = [NSString stringWithFormat:stringMethodNameFormat,ccBankName, last4No];
                        [self textFieldShowError:tfNumber withErrorText:[NSString stringWithFormat:stringErrorCardIsExist, title]];

                        return YES;
                    }
                }
            }
            
        }
        
    }
    return NO;
}
-(void)removeBtnPreNext{
    
    if(viewPreNext){
        [viewPreNext removeFromSuperview];
        viewPreNext = nil;
        [self  UnRegisterForKeyboardNotifications];
        [ZPManagerSaveCardCell sharedInstance].isShowAMTCardCell = false;
    }
    
}
-(JVFloatLabeledTextField*)getCurrentTf{
    ZPATMSaveCardObject *mdGet = self.arrTF[lastIndexSV];
    return (JVFloatLabeledTextField*)mdGet.mTextField;
}
#pragma mark - Animations
-(void)flipByIndexTextField{
    
    ZPATMSaveCardObject *mdGet = self.arrTF[self.indexCurrentSV];
    
    if(mdGet.mType == ZPATMSaveCardObjectType_CardCVV){
    
        [self animatedFlip:self.mViewCardBG andViewBack:self.mViewCardBackCVV andLeft:true];
    }
    else if(mdGet.mType != ZPATMSaveCardObjectType_CardCVV) {
        [self animatedFlip:self.mViewCardBG andViewBack:self.mViewCardBackCVV andLeft:false];
    }
    
}

-(void) animatedFlip:(UIView*)viewFront andViewBack:(UIView*)viewBack andLeft:(BOOL)isLeftToRight{
    
    if(self.isFlippingViewCard) return;
    self.isFlippingViewCard = true;
    
    isCurrentOrientationAniFlip = isLeftToRight;
    if(LastOrientationAniFlip == isCurrentOrientationAniFlip){ self.isFlippingViewCard = false; return ; };
    LastOrientationAniFlip = isCurrentOrientationAniFlip;
    UIViewAnimationOptions option = UIViewAnimationOptionTransitionFlipFromLeft;
    if(!isLeftToRight){
        viewBack.alpha = 1;
        option = UIViewAnimationOptionTransitionFlipFromRight;
    }
    else {
        viewBack.alpha = 0;
    }
    [UIView transitionWithView:viewFront duration:0.5 options:option animations:^{
        
        if(!isLeftToRight){
            
            viewBack.alpha = 0;
        }
        else {
            viewBack.alpha = 1;
        }
        
    } completion:^(BOOL finished) {
        self.isFlippingViewCard = false;
    }];
    
}
-(void) animatedFlipOld:(UIView*)viewFront andViewBack:(UIView*)viewBack andLeft:(Boolean)isLeftToRight{
    
    
    isCurrentOrientationAniFlip = isLeftToRight;
    CATransform3D transform = CATransform3DIdentity;
    transform.m34 = 0.0015;
    viewFront.layer.transform = transform;
    
    CGFloat kAngle = -M_PI / 2;
    
    if(!isLeftToRight){
        kAngle = M_PI / 2;
        viewBack.alpha = 1;
    }
    else {
        viewBack.alpha = 0;
    }
    
    [CATransaction begin]; {
        
        [CATransaction setCompletionBlock:^{
            
            if(!isLeftToRight){
                
                viewBack.alpha = 0;
            }
            else {
                viewBack.alpha = 1;
            }
            self.mViewCardBG.transform = CGAffineTransformMakeScale(-1, 1);
            [CATransaction begin]; {
                
                [CATransaction setCompletionBlock:^{
                    self.mViewCardBG.transform = CGAffineTransformMakeScale(1, 1);
                }];
                
                CABasicAnimation *animation = [CABasicAnimation
                                               animationWithKeyPath:@"transform.rotation.y"];
                animation.fromValue = @(kAngle);
                animation.toValue = @(kAngle * 2);
                animation.duration = 0.2;
                [viewFront.layer addAnimation:animation forKey:animation.keyPath];
            } [CATransaction commit];
            
        }];
        
        CABasicAnimation *animation = [CABasicAnimation
                                       animationWithKeyPath:@"transform.rotation.y"];
        animation.fromValue = @0;
        animation.toValue = @(kAngle);
        animation.duration = 0.4;
        
        [viewFront.layer addAnimation:animation forKey:animation.keyPath];
        
        
    } [CATransaction commit];
    
    
}

-(void)animationFadeInOut:(UIView*)view{
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.2f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    
    [view.layer addAnimation:transition forKey:nil];
}
#pragma mark - Date and List Picker
- (ZPMonthYearPicker*)getDatePicker{
    if (!datePickerView) {
        
        datePickerView = [[ZPMonthYearPicker alloc] init];
        datePickerView.pickerDelegate = self;
        [datePickerView setCurrentDate];
    }
    
    return datePickerView;
    
}
- (ZPPickerList*)getListConfirmTransitionPicker{
    if (!listConfirmTransitionPickerView) {
        
        listConfirmTransitionPickerView = [[ZPPickerList alloc] init];
        listConfirmTransitionPickerView.arrData= [NSMutableArray arrayWithObjects:kTitleBtnSMS,kTitleBtnToken, nil];
        listConfirmTransitionPickerView.pickerDelegate = self;
        
    }
    
    return listConfirmTransitionPickerView;
    
}
- (void)onChangeMonth:(int)m year:(int)y{
    
    self.mLblCardExpire.text = [NSString stringWithFormat:@"%02d/%02d", m,y];
    [self updateTextForTfByType:ZPATMSaveCardObjectType_CardExpire withText:self.mLblCardExpire.text];
    
    
    NSDate * date = [NSDate date];
    NSCalendar *calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    int month = (int)[[calendar components:NSCalendarUnitYear|NSCalendarUnitMonth fromDate:date] month];
    int year = (int)[[calendar components:NSCalendarUnitYear|NSCalendarUnitMonth fromDate:date] year] - 2000;
    DDLogInfo(@"y year m month %d.%d.%d.%d",y,year,m,month);
    
    JVFloatLabeledTextField *tDate = [self getTextFieldByType:ZPATMSaveCardObjectType_CardExpire];
    if (y < year) {
        isCardDateValid = NO;
    } else if (y == year && m < month) {
        isCardDateValid = NO;
    } else {
        isCardDateValid = YES;
    }
    if (isCardDateValid) {
        [self textFieldNonError:tDate];
        
    } else {
        [self textFieldShowError:tDate withErrorText:@"Ngày hết hạn không hợp lệ"];
    }
}
- (void)ZPPickerListDelegate_onChangeData:(int) index arr:(NSMutableArray*) arr{
    
    if(index == kIndexBtnSMS){
        [self resetAllBtnSelected];
        [mBtnSMS setSelected:true];
    }
    if(index == kIndexBtnToken){
        [self resetAllBtnSelected];
        [mBtnToken setSelected:true];
    }
    
}
#pragma mark - Clear
-(void)clearSubviewsScrollView{
    //Clear all
    while (self.arrTF.count > 1){
        [self.arrTF removeLastObject];
    }
    
    for (UIView *subView in self.mScrollTextfeild.subviews) {
        if(subView == self.mScrollTextfeild.subviews[0]){
            break;
        }
        [subView removeFromSuperview];
    }
}
#pragma mark - Events
-(void)btnPreOnAccessoryViewClick{
    if(!self.isEndDecelerating) return;
    if(self.indexCurrentSV == 0 ) return;
    ZPATMSaveCardObject *mdGet = self.arrTF[self.indexCurrentSV];
    enum ZPATMSaveCardObjectType type = mdGet.mType;
    switch(type){
        case ZPATMSaveCardObjectType_CardExpire :
            [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionLinkbank_add_bank_date_back];
            break;
        case ZPATMSaveCardObjectType_CardName :
            [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionLinkbank_add_bank_name_back];
            break;
        default :
            break;
    }
    self.indexCurrentSV--;
    self.isScrolling = YES;
    [self flipByIndexTextField];
    [self scrollToIndex:self.indexCurrentSV];
    [self firstResponderKeyboardTextField:self.indexCurrentSV];
    
    [self setTimerDecelerating];
}
-(void)btnNextOnAccessoryViewClick{
    if(!self.isEndDecelerating) return;
    if(self.indexCurrentSV + 1 == self.arrTF.count ){
        //Done
        if([self checkValidateTextFieldLastest] == false) return;
        //click btnNext when input CardName
        [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionLinkbank_add_bank_name_forward];
        [self resignKeyboardTextField:lastIndexSV];
        return;
    }
    if([self checkValidateTextFieldByType:self.indexCurrentSV andShowError:true] == false){
        return;
    }
    ZPATMSaveCardObject *mdGet = self.arrTF[self.indexCurrentSV];
    enum ZPATMSaveCardObjectType type = mdGet.mType;
    switch(type){
        case ZPATMSaveCardObjectType_CardNumber :
            [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionLinkbank_add_bank_cardnumber_forward];
            break;
        case ZPATMSaveCardObjectType_CardExpire :
            [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionLinkbank_add_bank_date_forward];
            break;
        default:
            break;
    }
    self.indexCurrentSV++;
    self.isScrolling = YES;
    [self flipByIndexTextField];
    [self scrollToIndex:self.indexCurrentSV];
    [self firstResponderKeyboardTextField:self.indexCurrentSV];
    
    [self setTimerDecelerating];
    
}
-(void)btnDoneOnAccessoryViewClick{
    
    [self resignKeyboardTextField:self.indexCurrentSV];
    
}
-(void)btnSMSClick{
    [self resetAllBtnSelected];
    [mBtnSMS setSelected:true];
    
    choseAuthMethod = choseFirstMethod;
    
    ZPPickerList *picker = [self getListConfirmTransitionPicker];
    [picker selectIndexCurrent:kIndexBtnSMS];
}
-(void)btnTokenClick{
    
    [self resetAllBtnSelected];
    [mBtnToken setSelected:true];
    
    choseAuthMethod = choseSecondMethod;
    
    ZPPickerList *picker = [self getListConfirmTransitionPicker];
    [picker selectIndexCurrent:kIndexBtnToken];
    
}
-(void)resetAllBtnSelected{
    [mBtnToken setSelected:false];
    [mBtnSMS setSelected:false];
}
#pragma mark - Overrides
- (void)updateLayouts {
    
    if([ZPManagerSaveCardCell sharedInstance].isShowCreditCardCell){
        [self registerForKeyboardNotifications];
    }
    
}
- (BOOL)checkValidate {
    
    return [self textFieldCheckValidate];
    
}

- (NSDictionary *)outputParams {
    
    NSMutableDictionary * params = [[NSMutableDictionary alloc] init];
    
    JVFloatLabeledTextField *tfNumber = [self getTextFieldByType:ZPATMSaveCardObjectType_CardNumber];
    JVFloatLabeledTextField *tfName = [self getTextFieldByType:ZPATMSaveCardObjectType_CardName];
    JVFloatLabeledTextField *tfCVV = [self getTextFieldByType:ZPATMSaveCardObjectType_CardCVV];
    JVFloatLabeledTextField *tfDate = [self getTextFieldByType:ZPATMSaveCardObjectType_CardExpire];
    
    if (self.bankCode.length > 0) {
        [params setObject:self.bankCode forKey:@"ccBankCode"];
    }
    NSDictionary *bank = [[ZPDataManager sharedInstance].bankManager.ccBanksDict objectForKey:self.bankCode];
    if ([[bank objectForKey:@"bank_name"] length] > 0) {
        [params setObject:[bank objectForKey:@"bank_name"] forKey:@"ccBankName"];
    }
    if (tfName.text.length > 0)
        [params setObject:[[[NSString alloc] initWithData:[tfName.text dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES] encoding:NSASCIIStringEncoding] zpTrimSpace] forKey:@"cardName"];
    if (tfNumber.text.length > 0)
        [params setObject:[[tfNumber.text zpTrimSpace] zpRemoveFormat:formats] forKey:@"cardNumber"];
    if (tfDate.text.length > 0)
        [params setObject:[[tfDate.text zpTrimSpace] zpRemoveFormat:formatsDate] forKey:@"cardDate"];
    if (tfCVV.text.length > 0)
        [params setObject:tfCVV.text forKey:@"cardCVV"];
    
    return params;
    
}
- (void)prepareForReuse {
    [super prepareForReuse];
    self.hasDate = self.hasPass = NO;
    isViewBackClicked = FALSE;
    [self firstResponderKeyboardTextField:self.indexCurrentSV];
    [self removeBtnPreNext];
}
-(void)viewWillBack{
    isViewBackClicked = YES;
    [self removeBtnPreNext];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    //[self endEditing:NO];
}
#pragma mark - Keyboard Activity

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWasShown:)
     
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWillBeHidden:)
     
                                                 name:UIKeyboardWillHideNotification object:nil];
}
- (void)UnRegisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
}
- (void)keyboardWasShown:(NSNotification*)aNotification

{
    CGSize beginFrame = [[[aNotification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGSize endFrame = [[[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    CGSize keyboardSize = endFrame.height > beginFrame.height ? endFrame  : beginFrame;
    if(keyboardSize.height >= 100){
        if(!heightKeyboard)
            heightKeyboard = keyboardSize.height;
        [self getHeightInputView];
    }
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
}
#pragma mark - UITextFieldDelegate
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    [self hightLightLabelOnCard_NotUse:textField];
    
    JVFloatLabeledTextField *tf = (JVFloatLabeledTextField*)textField;
    [tf setPlaceholderOnly:@""];
    [self getFormatXHighLight:textField andDefault:false];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    UITextField *tfNumber = [self getTextFieldByType:ZPATMSaveCardObjectType_CardNumber];
    if(tfNumber == textField && [tfNumber.text zpRemoveFormat:formats].length >= [[ZPDataManager sharedInstance] doubleForKey:@"atm_card_number_min_length"]){
        
        DDLogInfo(@"detect str : %@",tfNumber.text);
        NSString * detectedBankCode = @"";
        if (self.delegate && [self.delegate respondsToSelector:@selector(isAtmMapcard)]) {
            if ([self.delegate isAtmMapcard]) {
                detectedBankCode = [ZPResourceManager getAtmBankCodeFromConfig:[tfNumber.text zpRemoveFormat:formats]];
                if (detectedBankCode != nil && [detectedBankCode isEqualToString:@""] == NO) {
                    NSDictionary *ccBankDict = [[ZPDataManager sharedInstance].bankManager.ccBanksDict objectForKey:detectedBankCode];
                    if (self.delegate && [self.delegate respondsToSelector:@selector(detectAtmCard:didDetectBank:)]) {
                        [self.delegate detectAtmCard:self didDetectBank:ccBankDict];
                    }
                    return;
                }
                
            }
        }
        
//        ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:stringCreditCardBankCode];
//        if (self.delegate && [self.delegate respondsToSelector:@selector(detectCell:didEditCardNumberWithDetectedBank:)]) {
//            [self.delegate detectCell:self didEditCardNumberWithDetectedBank:bank];
//        }
        
        
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    JVFloatLabeledTextField *tfAmount = [self getTextFieldByType:ZPATMSaveCardObjectType_CardAmount];
    JVFloatLabeledTextField *tfNumber = [self getTextFieldByType:ZPATMSaveCardObjectType_CardNumber];
    JVFloatLabeledTextField *tfName = [self getTextFieldByType:ZPATMSaveCardObjectType_CardName];
    //JVFloatLabeledTextField *tfPass = [self getTextFieldByType:ZPATMSaveCardObjectType_CardPassword];
    JVFloatLabeledTextField *tfDate = [self getTextFieldByType:ZPATMSaveCardObjectType_CardExpire];
    JVFloatLabeledTextField *tfCVV = [self getTextFieldByType:ZPATMSaveCardObjectType_CardCVV];
    
    DDLogInfo(@"shouldChangeCharactersInRange");
    if ([textField isKindOfClass:[JVFloatLabeledTextField class]]) {
        [self textFieldNonError:(JVFloatLabeledTextField *)textField];
    }
    
    if(self.isScrolling) {
        return NO;
    }
    
    NSString * textFieldString = textField.text;
    if (range.location <= textFieldString.length) {
        textFieldString = [textFieldString stringByReplacingCharactersInRange:range withString:string];
    }
    
    if (textField == tfName) {
        
        NSString * tfString = textField.text;
        if(textFieldString.length > [[ZPDataManager sharedInstance] doubleForKey:@"atm_card_holder_max_length"]){
            
            return  NO;
        }
        //Space
        if(tfString.length > 0){
            
            NSString *subStr = [tfString substringWithRange:NSMakeRange(tfString.length-1, 1)];
            if([subStr isEqualToString:@" "] && [string isEqualToString:@" "]){
                [self textFieldShowError:(JVFloatLabeledTextField *)textField withErrorText:@"Tên không được nhập 2 khoảng trắng"];
                return NO;
            }
        }
        textField.text = [textFieldString uppercaseString];
        self.mLblCardName.text = textField.text;
        
        [self getFormatXHighLight:textField andDefault:false];
        
        return NO;
        
        
    }
    if (textField == tfAmount) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            textField.text = [textField.text zpFormatNumberWithDilimeter];
        });
    }
    
    if (textField == tfNumber) {
        NSString * detectString = [[textFieldString zpRemoveFormat:formats] zpTrimSpace];
        NSString * tfString = [[textField.text zpRemoveFormat:formats] zpTrimSpace];
        //Prevent paste text
        int checkNumber = [detectString intValue];
        
        if(checkNumber <= 0 && detectString.length > 0 && ![detectString isEqualToString:@"0"])
            return NO;
        //Switch to ATM
        if([textFieldString zpRemoveFormat:formats].length >= 6 ){

            DDLogInfo(@"detect str : %@",tfNumber.text);
            NSString * detectedBankCode = @"";
            if (self.delegate && [self.delegate respondsToSelector:@selector(isAtmMapcard)]) {
                if ([self.delegate isAtmMapcard]) {
                    
                    detectedBankCode = [ZPResourceManager getAtmBankCodeFromConfig:[textFieldString zpRemoveFormat:formats]];
                    if (detectedBankCode != nil && [detectedBankCode isEqualToString:@""] == NO) {
                        NSDictionary *ccBankDict = [[ZPDataManager sharedInstance].bankManager.ccBanksDict objectForKey:detectedBankCode];
                        if (self.delegate && [self.delegate respondsToSelector:@selector(detectAtmCard:didDetectBank:)]) {
                            textField.text = textFieldString;
                            
                            [self removeBtnPreNext];
                            
                            [self.delegate detectAtmCard:self didDetectBank:ccBankDict];
                            return YES;
                        }
                        
                    }
                    
                }
            }
            
        }
        
        
        [self detectBankCodeFromBankNumber:detectString];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self formatTextField:textField];
        });
        
        if ([self.bankCode length] <= 0 && [detectString length] >= 6 + 1) {
            [self textFieldCheckValidate];
            //detect cc card
            NSString * detectedBankCode = @"";
            if (self.delegate && [self.delegate respondsToSelector:@selector(isAtmMapcard)]) {
                if ([self.delegate isAtmMapcard]) {
                    detectedBankCode = [ZPResourceManager getAtmBankCodeFromConfig:detectString];
                    if (detectedBankCode != nil && [detectedBankCode isEqualToString:@""] == NO) {
                        
                    } else {
                        //[_line setBackgroundColor:TEXTFIELD_ERROR_COLOR];
                    }
                    
                }
            }
            
            //Clear Image
            self.mImgIconBank.image = [UIImage imageNamed:@""];
            self.mImgIconBank.backgroundColor = [UIColor lightGrayColor];
            
            self.mImgIconBankBackView.image = [UIImage imageNamed:@""];
            self.mImgIconBankBackView.backgroundColor = [UIColor lightGrayColor];
            return NO;
        }
        if(self.bankCode.length > 0 && detectString.length > 0){
            //Get image bankcode
            NSString *imgGet = [ZPResourceManager bankValueFromBankCodeBundle:@"bank_icon" andBankCode:self.bankCode andIsBankOrCC:false];
            self.mImgIconBank.image =  [ZPResourceManager getImageWithName:imgGet];
            
            self.mImgIconBank.backgroundColor = [UIColor clearColor];
            
            self.mImgIconBankBackView.image = [ZPResourceManager getImageWithName:imgGet];
            self.mImgIconBankBackView.backgroundColor = [UIColor clearColor];
            
            [self animationFadeInOut:self.mImgIconBank];
            
            [self updatePageControl:textField];
        }
        else {
            //Clear Image
            self.mImgIconBank.image = [UIImage imageNamed:@""];
            self.mImgIconBank.backgroundColor = [UIColor lightGrayColor];
            
            self.mImgIconBankBackView.image = [UIImage imageNamed:@""];
            self.mImgIconBankBackView.backgroundColor = [UIColor lightGrayColor];
            
        }
        [self setGradientColorForCardView:self.bankCode];
        
        //Length
        int lengthMax =  [[ZPDataManager sharedInstance] doubleForKey:@"atm_card_number_max_length"];
        int lengthMin = 0;
        if(self.bankCode.length > 0){
            lengthMax = [[ZPResourceManager bankValueFromBankCodeBundle:@"bank_maxlength" andBankCode:self.bankCode andIsBankOrCC:false] intValue];
            
            //Show error save card before
            lengthMin = [[ZPResourceManager bankValueFromBankCodeBundle:@"bank_minlength" andBankCode:self.bankCode andIsBankOrCC:false] intValue];
            
            NSString *strCheck = tfString;
            //19->20
            if(detectString.length <= lengthMax){
                strCheck = detectString;
            }
            
            if(strCheck.length >= lengthMin){
                [self isSaveCardBefore:tfString andTf:tfNumber];
                if (![self checkInPutCardIsLuhnValid:strCheck]) {
                    [self textFieldShowError:tfNumber withErrorText:@"Thông tin số thẻ không chính xác"];
                }
            }
        }
        
        //Check Save Card and Link Card
        if([self isSaveCardBefore:detectString andTf:tfNumber]){
            if(detectString.length <= lengthMax)
                return YES;
            else
                return NO;
            
        }
        
        if ([self.bankCode length] > 0) {
            ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:stringCreditCardBankCode];

            if (bank.status != ZPBankStatusEnable) {
                ZPCCBankCode ccBankCode = [ZPResourceManager convertCCBankCodeToEnum:self.bankCode];
                if(ccBankCode == ZPCCBankCodeJCB) return detectString.length <= 4;
                if(ccBankCode == ZPCCBankCodeMASTER) return detectString.length == 2 || detectString.length == 4;
                if(ccBankCode == ZPCCBankCodeVISA) return detectString.length <= 1;
                
                
            }
            if ([self validateCCdebit:detectString inTextField:tfNumber]) {
                if(detectString.length >= binLength) {
                    tfNumber.text = [detectString substringToIndex:binLength];
                }
                return NO;
            }
        }
        return detectString.length <= lengthMax;
    }
    
    if (textField == tfCVV) {
        int maxCCVLength = [[ZPDataManager sharedInstance] doubleForKey:@"max_card_ccv_length"] > 0 ? [[ZPDataManager sharedInstance] doubleForKey:@"max_card_ccv_length"] : 3;
        DDLogInfo(@"max ccv length : %d", maxCCVLength);
        return textFieldString.length <= maxCCVLength;
    }
    if (textField == tfDate) {
        NSString * detectString = [[textFieldString zpRemoveFormat:formatsDate] zpTrimSpace];
        NSString * tfString = [[textField.text zpRemoveFormat:formatsDate] zpTrimSpace];
        //Remove
        if ([string isEqualToString:@""] && textField.text.length == 3) {
            
            NSString *dateString = textField.text;
            textField.text =
            [dateString stringByReplacingOccurrencesOfString:@"/" withString:@""];
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if(detectString.length >= 3){
                [self formatTextFieldDate:textField];
            }
            
        });
        
        if(detectString.length > tfString.length && detectString.length == 5)
            [self checkValidateTextFieldByType:self.indexCurrentSV andShowError:true];
        
        return detectString.length <= 4;
    }
    return YES;
}
- (BOOL)textFieldShouldClear:(UITextField *)textField{
    
    JVFloatLabeledTextField *tfNumber = [self getTextFieldByType:ZPATMSaveCardObjectType_CardNumber];
    JVFloatLabeledTextField *tfName = [self getTextFieldByType:ZPATMSaveCardObjectType_CardName];
    if (textField == tfNumber) {
        self.bankCode = @"";
        if (self.delegate && [self.delegate respondsToSelector:@selector(detectCell:didDetectBank:)]) {
            [self.delegate detectCell:self didDetectBank:nil];
            [self updateLayouts];
        }
        
        //Reset default
        self.mImgIconBank.image = [UIImage imageNamed:@""];
        self.mImgIconBank.backgroundColor = [UIColor lightGrayColor];
        self.mImgIconBankBackView.image = [UIImage imageNamed:@""];
        self.mImgIconBankBackView.backgroundColor = [UIColor lightGrayColor];
        
        [self setGradientColorForCardView:self.bankCode];
        self.mLblCardNumber.text = kFormatDefaultNumber;
        self.mLblCardExpire.text = self.titleExpireDefault;
        self.mLblCardName.text = titleNameDefault;
        
    }
    if (textField == tfName) {
        self.mLblCardName.text = titleNameDefault;
    }
    if ([textField isKindOfClass:[JVFloatLabeledTextField class]]) {
        [self textFieldNonError:(JVFloatLabeledTextField *)textField];
    }
    
    
    
    return YES;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self registerForKeyboardNotifications];
    return YES;
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [self btnNextOnAccessoryViewClick];
    
    return  YES;
}
#pragma mark - ScrollViewDelegate
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    self.isEndDecelerating = FALSE;
    
    lastContentOffset = scrollView.contentOffset.x;
    
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    
}
-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    self.isEndDecelerating = FALSE;
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    CGFloat widthSV = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x -  widthSV /2) / widthSV) + 1;
    
    //Disable scroll
    CGPoint nowOffset = scrollView.contentOffset;
    if ((lastContentOffset - nowOffset.x) < 0) {
        if(self.isCheckValidScroll){
            
            JVFloatLabeledTextField *tf = (JVFloatLabeledTextField*)((ZPATMSaveCardObject*)self.arrTF[self.indexCurrentSV]).mTextField;
            if(tf != nil){
                BOOL showError = tf.text.length > 0 ? true : false;
                if(![self checkValidateTextFieldByType:self.indexCurrentSV andShowError:showError]){
                    self.isEndDecelerating = TRUE;
                    if(!isScrollingToError){
                        isScrollingToError = true;
                        [self scrollToIndex:self.indexCurrentSV];
                        [self firstResponderKeyboardTextField:self.indexCurrentSV];
                        [self setTimerEnableScroll];
                    }
                    
                }
            }
            
        }
        
    } else if ((lastContentOffset - nowOffset.x) > 0) {
        self.isEndDecelerating = TRUE;
    } else {
        //scrollView.scrollEnabled = YES;
        self.isEndDecelerating = TRUE;
    }
    
    
    
    [self updatePageControlByIndex:page];
    if(lastIndexSV !=page){
        
        [self updatePageControlByIndex:page];
        [self updatePreNextEnable];
    }
    
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    CGFloat widthSV = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x -  widthSV /2) / widthSV) + 1;
    self.isEndDecelerating = TRUE;
    if(lastIndexSV !=page){
        self.indexCurrentSV = lastIndexSV = page;
        [self firstResponderKeyboardTextField:self.indexCurrentSV];
        [self updatePageControl];
        [self updatePageControlByIndex:page];
        [self updatePreNextEnable];
        
        [self flipByIndexTextField];
    }
    
}
-(void)tapTF{
    self.mScrollTextfeild.scrollEnabled = YES;
    isScrollingToError = false;
    
}
#pragma mark - Validate
-(BOOL)textFieldCheckValidate{
    
    JVFloatLabeledTextField *tfNumber = [self getTextFieldByType:ZPATMSaveCardObjectType_CardNumber];
    JVFloatLabeledTextField *tfName = [self getTextFieldByType:ZPATMSaveCardObjectType_CardName];
    JVFloatLabeledTextField *tfDate = [self getTextFieldByType:ZPATMSaveCardObjectType_CardExpire];
    JVFloatLabeledTextField *tfCVV = [self getTextFieldByType:ZPATMSaveCardObjectType_CardCVV];
    
    NSString * errorMessage = @"";
    if ([tfNumber.text zpTrimSpace].length <= 0) {
        errorMessage = @"Vui lòng nhập số thẻ";
        [self textFieldShowError:tfNumber withErrorText:errorMessage];
    }
    else if ([[[tfNumber.text zpRemoveFormat:formats] zpTrimSpace] length] >= 6) {
        
        if([[tfNumber.text zpTrimSpace] zpRemoveFormat:formats].length >=6) {
            
            if(self.bankCode == nil || self.bankCode.length == 0) {
                
                errorMessage = @"Chưa hỗ trợ ngân hàng này";
                [tfNumber setFloatingTitleErrorWithImage:@"Chưa hỗ trợ ngân hàng này" andImage:[ZPResourceManager getImageWithName:@"question"]];
                [self setupFloatingLabelShowBankSupport:tfNumber];
                
                return NO;
                
            }
        }
    }
    if ([[tfNumber.text zpTrimSpace] zpRemoveFormat:formats].length < [[ZPDataManager sharedInstance] doubleForKey:@"atm_card_number_min_length"] ) {
        errorMessage = @"Thông tin số thẻ không chính xác";
        [self textFieldShowError:tfNumber withErrorText:errorMessage];
        return NO;
    }
    
    
    if ([tfName.text zpTrimSpace].length <= 0 ) {
        errorMessage = @"Vui lòng nhập tên chủ thẻ";
        [self textFieldShowError:tfName withErrorText:errorMessage];
        return NO;
    }
    
    if ([tfDate.text zpTrimSpace].length <= 0 ) {
        errorMessage = @"Vui lòng nhập ngày hết hạn";
        [self textFieldShowError:tfDate withErrorText:errorMessage];
        return NO;
    } else if (!isCardDateValid) {
        errorMessage = @"Ngày hết hạn không hợp lệ";
        [self textFieldShowError:tfDate withErrorText:errorMessage];
        return NO;
    }
    
    if ([tfCVV.text zpTrimSpace].length <= 0 ) {
        errorMessage = @"Vui lòng nhập mã CVV";
        [self textFieldShowError:tfCVV withErrorText:errorMessage];
        return NO;
    }
    else {
        int maxCCVLength = [[ZPDataManager sharedInstance] doubleForKey:@"max_card_ccv_length"] > 0 ? [[ZPDataManager sharedInstance] doubleForKey:@"max_card_ccv_length"] : 3;
        DDLogInfo(@"max ccv length : %d", maxCCVLength);
        if (tfCVV.text.length < maxCCVLength) {
            errorMessage = @"CVV không hợp lệ";
            [self textFieldShowError:tfCVV withErrorText:errorMessage];
            return NO;
        }
    }
    
    if([errorMessage length] > 0) {
        return NO;
    }
    return YES;
}
//need
- (BOOL)checkValidateTextFieldLastest{
    
    JVFloatLabeledTextField *tfNumber = [self getTextFieldByType:ZPATMSaveCardObjectType_CardNumber];
    JVFloatLabeledTextField *tfName = [self getTextFieldByType:ZPATMSaveCardObjectType_CardName];
    JVFloatLabeledTextField *tfDate = [self getTextFieldByType:ZPATMSaveCardObjectType_CardExpire];
    JVFloatLabeledTextField *tfCVV = [self getTextFieldByType:ZPATMSaveCardObjectType_CardCVV];
    
    NSString * errorMessage = @"";
    if ([tfNumber.text zpTrimSpace].length <= 0) {
        errorMessage = @"Vui lòng nhập số thẻ";
        
        [self textFieldShowError:tfNumber withErrorText:errorMessage];
        [self scrollToIndexByType:ZPATMSaveCardObjectType_CardNumber];
        [self flipByIndexTextField];
        return NO;
    }
    else if ([[tfNumber.text zpTrimSpace] zpRemoveFormat:formats].length < [[ZPDataManager sharedInstance] doubleForKey:@"atm_card_number_min_length"] || ![self checkInPutCardIsLuhnValid:[[tfNumber.text zpTrimSpace] zpRemoveFormat:formats]]) {
        
        if([tfNumber isFirstResponder]) {
            errorMessage = @"Chưa hỗ trợ ngân hàng này";
            [tfNumber setFloatingTitleErrorWithImage:@"Chưa hỗ trợ ngân hàng này" andImage:[ZPResourceManager getImageWithName:@"question"]];
            [self setupFloatingLabelShowBankSupport:tfNumber];
            
            [self flipByIndexTextField];
            return NO;
        } else {
            errorMessage = @"Thông tin số thẻ không chính xác";
            [self textFieldShowError:tfNumber withErrorText:errorMessage];
            [self scrollToIndexByType:ZPATMSaveCardObjectType_CardNumber];
            [self flipByIndexTextField];
            return NO;
        }
    }
    
    
    if ([tfDate.text zpTrimSpace].length <= 0 ) {
        errorMessage = @"Vui lòng nhập ngày hết hạn";
        [self textFieldShowError:tfDate withErrorText:errorMessage];
        [self scrollToIndexByType:ZPATMSaveCardObjectType_CardExpire];
        [self flipByIndexTextField];
        return NO;
    } else if (!isCardDateValid) {
        errorMessage = @"Ngày hết hạn không hợp lệ";
        [self textFieldShowError:tfDate withErrorText:errorMessage];
        [self scrollToIndexByType:ZPATMSaveCardObjectType_CardExpire];
        [self flipByIndexTextField];
        return NO;
    }
    
    if ([tfCVV.text zpTrimSpace].length <= 0 ) {
        errorMessage = @"Vui lòng nhập mã CVV";
        [self textFieldShowError:tfCVV withErrorText:errorMessage];
        [self scrollToIndexByType:ZPATMSaveCardObjectType_CardCVV];
        [self flipByIndexTextField];
        return NO;
    }
    else {
        int maxCCVLength = [[ZPDataManager sharedInstance] doubleForKey:@"max_card_ccv_length"] > 0 ? [[ZPDataManager sharedInstance] doubleForKey:@"max_card_ccv_length"] : 3;
        DDLogInfo(@"max ccv length : %d", maxCCVLength);
        if (tfCVV.text.length < maxCCVLength) {
            errorMessage = @"CVV không hợp lệ";
            [self textFieldShowError:tfCVV withErrorText:errorMessage];
            [self scrollToIndexByType:ZPATMSaveCardObjectType_CardCVV];
            [self flipByIndexTextField];
            return NO;
        }
    }
    if ([tfName.text zpTrimSpace].length <= 0 ) {
        errorMessage = @"Vui lòng nhập tên chủ thẻ";
        [self textFieldShowError:tfName withErrorText:errorMessage];
        [self scrollToIndexByType:ZPATMSaveCardObjectType_CardName];
        [self flipByIndexTextField];
        return NO;
    }
    
    if([errorMessage length] > 0) {
        [self flipByIndexTextField];
        return NO;
    }
    
    //Ok clicked
    if (self.delegate && [self.delegate respondsToSelector:@selector(btnOkClicked)]) {
        
        //Default color on Card
        [self hightLightLabelOnCardDefault];
        ZPATMSaveCardObject *mdGet = self.arrTF[self.indexCurrentSV];
        [self getFormatXHighLight:mdGet.mTextField andDefault:true];
        ((JVFloatLabeledTextField*)mdGet.mTextField).isBGBottomLineDefault = true;
        
        [timerFirstBecome invalidate];
        [self.delegate btnOkClicked];
    }
    
    return true;
}
-(void)checkValidateDate:(NSString*)strDate{
    
    int y = 0;
    int m = 0;
    
    if([strDate zpRemoveFormat:formatsDate].length < 4){
        
        isCardDateValid = NO;
        return;
    }
    
    NSArray *arrMY = [strDate componentsSeparatedByString:@"/"];
    
    NSScanner *scannerMM = [NSScanner scannerWithString:arrMY[0]];
    BOOL isNumericMM = [scannerMM scanInteger:NULL] && [scannerMM isAtEnd];
    
    NSScanner *scannerYY = [NSScanner scannerWithString:arrMY[1]];
    BOOL isNumericYY = [scannerYY scanInteger:NULL] && [scannerYY isAtEnd];
    
    if(!isNumericMM || !isNumericYY){
        isCardDateValid = NO;
        return;
    }
    
    m = [arrMY[0] intValue];
    y = [arrMY[1] intValue];
    
    NSDate * date = [NSDate date];
    NSCalendar *calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];;
    int month = (int)[[calendar components:NSCalendarUnitYear|NSCalendarUnitMonth fromDate:date] month];
    int year = (int)[[calendar components:NSCalendarUnitYear|NSCalendarUnitMonth fromDate:date] year] - 2000;
    DDLogInfo(@"y year m month %d.%d.%d.%d",y,year,m,month);
    
    if(m == 0 || m > 12 || y < year - 15 || y > year + 15 ){
        isCardDateValid = NO;
        return;
    }
    
    if (y < year) {
        isCardDateValid = NO;
    } else if (y == year && m < month) {
        isCardDateValid = NO;
    } else {
        isCardDateValid = YES;
    }
    
}
- (BOOL)checkInPutCardIsLuhnValid:(NSString *)cardNumber {
    if(cardNumber.length < [[ZPDataManager sharedInstance] doubleForKey:@"atm_card_number_min_length"]) return NO;
    if (!CC_CHECK_CARD_IS_LUHN_VALID) {
        return YES;
    }
    NSMutableArray *stringAsChars = [cardNumber zpStringToArray];
    BOOL isOdd = YES;
    int oddSum = 0;
    int evenSum = 0;
    
    for (int i = (int)[cardNumber length] - 1; i >= 0; i--) {
        
        int digit = [(NSString *)[stringAsChars objectAtIndex:i] intValue];
        
        if (isOdd)
            oddSum += digit;
        else
            evenSum += digit/5 + (2*digit) % 10;
        
        isOdd = !isOdd;
    }
    return ((oddSum + evenSum) % 10 == 0);
}

- (BOOL)checkValidateTextFieldByType:(int )index andShowError:(BOOL)show{
    
    
    NSString * errorMessage = @"";
    ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:self.bankCode];

    NSInteger opt = bank.bankType;
    DDLogInfo(@"banktype %ld",(long)opt);
    
    
    JVFloatLabeledTextField *tfNumber = [self getTextFieldByType:ZPATMSaveCardObjectType_CardNumber];
    JVFloatLabeledTextField *tfName = [self getTextFieldByType:ZPATMSaveCardObjectType_CardName];
    JVFloatLabeledTextField *tfDate = [self getTextFieldByType:ZPATMSaveCardObjectType_CardExpire];
    JVFloatLabeledTextField *tfCVV = [self getTextFieldByType:ZPATMSaveCardObjectType_CardCVV];
    
    
    ZPATMSaveCardObject *mdGet = self.arrTF[index];
    enum ZPATMSaveCardObjectType type = mdGet.mType;
    if(type == ZPATMSaveCardObjectType_CardNumber){
        
        if ([tfNumber.text zpTrimSpace].length <= 0) {
            errorMessage = @"Vui lòng nhập số thẻ";
            if(show)
                [self textFieldShowError:tfNumber withErrorText:errorMessage];
        }
        else if ([[[tfNumber.text zpRemoveFormat:formats] zpTrimSpace] length] >= 6) {
            
            if([[tfNumber.text zpTrimSpace] zpRemoveFormat:formats].length >=6) {
                
                if(self.bankCode == nil || self.bankCode.length == 0) {
                    
                    errorMessage = @"Chưa hỗ trợ ngân hàng này";
                    if(show){
                        [tfNumber setFloatingTitleErrorWithImage:@"Chưa hỗ trợ ngân hàng này" andImage:[ZPResourceManager getImageWithName:@"question"]];
                        [self setupFloatingLabelShowBankSupport:tfNumber];
                    }
                    
                    return NO;
                    
                }
                else {
                    //Check Save Card and Link Card
                    if([self isSaveCardBefore:[[tfNumber.text zpRemoveFormat:formats] zpTrimSpace] andTf:tfNumber])
                        return NO;
                }
                
            }
        }
        if ([[tfNumber.text zpTrimSpace] zpRemoveFormat:formats].length < [[ZPDataManager sharedInstance] doubleForKey:@"atm_card_number_min_length"] || ![self checkInPutCardIsLuhnValid:[[tfNumber.text zpTrimSpace] zpRemoveFormat:formats]]) {            
            errorMessage = @"Thông tin số thẻ không chính xác";
            if(show)
                [self textFieldShowError:tfNumber withErrorText:errorMessage];
            
            return NO;
        }
        
    }
    if(type == ZPATMSaveCardObjectType_CardName){
        if ([tfName.text zpTrimSpace].length <= 0) {
            errorMessage = @"Vui lòng nhập tên chủ thẻ";
            if(show)
                [self textFieldShowError:tfName withErrorText:errorMessage];
        }
        
    }
    
    if(type == ZPATMSaveCardObjectType_CardExpire){
        if ([tfDate.text zpTrimSpace].length <= 0 ) {
            errorMessage = @"Vui lòng nhập ngày hết hạn";
            if(show)
                [self textFieldShowError:tfDate withErrorText:errorMessage];
        } else if (!isCardDateValid) {
            errorMessage = @"Ngày hết hạn không hợp lệ";
            if(show)
                [self textFieldShowError:tfDate withErrorText:errorMessage];
        }
        
    }
    if(type == ZPATMSaveCardObjectType_CardCVV){
        if ([tfCVV.text zpTrimSpace].length <= 0 ) {
            errorMessage = @"Vui lòng nhập mã CVV";
            if(show)
                [self textFieldShowError:tfCVV withErrorText:errorMessage];
        }
        else {
            int maxCCVLength = [[ZPDataManager sharedInstance] doubleForKey:@"max_card_ccv_length"] > 0 ? [[ZPDataManager sharedInstance] doubleForKey:@"max_card_ccv_length"] : 3;
            DDLogInfo(@"max ccv length : %d", maxCCVLength);
            if (tfCVV.text.length < maxCCVLength) {
                errorMessage = @"CVV không hợp lệ";
                if(show)
                    [self textFieldShowError:tfCVV withErrorText:errorMessage];
            }
        }
    }
    
    
    if([errorMessage length] > 0) {
        return NO;
    }
    return true;
}
#pragma mark - Detect Bank
- (void)detectBankCodeFromBankNumber:(NSString *)detectString {
    
    JVFloatLabeledTextField *tfNumber = [self getTextFieldByType:ZPATMSaveCardObjectType_CardNumber];
    
    if (detectString.length == 0) {
        self.bankCode = @"";
        if (self.delegate && [self.delegate respondsToSelector:@selector(detectCell:didDetectBank:)]) {
            [self.delegate detectCell:self didDetectBank:nil];
            [self updateLayouts];
        }
    } else {
        NSString * detectedBankCode = [ZPResourceManager getCcBankCodeFromConfig:detectString];
        
        DDLogInfo(@"detectedBankCode: %@", detectedBankCode);
        self.bankCode = detectedBankCode;
        NSDictionary *bankDict = [[ZPDataManager sharedInstance].bankManager.ccBanksDict objectForKey:self.bankCode];
        if (detectedBankCode != nil) {
            [tfNumber setFloatingTitle:[NSString stringWithFormat:@"%@ %@", @"Thẻ", [bankDict objectForKey:@"bank_name"] ?: @""]];
        }
        if(self.bill.errorCCCanNotMapWhenPay.length > 0 && detectedBankCode.length > 0 && self.delegate && [self.delegate respondsToSelector:@selector(detectCell:didDetectBank:)]) {
            ZPBank *bank = [ZPBank new];
            bank.detectString = detectString;
            [self.delegate detectCell:self didDetectBank:bank];
            [self updateLayouts];
            return;
        }
        // handle case cc unuppport but cc debit is acceptable
        if (self.bill.unsuportCCCredit && detectString.length < binLength) {
            return;
        }
        
        // show error via the cc is not debit
        if (self.bill.unsuportCCCredit && ![[ZaloPayWalletSDKPayment sharedInstance].appDependence IsCCDebit:detectString] && detectString.length >= binLength) {
            [self textFieldShowError:tfNumber withErrorText:[R string_LinkCard_NotSupport_CreditCard]];
            return;
        }
        
        if (detectedBankCode != nil && self.delegate && [self.delegate respondsToSelector:@selector(detectCell:didDetectBank:)]){
            ZPBank *bank = [ZPBank copyFrom:[[ZPDataManager sharedInstance].bankManager getBankInfoWith:stringCreditCardBankCode]];
            bank.bankName = [bankDict objectForKey:stringKeyCCBankName];
            bank.bankCode = self.bankCode;
            bank.detectString = detectString;
            [self.delegate detectCell:self didDetectBank:bank];
            [self updateLayouts];
        }
    }
}

- (BOOL)validateCCdebit:(NSString *)detectString inTextField:(JVFloatLabeledTextField *)textField  {
    if (detectString.length < binLength) {
        return NO;
    }
    if (self.bill.unsuportCCCredit && ![[ZaloPayWalletSDKPayment sharedInstance].appDependence IsCCDebit:detectString]) {
        [self textFieldShowError:textField withErrorText:[R string_LinkCard_NotSupport_CreditCard]];
        return YES;
    }
    return NO;
}
#pragma mark - unKnow
- (void)setBankCode:(NSString *)bankCode{
    _bankCode = bankCode;
    [self updateLayouts];
}

- (void)setCardNumber:(NSString *)number {
    DDLogInfo(@"setCardNumber");
    if (number.length > 0) {
        
        JVFloatLabeledTextField *tfNumber = [self getTextFieldByType:ZPATMSaveCardObjectType_CardNumber];
        tfNumber.text = number;
        [self detectBankCodeFromBankNumber:number];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self formatTextField:tfNumber];
            self.mLblCardNumber.text = tfNumber.text;
        });
    }
}
- (void)setCreditCardInfo:(ZPCreditCard *)cachedCard {
    DDLogInfo(@"cardNumber: %@", cachedCard);
    if (cachedCard.cardNumber != nil) {
        
        JVFloatLabeledTextField *tfNumber = [self getTextFieldByType:ZPATMSaveCardObjectType_CardNumber];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            ZPATMSaveCardObject *mdGet = self.arrTF[self.indexCurrentSV];
            [mdGet.mTextField becomeFirstResponder];
            
            tfNumber.text = cachedCard.cardNumber;
            
            [self formatTextField:tfNumber];
            self.mLblCardNumber.text = [self getFormatXXXXDefaultNumber:tfNumber.text];
            
            [self detectBankCodeFromBankNumber:cachedCard.cardNumber];
            [self getFormatXHighLight:tfNumber andDefault:false];
            [self hightLightLabelOnCard_NotUse:tfNumber];
            
            self.mLblCardExpire.text = self.titleExpireDefault;
            
            //Set floating color
            tfNumber.isShowTailImage = false;
            tfNumber.isShowError = false;
            
            if(self.bankCode.length > 0){
                //Get image bankcode
                NSString *imgGet = [ZPResourceManager bankValueFromBankCodeBundle:@"bank_icon" andBankCode:self.bankCode andIsBankOrCC:false];
                self.mImgIconBank.image =  [ZPResourceManager getImageWithName:imgGet];
                
                self.mImgIconBank.backgroundColor = [UIColor clearColor];
                self.mImgIconBankBackView.image = [ZPResourceManager getImageWithName:imgGet];
                self.mImgIconBankBackView.backgroundColor =  [UIColor clearColor];
                
                [self animationFadeInOut:self.mImgIconBank];
                [self setGradientColorForCardView:self.bankCode];
                [self validateCCdebit:cachedCard.cardNumber inTextField:tfNumber];
            }
        });
    }
}

+ (ZPAtmInputType) inputTypeForBankCode: (NSString *) bankCode {
    ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:bankCode];
    int opt = bank.bankOPT;

    return [ZPResourceManager classifyAtmInputTypeWithOption:opt andBankType:nil];;
}
#pragma mark - Gradient Color
-(void)setGradientColorForCardView:(NSString*)bankCode{
    if (gradient) {
        [gradient removeFromSuperlayer];
    }
    NSArray *colors = [ZPResourceManager bankColorGradientFromBankCodeBundle:bankCode andIsBankOrCC:false];
    if(bankCode == nil || [bankCode isEqualToString:@""]){
        colors = @[
                   [UIColor zpColorFromHexString:kColorDefaultGradientStart],
                   [UIColor zpColorFromHexString:kColorDefaultGradientEnd]
                   ];
    }
    gradient = [CAGradientLayer layer];
    gradient.frame = self.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[colors firstObject] CGColor],
                       //[[colors firstObject] CGColor],
                       (id)[[colors lastObject] CGColor],
                       //(id)[[colors firstObject] CGColor],
                       //[[colors firstObject] CGColor],
                       nil];
    gradient.startPoint = CGPointMake(0, 1);
    gradient.endPoint = CGPointMake(1, 0);
    [self.mViewCardBG.layer insertSublayer:gradient atIndex:0];
    
    //On Back
    [self setGradientColorForCardViewBack:self.bankCode];
}
-(void)setGradientColorForCardViewBack:(NSString*)bankCode{
    if (gradientBack) {
        [gradientBack removeFromSuperlayer];
    }
    NSArray *colors = [ZPResourceManager bankColorGradientFromBankCodeBundle:bankCode andIsBankOrCC:false];
    if(bankCode == nil || [bankCode isEqualToString:@""]){
        colors = @[
                   [UIColor zpColorFromHexString:kColorDefaultGradientStart],
                   [UIColor zpColorFromHexString:kColorDefaultGradientEnd]
                   ];
    }
    gradientBack = [CAGradientLayer layer];
    gradientBack.frame = self.bounds;
    gradientBack.colors = [NSArray arrayWithObjects:(id)[[colors firstObject] CGColor],
                           [[colors firstObject] CGColor],
                           (id)[[colors lastObject] CGColor],
                           (id)[[colors firstObject] CGColor],
                           [[colors firstObject] CGColor], nil];
    gradientBack.startPoint = CGPointMake(0, 1);
    gradientBack.endPoint = CGPointMake(1, 0);
    [self.mViewCardBackCVV.layer insertSublayer:gradientBack atIndex:0];
}
#pragma mark - Show Bank Support
-(void)setupFloatingLabelShowBankSupport:(JVFloatLabeledTextField*)tf{
    
    
    double delayInSeconds = .2;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapFloatingLabel:)];
        tf.floatingLabel.userInteractionEnabled = YES;
        [tf.floatingLabel addGestureRecognizer:tap];
        
        if(tf.viewTapOnFloatingLabel){
            UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapFloatingLabel:)];
            tf.viewTapOnFloatingLabel.userInteractionEnabled = YES;
            [tf.viewTapOnFloatingLabel addGestureRecognizer:tap2];
            
            tf.viewTapOnFloatingLabel.backgroundColor = [UIColor clearColor];
            
            
        }
    });
    
    
}
-(void)tapFloatingLabel:(UITapGestureRecognizer*)tap{
    ZPATMSaveCardObject *mdGet = self.arrTF[0];
    JVFloatLabeledTextField *tf = (JVFloatLabeledTextField*)mdGet.mTextField;
    if(tf.isShowTailImage){
        ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:self.bankCode];
        if (self.delegate && [self.delegate respondsToSelector:@selector(detectCell:didEditCardNumberWithDetectedBank:)]) {
            [self.delegate detectCell:self didEditCardNumberWithDetectedBank:bank];
        }
    }
}
@end
