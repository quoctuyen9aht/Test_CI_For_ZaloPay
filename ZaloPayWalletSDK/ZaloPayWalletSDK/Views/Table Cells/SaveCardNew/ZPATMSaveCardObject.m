//
//  ZPATMSaveCardObject.m
//  ZaloPayWalletSDK
//
//  Created by CPU11689 on 9/22/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPATMSaveCardObject.h"

@implementation ZPATMSaveCardObject


-(ZPATMSaveCardObject*)initWithTitle:(NSString*)title andValue:(NSString*)value andPlaceHolder:(NSString*)placeHolder andType:(enum ZPATMSaveCardObjectType)type{
    self.mTitle = title;
    self.mValue = value;
    self.mType = type;
    self.mPlaceHolder = placeHolder;
    return self;
}
@end
