//
//  ZPPickerList.m
//  DemoCreditCardView
//
//  Created by CPU11689 on 9/26/16.
//  Copyright © 2016 CPU11689. All rights reserved.
//

#import "ZPPickerList.h"

@implementation ZPPickerList

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.dataSource = self;
        self.delegate = self;
        self.showsSelectionIndicator = true;
    }
    return self;
}
-(void)selectIndexCurrent:(int)index{
    
    [self selectRow:index inComponent:0 animated:true];

}
#pragma mark - UIPickerViewDelegate
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return _arrData.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{

    return _arrData[row];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
        
    if (_pickerDelegate != NULL && [_pickerDelegate respondsToSelector:@selector(ZPPickerListDelegate_onChangeData:arr:)]) {
        [_pickerDelegate ZPPickerListDelegate_onChangeData:(int)row arr:_arrData];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
