//
//  ZPManagerSaveCardCell.m
//  ZaloPayWalletSDK
//
//  Created by vuongvv on 10/19/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPManagerSaveCardCell.h"

@implementation ZPManagerSaveCardCell

+ (instancetype)sharedInstance {
    static ZPManagerSaveCardCell * _sharedInstance = nil;
    static dispatch_once_t predicate;
    
    dispatch_once(&predicate, ^{
        _sharedInstance = [[ZPManagerSaveCardCell alloc] init];
    });
    
    return _sharedInstance;
}

@end
