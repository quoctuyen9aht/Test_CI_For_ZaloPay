//
//  ZPCreditSaveCardCell.h
//  ZaloPayWalletSDK
//
//  Created by CPU11689 on 9/29/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ZPATMSaveCardObject.h"
#import "ZPTopAlignedLabel.h"
#import "ZPMonthYearPicker.h"
#import "ZPPickerList.h"
#import "ZPPaymentViewCell.h"

#import "ZPConfig.h"
#import "ZPPaymentChannel.h"

@interface ZPCreditSaveCardCell : ZPPaymentViewCell

//Pass
@property (weak, nonatomic) UITableView *mTableView;
@property (weak, nonatomic) UIViewController * zpParentController;

@property (weak, nonatomic) IBOutlet UIView *mViewCardBG;

//Front
@property (weak, nonatomic) IBOutlet UILabel *mLblCardNumber;
@property (weak, nonatomic) IBOutlet ZPTopAlignedLabel *mLblCardName;
@property (weak, nonatomic) IBOutlet ZPTopAlignedLabel *mLblCardExpire;
@property (weak, nonatomic) IBOutlet UIImageView *mImgBG;
@property (weak, nonatomic) IBOutlet UIImageView *mImgIconBank;

//Back
@property (weak, nonatomic) IBOutlet UIView *mViewCardBackCVV;
@property (weak, nonatomic) IBOutlet UILabel *mLblCardCVV;
@property (weak, nonatomic) IBOutlet UIScrollView *mScrollTextfeild;
@property (weak, nonatomic) IBOutlet UIImageView *mImgIconBankBackView;
//Title
@property (weak, nonatomic) IBOutlet UILabel *mLblTitleCardName;
@property (weak, nonatomic) IBOutlet UILabel *mLblTitleCardExpire;

//Constraint
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_1_icon_number;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_1_number_date;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_1_date_name;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_top_cardview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_bottom_cardview;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_height_numberlbl;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_width_icon;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_height_icon;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_width_icon_back;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_height_icon_back;

@property (assign, nonatomic) BOOL hasAmount;
@property (assign, nonatomic) long suggestAmount;
@property (strong, nonatomic) NSString * bankCode;
@property (strong, nonatomic) NSString * lastBankCode;
@property (assign, nonatomic) BOOL hasDate;
@property (assign, nonatomic) BOOL hasPass;
@property (assign, nonatomic) BOOL hasAuthMethod;
@property (assign, nonatomic) long discountAmount;
@property (assign, nonatomic) double discountPercent;
@property (assign, nonatomic) ZPChannel *channel;
@property (assign, nonatomic) ZPBill *bill;


@property (assign, nonatomic) BOOL isShouldHideKB;

-(void)firstResponderKeyboardTextFieldCurrent;

+ (ZPAtmInputType) inputTypeForBankCode: (NSString *) bankCode;
- (void)setCardNumber:(NSString *)number;
-(void)viewWillBack;
-(JVFloatLabeledTextField*)getTextFieldByType:(enum ZPATMSaveCardObjectType)type;
-(void)resetAll;
-(JVFloatLabeledTextField*)getCurrentTf;
@end

