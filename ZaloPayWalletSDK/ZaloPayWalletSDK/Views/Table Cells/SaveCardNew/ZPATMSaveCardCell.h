
//
//  ZPATMSaveCard.h
//  ZaloPayWalletSDK
//
//  Created by CPU11689 on 9/22/16.
//  Copyright © 2016-present VNG. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

#import <ZaloPayCommon/JVFloatLabeledTextField.h>
#import "ZPATMSaveCardObject.h"
#import "ZPTopAlignedLabel.h"
#import "ZPMonthYearPicker.h"
#import "ZPPickerList.h"
#import "ZPPaymentViewCell.h"

#import "ZPConfig.h"
#import "ZPPaymentChannel.h"

//Number
//Name
//Password
//Expire
//CVV
//Type sms or token



@interface ZPATMSaveCardCell : ZPPaymentViewCell

//Pass
@property (weak, nonatomic) UITableView *mTableView;
@property (weak, nonatomic) UIViewController * zpParentController;

@property (weak, nonatomic) IBOutlet UIView *mViewCardBG;
@property (weak, nonatomic) IBOutlet UIView *mViewCardBackCVV;
//Front
@property (weak, nonatomic) IBOutlet UILabel *mLblCardNumber;
@property (weak, nonatomic) IBOutlet ZPTopAlignedLabel *mLblCardName;
@property (weak, nonatomic) IBOutlet ZPTopAlignedLabel *mLblCardExpire;
@property (weak, nonatomic) IBOutlet UIImageView *mImgBG;
@property (weak, nonatomic) IBOutlet UIImageView *mImgIconBank;

//Back
@property (weak, nonatomic) IBOutlet UILabel *mLblCardCVV;
@property (weak, nonatomic) IBOutlet UIScrollView *mScrollTextfeild;

//Title
@property (weak, nonatomic) IBOutlet UILabel *mLblTitleCardName;
@property (weak, nonatomic) IBOutlet UILabel *mLblTitleCardExpire;

//Constraint
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_1_icon_number;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_1_number_date;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_1_date_name;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_top_cardview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_bottom_cardview;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_height_numberlbl;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_width_icon;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_height_icon;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_width_icon_back;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_height_icon_back;

@property (assign, nonatomic) BOOL isMapCard;
@property (assign, nonatomic) BOOL hasAmount;
@property (assign, nonatomic) long suggestAmount;
@property (strong, nonatomic) NSString * bankCode;
@property (strong, nonatomic) NSString * captcha;
@property (strong, nonatomic) NSString * otp;

@property (strong, nonatomic) NSString * lastBankCode;
@property (assign, nonatomic) BOOL hasDate;
@property (assign, nonatomic) BOOL hasPass;
@property (assign, nonatomic) BOOL hasAuthMethod;
@property (assign, nonatomic) long discountAmount;
@property (assign, nonatomic) double discountPercent;
@property (assign, nonatomic) ZPChannel *channel;

@property (assign, nonatomic) BOOL isShouldHideKB;
@property (assign, nonatomic) BOOL isEndDecelerating;
-(void) excuteView;
+ (ZPAtmInputType) inputTypeForBankCode: (NSString *) bankCode;
- (void)setCardNumber:(NSString *)number;
-(void)viewWillBack;
-(void)firstResponderKeyboardTextFieldCurrent;

-(JVFloatLabeledTextField*)getCurrentTf;
-(JVFloatLabeledTextField*)getTextFieldByType:(enum ZPATMSaveCardObjectType)type;
-(void)resetAll;

-(void)btnPreOnAccessoryViewClick;
@property (assign, nonatomic) ZPAtmPaymentStep currentStep;

//Add
+ (BOOL)checkInPutCardIsLuhnValid:(NSString *)cardNumber;

@end
