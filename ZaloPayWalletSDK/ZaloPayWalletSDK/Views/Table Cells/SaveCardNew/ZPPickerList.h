//
//  ZPPickerList.h
//  DemoCreditCardView
//
//  Created by CPU11689 on 9/26/16.
//  Copyright © 2016 CPU11689. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ZPPickerListDelegate<NSObject>
@required
- (void)ZPPickerListDelegate_onChangeData:(int) index arr:(NSMutableArray*) arr;
@end

@interface ZPPickerList : UIPickerView<UIPickerViewDataSource, UIPickerViewDelegate>

@property (strong, nonatomic) NSMutableArray * arrData;
@property id<ZPPickerListDelegate> pickerDelegate;

-(void)selectIndexCurrent:(int)index;

@end
