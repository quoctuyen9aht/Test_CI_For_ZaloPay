//
//  ZPResultSupportView.h
//  ZaloPay
//
//  Created by TaLi on 3/14/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZPResultRowView.h"

@interface ZPResultSupportView : ZPResultRowView
@property(unsafe_unretained, nonatomic) IBOutlet UIImageView *methodIcon;
@property(unsafe_unretained, nonatomic) IBOutlet UILabel *methodName;
@property(weak, nonatomic) IBOutlet UIImageView *arrowIcon;
@end
