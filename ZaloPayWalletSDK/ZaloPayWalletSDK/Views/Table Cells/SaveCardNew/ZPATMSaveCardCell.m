//
//  ZPATMSaveCard.m
//  ZaloPayWalletSDK
//
//  Created by CPU11689 on 9/22/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPATMSaveCardCell.h"
#import "ZPDataManager.h"
#import "NSString+ZPExtension.h"
#import "ZPResourceManager.h"
#import "UIColor+ZPExtension.h"
#import "ZPPaymentInfo.h"
#import "UIImage+ZPExtension.h"
#import "ZPManagerSaveCardCell.h"
#import "ZaloMobilePaymentSDK+ChargeableMethod.h"
#import "ZaloPayWalletSDKLog.h"
#import "ZPBank.h"
#import <ZaloPayCommon/UIView+Extention.h>
#import <ZaloPayAnalyticsSwift/ZaloPayAnalyticsSwift-Swift.h>

#define kWidthScreen [UIScreen mainScreen].bounds.size.width
#define kHeightScreen [UIScreen mainScreen].bounds.size.height
//#define kHeightInputView 88 //IS_IPAD ? 60 : 38
#define KMarginViewOnSV 0
#define kMarginTextField 10
#define kHeightCell 300

#define kHeightBtnAcceptOK IS_IPHONE_4_OR_LESS ? 25 : 48
#define kHeightBtnAccept 44

#define kTitleBtnPre @"Previous"
#define kTitleBtnNext @"Tiếp Tục"
#define kTitleBtnDone @"Xong"

#define kTitleBtnSMS @"SMS"
#define kTitleBtnToken @"Token"

#define kTitleConfirmTransition @"Hình thức xác thực giao dịch"

#define kIndexBtnSMS 0
#define kIndexBtnToken 1

#define kFormatDefaultNumber @"XXXX XXXX XXXX XXXX"
#define kFormatDefaultDate @"MM/YY"

#define kColorDefaultGradientStart @"#767A7B"
#define kColorDefaultGradientEnd @"#5C5D5F"
#define kFontNumberCard @"OCRATributeW01-RegularMono"
#define kFontNumberCardFile @"OCRA"

#define kColorHightLightLabel @"#ffffff"
#define kColorNormalLabel @"#c1c1c1"
#define kColorTextChange @"#4387f6"

#define kColorLineBottom @"#E3E6E7"

#define kWidthImageCaptcha 100

@interface ZPATMSaveCardCell () <UITextFieldDelegate, ZPMonthYearPickerDelegate, ZPPickerListDelegate, UIScrollViewDelegate> {

    NSString *titleAmount;
    NSString *titleNumber;
    NSString *titleName;
    NSString *titlePass;
    NSString *titleExpire;

    NSString *titleNameDefault;
    CGFloat kHeightInputView;

    CGFloat kHSizeNumberFontBig;

    
    NSMutableArray *arrTFStoreStep1;
    
    int lastIndexSV;
    BOOL isCurrentOrientationAniFlip;
    BOOL isHaveNumberField;
    BOOL isFirstShowKB;

    ZPMonthYearPicker *datePickerView;
    ZPPickerList *listConfirmTransitionPickerView;

    UIButton *mBtnSMS;
    UIButton *mBtnToken;

    NSArray *formats;
    NSArray *formatsDate;

    UIColor *lineColor;
    BOOL validFromDate;
    BOOL isCardDateValid;
    UIView *line;
    NSString *choseAuthMethod;
    NSString *choseFirstMethod;
    NSString *choseSecondMethod;

    CAGradientLayer *gradient;

    UIImage *imageCaptcha;
    BOOL isDontResetDefaultCardView;

    UIPageControl *pageControlCur;

    NSTimer *timerDelayNextPreClicked;
    NSTimer *timerEnableScroll;
    NSTimer *timerFirstBecome;

    UIView *viewPreNext;

    CGFloat heightKeyboard;
    CGFloat lastContentOffset;

    UIButton *btnNextCaptchaOTP;

    BOOL isViewBackClicked;
    BOOL isLoadedCaptchaView;
    UIImageView *imageView_Captcha;

    CGFloat scaleImgCaptchaOld;

    BOOL isScrollingToError;
    BOOL isHideKeyBoard;
    BOOL isBackFromCC;
}

@property (nonatomic) BOOL isScrolling;
@property (nonatomic, strong) NSMutableArray *arrTF;
@property (nonatomic, strong) NSString *titleExpireDefault;
@property (nonatomic) int indexCurrentSV;
@end

@implementation ZPATMSaveCardCell
#pragma mark - LIFE CYCLE

- (void)awakeFromNib {
    [super awakeFromNib];

    [self setupDefaultData];
    [self initView];
}
- (void)didMoveToSuperview {
    [super didMoveToSuperview];
    [self excuteView];
}
#pragma mark - Init
- (void)initView {

    //Variables
    formats = @[@" ", @(4), @(4), @(4), @(4)];
    formatsDate = @[@"/", @(2), @(2)];

    if (!isDontResetDefaultCardView) {

        //viewPreNext = nil;
        line = [[UIView alloc] init];
        choseAuthMethod = @"otp";
        self.isEndDecelerating = TRUE;
        timerDelayNextPreClicked = [[NSTimer alloc] init];
        timerEnableScroll = [[NSTimer alloc] init];
        timerFirstBecome = [[NSTimer alloc] init];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideKeyBoard:) name:ZPUIKeyboardDidHideNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideBtnPreNext:) name:ZPUIButtonPreNextDidHideNotification object:nil];

        [self loadMyCustomFont];
        [self setSizeWithOtherDevices];

        self.mLblTitleCardName.text = titleName;
        self.mLblTitleCardExpire.text = titleExpire;

        self.mLblCardName.text = titleNameDefault;


        self.mViewCardBG.layer.cornerRadius = 5;
        self.mViewCardBG.clipsToBounds = true;
        self.mViewCardBackCVV.alpha = 0;

        //self.mImgBG.image = [UIImage imageNamed:@"ico_map2"];
        self.mImgBG.image = [ZPResourceManager getImageWithName:@"ico_map"];


        JVFloatLabeledTextField *tfNumber = [self getTextFieldByType:ZPATMSaveCardObjectType_CardNumber];
        if (tfNumber.text.length == 0) {
            self.mLblCardNumber.text = kFormatDefaultNumber;
        }
        if (self.mLblCardExpire.text.length == 0) {
            self.mLblCardExpire.text = kFormatDefaultDate;
        }

        CGFloat offsetShadow = 0.1;
        self.mLblCardNumber.layer.shadowColor = [UIColor zpColorFromHexString:@"#7F000000"].CGColor;
        self.mLblCardNumber.layer.shadowOffset = CGSizeMake(offsetShadow, offsetShadow);
        self.mLblCardNumber.layer.shadowOpacity = 0.5;
        self.mLblCardNumber.layer.shadowRadius = 0.5;

        self.mLblCardName.layer.shadowColor = [UIColor zpColorFromHexString:@"#7F000000"].CGColor;
        self.mLblCardName.layer.shadowOffset = CGSizeMake(offsetShadow, offsetShadow);
        self.mLblCardName.layer.shadowOpacity = 0.5;
        self.mLblCardName.layer.shadowRadius = 0.5;

        self.mLblCardExpire.layer.shadowColor = [UIColor zpColorFromHexString:@"#7F000000"].CGColor;
        self.mLblCardExpire.layer.shadowOffset = CGSizeMake(offsetShadow, offsetShadow);
        self.mLblCardExpire.layer.shadowOpacity = 0.5;
        self.mLblCardExpire.layer.shadowRadius = 0.5;

        [self getFontSizeNumber];
        self.mViewCardBackCVV.alpha = 0;
        [self setGradientColorForCardView:self.bankCode];
        self.mViewCardBG.backgroundColor = [UIColor whiteColor];

        if (!self.hasDate) {
            self.mLblCardExpire.text = @"";
        }


    }

    self.isScrolling = NO;
    [self setupScrollViewTF];


}

- (void)excuteView {


    if (!isFirstShowKB) {
        isFirstShowKB = true;
        if (!self.isMapCard) {
            ZPATMSaveCardObject *mdGet = self.arrTF[0];
            [mdGet.mTextField becomeFirstResponder];
            self.isScrolling = NO;

        } else {
            double delayInSeconds = 0.1;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                [self firstResponderKeyboardTextField:0];
            });
        }

    } else {
        ZPATMSaveCardObject *mdGet = self.arrTF[0];
        [mdGet.mTextField becomeFirstResponder];
        self.isScrolling = NO;
    }

}

- (void)setupDefaultData {

    //Title
    titleAmount = @"Nhập số tiền";
    titleNumber = @"Nhập số thẻ";
    titleName = @"Nhập tên chủ thẻ";
    titlePass = @"Nhập mật khẩu DV thanh toán online";
    titleExpire = @"Nhập ngày hết hạn (mm/yy)";
    titleNameDefault = @""; //@"TÊN CHỦ THẺ";
    self.titleExpireDefault = @""; //@"NGÀY HẾT HẠN";

    self.bankCode = @"";
    self.lastBankCode = @"";

    self.arrTF = [NSMutableArray array];

    [self.arrTF addObject:[[ZPATMSaveCardObject alloc] initWithTitle:titleNumber andValue:@"" andPlaceHolder:@"" andType:ZPATMSaveCardObjectType_CardNumber]];
}

- (void)setupScrollViewTF {

    self.indexCurrentSV = 0;
    lastIndexSV = self.indexCurrentSV;
    isCurrentOrientationAniFlip = false;

    [self registerForKeyboardNotifications];

    self.mScrollTextfeild.pagingEnabled = true;
    self.mScrollTextfeild.showsVerticalScrollIndicator = false;
    self.mScrollTextfeild.showsHorizontalScrollIndicator = false;
    self.mScrollTextfeild.backgroundColor = [UIColor whiteColor];
    self.mScrollTextfeild.delegate = self;


    CGRect rectSV = self.mScrollTextfeild.frame;
    rectSV.size.width = kWidthScreen;// - rectSV.origin.x * 2;


    [self.mScrollTextfeild setContentSize:CGSizeMake((rectSV.size.width * self.arrTF.count), rectSV.size.height)];

    int index = 0;
    if (isHaveNumberField) {
        index = 1;
    }
    for (int i = index; i < self.arrTF.count; i++) {

        ZPATMSaveCardObject *mdGet = self.arrTF[i];
        CGRect rectTF = CGRectMake(rectSV.size.width * i + KMarginViewOnSV, KMarginViewOnSV, rectSV.size.width - KMarginViewOnSV * 2, rectSV.size.height - KMarginViewOnSV * 2);

        //UI Container
        UIView *view = [[UIView alloc] initWithFrame:rectTF];
        view.backgroundColor = [UIColor whiteColor];
        view.layer.cornerRadius = 0;
        view.clipsToBounds = true;
        view.translatesAutoresizingMaskIntoConstraints = true;
        view.tag = 10 + i;
        UITapGestureRecognizer *panGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapTF)];
        [view addGestureRecognizer:panGestureRecognizer];

        if (mdGet.mType == ZPATMSaveCardObjectType_OptionSMSToken) {
            JVFloatLabeledTextField *titleFieldFake = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectZero];
            mdGet.mTextField = titleFieldFake;
            mdGet.mTextField.delegate = self;
            mdGet.mTextField.hidden = true;
            mdGet.mTextField.inputView = [self getListConfirmTransitionPicker];
            mdGet.mTextField.autocorrectionType = UITextAutocorrectionTypeNo;
            mdGet.mTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
            mdGet.mTextField.spellCheckingType = UITextSpellCheckingTypeNo;
            mdGet.mTextField.returnKeyType = UIReturnKeyNext;
            titleFieldFake.isShowTopBottomLine = YES;
            titleFieldFake.isBGBottomLineDefault = YES;

            //[self setupInputViewForTF:titleFieldFake];

            [view addSubview:titleFieldFake];

            //Constaint
            [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(xMargin)-[titleFieldFake]-(1)-|" options:NSLayoutFormatAlignAllCenterY metrics:@{@"xMargin": @(kMarginTextField)} views:NSDictionaryOfVariableBindings(titleFieldFake)]];

            [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(1)-[titleFieldFake]-(1)-|" options:NSLayoutFormatAlignAllCenterX metrics:@{@"xMargin": @(kMarginTextField)} views:NSDictionaryOfVariableBindings(titleFieldFake)]];

            [self.mScrollTextfeild addSubview:view];
            [self setupBtnSMS_Token:view];

        } else if (mdGet.mType == ZPATMSaveCardObjectType_Captcha) {


            //TF
            JVFloatLabeledTextField *titleField = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectZero];
            if ([mdGet.mPlaceHolder isEqualToString:@""]) {
                [titleField setPlaceholder:mdGet.mTitle floatingTitle:mdGet.mTitle];
            } else {
                [titleField setPlaceholder:mdGet.mPlaceHolder floatingTitle:mdGet.mTitle];
            }


            titleField.keepBaseline = YES;
            titleField.alwaysShowFloatingLabel = YES;
            titleField.clearButtonMode = UITextFieldViewModeWhileEditing;
            titleField.isShowTopBottomLine = YES;
            titleField.isAlwayActiveTextColorFloating = YES;

            mdGet.mTextField = titleField;
            mdGet.mTextField.delegate = self;
            mdGet.mTextField.tag = i;
            [mdGet.mTextField addTarget:self action:@selector(changeTextOnCardView:) forControlEvents:UIControlEventEditingChanged];
            mdGet.mTextField.text = mdGet.mValue;
            mdGet.mTextField.autocorrectionType = UITextAutocorrectionTypeNo;
            mdGet.mTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
            mdGet.mTextField.spellCheckingType = UITextSpellCheckingTypeNo;
            mdGet.mTextField.inputView = nil;

            //Image
            UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectZero];
            imgV.translatesAutoresizingMaskIntoConstraints = false;
            imgV.backgroundColor = [UIColor clearColor];
            imgV.image = imageCaptcha;
            imgV.contentMode = UIViewContentModeScaleToFill;
            imageView_Captcha = imgV;

            [view addSubview:titleField];
            [view addSubview:imgV];
            [self.mScrollTextfeild addSubview:view];

            //Constaint
            [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(xMargin)-[titleField]-(1)-[imgV]-(10)-|" options:NSLayoutFormatAlignAllCenterY metrics:@{@"xMargin": @(kMarginTextField)} views:NSDictionaryOfVariableBindings(titleField, imgV)]];


            [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(1)-[titleField]-(1)-|" options:NSLayoutFormatAlignAllCenterX metrics:@{@"xMargin": @(kMarginTextField)} views:NSDictionaryOfVariableBindings(titleField)]];
            [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(10)-[imgV]-(10)-|" options:NSLayoutFormatAlignAllCenterX metrics:@{@"xMargin": @(kMarginTextField)} views:NSDictionaryOfVariableBindings(imgV)]];

            CGFloat widthImgCaptcha = rectTF.size.width * 0.4;
            if (scaleImgCaptchaOld) {
                if (scaleImgCaptchaOld * rectTF.size.height < widthImgCaptcha) {
                    widthImgCaptcha = scaleImgCaptchaOld * rectTF.size.height;
                } else {
                    widthImgCaptcha = scaleImgCaptchaOld / 2.0f * rectTF.size.height;

                }
            }
            [view addConstraint:[NSLayoutConstraint constraintWithItem:imgV
                                                             attribute:NSLayoutAttributeWidth
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1.0
                                                              constant:widthImgCaptcha]];

        } else {

            //TF
            JVFloatLabeledTextField *titleField = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectZero];
            if ([mdGet.mPlaceHolder isEqualToString:@""]) {
                [titleField setPlaceholder:mdGet.mTitle floatingTitle:mdGet.mTitle];
            } else {
                [titleField setPlaceholder:mdGet.mPlaceHolder floatingTitle:mdGet.mTitle];
            }


            titleField.keepBaseline = YES;
            titleField.alwaysShowFloatingLabel = YES;
            titleField.clearButtonMode = UITextFieldViewModeWhileEditing;
            titleField.isShowTopBottomLine = YES;
            titleField.isAlwayActiveTextColorFloating = YES;

            mdGet.mTextField = titleField;
            mdGet.mTextField.delegate = self;
            mdGet.mTextField.tag = i;
            [mdGet.mTextField addTarget:self action:@selector(changeTextOnCardView:) forControlEvents:UIControlEventEditingChanged];
            mdGet.mTextField.text = mdGet.mValue;
            mdGet.mTextField.autocorrectionType = UITextAutocorrectionTypeNo;
            mdGet.mTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
            mdGet.mTextField.spellCheckingType = UITextSpellCheckingTypeNo;

            if (mdGet.mType == ZPATMSaveCardObjectType_CardNumber) {
                mdGet.mTextField.keyboardType = UIKeyboardTypeNumberPad;
                [self setupInputViewForTFLineTopNumber:mdGet.mTextField];
            } else if (mdGet.mType == ZPATMSaveCardObjectType_CardExpire) {
                mdGet.mTextField.keyboardType = UIKeyboardTypeNumberPad;
                [self setupInputViewForTFLineTopNumber:mdGet.mTextField];
                //[mdGet.mTextField setInputView:[self getDatePicker]];
            }

            if (mdGet.mType == ZPATMSaveCardObjectType_CardAmount) {
                UILabel *rightView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
                rightView.font = [UIFont boldSystemFontOfSize:15];
                rightView.text = @"VND";
                rightView.textColor = zpRGB(200, 200, 200);
                rightView.backgroundColor = [UIColor clearColor];
                mdGet.mTextField.rightViewMode = UITextFieldViewModeAlways;
                mdGet.mTextField.rightView = rightView;

            }
            if (mdGet.mType == ZPATMSaveCardObjectType_CardPassword) {
                mdGet.mTextField.secureTextEntry = true;
            } else if (mdGet.mType == ZPATMSaveCardObjectType_OTP) {
                int keyboardType = [ZPResourceManager otpKeyboardType:_bankCode];
                [titleField setKeyboardType:keyboardType];
                [self setupInputViewForTFLineTopNumber:mdGet.mTextField];
            }


            //[self setupInputViewForTF:titleField];
            if (i == self.arrTF.count - 1) {
                mdGet.mTextField.returnKeyType = UIReturnKeyNext;
            }
            [view addSubview:titleField];
            [self.mScrollTextfeild addSubview:view];

            //Constaint
            [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(xMargin)-[titleField]-(1)-|" options:NSLayoutFormatAlignAllCenterY metrics:@{@"xMargin": @(kMarginTextField)} views:NSDictionaryOfVariableBindings(titleField)]];

            [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(1)-[titleField]-(1)-|" options:NSLayoutFormatAlignAllCenterX metrics:@{@"xMargin": @(kMarginTextField)} views:NSDictionaryOfVariableBindings(titleField)]];

        }

    }


    for (int i = 0; i < self.arrTF.count; i++) {
        ZPATMSaveCardObject *mdGet = self.arrTF[i];
        [mdGet.mTextField addTarget:self action:@selector(tapTF) forControlEvents:UIControlEventAllEvents];
        [mdGet.mTextField addTarget:self action:@selector(tapTF) forControlEvents:UIControlEventAllEvents];
    }


    [self.mScrollTextfeild setContentOffset:CGPointMake(0, 0)];
    isHaveNumberField = true;

}

- (void)setupInputViewForTFFRameOnlyAccept {
    if (viewPreNext || (self.currentStep != ZPAtmPaymentStepVietinBankCapCha && self.currentStep != ZPAtmPaymentStepVietinBankOTP && self.currentStep != ZPAtmPaymentStepChooseSMSToken)) {
        return;
    }

    CGFloat cellHeight = (SCREEN_WIDTH - kMarginCardView) * (1 / ratioCardView) + cardViewScrollHeight;

    CGFloat bottomContraintCell = 15;
    if (IS_IPHONE_4_OR_LESS) {
        cellHeight = (SCREEN_WIDTH - kMarginCardView) * (1 / (ratioCardView + 1)) + cardViewScrollHeight;
    }
    CGRect rectInput = CGRectMake(0, cellHeight - bottomContraintCell + 4, kWidthScreen, kHeightInputView + bottomContraintCell);


    if (self.currentStep == ZPAtmPaymentStepChooseSMSToken) {
        rectInput.origin.y = cellHeight + 66 + 45 - 20;
        rectInput.size.height = kHeightBtnAcceptOK + 10;
    }
    viewPreNext = [[UIView alloc] initWithFrame:rectInput];
    viewPreNext.tag = 100;
    viewPreNext.backgroundColor = [UIColor clearColor];
    [self.zpParentController.view addSubview:viewPreNext];

    CGFloat heightViewContainer = kHeightBtnAcceptOK;

    CGRect rectContainer = CGRectMake(0, rectInput.size.height / 2.0 - heightViewContainer / 2.0 + 3, kWidthScreen, heightViewContainer);
    if (IS_IPHONE_4_OR_LESS) {
        rectContainer = CGRectMake(0, rectInput.size.height / 2.0 - heightViewContainer / 2.0, kWidthScreen, heightViewContainer);
    }
    UIView *viewContainer = [[UIView alloc] initWithFrame:rectContainer];
    [viewPreNext addSubview:viewContainer];

    //BtnPre
    btnNextCaptchaOTP = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, kWidthScreen - 20, heightViewContainer)];
    //Need
    [btnNextCaptchaOTP setTitle:kTitleBtnNext forState:UIControlStateNormal];
    [btnNextCaptchaOTP addTarget:self action:@selector(btnOkOnAccessoryViewClick) forControlEvents:UIControlEventTouchUpInside];
    btnNextCaptchaOTP.tag = 20;
    if (self.currentStep != ZPAtmPaymentStepChooseSMSToken) {
        btnNextCaptchaOTP.enabled = false;
    }

    [btnNextCaptchaOTP setBackgroundImage:[UIImage zpImageWithColor:[UIColor zaloBaseColor] withSize:CGSizeMake(80, 80)] forState:UIControlStateNormal];
    [btnNextCaptchaOTP setBackgroundImage:[UIImage zpImageWithColor:[UIColor disableButtonColor] withSize:CGSizeMake(80, 80)] forState:UIControlStateDisabled];
    [btnNextCaptchaOTP setBackgroundImage:[UIImage zpImageWithColor:[UIColor buttonHighlightColor] withSize:CGSizeMake(80, 80)] forState:UIControlStateHighlighted];

    [viewContainer addSubview:btnNextCaptchaOTP];

    viewContainer.backgroundColor = [UIColor clearColor];
    //viewInput.backgroundColor =[UIColor clearColor];

    btnNextCaptchaOTP.backgroundColor = [UIColor clearColor];

    btnNextCaptchaOTP.clipsToBounds = true;
    btnNextCaptchaOTP.layer.cornerRadius = 4;
}

- (void)setupInputViewForTFFRame {
    if (viewPreNext || self.currentStep == ZPAtmPaymentStepVietinBankCapCha || self.currentStep == ZPAtmPaymentStepVietinBankOTP || self.currentStep == ZPAtmPaymentStepChooseSMSToken) {
        return;
    }

    CGFloat cellHeight = (SCREEN_WIDTH - kMarginCardView) * (1 / ratioCardView) + cardViewScrollHeight;

    CGFloat bottomContraintCell = 15;
    if (IS_IPHONE_4_OR_LESS) {
        cellHeight = (SCREEN_WIDTH - kMarginCardView) * (1 / (ratioCardView + 1)) + cardViewScrollHeight;
    }
    CGFloat heightSafeArea = [UIView safeAreaInsets].top;
    CGRect rectInput = CGRectMake(0, cellHeight - bottomContraintCell + 4 + heightSafeArea / 2, kWidthScreen, kHeightInputView + bottomContraintCell - heightSafeArea);
    viewPreNext = [[UIView alloc] initWithFrame:rectInput];
    viewPreNext.tag = 100;
    //viewInput.translatesAutoresizingMaskIntoConstraints = false;
    viewPreNext.backgroundColor = [UIColor clearColor];
    [self.zpParentController.view addSubview:viewPreNext];
    //[self.contentView addSubview:viewPreNext];

    CGFloat heightViewContainer = kHeightBtnAccept;
    if (IS_IPHONE_4_OR_LESS) {
        heightViewContainer = 30;
    }
    CGRect rectContainer = CGRectMake(0, rectInput.size.height / 2.0 - heightViewContainer / 2.0 + 3, kWidthScreen, heightViewContainer + 1);
    if (IS_IPHONE_4_OR_LESS) {
        rectContainer = CGRectMake(0, rectInput.size.height / 2.0 - heightViewContainer / 2.0, kWidthScreen, heightViewContainer + 1);
    }
    UIView *viewContainer = [[UIView alloc] initWithFrame:rectContainer];

    [viewPreNext addSubview:viewContainer];
    //BtnPre
    UIButton *btnPre = [[UIButton alloc] initWithFrame:CGRectMake(kWidthScreen / 2.0 - 80 / 2.0 - 44, 0, heightViewContainer, heightViewContainer)];
    //Need
    [btnPre setImage:[UIImage zpImageResize:[ZPResourceManager getImageWithName:@"ico_pre"] andResizeTo:CGSizeMake(10, 17) andOffset:CGPointZero] forState:UIControlStateNormal];
    [btnPre setImageEdgeInsets:UIEdgeInsetsMake(0, -3, 0, 0)];
    [btnPre setBackgroundImage:[UIImage zpImageWithColor:[UIColor zpColorFromHexString:@"#008fe4"] withSize:CGSizeMake(heightViewContainer, heightViewContainer)] forState:UIControlStateNormal];
    [btnPre setBackgroundImage:[UIImage zpImageWithColor:[UIColor zpColorFromHexString:@"#78c1ec"] withSize:CGSizeMake(heightViewContainer, heightViewContainer)] forState:UIControlStateDisabled];
    [btnPre addTarget:self action:@selector(btnPreOnAccessoryViewClick) forControlEvents:UIControlEventTouchUpInside];
    btnPre.tag = 1;
    if (self.indexCurrentSV == 0) {
        btnPre.enabled = false;
    }


    //BtnNext
    UIButton *btnNext = [[UIButton alloc] initWithFrame:CGRectMake(kWidthScreen / 2.0 + 80 / 2.0, 0, heightViewContainer, heightViewContainer)];
    [btnNext addTarget:self action:@selector(btnNextOnAccessoryViewClick) forControlEvents:UIControlEventTouchUpInside];
    [btnNext setImage:[UIImage zpImageResize:[ZPResourceManager getImageWithName:@"ico_next"]
                                 andResizeTo:CGSizeMake(10, 17) andOffset:CGPointZero] forState:UIControlStateNormal];
    [btnNext setImageEdgeInsets:UIEdgeInsetsMake(0, 3, 0, 0)];
    [btnNext setBackgroundImage:[UIImage zpImageWithColor:[UIColor zpColorFromHexString:@"#008fe4"] withSize:CGSizeMake(heightViewContainer, heightViewContainer)] forState:UIControlStateNormal];
    [btnNext setBackgroundImage:[UIImage zpImageWithColor:[UIColor zpColorFromHexString:@"#78c1ec"] withSize:CGSizeMake(heightViewContainer, heightViewContainer)] forState:UIControlStateDisabled];
    btnNext.tag = 2;

    //Page control
    pageControlCur = [[UIPageControl alloc] initWithFrame:CGRectMake(kWidthScreen / 2.0 - 80 / 2.0, 0, 80, heightViewContainer)];
    pageControlCur.pageIndicatorTintColor = [UIColor lightGrayColor];
    pageControlCur.currentPageIndicatorTintColor = [UIColor grayColor];
    if (self.bankCode.length == 0 || self.bankCode == nil) {
        pageControlCur.numberOfPages = kDefaultStepCardView;
    } else {
        pageControlCur.numberOfPages = self.arrTF.count;
    }
    pageControlCur.currentPage = self.indexCurrentSV;
    pageControlCur.tag = 10;
    pageControlCur.userInteractionEnabled = false;

    //[self updatePageControl:tf];


    [viewContainer addSubview:btnPre];
    [viewContainer addSubview:btnNext];
    [viewContainer addSubview:pageControlCur];

    viewContainer.backgroundColor = [UIColor clearColor];

    btnPre.backgroundColor = [UIColor clearColor];
    btnNext.backgroundColor = [UIColor clearColor];
    pageControlCur.backgroundColor = [UIColor clearColor];

    btnPre.clipsToBounds = true;
    btnPre.layer.cornerRadius = heightViewContainer / 2;

    btnNext.clipsToBounds = true;
    btnNext.layer.cornerRadius = heightViewContainer / 2;

    //tf.inputAccessoryView = viewInput;

    if (self.currentStep == ZPAtmPaymentStepVietinBankOTP || self.currentStep == ZPAtmPaymentStepVietinBankCapCha) {
        // tf.inputAccessoryView = nil;
    }

    if (self.currentStep == ZPAtmPaymentStepBIDVCapChaPassword) {
        btnPre.enabled = NO;
    }     //viewPreNext = viewInput;

}

- (void)setupInputViewForTFLineTopNumber:(UITextField *)tf {
    CGRect rectInput = CGRectMake(0, 0, kWidthScreen, 1);
    UIView *viewInput = [[UIView alloc] initWithFrame:rectInput];
    viewInput.translatesAutoresizingMaskIntoConstraints = false;
    viewInput.backgroundColor = [UIColor zpColorFromHexString:@"#E3E6E7"];
    tf.inputAccessoryView = viewInput;
}

- (void)setupInputViewForTF:(UITextField *)tf {

    CGRect rectInput = CGRectMake(0, 0, kWidthScreen, kHeightInputView);
    UIView *viewInput = [[UIView alloc] initWithFrame:rectInput];
    viewInput.translatesAutoresizingMaskIntoConstraints = false;


    CGRect rectContainer = CGRectMake(0, 0, kWidthScreen, 44);
    UIView *viewContainer = [[UIView alloc] initWithFrame:rectContainer];
    viewContainer.translatesAutoresizingMaskIntoConstraints = false;

    //Line
    UIView *lineBottom = [[UIView alloc] initWithFrame:CGRectZero];
    lineBottom.backgroundColor = [UIColor zpColorFromHexString:@"#E3E6E7"];
    lineBottom.translatesAutoresizingMaskIntoConstraints = false;

    [viewInput addSubview:viewContainer];
    [viewInput addSubview:lineBottom];

    [viewInput addConstraint:[NSLayoutConstraint constraintWithItem:viewContainer
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.0
                                                           constant:SCREEN_WIDTH]];
    [viewInput addConstraint:[NSLayoutConstraint constraintWithItem:viewInput
                                                          attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:viewContainer
                                                          attribute:NSLayoutAttributeCenterY
                                                         multiplier:1.0
                                                           constant:0]];
    [viewInput addConstraint:[NSLayoutConstraint constraintWithItem:viewInput
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:lineBottom
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.0
                                                           constant:0.5]];
    [viewInput addConstraint:[NSLayoutConstraint constraintWithItem:lineBottom
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.0
                                                           constant:SCREEN_WIDTH]];
    [viewInput addConstraint:[NSLayoutConstraint constraintWithItem:lineBottom
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.0
                                                           constant:0.5]];

    //BtnPre
    UIButton *btnPre = [[UIButton alloc] initWithFrame:CGRectZero];
    //Need
    [btnPre setImage:[UIImage zpImageResize:[ZPResourceManager getImageWithName:@"ico_pre"] andResizeTo:CGSizeMake(10, 13) andOffset:CGPointZero] forState:UIControlStateNormal];
    [btnPre setBackgroundImage:[UIImage zpImageWithColor:[UIColor zpColorFromHexString:@"#008fe4"] withSize:CGSizeMake(80, 80)] forState:UIControlStateNormal];
    [btnPre setBackgroundImage:[UIImage zpImageWithColor:[UIColor zpColorFromHexString:@"#78c1ec"] withSize:CGSizeMake(80, 80)] forState:UIControlStateDisabled];


    [btnPre addTarget:self action:@selector(btnPreOnAccessoryViewClick) forControlEvents:UIControlEventTouchUpInside];
    //BtnNext
    UIButton *btnNext = [[UIButton alloc] initWithFrame:CGRectZero];
    //[btnNext setTitle:kTitleBtnNext forState:UIControlStateNormal];
    [btnNext addTarget:self action:@selector(btnNextOnAccessoryViewClick) forControlEvents:UIControlEventTouchUpInside];

    [btnNext setImage:[UIImage zpImageResize:[ZPResourceManager getImageWithName:@"ico_next"] andResizeTo:CGSizeMake(10, 13) andOffset:CGPointZero] forState:UIControlStateNormal];
    [btnNext setBackgroundImage:[UIImage zpImageWithColor:[UIColor zpColorFromHexString:@"#008fe4"] withSize:CGSizeMake(80, 80)] forState:UIControlStateNormal];
    [btnNext setBackgroundImage:[UIImage zpImageWithColor:[UIColor zpColorFromHexString:@"#78c1ec"] withSize:CGSizeMake(80, 80)] forState:UIControlStateDisabled];

    //Page control
    UIPageControl *pageControl = [[UIPageControl alloc] initWithFrame:CGRectZero];
    pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    pageControl.currentPageIndicatorTintColor = [UIColor grayColor];
    if (self.bankCode.length == 0 || self.bankCode == nil) {
        pageControl.numberOfPages = kDefaultStepCardView;
    } else {
        pageControl.numberOfPages = self.arrTF.count;
    }
    pageControl.currentPage = self.indexCurrentSV;
    pageControl.tag = 10;
    pageControl.userInteractionEnabled = false;
    pageControlCur = pageControl;
    [self updatePageControl:tf];


    [viewContainer addSubview:btnPre];
    [viewContainer addSubview:btnNext];
    [viewContainer addSubview:pageControl];

    viewContainer.backgroundColor = [UIColor clearColor];
    viewInput.backgroundColor = [UIColor clearColor];

    btnPre.backgroundColor = [UIColor clearColor];
    btnNext.backgroundColor = [UIColor clearColor];
    pageControl.backgroundColor = [UIColor clearColor];

    btnPre.clipsToBounds = true;
    btnPre.layer.cornerRadius = 44 / 2;

    btnNext.clipsToBounds = true;
    btnNext.layer.cornerRadius = 44 / 2;


    btnPre.translatesAutoresizingMaskIntoConstraints = false;
    btnNext.translatesAutoresizingMaskIntoConstraints = false;
    pageControl.translatesAutoresizingMaskIntoConstraints = false;


    [viewContainer addConstraint:[NSLayoutConstraint constraintWithItem:btnPre
                                                              attribute:NSLayoutAttributeWidth
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeNotAnAttribute
                                                             multiplier:1.0
                                                               constant:44]];
    [viewContainer addConstraint:[NSLayoutConstraint constraintWithItem:btnNext
                                                              attribute:NSLayoutAttributeWidth
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeNotAnAttribute
                                                             multiplier:1.0
                                                               constant:44]];
    [viewContainer addConstraint:[NSLayoutConstraint constraintWithItem:pageControl
                                                              attribute:NSLayoutAttributeWidth
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeNotAnAttribute
                                                             multiplier:1.0
                                                               constant:100]];


    [viewContainer addConstraint:[NSLayoutConstraint
            constraintWithItem:btnPre
                     attribute:NSLayoutAttributeWidth
                     relatedBy:0
                        toItem:btnPre
                     attribute:NSLayoutAttributeHeight
                    multiplier:1.0
                      constant:0]];

    [viewContainer addConstraint:[NSLayoutConstraint
            constraintWithItem:btnNext
                     attribute:NSLayoutAttributeWidth
                     relatedBy:0
                        toItem:btnNext
                     attribute:NSLayoutAttributeHeight
                    multiplier:1.0
                      constant:0]];
    [viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(xMargin)-[btnPre]-(xMargin)-|" options:NSLayoutFormatAlignAllCenterY metrics:@{@"xMargin": @(1)} views:NSDictionaryOfVariableBindings(btnPre)]];

    [viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(xMargin)-[btnNext]-(xMargin)-|" options:NSLayoutFormatAlignAllCenterY metrics:@{@"xMargin": @(1)} views:NSDictionaryOfVariableBindings(btnNext)]];

    [viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(xMargin)-[pageControl]-(xMargin)-|" options:NSLayoutFormatAlignAllCenterY metrics:@{@"xMargin": @(1)} views:NSDictionaryOfVariableBindings(pageControl)]];


    [viewContainer addConstraint:[NSLayoutConstraint constraintWithItem:btnNext
                                                              attribute:NSLayoutAttributeTrailing
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:viewContainer
                                                              attribute:NSLayoutAttributeTrailing
                                                             multiplier:1.0
                                                               constant:-5]];

    [viewContainer addConstraint:[NSLayoutConstraint constraintWithItem:btnPre
                                                              attribute:NSLayoutAttributeLeading
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:viewContainer
                                                              attribute:NSLayoutAttributeLeading
                                                             multiplier:1.0
                                                               constant:5]];

    [viewContainer addConstraint:[NSLayoutConstraint constraintWithItem:pageControl
                                                              attribute:NSLayoutAttributeCenterX
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:viewContainer
                                                              attribute:NSLayoutAttributeCenterX
                                                             multiplier:1.0
                                                               constant:0]];

    //tf.inputAccessoryView = viewInput;

    //set enable and disable
    if (self.indexCurrentSV == 0) {
        btnPre.enabled = false;
        //btnNext.enabled = arrTF.count == 1 ? false : true ;
    } else {
        btnPre.enabled = true;
        btnNext.enabled = true;
    }

    if (self.currentStep == ZPAtmPaymentStepVietinBankOTP || self.currentStep == ZPAtmPaymentStepVietinBankCapCha) {
        // tf.inputAccessoryView = nil;
    }

    //viewPreNext = viewInput;
}

- (void)setupBtnSMS_Token:(UIView *)viewContainer {


    //View Left
    UIView *viewLeft = [[UIView alloc] initWithFrame:CGRectZero];

    //View Right
    UIView *viewRight = [[UIView alloc] initWithFrame:CGRectZero];

    //BtnPre
    UIButton *btnSMS = [[UIButton alloc] initWithFrame:CGRectZero];
    [btnSMS addTarget:self action:@selector(btnSMSClick) forControlEvents:UIControlEventTouchUpInside];
    mBtnSMS = btnSMS;
    //BtnNext
    UIButton *btnToken = [[UIButton alloc] initWithFrame:CGRectZero];
    [btnToken addTarget:self action:@selector(btnTokenClick) forControlEvents:UIControlEventTouchUpInside];
    mBtnToken = btnToken;

    UILabel *lblSmS = [[UILabel alloc] initWithFrame:CGRectZero];
    lblSmS.text = kTitleBtnSMS;

    UILabel *lblToken = [[UILabel alloc] initWithFrame:CGRectZero];
    lblToken.text = kTitleBtnToken;

    //Title confirm
    UILabel *lblConfirm = [[UILabel alloc] initWithFrame:CGRectZero];
    lblConfirm.text = kTitleConfirmTransition;
    [lblConfirm setFont:[UIFont systemFontOfSize:12]];

    [viewContainer addSubview:viewLeft];
    [viewContainer addSubview:viewRight];
    [viewContainer addSubview:lblConfirm];


    [viewLeft addSubview:btnSMS];
    [viewLeft addSubview:lblSmS];

    [viewRight addSubview:btnToken];
    [viewRight addSubview:lblToken];


    btnSMS.backgroundColor = [UIColor clearColor];
    btnToken.backgroundColor = [UIColor clearColor];
    btnSMS.translatesAutoresizingMaskIntoConstraints = false;
    btnToken.translatesAutoresizingMaskIntoConstraints = false;

    [btnSMS setImage:[ZPResourceManager getImageWithName:@"ico_radioactive"] forState:UIControlStateSelected];
    [btnSMS setImage:[ZPResourceManager getImageWithName:@"ic_unchecked-mark"] forState:UIControlStateNormal];
    [btnToken setImage:[ZPResourceManager getImageWithName:@"ico_radioactive"] forState:UIControlStateSelected];
    [btnToken setImage:[ZPResourceManager getImageWithName:@"ic_unchecked-mark"] forState:UIControlStateNormal];
    btnSMS.selected = YES;

    lblSmS.translatesAutoresizingMaskIntoConstraints = false;
    lblToken.translatesAutoresizingMaskIntoConstraints = false;

    viewLeft.translatesAutoresizingMaskIntoConstraints = false;
    viewRight.translatesAutoresizingMaskIntoConstraints = false;
    lblConfirm.translatesAutoresizingMaskIntoConstraints = false;


    //View left

    [viewLeft addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(xMargin)-[btnSMS]-(xMargin)-|" options:NSLayoutFormatAlignAllCenterX metrics:@{@"xMargin": @(1)} views:NSDictionaryOfVariableBindings(btnSMS)]];
    [viewLeft addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(xMargin)-[lblSmS]-(xMargin)-|" options:NSLayoutFormatAlignAllCenterX metrics:@{@"xMargin": @(1)} views:NSDictionaryOfVariableBindings(lblSmS)]];

    [viewLeft addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(xMargin)-[btnSMS(==52)]-(-10)-[lblSmS]-(xMargin)-|" options:NSLayoutFormatAlignAllCenterY metrics:@{@"xMargin": @(1)} views:NSDictionaryOfVariableBindings(btnSMS, lblSmS)]];

    //View right
    [viewRight addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(xMargin)-[btnToken]-(xMargin)-|" options:NSLayoutFormatAlignAllCenterX metrics:@{@"xMargin": @(1)} views:NSDictionaryOfVariableBindings(btnToken)]];
    [viewRight addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(xMargin)-[lblToken]-(xMargin)-|" options:NSLayoutFormatAlignAllCenterX metrics:@{@"xMargin": @(1)} views:NSDictionaryOfVariableBindings(lblToken)]];

    [viewRight addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(xMargin)-[btnToken(==52)]-(-10)-[lblToken]-(xMargin)-|" options:NSLayoutFormatAlignAllCenterY metrics:@{@"xMargin": @(1)} views:NSDictionaryOfVariableBindings(btnToken, lblToken)]];

    //View left and right for view parent
    [viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(20)-[viewLeft]-(xMargin)-|" options:NSLayoutFormatAlignAllCenterX metrics:@{@"xMargin": @(1)} views:NSDictionaryOfVariableBindings(viewLeft)]];
    [viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(20)-[viewRight]-(xMargin)-|" options:NSLayoutFormatAlignAllCenterX metrics:@{@"xMargin": @(1)} views:NSDictionaryOfVariableBindings(viewRight)]];
    [viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(xMargin)-[viewLeft]-[viewRight]-(xMargin)-|" options:NSLayoutFormatAlignAllCenterY metrics:@{@"xMargin": @(1)} views:NSDictionaryOfVariableBindings(viewLeft, viewRight)]];
    [viewContainer addConstraint:[NSLayoutConstraint
            constraintWithItem:viewLeft
                     attribute:NSLayoutAttributeWidth
                     relatedBy:0
                        toItem:viewRight
                     attribute:NSLayoutAttributeWidth
                    multiplier:1.0
                      constant:0]];


    //Lbl Confirm
    [viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(8)-[lblConfirm]-(35)-|" options:NSLayoutFormatAlignAllCenterX metrics:@{@"xMargin": @(1)} views:NSDictionaryOfVariableBindings(lblConfirm)]];
    [viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(15)-[lblConfirm]-(xMargin)-|" options:NSLayoutFormatAlignAllCenterY metrics:@{@"xMargin": @(1)} views:NSDictionaryOfVariableBindings(lblConfirm)]];


}

- (void)setSizeWithOtherDevices {

    //Remove bottom constrain scrollview
    for (NSLayoutConstraint *con in self.contentView.constraints) {
        if (con.firstItem == self.contentView || con.secondItem == self.mScrollTextfeild) {
            if (con.constant == 8) {
                con.constant = 0;
            }
        }
    }
    if (IS_IPHONE_4_OR_LESS || IS_IPHONE_5) {

        self.constraint_1_icon_number.constant = 5;
        self.constraint_1_number_date.constant = 20;
        self.constraint_1_date_name.constant = 0;
        kHeightInputView = 50;
        self.constraint_top_cardview.constant = 10;
        self.constraint_bottom_cardview.constant = 10;

        self.constraint_height_numberlbl.constant = 30;


        self.constraint_width_icon.constant = 60;
        self.constraint_height_icon.constant = 20;

    } else {
        self.constraint_1_icon_number.constant = 10;
        self.constraint_1_number_date.constant = 30;
        self.constraint_1_date_name.constant = 5;
        self.constraint_top_cardview.constant = 15;
        self.constraint_bottom_cardview.constant = 15;

        kHeightInputView = 120;

        //kHSizeNumberFontBig = 20;
        // kHSizeNumberFontSmall = 18;
        self.constraint_height_numberlbl.constant = 30;

        self.constraint_width_icon.constant = 75;
        self.constraint_height_icon.constant = 25;


    }
    if (IS_IPHONE_4_OR_LESS) {
        self.constraint_top_cardview.constant = 0;
        self.constraint_bottom_cardview.constant = 5;

        self.constraint_height_icon.constant = 0;
        self.constraint_width_icon.constant = 0;
        self.constraint_1_icon_number.constant = 0;
        self.constraint_1_number_date.constant = 0;
        self.mImgIconBank.hidden = true;
    }
    if (IS_IPHONE_6) {
        self.constraint_width_icon.constant = 90;
        self.constraint_height_icon.constant = 30;

        self.constraint_1_icon_number.constant = 15;
        self.constraint_1_number_date.constant = 20;


    }
    if (IS_IPHONE_6P) {
        self.constraint_width_icon.constant = 105;
        self.constraint_height_icon.constant = 35;

        self.constraint_1_icon_number.constant = 20;
        self.constraint_1_number_date.constant = 30;


    }
    [self setNeedsLayout];
}

#pragma mark - Event Fake Data
#pragma mark - Others

- (void)scrollToIndex:(int)index {
    [self.mScrollTextfeild setContentOffset:CGPointMake(self.mScrollTextfeild.frame.size.width * index, 0) animated:true];
}

- (void)firstResponderKeyboardTextField:(int)index {
    //ZPATMSaveCardObject *mdGet = arrTF[index];
    lastIndexSV = index;
    [self setTimerFirstBecome];

}

- (void)firstResponderKeyboardTextFieldByTF {

    //Hide KB Bank is maintance
    if ([ZPDialogView isDialogShowing]) {
        return;
    }

    if (isHideKeyBoard && self.currentStep != ZPAtmPaymentStepVietinBankCapCha && self.currentStep != ZPAtmPaymentStepVietinBankOTP && isBackFromCC) {
        isHideKeyBoard = NO;
        isBackFromCC = NO;
        return;
    }

    ZPATMSaveCardObject *mdGet = self.arrTF[lastIndexSV];
    if (self.isEndDecelerating) {
        if (![ZPDialogView isDialogShowing]) {
            [mdGet.mTextField becomeFirstResponder];
        }
        self.isScrolling = NO;
    }
}

- (void)firstResponderKeyboardTextField_Old:(int)index {
    ZPATMSaveCardObject *mdGet = self.arrTF[index];
    lastIndexSV = index;
    double delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        if (self.isEndDecelerating) {
            [mdGet.mTextField becomeFirstResponder];
            self.isScrolling = NO;
        }
    });

}

- (void)firstResponderKeyboardTextFieldCurrent {
    [self firstResponderKeyboardTextField:self.indexCurrentSV];
}

- (void)resignKeyboardTextField:(int)index {
    ZPATMSaveCardObject *mdGet = self.arrTF[index];
    [mdGet.mTextField resignFirstResponder];

}

- (void)changeTextOnCardView:(UITextField *)tf {

    [self hightLightLabelOnCard_NotUse:tf];

    int indexChange = (int) tf.tag;

    //for(int i = 0 ; i < arrTF.count ; i++){

    ZPATMSaveCardObject *mdGet = self.arrTF[indexChange];
    if (mdGet.mType == ZPATMSaveCardObjectType_CardNumber) {
        //self.mLblCardNumber.text =  [self getFormatXXXXDefaultNumber:mdGet.mTextField.text];//mdGet.mTextField.text;


        [self checkAutocomplete:mdGet.mTextField.text andType:mdGet.mType];

        //Check
        if ([self.bankCode length] <= 0 && [mdGet.mTextField.text length] >= 6 + 1) {
            if (self.bankCode != nil && [self.bankCode length] > 0) {
            } else {

                [(JVFloatLabeledTextField *) mdGet.mTextField setFloatingTitleErrorWithImage:@"Chưa hỗ trợ ngân hàng này" andImage:[ZPResourceManager getImageWithName:@"question"]];
                [self setupFloatingLabelShowBankSupport:(JVFloatLabeledTextField *) mdGet.mTextField];
            }
        }

        double delayInSeconds = 0.2;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            [self setFontSizeByLength:mdGet.mTextField];
        });

        if (mdGet.mTextField.text.length == 0) {
            [self resetAll];
        }
        if (self.bankCode == nil || self.bankCode.length == 0) {
            self.mLblCardExpire.text = @"";
            [self resetAll];
        } else {
            JVFloatLabeledTextField *tfDate = [self getTextFieldByType:ZPATMSaveCardObjectType_CardExpire];
            if (tfDate.text.length == 0) {
                if (self.hasDate) {
                    self.mLblCardExpire.text = self.titleExpireDefault;
                } else {
                    self.mLblCardExpire.text = @"";
                }
            }

        }

        [self getFormatXHighLight:mdGet.mTextField andDefault:false];

        return;
    }

    if (mdGet.mType == ZPATMSaveCardObjectType_CardName) {
        self.mLblCardName.text = mdGet.mTextField.text;
        [self getFormatXHighLight:mdGet.mTextField andDefault:false];
        return;
    }
    if (mdGet.mType == ZPATMSaveCardObjectType_CardExpire) {
        self.mLblCardExpire.text = [self getFormatXXXXDefaultDate:mdGet.mTextField.text];
        if (mdGet.mTextField.text.length == 0) {
            self.mLblCardExpire.text = kFormatDefaultDate;
        }

        //Add slash
        NSString *string = mdGet.mTextField.text;
        if (string.length == 2) {

            mdGet.mTextField.text = [string stringByAppendingString:@"/"];

        }

        isCardDateValid = [self checkValidateDate:self.mLblCardExpire.text];

        NSString *errorMessage = @"";
        if (!isCardDateValid && self.hasDate && mdGet.mTextField.text.length == kFormatDefaultDate.length) {
            if (validFromDate) {
                errorMessage = @"Ngày phát hành không hợp lệ";
            } else {
                errorMessage = @"Ngày hết hạn không hợp lệ";
            }
            [self textFieldShowError:(JVFloatLabeledTextField *) mdGet.mTextField withErrorText:errorMessage];
        }

        [self checkAutocomplete:mdGet.mTextField.text andType:mdGet.mType];
        [self getFormatXHighLight:mdGet.mTextField andDefault:false];
        return;
    }
    if (mdGet.mType == ZPATMSaveCardObjectType_CardCVV) {
        self.mLblCardCVV.text = mdGet.mTextField.text;

        [self checkAutocomplete:mdGet.mTextField.text andType:mdGet.mType];
        [self getFormatXHighLight:mdGet.mTextField andDefault:false];
        return;
    }
    if (mdGet.mType == ZPATMSaveCardObjectType_Captcha) {
        self.captcha = mdGet.mTextField.text;

        // return;
    }

    if (self.currentStep == ZPAtmPaymentStepVietinBankCapCha || self.currentStep == ZPAtmPaymentStepVietinBankOTP) {

        if ([self checkValidateTextField]) {

            //[self updateBtnOk:tf andEnable:YES];
            [self updateBtnOk:YES];
        } else {

            // [self updateBtnOk:tf andEnable:NO];
            [self updateBtnOk:NO];
        }

    }


    //}
}
- (void)updateTextForTfByType:(enum ZPATMSaveCardObjectType)type withText:(NSString *)text {

    for (int i = 0; i < self.arrTF.count; i++) {

        ZPATMSaveCardObject *mdGet = self.arrTF[i];

        if (mdGet.mType == type) {
            mdGet.mTextField.text = text;
            break;
        }

    }

}

- (JVFloatLabeledTextField *)getTextFieldByType:(enum ZPATMSaveCardObjectType)type {

    for (int i = 0; i < self.arrTF.count; i++) {

        ZPATMSaveCardObject *mdGet = self.arrTF[i];

        if (mdGet.mType == type) {
            return (JVFloatLabeledTextField *) mdGet.mTextField;
        }

    }
    JVFloatLabeledTextField *tf = [[JVFloatLabeledTextField alloc] init];
    tf.tag = -1;
    if (type == ZPATMSaveCardObjectType_CardAmount) {
        tf.text = [NSString stringWithFormat:@"%ld", self.suggestAmount];
    }
    return tf;
}

- (JVFloatLabeledTextField *)getTextFieldByTypeStoreStep1:(enum ZPATMSaveCardObjectType)type {

    for (int i = 0; i < arrTFStoreStep1.count; i++) {

        ZPATMSaveCardObject *mdGet = arrTFStoreStep1[i];

        if (mdGet.mType == type) {
            return (JVFloatLabeledTextField *) mdGet.mTextField;
        }

    }
    JVFloatLabeledTextField *tf = [[JVFloatLabeledTextField alloc] init];
    if (type == ZPATMSaveCardObjectType_CardAmount) {
        tf.text = [NSString stringWithFormat:@"%ld", self.suggestAmount];
    }
    return tf;
}

- (enum ZPATMSaveCardObjectType)getTypeByTextField:(UITextField *)tf {

    for (int i = 0; i < self.arrTF.count; i++) {

        ZPATMSaveCardObject *mdGet = self.arrTF[i];

        if (mdGet.mTextField == tf) {
            return mdGet.mType;
        }

    }

    return -1;
}

- (void)SetBecomeFirstScrollTF:(UITextField *)tf {


    for (int i = 0; i < self.arrTF.count; i++) {

        ZPATMSaveCardObject *mdGet = self.arrTF[i];

        if (mdGet.mTextField == tf) {

            self.indexCurrentSV = i;
            [self scrollToIndex:i];

            double delayInSeconds = .5;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                [mdGet.mTextField becomeFirstResponder];
            });

            break;
        }

    }
}
- (void)textFieldShowError:(JVFloatLabeledTextField *)textField withErrorText:(NSString *)errorText {
    [textField setAlwaysShowFloatingLabel:YES];
    [textField setFloatingTitleWithError:errorText];
    textField.isShowTailImage = FALSE;
    textField.isShowBank = FALSE;
}

- (void)textFieldNonError:(JVFloatLabeledTextField *)textField {
    [textField setAlwaysShowFloatingLabel:YES];
    textField.isShowError = NO;
    textField.isShowTailImage = FALSE;
    textField.isShowBank = FALSE;

    //Reset floating label
    [self resetFloatingLabelPlaceHolder:textField];

}

- (void)formatTextField:(UITextField *)textField {
    NSString *text = textField.text;
    NSString *textWithoutFormat = [[textField.text zpRemoveFormat:formats] zpTrimSpace];
    UITextRange *selRange = textField.selectedTextRange;
    UITextPosition *selStartPos = selRange.start;
    NSInteger cursorOffset = [textField offsetFromPosition:textField.beginningOfDocument toPosition:selStartPos];

    NSString *leftString = [text substringToIndex:cursorOffset];
    leftString = [leftString zpFormatWithFormat:formats separator:nil];
    cursorOffset = leftString.length;

    textField.text = [textWithoutFormat zpFormatWithFormat:formats separator:nil];

    UITextPosition *newCursorPosition = [textField positionFromPosition:textField.beginningOfDocument offset:cursorOffset];
    if (newCursorPosition) {
        UITextRange *newSelectedRange = [textField textRangeFromPosition:newCursorPosition toPosition:newCursorPosition];
        [textField setSelectedTextRange:newSelectedRange];
    }
}

- (void)formatTextFieldDate:(UITextField *)textField {
    NSString *text = textField.text;
    NSString *textWithoutFormat = [[textField.text zpRemoveFormat:formatsDate] zpTrimSpace];
    UITextRange *selRange = textField.selectedTextRange;
    UITextPosition *selStartPos = selRange.start;
    NSInteger cursorOffset = [textField offsetFromPosition:textField.beginningOfDocument toPosition:selStartPos];

    NSString *leftString = [text substringToIndex:cursorOffset];
    leftString = [leftString zpFormatWithFormat:formatsDate separator:nil];
    cursorOffset = leftString.length;

    textField.text = [textWithoutFormat zpFormatWithFormat:formatsDate separator:nil];

    UITextPosition *newCursorPosition = [textField positionFromPosition:textField.beginningOfDocument offset:cursorOffset];
    if (newCursorPosition) {
        UITextRange *newSelectedRange = [textField textRangeFromPosition:newCursorPosition toPosition:newCursorPosition];
        [textField setSelectedTextRange:newSelectedRange];
    }
}

- (void)resetFloatingLabelPlaceHolder:(UITextField *)textField {

    for (int i = 0; i < self.arrTF.count; i++) {
        ZPATMSaveCardObject *mdGet = self.arrTF[i];
        if (mdGet.mTextField == textField) {

            JVFloatLabeledTextField *titleField = (JVFloatLabeledTextField *) textField;
            if ([mdGet.mPlaceHolder isEqualToString:@""]) {
                [titleField setPlaceholder:mdGet.mTitle floatingTitle:mdGet.mTitle];
            } else {
                [titleField setPlaceholder:mdGet.mPlaceHolder floatingTitle:mdGet.mTitle];
            }
            [titleField setPlaceholderOnly:@""];
        }
    }
}

- (NSString *)getFormatXXXXDefaultNumber:(NSString *)strTF {

    NSString *strTFGet = [strTF zpFormatWithFormat:formats separator:nil];

    NSString *strDefault = kFormatDefaultNumber;
    if (strTFGet.length >= strDefault.length) {
        return strTFGet;
    }
    NSRange range = NSMakeRange(0, strTFGet.length);
    NSString *newText = [strDefault stringByReplacingCharactersInRange:range withString:strTFGet];
    return newText;

}

- (void)getFormatXHighLight:(UITextField *)tf andDefault:(BOOL)isDefault {
    enum ZPATMSaveCardObjectType type = [self getTypeByTextField:tf];
    NSString *strTF = tf.text;
    NSString *strTFGet = @"";
    NSString *strTFGetStart = @"";
    if (type == ZPATMSaveCardObjectType_CardNumber) {
        strTFGet = [self getFormatXXXXDefaultNumber:strTF];
        strTFGetStart = [strTF zpFormatWithFormat:formats separator:nil];
    } else if (type == ZPATMSaveCardObjectType_CardExpire) {
        strTFGet = [self getFormatXXXXDefaultDate:strTF];
        strTFGetStart = [strTF zpFormatWithFormat:formatsDate separator:nil];

        if (strTF.length == 0) {
            strTFGet = self.titleExpireDefault;
        }
    } else if (type == ZPATMSaveCardObjectType_CardName) {
        strTFGet = strTF;
        strTFGetStart = strTF;
        if (strTF.length == 0) {
            strTFGet = titleNameDefault;
        }

    }
    int lengthCheck = (int) strTFGetStart.length;
    if (strTF.length == 0) {
        lengthCheck = 0;
    }

    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:strTFGet];
    int checkLength = lengthCheck - 1;
    if (checkLength < 0) {
        lengthCheck = 0;
    } else {
        lengthCheck--;
    }

    NSRange foundRange = NSMakeRange(lengthCheck, 1);
    if (foundRange.location >= attr.length || strTF.length == 0) {
        //return attr;
        [self setAttributeTextForLabel:type andAttr:attr];
        return;
    }
    NSString *strColor = kColorTextChange;
    if (self.bankCode.length > 0) {
        strColor = [ZPResourceManager bankValueFromBankCodeBundle:@"bank_texttyping" andBankCode:self.bankCode andIsBankOrCC:true];
    }
    if (foundRange.location != NSNotFound) {
        [attr beginEditing];
        //[attr addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:1] range:foundRange];
        if (!isDefault) {
            [attr addAttribute:NSForegroundColorAttributeName value:[UIColor zpColorFromHexString:strColor] range:foundRange];
        }
        [attr endEditing];
    }

    [self setAttributeTextForLabel:type andAttr:attr];

}

- (void)setAttributeTextForLabel:(enum ZPATMSaveCardObjectType)type andAttr:(NSMutableAttributedString *)attr {
    if (type == ZPATMSaveCardObjectType_CardNumber) {
        self.mLblCardNumber.attributedText = attr;
    } else if (type == ZPATMSaveCardObjectType_CardExpire) {
        self.mLblCardExpire.attributedText = attr;
    } else if (type == ZPATMSaveCardObjectType_CardName) {
        self.mLblCardName.attributedText = attr;
    }
}

- (NSString *)getFormatXXXXDefaultDate:(NSString *)strTF {

    NSString *strTFGet = [strTF zpFormatWithFormat:formatsDate separator:nil];

    NSString *strDefault = kFormatDefaultDate;
    if (strTFGet.length >= strDefault.length) {
        return strTFGet;
    }
    NSRange range = NSMakeRange(0, strTFGet.length);
    NSString *newText = [strDefault stringByReplacingCharactersInRange:range withString:strTFGet];
    return newText;
}

- (void)checkAutocomplete:(NSString *)strTF andType:(enum ZPATMSaveCardObjectType)type {

    if (self.bankCode.length <= 0) {
        return;
    }

    int maxlengthNumber = 0;

    if (type == ZPATMSaveCardObjectType_CardNumber) {

        maxlengthNumber = [[ZPResourceManager bankValueFromBankCodeBundle:@"bank_completelength" andBankCode:self.bankCode andIsBankOrCC:true] intValue];

        //Check Save Card and Link Card
        if ([self isSaveCardBefore:[[strTF zpRemoveFormat:formats] zpTrimSpace] andTf:[self getTextFieldByType:type]]) {
            return;
        }


        //Check bank support
        //Case : Pass text
        BOOL isSupportPayByCard = YES;
        ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:self.bankCode];
        ZPMiniBank *miniBank = [[ZPDataManager sharedInstance].bankManager getBankFunctionIsSupportMethod:bank withPaymentMethod:ZPPaymentMethodTypeSavedCard];
        if (miniBank == nil) {
            isSupportPayByCard = NO;
        }
        if (self.bankCode.length > 0 && isSupportPayByCard == false && ![self.delegate isAtmMapcard]) {
            return;
        }

        if ([strTF zpRemoveFormat:formats].length == maxlengthNumber) {
            [self btnNextOnAccessoryViewClick];
        }


        if (![ZPATMSaveCardCell checkInPutCardIsLuhnValid:[[strTF zpRemoveFormat:formats] zpTrimSpace]]) {
            return;
        }

        int lengthMax = [[ZPResourceManager bankValueFromBankCodeBundle:@"bank_maxlength" andBankCode:self.bankCode andIsBankOrCC:true] intValue];

        //Show error save card before
        int lengthMin = [[ZPResourceManager bankValueFromBankCodeBundle:@"bank_minlength" andBankCode:self.bankCode andIsBankOrCC:true] intValue];

        if ([strTF zpRemoveFormat:formats].length == lengthMax || [strTF zpRemoveFormat:formats].length == lengthMin) {
            [self btnNextOnAccessoryViewClick];
        }

    } else if (type == ZPATMSaveCardObjectType_CardCVV) {
        maxlengthNumber = 3;
        if (strTF.length == maxlengthNumber) {
            [self btnNextOnAccessoryViewClick];
        }

    } else if (type == ZPATMSaveCardObjectType_CardExpire) {
        maxlengthNumber = 4;

        if ([strTF zpRemoveFormat:formatsDate].length == maxlengthNumber && isCardDateValid) {
            [self btnNextOnAccessoryViewClick];
        }

    }

    if (maxlengthNumber == 0) {
        return;
    }
}

- (void)updatePageControl:(UITextField *)tf {
    [self updatePageControl];
}

- (void)updatePageControl {

    if (pageControlCur != nil) {
        pageControlCur.numberOfPages = self.arrTF.count;
        [pageControlCur setCurrentPage:self.indexCurrentSV];
    }

}

- (void)updatePageControlByIndex:(int)index {

    if (pageControlCur != nil) {
        pageControlCur.numberOfPages = self.arrTF.count;
        [pageControlCur setCurrentPage:index];
    }
}

- (void)updateBtnOk:(UITextField *)tf andEnable:(BOOL)enable {
    if (tf.inputAccessoryView == nil) {
        return;
    }
    UIButton *btn = (UIButton *) [tf.inputAccessoryView viewWithTag:20];
    if (btn != nil) {
        btn.enabled = enable;
    }
}

- (void)updateBtnOk:(BOOL)enable {
    if (btnNextCaptchaOTP != nil) {
        btnNextCaptchaOTP.enabled = enable;
    }
}

- (void)setTimerDecelerating {
    self.isEndDecelerating = FALSE;
    [timerDelayNextPreClicked invalidate];
    timerDelayNextPreClicked = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(enableDecelerating) userInfo:nil repeats:NO];
}

- (void)setTimerEnableScroll {
    self.mScrollTextfeild.scrollEnabled = NO;
    [timerEnableScroll invalidate];
    timerEnableScroll = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(tapTF) userInfo:nil repeats:NO];//[NSTimer
}

- (void)setTimerFirstBecome {
    [timerFirstBecome invalidate];
    timerFirstBecome = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(firstResponderKeyboardTextFieldByTF) userInfo:nil repeats:NO];//[NSTimer
}

- (void)enableDecelerating {

    self.isEndDecelerating = TRUE;
}

- (void)scrollToIndexByType:(enum ZPATMSaveCardObjectType)type {
    self.isEndDecelerating = FALSE;
    for (int i = 0; i < self.arrTF.count; i++) {
        ZPATMSaveCardObject *mdGet = self.arrTF[i];
        if (mdGet.mType == type) {

            self.indexCurrentSV = i;
            [self scrollToIndex: self.indexCurrentSV];
            [self firstResponderKeyboardTextField: self.indexCurrentSV];

            [self setTimerDecelerating];
            break;
        }
    }
}

- (void)getHeightInputView {
    CGFloat cellHeight = (SCREEN_WIDTH - kMarginCardView) * (1 / ratioCardView) + cardViewScrollHeight;
    if (IS_IPHONE_4_OR_LESS) {
        cellHeight = (SCREEN_WIDTH - kMarginCardView) * (1 / (ratioCardView + 1)) + cardViewScrollHeight;

    }
    CGFloat height = SCREEN_HEIGHT - (heightKeyboard + cellHeight + 66);
    kHeightInputView = height;

    if (!isViewBackClicked) {
        [self setupInputViewForTFFRame];
    }

    if (self.currentStep == ZPAtmPaymentStepVietinBankCapCha || self.currentStep == ZPAtmPaymentStepVietinBankOTP) {
        [self setupInputViewForTFFRameOnlyAccept];
    }
}

- (void)setFontSizeByLength:(UITextField *)tf {
    [self getFontSizeNumber];
}

- (void)hightLightLabelOnCardDefault {
    NSString *strColorHighLight = kColorHightLightLabel;
    self.mLblCardNumber.textColor = [UIColor zpColorFromHexString:strColorHighLight];
    self.mLblCardName.textColor = [UIColor zpColorFromHexString:strColorHighLight];
    self.mLblCardExpire.textColor = [UIColor zpColorFromHexString:strColorHighLight];
    self.mLblCardCVV.textColor = [UIColor zpColorFromHexString:strColorHighLight];

}

- (void)hightLightLabelOnCard_NotUse:(UITextField *)tf {

    if (self.currentStep == ZPAtmPaymentStepBIDVCapChaPassword || self.currentStep == ZPAtmPaymentStepVietinBankOTP) {
        [self hightLightLabelOnCardDefault];
        return;
    }

    NSString *strColorNormal = kColorNormalLabel;
    NSString *strColorHighLight = kColorHightLightLabel;
    self.mLblCardNumber.textColor = [UIColor zpColorFromHexString:strColorHighLight];
    self.mLblCardName.textColor = [UIColor zpColorFromHexString:strColorHighLight];
    self.mLblCardExpire.textColor = [UIColor zpColorFromHexString:strColorHighLight];
    self.mLblCardCVV.textColor = [UIColor zpColorFromHexString:strColorHighLight];

    if (self.bankCode.length > 0) {
        strColorNormal = [ZPResourceManager bankValueFromBankCodeBundle:@"bank_textunhighlight" andBankCode:self.bankCode andIsBankOrCC:true];
        strColorHighLight = [ZPResourceManager bankValueFromBankCodeBundle:@"bank_texthighlight" andBankCode:self.bankCode andIsBankOrCC:true];

    }
    //self.mLblCardNumber.textColor = [UIColor zpColorFromHexString:strColorNormal];
    self.mLblCardName.textColor = [UIColor zpColorFromHexString:strColorNormal];
    self.mLblCardExpire.textColor = [UIColor zpColorFromHexString:strColorNormal];
    self.mLblCardCVV.textColor = [UIColor zpColorFromHexString:strColorNormal];


    for (int i = 0; i < self.arrTF.count; i++) {
        ZPATMSaveCardObject *mdGet = self.arrTF[i];
        if (mdGet.mTextField == tf) {

            switch (mdGet.mType) {
                case ZPATMSaveCardObjectType_CardNumber:
                    self.mLblCardNumber.textColor = [UIColor zpColorFromHexString:strColorHighLight];
                    break;
                case ZPATMSaveCardObjectType_CardName:
                    self.mLblCardName.textColor = [UIColor zpColorFromHexString:strColorHighLight];
                    break;
                case ZPATMSaveCardObjectType_CardExpire:
                    self.mLblCardExpire.textColor = [UIColor zpColorFromHexString:strColorHighLight];
                    break;
                case ZPATMSaveCardObjectType_CardCVV:
                    self.mLblCardCVV.textColor = [UIColor zpColorFromHexString:strColorHighLight];
                    break;
                case ZPATMSaveCardObjectType_OptionSMSToken:
                    //self.mLblCardCVV.textColor = [UIColor zpColorFromHexString:strColorHighLight];
                    self.mLblCardNumber.textColor = [UIColor zpColorFromHexString:strColorHighLight];
                    self.mLblCardName.textColor = [UIColor zpColorFromHexString:strColorHighLight];
                    self.mLblCardExpire.textColor = [UIColor zpColorFromHexString:strColorHighLight];
                    break;
                default:
                    break;
            }

            break;
        }
    }


}

- (void)updatePreNextEnable {

    UIButton *btnPre = [viewPreNext viewWithTag:1];
    UIButton *btnNext = [viewPreNext viewWithTag:2];

    //set enable and disable
    if (self.indexCurrentSV == 0) {
        btnPre.enabled = false;

    } else {

        btnPre.enabled = true;
        btnNext.enabled = true;
    }


}

- (void)getFontSizeNumber {

    float largestFontSize = 50;
    NSString *text = self.mLblCardNumber.text;
    if (text.length == 0) {
        return;
    }

    self.mLblCardNumber.backgroundColor = [UIColor clearColor];

    CGFloat width = self.mLblCardNumber.frame.size.width;
    if (!kHSizeNumberFontBig) {
        width = SCREEN_WIDTH - (20 + 35) * 2;
    }

    NSString *fontPath = [[ZPDataManager sharedInstance].bundle pathForResource:kFontNumberCardFile ofType:@"ttf"];
    if (fontPath.length == 0) {
        while ([text sizeWithAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:largestFontSize]}].width > width) {
            largestFontSize -= 0.1;
        }
        self.mLblCardNumber.font = [UIFont systemFontOfSize:largestFontSize];
    } else {
        while ([text sizeWithAttributes:@{NSFontAttributeName: [UIFont fontWithName:kFontNumberCard size:largestFontSize]}].width > width) {
            largestFontSize -= 0.1;
        }
        self.mLblCardNumber.font = [UIFont fontWithName:kFontNumberCard size:largestFontSize];
    }
    //[UIFont systemFontOfSize:largestFontSize]
    kHSizeNumberFontBig = largestFontSize;
}

- (void)resetAll {
    self.bankCode = @"";
    self.isEndDecelerating = true;
    //Card View
    self.mImgIconBank.image = [UIImage imageNamed:@""];
    self.mImgIconBank.backgroundColor = [UIColor lightGrayColor];
    [self setGradientColorForCardView:self.bankCode];
    self.mLblCardNumber.text = kFormatDefaultNumber;
    self.mLblCardExpire.text = @"";
    self.mLblCardName.text = titleNameDefault;

    //Color
    NSString *strColorNormal = kColorNormalLabel;
    NSString *strColorHighLight = kColorHightLightLabel;
    self.mLblCardNumber.textColor = [UIColor zpColorFromHexString:strColorHighLight];
    self.mLblCardName.textColor = [UIColor zpColorFromHexString:strColorNormal];
    self.mLblCardExpire.textColor = [UIColor zpColorFromHexString:strColorNormal];
    self.mLblCardCVV.textColor = [UIColor zpColorFromHexString:strColorNormal];

    //Input
    for (int i = 1; i < self.arrTF.count; i++) {
        ZPATMSaveCardObject *mdGet = self.arrTF[i];
        mdGet.mTextField.text = @"";
    }
}

- (void)resetAllAttributeTextLabel {

    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:self.mLblCardNumber.text];
    self.mLblCardNumber.attributedText = attr;

    attr = [[NSMutableAttributedString alloc] initWithString:self.mLblCardExpire.text];
    self.mLblCardExpire.attributedText = attr;

    attr = [[NSMutableAttributedString alloc] initWithString:self.mLblCardName.text];
    self.mLblCardName.attributedText = attr;


}

- (BOOL)isSaveCardBefore:(NSString *)detectString andTf:(JVFloatLabeledTextField *)tfNumber {

    //Check Save Card and Link Card
    if (self.delegate && [self.delegate respondsToSelector:@selector(isAtmMapcard)]) {
        if ([self.delegate isAtmMapcard]) {

            if (self.bankCode.length > 0) {
                if (detectString.length == kLengthCheckExistMin || detectString.length == kLengthCheckExistMax) {
                    if([[ZPDataManager sharedInstance] checkSavedCardIsExist:detectString]){
                        ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:self.bankCode];

                        NSString *last4No = [detectString substringFromIndex:MAX((int)[detectString length]-4, 0)];
                        NSString *title = [[ZaloMobilePaymentSDK new] getSaveCardTitleFromConfig:bank.bankName andLast4CardNo:last4No];
                        [self textFieldShowError:tfNumber withErrorText:[NSString stringWithFormat:stringErrorCardIsExist, title]];

                        return YES;
                    }
                }
            }

        }

    }
    return NO;
}

- (void)removeBtnPreNext {

    if (viewPreNext) {
        [viewPreNext removeFromSuperview];
        viewPreNext = nil;
        [self UnRegisterForKeyboardNotifications];
        [ZPManagerSaveCardCell sharedInstance].isShowAMTCardCell = false;
    }
}

- (JVFloatLabeledTextField *)getCurrentTf {
    ZPATMSaveCardObject *mdGet = self.arrTF[lastIndexSV];
    return (JVFloatLabeledTextField *) mdGet.mTextField;
}
#pragma mark - Date and List Picker

- (ZPMonthYearPicker *)getDatePicker {
    if (!datePickerView) {

        datePickerView = [[ZPMonthYearPicker alloc] init];
        datePickerView.pickerDelegate = self;
        [datePickerView setCurrentDate];
    }

    return datePickerView;

}

- (UIView *)getListConfirmTransitionPicker {
    if (!listConfirmTransitionPickerView) {

        listConfirmTransitionPickerView = [[ZPPickerList alloc] init];
        listConfirmTransitionPickerView.arrData = [NSMutableArray arrayWithObjects:kTitleBtnSMS, kTitleBtnToken, nil];
        listConfirmTransitionPickerView.pickerDelegate = self;
        listConfirmTransitionPickerView.hidden = true;


    }

    //return listConfirmTransitionPickerView;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - heightKeyboard, SCREEN_WIDTH, heightKeyboard)];
    view.backgroundColor = [UIColor zpColorFromHexString:@"#F0F4F6"];
    return view;

}

- (void)onChangeMonth:(int)m year:(int)y {

    self.mLblCardExpire.text = [NSString stringWithFormat:@"%02d/%02d", m, y];
    [self updateTextForTfByType:ZPATMSaveCardObjectType_CardExpire withText:self.mLblCardExpire.text];


    NSDate *date = [NSDate date];
    NSCalendar *calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];;
    int month = (int) [[calendar components:NSCalendarUnitYear | NSCalendarUnitMonth fromDate:date] month];
    int year = (int) [[calendar components:NSCalendarUnitYear | NSCalendarUnitMonth fromDate:date] year] - 2000;
    DDLogInfo(@"y year m month %d.%d.%d.%d", y, year, m, month);

    if (validFromDate) {
        if (y > year) {
            isCardDateValid = NO;
        } else if (y == year && m > month) {
            isCardDateValid = NO;
        } else {
            isCardDateValid = YES;
        }
    } else if (!validFromDate) {
        if (y < year) {
            isCardDateValid = NO;
        } else if (y == year && m < month) {
            isCardDateValid = NO;
        } else {
            isCardDateValid = YES;
        }
    }
    NSString *errorMessage = @"";
    JVFloatLabeledTextField *tfDate = [self getTextFieldByType:ZPATMSaveCardObjectType_CardExpire];
    if (!isCardDateValid) {
        if (validFromDate) {
            errorMessage = @"Ngày phát hành không hợp lệ";
        } else {
            errorMessage = @"Ngày hết hạn không hợp lệ";
        }
        [line setBackgroundColor:TEXTFIELD_ERROR_COLOR];
        [self textFieldShowError:tfDate withErrorText:errorMessage];
    } else {
        [line setBackgroundColor:TEXTFIELD_SUCCESS_COLOR];
        [self textFieldNonError:tfDate];
    }
}

- (void)ZPPickerListDelegate_onChangeData:(int)index arr:(NSMutableArray *)arr {

    if (index == kIndexBtnSMS) {
        [self resetAllBtnSelected];
        [mBtnSMS setSelected:true];
    }
    if (index == kIndexBtnToken) {
        [self resetAllBtnSelected];
        [mBtnToken setSelected:true];
    }

}

#pragma mark - Clear

- (void)clearSubviewsScrollView {
    //Clear all
    while (self.arrTF.count > 1) {
        [self.arrTF removeLastObject];
    }

    for (UIView *subView in self.mScrollTextfeild.subviews) {
        if (subView != self.mScrollTextfeild.subviews[0] || self.currentStep == ZPAtmPaymentStepBIDVCapChaPassword || self.currentStep == ZPAtmPaymentStepVietinBankOTP || self.currentStep == ZPAtmPaymentStepVietinBankCapCha) {
            [subView removeFromSuperview];
        }

    }
}

#pragma mark - Events

- (void)btnPreOnAccessoryViewClick {
    if (!self.isEndDecelerating) {
        return;
    }
    if (self.indexCurrentSV == 0) {
        return;
    }
    ZPATMSaveCardObject *mdGet = self.arrTF[self.indexCurrentSV];
    enum ZPATMSaveCardObjectType type = mdGet.mType;
    [self analyticTrack:type next:NO];
    self.indexCurrentSV--;
    self.isScrolling = YES;
    [self scrollToIndex: self.indexCurrentSV];
    [self firstResponderKeyboardTextField: self.indexCurrentSV];
    [self setTimerDecelerating];

}

- (void)btnNextOnAccessoryViewClick {
    if (!self.isEndDecelerating) {
        return;
    }
    if (self.indexCurrentSV + 1 == self.arrTF.count) {
        //Done
        if ([self checkValidateTextFieldLastest] == false) {
            return;
        }
        //click btnNext when input CardName
//        [[ZPAppFactory sharedInstance] trackEventId:ZPAnalyticEventActionLinkbank_add_bank_name_forward];
        [self analyticTrack:ZPATMSaveCardObjectType_None next:YES];
        [self resignKeyboardTextField:lastIndexSV];
        return;
    }
    if ([self checkValidateTextFieldByType: self.indexCurrentSV andShowError:true] == false) {
        return;
    }
    ZPATMSaveCardObject *mdGet = self.arrTF[self.indexCurrentSV];
    enum ZPATMSaveCardObjectType type = mdGet.mType;
    [self analyticTrack:type next:YES];
    self.indexCurrentSV++;
    self.isScrolling = YES;
    [self scrollToIndex: self.indexCurrentSV];
    [self firstResponderKeyboardTextField: self.indexCurrentSV];
    [self setTimerDecelerating];
}

- (void)btnDoneOnAccessoryViewClick {
    [self resignKeyboardTextField: self.indexCurrentSV];
}

- (void)btnOkOnAccessoryViewClick {
    [timerFirstBecome invalidate];
    if (self.delegate && [self.delegate respondsToSelector:@selector(btnOkClicked)]) {
        [self.delegate btnOkClicked];
    }
}

- (void)btnSMSClick {
    [self resetAllBtnSelected];
    [mBtnSMS setSelected:true];

    choseAuthMethod = choseFirstMethod;
}

- (void)btnTokenClick {

    [self resetAllBtnSelected];
    [mBtnToken setSelected:true];

    choseAuthMethod = choseSecondMethod;

}

- (void)resetAllBtnSelected {
    [mBtnToken setSelected:false];
    [mBtnSMS setSelected:false];
}

#pragma mark - Overrides

- (void)updateLayouts {

    if ([ZPManagerSaveCardCell sharedInstance].isShowAMTCardCell) {
        [self registerForKeyboardNotifications];
    }

    ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:self.bankCode];
    NSInteger opt = bank.bankType;
    
    if (bank.otpType.length > 0) {
        if ([[bank.otpType componentsSeparatedByString:@"|"] count] >= 2) {
            self.hasAuthMethod = YES;
            choseAuthMethod = [[[[bank.otpType componentsSeparatedByString:@"|"] firstObject] componentsSeparatedByString:@":"] firstObject];
            choseFirstMethod = choseAuthMethod;
            DDLogInfo(@"choseFirstMthod %@", choseFirstMethod);
            choseSecondMethod = [[[[bank.otpType componentsSeparatedByString:@"|"] objectAtIndex:1] componentsSeparatedByString:@":"] firstObject];
            DDLogInfo(@"choseSecondMethod %@", choseSecondMethod);
        } else {
            self.hasAuthMethod = NO;
            choseAuthMethod = [[bank.otpType componentsSeparatedByString:@":"] firstObject];
        }
    } else {
        choseAuthMethod = @"otp";
    }


    self.hasDate = (opt == ZPAtmInputTypeApiStartDateInfo || opt == ZPAtmInputTypeApiEndDateInfo);
    validFromDate = (opt == ZPAtmInputTypeApiStartDateInfo) ? YES : NO;
    self.hasPass = (opt == ZPAtmInputTypeApiPasswordInfo) ? YES : NO;


    titleExpire = opt == ZPAtmInputTypeApiStartDateInfo ? @"Nhập ngày phát hành (mm/yy)" : @"Nhập ngày hết hạn (mm/yy)";
    self.titleExpireDefault = @"";
    self.mLblTitleCardExpire.text = self.titleExpireDefault;
    //Check reload
    if (self.bankCode.length == 0) {
        return;
    }
    if ([self.bankCode isEqualToString:self.lastBankCode]) {
        return;
    }
    self.lastBankCode = self.bankCode;
    if (self.hasDate) {
        self.mLblCardExpire.text = self.titleExpireDefault;
    } else {
        self.mLblCardExpire.text = @"";
    }

    // Cleanning
    [self clearSubviewsScrollView];

    if (!self.hasAmount) {
        [self.arrTF addObject:[[ZPATMSaveCardObject alloc] initWithTitle:titleAmount andValue:@"" andPlaceHolder:@"" andType:ZPATMSaveCardObjectType_CardAmount]];
    }
    //[arrTF addObject:[[ZPATMSaveCardObject alloc] initWithTitle:titleNumber andValue:TxtNumberLast andType:ZPATMSaveCardObjectType_CardNumber]];

    if (self.hasPass) {
        [self.arrTF addObject:[[ZPATMSaveCardObject alloc] initWithTitle:titlePass andValue:@"" andPlaceHolder:@"" andType:ZPATMSaveCardObjectType_CardPassword]];
    }
    if (self.hasDate) {
        [self.arrTF addObject:[[ZPATMSaveCardObject alloc] initWithTitle:titleExpire andValue:@"" andPlaceHolder:kFormatDefaultDate andType:ZPATMSaveCardObjectType_CardExpire]];
    }
    [self.arrTF addObject:[[ZPATMSaveCardObject alloc] initWithTitle:titleName andValue:@"" andPlaceHolder:@"" andType:ZPATMSaveCardObjectType_CardName]];

    //Reload
    [self initView];


}

- (BOOL)checkValidate {
    return [self checkValidateTextField];
}

- (NSDictionary *)outputParams {
    ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:self.bankCode];
    NSString *type = bank.type;
    NSMutableDictionary * params = [[NSMutableDictionary alloc] init];
    
    JVFloatLabeledTextField *tfNumber = [self getTextFieldByType:ZPATMSaveCardObjectType_CardNumber];
    JVFloatLabeledTextField *tfName = [self getTextFieldByType:ZPATMSaveCardObjectType_CardName];
    JVFloatLabeledTextField *tfPass = [self getTextFieldByType:ZPATMSaveCardObjectType_CardPassword];
    JVFloatLabeledTextField *tfDate = [self getTextFieldByType:ZPATMSaveCardObjectType_CardExpire];
    JVFloatLabeledTextField *tfAmount = [self getTextFieldByType:ZPATMSaveCardObjectType_CardAmount];
    JVFloatLabeledTextField *tfOTP = [self getTextFieldByType:ZPATMSaveCardObjectType_OTP];

    if (self.currentStep == ZPAtmPaymentStepChooseSMSToken) {

        tfNumber = [self getTextFieldByTypeStoreStep1:ZPATMSaveCardObjectType_CardNumber];
        tfName = [self getTextFieldByTypeStoreStep1:ZPATMSaveCardObjectType_CardName];
        tfPass = [self getTextFieldByTypeStoreStep1:ZPATMSaveCardObjectType_CardPassword];
        tfDate = [self getTextFieldByTypeStoreStep1:ZPATMSaveCardObjectType_CardExpire];
        tfAmount = [self getTextFieldByTypeStoreStep1:ZPATMSaveCardObjectType_CardAmount];
        tfOTP = [self getTextFieldByTypeStoreStep1:ZPATMSaveCardObjectType_OTP];
    }


    if (self.bankCode.length > 0) {
        [params setObject:self.bankCode forKey:@"bankcode"];
    }
    if (tfAmount.text.length > 0) {
        [params setObject:[[tfAmount.text zpTrimSpace] zpRemoveDilimeter] forKey:@"chargeamount"];
    }
    if (tfNumber.text.length > 0) {
        [params setObject:[[tfNumber.text zpTrimSpace] zpRemoveFormat:formats] forKey:@"number"];
    }
    if (tfName.text.length > 0) {
        [params setObject:[[[NSString alloc] initWithData:[tfName.text dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES] encoding:NSASCIIStringEncoding] zpTrimSpace] forKey:@"holdername"];
    }

    if (tfDate.text.length > 0) {
        if (validFromDate) {
            [params setObject:[[tfDate.text zpTrimSpace] zpRemoveFormat:formatsDate] forKey:@"validfrom"];
        } else {
            [params setObject:[[tfDate.text zpTrimSpace] zpRemoveFormat:formatsDate] forKey:@"validto"];
        }
    }
    if (tfPass.text.length > 0) {
        [params setObject:tfPass.text forKey:@"password"];
    }
    if (choseAuthMethod.length > 0) {
        [params setObject:choseAuthMethod forKey:@"otptype"];
    }
    if (type.length > 0) {
        [params setObject:type forKey:@"type"];
    }
    if (self.captcha.length > 0) {
        [params setObject:self.captcha forKey:@"captcha"];
    }
    if (tfOTP.text.length > 0) {
        [params setObject:tfOTP.text forKey:@"otp"];
    }

    return params;

}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.hasDate = self.hasPass = NO;
    isViewBackClicked = FALSE;
    [self removeBtnPreNext];


    if (self.currentStep == ZPAtmPaymentStepChooseSMSToken) {
        [self setupInputViewForTFFRameOnlyAccept];
    } else {
        [self firstResponderKeyboardTextField: self.indexCurrentSV];
    }

}

- (void)setCaptchaImage:(UIImage *)img {

    imageCaptcha = img;

    if (!isLoadedCaptchaView) {

        isLoadedCaptchaView = true;

        isHaveNumberField = false;
        isDontResetDefaultCardView = true;

        isHideKeyBoard = false;

        if ([self.bankCode isEqualToString:@"123PBIDV"]) {
            self.currentStep = ZPAtmPaymentStepBIDVCapChaPassword;
        } else {
            self.currentStep = ZPAtmPaymentStepVietinBankCapCha;
        }
        [self removeBtnPreNext];

        if (self.currentStep == ZPAtmPaymentStepBIDVCapChaPassword) {
            self.hasPass = true;
            [self setupInputViewForTFFRame];
        } else if (self.currentStep == ZPAtmPaymentStepVietinBankCapCha) {
            [self setupInputViewForTFFRameOnlyAccept];
        }
        // Cleanning
        [self clearSubviewsScrollView];

        //Set array
        self.arrTF = [NSMutableArray array];

        if (self.currentStep == ZPAtmPaymentStepBIDVCapChaPassword) {
            [self.arrTF addObject:[[ZPATMSaveCardObject alloc] initWithTitle:@"Nhập mật khẩu" andValue:@"" andPlaceHolder:@"" andType:ZPATMSaveCardObjectType_CardPassword]];

        }

        [self.arrTF addObject:[[ZPATMSaveCardObject alloc] initWithTitle:@"Nhập mã captcha" andValue:@"" andPlaceHolder:@"" andType:ZPATMSaveCardObjectType_Captcha]];

        //Get width view captcha
        scaleImgCaptchaOld = img.size.width / img.size.height;
        //Reload
        [self initView];
        [self excuteView];
    } else {

        if (imageView_Captcha) {
            imageView_Captcha.image = img;
        }
    }


}

- (void)setOTP {

    isHaveNumberField = false;
    isDontResetDefaultCardView = true;
    self.isShouldHideKB = false;
    [self resignKeyboardTextField: self.indexCurrentSV];
    self.currentStep = ZPAtmPaymentStepVietinBankOTP;
    [self removeBtnPreNext];
    [self setupInputViewForTFFRameOnlyAccept];
    // Cleanning
    [self clearSubviewsScrollView];

    //Set array
    self.arrTF = [NSMutableArray array];
    [self.arrTF addObject:[[ZPATMSaveCardObject alloc] initWithTitle:@"Nhập mã OTP" andValue:@"" andPlaceHolder:@"" andType:ZPATMSaveCardObjectType_OTP]];

    //Reload
    [self initView];
    [self excuteView];
}

//Add not ovverride
- (void)setChooseForm_OnlySMSToken {

    [self resetAllAttributeTextLabel];

    arrTFStoreStep1 = [NSMutableArray arrayWithArray:self.arrTF];

    isHaveNumberField = false;
    isDontResetDefaultCardView = true;
    self.isShouldHideKB = false;
    [self resignKeyboardTextField: self.indexCurrentSV];
    self.currentStep = ZPAtmPaymentStepChooseSMSToken;
    [self removeBtnPreNext];
    [self setupInputViewForTFFRameOnlyAccept];
    // Cleanning
    [self clearSubviewsScrollView];

    //Set array
    self.arrTF = [NSMutableArray array];
    [self.arrTF addObject:[[ZPATMSaveCardObject alloc] initWithTitle:@"" andValue:@"" andPlaceHolder:@"" andType:ZPATMSaveCardObjectType_OptionSMSToken]];
    //Reload
    [self initView];
    [self excuteView];

}

- (void)viewWillBack {
    isViewBackClicked = YES;
    [self removeBtnPreNext];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    //[self endEditing:NO];
}

#pragma mark - Keyboard Activity

- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self

                                             selector:@selector(keyboardWasShown:)

                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self

                                             selector:@selector(keyboardWillBeHidden:)

                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)UnRegisterForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];

}

- (void)keyboardWasShown:(NSNotification *)aNotification {
    CGSize beginFrame = [[[aNotification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGSize endFrame = [[[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    CGSize keyboardSize = endFrame.height > beginFrame.height ? endFrame : beginFrame;
    if (keyboardSize.height >= 100) {
        if (!heightKeyboard) {
            heightKeyboard = keyboardSize.height;
        }
        [self getHeightInputView];
    }
}

- (void)keyboardWillBeHidden:(NSNotification *)aNotification {
}

- (void)hideKeyBoard:(NSNotification *)note {

    [self resignKeyboardTextField: self.indexCurrentSV];
    isHideKeyBoard = YES;

    if (self.currentStep == ZPAtmPaymentStepChooseSMSToken || self.currentStep == ZPAtmPaymentStepVietinBankCapCha || self.currentStep == ZPAtmPaymentStepVietinBankOTP) {
        return;
    }
    // [self removeBtnPreNext];
}

- (void)hideBtnPreNext:(NSNotification *)note {
    [self removeBtnPreNext];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    JVFloatLabeledTextField *tf = (JVFloatLabeledTextField *) textField;
    [tf setPlaceholderOnly:@""];

    if (self.currentStep == ZPAtmPaymentStepVietinBankOTP || self.currentStep == ZPAtmPaymentStepVietinBankCapCha) {
        ((JVFloatLabeledTextField *) textField).isBGBottomLineDefault = NO;
    } else {
        //[self setupInputViewForTF:textField];
        [self hightLightLabelOnCard_NotUse:textField];

        [self getFormatXHighLight:textField andDefault:false];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {

    if (self.currentStep == ZPAtmPaymentStepVietinBankOTP || self.currentStep == ZPAtmPaymentStepVietinBankCapCha) {
        if ([textField isKindOfClass:[JVFloatLabeledTextField class]]) {

            ((JVFloatLabeledTextField *) textField).isBGBottomLineDefault = YES;

        }
    }

    UITextField *tfNumber = [self getTextFieldByType:ZPATMSaveCardObjectType_CardNumber];
    if (tfNumber == textField && [tfNumber.text zpRemoveFormat:formats].length >= [[ZPDataManager sharedInstance] doubleForKey:@"atm_card_number_min_length"]) {

        NSString *detectedBankCode = @"";
        if (self.delegate && [self.delegate respondsToSelector:@selector(isAtmMapcard)]) {
            if ([self.delegate isAtmMapcard]) {
                detectedBankCode = [ZPResourceManager getCcBankCodeFromConfig:[tfNumber.text zpRemoveFormat:formats]];
                if (detectedBankCode != nil && [detectedBankCode isEqualToString:@""] == NO) {
                    NSDictionary *ccBankDict = [[ZPDataManager sharedInstance].bankManager.ccBanksDict objectForKey:detectedBankCode];
                    if (self.delegate && [self.delegate respondsToSelector:@selector(detectCreditCard:didDetectBank:)]) {
                        [self.delegate detectCreditCard:self didDetectBank:ccBankDict];
                    }
                    return;
                }

            }
        }
//        ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:self.bankCode];
//        if (self.delegate && [self.delegate respondsToSelector:@selector(detectCell:didEditCardNumberWithDetectedBank:)]) {
//            [self.delegate detectCell:self didEditCardNumberWithDetectedBank:bank];
//        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {

    JVFloatLabeledTextField *tfAmount = [self getTextFieldByType:ZPATMSaveCardObjectType_CardAmount];
    JVFloatLabeledTextField *tfNumber = [self getTextFieldByType:ZPATMSaveCardObjectType_CardNumber];
    JVFloatLabeledTextField *tfName = [self getTextFieldByType:ZPATMSaveCardObjectType_CardName];
    //JVFloatLabeledTextField *tfPass = [self getTextFieldByType:ZPATMSavzeCardObjectType_CardPassword];
    JVFloatLabeledTextField *tfDate = [self getTextFieldByType:ZPATMSaveCardObjectType_CardExpire];
    JVFloatLabeledTextField *tfOTP = [self getTextFieldByType:ZPATMSaveCardObjectType_OTP];
    JVFloatLabeledTextField *tfCaptcha = [self getTextFieldByType:ZPATMSaveCardObjectType_Captcha];
    DDLogInfo(@"shouldChangeCharactersInRange");
    if ([textField isKindOfClass:[JVFloatLabeledTextField class]]) {
        [self textFieldNonError:(JVFloatLabeledTextField *) textField];
    }

    if (self.isScrolling) {
        return NO;
    }
    NSString *textFieldString = textField.text;
    if (range.location <= textFieldString.length) {
        textFieldString = [textFieldString stringByReplacingCharactersInRange:range withString:string];
    }

    if (textField == tfName) {

        NSString *tfString = textField.text;

        if (textFieldString.length > [[ZPDataManager sharedInstance] doubleForKey:@"atm_card_holder_max_length"]) {

            return NO;
        }
        //Space
        if (tfString.length > 0) {

            NSString *subStr = [tfString substringWithRange:NSMakeRange(tfString.length - 1, 1)];
            if ([subStr isEqualToString:@" "] && [string isEqualToString:@" "]) {
                [self textFieldShowError:(JVFloatLabeledTextField *) textField withErrorText:@"Tên không được nhập 2 khoảng trắng"];

                return NO;

            }

        }

        textField.text = [textFieldString uppercaseString];
        self.mLblCardName.text = textField.text;

        [self getFormatXHighLight:textField andDefault:false];


        return NO;
    }
    if (textField == tfAmount) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            textField.text = [textField.text zpFormatNumberWithDilimeter];
        });
    }

    if (textField == tfNumber) {
        NSString *detectString = [[textFieldString zpRemoveFormat:formats] zpTrimSpace];
        NSString *tfString = [[textField.text zpRemoveFormat:formats] zpTrimSpace];

        //Prevent paste text
        int checkNumber = [detectString intValue];

        if (checkNumber <= 0 && detectString.length > 0 && ![detectString isEqualToString:@"0"]) {
            return NO;
        }

        //Switch to CC

        if ([textFieldString zpRemoveFormat:formats].length >= 1) {
            NSString *detectedBankCode = @"";
            if (self.delegate && [self.delegate respondsToSelector:@selector(isAtmMapcard)]) {
                if ([self.delegate isAtmMapcard]) {

                    detectedBankCode = [ZPResourceManager getCcBankCodeFromConfig:[textFieldString zpRemoveFormat:formats]];
                    if (detectedBankCode != nil && [detectedBankCode isEqualToString:@""] == NO) {
                        NSDictionary *ccBankDict = [[ZPDataManager sharedInstance].bankManager.ccBanksDict objectForKey:detectedBankCode];
                        if (self.delegate && [self.delegate respondsToSelector:@selector(detectCreditCard:didDetectBank:)]) {
                            textField.text = textFieldString;

                            [self removeBtnPreNext];

                            [self.delegate detectCreditCard:self didDetectBank:ccBankDict];
                            return YES;
                        }

                    }

                }
            }
        }

        [self detectBankCodeFromBankNumber:detectString];

        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self formatTextField:textField];
        });

        if ([self.bankCode length] <= 0 && [detectString length] >= 6 + 1) {
            [self checkValidateTextField];
            //detect cc card
            NSString *detectedBankCode = @"";
            if (self.delegate && [self.delegate respondsToSelector:@selector(isAtmMapcard)]) {
                if ([self.delegate isAtmMapcard]) {
                    detectedBankCode = [ZPResourceManager getCcBankCodeFromConfig:detectString];
                    if (detectedBankCode != nil && [detectedBankCode isEqualToString:@""] == NO) {

                    } else {
                        //[_line setBackgroundColor:TEXTFIELD_ERROR_COLOR];
                    }

                }
            }
            //Clear Image
            self.mImgIconBank.image = [UIImage imageNamed:@""];
            self.mImgIconBank.backgroundColor = [UIColor lightGrayColor];
            if (detectString.length < tfString.length) {
                return YES;
            }
            return NO;

        }
        if (self.bankCode.length > 0 && detectString.length >= 6) {
            //Get image bankcode
            NSString *imgGet = [ZPResourceManager bankValueFromBankCodeBundle:@"bank_icon" andBankCode:self.bankCode andIsBankOrCC:true];
            self.mImgIconBank.image = [ZPResourceManager getImageWithName:imgGet];
            self.mImgIconBank.backgroundColor = [UIColor clearColor];
            [self updatePageControl:textField];
        } else {
            //Clear Image
            self.mImgIconBank.image = [UIImage imageNamed:@""];
            self.mImgIconBank.backgroundColor = [UIColor lightGrayColor];
        }
        [self setGradientColorForCardView:self.bankCode];

        //Length
        int lengthMax = [[ZPDataManager sharedInstance] doubleForKey:@"atm_card_number_max_length"];
        int lengthMin = 0;
        BOOL checkLuhn = true;;
        if (self.bankCode.length > 0) {
            lengthMax = [[ZPResourceManager bankValueFromBankCodeBundle:@"bank_maxlength" andBankCode:self.bankCode andIsBankOrCC:true] intValue];

            //Show error save card before
            lengthMin = [[ZPResourceManager bankValueFromBankCodeBundle:@"bank_minlength" andBankCode:self.bankCode andIsBankOrCC:true] intValue];

            NSString *strCheck = tfString;
            //19->20

            if (detectString.length <= lengthMax) {
                strCheck = detectString;
            }

            if (strCheck.length >= lengthMin) {
                [self isSaveCardBefore:tfString andTf:tfNumber];

                if (![ZPATMSaveCardCell checkInPutCardIsLuhnValid:strCheck]) {
                    checkLuhn = false;
                    [self textFieldShowError:tfNumber withErrorText:@"Thông tin số thẻ không chính xác"];
                }
            }

        }

        //Check Save Card and Link Card
        if ([self isSaveCardBefore:detectString andTf:tfNumber]) {
            if (detectString.length <= lengthMax) {
                return YES;
            } else {
                return NO;
            }

        }

        if ([self.bankCode length] > 0) {
            ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:self.bankCode];
            if (bank.status != ZPBankStatusEnable) {
                return detectString.length <= 6;
            }
        }


        //Map card only map by account bank (IBanking)
        //1 : card
        //2 : only bank account
        if (self.bankCode.length > 0 && detectString.length >= 6 && [self bankSupportType:self.bankCode] == ZPSDK_BankType_Account && [self.delegate isAtmMapcard]) {
            textField.text = detectString;
            self.mLblCardNumber.text = [self getFormatXXXXDefaultNumber:textField.text];
            return false;
        }
        return detectString.length <= lengthMax;
    }

    if (textField == tfDate) {
        NSString *detectString = [[textFieldString zpRemoveFormat:formatsDate] zpTrimSpace];
        NSString *tfString = [[textField.text zpRemoveFormat:formatsDate] zpTrimSpace];
        //Remove
        if ([string isEqualToString:@""] && textField.text.length == 3) {

            NSString *dateString = textField.text;
            textField.text =
                    [dateString stringByReplacingOccurrencesOfString:@"/" withString:@""];
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (detectString.length >= 3) {
                [self formatTextFieldDate:textField];
            }
        });
        if (detectString.length > tfString.length && detectString.length == 5) {
            [self checkValidateTextFieldByType: self.indexCurrentSV andShowError:true];
        }
        return detectString.length <= 4;
    }

    if (textField == tfOTP) {
        if (textField.keyboardType == UIKeyboardTypeNumberPad) {
            int checkNumber = [string intValue];
            if (checkNumber <= 0 && ![string isEqualToString:@""] && ![string isEqualToString:@"0"]) {
                return NO;
            }
        }
        NSString *detectString = [textFieldString zpTrimSpace];
        int maxLength = [[ZPDataManager sharedInstance] doubleForKey:@"otp_max_length"];
        if (maxLength == 0) {
            maxLength = 16;
        }
        return detectString.length <= maxLength;
    }
    if (textField == tfCaptcha) {
        NSString *detectString = [textFieldString zpTrimSpace];
        int maxLength = [[ZPDataManager sharedInstance] doubleForKey:@"captcha_max_length"];
        if (maxLength == 0) {
            maxLength = 10;
        }
        return detectString.length <= maxLength;
    }
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {

    JVFloatLabeledTextField *tfNumber = [self getTextFieldByType:ZPATMSaveCardObjectType_CardNumber];
    JVFloatLabeledTextField *tfName = [self getTextFieldByType:ZPATMSaveCardObjectType_CardName];
    JVFloatLabeledTextField *tfDate = [self getTextFieldByType:ZPATMSaveCardObjectType_CardExpire];
    if (textField == tfNumber) {
        self.bankCode = @"";
        if (self.delegate && [self.delegate respondsToSelector:@selector(detectCell:didDetectBank:)]) {
            [self.delegate detectCell:self didDetectBank:nil];
            [self updateLayouts];
        }
    }
    if (textField == tfName) {
        self.mLblCardName.text = titleNameDefault;
    }
    if (textField == tfDate) {
        if (self.hasDate) {
            self.mLblCardExpire.text = self.titleExpireDefault;
        } else {
            self.mLblCardExpire.text = @"";
        }
    }

    if ([textField isKindOfClass:[JVFloatLabeledTextField class]]) {
        [self textFieldNonError:(JVFloatLabeledTextField *) textField];
    }


    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [self registerForKeyboardNotifications];
    [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionLinkbank_add_bank_cardnumber_input];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (self.currentStep == ZPAtmPaymentStepVietinBankOTP || self.currentStep == ZPAtmPaymentStepVietinBankCapCha) {
        if ([self checkValidateTextField]) {
            [self btnOkOnAccessoryViewClick];
        }
    } else {
        [self btnNextOnAccessoryViewClick];
    }

    return YES;
}

#pragma mark - ScrollViewDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    self.isEndDecelerating = FALSE;

    lastContentOffset = scrollView.contentOffset.x;


}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat widthSV = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - widthSV / 2) / widthSV) + 1;

    //Disable scroll
    CGPoint nowOffset = scrollView.contentOffset;
    if ((lastContentOffset - nowOffset.x) < 0) {
        JVFloatLabeledTextField *tf = (JVFloatLabeledTextField *) ((ZPATMSaveCardObject *) self.arrTF[self.indexCurrentSV]).mTextField;
        if (tf != nil) {
            BOOL showError = tf.text.length > 0 ? true : false;
            if (![self checkValidateTextFieldByType: self.indexCurrentSV andShowError:showError]) {
                //scrollView.scrollEnabled = NO;
                self.isEndDecelerating = TRUE;
                if (!isScrollingToError) {
                    isScrollingToError = true;
                    [self scrollToIndex: self.indexCurrentSV];
                    [self firstResponderKeyboardTextField: self.indexCurrentSV];
                    [self setTimerEnableScroll];
                }

            }
        }

    } else if ((lastContentOffset - nowOffset.x) > 0) {
        self.isEndDecelerating = TRUE;
    } else {
        self.isEndDecelerating = TRUE;
    }


    [self updatePageControlByIndex:page];
    if (lastIndexSV != page) {
        [self updatePageControlByIndex:page];
        [self updatePreNextEnable];
    }
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    self.isEndDecelerating = FALSE;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {


    CGFloat widthSV = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - widthSV / 2) / widthSV) + 1;
    self.isEndDecelerating = TRUE;
    if (lastIndexSV != page) {

        self.indexCurrentSV = lastIndexSV = page;
        [self firstResponderKeyboardTextField: self.indexCurrentSV];

        [self updatePageControl];
        [self updatePreNextEnable];

    }
}

- (void)tapTF {
    self.mScrollTextfeild.scrollEnabled = YES;
    isScrollingToError = false;
}

#pragma mark - Validate

- (BOOL)checkValidateTextField {
    
    
    NSString * errorMessage = @"";
    ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:self.bankCode];

    NSInteger opt = bank.bankType;
    DDLogInfo(@"banktype %ld",(long)opt);
    
    if(self.currentStep == ZPAtmPaymentStepVietinBankCapCha) {
        JVFloatLabeledTextField *tfCaptcha = [self getTextFieldByType:ZPATMSaveCardObjectType_Captcha];
        if ([tfCaptcha.text length] > 0) {

            return YES;
        } else {
            errorMessage = @"Vui lòng nhập mã captcha";
            [self textFieldShowError:tfCaptcha withErrorText:errorMessage];
            return NO;
        }
    } else if (self.currentStep == ZPAtmPaymentStepVietinBankOTP) {

        JVFloatLabeledTextField *tfOTP = [self getTextFieldByType:ZPATMSaveCardObjectType_OTP];
        if ([tfOTP.text length] > 0) {

            return YES;
        } else {
            errorMessage = @"Vui lòng nhập mã OTP";
            [self textFieldShowError:tfOTP withErrorText:errorMessage];
            return NO;
        }
    } else if (self.currentStep == ZPAtmPaymentStepBIDVCapChaPassword) {

        JVFloatLabeledTextField *tfCaptcha = [self getTextFieldByType:ZPATMSaveCardObjectType_Captcha];
        JVFloatLabeledTextField *tfPass = [self getTextFieldByType:ZPATMSaveCardObjectType_CardPassword];

        if (self.hasPass && tfPass.text.length == 0) {
            errorMessage = @"Vui lòng nhập mật mã thẻ";
            [self textFieldShowError:tfPass withErrorText:errorMessage];
            return NO;
        }
        if ([tfCaptcha.text length] == 0) {
            errorMessage = @"Vui lòng nhập mã captcha";
            [self textFieldShowError:tfCaptcha withErrorText:errorMessage];
            return NO;
        }

        return YES;
    } else if (self.currentStep == ZPAtmPaymentStepChooseSMSToken) {

        return YES;
    } else {

        JVFloatLabeledTextField *tfNumber = [self getTextFieldByType:ZPATMSaveCardObjectType_CardNumber];
        JVFloatLabeledTextField *tfName = [self getTextFieldByType:ZPATMSaveCardObjectType_CardName];
        JVFloatLabeledTextField *tfPass = [self getTextFieldByType:ZPATMSaveCardObjectType_CardPassword];
        JVFloatLabeledTextField *tfDate = [self getTextFieldByType:ZPATMSaveCardObjectType_CardExpire];

        if ([[[tfNumber.text zpRemoveFormat:formats] zpTrimSpace] length] <= 5) {
            errorMessage = @"Vui lòng nhập số thẻ";
            [self textFieldShowError:tfNumber withErrorText:errorMessage];
            return NO;
        } else if ([[[tfNumber.text zpRemoveFormat:formats] zpTrimSpace] length] >= 5) {
            if (self.bankCode != nil && [self.bankCode length] > 0) {
                if (bank.bankName.length > 0 ) {
                    if([tfNumber isFirstResponder]) {
                        tfNumber.isShowError = NO;
                        [tfNumber setFloatingTitle:[NSString stringWithFormat:@"%@ %@", @"Thẻ", bank.bankName]];
                    } else {

                        if ([[[tfNumber.text zpRemoveFormat:formats] zpTrimSpace] length] < [[ZPDataManager sharedInstance] doubleForKey:@"atm_card_number_min_length"] || ![ZPATMSaveCardCell checkInPutCardIsLuhnValid:[[tfNumber.text zpRemoveFormat:formats] zpTrimSpace]]) {
                            errorMessage = @"Thông tin số thẻ không chính xác";
                            [self textFieldShowError:tfNumber withErrorText:errorMessage];
                            return NO;

                        }
                    }
                }
            } else {

                [tfNumber setFloatingTitleErrorWithImage:@"Chưa hỗ trợ ngân hàng này" andImage:[ZPResourceManager getImageWithName:@"question"]];
                [self setupFloatingLabelShowBankSupport:tfNumber];


                errorMessage = @"Chưa hỗ trợ ngân hàng này";
                //[self textFieldShowError:tfNumber withErrorText:errorMessage];
                return NO;
            }
        } else if ([[[tfNumber.text zpRemoveFormat:formats] zpTrimSpace] length] < [[ZPDataManager sharedInstance] doubleForKey:@"atm_card_number_min_length"]) {
            errorMessage = @"Thông tin số thẻ không chính xác";
            [self textFieldShowError:tfNumber withErrorText:errorMessage];
            return NO;
        }
        if ([tfName.text zpTrimSpace].length <= 0) {
            errorMessage = @"Vui lòng nhập tên chủ thẻ";
            [self textFieldShowError:tfName withErrorText:errorMessage];
            return NO;
        }
        if (self.hasPass && tfPass.text.length <= 0) {
            errorMessage = @"Vui lòng nhập mật mã thẻ";
            [self textFieldShowError:tfPass withErrorText:errorMessage];
            return NO;
        }

        if (self.hasDate && ([tfDate.text zpTrimSpace].length <= 0)) {
            [self textFieldShowError:tfDate withErrorText:errorMessage];
            if (validFromDate) {
                errorMessage = @"Vui lòng nhập ngày phát hành";
            } else {
                errorMessage = @"Vui lòng nhập ngày hết hạn";
            }
            [self textFieldShowError:tfDate withErrorText:errorMessage];
        } else if (!isCardDateValid && self.hasDate) {
            if (validFromDate) {
                errorMessage = @"Ngày phát hành không hợp lệ";
            } else {
                errorMessage = @"Ngày hết hạn không hợp lệ";
            }
            [self textFieldShowError:tfDate withErrorText:errorMessage];
            return NO;
        }

        if ([errorMessage length] > 0) {
            return NO;
        }

    }

    // [self removeBtnPreNext];
    return true;
}


- (BOOL)checkValidateTextFieldLastest {
    
    
    NSString * errorMessage = @"";
    ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:self.bankCode];

    NSInteger opt = bank.bankType;
    DDLogInfo(@"banktype %ld",(long)opt);
    
    JVFloatLabeledTextField *tfNumber = [self getTextFieldByType:ZPATMSaveCardObjectType_CardNumber];
    JVFloatLabeledTextField *tfName = [self getTextFieldByType:ZPATMSaveCardObjectType_CardName];
    JVFloatLabeledTextField *tfPass = [self getTextFieldByType:ZPATMSaveCardObjectType_CardPassword];
    JVFloatLabeledTextField *tfDate = [self getTextFieldByType:ZPATMSaveCardObjectType_CardExpire];

    if (self.currentStep == ZPAtmPaymentStepBIDVCapChaPassword) {

        JVFloatLabeledTextField *tfCaptcha = [self getTextFieldByType:ZPATMSaveCardObjectType_Captcha];
        JVFloatLabeledTextField *tfPass = [self getTextFieldByType:ZPATMSaveCardObjectType_CardPassword];

        if (self.hasPass && tfPass.text.length == 0) {
            errorMessage = @"Vui lòng nhập mật mã thẻ";
            [self textFieldShowError:tfPass withErrorText:errorMessage];
            [self scrollToIndexByType:ZPATMSaveCardObjectType_CardPassword];
            return NO;
        }
        if ([tfCaptcha.text length] == 0) {
            errorMessage = @"Vui lòng nhập mã captcha";
            [self textFieldShowError:tfCaptcha withErrorText:errorMessage];
            [self scrollToIndexByType:ZPATMSaveCardObjectType_Captcha];
            return NO;
        }
    } else {

        if ([[[tfNumber.text zpRemoveFormat:formats] zpTrimSpace] length] <= 0) {
            errorMessage = @"Vui lòng nhập số thẻ";
            [self textFieldShowError:tfNumber withErrorText:errorMessage];

            [self scrollToIndexByType:ZPATMSaveCardObjectType_CardNumber];

            return NO;

        } else if ([[[tfNumber.text zpRemoveFormat:formats] zpTrimSpace] length] <= 5) {
            errorMessage = @"Thông tin số thẻ không chính xác";
            [self textFieldShowError:tfNumber withErrorText:errorMessage];
            [self scrollToIndexByType:ZPATMSaveCardObjectType_CardNumber];
            return NO;
        } else if ([[[tfNumber.text zpRemoveFormat:formats] zpTrimSpace] length] > 5) {
            if (self.bankCode != nil && [self.bankCode length] > 0) {
                if (bank.bankName.length > 0) {
                    if([tfNumber isFirstResponder]) {
                        tfNumber.isShowError = NO;
                        [tfNumber setFloatingTitle:[NSString stringWithFormat:@"%@ %@", @"Thẻ", bank.bankName]];
                    } else {
                        if ([[[tfNumber.text zpRemoveFormat:formats] zpTrimSpace] length] < [[ZPDataManager sharedInstance] doubleForKey:@"atm_card_number_min_length"] || ![ZPATMSaveCardCell checkInPutCardIsLuhnValid:[[tfNumber.text zpRemoveFormat:formats] zpTrimSpace]]) {
                            errorMessage = @"Thông tin số thẻ không chính xác";
                            [self textFieldShowError:tfNumber withErrorText:errorMessage];
                            [self scrollToIndexByType:ZPATMSaveCardObjectType_CardNumber];
                            return NO;
                        }
                    }
                }
            } else {

                [tfNumber setFloatingTitleErrorWithImage:@"Chưa hỗ trợ ngân hàng này" andImage:[ZPResourceManager getImageWithName:@"question"]];
                [self setupFloatingLabelShowBankSupport:tfNumber];
                errorMessage = @"Chưa hỗ trợ ngân hàng này";
                return NO;
            }
        } else if ([[[tfNumber.text zpRemoveFormat:formats] zpTrimSpace] length] < [[ZPDataManager sharedInstance] doubleForKey:@"atm_card_number_min_length"]) {
            errorMessage = @"Thông tin số thẻ không chính xác";
            [self textFieldShowError:tfNumber withErrorText:errorMessage];
            [self scrollToIndexByType:ZPATMSaveCardObjectType_CardNumber];
            return NO;
        }
        if (self.hasPass && tfPass.text.length <= 0) {
            errorMessage = @"Vui lòng nhập mật mã thẻ";
            [self textFieldShowError:tfPass withErrorText:errorMessage];
            [self scrollToIndexByType:ZPATMSaveCardObjectType_CardPassword];
            return NO;
        }

        if (self.hasDate && ([tfDate.text zpTrimSpace].length <= 0)) {
            if (validFromDate) {
                errorMessage = @"Vui lòng nhập ngày phát hành";
            } else {
                errorMessage = @"Vui lòng nhập ngày hết hạn";
            }
            [self textFieldShowError:tfDate withErrorText:errorMessage];
            [self scrollToIndexByType:ZPATMSaveCardObjectType_CardExpire];
            return NO;

        } else if (!isCardDateValid && self.hasDate) {
            if (validFromDate) {
                errorMessage = @"Ngày phát hành không hợp lệ";
            } else {
                errorMessage = @"Ngày hết hạn không hợp lệ";
            }
            [self textFieldShowError:tfDate withErrorText:errorMessage];
            [self scrollToIndexByType:ZPATMSaveCardObjectType_CardExpire];
            return NO;
        }
        if ([tfName.text zpTrimSpace].length <= 0) {
            errorMessage = @"Vui lòng nhập tên chủ thẻ";
            [self textFieldShowError:tfName withErrorText:errorMessage];
            [self scrollToIndexByType:ZPATMSaveCardObjectType_CardName];
            return NO;
        }
    }

    if ([errorMessage length] > 0) {
        return NO;
    }

    if ([self checkValidateTextField]) {

        //Default color on Card
        [self hightLightLabelOnCardDefault];
        ZPATMSaveCardObject *mdGet = self.arrTF[self.indexCurrentSV];
        [self getFormatXHighLight:mdGet.mTextField andDefault:true];
        ((JVFloatLabeledTextField *) mdGet.mTextField).isBGBottomLineDefault = true;

        //[self removeBtnPreNext];

        //Go step chooseSMSToken
        if (self.hasAuthMethod) {
            [self setChooseForm_OnlySMSToken];
        } else {
            //Go next step
            [self btnOkOnAccessoryViewClick];
        }
        //Go next step
        //[self btnOkOnAccessoryViewClick];
    }

    return true;
}

- (BOOL)checkValidateDate:(NSString *)strDate {

    int y = 0;
    int m = 0;

    if ([strDate zpRemoveFormat:formatsDate].length < 4) {
        return NO;
    }

    NSArray *arrMY = [strDate componentsSeparatedByString:@"/"];

    NSScanner *scannerMM = [NSScanner scannerWithString:arrMY[0]];
    BOOL isNumericMM = [scannerMM scanInteger:NULL] && [scannerMM isAtEnd];

    NSScanner *scannerYY = [NSScanner scannerWithString:arrMY[1]];
    BOOL isNumericYY = [scannerYY scanInteger:NULL] && [scannerYY isAtEnd];

    if (!isNumericMM || !isNumericYY) {
        return NO;
    }

    m = [arrMY[0] intValue];
    y = [arrMY[1] intValue];

    NSDate *date = [NSDate date];
    NSCalendar *calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];;
    int month = (int) [[calendar components:NSCalendarUnitYear | NSCalendarUnitMonth fromDate:date] month];
    int year = (int) [[calendar components:NSCalendarUnitYear | NSCalendarUnitMonth fromDate:date] year] - 2000;
    DDLogInfo(@"y year m month %d.%d.%d.%d", y, year, m, month);

    if (m == 0 || m > 12) {
        return NO;
    }

    if (m == 0 || m > 12 || y < year - 15 || y > year + 15) {
        return NO;
    }
    if (validFromDate) {
        if (y > year) {
            return NO;
        } else if (y == year && m > month) {
            return NO;
        } else {
            return YES;
        }
    } else if (!validFromDate) {
        if (y < year) {
            return NO;
        } else if (y == year && m < month) {
            return NO;
        } else {
            return YES;
        }
    }

    return NO;
}

- (BOOL)checkValidateTextFieldByType:(int )index andShowError:(BOOL)show{
    
    NSString * errorMessage = @"";
    ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:self.bankCode];
    NSInteger opt = bank.bankType;
    DDLogInfo(@"banktype %ld",(long)opt);
    
    JVFloatLabeledTextField *tfNumber = [self getTextFieldByType:ZPATMSaveCardObjectType_CardNumber];
    JVFloatLabeledTextField *tfName = [self getTextFieldByType:ZPATMSaveCardObjectType_CardName];
    JVFloatLabeledTextField *tfPass = [self getTextFieldByType:ZPATMSaveCardObjectType_CardPassword];
    JVFloatLabeledTextField *tfDate = [self getTextFieldByType:ZPATMSaveCardObjectType_CardExpire];

    ZPATMSaveCardObject *mdGet = self.arrTF[index];
    enum ZPATMSaveCardObjectType type = mdGet.mType;
    if (type == ZPATMSaveCardObjectType_CardNumber) {

        if ([[[tfNumber.text zpRemoveFormat:formats] zpTrimSpace] length] <= 0) {
            errorMessage = @"Vui lòng nhập số thẻ";
            if (show) {
                [self textFieldShowError:tfNumber withErrorText:errorMessage];
            }
        } else if ([[[tfNumber.text zpRemoveFormat:formats] zpTrimSpace] length] <= 5) {
            errorMessage = @"Thông tin số thẻ không chính xác";
            if (show) {
                [self textFieldShowError:tfNumber withErrorText:errorMessage];
            }
        } else if ([[[tfNumber.text zpRemoveFormat:formats] zpTrimSpace] length] > 5) {
            if (self.bankCode != nil && [self.bankCode length] > 0) {
                if (bank.bankName.length > 0 ) {
                    if([tfNumber isFirstResponder]) {
                        tfNumber.isShowError = NO;
                        [tfNumber setFloatingTitle:[NSString stringWithFormat:@"%@ %@", @"Thẻ", bank.bankName]];
                    }


                    if ([[[tfNumber.text zpRemoveFormat:formats] zpTrimSpace] length] < [[ZPDataManager sharedInstance] doubleForKey:@"atm_card_number_min_length"] || ![ZPATMSaveCardCell checkInPutCardIsLuhnValid:[[tfNumber.text zpRemoveFormat:formats] zpTrimSpace]]) {
                        errorMessage = @"Thông tin số thẻ không chính xác";
                        if (show) {
                            [self textFieldShowError:tfNumber withErrorText:errorMessage];
                        }


                    }

                    //Check Save Card and Link Card
                    if ([self isSaveCardBefore:[[tfNumber.text zpRemoveFormat:formats] zpTrimSpace] andTf:tfNumber]) {
                        errorMessage = @"Error";
                    }
                }

            } else {

                [tfNumber setFloatingTitleErrorWithImage:@"Chưa hỗ trợ ngân hàng này" andImage:[ZPResourceManager getImageWithName:@"question"]];
                [self setupFloatingLabelShowBankSupport:tfNumber];


                errorMessage = @"Chưa hỗ trợ ngân hàng này";
                //[self textFieldShowError:tfNumber withErrorText:errorMessage];
            }
        }


    }
    if (type == ZPATMSaveCardObjectType_CardName) {
        if ([tfName.text zpTrimSpace].length <= 0) {
            errorMessage = @"Vui lòng nhập tên chủ thẻ";
            if (show) {
                [self textFieldShowError:tfName withErrorText:errorMessage];
            }
        }

    }
    if (type == ZPATMSaveCardObjectType_CardPassword) {
        if (self.hasPass && tfPass.text.length <= 0) {
            errorMessage = @"Vui lòng nhập mật khẩu";
            if (show) {
                [self textFieldShowError:tfPass withErrorText:errorMessage];
            }
        }
    }
    if (type == ZPATMSaveCardObjectType_CardExpire) {
        if (self.hasDate && ([tfDate.text zpTrimSpace].length <= 0)) {
            errorMessage = @"Vui lòng nhập ngày tháng";
            if (show) {
                [self textFieldShowError:tfDate withErrorText:errorMessage];
            }
        } else if (!isCardDateValid && self.hasDate) {
            if (validFromDate) {
                errorMessage = @"Ngày phát hành không hợp lệ";
            } else {
                errorMessage = @"Ngày hết hạn không hợp lệ";
            }
            if (show) {
                [self textFieldShowError:tfDate withErrorText:errorMessage];
            }
        }

    }
    if (type == ZPATMSaveCardObjectType_Captcha) {
        if (self.hasPass && tfPass.text.length <= 0) {
            errorMessage = @"Vui lòng nhập mã captcha";
            if (show) {
                [self textFieldShowError:tfPass withErrorText:errorMessage];
            }
        }
    }
    if ([errorMessage length] > 0) {
        return NO;
    }
    return true;
}

#pragma mark - Detect Bank

- (void)detectBankCodeFromBankNumber:(NSString *)detectString {
    DDLogInfo(@"detectString %@", detectString);

    JVFloatLabeledTextField *tfNumber = [self getTextFieldByType:ZPATMSaveCardObjectType_CardNumber];

    NSString *detectedBankCode = @"";
    //detect atm card
    if (detectString.length >= 6) {
        NSString * first6Chars = [detectString substringToIndex:6];
        detectedBankCode = [[ZPDataManager sharedInstance].bankManager.bankbins objectForKey:first6Chars];
    }
    if (detectString.length >= 4 && detectedBankCode.length <= 0){
        NSString * first4Chars = [detectString substringToIndex:4];
        detectedBankCode = [[ZPDataManager sharedInstance].bankManager.bankbins objectForKey:first4Chars];
    }

    self.bankCode = detectedBankCode;
    ZPBank *bank = [ZPBank copyFrom:[[ZPDataManager sharedInstance].bankManager getBankInfoWith:self.bankCode]];
    if (bank == nil) {
        self.bankCode = nil;
    }
    
    bank.detectString = detectString;
    bank.tfString = [tfNumber.text zpRemoveFormat:formats];
    if (detectedBankCode != nil && [detectedBankCode length] > 0) {
        if (bank.bankName.length > 0) {
            [tfNumber setFloatingTitle:[NSString stringWithFormat:@"%@ %@", @"Thẻ", bank.bankName]];
        }
    } else {
        //[tfNumber setFloatingTitle:tfNumber.placeholder];
    }

    if (((detectedBankCode.length > 0 && self.delegate) || (detectString.length < 6 && detectString.length > 0)) && [self.delegate respondsToSelector:@selector(detectCell:didDetectBank:)]) {
        [self.delegate detectCell:self didDetectBank:bank];
        [self updateLayouts];
    }

}


+ (BOOL)checkInPutCardIsLuhnValid:(NSString *)cardNumber {
    if (cardNumber.length < [[ZPDataManager sharedInstance] doubleForKey:@"atm_card_number_min_length"]) {
        return NO;
    }

    if (!ATM_CHECK_CARD_IS_LUHN_VALID) {
        return YES;
    }
    NSMutableArray *stringAsChars = [cardNumber zpStringToArray];
    BOOL isOdd = YES;
    int oddSum = 0;
    int evenSum = 0;

    for (int i = (int) [cardNumber length] - 1; i >= 0; i--) {

        int digit = [(NSString *) [stringAsChars objectAtIndex:i] intValue];

        if (isOdd) {
            oddSum += digit;
        } else {
            evenSum += digit / 5 + (2 * digit) % 10;
        }

        isOdd = !isOdd;
    }
    return ((oddSum + evenSum) % 10 == 0);
}

#pragma mark - unKnow

- (void)setBankCode:(NSString *)bankCode {
    _bankCode = bankCode;
    [self updateLayouts];
}

- (void)setCardNumber:(NSString *)number {
    DDLogInfo(@"setCardNumber");
    if (number.length <= 0) {
        return;
    }

    JVFloatLabeledTextField *tfNumber = [self getTextFieldByType:ZPATMSaveCardObjectType_CardNumber];
    tfNumber.text = number;
    [self detectBankCodeFromBankNumber:number];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self formatTextField:tfNumber];
        self.mLblCardNumber.text = tfNumber.text;
    });
}

- (void)setATMCardInfo:(ZPAtmCard *)cachedCard fromCC:(BOOL)backFromCC {
    DDLogInfo(@"setATMCardInfo");
    DDLogInfo(@"cardNumber: %@", cachedCard.cardNumber);
    if (cachedCard.cardNumber == nil) {
        return;
    }

    JVFloatLabeledTextField *tfNumber = [self getTextFieldByType:ZPATMSaveCardObjectType_CardNumber];
    isBackFromCC = backFromCC;

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

        ZPATMSaveCardObject *mdGet = self.arrTF[self.indexCurrentSV];
        [mdGet.mTextField becomeFirstResponder];

        tfNumber.text = cachedCard.cardNumber;
        [self formatTextField:tfNumber];
        self.mLblCardNumber.text = [self getFormatXXXXDefaultNumber:tfNumber.text];


        [self detectBankCodeFromBankNumber:cachedCard.cardNumber];
        //[self SetBecomeFirstScrollTF:tfNumber];


        [self getFormatXHighLight:tfNumber andDefault:false];
        [self hightLightLabelOnCard_NotUse:tfNumber];

        self.mLblCardExpire.text = self.titleExpireDefault;

        //Set floating color
        tfNumber.isShowTailImage = false;
        tfNumber.isShowError = false;
        if (self.bankCode.length <= 0) {
            return;
        }

        //Get image bankcode
        NSString *imgGet = [ZPResourceManager bankValueFromBankCodeBundle:@"bank_icon" andBankCode:self.bankCode andIsBankOrCC:true];
        self.mImgIconBank.image = [ZPResourceManager getImageWithName:imgGet];
        self.mImgIconBank.backgroundColor = [UIColor clearColor];
        [self setGradientColorForCardView:self.bankCode];

    });

}
        
+ (ZPAtmInputType) inputTypeForBankCode: (NSString *) bankCode {
    ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:bankCode];
    int opt = bank.bankOPT;
    return [ZPResourceManager classifyAtmInputTypeWithOption:opt andBankType:nil];;
}

#pragma mark - Gradient Color

- (void)setGradientColorForCardView:(NSString *)bankCode {
    if (gradient) {
        [gradient removeFromSuperlayer];
    }
    NSArray *colors = [ZPResourceManager bankColorGradientFromBankCodeBundle:bankCode andIsBankOrCC:true];
    if (bankCode == nil || [bankCode isEqualToString:@""]) {
        colors = @[
                [UIColor zpColorFromHexString:kColorDefaultGradientStart],
                [UIColor zpColorFromHexString:kColorDefaultGradientEnd]
        ];
    }
    gradient = [CAGradientLayer layer];
    gradient.frame = self.bounds;
    gradient.colors = @[(__bridge id) [[colors firstObject] CGColor], (__bridge id) [[colors lastObject] CGColor]];
    gradient.startPoint = CGPointMake(0, 1);
    gradient.endPoint = CGPointMake(1, 0);
    [self.mViewCardBG.layer insertSublayer:gradient atIndex:0];
}

#pragma mark - Show Bank Support

- (void)setupFloatingLabelShowBankSupport:(JVFloatLabeledTextField *)tf {

    double delayInSeconds = .2;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapFloatingLabel:)];
        tf.floatingLabel.userInteractionEnabled = YES;
        [tf.floatingLabel addGestureRecognizer:tap];

        if (!tf.viewTapOnFloatingLabel) {
            return;
        }

        UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapFloatingLabel:)];
        tf.viewTapOnFloatingLabel.userInteractionEnabled = YES;
        [tf.viewTapOnFloatingLabel addGestureRecognizer:tap2];

        tf.viewTapOnFloatingLabel.backgroundColor = [UIColor clearColor];
    });

}

- (void)tapFloatingLabel:(UITapGestureRecognizer *)tap {
    ZPATMSaveCardObject *mdGet = self.arrTF[0];
    JVFloatLabeledTextField *tf = (JVFloatLabeledTextField *) mdGet.mTextField;
    if (!tf.isShowTailImage) {
        return;
    }

    ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:self.bankCode];
    if (self.delegate && [self.delegate respondsToSelector:@selector(detectCell:didEditCardNumberWithDetectedBank:)]) {
        [self.delegate detectCell:self didEditCardNumberWithDetectedBank:bank];
    }
}
#pragma mark Utils
- (int)bankSupportType:(NSString *)bankCode {
    ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:bankCode];
    return bank.supportType;
}

#pragma mark - Analytic
- (void)analyticTrack:(enum ZPATMSaveCardObjectType)type next:(BOOL)next {
    ZPAnalyticEventAction eventid;
    switch (type) {
        case ZPATMSaveCardObjectType_CardExpire :
            eventid = next ? ZPAnalyticEventActionLinkbank_add_bank_cardnumber_forward : ZPAnalyticEventActionLinkbank_add_bank_date_back;
            break;
        case ZPATMSaveCardObjectType_CardName :
            eventid = next ? ZPAnalyticEventActionLinkbank_add_bank_date_forward : ZPAnalyticEventActionLinkbank_add_bank_name_back;
            break;
        default :
            eventid = ZPAnalyticEventActionLinkbank_add_bank_name_forward;
            break;
    }
    [[ZPTrackingHelper shared] trackEvent:eventid];
}

@end

