//
//  ZPATMSaveCardObject.h
//  ZaloPayWalletSDK
//
//  Created by CPU11689 on 9/22/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
enum ZPATMSaveCardObjectType{
    ZPATMSaveCardObjectType_CardAmount,
    ZPATMSaveCardObjectType_CardNumber,
    ZPATMSaveCardObjectType_CardName,
    ZPATMSaveCardObjectType_CardPassword,
    ZPATMSaveCardObjectType_CardExpire,
    ZPATMSaveCardObjectType_CardCVV,
    ZPATMSaveCardObjectType_OptionSMSToken,
    ZPATMSaveCardObjectType_Captcha,
    ZPATMSaveCardObjectType_OTP,
    ZPATMSaveCardObjectType_None
};


@interface ZPATMSaveCardObject : NSObject

@property(nonatomic,retain) NSString *mTitle;
@property(nonatomic,retain) NSString *mValue;
@property(nonatomic,retain) NSString *mPlaceHolder;
@property(nonatomic) enum ZPATMSaveCardObjectType mType;
@property(nonatomic,retain) UITextField *mTextField;

-(ZPATMSaveCardObject*)initWithTitle:(NSString*)title andValue:(NSString*)value andPlaceHolder:(NSString*)placeHolder andType:(enum ZPATMSaveCardObjectType)type;

@end
