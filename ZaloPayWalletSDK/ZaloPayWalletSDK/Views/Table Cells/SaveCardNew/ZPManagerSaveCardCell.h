//
//  ZPManagerSaveCardCell.h
//  ZaloPayWalletSDK
//
//  Created by vuongvv on 10/19/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZPManagerSaveCardCell : NSObject

+ (instancetype)sharedInstance;

@property(nonatomic) BOOL isShowAMTCardCell;
@property(nonatomic) BOOL isShowCreditCardCell;

@end
