//
//  ZPResultSupportView.m
//  ZaloPay
//
//  Created by TaLi on 3/14/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPResultSupportView.h"
#import "ZPResourceManager.h"
#import "ZPResultViewModel.h"
#import <ZaloPayCommonSwift/ZaloPayCommonSwift-Swift.h>

@implementation ZPResultSupportView

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupCommon];
}

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupCommon];
    }
    return self;
}

- (void)setupCommon {
    ZPLineView *topLine = [[ZPLineView alloc] init];
    [self.contentView addSubview:topLine];
    [topLine alignToTop];
    ZPLineView *bottomLine = [[ZPLineView alloc] init];
    [self.contentView addSubview:bottomLine];
    [bottomLine alignToBottom];

    self.methodIcon.contentMode = UIViewContentModeCenter;
    [self.methodName zpMainGrayRegular];
    self.selectionStyle = UITableViewCellSelectionStyleGray;

}

- (void)setModel:(ZPResultViewModel *)model {
    [super setModel:model];

    switch (model.cellType) {
        case ZPResultCellTypeSupport:
            self.methodIcon.image = [ZPResourceManager getImageWithName:@"support"];
            self.methodName.text = stringSupportTitle;

            break;
        case ZPResultCellTypeUpdateLevel3:
            self.methodIcon.image = [ZPResourceManager getImageWithName:@"updateinfo"];
            self.methodName.text = stringTitleUpdatelevel3;

            break;
        default:
            break;
    }
}
@end
