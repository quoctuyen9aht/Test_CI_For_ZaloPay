//
//  ZPZaloPayWalletMethodCell.m
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/12/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPZaloPayWalletMethodCell.h"
#import "ZaloPayWalletSDKPayment.h"

@implementation ZPZaloPayWalletMethodCell

- (void)setupCell {
    [super setupCell];
    [self addLabelBallance];

}

- (void)addLabelBallance {
    self.labelBallance = [[UILabel alloc] init];
    [self.contentView addSubview:self.labelBallance];
    self.labelBallance.textAlignment = NSTextAlignmentRight;
    self.labelBallance.textColor = [UIColor subText];
    self.labelBallance.font = [UIFont defaultSFUITextRegular];
    [self.labelBallance mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(-12);
        make.width.equalTo(150);
        make.height.equalTo(20);
        make.top.equalTo(20);
    }];
}

- (void)setData:(ZPPaymentMethodCellDisplay *)data {
    [super setData:data];
    id<ZPWalletDependenceProtocol> helper = [ZaloPayWalletSDKPayment sharedInstance].appDependence;
    long balance = helper.currentBalance;
    self.labelBallance.attributedText = [[@(balance) formatCurrency] formatCurrencyFont:[UIFont SFUITextMediumWithSize:17]
                                                                                  color:[UIColor subText]
                                                                                vndFont:[UIFont SFUITextRegularWithSize:13]
                                                                               vndcolor:[UIColor subText]
                                                                              alignment:NSTextAlignmentRight];
    int ballanceTop = data.type != ZPPaymentMethodCellTypeAvailable? 10 : 20;
    [self.labelBallance mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(ballanceTop);
    }];
}

@end
