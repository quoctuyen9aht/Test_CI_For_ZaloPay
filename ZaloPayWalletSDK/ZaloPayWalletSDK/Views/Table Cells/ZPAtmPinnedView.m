//
//  ZPAtmPinnedView.m
//  ZaloSDK.Payment
//
//  Created by Hoang Nguyen on 6/22/15.
//  Copyright (c) 2015-present VNG Corporation All rights reserved.
//

#import "ZPAtmPinnedView.h"
#import "ZPConfig.h"
#import "ZPDefine.h"
#import "ZPDataManager.h"
#import "ZPResourceManager.h"
#import "ZPBank.h"
#import "UIColor+ZPExtension.h"
#import "UIImage+ZPExtension.h"
#import "UIView+ZPBorders.h"
#import "ZaloPayWalletSDKLog.h"

#define CELL_SPACING (IS_IPAD?5:5)
#define CELL_MARGIN 10
#define GRADIENT_SIZE 50
#define HORIZONTAL_COLLECTION_VIEW_TOP_SPACE 17



@interface ZPAtmPinnedView() <UICollectionViewDataSource, UICollectionViewDelegate>
@property (assign, nonatomic) int selectedIndex;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) UIView * backgroundView;
@end

@implementation ZPAtmPinnedView {
    NSArray *arrBank;
}

@synthesize backgroundView;

- (instancetype)init{
    
    self = (ZPAtmPinnedView *)[[ZPDataManager sharedInstance] viewWithNibName:@"ZPAtmPinnedView" owner:self];
    
    self.contentView.layer.cornerRadius = 5;
    
    [self initBackgroundView];
    
    [self updateViewsSize];
    self.backgroundColor = [UIColor clearColor];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    arrBank = [[NSArray alloc] init];
    self.imageHeaderBd.layer.cornerRadius = 35;
    self.imageHeaderBd.layer.masksToBounds = YES;    
    return self;
}

- (void)initBackgroundView{
    self.backgroundView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.backgroundView setBackgroundColor:[UIColor blackColor]];
    [self.backgroundView setAlpha:0.6f];
    [self.backgroundView setUserInteractionEnabled:YES];
    
    self.imageHeaderBd.image = [ZPResourceManager getImageWithName:@"ic_alert_info"];
}

- (CGSize)containerSize{
    CGSize containerSize = [UIScreen mainScreen].bounds.size;
    CGFloat ratio = IS_IPAD ? 0.7 : 1.0f;
    int numOfRow = IS_IPAD ? 3 : 2;
    CGFloat small = MIN(containerSize.width, containerSize.height) * ratio;
    CGFloat portrait = 235 + (([arrBank count]/numOfRow) * 5) + (([arrBank count]/numOfRow) * 48) + (([arrBank count]%numOfRow) * 48);
    return CGSizeMake(small, portrait);
}




- (void)updateViewsSize{
    CGSize containerSize = [self containerSize];
    CGSize screenSize = self.superview.frame.size;
    
//    if (IS_LANDSCAPE) {
//        float tempWidth = MAX(screenSize.width, screenSize.height);
//        screenSize.height = MIN(screenSize.width, screenSize.height);
//        screenSize.width = tempWidth;
//    }
    
    
    CGRect frame = self.frame;
    
    frame.size.width = (int)(containerSize.width * 0.8f);
    frame.size.height = containerSize.height;
    frame.origin.y = (screenSize.height - frame.size.height) / 2;
    frame.origin.x = (screenSize.width - frame.size.width) / 2;
    self.frame = frame;
    
    CGRect backgroundViewFrame = self.backgroundView.frame;
    backgroundViewFrame.size.width = MAX(screenSize.width, screenSize.height);
    backgroundViewFrame.size.height = MAX(screenSize.width, screenSize.height);
    backgroundViewFrame.origin.x = 0;
    backgroundViewFrame.origin.y = 0;
    self.backgroundView.frame = backgroundViewFrame;
}
//- (void)setOkButtonEnabled:(BOOL)enabled {
//    [self.okButton setEnabled:enabled];
//    self.okButton.backgroundColor = enabled ? [UIColor zpLightBlueColor] : [UIColor zpButtonDisabledColor];
//}
- (void)awakeFromNib {
    [super awakeFromNib];

    [self.collectionView registerNib:[[ZPDataManager sharedInstance] nibNamed:@"ZPBasicCollectionCell"] forCellWithReuseIdentifier:@"cell"];
    
    self.selectedIndex = -1;
    [self.okButton addTopBorderWithHeight:1.0 andColor:[UIColor zpColorFromHexString:@"#E3E6E7"]];
    [self.okButton setTitleColor:[UIColor zpLightBlueColor] forState:UIControlStateNormal];
    [self.okButton setBackgroundColor:[UIColor whiteColor]];
//    [self.okButton setBackgroundImage:[UIImage zpImageWithColor:[UIColor zpButtonDisabledColor]] forState:UIControlStateDisabled];
//    [self.okButton setBackgroundImage:[UIImage zpImageWithColor:[UIColor zpLightBlueColor]] forState:UIControlStateNormal];
//    [self updateOkButtonState];
    
    for (UIView * view in self.layerViews) {
        view.hidden = YES;
    }
    self.titleLabel.layer.shadowOpacity = 0.5f;
    self.titleLabel.layer.shadowRadius = 1.0f;
    self.titleLabel.layer.shadowOffset = CGSizeMake(-1, 0);
    self.titleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    
//    self.collectionViewFlow.scrollDirection = IS_LANDSCAPE ? UICollectionViewScrollDirectionHorizontal : UICollectionViewScrollDirectionVertical;
    self.collectionViewFlow.scrollDirection =  UICollectionViewScrollDirectionVertical;
    
}

- (void)updateLayoutsWithAnimation:(BOOL)animation {
    for (UIView * view in self.layerViews) {
        view.hidden = YES;
    }
    
    [self updateViewsSize];
    self.constraintCollectionWidth.constant = self.frame.size.width;
    self.constraintCollectionViewBottom.constant = 0;
    self.constraintCollectionViewTop.constant = 0;
    self.topLayerView.hidden = self.botLayerView.hidden = NO;
    [self setNeedsLayout];
   // [self setNeedsUpdateConstraints];
    
    for (UIView * view in self.layerViews) {
        CAGradientLayer * gradient = [CAGradientLayer layer];
        gradient.frame = view.bounds;
        
    }
    [self.collectionView reloadData];
}

#pragma mark - UIAction
- (IBAction)onClickOkButton:(id)sender {
   
    if (self.delegate && [self.delegate respondsToSelector:@selector(pinnedCell:didSelectBank:)]) {
        [self.delegate pinnedCell:self didSelectBank:nil];
    }
    [self dismiss];
}


#pragma mark - UICollectionViewDataSource & Delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return arrBank.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    ZPBank * bank = [arrBank objectAtIndex:indexPath.row];
    UIImageView * image = (UIImageView *)[cell viewWithTag:10];
    UILabel * label = (UILabel *)[cell viewWithTag:11];
    label.hidden = YES;
    cell.translatesAutoresizingMaskIntoConstraints = YES;
    //image.translatesAutoresizingMaskIntoConstraints = YES;
    //image.frame = CGRectMake(0, 0, 96, 30);
    image.contentMode = UIViewContentModeCenter;
    image.image = [ZPResourceManager getImageWithName:[NSString stringWithFormat:@"%@.png",bank.imageName]];
    if (image.image == nil) {
        image.image = [UIImage zpImageWithColor:[UIColor whiteColor]];
    }
    cell.selected = NO;
    cell.layer.borderWidth = 0.5f;
    cell.layer.cornerRadius = 5;
    cell.layer.borderColor = indexPath.row == self.selectedIndex ? [UIColor zpLightBlueColor].CGColor : [UIColor zpColorFromHexString:@"#e3e6e7"].CGColor;
    cell.contentView.backgroundColor  = [UIColor zpColorFromHexString:@"#FFFFFF"];

    return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    //self.selectedIndex = (int)indexPath.row;
    //[self updateOkButtonState];
    //[collectionView reloadData];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    float width = 90;
    float height = 44;
    
    int numOfRow = IS_IPAD ? 3 : 2;
    
    float realWidth = width;
    float realHeight = height;
//    if (IS_LANDSCAPE) {
//        realHeight = (collectionView.frame.size.height - CELL_SPACING * (numOfRow - 1)) / numOfRow;
//        if (realHeight <= 0) {
//            realHeight = height;
//        }
//
//        realWidth = realHeight * width / height;
//
//
//    } else {
//        realWidth = (int)((collectionView.frame.size.width - CELL_SPACING - (2 * CELL_MARGIN)) * (numOfRow - 1))/ numOfRow;
//        if (realWidth <= 0) {
//            realWidth = width;
//        }
//
//        DDLogInfo(@"collectionView width: %f", collectionView.frame.size.width);
//        DDLogInfo(@"realWidth: %f", realWidth);
//
//       // realHeight = realWidth * height / width;
//    }
    
    realWidth = (int)((collectionView.frame.size.width - CELL_SPACING - (2 * CELL_MARGIN)) * (numOfRow - 1))/ numOfRow;
    if (realWidth <= 0) {
        realWidth = width;
    }
    
    return CGSizeMake(realWidth, realHeight);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return CELL_SPACING;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return CELL_SPACING;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 10, 0, 10);
}


#pragma mark - override
- (NSDictionary *)outputParams{
    ZPBank * bank = [arrBank objectAtIndex:self.selectedIndex];
    
    NSMutableDictionary * params = [[NSMutableDictionary alloc] init];
    
    if (bank) {
        if (bank.bankCode.length > 0)
            [params setObject:bank.bankCode forKey:@"bankCode"];
        
        if (bank.bankName.length > 0)
            [params setObject:bank.bankName forKey:@"bankName"];
        
    }
    
    return params;
}

- (void)updateLayouts{
    [super updateLayouts];
    [self updateLayoutsWithAnimation:YES];
}

- (void)updateConstraints{
    [super updateConstraints];
    [self updateLayoutsWithAnimation:YES];
}

- (void)updateOkButtonState{
    if (self.selectedIndex < 0 || self.selectedIndex >= [ZPDataManager sharedInstance].bankManager.allBanks.count) {
        [self.okButton setEnabled:NO];
    } else {
        [self.okButton setEnabled:YES];
    }
}
#pragma mark - Public
- (void)showInView:(UIView *)view withData:(NSArray*)arrData{
    arrBank = arrData;
    CGRect frame = self.frame;
    frame.origin.x = (view.frame.size.width - frame.size.width) / 2;
    frame.origin.y = (view.frame.size.height  - frame.size.height) / 2;
    self.frame = frame;
    UIView *rootView = [[[[UIApplication sharedApplication] delegate] window] rootViewController].view;
    [rootView addSubview:self.backgroundView];
    [rootView addSubview:self];
    [self updateLayoutsWithAnimation:YES];
}

- (void)dismiss{
    if (self.superview) {
        [self removeFromSuperview];
    }
    if (self.backgroundView.superview) {
        [self.backgroundView removeFromSuperview];
    }
    
}
@end
