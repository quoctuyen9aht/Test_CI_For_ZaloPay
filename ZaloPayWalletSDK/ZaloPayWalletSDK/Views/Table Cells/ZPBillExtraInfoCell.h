//
//  ZPBillExtraInfoCell.h
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/12/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZPBillExtraInfoData.h"
#import "ZPBill.h"
@class ZPLineView;
@class ZPVoucherInfo;
@class ZPPromotionInfo;
@interface ZPBillExtraInfoCell : UITableViewCell
@property (nonatomic, strong) ZPLineView *line;

- (void)setData:(ZPBillExtraInfoData *)data;
- (void)setBasePriceFrom:(ZPBill *)bill;
- (void)setVoucherFrom:(ZPVoucherInfo *)voucher;
- (void)setPromotionFrom:(ZPPromotionInfo *)promotionInfo;
@end
