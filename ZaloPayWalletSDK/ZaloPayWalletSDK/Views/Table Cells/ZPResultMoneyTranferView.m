//
//  ZPResultMoneyTranferView.m
//  ZaloPayWalletSDK
//
//  Created by thi la on 6/15/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPResultMoneyTranferView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ZPResultViewModel.h"

@interface ZPResultMoneyTranferView ()
@property(strong, nonatomic) UIImageView *imgeSourceUser;
@property(strong, nonatomic) UIImageView *imgeDestUser;
@property(strong, nonatomic) UIImageView *imageArrow;

@end

@implementation ZPResultMoneyTranferView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    _imgeSourceUser = [[UIImageView alloc] initWithFrame:CGRectZero];
    _imgeDestUser = [[UIImageView alloc] initWithFrame:CGRectZero];
    _imageArrow = [[UIImageView alloc] initWithFrame:CGRectZero];

    _imageArrow.contentMode = UIViewContentModeCenter;

    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    [self addSubview:view];

    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(@0);
        make.width.greaterThanOrEqualTo(0);
        make.height.greaterThanOrEqualTo(0);
    }];


    NSArray <UIView *> *views = @[_imgeSourceUser, _imageArrow, _imgeDestUser];

    [views enumerateObjectsUsingBlock:^(UIView *_Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
        [view addSubview:obj];
        if (idx == 0) {
            [obj mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(0);
                make.left.equalTo(0);
                make.width.equalTo(40);
                make.height.equalTo(40);
                make.bottom.equalTo(0);
            }];
        } else {
            UIView *before = views[idx - 1];
            [obj mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(0);
                make.left.equalTo(before.mas_right).offset(10);
                make.width.equalTo(40);
                make.height.equalTo(40);
                if (idx == views.count - 1) {
                    make.right.equalTo(0);
                }
                make.bottom.equalTo(0);
            }];
        }

    }];
    [_imageArrow setImage:[UIImage imageNamed:@"arrow_transfer"]];
    [self layoutIfNeeded];
    _imgeSourceUser.contentMode = UIViewContentModeScaleAspectFill;
    _imgeDestUser.contentMode = UIViewContentModeScaleAspectFill;
    [_imgeSourceUser roundRect:_imgeSourceUser.bounds.size.width / 2];
    [_imgeDestUser roundRect:_imgeDestUser.bounds.size.width / 2];
}

- (void)setModel:(ZPResultViewModel *)model {
    [super setModel:model];

    ZPResultTranferMoneyDetailViewModel *viewModel = (ZPResultTranferMoneyDetailViewModel *) model;

    [_imgeSourceUser sd_setImageWithURL:[NSURL URLWithString:viewModel.userAvatar] placeholderImage:[UIImage defaultAvatar]];
    [_imgeDestUser sd_setImageWithURL:[NSURL URLWithString:viewModel.friendAvatar] placeholderImage:[UIImage defaultAvatar]];
}
@end
