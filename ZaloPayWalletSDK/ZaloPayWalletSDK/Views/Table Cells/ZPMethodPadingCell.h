//
//  ZPMethodPadingCell.h
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/15/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZPMethodPadingData : NSObject
@end

@interface ZPMethodPadingCell : UITableViewCell

@end
