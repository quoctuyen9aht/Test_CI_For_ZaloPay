//
//  ZPMethodBaseCell.h
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/12/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZPPaymentMethodCellDisplay.h"

@class ZPLineView;
@interface ZPMethodBaseCell : UITableViewCell
@property (nonatomic, strong) ZPIconFontImageView *methodImageView;
@property (nonatomic, strong) ZPIconFontImageView *checkedImageView;
@property (nonatomic, strong) ZPLineView *line;
@property (nonatomic, strong) UILabel *lbMethodName;
@property (nonatomic, strong) UILabel *lbMethodInfo;
- (void)setData:(ZPPaymentMethodCellDisplay *)data;
- (void)setupCell;
@end
