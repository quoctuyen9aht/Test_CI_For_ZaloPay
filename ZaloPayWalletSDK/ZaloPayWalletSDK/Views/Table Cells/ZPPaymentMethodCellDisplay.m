//
//  ZPPaymentMethodCellDisplay.m
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/12/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPPaymentMethodCellDisplay.h"
#import "ZPPaymentMethod.h"
#import "ZPDataManager.h"
#import "ZPMiniBank.h"
#import "ZPResourceManager.h"
#import "ZaloPayWalletSDKPayment.h"
#import "ZPBill.h"

@implementation ZPPaymentMethodCellDisplay

- (instancetype)initWith:(ZPPaymentMethod *)method withAmount:(long)amount{
    self = [super init];
    self.method = method;
    ZPDataManager *dataManager = [ZPDataManager sharedInstance];
    ZPPaymentMethodCellType type = [ZPPaymentMethodCellDisplay checkTypeCell:method];
    self.fee = MAX(method.totalChargeAmount - amount, 0);
    self.image = [ZPResourceManager getImageWithName:method.methodIcon];
    self.iconfontName = method.methodIcon;
    self.methodName = method.methodName;
    UIColor *textColor = [UIColor redColor];
    UIColor *backgroundColor = [UIColor whiteColor];
    BOOL isHiddenArrow = YES;
    CGFloat alpha = 0.4;
    switch (type) {
        case ZPPaymentMethodCellTypeMinAppVersion:
            self.feeText = stringErrorMinVersion;
            break;
        case ZPPaymentMethodCellTypeChannelUnsupport:
            self.feeText = [dataManager messageForKey:@"zalo_pay_channel_unsupport"];
            break;
        case ZPPaymentMethodCellTypeMaintain:
            self.feeText = [dataManager messageForKey:@"message_chanel_is_maintain"];
            break;
        case ZPPaymentMethodCellTypeUnsupportAmmount:
            self.feeText = method.errorMessage ?: [dataManager messageForKey:@"zalo_pay_channel_unsupport_amount"];
            break;
        case ZPPaymentMethodCellTypeNotEnoughMoney:
            self.feeText = [R string_Withdraw_NotEnoughMoney];
            break;
        case ZPPaymentMethodCellTypeNotOverTransaction:
            self.feeText = @"Vượt quá hạn mức một lần giao dịch.";
            break;
        default:
            alpha = 1;
            isHiddenArrow = NO;
            textColor = [UIColor subText];
            if (self.method.savedBankAccount && [ZaloPayWalletSDKPayment sharedInstance].bill.transType != ZPTransTypeWithDraw) {
                self.feeText = stringMessageVCBMinimumBanlance;
            }
            break;
    }
    self.type = type;
    self.alpha = alpha;
    self.feeTextColor = textColor;
    self.isHiddenArrow = isHiddenArrow;
    self.backgroundColor = backgroundColor;
    self.isNewMethod = [method isNewMethod];
    return self;
}


+ (ZPPaymentMethodCellType)checkTypeCell:(ZPPaymentMethod *)method {
    if (method.defaultChannel && method.methodType == ZPPaymentMethodTypeAtm) {
        if(!method.isSupportAmount) { // check trường số tiền cần thanh toán < min PPAmount của tất cả channel
            return ZPPaymentMethodCellTypeUnsupportAmmount;
        }
        return ZPPaymentMethodCellTypeAvailable;
    }

    if (method.isAppVersionNotSupport) {
        return ZPPaymentMethodCellTypeMinAppVersion;
    }
    if (method.support == 0) {
        return ZPPaymentMethodCellTypeChannelUnsupport;
    }
    if (method.status == 2 || method.support == 2) {
        return ZPPaymentMethodCellTypeMaintain;
    }
    if(method.miniBank == nil) {
        return ZPPaymentMethodCellTypeChannelUnsupport;
    }
    if (method.miniBank.minibankStatus == ZPMiniBankStatus_Maintenance) {
        return ZPPaymentMethodCellTypeMaintain;
    }
    if(!method.isSupportAmount) {
        return ZPPaymentMethodCellTypeUnsupportAmmount;
    }
    if (method.isTotalChargeGreaterThanBalance) {
        return ZPPaymentMethodCellTypeNotEnoughMoney;
    }
    
    if (method.isTotalChargeGreaterThanMaxAmount) {
        return ZPPaymentMethodCellTypeNotOverTransaction;
    }
    return ZPPaymentMethodCellTypeAvailable;
}

+ (BOOL)isAvailable:(ZPPaymentMethod *)method {
    return [self checkTypeCell:method] == ZPPaymentMethodCellTypeAvailable;
}

@end
