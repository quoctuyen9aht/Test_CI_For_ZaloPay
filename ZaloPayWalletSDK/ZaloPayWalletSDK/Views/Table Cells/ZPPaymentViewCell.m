//
//  ZaloPaymentSDK
//
//  Created by hoangnx2 on 1/14/14.
//  Copyright (c) 2014 VNG Corporation. All rights reserved.
//

#import "ZPPaymentViewCell.h"

#import "ZPTextFieldRecordView.h"
#import "ZPCaptchaRecordView.h"
#import "ZPCaptchaRecordObject.h"
#import "ZPTextFieldRecordObject.h"
#import "ZPRecordObject.h"
#import "UIColor+ZPExtension.h"
#import "ZPConfig.h"
#import "ZPPaymentInfo.h"
#import "UIColor+ZPExtension.h"
#import "ZPResourceManager.h"
#import "ZPKeyboardToolbar.h"
#import "UIImage+ZPExtension.h"
#import "ZPDataManager.h"
#import "UIView+ZPBorders.h"
#import "ZaloPayWalletSDKLog.h"
#import <CoreText/CoreText.h>

#define kFontNumberCardFile @"OCRA"

@interface ZPPaymentViewCell()<ZPRecordViewDelegate> {
    //UIButton* okButton;
    UIButton* captureButton;
    UIImageView * betaView;
    
}
@end


@implementation ZPPaymentViewCell
@synthesize whiteView;

-(void)awakeFromNib{
    [super awakeFromNib];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showMapCardIntro:)
                                                 name:ZPShowMapCardIntro
                                               object:nil];
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self endEditing:YES];
}

- (instancetype)initWithGuiObject:(ZPCellObject*) object{
    if (!object) return nil;
    
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:object.reuseID];
    if (self) {
        
        whiteView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [ZPResourceManager getContainterWidth], 0)];
        whiteView.clipsToBounds = NO;
        [whiteView setBackgroundColor:[UIColor whiteColor]];
        [self addSubview:whiteView];
        self.mySubViews = [[NSMutableArray alloc] init];
        
        
        float totalHeight = 0;
        self.type = object.type;
        for (ZPRecordObject* recordObject in object.subViews) {
            
            ZPRecordView * view = nil;
            
            if ([recordObject isKindOfClass:[ZPTextFieldRecordObject class]]) {
                view = [[ZPTextFieldRecordView alloc] initWithRecordObject:recordObject];
            } else if ([recordObject isKindOfClass:[ZPCaptchaRecordObject class]]) {
                view = [[ZPCaptchaRecordView alloc] initWithRecordObject:recordObject];
            } else {
                view = [[ZPRecordView alloc] init];
            }
            
            view.delegate = self;
            CGRect viewFrame = view.frame;
            viewFrame.origin.y = totalHeight;
            view.frame = viewFrame;
            totalHeight += viewFrame.size.height;
            [whiteView addSubview:view];
            [self.mySubViews addObjectNotNil:view];
            


        }
      
        CGRect whiteViewFrame = whiteView.frame;
        whiteViewFrame.size.height = totalHeight;
        whiteView.frame = whiteViewFrame;
        //[self.whiteView addTopBorderWithHeight:1.0 andColor:[UIColor zpColorFromHexString:@"#E3E6E7"]];
        [self.whiteView addBottomBorderWithHeight:1.0 andColor:[UIColor zpColorFromHexString:@"#E3E6E7"]];

        //self.autoresizesSubviews = YES;
        self.smlId = object.smlID;
        self.banknetId = object.banknetID;
        self.errorElementIdInWebView = object.errorElementIdInWebView;
        self.submitButtonId = object.smlSubmitButtonID;
        self.jsSubmitForm = object.jsSubmitForm;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setBackgroundColor:[UIColor whiteColor]];
        
    }

    return self;
}


+ (float)caculateHeightCellWithGuiObject:(ZPCellObject*) object{
    if (!object) return 0;
    
    float totalHeight = 0;
    if (object.subViews) {
        DDLogInfo(@"---caculateHeightCellWithGuiObject");
        for (ZPRecordObject* recordObject in object.subViews) {
            if ([recordObject isKindOfClass:[ZPTextFieldRecordObject class]]) {
                totalHeight += [ZPTextFieldRecordView viewHeightWithRecordObject:recordObject];
            }
            else if ([recordObject isKindOfClass:[ZPCaptchaRecordObject class]]) {
                totalHeight += [ZPCaptchaRecordView viewHeightWithRecordObject:recordObject];
                
            }
            
        }
        DDLogInfo(@"---end caculateHeightCellWithGuiObject");
    }
    return totalHeight ;
}

- (void)updateLayouts {
    for (ZPRecordView * view in self.mySubViews) {
        [view updateLayouts];
    }
    
    CGRect frame;
    CGFloat height = 0;
    for (ZPRecordView * view in self.mySubViews){
        frame = view.frame;
        frame.origin.y = height;
        view.frame = frame;
        height += view.frame.size.height;
    }
    
    
    frame = self.whiteView.frame;
    frame.size.height = height;
    frame.size.width = [ZPResourceManager getContainterWidth];
    self.whiteView.frame = frame;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    for (ZPRecordView * view in self.mySubViews){
        [view prepareForReuse];
    }
}


- (BOOL)checkValidate{
    for (ZPRecordView* view in self.mySubViews) {
        if ([view checkValidate] == NO) {
            return NO;
        }
    }
    return YES;
}


- (NSDictionary *)outputParams{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    for (ZPRecordView* view in self.mySubViews) {
        NSDictionary * params = [view outputParams];
        if (params != NULL) {
            [dict setValuesForKeysWithDictionary:params];
        }
    }
    return dict;
}

- (NSString *)jsSubmitFormSml{
    NSString * js = [NSString stringWithFormat:@"\
                    javascript:function zac%@(){\
                    var form=document.getElementsByTagName('form')[0];" , self.smlId];
    
    for (ZPRecordView* view in self.mySubViews) {
        js = [js stringByAppendingString: view.jsSubmitFormSml];
    }
    
    if (self.jsSubmitForm && ![self.jsSubmitForm isEqualToString:@""]) {
        js = [js stringByAppendingString: self.jsSubmitForm];
    } else {
        js = [js stringByAppendingString: @"form.onsubmit=null;\
                                           form.submit();"];
    }
    js = [js stringByAppendingString: [NSString stringWithFormat:@"return form.innerHtml;};\
                                       zac%@();", self.smlId]];
    return js;
    
}

- (NSString *)jsSubmitFormBanknet{
    NSString * js = [NSString stringWithFormat:@"\
                     javascript:function zac%@(){\
                     var form=document.getElementsByTagName('form')[0];" , self.smlId];
    
    for (ZPRecordView* view in self.mySubViews) {
        js = [js stringByAppendingString: view.jsSubmitFormBanknet];
    }
    
    if (self.jsSubmitForm && ![self.jsSubmitForm isEqualToString:@""]) {
        js = [js stringByAppendingString: self.jsSubmitForm];
    } else {
        js = [js stringByAppendingString: @"form.onsubmit=null;\
              form.submit();"];
    }
    js = [js stringByAppendingString: [NSString stringWithFormat:@"return form.innerHtml;};\
                                       zac%@();", self.smlId]];
    DDLogInfo(@"js: %@", js);
    return js;
}

- (NSString *)jsSubmitFormSmlWithAtmCard:(ZPAtmCard *)atmCard {
    NSString * js = [NSString stringWithFormat:@"javascript:function zacverifycardForm(){\
                     var form=document.getElementsByTagName('form')[0];\
                     form.elements['cardHolderName'].value='%@';\
                     form.elements['cardNumber'].value='%@';\
                     form.elements['cardMonth'].value='%@';\
                     form.elements['cardYear'].value='%@';\
                     form.onsubmit=null;\
                     form.submit();return form.innerHtml;};\
                     zacverifycardForm();",atmCard.cardHolderName,atmCard.cardNumber,atmCard.validTo,atmCard.validFrom];
    return js;
}

- (NSString *)jsClickSubmitButtonSml{
    NSString * js = [NSString stringWithFormat:@"\
                    javascript:function zac%@(){\
                    var form=document.forms['%@'];" , self.smlId, self.smlId];
    
    for (ZPRecordView* view in self.mySubViews) {
        js = [js stringByAppendingString: view.jsSubmitFormSml];
    }
    
    //ctl00$_Default_Content$Center$OTPTypeList
     NSString * submit = [NSString stringWithFormat:@"var combobox = form.elements['ctl00$_Default_Content$Center$OTPTypeList']; if(combobox){combobox.value = 3;}; var checkbox =  form.elements['ctl00$_Default_Content$Center$Chk_Terms']; if(checkbox) {checkbox.checked = 'true';}; form.elements['%@'].click();", self.submitButtonId];
    js = [js stringByAppendingString: [NSString stringWithFormat:@"%@};\
                                       zac%@();", submit, self.smlId]];
    
    return js;
}

- (void)setAppendString:(NSString *) str{
    for (ZPRecordView* view in self.mySubViews) {
        [view setAppendString:str];
    }
}

- (void)setCaptchaImage:(UIImage*)img{
    for (ZPRecordView* view in self.mySubViews) {
        [view setCaptchaImage:img];
    }
}

- (void)setStaticValue:(NSString *) value andDisplayText:(NSString *) name{
    for (ZPRecordView* view in self.mySubViews) {
        [view setStaticValue:value andDisplayText:name];
    }
}


- (void)setValue:(NSString *) value viewById:(NSString *)viewID{
    for (ZPRecordView* view in self.mySubViews) {
        [view setValue:value viewById:viewID];
    }
}

- (void)setEnable:(bool)enable viewById:(NSString *)viewID{
    for (ZPRecordView* view in self.mySubViews) {
        [view setEnable:enable viewById:viewID];
    }
   
}

- (void)setFormatIndex:(int)index viewId:(NSString *)viewID {
    for (ZPRecordView* view in self.mySubViews) {
        [view setFormatIndex:index viewId:viewID];
    }
}


- (void)setHighlighted:(BOOL)highlighted viewId:(NSString *)viewID {
    for (ZPRecordView* view in self.mySubViews) {
        [view setHighlighted:highlighted viewId:viewID];
    }
}

- (void)setBankName:(NSString *)value viewId:(NSString *)viewId {
    for (ZPRecordView* view in self.mySubViews) {
        [view setBankName:value viewId:viewId];
    }
}

- (void)setATMCardInfo:(ZPAtmCard *)cachedCard fromCC:(BOOL)backFromCC {
    for (ZPRecordView * view in self.mySubViews) {
        [view setATMCardInfo:cachedCard];
    }
}

- (void)setCreditCardInfo:(ZPCreditCard *)cachedCard {
    for (ZPRecordView * view in self.mySubViews) {
        [view setCreditCardInfo:cachedCard];
    }
}

- (void)setSMPinnedViewInfo:(ZPBill *)bill {
    for (ZPRecordView * view in self.mySubViews) {
        [view setSMPinnedViewInfo:bill];
    }
}

- (void)setObject:(id)object viewId:(NSString *)viewId {
    for (ZPRecordView * view in self.mySubViews) {
        [view setObject:object viewId:viewId];
    }
}

- (void)setKeyboardType:(UIKeyboardType)keyboardType viewId:(NSString *)viewId {
    for (ZPRecordView * view in self.mySubViews) {
        [view setKeyboardType:keyboardType viewId:viewId];
    }
}

- (void)setOCRVisibility:(BOOL)flag viewId:(NSString *)viewId {
    for (ZPRecordView * view in self.mySubViews) {
        [view setOCRVisibility:flag viewId:viewId];
    }
}

- (void)updateSmPinnedViewWithAmount:(long)amount {
    for (ZPRecordView * view in self.mySubViews) {
        [view updateSmPinnedViewWithAmount:amount];
    }
}

- (void)onCameraButtonClicked {
    
}

- (void) setCaptureButtonEnabled: (BOOL) enabled {
    if (captureButton){
        [captureButton setEnabled:enabled];
        captureButton.backgroundColor = enabled ? [UIColor zpLightBlueColor] : [UIColor zpButtonDisabledColor];
        betaView.image = enabled ? [ZPResourceManager getImageWithName:@"i_beta.png"] : [ZPResourceManager getImageWithName:@"i_beta_disabled.png"];
    }
}

//- (void)setOkButtonEnabled:(BOOL)enabled {
//    [okButton setEnabled:enabled];
//    okButton.backgroundColor = enabled ? [UIColor znpLightBlueColor] : [UIColor znpButtonDisabledColor];
//}


- (void)cacheInfo{
    for (ZPRecordView* v in self.mySubViews) {
        [v cacheInfo];
    }
}

- (void)loadCachedInfo{
    for (ZPRecordView* v in self.mySubViews) {
        [v loadInfoFromCache];
    }
}


#pragma mark - ZPKeyboardToolbarDelegate
- (void)onNextButtonClicked:(id)sender {
    ZPTextField * textField = (ZPTextField *)[self findFirstResponderBeneathView:self.whiteView];
    if (textField){
        
        ZPTextField * nextView = (ZPTextField *)[whiteView viewWithTag:textField.nextTag];
        if (nextView){
            
            [nextView becomeFirstResponder];
        }
    }
    
}

- (void)onPreviousButtonClicked:(id)sender {
    ZPTextField * textField = (ZPTextField *)[self findFirstResponderBeneathView:self.whiteView];
    if (textField){
        
        ZPTextField * prevView = (ZPTextField *)[whiteView viewWithTag:textField.prevTag];
        if (prevView){
            [prevView becomeFirstResponder];
        }
    }
}


- (void)onDoneButtonClicked:(id)sender {
    [self.contentView endEditing:YES];
    [self.whiteView endEditing:YES];
    for (ZPRecordView* view in self.mySubViews) {
        if ([view checkValidate] == NO) {
            return;
        }
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(validateInputDoneAndCallProcessPayment)]) {
        [self.delegate validateInputDoneAndCallProcessPayment];
    }
}


- (UIView *)findFirstResponderBeneathView:(UIView *)view {
    // Search recursively for first responder
    for ( UIView *childView in view.subviews ) {
        if ( [childView respondsToSelector:@selector(isFirstResponder)] && [childView isFirstResponder] ) return childView;
        UIView *result = [self findFirstResponderBeneathView:childView];
        if ( result ) return result;
    }
    return nil;
}
- (ZPRecordView *)viewwithId:(NSString *)viewID {
    for (ZPRecordView * view in self.mySubViews){
        if ([view.viewID isEqualToString:viewID]){
            return view;
        }
    }
    return nil;
}

#pragma mark - ZPRecordViewDelegate
- (void)recordView:(ZPRecordView *)view didReturnData:(id)data {
    if (self.delegate && [self.delegate respondsToSelector:@selector(paymentCell:didReturnData:)]) {
        [self.delegate paymentCell:self didReturnData:data];
    }
}

- (void)recordViewOnKeyboardReturnButtonClicked {
    [self.contentView endEditing:YES];
    if (self.delegate && [self.delegate respondsToSelector:@selector(recordViewOnKeyboardReturnButtonClicked)]) {
        [self.delegate recordViewOnKeyboardReturnButtonClicked];
    }
}

- (void)recordViewOnCameraButtonClicked {
}

-(void)recordViewOnKeyboardDidEndEditing {
     [self checkValidateInputData];
}

#pragma mark - UITextFieldDelgate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(recordViewOnKeyboardReturnButtonClicked)]) {
        [self.delegate recordViewOnKeyboardReturnButtonClicked];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self checkValidateInputData];
    
}


-(void)checkValidateInputData {
    for (ZPRecordView* view in self.mySubViews) {
        if ([view checkValidate] == NO) {
            DDLogInfo(@"validateInputCompleted:NO");
            if (self.delegate && [self.delegate respondsToSelector:@selector(validateInputCompleted:)]) {
                [self.delegate validateInputCompleted:NO];
            }
            return;
        }
    }
    DDLogInfo(@"validateInputCompleted:YES");
    if (self.delegate && [self.delegate respondsToSelector:@selector(validateInputCompleted:)]) {
        [self.delegate validateInputCompleted:YES];
    }
    
}
- (void)recordViewOnValueChanged:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(recordViewOnValueChanged:)]) {
        [self.delegate recordViewOnValueChanged:sender];
    }
}
-(void)showMapCardIntro:(NSNotification *)ntf{
    
}
- (void)setOTP {}

+(CGFloat)cellHeightForData:(NSObject*)data { return 0;}
-(void)setupUIWithData:(NSObject*)data{};

#pragma mark LoadFont
- (void)loadMyCustomFont {
    NSString *fontPath = [[ZPDataManager sharedInstance].bundle pathForResource:kFontNumberCardFile ofType:@"ttf"];
    if (fontPath.length == 0) {
        return;
    }
    NSData *inData = [NSData dataWithContentsOfFile:fontPath];
    if (inData == nil) {
        return;
    }
    CFErrorRef error;
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef) inData);
    CGFontRef font = CGFontCreateWithDataProvider(provider);
    // NSString *fontName = (__bridge NSString *)CGFontCopyFullName(font);
    if (!CTFontManagerRegisterGraphicsFont(font, &error)) {
        CFStringRef errorDescription = CFErrorCopyDescription(error);
        DDLogInfo(@"Failed to load font: %@", errorDescription);
        CFRelease(errorDescription);
    }
    CFRelease(font);
    CFRelease(provider);
}
@end
