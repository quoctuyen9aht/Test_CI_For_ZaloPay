//
//  ZPAtmAuthenTypeViewCell.h
//  ZaloPayWalletSDK
//
//  Created by Thi La on 6/2/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZPPaymentViewCell.h"

@interface ZPAtmAuthenTypeViewCell : ZPPaymentViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnSMS;
@property (weak, nonatomic) IBOutlet UIButton *btnToken;
@property (strong, nonatomic) NSString *bankCode;

@end
