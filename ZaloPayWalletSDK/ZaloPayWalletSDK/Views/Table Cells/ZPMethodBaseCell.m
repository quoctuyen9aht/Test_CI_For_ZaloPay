//
//  ZPMethodBaseCell.m
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/12/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPMethodBaseCell.h"
#import <ZaloPayCommon/UIView+Config.h>
#import <ZaloPayCommonSwift/ZaloPayCommonSwift-Swift.h>
#import "ZPPaymentMethod.h"
@implementation ZPMethodBaseCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupCell];
    }
    return self;
}

+ (float)height {
    return 60;
}

- (NSDictionary *)configForScreen2X {
    return @{@"check_left" : @(10),
             @"image_left" : @(44),
             @"nameToImage" : @(12),
             };
}

- (NSDictionary *)configForScreen3X {
    return @{@"check_left" : @(12),
             @"image_left" : @(48),
             @"nameToImage" : @(15),
             };
}

- (void)setData:(ZPPaymentMethodCellDisplay *)data {
    if (data.image) {
        self.methodImageView.image = data.image;
    } else {
        [self.methodImageView setIconFont:data.iconfontName];
    }
    self.lbMethodName.text = data.methodName;
    self.lbMethodInfo.text = data.feeText;
    
    if(data.type == ZPPaymentMethodCellTypeNotEnoughMoney) {
        [self setupUIWithTypeNotEnoughMoney];
    }
    
    if(data.method.isShowHintTextPromotions) {
        [self setupUIWithHintTextPromotions:data.method];
    }
    
    [self.lbMethodName mas_updateConstraints:^(MASConstraintMaker *make) {
        int topMargin = data.feeText.length == 0 || data.type == ZPPaymentMethodCellTypeNotEnoughMoney ? 20 : 10;
        make.top.equalTo(topMargin);
    }];
    int left = 0;
    if (!data.isLastCell) {
        left = [UIView isScreen2x] ? 94 : 100;
    }
    [self.line mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(left);
    }];
    self.contentView.alpha = data.alpha;
}
- (void)setupUIWithHintTextPromotions:(ZPPaymentMethod *)method {
    [self.lbMethodInfo mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(-7);
    }];
    [self.lbMethodName mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(7);
    }];
    self.lbMethodInfo.text = [R string_Endow_Having_Text];
    self.lbMethodInfo.textColor = [UIColor zaloBaseColor];
}
- (void)setupUIWithTypeNotEnoughMoney {
    [self.lbMethodInfo mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(-12);
        make.bottom.equalTo(-10);
    }];
    self.lbMethodInfo.textAlignment = NSTextAlignmentRight;
}

- (void)setupCell {
    NSDictionary *config = [UIView isScreen2x] ? [self configForScreen2X] : [self configForScreen3X];
    [self addCheckIcon:config];
    [self addMethodIcon:config];
    [self addMethodName:config];
    [self addMethodInfo];
    [self addLine];
}

- (void)addMethodIcon:(NSDictionary *)config {
    self.methodImageView = [[ZPIconFontImageView alloc] initWithFrame:CGRectMake(0, 0, 36, 36)];
    [self.methodImageView setIconColor:[UIColor subText]];
    [self.contentView addSubview:self.methodImageView];
    [self.methodImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo([config intForKey:@"image_left"]);
        make.top.equalTo(12);
        make.size.equalTo(self.methodImageView.frame.size);
    }];
}

- (void)addCheckIcon:(NSDictionary *)config {
    self.checkedImageView = [[ZPIconFontImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [self.checkedImageView setIconFont:@"general_check"];
    [self.checkedImageView setIconColor:[UIColor zaloBaseColor]];
    [self.contentView addSubview:self.checkedImageView];
    [self.checkedImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo([config intForKey:@"check_left"]);
        make.size.equalTo(self.checkedImageView.frame.size);
        make.centerY.equalTo(0);
    }];
}

- (void)addMethodName :(NSDictionary *)config{
    self.lbMethodName = [[UILabel alloc] init];
    [self.contentView addSubview:self.lbMethodName];
    [self.lbMethodName zpMainBlackRegular];
    self.lbMethodName.textColor = [UIColor defaultText];
    [self.lbMethodName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.methodImageView.mas_right).offset([config intForKey:@"nameToImage"]);
        make.height.equalTo(20);
        make.right.equalTo(-20);
        make.top.equalTo(10);
    }];
}

- (void)addMethodInfo {
    self.lbMethodInfo = [[UILabel alloc] init];
    [self.contentView addSubview:self.lbMethodInfo];
    [self.lbMethodInfo zpSubTextGrayRegular];
    [self.lbMethodInfo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.lbMethodName.mas_left);
        make.right.equalTo(-5);
        make.bottom.equalTo(-10);
    }];
    self.lbMethodInfo.adjustsFontSizeToFitWidth = YES;
}

- (void)addLine {
    self.line = [[ZPLineView alloc] init];
    [self addSubview:self.line];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(0);
        make.right.equalTo(0);
        make.height.equalTo(1);
        make.bottom.equalTo(0);
    }];
}

@end

