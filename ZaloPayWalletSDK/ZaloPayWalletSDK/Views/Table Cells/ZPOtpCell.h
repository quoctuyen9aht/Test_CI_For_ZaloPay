//
//  ZPOtpCell.h
//  ZaloPayWalletSDK
//
//  Created by Nguyen Xuan Phung on 5/27/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPPaymentViewCell.h"
#import <ZaloPayCommon/JVFloatLabeledTextField.h>

@interface ZPOtpCell : ZPPaymentViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtOtpValue;

- (void)setupKeyboardType:(NSString*)bankCode;
@end
