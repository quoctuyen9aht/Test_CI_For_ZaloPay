//
//  VoucherHistoryCell.m
//  ZaloPayWalletSDK
//
//  Created by Phuoc Huynh on 8/25/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import "ZPVoucherHistoryCell.h"
#import "ZPDefine.h"
#import "ZPBill.h"
#import "ZPPaymentChannel.h"
#import "ZPPaymentMethod.h"
#import "ZPDataManager.h"

@implementation ZPVoucherHistoryCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle =  UITableViewCellSelectionStyleNone;
        [self setupVoucherHistoryCell];
    }
    return self;
}

-(void) setupVoucherHistoryCell {
    self.lblVoucherDescription = [[UILabel alloc] init];
    [self.contentView addSubview:self.lblVoucherDescription];
    [self.lblVoucherDescription mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(12);
        make.bottom.equalTo(-14);
        make.right.equalTo(-63);
        make.height.greaterThanOrEqualTo(16);
    }];
    self.lblVoucherDescription.font = [UIFont SFUITextRegularSmall];
    self.lblVoucherDescription.textColor = [UIColor subText];
    self.lblVoucherDescription.numberOfLines = 2;
    [self.lblVoucherDescription sizeToFit];
    
    self.lblVoucherValue = [[UILabel alloc] init];
    [self.contentView addSubview:self.lblVoucherValue];
    [self.lblVoucherValue mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(10);
        make.left.equalTo(12);
        make.bottom.equalTo(self.lblVoucherDescription.mas_top).offset(-5);
    }];
    self.lblVoucherValue.font = [UIFont SFUITextMediumWithSize:26];
    self.lblVoucherValue.textColor = [UIColor blackColor];
    [self.lblVoucherValue sizeToFit];
    
    self.imgCheck = [[ZPIconFontImageView alloc] init];
    [self.contentView addSubview:self.imgCheck];
    [self.imgCheck mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(-12);
        make.centerY.equalTo(self.lblVoucherValue.mas_centerY).offset(0);
        make.width.equalTo(20);
        make.height.equalTo(self.imgCheck.mas_width).multipliedBy(1);
    }];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (void) setVoucherHistoryData:(ZPVoucherHistory *)voucherHistory withBill:(ZPBill *)bill {
    
    if (voucherHistory.valuetype != ZPVoucherValueTypePercent) {
        self.lblVoucherValue.attributedText = voucherHistory.valueDisplayTableViewCell;
    }else {
        self.lblVoucherValue.font = [UIFont SFUITextMediumWithSize:26];
        self.lblVoucherValue.text = voucherHistory.valueDisplayTableViewCell;
    }
    
    if (voucherHistory.isSelected) {
        [self.imgCheck setIconFont:@"general_check"];
        [self.imgCheck setIconColor:[UIColor zaloBaseColor]];
        self.imgCheck.defaultView.font = [UIFont iconFontWithSize:20];
    }else {
        [self.imgCheck setIconFont:@""];
    }
    
    self.contentView.alpha = voucherHistory.isDisable ? 0.3 : 1.0;
    
    int right = voucherHistory.paymentConditions.voucherConditionPPAmount != ZPVoucherConditionPPAmountCorrect ? 5 : 63;
    [self.lblVoucherDescription mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(-right);
    }];
    NSString *stringDescription = @"";
    if (bill.currentPaymentMethod) {
        switch (voucherHistory.paymentConditions.voucherConditionPPAmount) {
            case ZPVoucherConditionPPAmountCorrect:
                stringDescription = voucherHistory.voucherHistoryDescription;
                break;
            case ZPVoucherConditionPPAmountLessThanMinPPAmount:
                stringDescription = [NSString stringWithFormat:[R string_Voucher_Condition_LessThanMinPPAmount],bill.currentPaymentMethod.methodName,bill.currentPaymentMethod.channel.minPPAmount];
                break;
            case ZPVoucherConditionPPAmountGreaterThanMaxPPAmount:
                stringDescription = [NSString stringWithFormat:[R string_Voucher_Condition_GreaterThanMaxPPAmount],bill.currentPaymentMethod.methodName,bill.currentPaymentMethod.channel.maxPPAmount];
                break;
            default:
                break;
        }
    }else {
        stringDescription = [[ZPDataManager sharedInstance] messageForKey:stringKeyUnsupportAmount];
    }
    
    self.lblVoucherDescription.text = stringDescription;
}
- (void) setPromotionData:(ZPPromotion *)promotion withBill:(ZPBill *)bill {
    if (promotion.discounttype != ZPVoucherValueTypePercent) {
        self.lblVoucherValue.attributedText = promotion.valueDisplayTableViewCell;
    }else {
        self.lblVoucherValue.font = [UIFont SFUITextMediumWithSize:26];
        self.lblVoucherValue.text = promotion.valueDisplayTableViewCell;
    }
    
    if (promotion.isSelected) {
        [self.imgCheck setIconFont:@"general_check"];
        [self.imgCheck setIconColor:[UIColor zaloBaseColor]];
        self.imgCheck.defaultView.font = [UIFont iconFontWithSize:20];
    }else {
        [self.imgCheck setIconFont:@""];
    }
    self.contentView.alpha = promotion.isDisable ? 0.3 : 1.0;
    int right = promotion.paymentConditions.voucherConditionPPAmount != ZPVoucherConditionPPAmountCorrect ? 5 : 63;
    [self.lblVoucherDescription mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(-right);
    }];
    NSString *stringDescription = @"";
    if (bill.currentPaymentMethod) {
        switch (promotion.paymentConditions.voucherConditionPPAmount) {
            case ZPVoucherConditionPPAmountCorrect:
                stringDescription = promotion.promotionDescription;
                break;
            case ZPVoucherConditionPPAmountLessThanMinPPAmount:
                stringDescription = [NSString stringWithFormat:[R string_Voucher_Condition_LessThanMinPPAmount],bill.currentPaymentMethod.methodName,bill.currentPaymentMethod.channel.minPPAmount];
                break;
            case ZPVoucherConditionPPAmountGreaterThanMaxPPAmount:
                stringDescription = [NSString stringWithFormat:[R string_Voucher_Condition_GreaterThanMaxPPAmount],bill.currentPaymentMethod.methodName,bill.currentPaymentMethod.channel.maxPPAmount];
                break;
            default:
                break;
        }
    }else {
        stringDescription = [[ZPDataManager sharedInstance] messageForKey:stringKeyUnsupportAmount];
    }
    self.lblVoucherDescription.text = stringDescription;
}

@end
