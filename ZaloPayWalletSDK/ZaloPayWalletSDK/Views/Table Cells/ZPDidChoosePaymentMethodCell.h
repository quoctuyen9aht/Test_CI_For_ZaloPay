//
//  ZPDidChoosePaymentMethodCell.h
//  ZaloPayWalletSDK
//
//  Created by Thi La on 6/13/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPPaymentViewCell.h"
@interface ZPDidChoosePaymentMethodCell : ZPPaymentViewCell

@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *bankIcon;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *bankName;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *title;

@end
