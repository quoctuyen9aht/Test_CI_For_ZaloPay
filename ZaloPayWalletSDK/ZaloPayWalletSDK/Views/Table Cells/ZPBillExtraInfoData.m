//
//  ZPBillExtraInfoData.m
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/12/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPBillExtraInfoData.h"

@implementation ZPBillExtraInfoData
- (id)initWithTitle:(NSString *)title value:(NSString *)value {
    return [self initWithTitle:title value:value type:ZPBillExtraInfoType_Info];
}

- (id)initWithTitle:(NSString *)title value:(NSString *)value type:(ZPBillExtraInfoType)type {
    self = [super init];
    if (self) {
        self.title = title;
        self.value = value;
        self.type = type;
    }
    return self;
}
@end
