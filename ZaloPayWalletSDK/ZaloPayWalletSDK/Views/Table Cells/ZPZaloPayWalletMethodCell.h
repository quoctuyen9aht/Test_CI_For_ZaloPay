//
//  ZPZaloPayWalletMethodCell.h
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/12/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPMethodBaseCell.h"

@interface ZPZaloPayWalletMethodCell : ZPMethodBaseCell
@property (nonatomic, strong) UILabel *labelBallance;
@end
