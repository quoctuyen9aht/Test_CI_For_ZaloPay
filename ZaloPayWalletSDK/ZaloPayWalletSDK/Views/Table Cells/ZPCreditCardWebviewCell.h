//
//  ZPCreditCardWebviewCell.h
//  ZaloPayWalletSDK
//
//  Created by Nguyen Xuan Phung on 5/7/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPPaymentViewCell.h"

@interface ZPCreditCardWebviewCell : ZPPaymentViewCell
@property (weak, nonatomic) IBOutlet UIWebView *ccWebview;

@end
