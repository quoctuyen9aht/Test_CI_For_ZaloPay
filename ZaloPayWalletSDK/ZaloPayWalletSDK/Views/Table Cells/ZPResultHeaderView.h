//
//  ZPResultHeaderView.h
//  ZaloPayWalletSDK
//
//  Created by Thi La on 4/26/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZPResultRowView.h"

@interface ZPResultHeaderView : ZPResultRowView
@property (weak, nonatomic) IBOutlet ZPIconFontImageView *resultImage;
@property (weak, nonatomic) IBOutlet UILabel *resultMessage;

@property (weak, nonatomic) IBOutlet UILabel *itemName;
@property (weak, nonatomic) IBOutlet UILabel *itemPrice;

@property (weak, nonatomic) IBOutlet UILabel *errorMessageLabel;
@end
