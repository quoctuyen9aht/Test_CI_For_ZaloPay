//
//  ZPOptionCell.h
//  ZaloPayWalletSDK
//
//  Created by Thi La on 6/9/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPPaymentViewCell.h"

@interface ZPOptionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *optionLabel;

@end
