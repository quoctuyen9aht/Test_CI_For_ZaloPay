//
//  ZPMethodPadingCell.m
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/15/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPMethodPadingCell.h"
#import <ZaloPayCommonSwift/ZaloPayCommonSwift-Swift.h>

@implementation ZPMethodPadingData
@end

@implementation ZPMethodPadingCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle =  UITableViewCellSelectionStyleNone;
        [self addLine];
    }
    return self;
}

+ (float)height {
    return 12;
}

- (void)addLine {
    ZPLineView *line = [[ZPLineView alloc] init];
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@12);
        make.right.equalTo(@-12);
        make.height.equalTo(@1);
        make.bottom.equalTo(@0);
    }];
}

@end
