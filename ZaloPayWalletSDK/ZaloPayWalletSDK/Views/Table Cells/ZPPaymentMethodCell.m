//
//  ZPPaymentMethodCell.m
//  ZaloPayWalletSDK
//
//  Created by Thi La on 4/23/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZPPaymentMethodCell.h"
#import "ZPResourceManager.h"
#import "ZPPaymentMethodCellDisplay.h"
#import "ZPPaymentMethod.h"
#import "ZaloPayWalletSDKLog.h"

@implementation ZPPaymentMethodCell


- (void)setupDisplayWith:(ZPPaymentMethod *)method withAmount:(long)amount
{
    self.methodIcon.image = [ZPResourceManager getImageWithName:method.methodIcon];
    self.methodName.text = method.methodName;
    DDLogInfo(@"method.userPLPAllow %d", method.userPLPAllow);
    ZPPaymentMethodCellDisplay *display = [[ZPPaymentMethodCellDisplay alloc] initWith:method withAmount:amount];
    self.backgroundColor = display.backgroundColor;
    self.feeLabel.textColor = display.feeTextColor;
    self.feeLabel.text = display.feeText;
    self.contentView.alpha = display.alpha;
    self.arrowIcon.hidden = display.isHiddenArrow;
}


@end


