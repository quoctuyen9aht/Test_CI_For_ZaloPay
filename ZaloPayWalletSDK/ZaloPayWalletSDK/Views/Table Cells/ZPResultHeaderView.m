//
//  ZPResultHeaderView.m
//  ZaloPayWalletSDK
//
//  Created by Thi La on 4/26/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPResultHeaderView.h"
#import "ZPDataManager.h"
#import "ZPResourceManager.h"
#import "NSString+ZPExtension.h"
#import <ZaloPayWalletSDK/ZaloPayWalletSDK-Swift.h>
#import "ZPPaymentResponse.h"
#import "ZPResultViewModel.h"
#import "ZPBill.h"

@interface ZPResultHeaderView()
@property (assign, nonatomic) BOOL needUpdateUI;
@end

@implementation ZPResultHeaderView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.contentView setBackgroundColor:[UIColor whiteColor]];
    [self.itemPrice zpMainGrayItalic];
    self.itemPrice.numberOfLines = 0;
    [self setupUI];
}

- (void)setupUI {
    self.resultMessage.text = @"";
    self.itemName.text = @"";
    self.errorMessageLabel.text = @"";
    self.itemPrice.text = @"";
}

- (void)setModel:(ZPResultViewModel *)model {
    [super setModel:model];

    ZPResultHeaderViewModel *headerModel = (ZPResultHeaderViewModel *)model;
    if (headerModel.isPaymentSuccess) {
        [self setupUIWithSuccessResponse:headerModel];
    } else {
        [self setupUIWithFailResponse:headerModel.bill response:headerModel.paymentResponse];
    }    
    //[self setupUIWithSuccessResponse:headerModel];
}

- (void)setupUIWithSuccessResponse:(ZPResultHeaderViewModel *)model {
    if (self.needUpdateUI) {
        [self setupUI];
        [self updateUIAfterReceivedNotify];
    }
    
    self.errorMessageLabel.hidden = YES;
    if ([self.resultImage isKindOfClass:[ZPIconFontImageView class]]) {
        [self.resultImage setIconFont:@"pay_success"];
        [self.resultImage setIconColor:[UIColor payColor]];
    }
    //set attribute text
    if (model.bill.transType != ZPTransTypeAtmMapCard) {
        self.itemPrice.textColor = [UIColor defaultText];
        NSString *moneyTitle = [@(model.amount) formatCurrency];
        self.itemPrice.numberOfLines = 1;
        self.itemPrice.adjustsFontSizeToFitWidth = YES;
        self.itemPrice.attributedText = [moneyTitle formatCurrencyFont:[UIFont SFUITextMediumWithAmount:model.amount]
                                                                 color:[UIColor defaultText]
                                                               vndFont:[UIFont SFUITextRegularWithSize:14]
                                                              vndcolor:[UIColor subText]
                                                             alignment:NSTextAlignmentCenter];
        [self.itemName zpMainGrayItalic];
        self.itemName.textColor = [UIColor subText];
        
        if(model.bill.appId == topupPhoneV2AppId) {
            self.itemName.text = [R string_Topup_phonev2_message];
            [[ZPAppFactory sharedInstance].orderTracking trackOrderDisplay:model.bill.appTransID errorCode:ZALOPAY_ERRORCODE_SUCCESSFUL paymentStatus:OrderResultDisplay_Success message:[R string_Topup_phonev2_message]];
            return;
        }
        
        if (model.bill.billDescription.length > 0) {
            self.itemName.text = model.bill.billDescription;
            [[ZPAppFactory sharedInstance].orderTracking trackOrderDisplay:model.bill.appTransID errorCode:ZALOPAY_ERRORCODE_SUCCESSFUL paymentStatus:OrderResultDisplay_Success message:model.bill.billDescription];
        }
    }
    else {
        if ([model.messageCustomAfterSuccess length] > 0) {
            self.resultMessage.attributedText = [NSString attributedString:model.messageCustomAfterSuccess];
            self.resultMessage.textColor = [UIColor subText];
            self.resultMessage.numberOfLines = 0;
            self.resultMessage.lineBreakMode = NSLineBreakByWordWrapping;
            [self.resultMessage zpMainGrayRegular];
        }
    }
}

- (void)updateUI {
    [self.contentView.constraints enumerateObjectsUsingBlock:^(__kindof NSLayoutConstraint * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.constant == 34) {
            obj.constant = -10;
        }
        if (obj.constant == 30) {
            obj.constant = 10;
        }
    }];
}

- (void)updateUIAfterReceivedNotify {
    [self.contentView.constraints enumerateObjectsUsingBlock:^(__kindof NSLayoutConstraint * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.constant == -10) {
            obj.constant = 34;
        }
    }];
}

- (void)setupUIWithFailResponse:(ZPBill*)bill response:(ZPPaymentResponse*)response  {
    self.needUpdateUI = YES;
    
    if (response == nil || response.errorCode == kZPZaloPayCreditErrorCodeRequestTimeout) {
        self.resultImage.image = [ZPResourceManager getImageWithName:@"no_internet"];
        self.itemPrice.attributedText = [NSString attributedString:stringTypeErrorMessageNetworkError];
        [[ZPAppFactory sharedInstance].orderTracking trackOrderDisplay:bill.appTransID errorCode:kZPZaloPayCreditErrorCodeGettingResourceFailed paymentStatus:OrderResultDisplay_NetworkError message:stringTypeErrorMessageNetworkError];
        [self updateUI];
        return;
    }
    
    if (response.exchangeStatus == kZPZaloPayCreditStatusCodeUnidentified) {
        self.resultImage.image = [ZPResourceManager getImageWithName:@"loading"];
        self.itemPrice.attributedText = [NSString attributedString:stringTypeErrorMessageUnidentified];
        [[ZPAppFactory sharedInstance].orderTracking trackOrderDisplay:bill.appTransID errorCode:response.originalCode paymentStatus:OrderResultDisplay_Processing message:stringTypeErrorMessageUnidentified];
        
        [self updateUI];
        return;
    }
    
    if ([self.resultImage  isKindOfClass:[ZPIconFontImageView class]]) {
        [self.resultImage setIconFont:@"pay_fail"];
        [self.resultImage setIconColor:[UIColor errorColor]];
    }
    
    if (response.message.length > 0) {
        self.itemPrice.attributedText = [NSString attributedString:response.message];
    }
    [[ZPAppFactory sharedInstance].orderTracking trackOrderDisplay:bill.appTransID errorCode:response.originalCode paymentStatus:OrderResultDisplay_Processing message:response.message];
    
    if (response.suggestMessage.length > 0 ) {
        self.errorMessageLabel.attributedText = [NSString attributedString:response.suggestMessage];
        self.errorMessageLabel.textColor = [UIColor subText];
        self.errorMessageLabel.numberOfLines = 0;
        [self.errorMessageLabel zpMainGrayItalic];
    } else {
        [self updateUI];
    }
}

@end
