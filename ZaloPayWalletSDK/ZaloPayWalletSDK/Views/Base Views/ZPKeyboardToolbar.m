//
//  ZPKeyboardToolbar.m
//  ZaloSDK.Payment
//
//  Created by Hoang Nguyen on 2/2/15.
//  Copyright (c) 2015-present VNG Corporation All rights reserved.
//

#import "ZPKeyboardToolbar.h"
#import "ZPDataManager.h"
#import "ZPResourceManager.h"
#import "ZPConfig.h"
#import "UIColor+ZPExtension.h"

@implementation ZPKeyboardToolbar


+ (instancetype)keyboardToolbar {
    ZPKeyboardToolbar * toolbar = [[ZPKeyboardToolbar alloc] init];
    [toolbar.nextBtn setImage:[ZPResourceManager nextIcon] forState:UIControlStateNormal];
    [toolbar.prevBtn setImage:[ZPResourceManager backIcon] forState:UIControlStateNormal];
    [toolbar.nextBtn setImage:[ZPResourceManager nextIconDisabled] forState:UIControlStateDisabled];
    [toolbar.prevBtn setImage:[ZPResourceManager backIconDisabled] forState:UIControlStateDisabled];
    //[toolbar.cameraBtn setTitle:[[ZPDataManager sharedInstance] messageForKey:@"btn_camera"] forState:UIControlStateNormal];
    [toolbar.doneBtn setTitleColor:[UIColor zpColorFromHexString:@"#008FE5"] forState:UIControlStateNormal];
    toolbar.frame = CGRectMake(toolbar.frame.origin.x, toolbar.frame.origin.y, toolbar.frame.size.width, IS_IPAD ? 60 : 40);
    toolbar.backgroundColor = [UIColor colorWithWhite:1 alpha:0.9];
    return toolbar;
}

- (instancetype)init {
    self = (ZPKeyboardToolbar *) [[ZPDataManager sharedInstance] viewWithNibName:@"ZPKeyboardToolbar" owner:self];
    return self;
}

- (IBAction)onNextBtnClicked:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(onNextButtonClicked:)]){
        [self.delegate onNextButtonClicked:sender];
    }
}

- (IBAction)onPrevBtnClicked:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(onPreviousButtonClicked:)]){
        [self.delegate onPreviousButtonClicked:sender];
    }
}

- (IBAction)onCameraBtnClicked:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(onCameraButtonClicked:)]){
        [self.delegate onCameraButtonClicked:sender];
    }
}

- (IBAction)onDoneBtnClicked:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(onDoneButtonClicked:)]){
        [self.delegate onDoneButtonClicked:sender];
    }
}

- (BOOL)validateOption:(int)option {
    BOOL isNavigationEnabled =  (option & 1 << 0) != 0;
    BOOL isCameraEnabled =      (option & 1 << 1) != 0;
    BOOL isDoneEnabled =        (option & 1 << 2) != 0;
    
    self.nextBtn.hidden = self.prevBtn.hidden = !isNavigationEnabled;
    self.cameraBtn.hidden = !isCameraEnabled;
    self.doneBtn.hidden = !isDoneEnabled;
    
    return (isNavigationEnabled || isCameraEnabled || isDoneEnabled);
}

@end
