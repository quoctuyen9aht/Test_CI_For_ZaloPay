//
//  ZPKeyboardToolbar.h
//  ZaloSDK.Payment
//
//  Created by Hoang Nguyen on 2/2/15.
//  Copyright (c) 2015-present VNG Corporation All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ZPKeyboardToolbarDelegate;


@interface ZPKeyboardToolbar : UIView

@property (strong, nonatomic) IBOutlet UIButton * nextBtn;
@property (strong, nonatomic) IBOutlet UIButton * prevBtn;
@property (strong, nonatomic) IBOutlet UIButton * cameraBtn;
@property (strong, nonatomic) IBOutlet UIButton * doneBtn;
@property (weak, nonatomic) id<ZPKeyboardToolbarDelegate> delegate;

+ (instancetype)keyboardToolbar;
- (BOOL)validateOption:(int)option;

@end


@protocol ZPKeyboardToolbarDelegate <NSObject>
@optional
- (void)onNextButtonClicked:(id)sender;
- (void)onPreviousButtonClicked:(id)sender;
- (void)onCameraButtonClicked:(id)sender;
- (void)onDoneButtonClicked:(id)sender;
@end
