//
//  ZPInsetLabel.h
//  ZaloSDK.Payment
//
//  Created by Liem Vo Uy on 7/1/15.
//  Copyright (c) 2015-present VNG Corporation All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface ZPInsetLabel : UILabel
@property (nonatomic) UIEdgeInsets insets;
@end
