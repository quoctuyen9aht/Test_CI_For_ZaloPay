//
//  ZPMonthYearPicker.m
//  ZaloPaymentSDK
//
//  Created by hoangnx2 on 1/9/14.
//  Copyright (c) 2014 VNG Corporation. All rights reserved.
//

#import "ZPMonthYearPicker.h"

@implementation ZPMonthYearPicker
@synthesize months, years, pickerDelegate;
- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initDateData];
        self.dataSource = self;
        self.delegate = self;
        self.showsSelectionIndicator = true;
    }
    return self;
}
- (void)initDateData{
    months = [[NSMutableArray alloc] init];
    for (int i = 1; i < 13; i++) {
        [months addObjectNotNil:[NSNumber numberWithInteger:i]];
    }
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];

    int currentYear = (int) components.year;
    
    years = [[NSMutableArray alloc] init];
    for (int i = -15; i < 16; i++) {
        int tempYear = ((int)(currentYear % 100)) + i;
        if (tempYear >= 0 && tempYear < 100) {
            [years addObjectNotNil:[NSNumber numberWithInt:tempYear]];
        }
        
    }
}
- (void)setCurrentDate{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    
    int currentYear = ((int)(components.year % 100));
    int currentMonth = (int) components.month;
  
    if (pickerDelegate != NULL && [pickerDelegate respondsToSelector:@selector(onChangeMonth:year:)]) {
        [pickerDelegate onChangeMonth:currentMonth year:currentYear];
    }
    
    int monthIndex = 15;
    int yearIndex = 15;
    for (int i = 0; i < months.count; i++) {
        int m = [[months objectAtIndex:i] intValue];
        if (m == currentMonth) {
            monthIndex = i;
            break;
        }
    }
    
    for (int i = 0; i < years.count; i++) {
        int y = [[years objectAtIndex:i] intValue];
        if (y == currentYear) {
            yearIndex = i;
            break;
        }
    }
    [self selectRow:monthIndex inComponent:0 animated:false];
    [self selectRow:yearIndex inComponent:1 animated:false];
    

}
- (void)setSelectedMonth:(int) month year:(int) year{
    int monthIndex = 15;
    int yearIndex = 15;
    for (int i = 0; i < months.count; i++) {
        int m = [[months objectAtIndex:i] intValue];
        if (m == month) {
            monthIndex = i;
            break;
        }
    }
    
    for (int i = 0; i < years.count; i++) {
        int y = [[years objectAtIndex:i] intValue];
        if (y == year) {
            yearIndex = i;
            break;
        }
    }
    [self selectRow:monthIndex inComponent:0 animated:false];
    [self selectRow:yearIndex inComponent:1 animated:false];
 
    
     int cMonth = [[months objectAtIndex:monthIndex] intValue];
    
     int cYear = [[years objectAtIndex:yearIndex] intValue];
   
    
    if (pickerDelegate != NULL && [pickerDelegate respondsToSelector:@selector(onChangeMonth:year:)]) {
        [pickerDelegate onChangeMonth:cMonth year:cYear];
    }
}

#pragma mark - UIPickerViewDelegate
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (component == 0) {
        return 12;
    }
    if (component == 1) {
        return 30;
    }
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSString * text = @"";
    if (component == 0) {
        text = [NSString stringWithFormat:@"Tháng %02d", [[months objectAtIndex:row] intValue]];
    } else{
        text = [NSString stringWithFormat:@"Năm 20%02d", [[years objectAtIndex:row] intValue]];
    }
    return text;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    int month = [[months objectAtIndex:[pickerView selectedRowInComponent:0]] intValue];
    int year = [[years objectAtIndex:[pickerView selectedRowInComponent:1]] intValue];

    if (component == 0) {
        month = [[months objectAtIndex:row] intValue];
    }
    if (component == 1) {
        year = [[years objectAtIndex:row] intValue];
    }

    if (pickerDelegate != NULL && [pickerDelegate respondsToSelector:@selector(onChangeMonth:year:)]) {
        [pickerDelegate onChangeMonth:month year:year];
    }
}
@end
