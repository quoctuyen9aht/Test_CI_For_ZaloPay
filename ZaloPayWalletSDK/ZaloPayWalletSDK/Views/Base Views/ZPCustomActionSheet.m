//
//  ZPCustomActionSheet.m
//  ZaloPayWalletSDK
//
//  Created by thi la on 12/30/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPCustomActionSheet.h"
#import "ZPConfig.h"
#import "UIColor+ZPExtension.h"
#import "ZPResourceManager.h"

@interface ZPCustomActionSheet()
@property (strong, nonatomic) RACSubject<NSNumber *> *eCompleted;
@end
@implementation ZPCustomActionSheet

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        //self.backgroundColor = [UIColor colorWithWhite:0.1 alpha:0.8];
        self.eCompleted = [RACSubject new];
        [self setupLayout];
    }
    return self;
}

- (void)setupLayout {
    UIView *actionSheetView = [[UIView alloc] initWithFrame: CGRectMake(PADDING, self.frame.size.height - 163, SCREEN_WIDTH - (PADDING * 2), 101)];
    actionSheetView.layer.masksToBounds = YES;
    actionSheetView.layer.cornerRadius = 6;
    actionSheetView.backgroundColor = [UIColor whiteColor];
    
    UIButton *faqButton = [[UIButton alloc] initWithFrame:CGRectMake(PADDING, 0, actionSheetView.frame.size.width, 50)];
    [faqButton setTitle:@"Câu Hỏi Thường Gặp" forState:UIControlStateNormal];
    faqButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [faqButton addTarget:self action:@selector(btnFaqClicked) forControlEvents:UIControlEventTouchUpInside];
    faqButton.titleLabel.font = [UIFont systemFontOfSize:16];
    [faqButton setTitleColor:[UIColor zpColorFromHexString:@"#24272B"] forState:UIControlStateNormal];
    UIImageView *faqimgView = [[UIImageView alloc] initWithFrame:CGRectMake(actionSheetView.frame.size.width - 42, 10, 32, 30)];
    faqimgView.image = [ZPResourceManager getImageWithName:@"faq.png"];
    
    UIView *line = [[UIView alloc] initWithFrame: CGRectMake(0, 50, actionSheetView.frame.size.width, 1)];
    line.backgroundColor = [UIColor zpColorFromHexString:@"#E3E6E7"];
    
    UIButton *supportButton = [[UIButton alloc] initWithFrame:CGRectMake(PADDING, 51, actionSheetView.frame.size.width, 50)];
    [supportButton setTitle:@"Gửi Báo Lỗi" forState:UIControlStateNormal];
    supportButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [supportButton addTarget:self action:@selector(btnSupportClicked) forControlEvents:UIControlEventTouchUpInside];
    supportButton.titleLabel.font = [UIFont systemFontOfSize:16];
    [supportButton setTitleColor:[UIColor zpColorFromHexString:@"#24272B"] forState:UIControlStateNormal];
    UIImageView *supportimgView = [[UIImageView alloc] initWithFrame:CGRectMake(actionSheetView.frame.size.width - 42, 63.5, 32, 25)];
    supportimgView.image = [ZPResourceManager getImageWithName:@"baoloi.png"];
    
    UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(PADDING, self.frame.size.height - 53, actionSheetView.frame.size.width, 50)];
    [cancelButton setTitle:@"Hủy" forState:UIControlStateNormal];
    cancelButton.layer.masksToBounds = YES;
    cancelButton.layer.cornerRadius = 6;
    cancelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [cancelButton addTarget:self action:@selector(btnCancelClicked) forControlEvents:UIControlEventTouchUpInside];
    cancelButton.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    cancelButton.backgroundColor = [UIColor whiteColor];
    [cancelButton setTitleColor:[UIColor zpColorFromHexString:@"#018FE5"] forState:UIControlStateNormal];
    
    [actionSheetView addSubview:faqButton];
    [actionSheetView addSubview:line];
    [actionSheetView addSubview:supportButton];
    [actionSheetView addSubview:faqimgView];
    [actionSheetView addSubview:supportimgView];

    
    [self addSubview:cancelButton];
    [self addSubview:actionSheetView];
//    NSArray* windows = [UIApplication sharedApplication].windows;
//    UIView *window = [windows firstObject];
//    [window addSubview:self];

}

- (void)btnFaqClicked {
    [self.eCompleted sendNext:@(ZPCustomActionSheetTypeFAQ)];
    [self.eCompleted sendCompleted];
    if (self.delegate && [self.delegate respondsToSelector:@selector(sheetDidChooseFAQ:)]) {
        [self.delegate sheetDidChooseFAQ:self];
    }
    
}

- (void)btnSupportClicked {
    
    [self.eCompleted sendNext:@(ZPCustomActionSheetTypeNeedSupport)];
    [self.eCompleted sendCompleted];
    if (self.delegate && [self.delegate respondsToSelector:@selector(sheetDidChooseNeedSupport:)]) {
        [self.delegate sheetDidChooseNeedSupport:self];
    }
}

- (void)btnCancelClicked {
    [self.eCompleted sendNext:@(ZPCustomActionSheetTypeCancel)];
    [self.eCompleted sendCompleted];
    if (self.delegate && [self.delegate respondsToSelector:@selector(sheetDidChooseCancel:)]) {
        [self.delegate sheetDidChooseCancel:self];
    }
}

+ (RACSignal <NSNumber *> *)show {
    if (![UIApplication sharedApplication].keyWindow) {
        return [RACSignal empty];
    }
    // bg
    UIView *window = [UIApplication sharedApplication].keyWindow;
    UIView *bgView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    bgView.backgroundColor = [UIColor blackColor];
    bgView.alpha = 0;
    [window addSubview:bgView];
    // action sheet
    ZPCustomActionSheet *actionSheet = [[ZPCustomActionSheet alloc] initWithFrame:window.bounds];
    [window addSubview:actionSheet];
    actionSheet.transform = CGAffineTransformMakeTranslation(0, 400);
   
    return [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
        // show
        [actionSheet.eCompleted subscribe:subscriber];
        [UIView animateWithDuration:0.3 animations:^{
            actionSheet.transform = CGAffineTransformIdentity;
            bgView.alpha = 0.6;
        }];
        return [RACDisposable disposableWithBlock:^{
            [UIView animateWithDuration:0.3 animations:^{
                actionSheet.transform = CGAffineTransformMakeTranslation(0, 400);
                bgView.alpha = 0;
            } completion:^(BOOL finished) {
                [actionSheet removeFromSuperview];
                [bgView removeFromSuperview];
            }];
        }];
        
    }];
}

@end
