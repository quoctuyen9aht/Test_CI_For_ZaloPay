//
//  ZPMonthYearPicker.h
//  ZaloPaymentSDK
//
//  Created by hoangnx2 on 1/9/14.
//  Copyright (c) 2014 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ZPMonthYearPickerDelegate<NSObject>
@required
- (void)onChangeMonth:(int) month year:(int) year;
@end


@interface ZPMonthYearPicker : UIPickerView<UIPickerViewDataSource, UIPickerViewDelegate>

@property (strong, nonatomic) NSMutableArray * months;
@property (strong, nonatomic) NSMutableArray * years;
@property id<ZPMonthYearPickerDelegate> pickerDelegate;

- (void)setSelectedMonth:(int)month year:(int)year;
- (void)setCurrentDate;

@end
