//
//  ZPTextField.m
//  ZaloPaymentSDK
//
//  Created by hoangnx2 on 12/26/13.
//  Copyright (c) 2013 VNG Corporation. All rights reserved.
//

#import "ZPTextField.h"
#import "UIColor+ZPExtension.h"
#import "ZPResourceManager.h"

@interface ZPTextField()
@property (strong, nonatomic) UIImageView* warningView;
@property (strong, nonatomic) UIButton * nextButton;
@end


@implementation ZPTextField
@synthesize warningView;

- (id)init{
    self = [super init];
    if (self != NULL) {
        [self initStyle];
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initStyle];
    }
    return self;
}
- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initStyle];
    }
    return self;
}
- (void)initStyle{
    [self setClearButtonMode:UITextFieldViewModeWhileEditing];
    [self setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
    [self setBorderStyle:UITextBorderStyleNone];
    [self setLeftView:[[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)]];
    [self setLeftViewMode:UITextFieldViewModeAlways];
    
    warningView = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width - 21, self.frame.size.height / 2 - 7, 15, 14)];
    [self addSubview:warningView];
    [warningView setAutoresizingMask:UIViewAutoresizingFlexibleRightMargin];
    
    [self setValidState:TRUE];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    CGRect warningViewFrame = CGRectMake(self.frame.size.width - 21, self.frame.size.height / 2 - 7, 15, 14);
    //CGRect rectMain = self.frame;
    [warningView setFrame:warningViewFrame];
}
- (void)setValidState:(bool)isValid{
    if (isValid) {
        [warningView setHidden:TRUE];
    } else{
        [warningView setHidden:false];
    }
}


@end
