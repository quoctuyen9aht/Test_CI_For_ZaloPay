//
//  ZPInsetLabel.m
//  ZaloSDK.Payment
//
//  Created by Liem Vo Uy on 7/1/15.
//  Copyright (c) 2015-present VNG Corporation All rights reserved.
//

#import "ZPInsetLabel.h"

@implementation ZPInsetLabel
- (instancetype)init{
    self = [super init];
    if (self) {
        _insets = UIEdgeInsetsMake(0, 8, 0, 8);
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _insets = UIEdgeInsetsMake(0, 8, 0, 8);
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        _insets = UIEdgeInsetsMake(0, 8, 0, 8);
    }
    return self;
}

- (void)setInsets:(UIEdgeInsets)insets{
    _insets = insets;
    [self setNeedsDisplay];
}

- (void) drawTextInRect:(CGRect)rect
{
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, self.insets)];
}

@end
