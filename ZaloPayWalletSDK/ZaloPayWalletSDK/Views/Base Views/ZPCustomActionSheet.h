//
//  ZPCustomActionSheet.h
//  ZaloPayWalletSDK
//
//  Created by thi la on 12/30/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ZPCustomActionSheet;

@protocol ChooseSupportActionSheetDelegate <NSObject>
- (void)sheetDidChooseCancel:(ZPCustomActionSheet *)sheet;
- (void)sheetDidChooseFAQ:(ZPCustomActionSheet *)sheet;
- (void)sheetDidChooseNeedSupport:(ZPCustomActionSheet *)sheet;
@end

typedef NS_ENUM(NSInteger, ZPCustomActionSheetType){
    ZPCustomActionSheetTypeCancel,
    ZPCustomActionSheetTypeFAQ,
    ZPCustomActionSheetTypeNeedSupport
};
@interface ZPCustomActionSheet : UIView
@property (nonatomic, weak) id <ChooseSupportActionSheetDelegate> delegate;
+ (RACSignal <NSNumber *> *)show;
@end
