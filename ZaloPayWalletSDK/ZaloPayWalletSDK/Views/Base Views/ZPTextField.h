//
//  ZPTextField.h
//  ZaloPaymentSDK
//
//  Created by hoangnx2 on 12/26/13.
//  Copyright (c) 2013 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ZaloPayCommon/JVFloatLabeledTextField.h>


@interface ZPTextField : JVFloatLabeledTextField

@property (assign, nonatomic) int nextTag;
@property (assign, nonatomic) int prevTag;

- (void)setValidState:(bool)isValid;

@end
