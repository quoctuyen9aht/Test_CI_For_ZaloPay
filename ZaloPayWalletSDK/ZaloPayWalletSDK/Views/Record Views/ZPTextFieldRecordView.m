//
//  ZPTextFieldRecordView.m
//  ZaloPaymentSDK
//
//  Created by hoangnx2 on 1/13/14.
//  Copyright (c) 2014 VNG Corporation. All rights reserved.
//

#import "ZPTextFieldRecordView.h"
#import "ZPTextFieldRecordObject.h"
#import "ZPResourceManager.h"
#import "ZPConfig.h"
#import "ZPDataManager.h"
#import "NSString+ZPExtension.h"
#import "ZPPaymentViewCell.h"
#import "UIColor+ZPExtension.h"
#import "ZPPaymentChannel.h"
#import "ZPTextField.h"
#import "ZPTextFieldRecordObject.h"
#import "ZPKeyboardToolbar.h"
#import "NSString+ZPExtension.h"
#import "ZaloPayWalletSDKLog.h"

#define BankNameView_W  100


@interface ZPTextFieldRecordView() {
    BOOL textfieldHighlighted;
    UIView * bankNameView;
    UITextField * bankNameTextField;
}
@property (strong, nonatomic) ZPTextFieldRecordObject * rObject;
@property (strong, nonatomic) UIButton * cameraBtn;
@property (strong, nonatomic) UIView *line;
@property (strong, nonatomic) UIView *bottomLine;


@end


@implementation ZPTextFieldRecordView
@synthesize rObject;

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithRecordObject:(ZPRecordObject *)object {
    
    rObject = (ZPTextFieldRecordObject *)object;
    
    CGRect frame = CGRectMake(0,0,[ZPTextFieldRecordView viewWidth],
                              [ZPTextFieldRecordView viewHeightWithRecordObject:object]);
    
    self = [super initWithFrame:frame];
    DDLogInfo(@"self.frame width, height origin x, y %f, %f, %f, %f", self.frame.size.width, self.frame.size.height,
          self.frame.origin.x, self.frame.origin.y);
    if (self) {
        //init
        
        [self initViewWithGuiObject:rObject];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(textFieldDidChanged)
                                                     name:UITextFieldTextDidChangeNotification
                                                   object:nil];
        
        _textField.borderStyle = UITextBorderStyleNone;
        [_textField setBackgroundColor:[UIColor clearColor]];
        _textField.keyboardType = rObject.keyboardType;
        _cacheParam = rObject.cacheParam;
        _textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(rObject.placeHolder, @"") attributes:@{}];         [_textField setSecureTextEntry:rObject.isPassword];
        _requiredFillMessage = rObject.requiredFillMessage;
        _smlID = rObject.smlID;
        _banknetID = rObject.banknetID;
        _invalidErrorCode = rObject.invalidErrorCode;
        _param = rObject.param;
        self.viewID = rObject.objectID;
        
        if (rObject.isCapLocksOn){
            [_textField setAutocapitalizationType:UITextAutocapitalizationTypeAllCharacters];
        } else {
            [_textField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
        }
        [_textField setReturnKeyType:UIReturnKeyGo];
        _textField.tag = rObject.tag;
        _textField.nextTag = rObject.nextTag;
        _textField.prevTag = rObject.prevTag;
        
        self.keyboardToolbar.prevBtn.enabled = _textField.prevTag > 0 ? YES : NO;
        self.keyboardToolbar.nextBtn.enabled = _textField.nextTag > 0 ? YES : NO;
        
        if (rObject.isDisabled){
            _textField.enabled = NO;
            [self renderTextField];
        }
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}


- (void)textFieldDidChanged {
    if ([self.viewID isEqualToString:@"chargeAmountTextField"]  || [self.viewID isEqualToString:@"zaloCoinInput"]){
        _textField.text = [_textField.text zpFormatNumberWithDilimeter];
    } else {
        
    }
}

- (void)initViewWithGuiObject:(ZPTextFieldRecordObject*) object{
    self.formatIndex = 0;
    [self setBackgroundColor:[UIColor clearColor]];
    
    //init textField
    _textField = [[ZPTextField alloc] initWithFrame:CGRectMake(PADDING, 0 , [ZPResourceManager getContainterWidth] - 2*PADDING , [ZPResourceManager getTextFieldHeight])];
    
    _textField.delegate = self;
    _textField.autocorrectionType = UITextAutocorrectionTypeNo;
    _textField.backgroundColor = [UIColor whiteColor];
    _textField.font = [UIFont systemFontOfSize:16];
    _textField.leftView = [[UIView alloc] initWithFrame:CGRectZero];
    _cameraBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _cameraBtn.frame = CGRectMake(0, 0, _textField.frame.size.height, _textField.frame.size.height);
    [_cameraBtn setImage:[ZPResourceManager getImageWithName:@"i_capture"] forState:UIControlStateNormal];
    if (IS_IPAD) {
        [_cameraBtn setImageEdgeInsets:UIEdgeInsetsMake(5, 0, 5, 5)];
    }
    [_cameraBtn addTarget:self action:@selector(onCameraBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    _textField.rightViewMode = UITextFieldViewModeAlways;
    
    self.keyboardToolbar = [ZPKeyboardToolbar keyboardToolbar];
    
    
    if ([self.keyboardToolbar validateOption:object.toolbarOption]){
        //[_textField setInputAccessoryView:self.keyboardToolbar];
    }
    
    [self addSubview:_textField];
    _bottomLine = [[UIView alloc] initWithFrame:CGRectMake(PADDING, self.frame.size.height - 1, self.frame.size.width - PADDING, 1)];
    [_bottomLine setBackgroundColor:[UIColor zpColorFromHexString:@"#E3E6E7"]];
    [self addSubview:_bottomLine];
    
    
    _line = [[UIView alloc] initWithFrame:CGRectMake(PADDING, self.frame.size.height - 1, self.frame.size.width - PADDING, 1)];
    [_line setBackgroundColor:TEXTFIELD_SUCCESS_COLOR];
    
    [self addSubview:_line];
    _line.hidden = YES;
    //add atm bank name
    [self addATMBankName:object];
}

- (void)onCameraBtnClicked {
    if (self.delegate && [self.delegate respondsToSelector:@selector(recordViewOnCameraButtonClicked)]) {
        [self.delegate recordViewOnCameraButtonClicked];
    }
}

- (void) addATMBankName:(ZPTextFieldRecordObject *)object {
    if (object.isBankName){
        bankNameView = [[UIView alloc] initWithFrame:CGRectMake([ZPResourceManager getContainterWidth] - 21 - BankNameView_W, object.topPadding + 1, BankNameView_W, [ZPResourceManager getTextFieldHeight] - 2)];
        bankNameView.backgroundColor = [UIColor clearColor];
        UIView * oneLine = [[UIView alloc] initWithFrame:CGRectMake(0, 8, 0.5, [ZPResourceManager getTextFieldHeight] - 16)];
        oneLine.backgroundColor = [UIColor colorWithWhite:0.7 alpha:1];
        [bankNameView addSubview:oneLine];
        bankNameTextField = [[UITextField alloc] initWithFrame:CGRectMake( 3 , 0, bankNameView.frame.size.width - 3, bankNameView.frame.size.height)];
        bankNameTextField.font = [ZPResourceManager boldFont];
        bankNameTextField.textColor = [UIColor zpLightBlueColor];
        bankNameTextField.textAlignment = NSTextAlignmentCenter;
        bankNameTextField.userInteractionEnabled = NO;
        bankNameTextField.backgroundColor = [UIColor whiteColor];
        [bankNameView addSubview:bankNameTextField];
        bankNameView.hidden = YES;
        
        
        [self addSubview:bankNameView];
    }
}


#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    _line.frame = CGRectMake(PADDING, self.textField.frame.origin.y + textField.frame.size.height - 1, self.frame.size.width - PADDING, 1);
    _line.hidden = NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(recordViewOnKeyboardReturnButtonClicked)]) {
        [self.delegate recordViewOnKeyboardReturnButtonClicked];
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    _line.hidden = YES;
    if (self.delegate && [self.delegate respondsToSelector:@selector(recordViewOnKeyboardDidEndEditing)]) {
        [self.delegate recordViewOnKeyboardDidEndEditing];
    }
}


- (BOOL) textField:(UITextField *) ltextField shouldChangeCharactersInRange:(NSRange) range replacementString:(NSString *) string
{
    
    
    if (string.length > 0 || (range.length > 0 && string.length == 0)){
        textfieldHighlighted = NO;
        [self renderTextField];
    }
    
    if([self.delegate respondsToSelector:@selector(recordViewOnValueChanged:)]) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.delegate recordViewOnValueChanged:self];
        });
    }
    
    if ([self.viewID isEqualToString:@"chargeAmountTextField"] || [self.viewID isEqualToString:@"zaloCoinInput"]){
        return YES;
    } else {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self formatTextField:ltextField];
        });
        return YES;
        
    }
}

- (void) formatTextField: (UITextField *) ltextField  {
    
    NSArray * format = rObject.formats[self.formatIndex];
    NSString * text = ltextField.text;
    NSString * textWithoutFormat = [_textField.text zpRemoveFormat:format];
    UITextRange *selRange = ltextField.selectedTextRange;
    UITextPosition *selStartPos = selRange.start;
    NSInteger cursorOffset = [ltextField offsetFromPosition:ltextField.beginningOfDocument toPosition:selStartPos];
    
    NSString * leftString = [text substringToIndex:cursorOffset];
    leftString = [leftString zpFormatWithFormat:format separator:nil];
    cursorOffset = leftString.length;
    
    ltextField.text = [textWithoutFormat zpFormatWithFormat:format separator:nil];
    
    UITextPosition *newCursorPosition = [ltextField positionFromPosition:ltextField.beginningOfDocument offset:cursorOffset];
    if(newCursorPosition) {
        UITextRange *newSelectedRange = [ltextField textRangeFromPosition:newCursorPosition toPosition:newCursorPosition];
        [ltextField setSelectedTextRange:newSelectedRange];
    }
}

- (void) formatTextField {
    
    NSArray * format = rObject.formats[self.formatIndex];
    _textField.text = [_textField.text zpFormatWithFormat:format separator:nil];
}

- (NSString *) textWithoutFormat {
    
    NSArray * format = rObject.formats[self.formatIndex];
    
    return [_textField.text zpRemoveFormat:format];
}

- (BOOL)textFieldShouldClear:(UITextField *) textField {
    textField.text = nil;
    textfieldHighlighted = NO;
    [self renderTextField];
    
    if([self.delegate respondsToSelector:@selector(recordViewOnValueChanged:)]) {
        
        [self.delegate recordViewOnValueChanged:self];
    }
    
    return YES;
}

#pragma mark - ZPUserControlDelegate
+ (float)viewHeightWithRecordObject:(ZPRecordObject*) object{
   // ZPTextFieldRecordObject * rObject = (ZPTextFieldRecordObject *)object;
    float height = [ZPResourceManager getTextFieldHeight] ;//+ rObject.topPadding + rObject.bottomPadding;
    return height;
}

+ (float)viewWidth{
    float width = [ZPResourceManager getContainterWidth];
    return width <=  0 ? 0 : width;
}

- (NSDictionary *)outputParams{
    NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] init];
    
    if ([self.viewID isEqualToString:@"chargeAmountTextField"] || [self.viewID isEqualToString:@"zaloCoinInput"]){
        NSString *value = [[self.textField.text zpRemoveDilimeter] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        [dictionary setObject: [NSString zpEmptyIfNilString:value]
                       forKey: self.param];
    } else {
        
        NSArray * format = rObject.formats[self.formatIndex];
        
        NSString * value = self.textField.isSecureTextEntry ? self.textField.text : [self.textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        [dictionary setObject:[[NSString zpEmptyIfNilString:value] zpRemoveFormat:format]
                       forKey: self.param];
    }
    
    
    return dictionary;
}
- (BOOL)checkValidate{
    if ((self.textField.text == NULL ||
         [self.textField.text isEqualToString:@""]) &&
        self.requiredFillMessage != NULL &&
        [self.requiredFillMessage isEqualToString:@""] == false) {
        //[self showAlertWithTitle:@"" andErrorMessage:self.requiredFillMessage];
        return NO;
    } else if (![self checkRegularExpression]){
        //[self showAlertWithTitle:@"" andErrorMessage:rObject.regrexErrorMessage];
        
        return NO;
    } else if (![self validateChannel]){
        return NO;
    }
    return YES;
}

- (void)showAlertWithTitle:(NSString *)title andErrorMessage:(NSString *)errorMessage {
    [self endEditing:YES];
    [ZPDialogView showDialogWithType:DialogTypeNotification title:title message:errorMessage buttonTitles:@[[R string_ButtonLabel_Close]] handler:nil];

}

- (BOOL)validateChannel {
    if (!self.channel) {
        return YES;
    }
    
    NSString * value = self.textField.text;
    if ([self.viewID isEqualToString:@"chargeAmountTextField"]){
        value = [[self.textField.text zpRemoveDilimeter] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }
    
    long amount = (long)[value longLongValue];
    
    if (self.channel.minPPAmount > 0 && amount < self.channel.minPPAmount) {
        NSString * minString = [[NSString stringWithFormat:@"%ld",self.channel.minPPAmount] zpFormatNumberWithDilimeter];
        [self showWarningMessage:[NSString stringWithFormat:[[ZPDataManager sharedInstance] messageForKey:@"message_validate_min_amount"],minString]];
        return NO;
    }
    
    if (self.channel.maxPPAmount > 0 && amount > self.channel.maxPPAmount) {
        NSString * maxString = [[NSString stringWithFormat:@"%ld",self.channel.maxPPAmount] zpFormatNumberWithDilimeter];
        [self showWarningMessage:[NSString stringWithFormat:[[ZPDataManager sharedInstance] messageForKey:@"message_validate_min_amount"],maxString]];
        return NO;
    }
    
    return YES;
    
}

- (BOOL)checkRegularExpression {
    if (!self.textField.text || !rObject.regrex) return YES;
    NSString * value = [self.textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([self.viewID isEqualToString:@"chargeAmountTextField"] || [self.viewID isEqualToString:@"zaloCoinInput"]){
        value = [self.textField.text zpRemoveDilimeter];
    }
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", rObject.regrex];
    
    return [emailTest evaluateWithObject:value];
}

- (NSString *)jsSubmitFormSml{
    NSString* textFieldValue = [[self textWithoutFormat] stringByReplacingOccurrencesOfString:@"'" withString:@"\\'"];
    
    if ([self.viewID isEqualToString:@"chargeAmountTextField"] || [self.viewID isEqualToString:@"zaloCoinInput"]){
        textFieldValue = [textFieldValue zpRemoveDilimeter];
    }
    
    return [NSString stringWithFormat:@"form.elements['%@'].value='%@';", self.smlID, textFieldValue];
}

- (NSString *)jsSubmitFormBanknet{
    NSString* textFieldValue = [[self textWithoutFormat] stringByReplacingOccurrencesOfString:@"'" withString:@"\\'"];
    
    if ([self.viewID isEqualToString:@"chargeAmountTextField"] || [self.viewID isEqualToString:@"zaloCoinInput"]){
        textFieldValue = [textFieldValue zpRemoveDilimeter];
    }
    
    return [NSString stringWithFormat:@"form.elements['%@'].value='%@';", self.banknetID, textFieldValue];
}

- (void)cacheInfo{
    if (self.cacheParam != NULL && [self.cacheParam isEqualToString:@""] == false && self.textField.text != NULL && [self.textField.text isEqualToString:@""] == false) {
        [[NSUserDefaults standardUserDefaults] setObject:self.textField.text forKey: [NSString stringWithFormat:@"%@%@", kPAYMENT_SDK_KEY_PREFIX, self.cacheParam]];
    }
}
- (void)loadInfoFromCache{
    if (self.cacheParam != NULL && [self.cacheParam isEqualToString:@""] == false) {
        NSString * stringCached = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@%@", kPAYMENT_SDK_KEY_PREFIX, self.cacheParam]];
        if (stringCached != NULL) {
            _textField.text = stringCached;
        }
    }
}

- (void)setValue:(NSString *) value viewById:(NSString *)viewID{
    if (viewID == NULL || value == NULL) {
        return;
    }
    if ([self.viewID isEqualToString:viewID]) {
        
        if ([self.viewID isEqualToString:@"chargeAmountTextField"] || [self.viewID isEqualToString:@"zaloCoinInput"]){
            _textField.text = [value zpFormatNumberWithDilimeter];
        } else {
            NSArray * format = rObject.formats[self.formatIndex];
            _textField.text = [value zpFormatWithFormat:format separator:nil];
        }
    }
}
- (void)setEnable:(bool)enable viewById:(NSString *)viewID{
    if ([self.viewID isEqualToString:viewID]) {
        [self.textField setEnabled:enable];
        [self renderTextField];
    }
}

- (void)setFormatIndex:(int)index viewId:(NSString *)viewID {
    NSArray * format = rObject.formats[self.formatIndex];
    
    NSString * rawString = [self.textField.text zpRemoveFormat:format];
    
    if (index < 0 || index > format.count - 1 || viewID == nil || ![self.viewID isEqualToString:viewID]){
        self.formatIndex = 0;
    } else {
        self.formatIndex = index;
    }
    
    format = rObject.formats[self.formatIndex];
    self.textField.text = [rawString zpFormatWithFormat:format separator:nil];
    
}

- (void)setHighlighted: (BOOL) highlighted viewId: (NSString *) viewID {
    if (viewID && [self.viewID isEqualToString:viewID]){
        textfieldHighlighted = highlighted;
        [self renderTextField];
    }
}

- (void)setBankName:(NSString *)value viewId:(NSString *)viewId {
    if (!value) {
        bankNameView.hidden = YES;
        return;
    }
    
    if (viewId && [self.viewID isEqualToString:viewId]){
        if (bankNameTextField ){
            bankNameView.hidden = NO;
            [bankNameTextField setText:value];
        }
    }
}

- (void) renderTextField {
    UIColor *color;
    if(!self.textField.isEnabled) {
        color = [ZPResourceManager textFieldDisabledBgColor];
    }
    else if(textfieldHighlighted) {
        color = [ZPResourceManager textFieldHighlightBgColor];
    }
    else {
        color = [UIColor whiteColor];
    }
    self.textField.backgroundColor = [UIColor clearColor];
}



- (void)setObject:(id)object viewId:(NSString *)viewId {
    if (viewId && [self.viewID isEqualToString:viewId]) {
        if ([object isKindOfClass:[ZPChannel class]]) {
            self.channel = (ZPChannel *)object;
        }
    }
}

- (void)updateLayouts {
    
    self.frame = CGRectMake(0,0,[ZPTextFieldRecordView viewWidth],
                            [ZPTextFieldRecordView viewHeightWithRecordObject:self.rObject]);
    self.textField.frame = CGRectMake(PADDING, 0 ,self.frame.size.width - 2*PADDING, [ZPResourceManager getTextFieldHeight]);
    self.bottomLine.frame = CGRectMake(PADDING, self.frame.size.height - 1, self.frame.size.width - PADDING, 1);
}

- (void)prepareForReuse {
    self.textField.text = nil;
    [self.textField setBackgroundColor:[UIColor whiteColor]];
}

- (void)setKeyboardType:(UIKeyboardType)keyboardType viewId:(NSString *)viewId {
    if ([viewId isEqualToString:self.viewID]) {
        self.textField.keyboardType = keyboardType;
        [self.textField reloadInputViews];
        //[self.textField resignFirstResponder];
    }
}

- (void)setOCRVisibility:(BOOL)flag viewId:(NSString *)viewId {
    if ([viewId isEqualToString:self.viewID]) {
        self.textField.rightView = flag ? self.cameraBtn : nil;
    }
}

@end
