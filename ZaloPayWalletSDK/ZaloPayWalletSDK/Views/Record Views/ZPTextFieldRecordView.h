//
//  ZPTextFieldRecordView.h
//  ZaloPaymentSDK
//
//  Created by hoangnx2 on 1/13/14.
//  Copyright (c) 2014 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZPUserControlDelegate.h"
#import "ZPRecordView.h"
@class ZPTextField;
@class ZPChannel;


@interface ZPTextFieldRecordView : ZPRecordView<ZPUserControlDelegate, UITextFieldDelegate>

@property (strong, nonatomic) NSString * requiredFillMessage;
@property (strong, nonatomic) NSString * param;
@property (strong, nonatomic) NSString * smlID;
@property (strong, nonatomic) NSString * banknetID;
@property (strong, nonatomic) NSString * cacheParam;
@property (assign, nonatomic) int invalidErrorCode;
@property (assign, nonatomic) int formatIndex;
@property (strong, nonatomic) ZPTextField * textField;
@property (weak, nonatomic) ZPChannel * channel;

- (void) formatTextField;
- (NSString *) textWithoutFormat;

@end

