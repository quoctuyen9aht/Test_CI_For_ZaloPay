//
//  ZPRecordView.m
//  ZaloPaymentSDK
//
//  Created by hoangnx2 on 1/14/14.
//  Copyright (c) 2014 VNG Corporation. All rights reserved.
//

#import "ZPRecordView.h"
#import "ZPPaymentInfo.h"
#import "ZPProgressHUD.h"


@implementation ZPRecordView

- (instancetype)initWithRecordObject:(ZPRecordObject *)object {
    return nil;
}

+ (float)viewHeightWithRecordObject:(ZPRecordObject *)object{
    return 0;
}
+ (float)viewWidth{
    return 0;
}
+ (UIView *)initWithRecordObject:(ZPRecordObject*) object{
    return NULL;
}
- (NSDictionary *)outputParams{
    return @{};
}
- (BOOL)checkValidate{
    return YES;
}
- (NSString *)jsSubmitFormSml{
    return @"";
}
- (NSString *)jsSubmitFormBanknet{
    return @"";
}

- (void)showWarningMessage:(NSString *)message {
    if (message.length > 0) {
//        [ZPProgressHUD showWarningWithStatus:message];
    }
}
- (void)setWidth:(CGFloat)width ofView:(UIView *)view {
    CGRect frame = view.frame;
    frame.size.width = width;
    view.frame = frame;
}

- (void)setDelegate:(id<ZPRecordViewDelegate,ZPKeyboardToolbarDelegate>)delegate {
    _delegate = delegate;
    _keyboardToolbar.delegate = delegate;
}

- (void)updateLayouts {}
- (void)prepareForReuse {}
- (void)setCaptchaImage:(UIImage*) img{}
- (void)setAppendString:(NSString *)str{}
- (void)setStaticValue:(NSString *) value andDisplayText:(NSString *) name{}
- (void)cacheInfo{}
- (void)loadInfoFromCache{}
- (void)setValue:(NSString *) value viewById:(NSString *)viewID{}
- (void)setEnable:(bool)enable viewById:(NSString *)viewID{}
- (void)loadInfoFromWebView:(UIWebView*)webView{}
- (void)setFormatIndex:(int)index viewId:(NSString *)viewID {}
- (void)setHighlighted: (BOOL) highlighted viewId: (NSString *) viewID {}
- (void)setBankName:(NSString *)value viewId:(NSString *)viewId {}
- (void)setATMCardInfo:(ZPAtmCard *)cachedCard {}
- (void)setCreditCardInfo:(ZPCreditCard *)cachedCard {}
- (void)setSMPinnedViewInfo:(ZPBill *)bill {}
- (void)setObject:(id)object viewId:(NSString *)viewId {}
- (void)setKeyboardType:(UIKeyboardType)keyboardType viewId:(NSString *)viewId {}
- (void)setOCRVisibility:(BOOL)flag viewId:(NSString *)viewId {}
- (void)updateSmPinnedViewWithAmount:(long)amount {}

@end
