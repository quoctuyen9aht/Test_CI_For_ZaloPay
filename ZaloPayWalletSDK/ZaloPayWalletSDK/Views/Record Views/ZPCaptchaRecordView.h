//
//  ZPCaptchaRecordView.h
//  ZaloPaymentSDK
//
//  Created by hoangnx2 on 1/13/14.
//  Copyright (c) 2014 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZPRecordView.h"
#import "ZPTextField.h"
#import "ZPTextFieldRecordView.h"


@interface ZPCaptchaRecordView : ZPRecordView<ZPUserControlDelegate>

@property (strong, nonatomic) UIImageView * imageView;
@property (strong, nonatomic) NSString * requiredFillMessage;
@property (strong, nonatomic) NSString * param;
@property (strong, nonatomic) NSString * smlID;
@property (strong, nonatomic) NSString * banknetID;
@property (strong, nonatomic) ZPTextField * textField;

@end
