//
//  ZPRecordView.h
//  ZaloPaymentSDK
//
//  Created by hoangnx2 on 1/14/14.
//  Copyright (c) 2014 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZPUserControlDelegate.h"
#import "ZPTextField.h"
#import "ZPKeyboardToolbar.h"
#import "ZPRecordObject.h"
@class ZPAtmCard;
@class ZPCreditCard;
@class ZPBill;
@class ZPRecordObject;

@protocol ZPRecordViewDelegate;

@interface ZPRecordView : UIView<ZPUserControlDelegate>

@property (assign, nonatomic) NSString* elementIdOfWebView;
@property (strong, nonatomic) ZPKeyboardToolbar * keyboardToolbar;
@property (strong, nonatomic) NSString * viewID;
@property (weak, nonatomic) id<ZPRecordViewDelegate,ZPKeyboardToolbarDelegate> delegate;


- (instancetype)initWithRecordObject:(ZPRecordObject *)object;
- (void)setCaptchaImage:(UIImage *)img;
- (void)setAppendString:(NSString *)str;
- (void)setStaticValue:(NSString *)value andDisplayText:(NSString *)name;
- (void)cacheInfo;
- (void)loadInfoFromCache;
- (void)setValue:(NSString *)value viewById:(NSString *)viewID;
- (void)setEnable:(bool)enable viewById:(NSString *)viewID;
- (void)loadInfoFromWebView:(UIWebView*)webView;
- (void)setFormatIndex:(int)index viewId:(NSString *)viewID;
- (void)setHighlighted: (BOOL) highlighted viewId: (NSString *) viewID;
- (void)setBankName:(NSString *)value viewId:(NSString *)viewId;
- (void)setATMCardInfo:(ZPAtmCard *)cachedCard;
- (void)setCreditCardInfo:(ZPCreditCard *)cachedCard;
- (void)setSMPinnedViewInfo:(ZPBill *)bill;
- (void)setObject:(id)object viewId:(NSString *)viewId;
- (void)showWarningMessage:(NSString *)message;
- (void)updateLayouts;
- (void)setWidth:(CGFloat)width ofView:(UIView *)view;
- (void)prepareForReuse;
- (void)setKeyboardType:(UIKeyboardType)keyboardType viewId:(NSString *)viewId;
- (void)setOCRVisibility:(BOOL)flag viewId:(NSString *)viewId;
- (void)updateSmPinnedViewWithAmount:(long)amount;

@end

@protocol  ZPRecordViewDelegate <NSObject>
@optional
- (void)recordView:(ZPRecordView *)view didReturnData:(id)data;
- (void)recordViewOnKeyboardReturnButtonClicked;
- (void)recordViewOnKeyboardDidEndEditing;
- (void)recordViewOnCameraButtonClicked;
- (void)recordViewOnValueChanged: (id) sender;
@end
