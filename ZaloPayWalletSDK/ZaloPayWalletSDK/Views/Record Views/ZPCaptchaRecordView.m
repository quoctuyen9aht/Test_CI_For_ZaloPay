//
//  ZPCaptchaRecordView.m
//  ZaloPaymentSDK
//
//  Created by hoangnx2 on 1/13/14.
//  Copyright (c) 2014 VNG Corporation. All rights reserved.
//

#import "ZPCaptchaRecordView.h"
#import "ZPCaptchaRecordObject.h"
#import "ZPResourceManager.h"
#import "ZPKeyboardToolbar.h"
#import "ZPConfig.h"
#import "UIColor+ZPExtension.h"
#import "ZPDataManager.h"
#import "NSString+ZPExtension.h"

@interface ZPCaptchaRecordView() <UITextFieldDelegate>
@property (strong, nonatomic) ZPCaptchaRecordObject* rObject;
@property (strong, nonatomic) UIView *line;

@end


@implementation ZPCaptchaRecordView
@synthesize textField, imageView,rObject;

- (void)dealloc {
    textField.delegate = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (instancetype)initWithRecordObject:(ZPRecordObject *)object {
    rObject = (ZPCaptchaRecordObject*)object;
    CGRect frame = CGRectMake(0, 0, [ZPCaptchaRecordView viewWidth], [ZPCaptchaRecordView viewHeightWithRecordObject:object] );
    
    self = [super initWithFrame:frame];
    if (self) {
        [self setAutoresizingMask: UIViewAutoresizingFlexibleWidth];
        //init textField
        CGFloat width = [ZPResourceManager getContainterWidth] - 2*PADDING;
        
        textField = [[ZPTextField alloc] initWithFrame:CGRectMake(PADDING, 0 , width * 0.6, [ZPResourceManager getTextFieldHeight])];
        textField.font = [UIFont systemFontOfSize:16];
        textField.delegate = self;
        textField.backgroundColor = [UIColor whiteColor];
        textField.tag = rObject.tag;
        textField.nextTag = rObject.nextTag;
        textField.prevTag = rObject.prevTag;
        
        self.keyboardToolbar = [ZPKeyboardToolbar keyboardToolbar];
        self.keyboardToolbar.prevBtn.enabled = textField.prevTag > 0 ? YES : NO;
        self.keyboardToolbar.nextBtn.enabled = textField.nextTag > 0 ? YES : NO;
        textField.returnKeyType = rObject.returnType;
        [textField setAutocapitalizationType:rObject.isCapLocksOn];
        textField.autocorrectionType = UITextAutocorrectionTypeNo;
        textField.spellCheckingType = UITextSpellCheckingTypeNo;
        textField.keyboardType = rObject.keyboardType;
        [textField addTarget:self action:@selector(textFieldDidChanged:) forControlEvents:UIControlEventEditingChanged];

        textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(rObject.placeHolder, @"") attributes:@{}];
        [textField setSecureTextEntry:rObject.isPassword];
        
        if ([self.keyboardToolbar validateOption:object.toolbarOption]){
            //[textField setInputAccessoryView:self.keyboardToolbar];
        }
        textField.borderStyle = UITextBorderStyleNone;
        [textField setBackgroundColor:[UIColor clearColor]];
        [self addSubview:textField];
        
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(textField.frame.size.width + 2 * PADDING, 0, width - textField.frame.size.width - PADDING, [ZPResourceManager getTextFieldHeight])];
        [imageView setContentMode:UIViewContentModeScaleToFill];
        imageView.backgroundColor = zpRGB(230, 230, 230);
        
        [self addSubview:imageView];
        _line = [[UIView alloc] initWithFrame:CGRectMake(0, [ZPResourceManager getTextFieldHeight] - 1, self.frame.size.width, 1)];
        [_line setBackgroundColor:TEXTFIELD_SUCCESS_COLOR];
        
        
        [self addSubview:_line];
        _line.hidden = YES;
        //add atm bank name
        
        _requiredFillMessage = rObject.requiredFillMessage;
        _param = rObject.param;
        _smlID = rObject.smlID;
        _banknetID = rObject.banknetID;
        self.backgroundColor = [UIColor whiteColor];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showKeyBoard:) name:ZPUIKeyboardDidShowNotification object:nil];

    }
    return self;
}


#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)atextField {
    [atextField resignFirstResponder];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(recordViewOnKeyboardReturnButtonClicked)]) {
        [self.delegate recordViewOnKeyboardReturnButtonClicked];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    _line.hidden = YES;
    [self updateOkButton];
}


- (void)textFieldDidBeginEditing:(UITextField *)atextField {
    _line.frame = CGRectMake(0, self.textField.frame.origin.y + atextField.frame.size.height - 2, self.frame.size.width, 1);
    _line.hidden = NO;
}

- (BOOL)textField:(UITextField *)atextField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString * textFieldString = atextField.text;
    textFieldString = [textFieldString stringByReplacingCharactersInRange:range withString:string];
    NSString * detectString = [textFieldString zpTrimSpace];
    int maxLength = [[ZPDataManager sharedInstance] doubleForKey:@"captcha_max_length"];
    if(maxLength == 0) maxLength = 10;
    [self updateOkButton];
    return detectString.length <= maxLength;

}


#pragma mark - ZPUserControlDelegate
+ (float)viewHeightWithRecordObject:(ZPRecordObject *)object{
    //return object.topPadding + [ZPResourceManager getTextFieldHeight] + object.bottomPadding;
    return [ZPResourceManager getTextFieldHeight];
    
}
+ (float)viewWidth{
    return [ZPResourceManager getContainterWidth];
}

- (NSDictionary *)outputParams{
    NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setObject:self.textField.text forKey: self.param];
    return dictionary;
}
- (BOOL)checkValidate{
    if ((self.textField.text == NULL || [self.textField.text isEqualToString:@""]) && self.requiredFillMessage != NULL && [self.requiredFillMessage isEqualToString:@""] == false) {
        return NO;
    } else if (![self checkRegularExpression]){
        return NO;
    }
    return YES;
}

- (void)showAlertWithTitle:(NSString *)title anderrorMessage:(NSString *)errorMessage {
    [self endEditing:YES];
    [ZPDialogView showDialogWithType:DialogTypeNotification title:title message:errorMessage buttonTitles:@[[R string_ButtonLabel_Close]] handler:nil];
}
- (BOOL)checkRegularExpression {
    if (!self.textField.text || !rObject.regrex) return YES;
    NSString * value = [self.textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", rObject.regrex];
    
    return [emailTest evaluateWithObject:value];
}

- (NSString *)jsSubmitFormSml{
    return [NSString stringWithFormat:@"form.elements['%@'].value='%@';", self.smlID, self.textField.text];
}
- (NSString *)jsSubmitFormBanknet{
    return [NSString stringWithFormat:@"form.elements['%@'].value='%@';", self.banknetID, self.textField.text];
}

- (void)setCaptchaImage:(UIImage *)img{
    self.textField.text = @"";
    [self.imageView setImage:img];
    [self updateOkButton];
}

- (void)updateLayouts {
    CGFloat width = [ZPResourceManager getContainterWidth] - 2*PADDING;
    textField.frame  = CGRectMake(PADDING, 0 , width * 0.6, [ZPResourceManager getTextFieldHeight]);
    imageView.frame = CGRectMake(PADDING + textField.frame.size.width + PADDING , 6, width - textField.frame.size.width - PADDING, [ZPResourceManager getTextFieldHeight] * 0.8);
}

- (void)textFieldDidChanged:(UITextField *)textField {
    [self updateOkButton];

}
- (void)prepareForReuse {
    self.textField.text = nil;
    imageView.image = nil;
}
- (void) showKeyBoard:(NSNotification *)note{
    if(self.textField != nil)
        [self.textField becomeFirstResponder];
}

- (void) updateOkButton {
    if (self.delegate && [self.delegate respondsToSelector:@selector(recordViewOnKeyboardDidEndEditing)]) {
        [self.delegate recordViewOnKeyboardDidEndEditing];
    }
}

@end
