//
//  ZPVoucherManager.m
//  ZaloPayWalletSDK
//
//  Created by Phuoc Huynh on 8/25/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import "ZPVoucherManager.h"
#import "NetworkManager+ZaloPayWalletSDK.h"
#import "ZPVoucherHistory.h"
#import "ZaloPayWalletSDKPayment.h"
#import "ZPBill.h"
#import "ZPPaymentManager.h"
#import <PINCache/PINMemoryCache.h>
#import <ZaloPayWalletSDK/ZaloPayWalletSDK-Swift.h>
#import "ZPVoucherInfo.h"
#import "ZaloPayWalletSDKLog.h"
//v3.0
#import "ZPPaymentMethod.h"
#import "ZPPaymentChannel.h"
#import "ZPDataManager.h"
#import "ZPResourceManager.h"

@interface ZPVoucherManager ()
@property (nonatomic,strong) PINMemoryCache *memoryCache;
@end

@implementation ZPVoucherManager

- (id)init {
    self = [super init];
    if (self != NULL) {
        self.arrVouchersWillRevert = [NSMutableArray new];
        [self createMemoryCache];
    }
    return self;
}

- (RACSignal <NSArray <ZPVoucherHistory *>*>*)loadAllVoucher {
    return  [[[[ZaloPayWalletSDKPayment sharedInstance] getVoucherHistory] map:^NSArray <ZPVoucherHistory *>*(NSDictionary *data) {
        NSArray *allVoucher = [data arrayForKey:@"voucherhistory"];
        NSArray *voucher = [self parseVoucherData:allVoucher];
        return voucher;
    }] deliverOnMainThread];
}

- (RACSignal *)loadAllAvailableVoucher {
    return [[self loadAllVoucher] map:^NSArray <ZPVoucherHistory *> * _Nullable (NSArray<ZPVoucherHistory *> * _Nullable value) {
        if (!value) {
            return @[];
        }
        
        NSMutableArray *allVoucher = [self filterAvailableVouchers:value];
        // case user có nhiều voucher -> user danh sách voucher đó -> vào xem danh sách voucher đang có.
        @synchronized (self.arrVouchersWillRevert) {
            for (ZPVoucherInfo *voucherInfo in self.arrVouchersWillRevert) {
                [allVoucher addObjectNotNil:voucherInfo.voucherUsed];
            }
        }
        return allVoucher;
    }];
}

- (RACSignal * )loadVoucherForBill:(ZPBill *)bill {
    return [[self loadAllVoucher] map:^NSArray <ZPVoucherHistory *> * _Nullable (NSArray<ZPVoucherHistory *> * _Nullable value) {
        if (!value) {
            return @[];
        }
        
        NSMutableArray *allVoucher = [self filterAvailableVouchers:value forBill:bill];
        // case: user thanh toán. user voucher -> map thẻ -> hết 5 phút clear cache -> load lại danh sách voucher -> trạng thái của 2 voucher kia đã là used -> lấy từ mảng revert thêm vào để hiển thị.
        
        @synchronized (self.arrVouchersWillRevert) {
            for (ZPVoucherInfo *voucher in self.arrVouchersWillRevert) {
                if (![allVoucher containsObject:voucher.voucherUsed]) {
                    [allVoucher addObjectNotNil:voucher.voucherUsed];
                }
            }
        }
        return allVoucher;
    }];
}

- (NSArray <ZPVoucherHistory *>*)parseVoucherData:(NSArray *)voucherData {
    if (voucherData.count == 0) {
        return @[];
    }
    NSMutableArray <ZPVoucherHistory *>*allVoucher = [NSMutableArray new];
    for (NSDictionary *oneDic in voucherData) {
        ZPVoucherHistory *voucher = [[ZPVoucherHistory alloc] initDataWithDict:oneDic];
        [allVoucher addObject:voucher];
    }
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"useconditionexpireddate"
                                                                   ascending:YES];
    return [allVoucher sortedArrayUsingDescriptors:@[sortDescriptor]];
}

- (NSMutableArray *)filterAvailableVouchers:(NSArray *)allVoucher {
    NSMutableArray *_return = [NSMutableArray array];
    for (ZPVoucherHistory *voucher in allVoucher) {
        NSDate* expireddate = [NSDate dateWithTimeIntervalSince1970:voucher.useconditionexpireddate/1000];
        if ([expireddate timeIntervalSinceNow] < 0) {
            continue;
        }
        if (voucher.historystatus != ZPVoucherHistoryStatusAvailabel) {
            continue;
        }
        [_return addObject:voucher];
    }
    return _return;
}

- (NSMutableArray <ZPVoucherHistory *>*)filterAvailableVouchers:(NSArray *)allVoucher forBill:(ZPBill *)bill {
    NSMutableArray <ZPVoucherHistory *>*_return = [NSMutableArray array];
    for (ZPVoucherHistory *voucher in allVoucher) {
        NSDate* expireddate = [NSDate dateWithTimeIntervalSince1970:voucher.useconditionexpireddate/1000];
        if ([expireddate timeIntervalSinceNow] < 0) {
            continue;
        }
        if (voucher.historystatus != ZPVoucherHistoryStatusAvailabel) {
            continue;
        }
        if (![self conditionAppListOfVoucher:voucher forBill:bill]) {
            continue;
        }
        if (![self conditionMinMaxFor:voucher bill:bill]) {
            continue;
        }
        [_return addObject:voucher];
    }
    return _return;
}

- (NSArray <ZPVoucherHistory *>*)filterAvailableVouchersWhenSelectedPaymentMethod:(NSArray *)allVoucher forBill:(ZPBill *)bill {
    NSMutableArray <ZPVoucherHistory *>*_return = [NSMutableArray array];
    for (ZPVoucherHistory *voucher in allVoucher) {
        if ([self conditionForPaymentMethod:voucher forBill:bill]) {
            voucher.paymentConditions.voucherConditionPPAmount = [self conditionMinMaxPPAmount:voucher forBill:bill];
            voucher.isDisable = voucher.paymentConditions.voucherConditionPPAmount != ZPVoucherConditionPPAmountCorrect;
            [_return addObject:voucher];
        }
    }
    return _return;
}

- (NSArray <ZPVoucherHistory *>*)filterAvailableVouchersToEnablePaymentMethod:(NSArray *)allVoucher forBill:(ZPBill *)bill {
    NSMutableArray <ZPVoucherHistory *>*_return = [NSMutableArray array];
    for (ZPVoucherHistory *voucher in allVoucher) {
        if ([self conditionForPaymentMethod:voucher forBill:bill] && [self conditionMinMaxPPAmount:voucher forBill:bill] == ZPVoucherConditionPPAmountCorrect) {
            [_return addObject:voucher];
        }
        
    }
    return _return;
}

- (ZPVoucherConditionPPAmount) conditionMinMaxPPAmount:(ZPVoucherHistory *)voucher forBill:(ZPBill *) bill {
    NSString *bankCode = bill.currentPaymentMethod.bankCode;
    if ([bankCode containsString:stringCreditCardBankCode]
             ||  [bankCode containsString:VISA]
             ||  [bankCode containsString:MASTER]
             ||  [bankCode containsString:JCB]
        ) {
        bankCode = stringCreditCardBankCode;
    }
    ZPChannel *channel = [[ZPDataManager sharedInstance] channelWithTypeAndBankCode:bill.currentPaymentMethod.methodType bankCode:bankCode];
    long long voucherValue = [self getValueCurrencyFromVoucher:voucher ofBill:bill];
    long long totalCharge = bill.amount + bill.currentPaymentMethod.totalChargeFee - voucherValue;
    long maxAmount = bill.currentPaymentMethod.methodType == ZPPaymentMethodTypeZaloPayWallet ? MIN([ZaloPayWalletSDKPayment sharedInstance].appDependence.currentBalance, channel.maxPPAmount) : channel.maxPPAmount;
    if (totalCharge < channel.minPPAmount) {
        return ZPVoucherConditionPPAmountLessThanMinPPAmount;
    }
    if (totalCharge > maxAmount) {
        return ZPVoucherConditionPPAmountGreaterThanMaxPPAmount;
    }
    return ZPVoucherConditionPPAmountCorrect;
}

- (BOOL) conditionForPaymentMethod:(ZPVoucherHistory *)voucher forBill:(ZPBill *) bill {
    if ([voucher.paymentConditions.useconditionpmcs containsObject:@(-1)]) {
        return true;
    }
    if (![self conditionPMCListOfVoucher:voucher forBill:bill]) {
        return false;
    }
    if (bill.currentPaymentMethod.methodType == ZPPaymentMethodTypeZaloPayWallet) {
        return true;
    }
    if ([bill.currentPaymentMethod isCCCard] || [bill.currentPaymentMethod isCCDebitCard]) {
        return [self conditionCCBankCodeListOfVoucher:voucher forBill:bill];
    }else {
        return [self conditionBankCodeListOfVoucher:voucher forBill:bill];
    }
}

- (BOOL) conditionAppListOfVoucher:(ZPVoucherHistory *)voucher forBill:(ZPBill *) bill {
    if ([voucher.paymentConditions.useconditionapplist containsObject:@(-1)] || voucher.paymentConditions.useconditionapplist.count == 0) {
        return true;
    }
    for (NSString *appIDString in voucher.paymentConditions.useconditionapplist) {
        if (bill.appId == appIDString.integerValue) {
            return true;
        }
    }
    return false;
}

- (BOOL) conditionOnlyWalletMethodListOfVoucher:(ZPVoucherHistory *)voucher forBill:(ZPBill *) bill {
    if ([voucher.paymentConditions.useconditionpmcs containsObject:@(-1)]) {
        return true;
    }
    for (NSString *pmcid in voucher.paymentConditions.useconditionpmcs) {
        if (pmcid.intValue == ZPPaymentMethodTypeZaloPayWallet) {
            voucher.paymentConditions.currentPMCID = pmcid.intValue;
            return true;
        }
    }
    return false;
}

- (BOOL) conditionPMCListOfVoucher:(ZPVoucherHistory *)voucher forBill:(ZPBill *) bill {
    NSString *bankCode = bill.currentPaymentMethod.bankCode;
    ZPChannel *channel = [[ZPDataManager sharedInstance] channelWithTypeAndBankCode:bill.currentPaymentMethod.methodType bankCode:bankCode];
    
    int pmcidOfBill = channel.channelID;
    for (NSString *pmcid in voucher.paymentConditions.useconditionpmcs) {
        if (pmcidOfBill == pmcid.intValue) {
            voucher.paymentConditions.currentPMCID = pmcid.intValue;
            return true;
        }
    }
    return false;
}

- (BOOL) conditionBankCodeListOfVoucher:(ZPVoucherHistory *)voucher forBill:(ZPBill *) bill {
    if (voucher.paymentConditions.useconditionbankcodes.count == 0 ) {
        return true;
    }
    NSString *bankcodeOfBill = bill.currentPaymentMethod.bankCode;
    for (NSString *bankcode in voucher.paymentConditions.useconditionbankcodes) {
        if ([bankcode.uppercaseString isEqualToString:bankcodeOfBill.uppercaseString]) {
            voucher.paymentConditions.currentBankcode = bankcode;
            return true;
        }
    }
    return false;
}

- (BOOL) conditionCCBankCodeListOfVoucher:(ZPVoucherHistory *)voucher forBill:(ZPBill *) bill {
    
    if (voucher.paymentConditions.useconditionccbankcodes.count == 0 ) {
        return true;
    }
    NSString *ccbankcodeOfBill = [ZPResourceManager getCcBankCodeFromConfig:bill.currentPaymentMethod.savedCard.first6CardNo];
    if ([ccbankcodeOfBill.uppercaseString containsString:@"VISA"]) {
        ccbankcodeOfBill = @"Visa";
    }
    if ([ccbankcodeOfBill.uppercaseString containsString:@"MASTER"]) {
        ccbankcodeOfBill = @"Master";
    }
    if ([ccbankcodeOfBill.uppercaseString containsString:@"JCB"]) {
        ccbankcodeOfBill = @"JCB";
    }
    for (NSString *ccbankcode in voucher.paymentConditions.useconditionccbankcodes) {
        if ([ccbankcode.uppercaseString isEqualToString:ccbankcodeOfBill.uppercaseString]) {
            voucher.paymentConditions.currentCCBankcode = ccbankcode;
            return true;
        }
    }
    return false;
}


- (BOOL)conditionMinMaxFor:(ZPVoucherHistory *)voucher bill:(ZPBill *)bill {
    if (bill.amount >= voucher.useconditionminamount) {
        if (voucher.useconditionmaxamount != -1) {
            return bill.amount <= voucher.useconditionmaxamount;
        }
        return YES;
    }
    return NO;
}

- (void)createMemoryCache {
    self.memoryCache = [[PINMemoryCache alloc] init];
}

- (void)arrVouchersWillRevertAddVoucher:(ZPVoucherInfo *)voucherInfo {
    if (![voucherInfo isKindOfClass:[ZPVoucherInfo class]]) {
        return;
    }
    
    @synchronized (self.arrVouchersWillRevert) {
        if ([self.arrVouchersWillRevert containsObject:voucherInfo]) {
            return;
        }
        [self.arrVouchersWillRevert addObject:voucherInfo];
    }
}

- (void)revertArrVoucherAndUseVoucher:(ZPVoucherInfo *)voucherUsed {
    @synchronized (self.arrVouchersWillRevert) {
        if ([voucherUsed isKindOfClass:[ZPVoucherInfo class]]) {
            [self.arrVouchersWillRevert removeObject:voucherUsed];
        }
        if ([self.arrVouchersWillRevert count] == 0) {
            return;
        }
        for (ZPVoucherInfo *voucher in self.arrVouchersWillRevert) {
            [self revertVoucher:voucher];
        }
        [self.arrVouchersWillRevert removeAllObjects];
    }
}

- (void)revertVoucher:(ZPVoucherInfo *)voucher {
    [[ZPPaymentManager revertVoucher:voucher.voucherSig] subscribeNext:^(id x) {
        
    }];
}

#pragma mark - Cache Voucher
    
- (NSMutableArray *)voucherCachesForBill:(ZPBill *)bill {
    NSString *cacheKey = [self cacheKeyForBill:bill];
    NSArray *data = [self.memoryCache objectForKey:cacheKey];
    NSMutableArray *_return = [NSMutableArray array];
    if (data.count > 0) {
        [_return addObjectsFromArray:data];
    }
    return  _return;
}

- (void)saveVouchers:(NSArray *)vouchers forBill:(ZPBill *)bill {
    if ([vouchers isKindOfClass:[NSArray class]]) {
        NSString *cacheKey = [self cacheKeyForBill:bill];
        [self.memoryCache setObject:vouchers forKey:cacheKey];
    }
}

- (void)clearVouchersCaches {
    [self.memoryCache removeAllObjects];
}
    
- (NSString *)cacheKeyForBill:(ZPBill *)bill {
    id<ZPWalletDependenceProtocol> helper = [ZaloPayWalletSDKPayment sharedInstance].appDependence;    
    NSString *userId = [helper zaloPayUserId];
    long billAmount = bill.amount;
    uint32_t appId = bill.appId;
    return [NSString stringWithFormat:@"arrVoucherHistories%d%@%ld",appId,userId,billAmount];    
}

- (void)updateCacheAfterPaymentSuccessWithVoucher:(ZPVoucherHistory *)voucher forBill:(ZPBill *)bill {
    if (voucher == nil) {
        return;
    }
    
    NSMutableArray *arrVouchers = [self voucherCachesForBill:bill];
    if (arrVouchers.count == 0) {
        return;
    }
    
    NSUInteger indexToRemove = [arrVouchers indexOfObjectPassingTest:^BOOL(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        ZPVoucherHistory *voucherHistory = (ZPVoucherHistory *)obj;
        return [voucherHistory.uid isEqualToString:voucher.uid];
    }];
    
    if (indexToRemove != NSNotFound) {
        [arrVouchers removeObjectAtIndex:indexToRemove];
    }
    NSString *key = [self cacheKeyForBill:bill];
    [self.memoryCache setObject:arrVouchers forKey:key];
}


- (RACSignal *)preloadVoucherForBill:(ZPBill *)bill {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSString *key = [self cacheKeyForBill:bill];
        NSArray *data = [self.memoryCache objectForKey:key];
        if (data) {
            [subscriber sendCompleted];
            return nil;
        }
        [[self loadVoucherForBill:bill] subscribeNext:^(NSArray *vouchers) {
            [self saveVouchers:vouchers forBill:bill];
            [subscriber sendCompleted];
        } error:^(NSError *error) {
            [subscriber sendCompleted];
        }];
        return nil;
    }];
}

- (ZPVoucherInfo *) getVoucherUsedExistedInCache:(ZPVoucherHistory *)voucher {
    return [self.arrVouchersWillRevert firstObjectUsing:^BOOL(ZPVoucherInfo *voucherInfo) {
        return [voucher.uid isEqualToString:voucherInfo.voucherUsed.uid];
    }];
}

- (ZPVoucherHistory *) getBestVoucherFromArrayVoucher:(NSArray *)arrVouchers ofBill:(ZPBill *)bill {
    ZPVoucherHistory *bestVoucher = arrVouchers.firstObject;
    long long bestVoucherValue = [self getValueCurrencyFromVoucher:bestVoucher ofBill:bill];
    for (ZPVoucherHistory *voucher in arrVouchers) {
        long long voucherValue = [self getValueCurrencyFromVoucher:voucher ofBill:bill];
        if (bestVoucherValue < voucherValue) {
            bestVoucher = voucher;
            bestVoucherValue = voucherValue;
        }
    }
    return bestVoucher;
}

- (long long) getValueCurrencyFromVoucher:(ZPVoucherHistory *)voucher ofBill:(ZPBill *)bill {
    long long valueCurrency = voucher.value;
    if (voucher.valuetype == ZPVoucherValueTypePercent) {
        valueCurrency *= bill.amount/100;
        if (voucher.cap > 0 && valueCurrency > voucher.cap) {
            valueCurrency = voucher.cap;
        }
    }
    return valueCurrency;
}
@end
