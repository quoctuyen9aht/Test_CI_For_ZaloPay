////
////  ZPPlaceHolderViewProtocol.h
////  ZaloPayWalletSDK
////
////  Created by bonnpv on 10/3/17.
////  Copyright © 2017 VNG Corporation. All rights reserved.
////
//
//#import <Foundation/Foundation.h>
//
//@protocol ZPPlaceHolderViewProtocol <NSObject>
//- (void)setupViewErrorLoad;
//- (void)setupViewNoNetwork;
//- (void)addRefreshTarget:(id)target action:(SEL)action forControlEvents:(UIControlEvents)controlEvents;
//@end
