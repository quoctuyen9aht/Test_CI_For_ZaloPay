//
//  ZPCreditCardHandler.h
//  ZPSDK
//
//  Created by Nguyen Xuan Phung on 1/19/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPPaymentHandler.h"
#import "ZPPaymentChannel.h"
#import "ZPPaymentInfo.h"
#import "ZPPaymentMethod.h"

@class ZPResponseObject;
#define ZP_CREDIT_CACHED_CARD_KEY  @"znp-credit-cached-card-key"
#define kZP_CREDIT_CACHED_CARD_KEY  @"kznp-credit-cached-card-key"
#define ZP_CREDIT_CACHED_CARD_LAST_UPDATE_KEY  @"znp-credit-cached-card-last-update-key"
#define ZP_CREDIT_CACHED_CARD_INTERVAL 20*60

@interface ZPCreditCardHandler : ZPPaymentHandler

@property (strong, nonatomic) ZPChannel * creditChannel;
@property (strong, nonatomic) ZPCreditCard * creditCard;
@property (strong, nonatomic) NSString *first6No;

- (ZPCreditCard *)getSavedCreditCard;
- (void)hideProgressHUD;
- (void)showLongProgressHUD;
- (void)showProgressHUD;
- (void)hideProgressHUDWithResultCode:(int)code;
@end
