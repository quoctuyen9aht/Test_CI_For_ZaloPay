//
//  ZPSubmiTransBaseModel.m
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 7/2/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPSubmiTransBaseModel.h"
#import "ZPPaymentManager.h"
#import "ZPPaymentResponse.h"

@implementation ZPSubmiTransBaseModel
+ (void)getSatusByAppTransId:(NSString *)appTransID
                       appId:(NSInteger)appId
                   zpTransId:(NSString *)zpTransId
                        bill:(ZPBill *)bill
                  subscriber:(id <RACSubscriber>)subscriber {
    [[ZPPaymentManager new] getStatusByAppTransid:appTransID appId:appId bankCode:@"" bill:bill callback:^(ZPPaymentResponse *response) {
        if (response.zpTransID.length == 0) {
            response.zpTransID = zpTransId;
        }
        [subscriber sendNext:response];
        [subscriber sendCompleted];
    }];
}

@end
