//
//  ZMPATMHandler.h
//  ZaloPayWalletSDK
//
//  Created by Thi La on 5/23/16.
//  Copyright © 2016 VNG. All rights reserved.
//

#import "ZMPPaymentHandler.h"

#define ZNP_ATM_CACHED_CARD_KEY  @"znp-atm-cached-card-key"
#define kZNP_ATM_CACHED_CARD_KEY  @"kznp-atm-cached-card-key"
#define ZNP_ATM_CACHED_CARD_LAST_UPDATE_KEY  @"znp-atm-cached-card-last-update-key"
#define ZNP_ATM_CACHED_CARD_INTERVAL 20*60

@interface ZMPATMHandler : ZMPPaymentHandler

@property (assign, nonatomic) ZMPAtmPaymentStep currentStep;

- (void)showAtmPinView;
- (void)showProgressHUD;
- (void)hideProgressHUD;

@end
