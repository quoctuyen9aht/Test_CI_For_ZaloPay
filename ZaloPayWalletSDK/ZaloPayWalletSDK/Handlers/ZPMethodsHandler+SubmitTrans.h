//
//  ZPMethodsHandler+SubmitTrans.h
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 7/1/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPMethodsHandler.h"

@interface ZPMethodsHandler (SubmitTrans)
- (void)handleMethod:(ZPPaymentMethod *)method password:(NSString *)password fromTouchId:(BOOL)fromTouchId;
@end
