//
//  ZMPATMHandler.m
//  ZaloPayWalletSDK
//
//  Created by Thi La on 5/23/16.
//  Copyright © 2016 VNG. All rights reserved.
//

#import "ZMPATMHandler.h"
#import "ZMPPaymentInfo.h"
#import "ZNPAtmPinnedView.h"
#import "ZMPSmartLinkHelper.h"
#import "ZMPBill.h"
#import "ZMPDataManager.h"
#import "ZNPBillDescriptionCell.h"
#import "ZNPAtmAutoFillCell.h"
#import "ZNPAtmAutoDetectInfoCell.h"
#import "NSData+ZMPExtension.h"
#import "ZMPConfig.h"
#import "ZMPPaymentChannel.h"
#import "NSString+ZMPExtension.h"
#import "ZMPPaymentManager.h"
#import "ZaloPayWalletSDKPayment.h"
#import "ZMPAtmResponseObject.h"
#import "ZMPPaymentResponse.h"
#import "ZMPLogManager.h"
#import "ZMPProgressHUD.h"
#import "ZMPBank.h"
#import "ZMPOCRViewController.h"
#import "ZNPTextFieldRecordView.h"
#import "ZMPProgressHUD.h"
#import "CardIO.h"
#import "NSString+TextDirectionality.h"
#import "UIColor+ZMPExtension.h"
#import "ZPWCreditCardWebviewCell.h"
#import "NSMutableDictionary+ZMPExtension.h"
#import "ZPWOtpCell.h"


#define kATM_AUTO_DETECT_INFO_CELL_ID   @"atm-auto-detect-info"
#define kATM_AUTO_FILL_INFO_CELL_ID     @"atm-auto-fill"
#define kATM_PINNED_VIEW_CELL_ID        @"atm-pinned-view"
#define zmp_atm_handler_step_timeout    60
#define kZPW_CREDIT_CARD_WEBVIEW_CELL_ID   @"credit-card-webview-cell"
#define kZPW_ATM_OTP_CELL_ID   @"otp-cell"

@interface ZMPATMHandler()<UIWebViewDelegate, CardIOPaymentViewControllerDelegate, ZNPRecordViewDelegate, UIScrollViewDelegate> {
    ZMPAtmCard* cachedCard;
    NSTimeInterval atmStartTimestamp;
    int otpRetry;
    int otpNCaptchaRetry;
    int useSaveCardRetry;
    BOOL isShowingProgressingMessage;
    bool isRedirecatble;
    NSTimer* checkWebViewTimeOutTimer;
    NSString * currentRequestURLString ;
    bool didReturned;
    BOOL isAbleToShowAtmPinView;
    NSTimer* checkStepTimeOutTimer;
    ZMPAtmCard* scanCard;
    long inputAmount;
    NSString *webviewUrl;
    CGRect webviewBound;
}

@property (strong, nonatomic) ZMPAtmCard* atmCard;
@property (strong, nonatomic) UIWebView* mWebView;
@property (strong, nonatomic) ZNPAtmPinnedView * atmPinnedView;

@end

@implementation ZMPATMHandler
@synthesize currentStep, atmCard;

- (void)dealloc {
    self.mWebView.delegate = nil;
    UITableViewCell * cell =
    [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    if ([cell isKindOfClass:[ZNPPaymentViewCell class]]) {
        ZNPPaymentViewCell * paymentCell = (ZNPPaymentViewCell *)cell;
        if ([paymentCell respondsToSelector:@selector(setDelegate:)]) {
            paymentCell.delegate = nil;
        }
    }
    
}

- (instancetype)init {
    self = [super init];
    if (self) {
        atmCard = [[ZMPAtmCard alloc] init];
        currentStep = ZMPAtmPaymentStepAutoFillCardInfo;
        didReturned= false;
        isRedirecatble = true;
        otpRetry = kATM_OTP_MAX_RETRY;
        otpNCaptchaRetry = kATM_OTP_CATPCHA_MAX_RETRY;
        useSaveCardRetry = kATM_SAVE_CARD_MAX_RETRY;
        self.title = @"Thanh toán bằng ATM";
        
        scanCard = [[ZMPAtmCard alloc] init];
    }
    //    NSDictionary *dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
    //                                @"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36", @"UserAgent", nil];
    //    [[NSUserDefaults standardUserDefaults] registerDefaults:dictionary];
    return self;
}

#pragma mark - Overrides
- (void)initialization {
    isAbleToShowAtmPinView = YES;
    atmCard.amount = self.bill.calculateAmount;
    [self.mTableView registerNib:[self.dataManager nibNamed:@"ZNPAtmAutoDetectInfoCell"] forCellReuseIdentifier:kATM_AUTO_DETECT_INFO_CELL_ID];
    [self.mTableView registerNib:[self.dataManager nibNamed:@"ZNPAtmAutoFillCell"] forCellReuseIdentifier:kATM_AUTO_FILL_INFO_CELL_ID];
    [self.mTableView registerNib:[self.dataManager nibNamed:@"ZNPAtmPinnedView"] forCellReuseIdentifier:kATM_PINNED_VIEW_CELL_ID];
    [self.mTableView registerNib:[self.dataManager nibNamed:@"ZPWOtpCell"] forCellReuseIdentifier:kZPW_ATM_OTP_CELL_ID];
    [self.mTableView registerNib:[self.dataManager nibNamed:@"ZPWCreditCardWebviewCell"] forCellReuseIdentifier:kZPW_CREDIT_CARD_WEBVIEW_CELL_ID];
    [self.delegate paymentControllerUpdateAmountLabel:0];
}

- (void)viewWillClose{
    isAbleToShowAtmPinView = NO;
    if (self.atmPinnedView) {
        [self.atmPinnedView dismiss];
    }
}

- (void)viewWillBack{
    isAbleToShowAtmPinView = NO;
    if (self.atmPinnedView) {
        [self.atmPinnedView dismiss];
    }
}

- (void)atmCardFinishedScanning:(NSString*)cardNumber{
    ZMPLog(@"cardScanSuccessfully: %@", cardNumber);
    scanCard.cardNumber = cardNumber;
    if (currentStep != ZMPAtmPaymentStepAutoDetectBank) {
        currentStep = ZMPAtmPaymentStepAutoDetectBank;
    }
    [self.mTableView reloadData];
}

- (BOOL) hasOkButton{
    if (currentStep != ZMPAtmPaymentStepWaitingWebview) {
        return YES;
    }
    return NO;
}

- (BOOL) hasAppInfo {
    if (currentStep != ZMPAtmPaymentStepWaitingWebview) {
        return YES;
    }
    return NO;
}

- (BOOL) hasTableMainInfo{
    if (currentStep != ZMPAtmPaymentStepSavedCardConfirm) {
        ZMPLog(@"hasTableMainInfo YES");
        return YES;
    }
    ZMPLog(@"hasTableMainInfo YES");
    return NO;
}

- (BOOL) hasFee{
    if (currentStep != ZMPAtmPaymentStepWaitingWebview) {
        return YES;
    }
    return NO;
}


- (BOOL)hasChannelDiscount {
    return (currentStep == ZMPAtmPaymentStepAutoDetectBank
            || currentStep == ZMPAtmPaymentStepAutoFillCardInfo);
}

- (BOOL)hasDescriptionBottom{
    return NO;
}

- (NSString *)paymentControllerTitle {
    return self.title ? self.title : [NSString stringWithFormat:@"%@ %@", [self.dataManager getTitleByTranstype:self.bill.transType], @"bằng ATM"];
}

- (NSString *)okButtonTitle {
    return (currentStep == ZMPAtmPaymentStepAtmAuthenPayer) ? [NSString stringWithFormat:@"%@ Ngay", [self.dataManager getTitleByTranstype:self.bill.transType]] : @"Tiếp Tục";
}

#pragma mark - UITableViewDelegate & DataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    ZNPBillDescriptionCell * desciptionCell = (ZNPBillDescriptionCell *)[super tableView:tableView cellForRowAtIndexPath:indexPath];
    if (desciptionCell) {
        [self updatePortraitBillDiscriptionCell:desciptionCell];
        return desciptionCell ;
    }
    ZNPPaymentViewCell * cell;
    
    ZMPLog(@"cellForRowAtIndexPath currentStep: %d", currentStep);
    switch (currentStep) {
            
        case ZMPAtmPaymentStepAutoFillCardInfo: {
            ZMPLog(@"--cellForRowAtIndexPath: ZMPAtmPaymentStepAutoFillCardInfo");
            //get banklist
            [ZMPPaymentManager paymentWithBill:self.bill
                                andPaymentInfo:atmCard
                                      andAppId:[[ZaloPayWalletSDKPayment sharedInstance] appID]
                                       channel:self.channel
                                        inMode:ZMPPaymentMethodTypeAtm
                                   andCallback:^(ZMPResponseObject* responseObject)
             {
                 
                 //todo
                 
             }];
            cachedCard = [self.dataManager getCachedAtmCard];
            
            if (!cachedCard) {
                currentStep = ZMPAtmPaymentStepAutoDetectBank;
                //note : will fall through this case, should not return
            } else {
                
                cell = [tableView dequeueReusableCellWithIdentifier:kATM_AUTO_FILL_INFO_CELL_ID];
                cachedCard.amount = [self.bill calculateAmount];
                
                self.title = cachedCard.bankName;
                [self.delegate paymentControllerNeedUpdateTitle];
                [(ZNPAtmAutoFillCell *)cell setATMCardInfo:cachedCard];
                ((ZNPAtmAutoFillCell *)cell).suggestAmount = self.bill.amount;
                ((ZNPAtmAutoFillCell *)cell).channel = self.channel;
                break;
            }
        }
        case ZMPAtmPaymentStepAutoDetectBank: {
            ZMPLog(@"--cellForRowAtIndexPath: ZMPAtmPaymentStepAutoDetectBank");
            cell = [tableView dequeueReusableCellWithIdentifier:kATM_AUTO_DETECT_INFO_CELL_ID];
            ((ZNPAtmAutoDetectInfoCell *)cell).hasAmount = YES;
            ((ZNPAtmAutoDetectInfoCell *)cell).suggestAmount = self.bill.amount;
            ((ZNPAtmAutoDetectInfoCell *)cell).channel = self.channel;
            [(ZNPAtmAutoDetectInfoCell *)cell setATMCardInfo:scanCard];
            
            //
            ((ZNPAtmAutoDetectInfoCell *)cell).amountField.attributedPlaceholder =
            [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Nhập số tiền", @"")
                                            attributes:@{}];
            ((ZNPAtmAutoDetectInfoCell *)cell).cardNumberField.attributedPlaceholder =
            [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Số thẻ", @"")
                                            attributes:@{}];
            ((ZNPAtmAutoDetectInfoCell *)cell).cardHolderField.attributedPlaceholder =
            [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Chủ thẻ", @"")
                                            attributes:@{}];
            ((ZNPAtmAutoDetectInfoCell *)cell).cardPassField.attributedPlaceholder =
            [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Mật mã thẻ", @"")
                                            attributes:@{}];
            ((ZNPAtmAutoDetectInfoCell *)cell).cardMonthField.attributedPlaceholder =
            [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Ngày hết hạn", @"")
                                            attributes:@{}];
            break;
        }
        case ZMPAtmPaymentStepSelectBank: {
            ZMPLog(@"--cellForRowAtIndexPath: ZMPAtmPaymentStepSelectBank");
            cell = [tableView dequeueReusableCellWithIdentifier:kATM_PINNED_VIEW_CELL_ID];
            [(ZNPAtmPinnedView *)cell updateLayoutsWithAnimation:YES];
            break;
        }
        case ZMPAtmPaymentStepWaitingWebview:{
            ZMPLog(@"--cellForRowAtIndexPath: ZMPAtmPaymentStepWaitingWebview");
            cell = (ZNPPaymentViewCell*)[tableView dequeueReusableCellWithIdentifier:kZPW_CREDIT_CARD_WEBVIEW_CELL_ID forIndexPath:indexPath];
            if (webviewUrl != nil && ![webviewUrl isEqualToString:@""]) {
                _mWebView = ((ZPWCreditCardWebviewCell*)cell).ccWebview;
                [self initWebView:((ZPWCreditCardWebviewCell*)cell).ccWebview];
                [self loadRequestWithStringUrl:((ZPWCreditCardWebviewCell*)cell).ccWebview :webviewUrl];
                webviewUrl = nil;
                [self.delegate paymentControllerNeedHiddenOkButton];
            }
            return cell;
            break;
        }
        case ZMPAtmPaymentStepAtmAuthenPayer:{
            ZMPLog(@"--cellForRowAtIndexPath: ZMPAtmPaymentStepAtmAuthenPayer");
            cell = [tableView dequeueReusableCellWithIdentifier:kZPW_ATM_OTP_CELL_ID];
            ((ZPWOtpCell*)cell).txtOtpValue.attributedPlaceholder =
            [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Mã OTP/Token", @"")
                                            attributes:@{}];
            break;
        }
        default: {
            return [[UITableViewCell alloc] init];
            //TODO (not important) - find a way to keep current layout unchanged
        }
    }
    [cell setBackgroundColor:[UIColor clearColor]];
    [cell updateLayouts];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.delegate = self;
    [cell setOCRVisibility:self.dataManager.isAtmOcrEnabled viewId:nil];
    return cell;
}

- (void)initWebView:(UIWebView*)wv {
    wv.delegate = self;
    wv.frame = self.mTableView.frame;
    wv.scalesPageToFit = YES;
    wv.multipleTouchEnabled = NO;
    wv.scrollView.scrollEnabled = NO;
    wv.scrollView.delegate = self;
    NSLog(@"wv.contentMode = UIViewContentModeScaleAspectFit");
}

#pragma mark - UIWebViewDelgate

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    ZMPLogv(@"didFailLoadWithError : %@", webView.request.URL);
    
    if (didReturned) {
        return;
    }
    
    if ([self isAuthenLoadFail:webView.request.URL.absoluteString]) {
        return;
    }
}

- (void)webViewDidFailLoadWithTimeOut:(UIWebView *)awebView{
    ZMPLogv(@"webViewDidFailLoadWithTimeOut : %@", awebView.request.URL);
    
}

-  (void) webViewDidStartLoad:(UIWebView *)webView {
    ZMPLogv(@"webViewDidStartLoad %@", webView.request.URL);
    
    NSString * js = @"var alert = function();";
    [webView stringByEvaluatingJavaScriptFromString:js];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    [self showProgressHUD];
    NSString * absoluteUrl = request.URL.absoluteString;
    ZMPLogv(@"should start load %@ main thread %@", absoluteUrl , [NSThread isMainThread] ? @"YES" : @"NO") ;
    if ([[request.URL host] isEqualToString:@"payment-complete"]) {
        currentStep = ZMPAtmPaymentStepUpdate;
        [self getStatusOfTranxId:self.atmCard.zpTransID callback:^(ZMPPaymentResponse *response) {
        }];
        return NO;
    }
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    ZMPLogv(@"webViewDidFinishLoad : %@", webView.request.URL);
    if (!webView.isLoading) {
        [self hideProgressHUD];
    }
}

- (void)loadRequestWithStringUrl:(UIWebView* )webView :(NSString *)stringUrl {
    NSURL * url = [NSURL URLWithString:stringUrl];
    ZMPLogv(@"url = %@", url);
    NSURLRequest * request = [NSURLRequest requestWithURL:url
                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                          timeoutInterval:120];
    [webView loadRequest:request];
}

- (BOOL)is123PResult:(NSString*)url{
    //http:://pay.sandbox.123pay.vn/ib/ocp/index.php/ocp/pay/payresultsucc?orderNo=123P1605070179404
    NSString *exp = @"(.*123pay\\.vn.*\\/ocp\\/pay\\/payresult.*)";
    return [self checkRegularExpression:exp url:url];
}

- (BOOL)isAuthenLoadFail:(NSString*)url{
    //http:://pay.sandbox.123pay.vn/ib/cc/index.php/index/authen
    NSString *exp = @"(.*123pay\\.vn.*cc.*\\/index\\/authen.*)";
    return [self checkRegularExpression:exp url:url];
}

- (BOOL)checkRegularExpression:(NSString *)exp url:(NSString *)url {
    NSRegularExpression *regExOtp = [[NSRegularExpression alloc]
                                     initWithPattern:exp
                                     options:NSRegularExpressionCaseInsensitive error:NULL];
    
    NSArray *matchArray = [regExOtp matchesInString:url options:0 range: NSMakeRange(0, url.length)];
    return (matchArray != NULL && matchArray.count > 0);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1) {
        return 1;
    }
    CGFloat height = [super tableView:tableView heightForRowAtIndexPath:indexPath];
    if (height > 0) return height;
    else {
        return  MAX(0, [self expectedPaymentCellHeight:tableView]);
    }
}

- (float)expectedPaymentCellHeight:(UITableView *)tableView{
    ZMPLog(@"atm currentStep: %d", self.currentStep);
    ZMPDataManager * dataManager = [ZMPDataManager sharedInstance];
    switch (self.currentStep) {
        case ZMPAtmPaymentStepSubmitTrans:
        case ZMPAtmPaymentStepAutoDetectBank:
        case ZMPAtmPaymentStepAutoFillCardInfo:{
            return [ZNPAtmAutoDetectInfoCell heightOfCellWithBankCode:self.atmCard.bankCode];
        }
        case ZMPAtmPaymentStepSelectBank:
            return [dataManager heightCellWithEnum:ZMPCellTypeAtmAutoFillDateInfo];
        case ZMPAtmPaymentStepAtmAuthenPayer:
            return 59;
        case ZMPAtmPaymentStepWaitingWebview:
            return [UIScreen mainScreen].bounds.size.height;
        default:
            break;
    }
    return 0;
}

- (ZMPPaymentMethodType)paymenMethodType {
    return ZMPPaymentMethodTypeAtm;
}

- (void)orientationChanged{
    if (self.atmPinnedView) {
        [self.atmPinnedView updateLayoutsWithAnimation:YES];
    }
}
#pragma mark - Payment cell delegate
- (void) loadCardInfoIntoCell: (ZNPPaymentViewCell *) cell {
    NSString *bankCode = atmCard.bankCode;
    if(!bankCode) return;
    
    cachedCard = [self.dataManager getCachedAtmCard];
    if(cachedCard) {
        [cell setValue:cachedCard.cardHolderName viewById:@"holdername"];
        [cell setValue:cachedCard.cardNumber viewById:@"number"];
        [cell setHighlighted:YES viewId:@"holdername"];
        [cell setHighlighted:YES viewId:@"number"];
        
        ZNPTextFieldRecordView *cardNumTextField = (ZNPTextFieldRecordView *) [cell viewwithId:@"number"];
        [cardNumTextField formatTextField];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [cardNumTextField.textField becomeFirstResponder];
        });
        
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            int offset = 0;
            for (int i = (int)cardNumTextField.textField.text.length - 1; i > 0; i--){
                if ([cardNumTextField.textField.text characterAtIndex:i] == '*'){
                    offset += 1;
                }
                
                if (offset == 4){
                    offset = (int)cardNumTextField.textField.text.length - i;
                    break;
                }
            }
            
            UITextPosition *position = [cardNumTextField.textField
                                        positionFromPosition:cardNumTextField.textField.endOfDocument
                                        offset:-offset];
            
            UITextRange *range  = [cardNumTextField.textField
                                   textRangeFromPosition:position
                                   toPosition:cardNumTextField.textField.endOfDocument];
            
            [cardNumTextField.textField setSelectedTextRange:range];
        });
    }
}



- (CGFloat) heightFromTextFieldToKeyboard {
    if (currentStep == ZMPAtmPaymentStepSubmitTrans){
        return 10;
    } else {
        return 55;
    }
}

#pragma mark - Private methods
- (ZNPPaymentViewCell *) paymentViewCellWithType: (ZMPCellType) type {
    ZNPPaymentViewCell* cell = [self.mTableView dequeueReusableCellWithIdentifier:
                                [[ZMPDataManager sharedInstance] reuseIdCellWithEnum:type]];
    
    if (cell == NULL) {
        cell = (ZNPPaymentViewCell*)[[ZMPDataManager sharedInstance] cellWithEnum:type];
    }
    
    NSString * amount = [NSString zmpFormatNumberWithDilimeter:self.atmCard.amount];
    [cell setValue:amount viewById:@"chargeamount"];
    
    ZMPChannel * channel = [[ZMPDataManager sharedInstance].paymentChannel
                            channelWithType:ZMPPaymentMethodTypeAtm];
    
    [cell setObject:channel viewId:@"chargeamount"];
    
    return cell;
}

- (void *)clearTextFieldInView:(UIView *)view {
    for ( UIView *childView in view.subviews ) {
        if ([childView isKindOfClass:[UITextField class]] ){
            ((UITextField *)childView).text = nil;
        }
        [self clearTextFieldInView:childView];
    }
    return nil;
}

- (void)processPayment {
    [self dismissKeyboard];
    didReturned = false;
    
    ZNPPaymentViewCell* cell = (ZNPPaymentViewCell*)
    [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    //time out
    NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
    if(atmStartTimestamp > 0 && now - atmStartTimestamp > kATM_TRANSATION_TIMEOUT) {
        NSString  *message = [self.dataManager messageForKey:@"message_atm_time_out"];
        [self finishWithErrorCode:0 message:message
                           status:kZMPZingPayCreditStatusCodeFail result:-1 errorStep:ZMPErrorStepNonRetry];
        didReturned = true;
        return;
    }
    self.atmCard.step = currentStep;
    ZMPLog(@"processPayment currentStep: %d", currentStep);
    switch (currentStep) {
        case ZMPAtmPaymentStepAutoDetectBank:{
            ZMPLog(@"--processPayment: ZMPAtmPaymentStepAutoDetectBank");
            NSMutableDictionary * params = [[cell outputParams] mutableCopy];
            atmCard.bankCode = params[@"bankcode"];
            
            
            if (atmCard.bankCode.length <= 0) {
                [self showAtmPinView];
                return;
            }
            
            if (((ZNPAtmAutoDetectInfoCell *)cell).bankCode.length > 0) {
                currentStep = ZMPAtmPaymentStepSubmitTrans;
                [self processPayment];
            }
            return;
        }
        case ZMPAtmPaymentStepSubmitTrans:{
            ZMPLog(@"--processPayment: ZMPAtmPaymentStepCreateOrder");
            [self prepareForPayment];
            NSMutableDictionary * params = [[cell outputParams] mutableCopy];
            atmCard.bankCode = params[@"bankcode"];
            if (atmCard.bankCode.length <= 0) {
                [self showAtmPinView];
                return;
            }
            if(![cell checkValidate]) return;
            
            atmCard.bankName = params[@"bankname"];
            atmCard.cardHolderName = params[@"holdername"];
            atmCard.cardNumber = params[@"number"];
            atmCard.validFrom = params[@"validfrom"];
            atmCard.validTo = params[@"validto"];
            atmCard.type = params[@"type"];
            atmCard.otpType = params[@"otptype"];
            atmCard.cardPassword = params[@"password"];
            atmCard.amount = (long)[params[@"chargeamount"] longLongValue];
            
            [cell cacheInfo];
            [self submitTran];
            
            break;
        }
        case ZMPAtmPaymentStepAtmAuthenPayer:{
            ZMPLog(@"--processPayment: ZMPAtmPaymentStepAtmAuthenPayer");
            if (atmCard == NULL) {
                return;
            }
            
            NSMutableDictionary * params = [[cell outputParams] mutableCopy];
            atmCard.authenValue = params[@"otp"];
            
            NSString * testOTP = [atmCard.dynamicParams objectForKey:@"authenvalue"];
            if (testOTP && [testOTP isEqualToString:@"14411441"]){
                [self saveCardInfo];
            }
            [self showProgressHUD];
            
            [ZMPPaymentManager paymentWithBill:self.bill
                                andPaymentInfo:atmCard
                                      andAppId:[[ZaloPayWalletSDKPayment sharedInstance] appID]
                                       channel:self.channel
                                        inMode:ZMPPaymentMethodTypeAtm
                                   andCallback:^(ZMPResponseObject* responseObject)
             {
                 [self hideProgressHUD];
                 ZMPPaymentResponse* response = (ZMPPaymentResponse*) responseObject;
                 
                 if(response.errorCode == kZMPZingPayCreditErrorCodeNoneError) {
                     
                 }
                 
                 [self handlePaymentResponse:response];
             }];
            break;
        }
        case ZMPAtmPaymentStepSavedCardConfirm:{
            ZMPLog(@"processPayment, _currentStep: ZMPAtmPaymentStepSavedCardConfirm");
            [self submitTran];
            break;
        }
            
            
        default:
            break;
    }
}


- (void)submitTran {
    [self showProgressHUD];
    self.atmCard.step = currentStep;
    [ZMPPaymentManager paymentWithBill:self.bill
                        andPaymentInfo:atmCard
                              andAppId:[[ZaloPayWalletSDKPayment sharedInstance] appID]
                               channel:self.channel
                                inMode:ZMPPaymentMethodTypeAtm
                           andCallback:^(ZMPResponseObject* responseObject)
     {
         
         atmStartTimestamp = [[NSDate date] timeIntervalSince1970];
         ZMPPaymentResponse* response = (ZMPPaymentResponse *)responseObject;
         
         if (response.errorCode >= 1) {
             [ZMPUserDefaults setObject:@"Create Oder" forKey:kZMP_ATM_CACHED_CARD_ORDER];
             [ZMPUserDefaults synchronize];
             [self saveAtmCard:self.atmCard];
             self.atmCard.zpTransID = response.zpTransID;
             
             if ([[response.nextAction lowercaseString] isEqualToString:@"authen"]) {
                 [self hideProgressHUD];
                 NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:[response.data dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:NULL];
                 
                 if ([[dictionary objectForKey:@"actiontype" ] intValue] == 1) {
                     
                     currentStep = ZMPAtmPaymentStepWaitingWebview;
                     NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:[response.data dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:NULL];
                     webviewUrl = [dictionary objectForKey:@"redirecturl"];
                 } else {
                     currentStep = ZMPAtmPaymentStepAtmAuthenPayer;
                 }
                 
                 [self.delegate paymentControllerNeedUpdateLayouts];
                 
             }else{
                 ZMPLog(@"processPayment not authen", response.zpTransID);
                 [self hideProgressHUD];
                 ZMPPaymentResponse* pResponse = [[ZMPPaymentResponse alloc] init];
                 pResponse.errorCode = responseObject.errorCode;
                 pResponse.message = responseObject.message;
                 pResponse.exchangeStatus = kZMPZingPayCreditStatusCodeSuccess;
                 pResponse.exchangeResult = -1;
                 pResponse.errorStep = responseObject.errorStep;
                 pResponse.paymentMethod = ZMPPaymentMethodTypeCreditCard;
                 pResponse.chargedAmount = response.chargedAmount;
                 pResponse.zpTransID = response.zpTransID;
                 [self handlePaymentResponse:pResponse];
             }
             
         } else{
             [self hideProgressHUD];
             ZMPPaymentResponse* pResponse = [[ZMPPaymentResponse alloc] init];
             pResponse.errorCode = responseObject.errorCode;
             pResponse.message = responseObject.message;
             pResponse.exchangeStatus = kZMPZingPayCreditStatusCodeFail;
             pResponse.exchangeResult = -1;
             pResponse.errorStep = responseObject.errorStep;
             pResponse.zpTransID = response.zpTransID;
             [self handlePaymentResponse:pResponse];
         }
     }];
}


- (void)getStatusOfTranxId:(NSString *)tranxId
                  callback:(void (^)(ZMPPaymentResponse *response))callback
{
    ZMPLog(@"start getStatusOfTranxId: %@", tranxId);
    [self showProgressHUD];
    if (!self.atmCard) {
        [self hideProgressHUD];
        return;
    }
    self.atmCard.step = currentStep;
    [ZMPPaymentManager paymentWithBill:self.bill
                        andPaymentInfo:self.atmCard
                              andAppId:[[ZaloPayWalletSDKPayment sharedInstance] appID]
                               channel:self.channel
                                inMode:ZMPPaymentMethodTypeAtm
                           andCallback:^(ZMPResponseObject* responseObject)
     {
         // process callback
         [self hideProgressHUD];
         ZMPPaymentResponse* response = (ZMPPaymentResponse*) responseObject;
         response.paymentMethod = ZMPPaymentMethodTypeAtm;
         response.zpTransID = tranxId;
         [self handlePaymentResponse:response];
     }];
}

- (void) finishWithErrorCode: (int) errorCode
                     message: (NSString *) message
                      status: (int) status
                      result: (int) result
                   errorStep: (enum ZMPErrorStep) errorStep
{
    if (checkStepTimeOutTimer) {
        [checkStepTimeOutTimer invalidate];
    }
    [self hideProgressHUD];
    ZMPPaymentResponse* pResponse = [[ZMPPaymentResponse alloc] init];
    pResponse.errorCode = errorCode;
    pResponse.message = message;
    pResponse.exchangeStatus = status;
    pResponse.exchangeResult = result;
    pResponse.errorStep = errorStep;
    [self handlePaymentResponse:pResponse];
}

#pragma mark - ZMPProgressHUD
- (void)showProgressHUD {
    [ZMPProgressHUD showWithStatus:[[ZMPDataManager sharedInstance]
                                    messageForKey:@"message_accounce_atm_in_processing"]];
    [NSTimer scheduledTimerWithTimeInterval:30
                                     target:self
                                   selector:@selector(hideProgressHUD)
                                   userInfo:nil repeats:NO];
    
}
- (void)hideProgressHUD {
    [ZMPProgressHUD dismiss];
}

- (void)handlePaymentResponse:(ZMPPaymentResponse*) response {
    if (checkStepTimeOutTimer) {
        [checkStepTimeOutTimer invalidate];
    }
    response.paymentMethod = ZMPPaymentMethodTypeAtm;
    if (response.errorStep == ZMPErrorStepAbleToRetry) {
        if (currentStep == ZMPAtmPaymentStepSubmitTrans){
            currentStep = ZMPAtmPaymentStepSubmitTrans;
        }
    }
    [super handlePaymentResponse:response];
}

- (void)prepareForPayment {
    NSTimeInterval lastTime = [ZMPUserDefaults doubleForKey:ZNP_ATM_CACHED_CARD_LAST_UPDATE_KEY];
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    if (lastTime + ZNP_ATM_CACHED_CARD_INTERVAL < currentTime) {
        [ZMPUserDefaults removeObjectForKey:ZNP_ATM_CACHED_CARD_KEY];
        [ZMPUserDefaults synchronize];
    }
    
}

#pragma mark - CardInfo

- (void)saveAtmCard:(ZMPAtmCard *)card {
    NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithDictionary:[ZMPUserDefaults dictionaryForKey:ZNP_ATM_CACHED_CARD_KEY]];
    NSData * data = [NSKeyedArchiver archivedDataWithRootObject:self.atmCard];
    
    [dict zmpSetObject:data forKey:kZNP_ATM_CACHED_CARD_KEY];
    
    [ZMPUserDefaults setDouble:[[NSDate date] timeIntervalSince1970] forKey:ZNP_ATM_CACHED_CARD_LAST_UPDATE_KEY];
    [ZMPUserDefaults setObject:dict forKey:ZNP_ATM_CACHED_CARD_KEY];
    [ZMPUserDefaults synchronize];
}

- (ZMPAtmCard *)getSavedAtmCard {
    NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithDictionary:[ZMPUserDefaults dictionaryForKey:ZNP_ATM_CACHED_CARD_KEY]];
    NSData * data = [dict objectForKey:kZNP_ATM_CACHED_CARD_KEY];
    if (data) {
        ZMPAtmCard * card = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        [dict removeObjectForKey:kZNP_ATM_CACHED_CARD_KEY];
        [ZMPUserDefaults setObject:dict forKey:ZNP_ATM_CACHED_CARD_KEY];
        [ZMPUserDefaults synchronize];
        ZMPLog(@"getSavedAtmCard", card.bankName);
        return card;
    }
    
    return nil;
}

- (void) saveCardInfo {
    if(atmCard && atmCard.cardNumber.length > 4) {
        
        atmCard.cardNumberHash = [NSString zmpAES128EncryptString:atmCard.cardNumber
                                                          withKey:kATMCardNumberHashSecretKey];
        NSString * cardNum = [atmCard.cardNumber copy];
        atmCard.cardNumber = [atmCard.cardNumber
                              stringByReplacingCharactersInRange:NSMakeRange(atmCard.cardNumber.length-4, 4)
                              withString:@"xxxx"];
        [self.dataManager saveATMCard:atmCard];
        atmCard.cardNumber = cardNum;
    }
}

- (void) deleteCardInfo {
    if(atmCard && atmCard.bankCode.length > 0) {
        [self.dataManager deleteSavedAtmCard];
    }
}

- (void)updatePortraitBillDiscriptionCell{
    ZNPBillDescriptionCell * desciptionCell =  (ZNPBillDescriptionCell *)[self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    [self updatePortraitBillDiscriptionCell:desciptionCell];
    
    [self.mTableView reloadRowsAtIndexPaths:0 withRowAnimation:UITableViewRowAnimationNone];
}

- (void)updatePortraitBillDiscriptionCell:(ZNPBillDescriptionCell *)desciptionCell{
    desciptionCell.textViewTopConstraint.constant = 0;
    desciptionCell.textViewBottomConstraint.constant = 0;
    [desciptionCell setNeedsUpdateConstraints];
}

#pragma mark - ZNPAPaymentViewCellDelegate
- (void)detectCell:(ZNPPaymentViewCell *)cell didDetectBank:(NSDictionary *)bank {
    ZMPLog(@"detectCell didDetectBank: %@", bank);
    if (bank) {
        self.title = [bank objectForKey:@"name"];
        self.atmCard.bankCode = ((ZNPAtmAutoDetectInfoCell *)cell).bankCode;
        self.atmCard.bankName = [bank objectForKey:@"name"];
    }else{
        self.title = @"thẻ ATM";
        self.atmCard.bankCode = nil;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(paymentControllerNeedUpdateTitle)]) {
        [self.delegate paymentControllerNeedUpdateTitle];
    }
    [self updatePortraitBillDiscriptionCell];
}

- (void)detectCell:(ZNPPaymentViewCell *)cell didEditCardNumberWithDetectedBank:(NSDictionary *)bank{
    if (!bank) {
        [self showAtmPinView];
    }
}

- (void)showAtmPinView{
    [self dismissKeyboard];
    if (!isAbleToShowAtmPinView) {
        isAbleToShowAtmPinView = YES;
        return;
    }
    if (!self.atmPinnedView) {
        self.atmPinnedView = [[ZNPAtmPinnedView alloc] init];
    }
    self.atmPinnedView.delegate = self;
    [self.atmPinnedView showInView:self.zmpParentController.view];
    
    //hide keyboard
    ZNPAtmAutoDetectInfoCell * cell = (ZNPAtmAutoDetectInfoCell *)
    [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    [cell performSelector:@selector(endEditing:) withObject:@YES afterDelay:0.1f];
}

- (void)pinnedCell:(ZNPPaymentViewCell *)cell didSelectBank:(ZMPBank *)bank {
    if (bank && self.delegate
        && [self.delegate respondsToSelector:@selector(paymentControllerNeedUpdateTitle)])
    {
        self.title = bank.bankName;
        [self.delegate paymentControllerNeedUpdateTitle];
        self.atmCard.bankCode = bank.bankCode;
        self.atmCard.bankName = bank.bankName;
        
        ZNPAtmAutoDetectInfoCell* cardDetectCell = (ZNPAtmAutoDetectInfoCell*)
        [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
        
        if (cardDetectCell) {
            [cardDetectCell setBankCode:bank.bankCode];
            [cardDetectCell.cardHolderField becomeFirstResponder];
        }
        
        [self updatePortraitBillDiscriptionCell];
    }
}

- (void)onClickCameraButtonInCell:(ZNPPaymentViewCell *)view {
    if(![ZMPOCRViewController isCameraAvailable]) return;
    
    if (![self hasCameraPermission]) return;
    
    ((ZNPAtmAutoDetectInfoCell *)view).suggestAmount = self.bill.amount;
    
    CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
    scanViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self.delegate paymentControllerShowScanView:scanViewController];
}

#pragma mark - CardIOPaymentViewControllerDelegate

- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    NSLog(@"Scan succeeded with info: %@", info);
    // Do whatever needs to be done to deliver the purchased items.
    [self.delegate paymentControllerDismissScanView];
    ;
    NSLog(@"%@", [NSString stringWithFormat:@"Received card info. Number: %@, expiry: %02lu/%lu, cvv: %@.", info.redactedCardNumber, (unsigned long)info.expiryMonth, (unsigned long)info.expiryYear, info.cvv]);
}

- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    NSLog(@"User cancelled scan");
    [self.delegate paymentControllerDismissScanView];
}

- (void)autoFillCellDidClickChangeButton {
    currentStep = ZMPAtmPaymentStepAutoDetectBank;
    self.atmCard.bankCode = nil;
    self.title = @"Thanh toán bằng ATM";
    [self.delegate paymentControllerNeedUpdateTitle];
    [self.delegate paymentControllerNeedUpdateLayouts];
}

@end
