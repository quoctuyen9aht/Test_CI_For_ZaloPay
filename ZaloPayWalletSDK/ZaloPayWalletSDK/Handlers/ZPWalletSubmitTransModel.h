//
//  ZPWalletSubmitTransModel.h
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/18/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPSubmiTransBaseModel.h"

@interface ZPWalletSubmitTransModel : ZPSubmiTransBaseModel
+ (RACSignal *)submiTransWithPassword:(NSString *)password
                                 bill:(ZPBill *)bill
                            channelId:(int)channelID;
@end
