//
//  ZPCreditCardHandler+Vertify.m
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 4/20/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPCreditCardHandler+Vertify.h"
#import "ZPPaymentManager.h"
#import "ZPDataManager.h"
#import "ZaloPayWalletSDKPayment.h"
#import "ZPPaymentResponse.h"
#import "ZPMiniBank.h"
#import "ZPBill.h"
#import "NSString+ZPExtension.h"
#import "ZaloPayWalletSDKLog.h"
#import "ZaloPayWalletSDKPayment.h"
#import "ZPBank.h"

@implementation ZPCreditCardHandler (Vertify)
#pragma mark - Vertify

- (void)verifyCardForMappingUsing:(ZPCreditCardHandlerModel *)model {

    if (![[ZPDataManager sharedInstance] checkInternetConnect]) {
        [self showAlertNotifyNetWorkProblem];
        return;
    }
    [self showProgressHUD];
    self.channel = [self.dataManager channelWithTypeAndBankCode:ZPPaymentMethodTypeCreditCard bankCode:stringCreditCardBankCode];
    DDLogInfo(@"call verrifyCardForMapping");
    __weak typeof(self) wSelf = self;
    [ZPPaymentManager paymentWithBill:self.bill
                       andPaymentInfo:self.creditCard
                             andAppId:self.bill.appId
                              channel:self.channel
                               inMode:ZPPaymentMethodTypeCreditCard
                          andCallback:^(ZPResponseObject *responseObject) {
                              // ????
                              ZPPaymentResponse *response = [ZPPaymentResponse castFrom:responseObject];
                              
                              //Set 6 first number card
                              NSString *first6No = self.creditCard.cardNumber.length > 6 ? [self.creditCard.cardNumber substringToIndex:6]: @"";
                              response.first6NumberCard = first6No;
                              
                              DDLogInfo(@"zpTransID: %@", response.zpTransID);
                              if ([response.zpTransID length] > 0) {
                                  wSelf.creditCard.zpTransID = response.zpTransID;
                              }

                              if (![wSelf vertifyNextStepFrom:responseObject with:model]) {
                                  wSelf.currentStep = ZPCcPaymentStepUpdate;
                                  [wSelf.delegate paymentControllerNeedUpdateLayouts];
                                  [wSelf performSelector:@selector(processPayment) withObject:wSelf afterDelay:1];
                                  return;
                              }

                              model.ccStartTimestamp = [[NSDate date] timeIntervalSince1970];
                              [wSelf hideProgressHUD];
                              [wSelf processResponsePayment:response with:model];
                          }];
}

#pragma mark - SubmitTrans

- (void)submitTransUsing:(ZPCreditCardHandlerModel *)model {
    if (![[ZPDataManager sharedInstance] checkInternetConnect]) {
        [self showAlertNotifyNetWorkProblem];
        return;
    }
    NSString *ccBankCode = self.paymentMethodDidChoose.savedCard.bankCode ?: stringCreditCardBankCode;
    if (self.paymentMethodDidChoose == nil) {
        self.channel = [self.dataManager channelWithTypeAndBankCode:ZPPaymentMethodTypeCreditCard bankCode:ccBankCode];
    }
    [self showProgressHUD];
    // set payment channel to zalopaywallet while using withdraw
    if (self.bill.transType == ZPTransTypeWithDraw) {
        self.channel = [self.dataManager channelWithTypeAndBankCode:ZPPaymentMethodTypeZaloPayWallet bankCode:ccBankCode];
    }
    [[ZPAppFactory sharedInstance].orderTracking trackChoosePayMethod:self.bill.appTransID pcmid:ZPPaymentMethodTypeCreditCard bankCode:ccBankCode result:OrderStepResult_Success];

    DDLogInfo(@"processPayment, call paymentWithBill");
    __weak typeof(self) wSelf = self;
    [ZPPaymentManager paymentWithBill:self.bill
                       andPaymentInfo:self.creditCard
                             andAppId:self.bill.appId
                              channel:self.channel
                               inMode:ZPPaymentMethodTypeCreditCard
                          andCallback:^(ZPResponseObject *responseObject) {
                              ZPPaymentResponse *response = (ZPPaymentResponse *) responseObject;
                              DDLogInfo(@"zpTransID: %@", response.zpTransID);
                              if ([response.zpTransID length] > 0) {
                                  self.creditCard.zpTransID = response.zpTransID;
                              }

                              if (![wSelf vertifyNextStepFrom:responseObject with:model]) {
                                  wSelf.currentStep = ZPCreditCardPaymentStepStepGetStatusByAppTransIdForClient;
                                  [wSelf.delegate paymentControllerNeedUpdateLayouts];
                                  [wSelf performSelector:@selector(processPayment) withObject:self afterDelay:1];
                                  return;
                              }

                              model.ccStartTimestamp = [[NSDate date] timeIntervalSince1970];
                              DDLogInfo(@"processPayment, response errorCode: %d", response.errorCode);
                              DDLogInfo(@"zpTransID: %@", self.creditCard.zpTransID);
                              DDLogInfo(@"cc submitTran balance %lld", response.balance);

                              [wSelf hideProgressHUDWithResultCode:response.errorCode];
                              if (response.errorCode == kZPZaloPayCreditErrorCodePinRetry) {
                                  NSString *message = response.message;
                                  [wSelf showAlertNotifyRetryPin:message];
                              } else {
                                  // add pin to touchid
                                  if (response.errorCode == 1 && [self isRequirePin] && [ZaloPayWalletSDKPayment sharedInstance].appDependence) {
                                      [[ZaloPayWalletSDKPayment sharedInstance].appDependence savePassword:self.creditCard.pin];
                                  }
                                  [wSelf processResponsePayment:response with:model];
                              }
                          }];
}

#pragma mark - helper

- (BOOL)vertifyNextStepFrom:(ZPResponseObject *)responseObject with:(ZPCreditCardHandlerModel *)model {
    if (!validObject(responseObject, [ZPResponseObject class])) {
        return YES;
    }
    return !((responseObject.errorCode == kZPZaloPayCreditErrorCodeRequestTimeout || responseObject.errorCode == ZPServerErrorCodeTransIdNotExist) && self.currentStep != ZPCreditCardPaymentStepStepGetStatusByAppTransIdForClient);
}

- (void)saveTimeOrder {
    self.creditCard.ts = [[NSDate date] timeIntervalSince1970];
    [self saveCreditCard:self.creditCard];
}

- (ZPPaymentResponse *)createPaymentResponseFrom:(ZPPaymentResponse *)response {
    ZPPaymentResponse *pResponse;
    if (response.errorCode >= 1) {
        [self saveTimeOrder];
        if (response.isProcessing && response.data != nil && ![response.data isEqualToString:@""]) {
            return pResponse;
        }
        BOOL isCreditCardUndefine = response.exchangeStatus == kZPZaloPayCreditStatusCodeUnidentified;
        pResponse = [[ZPPaymentResponse alloc] initWithPaymentResponse:response];
        pResponse.exchangeStatus = isCreditCardUndefine ? kZPZaloPayCreditStatusCodeUnidentified : kZPZaloPayCreditStatusCodeSuccess;
        pResponse.paymentMethod = ZPPaymentMethodTypeCreditCard;
    } else {
        pResponse = [[ZPPaymentResponse alloc] initWithPaymentResponse:response];
        pResponse.exchangeStatus = kZPZaloPayCreditStatusCodeFail;
    }
    pResponse.zpTransID = self.creditCard.zpTransID;
    pResponse.first6NumberCard = response.first6NumberCard;
    return pResponse;
}

- (void)processResponsePayment:(ZPPaymentResponse *)response with:(ZPCreditCardHandlerModel *)model {
    // validate
    if (!response) {
        return;
    }
    ZPPaymentResponse *nResponse = [self createPaymentResponseFrom:response];
    if (nResponse) {
        [self handlePaymentResponse:nResponse];
    } else {
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:[response.data dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:NULL];
        self.currentStep = ZPCcPaymentStepWaitingWebview;
        model.webviewUrl = [NSString castFrom:[dictionary objectForKey:@"redirecturl"]];
        [self.delegate paymentControllerNeedUpdateLayouts];
        self.isBankLoadWebview = YES;
    }
}

- (void)saveCreditCard:(ZPCreditCard *)card {
    card.isMapcard = [self isAtmMapcard];
    [ZPDataManager sharedInstance].tempSavedCard = card;
}

#pragma mark - handler alert from bank

- (ZPCreditCardAlertType)createTypeAlertFrom:(ZPBank *)bank
                                        with:(ZPCreditCardHandlerModel *)model
{
    ZPMiniBank *miniBank = [[ZPDataManager sharedInstance].bankManager getBankFunctionIsSupportTranType:bank withTranType:self.bill.transType];
    
    if (bank.status != ZPBankStatusEnable && !model.isShowDialogMaintenance) {
        return ZPCreditCardAlertTypeBankMaintain;
    }
    
    if ([[ZPDataManager sharedInstance] chekMinAppVersion:bank.minAppVersion]) {
        return ZPCreditCardAlertTypeMinAppVersion;
    }

    if (miniBank == nil || miniBank.minibankStatus == ZPMiniBankStatus_Disable) {
        return ZPCreditCardAlertTypeBankNotSupport;
    }


    if (miniBank != nil && miniBank.minibankStatus == ZPMiniBankStatus_Maintenance) {
        return ZPCreditCardAlertTypeBankMaintainWithTime;
    }

    return ZPCreditCardAlertTypeUnknown;

}

- (NSString *)makeMessageWith:(ZPCreditCardAlertType)type
                   detectBank:(ZPBank *)bank
{
    NSString *result;
    switch (type) {
        case ZPCreditCardAlertTypeBankMaintain:{
            NSString *strMaintain = bank.maintenanceMsg;
            long long timeStamp = bank.maintainenanceTo;
            
            if(strMaintain == nil || strMaintain.length == 0){
                strMaintain = [NSString stringWithFormat:[self.dataManager messageForKey:@"message_bank_maintain"], bank.bankName];
            }
            result = [strMaintain stringByReplacingOccurrencesOfString:@"%s"
                                                            withString:[NSString timeString:timeStamp]];
        }
            break;
        case ZPCreditCardAlertTypeMinAppVersion:{
            result = [NSString stringWithFormat:@"Quý khách vui lòng nâng cấp phiên bản mới nhất để thực hiện %@ với %@",[self.dataManager getTitleByTranstype:self.bill.transType], bank.bankName];
        }
            break;
        case ZPCreditCardAlertTypeBankNotSupport:{
            NSString *bankName = [bank.bankName hasPrefix: @"NH "] ? [bank.bankName substringFromIndex:3] :bank.bankName;

            NSString *typeStr = [self.dataManager getTitleByTranstype:self.bill.transType];

            result = [NSString stringWithFormat:@"%@ chưa hỗ trợ %@, vui lòng chọn ngân hàng khác hoặc quay lại sau.", bankName, typeStr];
        }
            break;
        case ZPCreditCardAlertTypeBankMaintainWithTime:{
            ZPMiniBank *miniBank = [[ZPDataManager sharedInstance].bankManager getBankFunctionIsSupportTranType:bank withTranType:self.bill.transType];
            result = miniBank.maintenancemsg;
            if (result.length == 0) {
                NSString *bankName = [bank.bankName hasPrefix: @"NH "] ? [bank.bankName substringFromIndex:3] :bank.bankName;
                NSString *typeStr = [self.dataManager getTitleByTranstype:self.bill.transType];
                NSString *timeString = [NSString timeString:miniBank.maintenanceto];

                result = [NSString stringWithFormat:@"%@ bảo trì %@ tới %@, vui lòng chọn ngân hàng khác hoặc quay lại sau.", bankName, typeStr, timeString];
            }

        }
            break;

        default:
            break;
    }
    return result;
}

- (BOOL)alertWithDetectBank:(ZPBank *)bank
                       with:(ZPCreditCardHandlerModel *)model
          excuteActionFirst:(void (^)(void))action
                    handler:(void (^)(NSInteger idx, ZPCreditCardAlertType type))handler {
    ZPCreditCardAlertType typeAlert = [self createTypeAlertFrom:bank
                                                           with:model];
    NSString *message = [self makeMessageWith:typeAlert
                                   detectBank:bank];
    if ([message length] > 0) {
        if (action) {
            action();
        }
        [self showAlertNotifyWith:message buttonTitles:@[[R string_ButtonLabel_Close]] handler:^(NSInteger idx) {
            if (handler) {
                handler(idx, typeAlert);
            }
        }];
        return YES;
    }

    return NO;
}
@end

#pragma mark - Model

@implementation ZPCreditCardHandlerModel
@end
