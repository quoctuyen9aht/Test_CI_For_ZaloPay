//
//  ZPBIDVIBankingHandler.m
//  ZaloPayWalletSDK
//
//  Created by thi la on 5/8/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import "ZPBIDVIBankingHandler.h"
#import "ZPConfig.h"
#import "ZPPaymentInfo.h"
#import "ZPBill.h"
#import "ZaloPayWalletSDKPayment.h"

@implementation ZPBIDVIBankingHandler

- (void)initialization {
    [super initialization];
    self.currentStep = ZPAtmPaymentStepSubmitTrans;
    [self processMapcard];
}
- (void) processMapcard {
    self.atmCard.bankCode = BIDV;
    self.atmCard.step = self.currentStep;
    [self updateBillMapAccount];
    [self verifyCardForMapping];
}

- (void)updateBillMapAccount {
    if ([self.bill isKindOfClass:[ZPIBankingAccountBill class]]) {
        return;
    }
    self.bill = [[ZaloPayWalletSDKPayment sharedInstance].appDependence mapAccountBill:self.atmCard.bankCode];
}

- (UIView *)configFooter {
    return nil;
}

- (void)setupButtonRegister:(UIWebView *)wv {    
}
@end
