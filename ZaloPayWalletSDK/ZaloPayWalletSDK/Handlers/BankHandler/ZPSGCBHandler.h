//
//  ZPSGCBHandler.h
//  ZaloPayWalletSDK
//
//  Created by thi la on 6/6/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//


#import "ZPBankHandlerBase.h"
#import "ZPPaymentMethod.h"

@interface ZPSGCBHandler : ZPBankHandlerBase<UITableViewDelegate, UITableViewDataSource>

@end
