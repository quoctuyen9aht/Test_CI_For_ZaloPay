//
//  ZPBIDVHandler.m
//  ZaloPayWalletSDK
//
//  Created by thi la on 6/5/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPBIDVHandler.h"
#import <JavaScriptCore/JavaScriptCore.h>
#import "ZPPaymentInfo.h"
#import "ZPSmartLinkHelper.h"
#import "ZPBill.h"
#import "ZPDataManager.h"
#import "ZPPaymentChannel.h"
#import "ZPPaymentManager.h"
#import "ZaloPayWalletSDKPayment.h"
#import "ZPAtmResponseObject.h"
#import "ZPPaymentResponse.h"
#import "ZPBank.h"

#import "ZPResourceManager.h"

#import "ZPCreditCardWebviewCell.h"
#import "ZPATMSaveCardCell.h"
#import "ZPOtpCell.h"

#import "ZPATMHandlerWebModel.h"
#import "NSHTTPCookieStorage+ZPExtension.h"
#import "ZaloPayWalletSDKLog.h"
#import "ZPMapCardLog.h"


extern NSString *kBIDVHelpUrl;

@interface ZPBIDVHandler () <UIWebViewDelegate>

@property(strong, nonatomic) UIWebView *mWebView;

@property(strong, nonatomic) NSIndexPath *checkedIndexPath;
@property(strong, nonatomic) NSTimer *checkWebViewTimeOutTimer;
@property(strong, nonatomic) NSTimer *checkStepTimeOutTimer;
@property(strong, nonatomic) NSString *webviewUrl;
@property(strong, nonatomic) NSArray *arrList;
@property(strong, nonatomic) NSURLRequest *urlRequest;
@property(strong, nonatomic) NSMutableArray *arrRequested;

@property(assign, nonatomic) bool isRedirecatble;
@property(assign, nonatomic) BOOL isEnabledClickButton;
@end

@implementation ZPBIDVHandler
@synthesize currentStep, atmCard, paymentMethodDidChoose;


- (void)dealloc {
    [_mWebView stopLoading];
    [_mWebView setDelegate:nil];
    [_mWebView removeFromSuperview];
    UITableViewCell *cell =
            [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    if ([cell isKindOfClass:[ZPPaymentViewCell class]]) {
        ZPPaymentViewCell *paymentCell = (ZPPaymentViewCell *) cell;
        if (paymentCell.delegate == self) {
            paymentCell.delegate = nil;
        }
        if ([paymentCell isKindOfClass:[ZPCreditCardWebviewCell class]] && ((ZPCreditCardWebviewCell *) paymentCell).ccWebview.delegate == self) {
            ((ZPCreditCardWebviewCell *) paymentCell).ccWebview.delegate = nil;
            ((ZPCreditCardWebviewCell *) paymentCell).ccWebview = nil;
        }
    }
    [[NSHTTPCookieStorage new] zpRemoveAllStoredCredentials:self.arrRequested];
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.arrRequested = [NSMutableArray new];
        atmCard = [[ZPAtmCard alloc] init];
        currentStep = ZPAtmPaymentStepAutoDetectBank;
        self.didReturned = false;
        self.isRedirecatble = true;
        self.model = [[ZPATMHandlerWebModel alloc] initWithStartEvent:nil
                                                                  end:nil
                                                                 with:self.mWebView];

        NSDictionary *dictionaryAgent = [[NSDictionary alloc] initWithObjectsAndKeys:
                @"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36", @"User-Agent", nil];
        [[NSUserDefaults standardUserDefaults] registerDefaults:dictionaryAgent];
    }
    return self;
}

#pragma mark - Overrides

- (void)initialization {
    [super initialization];
    atmCard.amount = self.bill.amount;
}

- (void)viewWillBack {
    [self handleCellWillBack];
}

- (BOOL)okButtonEnabled {
    return self.isEnabledClickButton || currentStep == ZPAtmPaymentStepSavedCardConfirm;
}

- (NSString *)paymentControllerTitle {
    return [self.dataManager getTitleByTranstype:self.bill.transType];
}

- (NSString *)okButtonTitle {
    return (currentStep == ZPAtmPaymentStepSavedCardConfirm) ?
            [[self.dataManager getButtonTitle:@"confirm"] capitalizedString] :
            [[self.dataManager getButtonTitle:@"continue"] capitalizedString];
}


#pragma mark - UITableViewDelegate & DataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    DDLogInfo(@"cellForRowAtIndexPath currentStep: %ld", (long) currentStep);

    if (currentStep != ZPAtmPaymentStepAutoDetectBank) {
        // Hide btn PreNext On ZPATMSaveCardCell
        [self notifyHideNextButton];
    }

    UITableViewCell *nCell = [self createCellAt:indexPath with:tableView];
    ZPPaymentViewCell *cell = [ZPPaymentViewCell castFrom:nCell];
    if (cell) {
        self.isEnabledClickButton = NO;
        [cell updateLayouts];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
    }
    return nCell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([indexPath row] == ((NSIndexPath *) [[tableView indexPathsForVisibleRows] lastObject]).row) {
        //  DDLogInfo(@"load completed");

    }
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell isKindOfClass:[ZPCreditCardWebviewCell class]]) {
        ((ZPCreditCardWebviewCell *) cell).ccWebview.delegate = nil;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1) {
        return 0;
    }
    if (currentStep == ZPAtmPaymentStepAutoDetectBank) {
        if (indexPath.section == 1) {
            return 0;
        }
    }
    CGFloat height = [super tableView:tableView heightForRowAtIndexPath:indexPath];

    if (height > 0) {
        return height;
    } else {
        return MAX(0, [self expectedPaymentCellHeight:tableView]);
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (currentStep == ZPAtmPaymentStepSavedCardConfirm) {
        if ([self isRequirePin]) {
            return 1;
        }
        ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:self.atmCard.bankCode];
        if (bank.otpType.length > 0 && [[bank.otpType componentsSeparatedByString:@"|"] count] >= 2) {
            return 2;
        }
    }
    if (currentStep == ZPAtmPaymentStepAutoDetectBank) {
        return 2;
    }
    return 1;
}

// Hidden Accept Button
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == tableView.numberOfSections - 1) {
        if ((currentStep == ZPAtmPaymentStepWaitingWebview  || currentStep == ZPAtmPaymentStepGetStatusByAppTransIdForClient || currentStep == ZPAtmPaymentStepAutoDetectBank)) {
            return nil;
        } else {
            return [self configFooter];
        }
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == tableView.numberOfSections - 1) {
        if ((currentStep == ZPAtmPaymentStepWaitingWebview || currentStep == ZPAtmPaymentStepGetStatusByAppTransIdForClient || currentStep == ZPAtmPaymentStepAutoDetectBank)) {
            return 0;
        } else {
            return 95;
        }
    }
    return 0;
}


- (void)setFirstResponder {
    if (self.currentStep == ZPAtmPaymentStepAutoDetectBank) {
        ZPPaymentViewCell *cell = (ZPPaymentViewCell *)
                [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
        if ([cell isKindOfClass:[ZPATMSaveCardCell class]]) {
            [((ZPATMSaveCardCell *) cell) excuteView];
        }
    } else if (self.currentStep == ZPAtmPaymentStepAtmAuthenPayer) {
        ZPPaymentViewCell *cell = (ZPPaymentViewCell *)
                [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
        if ([cell isKindOfClass:[ZPOtpCell class]]) {
            [((ZPOtpCell *) cell).txtOtpValue becomeFirstResponder];
        }

    }
}

- (float)expectedPaymentCellHeight:(UITableView *)tableView {
    DDLogInfo(@"atm currentStep: %ld", (long) self.currentStep);
    switch (self.currentStep) {
        case ZPAtmPaymentStepSubmitTrans:
        case ZPAtmPaymentStepAutoDetectBank:
            if (IS_IPHONE_4_OR_LESS) {

                return (SCREEN_WIDTH - kMarginCardView) * (1 / (ratioCardView + 1)) + cardViewScrollHeight - 5;
            }
            //Add
            return (SCREEN_WIDTH - kMarginCardView) * (1 / ratioCardView) + cardViewScrollHeight;
        case ZPAtmPaymentStepAtmAuthenPayer:
            return 60;
        case ZPAtmPaymentStepSavedCardConfirm:
            return 60;
        case ZPAtmPaymentStepWaitingWebview:
            return [UIScreen mainScreen].bounds.size.height - 64;
        case ZPAtmPaymentStepGetStatusByAppTransIdForClient:
            return 60;
        default:
            break;
    }
    return 0;
}

#pragma mark initWebview

- (void)initWebView:(UIWebView *)wv {
    wv.delegate = self;
    wv.frame = self.mTableView.frame;
    wv.scrollView.scrollEnabled = YES;
    //4    wv.scrollView.delegate = self;
    wv.contentMode = UIViewContentModeScaleAspectFit;
    wv.hidden = NO;
    [self logEventWith:wv];
    DDLogInfo(@"wv.contentMode = UIViewContentModeScaleAspectFit");
}

- (void)setTimerTimeout:(int)value { //in seconds
    DDLogInfo(@"SetTimerTimeout : %d ", value);

    self.checkWebViewTimeOutTimer = [NSTimer scheduledTimerWithTimeInterval:value
                                                                     target:self
                                                                   selector:@selector(requestTimeout:)
                                                                   userInfo:NULL repeats:NO];
}

- (void)requestTimeout:(NSTimer *)mtimer {
    DDLogInfo(@"Time out");
    if (self.checkWebViewTimeOutTimer) {
        [self.checkWebViewTimeOutTimer invalidate];
    }
    [self.mWebView stopLoading];
    [self webViewDidFailLoadWithTimeOut:self.mWebView];
}

- (void)startStepTimerTimeout:(int)value { //in seconds
    DDLogInfo(@"setStepTimerTimeout : %d ", value);

    if (self.checkStepTimeOutTimer) {
        [self.checkStepTimeOutTimer invalidate];
    }
    self.checkStepTimeOutTimer = [NSTimer scheduledTimerWithTimeInterval:zp_atm_handler_step_timeout
                                                                  target:self
                                                                selector:@selector(stepTimeout:)
                                                                userInfo:[NSNumber numberWithInt:value] repeats:NO];
}

/*!
 @top transaction after 30s
 @discussion: not check if current step in 30s
 */
- (void)stepTimeout:(NSTimer *)mtimer {
    DDLogInfo(@"Step time out");
    if (self.checkStepTimeOutTimer) {
        [self.checkStepTimeOutTimer invalidate];
    }
    if (self.isLoading) {
        [self requestTimeout:mtimer];
    }
}

- (BOOL)shouldOpenExternalBrowser:(NSString*)urlString {
    return [urlString containsString:BIDV_URL_REGISTER] || [urlString containsString:BIDV_URL_SUPPORT] ;
}

#pragma mark - UIWebViewDelgate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSString *absoluteUrl = request.URL.absoluteString;
    DDLogInfo(@"should start load %@ main thread %@", absoluteUrl, [NSThread isMainThread] ? @"YES" : @"NO");
    if ([self shouldOpenExternalBrowser:absoluteUrl]) {
        NSURL *url = [NSURL URLWithString:absoluteUrl];
        [[UIApplication sharedApplication] openURL:url];
        return NO;
    }
    if ([self checkOrderTimeout]) {
        return NO;
    }

    absoluteUrl = absoluteUrl.length > 100 ? [absoluteUrl substringToIndex:99] : absoluteUrl;
    if ([[request.URL host] isEqualToString:@"payment-complete"]) {
        currentStep = ZPAtmPaymentStepGetTransStatus;
        [self.mWebView stopLoading];
        self.didReturned = YES;
        [self getStatusOfTranxId:self.atmCard.zpTransID callback:^(ZPPaymentResponse *response) {
        }];
        return NO;
    }

    if (self.checkWebViewTimeOutTimer != NULL) {
        [self.checkWebViewTimeOutTimer invalidate];
    }

    if ([self.model.jsHelper isAtmError:absoluteUrl]) {
        [self hideProgressHUD];
        [self showBIDVErrorWith:request];
        return NO;
    }
    /**
     BIDV BANK
     */
    if (currentStep == ZPAtmPaymentStepBIDVCapChaPassword && ![self.model.jsHelper bidv_IsUrl:absoluteUrl]) {
    }
    absoluteUrl = absoluteUrl.length > 100 ? [absoluteUrl substringToIndex:99] : absoluteUrl;
    ///bidv bank
    if ([self.model.jsHelper bidv_IsCaptchaUrl:absoluteUrl]) {
        DDLogInfo(@"--shouldStartLoadWithRequest: vtb_IsCaptchaUrl");
        [self.model.jsHelper bidv_GetCaptcha];
        return NO;
    }
    if ([self.model.jsHelper bidv_IsOtpUrl:absoluteUrl]) {
        [self BIDV_SetupOtpUI];
        return NO;
    }
    /**
     Request Captcha Callback
     */

    if ([self.model.jsHelper isCaptchaCallback:absoluteUrl]) {
        if ([self handleCaptchaCallBackWith:request]) {
            return NO;
        }
    }

    if (self.isRedirecatble) {
        if ([self.model.jsHelper isMessageErrorUrl:request.URL.absoluteString]) {
            self.isRedirecatble = false;

        }
        return YES;
    }
    DDLogInfo(@"--end of shouldStartLoadWithRequest");
    [self setTimerTimeout:zp_atm_handler_step_timeout];
    return YES;
}

- (BOOL)handleCaptchaCallBackWith:(NSURLRequest *)request {
    DDLogInfo(@"--shouldStartLoadWithRequest: isCaptchaCallback");
    NSString *query = [request.URL.query stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *jsonString = [query stringByReplacingOccurrencesOfString:@"param=" withString:@""];
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:NULL];
    NSString *base64CaptchaImage = [dictionary objectForKey:@"captcha"];
    atmCard.captchaBase64 = base64CaptchaImage;
    NSString *errorMessage = [self.model.jsHelper bidv_GetError];
    if (errorMessage.length > 0) {
        //handle chua dang ky dich vu E Banking
        NSString *notRegisterString = [self getStringWithKey:@"bidv_not_register_string" andDefaultString:BIDV_NotRegister];
        if ([[errorMessage lowercaseString] containsString:notRegisterString]) {
            [self BIDV_RegisterEBanking:errorMessage];
            [self hideProgressHUD];
            return YES;
        }
        UITableViewCell *cell = [self activeCell];
        if ([cell isKindOfClass:[ZPATMSaveCardCell class]]) {
            ZPATMSaveCardCell *cellSaveCard = (ZPATMSaveCardCell *) cell;
            cellSaveCard.isShouldHideKB = true;
            [self dismissKeyboard];
        }

        [self showAlertNotifyWith:errorMessage
                          handler:^(NSInteger idx) {
                              [self updateUICaptchaWith:errorMessage inCell:cell];
                          }];

        if ([[errorMessage lowercaseString] containsString:BIDV_Card_Not_Found] || [[errorMessage lowercaseString] containsString:BIDV_Message_Username_invalidate]) {
            [self hideProgressHUD];
            return YES;
        }
    }
    [self setupCaptchaView];
    return NO;
}


- (void)setupCaptchaView {
    if (currentStep == ZPAtmPaymentStepSubmitTrans ||
            currentStep == ZPAtmPaymentStepSavedCardConfirm
        // BIDV PIN + WEBVIEW
            ) {
        currentStep = ZPAtmPaymentStepBIDVCapChaPassword;
        //Need

        // Card da luu
        if (([self isAtmMapcard] == false)) {
            [self.delegate paymentControllerNeedUpdateLayouts];
        }

            //Map card and pay
        else {
            [self setImageCellAt:[NSIndexPath indexPathForRow:0 inSection:0]];
        }

    } else if (currentStep == ZPAtmPaymentStepBIDVCapChaPassword) {
        //Need
        [self setImageCellAt:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
    [self hideProgressHUD];
}

- (void)updateUICaptchaWith:(NSString *)errorMessage inCell:(UITableViewCell *)cell {
    ZPATMSaveCardCell *cardCell = [ZPATMSaveCardCell castFrom:cell];
    JVFloatLabeledTextField *tfCaptchar = [cardCell getTextFieldByType:ZPATMSaveCardObjectType_Captcha];
    JVFloatLabeledTextField *tfPassword = [cardCell getTextFieldByType:ZPATMSaveCardObjectType_CardPassword];
    tfCaptchar.text = @"";
    NSString *wrongPW = [self getStringWithKey:@"bidv_wrong_password" andDefaultString:@"mật khẩu"];
    NSString *wrongCaptcha = [self getStringWithKey:@"bidv_wrong_captcha" andDefaultString:@"captcha"];
    NSString *userNameInvalidate = [self getStringWithKey:@"bidv_username_invalidate" andDefaultString:BIDV_Message_Username_invalidate];
    NSString *cardNotFound = [self getStringWithKey:@"bidv_card_not_found_string" andDefaultString:BIDV_Card_Not_Found];
    if ([[errorMessage lowercaseString] containsString:wrongPW]) {
        tfPassword.text = @"";
        [cardCell btnPreOnAccessoryViewClick];
    } else if ([[errorMessage lowercaseString] containsString:wrongCaptcha]) {
        [self showKeyboard];
    } else if ([[errorMessage lowercaseString] containsString:cardNotFound] || [[errorMessage lowercaseString] containsString:userNameInvalidate]) {
        currentStep = ZPAtmPaymentStepAutoDetectBank;
        [self.delegate paymentControllerNeedUpdateLayouts];
    }
}

- (void)showBIDVErrorWith:(NSURLRequest *)request {
    NSString *query = [request.URL.query stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *errorMsg = [query stringByReplacingOccurrencesOfString:@"msg=" withString:@""];
    NSString *errorURL = @"";
    if ([errorMsg containsString:@"urlerror="]) {
        NSArray *arrMess = [errorMsg componentsSeparatedByString:@"urlerror="];
        if (arrMess.count == 2) {
            errorMsg = arrMess[0];
            errorURL = arrMess[1];
        }

        NSMutableDictionary *response = [NSMutableDictionary dictionary];
        [response setObjectCheckNil:errorMsg forKey:@"error"];

        [ZPPaymentManager sendRequestErrorWithData:self.atmCard.zpTransID bankCode:self.atmCard.bankCode exInfo:[NSString stringWithFormat:@"%@|%@", [ZPResourceManager covertEnumStepToString:self.currentStep], query] apiPath:query params:[NSMutableDictionary dictionary] response:response appId:self.bill.appId];

    }
    if (errorMsg == nil || [errorMsg length] == 0) {
        errorMsg = [self.dataManager messageForKey:@"message_announce_fail"];
    }
    self.didReturned = YES;
    [self finishWithErrorCode:0 message:errorMsg
                       status:kZPZaloPayCreditStatusCodeFail result:-1 errorStep:ZPErrorStepNonRetry];
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    DDLogInfo(@"webViewDidStartLoad %@", webView.request.URL);

    NSString *js = @"var alert = function();";
    [webView stringByEvaluatingJavaScriptFromString:js];
}

- (void)validateWebviewStatus:(UIWebView *)webView {
    NSCachedURLResponse *urlResponse = [[NSURLCache sharedURLCache] cachedResponseForRequest:webView.request];
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) urlResponse.response;
    NSInteger statusCode = httpResponse.statusCode;
    if (statusCode > 399) {
        [self hideProgressHUD];
        NSError *error = [NSError errorWithDomain:@"HTTP Error" code:httpResponse.statusCode userInfo:@{@"response": httpResponse}];

        NSMutableDictionary *response = [NSMutableDictionary dictionary];
        [response setObjectCheckNil:error.description forKey:@"error"];
        [response setObjectCheckNil:[NSString stringWithFormat:@"%ld", (long) statusCode] forKey:@"statusCode"];

        [ZPPaymentManager sendRequestErrorWithData:self.atmCard.zpTransID bankCode:self.atmCard.bankCode exInfo:[NSString stringWithFormat:@"%@|%@", [ZPResourceManager covertEnumStepToString:self.currentStep], error.description] apiPath:webView.request.URL.absoluteString params:[NSMutableDictionary dictionary] response:response appId:self.bill.appId];

        DDLogInfo(@"validateWebviewStatus %@", error);
        ZPPaymentResponse *timeoutResponse = [ZPPaymentResponse unknownExceptionResponseObject];
        timeoutResponse.errorStep = ZPErrorStepNonRetry;
        timeoutResponse.message = @"Kết nối đên ngân hàng bị lỗi, giao dịch bị hủy";
        timeoutResponse.zpTransID = self.atmCard.zpTransID;
        [self handlePaymentResponse:timeoutResponse];
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    DDLogInfo(@"webViewDidFinishLoad : step %ld", (long) currentStep);
    NSString *absoluteUrl = webView.request.URL.absoluteString;
    DDLogInfo(@"webViewDidFinishLoad : absoluteUrl %@", absoluteUrl);
    [self validateWebviewStatus:webView];

    if (currentStep == ZPAtmPaymentStepWaitingWebview) {
        [[ZPAppFactory sharedInstance].orderTracking trackWebLogin:self.bill.appTransID result:OrderStepResult_Success];
    }

    if ([[webView stringByEvaluatingJavaScriptFromString:@"document.readyState"] isEqualToString:@"complete"]) {
        [self hideProgressHUD];
    }
    if (self.checkWebViewTimeOutTimer) {
        [self.checkWebViewTimeOutTimer invalidate];
    }

    if (currentStep != ZPAtmPaymentStepWaitingWebview) {

        __weak ZPBIDVHandler *weakSelf = self;
        JSContext *context = [webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
        context[@"nativeListenReloadCaptcha"] = ^(NSString *url) {
            weakSelf.model.atmCaptchaStartTime = [weakSelf setLogTime:weakSelf.model.atmCaptchaStartTime isUpdate:YES];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakSelf.model.jsHelper bidv_CheckStep];
            });
        };
        context[@"nativeListenCaptchaSucess"] = ^(void) {
            weakSelf.model.atmOtpStartTime = [weakSelf setLogTime:weakSelf.model.atmOtpStartTime isUpdate:YES];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakSelf.model.jsHelper bidv_CheckStep];
            });
        };
        [self.model.jsHelper bidv_Inject_Reloadcaptcha];
        [self.model.jsHelper bidv_Inject_submitCaptchaSusscess];
        if (currentStep == ZPAtmPaymentStepSubmitTrans) {
            self.model.atmCaptchaEndTime = [self setLogTime:self.model.atmCaptchaEndTime isUpdate:YES];
            DDLogInfo(@"atmCaptchaEndTime: %f", self.model.atmCaptchaEndTime);
        }
        if (!webView.isLoading) {
            DDLogInfo(@"--webViewDidFinishLoad: bidv_IsUrl");
            [self.model.jsHelper bidv_CheckStep];
            return;
        }
    }
}

- (BOOL)showWebviewWith:(UIWebView *)webView dict:(NSDictionary *)dict {
    if (dict) {
        NSString *urlString = [dict stringForKey:NSURLErrorFailingURLStringErrorKey];
        if ([urlString containsString:BIDV_URL_SUPPORT]) {
            NSURL *url = [NSURL URLWithString:urlString];
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            [webView loadRequest:request];
            return true;
        } else if ([urlString containsString:BIDV_URL_REGISTER]) {
            NSURL *url = [NSURL URLWithString:urlString];
            if ([[UIApplication sharedApplication] canOpenURL:url]) {
                [[UIApplication sharedApplication] openURL:url];
                return true;
            }
        }
    }
    return false;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    DDLogInfo(@"didFailLoadWithError : %@", webView.request.URL);
    DDLogInfo(@"didFailLoadWithError : %@", error.description);
    if (error.code == ZPServerErrorCodeMaintainance && [self showWebviewWith:webView dict:error.userInfo]) {
        return;
    }
    if ([self.model.jsHelper isOnePayFailLoadURL:webView.request.URL.absoluteString]) {
        return;
    }

    if (self.checkWebViewTimeOutTimer != NULL) {
        [self.checkWebViewTimeOutTimer invalidate];
        DDLogInfo(@"STOP TIMER");
    }
    if (currentStep != ZPAtmPaymentStepVietinBankOTP && !self.didReturned) {
        [self hideProgressHUD];
    }

    if (self.didReturned) {
        return;
    }
    [[ZPAppFactory sharedInstance].orderTracking trackWebLogin:self.bill.appTransID result:OrderStepResult_Fail];

    NSMutableDictionary *response = [NSMutableDictionary dictionary];
    [response setObjectCheckNil:error.description forKey:@"error"];

    [ZPPaymentManager sendRequestErrorWithData:self.atmCard.zpTransID bankCode:self.atmCard.bankCode exInfo:[NSString stringWithFormat:@"%@|%@", [ZPResourceManager covertEnumStepToString:self.currentStep], error.description] apiPath:webView.request.URL.absoluteString params:[NSMutableDictionary dictionary] response:response appId:self.bill.appId];


    //ssl error
    for (int i = -1206; i <= -1200; i++) {
        if (error.code == i || error.code == -2000) {
            self.didReturned = true;
            [webView stopLoading];
            [self getStatusOfTranxId:self.atmCard.zpTransID callback:^(ZPPaymentResponse *response) {
            }];
            break;
        }
    }

}

- (void)webViewDidFailLoadWithTimeOut:(UIWebView *)awebView {
    DDLogInfo(@"webViewDidFailLoadWithTimeOut : %@", awebView.request.URL);

    [self hideProgressHUD];
    if (self.checkWebViewTimeOutTimer != NULL) {
        [self.checkWebViewTimeOutTimer invalidate];
        DDLogInfo(@"STOP TIMER");
    }
    if (self.didReturned) {
        return;
    }
    [[ZPAppFactory sharedInstance].orderTracking trackWebLogin:self.bill.appTransID result:OrderStepResult_Fail];
    self.didReturned = true;
    ZPPaymentResponse *timeoutResponse = [ZPPaymentResponse timeOutRequestObject];
    timeoutResponse.errorStep = ZPErrorStepNonRetry;
    [self.mWebView stopLoading];
    timeoutResponse.zpTransID = atmCard.zpTransID;
    [self handlePaymentResponse:timeoutResponse];
}

- (void)loadRequestWithStringUrl:(UIWebView *)webView :(NSString *)stringUrl {
    NSURL *url = [NSURL URLWithString:stringUrl];
    DDLogInfo(@"url = %@", url);
    self.urlRequest = [NSURLRequest requestWithURL:url
                                       cachePolicy:NSURLRequestUseProtocolCachePolicy
                                   timeoutInterval:120];
    [self.arrRequested addObject:self.urlRequest];

    [webView loadRequest:self.urlRequest];
}

#pragma ZPPaymentCellDelegate

- (void)validateInputDoneAndCallProcessPayment {
    [self dismissKeyboard];
    [self processPayment];
}

- (void)validateInputCompleted:(BOOL)isValid {
    self.isEnabledClickButton = isValid;
    [self updateOKButtonColor];
}

#pragma mark - Private methods

- (NSString *)okButtonBackgroundColor {
    if ((self.currentStep == ZPAtmPaymentStepSavedCardConfirm)
            || (self.currentStep == ZPAtmPaymentStepAtmAuthenPayer && self.isEnabledClickButton)) {
        return @"#06be04";
    } else if (!self.isEnabledClickButton) {
        return @"#C7C7CC";
    }
    return @"#008FE5";
}

- (void)processPayment {
    [self dismissKeyboard];
    self.didReturned = false;
    if ([self checkOrderTimeout]) {
        return;
    }
    if (![[ZPDataManager sharedInstance] checkInternetConnect]) {
        [self showAlertNotifyNetWorkProblem];
        return;
    }
    self.atmCard.step = currentStep;
    ZPPaymentViewCell *cell = [ZPPaymentViewCell castFrom:[self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]]];

    DDLogInfo(@"processPayment currentStep: %ld", (long) currentStep);
    switch (currentStep) {
        case ZPAtmPaymentStepAutoDetectBank: {
            DDLogInfo(@"--processPayment: ZPAtmPaymentStepAutoDetectBank");
            [[ZPAppFactory sharedInstance].orderTracking trackUserInputCardInfo:self.bill.appTransID];
            if (![cell checkValidate]) {
                return;
            }
            currentStep = ZPAtmPaymentStepSubmitTrans;
            [self processPayment];

            return;
        }
        case ZPAtmPaymentStepSubmitTrans: {
            DDLogInfo(@"--processPayment: ZPAtmPaymentStepSubmitTrans");
            if (![cell checkValidate]) {
                return;
            }
            NSDictionary *params = [cell outputParams];
            [self updateATMCardInfo:params];
            if ([self isAtmMapcard]) {
                [self verifyCardForMapping];
                return;
            }
            [self submitTran];
            break;
        }
        case ZPAtmPaymentStepAtmAuthenPayer: {
            DDLogInfo(@"--processPayment: ZPAtmPaymentStepAtmAuthenPayer");
            if (![cell checkValidate]) {
                return;
            }
            atmCard.authenValue = [[cell outputParams] stringForKey:@"otp"];
            [self submitAuthenPayer];
            break;
        }

        case ZPAtmPaymentStepSavedCardConfirm: {
            DDLogInfo(@"processPayment, _currentStep: ZPAtmPaymentStepSavedCardConfirm");
            [self updateConfigATMCard];
            [self submitTran];
            break;
        }
        case ZPAtmPaymentStepGetStatusByAppTransIdForClient: {
            self.atmCard.step = ZPAtmPaymentStepGetStatusByAppTransIdForClient;
            [self submitTran];
            break;
        }
            //BIDV /*Tai do*/
        case ZPAtmPaymentStepBIDVCapChaPassword: {
            if (![cell checkValidate]) {
                return;
            }
            DDLogInfo(@"--processPayment: ZPAtmPaymentBIDVBankCapCha");
            [self showProgressHUD];
            NSString *captchaText = [[cell outputParams] objectForKey:@"captcha"];
            NSString *passwordText = [[cell outputParams] objectForKey:@"password"];
            [self.model.jsHelper bidv_SubmitCaptcha:captchaText andPassWord:passwordText];
            break;
        }
        case ZPAtmPaymentStepBIDVBankOTP: {
            if (![cell checkValidate]) {
                return;
            }
            DDLogInfo(@"--processPayment: ZPAtmPaymentStepBIDVBankOTP");
            atmCard.authenValue = [[cell outputParams] stringForKey:@"otp"];
            [self showProgressHUD];
            [self.model.jsHelper bidv_SubmitOtp:atmCard.authenValue];
            self.model.atmOtpStartTime = [self setLogTime:self.model.atmOtpStartTime isUpdate:YES];
            break;
        }
        default:
            break;
    }
}

- (BOOL)checkOrderTimeout {
    //time out
    NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
    if (self.model.atmStartTimestamp > 0 && now - self.model.atmStartTimestamp > kATM_TRANSATION_TIMEOUT) {
        NSString *message = [self.dataManager messageForKey:@"message_atm_time_out"];
        [self finishWithErrorCode:0 message:message
                           status:kZPZaloPayCreditStatusCodeFail result:-1 errorStep:ZPErrorStepNonRetry];
        self.didReturned = YES;
        return YES;
    }
    return NO;
}

- (void)submitAuthenPayer {
    [self showProgressHUD];

    [ZPPaymentManager paymentWithBill:self.bill
                       andPaymentInfo:atmCard
                             andAppId:self.bill.appId
                              channel:self.channel
                               inMode:ZPPaymentMethodTypeAtm
                          andCallback:^(ZPResponseObject *responseObject) {
                              [self hideProgressHUD];
                              ZPPaymentResponse *response = (ZPPaymentResponse *) responseObject;
                              if (responseObject.errorCode == kZPZaloPayCreditErrorCodeUnknownException) {
                                  self.currentStep = ZPAtmPaymentStepGetTransStatus;
                                  [self getStatusOfTranxId:self.atmCard.zpTransID callback:^(ZPPaymentResponse *response) {

                                  }];
                                  return;
                              }
                              if (responseObject.errorCode == ZPServerErrorCodeAtmRetryOtp) {
                                  [self handleRetryOTP:response.message];
                                  return;
                              }
                              response.zpTransID = self.atmCard.zpTransID;
                              [self handlePaymentResponse:response];
                          }];
}

- (void)internalMapCard {

    if (![[ZPDataManager sharedInstance] checkInternetConnect]) {
        [self showAlertNotifyNetWorkProblem];
        return;
    }
    self.isSubmitStrans = YES;
    [self showProgressHUD];
    self.channel = [self.dataManager channelWithTypeAndBankCode:ZPPaymentMethodTypeAtm bankCode:self.atmCard.bankCode];
    [[ZPAppFactory sharedInstance].orderTracking trackChoosePayMethod:self.bill.appTransID pcmid:self.channel.channelID bankCode:self.atmCard.bankCode result:OrderStepResult_Success];

    [ZPPaymentManager paymentWithBill:self.bill
                       andPaymentInfo:atmCard
                             andAppId:self.bill.appId
                              channel:self.channel
                               inMode:ZPPaymentMethodTypeAtm
                          andCallback:^(ZPResponseObject *responseObject) {

                              ZPPaymentResponse *response = (ZPPaymentResponse *) responseObject;
                              if (response.zpTransID.length > 0) {
                                  self.atmCard.zpTransID = response.zpTransID;
                              }

                              [self hideProgressHUDWithResultCode:response.errorCode];

                              if ([self handleExceptionCaseWith:response.errorCode]) {
                                  return;
                              }

                              self.model.atmStartTimestamp = [[NSDate date] timeIntervalSince1970];
                              [self handlePaymentWith:response];
                          }];
}

#pragma mark SubmitTrans

- (void)submitTran {

    if (![[ZPDataManager sharedInstance] checkInternetConnect]) {
        [self showAlertNotifyNetWorkProblem];
        return;
    }
    self.isSubmitStrans = YES;
    [self showProgressHUD];
    [self updateChannel];

    [[ZPAppFactory sharedInstance].orderTracking trackChoosePayMethod:self.bill.appTransID pcmid:self.channel.channelID bankCode:self.atmCard.bankCode result:OrderStepResult_Success];

    [ZPPaymentManager paymentWithBill:self.bill
                       andPaymentInfo:atmCard
                             andAppId:self.bill.appId
                              channel:self.channel
                               inMode:ZPPaymentMethodTypeAtm
                          andCallback:^(ZPResponseObject *responseObject) {
                              ZPPaymentResponse *response = (ZPPaymentResponse *) responseObject;
                              if (response.zpTransID.length > 0) {
                                  self.atmCard.zpTransID = response.zpTransID;
                              }

                              [self hideProgressHUDWithResultCode:response.errorCode];

                              if ([self handleExceptionCaseWith:response.errorCode]) {
                                  return;
                              }

                              self.model.atmStartTimestamp = [[NSDate date] timeIntervalSince1970];
                              [self handlePaymentWith:response];

                          }];
}

- (void)updateChannel {
    if (self.paymentMethodDidChoose == nil) {
        self.channel = [self.dataManager channelWithTypeAndBankCode:ZPPaymentMethodTypeAtm bankCode:self.atmCard.bankCode];
    }
    // set payment channel to zalopaywallet while using withdraw
    if (self.bill.transType == ZPTransTypeWithDraw) {
        self.channel = [self.dataManager channelWithTypeAndBankCode:ZPPaymentMethodTypeZaloPayWallet bankCode:self.atmCard.bankCode];
    }
}

- (void)handlePaymentWith:(ZPPaymentResponse *)response {
    int resultCode = response.errorCode;
    if (resultCode == kZPZaloPayCreditErrorCodePinRetry) {
        [self showAlertNotifyRetryPin:response.message];
        return;
    }
    if (resultCode == ZPServerErrorCodeAtmRetryOtp) {
        [self handleRetryOTP:response.message];
        return;
    }
    if (resultCode == ZPServerErrorCodeNoneError) {
        [self saveAtmCard:self.atmCard];
        if ([self isRequirePin] && [ZaloPayWalletSDKPayment sharedInstance].appDependence) {
            [[ZaloPayWalletSDKPayment sharedInstance].appDependence savePassword:self.atmCard.pin];
        }
        response.zpTransID = self.atmCard.zpTransID;
        [self handlePaymentResponse:response];
        return;
    }
    if (resultCode > ZPServerErrorCodeNoneError) {
        [self saveAtmCard:self.atmCard];
        DDLogInfo(@"self.atmCard.zpTransID %@", self.atmCard.zpTransID);
        if (response.isProcessing && response.data.length > 0) {
            if (response.actionType == ZPBankActionTypeWeb) {
                self.isBankLoadWebview = YES;
                [self bankSupportWebview:response.data];
            } else {
                self.isBankLoadWebview = NO;
                [self bankSupportAPI];
            }
            return;
        }
        if (response.exchangeStatus != kZPZaloPayCreditStatusCodeUnidentified) {
            response.exchangeStatus = kZPZaloPayCreditStatusCodeSuccess;
        }
        response.zpTransID = self.atmCard.zpTransID;
        [self handlePaymentResponse:response];
        return;
    }
    if (resultCode < ZPServerErrorCodeNoneError) {
        response.exchangeStatus = kZPZaloPayCreditStatusCodeFail;
        response.zpTransID = self.atmCard.zpTransID;
        [self handlePaymentResponse:response];
    }
}

- (BOOL)handleExceptionCaseWith:(int)resultCode {
    if ((resultCode == kZPZaloPayCreditErrorCodeRequestTimeout || resultCode == ZPServerErrorCodeTransIdNotExist) && currentStep != ZPAtmPaymentStepGetStatusByAppTransIdForClient) {
        if ([self isAtmMapcard]) {
            return NO;
        }
        currentStep = ZPAtmPaymentStepGetStatusByAppTransIdForClient;
        [self.delegate paymentControllerNeedUpdateLayouts];
        [self performSelector:@selector(processPayment) withObject:self afterDelay:1];
        return YES;
    }
    // hanle server not response
    if (resultCode == kZPZaloPayCreditErrorCodeUnknownException) {
        currentStep = ZPAtmPaymentStepGetTransStatus;
        [self getStatusOfTranxId:self.atmCard.zpTransID callback:^(ZPPaymentResponse *response) {

        }];
        return YES;
    }
    return NO;
}

- (void)bankSupportAPI {
    currentStep = ZPAtmPaymentStepAtmAuthenPayer;
    // Card da luu
    if (([self isAtmMapcard] == false)) {

        [self.delegate paymentControllerNeedUpdateLayouts];
        [self setFirstResponder];
    }

        //Map card and pay
    else {
        ZPPaymentViewCell *cell = (ZPPaymentViewCell *) [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [cell setOTP];
        });
    }
}

- (void)bankSupportWebview:(NSString *)jsonString {
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:NULL];
    DDLogInfo(@"self.atmCard.interfaceType %d", self.atmCard.interfaceType);
    if (self.atmCard.interfaceType == ZPBankInterfaceTypeParseWeb) {
        self.webviewUrl = [dictionary objectForKey:@"redirecturl"];
        DDLogInfo(@"webviewUrl: %@", self.webviewUrl);
        [self webViewLoadRequestWithStringUrl:self.webviewUrl];
        return;
    }
    NSDictionary *dictionaryAgent = [[NSDictionary alloc] initWithObjectsAndKeys:
            WEB_USER_AGENT, @"UserAgent", nil];
    [[NSUserDefaults standardUserDefaults] registerDefaults:dictionaryAgent];
    self.webviewUrl = [dictionary objectForKey:@"redirecturl"];
    currentStep = ZPAtmPaymentStepWaitingWebview;

    [self.delegate paymentControllerNeedUpdateLayouts];
}


- (UIWebView *)mWebView {
    if (!_mWebView) {
#if ATM_SHOW_WEBVIEW == 1
        CGRect bounds = self.zpParentController.view.bounds;
        _mWebView = [[UIWebView alloc] initWithFrame:CGRectMake(10, 50, bounds.size.width, bounds.size.height * 0.3)];
        [self.zpParentController.view addSubview:_mWebView];
        [_mWebView setHidden:NO];
        [_mWebView setDelegate:self];
        _mWebView.scalesPageToFit = YES;
        [self.zpParentController.view bringSubviewToFront:_mWebView];
        _mWebView.tag = 55;
#else
        _mWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
        [self.zpParentController.view addSubview:_mWebView];
        [_mWebView setHidden:YES];
        [_mWebView setDelegate:self];
        _mWebView.scalesPageToFit = YES;
        _mWebView.tag = 55;
#endif
    }
    return _mWebView;
}


- (void)webViewLoadRequestWithStringUrl:(NSString *)stringUrl {
    self.isRedirecatble = true;
    NSURL *url = [NSURL URLWithString:stringUrl];
    DDLogInfo(@"url = %@", url);
    self.urlRequest = [NSURLRequest requestWithURL:url
                                       cachePolicy:NSURLRequestUseProtocolCachePolicy
                                   timeoutInterval:120];
    [self.mWebView loadRequest:self.urlRequest];

    self.model.atmCaptchaStartTime = [self setLogTime:self.model.atmCaptchaStartTime isUpdate:NO];
    DDLogInfo(@"vcb atmCaptchaStartTime: %f", self.model.atmCaptchaStartTime);
    //[self showProgressHUD];
}

- (void)finishWithErrorCode:(int)errorCode
                    message:(NSString *)message
                     status:(int)status
                     result:(int)result
                  errorStep:(enum ZPErrorStep)errorStep {
    if (self.checkStepTimeOutTimer) {
        [self.checkStepTimeOutTimer invalidate];
    }
    [self hideProgressHUD];
    ZPPaymentResponse *pResponse = [[ZPPaymentResponse alloc] init];
    pResponse.errorCode = errorCode;
    pResponse.originalCode = errorCode;
    pResponse.message = message;
    pResponse.exchangeStatus = status;
    pResponse.errorStep = errorStep;
    pResponse.zpTransID = atmCard.zpTransID;
    [self handlePaymentResponse:pResponse];
    [self writeLogTimeToServer];
}


- (void)handlePaymentResponse:(ZPPaymentResponse *)response {
    if (self.checkStepTimeOutTimer) {
        [self.checkStepTimeOutTimer invalidate];
    }
    response.paymentMethod = ZPPaymentMethodTypeAtm;
    if (response.errorStep == ZPErrorStepAbleToRetry) {
        if (currentStep == ZPAtmPaymentStepSubmitTrans) {
            currentStep = ZPAtmPaymentStepSubmitTrans;
        }
    }
    [super handlePaymentResponse:response];
}

#pragma mark - ZPAPaymentViewCellDelegate

- (void)detectCell:(ZPPaymentViewCell *)cell didDetectBank:(ZPBank *)bank {
    DDLogInfo(@"detectCell didDetectBank: %@", bank.bankName);
    NSString *bankCode = bank.bankCode;
    if (bankCode.length == 0) {
        [self switchToATMHandler:bank];
        return;
    }

    ZPPaymentMethod *method = [self validateBank:bankCode];

    if ([self checkErrorWithMethod:method]) {
        return;
    }

    if ([[ZPDataManager sharedInstance] chekMinAppVersion:method.minAppVersion]) {
        [self showAlertUpdateVersion:[self getBankNameForAlert]];
        return;

    }
    if ([self checkBankMaintain:bank]) {
        return;
    }
    if ([[ZPDataManager sharedInstance] chekMinAppVersion:bank.minAppVersion]) {
        [self showAlertUpdateVersion:[self getBankNameForAlert]];
        return;
    }

    if ([self validateMiniBank:bank]) {
        return;
    }

    if ([self checkMiniBankMaintain:bank]) {
        return;
    }

    //self.title = [bank objectForKey:@"name"];
    //Add SDK
    self.atmCard.bankCode = ((ZPATMSaveCardCell *)cell).bankCode;
    self.atmCard.bankName = bank.bankName;
    self.atmCard.interfaceType = bank.interfaceType;
    self.atmCard.requireOTP = bank.requireOTP;
    DDLogInfo(@"self.atmCard.interfaceType: %d",self.atmCard.interfaceType);
    [self callControllerUpdateTitle];

}

- (void)detectCell:(ZPPaymentViewCell *)cell didEditCardNumberWithDetectedBank:(ZPBank *)bank {
    if (!bank) {
        [self showAtmPinView];
        return;
    }
}

- (BOOL)isAtmMapcard {
    DDLogInfo(@"self.bill.transType: %ld", (long) self.bill.transType);
    return self.bill.transType == ZPTransTypeAtmMapCard;
}

- (void)detectCreditCard:(ZPPaymentViewCell *)cell didDetectBank:(NSDictionary *)bank {
    DDLogInfo(@"detectCreditCard: %@", bank);
    DDLogInfo(@"alertView: form redirect to credit card");
    ZPPaymentViewCell *cellDetect = (ZPPaymentViewCell *)
            [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    NSDictionary *params = [cellDetect outputParams];
    if (self.delegate && [self.delegate respondsToSelector:@selector(paymentControllerSwitchToCreditCardHandler:)]) {
        [self.delegate paymentControllerSwitchToCreditCardHandler:params[@"number"]];
    }
}

- (void)writeLogTimeToServer {
    ZPMapCardLog *log = [ZPMapCardLog new];
    log.transid = atmCard.zpTransID;
    log.pmcid = @(self.channel.channelID);
    log.atmcaptcha_begindate = @(self.model.atmCaptchaStartTime);
    log.atmcaptcha_enddate = @(self.model.atmCaptchaEndTime);
    log.atmotp_begindate = @(self.model.atmOtpStartTime);
    log.atmotp_enddate = @(self.model.atmOtpEndTime);
    [ZPPaymentManager writeMapCardLog:log];
}

- (long long)setLogTime:(long long)timeParam isUpdate:(BOOL)isUpdate {
    if (isUpdate) {
        return (long long) ([[NSDate date] timeIntervalSince1970] * 1000);
    }
    if (timeParam != 0) {
        return timeParam;
    }
    return (long long) ([[NSDate date] timeIntervalSince1970] * 1000);
}


- (void)btnOkClicked {

    [self processPayment];
}

- (void)BIDV_SetupOtpUI {
    if (currentStep == ZPAtmPaymentStepBIDVBankOTP) {
        return;
    }
    DDLogInfo(@"bidv self.model.atmOtpStartTime: %f", self.model.atmOtpStartTime);
    [self hideProgressHUD];
    currentStep = ZPAtmPaymentStepBIDVBankOTP;
    if (([self isAtmMapcard] == false)) {
        [self.delegate paymentControllerNeedUpdateLayouts];
    }
    //Map card and pay
    ZPPaymentViewCell *cell = (ZPPaymentViewCell *) [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [cell setOTP];
}

- (void)BIDV_RegisterEBanking:(NSString *)errorMessage {
    NSString *message = [self getStringWithKey:@"bidv_not_register_result_message" andDefaultString:@"Quý khách chưa đăng ký sử dụng dịch vụ BIDV online. Vui lòng đăng ký tại các Quầy giao dịch BIDV hoặc đăng ký trực tuyến dịch vụ BIDV Online."];
    [self finishWithErrorCode:0 message:message
                       status:kZPZaloPayCreditStatusCodeFail result:-1 errorStep:ZPErrorStepNonRetry];
}

#pragma mark - Utils

- (void)loadWebView:(UIWebView *)wv {
    if (!wv) {
        return;
    }

    if (self.webviewUrl != nil && ![self.webviewUrl isEqualToString:@""]) {
        [self initWebView:wv];
        [self loadRequestWithStringUrl:wv :self.webviewUrl];
        self.webviewUrl = nil;
        [self setupButtonRegister:wv];
        self.zpParentController.navigationItem.rightBarButtonItems = [self rightBarButtonItems];
    }
}

- (void)setupButtonRegister:(UIWebView *)wv {
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    view.backgroundColor = [UIColor defaultBackground];
    [wv.superview addSubview:view];

    int bottomViewHeight = 68;
    wv.scrollView.contentInset = UIEdgeInsetsMake(0, 0, bottomViewHeight, 0);
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(0);
        make.left.equalTo(0);
        make.right.equalTo(0);
        make.height.equalTo(bottomViewHeight);
    }];

    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [view addSubview:button];
    [button setupZaloPayButton];
    [button setTitle:stringRegisterBIDV forState:UIControlStateNormal];
    [button addTarget:self action:@selector(openURLRegisterBIDVOnline) forControlEvents:UIControlEventTouchUpInside];

    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(-10);
        make.left.equalTo(10);
        make.right.equalTo(-10);
        make.height.equalTo(kZaloButtonHeight);
    }];
}

- (NSArray *)rightBarButtonItems {
    UIButton *helpButton = [UIButton buttonWithType:UIButtonTypeCustom];
    helpButton.titleLabel.font = [UIFont zaloPayWithSize:20];
    [helpButton setIconFont:@"general_question" forState:UIControlStateNormal];
    [helpButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [helpButton setTintColor:[UIColor whiteColor]];
    helpButton.multipleTouchEnabled = NO;
    helpButton.exclusiveTouch = YES;
    helpButton.frame = CGRectMake(0, 0, 30, 30);
    [helpButton addTarget:self action:@selector(helpButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:helpButton];
    return @[rightBarButtonItem];
}

- (void)helpButtonClicked {
    [self openURLWithString:kBIDVHelpUrl];
}

- (void)openURLRegisterBIDVOnline {
    id <ZPWalletDependenceProtocol> helper = [ZaloPayWalletSDKPayment sharedInstance].appDependence;
    NSString *urlString = [helper bidvRegisterPayOnlineUrl];

//    NSString *urlString = [self getStringFromConfigWithKey:@"bidv_register_link"];
    if (urlString.length == 0) {
        urlString = BIDV_OPEN_URL_REGISTER;
    }
    [self openURLWithString:urlString];
}


- (void)openURLWithString:(NSString *)urlString {
    NSURL *url = [NSURL URLWithString:urlString];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (void)setImageCellAt:(NSIndexPath *)indexPath {
    if (!indexPath) {
        return;
    }
    ZPPaymentViewCell *cell = [ZPPaymentViewCell castFrom:[self.mTableView cellForRowAtIndexPath:indexPath]];
    if (!cell) {
        return;
    }
    @weakify(cell);
    [[loadImageFromString(atmCard.captchaBase64)
            takeUntil:[cell rac_prepareForReuseSignal]]
            subscribeNext:^(UIImage *img) {
                @strongify(cell);
                [cell setCaptchaImage:img];
            }];
}

@end


