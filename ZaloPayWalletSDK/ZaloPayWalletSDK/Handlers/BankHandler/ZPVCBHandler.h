//
//  ZPVCBHandler.h
//  ZaloPayWalletSDK
//
//  Created by thi la on 6/8/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPBankHandlerBase.h"
#import "ZPPaymentMethod.h"

@interface ZPVCBHandler : ZPBankHandlerBase<UITableViewDelegate, UITableViewDataSource>

- (NSString *)getOptionValueAt:(NSInteger)idx;
- (BOOL)isCheckedAt:(NSIndexPath *)indexPath;

@end
