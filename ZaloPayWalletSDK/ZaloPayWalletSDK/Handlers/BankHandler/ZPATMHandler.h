//
//  ZPATMHandler.h
//  ZaloPayWalletSDK
//
//  Created by Thi La on 5/23/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPPaymentHandler.h"

@interface ZPATMHandler : ZPPaymentHandler<UITableViewDelegate, UITableViewDataSource>
@end
