//
//  ZPCellIdentifier.m
//  ZaloPayWalletSDK
//
//  Created by thi la on 6/5/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPCellIdentifier.h"

NSString *identify(ZPATMCellType type) {
    switch (type) {
        case ZPATMCellTypePinnedViewCell:
            return @"atm-pinned-view";
        case ZPATMCellTypeCardWebView:
            return @"credit-card-webview-cell";
        case ZPATMCellTypeOtp:
            return @"otp-cell";
        case ZPATMCellTypeAuthen:
            return @"atm-authen-type-cell";
        case ZPATMCellTypeOption:
            return @"option-cell";
        case ZPATMCellTypeDidChoosePaymentMethod:
            return @"did-choose-payment-method-cell";
        case ZPATMCellTypeAutoDetect:
            return @"ZPATMSaveCardCell";
    }
}
