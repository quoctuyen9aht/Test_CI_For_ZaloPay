//
//  ZPCellIdentifier.h
//  ZaloPayWalletSDK
//
//  Created by thi la on 6/5/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZPDefine.h"

NSString *identify(ZPATMCellType type);
