//
//  ZPATMHandler.m
//  ZaloPayWalletSDK
//
//  Created by Thi La on 5/23/16.
//  Copyright © 2016-present VNG. All rights reserved.
//
#import "ZPATMHandler.h"
#import "ZPAtmPinnedView.h"
#import "ZPBill.h"
#import "ZPDataManager.h"
#import "ZPATMHandler+Cell.h"
#import "ZPATMSaveCardCell.h"
#import "ZPPaymentInfo.h"
#import "ZaloPayWalletSDKLog.h"
#import "ZPBank.h"

@interface ZPATMHandler ()

@property(strong, nonatomic) ZPAtmPinnedView *atmPinnedView;
@property(assign, nonatomic) BOOL isEnabledClickButton;
@end

@implementation ZPATMHandler

#pragma mark - Overrides

- (void)initialization {
    [super initialization];
    //self.mTableView.tableHeaderView = nil;

}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.currentStep = ZPAtmPaymentStepAutoDetectBank;
    }
    return self;
}

- (void)viewWillBack {
    ZPATMSaveCardCell *cell = [ZPATMSaveCardCell castFrom:[self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]]];
    if (cell) {
        [cell viewWillBack];
    }
}

- (NSString *)paymentControllerTitle {
    return [self.dataManager getTitleByTranstype:self.bill.transType];
}

- (NSString *)okButtonTitle {
    return [[self.dataManager getButtonTitle:@"continue"] capitalizedString];
}

#pragma mark - UITableViewDelegate & DataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    UITableViewCell *nCell = [self createCellAt:indexPath with:tableView];
    ZPPaymentViewCell *cell = [ZPPaymentViewCell castFrom:nCell];
    if (cell) {
        self.isEnabledClickButton = NO;
        [cell updateLayouts];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
    }
    return nCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = [super tableView:tableView heightForRowAtIndexPath:indexPath];
    if (height > 0) {
        return height;
    } else {
        return MAX(0, [self expectedPaymentCellHeight:tableView]);
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
   // [self setFirstResponder];
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (float)expectedPaymentCellHeight:(UITableView *)tableView {
    if (IS_IPHONE_4_OR_LESS) {

        return (SCREEN_WIDTH - kMarginCardView) * (1 / (ratioCardView + 1)) + cardViewScrollHeight - 10;
    }

    CGFloat heightIPX = 0;
    if (@available(iOS 11.0, *)) {
        heightIPX = UIApplication.sharedApplication.keyWindow.safeAreaInsets.top / 2;
    }
    //Add
    return (SCREEN_WIDTH - kMarginCardView) * (1 / ratioCardView) + cardViewScrollHeight + (IS_IPHONE_X ? heightIPX : 0);
}

#pragma mark - ZPAPaymentViewCellDelegate

- (void)detectCell:(ZPPaymentViewCell *)cell didDetectBank:(ZPBank *)bank {
    DDLogInfo(@"detectCell didDetectBank: %@", bank);
    NSString *bankCode = bank.bankCode;
    if (bankCode.length == 0) {
        self.atmCard.bankCode = nil;
        return;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(paymentControllerSwitchToBankHandler:)]) {
        [self.delegate paymentControllerSwitchToBankHandler:bank];
    }
}

- (void)detectCell:(ZPPaymentViewCell *)cell didEditCardNumberWithDetectedBank:(ZPBank *)bank {
    if (!bank) {
        [self showAtmPinView];
        return;
    }
}


- (void)detectCreditCard:(ZPPaymentViewCell *)cell didDetectBank:(NSDictionary *)bank {
    DDLogInfo(@"detectCreditCard: %@", bank);
    ZPPaymentViewCell *cellDetect = (ZPPaymentViewCell *)
            [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    NSDictionary *params = [cellDetect outputParams];
    if (self.delegate && [self.delegate respondsToSelector:@selector(paymentControllerSwitchToCreditCardHandler:)]) {
        [self.delegate paymentControllerSwitchToCreditCardHandler:params[@"number"]];
    }
}

- (void)showAtmPinView {
    [self dismissKeyboard];
    if (!self.atmPinnedView) {
        self.atmPinnedView = [[ZPAtmPinnedView alloc] init];
    }
    self.atmPinnedView.delegate = self;
    [self.atmPinnedView showInView:self.zpParentController.view withData:[self.dataManager.bankManager getArrayBankInPinViewWithTranstype:self.bill.transType]];
}

- (void)pinnedCell:(ZPPaymentViewCell *)cell didSelectBank:(ZPBank *)bank {
    [self showKeyboard];
}

- (void)setFirstResponder {
    ZPPaymentViewCell *cell = (ZPPaymentViewCell *)
            [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    if ([cell isKindOfClass:[ZPATMSaveCardCell class]]) {
        [((ZPATMSaveCardCell *) cell) excuteView];
    }
}

@end


