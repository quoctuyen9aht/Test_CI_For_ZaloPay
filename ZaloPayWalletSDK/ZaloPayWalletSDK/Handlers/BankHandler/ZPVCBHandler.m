//
//  ZPVCBHandler.m
//  ZaloPayWalletSDK
//
//  Created by thi la on 6/8/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPVCBHandler.h"
#import "ZPPaymentInfo.h"
#import "ZPSmartLinkHelper.h"
#import "ZPBill.h"
#import "ZPDataManager.h"
#import "ZPPaymentChannel.h"
#import "NSString+ZPExtension.h"
#import "ZPPaymentManager.h"
#import "ZaloPayWalletSDKPayment.h"
#import "ZPAtmResponseObject.h"
#import "ZPPaymentResponse.h"
#import "ZPBank.h"
#import "ZPResourceManager.h"
#import "ZPCreditCardWebviewCell.h"
#import "ZPATMSaveCardCell.h"
#import "ZPOtpCell.h"
#import "ZPATMHandlerWebModel.h"
#import "NSHTTPCookieStorage+ZPExtension.h"
#import "ZaloPayWalletSDKLog.h"
#import "ZPMapCardLog.h"

@interface ZPVCBHandler () <UIWebViewDelegate>

@property(strong, nonatomic) UIWebView *mWebView;

@property(strong, nonatomic) NSIndexPath *checkedIndexPath;
@property(strong, nonatomic) NSTimer *checkWebViewTimeOutTimer;
@property(strong, nonatomic) NSTimer *checkStepTimeOutTimer;
@property(strong, nonatomic) NSString *webviewUrl;
@property(strong, nonatomic) NSArray *arrList;
@property(strong, nonatomic) NSURLRequest *urlRequest;
@property(strong, nonatomic) NSMutableArray *arrRequested;

@property(assign, nonatomic) bool isRedirecatble;
@property(assign, nonatomic) BOOL isEnabledClickButton;

@end

@implementation ZPVCBHandler
@synthesize currentStep, atmCard, paymentMethodDidChoose;

- (void)dealloc {
    [_mWebView stopLoading];
    [_mWebView setDelegate:nil];
    [_mWebView removeFromSuperview];
    UITableViewCell *cell =
            [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    if ([cell isKindOfClass:[ZPPaymentViewCell class]]) {
        ZPPaymentViewCell *paymentCell = (ZPPaymentViewCell *) cell;
        if (paymentCell.delegate == self) {
            paymentCell.delegate = nil;
        }
        if ([paymentCell isKindOfClass:[ZPCreditCardWebviewCell class]] && ((ZPCreditCardWebviewCell *) paymentCell).ccWebview.delegate == self) {
            ((ZPCreditCardWebviewCell *) paymentCell).ccWebview.delegate = nil;
            ((ZPCreditCardWebviewCell *) paymentCell).ccWebview = nil;
        }
    }
    [[NSHTTPCookieStorage new] zpRemoveAllStoredCredentials:self.arrRequested];
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.arrRequested = [NSMutableArray new];
        atmCard = [[ZPAtmCard alloc] init];
        currentStep = ZPAtmPaymentStepAutoDetectBank;
        self.didReturned = false;
        self.isRedirecatble = true;
        self.model = [[ZPATMHandlerWebModel alloc] initWithStartEvent:nil
                                                                  end:nil
                                                                 with:self.mWebView];

        NSDictionary *dictionaryAgent = [[NSDictionary alloc] initWithObjectsAndKeys:
               VCB_WEB_USER_AGENT, @"User-Agent", nil];
        [[NSUserDefaults standardUserDefaults] registerDefaults:dictionaryAgent];
    }
    return self;
}

#pragma mark - Overrides

- (void)initialization {
    [super initialization];
    atmCard.amount = self.bill.amount;
}

- (void)viewWillBack {
    [self handleCellWillBack];
}

- (BOOL)okButtonEnabled {
    return self.isEnabledClickButton || currentStep == ZPAtmPaymentStepSavedCardConfirm;
}

- (NSString *)paymentControllerTitle {
    return [self.dataManager getTitleByTranstype:self.bill.transType];
}

- (NSString *)okButtonTitle {
    return (currentStep == ZPAtmPaymentStepSavedCardConfirm) ? [[self.dataManager getButtonTitle:@"confirm"] capitalizedString] : [[self.dataManager getButtonTitle:@"continue"] capitalizedString];
}


#pragma mark - UITableViewDelegate & DataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    DDLogInfo(@"cellForRowAtIndexPath currentStep: %ld", (long) currentStep);

    if (currentStep != ZPAtmPaymentStepAutoDetectBank) {
        // Hide btn PreNext On ZPATMSaveCardCell
        [self notifyHideNextButton];
    }

    UITableViewCell *nCell = [self createCellAt:indexPath with:tableView];
    ZPPaymentViewCell *cell = [ZPPaymentViewCell castFrom:nCell];
    if (cell) {
        self.isEnabledClickButton = NO;
        [cell updateLayouts];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
    }
    return nCell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([indexPath row] == ((NSIndexPath *) [[tableView indexPathsForVisibleRows] lastObject]).row) {
        //  DDLogInfo(@"load completed");

    }
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell isKindOfClass:[ZPCreditCardWebviewCell class]]) {
        ((ZPCreditCardWebviewCell *) cell).ccWebview.delegate = nil;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1 && currentStep != ZPAtmPaymentStepChooseAccountVicom) {
        return 0;
    }
    if (currentStep == ZPAtmPaymentStepAutoDetectBank) {
        if (indexPath.section == 1) {
            return 0;
        }
    }
    CGFloat height = [super tableView:tableView heightForRowAtIndexPath:indexPath];

    if (height > 0) {
        return height;
    } else {
        return MAX(0, [self expectedPaymentCellHeight:tableView]);
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return currentStep == ZPAtmPaymentStepChooseAccountVicom ? self.arrList.count : 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (currentStep == ZPAtmPaymentStepSavedCardConfirm) {
        if ([self isRequirePin]) {
            return 1;
        }
        ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:self.atmCard.bankCode];
        if (bank.otpType.length > 0 && [[bank.otpType componentsSeparatedByString:@"|"] count] >= 2) {
            return 2;
        }
    }
    if (currentStep == ZPAtmPaymentStepAutoDetectBank || currentStep == ZPAtmPaymentStepSubmitOtpVicom) {
        return 2;
    }
    return 1;
}

// Hidden Accept Button
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == tableView.numberOfSections - 1) {
        if ((currentStep == ZPAtmPaymentStepWaitingWebview || currentStep == ZPAtmPaymentStepGetStatusByAppTransIdForClient || currentStep == ZPAtmPaymentStepAutoDetectBank)) {
            return nil;
        } else {
            return [self configFooter];
        }
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == tableView.numberOfSections - 1) {
        if ((currentStep == ZPAtmPaymentStepWaitingWebview || currentStep == ZPAtmPaymentStepGetStatusByAppTransIdForClient || currentStep == ZPAtmPaymentStepAutoDetectBank)) {
            return 0;
        } else {
            return 95;
        }
    }
    return 0;
}

- (void)setFirstResponder {
 if (self.currentStep == ZPAtmPaymentStepAutoDetectBank) {
        ZPPaymentViewCell *cell = (ZPPaymentViewCell *)
                [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
        if ([cell isKindOfClass:[ZPATMSaveCardCell class]]) {
            [((ZPATMSaveCardCell *) cell) excuteView];
        }
    } else if (self.currentStep == ZPAtmPaymentStepAtmAuthenPayer || currentStep == ZPAtmPaymentStepSubmitOtpVicom) {
        ZPPaymentViewCell *cell = (ZPPaymentViewCell *)
                [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
        if ([cell isKindOfClass:[ZPOtpCell class]]) {
            [((ZPOtpCell *) cell).txtOtpValue becomeFirstResponder];
        }

    }
}

- (float)expectedPaymentCellHeight:(UITableView *)tableView {
    DDLogInfo(@"atm currentStep: %ld", (long) self.currentStep);
    switch (self.currentStep) {
        case ZPAtmPaymentStepSubmitTrans:
        case ZPAtmPaymentStepAutoDetectBank:
            if (IS_IPHONE_4_OR_LESS) {

                return (SCREEN_WIDTH - kMarginCardView) * (1 / (ratioCardView + 1)) + cardViewScrollHeight - 5;
            }
            //Add
            return (SCREEN_WIDTH - kMarginCardView) * (1 / ratioCardView) + cardViewScrollHeight;

        case ZPAtmPaymentStepWaitingWebview:
            return [UIScreen mainScreen].bounds.size.height - 64;
        case ZPAtmPaymentStepAtmAuthenPayer:
        case ZPAtmPaymentStepSavedCardConfirm:
        case ZPAtmPaymentStepSubmitOtpVicom:
        case ZPAtmPaymentStepChooseAccountVicom:
        case ZPAtmPaymentStepGetStatusByAppTransIdForClient:
            return 60;
        default:
            break;
    }
    return 0;
}

#pragma mark initWebview

- (void)initWebView:(UIWebView *)wv {
    wv.delegate = self;
    wv.frame = self.mTableView.frame;
    wv.scrollView.scrollEnabled = YES;
    //4    wv.scrollView.delegate = self;
    wv.contentMode = UIViewContentModeScaleAspectFit;
    wv.hidden = NO;
    [self logEventWith:wv];
    DDLogInfo(@"wv.contentMode = UIViewContentModeScaleAspectFit");
}

- (void)setTimerTimeout:(int)value { //in seconds
    DDLogInfo(@"SetTimerTimeout : %d ", value);

    self.checkWebViewTimeOutTimer = [NSTimer scheduledTimerWithTimeInterval:value
                                                                     target:self
                                                                   selector:@selector(requestTimeout:)
                                                                   userInfo:NULL repeats:NO];
}

- (void)requestTimeout:(NSTimer *)mtimer {
    DDLogInfo(@"Time out");
    if (self.checkWebViewTimeOutTimer) {
        [self.checkWebViewTimeOutTimer invalidate];
    }
    [self.mWebView stopLoading];
    [self webViewDidFailLoadWithTimeOut:self.mWebView];
}

- (void)startStepTimerTimeout:(int)value { //in seconds
    DDLogInfo(@"setStepTimerTimeout : %d ", value);

    if (self.checkStepTimeOutTimer) {
        [self.checkStepTimeOutTimer invalidate];
    }
    self.checkStepTimeOutTimer = [NSTimer scheduledTimerWithTimeInterval:zp_atm_handler_step_timeout
                                                                  target:self
                                                                selector:@selector(stepTimeout:)
                                                                userInfo:[NSNumber numberWithInt:value] repeats:NO];
}

/*!
 @top transaction after 30s
 @discussion: not check if current step in 30s
 */
- (void)stepTimeout:(NSTimer *)mtimer {
    DDLogInfo(@"Step time out");
    if (self.checkStepTimeOutTimer) {
        [self.checkStepTimeOutTimer invalidate];
    }
    if (self.isLoading) {
        [self requestTimeout:mtimer];
    }
}

#pragma mark - UIWebViewDelgate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {

    NSString *absoluteUrl = request.URL.absoluteString;
    DDLogInfo(@"should start load %@ main thread %@", absoluteUrl, [NSThread isMainThread] ? @"YES" : @"NO");
    if ([self checkOrderTimeout]) {
        return NO;
    }

    absoluteUrl = absoluteUrl.length > 100 ? [absoluteUrl substringToIndex:99] : absoluteUrl;
    if ([[request.URL host] isEqualToString:@"payment-complete"]) {
        currentStep = ZPAtmPaymentStepGetTransStatus;
        [self.mWebView stopLoading];
        self.didReturned = YES;
        [self getStatusOfTranxId:self.atmCard.zpTransID callback:^(ZPPaymentResponse *response) {
        }];
        return NO;
    }

    if (self.checkWebViewTimeOutTimer != NULL) {
        [self.checkWebViewTimeOutTimer invalidate];
    }

    if ([self.model.jsHelper isAtmError:absoluteUrl]) {
        [self hideProgressHUD];
        [self showVCBErrorWith:request];
        return NO;
    }

    if ([self.model.jsHelper isCaptchaCallback:absoluteUrl]) {
        DDLogInfo(@"--shouldStartLoadWithRequest: isCaptchaCallback");
        [self handleCaptchaCallBackWith:request];
        [self hideProgressHUD];
        return NO;
    }

    if ([self.model.jsHelper vcb_DirectIsCaptchaUrl:absoluteUrl]) {
        DDLogInfo(@"--shouldStartLoadWithRequest: vcb_DirectIsCaptchaUrl");
        [self.model.jsHelper vcb_DirectRequestCaptcha];
        return NO;
    }

    if ([self.model.jsHelper isOtpCallback:absoluteUrl]) {
        DDLogInfo(@"--shouldStartLoadWithRequest: isOtpCallback");
        [self hideProgressHUD];
        [self handleOTPCallback:request];
        return NO;
    }
    if ([self isVCBWebError:absoluteUrl]) {
        return NO;
    }
    DDLogInfo(@"--end of shouldStartLoadWithRequest");
    [self setTimerTimeout:zp_atm_handler_step_timeout];
    return YES;
}

- (BOOL)isVCBWebError:(NSString *)absoluteUrl {
    if ([absoluteUrl zpContainsString:@"vietcombank"] && [absoluteUrl zpContainsString:@"/Err.aspx"]) {
        DDLogInfo(@"--shouldStartLoadWithRequest: vietcombank error");
        NSString *message;
        NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
        if (now - self.model.atmStartTimestamp > kATM_TRANSATION_TIMEOUT) {
            message = [self.dataManager messageForKey:@"message_atm_time_out"];
        } else if (currentStep == ZPAtmPaymentStepSubmitOtpVicom) {
            message = [self.dataManager messageForKey:@"message_announce_wrong_otp"];
        } else {
            message = [self.dataManager messageForKey:@"message_announce_fail"];
        }
        [self finishWithErrorCode:0 message:message
                           status:kZPZaloPayCreditStatusCodeFail result:-1 errorStep:ZPErrorStepNonRetry];
        self.didReturned = YES;
        return YES;
    }
    return NO;
}

- (void)handleOTPCallback:(NSURLRequest *)request {
    NSString *query = [request.URL.query stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *jsonString = [query stringByReplacingOccurrencesOfString:@"param=" withString:@""];
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];

    NSMutableDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:NULL];
    NSString *message = [dictionary objectForKey:@"message"];
    NSString *base64CaptchaImage = [dictionary objectForKey:@"otpimg"];

    atmCard.captchaBase64 = base64CaptchaImage;

    if (message != NULL && [message isEqualToString:@""] == false) {
        [self showAlertWithMessage:message];
        ZPPaymentViewCell *cell = (ZPPaymentViewCell *) [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        if (![message zpContainsString:[self.dataManager messageForKey:@"vcb_error_capcha_message"]]) {
            [cell setValue:@"" viewById:@"otp"];
            [cell setValue:@"" viewById:@"password"];
        }

    }
}

- (void)showVCBErrorWith:(NSURLRequest *)request {
    NSString *query = [request.URL.query stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *errorMsg = [query stringByReplacingOccurrencesOfString:@"msg=" withString:@""];
    NSString *errorURL = @"";
    if ([errorMsg containsString:@"urlerror="]) {
        NSArray *arrMess = [errorMsg componentsSeparatedByString:@"urlerror="];
        if (arrMess.count == 2) {
            errorMsg = arrMess[0];
            errorURL = arrMess[1];
        }

        NSMutableDictionary *response = [NSMutableDictionary dictionary];
        [response setObjectCheckNil:errorMsg forKey:@"error"];

        [ZPPaymentManager sendRequestErrorWithData:self.atmCard.zpTransID bankCode:self.atmCard.bankCode exInfo:[NSString stringWithFormat:@"%@|%@", [ZPResourceManager covertEnumStepToString:self.currentStep], query] apiPath:query params:[NSMutableDictionary dictionary] response:response appId:self.bill.appId];

    }
    if (errorMsg == nil || [errorMsg length] == 0) {
        errorMsg = [self.dataManager messageForKey:@"message_announce_fail"];
    }
    self.didReturned = YES;
    [self finishWithErrorCode:0 message:errorMsg
                       status:kZPZaloPayCreditStatusCodeFail result:-1 errorStep:ZPErrorStepNonRetry];
}

- (void)handleCaptchaCallBackWith:(NSURLRequest *)request {

    NSString *query = [request.URL.query stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *jsonString = [query stringByReplacingOccurrencesOfString:@"param=" withString:@""];
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:NULL];
    NSString *base64CaptchaImage = [dictionary objectForKey:@"captcha"];
    NSString *message = [dictionary objectForKey:@"message"];
    atmCard.captchaBase64 = base64CaptchaImage;

    if (message.length > 0) {
        [self resetCaptchaView];
        [self showAlertNotifyWith:message handler:^(NSInteger idx) {
            [self showKeyboard];
        }];
    }
    [self setupCaptchaView];
}


- (void)setupCaptchaView {
    if (currentStep == ZPAtmPaymentStepSubmitTrans || currentStep == ZPAtmPaymentStepSavedCardConfirm
        // VIETCOMBANK PIN + WEBVIEW
            ) {
        currentStep = ZPAtmPaymentStepLoginVicom;
        [self.delegate paymentControllerNeedUpdateLayouts];
    } else if (currentStep == ZPAtmPaymentStepLoginVicom) {
        //Need
        // Card da luu
        if (([self isAtmMapcard] == false && self.paymentMethodDidChoose.methodType == ZPPaymentMethodTypeSavedCard)) {

            [self.delegate paymentControllerNeedUpdateLayouts];
        }

            //Map card and pay
        else {
            [self setImageCellAt:[NSIndexPath indexPathForRow:0 inSection:0]];

        }
    }
}

- (void)resetCaptchaView {
    ZPPaymentViewCell *cell = (ZPPaymentViewCell *) [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    if ([cell isKindOfClass:[ZPATMSaveCardCell class]]) {
        ZPATMSaveCardCell *cellSaveCard = (ZPATMSaveCardCell *) cell;
        JVFloatLabeledTextField *tf = [cellSaveCard getCurrentTf];
        tf.text = @"";
        tf.floatingLabel.text = stringTitleInputCaptcha;
        cellSaveCard.isShouldHideKB = true;
        [self dismissKeyboard];
    } else {
        ZPTextField *textField = (ZPTextField *) [cell findFirstResponderBeneathView:cell.whiteView];
        textField.text = @"";
    }
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    DDLogInfo(@"webViewDidStartLoad %@", webView.request.URL);

    NSString *js = @"var alert = function();";
    [webView stringByEvaluatingJavaScriptFromString:js];
}

- (void)validateWebviewStatus:(UIWebView *)webView {
    NSCachedURLResponse *urlResponse = [[NSURLCache sharedURLCache] cachedResponseForRequest:webView.request];
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) urlResponse.response;
    NSInteger statusCode = httpResponse.statusCode;
    if (statusCode > 399) {
        [self hideProgressHUD];
        NSError *error = [NSError errorWithDomain:@"HTTP Error" code:httpResponse.statusCode userInfo:@{@"response": httpResponse}];
        NSMutableDictionary *response = [NSMutableDictionary dictionary];
        [response setObjectCheckNil:error.description forKey:@"error"];

        [ZPPaymentManager sendRequestErrorWithData:self.atmCard.zpTransID bankCode:self.atmCard.bankCode exInfo:[NSString stringWithFormat:@"%@|%@", [ZPResourceManager covertEnumStepToString:self.currentStep], error.description] apiPath:webView.request.URL.absoluteString params:[NSMutableDictionary dictionary] response:response appId:self.bill.appId];
        DDLogInfo(@"validateWebviewStatus %@", error);
        ZPPaymentResponse *timeoutResponse = [ZPPaymentResponse unknownExceptionResponseObject];
        timeoutResponse.errorStep = ZPErrorStepNonRetry;
        timeoutResponse.message = @"Kết nối đên ngân hàng bị lỗi, giao dịch bị hủy";
        timeoutResponse.zpTransID = self.atmCard.zpTransID;
        [self handlePaymentResponse:timeoutResponse];
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    DDLogInfo(@"webViewDidFinishLoad : %@", webView.request.URL);
    DDLogInfo(@"webViewDidFinishLoad step: %ld", (long) currentStep);
    BOOL needDismissHUD = YES;
    DDLogInfo(@"webViewDidFinishLoad : step %ld", (long) currentStep);
    NSString *absoluteUrl = webView.request.URL.absoluteString;

    [self validateWebviewStatus:webView];

    if (currentStep == ZPAtmPaymentStepWaitingWebview) {
        [[ZPAppFactory sharedInstance].orderTracking trackWebLogin:self.bill.appTransID result:OrderStepResult_Success];
    }

    if ([[webView stringByEvaluatingJavaScriptFromString:@"document.readyState"] isEqualToString:@"complete"]) {
        [self hideProgressHUD];
    }
    if (self.checkWebViewTimeOutTimer) {
        [self.checkWebViewTimeOutTimer invalidate];
    }

    if (currentStep != ZPAtmPaymentStepWaitingWebview) {

        // write log time of webview
        if (currentStep == ZPAtmPaymentStepSubmitTrans) {
            self.model.atmCaptchaEndTime = [self setLogTime:self.model.atmCaptchaEndTime isUpdate:YES];
            DDLogInfo(@"atmCaptchaEndTime: %f", self.model.atmCaptchaEndTime);
        }
        if ([self.model.jsHelper vcb_DirectIsOtpUrl:absoluteUrl]) {
            self.model.atmOtpEndTime = [self setLogTime:self.model.atmOtpEndTime isUpdate:YES];
            DDLogInfo(@"vcb atmOtpEndTime: %f", self.model.atmOtpEndTime);
        }
        // end write

        if ([self.model.jsHelper vcb_DirectErrorUrl:absoluteUrl]) {
            DDLogInfo(@"--webViewDidFinishLoad: vcb_DirectErrorUrl");
            [webView stringByEvaluatingJavaScriptFromString:[self.model.jsHelper vcb_DirectGetErrorMessage]];
            return;
        }
        if ([self.model.jsHelper isMessageErrorUrl:absoluteUrl]) {
            DDLogInfo(@"--webViewDidFinishLoad: isMessageErrorUrl");
            [self hideProgressHUD];
            NSString *message = [webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByClassName('error')[0].innerText"];
            self.isRedirecatble = false;

            ZPPaymentResponse *response = [[ZPPaymentResponse alloc] init];
            response.errorCode = kZPZaloPayCreditErrorCodeNoneError;
            response.errorStep = ZPErrorStepNonRetry;
            response.exchangeStatus = kZPZaloPayCreditStatusCodeFail;
            response.message = message;
            [self handlePaymentResponse:response];

            return;
        }
        if ([self.model.jsHelper vcb_ErrorUrl:webView.request.URL.absoluteString]) {
            DDLogInfo(@"--webViewDidFinishLoad: vcb_ErrorUrl");
            [self hideProgressHUD];
            NSString *message = [webView stringByEvaluatingJavaScriptFromString:@"document.getElementById('ctl00__Default_Content_Error_Lbl_Warning').innerText"];
            self.isRedirecatble = false;

            ZPPaymentResponse *response = [[ZPPaymentResponse alloc] init];
            response.errorCode = kZPZaloPayCreditErrorCodeNoneError;
            response.errorStep = ZPErrorStepNonRetry;
            response.exchangeStatus = kZPZaloPayCreditStatusCodeFail;
            response.message = message;
            [self handlePaymentResponse:response];
            return;
        }

        if (currentStep == ZPAtmPaymentStepLoginVicom) {
            DDLogInfo(@"--webViewDidFinishLoad: ZPAtmPaymentStepLoginVicom");

            if ([self.model.jsHelper vcb_IsOtpUrl:webView.request.URL.absoluteString]) {
                currentStep = ZPAtmPaymentStepSubmitOtpVicom;
                [self hideProgressHUD];

                [self.delegate paymentControllerNeedUpdateLayouts];
                return;
            } else if ([self.model.jsHelper vcb_DirectIsOtpUrl:webView.request.URL.absoluteString]) {
                NSString *js = [self.model.jsHelper vcb_DirectConfirmChooseAccount];
                NSString *message = [webView stringByEvaluatingJavaScriptFromString:js];
                if (message != nil && ![message isEqualToString:@""]) {
                    NSString *newMessage = [message substringToIndex:[message length] - 1];
                    currentStep = ZPAtmPaymentStepChooseAccountVicom;
                    self.arrList = [newMessage componentsSeparatedByString:@","];
                    [self.delegate paymentControllerNeedUpdateLayouts];
                    DDLogInfo(@"arrList %lu", (unsigned long) [self.arrList count]);
                } else {
                    currentStep = ZPAtmPaymentStepSubmitOtpVicom;
                    [self.model.jsHelper vcb_DirectConfirm:@""];
                    [self.delegate paymentControllerNeedUpdateLayouts];
                }
                self.didReturned = YES;
                return;
            } else {
                NSString *message = [webView stringByEvaluatingJavaScriptFromString:[self jsVicomErrorMessage]];

                if (atmCard.isVicomDirect) {
                    message = [webView stringByEvaluatingJavaScriptFromString:[self.model.jsHelper vcb_DirectGetErrorMessage]];
                }

                DDLogInfo(@"message vcb: %@", message);
                if (message != NULL && [message isEqualToString:@""] == false && !self.isShowAlertMessage) {
                    //currentStep = ZPAtmPaymentStepCreateOrder;
                    [self hideProgressHUD];
                    self.isShowAlertMessage = YES;
                    [self showAlertWithMessage:message];
                    if (!atmCard.isVicomDirect) {
                        return;
                    }
                }
            }
        }
        if ([self.model.jsHelper vcb_IsLoginUrl:webView.request.URL.absoluteString]) {
            currentStep = ZPAtmPaymentStepLoginVicom;
            DDLogInfo(@"--webViewDidFinishLoad vcb_IsLoginUrl");
            needDismissHUD = NO;
            //[self startRequestVicomCapcha:webView];
        } else {
            NSString *message = [webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByClassName('error')[0].innerText"];
            if (message == nil || [message isEqualToString:@""]) {
                message = [webView stringByEvaluatingJavaScriptFromString:@"document.getElementById('m-message').innerText.trim()"];
            }

            if ([self.model.jsHelper isSmartLinkOTP:webView.request.URL.absoluteString]) {
                DDLogInfo(@"--isSmartLinkOTP");
                needDismissHUD = NO;

                [self startRequestCaptchaSML:webView];
            } else if ([self.model.jsHelper isVicomDirect:webView.request.URL.absoluteString]) {
                DDLogInfo(@"--isVicomDirect");
                atmCard.isVicomDirect = YES;
                needDismissHUD = NO;
                [self.model.jsHelper vcb_DirectRequestCaptcha];
                self.model.atmCaptchaEndTime = [self setLogTime:self.model.atmCaptchaEndTime isUpdate:NO];
                DDLogInfo(@"vcb atmCaptchaEndTime: %f", self.model.atmCaptchaEndTime);

            } else {
                if (message != NULL && [message isEqualToString:@""] == false) {
                    [self showAlertWithMessage:message];
                    self.currentStep = ZPAtmPaymentStepSubmitTrans;
                }
            }
        }
    }
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    DDLogInfo(@"didFailLoadWithError : %@", webView.request.URL);
    DDLogInfo(@"didFailLoadWithError : %@", error.description);
    if ([self.model.jsHelper isOnePayFailLoadURL:webView.request.URL.absoluteString]) {
        return;
    }

    if (self.checkWebViewTimeOutTimer != NULL) {
        [self.checkWebViewTimeOutTimer invalidate];
        DDLogInfo(@"STOP TIMER");
    }
    if (currentStep != ZPAtmPaymentStepVietinBankOTP && !self.didReturned) {
        [self hideProgressHUD];
    }

    if (self.didReturned) {
        return;
    }
    [[ZPAppFactory sharedInstance].orderTracking trackWebLogin:self.bill.appTransID result:OrderStepResult_Fail];
    
    NSMutableDictionary *response = [NSMutableDictionary dictionary];
    [response setObjectCheckNil:error.description forKey:@"error"];

    [ZPPaymentManager sendRequestErrorWithData:self.atmCard.zpTransID bankCode:self.atmCard.bankCode exInfo:[NSString stringWithFormat:@"%@|%@", [ZPResourceManager covertEnumStepToString:self.currentStep], error.description] apiPath:webView.request.URL.absoluteString params:[NSMutableDictionary dictionary] response:response appId:self.bill.appId];

    //ssl error
    for (int i = -1206; i <= -1200; i++) {
        if (error.code == i || error.code == -2000) {
            self.didReturned = true;
            [webView stopLoading];
            [self getStatusOfTranxId:self.atmCard.zpTransID callback:^(ZPPaymentResponse *response) {
            }];
            break;
        }
    }
}

- (void)webViewDidFailLoadWithTimeOut:(UIWebView *)awebView {
    DDLogInfo(@"webViewDidFailLoadWithTimeOut : %@", awebView.request.URL);

    [self hideProgressHUD];
    if (self.checkWebViewTimeOutTimer != NULL) {
        [self.checkWebViewTimeOutTimer invalidate];
        DDLogInfo(@"STOP TIMER");
    }
    if (self.didReturned) {
        return;
    }
    [[ZPAppFactory sharedInstance].orderTracking trackWebLogin:self.bill.appTransID result:OrderStepResult_Fail];
    self.didReturned = true;
    ZPPaymentResponse *timeoutResponse = [ZPPaymentResponse timeOutRequestObject];
    timeoutResponse.errorStep = ZPErrorStepNonRetry;
    [self.mWebView stopLoading];
    timeoutResponse.zpTransID = atmCard.zpTransID;
    [self handlePaymentResponse:timeoutResponse];
}

- (void)startRequestVicomCapcha:(UIWebView *)webView {
    NSString *imgJS = [self.model jsRequestVicomCaptcha];
    DDLogInfo(@"imgJS = %@", imgJS);
    [webView stringByEvaluatingJavaScriptFromString:imgJS];

}

- (void)startRequestCaptchaSML:(UIWebView *)webview {

    [self setTimerTimeout:20];

    NSString *imgJs = [self.model jsCaptchaSML];
    DDLogInfo(@"imgJS = %@", imgJs);
    [webview stringByEvaluatingJavaScriptFromString:imgJs];
}

- (void)startRequestCaptchaBanknet:(UIWebView *)webview {
    [self setTimerTimeout:20];

    NSString *imgJs = [self.model jsCaptchaBanknet];

    DDLogInfo(@"imgJS = %@", imgJs);
    [webview stringByEvaluatingJavaScriptFromString:imgJs];
}

- (NSString *)jsVicomErrorMessage {
    return [self.model jsVicomErrorMessage];
}

- (void)loadRequestWithStringUrl:(UIWebView *)webView :(NSString *)stringUrl {
    NSURL *url = [NSURL URLWithString:stringUrl];
    DDLogInfo(@"url = %@", url);
    self.urlRequest = [NSURLRequest requestWithURL:url
                                       cachePolicy:NSURLRequestUseProtocolCachePolicy
                                   timeoutInterval:120];
    [self.arrRequested addObject:self.urlRequest];

    [webView loadRequest:self.urlRequest];
}

#pragma ZPPaymentCellDelegate

- (void)validateInputDoneAndCallProcessPayment {
    [self dismissKeyboard];
    [self processPayment];
}

- (void)validateInputCompleted:(BOOL)isValid {
    self.isEnabledClickButton = isValid;
    [self updateOKButtonColor];
}

#pragma mark - Private methods

- (NSString *)okButtonBackgroundColor {
    if ((self.currentStep == ZPAtmPaymentStepSavedCardConfirm)
            || (self.currentStep == ZPAtmPaymentStepVietinBankOTP && self.isEnabledClickButton)
            || (self.currentStep == ZPAtmPaymentStepSubmitOtpVicom && self.isEnabledClickButton)
            || (self.currentStep == ZPAtmPaymentStepAtmAuthenPayer && self.isEnabledClickButton)) {
        return @"#06be04";
    } else if (!self.isEnabledClickButton) {
        return @"#C7C7CC";
    }
    return @"#008FE5";
}

- (void)processPayment {
    [self dismissKeyboard];
    self.didReturned = false;
    if ([self checkOrderTimeout]) {
        return;
    }
    if (![[ZPDataManager sharedInstance] checkInternetConnect]) {
        [self showAlertNotifyNetWorkProblem];
        return;
    }
    self.atmCard.step = currentStep;
    ZPPaymentViewCell *cell = [ZPPaymentViewCell castFrom:[self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]]];

    DDLogInfo(@"processPayment currentStep: %ld", (long) currentStep);
    switch (currentStep) {
        case ZPAtmPaymentStepAutoDetectBank: {
            DDLogInfo(@"--processPayment: ZPAtmPaymentStepAutoDetectBank");
            [[ZPAppFactory sharedInstance].orderTracking trackUserInputCardInfo:self.bill.appTransID];
            if (![cell checkValidate]) {
                return;
            }
            currentStep = ZPAtmPaymentStepSubmitTrans;
            [self processPayment];
            return;
        }
        case ZPAtmPaymentStepSubmitTrans: {
            DDLogInfo(@"--processPayment: ZPAtmPaymentStepSubmitTrans");
            if (![cell checkValidate]) {
                return;
            }
            NSDictionary *params = [cell outputParams];
            [self updateATMCardInfo:params];
            if ([self isAtmMapcard]) {
                [self verifyCardForMapping];
                return;
            }
            [self submitTran];
            break;
        }
        case ZPAtmPaymentStepAtmAuthenPayer: {
            DDLogInfo(@"--processPayment: ZPAtmPaymentStepAtmAuthenPayer");
            if (![cell checkValidate]) {
                return;
            }
            atmCard.authenValue = [[cell outputParams] stringForKey:@"otp"];
            [self submitAuthenPayer];
            break;
        }

        case ZPAtmPaymentStepSavedCardConfirm: {
            DDLogInfo(@"processPayment, _currentStep: ZPAtmPaymentStepSavedCardConfirm");
            [self updateConfigATMCard];
            [self submitTran];
            break;
        }
        case ZPAtmPaymentStepGetStatusByAppTransIdForClient: {
            self.atmCard.step = ZPAtmPaymentStepGetStatusByAppTransIdForClient;
            [self submitTran];
            break;
        }
        default:
            break;
    }
}

- (BOOL)checkOrderTimeout {
    //time out
    NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
    if (self.model.atmStartTimestamp <= 0 || now - self.model.atmStartTimestamp <= kATM_TRANSATION_TIMEOUT) {
        return NO;
    }

    NSString *message = [self.dataManager messageForKey:@"message_atm_time_out"];
    [self finishWithErrorCode:0 message:message
                       status:kZPZaloPayCreditStatusCodeFail result:-1 errorStep:ZPErrorStepNonRetry];
    self.didReturned = YES;
    return YES;
}

- (void)submitAuthenPayer {
    [self showProgressHUD];

    [ZPPaymentManager paymentWithBill:self.bill
                       andPaymentInfo:atmCard
                             andAppId:self.bill.appId
                              channel:self.channel
                               inMode:ZPPaymentMethodTypeAtm
                          andCallback:^(ZPResponseObject *responseObject) {
                              [self hideProgressHUD];
                              ZPPaymentResponse *response = (ZPPaymentResponse *) responseObject;
                              if (responseObject.errorCode == kZPZaloPayCreditErrorCodeUnknownException) {
                                  self.currentStep = ZPAtmPaymentStepGetTransStatus;
                                  [self getStatusOfTranxId:self.atmCard.zpTransID callback:^(ZPPaymentResponse *response) {

                                  }];
                                  return;
                              }
                              if (responseObject.errorCode == ZPServerErrorCodeAtmRetryOtp) {
                                  [self handleRetryOTP:response.message];
                                  return;
                              }
                              response.zpTransID = self.atmCard.zpTransID;
                              [self handlePaymentResponse:response];
                          }];
}

- (void)verifyCardForMapping {

    if (![[ZPDataManager sharedInstance] checkInternetConnect]) {
        [self showAlertNotifyNetWorkProblem];
        return;
    }
    self.isSubmitStrans = YES;
    [self showProgressHUD];
    self.channel = [self.dataManager channelWithTypeAndBankCode:ZPPaymentMethodTypeAtm bankCode:self.atmCard.bankCode];
    [[ZPAppFactory sharedInstance].orderTracking trackChoosePayMethod:self.bill.appTransID pcmid:self.channel.channelID bankCode:self.atmCard.bankCode result:OrderStepResult_Success];

    [ZPPaymentManager paymentWithBill:self.bill
                       andPaymentInfo:atmCard
                             andAppId:self.bill.appId
                              channel:self.channel
                               inMode:ZPPaymentMethodTypeAtm
                          andCallback:^(ZPResponseObject *responseObject) {

                              ZPPaymentResponse *response = (ZPPaymentResponse *) responseObject;
                              if (response.zpTransID.length > 0) {
                                  self.atmCard.zpTransID = response.zpTransID;
                              }

                              [self hideProgressHUDWithResultCode:response.errorCode];

                              if ([self handleExceptionCaseWith:response.errorCode]) {
                                  return;
                              }

                              self.model.atmStartTimestamp = [[NSDate date] timeIntervalSince1970];

                              [self handlePaymentWith:response];
                          }];
}


#pragma mark SubmitTrans

- (void)submitTran {

    if (![[ZPDataManager sharedInstance] checkInternetConnect]) {
        [self showAlertNotifyNetWorkProblem];
        return;
    }
    self.isSubmitStrans = YES;
    [self showProgressHUD];
    [self updateChannel];

    [[ZPAppFactory sharedInstance].orderTracking trackChoosePayMethod:self.bill.appTransID pcmid:self.channel.channelID bankCode:self.atmCard.bankCode result:OrderStepResult_Success];

    [ZPPaymentManager paymentWithBill:self.bill
                       andPaymentInfo:atmCard
                             andAppId:self.bill.appId
                              channel:self.channel
                               inMode:ZPPaymentMethodTypeAtm
                          andCallback:^(ZPResponseObject *responseObject) {
                              ZPPaymentResponse *response = (ZPPaymentResponse *) responseObject;
                              if (response.zpTransID.length > 0) {
                                  self.atmCard.zpTransID = response.zpTransID;
                              }

                              [self hideProgressHUDWithResultCode:response.errorCode];

                              if ([self handleExceptionCaseWith:response.errorCode]) {
                                  return;
                              }

                              self.model.atmStartTimestamp = [[NSDate date] timeIntervalSince1970];

                              [self handlePaymentWith:response];

                          }];
}

- (void)updateChannel {
    if (self.paymentMethodDidChoose == nil) {
        self.channel = [self.dataManager channelWithTypeAndBankCode:ZPPaymentMethodTypeIbanking bankCode:self.atmCard.bankCode];
    }
    // set payment channel to zalopaywallet while using withdraw
    if (self.bill.transType == ZPTransTypeWithDraw) {
        self.channel = [self.dataManager channelWithTypeAndBankCode:ZPPaymentMethodTypeZaloPayWallet bankCode:self.atmCard.bankCode];
    }
}

- (void)handlePaymentWith:(ZPPaymentResponse *)response {
    int resultCode = response.errorCode;
    if (resultCode == kZPZaloPayCreditErrorCodePinRetry) {
        [self showAlertNotifyRetryPin:response.message];
        return;
    }
    if (resultCode == ZPServerErrorCodeAtmRetryOtp) {
        [self handleRetryOTP:response.message];
        return;
    }
    if (resultCode == ZPServerErrorCodeNoneError) {
        [self saveAtmCard:self.atmCard];
        if ([self isRequirePin] && [ZaloPayWalletSDKPayment sharedInstance].appDependence) {
            [[ZaloPayWalletSDKPayment sharedInstance].appDependence savePassword:self.atmCard.pin];
        }
        response.zpTransID = self.atmCard.zpTransID;
        [self handlePaymentResponse:response];
        return;
    }
    if (resultCode > ZPServerErrorCodeNoneError) {
        [self saveAtmCard:self.atmCard];
        DDLogInfo(@"self.atmCard.zpTransID %@", self.atmCard.zpTransID);
        if (response.isProcessing && response.data.length > 0) {
            if (response.actionType == ZPBankActionTypeWeb) {
                self.isBankLoadWebview = YES;
                [self bankSupportWebview:response.data];
            } else {
                self.isBankLoadWebview = NO;
                [self bankSupportAPI];
            }
            return;
        }
        if (response.exchangeStatus != kZPZaloPayCreditStatusCodeUnidentified) {
            response.exchangeStatus = kZPZaloPayCreditStatusCodeSuccess;
        }
        response.zpTransID = self.atmCard.zpTransID;
        [self handlePaymentResponse:response];
        return;
    }
    if (resultCode < ZPServerErrorCodeNoneError) {
        response.exchangeStatus = kZPZaloPayCreditStatusCodeFail;
        response.zpTransID = self.atmCard.zpTransID;
        [self handlePaymentResponse:response];
    }
}

- (BOOL)isMapAccount {
    ZPIBankingAccountBill *bill = [ZPIBankingAccountBill castFrom:self.bill];
    if (bill) {
        return bill.isMapAccount;
    }
    return NO;
}

- (BOOL)handleExceptionCaseWith:(int)resultCode {

    if ((resultCode == kZPZaloPayCreditErrorCodeRequestTimeout || resultCode == ZPServerErrorCodeTransIdNotExist) && self.currentStep != ZPAtmPaymentStepGetStatusByAppTransIdForClient) {
        if ([self isMapAccount]) {
            return NO;
        }
        currentStep = ZPAtmPaymentStepGetStatusByAppTransIdForClient;
        [self.delegate paymentControllerNeedUpdateLayouts];
        [self performSelector:@selector(processPayment) withObject:self afterDelay:1];
        return YES;
    }
    // hanle server not response
    if (resultCode == kZPZaloPayCreditErrorCodeUnknownException) {
        currentStep = ZPAtmPaymentStepGetTransStatus;
        [self getStatusOfTranxId:self.atmCard.zpTransID callback:^(ZPPaymentResponse *response) {

        }];
        return YES;
    }
    return NO;
}

- (void)bankSupportAPI {
    currentStep = ZPAtmPaymentStepAtmAuthenPayer;
    // Card da luu
    if (([self isAtmMapcard] == false && self.paymentMethodDidChoose.methodType == ZPPaymentMethodTypeSavedAccount)) {

        [self.delegate paymentControllerNeedUpdateLayouts];
        [self setFirstResponder];
    }

        //Map card and pay
    else {
        ZPPaymentViewCell *cell = (ZPPaymentViewCell *) [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [cell setOTP];
        });
    }
}

- (void)bankSupportWebview:(NSString *)jsonString {
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:NULL];
    DDLogInfo(@"self.atmCard.interfaceType %d", self.atmCard.interfaceType);
    if (self.atmCard.interfaceType == ZPBankInterfaceTypeParseWeb) {
        self.webviewUrl = [dictionary objectForKey:@"redirecturl"];
        DDLogInfo(@"webviewUrl: %@", self.webviewUrl);
        [self webViewLoadRequestWithStringUrl:self.webviewUrl];
        return;
    }
    NSDictionary *dictionaryAgent = [[NSDictionary alloc] initWithObjectsAndKeys:
            WEB_USER_AGENT, @"UserAgent", nil];
    [[NSUserDefaults standardUserDefaults] registerDefaults:dictionaryAgent];
    self.webviewUrl = [dictionary objectForKey:@"redirecturl"];
    currentStep = ZPAtmPaymentStepWaitingWebview;

    [self.delegate paymentControllerNeedUpdateLayouts];
}

- (UIWebView *)mWebView {
    if (!_mWebView) {
#if ATM_SHOW_WEBVIEW == 1
        CGRect bounds = self.zpParentController.view.bounds;
        _mWebView = [[UIWebView alloc] initWithFrame:CGRectMake(10, 50, bounds.size.width, bounds.size.height * 0.3)];
        [self.zpParentController.view addSubview:_mWebView];
        [_mWebView setHidden:NO];
        [_mWebView setDelegate:self];
        _mWebView.scalesPageToFit = YES;
        [self.zpParentController.view bringSubviewToFront:_mWebView];
        _mWebView.tag = 55;
#else
        _mWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
        [self.zpParentController.view addSubview:_mWebView];
        [_mWebView setHidden:YES];
        [_mWebView setDelegate:self];
        _mWebView.scalesPageToFit = YES;
        _mWebView.tag = 55;
#endif
    }
    return _mWebView;
}


- (void)webViewLoadRequestWithStringUrl:(NSString *)stringUrl {
    self.isRedirecatble = true;
    NSURL *url = [NSURL URLWithString:stringUrl];
    DDLogInfo(@"url = %@", url);
    self.urlRequest = [NSURLRequest requestWithURL:url
                                       cachePolicy:NSURLRequestUseProtocolCachePolicy
                                   timeoutInterval:120];
    [self.mWebView loadRequest:self.urlRequest];

    self.model.atmCaptchaStartTime = [self setLogTime:self.model.atmCaptchaStartTime isUpdate:NO];
    DDLogInfo(@"vcb atmCaptchaStartTime: %f", self.model.atmCaptchaStartTime);
    //[self showProgressHUD];
}

- (void)finishWithErrorCode:(int)errorCode
                    message:(NSString *)message
                     status:(int)status
                     result:(int)result
                  errorStep:(enum ZPErrorStep)errorStep {
    if (self.checkStepTimeOutTimer) {
        [self.checkStepTimeOutTimer invalidate];
    }
    [self hideProgressHUD];
    ZPPaymentResponse *pResponse = [[ZPPaymentResponse alloc] init];
    pResponse.errorCode = errorCode;
    pResponse.originalCode = errorCode;
    pResponse.message = message;
    pResponse.exchangeStatus = status;
    pResponse.errorStep = errorStep;
    pResponse.zpTransID = atmCard.zpTransID;
    [self handlePaymentResponse:pResponse];
    [self writeLogTimeToServer];
}

- (void)handlePaymentResponse:(ZPPaymentResponse *)response {
    if (self.checkStepTimeOutTimer) {
        [self.checkStepTimeOutTimer invalidate];
    }
    response.paymentMethod = ZPPaymentMethodTypeIbanking;
    if (response.errorStep == ZPErrorStepAbleToRetry) {
        if (currentStep == ZPAtmPaymentStepSubmitTrans) {
            currentStep = ZPAtmPaymentStepSubmitTrans;
        }
    }
    [super handlePaymentResponse:response];
}

#pragma mark - ZPAPaymentViewCellDelegate

- (void)detectCell:(ZPPaymentViewCell *)cell didDetectBank:(ZPBank *)bank {
    DDLogInfo(@"detectCell didDetectBank: %@", bank);
    NSString *bankCode = bank.bankCode;
    if (bankCode.length == 0) {
        [self switchToATMHandler:bank];
        return;
    }

    ZPPaymentMethod *method = [self validateBank:bankCode];

    if ([self checkErrorWithMethod:method]) {
        return;
    }

    if ([[ZPDataManager sharedInstance] chekMinAppVersion:method.minAppVersion]) {
        [self showAlertUpdateVersion:[self getBankNameForAlert]];
        return;

    }
    if ([self checkBankMaintain:bank]) {
        return;
    }
    if ([[ZPDataManager sharedInstance] chekMinAppVersion:bank.minAppVersion]) {
        [self showAlertUpdateVersion:[self getBankNameForAlert]];
        return;
    }

    if ([self validateMiniBank:bank]) {
        return;
    }

    if ([self checkMiniBankMaintain:bank]) {
        return;
    }

    //self.title = [bank objectForKey:@"name"];
    //Add SDK
    self.atmCard.bankCode = ((ZPATMSaveCardCell *)cell).bankCode;
    self.atmCard.bankName = bank.bankName;
    self.atmCard.interfaceType = bank.interfaceType;
    self.atmCard.requireOTP = bank.requireOTP;
    DDLogInfo(@"self.atmCard.interfaceType: %d",self.atmCard.interfaceType);
    [self callControllerUpdateTitle];

}

- (void)detectCell:(ZPPaymentViewCell *)cell didEditCardNumberWithDetectedBank:(ZPBank *)bank {
    if (!bank) {
        [self showAtmPinView];
        return;
    }
}

- (BOOL)isAtmMapcard {
    DDLogInfo(@"self.bill.transType: %ld", (long) self.bill.transType);
    return self.bill.transType == ZPTransTypeAtmMapCard;
}

- (void)detectCreditCard:(ZPPaymentViewCell *)cell didDetectBank:(NSDictionary *)bank {
    DDLogInfo(@"detectCreditCard: %@", bank);
    DDLogInfo(@"alertView: form redirect to credit card");
    ZPPaymentViewCell *cellDetect = (ZPPaymentViewCell *)
            [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    NSDictionary *params = [cellDetect outputParams];
    if (self.delegate && [self.delegate respondsToSelector:@selector(paymentControllerSwitchToCreditCardHandler:)]) {
        [self.delegate paymentControllerSwitchToCreditCardHandler:params[@"number"]];
    }
}

- (void)writeLogTimeToServer {
    ZPMapCardLog *log = [ZPMapCardLog new];
    log.transid = atmCard.zpTransID;
    log.pmcid = @(self.channel.channelID);
    log.atmcaptcha_begindate = @(self.model.atmCaptchaStartTime);
    log.atmcaptcha_enddate = @(self.model.atmCaptchaEndTime);
    log.atmotp_begindate = @(self.model.atmOtpStartTime);
    log.atmotp_enddate = @(self.model.atmOtpEndTime);
    [ZPPaymentManager writeMapCardLog:log];
}

- (long long)setLogTime:(long long)timeParam isUpdate:(BOOL)isUpdate {
    if (isUpdate) {
        return (long long) ([[NSDate date] timeIntervalSince1970] * 1000);
    }
    if (timeParam != 0) {
        return timeParam;
    }
    return (long long) ([[NSDate date] timeIntervalSince1970] * 1000);
}

- (void)btnOkClicked {

    [self processPayment];
}


#pragma mark - Utils

- (NSString *)getOptionValueAt:(NSInteger)idx {
    if (_arrList.count == 0 || !_arrList) {
        return nil;
    }
    return [_arrList safeObjectAtIndex:idx];
}

- (BOOL)isCheckedAt:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        self.checkedIndexPath = indexPath;
    }
    return [self.checkedIndexPath isEqual:indexPath];
}

- (void)loadWebView:(UIWebView *)wv {
    if (!wv) {
        return;
    }

    if (self.webviewUrl != nil && ![self.webviewUrl isEqualToString:@""]) {
        [self initWebView:wv];
        [self loadRequestWithStringUrl:wv :self.webviewUrl];
        self.webviewUrl = nil;
    }
}

- (void)setImageCellAt:(NSIndexPath *)indexPath {
    if (!indexPath) {
        return;
    }
    ZPPaymentViewCell *cell = [ZPPaymentViewCell castFrom:[self.mTableView cellForRowAtIndexPath:indexPath]];
    if (!cell) {
        return;
    }
    @weakify(cell);
    [[loadImageFromString(atmCard.captchaBase64)
            takeUntil:[cell rac_prepareForReuseSignal]]
            subscribeNext:^(UIImage *img) {
                @strongify(cell);
                [cell setCaptchaImage:img];
            }];
}

@end


