//
//  ZPBIDVHandler.h
//  ZaloPayWalletSDK
//
//  Created by thi la on 6/5/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//


#import "ZPBankHandlerBase.h"
#import "ZPPaymentMethod.h"

@interface ZPBIDVHandler : ZPBankHandlerBase<UITableViewDelegate, UITableViewDataSource>

@end
