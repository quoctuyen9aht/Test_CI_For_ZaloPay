//
//  ZPBankHandlerBase.h
//  ZaloPayWalletSDK
//
//  Created by thi la on 6/17/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPPaymentHandler.h"

@class ZPAtmPinnedView;
@class ZPATMHandlerWebModel;
@class ZPATMSaveCardCell;
@class ZPBank;

@interface ZPBankHandlerBase : ZPPaymentHandler
@property (strong, nonatomic) ZPAtmPinnedView * atmPinnedView;
@property (assign, nonatomic) BOOL isLoading;
@property (strong, nonatomic) NSTimer *progressHUDTimer;
@property (assign, nonatomic) bool didReturned;
@property (strong, nonatomic) ZPATMHandlerWebModel *model;

- (void)writeLogTimeToServer;
- (void)showAtmPinView;
- (void)showProgressHUD;
- (void)hideProgressHUD;
- (void)hideProgressHUDWithResultCode:(int)code;
- (void)getStatusOfTranxId:(NSString *)tranxId
                  callback:(void (^)(ZPPaymentResponse *response))callback;
- (void)notifyGoToLinkBankAccount:(NSString *)bankCode;
- (NSString *)message_guideUserLinkToMapAccount:(NSString *)bankCode;
- (NSString *)message_guideUserChooseSavedCard:(ZPAtmCard *)atm;
- (int)bankSupportType:(NSString *)bankCode;
- (NSString *)getBankNameForAlert;
- (void)switchToATMHandler:(ZPBank*)bank;
- (BOOL)checkErrorWithMethod:(ZPPaymentMethod *)method;
- (BOOL)checkBankMaintain:(ZPBank*)bank;
- (BOOL)validateMiniBank:(ZPBank*)bank;
- (BOOL)checkMiniBankMaintain:(ZPBank*)bank;
- (void)callControllerUpdateTitle;
- (void)showAlertWithMessage:(NSString *)errorMessage;
- (void)clearInvalidAtmNumber;
- (void)dissmissKeyBoardFromATMCell;
- (void)notifyHideNextButton;
- (UITableViewCell *)activeCell;
- (void)showAlertUpdateVersion:(NSString *)bankName;
- (void)updateATMCardInfo:(NSDictionary*)data;
- (void)updateConfigATMCard;
- (void)handleRetryOTP:(NSString*)message;
- (void)handleCellWillBack;

/**
 Using for kyc, some case need check ....
 */
- (void)verifyCardForMapping;

/**
 Run flow mapcard
 */
- (void)internalMapCard;
@end
