//
// ZaloPayWalletSDK
//
// Created by Huu Hoa Nguyen on 3/21/18.
// Copyright (c) 2018 VNG Corporation. All rights reserved.
//

#import "ZPSBIBankHandler.h"


#import "ZPPaymentInfo.h"
#import "ZPBill.h"
#import "ZPDataManager.h"
#import "ZPPaymentChannel.h"
#import "ZPPaymentManager.h"
#import "ZaloPayWalletSDKPayment.h"
#import "ZPAtmResponseObject.h"
#import "ZPPaymentResponse.h"
#import "ZPBank.h"

#import "ZPCreditCardWebviewCell.h"
#import "ZPATMSaveCardCell.h"
#import "ZPOtpCell.h"

#import "ZPSCBHandler+Cell.h"
#import "ZPATMHandlerWebModel.h"
#import "ZaloPayWalletSDKLog.h"
#import "ZPMapCardLog.h"


@interface ZPSBIBankHandler ()
@property(assign, nonatomic) BOOL isEnabledClickButton;
@end

@implementation ZPSBIBankHandler

@synthesize currentStep, atmCard, paymentMethodDidChoose;

- (void)dealloc {

}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.atmCard = [[ZPAtmCard alloc] init];
        self.currentStep = ZPAtmPaymentStepAutoDetectBank;
        self.didReturned = false;
        self.model = [[ZPATMHandlerWebModel alloc] initWithStartEvent:nil
                                                                  end:nil
                                                                 with:nil];
    }
    return self;
}

#pragma mark - Overrides

- (void)initialization {
    [super initialization];
    atmCard.amount = self.bill.amount;
}

- (void)viewWillBack {
    [self handleCellWillBack];
}

- (BOOL)okButtonEnabled {
    return self.isEnabledClickButton || currentStep == ZPAtmPaymentStepSavedCardConfirm;
}

- (NSString *)paymentControllerTitle {
    return [self.dataManager getTitleByTranstype:self.bill.transType];
}

- (NSString *)okButtonTitle {
    return (currentStep == ZPAtmPaymentStepSavedCardConfirm) ?
            [[self.dataManager getButtonTitle:@"confirm"] capitalizedString] :
            [[self.dataManager getButtonTitle:@"continue"] capitalizedString];
}


#pragma mark - UITableViewDelegate & DataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    if (currentStep != ZPAtmPaymentStepAutoDetectBank) {
        [self notifyHideNextButton];
    }

    UITableViewCell *nCell = [self createCellAt:indexPath with:tableView];
    ZPPaymentViewCell *cell = [ZPPaymentViewCell castFrom:nCell];
    if (cell) {
        self.isEnabledClickButton = NO;
        [cell updateLayouts];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
    }
    return nCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1) {
        return 0;
    }
    if (currentStep == ZPAtmPaymentStepAutoDetectBank) {
        if (indexPath.section == 1) {
            return 0;
        }
    }

    CGFloat height = [super tableView:tableView heightForRowAtIndexPath:indexPath];

    if (height > 0) {
        return height;
    } else {
        return MAX(0, [self expectedPaymentCellHeight:tableView]);
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (currentStep == ZPAtmPaymentStepSavedCardConfirm) {
        if ([self isRequirePin]) {
            return 1;
        }
        ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:self.atmCard.bankCode];
        if (bank.otpType.length > 0 && [[bank.otpType componentsSeparatedByString:@"|"] count] >= 2) {
            return 2;
        }

    }
    if (currentStep == ZPAtmPaymentStepAutoDetectBank) {
        return 2;
    }
    return 1;
}

// Hidden Accept Button
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == tableView.numberOfSections - 1) {
        if ((currentStep == ZPAtmPaymentStepGetStatusByAppTransIdForClient ||
             currentStep == ZPAtmPaymentStepAutoDetectBank)) {
            return nil;
        }
        return [self configFooter];
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == tableView.numberOfSections - 1) {
        if ( currentStep == ZPAtmPaymentStepGetStatusByAppTransIdForClient ||
            currentStep == ZPAtmPaymentStepAutoDetectBank) {
            return 0;
        }
        return 95;
    }
    return 0;
}

- (void)setFirstResponder {
   if (self.currentStep == ZPAtmPaymentStepAutoDetectBank) {
        ZPATMSaveCardCell *cell = (ZPATMSaveCardCell *)[self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
        if ([cell isKindOfClass:[ZPATMSaveCardCell class]]) {
            [cell excuteView];
        }
    } else if (self.currentStep == ZPAtmPaymentStepAtmAuthenPayer) {
        ZPOtpCell *cell = (ZPOtpCell *) [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
        if ([cell isKindOfClass:[ZPOtpCell class]]) {
            [cell.txtOtpValue becomeFirstResponder];
        }
    }
}

- (float)expectedPaymentCellHeight:(UITableView *)tableView {
    DDLogInfo(@"atm currentStep: %ld", (long) self.currentStep);
    switch (self.currentStep) {
        case ZPAtmPaymentStepSubmitTrans:
        case ZPAtmPaymentStepAutoDetectBank:
            if (IS_IPHONE_4_OR_LESS) {

                return (SCREEN_WIDTH - kMarginCardView) * (1 / (ratioCardView + 1)) + cardViewScrollHeight - 5;
            }
            //Add
            return (SCREEN_WIDTH - kMarginCardView) * (1 / ratioCardView) + cardViewScrollHeight;

        case ZPAtmPaymentStepAtmAuthenPayer:
        case ZPAtmPaymentStepSavedCardConfirm:
        case ZPAtmPaymentStepGetStatusByAppTransIdForClient:
            return 60;
        default:
            break;
    }
    return 0;
}

#pragma ZPPaymentCellDelegate

- (void)validateInputDoneAndCallProcessPayment {
    [self dismissKeyboard];
    [self processPayment];
}

- (void)validateInputCompleted:(BOOL)isValid {
    self.isEnabledClickButton = isValid;
    [self updateOKButtonColor];
}

#pragma mark - Private methods
- (NSString *)okButtonBackgroundColor {
    if ((self.currentStep == ZPAtmPaymentStepSavedCardConfirm)
            || (self.currentStep == ZPAtmPaymentStepAtmAuthenPayer && self.isEnabledClickButton)) {
        return @"#06be04";
    } else if (!self.isEnabledClickButton) {
        return @"#C7C7CC";
    }
    return @"#008FE5";
}

/**
 * @callgraph
 * Handle payment steps for Eximbank
 */
- (void)processPayment {
    [self dismissKeyboard];
    self.didReturned = false;
    if ([self checkOrderTimeout]) {
        return;
    }

    if (![[ZPDataManager sharedInstance] checkInternetConnect]) {
        [self showAlertNotifyNetWorkProblem];
        return;
    }
    self.atmCard.step = currentStep;
    ZPPaymentViewCell *cell = [ZPPaymentViewCell castFrom:[self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]]];
    DDLogInfo(@"processPayment currentStep: %ld", (long) currentStep);
    switch (currentStep) {
        case ZPAtmPaymentStepAutoDetectBank: {
            DDLogInfo(@"--processPayment: ZPAtmPaymentStepAutoDetectBank");
            [[ZPAppFactory sharedInstance].orderTracking trackUserInputCardInfo:self.bill.appTransID];
            if (![cell checkValidate]) {
                return;
            }
            currentStep = ZPAtmPaymentStepSubmitTrans;
            [self processPayment];
            return;
        }
        case ZPAtmPaymentStepSubmitTrans: {
            DDLogInfo(@"--processPayment: ZPAtmPaymentStepSubmitTrans");
            if (![cell checkValidate]) {
                return;
            }
            NSDictionary *params = [cell outputParams];
            [self updateATMCardInfo:params];
            if ([self isAtmMapcard]) {
                [self verifyCardForMapping];
                return;
            }
            [self submitTran];
            break;
        }
        case ZPAtmPaymentStepAtmAuthenPayer: {
            DDLogInfo(@"--processPayment: ZPAtmPaymentStepAtmAuthenPayer");
            if (![cell checkValidate]) {
                return;
            }
            atmCard.authenValue = [[cell outputParams] stringForKey:@"otp"];
            [self submitAuthenPayer];
            break;
        }

        case ZPAtmPaymentStepSavedCardConfirm: {
            DDLogInfo(@"processPayment, _currentStep: ZPAtmPaymentStepSavedCardConfirm");
            [self updateConfigATMCard];
            [self submitTran];
            break;
        }
        case ZPAtmPaymentStepGetStatusByAppTransIdForClient: {
            self.atmCard.step = ZPAtmPaymentStepGetStatusByAppTransIdForClient;
            [self submitTran];
            break;
        }
        default:
            break;
    }
}

- (BOOL)checkOrderTimeout {
    //time out
    NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
    if (self.model.atmStartTimestamp > 0 && now - self.model.atmStartTimestamp > kATM_TRANSATION_TIMEOUT) {
        NSString *message = [self.dataManager messageForKey:@"message_atm_time_out"];
        [self finishWithErrorCode:0 message:message
                           status:kZPZaloPayCreditStatusCodeFail
                           result:-1
                        errorStep:ZPErrorStepNonRetry];
        self.didReturned = YES;
        return YES;
    }
    return NO;
}

- (void)submitAuthenPayer {
    [self showProgressHUD];
    [ZPPaymentManager paymentWithBill:self.bill
                       andPaymentInfo:atmCard
                             andAppId:self.bill.appId
                              channel:self.channel
                               inMode:ZPPaymentMethodTypeAtm
                          andCallback:^(ZPPaymentResponse *responseObject) {
                              [self preHandlePaymentResponse:responseObject];
                          }];
}

- (void)preHandlePaymentResponse:(ZPResponseObject *)responseObject {
    [self hideProgressHUD];
    ZPPaymentResponse *response = (ZPPaymentResponse *) responseObject;

    if (responseObject.errorCode == kZPZaloPayCreditErrorCodeUnknownException) {
        currentStep = ZPAtmPaymentStepGetTransStatus;
        [self getStatusOfTranxId:self.atmCard.zpTransID callback:^(ZPPaymentResponse *response) {

        }];
        return;
    }
    if (responseObject.errorCode == ZPServerErrorCodeAtmRetryOtp) {
        [self handleRetryOTP:response.message];
        return;
    }
    response.zpTransID = self.atmCard.zpTransID;
    [self handlePaymentResponse:response];
}

- (void)internalMapCard {

    if (![[ZPDataManager sharedInstance] checkInternetConnect]) {
        [self showAlertNotifyNetWorkProblem];
        return;
    }
    self.isSubmitStrans = YES;
    [self showProgressHUD];
    self.channel = [self.dataManager channelWithTypeAndBankCode:ZPPaymentMethodTypeAtm bankCode:self.atmCard.bankCode];
    [[ZPAppFactory sharedInstance].orderTracking trackChoosePayMethod:self.bill.appTransID pcmid:self.channel.channelID bankCode:self.atmCard.bankCode result:OrderStepResult_Success];

    [ZPPaymentManager paymentWithBill:self.bill
                       andPaymentInfo:atmCard
                             andAppId:self.bill.appId
                              channel:self.channel
                               inMode:ZPPaymentMethodTypeAtm
                          andCallback:^(ZPResponseObject *responseObject) {

                              ZPPaymentResponse *response = (ZPPaymentResponse *) responseObject;
                              if (response.zpTransID.length > 0) {
                                  self.atmCard.zpTransID = response.zpTransID;
                              }

                              [self hideProgressHUDWithResultCode:response.errorCode];

                              if ([self handleExceptionCaseWith:response.errorCode]) {
                                  return;
                              }

                              self.model.atmStartTimestamp = [[NSDate date] timeIntervalSince1970];

                              [self handlePaymentWith:response];
                          }];
}


#pragma mark SubmitTrans

- (void)submitTran {

    if (![[ZPDataManager sharedInstance] checkInternetConnect]) {
        [self showAlertNotifyNetWorkProblem];
        return;
    }
    self.isSubmitStrans = YES;
    [self showProgressHUD];
    [self updateChannel];

    [[ZPAppFactory sharedInstance].orderTracking trackChoosePayMethod:self.bill.appTransID pcmid:self.channel.channelID bankCode:self.atmCard.bankCode result:OrderStepResult_Success];

    [ZPPaymentManager paymentWithBill:self.bill
                       andPaymentInfo:atmCard
                             andAppId:self.bill.appId
                              channel:self.channel
                               inMode:ZPPaymentMethodTypeAtm
                          andCallback:^(ZPResponseObject *responseObject) {
                              ZPPaymentResponse *response = (ZPPaymentResponse *) responseObject;
                              if (response.zpTransID.length > 0) {
                                  self.atmCard.zpTransID = response.zpTransID;
                              }

                              [self hideProgressHUDWithResultCode:response.errorCode];

                              if ([self handleExceptionCaseWith:response.errorCode]) {
                                  return;
                              }

                              self.model.atmStartTimestamp = [[NSDate date] timeIntervalSince1970];

                              [self handlePaymentWith:response];

                          }];
}

- (void)updateChannel {
    if (self.paymentMethodDidChoose == nil) {
        self.channel = [self.dataManager channelWithTypeAndBankCode:ZPPaymentMethodTypeAtm bankCode:self.atmCard.bankCode];
    }
    // set payment channel to zalopaywallet while using withdraw
    if (self.bill.transType == ZPTransTypeWithDraw) {
        self.channel = [self.dataManager channelWithTypeAndBankCode:ZPPaymentMethodTypeZaloPayWallet bankCode:self.atmCard.bankCode];
    }
}

- (void)handlePaymentWith:(ZPPaymentResponse *)response {
    int resultCode = response.errorCode;
    if (resultCode == kZPZaloPayCreditErrorCodePinRetry) {
        [self showAlertNotifyRetryPin:response.message];
        return;
    }
    if (resultCode == ZPServerErrorCodeAtmRetryOtp) {
        [self handleRetryOTP:response.message];
        return;
    }
    if (resultCode == ZPServerErrorCodeNoneError) {
        [self saveAtmCard:self.atmCard];
        if ([self isRequirePin] && [ZaloPayWalletSDKPayment sharedInstance].appDependence) {
            [[ZaloPayWalletSDKPayment sharedInstance].appDependence savePassword:self.atmCard.pin];
        }
        response.zpTransID = self.atmCard.zpTransID;
        [self handlePaymentResponse:response];
        return;
    }
    if (resultCode > ZPServerErrorCodeNoneError) {
        [self saveAtmCard:self.atmCard];
        DDLogInfo(@"self.atmCard.zpTransID %@", self.atmCard.zpTransID);
        if (response.isProcessing && response.data.length > 0) {
            if (response.actionType == ZPBankActionTypeWeb) {
                // case dùng webview

            } else {
                [self bankSupportAPI];
            }
            return;
        }
        if (response.exchangeStatus != kZPZaloPayCreditStatusCodeUnidentified) {
            response.exchangeStatus = kZPZaloPayCreditStatusCodeSuccess;
        }
        response.zpTransID = self.atmCard.zpTransID;
        [self handlePaymentResponse:response];
        return;
    }
    if (resultCode < ZPServerErrorCodeNoneError) {
        response.exchangeStatus = kZPZaloPayCreditStatusCodeFail;
        response.zpTransID = self.atmCard.zpTransID;
        [self handlePaymentResponse:response];
    }
}

- (BOOL)handleExceptionCaseWith:(int)resultCode {
    if ((resultCode == kZPZaloPayCreditErrorCodeRequestTimeout || resultCode == ZPServerErrorCodeTransIdNotExist) && currentStep != ZPAtmPaymentStepGetStatusByAppTransIdForClient) {
        if ([self isAtmMapcard]) {
            return NO;
        }
        currentStep = ZPAtmPaymentStepGetStatusByAppTransIdForClient;
        [self.delegate paymentControllerNeedUpdateLayouts];
        [self performSelector:@selector(processPayment) withObject:self afterDelay:1];
        return YES;
    }
    // hanle server not response
    if (resultCode == kZPZaloPayCreditErrorCodeUnknownException) {
        currentStep = ZPAtmPaymentStepGetTransStatus;
        [self getStatusOfTranxId:self.atmCard.zpTransID callback:^(ZPPaymentResponse *response) {

        }];
        return YES;
    }
    return NO;
}

- (void)bankSupportAPI {
    currentStep = ZPAtmPaymentStepAtmAuthenPayer;
    // Card da luu
    if (([self isAtmMapcard] == false && self.paymentMethodDidChoose.methodType == ZPPaymentMethodTypeSavedCard)) {

        [self.delegate paymentControllerNeedUpdateLayouts];
        [self setFirstResponder];
    }

        //Map card and pay
    else {
        ZPPaymentViewCell *cell = (ZPPaymentViewCell *) [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [cell setOTP];
        });
    }
}

- (void)finishWithErrorCode:(int)errorCode
                    message:(NSString *)message
                     status:(int)status
                     result:(int)result
                  errorStep:(enum ZPErrorStep)errorStep {
    [self hideProgressHUD];
    ZPPaymentResponse *pResponse = [[ZPPaymentResponse alloc] init];
    pResponse.errorCode = errorCode;
    pResponse.originalCode = errorCode;
    pResponse.message = message;
    pResponse.exchangeStatus = status;
    pResponse.errorStep = errorStep;
    pResponse.zpTransID = atmCard.zpTransID;
    [self handlePaymentResponse:pResponse];
    [self writeLogTimeToServer];
}

- (void)handlePaymentResponse:(ZPPaymentResponse *)response {
    response.paymentMethod = ZPPaymentMethodTypeAtm;
    if (response.errorStep == ZPErrorStepAbleToRetry) {
        if (currentStep == ZPAtmPaymentStepSubmitTrans) {
            currentStep = ZPAtmPaymentStepSubmitTrans;
        }
    }
    [super handlePaymentResponse:response];
}

#pragma mark - ZPAPaymentViewCellDelegate

- (void)detectCell:(ZPPaymentViewCell *)cell didDetectBank:(ZPBank *)bank {
    DDLogInfo(@"detectCell didDetectBank: %@", bank.bankName);
    NSString *bankCode = bank.bankCode;
    if (bankCode.length == 0) {
        [self switchToATMHandler:bank];
        return;
    }

    ZPPaymentMethod *method = [self validateBank:bankCode];

    if ([self checkErrorWithMethod:method]) {
        return;
    }

    if ([[ZPDataManager sharedInstance] chekMinAppVersion:method.minAppVersion]) {
        [self showAlertUpdateVersion:[self getBankNameForAlert]];
        return;

    }
    if ([self checkBankMaintain:bank]) {
        return;
    }
    
    if ([[ZPDataManager sharedInstance] chekMinAppVersion:bank.minAppVersion]) {
        [self showAlertUpdateVersion:[self getBankNameForAlert]];
        return;
    }

    if ([self validateMiniBank:bank]) {
        return;
    }

    if ([self checkMiniBankMaintain:bank]) {
        return;
    }

    //self.title = [bank objectForKey:@"name"];
    //Add SDK
    self.atmCard.bankCode = ((ZPATMSaveCardCell *)cell).bankCode;
    self.atmCard.bankName = bank.bankName;
    self.atmCard.interfaceType = bank.interfaceType;
    self.atmCard.requireOTP = bank.requireOTP;
    DDLogInfo(@"self.atmCard.interfaceType: %d",self.atmCard.interfaceType);
    [self callControllerUpdateTitle];
}

- (void)detectCell:(ZPPaymentViewCell *)cell didEditCardNumberWithDetectedBank:(NSDictionary *)bank {
    if (!bank) {
        [self showAtmPinView];
        return;
    }
}

- (BOOL)isAtmMapcard {
    DDLogInfo(@"self.bill.transType: %ld", (long) self.bill.transType);
    return self.bill.transType == ZPTransTypeAtmMapCard;
}

- (void)detectCreditCard:(ZPPaymentViewCell *)cell didDetectBank:(NSDictionary *)bank {
    DDLogInfo(@"detectCreditCard: %@", bank);
    DDLogInfo(@"alertView: form redirect to credit card");
    ZPPaymentViewCell *cellDetect = (ZPPaymentViewCell *)
            [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    NSDictionary *params = [cellDetect outputParams];
    if (self.delegate && [self.delegate respondsToSelector:@selector(paymentControllerSwitchToCreditCardHandler:)]) {
        [self.delegate paymentControllerSwitchToCreditCardHandler:params[@"number"]];
    }
}

- (void)writeLogTimeToServer {
    ZPMapCardLog *log = [ZPMapCardLog new];
    log.transid = atmCard.zpTransID;
    log.pmcid = @(self.channel.channelID);
    log.atmcaptcha_begindate = @(self.model.atmCaptchaStartTime);
    log.atmcaptcha_enddate = @(self.model.atmCaptchaEndTime);
    log.atmotp_begindate = @(self.model.atmOtpStartTime);
    log.atmotp_enddate = @(self.model.atmOtpEndTime);
    [ZPPaymentManager writeMapCardLog:log];
}

- (void)btnOkClicked {
    [self processPayment];
}

@end
