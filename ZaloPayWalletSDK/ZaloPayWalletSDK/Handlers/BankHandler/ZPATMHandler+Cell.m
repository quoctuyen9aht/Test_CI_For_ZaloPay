//
//  ZPATMHandler+Cell.m
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPATMHandler+Cell.h"
#import "ZPManagerSaveCardCell.h"
#import "UIColor+ZPExtension.h"
#import "ZPBill.h"
#import "ZPDataManager.h"
#import "ZPPaymentInfo.h"
#import "ZPATMSaveCardCell.h"
#import "ZPCellIdentifier.h"

@implementation ZPATMHandler (Cell)

- (UITableViewCell *)createCellAt:(NSIndexPath *)indexPath with:(UITableView *)tableView {
    //Add
    ZPATMSaveCardCell *nCell = [ZPATMSaveCardCell castFrom:[self.dataManager viewWithNibName:identify(ZPATMCellTypeAutoDetect) owner:self]];

    if ([self.atmCard.cardNumber length] > 0) {
        [nCell setATMCardInfo:self.atmCard fromCC:self.isBackFromCC];
    }

    nCell.mTableView = self.mTableView;
    nCell.zpParentController = self.zpParentController;
    nCell.hasAmount = YES;
    nCell.suggestAmount = self.bill.amount;
    nCell.channel = self.channel;
    nCell.isMapCard = [self isAtmMapcard];
    [ZPManagerSaveCardCell sharedInstance].isShowCreditCardCell = false;
    [ZPManagerSaveCardCell sharedInstance].isShowAMTCardCell = true;
    [nCell setBackgroundColor:[UIColor zpColorFromHexString:@"#F0F4F6"]];
    return nCell;

}

@end




