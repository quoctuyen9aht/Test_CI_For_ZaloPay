//
// Created by Huu Hoa Nguyen on 3/21/18.
// Copyright (c) 2018 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ZPBankHandlerBase.h"

@interface ZPSBIBankHandler : ZPBankHandlerBase
@end
