//
//  ZPSGCBHandler+Cell.m
//  ZaloPayWalletSDK
//
//  Created by thi la on 6/6/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPSGCBHandler+Cell.h"

#import "ZPCreditCardWebviewCell.h"
#import "ZPATMSaveCardCell.h"
#import "ZPManagerSaveCardCell.h"
#import "ZPDidChoosePaymentMethodCell.h"
#import "ZPOtpCell.h"
#import "ZPAtmAuthenTypeViewCell.h"

#import "ZPPaymentInfo.h"
#import "ZPDataManager.h"
#import "UIColor+ZPExtension.h"
#import "ZPResourceManager.h"
#import "UITableViewCell+ZPExtension.h"
#import "ZPCellIdentifier.h"
#import "ZaloPayWalletSDKLog.h"
#import "ZPBill.h"
#import "ZPBank.h"

@implementation ZPSGCBHandler(Cell)
- (UITableViewCell *)createCellAt:(NSIndexPath *)indexPath with:(UITableView *)tableView {
    switch (self.currentStep) {
        case ZPAtmPaymentStepAutoDetectBank: {
            if (indexPath.section == 0) {
                //Add
                ZPATMSaveCardCell *nCell = [ZPATMSaveCardCell castFrom:[self.dataManager viewWithNibName:identify(ZPATMCellTypeAutoDetect) owner:self]];

                if ([self.atmCard.cardNumber length] > 0) {
                    [nCell setATMCardInfo:self.atmCard fromCC:self.isBackFromCC];
                }

                nCell.mTableView = self.mTableView;
                nCell.zpParentController = self.zpParentController;
                nCell.hasAmount = YES;
                nCell.suggestAmount = self.bill.amount;
                nCell.channel = self.channel;
                nCell.isMapCard = [self isAtmMapcard];
                [ZPManagerSaveCardCell sharedInstance].isShowCreditCardCell = false;
                [ZPManagerSaveCardCell sharedInstance].isShowAMTCardCell = true;
                [nCell setBackgroundColor:[UIColor zpColorFromHexString:@"#F0F4F6"]];
                return nCell;
            }
            return [UITableViewCell makeATemplateCell];
        }
        case ZPAtmPaymentStepWaitingWebview: {
            DDLogInfo(@"--cellForRowAtIndexPath: ZPAtmPaymentStepWaitingWebview");
            ZPCreditCardWebviewCell *nCell = [ZPCreditCardWebviewCell cellFrom:tableView
                                                                          with:identify(ZPATMCellTypeCardWebView)];
            [self loadWebView:nCell.ccWebview];
            [nCell setBackgroundColor:[UIColor whiteColor]];
            return nCell;
        }
        case ZPAtmPaymentStepAtmAuthenPayer: {
            if (indexPath.section == 0) {
                DDLogInfo(@"--cellForRowAtIndexPath: ZPAtmPaymentStepAtmAuthenPayer");
                ZPOtpCell *nCell = [ZPOtpCell cellFrom:tableView with:identify(ZPATMCellTypeOtp)];
                nCell.txtOtpValue.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString([[ZPDataManager sharedInstance] messageForKey:@"zalo_pay_otp_message"], @"")];
                [nCell setBackgroundColor:[UIColor whiteColor]];
                [nCell setupKeyboardType:self.atmCard.bankCode];
                return nCell;
            }
            return [UITableViewCell makeATemplateCell];
        }
        case ZPAtmPaymentStepSavedCardConfirm: {
            DDLogInfo(@"--cellForRowAtIndexPath: ZPAtmPaymentStepSavedCardConfirm");
            tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
            UITableViewCell *cell;
            if (indexPath.section == 0) {
                ZPDidChoosePaymentMethodCell *nCell = [ZPDidChoosePaymentMethodCell cellFrom:tableView with:identify(ZPATMCellTypeDidChoosePaymentMethod)];;
                nCell.selectionStyle = UITableViewCellSelectionStyleNone;
                nCell.bankIcon.image = [ZPResourceManager getImageWithName:self.paymentMethodDidChoose.methodIcon];
                nCell.title.text = self.paymentMethodDidChoose.methodName;
                ZPBank *bank = [self.dataManager.bankManager getBankInfoWith:self.atmCard.bankCode];
                nCell.bankName.text = bank.bankName ?: @"";
                cell = nCell;
            } else if (indexPath.section == 1) {
                ZPAtmAuthenTypeViewCell *nCell = [ZPAtmAuthenTypeViewCell cellFrom:tableView with:identify(ZPATMCellTypeAuthen)];
                DDLogInfo(@"method.savedCard.bankCode2: %@", self.atmCard.bankCode);
                nCell.bankCode = self.atmCard.bankCode;
                cell = nCell;
            }
            [cell setBackgroundColor:[UIColor whiteColor]];
            return cell ?: [UITableViewCell makeATemplateCell];
        }        
        case ZPAtmPaymentStepGetStatusByAppTransIdForClient : {
            tableView.separatorStyle = UITableViewCellSelectionStyleNone;
            return [self createCellWithTableView:tableView];

        }
        default: {
            DDLogInfo(@"--default");
            return [UITableViewCell makeATemplateCell];
            //TODO (not important) - find a way to keep current layout unchanged
        }
    }
}

@end
