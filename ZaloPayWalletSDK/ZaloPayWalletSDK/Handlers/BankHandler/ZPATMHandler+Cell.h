//
//  ZPATMHandler+Cell.h
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 4/24/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZPATMHandler.h"
@interface ZPATMHandler(Cell)
- (UITableViewCell *)createCellAt:(NSIndexPath *)indexPath with:(UITableView *)tableView;
@end

