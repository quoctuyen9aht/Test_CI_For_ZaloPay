//
//  ZPBankHandlerBase.m
//  ZaloPayWalletSDK
//
//  Created by thi la on 6/17/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPBankHandlerBase.h"
#import "ZPAtmPinnedView.h"
#import "ZPProgressHUD.h"
#import "ZPDataManager.h"
#import "ZPBill.h"
#import "ZPPaymentInfo.h"
#import "ZPPaymentManager.h"
#import "ZPResponseObject.h"
#import "ZPPaymentResponse.h"
#import "ZPMiniBank.h"
#import "ZPATMSaveCardCell.h"
#import "NSString+ZPExtension.h"
#import "ZPResourceManager.h"
#import "ZPPaymentMethod.h"
#import "ZPBank.h"
#import "ZaloPayWalletSDKLog.h"
#import "ZaloPayWalletSDKPayment.h"
#import <ZaloPayAnalyticsSwift/ZaloPayAnalyticsSwift-Swift.h>

@interface ZPBankHandlerBase()
@property (assign, nonatomic) BOOL isCollectedKYC;
@end

@implementation ZPBankHandlerBase
- (void)writeLogTimeToServer {
}

- (void)showAtmPinView {
    [self dismissKeyboard];  
    if (!self.atmPinnedView) {
        self.atmPinnedView = [[ZPAtmPinnedView alloc] init];
    }
    self.atmPinnedView.delegate = self;
    [self.atmPinnedView showInView:self.zpParentController.view withData:[self.dataManager.bankManager getArrayBankInPinViewWithTranstype:self.bill.transType]];
}

#pragma mark - ZPProgressHUD

- (void)showProgressHUD {
    if (self.currentStep == ZPAtmPaymentStepSavedCardConfirm) {
        return;
    }
    [ZPProgressHUD showWithStatus:@""];
    if (self.progressHUDTimer) {
        [self.progressHUDTimer invalidate];
    }
    self.progressHUDTimer = [NSTimer scheduledTimerWithTimeInterval:kATM_LONG_PROGESSHUD_TIME
                                                             target:self
                                                           selector:@selector(showLongProgressHUD)
                                                           userInfo:nil
                                                            repeats:NO];
    self.isLoading = YES;

}

- (void)hideProgressHUDWithResultCode:(int)code {
    if (code != kZPZaloPayCreditErrorCodePinRetry) {
        [self dismissPinView:code == ZALOPAY_ERRORCODE_SUCCESSFUL];
    }
    [self hideProgressHUD];

}

- (void)hideProgressHUD {
    [ZPProgressHUD dismiss];
    if (self.progressHUDTimer) {
        [self.progressHUDTimer invalidate];
    }
    self.isLoading = NO;

}

- (void)showLongProgressHUD {
    [self hideProgressHUD];
    self.currentStep = ZPAtmPaymentStepGetTransStatus;
    self.didReturned = YES;
    if (self.atmCard.zpTransID.length > 0 && self.isBankLoadWebview) {
        [self getStatusOfTranxId:self.atmCard.zpTransID callback:^(ZPPaymentResponse *response) {
        }];
    }

}


- (void)getStatusOfTranxId:(NSString *)tranxId
                  callback:(void (^)(ZPPaymentResponse *response))callback {
    DDLogInfo(@"start getStatusOfTranxId: %@", tranxId);
    [self showProgressHUD];
    if (!self.atmCard) {
        [self hideProgressHUD];
        return;
    }

    self.atmCard.step = self.currentStep;
    [ZPPaymentManager paymentWithBill:self.bill
                       andPaymentInfo:self.atmCard
                             andAppId:self.bill.appId
                              channel:self.channel
                               inMode:ZPPaymentMethodTypeAtm
                          andCallback:^(ZPPaymentResponse *responseObject) {
                              // process callback
                              [self hideProgressHUD];
                              ZPPaymentResponse *response = responseObject;
                              response.paymentMethod = ZPPaymentMethodTypeAtm;
                              response.zpTransID = tranxId;
                              [self handlePaymentResponse:response];
                              [self writeLogTimeToServer];
                          }];
}

- (void)notifyGoToLinkBankAccount:(NSString *)bankCode {
    NSString *bankCodeString = [@{stringKeyBankCode2: self.atmCard.bankCode} JSONRepresentation];
    [self.delegate paymentControllerDidClose:ZP_NOTIF_PAYMENT_GO_LINKACOUNT data:bankCodeString];
}

- (NSString *)message_guideUserLinkToMapAccount:(NSString *)bankCode {
    return [NSString stringWithFormat:stringMessageBankSupportMapAccount, [self getBankNameForAlert]];
}

- (NSString *)message_guideUserChooseSavedCard:(ZPAtmCard *)atm {
    NSString *msgKey;
    NSString *defaultMsg;
    if ([self bankSupportType:atm.bankCode] != ZPSDK_BankType_Account) {
        msgKey = stringKeyBankNotSupportPayByCard;
        defaultMsg = BIDV_Not_Support_Pay_By_Card;

    } else {
        msgKey = stringKeyBankAccountPayByCard;
        defaultMsg = stringBankAccountPayByCardFormat;
    }

    NSString *bankName = [self getBankNameForAlert];
    NSString *msgFormat = [self getStringWithKey:msgKey andDefaultString:defaultMsg];
    return [NSString stringWithFormat:msgFormat, bankName];
}

- (int)bankSupportType:(NSString *)bankCode{
    ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:bankCode];
    return bank.supportType;
}

- (NSString *)getBankNameForAlert {
    NSString *bankName = self.atmCard.bankName;
    if ([bankName hasPrefix:stringBankPrefix]) {
        return [bankName stringByReplacingOccurrencesOfString:stringBankPrefix withString:@""];
    }
    return bankName;
}

- (void)switchToATMHandler:(ZPBank*)bank {
    self.atmCard.bankCode = nil;
    if(self.delegate && [self.delegate respondsToSelector:@selector(paymentControllerSwitchToAtmCardHandler:)]){
        [self.delegate paymentControllerSwitchToAtmCardHandler :bank.detectString];
        return;
    }
    [self callControllerUpdateTitle];
}

- (BOOL)validateMiniBank:(ZPBank*)bank {
    ZPMiniBank *miniBank = [[ZPDataManager sharedInstance].bankManager getBankFunctionIsSupportTranType:bank withTranType:self.bill.transType];
    int bankType = [self bankSupportType:bank.bankCode];
    if (miniBank == nil || miniBank.minibankStatus == ZPMiniBankStatus_Disable) {
        if (bankType == ZPSDK_BankType_Card) {
            [self validatePayTypeOfBank:bank];
        } else {
            [self validateBankAccount:bank];
        }
        return true;
    }

    if (miniBank.minibankStatus == ZPMiniBankStatus_Enable && bankType == ZPSDK_BankType_Account) {
        [self validateBankAccount:bank];
        return true;
    }
    return false;
}


- (void)validatePayTypeOfBank:(ZPBank*)bank {
    //Not Support Pay By Card
    if(![self isAtmMapcard]){
        NSString *bankCode = bank.bankCode;
        self.atmCard.bankCode = bankCode;
        self.atmCard.bankName = bank.bankName;
        NSString *detectString = bank.detectString;
        NSString *tfString = bank.tfString;;
        //Length
        int lengthMax = [[ZPDataManager sharedInstance] doubleForKey:@"atm_card_number_max_length"];
        int lengthMin = 0;
        BOOL checkLuhn = true;;
        if (bankCode.length > 0) {
            lengthMax = [[ZPResourceManager bankValueFromBankCodeBundle:@"bank_maxlength" andBankCode:bankCode andIsBankOrCC:true] intValue];

            //Show error save card before
            lengthMin = [[ZPResourceManager bankValueFromBankCodeBundle:@"bank_minlength" andBankCode:bankCode andIsBankOrCC:true] intValue];

            NSString *strCheck = tfString;
            //19->20

            if (detectString.length <= lengthMax) {
                strCheck = detectString;
            }

            if (strCheck.length >= lengthMin) {
                if (![ZPATMSaveCardCell checkInPutCardIsLuhnValid:strCheck]) {
                    checkLuhn = false;
                }
            }

        }

        NSArray *savedCardsForBank = [[ZPDataManager sharedInstance] getSavedCardsForBank:detectString];
        if (![ZaloPayWalletSDKPayment.sharedInstance.appDependence isBankAlreadyMapped:bankCode]) {
            [self showAlertGuideUserChooseSavedCard:savedCardsForBank];
            return;
        }

        if (detectString.length == lengthMax && checkLuhn) {
            if ([[ZPDataManager sharedInstance] checkSavedCardIsExist:detectString]) {
                [self autoChooseMethodWithCardNumber:detectString];
                return;
            }

            [self showAlertGuideUserChooseSavedCard:savedCardsForBank];
            return;
        }
    }
        //Others
    else {
        [self showAlertBankNotSupportType:bank];
    }
}

- (void)showAlertBankNotSupportType:(ZPBank*)bank {
    [self dismissKeyboard];
    NSString *bankName = [bank.bankName hasPrefix: @"NH "] ? [bank.bankName substringFromIndex:3] :bank.bankName;
    NSString *typeStr = [self.dataManager getTitleByTranstype:self.bill.transType];

    NSString *strMaintain = [NSString stringWithFormat:stringDefaultUnSupportFormat, bankName, typeStr];
    [self showAlertNotifyWith:strMaintain handler:^(NSInteger idx) {
        [self showKeyboard];
    }];
}

- (void)validateBankAccount:(ZPBank*)bank {
    NSString *bankCode = bank.bankCode;
    // Not Support Map By Card (VCB)
    self.atmCard.bankCode = bankCode;
    self.atmCard.bankName = bank.bankName;
    if([self isAtmMapcard]){
        UITableViewCell* cell = [self activeCell];
        if ([cell isKindOfClass:[ZPATMSaveCardCell class]]) {
            ZPATMSaveCardCell *cellSaveCard = (ZPATMSaveCardCell *) cell;
            cellSaveCard.isShouldHideKB = true;
            [self dismissKeyboard];
        }

        if (![ZaloPayWalletSDKPayment.sharedInstance.appDependence isBankAlreadyMapped:bankCode]) {
            [self showAlertGuideUserLinkToMapAccount];
        } else {
            [self showAlertNotifyShowBankAccount:[self getBankNameForAlert]];
            //Vietcombank
        }

    } else {
        NSArray *savedAccountsForBank = [[ZPDataManager sharedInstance] getSavedAccountForBank:bankCode];
        if (savedAccountsForBank.count >= 1) {
            [self autoChooseMethodWithBankCode:bankCode];
        } else {
            [self showAlertGuideUserChooseSavedCard:savedAccountsForBank];
        }
    }
}

- (BOOL)checkMiniBankMaintain:(ZPBank*)bank {
    ZPMiniBank *miniBank = [[ZPDataManager sharedInstance].bankManager getBankFunctionIsSupportTranType:bank withTranType:self.bill.transType];
    
    if (miniBank == nil) {
        return false;
    }
    if (miniBank.minibankStatus == ZPMiniBankStatus_Maintenance) {
        [self dismissKeyboard];
        NSString *strMaintain = miniBank.maintenancemsg;
        if (strMaintain.length == 0) {
            NSString *bankName = [bank.bankName hasPrefix: @"NH "] ? [bank.bankName substringFromIndex:3] :bank.bankName;
            NSString *typeStr = [self.dataManager getTitleByTranstype:self.bill.transType];
            NSString *timeString = [NSString timeString:miniBank.maintenanceto];

            strMaintain = [NSString stringWithFormat:stringDefaultMaintainFormat, bankName, typeStr, timeString];
        }

        [self showAlertNotifyWith:strMaintain handler:^(NSInteger idx) {
            [self clearInvalidAtmNumber];
        }];
        return true;
    }

    return false;
}

- (BOOL)checkBankMaintain:(ZPBank*)bank {
    if (bank == nil) {
        return false;
    }
    
    if (bank.status != ZPAPPEnable && !self.isShowDialogMaintenance) {
        [self dismissKeyboard];
        NSString *strMaintain = [self maintainMessageWithBank:bank];
        @weakify(self);
        [ZPDialogView showDialogWithType:DialogTypeNotification
                                 message:strMaintain
                       cancelButtonTitle:[R string_ButtonLabel_Close]
                        otherButtonTitle:nil
                          completeHandle:^(int buttonIndex, int cancelButtonIndex) {
                              @strongify(self);
                              [self clearInvalidAtmNumber];
                          }];
        return true;
    }
    return false;
}

- (BOOL)checkErrorWithMethod:(ZPPaymentMethod *)method {
    if (method == nil) {
        return false;
    }

    NSString *errorMsg = [self checkMethodIsValid:method];

    if (errorMsg.length > 0) {
        [self dismissKeyboard];
        @weakify(self);
        [ZPDialogView showDialogWithType:DialogTypeNotification
                                 message:errorMsg
                       cancelButtonTitle:[R string_ButtonLabel_Close]
                        otherButtonTitle:nil
                          completeHandle:^(int buttonIndex, int cancelButtonIndex) {
                              @strongify(self);
                              [self clearInvalidAtmNumber];
                          }];
        return true;

    }
    return false;
}

- (void)showAlertWithMessage:(NSString *)errorMessage {
    [self dismissKeyboard];
    [self showAlertNotifyWith:errorMessage handler:^(NSInteger idx) {
        [self showKeyboard];
    }];
}

- (NSString *)maintainMessageWithBank:(ZPBank *)bank {
    NSString *strMaintain = bank.maintenanceMsg;
    long long timeStamp = bank.maintainenanceTo;
    if(strMaintain == nil || strMaintain.length == 0){
        strMaintain = [NSString stringWithFormat:[self.dataManager messageForKey:@"message_bank_maintain"], bank.bankName];
    }
    return [strMaintain stringByReplacingOccurrencesOfString:@"%s" withString:[NSString timeString:timeStamp]];
}

- (void)callControllerUpdateTitle {
    if ([self.delegate respondsToSelector:@selector(paymentControllerNeedUpdateTitle)]) {
        [self.delegate paymentControllerNeedUpdateTitle];
    }
}

// Sử dụng cho những ngân hàng không hỗ trợ Pay by card.
- (void)autoChooseMethodWithCardNumber:(NSString *)cardNumber {
    NSMutableArray *arrMethod = [NSMutableArray array];
    for (ZPPaymentMethod *method in self.paymentMethods) {
        if (method.methodType == ZPPaymentMethodTypeSavedCard) {
            if ([cardNumber hasPrefix:method.savedCard.first6CardNo] && [cardNumber hasSuffix:method.savedCard.last4CardNo]) {
                [arrMethod addObject:method];
                break;
            }
        }
    }
    if (self.isShowAlertMessage) {
        return;
    }
    self.isShowAlertMessage = true;
    [self dissmissKeyBoardFromATMCell];

    NSString *alertMessage = @"";
    alertMessage = [self getStringWithKey:@"bank_alert_choose_card" andDefaultString:BIDV_Not_Support_Pay_By_Card];
    alertMessage = [NSString stringWithFormat:alertMessage, [self getBankNameForAlert], [self getBankNameForAlert]];

    @weakify(self);
    [ZPDialogView showDialogWithType:DialogTypeNotification
                             message:alertMessage
                   cancelButtonTitle:[R string_ButtonLabel_Cancel]
                    otherButtonTitle:nil
                      completeHandle:^(int buttonIndex, int cancelButtonIndex) {
                          @strongify(self);
                          if ([self.delegate respondsToSelector:@selector(paymentControllerDidGoBack)]) {
                              [self.delegate paymentControllerDidGoBack];
                          }
                      }];
}

// Sử dụng cho những ngân hàng chỉ hỗ trợ iBanking.
- (void)autoChooseMethodWithBankCode:(NSString *)bankCode {

    NSMutableArray *arrMethod = [NSMutableArray array];
    for (ZPPaymentMethod *method in self.paymentMethods) {
        if (method.methodType == ZPPaymentMethodTypeSavedAccount) {
            if ([method.savedBankAccount.bankCode isEqualToString:bankCode]) {
                [arrMethod addObject:method];
                // break;
            }
        }
    }

    if (self.isShowAlertMessage) {
        return;
    }

    self.isShowAlertMessage = true;

    [self dissmissKeyBoardFromATMCell];
    @weakify(self);
    [ZPDialogView showDialogWithType:DialogTypeNotification
                             message:stringAlertAlreadyMapBankAccount
                   cancelButtonTitle:[R string_ButtonLabel_Cancel]
                    otherButtonTitle:nil
                      completeHandle:^(int buttonIndex, int cancelButtonIndex) {
                          @strongify(self);
                          if ([self.delegate respondsToSelector:@selector(paymentControllerDidGoBack)]) {
                              [self.delegate paymentControllerDidGoBack];
                          }
                      }];

}

- (void)showAlertGuideUserLinkToMapAccount {
    if ([ZPDialogView isDialogShowing]) {
        return;
    }
    NSString *msg = [self message_guideUserLinkToMapAccount:self.atmCard.bankCode];
    @weakify(self);
    [ZPDialogView showDialogWithType:DialogTypeNotification
                             message:msg
                   cancelButtonTitle:[R string_ButtonLabel_Cancel]
                    otherButtonTitle:@[[R string_ButtonLabel_OK]] completeHandle:^(int buttonIndex, int cancelButtonIndex) {
                @strongify(self)
                if (buttonIndex == cancelButtonIndex) {
                    [self clearInvalidAtmNumber];
                    return;
                }
                [self notifyHideNextButton];
                if ([self bankSupportType:self.atmCard.bankCode] == ZPSDK_BankType_Account) {
                    [self.delegate paymentControllerSwitchToIBankingHandler:self.atmCard];
                }
            }];
}

- (void)showAlertGuideUserChooseSavedCard:(NSArray *)savedCardList {

    if ([ZPDialogView isDialogShowing]) {
        return;
    }
    [self dissmissKeyBoardFromATMCell];
    NSString *alertMessage = [self message_guideUserChooseSavedCard:self.atmCard];

    @weakify(self);
    [ZPDialogView showDialogWithType:DialogTypeNotification
                             message:alertMessage
                   cancelButtonTitle:[R string_ButtonLabel_Cancel]
                    otherButtonTitle:@[[R string_ButtonLabel_OK]]
                      completeHandle:^(int buttonIndex, int cancelButtonIndex) {
                          @strongify(self);

                          if (buttonIndex == cancelButtonIndex) {
                              [self clearInvalidAtmNumber];
                              return;
                          }
                          [self notifyHideNextButton];
                          if ([self bankSupportType:self.atmCard.bankCode] == ZPSDK_BankType_Account) {
                              [self notifyGoToLinkBankAccount:self.atmCard.bankCode];
                          } else {
                              [self notifyGoToMapCard];
                          }
                      }];
}

- (void)notifyGoToMapCard {
    UITableViewCell *cell = [self activeCell];
    if ([cell isKindOfClass:[ZPATMSaveCardCell class]]) {
        ZPATMSaveCardCell *savedCardCell = (ZPATMSaveCardCell *) cell;
        self.dataManager.cachedCardNumber = [[savedCardCell outputParams] stringForKey:@"number"];
    }
    [self.delegate paymentControllerDidClose:ZP_NOTIF_PAYMENT_GO_MAPCARD data:nil];
}

- (void)clearInvalidAtmNumber {
    UITableViewCell *cell = [self activeCell];
    if ([cell isKindOfClass:[ZPATMSaveCardCell class]]) {
        ZPATMSaveCardCell *savedCardCell = (ZPATMSaveCardCell *) cell;
        JVFloatLabeledTextField *tf = [savedCardCell getCurrentTf];
        tf.text = @"";
        tf.floatingLabel.text = stringInputCardNumber;
        [savedCardCell resetAll];
    }
    [self showKeyboard];
}

- (void)handleRetryOTP:(NSString *)message {
    [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionSdk_otp_fail];
    [self dismissKeyboard];
    @weakify(self);
    [self showAlertNotifyWith:message handler:^(NSInteger idx) {
        @strongify(self);
        [self showKeyboard];
        [self clearWrongOTP];
    }];
}

- (void)clearWrongOTP {
    UITableViewCell *cell = [self activeCell];
    if ([cell isKindOfClass:[ZPATMSaveCardCell class]]) {
        ZPATMSaveCardCell *savedCardCell = (ZPATMSaveCardCell *) cell;
        JVFloatLabeledTextField *tf = [savedCardCell getCurrentTf];
        tf.text = @"";
    }
}

- (void)notifyHideNextButton {
    [[NSNotificationCenter defaultCenter] postNotificationName:ZPUIButtonPreNextDidHideNotification object:nil];
}

- (void)dissmissKeyBoardFromATMCell {
    UITableViewCell *cell = [self activeCell];
    if ([cell isKindOfClass:[ZPATMSaveCardCell class]]) {
        ZPATMSaveCardCell *savedCardCell = (ZPATMSaveCardCell *) cell;
        savedCardCell.isShouldHideKB = true;
        savedCardCell.isEndDecelerating = false;
        [self dismissKeyboard];
    }
}

- (UITableViewCell *)activeCell {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    return [self.mTableView cellForRowAtIndexPath:indexPath];
}

- (void)showAlertNotifyShowBankAccount:(NSString *)bankName {
    NSString *message = [NSString stringWithFormat:[R string_LinkBank_BankExist], bankName];
    @weakify(self);
    [self showAlertNotifyWith:message handler:^(NSInteger idx) {
        @strongify(self);
        [self clearInvalidAtmNumber];
    }];
}

- (void)showAlertUpdateVersion:(NSString *)bankName {
    [self dismissKeyboard];
    NSString *msg = [NSString stringWithFormat:stringUpdateNewVersionFormat, [self.dataManager getTitleByTranstype:self.bill.transType], bankName];

    [self showAlertNotifyWith:msg
                 buttonTitles:@[[R string_UpdateProfileLevel3_Update_Title], [R string_ButtonLabel_Close]] handler:^(NSInteger idx) {
                if (idx == 1) {
                    [self clearInvalidAtmNumber];
                    return;
                } else {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:ZaloPayAppStoreUrl]];
                }
                [self showKeyboard];
            }];
}

- (void)updateATMCardInfo:(NSDictionary *)data {
    if (data == nil) {
        return;
    }

    self.atmCard.bankCode = [data stringForKey:@"bankcode"];
    self.atmCard.cardHolderName = [data stringForKey:@"holdername"];
    self.atmCard.cardNumber = [data stringForKey:@"number"];
    self.atmCard.validFrom = [data stringForKey:@"validfrom"];
    self.atmCard.validTo = [data stringForKey:@"validto"];
    self.atmCard.type = [data stringForKey:@"type"];
    self.atmCard.otpType = [data stringForKey:@"otptype"];
    self.atmCard.cardPassword = [data stringForKey:@"password"];
    self.atmCard.amount = [data uint32ForKey:@"chargeamount"];
    self.atmCard.step = ZPAtmPaymentStepSubmitTrans;
}

- (void)updateConfigATMCard {
    UITableViewCell *cell = [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:1]];
    if ([cell isKindOfClass:[ZPPaymentViewCell class]]) {
        ZPPaymentViewCell *paymentCell = (ZPPaymentViewCell *) cell;
        NSDictionary *params = [paymentCell outputParams];
        self.atmCard.otpType = [params stringForKey:@"otptype"];

    }

    ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:self.atmCard.bankCode];
    if (bank == nil) {
        return;
    }
    
    self.atmCard.requireOTP = bank.requireOTP;
    self.atmCard.bankName = bank.bankName;
    self.atmCard.interfaceType = bank.interfaceType;
    self.atmCard.step = ZPAtmPaymentStepSubmitPurchase;
}

- (void)handleCellWillBack {
    if (self.currentStep == ZPAtmPaymentStepAutoDetectBank) {
        ZPATMSaveCardCell *cell = [ZPATMSaveCardCell castFrom:[self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]]];
        if (cell) {
            [cell viewWillBack];
        }
    }
}

- (void)verifyCardForMapping {
    // Check need support
    id<ZPWalletDependenceProtocol> dependency = [ZaloPayWalletSDKPayment sharedInstance].appDependence;
    if (!dependency || ![dependency kycEnableMapCard]) {
        // Run normal
        [self internalMapCard];
        return;
    }
    
    // check some bank not support
    ZPBankCode code = [ZPResourceManager convertBankCodeToEnum:self.atmCard.bankCode ?: @""];
    // Case bidv 'll ignore
    if (code == ZPBankCodeBIDV) {
        [self internalMapCard];
        return;
    }
    
    if (![[ZPDataManager sharedInstance] checkInternetConnect]) {
        [self showAlertNotifyNetWorkProblem];
        return;
    }
    
    if (self.isCollectedKYC) {
        [self internalMapCard];
        return;
    }
    
    NSMutableDictionary *infor = [NSMutableDictionary new];
    NSString *bankCode = self.atmCard.bankCode ?: @"";
    NSString *bankName = self.atmCard.bankName ?: @"";
    NSString *bankNumber = self.atmCard.cardNumber ?: @"";
    [infor addEntriesFromDictionary:@{@"bankName" : bankName}];
    [infor addEntriesFromDictionary:@{@"bankNumber": bankNumber}];
    NSString *imageName = [ZPResourceManager bankIconFromBankCode:bankCode];
    if ([imageName length] > 0) {
        UIImage *image = [ZPResourceManager getImageWithName:imageName];
        if (image) {
            [infor addEntriesFromDictionary:@{@"image": image}];
        }
    }
    
    
    @weakify(self);
    [[[[ZaloPayWalletSDKPayment sharedInstance] runKYCFlow:self.zpParentController title:[R string_UpdateProfileLevel2_Title] infor:infor type:ZPKYCTypeMapCard] deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        NSDictionary *dic = [NSDictionary castFrom:x];
        [self.atmCard setKYC:dic];
        
        if (![[ZPDataManager sharedInstance] checkInternetConnect]) {
            [self showAlertNotifyNetWorkProblem];
            return;
        }
        self.isCollectedKYC = YES;
        // move to parent
        [self.zpParentController.navigationController popViewControllerAnimated:NO];
    } error:^(NSError * _Nullable error) {
        @strongify(self);
        [self.zpParentController.navigationController popViewControllerAnimated:NO];
        if (error.code == NSURLErrorCancelled) {
            if ([self.zpParentController conformsToProtocol:@protocol(KYCFlowDelegateProtocol)]) {
                [self.zpParentController performSelector:@selector(KYCFlowDidCancel)];
            }
            return;
        }
        [ZPDialogView showDialogWithError:error handle:nil];
    } completed:^{
        @strongify(self);
        [self internalMapCard];
    }];
}

- (void)internalMapCard {}
@end
