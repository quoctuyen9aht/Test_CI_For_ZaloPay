//
//  iBankingHandle.h
//  ibankingHandle
//
//  Created by CPU11695 on 11/1/16.
//  Copyright © 2016 CPU11695. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ZPDefine.h"
#import "ZPSmartLinkHelper.h"
#import "ZPBankHandlerBase.h"
#import "ZPPaymentMethod.h"
#import "ZPPaymentInfo.h"
@interface ZPIBankingHandle :ZPBankHandlerBase
@property (strong, nonatomic) UIWebView* myWebView;
@property (strong, nonatomic) ZPSmartLinkHelper * jsHelper;
@property (nonatomic, assign) NSInteger type;
@property (strong, nonatomic) ZPIbankingAccount* bankInfo;
-(void)setUpUI;
-(void)startHandle;
@end
