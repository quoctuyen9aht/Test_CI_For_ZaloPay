//
//  ZPCreditCardHandler+Vertify.h
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 4/20/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZPCreditCardHandler.h"
@class ZPCreditCardHandlerModel;

typedef NS_ENUM(NSInteger,ZPCreditCardAlertType){
    ZPCreditCardAlertTypeBankMaintain,
    ZPCreditCardAlertTypeBankNotSupport,
    ZPCreditCardAlertTypeMinAppVersion,
    ZPCreditCardAlertTypeBankMaintainWithTime,
    ZPCreditCardAlertTypeUnknown
};

@interface ZPCreditCardHandler(Vertify)
- (void)verifyCardForMappingUsing:(ZPCreditCardHandlerModel *)model;
- (void)submitTransUsing:(ZPCreditCardHandlerModel *)model;
- (void)saveCreditCard:(ZPCreditCard *)card;

- (BOOL)alertWithDetectBank:(ZPBank *)bank
                       with:(ZPCreditCardHandlerModel *)model
          excuteActionFirst:(void(^)(void))action
                    handler:(void(^)(NSInteger idx,ZPCreditCardAlertType type))handler;


@end

@interface ZPCreditCardHandlerModel : NSObject
@property (assign, nonatomic) BOOL didReturned;
@property (assign, nonatomic) BOOL isEnabledClickButton;
@property (assign, nonatomic) BOOL isShowDialogMaintenance;

@property (assign, nonatomic) NSTimeInterval ccOtpStartTime;
@property (assign, nonatomic) NSTimeInterval ccOtpEndTime;
@property (assign, nonatomic) NSTimeInterval ccStartTimestamp;

@property (copy, nonatomic) NSString *webviewUrl;
@property (strong, nonatomic) NSTimer *progressHUDTimer;
@end
