//
//  ZPMethodsHandler+Action.m
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 4/21/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPMethodsHandler+Action.h"
#import "ZPDataManager.h"
#import "ZPPaymentMethod.h"
#import "ZaloPayWalletSDKPayment.h"
#import "ZPBank.h"
#import "ZPMiniBank.h"
#import "NSString+ZPExtension.h"
#import "ZPPaymentChannel.h"
#import "ZPBill.h"
#import "ZPPaymentMethodCellDisplay.h"
#import "ZPCheckPinViewController.h"
#import "ZPMethodBaseCell.h"
#import "ZPMethodsHandler+SubmitTrans.h"
#import "ZPAddNewMethodCell.h"
#import "ZaloPayWalletSDKLog.h"
#import "ZPOrderHeaderView.h"
#import <ZaloPayAnalyticsSwift/ZaloPayAnalyticsSwift-Swift.h>
#import "ZPBank.h"
#import "ZPVoucherViewWithButton.h"
#import "ZPProgressHUD.h"
#import "ZaloPayWalletSDKPayment.h"
#import "NetworkManager+ZaloPayWalletSDK.h"
@interface ZPMethodsHandler(KYC)
@property(nonatomic, assign) BOOL isHaveKYC;
@end

@implementation ZPMethodsHandler (Action)

- (void)selectRowAtIndex:(NSInteger)index {
    
    if (self.selectedIndex == index) {
        return;
    }
    
    [self trackEventSelectPaymentMethod:index];
    ZPAddNewMethodData *dataAddNew = [self.dataSource safeObjectAtIndex:index];
    
    if ([dataAddNew isKindOfClass:[ZPAddNewMethodData class]]) {
        if (self.delegate) {
            [self.delegate paymentControllerDidClose:ZP_NOTIF_PAYMENT_GO_LISTBANK_MAPCARD data:nil];
        }
        return;
    }
    
    ZPPaymentMethodCellDisplay *data = [self.dataSource safeObjectAtIndex:index];
    if (![data isKindOfClass:[ZPPaymentMethodCellDisplay class]]) {
        return;
    }
    
    if ([self validSelectMethod:data.method] == FALSE) {
        return;
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.selectedIndex inSection:0];
    ZPMethodBaseCell *cell = [ZPMethodBaseCell castFrom:[self.mTableView cellForRowAtIndexPath:indexPath]];
    cell.checkedImageView.hidden = YES;
    
    self.selectedIndex = index;
    indexPath = [NSIndexPath indexPathForRow:self.selectedIndex inSection:0];
    cell = [ZPMethodBaseCell castFrom:[self.mTableView cellForRowAtIndexPath:indexPath]];
    cell.checkedImageView.hidden = NO;
    [self showFeeOfSelectedMethod];
    [self updateActionButtonWithSelectedIndex];
    [self updateListVoucherWhenSwitchPaymentMethod];
}

- (void)trackEventSelectPaymentMethod:(NSInteger)index  {
    ZPAddNewMethodData *dataAddNew = [self.dataSource safeObjectAtIndex:index];
    
    if ([dataAddNew isKindOfClass:[ZPAddNewMethodData class]]) {
        [[ZPTrackingHelper shared] trackEvent: ZPAnalyticEventActionSdk_add];
        return;
    }
    
    ZPPaymentMethodCellDisplay *data = [self.dataSource safeObjectAtIndex:index];
    if (![data isKindOfClass:[ZPPaymentMethodCellDisplay class]]) {
        return;
    }
    if([[data method] methodType] == ZPPaymentMethodTypeZaloPayWallet) {
        [[ZPTrackingHelper shared] trackEvent: ZPAnalyticEventActionSdk_zalopay];
    } else {
        [[ZPTrackingHelper shared] trackEvent: ZPAnalyticEventActionSdk_changesource];
    }
}

- (void)updateListVoucherWhenSwitchPaymentMethod {
    ZPContainerViewController *controller = (ZPContainerViewController *) self.mTableView.viewController;
    [controller.zpVoucherViewWithButton updateListVoucherWhenSwitchPaymentMethod];
}

- (void)showFeeOfSelectedMethod {
    if (self.selectedIndex < 0) {
        [self updateHeaderViewWithFee:0];
        return;
    }
    ZPPaymentMethodCellDisplay *selectedMethod = [self.dataSource safeObjectAtIndex:self.selectedIndex];
    self.bill.currentPaymentMethod = selectedMethod.method;
    if ([selectedMethod isKindOfClass:[ZPPaymentMethodCellDisplay class]]) {
        [selectedMethod.method calculateChargeWith:self.bill.finalAmount];
        float fee = selectedMethod.method.defaultChannel == true ? 0 : selectedMethod.method.totalChargeFee;
        [self updateFee:fee];
        [self updateHeaderViewWithFee:fee];
    }
}

- (void)updateHeaderViewWithFee:(float)fee {
    long amount = [self.bill finalAmount] + fee;
    ZPOrderHeaderView *header = [ZPOrderHeaderView castFrom:self.mTableView.tableHeaderView];
    [header updateAmountTitle:amount];
}

- (void)updateActionButtonWithSelectedIndex {
    if (self.selectedIndex < 0) {
        [self.actionButton setBackgroundColor:[UIColor disableButtonColor] forState:UIControlStateNormal];
        [self.actionButton setTitle:[R string_ButtonLabel_Confirm_To_Pay] forState:UIControlStateNormal];
        return;
    }
    ZPPaymentMethodCellDisplay *data = [self.dataSource safeObjectAtIndex:self.selectedIndex];
    if (![data isKindOfClass:[ZPPaymentMethodCellDisplay class]]) {
        return;
    }
    
    if ([[data method] methodType] == ZPPaymentMethodTypeZaloPayWallet) {
        [[ZPTrackingHelper shared] trackEvent: ZPAnalyticEventActionSdk_zalopay];
    } else {
        [[ZPTrackingHelper shared] trackEvent: ZPAnalyticEventActionSdk_changesource];
    }
    
    NSString *title = data.method.defaultChannel ? [R string_ButtonLabel_Next] : [R string_ButtonLabel_Confirm_To_Pay];
    UIColor *color = data.method.defaultChannel ? [UIColor zaloBaseColor] : [UIColor payColor];
    UIColor *highlightColor = data.method.defaultChannel ? [UIColor buttonHighlightColor] : [UIColor payHighlightColor];
    [self.actionButton setTitle:title forState:UIControlStateNormal];
    [self.actionButton setBackgroundColor:color forState:UIControlStateNormal];
    [self.actionButton setBackgroundColor:highlightColor forState:UIControlStateHighlighted];
}

- (BOOL)validSelectMethod:(ZPPaymentMethod *)paymentMethod {
    
    if ([self handleMinAppVersionMethod:paymentMethod]) {
        return FALSE;
    }
    if (paymentMethod.support == 0 || paymentMethod.miniBank == nil) {
        return FALSE;
    }
    if (paymentMethod.isSupportAmount == false ||
        paymentMethod.isTotalChargeGreaterThanBalance ||
        paymentMethod.isTotalChargeGreaterThanMaxAmount) {
        return FALSE;
    }
    if ([self handleMaintainceMethod:paymentMethod]) {
        return FALSE;
    }
    if ([self handleMiniBankMaintaince:paymentMethod]) {
        return FALSE;
    }
    //    if ([self handleUserPermission:paymentMethod]) {
    //        return FALSE;
    //    }
    return TRUE;
}
- (RACSignal <NSNumber *>*) checkExistKYC {
    [ZPProgressHUD showWithStatus:nil];
    return [[[[[[ZaloPayWalletSDKPayment sharedInstance] getUserProfile] map:^NSNumber *_Nullable(id  _Nullable value) {
        NSDictionary *userProfile = [NSDictionary castFrom:value];
        if (!userProfile) {
            return @(NO);
        }
        NSDictionary *kycInfo = [userProfile dictionaryForKey:@"kycInfo"];
        if (!kycInfo) {
            return @(NO);
        }
        
        NSNumber *isKYCMissing = [kycInfo numericForKey:@"kycInfoMissing" defaultValue:@(YES)];
        return @(![isKYCMissing boolValue]); // isKYCExisted
    }] deliverOnMainThread] doNext:^(id  _Nullable x) {
        [ZPProgressHUD dismiss];
    }] doError:^(NSError * _Nonnull error) {
        [ZPProgressHUD dismiss];
    }];
}

- (RACSignal <NSNumber *>*)alertKYC {
    //    NSString *message = @"Để bảo mật tài khoản và tiếp tục sử dụng ZaloPay, bạn cần phải cung cấp thông tin cá nhân của chính bạn";
    NSString *message = [R string_KYC_Alert];
    return [[RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
        [ZPDialogView showDialogWithType:DialogTypeNotification title:nil message:message buttonTitles:@[[R string_ButtonLabel_Later], [R string_ButtonLabel_OK]] handler:^(NSInteger index) {
            NSNumber *btnIndex = (index == 1) ? @(NO) : @(YES);
            [subscriber sendNext:btnIndex];
            [subscriber sendCompleted];
        }];
        return nil;
    }] filter:^BOOL(NSNumber *_Nullable value) {
        return [value integerValue] == 0;
    }];
}

- (RACSignal *)updateKYC:(NSDictionary *)infor {
    NSError *err = nil;
    NSString *kycStr = [infor stringForKey:@"kyc" defaultValue:@""];
    NSDictionary *kyc = [NSJSONSerialization JSONObjectWithData:[kycStr dataUsingEncoding:NSUTF8StringEncoding]
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if (err) {
        return [RACSignal return:@(NO)];
    }
    
    NSNumber *gender = [kyc numericForKey:@"gender"];
    NSString *idValue = [kyc stringForKey:@"idValue"];
    NSString *zaloPhone = [kyc stringForKey:@"zaloPhone"];
    NSString *fullName = [kyc stringForKey:@"fullName"];
    NSNumber *idType = [kyc numericForKey:@"idType"];
    NSNumber *dob = [kyc numericForKey:@"dob"];
    
    [ZPProgressHUD showWithStatus:nil];
    return [[[[[[ZaloPayWalletSDKPayment sharedInstance] updateKYC:zaloPhone fullName:fullName idType:idType idValue:idValue gender:gender dob:dob] map:^NSNumber *_Nullable(id  _Nullable value) {
        NSNumber *errorCode = [value numericForKey:@"returncode" defaultValue:@(0)];
        return errorCode.intValue == 1 ? @(YES) : @(NO);
    }] deliverOnMainThread] doNext:^(id  _Nullable x) {
        [ZPProgressHUD dismiss];
    }] doError:^(NSError * _Nonnull error) {
        [ZPProgressHUD dismiss];
    }];
}

- (BOOL) isNeedKYC {
    id<ZPWalletDependenceProtocol> dependency = [ZaloPayWalletSDKPayment sharedInstance].appDependence;
    if (!dependency || ![dependency kycEnableLimitTransaction]) {
        return NO;
    }
    
    NSUInteger limit = [[ZaloPayWalletSDKPayment sharedInstance].appDependence kycLimitTransaction];
    return !self.isHaveKYC && limit > 0 && self.bill.amount >= limit;
}

// Flow : Check -> alert -> kyc -> excute
- (void)handleSelectedMethod {
    // TODO: - Check KYC Flow with limit
    if ([self isNeedKYC]) {
        RACSignal <NSNumber *> *checkKYC = [self checkExistKYC];
        RACSignal <NSNumber *> *alertKYC = [self alertKYC];
        
        RACSignal <NSDictionary *> *signalKYC = [[ZaloPayWalletSDKPayment sharedInstance] runKYCFlow:self.zpParentController title:@"Thông tin cá nhân" infor:nil type:ZPKYCTypeTransaction];
        @weakify(self);
        [[[[[checkKYC flattenMap:^__kindof RACSignal * _Nullable(NSNumber * _Nullable value) {
            @strongify(self);
            self.isHaveKYC = [value boolValue];
            return self.isHaveKYC ? [RACSignal return:@(YES)] : alertKYC;
        }] filter:^BOOL(NSNumber  *_Nullable value) {
            return ![value boolValue];
        }] flattenMap:^__kindof RACSignal * _Nullable(id  _Nullable value) {
            return signalKYC;
        }] flattenMap:^__kindof RACSignal * _Nullable(NSDictionary *_Nullable value) {
            @strongify(self);
            [self.zpParentController.navigationController popViewControllerAnimated:YES];
            return [self updateKYC:value];
        }] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            self.isHaveKYC = x ? [x boolValue] : NO;
        } error:^(NSError * _Nullable error) {
            @strongify(self);
            if (error && error.code == NSURLErrorCancelled) {
                [self.zpParentController.navigationController popViewControllerAnimated:YES];
                return;
            }
            [ZPDialogView showDialogWithError:error handle:nil];
        } completed:^{
            @strongify(self);
            if (self.isHaveKYC) {
                // Waiting  finish flow
                [self handleSelectedMethod];
            }
        }];
        return;
    }
    
    // TODO: - Process Transaction
    [self trackConfirmAction];
    
    ZPPaymentMethodCellDisplay *model = [self.dataSource safeObjectAtIndex:self.selectedIndex];
    if (![model isKindOfClass:[ZPPaymentMethodCellDisplay class]]) {
        return;
    }
    ZPPaymentMethod *paymentMethod = model.method;
    if (paymentMethod.defaultChannel) {
        [self handleDefaultMethod:paymentMethod];
        return;
    }
    
    if ([ZaloPaySDKUtil isRequirePin:self.bill method:model.method]) {
        [self requestPinWithData:model];
        return;
    }
    
    [self handleMethod:model.method password:@"" fromTouchId:false];
    
}

- (void)trackConfirmAction {
    [[ZPTrackingHelper shared] trackEvent: ZPAnalyticEventActionSdk_pay];
}

- (void)handleDefaultMethod:(ZPPaymentMethod *)method {
    if ([self.delegate respondsToSelector:@selector(paymentMethodsControllerDidChooseMethod:)]) {
        [self.delegate paymentMethodsControllerDidChooseMethod:method];
    }
}

- (void)requestPinWithData:(ZPPaymentMethodCellDisplay *)data {
    if ([[ZaloPayWalletSDKPayment sharedInstance].appDependence phoneIsSupportTouchId] == false) {
        [self showInputPinView:data];
        return;
    }
    @weakify(self);
    id <ZPWalletDependenceProtocol> handler = [ZaloPayWalletSDKPayment sharedInstance].appDependence;
    [handler authenFromTouchIdCancel:^{
        
    }                          error:^(NSError *error) {
        @strongify(self);
        UINavigationController *navi = [[ZPAppFactory sharedInstance] rootNavigation];
        navi.view.userInteractionEnabled = NO;
        [[RACScheduler mainThreadScheduler] afterDelay:0.3 schedule:^{
            @strongify(self);
            navi.view.userInteractionEnabled = YES;
            [self showInputPinView:data];
        }];
    }                       complete:^(NSString *password) {
        @strongify(self);
        [self handleMethod:data.method password:password fromTouchId:true];
    }];
}


- (NSString *)passwordTitle {
    switch (self.bill.transType) {
        case ZPTransTypeWalletTopup:
            return [R string_Recharge_Password_Title];
        case ZPTransTypeTranfer:
            return [R string_TransferMoney_Password_Title];
        case ZPTransTypeWithDraw:
            return [R string_Withdraw_Password_Title];
        case ZPTransTypeBillPay:
        case ZPTransTypeAtmMapCard:
        default:
            return [R string_Title_PIN];
    }
}

- (void)showInputPinView:(ZPPaymentMethodCellDisplay *)data {
    UIViewController *controller = [self.mTableView viewController];
    @weakify(self);
    [[ZPCheckPinViewController showOn:controller
                                using:[self passwordTitle]
                           methodName:data.methodName
                          methodImage:data.image
                       suggestTouchId:YES] subscribeNext:^(NSString *password) {
        @strongify(self);
        DDLogInfo(@"receive pass :%@", password);
        if (password.length > 0) {
            [[ZPTrackingHelper shared] trackEvent: ZPAnalyticEventActionSdk_password_input];
            [self handleMethod:data.method password:[password zpSha256] fromTouchId:false];
        }
    }];
}

//- (BOOL)handleUserPermission:(ZPPaymentMethod *)method {
//
//    if((!method.userPLPAllow || (method.savedCard != nil && [ZaloPayWalletSDKPayment sharedInstance].userProfileLevel == 1)) && (method.methodType != ZPPaymentMethodTypeIbanking)){
//        NSString *alertMessage = (method.methodType == ZPPaymentMethodTypeZaloPayWallet ? @"Hãy cập nhật thông tin để thanh toán bằng số dư tài khoản": @"Hãy cập nhật thông tin để thanh toán bằng thẻ liên kết");
//        @weakify(self);
//        [ZPDialogView showDialogWithType:DialogTypeNotification
//                                 message:alertMessage
//                       cancelButtonTitle:[R string_ButtonLabel_Close]
//                       otherButtonTitle:@[[R string_ButtonLabel_OK]]
//                          completeHandle:^(int buttonIndex, int cancelButtonIndex) {
//                              @strongify(self);
//                              if (buttonIndex == cancelButtonIndex) {
//                                  return;
//                              }
//                              if ( [self.delegate respondsToSelector:@selector(paymentControllerDidClose:data:)]){
//                                  [self.delegate paymentControllerDidClose:ZP_NOTIF_UPGRADE_LEVEL data:nil];
//                              }
//
//                          }];
//
//        return TRUE;
//    }
//    return FALSE;
//}

- (ZPBank *)findBankWithCode:(NSString *)bankCode {
    NSArray *allBank = [ZPDataManager sharedInstance].bankManager.allBanks;
    for (ZPBank *bank in allBank) {
        if ([bank.bankCode isEqualToString:bankCode]) {
            return bank;
        }
    }
    return nil;
}

- (NSString *)filterBankName:(NSString *)bankName {
    return [bankName hasPrefix:@"NH "] ? [bankName substringFromIndex:3] : bankName;
}

- (BOOL)handleMiniBankMaintaince:(ZPPaymentMethod *)method {
    if (method.miniBank.minibankStatus == ZPMiniBankStatus_Maintenance) {
        ZPBank *bankGet;
        if (method.savedCard) {
            bankGet = [self findBankWithCode:method.savedCard.bankCode];
        } else if (method.savedBankAccount) {
            bankGet = [self findBankWithCode:method.savedBankAccount.bankCode];
        }
        NSString *strMaintain = method.miniBank.maintenancemsg;
        NSString *bankName =  [self filterBankName:bankGet.bankName];
        if(method.methodType == ZPPaymentMethodTypeCreditCard || [method.savedCard isCCCard]) {
            ZPBank *ccBank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:method.savedCard.bankCode];
            bankName = [self filterBankName:ccBank.bankName];
        }
        if (strMaintain.length == 0) {
            NSString *typeStr = [self.dataManager getTitleByTranstype:self.bill.transType];
            NSString *timeString = [NSString timeString:method.miniBank.maintenanceto];
            strMaintain = [NSString stringWithFormat:@"%@ bảo trì %@ tới %@, vui lòng chọn ngân hàng khác hoặc quay lại sau.", bankName, typeStr, timeString];
        }
        
        [self showAlertWithMessage:strMaintain];
        return YES;
    }
    return NO;
}

- (BOOL)handleMinAppVersionMethod:(ZPPaymentMethod *)method {
    if (method.isAppVersionNotSupport) {
        NSString *msg = [NSString stringWithFormat:stringUpdateNewVersionFormat, [self.dataManager getTitleByTranstype:self.bill.transType], method.methodName];
        [ZPDialogView showDialogWithType:DialogTypeNotification
                                 message:msg
                       cancelButtonTitle:[R string_ButtonLabel_Close]
                        otherButtonTitle:@[[R string_UpdateProfileLevel3_Update_Title]]
                          completeHandle:^(int buttonIndex, int cancelButtonIndex) {
                              if (buttonIndex == cancelButtonIndex) {
                                  return;
                              }
                              [[UIApplication sharedApplication] openURL:[NSURL URLWithString:ZaloPayAppStoreUrl]];
                          }];
        return TRUE;
    }
    return FALSE;
}

- (BOOL)handleMaintainceMethod:(ZPPaymentMethod *)method {
    
    if (method.status == ZPPMCMaintenance ||
        method.support == ZPPMCMaintenance) {
        ZPBank *bankGet = [self findBankWithCode:method.savedCard.bankCode];
        NSString * strMaintain = bankGet.maintenanceMsg;
        if(method.methodType == ZPPaymentMethodTypeCreditCard || [method.savedCard isCCCard] ) {
            ZPBank *ccBank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:method.savedCard.bankCode];
            strMaintain = ccBank.maintenanceMsg;
        }
        
        if (strMaintain.length == 0) {
            NSString *formatString = [self.dataManager messageForKey:@"message_bank_maintain"];
            NSString *name = bankGet.bankName.length > 0 ? bankGet.bankName : @"Kênh thanh toán";
            strMaintain = [NSString stringWithFormat:formatString, name];
        }
        [self showAlertWithMessage:strMaintain];
        return TRUE;
    }
    return FALSE;
}

- (void)showAlertWithMessage:(NSString *)msg {
    [ZPCheckPinViewController dissmiss:NO];
    [ZPDialogView showDialogWithType:DialogTypeNotification
                             message:msg
                   cancelButtonTitle:[R string_ButtonLabel_Close]
                    otherButtonTitle:nil
                      completeHandle:nil];
}

- (void)callGoToLinkAccount:(NSString *)bankCode {
    NSString *code = bankCode.length > 0 ? bankCode : @"";
    NSString *bankCodeString = [@{@"bankCode": code} JSONRepresentation];
    [self.delegate paymentControllerDidClose:ZP_NOTIF_PAYMENT_GO_LINKACOUNT data:bankCodeString];
}

- (BOOL)checkSavedAccount {
    for (ZPPaymentMethod *method in self.paymentMethods) {
        if (method.methodType == ZPPaymentMethodTypeSavedAccount) {
            return YES;
        }
    }
    return NO;
}


@end
