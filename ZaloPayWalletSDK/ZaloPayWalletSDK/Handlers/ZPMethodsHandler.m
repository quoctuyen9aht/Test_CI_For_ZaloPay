//
//  ZPMethodsHandler.m
//  ZaloPayWalletSDK
//
//  Created by Thi La on 4/23/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPMethodsHandler.h"
#import "ZPDataManager.h"
#import "ZPPaymentMethod.h"
#import "ZPBill.h"
#import "ZaloPayWalletSDKPayment.h"
#import "ZPMethodsHandler+Action.h"
#import "ZPMethodBaseCell.h"
#import "ZPBillExtraInfoCell.h"
#import "ZPOrtherMethodCell.h"
#import "ZPZaloPayWalletMethodCell.h"
#import "ZPMethodPadingCell.h"
#import "ZPMethodsHandler+DataSource.h"
#import "ZPContainerViewController.h"
#import "ZPAddNewMethodCell.h"
#import <ZaloPayWalletSDK/ZaloPayWalletSDK-Swift.h>
#import <ZaloPayAnalyticsSwift/ZaloPayAnalyticsSwift-Swift.h>
#import <ZaloPayCommonSwift/ZaloPayCommonSwift-Swift.h>
#import <MDHTMLLabel/MDHTMLLabel.h>
#import "ZPVoucherInfo.h"
#import "NetworkManager+ZaloPayWalletSDK.h"
#import "ZPProgressHUD.h"
#import "ZPPromotionInfo.h"
#import "ZPEndowUtils.h"
#import "ZPPromotionManager.h"

@interface ZPMethodsHandler () <MDHTMLLabelDelegate>
@property(nonatomic, strong) UILabel *labelNoMethod;
@property(nonatomic, strong) MDHTMLLabel *htmlLabelWithHref;
@property(nonatomic, assign) BOOL isHaveKYC;

@end

@implementation ZPMethodsHandler

- (void)initialization {
    self.mTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self createAllDataSource];
    [self showFeeOfSelectedMethod];
    [self paymentMethodLaunch];
    [self updateListVoucherWhenSwitchPaymentMethod];
}

- (void)createAllDataSource {
    //NSInteger numberItemOfOldDatasource = self.dataSource.count;
    self.dataSource = [self loadDataSource];
//    BOOL showGetDefaultIndex = TRUE;
//    if (self.selectedIndex > 0) {
//        self.selectedIndex += self.dataSource.count - numberItemOfOldDatasource;
//        ZPPaymentMethodCellDisplay *model = self.dataSource[self.selectedIndex];
//        showGetDefaultIndex = model.type != ZPPaymentMethodCellTypeAvailable;
//    }
//    if (showGetDefaultIndex){
    self.selectedIndex = [self selectedIndexFromDataSource:self.dataSource];
    //}
}

- (void)paymentMethodLaunch {
//    if (self.bill.transType == ZPTransTypeWalletTopup) {
//        [[ZPAppFactory sharedInstance] trackEventId:ZPAnalyticEventActionBalance_addcash_result];
//        return;
//    }
//
//    if (self.bill.transType == ZPTransTypeWithDraw) {
//        [[ZPAppFactory sharedInstance] trackEventId:ZPAnalyticEventActionBalance_addcash_result];
//        return;
//    }
//    if (self.bill.transType == ZPTransTypeTranfer) {
//        [[ZPAppFactory sharedInstance] trackEventId:ZPAnalyticEventActionBalance_addcash_result];
//        return;
//    }
}

- (void)showPinViewControllerIfNeeded {
    if (self.bill.doneShowPinViewOrTouchId) {
        return;
    }

    int methodsAvailable = [self getNumberOfMethodsAvailable];
    if (methodsAvailable == 1 && ![self isVoucherAvaliable]) {
        self.bill.doneShowPinViewOrTouchId = true;
        [self handleSelectedMethod];
    }
}

- (BOOL)isVoucherAvaliable {
    ZPVoucherManager *manager = [ZaloPayWalletSDKPayment sharedInstance].zpVoucherManager;
    NSArray *vouchers = [manager voucherCachesForBill:self.bill];
    NSArray *vouchersFiltered = [manager filterAvailableVouchersWhenSelectedPaymentMethod:vouchers forBill:self.bill];
    
    ZPPromotionManager *promotionManager = [ZaloPayWalletSDKPayment sharedInstance].zpPromotionManager;
    NSArray *promotions = [promotionManager promotionsCachesForBill:self.bill];
    NSArray *promotionsFiltered = [promotionManager filterAvailablePromotionsWhenSelectedPaymentMethod:promotions forBill:self.bill];
    
    return vouchersFiltered.count > 0 || promotionsFiltered.count > 0;
}

- (int)getNumberOfMethodsAvailable {
    int numberOfMethodsAvailable = 0;
    for (int i = 0; i < self.dataSource.count; i++) {
        ZPPaymentMethodCellDisplay *model = self.dataSource[i];
        if (![model isKindOfClass:[ZPPaymentMethodCellDisplay class]]) {
            continue;
        }
        ZPPaymentMethodCellDisplay *selectedMethod = [self.dataSource safeObjectAtIndex:self.selectedIndex];
        if (selectedMethod.method.defaultChannel) {
            continue;
        }
        if (model.type == ZPPaymentMethodCellTypeAvailable && !model.method.defaultChannel) {
            numberOfMethodsAvailable++;
        }
    }
    return numberOfMethodsAvailable;
}

- (void)prepareHandleSelectedMethod {
    if (!self.bill.promotionInfo && !self.bill.voucherInfo && self.bill.currentPaymentMethod.isHaveAvailableEndow) {
        @weakify(self);
        [ZPEndowUtils showAlertNotUseEndow:^{
            @strongify(self);
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self handleSelectedMethod];
            });
        }];
        return;
    }
    [self handleSelectedMethod];
}
- (void)displayOnTableView:(UITableView *)tableView fromController:(ZPContainerViewController *)controller {
    self.actionButton = controller.actionButton;
    [controller.actionButton addTarget:self action:@selector(prepareHandleSelectedMethod) forControlEvents:UIControlEventTouchUpInside];
//    [self updateActionButtonWithSelectedIndex];
    [super displayOnTableView:tableView fromController:controller];
    [self reloadUI];
//    [self showInvalidMethodMessage];

    int padding = 10;
    int bottom = [UIView isIPhoneX] ? padding + 10 : padding;
    [controller.actionButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(-bottom);
        make.height.equalTo(kZaloButtonHeight);
        make.left.equalTo(10);
        make.right.equalTo(-10);
    }];
    [tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.left.equalTo(0);
        make.right.equalTo(0);
        make.bottom.equalTo(-kZaloButtonHeight - bottom - padding);
    }];
    [self scrollToSelectedIndex:controller];
    id<ZPWalletDependenceProtocol> dependency = [ZaloPayWalletSDKPayment sharedInstance].appDependence;
    BOOL isEnableKYC = [dependency kycEnableLimitTransaction];
    if (isEnableKYC) {
        return;
    }
    [self showPinViewControllerIfNeeded];
}

- (BOOL)shouldShowRechargeView {
    if ([self isAllMethodUnSupportCurrentBill] && [self needRechargeMoney]) {
        ZPTransType transtype = self.bill.transType;
        return (transtype == ZPTransTypeBillPay || transtype == ZPTransTypeTranfer);
    }
    return NO;
}

- (void)scrollToSelectedIndex:(UIViewController *)controller {
    if (self.selectedIndex < 0) {
        return;
    }
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.selectedIndex inSection:0];
    CGRect rect = [self.mTableView rectForRowAtIndexPath:indexPath];
    float height = controller.view.frame.size.height - 2 * kZaloButtonHeight;
    if (height > rect.origin.y + rect.size.height) {
        return;
    }
    if (self.selectedIndex >= 3) {
        NSInteger indexToScroll = self.selectedIndex - 3;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexToScroll inSection:0];
        [self.mTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionNone animated:false];
    }
}

- (void)reloadUI {
    [self createAllDataSource];
    [self paymentMethodLaunch];
    [self showInvalidMethodMessage];
    [self showFeeOfSelectedMethod];
    [self updateActionButtonWithSelectedIndex];
    [self.mTableView reloadData];
}

- (void)showInvalidMethodMessage {
    if (self.bill.transType == ZPTransTypeWalletTopup) {
        return;
    }
    if ([self shouldShowRechargeView]) {
        [self showRecharge];
    } else if ([self getNumberOfMethodsAvailable] == 0) {
        [self createLabelNoMethod:[self.dataManager messageForKey:stringKeyUnsupportAmount]];
    } else {
        _labelNoMethod.hidden = YES;
        _htmlLabelWithHref.hidden = YES;
    }
}


- (void)createLabelNoMethod:(NSString *)errorMessage {
    if (!_labelNoMethod) {
        _labelNoMethod = [[UILabel alloc] init];
        [_labelNoMethod zpMainBlackRegular];
        [self.mTableView.viewController.view addSubview:_labelNoMethod];
        [_labelNoMethod mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(0);
            make.right.equalTo(0);
            make.top.equalTo(0);
            make.height.equalTo(30);
        }];
        _labelNoMethod.adjustsFontSizeToFitWidth = YES;
        _labelNoMethod.backgroundColor = [UIColor hex_0xffffc1];
        _labelNoMethod.textAlignment = NSTextAlignmentCenter;
        _labelNoMethod.text = errorMessage;

        ZPLineView *line = [[ZPLineView alloc] init];
        [_labelNoMethod addSubview:line];
        [line alignToBottom];
    }
    _labelNoMethod.hidden = NO;
}

- (void)showRecharge {
    if (!_htmlLabelWithHref) {
        MDHTMLLabel *htmlLabel = [[MDHTMLLabel alloc] init];
        [self.mTableView.viewController.view addSubview:htmlLabel];
        htmlLabel.tag = 999;
        htmlLabel.delegate = self;
        [htmlLabel zpMainBlackRegular];
        htmlLabel.htmlText = [R string_Method_Handler_AllMethodUnSupportCurrentBill];
        htmlLabel.numberOfLines = 0;
        htmlLabel.backgroundColor = [UIColor hex_0xffffc1];
        htmlLabel.textAlignment = NSTextAlignmentCenter;

        //CGSize labelSize = [htmlLabel sizeThatFits:CGSizeMake(CGRectGetWidth([UIScreen mainScreen].bounds), 0)];

        [htmlLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(0);
            make.right.equalTo(0);
            make.top.equalTo(0);
            make.height.equalTo(36);
        }];
        _htmlLabelWithHref = htmlLabel;
    }
    _htmlLabelWithHref.hidden = NO;
}

- (NSString *)paymentControllerTitle {
    return [self.dataManager getTitleByTranstype:self.bill.transType];
}

- (void)updateFee:(long)fee {
    self.feeModel.value = fee == 0 ? [R string_Pay_Bill_Fee_Free] : [@(fee) formatCurrency];
    for (int i = 0; i < self.dataSource.count; i++) {
        if (self.dataSource[i] == self.feeModel) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            ZPBillExtraInfoCell *cell = (ZPBillExtraInfoCell *) [self.mTableView cellForRowAtIndexPath:indexPath];
            if ([cell isKindOfClass:[ZPBillExtraInfoCell class]]) {
                [cell setData:self.feeModel];
            }
        }
    }
}

- (void)goToRecharge {
    if (self.delegate) {
        [self.delegate paymentControllerDidClose:ZP_NOTIF_PAYMENT_MY_WALLET data:nil];
    }
}

- (void)showToastMapCardSuccess {
    [[RACScheduler mainThreadScheduler] afterDelay:0.5 schedule:^{
        UIViewController *top = [[ZPAppFactory sharedInstance] rootNavigation].viewControllers.lastObject;
        id <ZPWalletDependenceProtocol> helper = [ZaloPayWalletSDKPayment sharedInstance].appDependence;
        [helper showToastFrom:top message:stringMapcardSuccess delay:0.5];
        [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionLinkbank_delete_result];
    }];
}

#pragma mark - UITableViewDelegate vs DataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZPPaymentMethodCellDisplay *model = [self.dataSource safeObjectAtIndex:indexPath.row];
    if ([model isKindOfClass:[ZPPaymentMethodCellDisplay class]]) {
        Class cellClass = model.method.methodType == ZPPaymentMethodTypeZaloPayWallet ?
                [ZPZaloPayWalletMethodCell class] : [ZPMethodBaseCell class];
        ZPMethodBaseCell *cell = [cellClass defaultCellForTableView:tableView];
        [cell setData:model];
        cell.checkedImageView.hidden = indexPath.row != self.selectedIndex;
        return cell;
    }

    if ([model isKindOfClass:[ZPOrtherMethodData class]]) {
        ZPOrtherMethodCell *cell = [ZPOrtherMethodCell defaultCellForTableView:tableView];
        [cell setData:(ZPOrtherMethodData *) model];
        return cell;
    }

    if ([model isKindOfClass:[ZPMethodPadingData class]]) {
        return [ZPMethodPadingCell defaultCellForTableView:tableView];
    }

    if ([model isKindOfClass:[ZPAddNewMethodData class]]) {
        ZPAddNewMethodCell *cell = [ZPAddNewMethodCell defaultCellForTableView:tableView];
        if ([self getNumberOfMethodsAvailable] > 0) {
            [cell setData];
        } else {
            [cell setDataWithTransType:self.bill.transType];
        }
        // Check is kind of payment
        NSInteger appid = self.bill.appId;
        [cell canUsePayment:appid];
        return cell;
    }

    if ([model isKindOfClass:[ZPBill class]]) {
        ZPBill *data = (ZPBill *) model;
        ZPBillExtraInfoCell *cell = [ZPBillExtraInfoCell defaultCellForTableView:tableView];
        [cell setBasePriceFrom:data];
        return cell;
    }
    if ([model isKindOfClass:[ZPVoucherInfo class]]) {
        ZPVoucherInfo *data = (ZPVoucherInfo *) model;
        ZPBillExtraInfoCell *cell = [ZPBillExtraInfoCell defaultCellForTableView:tableView];
        [cell setVoucherFrom:data];
        return cell;
    }
    if ([model isKindOfClass:[ZPPromotionInfo class]]) {
        ZPPromotionInfo *data = (ZPPromotionInfo *) model;
        ZPBillExtraInfoCell *cell = [ZPBillExtraInfoCell defaultCellForTableView:tableView];
        [cell setPromotionFrom:data];
        return cell;
    }

    ZPBillExtraInfoData *data = (ZPBillExtraInfoData *) model;
    ZPBillExtraInfoCell *cell = [ZPBillExtraInfoCell defaultCellForTableView:tableView];
    [cell setData:data];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    ZPPaymentMethodCellDisplay *model = [self.dataSource safeObjectAtIndex:indexPath.row];
    if ([model isKindOfClass:[ZPPaymentMethodCellDisplay class]] && model.isNewMethod) {
        [ZPDataManager sharedInstance].savedCardNumber = nil;
        [ZPDataManager sharedInstance].isExistNewBankAccount = NO;
        [UIView animateWithDuration:1.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             [cell.contentView setAlpha:0.0];
                         }
                         completion:^(BOOL finished) {
                             [cell.contentView setAlpha:model.alpha];
                         }];
        [self showToastMapCardSuccess];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    id item = [self.dataSource safeObjectAtIndex:indexPath.row];
    if ([item isKindOfClass:[ZPPaymentMethodCellDisplay class]]) {
        return [ZPMethodBaseCell height];
    }
    if ([item isKindOfClass:[ZPOrtherMethodData class]]) {
        return [ZPOrtherMethodCell height];
    }
    if ([item isKindOfClass:[ZPMethodPadingData class]]) {
        return [ZPMethodPadingCell height];
    }
    if ([item isKindOfClass:[ZPAddNewMethodData class]]) {
        return [ZPAddNewMethodCell height];
    }
    return [ZPBillExtraInfoCell height];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self selectRowAtIndex:indexPath.row];
}

#pragma mark - Html Delegate

- (void)HTMLLabel:(MDHTMLLabel *)label didSelectLinkWithURL:(NSURL *)URL {
    if ([[URL absoluteString] isEqualToString:@"deposit"]) {
        [self goToRecharge];
    }
}

@end
