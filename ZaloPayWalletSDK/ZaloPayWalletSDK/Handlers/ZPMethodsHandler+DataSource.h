//
//  ZPMethodsHandler+DefaultMethods.h
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/17/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPMethodsHandler.h"
@class ZPPaymentMethod;
@interface ZPMethodsHandler (DataSource)
- (NSArray *)loadDataSource;
- (NSInteger)selectedIndexFromDataSource:(NSArray *)dataSource;
- (BOOL)isAllMethodUnSupportCurrentBill;
- (BOOL)needRechargeMoney;
@end
