//
//  ZPMethodsHandler+Action.h
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 4/21/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPMethodsHandler.h"

@interface ZPMethodsHandler (Action)
- (void)selectRowAtIndex:(NSInteger)index;
- (void)handleSelectedMethod;
- (void)updateActionButtonWithSelectedIndex;
- (void)showFeeOfSelectedMethod;
- (void)showAlertWithMessage:(NSString *)msg;
- (void) updateListVoucherWhenSwitchPaymentMethod;
@end
