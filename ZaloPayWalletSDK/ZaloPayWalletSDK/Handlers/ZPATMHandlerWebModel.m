//
//  ZPATMHandlerWebModel.m
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 4/26/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPATMHandlerWebModel.h"
#import "ZPSmartLinkHelper.h"

@implementation ZPATMHandlerWebModel
- (instancetype)initWithStartEvent:(RACSignal *)start
                               end:(RACSignal *)end
                              with:(UIWebView *)wv {
    self = [super init];
    self.jsHelper = [ZPSmartLinkHelper new];
    _jsHelper.webview = wv;
    return self;
}

- (ZPATMHandlerWebURLType)typeWith:(NSString *)link {
    if (!link) {
        return ZPATMHandlerWebURLTypeUnknown;
    }

    if ([_jsHelper isAtmError:link]) {
        return ZPATMHandlerWebURLTypeATMError;
    }

    if ([_jsHelper isCaptchaCallback:link]) {
        return ZPATMHandlerWebURLTypeCaptchaCallback;
    }

    if ([_jsHelper vcb_DirectIsOtpUrl:link]) {
        return ZPATMHandlerWebURLTypeVCBDirectIsOTP;
    }

    if ([_jsHelper vtb_IsCaptchaUrl:link]) {
        return ZPATMHandlerWebURLTypeVTBCaptcha;
    }

    if ([_jsHelper vtb_IsOtpUrl:link]) {
        return ZPATMHandlerWebURLTypeVTBOTP;
    }

    if ([_jsHelper vcb_DirectIsCaptchaUrl:link]) {
        return ZPATMHandlerWebURLTypeVCBDirectIsCaptcha;
    }

    if ([_jsHelper isOtpCallback:link]) {
        return ZPATMHandlerWebURLTypeOTPCallback;
    }

    return ZPATMHandlerWebURLTypeUnknown;
}

- (NSString *)jsRequestVicomCaptcha {
    return @" \
    var result = {};\
    var img = new Image();\
    img.onload = function() {\
    var c = document.createElement('canvas');\
    var ctx = c.getContext('2d');\
    c.width=img.width;\
    c.height=img.height;\
    ctx.drawImage(img, 0, 0);\
    result.otpimg=c.toDataURL().replace('data:image/png;base64,', '');\
    window.location = \"http://otpcallback.com?param=\" + encodeURI(JSON.stringify(result));\
    };\
    img.src='_ScriptLibrary/JpegImage.aspx'; \
    ";
}

- (NSString *)jsCaptchaSML {
    return @"\
    var gI=null;\
    var inputs = document.getElementsByTagName('input');\
    for (var i = 0; i < inputs.length; ++i) {\
    var input=inputs[i];\
    if(input.type=='image') gI=input;\
    };\
    var otp_onload = function() {\
    var c = document.createElement('canvas');\
    var ctx = c.getContext('2d');\
    var img = document.getElementById('js_otpimg');\
    c.width=img.width;\
    c.height=img.height;\
    ctx.drawImage(img, 0, 0);\
    var result = {pageId:2};\
    if(document.getElementsByClassName('error').length > 0) {\
    result.message=(document.getElementsByClassName('error')[0]).innerText;\
    };\
    result.otpimg=c.toDataURL().replace('data:image/png;base64,', '');\
    window.location = \"http://otpcallback.com?param=\"+encodeURI(JSON.stringify(result));\
    };\
    gI.outerHTML = '<img src=\"' + gI.src + '\" id=\"js_otpimg\" onload=\"otp_onload()\"/>';\
    ";
}

- (NSString *)jsCaptchaBanknet {
    return @"\
    var otp_onload = function() {\
    var c = document.createElement('canvas');\
    var ctx = c.getContext('2d');\
    var img = document.getElementById('sVerifyImgNap');\
    c.width=img.width;\
    c.height=img.height;\
    ctx.drawImage(img, 0, 0);\
    var result = {pageId:2};\
    if(document.getElementById('m-message').innerHTML.trim().length > 0) {\
    result.message=document.getElementById('m-message').innerHTML.trim();\
    };\
    result.otpimg=c.toDataURL().replace('data:image/png;base64,', '');\
    window.location = \"http://otpcallback.com?param=\"+encodeURI(JSON.stringify(result));\
    };\
    otp_onload();\
    ";
}

- (NSString *)jsVicomErrorMessage {

    return @"javascript:{\
    function isVisible(elem) {\
    return elem && elem.style.visibility != 'hidden' && elem.innerHTML.trim() != '';\
    }\
    function getErrorMessage(){\
    var result = {pageId:11};\
    var eItem = document.getElementById('ctl00__Default_Content_Center_CustomValidator1');\
    var message = '';\
    if(isVisible(eItem))\
    {\
    message = eItem.innerHTML;\
    return message;\
    };\
    eItem = document.getElementById('ctl00__Default_Content_Center_Lit_AuthWarn');\
    if(isVisible(eItem))\
    {		\
    message=eItem.innerHTML;	\
    return message;	\
    };	\
    return message;\
    };\
    getErrorMessage(); };";
}

- (BOOL)is123PResult:(NSString *)url {
    NSString *exp = @"(.*123pay\\.vn.*\\/ocp\\/pay\\/payresult.*)";
    return [NSRegularExpression checkRegularExpression:exp url:url];
}

- (BOOL)isAuthenLoadFail:(NSString *)url {
    NSString *exp = @"(.*123pay\\.vn.*cc.*\\/index\\/authen.*)";
    return [NSRegularExpression checkRegularExpression:exp url:url];
}

@end
