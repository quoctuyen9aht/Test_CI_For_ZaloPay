//
//  ZPPaymentHandler.m
//  ZPSDK
//
//  Created by ZaloPayTeam on 12/22/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import "ZPPaymentHandler.h"
#import "ZPPaymentResponse.h"
#import "ZPDataManager.h"
#import "ZPBill.h"
#import "ZPPaymentChannel.h"
#import "ZPResultHandler.h"
#import "ZPCellIdentifier.h"
#import "ZPPaymentInfo.h"
#import "ZPATMSaveCardCell.h"
#import "ZPCreditSaveCardCell.h"
#import "ZPOtpCell.h"
#import "ZPPaymentMethod.h"
#import "ZPCheckPinViewController.h"
#import "ZPProgressHUD.h"
#import "ZPContainerViewController.h"
#import "ZPOrderHeaderView.h"
#import "ZaloPayWalletSDKLog.h"
#import "ZaloPayWalletSDKPayment.h"
#import "ZPBank.h"
#import <ZaloPayWalletSDK/ZaloPayWalletSDK-Swift.h>

@interface ZPPaymentHandler ()
@property(strong, nonatomic) NSTimer *progressHUDTimer;
@property(strong, nonatomic) UIButton *uibutton;
@property(assign, nonatomic) CGRect originalWebView;
@property (strong, nonatomic) ZPWalletLogWebEvent *logWeb;
@end

@implementation ZPPaymentHandler

- (void)dealloc {
    DDLogInfo(@"dealloc %@", self.description);
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)registerCell {
    [self.mTableView registerNib:[self.dataManager nibNamed:@"ZPAtmPinnedView"] forCellReuseIdentifier:identify(ZPATMCellTypePinnedViewCell)];
    [self.mTableView registerNib:[self.dataManager nibNamed:@"ZPOtpCell"] forCellReuseIdentifier:identify(ZPATMCellTypeOtp)];
    [self.mTableView registerNib:[self.dataManager nibNamed:@"ZPCreditCardWebviewCell"] forCellReuseIdentifier:identify(ZPATMCellTypeCardWebView)];
    [self.mTableView registerNib:[self.dataManager nibNamed:@"ZPAtmAuthenType"] forCellReuseIdentifier:identify(ZPATMCellTypeAuthen)];
    [self.mTableView registerNib:[self.dataManager nibNamed:@"ZPOptionCell"] forCellReuseIdentifier:identify(ZPATMCellTypeOption)];
    [self.mTableView registerNib:[self.dataManager nibNamed:@"ZPDidChooseMethodCell"] forCellReuseIdentifier:identify(ZPATMCellTypeDidChoosePaymentMethod)];
    [self.mTableView registerNib:[self.dataManager nibNamed:@"ZPATMSaveCardCell"] forCellReuseIdentifier:identify(ZPATMCellTypeAutoDetect)];
}

- (instancetype)init {
    self = [super init];
    if (self) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [[ZPDataManager sharedInstance] getPaymentGatewayInfoCompleteHandle:^(int errorCode, NSString *mesage) {}];
            [[ZPDataManager sharedInstance].bankManager getBanklistWithCallBack:^(NSDictionary *dictionary) {}];
        });
        self.atmCard = [[ZPAtmCard alloc] init];
    }
    return self;
}


- (void)displayOnTableView:(UITableView *)tableView fromController:(ZPContainerViewController *)controller {
    self.mTableView = tableView;
    [tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.bottom.equalTo(0);
        make.left.equalTo(0);
        make.right.equalTo(0);

    }];
    [self viewWillShow];
    [tableView reloadData];
}

- (void)viewWillBack {
}

- (void)loadWebView:(UIWebView *)wv {
    RACSignal *until = [self rac_willDeallocSignal];
    RACSignal *skeyboard = [[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillShowNotification object:nil];
    RACSignal *hkeyboard = [[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillHideNotification object:nil];
    RACSignal *keyboard = [skeyboard merge:hkeyboard];
    @weakify(self);
    [[keyboard takeUntil:until] subscribeNext:^(NSNotification *notification) {
        @strongify(self);
        CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
        if ([notification.name isEqualToString:UIKeyboardWillShowNotification]) {
            self.originalWebView = [wv frame];
            CGRect f = [wv frame];
            f.size.height = [UIScreen mainScreen].bounds.size.height - keyboardSize.height + 64;
            wv.frame = f;
        } else {
            if (!CGRectIsEmpty(self.originalWebView)) {
                wv.frame = self.originalWebView;
            }
        }
    }];
}

- (void)logEventWith:(UIWebView *)wv {
    self.logWeb = [[ZPWalletLogWebEvent alloc] initWith:wv transid:self.bill.appTransID];
}

- (void)viewWillShow {
    self.mTableView.tableHeaderView = nil;
    if ([self isAtmMapcard]) {
        return;
    }
    //cc
    if (self.currentStep == ZPCcPaymentStepWaitingWebview || self.currentStep == ZPCcPaymentStepFillCardInfo) {
        return;
    }
    //atm
    if (self.currentStep == ZPAtmPaymentStepWaitingWebview || self.currentStep == ZPAtmPaymentStepAutoDetectBank) {
        return;
    }
    if ([self.mTableView.tableHeaderView isKindOfClass:[ZPOrderHeaderView class]]) {
        return;
    }

    long finalAmount = self.bill.finalAmount;
    ZPPaymentMethod *method = self.bill.currentPaymentMethod;
    if (method) {
        [method calculateChargeWith:finalAmount];
        finalAmount = [self.bill finalAmount] + method.totalChargeFee;
    }
    ZPOrderHeaderView *orderHeader = [[ZPOrderHeaderView alloc] initWithAmount:finalAmount
                                                                       message:self.bill.billDescription
    ];

    self.mTableView.tableHeaderView = orderHeader;
}

- (void)viewWillClose {
}

- (void)initialization {
    [self registerCell];
}

- (void)processPayment {
}

- (void)dismissKeyboard {
    UITableViewCell *cell = [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    self.isShowDialogMaintenance = YES;
    [self.mTableView endEditing:YES];
    if ([cell isKindOfClass:[ZPOtpCell class]]) {
        [((ZPOtpCell *) cell).txtOtpValue resignFirstResponder];
        return;
    }

    if ([cell isKindOfClass:[ZPATMSaveCardCell class]]) {
        ZPATMSaveCardCell *cellSaveCard = (ZPATMSaveCardCell *) cell;
        cellSaveCard.isShouldHideKB = true;
        return;
    }
    if ([cell isKindOfClass:[ZPCreditSaveCardCell class]]) {
        ZPCreditSaveCardCell *cellSaveCard = (ZPCreditSaveCardCell *) cell;
        cellSaveCard.isShouldHideKB = true;
        return;
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:ZPUIKeyboardDidHideNotification object:nil];
}

- (void)showKeyboard {
    UITableViewCell *cell = [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    self.isShowDialogMaintenance = NO;

    if ([cell isKindOfClass:[ZPOtpCell class]]) {
        ((ZPOtpCell *) cell).txtOtpValue.text = @"";
        [((ZPOtpCell *) cell).txtOtpValue becomeFirstResponder];
        return;
    }

    if ([cell isKindOfClass:[ZPATMSaveCardCell class]]) {
        ZPATMSaveCardCell *cellSaveCard = (ZPATMSaveCardCell *) cell;
        cellSaveCard.isShouldHideKB = false;
        [cellSaveCard firstResponderKeyboardTextFieldCurrent];
        return;
    }
    if ([cell isKindOfClass:[ZPCreditSaveCardCell class]]) {
        ZPCreditSaveCardCell *cellSaveCard = (ZPCreditSaveCardCell *) cell;
        cellSaveCard.isShouldHideKB = false;
        [cellSaveCard firstResponderKeyboardTextFieldCurrent];
        return;
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:ZPUIKeyboardDidShowNotification object:nil];
}

- (NSString *)paymentControllerTitle {
    return nil;
}

- (NSString *)okButtonBackgroundColor {
    return @"";
}

- (NSString *)okButtonTitle {
    return [R string_ButtonLabel_Next];
}

- (BOOL)isAtmMapcard {
    DDLogInfo(@"self.bill.transType: %ld", (long) self.bill.transType);
    return self.bill.transType == ZPTransTypeAtmMapCard;
}

- (void)handlePaymentResponse:(ZPPaymentResponse *)response {
    if (response.errorCode == ZALOPAY_ERRORCODE_REGISTER_BANK_ONLINE) {
        if ([self.delegate respondsToSelector:@selector(paymentControllerNeedRegisterWith:using:)]) {
            [self.delegate paymentControllerNeedRegisterWith:response using:self.atmCard];
            return;
        }
    }

    [self dismissPinView:response.errorCode == ZALOPAY_ERRORCODE_SUCCESSFUL];
    if ([self.delegate respondsToSelector:@selector(paymentControllerDidFinishWithResponse:)]) {
        [self.delegate paymentControllerDidFinishWithResponse:response];
    }
}

- (void)handlePaymentClose:(NSString *)notifyKey {
    if ([self.delegate respondsToSelector:@selector(paymentControllerDidClose:data:)]) {
        [self.delegate paymentControllerDidClose:notifyKey data:nil];
    }
}

- (void)handlePaymentNotifyResult:(NSString *)notifyKey {
    if ([self.delegate respondsToSelector:@selector(paymentControllerNotifyCloseWithKey:data:)]) {
        [self.delegate paymentControllerNotifyCloseWithKey:notifyKey data:nil];
    }
}

#pragma mark UITableViewDelegate vs DataSource

/**
 @discussion - PaymentMethodsViewController must override this method to return correct number of
 rows, other controllers should not override this one
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

/**
 @discussion this method always return description cell at last cell when hasDescriptionBottom,
 sublcass must override this method to return other cell except description cell, should check
 [super tableView:cellForRowAtIndexPath:] before return other cells
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return nil; //must override
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (void)showAlertWithTitle:(NSString *)title anderrorMessage:(NSString *)errorMessage {
    [self dismissKeyboard];
    [ZPDialogView showDialogWithType:DialogTypeNotification title:title message:errorMessage buttonTitles:@[[R string_ButtonLabel_Close]] handler:nil];


}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == tableView.numberOfSections - 1) {
        return [self configFooter];
    }
    return [[UIView alloc] initWithFrame:CGRectZero];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == tableView.numberOfSections - 1) {
        return [self isKindOfClass:[ZPResultHandler class]] ? 150 : 95;
    }
    return 0;
}

- (void)tableView:(UITableView *)tableView willDisplayFooterView:(nonnull UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *headerView = (UITableViewHeaderFooterView *) view;
    if ([headerView isKindOfClass:[UITableViewHeaderFooterView class]]) {
        [headerView.backgroundView setBackgroundColor:[UIColor clearColor]];
    }
}

- (void)updateBtnOk:(NSNotification *)note {

    UIButton *btnFooter = (UIButton *) [self.mTableView viewWithTag:200];
    UIButton *btnGet = (UIButton *) note.object;
    [btnFooter setTitle:btnGet.currentTitle forState:UIControlStateNormal];
    //[btnFooter setHidden:!btnGet.isHidden];
    [btnFooter setBackgroundColor:btnGet.backgroundColor];
    [btnFooter setEnabled:btnGet.isEnabled];
}

- (void)btnOkClickedHandler {
    [self processPayment];
}

- (void)updateOKButtonColor {
    _uibutton.backgroundColor = [UIColor colorFromHexString:[self okButtonBackgroundColor]];
}

- (UIView *)configFooter {
    UIView *viewHeader = [UIView.alloc initWithFrame:CGRectMake(0, 0, self.mTableView.frame.size.width, 95)];

    _uibutton = [UIButton.alloc initWithFrame:CGRectMake(10, 45, self.mTableView.frame.size.width - 20, 50)];
    [_uibutton setTitle:[self okButtonTitle] forState:UIControlStateNormal];
    _uibutton.backgroundColor = [UIColor colorFromHexString:[self okButtonBackgroundColor]];
    [_uibutton setBackgroundColor:[UIColor buttonHighlightColor] forState:UIControlStateHighlighted];
    [_uibutton setBackgroundColor:[UIColor disableButtonColor] forState:UIControlStateDisabled];
    [_uibutton.titleLabel zpMainBlackRegular];
    _uibutton.titleLabel.textAlignment = NSTextAlignmentCenter;
    _uibutton.layer.cornerRadius = 3;
    _uibutton.tag = 200;
    [_uibutton addTarget:self action:@selector(btnOkClickedHandler) forControlEvents:UIControlEventTouchUpInside];

    [viewHeader addSubview:_uibutton];
    viewHeader.tag = 100;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBtnOk:) name:@"updateBtnOk" object:nil];

    return viewHeader;
}

#pragma mark - UIPaymentCellDelegate

- (void)recordViewOnKeyboardReturnButtonClicked {
    [self dismissKeyboard];

    [self processPayment];
}

#pragma mark - alert notify

- (void)showAlertNotifyWith:(NSString *)message
               buttonTitles:(NSArray *)buttonTitles
                    handler:(void (^)(NSInteger idx))handler {
    [ZPDialogView showDialogWithType:DialogTypeNotification
                               title:[R string_Dialog_Title_Notification]
                             message:message
                        buttonTitles:buttonTitles
                             handler:handler];
}


- (void)showAlertNotifyWith:(NSString *)message
                    handler:(void (^)(NSInteger idx))handler {
    [ZPDialogView showDialogWithType:DialogTypeNotification
                               title:[R string_Dialog_Title_Notification]
                             message:message
                        buttonTitles:@[[R string_ButtonLabel_Close]] handler:handler];
}


- (NSString *)getStringWithKey:(NSString *)key andDefaultString:(NSString *)defaultString {
    NSString *resultString = [[ZPDataManager sharedInstance] messageForKey:key];
    if (resultString.length <= 0 || [resultString isEqualToString:key]) {
        resultString = defaultString;
    }
    return resultString;
}

- (UITableViewCell *)createCellAt:(NSIndexPath *)indexPath with:(UITableView *)tableView {
    return nil;
}

- (NSString *)checkMethodIsValid:(ZPPaymentMethod *)method {

    NSString *bankCode = method.channel.bankCode;
    ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:self.atmCard.bankCode];
    if (bankCode.length > 0 && bank) {
        if(!method.isSupportAmount || method.isTotalChargeGreaterThanMaxAmount) {
            NSString *channelName = bank.fullName;
            NSString *min = [@(method.channel.minPPAmount) formatCurrency];
            NSString *max = [@(method.channel.maxPPAmount) formatCurrency];
            return [NSString stringWithFormat:[R string_Atm_Map_Card_Min_Max_Message], channelName, min, max];
        }
    }

    NSString *errorMsg = @"";
    if (method.status == ZPAPPDisable) {
        errorMsg = [[ZPDataManager sharedInstance] messageForKey:stringKeyChannelUnsupport];
    } else if (method.status == ZPAPPMaintenance) {
        errorMsg = [self.dataManager messageForKey:stringKeyChannelIsMaintain];
    } else if (!method.isSupportAmount) {
        errorMsg = [self.dataManager messageForKey:stringKeyChannelUnsupportAmount];
    } else if (method.isTotalChargeGreaterThanMaxAmount) {
        errorMsg = stringErrorOverAmountPerTrans;
    }
    return errorMsg;
}

- (UITableViewCell *)createCellWithTableView:(UITableView *)tableView {
    UITableViewCell *cell = [UITableViewCell defaultCellForTableView:tableView];
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.textLabel.text = stringGettingPaymentStatus;
    cell.textLabel.textColor = [UIColor redColor];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    return cell;
}

- (void)showAlertNotifyRetryPin:(NSString *)errorMessage {
    ZPCheckPinViewController *popup = [ZPCheckPinViewController currentView];
    if (popup) { // case user thanh toán bằng nhập mật khẩu thanh toán
        [ZPCheckPinViewController showErrorMessage:errorMessage];
        return;
    }
    // case lấy password từ touchId.    
    [self handleSubmitTransWithWrongPasswordFromdTouchId:errorMessage];
}

- (void)handleSubmitTransWithWrongPasswordFromdTouchId:(NSString *)message {
    id <ZPWalletDependenceProtocol> helper = [ZaloPayWalletSDKPayment sharedInstance].appDependence;
    [helper setUsingTouchId:false];
    [helper removeZaloPayPassword];
    [ZPProgressHUD dismiss];
    @weakify(self);
    [ZPDialogView showDialogWithType:DialogTypeNotification
                             message:message
                   cancelButtonTitle:[R string_ButtonLabel_Close]
                    otherButtonTitle:nil
                      completeHandle:^(int buttonIndex, int cancelButtonIndex) {
                          @strongify(self);
                          ZPContainerViewController *controller = [ZPContainerViewController castFrom:self.mTableView.viewController];
                          [controller.actionButton sendActionsForControlEvents:UIControlEventTouchUpInside];
                      }];
}

- (void)showAlertNotifyNetWorkProblem {
    [self dismissKeyboard];
    @weakify(self);
    [ZPDialogView showDialogWithType:DialogTypeWarning
                             message:stringNetworkError
                   cancelButtonTitle:[R string_ButtonLabel_Close]
                    otherButtonTitle:nil
                      completeHandle:^(int buttonIndex, int cancelButtonIndex) {
                          @strongify(self);
                          [self showKeyboard];
                      }];
}

- (void)dismissPinView:(BOOL)success {
    [ZPCheckPinViewController dissmiss:success];
}

- (ZPPaymentMethod *)validateBank:(NSString *)bankCode {
    ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:bankCode];
    ZPPaymentMethodType methodType = bank.supportType == ZPSDK_BankType_Account ? ZPPaymentMethodTypeIbanking : ZPPaymentMethodTypeAtm;
    ZPChannel *channel = [[ZPDataManager sharedInstance] channelWithTypeAndBankCode:methodType bankCode:bankCode];
    ZPPaymentMethod *method = [[ZPPaymentMethod alloc] init];
    method.minFee = channel.minFee;
    method.feeRate = channel.feeRate;
    method.feeCalType = channel.feeCalType;
    method.status = channel.status;
    method.minAppVersion = channel.minAppVersion;
    method.isSupportAmount = [channel shouldSupportAmount:self.bill.amount];
    method.isTotalChargeGreaterThanMaxAmount = ![channel shouldSupportAmount:[method calculateChargeWith:self.bill.finalAmount]];
    method.channel = channel;
    return method;
}

- (void)saveAtmCard:(ZPAtmCard *)card {
    card.isMapcard = [self isAtmMapcard];
    [ZPDataManager sharedInstance].tempSavedCard = card;
}

- (BOOL)isRequirePin {
    return [ZaloPaySDKUtil isRequirePin:self.bill method:self.paymentMethodDidChoose];
}
@end
