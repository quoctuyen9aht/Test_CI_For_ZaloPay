//
//  ZPMethodsHandler.h
//  ZaloPayWalletSDK
//
//  Created by Thi La on 4/23/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPPaymentHandler.h"
@class ZPBillExtraInfoData;

@interface ZPMethodsHandler : ZPPaymentHandler<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) NSArray *dataSource;
@property (nonatomic, strong) UIButton *actionButton;
@property (nonatomic) NSInteger selectedIndex;
@property (nonatomic, strong) ZPBillExtraInfoData *feeModel;

- (void)updateFee:(long)fee;

- (void)reloadUI;
@end
