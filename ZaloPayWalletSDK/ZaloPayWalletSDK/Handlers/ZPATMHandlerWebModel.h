//
//  ZPATMHandlerWebModel.h
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 4/26/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ZPSmartLinkHelper;
typedef NS_ENUM(NSInteger, ZPATMHandlerWebURLType) {
    ZPATMHandlerWebURLTypeATMError,
    ZPATMHandlerWebURLTypeVTBCaptcha,
    ZPATMHandlerWebURLTypeVTBOTP,
    ZPATMHandlerWebURLTypeCaptchaCallback,
    ZPATMHandlerWebURLTypeVCBDirectIsCaptcha,
    ZPATMHandlerWebURLTypeVCBDirectIsOTP,
    ZPATMHandlerWebURLTypeOTPCallback,
    ZPATMHandlerWebURLTypeUnknown
};


@interface ZPATMHandlerWebModel : NSObject
@property (strong, nonatomic) ZPSmartLinkHelper * jsHelper;
@property (assign, nonatomic) NSTimeInterval atmStartTimestamp;
@property (assign, nonatomic) NSTimeInterval atmCaptchaStartTime;
@property (assign, nonatomic) NSTimeInterval atmCaptchaEndTime;
@property (assign, nonatomic) NSTimeInterval atmOtpStartTime;
@property (assign, nonatomic) NSTimeInterval atmOtpEndTime;

- (instancetype)initWithStartEvent:(RACSignal *)start
                               end:(RACSignal *)end
                              with:(UIWebView *)wv;
- (BOOL)is123PResult:(NSString *)url;
- (BOOL)isAuthenLoadFail:(NSString *)url;
- (NSString *)jsRequestVicomCaptcha;
- (NSString *)jsCaptchaSML;
- (NSString *)jsCaptchaBanknet;
- (NSString *)jsVicomErrorMessage;
- (ZPATMHandlerWebURLType)typeWith:(NSString *)link;
@end
