//
//  ZPMethodsHandler+SubmitTrans.m
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 7/1/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPMethodsHandler+SubmitTrans.h"
#import "ZPPaymentMethod.h"
#import "ZPWalletSubmitTransModel.h"
#import "ZPPaymentChannel.h"
#import "ZPBill.h"
#import "ZPPaymentResponse.h"
#import "ZPContainerViewController.h"
#import "ZaloPayWalletSDKPayment.h"
#import "ZPCheckPinViewController.h"
#import "ZPProgressHUD.h"
#import "ZPDataManager.h"
#import "ZPMethodsHandler+Action.h"
#import <ZaloPayAnalyticsSwift/ZaloPayAnalyticsSwift-Swift.h>

@implementation ZPMethodsHandler (SubmitTrans)

- (void)handleMethod:(ZPPaymentMethod *)method
            password:(NSString *)password
         fromTouchId:(BOOL)fromTouchId {

    if (![[ZPDataManager sharedInstance] checkInternetConnect]) {
        [self showAlertWithMessage:stringNetworkError];
        return;
    }

    [ZPCheckPinViewController currentView] == nil ? [ZPProgressHUD showWithStatus:nil] : [ZPCheckPinViewController startLoading];

    if (method.methodType == ZPPaymentMethodTypeZaloPayWallet) {
        [self zalopayWalletSubmitTrans:method password:password fromTouchId:fromTouchId];
        return;
    }
    if ([self.delegate respondsToSelector:@selector(paymentMethodsControllerDidChooseMethod:password:)]) {
        [self.delegate paymentMethodsControllerDidChooseMethod:method password:password];
    }
}

- (void)trackChoosePaymentMethod:(ZPPaymentMethodType)method bankcode:(NSString *)bankCode {
    [[ZPAppFactory sharedInstance].orderTracking trackChoosePayMethod:self.bill.appTransID
                                                                pcmid:method
                                                             bankCode:bankCode
                                                               result:OrderStepResult_Success];
}


#pragma mark - ZaloPayWallet

- (void)zalopayWalletSubmitTrans:(ZPPaymentMethod *)method
                        password:(NSString *)password
                     fromTouchId:(BOOL)fromTouchId {
    int channelId = method.channel.channelID;
    RACSignal *signal = [ZPWalletSubmitTransModel submiTransWithPassword:password
                                                                    bill:self.bill
                                                               channelId:channelId];
    if (self.bill.voucherInfo == nil) {
        [[ZPTrackingHelper shared] trackEvent: ZPAnalyticEventActionSdk_voucher_non];
    }
    [self handleSubmitTransSignal:signal method:method password:password fromTouchId:fromTouchId];
    [self trackChoosePaymentMethod:ZPPaymentMethodTypeZaloPayWallet bankcode:@""];
}

#pragma mark - Handle SubmitTransSignal

- (void)handleSubmitTransSignal:(RACSignal *)signal
                         method:(ZPPaymentMethod *)method
                       password:(NSString *)password
                    fromTouchId:(BOOL)fromTouchId {
    [signal subscribeNext:^(ZPPaymentResponse *response) {
        if (response.errorCode == kZPZaloPayCreditErrorCodePinRetry) {
            [[ZPTrackingHelper shared] trackEvent: ZPAnalyticEventActionSdk_password_fail];
            [self showAlertNotifyRetryPin:response.message];
            return;
        }
        if (response.errorCode == ZALOPAY_ERRORCODE_SUCCESSFUL) {
            [[ZaloPayWalletSDKPayment sharedInstance].appDependence savePassword:password];
        }
        [ZPProgressHUD dismiss];
        ZPContainerViewController *container = [ZPContainerViewController castFrom:self.mTableView.viewController];
        [container calculateFeeWithMethod:method];
        [self handlePaymentResponse:response];
    }];
}

@end
