//
//  ZPMethodsHandler+DefaultMethods.m
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/17/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPMethodsHandler+DataSource.h"
#import "ZPMethodBaseCell.h"
#import "ZPBillExtraInfoCell.h"
#import "ZPOrtherMethodCell.h"
#import "ZPMethodPadingCell.h"
#import "ZPPaymentMethod.h"
#import "ZaloPayWalletSDKPayment.h"
#import "ZPBill+ExtInfo.h"
#import "ZPMiniBank.h"
#import "ZPDataManager.h"
#import "ZPAddNewMethodCell.h"
#import "ZPVoucherInfo.h"
#import "ZPZaloPayWalletMethodCell.h"
#import "ZPPaymentChannel.h"
#import "ZPBill.h"
#import "ZPMethodsHandler+Action.h"


@implementation ZPMethodsHandler (DataSource)


- (NSArray *)loadDataSource {
    
    // 1. lọc lại kênh thanh toán nếu có voucher.
    ZPBill *bill = [ZPBill coppyFrom:self.bill];
    [self updateMethods:self.paymentMethods withVoucherOfBill:bill];
    
    // 2. sắp xếp danh sách kênh thanh toán
    NSArray *sortMethods = [self sortAllMethod:self.paymentMethods];
    
    // 3. tạo datasoure cho tableview
    NSMutableArray *dataSource = [self billInfo:self.bill];
    ZPMethodPadingData *padding = [ZPMethodPadingData new];
    [dataSource addObject:padding];
    [dataSource addObjectsFromArray:[self createCellModel:sortMethods]];
    return dataSource;
}

- (NSMutableArray *)createCellModel:(NSArray *)sortMethod {
    NSMutableArray *arrReturn = [NSMutableArray new];
    NSMutableArray *userMethod = [NSMutableArray array];
    for (ZPPaymentMethod *method in sortMethod) {
        if (method.defaultChannel) {
            continue;
        }
        long amount = [self.bill finalAmount];
        ZPPaymentMethodCellDisplay *model = [[ZPPaymentMethodCellDisplay alloc] initWith:method
                                                                              withAmount:amount];
        [userMethod addObject:model];
    }
    ((ZPPaymentMethodCellDisplay *) userMethod.lastObject).isLastCell = false;
    
    [arrReturn addObjectsFromArray:userMethod];
    [arrReturn addObject:[ZPAddNewMethodData new]];
    return arrReturn;
}

- (void)updateMethods:(NSArray *)allMethods withVoucherOfBill:(ZPBill *)bill {
    if (bill.transType != ZPTransTypeBillPay) {
        return;
    }
    
    long currentBalance = [ZaloPayWalletSDKPayment sharedInstance].appDependence.currentBalance;
    ZPVoucherManager *manager = [ZaloPayWalletSDKPayment sharedInstance].zpVoucherManager;
    ZPPromotionManager *promotionManager = [ZaloPayWalletSDKPayment sharedInstance].zpPromotionManager;

    NSArray *arrVoucherFiltered = [manager voucherCachesForBill:bill];
    NSArray *arrPromotionsFiltered = [promotionManager promotionsCachesForBill:bill];
    
    for (ZPPaymentMethod *method in allMethods) {
        if (method.defaultChannel) {
            continue;
        }
        NSString *bankCode = method.bankCode;
        
        if ([method isCCCard]) {
            bankCode = stringCreditCardBankCode;
        }
        if ([method isCCDebitCard]) {
            bankCode = stringDebitCardBankCode;
        }
        ZPChannel *channel = [[ZPDataManager sharedInstance] channelWithTypeAndBankCode:method.methodType bankCode:bankCode];
        method.channel = channel;
        bill.currentPaymentMethod = method;
        
        NSArray <ZPVoucherHistory *> *arrVoucherAvailables = [manager filterAvailableVouchersToEnablePaymentMethod:arrVoucherFiltered forBill:bill];
        NSArray <ZPPromotion *> *arrPromotionsAvailables = [promotionManager filterAvailablePromotionsToEnablePaymentMethod:arrPromotionsFiltered forBill:bill];
        
        // 3 Điều kiện ban đầu đã thoả với giá tiền gốc của Bill thì có hay không có (voucher or promotion) cũng phải bật kênh lên
        method.isSupportAmount = (bill.amount + method.totalChargeFee >= method.channel.minPPAmount) &&
                                 (bill.amount + method.totalChargeFee <= method.channel.maxPPAmount);
        method.isTotalChargeGreaterThanMaxAmount = bill.amount + method.totalChargeFee > method.channel.maxPPAmount;
        method.isTotalChargeGreaterThanBalance = method.methodType == ZPPaymentMethodTypeZaloPayWallet ? bill.amount + method.totalChargeFee > currentBalance : NO;
        
        // Nếu có ít nhất 1 điều kiện ban đầu k thoả thì xét xem trong list (vouchers or promotions) nếu có bất kì voucher nào làm cho thoả cả 3 thì bật lên.
        if (!method.isSupportAmount ||
            method.isTotalChargeGreaterThanMaxAmount ||
            method.isTotalChargeGreaterThanBalance) {
            if (arrVoucherAvailables.count == 0 && arrPromotionsAvailables.count == 0) {
                long totalCharge = bill.amount + method.totalChargeFee;
                if (totalCharge >= method.channel.minPPAmount && totalCharge <= method.channel.maxPPAmount) {
                    method.isSupportAmount = YES;
                    method.isTotalChargeGreaterThanMaxAmount = NO;
                }
                method.isTotalChargeGreaterThanBalance = method.methodType == ZPPaymentMethodTypeZaloPayWallet ? totalCharge > currentBalance : NO;
            } else {
                NSMutableArray *arrVouchersAndPromotions = [NSMutableArray new];
                [arrVouchersAndPromotions addObjectsFromArray:arrVoucherAvailables];
                [arrVouchersAndPromotions addObjectsFromArray:arrPromotionsAvailables];
                for (NSObject *endow in arrVouchersAndPromotions) {
                    long long totalCharge = bill.amount + method.totalChargeFee;
                    long long endowValue = [endow isKindOfClass:[ZPPromotion class]] ? [promotionManager getValueCurrencyFromPromotion:(ZPPromotion *)endow ofBill:bill] : [manager getValueCurrencyFromVoucher:(ZPVoucherHistory *)endow ofBill:bill];
                    totalCharge = totalCharge - endowValue;
                    if (method.methodType == ZPPaymentMethodTypeZaloPayWallet) {
                        if (totalCharge >= method.channel.minPPAmount && totalCharge <= method.channel.maxPPAmount && totalCharge <= currentBalance) {
                            method.isSupportAmount = YES;
                            method.isTotalChargeGreaterThanMaxAmount = NO;
                            method.isTotalChargeGreaterThanBalance = NO;
                            break;
                        }
                    } else {
                        if (totalCharge >= method.channel.minPPAmount && totalCharge <= method.channel.maxPPAmount) {
                            method.isSupportAmount = YES;
                            method.isTotalChargeGreaterThanMaxAmount = NO;
                            method.isTotalChargeGreaterThanBalance = NO;
                            break;
                        }
                    }
                    
                }
                
            }
        }
        
        method.isHaveAvailableEndow = arrPromotionsAvailables.count > 0 || arrVoucherAvailables.count > 0;
    }
    
    [self updatePromotionHint:arrPromotionsFiltered allMethod:allMethods];
}

- (void)updatePromotionHint:(NSArray *)allPromotions allMethod:(NSArray *)allMethods {
    if (allPromotions.count == 0) {
        return;
    }
    
    for (ZPPaymentMethod *method in allMethods) {
        if (method.defaultChannel) {
            continue;
        }
        if ([ZPPaymentMethodCellDisplay isAvailable:method] == false) {
            continue;
        }
        method.isShowHintTextPromotions = [self paymentChannelHasPromotionIn:allPromotions and:method];
    }
}

- (BOOL) paymentChannelHasPromotionIn:(NSArray <ZPPromotion *> *)promotions and:(ZPPaymentMethod *)method {
    
    for (ZPPromotion *promotion in promotions) {
        // Nếu promotions đều cho tất cả thì không cần hiện thông báo có khuyến mãi cho kênh nào cả.
        //useconditionpmcs chứa -1 nghĩa là cho toàn bộ kênh
        if ([promotion.paymentConditions.useconditionpmcs containsObject:@(-1)]) {
            continue;
        }
        
        if (![promotion.paymentConditions.useconditionpmcs containsObject:@(method.channel.channelType)]) {
            continue;
        }
        
        BOOL isCC = [method isCCCard] || [method isCCDebitCard];
        NSArray *bankCodes = isCC ? promotion.paymentConditions.useconditionccbankcodes: promotion.paymentConditions.useconditionbankcodes;
        NSString *methodBankCode = method.bankCode.lowercaseString;
        for (NSString *oneCode in bankCodes) {
            NSString *code = [[NSString castFrom:oneCode] lowercaseString];
            if ([methodBankCode containsString:code]) {
                return true;
            }
        }
    }
    return false;
}

- (ZPOrtherMethodData *)ortherMethodModel {
    if (self.bill.transType != ZPTransTypeWithDraw && self.paymentMethods.count > 0) {
        ZPOrtherMethodData *orther = [[ZPOrtherMethodData alloc] init];
        orther.title = [R string_Payment_Method_Orther_Method];
        return orther;
    }
    return nil;
}

- (NSMutableArray *)billInfo:(ZPBill *)bill {
    NSMutableArray *arrayInfo = [NSMutableArray array];
    ZPBillExtraInfoData *data = [self createAppNameModel];
    self.feeModel = [self createFeeModel];
    
    
    [arrayInfo addObject:data];
    NSArray *infos = [bill extFromItem];
    if (infos.count > 0) {
        NSArray *newArray = [infos map:^ZPBillExtraInfoData *(ZPBillExtraInfo *obj) {
            ZPBillExtraInfoData *model = [[ZPBillExtraInfoData alloc] initWithTitle:obj.extraKey value:obj.extraValue];
            return model;
        }];
        [arrayInfo addObjectsFromArray:newArray];
    }
    
    if (self.bill.voucherInfo != nil) {
        [arrayInfo addObject:self.bill];
        [arrayInfo addObject:self.bill.voucherInfo];
    }else if (self.bill.promotionInfo != nil) {
        [arrayInfo addObject:self.bill];
        [arrayInfo addObject:self.bill.promotionInfo];
    }
    [arrayInfo addObject:self.feeModel];
    
    return arrayInfo;
}

- (ZPBillExtraInfoData *)createAppNameModel {
    NSString *appInfo = [self.bill appName];
    return [[ZPBillExtraInfoData alloc] initWithTitle:[R string_Pay_Bill_Merchant_Name]
                                                value:appInfo
                                                 type:ZPBillExtraInfoType_Info];
    
}

- (ZPBillExtraInfoData *)createFeeModel {
    return [[ZPBillExtraInfoData alloc] initWithTitle:[R string_Pay_Bill_Fee_Title]
                                                value:nil
                                                 type:ZPBillExtraInfoType_Fee];
}


- (NSInteger)selectedIndexFromDataSource:(NSArray *)dataSource {
    NSInteger selectedIndex = -1;
    NSInteger newMethodIndex = -1;
    NSInteger oldMethodIndex = -1;
    
    for (int i = 0; i < dataSource.count; i++) {
        
        ZPPaymentMethodCellDisplay *model = dataSource[i];
        if (![model isKindOfClass:[ZPPaymentMethodCellDisplay class]]) {
            continue;
        }
        ZPPaymentMethod *method = model.method;
        if (model.type != ZPPaymentMethodCellTypeAvailable) {
            continue;
        }
        if (method.methodType == ZPPaymentMethodTypeZaloPayWallet) {
            selectedIndex = i;
            continue;
        }
        if (model.isNewMethod) {
            newMethodIndex = i;
            break;
        }
        // assure current payment method is lower priority than new method
        if ([method.paymenMethodId isEqualToString:self.bill.currentPaymentMethod.paymenMethodId]) {
            oldMethodIndex = i;
            continue;
        }
        if (selectedIndex < 0) {
            selectedIndex = i;
            continue;
        }
    }
    
    // case thanh toán xong đi map thẻ -> ưu tiên chọn kện mới.
    if (newMethodIndex >= 0) {
        return newMethodIndex;
    }
    
    // case thanh toán chọn map thẻ xong back lại -> chọn lại index cũ.
    if (oldMethodIndex >= 0) {
        return oldMethodIndex;
    }
    // chọn kênh mặc định (kênh ví).
    return selectedIndex;
}

- (NSArray *)sortAllMethod:(NSArray *)methods {
    if ([methods count] <= 0) {
        return methods;
    }
    NSDictionary *sortMap = [[ZPDataManager sharedInstance].bankManager bankDisplayOrderForPayment];
    methods = [methods sortedArrayUsingComparator:^NSComparisonResult(ZPPaymentMethod *obj1, ZPPaymentMethod *obj2) {
        int order1 = -1;
        int order2 = -1;
        if (obj1.bankCode) {
            order1 = [sortMap intForKey:obj1.bankCode];
        }
        if (obj2.bankCode) {
            order2 = [sortMap intForKey:obj2.bankCode];
        }
        return (order1 < order2) ? NSOrderedAscending : NSOrderedDescending;
    }];
    
    methods = [methods sortedArrayUsingComparator:^NSComparisonResult(ZPPaymentMethod *obj1, ZPPaymentMethod *obj2) {
        return obj1.displayOrder > obj2.displayOrder;
    }];
    
    ZPPaymentMethod *zaloPayWallet = nil;
    NSMutableArray *allCard = [NSMutableArray array];
    NSMutableArray *unSupport = [NSMutableArray array];
    NSMutableArray *defaultMethod = [NSMutableArray array];
    NSMutableArray *defaultUnSupportMethod = [NSMutableArray array];
    
    for (int index = 0; index < methods.count; index++) {
        ZPPaymentMethod *method = methods[index];
        if (method.methodType == ZPPaymentMethodTypeZaloPayWallet && method.status == ZPAPPEnable) {
            zaloPayWallet = method;
            continue;
        }
        BOOL isUnSupport = [self checkChannelUnSupport:method];
        if (isUnSupport && !method.defaultChannel) {
            [unSupport addObject:method];
            continue;
        }
        if (method.methodType == ZPPaymentMethodTypeSavedCard ||
            method.methodType == ZPPaymentMethodTypeCCDebit ||
            method.methodType == ZPPaymentMethodTypeSavedAccount) {
            [allCard addObject:method];
            continue;
        }
        BOOL isCC = [method.savedCard isCCCard];
        if (isUnSupport) {
            isCC ? [defaultUnSupportMethod insertObject:method atIndex:0] : [defaultUnSupportMethod addObject:method];
            continue;
        }
        //isCC ? [defaultMethod insertObject:method atIndex:0] : [defaultMethod addObject:method];
    }
    
    NSMutableArray *allMethod = [NSMutableArray array];
    [allMethod addObjectNotNil:zaloPayWallet];
    [allMethod addObjectsFromArray:allCard];
    [allMethod addObjectsFromArray:unSupport];
    [allMethod addObjectsFromArray:defaultMethod];
    [allMethod addObjectsFromArray:defaultUnSupportMethod];
    return allMethod;
}

- (BOOL)checkChannelUnSupport:(ZPPaymentMethod *)method {
    if (method.support == ZPAPPDisable || method.support == ZPAPPMaintenance) {
        return YES;
    }
    if (method.status == ZPAPPMaintenance) {
        return YES;
    }
    if (method.miniBank == nil || method.miniBank.minibankStatus == ZPMiniBankStatus_Maintenance) {
        return YES;
    }
    if (!method.isSupportAmount) {
        return YES;
    }
    if ([[ZPDataManager sharedInstance] chekMinAppVersion:method.minAppVersion]) {
        return YES;
    }
    if (method.isTotalChargeGreaterThanBalance || method.isTotalChargeGreaterThanMaxAmount) {
        return YES;
    }
    return NO;
}

- (BOOL)isAllMethodUnSupportCurrentBill {
    for (ZPPaymentMethodCellDisplay *dispay in self.dataSource) {
        if ([dispay isKindOfClass:[ZPPaymentMethodCellDisplay class]] &&
            dispay.type == ZPPaymentMethodCellTypeAvailable) {
            return NO;
        }
    }
    return YES;
}

- (BOOL)needRechargeMoney {
    
    for (ZPPaymentMethodCellDisplay *dispay in self.dataSource) {
        if (![dispay isKindOfClass:[ZPPaymentMethodCellDisplay class]]) {
            continue;
        }
        if (dispay.method.methodType == ZPPaymentMethodTypeZaloPayWallet && dispay.type == ZPPaymentMethodCellTypeNotEnoughMoney) {
            return YES;
        }
    }
    return NO;
}


@end
