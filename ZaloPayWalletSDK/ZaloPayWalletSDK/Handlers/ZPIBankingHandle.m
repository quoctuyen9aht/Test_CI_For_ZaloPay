//
//  iBankingHandle.m
//  ibankingHandle
//
//  Created by CPU11695 on 11/1/16.
//  Copyright © 2016 CPU11695. All rights reserved.
//
#import <JavaScriptCore/JavaScriptCore.h>
#import "ZPIBankingHandle.h"
#import "NSData+ZPExtension.h"
#import "ZPProgressHUD.h"
#import "ZPResourceManager.h"
#import "ZPBill.h"
#import "ZPDataManager.h"
#import "ZPCreditCardWebviewCell.h"
#import "ZPPaymentResponse.h"
#import "ZPPaymentManager.h"
#import "NetworkManager+ZaloPayWalletSDK.h"
#import "NSString+ZPExtension.h"
#import "ZPPaymentChannel.h"
#import "ZPDidChoosePaymentMethodCell.h"
#import "ZPAtmAuthenTypeViewCell.h"
#import "UIColor+ZPExtension.h"
#import "ZPOtpCell.h"
#import "ZaloPayWalletSDKLog.h"
#import "ZPBank.h"
#import "ZPMapCardLog.h"
#import <ZaloPayShareControl/ZaloPayShareControl-Swift.h>

#define kZP_CREDIT_CARD_WEBVIEW_CELL_ID         @"credit-card-webview-cell"
#define kZP_DID_CHOOSE_PAYMENT_METHOD_CELL_ID   @"did-choose-payment-method-cell"
#define kZP_ATM_AUTHEN_TYPE_CELL_ID             @"atm-authen-type-cell"
#define kZP_ATM_OTP_CELL_ID                     @"otp-cell"

@interface ZPIBankingHandle () <UITableViewDataSource, UITableViewDelegate, UIWebViewDelegate, ZPPaymentCellDelegate> {
    NSInteger loginCount;
    NSURLRequest *urlRequest;
    NSString *bankUrl;
    long long atmCaptchaStartTime;
    long long atmCaptchaEndTime;
    long long atmOtpStartTime;
    long long atmOtpEndTime;
    BOOL isShowDialogMaintenance;
    BOOL didReturned;
    NSMutableArray *arrRequested;
    NSTimer *progressHUDTimer;
    BOOL isLoading;
    NSTimer *checkWebViewTimeOutTimer;
    JSContext *context;
    BOOL didFinish;
    
    int checkLoadingCount;

}
@property (nonatomic) NSTimeInterval atmStartTimestamp;
@property (nonatomic, strong) NSArray *listPhoneNumber;
@property(nonatomic, strong) UIView <ZPPlaceHolderViewProtocol> *placeholderView;
@end

@implementation ZPIBankingHandle
- (void)dealloc {
    [self.myWebView stopLoading];
    [self.myWebView setDelegate:nil];
    [self.myWebView removeFromSuperview];
    self.myWebView = nil;
    UITableViewCell *cell =
            [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    if ([cell isKindOfClass:[ZPPaymentViewCell class]]) {
        ZPPaymentViewCell *paymentCell = (ZPPaymentViewCell *) cell;
        if ([paymentCell respondsToSelector:@selector(setDelegate:)]) {
            paymentCell.delegate = nil;
        }
        if ([paymentCell isKindOfClass:[ZPCreditCardWebviewCell class]]) {
            ((ZPCreditCardWebviewCell *) paymentCell).ccWebview.delegate = nil;
            ((ZPCreditCardWebviewCell *) paymentCell).ccWebview = nil;
        }

    }
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (instancetype)init {
    self = [super init];
    if (self) {
        loginCount = 0;
        arrRequested = [[NSMutableArray alloc] init];
    }
    return self;
}

#pragma mark - Overrides

- (void)initialization {
    checkLoadingCount = 0;
    [self initBankInfoWithBankCode:self.bill.bankCode];
    self.bankInfo.amount = self.bill.amount;
    [self.mTableView setSeparatorColor:[UIColor clearColor]];
    [self.mTableView registerNib:[self.dataManager nibNamed:@"ZPCreditCardWebviewCell"] forCellReuseIdentifier:kZP_CREDIT_CARD_WEBVIEW_CELL_ID];

    [self.mTableView registerNib:[self.dataManager nibNamed:@"ZPDidChooseMethodCell"] forCellReuseIdentifier:kZP_DID_CHOOSE_PAYMENT_METHOD_CELL_ID];
    [self.mTableView registerNib:[self.dataManager nibNamed:@"ZPAtmAuthenType"] forCellReuseIdentifier:kZP_ATM_AUTHEN_TYPE_CELL_ID];
    [self.mTableView registerNib:[self.dataManager nibNamed:@"ZPOtpCell"] forCellReuseIdentifier:kZP_ATM_OTP_CELL_ID];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(ZPLinkAccountNotification:)
                                                 name:@"ZPInApp_Notification_Key"
                                               object:nil];
}

- (void)initBankInfoWithBankCode:(NSString *)bankCode{
    ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:bankCode];
    DDLogInfo(@"detectCell didDetectBank: %@", bank.bankName);
    self.bankInfo = [[ZPIbankingAccount alloc] init];
    if (bank) {
        if (bank.status != ZPBankStatusEnable && !isShowDialogMaintenance) {
            isShowDialogMaintenance = YES;
            NSString *strMaintain = bank.maintenanceMsg;
            uint64_t timeStamp = bank.maintainenanceTo;
            if(strMaintain == nil || strMaintain.length == 0){
                strMaintain = [NSString stringWithFormat:[self.dataManager messageForKey:@"message_bank_maintain"], bank.bankName];
            }
            strMaintain = [strMaintain stringByReplacingOccurrencesOfString:@"%s" withString:[NSString timeString:timeStamp]];

            [self showAlertNotifyWith:strMaintain handler:^(NSInteger idx) {
                if (self.delegate && [self.delegate respondsToSelector:@selector(paymentControllerDidGoBack)]) {
                    [self.delegate paymentControllerDidGoBack];
                }
            }];

        }
        self.bankInfo.bankCode = bankCode;
        self.bankInfo.bankName = bank.bankName;
        self.bankInfo.interfaceType = bank.interfaceType;
        self.bankInfo.requireOTP = bank.requireOTP;
        bankUrl = bank.loginbankurl;
        self.currentStep = ZPIBankingPaymentStepWaitingWebview;
        [self initPlaceholderView];
        self.isBankLoadWebview = YES;
    } else {

    }
}

- (void)initPlaceholderView {
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    self.placeholderView = (UIView <ZPPlaceHolderViewProtocol> *) [[ZaloPayWalletSDKPayment sharedInstance].appDependence createPlaceholderView];
    self.placeholderView.frame = CGRectMake(0, 0, screenSize.width, screenSize.height);
    self.placeholderView.hidden = YES;
    self.placeholderView.clipsToBounds = true;
    [self.mTableView addSubview:self.placeholderView];
    [self.placeholderView addRefreshTarget:self action:@selector(refreshButtonClick) for:UIControlEventTouchUpInside];
}

- (void)refreshButtonClick {

    if (![[ZPDataManager sharedInstance] checkInternetConnect]) {
        [self showAlertNotifyNetWorkProblem];
        return;
    }
    self.placeholderView.hidden = YES;
    [self showProgressHUD];
    ZPCreditCardWebviewCell *cell = [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    if ([cell isKindOfClass:[ZPCreditCardWebviewCell class]]) {
        [cell.ccWebview stopLoading];
        NSURLRequest *request = [arrRequested lastObject];
        [arrRequested removeLastObject];
        [cell.ccWebview loadRequest:request];
    }
}

- (NSString *)paymentControllerTitle {
    ZPIBankingAccountBill *bill = [ZPIBankingAccountBill castFrom:self.bill];
    if (!bill) {
        return [self.dataManager getTitleByTranstype:self.bill.transType];
    }
    if (bill.isMapAccount) {
        return @"Liên kết tài khoản";
    }
    return @"Huỷ liên kết tài khoản";
}

- (void)startHandle {
    loginCount = 0;
    self.currentStep = ZPIBankingPaymentStepLogin;
    [self showProgressHUD];
    [self loadRequestWithStringUrl:self.myWebView :bankUrl];
}

#pragma API handler

- (void)processPayment {
    [self dismissKeyboard];
    didReturned = false;
    if ([self checkOrderTimeout]) {
        return;
    }
    if (![[ZPDataManager sharedInstance] checkInternetConnect]) {
        [self showAlertNotifyNetWorkProblem];
        return;
    }
    self.bankInfo.step = self.currentStep;
    DDLogInfo(@"processPayment currentStep: %ld", (long) self.currentStep);
    switch (self.currentStep) {
        case ZPIBankingPaymentStepSavedAccountConfirm: {
            self.bankInfo.step = ZPIBankingPaymentStepSubmitPurchase;
            [self submitTran];
            break;
        }

        case ZPIBankingPaymentStepGetStatusByAppTransIdForClient: {
            self.bankInfo.step = ZPIBankingPaymentStepGetStatusByAppTransIdForClient;
            [self submitTran];
            break;
        }
        default:
            break;
    }
}

- (BOOL)checkOrderTimeout {
    //time out
    NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
    if (self.atmStartTimestamp <= 0 || now - self.atmStartTimestamp <= kATM_TRANSATION_TIMEOUT) {
        return NO;
    }

    NSString *message = [self.dataManager messageForKey:@"message_atm_time_out"];
    [self finishWithErrorCode:0 message:message
                       status:kZPZaloPayCreditStatusCodeFail result:-1 errorStep:ZPErrorStepNonRetry];
    self.didReturned = YES;
    return YES;
}

#pragma mark SubmitTrans

- (void)submitTran {
    if (![[ZPDataManager sharedInstance] checkInternetConnect]) {
        [self showAlertNotifyNetWorkProblem];
        return;
    }

    self.isSubmitStrans = YES;
    [self showProgressHUD];
    [self updateChannel];

    [[ZPAppFactory sharedInstance].orderTracking trackChoosePayMethod:self.bill.appTransID
                                                                pcmid:ZPPaymentMethodTypeIbanking
                                                             bankCode:stringVietcomBankCode
                                                               result:OrderStepResult_Success];


    [ZPPaymentManager paymentWithBill:self.bill
                       andPaymentInfo:self.bankInfo
                             andAppId:self.bill.appId
                              channel:self.channel
                               inMode:ZPPaymentMethodTypeIbanking
                          andCallback:^(ZPResponseObject *responseObject) {
                              ZPPaymentResponse *response = (ZPPaymentResponse *) responseObject;
                              if (response.zpTransID.length > 0) {
                                  self.bankInfo.zpTransID = response.zpTransID;
                              }

                              [self hideProgressHUDWithResultCode:response.errorCode];

                              if ([self handleExceptionCaseWith:response.errorCode]) {
                                  return;
                              }

                              self.atmStartTimestamp = [[NSDate date] timeIntervalSince1970];
                              [self handlePaymentWith:response];
                          }];
}

- (void)updateChannel {
    self.channel = [self.dataManager channelWithTypeAndBankCode:ZPPaymentMethodTypeIbanking bankCode:self.bill.bankCode];
    // set payment channel to zalopaywallet while using withdraw
    if (self.bill.transType == ZPTransTypeWithDraw) {
        self.channel = [self.dataManager channelWithTypeAndBankCode:ZPPaymentMethodTypeZaloPayWallet bankCode:self.bill.bankCode];
    }
}

- (void)handlePaymentWith:(ZPPaymentResponse *)response {
    int resultCode = response.errorCode;
    if (resultCode == kZPZaloPayCreditErrorCodePinRetry) {
        [self showAlertNotifyRetryPin:response.message];
        return;
    }
    if (resultCode == ZPServerErrorCodeAtmRetryOtp) {
        [self handleRetryOTP:response.message];
        return;
    }
    if (resultCode == ZPServerErrorCodeNoneError) {
        if ([self isRequirePin] && [ZaloPayWalletSDKPayment sharedInstance].appDependence) {
            [[ZaloPayWalletSDKPayment sharedInstance].appDependence savePassword:self.bankInfo.pin];
        }
        response.zpTransID = self.bankInfo.zpTransID;
        [self handlePaymentResponse:response];
        return;
    }
    if (resultCode > ZPServerErrorCodeNoneError) {
        DDLogInfo(@"self.bankInfo.zpTransID %@", self.bankInfo.zpTransID);
        if (response.isProcessing && response.data.length > 0) {
            if (response.actionType == ZPBankActionTypeWeb) {
                self.isBankLoadWebview = YES;
                [self bankSupportWebview:response.data];
            } else {
                self.isBankLoadWebview = NO;
                [self bankSupportAPI];
            }
            return;
        }
        if (response.exchangeStatus != kZPZaloPayCreditStatusCodeUnidentified) {
            response.exchangeStatus = kZPZaloPayCreditStatusCodeSuccess;
        }
        response.zpTransID = self.bankInfo.zpTransID;
        [self handlePaymentResponse:response];
        return;
    }
    if (resultCode < ZPServerErrorCodeNoneError) {
        response.exchangeStatus = kZPZaloPayCreditStatusCodeFail;
        response.zpTransID = self.bankInfo.zpTransID;
        [self handlePaymentResponse:response];
    }
}

- (BOOL)handleExceptionCaseWith:(int)resultCode {
    if ((resultCode == kZPZaloPayCreditErrorCodeRequestTimeout || resultCode == ZPServerErrorCodeTransIdNotExist)
            && self.currentStep != ZPAtmPaymentStepGetStatusByAppTransIdForClient) {
        self.currentStep = ZPAtmPaymentStepGetStatusByAppTransIdForClient;
        [self.delegate paymentControllerNeedUpdateLayouts];
        [self performSelector:@selector(processPayment) withObject:self afterDelay:1];
        return YES;
    }
    // hanle server not response
    if (resultCode == kZPZaloPayCreditErrorCodeUnknownException) {
        self.currentStep = ZPAtmPaymentStepGetTransStatus;
        [self getStatusOfTranxId:self.bankInfo.zpTransID callback:^(ZPPaymentResponse *response) {

        }];
        return YES;
    }
    return NO;
}

- (void)bankSupportAPI {
    self.currentStep = ZPAtmPaymentStepAtmAuthenPayer;
    // Card da luu
    if (([self isAtmMapcard] == false && self.paymentMethodDidChoose.methodType == ZPPaymentMethodTypeSavedCard)) {
        [self.delegate paymentControllerNeedUpdateLayouts];
    }

        //Map card and pay
    else {
        ZPPaymentViewCell *cell = (ZPPaymentViewCell *) [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [cell setOTP];
        });
    }
}

- (void)bankSupportWebview:(NSString *)jsonString {
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:NULL];
    if (self.bankInfo.interfaceType == ZPBankInterfaceTypeParseWeb) {
        bankUrl = [dictionary objectForKey:@"redirecturl"];
        [self loadRequestWithStringUrl:self.myWebView :bankUrl];
        return;
    }
    NSDictionary *dictionaryAgent = [[NSDictionary alloc] initWithObjectsAndKeys:
            WEB_USER_AGENT, @"UserAgent", nil];
    [[NSUserDefaults standardUserDefaults] registerDefaults:dictionaryAgent];
    bankUrl = [dictionary objectForKey:@"redirecturl"];
    self.currentStep = ZPAtmPaymentStepWaitingWebview;

    [self.delegate paymentControllerNeedUpdateLayouts];
}

- (void)getStatusOfTranxId:(NSString *)tranxId
                  callback:(void (^)(ZPPaymentResponse *response))callback {
    DDLogInfo(@"start getStatusOfTranxId: %@", tranxId);
    [self showProgressHUD];
    if (!self.bankInfo) {
        [self hideProgressHUD];
        return;
    }

    self.bankInfo.step = self.currentStep;
    [ZPPaymentManager paymentWithBill:self.bill
                       andPaymentInfo:self.bankInfo
                             andAppId:self.bill.appId
                              channel:self.channel
                               inMode:ZPPaymentMethodTypeAtm
                          andCallback:^(ZPResponseObject *responseObject) {
                              // process callback
                              [self hideProgressHUD];
                              ZPPaymentResponse *response = (ZPPaymentResponse *) responseObject;
                              response.paymentMethod = ZPPaymentMethodTypeAtm;
                              response.zpTransID = tranxId;
                              [self handlePaymentResponse:response];
                              [self writeLogTimeToServer];
                          }];
}

- (NSString *)okButtonTitle {
    return (self.currentStep == ZPIBankingPaymentStepSavedAccountConfirm) ?
            [[self.dataManager getButtonTitle:@"confirm"] capitalizedString] :
            [[self.dataManager getButtonTitle:@"continue"] capitalizedString];
}

- (BOOL)okButtonEnabled {
    return self.currentStep == ZPIBankingPaymentStepSavedAccountConfirm;
}

- (void)finishWithErrorCode:(int)errorCode
                    message:(NSString *)message
                     status:(int)status
                     result:(int)result
                  errorStep:(enum ZPErrorStep)errorStep {
    //    [self hideProgressHUD];
    if (didReturned) {
        return;
    }

    didReturned = YES;
    ZPPaymentResponse *pResponse = [[ZPPaymentResponse alloc] init];
    pResponse.errorCode = errorCode;
    pResponse.originalCode = errorCode;
    pResponse.message = message;
    pResponse.exchangeStatus = status;
    pResponse.errorStep = errorStep;
    pResponse.zpTransID = @"";
    [self handlePaymentResponse:pResponse];
    [self writeLogTimeToServer];
}

- (void)writeLogTimeToServer {
    ZPMapCardLog *log = [ZPMapCardLog new];
    log.transid = _bankInfo.zpTransID;
    log.pmcid = @(ZPPaymentMethodTypeIbanking);
    log.atmcaptcha_begindate = @(atmCaptchaStartTime);
    log.atmcaptcha_enddate = @(atmCaptchaEndTime);
    log.atmotp_begindate = @(atmOtpStartTime);
    log.atmotp_enddate = @(atmOtpEndTime);
    [ZPPaymentManager writeMapCardLog:log];
}

- (void)initWebView:(UIWebView *)wv {
    wv.delegate = self;
    wv.scrollView.scrollEnabled = YES;
    wv.scrollView.delegate = self;
    wv.contentMode = UIViewContentModeScaleAspectFit;
    [self logEventWith:wv];
}

- (void)loadRequestWithStringUrl:(UIWebView *)webView :(NSString *)stringUrl {
    NSURL *url = [NSURL URLWithString:stringUrl];
    urlRequest = [NSURLRequest requestWithURL:url
                                  cachePolicy:NSURLRequestUseProtocolCachePolicy
                              timeoutInterval:120];
    [self showProgressHUD];
    [webView loadRequest:urlRequest];
    self.jsHelper = [[ZPSmartLinkHelper alloc] init];
    self.jsHelper.webview = webView;
}

- (void)setUpUI {
}

- (UIWebView *)myWebView {
    if (!_myWebView) {
        _myWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        [self.zpParentController.view addSubview:_myWebView];
        [self.zpParentController.view bringSubviewToFront:_myWebView];
        [_myWebView setHidden:YES];
        [_myWebView setDelegate:self];
        _myWebView.scalesPageToFit = YES;
        _myWebView.tag = 55;
    }
    return _myWebView;
}

#pragma mark UITableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.currentStep == ZPIBankingPaymentStepSavedAccountConfirm) {
        if([self isRequirePin]) {
            return 1;
        }
        ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:self.bankInfo.bankCode];
        if (bank.otpType.length > 0 && [[bank.otpType componentsSeparatedByString:@"|"] count] >= 2) {

            return 2;
        }
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZPPaymentViewCell *cell;
    switch (self.currentStep) {
        case ZPIBankingPaymentStepWaitingWebview:
            DDLogInfo(@"--cellForRowAtIndexPath: ZPAtmPaymentStepWaitingWebview");
            cell = [tableView dequeueReusableCellWithIdentifier:kZP_CREDIT_CARD_WEBVIEW_CELL_ID];
            if (bankUrl != nil && ![bankUrl isEqualToString:@""]) {
                [self initWebView:((ZPCreditCardWebviewCell *) cell).ccWebview];
                [self loadRequestWithStringUrl:((ZPCreditCardWebviewCell *) cell).ccWebview :bankUrl];
            }
            [cell setBackgroundColor:[UIColor whiteColor]];
            break;
        case ZPIBankingPaymentStepSavedAccountConfirm: {
            DDLogInfo(@"--cellForRowAtIndexPath: ZPIBankingPaymentStepSavedAccountConfirm");
            tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
            [self.mTableView setSeparatorColor:[UIColor zpColorFromHexString:@"#E3E6E7"]];
            if (indexPath.section == 0) {
                cell = [tableView dequeueReusableCellWithIdentifier:kZP_DID_CHOOSE_PAYMENT_METHOD_CELL_ID];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                ((ZPDidChoosePaymentMethodCell*)cell).bankIcon.image = [ZPResourceManager getImageWithName:self.paymentMethodDidChoose.methodIcon];
                ((ZPDidChoosePaymentMethodCell*)cell).title.text = self.paymentMethodDidChoose.methodName;
                ZPBank *bank = [[ZPDataManager sharedInstance].bankManager getBankInfoWith:self.bankInfo.bankCode];
                ((ZPDidChoosePaymentMethodCell*)cell).bankName.text = bank.bankName ?: @"";
            } else if (indexPath.section == 1) {
                cell = [tableView dequeueReusableCellWithIdentifier:kZP_ATM_AUTHEN_TYPE_CELL_ID];
                DDLogInfo(@"method.savedCard.bankCode2: %@", self.bankInfo.bankCode);
                ((ZPAtmAuthenTypeViewCell *) cell).bankCode = self.bankInfo.bankCode;
            } else if (indexPath.section == 2) {
                return [self makeATemplateCell];
            }
            [cell setBackgroundColor:[UIColor whiteColor]];
            break;
        }
        case ZPIBankingPaymentStepGetStatusByAppTransIdForClient : {
            tableView.separatorStyle = UITableViewCellSelectionStyleNone;
            return [self createCellWithTableView:tableView];
        }
        case ZPIBankingPaymentStepAtmAuthenPayer: {
            if (indexPath.section == 0) {
                DDLogInfo(@"--cellForRowAtIndexPath: ZPAtmPaymentStepAtmAuthenPayer");
                cell = [tableView dequeueReusableCellWithIdentifier:kZP_ATM_OTP_CELL_ID];
                ((ZPOtpCell *) cell).txtOtpValue.attributedPlaceholder =
                        [[NSAttributedString alloc] initWithString:NSLocalizedString([[ZPDataManager sharedInstance] messageForKey:@"zalo_pay_otp_message"], @"")
                                                        attributes:@{}];
                [cell setBackgroundColor:[UIColor whiteColor]];
            } else if (indexPath.section == 1) {
                return [self makeATemplateCell];
            }

            break;
        }

        default:
            break;
    }
    [cell updateLayouts];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.delegate = self;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (self.currentStep) {
        case ZPIBankingPaymentStepWaitingWebview:
            return [UIScreen mainScreen].bounds.size.height - 64;
        case ZPIBankingPaymentStepSavedAccountConfirm:
        case ZPIBankingPaymentStepGetStatusByAppTransIdForClient:
        case ZPIBankingPaymentStepAtmAuthenPayer:
            return 60;
        default:
            break;
    }
    return 0;
}

// Hidden Accept Button
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == tableView.numberOfSections - 1) {
        if (self.currentStep == ZPIBankingPaymentStepSavedAccountConfirm) {
            return [self configFooter];
        }
    }
    if (section == tableView.numberOfSections - 1) {
        [self setFirstResponder];
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == tableView.numberOfSections - 1) {
        if (self.currentStep == ZPIBankingPaymentStepSavedAccountConfirm) {
            return 95;
        }
    }
    return 0;
}

- (void)setFirstResponder {
   
}

- (void)inputPinShowKeyboard {
}

- (NSBundle *)getBundle {
    return [ZPDataManager sharedInstance].bundle;
    // return [NSBundle mainBundle];
}

- (NSString *)okButtonBackgroundColor {
    if (self.currentStep == ZPIBankingPaymentStepSavedAccountConfirm || self.currentStep == ZPAtmPaymentStepAtmAuthenPayer) {
        return @"#06be04";
    }
    return @"#008FE5";
}

#pragma mark - ZPProgressHUD

- (void)setTimerTimeout:(int)value {
    DDLogInfo(@"SetTimerTimeout : %d ", value);
    if (checkWebViewTimeOutTimer) {
        [checkWebViewTimeOutTimer invalidate];
        checkWebViewTimeOutTimer = nil;
    }
    checkWebViewTimeOutTimer = [NSTimer scheduledTimerWithTimeInterval:value
                                                                target:self
                                                              selector:@selector(requestTimeout:)
                                                              userInfo:NULL repeats:NO];
}

- (void)requestTimeout:(NSTimer *)mtimer {
    DDLogInfo(@"Time out");
    if (checkWebViewTimeOutTimer) {
        [checkWebViewTimeOutTimer invalidate];
    }
    [self.myWebView stopLoading];
    [self webViewDidFailLoadWithTimeOut:self.myWebView];
}

- (void)showAlertNotifyRetryPin:(NSString *)errorMessage {
    [self dismissKeyboard];
    [self showAlertNotifyWith:errorMessage buttonTitles:@[[R string_ButtonLabel_Close]] handler:^(NSInteger idx) {
        [self showKeyboard];
    }];
}

- (void)showProgressHUD {
  
    [ZPProgressHUD showWithStatus:[[ZPDataManager sharedInstance]
            messageForKey:@"message_accounce_atm_in_processing"]];
    if (progressHUDTimer) {
        [progressHUDTimer invalidate];
    }
    progressHUDTimer = [NSTimer scheduledTimerWithTimeInterval:kATM_LONG_PROGESSHUD_TIME
                                                        target:self
                                                      selector:@selector(showLongProgressHUD)
                                                      userInfo:nil
                                                       repeats:NO];
    isLoading = YES;

}

- (void)hideProgressHUDWithResultCode:(int)code {
    if (code != kZPZaloPayCreditErrorCodePinRetry) {
        [self dismissPinView:code == ZALOPAY_ERRORCODE_SUCCESSFUL];
    }
    [ZPProgressHUD dismiss];
    [progressHUDTimer invalidate];
    isLoading = NO;
}

- (void)hideProgressHUD {
    [ZPProgressHUD dismiss];
    [progressHUDTimer invalidate];
    isLoading = NO;
}

- (void)showLongProgressHUD {
    [ZPProgressHUD dismiss];
    [progressHUDTimer invalidate];
    isLoading = NO;
    [self.myWebView stopLoading];
    if (self.currentStep == ZPIBankingPaymentStepWaitingWebview) {
        [self webView:self.myWebView didFailLoadWithError:[NSError new]];
        return;
    }
    self.currentStep = ZPAtmPaymentStepGetTransStatus;
    didReturned = YES;
    if (self.bankInfo.zpTransID.length > 0 && self.isBankLoadWebview) {
        [self getStatusOfTranxId:self.bankInfo.zpTransID callback:^(ZPPaymentResponse *response) {
        }];
    }

}

#pragma mark PaymentCell Delegate

- (void)paymentCellDidTouchAccept:(ZPPaymentViewCell *)cell {
    if (![[ZPDataManager sharedInstance] checkInternetConnect]) {
        [self showAlertNotifyNetWorkProblem];
        return;
    }
}

- (void)refreshCaptCha:(ZPPaymentViewCell *)cell {
    if (![[ZPDataManager sharedInstance] checkInternetConnect]) {
        [self showAlertNotifyNetWorkProblem];
        return;
    }
}

- (void)updateLayoutWhenInputText {
    self.mTableView.scrollEnabled = YES;
    CGPoint bottomOffset = CGPointMake(0, self.mTableView.contentSize.height - self.mTableView.frame.size.height + self.mTableView.contentInset.bottom);
    if (bottomOffset.y > -self.mTableView.contentInset.top) {
        [self.mTableView setContentOffset:bottomOffset animated:YES];
    }
}


#pragma WEBVIEW delegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSString *absoluteUrl = request.URL.absoluteString;
    DDLogInfo(@"should start load %@ main thread %@", absoluteUrl, [NSThread isMainThread] ? @"YES" : @"NO");
    if ([self checkOrderTimeout]) {
        return NO;
    }
    if (self.currentStep == ZPIBankingPaymentStepCancelWalletSubmitInfo) {
        return NO;
    }

    if ([absoluteUrl containsString:@"vcb-waiting-result.com"]) {
        self.currentStep = ZPIBankingPaymentStepOTP;
        [self VCB_HandleLinkAccount];
        return NO;
    }
    if ([self.jsHelper isCaptchaCallback:absoluteUrl]) {
        [self showCaptCharWithObject:request];
        return NO;
    }
    if ([self.jsHelper isIbankingError:absoluteUrl]) {
        [self hideProgressHUD];
        [self showIbankingErrorWith:request];
        return NO;
    }
    [self setTimerTimeout:zp_atm_handler_step_timeout];
    [arrRequested addObject:[request copy]];

    return YES;
}

- (void)showIbankingErrorWith:(NSURLRequest *)request {
    NSString *query = [request.URL.query stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *errorMsg = [query stringByReplacingOccurrencesOfString:@"msg=" withString:@""];
    NSString *errorURL = @"";
    if ([errorMsg containsString:@"urlerror="]) {
        NSArray *arrMess = [errorMsg componentsSeparatedByString:@"urlerror="];
        if (arrMess.count == 2) {
            errorMsg = arrMess[0];
            errorURL = arrMess[1];
        }

        NSMutableDictionary *response = [NSMutableDictionary dictionary];
        [response setObjectCheckNil:errorMsg forKey:@"error"];

        [ZPPaymentManager sendRequestErrorWithData:self.atmCard.zpTransID bankCode:self.atmCard.bankCode exInfo:[NSString stringWithFormat:@"%@|%@", [ZPResourceManager covertEnumStepToString:self.currentStep], errorMsg] apiPath:query params:[NSMutableDictionary dictionary] response:response appId:self.bill.appId];

    }
    if (errorMsg == nil || [errorMsg length] == 0) {
        errorMsg = [self.dataManager messageForKey:@"message_announce_fail"];
    }
    self.didReturned = YES;
    [self finishWithErrorCode:0 message:errorMsg
                       status:kZPZaloPayCreditStatusCodeFail result:-1 errorStep:ZPErrorStepNonRetry];
}

- (void)webViewDidStartLoad:(UIWebView *)webView {

}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self validateWebviewStatus:webView];
    if (checkWebViewTimeOutTimer) {
        [checkWebViewTimeOutTimer invalidate];
    }
    [self VCB_webViewDidFinishLoad:webView];

}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [self hideProgressHUD];
    switch (self.currentStep) {
        case ZPIBankingPaymentStepLogin:
            [self showBankWebConnectionError:webView];
            break;
        case ZPIBankingPaymentStepWaitingWebview:
            [self showGeneralErrorView:error];
            break;
        default:
            break;
    }
}

- (void)showBankWebConnectionError:(UIWebView *)webView {
    NSString *message = @"Không thể liên kết với ngân hàng. Bạn hãy thử lại, hoặc quay lại sau.";
    [self showAlertNotifyWith:message buttonTitles:@[[R string_ButtonLabel_Close], [R string_ButtonLabel_Retry]] handler:^(NSInteger idx) {
        if (idx == 0) {
            [self.delegate paymentControllerDidGoBack];
        } else {
            [webView reload];
        }
    }];
}

- (void)showGeneralErrorView:(NSError *)error {
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    self.placeholderView.frame = CGRectMake(0, 0, screenSize.width, screenSize.height);
    if (error.code == NSURLErrorNetworkConnectionLost || error.code == NSURLErrorNotConnectedToInternet) {
        [self.placeholderView setupViewNoNetwork];
    } else {
        [self.placeholderView setupViewErrorLoad];
    }
    self.placeholderView.hidden = NO;
}

- (void)webViewDidFailLoadWithTimeOut:(UIWebView *)awebView {
    DDLogInfo(@"webViewDidFailLoadWithTimeOut : %@", awebView.request.URL);
    [self hideProgressHUD];
    if (self.currentStep == ZPIBankingPaymentStepWaitingWebview) {
        return;
    }
    if (checkWebViewTimeOutTimer != NULL) {
        [checkWebViewTimeOutTimer invalidate];
        DDLogInfo(@"STOP TIMER");
    }
    if (didReturned) {
        return;
    }
    didReturned = true;
    ZPPaymentResponse *timeoutResponse = [ZPPaymentResponse timeOutRequestObject];
    timeoutResponse.errorStep = ZPErrorStepNonRetry;
    [self.myWebView stopLoading];
    timeoutResponse.zpTransID = self.bankInfo.zpTransID;
    [self handlePaymentResponse:timeoutResponse];
}

- (void)validateWebviewStatus:(UIWebView *)webView {
    NSCachedURLResponse *urlResponse = [[NSURLCache sharedURLCache] cachedResponseForRequest:webView.request];
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) urlResponse.response;
    NSInteger statusCode = httpResponse.statusCode;
    if (statusCode > 399) {
        [self hideProgressHUD];
        NSError *error = [NSError errorWithDomain:@"HTTP Error" code:httpResponse.statusCode userInfo:@{@"response": httpResponse}];


        NSMutableDictionary *response = [NSMutableDictionary dictionary];
        [response setObjectCheckNil:error.description forKey:@"error"];

        [ZPPaymentManager sendRequestErrorWithData:self.atmCard.zpTransID bankCode:self.atmCard.bankCode exInfo:[NSString stringWithFormat:@"%@|%@", [ZPResourceManager covertEnumStepToString:self.currentStep], error.description] apiPath:webView.request.URL.absoluteString params:[NSMutableDictionary dictionary] response:response appId:self.bill.appId];


        DDLogInfo(@"validateWebviewStatus %@", error);
        ZPPaymentResponse *timeoutResponse = [ZPPaymentResponse unknownExceptionResponseObject];
        timeoutResponse.errorStep = ZPErrorStepNonRetry;
        timeoutResponse.message = @"Kết nối đên ngân hàng bị lỗi, giao dịch bị hủy";
        timeoutResponse.zpTransID = self.bankInfo.zpTransID;
        [self handlePaymentResponse:timeoutResponse];
    }
}

- (void)getCaptChar {
    DDLogInfo(@"get captcha");
    [self.jsHelper iBanking_VCB_GetCaptchar];
}

#pragma mark webview Handle

- (void)VCB_webViewDidFinishLoad:(UIWebView *)webView {
    NSString *absoluteUrl = webView.request.URL.absoluteString;
    NSString *currentURL = [webView stringByEvaluatingJavaScriptFromString:@"window.location.href"];
    if (!absoluteUrl) {
        absoluteUrl = currentURL;
    }
    context = [webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    if (self.currentStep == ZPIBankingPaymentStepWaitingWebview) {
        [self hideProgressHUD];
        __weak ZPIBankingHandle *weakSelf = self;
        if ([absoluteUrl containsString:@"Account/Login"]) {
            context[@"objcLoginAction"] = ^(void) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSelf showProgressHUD];
                });
            };
            NSString *login =
                    @"var btnLogin = document.getElementsByClassName('btn-dangnhap')[0];"
                            "var listener = function() {"
                            "    objcLoginAction();"
                            "};"
                            "btnLogin.addEventListener('click', listener);";
            [context evaluateScript:login];
            return;
        }
        if ([absoluteUrl containsString:@"vidientu/dangkysudungmoi"] || [absoluteUrl containsString:@"vidientu/ngungsudung"]) {
            context[@"nativeListenSubmitOTP"] = ^(void) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSelf VCB_ListenWebSubmitOTP];
                });
            };
            NSString *submitOTP =
                    @"var btnConfirmOTP = document.getElementById('btnXacNhan2');"
                            "var listener = function() {"
                            "    nativeListenSubmitOTP();"
                            "};"
                            "btnConfirmOTP.addEventListener('click', listener);";
            [context evaluateScript:submitOTP];
            return;
        }

        ZPIBankingAccountBill *bill = [ZPIBankingAccountBill castFrom:self.bill];
        if (bill && bill.isMapAccount == false) {
            [self VCB_webViewGoToCancelWallet:webView];
            return;
        }
        [self VCB_webViewGoToLinkWallet:webView];
        return;
    }
    // Parse Web Handle
    if ([absoluteUrl containsString:@"Account/Login"]) {
        if (loginCount > 0) {
            [self VCB_SetUIWhenLoginFailed];
        }
        [self.jsHelper iBanking_VCB_RefreshCaptCha];
        [self listenRefreshCaptcha];
        [self getCaptChar];
        return;
    }
    if ([absoluteUrl containsString:@"vidientu/dangkysudungmoi"]) {
        __weak ZPIBankingHandle *weakSelf = self;
        context[@"endLoading"] = ^(void) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (weakSelf.currentStep == ZPIBankingPaymentStepLogin) {
                    [weakSelf VCB_LinkAccount];
                } else {
                    [weakSelf performSelector:@selector(VCB_LinkAccountStepProgress) withObject:nil afterDelay:0.15];
                }
            });
        };
        [self.jsHelper iBanking_VCB_RefreshCaptCha];
        [self listenRefreshCaptcha];
        return;
    } else if ([absoluteUrl containsString:@"vidientu/ngungsudung"]) {
        __weak ZPIBankingHandle *weakSelf = self;
        [_jsHelper iBanking_VCB_SelectZaloPay];
        self.currentStep = ZPIBankingPaymentStepCancelWalletSubmitInfo;
        context[@"endLoading"] = ^(void) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                weakSelf.listPhoneNumber = [weakSelf.jsHelper iBanking_VCB_GetListZaloPayID];
                if (weakSelf.listPhoneNumber.count > 0) {
                    [weakSelf.mTableView reloadData];
                    [weakSelf hideProgressHUD];
                }
            });
        };
        return;
    }

    ZPIBankingAccountBill *bill = [ZPIBankingAccountBill castFrom:self.bill];
    if (bill && bill.isMapAccount == false) {
        [self VCB_webViewGoToCancelWallet:webView];
        return;
    }
    [self VCB_webViewGoToLinkWallet:webView];
}

- (void)listenRefreshCaptcha {
    __weak ZPIBankingHandle *weakSelf = self;
    context[@"nativeListenRefreshCaptcha"] = ^(void) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf getCaptChar];
        });
    };
}

- (void)VCB_LinkAccount {
    [self hideProgressHUD];
    if (![self VCB_checkPhoneNumber]) {
        return;
    }
    self.currentStep = ZPIBankingPaymentStepLinkWalletSubmitInfo;
    [self.mTableView reloadData];
    [self getCaptChar];
}

- (BOOL)VCB_checkPhoneNumber {
    NSMutableArray *listPhone = [_jsHelper iBanking_VCB_GetListZaloPayID];
    NSString *myPhoneNumber = [NSString stringWithFormat:@"%@", [self.bill.appUserInfo objectForKey:@"phone"]];
    NSString *errorMsg = @"Số điện thoại Vietcombank %@ và số điện thoại đăng ký mật khẩu thanh toán ZaloPay %@ không giống nhau. Vui lòng kiểm tra lại.";
    if (listPhone.count > 0 && ![self checkPhoneNumber:listPhone[0] sameMyPhoneNumber:myPhoneNumber]) {
        myPhoneNumber = [NSString stringWithFormat:@"%@****%@", [myPhoneNumber substringToIndex:3], [myPhoneNumber substringFromIndex:(myPhoneNumber.length - 3)]];
        errorMsg = [NSString stringWithFormat:errorMsg, listPhone.firstObject, myPhoneNumber];
        [self finishWithErrorCode:0 message:errorMsg
                           status:kZPZaloPayCreditStatusCodeFail result:-1 errorStep:ZPErrorStepNonRetry];
        return NO;
    }
    return YES;
}

- (BOOL)checkPhoneNumber:(NSString *)phone sameMyPhoneNumber:(NSString *)myPhone {
    NSString *firtNo = [phone substringToIndex:3];
    NSString *lastNO = [phone substringFromIndex:(phone.length - 3)];
    if ([myPhone hasPrefix:firtNo] && [myPhone hasSuffix:lastNO]) {
        return YES;
    }
    return NO;
}

- (void)VCB_ListenWebSubmitOTP {
    if (checkLoadingCount > 100) {
        return;
    }
    checkLoadingCount++;
    NSString *checking = [self.jsHelper iBanking_VCB_Loading];
    if ([checking isEqualToString:@"loading"]) {
        [self performSelector:@selector(VCB_ListenWebSubmitOTP) withObject:nil afterDelay:0.15];
    } else {
        NSString *stepProgress = [self.jsHelper iBanking_VCB_Step];
        if ([stepProgress isEqualToString:@"vcb_result"]) {
            //thanh cong
            [self hideProgressHUD];
            NSString *successMsg = [[ZPDataManager sharedInstance] messageForKey:@"Ibanking_link_account_success_message"];
            [self finishWithErrorCode:1 message:successMsg
                               status:kZPZaloPayCreditStatusCodeSuccess result:1 errorStep:ZPErrorStepNonRetry];
        }
    }
}

- (void)VCB_HandleLinkAccount {
    __weak ZPIBankingHandle *weakSelf = self;
    context[@"endLoading"] = ^(void) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (0.15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [weakSelf hideProgressHUD];
            [weakSelf VCB_LinkAccountStepProgress];
        });
    };
}

- (void)VCB_LinkAccountStepProgress {
    if (didReturned) {
        return;
    }
    NSString *stepProgress = [self.jsHelper iBanking_VCB_Step];
    if ([stepProgress isEqualToString:@"vcb_otp"]) {
        [self hideProgressHUD];
        if (self.currentStep == ZPIBankingPaymentStepOTP) {
            // thất bại
            NSString *errorMsg = [self.jsHelper iBanking_VCB_GetErrorMessage];
            [self showAlertNotifyWith:errorMsg buttonTitles:@[[R string_ButtonLabel_Close], [R string_ButtonLabel_Retry]] handler:^(NSInteger idx) {
                if (idx == 0) {
                    [self finishWithErrorCode:0 message:errorMsg
                                       status:kZPZaloPayCreditStatusCodeFail result:-1 errorStep:ZPErrorStepNonRetry];
                    return;
                }
                [self showProgressHUD];
                self.currentStep = ZPIBankingPaymentStepLogin;
                [self VCB_webViewGoToLinkWallet:self.myWebView];
            }];
        } else {
            NSDictionary *data = @{@"bankcode": self.bankInfo.bankCode,
                    @"firstaccountno": [self.bankInfo.cardNumber substringToIndex:5],
                    @"lastaccountno": [self.bankInfo.cardNumber substringFromIndex:(self.bankInfo.cardNumber.length - 4)],
            };
            NSString *info = [data JSONRepresentation];
            @weakify(self);
            [[[ZaloPayWalletSDKPayment sharedInstance] submitMapAccount:info] subscribeNext:^(NSDictionary *dictionary) {
                @strongify(self);
                self.bankInfo.zpTransID = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"zptransid"]];
            }];
            self.currentStep = ZPIBankingPaymentStepOTP;
            [self.mTableView reloadData];
        }
    } else if ([stepProgress isEqualToString:@"vcb_result"]) {
        //thanh cong
        [self hideProgressHUD];
        NSString *successMsg = [[ZPDataManager sharedInstance] messageForKey:@"Ibanking_link_account_success_message"];
        [self finishWithErrorCode:1 message:successMsg
                           status:kZPZaloPayCreditStatusCodeSuccess result:1 errorStep:ZPErrorStepNonRetry];
    }
}

- (void)VCB_HandleUnLinkAccount {
    __weak ZPIBankingHandle *weakSelf = self;
    context[@"endLoading"] = ^(void) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [weakSelf hideProgressHUD];
            [weakSelf VCB_UnLinkAccountStepProgress];
        });
    };
}

- (void)VCB_UnLinkAccountStepProgress {
    if (didReturned) {
        return;
    }
    NSString *step = [self.jsHelper iBanking_VCB_UnlinkStep];
    if ([step isEqualToString:@"unlink_success"]) {
        NSString *successMsg = [[ZPDataManager sharedInstance] messageForKey:@"Ibanking_unlink_account_success_message"];
        successMsg = [NSString stringWithFormat:successMsg, self.bankInfo.bankName];
        [self finishWithErrorCode:1 message:successMsg
                           status:kZPZaloPayCreditStatusCodeSuccess result:1 errorStep:ZPErrorStepNonRetry];
        return;
    }
}

- (void)VCB_webViewGoToLinkWallet:(UIWebView *)webView {
    NSString *func = [self.jsHelper getJsFunc:@"zp_ibanking_VCB_goto_LinkWallet();" injectFile:@"zp_ibanking_VCB_goto_LinkWallet"];
    [webView stringByEvaluatingJavaScriptFromString:func];
}

- (void)VCB_webViewGoToCancelWallet:(UIWebView *)webView {
    NSString *func = [self.jsHelper getJsFunc:@"zp_ibanking_VCB_goto_cancelWallet();" injectFile:@"zp_ibanking_VCB_goto_cancelWallet"];
    [webView stringByEvaluatingJavaScriptFromString:func];
}

- (void)showCaptCharWithObject:(NSObject *)object {
    [self hideProgressHUD];
    if ([object isKindOfClass:[NSURLRequest class]]) {
        NSURLRequest *request = (NSURLRequest *) object;
        NSString *query = [request.URL.query stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *jsonString = [query stringByReplacingOccurrencesOfString:@"param=" withString:@""];
        NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:NULL];
        NSString *base64CaptchaImage = [dictionary objectForKey:@"captcha"];
        //  NSString * message = [dictionary objectForKey:@"message"];
        NSData *imageData = [NSData zpBase64DecodeString:base64CaptchaImage];
        ZPPaymentViewCell *cell = [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        if (imageData) {
            UIImage *imageCaptchar = [UIImage imageWithData:imageData];

            [cell setupUIWithData:imageCaptchar];
        } else {
            [cell setupUIWithData:[UIImage new]];
        }

    }
    [self hideProgressHUD];
}

- (void)VCB_SetUIWhenLoginFailed {
    NSString *msgError = [self.jsHelper iBanking_VCB_GetErrorMessage];
    if ([msgError containsString:@"Dịch vụ đã bị khóa"]) {
        [self finishWithErrorCode:0 message:msgError
                           status:kZPZaloPayCreditStatusCodeFail result:-1 errorStep:ZPErrorStepNonRetry];
    } else {
        NSString *message = @"Tên truy cập hoặc mật khẩu không chính xác.\nQuý khách lưu ý, dịch vụ bị tạm khóa nếu Quý khách nhập sai mật khẩu quá 5 lần.";

        [ZPDialogView showDialogWithType:DialogTypeNotification
                                 message:message
                       cancelButtonTitle:[R string_ButtonLabel_Close]
                        otherButtonTitle:nil
                          completeHandle:nil];
    }
}

- (void)keyboardWillHide:(NSNotification *)ntf {
    self.mTableView.scrollEnabled = NO;
    [self.mTableView setContentOffset:CGPointMake(0, -self.mTableView.contentInset.top) animated:YES];
}

- (long long)setLogTime:(long long)timeParam isUpdate:(BOOL)isUpdate {
    if (isUpdate) {
        return (long long) ([[NSDate date] timeIntervalSince1970] * 1000);
    }
    if (timeParam != 0) {
        return timeParam;
    }
    return (long long) ([[NSDate date] timeIntervalSince1970] * 1000);
}

#pragma mark Others

- (UITableViewCell *)makeATemplateCell {
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)ZPLinkAccountNotification:(NSNotification *)ntf {
    NSDictionary *data = ntf.object;
    int type = [data intForKey:@"type"];
    id <ZPWalletDependenceProtocol> helper = [ZaloPayWalletSDKPayment sharedInstance].appDependence;
    BOOL isMapAccount = [helper isNotificationMapAccount:type];
    BOOL isRemoveAccount = [helper isNotificationRemoveAccount:type];

    if (!isMapAccount && !isRemoveAccount) {
        return;
    }
    @weakify(self);
    [[ZPDataManager sharedInstance] updateListBankAccount:^(NSArray *listAccount, NSError *error) {
        @strongify(self);
        [self hideProgressHUD];
        NSString *bankCode = self.bill.bankCode;
        NSArray *allBank = listAccount;
        BOOL isExist = [self isBank:bankCode existInList:allBank];

        BOOL success = (isExist == true && isMapAccount) || // map thành công
                (isExist == false && isRemoveAccount); // xoá thành công
        if (success) {
            NSString *successMsg = isMapAccount ?
                    [[ZPDataManager sharedInstance] messageForKey:@"Ibanking_link_account_success_message"] :
                    [[ZPDataManager sharedInstance] messageForKey:@"Ibanking_unlink_account_success_message"];
            [self finishWithErrorCode:1
                              message:successMsg
                               status:kZPZaloPayCreditStatusCodeSuccess
                               result:1
                            errorStep:ZPErrorStepNonRetry];
            return;
        }

        NSString *failMsg = isMapAccount ?
                @"Liên kết ngân hàng thất bại." :
                @"Huỷ liên kết ngân hàng thất bại.";
        [self finishWithErrorCode:0
                          message:failMsg
                           status:kZPZaloPayCreditStatusCodeFail
                           result:-1
                        errorStep:ZPErrorStepNonRetry];
    }];
}


- (BOOL)isBank:(NSString *)bankCode existInList:(NSArray *)allBank {
    for (ZPSavedBankAccount *bank in allBank) {
        if ([bank.bankCode isEqualToString:bankCode]) {
            return true;
        }
    }
    return false;
}
@end
