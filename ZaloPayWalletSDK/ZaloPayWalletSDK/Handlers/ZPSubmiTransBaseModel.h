//
//  ZPSubmiTransBaseModel.h
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 7/2/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ZPBill;
@protocol RACSubscriber;
@interface ZPSubmiTransBaseModel : NSObject
+ (void)getSatusByAppTransId:(NSString *)appTransID
                       appId:(NSInteger )appId
                   zpTransId:(NSString *)zpTransId
                        bill:(ZPBill *)bill
                  subscriber:(id<RACSubscriber>)subscriber;
@end
