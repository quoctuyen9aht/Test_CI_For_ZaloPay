//
//  ZPWalletSubmitTransModel.m
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/18/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPWalletSubmitTransModel.h"
#import "ZPPaymentManager.h"
#import "ZPBill.h"
#import "ZPPaymentResponse.h"

@implementation ZPWalletSubmitTransModel

+ (RACSignal *)submiTransWithPassword:(NSString *)password
                                 bill:(ZPBill *)bill
                            channelId:(int)channelID {
    return [RACSignal createSignal:^RACDisposable *(id <RACSubscriber> subscriber) {
        [[ZPPaymentManager new] zaloPayWalletSubmitTransidWithBill:bill
                                                               pin:password
                                                         channelId:channelID
                                                          callback:^(ZPPaymentResponse *response) {
                                                              if (response.errorCode == kZPZaloPayCreditErrorCodeRequestTimeout) {
                                                                  [self getSatusByAppTransId:bill.appTransID
                                                                                       appId:bill.appId
                                                                                   zpTransId:response.zpTransID
                                                                                        bill:bill
                                                                                  subscriber:subscriber];
                                                                  return;
                                                              }
                                                              [subscriber sendNext:response];
                                                              [subscriber sendCompleted];
                                                          }];
        return nil;
    }];
}

@end
