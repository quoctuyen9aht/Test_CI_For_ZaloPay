//
//  ZPPaymentHandler.h
//  ZPSDK
//
//  Created by ZaloPayTeam on 12/22/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZPPaymentViewCell.h"
#import "ZPDefine.h"
@class ZPContainerViewController;

#define zp_atm_handler_step_timeout    60
@class ZPBill;
@class ZPPaymentResponse;
@class ZPPaymentMethod;
@class ZPDataManager;
@class ZPChannel;
@class ZPPaymentHandler;

@protocol ZPPaymentViewControllerDelegate <NSObject>

@required
- (void)paymentControllerDidFinishWithResponse:(ZPPaymentResponse *)response;
- (void)paymentControllerNotifyResultWithResponse:(ZPPaymentResponse *)response;

@optional
- (void)paymentControllerDidGoBack;
- (void)paymentMethodsControllerDidChooseMethod:(ZPPaymentMethod*)method;
- (void)paymentMethodsControllerDidChooseMethod:(ZPPaymentMethod*)method password:(NSString*)password;
- (void)paymentControllerNeedUpdateTitle;
- (void)paymentControllerDidClose:(NSString*)key data:(NSString*)data;
- (void)paymentControllerNotifyCloseWithKey:(NSString*)key data:(NSString*)data;
- (void)paymentControllerNeedUpdateLayouts;
- (void)paymentControllerSwitchToCreditCardHandler:(NSString*)data;
- (void)paymentControllerSwitchToAtmCardHandler:(NSString*)data;
- (void)paymentControllerSwitchToIBankingHandler:(ZPAtmCard*) bankInfo;
- (void)paymentControllerSwitchToBankHandler:(ZPBank*)bank;
- (void)paymentControllerNeedShowSupportAction;
- (void)paymentControllerDidChooseSupportWithData:(NSDictionary*)data;
- (void)paymentControllerDidChooseFAQ;
- (void)paymentControllerNeedRegisterWith:(ZPPaymentResponse *)response using:(id)cardInfo;
@end

@interface ZPPaymentHandler : NSObject <UITableViewDataSource, UITableViewDelegate, ZPPaymentCellDelegate>
@property (assign, nonatomic) ZPPaymentMethodType methodType;
@property (weak, nonatomic) id<ZPPaymentViewControllerDelegate> delegate;
@property (weak, nonatomic) UITableView * mTableView;
@property (strong, nonatomic) ZPDataManager * dataManager;
@property (strong, nonatomic) ZPBill * bill;
@property (weak, nonatomic) UIViewController * zpParentController;
@property (strong, nonatomic) NSArray * paymentMethods;
@property (strong, nonatomic) ZPChannel * channel;
@property (assign, nonatomic) BOOL isSubmitStrans;
@property (strong, nonatomic) ZPAtmCard* atmCard;
@property (nonatomic, assign) BOOL isBackFromCC;
@property (strong, nonatomic) ZPPaymentMethod *paymentMethodDidChoose;
@property (assign, nonatomic) NSInteger currentStep;
@property (strong, nonatomic) NSString * password;
@property (assign, nonatomic) BOOL isShowDialogMaintenance;
@property (assign, nonatomic) BOOL isShowAlertMessage;
@property (assign, nonatomic) BOOL isBankLoadWebview;


- (BOOL)isRequirePin;
- (void)displayOnTableView:(UITableView *)tableView fromController:(ZPContainerViewController *)controller;
- (void)dismissKeyboard;
- (void)viewWillBack;
- (void)viewWillClose;
- (void)initialization;

//! Empty virtual method
//! @callergraph
- (void)processPayment;
- (void)handlePaymentResponse:(ZPPaymentResponse *)response;
- (void)handlePaymentClose:(NSString*) notifyKey;
- (void)handlePaymentNotifyResult:(NSString*) notifyKey;
- (void)showKeyboard;
- (NSString *)okButtonBackgroundColor;
- (NSString *)paymentControllerTitle;
- (void)updateOKButtonColor;
- (NSString *)okButtonTitle;
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
- (UIView*)configFooter;
- (void)loadWebView:(UIWebView *)wv;

- (void)showAlertNotifyWith:(NSString *)message
          buttonTitles:(NSArray *)buttonTitles
                    handler:(void(^)(NSInteger idx))handler;

- (void)showAlertNotifyWith:(NSString *)message handler:(void(^)(NSInteger idx))handler;

- (BOOL)isAtmMapcard;
- (NSString *)getStringWithKey:(NSString *)key andDefaultString:(NSString *)defaultString;
- (UITableViewCell *)createCellAt:(NSIndexPath *)indexPath with:(UITableView *)tableView;
- (NSString*)checkMethodIsValid:(ZPPaymentMethod *)method;
- (UITableViewCell *)createCellWithTableView:(UITableView*)tableView;
- (void)showAlertNotifyRetryPin:(NSString *)errorMessage;
- (void)showAlertNotifyNetWorkProblem;
- (void)dismissPinView:(BOOL)success;
- (ZPPaymentMethod*)validateBank:(NSString *)bankCode;
- (void)saveAtmCard:(ZPAtmCard *)card;
- (void)logEventWith:(UIWebView *)wv;
@end
