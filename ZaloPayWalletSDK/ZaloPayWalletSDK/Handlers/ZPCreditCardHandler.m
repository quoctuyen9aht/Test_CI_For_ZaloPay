//
//  ZPCreditCardHandler.m
//  ZPSDK
//
//  Created by Nguyen Xuan Phung on 1/19/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPCreditCardHandler.h"
#import "ZPDataManager.h"
#import "ZPPaymentManager.h"
#import "ZPCreditCardResponseObject.h"
#import "ZPPaymentResponse.h"
#import "ZaloPayWalletSDKPayment.h"
#import "ZPProgressHUD.h"
#import "ZPBill.h"
//#import "ZPCreditCardCell.h"

#import "ZPResourceManager.h"
#import "ZPCreditCardWebviewCell.h"
#import "ZPDidChoosePaymentMethodCell.h"
#import "ZPAtmPinnedView.h"
#import "ZPBank.h"
#import "ZPCreditSaveCardCell.h"
#import "ZPManagerSaveCardCell.h"
#import "ZPCreditCardHandler+Vertify.h"
#import "ZaloPayWalletSDKLog.h"
#import "ZPBank.h"
#import "ZPMapCardLog.h"

//#define kZP_CREDIT_CARD_CELL_ID   @"credit-card-cell"
#define kZP_CREDIT_CARD_WEBVIEW_CELL_ID   @"credit-card-webview-cell"
#define kZP_DID_CHOOSE_PAYMENT_METHOD_CELL_ID   @"did-choose-payment-method-cell"

#define kATM_AUTO_DETECT_INFO_CELL_New_ID   @"ZPCreditSaveCardCell"
@interface ZPCreditCardHandler () <ZPRecordViewDelegate, UIWebViewDelegate>


@property(strong, nonatomic) ZPAtmPinnedView *atmPinnedView;
@property(strong, nonatomic) UIWebView *mWebView;
@property(strong, nonatomic) ZPCreditCardHandlerModel *model;
@property (assign, nonatomic) BOOL isCollectedKYC;

@end

@implementation ZPCreditCardHandler
@synthesize creditChannel;

#pragma mark - Overrides

- (void)dealloc {
    if (_mWebView) {
        [_mWebView stopLoading];
        [_mWebView setDelegate:nil];
        [_mWebView removeFromSuperview];
    }
    UITableViewCell *cell =
            [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    if ([cell isKindOfClass:[ZPPaymentViewCell class]]) {
        ZPPaymentViewCell *paymentCell = [ZPPaymentViewCell castFrom:cell];
        if (paymentCell && [paymentCell respondsToSelector:@selector(setDelegate:)]) {
            paymentCell.delegate = nil;
        }

        ZPCreditCardWebviewCell *paymentWebCell = [ZPCreditCardWebviewCell castFrom:paymentCell];
        if (paymentWebCell && paymentWebCell.ccWebview.delegate == self) {
            paymentWebCell.ccWebview.delegate = nil;
            paymentWebCell.ccWebview = nil;
        }
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)initialization {

    [self.mTableView registerNib:[self.dataManager nibNamed:@"ZPCreditCardWebviewCell"] forCellReuseIdentifier:kZP_CREDIT_CARD_WEBVIEW_CELL_ID];
    [self.mTableView registerNib:[self.dataManager nibNamed:@"ZPDidChooseMethodCell"] forCellReuseIdentifier:kZP_DID_CHOOSE_PAYMENT_METHOD_CELL_ID];
    [self.mTableView registerNib:[self.dataManager nibNamed:@"ZPCreditSaveCardCell"] forCellReuseIdentifier:kATM_AUTO_DETECT_INFO_CELL_New_ID];
    // self.mTableView.tableHeaderView = nil;


}

- (instancetype)init {
    self = [super init];
    if (self) {
        NSDictionary *dictionaryAgent = [[NSDictionary alloc] initWithObjectsAndKeys:
                @"Mozilla/5.0 (iPhone; CPU iPhone OS 9_3_2 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Mobile/13F69", @"UserAgent", nil];
        [[NSUserDefaults standardUserDefaults] registerDefaults:dictionaryAgent];
        self.model = [ZPCreditCardHandlerModel new];
        self.currentStep = ZPCcPaymentStepFillCardInfo;
        self.creditCard = [[ZPCreditCard alloc] init];
        self.bill.data = nil;
        self.model.ccOtpStartTime = 0;
        self.model.ccOtpEndTime = 0;
        self.model.ccStartTimestamp = 0;

    }
    return self;
}

- (UIWebView *)mWebView {
    if (!_mWebView) {
        CGRect bounds = self.zpParentController.view.bounds;
        _mWebView = [[UIWebView alloc] initWithFrame:CGRectMake(10, 50, bounds.size.width * 0.3, bounds.size.height - 60)];
        [self.zpParentController.view addSubview:_mWebView];
        [_mWebView setHidden:NO];
        [_mWebView setDelegate:self];
        _mWebView.scalesPageToFit = YES;
        [self.zpParentController.view bringSubviewToFront:_mWebView];
        _mWebView.tag = 55;
    }
    return _mWebView;
}

- (void)initWebView:(UIWebView *)wv {
    wv.delegate = self;
    wv.frame = self.mTableView.frame;
    wv.scrollView.scrollEnabled = YES;
    wv.contentMode = UIViewContentModeScaleAspectFit;
    wv.hidden = NO;
    wv.scalesPageToFit = YES;
    [self logEventWith:wv];
    [self loadWebView:wv];
    DDLogInfo(@"wv.contentMode = UIViewContentModeScaleAspectFit");
}

- (BOOL)okButtonEnabled {
    return self.model.isEnabledClickButton || self.currentStep == ZPCcPaymentStepSavedCardConfirm;
}


- (NSString *)paymentControllerTitle {

    return [self.dataManager getTitleByTranstype:self.bill.transType];
}


- (void)viewWillClose {
}

- (void)viewWillBack {
    if (self.currentStep == ZPCcPaymentStepFillCardInfo) {
        ZPPaymentViewCell *cell = (ZPPaymentViewCell *) [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
        [((ZPCreditSaveCardCell *) cell) viewWillBack];
    }

}

- (void)processPayment {
    [self dismissKeyboard];
    self.model.didReturned = NO;
    if ([self checkOrderTimeout]) {
        return;
    }
    if (![[ZPDataManager sharedInstance] checkInternetConnect]) {
        [self showAlertNotifyNetWorkProblem];
        return;
    }
    self.creditCard.step = self.currentStep;
    switch (self.currentStep) {
        case ZPCcPaymentStepFillCardInfo: {
            if (![self prepareForSubmitTran]) {
                return;
            }
            if ([self isAtmMapcard]) {
                [self verifyCardForMapping];
                return;
            }
            [self submitTran];
            break;
        }
        case ZPCcPaymentStepSavedCardConfirm: {
            DDLogInfo(@"processPayment, self.currentStep: ZPCcPaymentStepSavedCardConfirm");
            self.creditCard.step = ZPCcPaymentStepSubmitPurchase;
            [self submitTran];
            break;
        }
        case ZPCreditCardPaymentStepStepGetStatusByAppTransIdForClient: {
            self.creditCard.step = ZPCreditCardPaymentStepStepGetStatusByAppTransIdForClient;
            [self submitTran];
            break;
        }
        default:
            break;
    }
}

- (BOOL)prepareForSubmitTran {
    DDLogInfo(@"processPayment, self.currentStep: ZPCcPaymentStepFillCardInfo");
    [[ZPAppFactory sharedInstance].orderTracking trackUserInputCardInfo:self.bill.appTransID];
    ZPPaymentViewCell *cell = [ZPPaymentViewCell castFrom:[self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]]];
    if (![cell checkValidate]) {
        return NO;
    }
    NSDictionary *params = [cell outputParams];
    [self updateCCCardInfo:params];
    return YES;
}

- (void)updateCCCardInfo:(NSDictionary *)data {
    if (data == nil) {
        return;
    }
    self.creditCard.ccBankCode = stringCreditCardBankCode;
    self.creditCard.cardNumber = [data stringForKey:@"cardNumber"];
    self.creditCard.cardName = [data stringForKey:@"cardName"];
    self.creditCard.cardCVV = [data stringForKey:@"cardCVV"];
    self.creditCard.cardValidTo = [data stringForKey:@"cardDate"];
    self.creditCard.cardValidFrom = @"";
}

- (BOOL)checkOrderTimeout {
    //time out
    NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
    if (self.model.ccStartTimestamp > 0 && now - self.model.ccStartTimestamp > kATM_TRANSATION_TIMEOUT) {
        NSString *message = [self.dataManager messageForKey:@"message_atm_time_out"];
        [self finishWithErrorCode:0 message:message
                           status:kZPZaloPayCreditStatusCodeFail result:-1 errorStep:ZPErrorStepNonRetry];
        self.model.didReturned = YES;
        return YES;
    }
    return NO;
}

#pragma ZPPaymentCellDelegate

- (void)validateInputDoneAndCallProcessPayment {
    [self dismissKeyboard];
    [self processPayment];
}

- (void)validateInputCompleted:(BOOL)isValid {
    self.model.isEnabledClickButton = isValid;
    [self updateOKButtonColor];
}

#pragma mark - vertify card for mapping

- (void)verifyCardForMapping {
    id<ZPWalletDependenceProtocol> dependency = [ZaloPayWalletSDKPayment sharedInstance].appDependence;
    if (!dependency || ![dependency kycEnableMapCard]) {
        // Run normal
        [self internalMapCard];
        return;
    }
    
    if (![[ZPDataManager sharedInstance] checkInternetConnect]) {
        [self showAlertNotifyNetWorkProblem];
        return;
    }
    
    if (self.isCollectedKYC) {
        [self internalMapCard];
        return;
    }
    
    NSString *bankNumber = self.creditCard.cardNumber ?: @"";
    NSString *f6No;
    if ([bankNumber length] > 6) {
        f6No = [bankNumber substringToIndex:6];
    }
    NSString *detectedBankCode = [ZPResourceManager getCcBankCodeFromConfig:f6No ?: @""] ?: @"";
    NSString *bankIconName = [ZPResourceManager bankIconFromBankCode:detectedBankCode] ?: @"";
    UIImage *bankImage = [ZPResourceManager getImageWithName:bankIconName];
    
    NSDictionary *bankDict = [[ZPDataManager sharedInstance].bankManager.ccBanksDict objectForKey:detectedBankCode];
    NSMutableDictionary *infor = [NSMutableDictionary new];
    NSString * bankName = [bankDict stringForKey:stringKeyCCBankName] ?: @"";
    
    [infor addEntriesFromDictionary:@{@"bankName" : bankName}];
    [infor addEntriesFromDictionary:@{@"bankNumber": bankNumber}];
    if (bankImage) {
        [infor addEntriesFromDictionary:@{@"image": bankImage}];
    }
    
    @weakify(self);
    [[[[ZaloPayWalletSDKPayment sharedInstance] runKYCFlow:self.zpParentController title:[R string_UpdateProfileLevel2_Title] infor:infor type:ZPKYCTypeMapCard] deliverOnMainThread] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        NSDictionary *dic = [NSDictionary castFrom:x];
        [self.creditCard setKYC:dic];
        if (![[ZPDataManager sharedInstance] checkInternetConnect]) {
            [self showAlertNotifyNetWorkProblem];
            return;
        }
        self.isCollectedKYC = YES;
        [self.zpParentController.navigationController popViewControllerAnimated:NO];
    } error:^(NSError * _Nullable error) {
        @strongify(self);
        [self.zpParentController.navigationController popViewControllerAnimated:NO];
        if (error.code == NSURLErrorCancelled) {
            if ([self.zpParentController conformsToProtocol:@protocol(KYCFlowDelegateProtocol)]) {
                [self.zpParentController performSelector:@selector(KYCFlowDidCancel)];
            }
            return;
        }
        [ZPDialogView showDialogWithError:error handle:nil];
    } completed:^{
        @strongify(self);
        [self internalMapCard];
    }];
}

- (void)internalMapCard {
    self.isSubmitStrans = YES;
    [self verifyCardForMappingUsing:self.model];
}

- (void)submitTran {
    self.isSubmitStrans = YES;
    [self submitTransUsing:self.model];
}

- (void)getStatusOfTranxId:(NSString *)tranxId
                  callback:(void (^)(ZPPaymentResponse *response))callback {
    DDLogInfo(@"start getStatusOfTranxId: %@", tranxId);
    [self showProgressHUD];
    if (!self.creditCard) {
        [self hideProgressHUD];
        return;
    }

    self.creditCard.step = self.currentStep;
    [ZPPaymentManager paymentWithBill:self.bill
                       andPaymentInfo:self.creditCard
                             andAppId:self.bill.appId
                              channel:self.channel
                               inMode:ZPPaymentMethodTypeCreditCard
                          andCallback:^(ZPResponseObject *responseObject) {
                              // process callback
                              [self hideProgressHUD];

                              ZPPaymentResponse *response = (ZPPaymentResponse *) responseObject;
                              response.paymentMethod = ZPPaymentMethodTypeCreditCard;
                              response.zpTransID = tranxId;
                              
                              //Set 6 first number card
                              NSString *first6No = self.creditCard.cardNumber.length > 6 ? [self.creditCard.cardNumber substringToIndex:6]: @"";
                              response.first6NumberCard = first6No;
                              
                              [self handlePaymentResponse:response];
                              [self writeLogTimeToServer];
                          }];
}

- (void)showProgressHUD {
    if (self.currentStep == ZPCcPaymentStepSavedCardConfirm) {
        return;
    }

    [ZPProgressHUD showWithStatus:[[ZPDataManager sharedInstance]
            messageForKey:@"message_accounce_atm_in_processing"]];
    if (self.model.progressHUDTimer) {
        [self.model.progressHUDTimer invalidate];
    }
    self.model.progressHUDTimer = [NSTimer scheduledTimerWithTimeInterval:kATM_LONG_PROGESSHUD_TIME
                                                                   target:self
                                                                 selector:@selector(showLongProgressHUD)
                                                                 userInfo:nil
                                                                  repeats:NO];

}

- (void)hideProgressHUDWithResultCode:(int)code {
    if (code != kZPZaloPayCreditErrorCodePinRetry) {
        [self dismissPinView:code == ZALOPAY_ERRORCODE_SUCCESSFUL];
    }
    [ZPProgressHUD dismiss];
    [self.model.progressHUDTimer invalidate];
}

- (void)hideProgressHUD {
    [ZPProgressHUD dismiss];
    [self.model.progressHUDTimer invalidate];
}

- (void)showLongProgressHUD {
    [ZPProgressHUD showWithStatus:[[ZPDataManager sharedInstance]
            messageForKey:@"message_accounce_atm_after_15s_processing"]];
    if (self.model.progressHUDTimer) {
        [self.model.progressHUDTimer invalidate];
    }
    self.model.progressHUDTimer = [NSTimer scheduledTimerWithTimeInterval:kATM_LONG_PROGESSHUD_TIME
                                                                   target:self
                                                                 selector:@selector(hideProgressHUD)
                                                                 userInfo:nil
                                                                  repeats:NO];
}


- (ZPCreditCard *)getSavedCreditCard {
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[ZPUserDefaults dictionaryForKey:ZP_CREDIT_CACHED_CARD_KEY]];
    NSData *data = [dict objectForKey:kZP_CREDIT_CACHED_CARD_KEY];
    if (data) {
        ZPCreditCard *card = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        [dict removeObjectForKey:kZP_CREDIT_CACHED_CARD_KEY];
        [ZPUserDefaults setObject:dict forKey:ZP_CREDIT_CACHED_CARD_KEY];
        [ZPUserDefaults synchronize];
        return card;
    }

    return nil;
}

- (NSString *)okButtonBackgroundColor {
    if (self.isRequirePin || self.currentStep == ZPCcPaymentStepSavedCardConfirm) {
        return @"#06be04";
    } else if (self.model.isEnabledClickButton) {
        return @"#008FE5";
    }
    return @"#C7C7CC";
}

#pragma mark - ZPPaymentViewControllerDelegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    ZPPaymentViewCell *cell;

    if (self.currentStep != ZPCcPaymentStepFillCardInfo) {
        // Hide btn PreNext On ZPATMSaveCardCell
        [[NSNotificationCenter defaultCenter] postNotificationName:ZPUIButtonPreNextDidHideNotification object:nil];
    }

    switch (self.currentStep) {
        case ZPCcPaymentStepFillCardInfo: {
            if (indexPath.section == 0) {
                cell = (ZPPaymentViewCell *) [self.dataManager viewWithNibName:kATM_AUTO_DETECT_INFO_CELL_New_ID owner:self];
                if (self.creditCard.cardNumber != nil) {
                    // Hide btn PreNext On ZPATMSaveCardCell
                    [[NSNotificationCenter defaultCenter] postNotificationName:ZPUIButtonPreNextDidHideNotification object:nil];

                    [((ZPCreditSaveCardCell *) cell) setCreditCardInfo:self.creditCard];
                }
                ((ZPCreditSaveCardCell *) cell).bill = self.bill;
                ((ZPCreditSaveCardCell *) cell).mTableView = self.mTableView;
                ((ZPCreditSaveCardCell *) cell).zpParentController = self.zpParentController;

                [ZPManagerSaveCardCell sharedInstance].isShowCreditCardCell = true;
                [ZPManagerSaveCardCell sharedInstance].isShowAMTCardCell = false;

                [cell setBackgroundColor:[UIColor defaultBackground]];

            } else if (indexPath.section == 1) {
                UITableViewCell *cell = [[UITableViewCell alloc] init];
                [cell setBackgroundColor:[UIColor clearColor]];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
            break;
        }
        case ZPCcPaymentStepWaitingWebview: {
            DDLogInfo(@"--cellForRowAtIndexPath: ZPCcPaymentStepWaitingWebview");
            cell = (ZPPaymentViewCell *) [tableView dequeueReusableCellWithIdentifier:kZP_CREDIT_CARD_WEBVIEW_CELL_ID];
            if (self.model.webviewUrl != nil && ![self.model.webviewUrl isEqualToString:@""]) {
                [self initWebView:((ZPCreditCardWebviewCell *) cell).ccWebview];
                [self loadRequestWithStringUrl:((ZPCreditCardWebviewCell *) cell).ccWebview :self.model.webviewUrl];
                self.model.webviewUrl = nil;
            }
            [cell setBackgroundColor:[UIColor whiteColor]];
            return cell;
            break;
        }
        case ZPCcPaymentStepSavedCardConfirm: {
            DDLogInfo(@"--cellForRowAtIndexPath: ZPCcPaymentStepSavedCardConfirm");
            tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
            cell = [tableView dequeueReusableCellWithIdentifier:kZP_DID_CHOOSE_PAYMENT_METHOD_CELL_ID];
            ((ZPDidChoosePaymentMethodCell *) cell).bankIcon.image = [ZPResourceManager getImageWithName:self.paymentMethodDidChoose.methodIcon];
            ((ZPDidChoosePaymentMethodCell *) cell).title.text = self.paymentMethodDidChoose.methodName;
            NSString *detectedBankCode = [ZPResourceManager getCcBankCodeFromConfig:self.first6No];
            self.creditCard.ccBankCode = detectedBankCode;
            NSDictionary * bank = [self.dataManager.bankManager.ccBanksDict objectForKey:self.creditCard.ccBankCode];
            ((ZPDidChoosePaymentMethodCell*)cell).bankName.text = [bank objectForKey:@"bank_name"] != nil ? [bank objectForKey:@"bank_name"] : @"";
            [cell setBackgroundColor:[UIColor whiteColor]];
            break;
        }
        case ZPCreditCardPaymentStepStepGetStatusByAppTransIdForClient : {
            tableView.separatorStyle = UITableViewCellSelectionStyleNone;
            return [self createCellWithTableView:tableView];
        }
        default:
            cell = [ZPPaymentViewCell new];
            break;
    }

    [cell updateLayouts];
    cell.delegate = self;
    return cell;
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell isKindOfClass:[ZPCreditCardWebviewCell class]]) {
        ((ZPCreditCardWebviewCell *) cell).ccWebview.delegate = nil;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.currentStep == ZPCcPaymentStepFillCardInfo) {
        if (indexPath.section == 0) {
            if (IS_IPHONE_4_OR_LESS) {

                return (SCREEN_WIDTH - kMarginCardView) * (1 / (ratioCardView + 1)) + cardViewScrollHeight - 10;
            }

            CGFloat heightIPX = 0;
            if (@available(iOS 11.0, *)) {
                heightIPX = UIApplication.sharedApplication.keyWindow.safeAreaInsets.top / 2;
            }
            //Add
            return (SCREEN_WIDTH - kMarginCardView) * (1 / ratioCardView) + cardViewScrollHeight + (IS_IPHONE_X ? heightIPX : 0);

        } else {
            return 1;
        }

    } else if (self.currentStep == ZPCcPaymentStepSavedCardConfirm || self.currentStep == ZPCreditCardPaymentStepStepGetStatusByAppTransIdForClient) {
        return 60;
    } else {
        CGSize size = [UIScreen mainScreen].bounds.size;
        DDLogInfo(@"table height: %f", size.height);
        return size.height - 64;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.currentStep == ZPCcPaymentStepFillCardInfo  ? 2 : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


// Hidden Accept Button
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == tableView.numberOfSections - 1) {
        if (self.currentStep == ZPCcPaymentStepWaitingWebview
                || self.currentStep == ZPCreditCardPaymentStepStepGetStatusByAppTransIdForClient
                || self.bill.transType == ZPTransTypeAtmMapCard
                || self.currentStep == ZPCcPaymentStepFillCardInfo) {
            return nil;
        } else {
            return [self configFooter];
        }
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == tableView.numberOfSections - 1) {
        if (self.currentStep == ZPCcPaymentStepWaitingWebview
                || self.currentStep == ZPCreditCardPaymentStepStepGetStatusByAppTransIdForClient
                || self.bill.transType == ZPTransTypeAtmMapCard
                || self.currentStep == ZPCcPaymentStepFillCardInfo) {
            return 0;
        } else {
            return 95;
        }
    }
    return 0;
}

#pragma mark - ZPAPaymentViewCellDelegate


- (void)closeKeyBoard {
    self.model.isShowDialogMaintenance = YES;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self dismissKeyboard];
    });
}

- (BOOL)handlerWithDetectBank:(ZPBank *)bank
                        using:(void(^)(ZPCreditCardAlertType type))config
{
    if (bank.bankCode.length > 0) {
        // Will Show Alert
        return [self alertWithDetectBank:bank with:self.model excuteActionFirst:^{
            [self closeKeyBoard];
        }                        handler:^(NSInteger idx, ZPCreditCardAlertType type) {
            if (config) {
                config(type);
            }
        }];
    } else {
        self.creditCard.ccBankCode = nil;
    }
    return NO;
}

- (void)clearCCCardNumber {
    UITableViewCell *cell = [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    if ([cell isKindOfClass:[ZPCreditSaveCardCell class]]) {
        ZPCreditSaveCardCell *cellSaveCard = (ZPCreditSaveCardCell *) cell;
        JVFloatLabeledTextField *tf = [cellSaveCard getCurrentTf];
        tf.text = @"";
        tf.floatingLabel.text = stringInputCardNumber;
        [cellSaveCard resetAll];
    }
    [self showKeyboard];

}

- (void)showDialogWithTypeWith:(NSString *)errorMessage {
    [ZPDialogView showDialogWithType:DialogTypeNotification
                             message:errorMessage
                   cancelButtonTitle:[R string_ButtonLabel_Close]
                    otherButtonTitle:nil completeHandle:^(int buttonIndex, int cancelButtonIndex) {
                [self clearCCCardNumber];
            }];
}
- (void)detectCell:(ZPPaymentViewCell *)cell didDetectBank:(ZPBank *)bank
{
    NSString *cardNumber = bank.detectString;
    if (self.bill.errorCCCanNotMapWhenPay.length > 0 && bank.detectString.length > 0){
        [self showDialogWithTypeWith:self.bill.errorCCCanNotMapWhenPay];
        return;
    }

    NSString *errorMessage = [[ZPDataManager sharedInstance] checkMaxCCSavedCard];
    if (bank && errorMessage.length > 0) {
        [self showDialogWithTypeWith:errorMessage];
        return;
    }
    BOOL isCCDebit = [[ZaloPayWalletSDKPayment sharedInstance].appDependence IsCCDebit:cardNumber];
    if (self.bill.unsuportCCCredit && !isCCDebit) {
        return;
    }
    if (bank && ![[ZaloPayWalletSDKPayment sharedInstance].appDependence checkCCBinIsSupport:cardNumber]) {
        [self showDialogWithTypeWith:stringMessageCCCardUnSupport];
        return;
    }
    // If show alert stop process
    if ([self handlerWithDetectBank:bank using:^(ZPCreditCardAlertType type) {
             [self showKeyboard];
             self.model.isShowDialogMaintenance = NO;
             if (type == ZPCreditCardAlertTypeMinAppVersion) {
                 [self clearCCCardNumber];
             }
         }])
    {
        return;
    }

    if (self.delegate && [self.delegate respondsToSelector:@selector(paymentControllerNeedUpdateTitle)]) {
        [self.delegate paymentControllerNeedUpdateTitle];
        //self.creditCard.ccBankCode = ((ZPCreditCardCell *)cell).ccBankCode;
        //Add SDK
        if ([cell isKindOfClass:[ZPCreditSaveCardCell class]]) {
            self.creditCard.ccBankCode = ((ZPCreditSaveCardCell *)cell).bankCode;
            self.creditCard.ccBankName = bank.bankName;
            self.creditCard.requireOTP = bank.requireOTP ;
        }
    }
}

- (void)detectCell:(ZPPaymentViewCell *)cell didEditCardNumberWithDetectedBank:(ZPBank *)bank {
    if (!bank) {
        [self dismissKeyboard];
        [self showAtmPinView];
    }
}

- (void)showAtmPinView {
    [self dismissKeyboard];
    if (!self.atmPinnedView) {
        self.atmPinnedView = [[ZPAtmPinnedView alloc] init];
    }
    self.atmPinnedView.delegate = self;
    if(self.bill.transType == ZPTransTypeAtmMapCard) {
        [self.atmPinnedView showInView:self.zpParentController.view withData:[self.dataManager.bankManager getArrayBankInPinViewWithTranstype:self.bill.transType]];
    } else {
        NSArray *listBankSupport = [[ZPDataManager sharedInstance].bankManager listCCBankSupport];
        [self.atmPinnedView showInView:self.zpParentController.view withData:listBankSupport];
    }
}

- (BOOL)isAtmMapcard {
    DDLogInfo(@"self.bill.transType: %ld", (long) self.bill.transType);
    return self.bill.transType == ZPTransTypeAtmMapCard;
}

- (void)detectAtmCard:(ZPPaymentViewCell *)cell didDetectBank:(ZPBank *)bank {
    DDLogInfo(@"detectAtmCard: %@", bank);
    DDLogInfo(@"alertView: form redirect to atm card");
    ZPPaymentViewCell *cellDetect = (ZPPaymentViewCell *)
            [self.mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    NSDictionary *params = [cellDetect outputParams];
    DDLogInfo(@"card number: %@", params[@"cardNumber"]);
    if (self.delegate && [self.delegate respondsToSelector:@selector(paymentControllerSwitchToAtmCardHandler:)]) {
        [self.delegate paymentControllerSwitchToAtmCardHandler:params[@"cardNumber"]];
    }
}

- (void)pinnedCell:(ZPPaymentViewCell *)cell didSelectBank:(ZPBank *)bank {
    [self showKeyboard];
}

- (void)finishWithErrorCode:(int)errorCode
                    message:(NSString *)message
                     status:(int)status
                     result:(int)result
                  errorStep:(enum ZPErrorStep)errorStep {
    [self hideProgressHUD];
    ZPPaymentResponse *pResponse = [[ZPPaymentResponse alloc] init];
    pResponse.errorCode = errorCode;
    pResponse.originalCode = errorCode;
    pResponse.message = message;
    pResponse.exchangeStatus = status;
    pResponse.errorStep = errorStep;
    pResponse.zpTransID = self.creditCard.zpTransID;
    [self handlePaymentResponse:pResponse];
    [self writeLogTimeToServer];
}

#pragma mark - ZPRecordViewDelegate

- (void)paymentCell:(ZPPaymentViewCell *)cell didReturnData:(id)data {
    /*creditChannel = (ZPChannel *)data;
     [self createOrder:creditChannel];*/
}


#pragma mark - UIWebViewDelgate

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    DDLogInfo(@"didFailLoadWithError : %@", webView.request.URL);
    DDLogInfo(@"didFailLoadWithError : %@", error.description);

    if (self.model.didReturned) {
        return;
    }
    [[ZPAppFactory sharedInstance].orderTracking trackWebLogin:self.bill.appTransID result:OrderStepResult_Fail];

    if ([self isAuthenLoadFail:webView.request.URL.absoluteString]) {
        return;
    }

    //ssl error
    NSMutableDictionary *response = [NSMutableDictionary dictionary];
    [response setObjectCheckNil:error.description forKey:@"error"];
    [response setObjectCheckNil:[NSString stringWithFormat:@"%ld", (long) error.code] forKey:@"errorCode"];

    [ZPPaymentManager sendRequestErrorWithData:self.creditCard.zpTransID bankCode:self.creditCard.ccBankCode exInfo:[NSString stringWithFormat:@"%@|%@", [ZPResourceManager covertEnumStepToString:self.currentStep], error.description] apiPath:webView.request.URL.absoluteString params:[NSMutableDictionary dictionary] response:response appId:self.bill.appId];

    for (int i = -1206; i <= -1200; i++) {
        if (error.code == i || error.code == -2000) {
            self.model.didReturned = true;
            [webView stopLoading];
            [self getStatusOfTranxId:self.creditCard.zpTransID callback:^(ZPPaymentResponse *response) {
            }];
            break;
        }
    }
}

- (void)webViewDidFailLoadWithTimeOut:(UIWebView *)awebView {
    DDLogInfo(@"webViewDidFailLoadWithTimeOut : %@", awebView.request.URL);

    [self hideProgressHUD];
    if (self.model.didReturned) {
        return;
    }
    [[ZPAppFactory sharedInstance].orderTracking trackWebLogin:self.bill.appTransID result:OrderStepResult_Fail];
    self.model.didReturned = true;
    ZPPaymentResponse *timeoutResponse = [ZPPaymentResponse timeOutRequestObject];
    timeoutResponse.errorStep = ZPErrorStepNonRetry;
    [awebView stopLoading];
    timeoutResponse.zpTransID = self.creditCard.zpTransID;
    [self handlePaymentResponse:timeoutResponse];
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    DDLogInfo(@"webViewDidStartLoad %@", webView.request.URL);

    NSString *js = @"var alert = function();";
    [webView stringByEvaluatingJavaScriptFromString:js];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    [[ZPAppFactory sharedInstance].orderTracking trackWebLogin:self.bill.appTransID result:OrderStepResult_Success];

    NSString *absoluteUrl = request.URL.absoluteString;
    DDLogInfo(@"should start load %@ main thread %@", absoluteUrl, [NSThread isMainThread] ? @"YES" : @"NO");
    if ([self checkOrderTimeout]) {
        return NO;
    }

    if ([self is123PResult:absoluteUrl] || [[request.URL host] isEqualToString:@"payment-complete"]) {
        DDLogInfo(@"shouldStartLoadWithRequest: is123PResult");
        self.currentStep = ZPCcPaymentStepUpdate;
        self.model.didReturned = YES;
        [webView stopLoading];
        [self hideProgressHUD];
        self.model.ccOtpEndTime = [self setLogTime:self.model.ccOtpEndTime isUpdate:YES];
        [self getStatusOfTranxId:self.creditCard.zpTransID callback:^(ZPPaymentResponse *response) {
            //[self handleReturnResponse:response];
        }];

        return NO;
    }
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    DDLogInfo(@"webViewDidFinishLoad : %@", webView.request.URL);
    [self hideProgressHUD];
    webView.contentMode = UIViewContentModeScaleAspectFill;
    [self validateWebviewStatus:webView];
}


- (void)validateWebviewStatus:(UIWebView *)webView {
    NSCachedURLResponse *urlResponse = [[NSURLCache sharedURLCache] cachedResponseForRequest:webView.request];
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) urlResponse.response;
    NSInteger statusCode = httpResponse.statusCode;
    if (statusCode > 399) {
        [self hideProgressHUD];
        NSError *error = [NSError errorWithDomain:@"HTTP Error" code:httpResponse.statusCode userInfo:@{@"response": httpResponse}];

        NSMutableDictionary *response = [NSMutableDictionary dictionary];
        [response setObjectCheckNil:error.description forKey:@"error"];
        [response setObjectCheckNil:[NSString stringWithFormat:@"%ld", (long) error.code] forKey:@"errorCode"];

        [ZPPaymentManager sendRequestErrorWithData:self.creditCard.zpTransID bankCode:self.creditCard.ccBankCode exInfo:[NSString stringWithFormat:@"%@|%@", [ZPResourceManager covertEnumStepToString:self.currentStep], error.description] apiPath:webView.request.URL.absoluteString params:[NSMutableDictionary dictionary] response:response appId:self.bill.appId];


        ZPPaymentResponse *timeoutResponse = [ZPPaymentResponse unknownExceptionResponseObject];
        timeoutResponse.errorStep = ZPErrorStepNonRetry;
        timeoutResponse.originalCode = -1;
        timeoutResponse.message = @"Ngân hàng đang có lỗi, vui lòng thử lại sau";
        timeoutResponse.zpTransID = self.creditCard.zpTransID;
        [self handlePaymentResponse:timeoutResponse];
    }
}

- (void)loadRequestWithStringUrl:(UIWebView *)webView :(NSString *)stringUrl {
    NSURL *url = [NSURL URLWithString:stringUrl];
    DDLogInfo(@"url = %@", url);
    NSURLRequest *request = [NSURLRequest requestWithURL:url
                                             cachePolicy:NSURLRequestUseProtocolCachePolicy
                                         timeoutInterval:120];
    [webView loadRequest:request];
    self.model.ccOtpStartTime = [self setLogTime:self.model.ccOtpStartTime isUpdate:YES];
    [self showProgressHUD];

}

- (BOOL)is123PResult:(NSString *)url {
    //http:://pay.sandbox.123pay.vn/ib/ocp/index.php/ocp/pay/payresultsucc?orderNo=123P1605070179404
    NSString *exp = @"(.*123pay\\.vn.*\\/ocp\\/pay\\/payresult.*)";
    return [self checkRegularExpression:exp url:url];
}

- (BOOL)isAuthenLoadFail:(NSString *)url {
    //http:://pay.sandbox.123pay.vn/ib/cc/index.php/index/authen
    NSString *exp = @"(.*123pay\\.vn.*cc.*\\/index\\/authen.*)";
    return [self checkRegularExpression:exp url:url];
}

- (BOOL)checkRegularExpression:(NSString *)exp url:(NSString *)url {
    NSRegularExpression *regExOtp = [[NSRegularExpression alloc]
            initWithPattern:exp
                    options:NSRegularExpressionCaseInsensitive error:NULL];

    NSArray *matchArray = [regExOtp matchesInString:url options:0 range:NSMakeRange(0, url.length)];
    return (matchArray != NULL && matchArray.count > 0);
}

- (void)btnOkClicked {

    [self processPayment];
}

//
- (void)writeLogTimeToServer {
    ZPMapCardLog *log = [ZPMapCardLog new];
    log.transid = self.creditCard.zpTransID;
    log.pmcid = @(self.channel.channelID);
    log.atmotp_begindate = @(self.model.ccOtpStartTime);
    log.atmotp_enddate = @(self.model.ccOtpEndTime);
    [ZPPaymentManager writeMapCardLog:log];
}

- (long long)setLogTime:(long long)timeParam isUpdate:(BOOL)isUpdate {
    if (isUpdate) {
        return (long long) ([[NSDate date] timeIntervalSince1970] * 1000);
    }
    if (timeParam != 0) {
        return timeParam;
    }
    return (long long) ([[NSDate date] timeIntervalSince1970] * 1000);
}

@end
