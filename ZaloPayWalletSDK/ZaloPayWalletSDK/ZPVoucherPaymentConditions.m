//
//  VoucherPaymentConditions.m
//  ZaloPayWalletSDK
//
//  Created by Phuoc Huynh on 1/3/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import "ZPVoucherPaymentConditions.h"

@implementation ZPVoucherPaymentConditions

- (instancetype)initDataWithDict:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        self.useconditionapplist = [dict arrayForKey:@"useconditionapplist" defaultValue:@[]];
        self.useconditionpmcs = [dict arrayForKey:@"useconditionpmcs" defaultValue:@[]];
        self.useconditionbankcodes = [dict arrayForKey:@"useconditionbankcodes" defaultValue:@[]];
        self.useconditionccbankcodes = [dict arrayForKey:@"useconditionccbankcodes" defaultValue:@[]];
        self.currentCCBankcode = @"";
        self.currentBankcode = @"";
        self.currentPMCID = -1;
    }
    return self;
    
}
- (instancetype)initDataWithPromotionDict:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        self.useconditionpmcs = [dict arrayForKey:@"pmcs" defaultValue:@[]];
        self.useconditionbankcodes = [dict arrayForKey:@"bankcodes" defaultValue:@[]];
        self.useconditionccbankcodes = [dict arrayForKey:@"ccbankcodes" defaultValue:@[]];
        self.currentCCBankcode = @"";
        self.currentBankcode = @"";
        self.currentPMCID = -1;
    }
    return self;
    
}

@end
