//
//  ZPCheckPinViewController.m
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 5/11/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPCheckPinViewController.h"
#import "ZPCheckPinView.h"
#import "ZPProgressHUD.h"
#import "ZaloPayWalletSDKLog.h"
#import "ZaloPayWalletSDKPayment.h"

static ZPCheckPinViewController __weak *activePin;

@interface ZPCheckPinViewController ()<ZPCheckPinViewDataSource, ZPCheckPinViewDelegate, UITextFieldDelegate>
@property (strong, nonatomic) UITextField *textField;
@property (strong, nonatomic) ZPCheckPinView *pinView;
@property (strong, nonatomic) UIView *overLayView;
@end

@implementation ZPCheckPinViewController

- (id)init {
    self = [super init];
    if (self) {
        self.check = [RACSubject new];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    self.overLayView = [[UIView alloc] initWithFrame:self.view.bounds];
    self.overLayView.backgroundColor = [UIColor blackColor];
    self.overLayView.alpha = 0;
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(checkPinDidTapClose)];
    [self.overLayView addGestureRecognizer:tap];
    
    [self.view addSubview:self.overLayView];
    
    
    self.textField = [[UITextField alloc] initWithFrame:CGRectZero];
    self.textField.keyboardType = UIKeyboardTypeNumberPad;
    self.textField.delegate = self;
    [self.view addSubview:_textField];
    
    self.pinView = [[ZPCheckPinView alloc] initWith:self.textField.rac_textSignal
                                          withSpace:[self spacePassword]
                                         methodName:self.methodName
                                        methodImage:self.methodImage
                                     suggestTouchId:_isShowSuggestTouchId];
    self.pinView.dataSource = self;
    self.pinView.delegate = self;
    
    @weakify(self);
    [[[self rac_signalForSelector:@selector(viewWillAppear:)] deliverOnMainThread] subscribeNext:^(id x) {
        @strongify(self);
        [self.pinView reloadData];
        self.textField.inputAccessoryView = self.pinView;
        // Valid if have alert
        [self showKeyboard];
    }];
    
    [[self rac_signalForSelector:@selector(viewWillDisappear:)] subscribeNext:^(id x) {
        @strongify(self);
        [self.textField resignFirstResponder];
    }];
    
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:ZPDialogViewHideNotification object:nil] takeUntil:self.rac_willDeallocSignal] subscribeNext:^(NSNotification * _Nullable x) {
        @strongify(self);
        DDLogInfo(@"Will Show Again!!!");
        [self.textField becomeFirstResponder];
    }];
    
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillShowNotification object:nil] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(NSNotification *notify) {
        @strongify(self);
        if (!notify) return;
        
        NSDictionary *info = [notify userInfo];
        NSNumber *duration = info[UIKeyboardAnimationDurationUserInfoKey];
        NSTimeInterval time = duration ? [duration doubleValue] : 0;
        [UIView animateWithDuration:time animations:^{
            self.overLayView.alpha = 0.6;
        }];
    }];
    
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillHideNotification object:nil] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(NSNotification *notify) {
        @strongify(self);
        if (!notify) return;
        
        NSDictionary *info = [notify userInfo];
        NSNumber *duration = info[UIKeyboardAnimationDurationUserInfoKey];
        NSTimeInterval time = duration ? [duration doubleValue] : 0;
        [UIView animateWithDuration:time animations:^{
            self.overLayView.alpha = 0;
        }];
    }];
    
    [[[[[NSNotificationCenter defaultCenter]
        rac_addObserverForName:UIApplicationDidBecomeActiveNotification object:nil]
       takeUntil:[self rac_willDeallocSignal]]
      filter:^BOOL(id value) {
          @strongify(self);
          return ![self.textField isFirstResponder];
      }] subscribeNext:^(id x) {
          @strongify(self);
          [self showKeyboard];
      }];
}

- (void) showKeyboard {
    // Valid if have alert
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    if ([keyWindow isKindOfClass:[MMPopupWindow class]]) {
        return;
    }
    
    [self.textField becomeFirstResponder];
}

+ (RACSignal *)showOn:(UIViewController *)root
                using:(NSString *)title
            methodName:(NSString *)methodName
          methodImage:(UIImage*)methodImage
       suggestTouchId:(BOOL)isSuggest {
#if DEBUG
    // Tracking if dealloc not call
    NSAssert(activePin == nil, @"Error Dealloc !!!!");
#endif
    ZPCheckPinViewController *pinVC = [ZPCheckPinViewController new];
    activePin = pinVC;
    pinVC.titlePopUp = title;
    pinVC.methodName = methodName;
    pinVC.methodImage = methodImage;
    pinVC.isShowSuggestTouchId = isSuggest;
    pinVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    pinVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [root presentViewController:pinVC animated:YES completion:nil];
    return pinVC.check;
}

+ (ZPCheckPinViewController *)currentView {
    return activePin;
}

+ (void)dissmiss:(BOOL)success {
    if (success) {
        [[ZPAppFactory sharedInstance] saveLastTimeInputPin];
    }
    
    ZPCheckPinViewController *controller = [self currentView];
    if ([controller.pinView enableTouchIdChecked] && success) {
        [self enableTouchId];
    }
    [controller dismissViewControllerAnimated:YES completion:nil];
}

+ (void)forceClose {
    [self dissmiss:FALSE];
}

+ (void)showErrorMessage:(NSString *)errorMessage {
    [self currentView].textField.text = nil;
    [[self currentView].textField sendActionsForControlEvents:UIControlEventAllEditingEvents];
    [[self currentView].pinView showErrorMessage:errorMessage];
}

+ (void)startLoading {
    [[self currentView].pinView startLoading];
}

+ (void)enableTouchId {
    id <ZPWalletDependenceProtocol> helper = [ZaloPayWalletSDKPayment sharedInstance].appDependence;    
    if (helper.usingTouchId == true) {
        return;
    }
    [helper setUsingTouchId:true];
    NSString *message = [R string_TouchID_Enable_Success_Message];
    message = [helper checkToReplaceFaceIDMessage: message];
    [[RACScheduler mainThreadScheduler] afterDelay:1.0 schedule:^{
        UIViewController *top = [[ZPAppFactory sharedInstance] rootNavigation].viewControllers.lastObject;
        [helper showToastFrom:top message:message delay:0];
    }];
}

- (void)dealloc {
#if DEBUG
    NSAssert(activePin == nil, @"Error Dealloc !!!!");
#endif
    [_check sendCompleted];
    DDLogInfo(@"dealloc!!!");
    
}

#pragma mark - Delegate

- (void)checkPinDidTapClose {
    if ([self.pinView isLoadingData]) {
        return;
    }
    [ZPCheckPinViewController dissmiss:false];
}

#pragma mark - DataSource View

- (NSString *)title {
    return _titlePopUp ?: @"Please set title!!!";
}

- (NSInteger)lengthPassword {
    return 6;
}

- (UIColor *)colorSelected {
    return [UIColor colorWithHexValue:0x008fe5];
}

- (UIColor *)colorUnselected{
    return [UIColor whiteColor];
}

- (CGFloat)borderWidth {
    return 1;
}

- (CGFloat)spacePassword {
    return 16;
}
- (CGSize)sizePassword {
    return CGSizeMake(16, 16);
}

#pragma mark - Text Field

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([self.pinView isLoadingData]) {
        return false;
    }
    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (newText.length == [self lengthPassword] ) {
        [_check sendNext:newText];
    }
    return !(newText.length > [self lengthPassword]);
}
@end
