//
//  ZPCheckPinViewController.h
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 5/11/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface ZPCheckPinViewController : UIViewController
@property (copy, nonatomic) NSString *titlePopUp;
@property (nonatomic, strong) NSString *methodName;
@property (nonatomic, strong) UIImage *methodImage;
@property (strong, nonatomic) RACSubject *check;
@property (assign, nonatomic) BOOL isShowSuggestTouchId;
+ (RACSignal *)showOn:(UIViewController *)root
                using:(NSString *)title
            methodName:(NSString *)methodName
          methodImage:(UIImage*)methodImage
       suggestTouchId:(BOOL)isSuggest;
+ (ZPCheckPinViewController *)currentView;
+ (void)dissmiss:(BOOL)success;
+ (void)startLoading;
+ (void)showErrorMessage:(NSString *)errorMessage;
+ (void)forceClose;
@end
