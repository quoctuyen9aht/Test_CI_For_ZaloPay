//
//  ZaloPaySDKUtil.h
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 7/11/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ZPBill;
@class ZPPaymentMethod;
@class ZPBillExtraInfo;

@interface ZaloPaySDKUtil : NSObject
+ (NSArray<ZPBillExtraInfo*> *)extInfoFromString:(NSString *)item;
+ (void)writeAppTransIdLog:(ZPBill *)bill;
+ (BOOL)isRequirePin:(ZPBill *)bill method:(ZPPaymentMethod*)method;
@end
