//
//  ZPVoucherInfo.m
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/20/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import "ZPVoucherInfo.h"
#import "ZPVoucherHistory.h"

@implementation ZPVoucherInfo
+ (instancetype)fromDic:(NSDictionary *)dic {
    ZPVoucherInfo *nVoucher = [ZPVoucherInfo new];
    nVoucher.voucherSig = [dic stringForKey:@"vouchersig"];
    nVoucher.campaignId = [dic intForKey:@"campaignid"];
    nVoucher.discountAmount = [dic intForKey:@"discountamount"];
    nVoucher.useVoucheTime = [dic doubleForKey:@"usevouchertime"];
    return nVoucher;
}

- (BOOL)isEqual:(id)object {
    ZPVoucherInfo *otherVoucher = [ZPVoucherInfo castFrom:object];
    if (!otherVoucher || [otherVoucher.voucherUsed.uid length] == 0) {
        return NO;
    }
    @try {
        return [self.voucherUsed.uid isEqualToString:otherVoucher.voucherUsed.uid];
    } @catch(NSException *e) {
        return NO;
    }
}
@end
