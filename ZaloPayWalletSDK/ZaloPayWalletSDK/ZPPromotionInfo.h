//
//  ZPPromotionInfo.h
//  ZaloPayWalletSDK
//
//  Created by Phuochh on 5/30/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZPPromotion.h"

@interface ZPPromotionInfo : NSObject
@property (nonatomic, strong) NSString *promotionsig;
@property (nonatomic, strong) NSString *promotiontransid;
@property (nonatomic) long long discountamount;
@property (nonatomic, strong) ZPPromotion *promotionUsed;
- (instancetype)initDataWithDict:(NSDictionary *)dict;
@end
