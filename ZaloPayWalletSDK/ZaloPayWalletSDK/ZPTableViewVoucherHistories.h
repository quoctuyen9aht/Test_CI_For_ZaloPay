//
//  ZPTableViewVoucherHistories.h
//  ZaloPayWalletSDK
//
//  Created by Phuoc Huynh on 9/7/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZPVoucherHistory.h"
#import "ZPPromotion.h"
#import "ZPBill.h"

@protocol ZPTableViewVoucherHistoriesDelegate <NSObject>
@optional
- (void) didSelectRowVoucher:(ZPVoucherHistory *)voucher atIndexPath:(NSIndexPath *)indexPath;
- (void) didSelectRowPromotion:(ZPPromotion *)promotion atIndexPath:(NSIndexPath *)indexPath;
- (void) updateUIWhenHaveVouchers;
- (void) tapFooterView;
@end
@interface ZPTableViewVoucherHistories : UITableView <UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) NSMutableArray<ZPVoucherHistory *> *arrVoucherHistories;
@property (nonatomic,strong) NSArray<ZPPromotion *> *arrPromotions;
@property (nonatomic,strong) NSMutableArray *arrAllVouchersAndPromotions;
@property (nonatomic) ZPBill *bill;
@property (nonatomic) UIView *footerView;
@property (nonatomic, weak) id <ZPTableViewVoucherHistoriesDelegate> zpTableViewVoucherHistoriesDelegate;
- (void) setupBill:(ZPBill *)bill;
- (void)setupArrAllVouchersAndPromotions;
- (void)resetSelectedStateAllVouchersAndPromotions;
- (void)resetStateAllVouchersAndPromotions;
@end
