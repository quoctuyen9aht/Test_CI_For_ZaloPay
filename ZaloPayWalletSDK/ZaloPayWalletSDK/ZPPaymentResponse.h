//
//  ZPPaymentResponse.h
//  ZPSDK
//
//  Created by phungnx on 12/22/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZPResponseObject.h"
#import "ZPDefine.h"
@class ZPBill;
@interface ZPPaymentResponse : ZPResponseObject

@property (assign, nonatomic) int exchangeStatus;
@property (assign, nonatomic) ZPPaymentMethodType paymentMethod;
@property (assign, nonatomic) BOOL isProcessing;
@property (strong, nonatomic) NSString * data;
@property (strong, nonatomic) NSString * zpTransID;
@property (assign, nonatomic) long long balance;
@property (assign, nonatomic) long long reqdate;
@property (assign, nonatomic) int actionType;
@property (assign, nonatomic) BOOL shouldHandlePaymentResponse;
@property (strong, nonatomic) NSString *first6NumberCard;
// Using for Gateway
@property (assign, nonatomic) long long userFeeAmount;

+ (ZPPaymentResponse *)unidentifiedStatusObject;
+ (ZPPaymentResponse *)duplicateTransIDResponseObject;
+ (ZPPaymentResponse *)timeOutRequestObject;
+ (ZPPaymentResponse *)retryRequestObject;
+ (ZPPaymentResponse *)unknownExceptionResponseObject;
+ (ZPPaymentResponse *)sandboxResponse:(double)chargeAmount currency:(NSString *)currency;
+ (instancetype)paymentResponseFrom:(NSDictionary *)dictionary
                               with:(NSString *)zpTransID
                                 at:(ZPPaymentStep)step
                           bankCode:(NSString *)bankCode
                               path:(NSString *)path
                             params:(NSDictionary *)params;

- (instancetype)initWith:(ZPResponseObject *)responseObject;
- (instancetype)initWithPaymentResponse:(ZPPaymentResponse *)responseObject;
- (NSString*)appsFlyerValue;
- (NSDictionary *)toJsonResponse:(ZPBill *)bill;
@end
