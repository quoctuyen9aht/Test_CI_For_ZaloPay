//
//  ZPPromotionUtils.m
//  ZaloPayWalletSDK
//
//  Created by Phuochh on 6/1/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import "ZPEndowUtils.h"
#import "ZPVoucherPaymentConditions.h"

@implementation ZPEndowUtils
+ (NSString *)getValueStringPercentStyleWithValue:(long long)percentValue {
    if (fmod(percentValue, 1.0) == 0.0) {
        return [NSString stringWithFormat:@"%.0lld%%",percentValue];
    }
    return [NSString stringWithFormat:@"%.1lld%%",percentValue];
}
+ (NSAttributedString *)getValueStringVNDStyleWithValue:(long long)value {
    NSString *moneyTitle = [@(value) formatCurrency];
    return [moneyTitle formatCurrencyFont:[UIFont SFUITextMediumWithSize:26]
                                    color:[UIColor defaultText]
                                  vndFont:[UIFont SFUITextRegularWithSize:18]
                                 vndcolor:[UIColor blackColor]
                                alignment:NSTextAlignmentCenter];
}
+ (id)getStringValueTableViewCellWithValueType:(int)valuetype andValue:(long long)value {
    if (valuetype == ZPVoucherValueTypePercent) {
        return [ZPEndowUtils getValueStringPercentStyleWithValue:value];
    }
    return [ZPEndowUtils getValueStringVNDStyleWithValue:value];
}
+ (NSString *)getStringValueCollectionViewCellWithValueType:(int)valuetype andValue:(long long)value {
    if (valuetype == ZPVoucherValueTypePercent) {
        return [ZPEndowUtils getValueStringPercentStyleWithValue:value];
    }
    return [ZPEndowUtils getValueStringKStyleWithValue:value];
}
+ (NSString *)getValueStringKStyleWithValue:(long long)value {
    float valueDisplay = value >= 1000 && value % 1000 == 0 ? value/1000 : value;
    NSString *valueStringReturn = [NSString stringWithFormat:@"%.0fk",valueDisplay];
    if (valueDisplay == value) {
        long long first = value/1000;
        long long last = value - (first * 1000);
        if (last < 1000) {
            last = last / 100;
        }
        valueStringReturn = [NSString stringWithFormat:@"%llik%lli",first,last];
    }
    return valueStringReturn;
}
+ (void)showAlertNotUseEndow:(void (^) (void))handler {
    [ZPDialogView showDialogWithType:DialogTypeNotification
                             message:[R string_Endow_Warning_NotUse]
                   cancelButtonTitle:[R string_ButtonLabel_Next]
                    otherButtonTitle:@[[R string_Voucher_select_gift_text]]
                      completeHandle:^(int buttonIndex, int cancelButtonIndex) {
                          if (buttonIndex != cancelButtonIndex) {
                              return;
                          }
                          handler();
                      }];
    
}
@end
