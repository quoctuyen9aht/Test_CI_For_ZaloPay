//
//  VoucherPaymentConditions.h
//  ZaloPayWalletSDK
//
//  Created by Phuoc Huynh on 1/3/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZPVoucherPaymentConditions : NSObject
@property (nonatomic, strong) NSArray *useconditionapplist;
@property (nonatomic, strong) NSArray *useconditionpmcs;
@property (nonatomic, strong) NSArray *useconditionbankcodes;
@property (nonatomic, strong) NSArray *useconditionccbankcodes;
@property (nonatomic) int currentPMCID;
@property (nonatomic, strong) NSString *currentBankcode;
@property (nonatomic, strong) NSString *currentCCBankcode;
- (instancetype)initDataWithDict:(NSDictionary *)dict;
@end
