//
//  UITextField+ZPExtension.h
//  ZPSDK
//
//  Created by phungnx on 12/21/15.
//  Copyright (c) 2015-present VNG Corporation All rights reserved.
//

#import "UITextField+ZPExtension.h"
#import "ZPConfig.h"

@implementation UITextField (ZPExtension)

- (void)zpSetIndent:(int)indent {
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, indent, 10)];
    [self setLeftViewMode:UITextFieldViewModeAlways];
    [self setLeftView:spacerView];
}

- (void)zpSetDefaultBorder {
    self.layer.borderWidth = 1;
    self.layer.borderColor = zpRGB(210, 210, 210).CGColor;
}

@end
