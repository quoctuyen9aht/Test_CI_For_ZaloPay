//
//  UIColor+Custom.h
//  ZPSDK
//
//  Created by phungnx on 12/21/15.
//  Copyright (c) 2015-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (ZPExtension)
+ (UIColor *)zpLightBlueColor;
+ (UIColor *)zpLightLightBlueColor;
+ (UIColor *)zpDarkGreenColor;
+ (UIColor *)zpLightLightGrayColor;
+ (UIColor *)zpLightLightLightGrayColor;
+ (UIColor *)zpButtonDisabledColor;
+ (UIColor *)zpColorFromHexString:(NSString *)hexString;

@end
