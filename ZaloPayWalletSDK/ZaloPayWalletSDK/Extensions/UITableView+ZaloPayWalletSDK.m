//
//  UITableView+ZaloPayWalletSDK.m
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/1/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "UITableView+ZaloPayWalletSDK.h"
#import "ZPDataManager.h"

@implementation UITableView (ZaloPayWalletSDK)

- (void)registerCellFromSDKBundle:(Class)cellClass {
    NSString *name = NSStringFromClass(cellClass);
    [self registerNib:[[ZPDataManager sharedInstance] nibNamed:name] forCellReuseIdentifier:name];
}
@end

@implementation UITableViewCell (ZaloPayWalletSDK)
+ (instancetype)sdkCelllForTableView:(UITableView *)tableView {
    NSString *name = NSStringFromClass(self);
    return [tableView dequeueReusableCellWithIdentifier:name];
}
@end
