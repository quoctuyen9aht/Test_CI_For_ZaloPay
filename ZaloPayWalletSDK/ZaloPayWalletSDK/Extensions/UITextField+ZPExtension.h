//
//  UITextField+ZPExtension.h
//  ZPSDK
//
//  Created by phungnx on 12/21/15.
//  Copyright (c) 2015-present VNG Corporation All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (ZPExtension)

- (void)zpSetIndent:(int)indent;
- (void)zpSetDefaultBorder;

@end
