//
//  UIImage+ZPExtension.h
//  ZPSDK
//
//  Created by phungnx on 12/21/15.
//  Copyright (c) 2015-present VNG Corporation All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ZPExtension)
+ (UIImage *)zpScaleAndRotateImage:(UIImage *)image resolution: (int) resolution;
+ (UIImage *)zpImageWithColor:(UIColor *)color;
+ (UIImage *)zpImageWithColor:(UIColor *)color withSize:(CGSize)size;
+ (UIImage *)zpImageResize :(UIImage*)img andResizeTo:(CGSize)newSize andOffset:(CGPoint)offset;
+ (UIImage *)drawText:(NSString*)text
                font:(UIFont *)f
                size:(CGSize)s
               color:(UIColor *)c;
@end
