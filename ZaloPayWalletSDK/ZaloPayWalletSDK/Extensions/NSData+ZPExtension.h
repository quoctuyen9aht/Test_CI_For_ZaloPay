//
//  NSData+ZPExtension.h
//  ZPSDK
//
//  Created by phungnx on 12/21/15.
//  Copyright (c) 2015-present VNG Corporation All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (ZPExtension)

+ (NSData *)zpBase64DecodeString:(NSString *)string;
- (NSString *)zpBase64EncodeData;
- (NSString *)zpToHex;
- (NSData *)zpAES256EncryptWithKey:(NSString *)key;
- (NSData *)zpAES256DecryptWithKey:(NSString *)key;
@end
