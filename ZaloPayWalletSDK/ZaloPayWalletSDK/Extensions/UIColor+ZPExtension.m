//
//  UIColor+Custom.m
//  ZPSDK
//
//  Created by phungnx on 12/21/15.
//  Copyright (c) 2015-present VNG Corporation. All rights reserved.
//

#import "UIColor+ZPExtension.h"

@implementation UIColor (ZPExtension)
+ (UIColor*) zpLightBlueColor{
    return [UIColor colorWithRed:(5.0 /255) green:(163.0f / 255) blue:(229.0f / 255) alpha:1];
}
+ (UIColor*) zpLightLightBlueColor{
    return [UIColor colorWithRed:(5.0 /255) green:(163.0f / 255) blue:(229.0f / 255) alpha:0.4];
}
+ (UIColor*) zpLightLightGrayColor{
    return [UIColor colorWithWhite:0.88f alpha:1.0];
}

+ (UIColor*) zpLightLightLightGrayColor{
    return [UIColor colorWithWhite:0.95f alpha:1.0];
}


+ (UIColor*) zpDarkGreenColor{
    return [UIColor colorWithRed:(77.0 /255) green:(174.0f / 255) blue:(0.0f / 255) alpha:1];
}

+ (UIColor*) zpButtonDisabledColor {

    return [UIColor colorWithRed:0.69f green:0.82f blue:0.88f alpha:1];
}

+ (UIColor *)zpColorFromHexString:(NSString *)hexString {
    if(hexString.length == 0 || [hexString isEqualToString:@"#"]) return [UIColor clearColor];
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

@end
