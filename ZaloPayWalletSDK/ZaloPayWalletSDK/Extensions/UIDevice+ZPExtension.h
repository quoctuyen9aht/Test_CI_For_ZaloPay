//
//  UIDevice+ZPExtension.h
//  ZaloSDK
//
//  Created by Liem Vo Uy on 1/23/15.
//  Copyright (c) 2015-present VNG Corporation All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (ZPExtension)

- (NSString *) zpMobileNetworkCode;
- (NSString *) zpDeviceModel;
- (NSString *) zpOsVersion;
- (NSString *) zpAppVersion;
- (NSString *) zpAppBuildVersion;
@end
