//
//  NSHTTPCookieStorage+ZPExtension.h
//  ZaloSDK.Payment
//
//  Created by Dung My on 5/2/15.
//  Copyright (c) 2015-present VNG Corporation All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSHTTPCookieStorage (ZPExtension)
- (void)zpSaveCookiesToCache;
- (void)zpLoadCookiesFromCache;
- (void)zpClearCookiesFromCache;
- (void)zpRemoveAllStoredCredentials:(NSArray *)arrRequested;

@end
