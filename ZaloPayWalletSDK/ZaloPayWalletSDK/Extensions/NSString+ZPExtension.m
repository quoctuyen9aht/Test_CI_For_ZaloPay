//
//  NSString+zpExtension.h
//  ZPSDK
//
//  Created by phungnx on 12/21/15.
//  Copyright (c) 2015-present VNG Corporation All rights reserved.
//

#import "NSString+ZPExtension.h"
#import <CommonCrypto/CommonCrypto.h>
#import "NSData+ZPExtension.h"
#import <UIKit/UIKit.h>

@implementation NSString (ZPExtension)

- (NSString *) zpHmacForKey:(NSString *)key securityMode: (ZPSecurityMode) mode
{
    if(mode == ZPSecurityModeUnsupport) return self;
    
    const char *cKey = [key cStringUsingEncoding:NSUTF8StringEncoding];
    const char *cData = [self cStringUsingEncoding:NSUTF8StringEncoding];
    
    NSData * hash;
    if(mode == ZPSecurityModeHMACSHA256) {
        unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
        CCHmac(kCCHmacAlgSHA256, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
        hash = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
    }
    else if(mode == ZPSecurityModeHMACMD5) {
        unsigned char cHMAC[CC_MD5_DIGEST_LENGTH];
        CCHmac(kCCHmacAlgMD5, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
        hash = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
    }
    else if(mode == ZPSecurityModeHMACSHA384) {
        unsigned char cHMAC[CC_SHA384_DIGEST_LENGTH];
        CCHmac(kCCHmacAlgMD5, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
        hash = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
    }
    else if(mode == ZPSecurityModeHMACSHA1) {
        unsigned char cHMAC[CC_SHA1_DIGEST_LENGTH];
        CCHmac(kCCHmacAlgMD5, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
        hash = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
    }
    else if(mode == ZPSecurityModeHMACSHA512) {
        unsigned char cHMAC[CC_SHA512_DIGEST_LENGTH];
        CCHmac(kCCHmacAlgMD5, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
        hash = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
    }
    
    NSString * s = [hash zpToHex];
    return s;
}

+ (NSString *) zpFormatNumberWithDilimeter: (long) number {
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    [formatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setDecimalSeparator:@","];
    [formatter setGroupingSeparator:@"."];
    [formatter setMaximumFractionDigits:0];
    NSString * res = [formatter stringFromNumber:@(number)];
    return res;
}

- (NSString *)zpFormatNumberWithDilimeter {
    long long number = (long long)[[self zpRemoveDilimeter] longLongValue];
    if (number == 0) return nil;
    
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    //[formatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setGroupingSeparator:@"."];
    NSString * res = [formatter stringFromNumber:@(number)];
    return res;
}

- (NSString *)zpRemoveDilimeter {
    return [self stringByReplacingOccurrencesOfString:@"." withString:@""];
}
- (NSString *)zpStringByUnescapingFromURLQuery {
    NSString *deplussed = [self stringByReplacingOccurrencesOfString:@"+" withString:@" "];
    return [deplussed stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}
- (NSDictionary *)zpQueryDictionaryUsingEncoding:(NSStringEncoding)encoding{
    NSCharacterSet* delimiterSet = [NSCharacterSet characterSetWithCharactersInString:@"&;"] ;
    NSMutableDictionary* pairs = [NSMutableDictionary dictionary] ;
    NSScanner* scanner = [[NSScanner alloc] initWithString:self] ;
    while (![scanner isAtEnd]) {
        NSString* pairString ;
        [scanner scanUpToCharactersFromSet:delimiterSet
                                intoString:&pairString] ;
        [scanner scanCharactersFromSet:delimiterSet intoString:NULL] ;
        NSArray* kvPair = [pairString componentsSeparatedByString:@"="] ;
        if ([kvPair count] == 2) {
            NSString* key = [[kvPair objectAtIndex:0] zpStringByUnescapingFromURLQuery];
            NSString* value = [[kvPair objectAtIndex:1] zpStringByUnescapingFromURLQuery];
            [pairs setObject:value forKey:key] ;
        }
    }
    return [NSDictionary dictionaryWithDictionary:pairs] ;
}
- (NSString *) zpFormatWithFormat:(NSArray *)format separator: (NSString **) str{
    if (!format || format.count < 2){
        return self;
    }
    
    [self zpRemoveFormat:format];
    NSString *separator = [format objectAtIndex:0];
    if(str != NULL) {
        *str = separator;
    }
    
    NSString * abc = [self stringByReplacingOccurrencesOfString:separator withString:@""];
    NSMutableString * result = [NSMutableString stringWithString:abc];

    int formatLenght = 0;
    
    for (int i = 1; i < format.count; i++){
        int rx = [[format objectAtIndex:i] intValue];
        formatLenght += rx;
        
        if (result.length > formatLenght){
            [result insertString:separator atIndex:formatLenght];
            formatLenght += 1;
        }
        
    }
    
    return result;
}

- (NSString *) zpRemoveFormat:(NSArray *)format {
    if (!format || format.count < 2) return self;
    
    NSString * separator = format[0];
    
    return [self stringByReplacingOccurrencesOfString:separator withString:@""];
}

- (NSString *)zpTrimSpace {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

- (NSString *)zpTrimStringWithMaxLength:(NSInteger)maxLenght{
    
    if (self.length > maxLenght) {
        NSString * trimString = [self substringToIndex:maxLenght];
        trimString = [trimString stringByAppendingString:@"..."];
        return trimString;
    } else {
        return self;
    }
    
}

- (NSMutableArray *)zpStringToArray {
    
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < [self length]; i++) {
        [array addObject:[NSString stringWithFormat:@"%C", [self characterAtIndex:i]]];
    }
    
    return array;
}

+ (NSString *) zpEmptyIfNilString: (NSString *) str {
  if(str == nil) return @"";
  return str;
}

- (BOOL) zpContainsString: (NSString *) str {
  return [self rangeOfString:str].length > 0;
}

- (NSString*)zpEncodeURLString {
  NSString* encodedURL = (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(
                                                                                               NULL,
                                                                                               (CFStringRef)self,
                                                                                               NULL,
                                                                                               (CFStringRef)@"!*'();:@&=+$,/?%#[] ",
                                                                                               kCFStringEncodingUTF8 );
  return encodedURL ;
}

- (NSString*)zpDecodedURLString {
  NSString*result = (__bridge_transfer NSString *)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(kCFAllocatorDefault,
                                                                                                          (CFStringRef)self,
                                                                                                          CFSTR(""),
                                                                                                          kCFStringEncodingUTF8);
  return result;
}

- (NSString *)zpRemovePercentEncoding {
  return [self stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

- (NSString *) zpMd5 {
  const char *cStr = [self UTF8String];
  unsigned char digest[16];
  CC_MD5(cStr, (unsigned int) strlen(cStr), digest);
  
  NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
  for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
    [output appendFormat:@"%02x", digest[i]];
  }
  return  output;
}

- (NSString *) zpSha1 {
  const char *cstr = [self cStringUsingEncoding:NSUTF8StringEncoding];
  NSData *data = [NSData dataWithBytes:cstr length:self.length];
  uint8_t digest[CC_SHA1_DIGEST_LENGTH];
  CC_SHA1(data.bytes, (unsigned int) data.length, digest);
  
  NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
  for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++) {
    [output appendFormat:@"%02x", digest[i]];
  }
  return output;
}

- (NSString *) zpSha256 {
    const char *cstr = [self cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:self.length];
    uint8_t digest[CC_SHA256_DIGEST_LENGTH];
    CC_SHA256(data.bytes, (unsigned int) data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_SHA256_DIGEST_LENGTH; i++) {
        [output appendFormat:@"%02x", digest[i]];
    }
    return output;
}


- (NSString*)zpStringByAddingQueryDictionary:(NSDictionary*)query {
  NSMutableArray* pairs = [NSMutableArray array];
  for (NSString* key in [query keyEnumerator]) {
    NSString* value = [query objectForKey:key];
    if (value && [value isKindOfClass:[NSString class]]) {
      value = [value zpEncodeURLString];
    }
    
    NSString* pair = [NSString stringWithFormat:@"%@=%@", key, value];
    [pairs addObject:pair];
  }
  
  NSString* params = [pairs componentsJoinedByString:@"&"];
  if ([self rangeOfString:@"?"].location == NSNotFound) {
    return [self stringByAppendingFormat:@"?%@", params];
  } else {
    return [self stringByAppendingFormat:@"%@", params];
  }
}

- (NSString*)zpStringByAddingQueryDictionary2:(NSDictionary*)query {
    NSMutableArray* pairs = [NSMutableArray array];
    for (NSString* key in [query keyEnumerator]) {
        NSString* value = [query objectForKey:key];
        if (value && [value isKindOfClass:[NSString class]]) {
            value = [value zpEncodeURLString];
        }
        
        NSString* pair = [NSString stringWithFormat:@"%@=%@", key, value];
        [pairs addObject:pair];
    }
    
    NSString* params = [pairs componentsJoinedByString:@"&"];
    return [self stringByAppendingFormat:@"%@", params];
}


+ (NSString *) zpRequestWithBaseUrl:(NSString *)baseUrl
                              subUrl:(NSString *)subUrl
                              params:(NSDictionary *)params {
  NSString * requestUrl = [NSString stringWithFormat:@"%@%@?",baseUrl, subUrl];
  return [requestUrl zpStringByAddingQueryDictionary:params];
}

+ (NSString *) zpAES128EncryptData:(NSData *) data withKey:(NSString *)key {
  // 'key' should be 16 bytes for AES128, will be null-padded otherwise
  char keyPtr[kCCKeySizeAES128+1]; // room for terminator (unused)
  bzero(keyPtr, sizeof(keyPtr)); // fill with zeroes (for padding)
  
  
  NSData * keyData = [key dataUsingEncoding:NSUTF8StringEncoding];
  
  // fetch key data
  [keyData getBytes:keyPtr length:[keyData length]];
  
  NSUInteger dataLength = [data length];
  
  //See the doc: For block ciphers, the output size will always be less than or
  //equal to the input size plus the size of one block.
  //That's why we need to add the size of one block here
  size_t bufferSize = dataLength + kCCBlockSizeAES128;
  void *buffer = malloc(bufferSize);
  
  size_t numBytesEncrypted = 0;
  CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt, kCCAlgorithmAES128, kCCOptionPKCS7Padding | kCCOptionECBMode,
                                        keyPtr, kCCKeySizeAES128,
                                        NULL /* initialization vector (optional) */,
                                        [data bytes], dataLength, /* input */
                                        buffer, bufferSize, /* output */
                                        &numBytesEncrypted);
  if (cryptStatus == kCCSuccess) {
    //the returned NSData takes ownership of the buffer and will free it on deallocation
    NSData * result = [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted];
    //free(buffer); //free the buffer;
    return [result zpBase64EncodeData];
  }
  
  free(buffer); //free the buffer;
  return nil;
  
}

+ (NSString *) zpAES128EncryptString:(NSString *) str withKey:(NSString *)key {
  NSData * data = [str dataUsingEncoding:NSUTF8StringEncoding];
  return [self zpAES128EncryptData:data withKey:key];
}

+ (NSString *) zpAES128DecryptData:(NSData *) data withKey:(NSString *)key {
  // 'key' should be 16 bytes for AES128, will be null-padded otherwise
  char keyPtr[kCCKeySizeAES128+1]; // room for terminator (unused)
  bzero(keyPtr, sizeof(keyPtr)); // fill with zeroes (for padding)
  
  NSData * keyData = [key dataUsingEncoding:NSUTF8StringEncoding];
  // fetch key data
  [keyData getBytes:keyPtr length:[keyData length]];
  
  NSUInteger dataLength = [data length];
  
  //See the doc: For block ciphers, the output size will always be less than or
  //equal to the input size plus the size of one block.
  //That's why we need to add the size of one block here
  size_t bufferSize = dataLength + kCCBlockSizeAES128;
  void *buffer = malloc(bufferSize);
  
  size_t numBytesDecrypted = 0;
  CCCryptorStatus cryptStatus = CCCrypt(kCCDecrypt, kCCAlgorithmAES128, kCCOptionPKCS7Padding | kCCOptionECBMode,
                                        keyPtr, kCCKeySizeAES128,
                                        NULL /* initialization vector (optional) */,
                                        [data bytes], dataLength, /* input */
                                        buffer, bufferSize, /* output */
                                        &numBytesDecrypted);
  
  if (cryptStatus == kCCSuccess) {
    //the returned NSData takes ownership of the buffer and will free it on deallocation
    NSData * result = [NSData dataWithBytesNoCopy:buffer length:numBytesDecrypted];
    //free(buffer); //free the buffer;
    return [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
  }
  
  free(buffer); //free the buffer;
  return nil;
}

+ (NSString *) zpAES128DecryptString:(NSString *) str withKey:(NSString *)key {
  NSData * data = [NSData zpBase64DecodeString:str];
  return [self zpAES128DecryptData:data withKey:key];
}

+(NSString *)timeString:(long long)timeStamp {
    //11:30 26/11/2016
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm dd/MM/yyyy"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeStamp/1000.0];
    return [dateFormatter stringFromDate:date];
}

+ (NSAttributedString *)attributedString:(NSString *)string {
    NSMutableParagraphStyle *style =  [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    style.alignment = NSTextAlignmentCenter;
    style.firstLineHeadIndent = 8.0f;
    style.headIndent = 8.0f;
    style.tailIndent = -8.0f;
    NSAttributedString *attrText = [[NSAttributedString alloc]
                                    initWithString:[NSString stringWithFormat:@"\n%@\n",string]
                                    attributes:@{ NSParagraphStyleAttributeName : style}];
    return attrText;
}

@end

