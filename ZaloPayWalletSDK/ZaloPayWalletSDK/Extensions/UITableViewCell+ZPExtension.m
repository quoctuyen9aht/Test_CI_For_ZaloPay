//
//  UITableViewCell+ZPExtension.m
//  ZaloPayWalletSDK
//
//  Created by thi la on 6/11/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "UITableViewCell+ZPExtension.h"

@implementation UITableViewCell (ZPExtension)
+(instancetype)cellFrom:(UITableView *)tableView
                   with:(NSString *)identify {
    return [self castFrom:[tableView dequeueReusableCellWithIdentifier:identify]];
}
@end
