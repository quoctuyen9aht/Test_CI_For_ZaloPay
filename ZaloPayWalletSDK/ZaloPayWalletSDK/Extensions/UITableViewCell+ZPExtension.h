//
//  UITableViewCell+ZPExtension.h
//  ZaloPayWalletSDK
//
//  Created by thi la on 6/11/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (ZPExtension)
+(instancetype)cellFrom:(UITableView *)tableView
                   with:(NSString *)identify;
@end
