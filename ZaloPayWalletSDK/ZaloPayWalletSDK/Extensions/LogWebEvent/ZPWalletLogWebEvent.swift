//
//  ZPWalletLogWebEvent.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 5/2/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import ZaloPayAnalyticsSwift
import ZaloPayWalletSDKPrivate
import WebKit

enum ZPWalletLogWebEventType {
    case input(String?)
    case error(Error)
    case url(String?)
    case appEvent(String?)
    
    var type: Int {
        switch self {
        case .url:
            return 1
        case .input:
            return 2
        case .appEvent:
            return 3
        case .error:
            return 4
        }
    }
    
    var description: String? {
        switch self {
        case .url(let path):
            return path
        case .input(let name):
            return name
        case .appEvent(let event):
            return event
        case .error(let e):
            return e.localizedDescription
        }
    }
}

struct ZPWalletLogWebEventValue {
    let eventType: ZPWalletLogWebEventType
    let apptransid: String?
    let date: Date
    let state: Int
    
    
//    {
//        type: Int (loại input: 1> url , 2> userInput, 3> appEvent, 4> error)
//        apptransid: String (180424000000026)
//        description: String? (error message , hoặc tương ứng với mỗi loại type vd url thì description là url string)
//        time: double (time hiện tại tracking)
//        state: Int (0: start, 1: end)
//    }

    func exportJson() -> [String : Any] {
        var result: [String : Any] = [:]
        result["type"] = eventType.type
        result["apptransid"] = apptransid
        result["description"] = eventType.description
        result["time"] = date.timeIntervalSince1970
        result["state"] = state
        return result
    }
}

protocol WebDelegateProtocol: NSObjectProtocol {
    associatedtype DelegateType
    var delegate: DelegateType? { get set }
}

extension UIWebView: WebDelegateProtocol {}
extension WKWebView: WebDelegateProtocol {
    var delegate: WKNavigationDelegate? {
        get {
            return navigationDelegate
        }
        set {
            self.navigationDelegate = newValue
        }
    }
}

protocol WebTrackDelegateProtocol: class {
    associatedtype WebType: WebDelegateProtocol where WebType: UIView
    var originalDelegate: WebType.DelegateType? { get set }
    var webView: WebType? { get set }
    var queue: DispatchQueue { get }
    var apptransid: String? { get set }
    var disposeBag: DisposeBag { get }
    var keyPath: String { get }
    init?(with web: WebType?, transid: String?)
    func isNeedResetDelegate() -> Bool
}

extension WebTrackDelegateProtocol {
    func commonSetup(from web: WebType?, with apptransid: String?) {
        self.apptransid = apptransid
        self.webView = web
        resetDelegate()
        setupEvent()
    }
    
    func writeLog(with type: ZPWalletLogWebEventType, state: Int = 0) {
        let value = ZPWalletLogWebEventValue(eventType: type, apptransid: self.apptransid, date: Date(), state: state)
        let result = value.exportJson()
        #if DEBUG
        print("Will Write Log: \(result)")
        #endif
        queue.async {
            ZPTrackingHelper.shared().eventTracker?.writeLogWebEvent(log: result as NSDictionary)
        }
    }
    
    func resetDelegate() {
        guard isNeedResetDelegate() else {
            return
        }
        
        // Check if exist delegate -> copy
        guard let originalDelegate = self.webView?.delegate else {
            // If not waiting set delegate and capture
            return
        }
    
        // Capture
        #if DEBUG
        print("Class : \(originalDelegate.self)")
        #endif
        self.originalDelegate = originalDelegate
        // Sure that self has protocol need capture
        guard let dependency = self as? WebType.DelegateType else {
            return
        }
        webView?.delegate = dependency
    }
    
    func setupEvent() {
        guard let webView = self.webView else {
            return
        }
        let eventRemove = webView.rx.methodInvoked(#selector(UIView.removeFromSuperview))
        webView.rx.observeWeakly(WebType.DelegateType.self, keyPath , options: .new).takeUntil(eventRemove).bind(onNext: { [weak self](_) in
            self?.resetDelegate()
        }).disposed(by: disposeBag)
        
        // Keyboard
        let eventKeyboards = [NotificationCenter.default.rx
            .notification(Notification.Name.UIKeyboardWillShow),
                              NotificationCenter.default.rx
                                .notification(Notification.Name.UIKeyboardWillHide)]
        
        Observable.merge(eventKeyboards).takeUntil(eventRemove)
            .subscribe(onNext: { [weak self](n) in
                // write log input
                let name = n.name
                let type = ZPWalletLogWebEventType.input(name.rawValue)
                self?.writeLog(with: type)
            }).disposed(by: disposeBag)
        
        // App event
        let eventApps = [NotificationCenter.default.rx
            .notification(Notification.Name.UIApplicationDidEnterBackground),
                         NotificationCenter.default.rx
                            .notification(Notification.Name.UIApplicationWillEnterForeground),
                         NotificationCenter.default.rx
                            .notification(Notification.Name.UIApplicationDidBecomeActive),
                         NotificationCenter.default.rx
                            .notification(Notification.Name.UIApplicationWillResignActive),
                         NotificationCenter.default.rx
                            .notification(Notification.Name.UIApplicationWillTerminate)]
        
        Observable.merge(eventApps).takeUntil(eventRemove)
            .subscribe(onNext: { [weak self](n) in
                // write log input
                let name = n.name
                let type = ZPWalletLogWebEventType.appEvent(name.rawValue)
                self?.writeLog(with: type)
            }).disposed(by: disposeBag)
        
    }
}

extension WebTrackDelegateProtocol where WebType.DelegateType: NSObjectProtocol {
    func isNeedResetDelegate() -> Bool {
        guard let currentDelegate = self.originalDelegate else {
            return true
        }
        let reset = self.webView?.delegate !== currentDelegate
        return reset
    }
}

// MARK: - uiwebview
/// Using for track uiwebview
@objcMembers
public class ZPWalletLogWebEvent: NSObject, WebTrackDelegateProtocol {
    internal weak var webView: UIWebView?
    // Use for catch event from web
    internal weak var originalDelegate: UIWebViewDelegate?
    internal var disposeBag: DisposeBag = DisposeBag()
    internal var apptransid: String?
    internal private (set) lazy var queue = DispatchQueue.init(label: "sdk.webtrack.syncLog", qos: .background)
    internal var keyPath: String = "delegate"
    public convenience required init?(with webView: UIWebView?, transid: String?) {
        guard let webView = webView else {
            return nil
        }
        self.init()
        commonSetup(from: webView, with: transid)
    }
}

// Web view delegate , using for check and return result to original delegate
extension ZPWalletLogWebEvent: UIWebViewDelegate {
    public func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        let type = ZPWalletLogWebEventType.url(request.url?.absoluteString)
        self.writeLog(with: type)
        return self.originalDelegate?.webView?(webView, shouldStartLoadWith: request, navigationType: navigationType) ?? false
    }
    
    public func webViewDidStartLoad(_ webView: UIWebView) {
        let type = ZPWalletLogWebEventType.url(webView.request?.url?.absoluteString)
        self.writeLog(with: type)
        self.originalDelegate?.webViewDidStartLoad?(webView)
    }
    
    public func webViewDidFinishLoad(_ webView: UIWebView) {
        let type = ZPWalletLogWebEventType.url(webView.request?.url?.absoluteString)
        self.writeLog(with: type, state: 1)
        self.originalDelegate?.webViewDidFinishLoad?(webView)
    }
    
    
    public func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        let type = ZPWalletLogWebEventType.error(error)
        self.writeLog(with: type, state: 1)
        self.originalDelegate?.webView?(webView, didFailLoadWithError: error)
    }
}

// MARK: - wkwebview
public class ZPWalletLogWKWebEvent<T: WKWebView>: NSObject, WebTrackDelegateProtocol, WKNavigationDelegate {
    internal weak var webView: T?
    // Use for catch event from web
    internal weak var originalDelegate: WKNavigationDelegate?
    internal var disposeBag: DisposeBag = DisposeBag()
    internal var apptransid: String?
    internal var keyPath: String = "navigationDelegate"
    internal private (set) lazy var queue = DispatchQueue.init(label: "sdk.webkit.syncLog", qos: .background)
    public convenience required init?(with webView: T?, transid: String?) {
        guard let webView = webView else {
            return nil
        }
        self.init()
        commonSetup(from: webView, with: transid)
    }
    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Swift.Void) {
        let url = navigationAction.request.url
        let type = ZPWalletLogWebEventType.url(url?.absoluteString)
        self.writeLog(with: type)
        guard self.originalDelegate?.webView?(webView, decidePolicyFor: navigationAction, decisionHandler: decisionHandler) == nil else {
            return
        }
        decisionHandler(.allow)
    }
    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Swift.Void) {
        guard self.originalDelegate?.webView?(webView, decidePolicyFor: navigationResponse, decisionHandler: decisionHandler) == nil else {
            return
        }
        decisionHandler(.allow)
    }
    
    
    public func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        self.originalDelegate?.webView?(webView, didStartProvisionalNavigation: navigation)
    }
    
    public func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        self.originalDelegate?.webView?(webView, didReceiveServerRedirectForProvisionalNavigation: navigation)
    }
    
    public func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        let type = ZPWalletLogWebEventType.error(error)
        self.writeLog(with: type)
        self.originalDelegate?.webView?(webView, didFailProvisionalNavigation: navigation, withError: error)
    }
    
    public func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        self.originalDelegate?.webView?(webView, didCommit: navigation)
    }
    
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        let url = webView.url
        let type = ZPWalletLogWebEventType.url(url?.absoluteString)
        self.writeLog(with: type, state: 1)
        self.originalDelegate?.webView?(webView, didFinish: navigation)
    }
    
    public func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        let type = ZPWalletLogWebEventType.error(error)
        self.writeLog(with: type)
        self.originalDelegate?.webView?(webView, didFail: navigation, withError: error)
    }
    
    public func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Swift.Void) {
        guard self.originalDelegate?.webView?(webView, didReceive: challenge, completionHandler: completionHandler) == nil else {
            return
        }
        completionHandler(.performDefaultHandling, nil)
    }
    
    @available(iOS 9.0, *)
    public func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
        self.originalDelegate?.webViewWebContentProcessDidTerminate?(webView)
    }
}



