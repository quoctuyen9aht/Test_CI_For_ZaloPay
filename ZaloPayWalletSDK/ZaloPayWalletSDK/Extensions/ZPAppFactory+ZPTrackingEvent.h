//
//  ZPAppFactory+ZPTrackingEvent.h
//  ZaloPayWalletSDK
//
//  Created by nhatnt on 5/9/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#ifndef ZPAppFactory_ZPTrackingEvent_h
#define ZPAppFactory_ZPTrackingEvent_h

@interface ZPAppFactory
- (void)trackEventId:(ZPAnalyticEventAction)eventId;
@end

#endif /* ZPAppFactory_ZPTrackingEvent_h */
