//
//  UITableView+ZaloPayWalletSDK.h
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/1/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (ZaloPayWalletSDK)
- (void)registerCellFromSDKBundle:(Class)cellClass;
@end

@interface UITableViewCell (ZaloPayWalletSDK)
+ (instancetype)sdkCelllForTableView:(UITableView *)tableView;
@end
