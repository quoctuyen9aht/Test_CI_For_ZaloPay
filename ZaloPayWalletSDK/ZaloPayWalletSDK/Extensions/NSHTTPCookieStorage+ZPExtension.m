//
//  NSHTTPCookieStorage+ZPExtension.m
//  ZPSDK
//
//  Created by phungnx on 12/21/15.
//  Copyright (c) 2015-present VNG Corporation All rights reserved.
//

#import "NSHTTPCookieStorage+ZPExtension.h"

#import "NSData+ZPExtension.h"
#import "ZPDataManager.h"
#import "ZaloPayWalletSDKLog.h"
#import <ZaloPayCommon/NSArray+Safe.h>

@implementation NSHTTPCookieStorage (ZPExtension)

- (void)zpLoadCookiesFromCache
{
    NSData* encryptData = [[NSUserDefaults standardUserDefaults] objectForKey: @"zp_zx_cookies"];
    NSData* decryptData = [encryptData zpAES256DecryptWithKey:[[ZPDataManager sharedInstance] stringByEnum:ZPStringTypePrivateKeyForCookieEncrypt]];
    NSArray *cookies = nil;
    @try {
        if (decryptData) {
            cookies = [NSKeyedUnarchiver unarchiveObjectWithData: decryptData];
        }
    }
    @catch (NSException *exception) {
        cookies = nil;
    }
    if (cookies) {
        NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        for (NSHTTPCookie *cookie in cookies)
        {
            [cookieStorage setCookie: cookie];
            DDLogInfo(@"%@",cookie);
        }
    }
    
}
- (void)zpSaveCookiesToCache
{
    NSData *cookiesData = [NSKeyedArchiver archivedDataWithRootObject: [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]];
    NSData* encryptData = [cookiesData zpAES256EncryptWithKey:[[ZPDataManager sharedInstance] stringByEnum:ZPStringTypePrivateKeyForCookieEncrypt]];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject: encryptData forKey: @"zp_zx_cookies"];
    [defaults synchronize];
}

- (void)zpClearCookiesFromCache {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"zp_zx_cookies"];
    [defaults synchronize];
}

- (void)zpRemoveAllStoredCredentials:(NSArray *)arrRequested
{
    [self removeCacheRequests:arrRequested];
    [self removeCookies];
    [self removeCredentials];
    
}

- (void) removeCacheRequests:(NSArray *)arrRequested {
    // Delete any cached URLrequests!
    NSURLCache *sharedCache = [NSURLCache sharedURLCache];
    //[sharedCache removeAllCachedResponses];
    if (IS_IOS_8_AND_GREATER()) {
        [sharedCache removeCachedResponsesSinceDate:[NSDate dateWithTimeIntervalSince1970:1451606400]];
    }
    
    [arrRequested forEach:^(NSURLRequest *request) {
        [sharedCache removeCachedResponseForRequest:request];
    }];
}

- (void)removeCookies {
    // Also delete all stored cookies!
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    [[cookieStorage cookies] forEach:^(NSHTTPCookie *cookie) {
        [cookieStorage deleteCookie:cookie];
    }];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)removeCredentials {
    NSURLCredentialStorage *sharedCredentialStorage = [NSURLCredentialStorage sharedCredentialStorage];
    NSDictionary<NSURLProtectionSpace *, NSDictionary<NSString *, NSURLCredential *> *> *credentialsDict = [sharedCredentialStorage allCredentials];
    
    [credentialsDict enumerateKeysAndObjectsUsingBlock:^(NSURLProtectionSpace * _Nonnull protectionSpace, NSDictionary<NSString *,NSURLCredential *> * _Nonnull obj, BOOL * _Nonnull stop) {
        [obj enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, NSURLCredential * _Nonnull cred, BOOL * _Nonnull stop) {
            [sharedCredentialStorage removeCredential:cred forProtectionSpace:protectionSpace];
        }];
    }];
}


@end
