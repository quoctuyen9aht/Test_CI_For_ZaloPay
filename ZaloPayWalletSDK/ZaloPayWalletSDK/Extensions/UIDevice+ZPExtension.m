//
//  UIDevice+ZPExtension.m
//  ZaloSDK
//
//  Created by Liem Vo Uy on 1/23/15.
//  Copyright (c) 2015-present VNG Corporation All rights reserved.
//

#import "UIDevice+ZPExtension.h"
#include <sys/utsname.h>
#include <sys/socket.h>
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_dl.h>
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <UIKit/UIKit.h>
#import "NSString+ZPExtension.h"
#import "ZaloPayWalletSDKLog.h"

#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

#define SuppressPerformSelectorLeakWarning(Stuff) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
Stuff \
_Pragma("clang diagnostic pop") \
} while (0)

@implementation UIDevice (ZPExtension)

#pragma mark - Get Device info
- (NSString *) zpMobileNetworkCode {
#if TARGET_IPHONE_SIMULATOR
    return @"45201";
#endif
    
    static NSString * mnc = nil;
    CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = networkInfo.subscriberCellularProvider;
    if (carrier.mobileCountryCode.length > 0 && carrier.mobileNetworkCode.length > 0) {
        mnc = [NSString stringWithFormat:@"%@%@", carrier.mobileCountryCode, carrier.mobileNetworkCode];
    }
    return mnc;
}

- (NSString *) zpDeviceModel {
    static NSString * model = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        int mib[2];
        size_t len;
        char *machine;
        
        mib[0] = CTL_HW;
        mib[1] = HW_MACHINE;
        sysctl(mib, 2, NULL, &len, NULL, 0);
        machine = malloc(len);
        sysctl(mib, 2, machine, &len, NULL, 0);
        
        model = [NSString stringWithCString:machine encoding:NSASCIIStringEncoding];
        free(machine);
        
    });
    
    return model;
}

- (NSString *) zpOsVersion {
    static NSString * osv = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        osv = [[UIDevice currentDevice] systemVersion];
    });
    
    return osv;
}

- (NSString *) zpAppVersion {
    static NSString * appVersion = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    });
    
    return appVersion;
}

- (NSString *)zpAppBuildVersion {
    static NSString * appBuildVersion = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        appBuildVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    });
    
    return appBuildVersion;
}

@end

