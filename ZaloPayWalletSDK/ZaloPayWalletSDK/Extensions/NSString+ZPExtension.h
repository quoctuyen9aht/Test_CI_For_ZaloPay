//
//  NSString+zpExtension.h
//  ZPSDK
//
//  Created by phungnx on 12/21/15.
//  Copyright (c) 2015-present VNG Corporation All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ZPDefine.h"

@interface NSString (ZPExtension)

- (NSString *)zpHmacForKey:(NSString *)key securityMode: (ZPSecurityMode) mode;
+ (NSString *)zpFormatNumberWithDilimeter: (long) number;
- (NSString *)zpFormatNumberWithDilimeter;
- (NSString *)zpRemoveDilimeter;
- (NSString *)zpFormatWithFormat:(NSArray *)format separator: (NSString **) separator;
- (NSString *)zpRemoveFormat:(NSArray *)format;
- (NSString *)zpTrimSpace;
- (NSString *)zpTrimStringWithMaxLength:(NSInteger)maxLenght;
- (NSMutableArray*)zpStringToArray;
+ (NSString *)zpEmptyIfNilString:(NSString *)str;
- (NSString *)zpEncodeURLString;
- (NSString *)zpDecodedURLString;
- (NSString *)zpMd5;
- (NSString *)zpSha1;
- (NSString *)zpSha256;
- (NSString *)zpRemovePercentEncoding;
- (BOOL)zpContainsString:(NSString *)str;
/**
 * Parses a URL query string into a dictionary.
 */
- (NSDictionary*) zpQueryDictionaryUsingEncoding:(NSStringEncoding)encoding;

/**s
 * Parses a URL, adds query parameters to its query, and re-encodes it as a new URL.
 */
- (NSString*) zpStringByAddingQueryDictionary:(NSDictionary*)query;
- (NSString*) zpStringByAddingQueryDictionary2:(NSDictionary*)query;

+ (NSString *) zpRequestWithBaseUrl:(NSString *)baseUrl
                              subUrl:(NSString *)subUrl
                              params:(NSDictionary *)params;


+ (NSString *) zpAES128EncryptData:(NSData *) data withKey:(NSString *)key;
+ (NSString *) zpAES128EncryptString:(NSString *) str withKey:(NSString *)key;
+ (NSString *) zpAES128DecryptData:(NSData *) data withKey:(NSString *)key;
+ (NSString *) zpAES128DecryptString:(NSString *) str withKey:(NSString *)key;
+ (NSString *)timeString:(long long)timeStamp;
+ (NSAttributedString *)attributedString:(NSString *)string;
@end
