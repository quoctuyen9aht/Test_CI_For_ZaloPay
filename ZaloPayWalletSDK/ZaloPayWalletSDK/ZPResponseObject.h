//
//  ZPResponseObject.h
//  ZPSDK
//
//  Created by ZaloPayTeam on 12/22/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZPDefine.h"

@interface ZPResponseObject : NSObject

@property (nonatomic, strong) NSString *message;
@property (nonatomic, assign) int errorCode;
@property (nonatomic, assign) int originalCode;
@property (nonatomic, assign) ZPErrorStep errorStep;
@property (nonatomic, strong) NSString *captchaImage;
@property (strong, nonatomic) NSString* suggestMessage;
@property (strong, nonatomic) NSArray *suggestAction;

- (void)copyFrom:(ZPResponseObject *)others;
@end
