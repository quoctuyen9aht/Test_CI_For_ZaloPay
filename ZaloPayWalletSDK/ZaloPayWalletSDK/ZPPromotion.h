//
//  ZPPromotion.h
//  ZaloPayWalletSDK
//
//  Created by Phuochh on 5/30/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZPVoucherPaymentConditions.h"

@interface ZPPromotion : NSObject

@property (nonatomic, strong) NSString *uid;
@property (nonatomic, strong) NSString *campaigncode;
@property (nonatomic, assign) NSInteger campaignId;
@property (nonatomic, assign) double minamount;
@property (nonatomic, assign) double discountamount;
@property (nonatomic, assign) double discountpercent;
@property (nonatomic, assign) long long discountAmountFinal;
@property (nonatomic, assign) long cap;
@property (nonatomic, assign) int discounttype;
@property (nonatomic, strong) NSString *minappversion;
@property (nonatomic, strong) NSString *promotionDescription;
@property (nonatomic)         double startdate;
@property (nonatomic)         double enddate;
@property (nonatomic, assign) double useconditionexpireddate;
@property (nonatomic, assign) BOOL isSelected;
@property (nonatomic, assign) BOOL isDisable;
@property (nonatomic, strong) id valueDisplayTableViewCell;
@property (nonatomic, strong) NSString *promotiontransid;

@property (nonatomic, strong) ZPVoucherPaymentConditions *paymentConditions;
- (instancetype)initDataWithDict:(NSDictionary *)dict;

@end
