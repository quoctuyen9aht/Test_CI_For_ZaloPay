//
//  ZPPromotionUtils.h
//  ZaloPayWalletSDK
//
//  Created by Phuochh on 6/1/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZPEndowUtils : NSObject
+ (NSString *)getValueStringPercentStyleWithValue:(long long)percentValue;
+ (NSAttributedString *)getValueStringVNDStyleWithValue:(long long)value;
+ (id)getStringValueTableViewCellWithValueType:(int)valuetype andValue:(long long)value;
+ (NSString *)getStringValueCollectionViewCellWithValueType:(int)valuetype andValue:(long long)value;
+ (void)showAlertNotUseEndow:(void (^) (void))handler;
@end
