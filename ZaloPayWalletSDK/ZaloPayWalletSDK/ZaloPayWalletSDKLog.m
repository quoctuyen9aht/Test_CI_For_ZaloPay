//
//  ZaloPayWalletSDKLog.m
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 4/13/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZaloPayWalletSDKLog.h"

const DDLogLevel zaloPayWalletSDKLogLevel = DDLogLevelInfo;
