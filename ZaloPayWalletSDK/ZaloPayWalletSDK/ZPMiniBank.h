//
//  ZPMiniBank.h
//  ZaloPayWalletSDK
//
//  Created by thi la on 1/17/17.
//  Copyright © 2017-present VNG. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, ZPMiniBankStatus) {
    ZPMiniBankStatus_Disable        = 0,
    ZPMiniBankStatus_Enable         = 1,
    ZPMiniBankStatus_Maintenance    = 2
};

@interface ZPMiniBank : NSObject
@property (strong, nonatomic) NSString * bankCode;
@property (assign, nonatomic) int bankFuntion;
@property (assign, nonatomic) ZPMiniBankStatus minibankStatus;
@property (assign, nonatomic) long long maintenancefrom;
@property (assign, nonatomic) long long maintenanceto;
@property (strong, nonatomic) NSString * maintenancemsg;
+ (instancetype)fromDictionary:(NSDictionary *)bankFunction;
@end
