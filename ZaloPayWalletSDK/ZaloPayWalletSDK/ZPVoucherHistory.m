//
//  ZPVoucherHistory.m
//  ZaloPayWalletSDK
//
//  Created by Phuoc Huynh on 8/24/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import "ZPVoucherHistory.h"
#import <ZaloPayCommon/R.h>
#import "ZPDefine.h"
#import "ZPVoucherPaymentConditions.h"

#import "ZPEndowUtils.h"

@implementation ZPVoucherHistory

- (instancetype)initDataWithDict:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        self.userid = [dict stringForKey:@"userid"];
        self.expireymd = [dict uint32ForKey:@"expireymd"];
        self.uid = [dict stringForKey:@"uid"];
        self.historystatus = [dict doubleForKey:@"historystatus"];
        self.givevoucherforuserid = [dict stringForKey:@"givevoucherforuserid"];
        
        self.givevoucherdate = [dict uint32ForKey:@"givevoucherdate"];
        self.vouchername = [dict stringForKey:@"vouchername"];
        self.vouchercode = [dict stringForKey:@"vouchercode"];
        self.usevoucherapptransid = [dict stringForKey:@"usevoucherapptransid"];
        self.voucherconfigid = [dict stringForKey:@"voucherconfigid"];
        
        self.voucherstatus = [dict uint32ForKey:@"voucherstatus"];
        self.discountamount = [dict uint32ForKey:@"discountamount"];
        
        self.valuetype = [dict uint32ForKey:@"valuetype"];
        self.value = [dict doubleForKey:@"value"];
        self.percentValue = [dict doubleForKey:@"value"];
        self.valueDisplayCollectionViewCell = [ZPEndowUtils getStringValueCollectionViewCellWithValueType:self.valuetype andValue:self.value];
        self.valueDisplayTableViewCell = [ZPEndowUtils getStringValueTableViewCellWithValueType:self.valuetype andValue:self.value];
        
        self.useconditonappid = [dict doubleForKey:@"useconditonappid"];
        
        self.useconditionpmcid = [dict stringForKey:@"useconditionpmcid"];
        self.useconditionminamount = [dict doubleForKey:@"useconditionminamount"];
        self.useconditionmaxamount = [dict doubleForKey:@"useconditionmaxamount"];
        
        self.useconditionexpireddate = [dict doubleForKey:@"useconditionexpireddate"];
        NSDate* expireddate = [NSDate dateWithTimeIntervalSince1970:self.useconditionexpireddate/1000 ];
        NSTimeInterval secondsBetween = [expireddate timeIntervalSinceDate:[NSDate date]];
        self.countExpiredDate = secondsBetween;
        self.countExpiredDateDisplay = [NSString timeIntervalToStringExpire:self.useconditionexpireddate];
        self.useconditionusedcountlimit = [dict doubleForKey:@"useconditionusedcountlimit"];
        self.cap = [dict uint32ForKey:@"cap"];
        self.voucherHistoryDescription = [dict stringForKey:@"description"];
        self.avatar = [dict stringForKey:@"avatar"];
        self.isSelected = NO;
        self.detail_url = [dict stringForKey:@"detail_url" defaultValue:@""];
        // waiting actual data - hard code to test
        self.paymentConditions = [[ZPVoucherPaymentConditions alloc] initDataWithDict:dict];
    }
    return self;
    
}

@end
