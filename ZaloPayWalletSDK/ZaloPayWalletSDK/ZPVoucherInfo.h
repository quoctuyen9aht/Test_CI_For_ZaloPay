//
//  ZPVoucherInfo.h
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/20/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ZPVoucherHistory;
@interface ZPVoucherInfo : NSObject
@property (nonatomic, copy, nullable) NSString *voucherSig;
@property (nonatomic, assign) NSInteger campaignId;
@property (nonatomic, assign) NSInteger discountAmount;
@property (nonatomic, assign) NSTimeInterval useVoucheTime;
@property (nonatomic, copy, nullable) NSString *voucherCode;
@property (nonatomic, strong, nullable) ZPVoucherHistory *voucherUsed;
//@property (nonatomic, copy, nullable) NSString *appTransId;
+ (instancetype _Nonnull) fromDic:(NSDictionary *_Nonnull)dic;
@end
