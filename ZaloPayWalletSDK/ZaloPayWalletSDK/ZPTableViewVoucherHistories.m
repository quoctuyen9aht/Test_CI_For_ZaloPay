//
//  ZPTableViewVoucherHistories.m
//  ZaloPayWalletSDK
//
//  Created by Phuoc Huynh on 9/7/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import "ZPTableViewVoucherHistories.h"
#import "ZPVoucherHistory.h"
#import "ZPVoucherHistoryCell.h"
#import "ZPPaymentManager.h"
#import "ZaloPayWalletSDKPayment.h"

#import "ZPPromotionManager.h"
#import "ZPPromotion.h"
@implementation ZPTableViewVoucherHistories

- (id)init {
    self = [super init];
    if (self) {
        [self setupView];
    }
    return self;
}

- (void)setupBill:(ZPBill *)bill {
    self.bill = bill;
    [self loadData];
}

- (void)setupView {
    self.backgroundColor = [UIColor whiteColor];
    self.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.separatorColor = [UIColor lineColor];
    self.layer.borderColor = [[UIColor lineColor] CGColor];
    self.layer.borderWidth = 1;
    self.dataSource = self;
    self.delegate = self;
    
    //footer
    self.footerView = [[UIView alloc] init];
    self.footerView.frame = CGRectMake(0, 0, self.frame.size.width, 44);
    
    UIView *lineView = [[UIView alloc] init];
    [self.footerView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.left.equalTo(12);
        make.right.equalTo(0);
        make.height.equalTo(1);
    }];
    lineView.backgroundColor = [UIColor lineColor];
    
    UILabel *lblMore = [[UILabel alloc] init];
    [self.footerView addSubview:lblMore];
    [lblMore mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(0);
        make.left.equalTo(12);
        make.height.equalTo(15);
    }];
    lblMore.font = [UIFont SFUITextRegularItalicWithSize:13];
    lblMore.textColor = [UIColor subText];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[R string_Voucher_view_more]];
    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:15.0f / 255.0f green:121.0f / 255.0f blue:222.0f / 255.0f alpha:1.0f] range:NSMakeRange(0, [R string_Voucher_view_more].length)];
    lblMore.attributedText = attributedString;
    
    ZPIconFontImageView *imgArrowRight = [[ZPIconFontImageView alloc] init];
    [self.footerView addSubview:imgArrowRight];
    [imgArrowRight mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(-12);
        make.height.equalTo(28);
        make.width.equalTo(14);
        make.centerY.equalTo(0);
    }];
    [imgArrowRight setIconFont:@"general_arrowright"];
    [imgArrowRight setIconColor:[UIColor hex_0xc7c7cc]];
    imgArrowRight.defaultView.font = [UIFont iconFontWithSize:14];
    imgArrowRight.backgroundColor = [UIColor whiteColor];
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(tapFooterView)];
    [self.footerView addGestureRecognizer:singleFingerTap];
    
    
}

- (void)loadData {
    ZPVoucherManager *voucherManager = [ZaloPayWalletSDKPayment sharedInstance].zpVoucherManager;
    NSMutableArray *cacheData = [voucherManager voucherCachesForBill:self.bill];
    if (!cacheData) {
        [self requestUpdateVoucherList];
        return;
    }
    self.arrVoucherHistories = cacheData;
    [self setupArrAllVouchersAndPromotions];
    
}

- (void)setupArrAllVouchersAndPromotions {
    self.arrAllVouchersAndPromotions = [NSMutableArray new];
    [self.arrAllVouchersAndPromotions addObjectsFromArray:self.arrVoucherHistories];
    [self.arrAllVouchersAndPromotions addObjectsFromArray:self.arrPromotions];
    [self resetSelectedStateAllVouchersAndPromotions];
}

- (void)requestUpdateVoucherList {
    ZPVoucherManager *manager = [ZaloPayWalletSDKPayment sharedInstance].zpVoucherManager;
    @weakify(self)
    self.arrVoucherHistories = [NSMutableArray new];
    [[manager loadVoucherForBill:self.bill] subscribeNext:^(NSArray *vouchers) {
        @strongify(self);
        if (vouchers.count == 0) {
            return;
        }
        [self.arrVoucherHistories addObjectsFromArray:vouchers];
        [manager saveVouchers:self.arrVoucherHistories forBill:self.bill];
        [self setupArrAllVouchersAndPromotions];
    }];
    
}

- (void)resetSelectedStateAllVouchersAndPromotions {
    for (ZPVoucherHistory *voucher in self.arrVoucherHistories) {
        voucher.isSelected = NO;
    }
    for (ZPPromotion *promotion in self.arrPromotions) {
        promotion.isSelected = NO;
    }
}
- (void)resetStateAllVouchersAndPromotions {
    for (ZPVoucherHistory *voucher in self.arrVoucherHistories) {
        voucher.isSelected = NO;
        voucher.isDisable = NO;
        voucher.paymentConditions.voucherConditionPPAmount = ZPVoucherConditionPPAmountCorrect;
    }
    for (ZPPromotion *promotion in self.arrPromotions) {
        promotion.isSelected = NO;
        promotion.isDisable = NO;
        promotion.paymentConditions.voucherConditionPPAmount = ZPVoucherConditionPPAmountCorrect;
    }
}


#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arrAllVouchersAndPromotions.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 77;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id data = [self.arrAllVouchersAndPromotions safeObjectAtIndex:indexPath.row];
    ZPVoucherHistoryCell *cell = [ZPVoucherHistoryCell defaultCellForTableView:tableView];
    if ([data isKindOfClass:[ZPVoucherHistory class]]) {
        [cell setVoucherHistoryData:data withBill:self.bill];
    }else{
        [cell setPromotionData:data withBill:self.bill];
    }
    
    return cell;
}

#pragma mark UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    id data = [self.arrAllVouchersAndPromotions safeObjectAtIndex:indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:false];
    if ([data isKindOfClass:[ZPVoucherHistory class]]) {
        ZPVoucherHistory *voucherData = data;
        if (voucherData.isDisable) {
            return;
        }
        [self.zpTableViewVoucherHistoriesDelegate didSelectRowVoucher:voucherData atIndexPath:indexPath];
    }else if ([data isKindOfClass:[ZPPromotion class]]) {
        ZPPromotion *promotionData = data;
        if (promotionData.isDisable) {
            return;
        }
        [self.zpTableViewVoucherHistoriesDelegate didSelectRowPromotion:promotionData atIndexPath:indexPath];
    }
    
}

#pragma mark tapGesture

- (void) tapFooterView {
    [self.zpTableViewVoucherHistoriesDelegate tapFooterView];
}
@end

