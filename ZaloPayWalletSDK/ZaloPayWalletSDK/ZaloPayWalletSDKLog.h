//
//  ZaloPayWalletSDKLog.h
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 4/13/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//


#import <CocoaLumberjack/CocoaLumberjack.h>
extern const DDLogLevel zaloPayWalletSDKLogLevel;
#define ddLogLevel  zaloPayWalletSDKLogLevel
