//
//  ZPPromotion.m
//  ZaloPayWalletSDK
//
//  Created by Phuochh on 5/30/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import "ZPPromotion.h"
#import "ZPEndowUtils.h"

@implementation ZPPromotion
- (instancetype)initDataWithDict:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        self.campaigncode = [dict stringForKey:@"campaigncode"];
        self.campaignId = [dict intForKey:@"campaignid"];
        self.minamount = [dict uint32ForKey:@"minamount"];
        self.discountamount = [dict uint32ForKey:@"discountamount"];
        self.discountpercent = [dict uint32ForKey:@"discountpercent"];
        self.cap = [dict uint32ForKey:@"cap"];
        self.discounttype = [dict uint32ForKey:@"discounttype"];
        self.minappversion = [dict stringForKey:@"minappversion"];
        self.startdate = [dict doubleForKey:@"startdate"];
        self.enddate = [dict doubleForKey:@"enddate"];
        self.useconditionexpireddate = self.enddate;
        self.promotionDescription = [dict stringForKey:@"description"];
        self.isSelected = NO;
        self.discountAmountFinal = self.discounttype == ZPPromotionTypePercent ? self.discountpercent : self.discountamount;
        self.valueDisplayTableViewCell = [ZPEndowUtils getStringValueTableViewCellWithValueType:self.discounttype andValue:self.discountAmountFinal];
        self.paymentConditions = [[ZPVoucherPaymentConditions alloc] initDataWithPromotionDict:dict];
    }
    return self;
    
}
@end
