//
//  ZPBank.m
//  ZPSDK
//
//  Created by ZaloPayTeam on 12/23/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import "ZPBank.h"
#import "ZPResourceManager.h"

@implementation ZPBank

+ (instancetype)initBankWithData:(NSDictionary *)data {
    ZPBank *bank = [[ZPBank alloc] initWithImageName: [ZPResourceManager bankImageFromBankCode: [data stringForKey:@"tpebankcode"]]
                                            bankCode:[data stringForKey:@"tpebankcode"]
                                            bankName:[data stringForKey:@"name"]
                                            fullName:[data stringForKey:@"fullname"]
                                            bankType:[data intForKey:@"banktype"]
                                                type:[data stringForKey:@"type"]
                                             otpType:[data stringForKey:@"otptype"]
                                       interfaceType:[data intForKey:@"interfacetype"]
                                          requireOTP:[data intForKey:@"requireotp"]
                                              status:[data intForKey:@"status"]
                                      maintenanceMsg:[data stringForKey:@"maintenancemsg"]
                                         supportType:[data intForKey:@"supporttype"]
                                        loginbankurl:[data stringForKey:@"loginbankurl"]
                                       bankFunctions:[[data arrayForKey:@"functions"] mutableCopy]
                                    maintainenanceTo:[data uint64ForKey:@"maintainenanceto"]
                                             bankOPT:[data intForKey:@"bank_opt"]
                                       minAppVersion:[data stringForKey:@"minappversion"]
                                         dispayOrder:[data intForKey:@"displayorder"]];
    bank.supportType = [ZPBank bankSupportType:bank.bankFunctions];
    return bank;
}
// params supporttype in api getbanklist don't support by version, need to know bank supporttype using this method
// bank function only return ZPBankFuntionMapCard : 301 or ZPBankFuntionMapAccount: 302 -> if return both of this server return wrong value
+ (ZPSDK_BankType)bankSupportType:(NSArray*)bankFunctions {
    for (NSDictionary *bankFunction in bankFunctions) {
        ZPBankFuntion currentType = [bankFunction intForKey:@"bankfunction"];
        if (currentType == ZPBankFuntionMapCard) {
            return ZPSDK_BankType_Card;
        }
        if (currentType == ZPBankFuntionMapAccount) {
            return ZPSDK_BankType_Account;
        }
    }
    return ZPSDK_BankType_Card;
}

- (instancetype)initWithImageName:(NSString *) imgName
                         bankCode:(NSString *) bcode
                         bankName:(NSString *)bankName
                         fullName:(NSString *)fullName
                         bankType:(int)bankType
                             type:(NSString *)type
                          otpType:(NSString *)otpType
                    interfaceType:(int)interfaceType
                       requireOTP:(int)requireOTP
                           status: (int)status
                   maintenanceMsg:(NSString *)maintenanceMsg
                      supportType: (int) supportType
                     loginbankurl:(NSString *)loginbankurl
                    bankFunctions:(NSMutableArray*)bankFunctions
                 maintainenanceTo:(uint64_t)maintainenanceTo
                          bankOPT: (int) bankOPT
                    minAppVersion:(NSString *)minAppVersion
                      dispayOrder:(int) dispayOrder

{
    self = [super init];
    if (self) {
        _imageName = imgName;
        _bankCode = bcode;
        _bankName = bankName;
        _fullName = fullName;
        _bankType = bankType;
        _type = type;
        _otpType = otpType;
        _interfaceType = interfaceType;
        _requireOTP = requireOTP;
        _status = status;
        _maintenanceMsg = maintenanceMsg;
        _supportType = supportType;
        _loginbankurl = loginbankurl;
        _bankFunctions = bankFunctions;
        _maintainenanceTo = maintainenanceTo;
        _bankOPT = bankOPT;
        _minAppVersion = minAppVersion;
        _dispayOrder = dispayOrder;
        
    }
    return self;
}

- (ZPBankCode) bankCodeInEnum {
    return [ZPResourceManager convertBankCodeToEnum:self.bankCode];
}

+ (ZPBank *)copyFrom:(ZPBank *)bank {
    ZPBank *newBank = [ZPBank new];
    newBank.imageName = bank.imageName;
    newBank.bankCode = bank.bankCode;
    newBank.bankName = bank.bankName;
    newBank.fullName = bank.fullName;
    newBank.bankType = bank.bankType;
    newBank.type = bank.type;
    newBank.otpType = bank.otpType;
    newBank.interfaceType = bank.interfaceType;
    newBank.requireOTP = bank.requireOTP;
    newBank.status = bank.status;
    newBank.maintenanceMsg = bank.maintenanceMsg;
    newBank.supportType = bank.supportType;
    newBank.loginbankurl = bank.loginbankurl;
    newBank.bankFunctions = bank.bankFunctions;
    newBank.maintainenanceTo = bank.maintainenanceTo;
    newBank.bankOPT = bank.bankOPT;
    newBank.minAppVersion = bank.minAppVersion;
    newBank.dispayOrder = bank.dispayOrder;
    return newBank;
}
@end

