//
//  ZMPPaymentShare.m
//  ZaloPayWalletSDK
//
//  Created by Nguyen Xuan Phung on 5/4/16.
//  Copyright © 2016 VNG. All rights reserved.
//

#import "ZMPPaymentShare.h"
#import "NSMutableArray+ZMPExtension.h"
#import "NSMutableDictionary+ZMPExtension.h"
#import "ZMPPaymentChannel.h"

#define KEY_CN_TYPE @"ch_type"
#define KEY_CN_CHANNEL_TYPE @"ch_channel_type"
#define KEY_CN_CHANNEL_ID @"cn_channel_id"
#define KEY_CN_CHANNEL_NAME @"cn_channel_name"
#define KEY_CN_IS_ENABLE @"cn_channel_is_enable"
#define KEY_CN_MIN_PP_AMOUNT @"cn_min_pp_amount"
#define KEY_CN_MAX_PP_AMOUNT @"cn_max_pp_amount"
#define KEY_CN_GROUP_ID @"cn_group_id"
#define KEY_APP_ID @"app_key_id"
#define KEY_APP_NAME @"app_key_name"
#define KEY_APP_LOGO @"app_key_logo"
#define KEY_APP_STATUS @"app_key_status"

@implementation ZMPPaymentShare
@end







