//
//  ZPBank.h
//  ZPSDK
//
//  Created by ZaloPayTeam on 12/23/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZPConfig.h"
#import <ZaloPayCommon/CodableObject.h>
#import "ZPDefine.h"

@interface ZPBank : CodableObject
@property (strong, nonatomic) NSString * imageName;
@property (strong, nonatomic) NSString * bankCode;
@property (strong, nonatomic) NSString * bankName;
@property (strong, nonatomic) NSString * fullName;
@property (assign, nonatomic) uint64_t maintainenanceTo;
@property (assign, nonatomic) int bankType;
@property (strong, nonatomic) NSString * type;
@property (strong, nonatomic) NSString * otpType;
@property (assign, nonatomic) int interfaceType;
@property (assign, nonatomic) int requireOTP;
@property (assign, nonatomic) int status;
@property (assign, nonatomic) int allowithdraw;
@property (strong, nonatomic) NSString *withdrawtimemsg;
@property (assign, nonatomic) double feeRate;
@property (assign, nonatomic) double minFee;
@property (strong, nonatomic) NSString * feeCalType;
@property (strong, nonatomic) NSString * maintenanceMsg;
@property (assign, nonatomic) ZPSDK_BankType supportType; //1. Bank card, 2. Bank account
@property (strong, nonatomic) NSMutableArray * bankFunctions;
@property (strong, nonatomic) NSString * loginbankurl;
@property (assign, nonatomic) int bankOPT;
@property (strong, nonatomic) NSString *minAppVersion;
@property (assign, nonatomic) int dispayOrder;

@property (strong, nonatomic) NSString *detectString;
@property (strong, nonatomic) NSString *tfString;

+ (instancetype)initBankWithData:(NSDictionary*)data;
+ (ZPSDK_BankType)bankSupportType:(NSArray*)bankFunctions;
+ (ZPBank*)copyFrom:(ZPBank*)bank;
- (ZPBankCode)bankCodeInEnum;
@end

