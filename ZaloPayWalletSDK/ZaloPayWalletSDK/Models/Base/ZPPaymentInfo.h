//
//  ZPPaymentInfo.h
//  ZaloPaySDK
//
//  Created by ZaloPayTeam on 12/22/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZPDefine.h"

@protocol KYCGetInforProtocol<NSObject>
- (void)setKYC:(NSDictionary *)infor;
- (NSDictionary *)kycInfor;
@end

#pragma mark - ZPPaymentInfo
@interface ZPPaymentInfo : NSObject<NSCoding>
@property (strong, nonatomic) NSString * zpTransID;
@property (strong, nonatomic) NSString * pin;
@property (assign, nonatomic) BOOL isMapcard;
@property (assign, nonatomic) ZPPaymentMethodType method;
@property (strong, nonatomic) NSString * authenValue;
@property (assign, nonatomic) NSInteger step;
@end

#pragma mark - ZPAtmCard
@interface ZPAtmCard : ZPPaymentInfo<KYCGetInforProtocol>
@property (strong, nonatomic) NSString * bankCode;
@property (assign, nonatomic) NSInteger bankCodeToEnum;
@property (strong, nonatomic) NSString * bankName;
@property (strong, nonatomic) NSString * mac;
@property (strong, nonatomic) NSString * cardNumber;
@property (strong, nonatomic) NSString * cardHolderName;
@property (strong, nonatomic) NSString * cardNumberHash;
@property (strong, nonatomic) NSString * validTo;
@property (strong, nonatomic) NSString * validFrom;
@property (strong, nonatomic) NSString * type;
@property (copy, nonatomic) NSString * otpType;
@property (strong, nonatomic) NSString * cardPassword;
@property (strong, nonatomic) NSString * captchaBase64;
@property (strong, nonatomic) NSString * captcha;
@property (strong, nonatomic) NSString * statusUrl;
@property (strong, nonatomic) NSString * statusParams;
@property (strong, nonatomic) NSString * cardUsername;
@property (strong, nonatomic) NSString * ocrResponse;
@property (assign, nonatomic) int interfaceType;
@property (assign, nonatomic) int requireOTP;
@property (assign, nonatomic) int atmFlag;
@property (assign, nonatomic) long amount;
@property (assign, nonatomic) NSInteger atmType;
@property (assign, nonatomic) BOOL isVicomDirect;
- (NSInteger) bankCodeInEnum;
@end

#pragma mark - ZPZCard
@interface ZPZCard : ZPPaymentInfo
@property (strong, nonatomic) NSString *cardNumber;
@property (strong, nonatomic) NSString *cardSeri;
@property (strong, nonatomic) NSString *ocrResponse;
@end

#pragma mark - ZPCreditCard
@interface ZPCreditCard : ZPPaymentInfo<KYCGetInforProtocol>
@property (strong, nonatomic) NSString * ccBankCode;
@property (assign, nonatomic) NSInteger ccbankCodeToEnum;
@property (strong, nonatomic) NSString * ccBankName;
@property (strong, nonatomic) NSString * statusUrl;
@property (strong, nonatomic) NSString * src;
@property (assign, nonatomic) NSTimeInterval ts;
@property (strong, nonatomic) NSString * cardNumber;
@property (strong, nonatomic) NSString * cardMonth;
@property (strong, nonatomic) NSString * cardYear;
@property (strong, nonatomic) NSString * cardValidFrom;
@property (strong, nonatomic) NSString * cardValidTo;
@property (strong, nonatomic) NSString * cardCVV;
@property (strong, nonatomic) NSString * cardName;
@property (strong, nonatomic) NSString * userID;
@property (assign, nonatomic) int requireOTP;
@end

#pragma mark - ZPAtmCard
@interface ZPIbankingAccount : ZPPaymentInfo
@property (strong, nonatomic) NSString * bankCode;
@property (strong, nonatomic) NSString * bankName;
@property (strong, nonatomic) NSString * mac;
@property (strong, nonatomic) NSString * cardNumber;
@property (strong, nonatomic) NSString * accountHolderName;
@property (strong, nonatomic) NSString * cardNumberHash;
@property (strong, nonatomic) NSString * validTo;
@property (strong, nonatomic) NSString * validFrom;
@property (strong, nonatomic) NSString * type;
@property (copy, nonatomic) NSString * otpType;
@property (strong, nonatomic) NSString * cardPassword;
@property (strong, nonatomic) NSString * captchaBase64;
@property (strong, nonatomic) NSString * captcha;
@property (strong, nonatomic) NSString * statusUrl;
@property (strong, nonatomic) NSString * statusParams;
@property (strong, nonatomic) NSString * cardUsername;
@property (strong, nonatomic) NSString * ocrResponse;
@property (assign, nonatomic) int interfaceType;
@property (assign, nonatomic) int requireOTP;
@property (assign, nonatomic) int atmFlag;
@property (assign, nonatomic) long amount;
@property (assign, nonatomic) NSInteger atmType;

- (NSInteger) bankCodeInEnum;
@end



