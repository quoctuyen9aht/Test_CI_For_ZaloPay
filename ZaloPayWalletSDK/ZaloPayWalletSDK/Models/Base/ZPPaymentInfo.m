//
//  ZPPaymentInfo.m
//  ZPSDK
//
//  Created by ZaloPayTeam on 12/22/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import "ZPPaymentInfo.h"
#import "ZPResourceManager.h"
#import "ZaloPayWalletSDKLog.h"
#import <StoreKit/StoreKit.h>
#import "ZPConfig.h"
#pragma mark - ZPPaymentInfo
@implementation ZPPaymentInfo

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    return self;
}

- (void)setAuthenValue:(NSString *)authenValue{
    if (authenValue != NULL && [authenValue isEqualToString:@""] == false) {
        DDLogInfo(@"authenValue: %@",authenValue);
        authenValue = [authenValue stringByReplacingOccurrencesOfString:@"'" withString:@"\'"];
    }
    _authenValue = authenValue;
}

@end


#pragma mark - ZPAtmCard
@interface ZPAtmCard()
@property (strong, nonatomic) NSDictionary *inforKYC;
@end
@implementation ZPAtmCard

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.bankCode forKey:@"bankcode"];
    [aCoder encodeObject:self.bankName forKey:@"bankname"];
    [aCoder encodeObject:self.cardNumber forKey:@"number"];
    [aCoder encodeObject:self.cardNumberHash forKey:@"cardnumberhash"];
    [aCoder encodeObject:self.cardHolderName forKey:@"cardholdername"];
    [aCoder encodeObject:self.validTo forKey:@"validto"];
    [aCoder encodeObject:self.validFrom forKey:@"validfrom"];
    [aCoder encodeObject:@(self.atmType) forKey:@"atmtype"];
    [aCoder encodeObject:@(self.atmFlag) forKey:@"atmflag"];
    [aCoder encodeObject:self.zpTransID forKey:@"zptransid"];
    
}

- (void)setOtpType:(NSString *)otpType {
    _otpType = otpType;
}

- (void)setAuthenValue:(NSString *)authenValue {
    super.authenValue = authenValue;
}

- (void)setZpTransID:(NSString *)zpTransID {
    super.zpTransID = zpTransID;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    self.bankCode = [aDecoder decodeObjectForKey:@"bankcode"];
    self.bankName = [aDecoder decodeObjectForKey:@"bankname"];
    self.cardNumber = [aDecoder decodeObjectForKey:@"number"];
    self.cardNumberHash = [aDecoder decodeObjectForKey:@"cardnumberhash"];
    self.cardHolderName = [aDecoder decodeObjectForKey:@"cardholdername"];
    self.validTo = [aDecoder decodeObjectForKey:@"validto"];
    self.validFrom = [aDecoder decodeObjectForKey:@"validfrom"];
    self.atmType = [[aDecoder decodeObjectForKey:@"atmtype"] intValue];
    self.atmFlag = [[aDecoder decodeObjectForKey:@"atmflag"] intValue];
    self.zpTransID = [aDecoder decodeObjectForKey:@"zptransid"];
    self.method = ZPPaymentMethodTypeAtm;
    return self;
}

- (id)init{
    self = [super init];
    if (self) {
        self.bankCode = @"";
        self.bankName = @"";
        self.amount = 0;
        self.zpTransID = @"";
        self.mac = @"";
        self.cardNumber = @"";
        self.cardHolderName = @"";
        self.cardPassword = @"";
        self.validFrom = @"";
        self.validTo = @"";
        self.method = ZPPaymentMethodTypeAtm;
    }
    return self;
}

-(void)setBankCode:(NSString *)bankCode{
    _bankCode = bankCode;
    _bankCodeToEnum = [ZPResourceManager convertBankCodeToEnum:self.bankCode];
}

- (void)setCardHolderName:(NSString *)cardHolderName{
    if (cardHolderName != NULL && [cardHolderName isEqualToString:@""] == false) {
        cardHolderName = [cardHolderName stringByReplacingOccurrencesOfString:@"'" withString:@"\\'"];
    }
    
    _cardHolderName = cardHolderName;
}

- (NSMutableDictionary *)dynamicParams{
    NSMutableDictionary * params = [[NSMutableDictionary alloc] init];
    
    if (self.bankCode.length > 0)
        [params setObject:self.bankCode forKey:@"bankcode"];
    
    if (self.bankName.length > 0)
        [params setObject:self.bankName forKey:@"bankname"];
    
    if (self.cardNumber.length > 0)
        [params setObject:self.cardNumber forKey:@"cardnumber"];
    
    if (self.cardHolderName.length > 0)
        [params setObject:self.cardHolderName  forKey:@"cardholdername"];
    
    if (self.validTo.length > 0)
        [params setObject:self.validTo forKey:@"validto"];
    
    if (self.validFrom.length > 0)
        [params setObject:self.validFrom forKey:@"validfrom"];
    
    if (self.cardPassword.length > 0)
        [params setObject:self.cardPassword forKey:@"cardpass"];
    
    if (self.authenValue.length > 0)
        [params setObject:self.authenValue forKey:@"authenvalue"];
    
    if (self.captcha.length > 0)
        [params setObject:self.captcha forKey:@"captcha"];
    
    
    [params setObject:[NSString stringWithFormat:@"%ld", self.amount] forKey:@"chargeamount"];
    
    
    return params;
}

- (NSInteger) bankCodeInEnum{
    return [ZPResourceManager convertBankCodeToEnum:self.bankCode];
}
- (void)setKYC:(NSDictionary *)infor {
    self.inforKYC = infor;
}
- (NSDictionary *)kycInfor {
    NSDictionary *result = [self.inforKYC copy];
    // reset
    self.inforKYC = nil;
    return result;
}

@end


#pragma mark - ZPZCard
@implementation ZPZCard
@end


#pragma mark - ZPCreditCard
@interface ZPCreditCard()
@property (strong, nonatomic) NSDictionary *inforKYC;
@end
@implementation ZPCreditCard
- (instancetype)init {
    self = [super init];
    self.method = ZPPaymentMethodTypeCreditCard;
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.ccBankCode forKey:@"bankCode"];
    [aCoder encodeObject:self.ccBankName forKey:@"bankName"];
    [aCoder encodeObject:self.cardNumber forKey:@"cardNumber"];
    [aCoder encodeObject:self.cardName forKey:@"cardName"];
    [aCoder encodeObject:self.zpTransID forKey:@"zpTransID"];
    [aCoder encodeObject:self.statusUrl forKey:@"statusUrl"];
    [aCoder encodeDouble:self.ts forKey:@"timestamp"];
    [aCoder encodeObject:self.src forKey:@"src"];
    [aCoder encodeObject:self.userID forKey:@"userID"];
    [aCoder encodeObject:[NSNumber numberWithInt:self.requireOTP] forKey:@"requireotp"];

}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
  
    self.ccBankCode = [aDecoder decodeObjectForKey:@"bankCode"];
    self.ccBankName = [aDecoder decodeObjectForKey:@"bankName"];
    self.cardNumber = [aDecoder decodeObjectForKey:@"cardNumber"];
    self.cardName = [aDecoder decodeObjectForKey:@"cardName"];
    self.zpTransID = [aDecoder decodeObjectForKey:@"zpTransID"];
    self.statusUrl = [aDecoder decodeObjectForKey:@"statusUrl"];
    self.ts = [aDecoder decodeDoubleForKey:@"timestamp"];
    self.src = [aDecoder decodeObjectForKey:@"src"];
    self.userID = [aDecoder decodeObjectForKey:@"userID"];
    self.requireOTP = [[aDecoder decodeObjectForKey:@"requireotp"] intValue];
    self.method = ZPPaymentMethodTypeCreditCard;
    return self;
}
-(void)setCcBankCode:(NSString *)ccBankCode{
    _ccBankCode = ccBankCode;
    _ccbankCodeToEnum = [ZPResourceManager convertCCBankCodeToEnum:_ccBankCode];
    }

- (void)setKYC:(NSDictionary *)infor {
    self.inforKYC = infor;
}
- (NSDictionary *)kycInfor {
    NSDictionary *result = [self.inforKYC copy];
    // reset
    self.inforKYC = nil;
    return result;
}

@end

@implementation ZPIbankingAccount

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.bankCode forKey:@"bankcode"];
    [aCoder encodeObject:self.bankName forKey:@"bankname"];
    [aCoder encodeObject:self.cardNumber forKey:@"number"];
    [aCoder encodeObject:self.cardNumberHash forKey:@"cardnumberhash"];
    [aCoder encodeObject:self.accountHolderName forKey:@"cardholdername"];
    [aCoder encodeObject:self.validTo forKey:@"validto"];
    [aCoder encodeObject:self.validFrom forKey:@"validfrom"];
    [aCoder encodeObject:@(self.atmType) forKey:@"atmtype"];
    [aCoder encodeObject:@(self.atmFlag) forKey:@"atmflag"];
    [aCoder encodeObject:self.zpTransID forKey:@"zptransid"];
    
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    self.bankCode = [aDecoder decodeObjectForKey:@"bankcode"];
    self.bankName = [aDecoder decodeObjectForKey:@"bankname"];
    self.cardNumber = [aDecoder decodeObjectForKey:@"number"];
    self.cardNumberHash = [aDecoder decodeObjectForKey:@"cardnumberhash"];
    self.accountHolderName = [aDecoder decodeObjectForKey:@"cardholdername"];
    self.validTo = [aDecoder decodeObjectForKey:@"validto"];
    self.validFrom = [aDecoder decodeObjectForKey:@"validfrom"];
    self.atmType = [[aDecoder decodeObjectForKey:@"atmtype"] intValue];
    self.atmFlag = [[aDecoder decodeObjectForKey:@"atmflag"] intValue];
    self.zpTransID = [aDecoder decodeObjectForKey:@"zptransid"];
    self.method = ZPPaymentMethodTypeIbanking;
    return self;
}

- (id)init{
    self = [super init];
    if (self) {
        
        self.bankCode = @"";
        self.bankName = @"";
        self.amount = 0;
        self.zpTransID = @"";
        self.mac = @"";
        self.cardNumber = @"";
        self.accountHolderName = @"";
        self.cardPassword = @"";
        self.validFrom = @"";
        self.validTo = @"";
        self.method = ZPPaymentMethodTypeIbanking;
    }
    return self;
}

- (void)setCardHolderName:(NSString *)cardHolderName{
    if (cardHolderName != NULL && [cardHolderName isEqualToString:@""] == false) {
        cardHolderName = [cardHolderName stringByReplacingOccurrencesOfString:@"'" withString:@"\\'"];
    }
    
    _accountHolderName = cardHolderName;
}

- (NSInteger) bankCodeInEnum{
    return [ZPResourceManager convertBankCodeToEnum:self.bankCode];
}

@end

