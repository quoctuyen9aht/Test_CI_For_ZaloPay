//
//  ZPPaymentMethod.m
//  ZPSDK
//
//  Created by ZaloPayTeam on 12/25/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import "ZPPaymentMethod.h"
#import "ZPSDKStringConstants.h"
#import "ZPMiniBank.h"
#import "ZPDataManager.h"
#import "ZPPaymentChannel.h"
#import "ZaloPayWalletSDKPayment.h"

@implementation ZPPaymentMethod
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.isShowHintTextPromotions = NO;
    }
    return self;
}
- (long)calculateChargeWith:(long)amount {
    long totalChargeFeeAmount = 0;
    if ([self.feeCalType isEqual: stringFeeCal]) {
        double mFee = MAX(self.minFee, 0);
        double fRate = MAX(self.feeRate, 0);
        totalChargeFeeAmount = (amount * fRate) + mFee;
    } else {
        long fee = self.feeRate * amount;
        totalChargeFeeAmount = MAX(MAX(self.minFee, 0), fee);
    }
    self.totalChargeFee = totalChargeFeeAmount;
    self.totalChargeAmount = totalChargeFeeAmount + amount;
    return self.totalChargeAmount;
}

-(BOOL)isEnable{
//    NSAssert(self.isTotalChargeGreaterThanBalance, nil);
//    NSAssert(self.isTotalChargeGreaterThanMaxAmount, nil);
    if (self.support == 0 ||
        self.status == ZPAPPMaintenance ||
        self.support == ZPAPPMaintenance ||
        self.miniBank == nil ||
        self.miniBank.minibankStatus == ZPMiniBankStatus_Maintenance ||
        !self.isSupportAmount ||
        [[ZPDataManager sharedInstance] chekMinAppVersion:self.minAppVersion] ||
        self.isTotalChargeGreaterThanBalance ||
        self.isTotalChargeGreaterThanMaxAmount) {
        return NO;
    }
    return YES;
}

- (void)setMinAppVersion:(NSString *)minAppVersion {
    _minAppVersion = minAppVersion;
    self.isAppVersionNotSupport = [minAppVersion length] > 0 ? [[ZPDataManager sharedInstance] chekMinAppVersion:minAppVersion] : NO;
}

- (BOOL)isEqual:(id)other {
    if ([other isKindOfClass:[self class]]) {
        ZPPaymentMethod *otherMethod = other;
        return self.methodType == otherMethod.methodType;;
    }
    return NO;
}

- (NSString *)paymenMethodId {
    if (self.defaultChannel) {
        return nil;
    }
    if (self.savedCard) {
        return [NSString stringWithFormat:@"%@_%@_%@",self.bankCode,self.savedCard.first6CardNo,self.savedCard.last4CardNo];
    }
    if (self.savedBankAccount) {
        return [NSString stringWithFormat:@"%@_%@_%@",self.bankCode, self.savedBankAccount.firstAccountNo, self.savedBankAccount.lastAccountNo];
    }
    return @"ZaloPayWallet";
}

- (BOOL)isCCCard {
    return  [[ZPDataManager sharedInstance] isCCCard:self.bankCode];
}

- (BOOL)isCCDebitCard {
    return  [[ZPDataManager sharedInstance] isCCDebitCard:self.bankCode];
}

- (BOOL)isNewMethod {
    if (self.savedCard) {
        NSString * savedCardnumber = [ZPDataManager sharedInstance].savedCardNumber;
        if (savedCardnumber.length <= 0) {
            return NO;
        }
        return [savedCardnumber hasPrefix:self.savedCard.first6CardNo] && [savedCardnumber hasSuffix:self.savedCard.last4CardNo];
    }
    
    if (self.savedBankAccount) {
        return [ZPDataManager sharedInstance].isExistNewBankAccount;
    }
    return NO;
}

@end


@implementation ZPNewAtmCardMethod
@end

@implementation ZPZaloPayWalletMethod
@end

@implementation ZPNewIBankingAccount
@end

@implementation ZPUserAtmCardMethod
@end

@implementation ZPUserCreditCardMethod
@end

@implementation ZPUserIBankingAccount
@end


