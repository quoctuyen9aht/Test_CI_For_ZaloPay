//
//  ZPPaymentMethod.h
//  ZPSDK
//
//  Created by ZaloPayTeam on 12/25/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZPDefine.h"
@class ZPChannel;
@class ZPSavedCard;
@class ZPSavedBankAccount;
@class ZPMiniBank;

@interface ZPPaymentMethod : NSObject
@property (strong, nonatomic) NSString *methodName;
@property (assign, nonatomic) double feeRate;
@property (assign, nonatomic) double minFee;
@property (strong, nonatomic) NSString * feeCalType;
@property (strong, nonatomic) NSString *methodIcon;
@property (strong, nonatomic) NSString *discountIcon;
@property (assign, nonatomic) ZPPaymentMethodType methodType;
@property (strong, nonatomic) ZPChannel * channel;
@property (strong, nonatomic) ZPSavedCard *savedCard;
@property (strong, nonatomic) ZPSavedBankAccount *savedBankAccount;
@property (assign, nonatomic) BOOL userPLPAllow;
@property (assign, nonatomic) int requireOTP;
@property (assign, nonatomic) int status;
@property (assign, nonatomic) long amountRequireOTP;
@property (assign, nonatomic) long totalChargeFee;
@property (assign, nonatomic) int support;
@property (assign, nonatomic) BOOL isSupportAmount;
@property (assign, nonatomic) BOOL isTotalChargeGreaterThanBalance;
@property (assign, nonatomic) BOOL isTotalChargeGreaterThanMaxAmount;
@property (assign, nonatomic) long totalChargeAmount;
@property (strong, nonatomic) ZPMiniBank *miniBank;
@property (strong, nonatomic) NSString *minAppVersion;
@property (assign, nonatomic) BOOL isAppVersionNotSupport;
@property (strong, nonatomic) NSString * bankCode;
@property (assign, nonatomic) int inamounttype;
@property (assign, nonatomic) int overamounttype;
@property (assign, nonatomic) BOOL defaultChannel;
@property (strong, nonatomic) NSString *errorMessage;
@property (assign, nonatomic) int displayOrder;
@property (assign, nonatomic) BOOL isShowHintTextPromotions;
@property (assign, nonatomic) BOOL isHaveAvailableEndow;

- (long)calculateChargeWith:(long)amount;
- (BOOL)isEnable;
- (NSString *)paymenMethodId;
- (BOOL)isCCCard;
- (BOOL)isCCDebitCard;
- (BOOL)isNewMethod;
@end

@interface ZPNewAtmCardMethod : ZPPaymentMethod
@end

@interface ZPNewIBankingAccount : ZPPaymentMethod
@end

@interface ZPZaloPayWalletMethod : ZPPaymentMethod
@end

@interface ZPUserAtmCardMethod : ZPPaymentMethod
@end

@interface ZPUserCreditCardMethod : ZPPaymentMethod
@end

@interface ZPUserIBankingAccount : ZPPaymentMethod
@end





