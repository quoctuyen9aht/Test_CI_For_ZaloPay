//
//  ZPPaymentChannel.m
//  ZPSDK
//
//  Created by phungnx on 12/22/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import "ZPPaymentChannel.h"
#import "ZPConfig.h"
#import "ZPResourceManager.h"
#import "UIDevice+ZPExtension.h"
#import "ZaloPayWalletSDKLog.h"
#import "ZPDataManager.h"
#import "ZaloPayWalletSDKPayment.h"

#define KEY_CN_CHANNEL_TYPE @"ch_channel_type"
#define KEY_CN_CHANNEL_ID @"cn_channel_id"
#define KEY_CN_CHANNEL_NAME @"cn_channel_name"
#define KEY_CN_IS_ENABLE @"cn_channel_status"
#define KEY_CN_MIN_PP_AMOUNT @"cn_min_pp_amount"
#define KEY_CN_MAX_PP_AMOUNT @"cn_max_pp_amount"
#define KEY_CN_FEE_RATE @"cn_fee_rate"
#define KEY_CN_MIN_FEE @"ch_min_fee"
#define KEY_CN_FEE_CAL_TYPE @"ch_fee_cal_type"
#define KEY_CN_REQUIRE_OTP @"require_otp"
#define KEY_AMOUNT_REQUIRE_OTP @"amount_require_otp"
#define KEY_MIN_APP_VERSION @"min_app_version"
#define KEY_BANK_CODE @"bank_code"
#define KEY_IN_AMOUNT_TYPE @"in_amount_type"
#define KEY_OVER_AMOUNT_TYPE @"over_amount_type"
#define KEY_TRANSTYPE @"key_trans_type"



#pragma mark - ZPChannel
@implementation ZPChannel

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:[NSNumber numberWithInt:self.channelType] forKey:KEY_CN_CHANNEL_TYPE];
    [aCoder encodeObject:[NSNumber numberWithInt:self.channelID] forKey:KEY_CN_CHANNEL_ID];
    [aCoder encodeObject:self.channelName forKey:KEY_CN_CHANNEL_NAME];
    [aCoder encodeObject:[NSNumber numberWithInt:self.status] forKey:KEY_CN_IS_ENABLE];
    [aCoder encodeObject:[NSNumber numberWithLong:self.minPPAmount] forKey:KEY_CN_MIN_PP_AMOUNT];
    [aCoder encodeObject:[NSNumber numberWithLong:self.maxPPAmount] forKey:KEY_CN_MAX_PP_AMOUNT];
    [aCoder encodeObject:[NSNumber numberWithDouble:self.feeRate] forKey:KEY_CN_FEE_RATE];
    [aCoder encodeObject:[NSNumber numberWithDouble:self.minFee] forKey:KEY_CN_MIN_FEE];
    [aCoder encodeObject:self.feeCalType forKey:KEY_CN_FEE_CAL_TYPE];
    [aCoder encodeObject:[NSNumber numberWithInt:self.requireOTP] forKey:KEY_CN_REQUIRE_OTP];
    [aCoder encodeObject:[NSNumber numberWithLong:self.amountRequireOTP] forKey:KEY_AMOUNT_REQUIRE_OTP];
    [aCoder encodeObject:self.minAppVersion forKey:KEY_MIN_APP_VERSION];
    [aCoder encodeObject:self.bankCode forKey:KEY_BANK_CODE];
    [aCoder encodeObject:[NSNumber numberWithInt:self.inamounttype] forKey:KEY_IN_AMOUNT_TYPE];
    [aCoder encodeObject:[NSNumber numberWithInt:self.overamounttype] forKey:KEY_OVER_AMOUNT_TYPE];
    [aCoder encodeObject:[NSNumber numberWithInt:self.transtype] forKey:KEY_TRANSTYPE];

    
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        self.channelName = [aDecoder decodeObjectForKey:KEY_CN_CHANNEL_NAME];
        self.channelType = [[aDecoder decodeObjectForKey:KEY_CN_CHANNEL_TYPE] intValue];
        self.channelID = [[aDecoder decodeObjectForKey:KEY_CN_CHANNEL_ID] intValue];
        self.status = [[aDecoder decodeObjectForKey:KEY_CN_IS_ENABLE] boolValue];
        self.minPPAmount = [[aDecoder decodeObjectForKey:KEY_CN_MIN_PP_AMOUNT] longValue];
        self.maxPPAmount = [[aDecoder decodeObjectForKey:KEY_CN_MAX_PP_AMOUNT] longValue];
        self.feeRate = [[aDecoder decodeObjectForKey:KEY_CN_FEE_RATE] doubleValue];
        self.minFee = [[aDecoder decodeObjectForKey:KEY_CN_MIN_FEE] doubleValue];
        self.feeCalType = [aDecoder decodeObjectForKey:KEY_CN_FEE_CAL_TYPE];
        self.requireOTP = [[aDecoder decodeObjectForKey:KEY_CN_REQUIRE_OTP] intValue];
        self.amountRequireOTP = [[aDecoder decodeObjectForKey:KEY_AMOUNT_REQUIRE_OTP] longValue];
        self.minAppVersion = [aDecoder decodeObjectForKey:KEY_MIN_APP_VERSION];
        self.bankCode = [aDecoder decodeObjectForKey:KEY_BANK_CODE];
        self.inamounttype = [[aDecoder decodeObjectForKey:KEY_IN_AMOUNT_TYPE] intValue];
        self.overamounttype = [[aDecoder decodeObjectForKey:KEY_OVER_AMOUNT_TYPE] intValue];
        self.transtype = [[aDecoder decodeObjectForKey:KEY_TRANSTYPE] intValue];

    }
    return self;
}

+ (instancetype)fromJson:(NSDictionary *)dict {
    ZPChannel * channel = [[ZPChannel alloc] init];
    channel.channelID = [dict intForKey:@"pmcid"];
    channel.status = [dict intForKey:@"status"];
    channel.channelType = [dict intForKey:@"pmcid"];
    channel.minPPAmount = [dict uint32ForKey:@"minvalue"];
    channel.maxPPAmount = [dict uint32ForKey:@"maxvalue"];
    channel.channelName = [dict stringForKey:@"pmcname"];
    channel.feeRate = [dict doubleForKey:@"feerate"];
    channel.minFee = [dict doubleForKey:@"minfee"];
    channel.feeCalType = [dict stringForKey:@"feecaltype"];
    channel.requireOTP = [dict intForKey:@"requireotp"];
    channel.amountRequireOTP = [dict uint32ForKey:@"amountrequireotp"];
    channel.minAppVersion = [dict stringForKey:@"minappversion"];
    channel.bankCode = [dict stringForKey:@"tpebankcode"];
    channel.inamounttype = [dict intForKey:@"inamounttype"];
    channel.overamounttype = [dict intForKey:@"overamounttype"];
    channel.transtype = [dict intForKey:@"transtype"];
    return channel;
}

- (BOOL)shouldSupportAmount:(long)amount {
    BOOL result = YES;
    
    if (self.minPPAmount >= 0 && self.minPPAmount > amount) result = NO;
    if (self.maxPPAmount >= 0 && self.maxPPAmount < amount) result = NO;
    
    return result;
}

- (NSString*)getErrorWithAmount:(long)amount {    
    if (self.minPPAmount >= 0 && self.minPPAmount > amount) {
        return [NSString stringWithFormat:stringMessageMinAmount, [@(self.minPPAmount) formatCurrency]];
    }
    if (self.maxPPAmount >= 0 && self.maxPPAmount < amount) {
        return [NSString stringWithFormat:stringMessageMaxAmount, [@(self.maxPPAmount) formatCurrency]];
    }
    
    return @"";
}
- (BOOL)shouldSupportAmount:(long)amount suggestAmount:(long)suggestAmount {
    DDLogInfo(@"111 status: %ld", (long)self.status);
    if (self.status != ZPPMCEnable) {
        return NO;
    }
    BOOL aaa = [self shouldSupportAmount:suggestAmount];
    DDLogInfo(@"support amount: %d", aaa);
    return [self shouldSupportAmount:suggestAmount];
}

- (id)copyWithZone:(NSZone *)zone {
    ZPChannel * channel = [[ZPChannel alloc] init];
    channel.status = self.status;
    return channel;
}

@end

// ZPAppData
#define kAppIdChecksumVersion [NSString stringWithFormat:@"APPDATA_CHECKSUM-%@%@",[UIDevice currentDevice].zpAppVersion,[UIDevice currentDevice].zpAppBuildVersion]

#define kPmcTransTypeMap        @"pmcTransTypeMap"
#define kPmcsDisplayOrder       @"pmcsDisplayOrderMap"
#define kAppID                  @"appID"
#define kAppName                @"appName"
#define kAppLogo                @"appLogo"
#define kStatus                 @"status"
#define kExpiredTime            @"expiredTime"
#define kViewResultType         @"viewResultType"
#define kWebredirecturl         @"webredirecturl"

@implementation ZPAppData

- (id)init {
    self = [super init];
    if (self) {
        self.pmcTransTypeMap = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:@(self.appID) forKey:kAppID];
    [aCoder encodeObject:self.appName forKey:kAppName];
    [aCoder encodeObject:self.appLogo forKey:kAppLogo];
    [aCoder encodeObject:self.checkSum forKey:kAppIdChecksumVersion];
    [aCoder encodeBool:self.status forKey:kStatus];
    [aCoder encodeDouble:self.expiredTime forKey:kExpiredTime];
    [aCoder encodeObject:@(self.viewResultType) forKey:kViewResultType];
    [aCoder encodeObject:self.redirect_url forKey:kWebredirecturl];
    [aCoder encodeObject:self.pmcTransTypeMap forKey:kPmcTransTypeMap];
    [aCoder encodeObject:self.pmcsDisplayOrderMap forKey:kPmcsDisplayOrder];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        NSNumber *appIdNumber = [aDecoder decodeObjectForKey:kAppID];
        if ([appIdNumber isKindOfClass:[NSNumber class]]) {
            self.appID = [appIdNumber integerValue];
        }        
        self.appName = [aDecoder decodeObjectForKey:kAppName];
        self.appLogo = [aDecoder decodeObjectForKey:kAppLogo];
        self.status = [aDecoder decodeBoolForKey:kStatus];
        self.checkSum = [aDecoder decodeObjectForKey:kAppIdChecksumVersion];
        self.expiredTime = [aDecoder decodeDoubleForKey:kExpiredTime];
        self.viewResultType = [[aDecoder decodeObjectForKey:kViewResultType] intValue];
        self.redirect_url = [aDecoder decodeObjectForKey:kWebredirecturl];
        NSDictionary *pmcs = [aDecoder decodeObjectForKey:kPmcTransTypeMap];
        self.pmcTransTypeMap = [NSMutableDictionary dictionary];
        if (pmcs.count > 0) {
            [self.pmcTransTypeMap addEntriesFromDictionary:pmcs];
        }
        
        self.pmcsDisplayOrderMap = [aDecoder decodeObjectForKey:kPmcsDisplayOrder];
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    ZPAppData * channel = [[ZPAppData alloc] init];
    channel.status = self.status;
    return channel;
}

@end

// ZPSavedCard
@implementation ZPSavedCard
+ (instancetype)fromJson:(NSDictionary*)dict {
    ZPSavedCard * savedCard = [[ZPSavedCard alloc] init];
    savedCard.cardHash = [dict objectForKey:@"cardhash"];
    savedCard.first6CardNo = [dict objectForKey:@"first6cardno"];
    savedCard.last4CardNo = [dict objectForKey:@"last4cardno"];
    savedCard.bankCode = [dict objectForKey:@"tpebankcode"];
    DDLogInfo(@"savecard: %@", savedCard.first6CardNo);
    return savedCard;
}

+ (NSDictionary *)jsonFromObject:(ZPSavedCard *)card andName:(NSString *)name andFinalBankcode:(NSString *)bankCode {
    NSMutableDictionary *jsonCard = [NSMutableDictionary new];
    NSString *imageName = [ZPResourceManager bankImageFromBankCode:bankCode];
    NSURL *url = [ZPResourceManager urlFromImageName:imageName];

    [jsonCard setObjectCheckNil:name forKey:@"cardname"];
    [jsonCard setObjectCheckNil:card.first6CardNo forKey:@"first6cardno"];
    [jsonCard setObjectCheckNil:card.last4CardNo forKey:@"last4cardno"];
    [jsonCard setObjectCheckNil:[bankCode stringByReplacingOccurrencesOfString:@"123P" withString:@""] forKey:@"bankcode"];
    [jsonCard setObjectCheckNil:url.path forKey:@"logo"];
    [jsonCard setObjectCheckNil:[NSNumber numberWithInt:1] forKey:@"type"];
    return jsonCard;
}

- (NSString *)bankCode {
    if ([self isCCDeBit]) {
        return stringDebitCardBankCode;
    }
    return _bankCode;
}

- (BOOL)isCCDeBit {
    return [[ZaloPayWalletSDKPayment sharedInstance].appDependence IsCCDebit:self.first6CardNo];
}

- (BOOL)isCCCard {
    return [self.bankCode isEqualToString:stringCreditCardBankCode] || [self.bankCode isEqualToString:stringDebitCardBankCode];
}
- (NSDictionary<NSString *,id> *)billData {
    NSMutableDictionary *infor = [NSMutableDictionary new];
    [infor setObjectCheckNil:self.first6CardNo forKey:@"f6no"];
    [infor setObjectCheckNil:self.last4CardNo forKey:@"l4no"];
    [infor setObjectCheckNil:self.bankCode forKey:@"bankcode"];
    [infor setObjectCheckNil:self.cardHolderName forKey:@"cardname"];
    [infor setObjectCheckNil:self.cardHash forKey:@"cardhash"];
    return infor;
}

@end

@implementation ZPSavedBankAccount
+ (instancetype)fromJson:(NSDictionary *)dict {
    ZPSavedBankAccount * savedBankAccount = [[ZPSavedBankAccount alloc] init];
    savedBankAccount.firstAccountNo = [dict objectForKey:@"firstaccountno"];
    savedBankAccount.lastAccountNo = [dict objectForKey:@"lastaccountno"];
    savedBankAccount.bankCode = [dict objectForKey:@"tpebankcode"];
    return savedBankAccount;
}
+ (NSDictionary *)jsonFromObject:(ZPSavedBankAccount *)card andName:(NSString *)name andFinalBankcode:(NSString *)bankCode {
    NSMutableDictionary *jsonCard = [NSMutableDictionary new];
    NSString *imageName = [ZPResourceManager bankImageFromBankCode:bankCode];
    NSURL *url = [ZPResourceManager urlFromImageName:imageName];
    [jsonCard setObjectCheckNil:name forKey:@"cardname"];
    [jsonCard setObjectCheckNil:card.firstAccountNo forKey:@"first6cardno"];
    [jsonCard setObjectCheckNil:card.lastAccountNo forKey:@"last4cardno"];
    [jsonCard setObjectCheckNil:[bankCode stringByReplacingOccurrencesOfString:@"123P" withString:@""] forKey:@"bankcode"];
    [jsonCard setObjectCheckNil:url.path forKey:@"logo"];
    [jsonCard setObjectCheckNil:[NSNumber numberWithInt:2] forKey:@"type"];
    return jsonCard;
}

- (NSDictionary<NSString *,id> *)billData {
    NSMutableDictionary *infor = [NSMutableDictionary new];
    [infor setObjectCheckNil:self.firstAccountNo forKey:@"firstaccountno"];
    [infor setObjectCheckNil:self.lastAccountNo forKey:@"lastaccountno"];
    [infor setObjectCheckNil:self.bankCode forKey:@"bankcode"];
    return infor;
}
@end

// ZPTransTypePmc
@implementation ZPTransTypePmc

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:[NSNumber numberWithInt:self.transType] forKey:@"TRANS_TYPE_PMC_TRANSTYPE"];
    [aCoder encodeObject:self.checksum forKey:@"TRANS_TYPE_CHECKSUM"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:self.pmcList] forKey:@"TRANS_TYPE_PMC_PMCLIST"];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        self.transType = [[aDecoder decodeObjectForKey:@"TRANS_TYPE_PMC_TRANSTYPE"] intValue];
        self.checksum = [aDecoder decodeObjectForKey:@"TRANS_TYPE_CHECKSUM"];
        self.pmcList = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"TRANS_TYPE_PMC_PMCLIST"]];
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary{
    self = [super init];
    if (self) {
        NSArray *transtypes = [dictionary arrayForKey:@"transtypes"];
        _pmcList = [self transtypeFromJsonArray:transtypes];
        _transType = [dictionary intForKey:@"transtype"];
        _checksum = [dictionary stringForKey:@"checksum"];

    }
    return self;
}

- (NSArray *)transtypeFromJsonArray:(NSArray *)transtypes {
    NSMutableArray *_return = [NSMutableArray array];
    for (NSDictionary *oneDic in transtypes) {
        ZPChannel * channel = [ZPChannel fromJson:oneDic];
        [_return addObject:channel];
    }
    return _return;
}
@end

//-------------
@implementation ZPUserPLP
+ (instancetype)fromJson:(NSDictionary *)dict {
    ZPUserPLP * profileLevel = [[ZPUserPLP alloc] init];
    profileLevel.transType = [dict intForKey:@"transtype"];
    profileLevel.pmcID = [dict intForKey:@"pmcid"];
    profileLevel.profileLevel = [dict intForKey:@"profilelevel"];
    profileLevel.allow = [dict boolForKey:@"allow"];
    return profileLevel;
}
@end
