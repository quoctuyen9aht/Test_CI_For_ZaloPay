//
//  ZPZaloPayResponseObject.h
//  PaymentDemoSDK
//
//  Created by Nguyen Xuan Phung on 4/11/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPResponseObject.h"

@interface ZPZaloPayWalletResponseObject : ZPResponseObject

@property (assign, nonatomic) long balance;

@end
