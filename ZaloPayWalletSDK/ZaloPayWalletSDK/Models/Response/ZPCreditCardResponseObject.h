//
//  ZPCreditCardResponseObject.h
//  ZPSDK
//
//  Created by Nguyen Xuan Phung on 1/19/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import <ZPResponseObject.h>

@interface ZPCreditCardResponseObject : ZPResponseObject

@property (strong, nonatomic) NSString * bankCode;
@property (strong, nonatomic) NSString * zpTransID;
@property (strong, nonatomic) NSString * statusUrl;
@property (strong, nonatomic) NSString * src;
@property (strong, nonatomic) NSString * payUrl;
@property (strong, nonatomic) NSString * appTransID;

@end
