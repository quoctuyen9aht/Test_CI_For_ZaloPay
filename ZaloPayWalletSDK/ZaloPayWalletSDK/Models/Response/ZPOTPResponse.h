//
//  ZPOTPResponse.h
//  ZPSDK
//
//  Created by ZaloPayTeam on 12/29/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import "ZPResponseObject.h"


//OTP : One-Time Password
@interface ZPOTPResponse : ZPResponseObject


@property (strong, nonatomic) NSString * zpTransID;
@property (strong, nonatomic) NSString * mac;
@property (strong, nonatomic) NSString * capcharImage;
@property (strong, nonatomic) NSString * chargePhoneNum;

@end
