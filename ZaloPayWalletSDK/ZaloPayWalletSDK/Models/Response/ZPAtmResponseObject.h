//
//  ZPAtmResponseObject.h
//  ZPSDK
//
//  Created by Nguyen Xuan Phung on 1/13/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPResponseObject.h"
#import "ZPConfig.h"

@interface ZPAtmResponseObject : ZPResponseObject

@property (strong, nonatomic) NSString *payUrl;
@property (assign, nonatomic) ZPAtmInputType inputType;
@property (strong, nonatomic) NSString *zpTransID;
@property (strong, nonatomic) NSString *mac;
@property (strong, nonatomic) NSString *statusUrl;
@property (strong, nonatomic) NSString *statusParam;
@property (strong, nonatomic) NSMutableDictionary *enabledBankDict;
@property (nonatomic) int atmFlag;

@end
