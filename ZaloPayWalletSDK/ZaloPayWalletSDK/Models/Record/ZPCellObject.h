//
//  ZPCellObject.h
//  ZPSDK
//
//  Created by ZaloPayTeam on 12/22/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZPConfig.h"

@interface ZPCellObject : NSObject

@property (assign, nonatomic) ZPCellType type;
@property (strong, nonatomic) NSString *reuseID;
@property (strong, nonatomic) NSString *smlID;
@property (strong, nonatomic) NSString *banknetID;
@property (strong, nonatomic) NSString *smlSubmitButtonID;
@property (strong, nonatomic) NSMutableArray *subViews;
@property (assign, nonatomic) float height;
@property (assign, nonatomic) bool hasOkButton;
@property (assign, nonatomic) NSString* jsSubmitForm;
@property (strong, nonatomic) NSString * errorElementIdInWebView;


+ (instancetype)initWithDictionary:(NSDictionary *)dictionary;


@end
