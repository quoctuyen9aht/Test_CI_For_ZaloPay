//
//  ZPCaptchaRecordObject.m
//  ZPSDK
//
//  Created by ZaloPayTeam on 12/22/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import "ZPCaptchaRecordObject.h"

@implementation ZPCaptchaRecordObject

+ (instancetype)initWithDictionary:(NSDictionary *)dictionary{
    
    ZPCaptchaRecordObject* recordObject = [[ZPCaptchaRecordObject alloc] init];
    recordObject.topPadding = [[dictionary objectForKey:@"top_padding"] intValue];
    recordObject.bottomPadding = [[dictionary objectForKey:@"bottom_padding"] intValue];
    recordObject.param = [dictionary objectForKey:@"param"];
    recordObject.placeHolder = [dictionary objectForKey:@"place_holder"];
    recordObject.requiredFillMessage = [dictionary objectForKey:@"required_fill_text"];
    recordObject.keyboardType = [[dictionary objectForKey:@"keyboard_type"] intValue];
    recordObject.isPassword = [[dictionary objectForKey:@"hiden_text"] boolValue];
    recordObject.smlID = [dictionary objectForKey:@"sml_id"];
    recordObject.banknetID = [dictionary objectForKey:@"banknet_id"];
    recordObject.tag = [[dictionary objectForKey:@"tag"] intValue];
    recordObject.nextTag = [[dictionary objectForKey:@"nextTag"] intValue];
    recordObject.prevTag = [[dictionary objectForKey:@"prevTag"] intValue];
    recordObject.returnType = [[dictionary objectForKey:@"returnType"] intValue];
    recordObject.isCapLocksOn = [[dictionary objectForKey:@"isCapLocksOn"] boolValue];
    recordObject.toolbarOption = [[dictionary objectForKey:@"toolbarOption"] intValue];
    recordObject.regrex = [dictionary objectForKey:@"regrex"];
    recordObject.regrexErrorMessage = [dictionary objectForKey:@"regex_error_message"];
    
    return recordObject;
    
}

@end
