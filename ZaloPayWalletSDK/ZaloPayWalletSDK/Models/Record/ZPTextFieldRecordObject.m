//
//  ZPTextFieldRecordObject.m
//  ZPSDK
//
//  Created by ZaloPayTeam on 12/22/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import "ZPTextFieldRecordObject.h"

@implementation ZPTextFieldRecordObject

+ (instancetype)initWithDictionary:(NSDictionary *)dictionary{
    
    ZPTextFieldRecordObject *recordObject = [[ZPTextFieldRecordObject alloc] init];
    recordObject.topPadding = [[dictionary objectForKey:@"top_padding"] intValue];
    recordObject.bottomPadding = [[dictionary objectForKey:@"bottom_padding"] intValue];
    recordObject.placeHolder = [dictionary objectForKey:@"place_holder"];
    recordObject.cacheParam = [dictionary objectForKey:@"cache_param"];
    recordObject.param = [dictionary objectForKey:@"param"];
    recordObject.requiredFillMessage = [dictionary objectForKey:@"required_fill_text"];
    recordObject.keyboardType = [[dictionary objectForKey:@"keyboard_type"] intValue];
    recordObject.zaloKeyboardType = [[dictionary objectForKey:@"zalo_keyboard_type"] intValue];
    recordObject.isPassword = [[dictionary objectForKey:@"hiden_text"] boolValue];
    recordObject.smlID = [dictionary objectForKey:@"sml_id"];
    recordObject.banknetID = [dictionary objectForKey:@"banknet_id"];
    recordObject.isTitleAppendable = [[dictionary objectForKey:@"title_appendable"] boolValue];
    recordObject.invalidErrorCode = [[dictionary objectForKey:@"invalid_error_code"] intValue];
    recordObject.objectID = [dictionary objectForKey:@"id"];
    recordObject.isCapLocksOn = [[dictionary objectForKey:@"isCapLocksOn"] boolValue];
    recordObject.tag = [[dictionary objectForKey:@"tag"] intValue];
    recordObject.nextTag = [[dictionary objectForKey:@"nextTag"] intValue];
    recordObject.prevTag = [[dictionary objectForKey:@"prevTag"] intValue];
    recordObject.returnType = [[dictionary objectForKey:@"returnType"] intValue];
    recordObject.formats = [dictionary objectForKey:@"format"];
    recordObject.toolbarOption = [[dictionary objectForKey:@"toolbarOption"] intValue];
    recordObject.isDisabled = [[dictionary objectForKey:@"isDisabled"] boolValue];
    recordObject.isBankName = [[dictionary objectForKey:@"isBankName"] boolValue];
    recordObject.regrex = [dictionary objectForKey:@"regrex"];
    recordObject.regrexErrorMessage = [dictionary objectForKey:@"regex_error_message"];
    recordObject.hasCamera = [[dictionary objectForKey:@"hasCamera"] boolValue];
    
    return recordObject;
    
}

@end
