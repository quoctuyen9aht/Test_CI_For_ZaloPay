//
//  ZPCardLog.h
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 5/5/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZPMapCardLog : NSObject
@property (nonatomic, strong) NSString *transid;
@property (nonatomic, strong) NSNumber *pmcid;
@property (nonatomic, strong) NSNumber *atmcaptcha_begindate;
@property (nonatomic, strong) NSNumber *atmcaptcha_enddate;
@property (nonatomic, strong) NSNumber *atmotp_begindate;
@property (nonatomic, strong) NSNumber *atmotp_enddate;
@end
