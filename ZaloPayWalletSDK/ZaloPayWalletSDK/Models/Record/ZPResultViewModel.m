//
//  ZPResultRowViewModel.m
//  ZaloPayWalletSDK
//
//  Created by thi la on 6/15/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPResultViewModel.h"
#import "ZPConfig.h"
#import "ZPBill.h"
#import "ZPVoucherInfo.h"

@implementation ZPResultViewModel : NSObject
- (NSString *)reuseIdentifier {
    return NSStringFromClass([self class]);
}
@end

@implementation ZPResultRowViewModel
- (id)initWithTitle:(NSString *)title value:(NSString *)value {
    self = [super init];
    if (self) {
        self.title = title;
        self.value = value;
    }
    return self;

}
- (ZPResultCellType)cellType {
    return ZPResultCellTypeResultDetails;
}

- (CGFloat)height {
    return IS_2X ? 28 : 30;
}
@end

@implementation ZPResultHeaderViewModel
- (ZPResultCellType)cellType {
    return ZPResultCellTypeResult;
}

- (CGFloat)height {
    return UITableViewAutomaticDimension;
}

- (NSString *)reuseIdentifier {
    return @"ZPResultHeaderView";
}
@end

@implementation ZPResultLineViewModel
- (ZPResultCellType)cellType {
    return ZPResultCellTypeResultLine;
}

- (CGFloat)height {
    return 2;
}
@end

@implementation ZPResultBlankViewModel
- (ZPResultCellType)cellType {
    return ZPResultCellTypeBlank;
}

- (CGFloat)height {
    return 10;
}
@end

@implementation ZPResultBlank4pxViewModel
- (ZPResultCellType)cellType {
    return ZPResultCellTypeBlank4px;
}

- (CGFloat)height {
    return 4;
}
@end

@implementation ZPResultSupportViewModel
- (ZPResultCellType)cellType {
    return ZPResultCellTypeSupport;
}

- (CGFloat)height {
    return 60;
}

- (NSString *)reuseIdentifier {
    return @"ZPResultSupportView";
}
@end

@implementation ZPResultTranferMoneyDetailViewModel
- (ZPResultCellType)cellType {
    return ZPResultCellTypeTransferMoneyDetail;
}

- (CGFloat)height {
    return 40;
}
@end

@implementation ZPResultUpdateProfileLevel3ViewModel
- (ZPResultCellType)cellType {
    return ZPResultCellTypeUpdateLevel3;
}

- (CGFloat)height {
    return 60;
}

- (NSString *)reuseIdentifier {
    return @"ZPResultSupportView";
}
@end
