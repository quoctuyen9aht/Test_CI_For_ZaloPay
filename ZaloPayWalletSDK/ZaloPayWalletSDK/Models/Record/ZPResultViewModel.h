//
//  ResultModel.h
//  ZaloPayWalletSDK
//
//  Created by thi la on 6/15/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, ZPResultCellType) {
    ZPResultCellTypeBlank4px = -2,
    ZPResultCellTypeBlank = -1,
    ZPResultCellTypeResult = 0,
    ZPResultCellTypeSupport = 1,
    ZPResultCellTypeUpdateLevel3 = 2,
    ZPResultCellTypeTransferMoneyDetail = 1000,
    ZPResultCellTypeResultDetails = 1001,
    ZPResultCellTypeResultLine = 1002
};

@interface ZPResultViewModel : NSObject
@property(nonatomic, assign) ZPResultCellType cellType;
@property(nonatomic) CGFloat height;
@property(nonatomic, readonly) NSString *reuseIdentifier;
@end


@interface ZPResultRowViewModel : ZPResultViewModel
@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) NSString *value;

- (id)initWithTitle:(NSString *)title value:(NSString *)value;
@end

@class ZPPaymentResponse;

@interface ZPResultHeaderViewModel : ZPResultViewModel
@property(nonatomic) BOOL isPaymentSuccess;
@property(nonatomic, strong) ZPBill *bill;
@property(nonatomic, strong) ZPPaymentResponse *paymentResponse;
@property(nonatomic) long amount;
@property(nonatomic, strong) NSString *messageCustomAfterSuccess;
@end

@interface ZPResultLineViewModel : ZPResultViewModel

@end

@interface ZPResultBlankViewModel : ZPResultViewModel

@end

@interface ZPResultBlank4pxViewModel : ZPResultViewModel

@end

@interface ZPResultSupportViewModel : ZPResultViewModel

@end

@interface ZPResultTranferMoneyDetailViewModel : ZPResultViewModel
@property(nonatomic, strong) NSString *userAvatar;
@property(nonatomic, strong) NSString *friendAvatar;
@end

@interface ZPResultUpdateProfileLevel3ViewModel : ZPResultViewModel

@end
