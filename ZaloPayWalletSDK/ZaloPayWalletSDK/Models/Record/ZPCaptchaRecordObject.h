//
//  ZPCaptchaRecordObject.h
//  ZPSDK
//
//  Created by ZaloPayTeam on 12/22/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ZPRecordObject.h"


@interface ZPCaptchaRecordObject : ZPRecordObject

@property (strong, nonatomic) NSString *placeHolder;
@property (strong, nonatomic) NSString *param;
@property (strong, nonatomic) NSString *requiredFillMessage;
@property (strong, nonatomic) NSString *smlID;
@property (strong, nonatomic) NSString *banknetID;
@property (assign, nonatomic) UIKeyboardType keyboardType;
@property (assign, nonatomic) bool isPassword;
@property (assign, nonatomic) BOOL isCapLocksOn;
@property (strong, nonatomic) NSString * regrex;
@property (strong, nonatomic) NSString * regrexErrorMessage;

+ (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
