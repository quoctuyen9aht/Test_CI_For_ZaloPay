//
//  ZPTextFieldRecordObject.h
//  ZPSDK
//
//  Created by ZaloPayTeam on 12/22/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ZPRecordObject.h"

@interface ZPTextFieldRecordObject : ZPRecordObject

@property (strong, nonatomic) NSString *placeHolder;
@property (strong, nonatomic) NSString *param;
@property (strong, nonatomic) NSString *cacheParam;
@property (assign, nonatomic) int invalidErrorCode;
@property (strong, nonatomic) NSString *requiredFillMessage;
@property (assign, nonatomic) UIKeyboardType keyboardType;
@property (assign, nonatomic) UIKeyboardType zaloKeyboardType;
@property (assign, nonatomic) bool isPassword;
@property (strong, nonatomic) NSString *smlID;
@property (strong, nonatomic) NSString *banknetID;
@property (assign, nonatomic) bool isTitleAppendable;
@property (strong, nonatomic) NSString * textValue;
@property (assign, nonatomic) BOOL isCapLocksOn;
@property (strong, nonatomic) NSArray * formats;
@property (assign, nonatomic) BOOL isDisabled;
@property (assign, nonatomic) BOOL isBankName;
@property (strong, nonatomic) NSString * regrex;
@property (strong, nonatomic) NSString * regrexErrorMessage;
@property (assign, nonatomic) BOOL hasCamera;


+ (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
