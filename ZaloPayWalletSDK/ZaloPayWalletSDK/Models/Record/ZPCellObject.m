//
//  ZPCellObject.m
//  ZPSDK
//
//  Created by ZaloPayTeam on 12/22/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import "ZPCellObject.h"
#import "ZPRecordObject.h"
#import "ZPCaptchaRecordObject.h"
#import "ZPTextFieldRecordObject.h"
#import "ZaloPayWalletSDKLog.h"

@implementation ZPCellObject

+ (instancetype)initWithDictionary:(NSDictionary *)dictionary{
    if (dictionary == NULL) {
        return NULL;
    }
    ZPCellObject* object = [[ZPCellObject alloc] init];
    object.reuseID = [dictionary objectForKey:@"reuse_identity"];
    object.subViews = [[NSMutableArray alloc] init];
    object.smlID = [dictionary objectForKey:@"sml_id"];
    object.banknetID = [dictionary objectForKey:@"banknet_id"];
    object.smlSubmitButtonID = [dictionary objectForKey:@"sml_submit_button_id"];
    object.hasOkButton = [[dictionary objectForKey:@"cell_type"] boolValue];
    object.jsSubmitForm = [dictionary objectForKey:@"js_submit_form"];
    object.errorElementIdInWebView = [dictionary objectForKey:@"error_element_Id_in_webview"];
    object.height = [[dictionary objectForKey:@"height"] floatValue];
    NSArray* sViewDict = [dictionary objectForKey:@"subviews"];
    for (NSDictionary * dict in sViewDict) {
        enum ZPRecordType type = [[dict objectForKey:@"type"] intValue];
        ZPRecordObject* recordObject = NULL;
        DDLogInfo(@"ZPRecordType: %ld", (long)type);
        switch (type) {
                
            case ZPRecordTypeCaptcha:
                recordObject = [ZPCaptchaRecordObject initWithDictionary:dict];
                break;
            case ZPRecordTypeTextField:
                recordObject = [ZPTextFieldRecordObject initWithDictionary:dict];
                break;
            default:
                recordObject = [ZPRecordObject initWithDictionary:dict];
                break;
        }
        [object.subViews addObjectNotNil:recordObject];
    }
    return object;
}

@end
