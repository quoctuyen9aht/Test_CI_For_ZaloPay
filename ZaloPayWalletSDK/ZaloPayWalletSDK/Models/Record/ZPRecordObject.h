//
//  ZPRecordObject.h
//  ZPSDK
//
//  Created by ZaloPayTeam on 12/22/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZPConfig.h"
#import <UIKit/UIKit.h>


@interface ZPRecordObject : NSObject

@property (assign, nonatomic) int topPadding;
@property (strong, nonatomic) NSString *objectID;
@property (assign, nonatomic) int bottomPadding;
@property (assign, nonatomic) int tag;
@property (assign, nonatomic) int nextTag;
@property (assign, nonatomic) int prevTag;
@property (assign, nonatomic) UIReturnKeyType returnType;
@property (assign, nonatomic) NSString* elementIdOfWebView;
@property (assign, nonatomic) int toolbarOption;


+ (instancetype)initWithDictionary:(NSDictionary *)dictionary;
+ (UIColor *)colorWithDict:(NSDictionary *) dict;
+ (UIFont *)fontWithStyle:(ZPFontStyle)style andSize:(int)size;

@end
