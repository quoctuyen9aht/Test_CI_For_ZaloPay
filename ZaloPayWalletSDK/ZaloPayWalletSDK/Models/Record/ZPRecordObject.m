//
//  ZPRecordObject.m
//  ZPSDK
//
//  Created by ZaloPayTeam on 12/22/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import "ZPRecordObject.h"

@implementation ZPRecordObject

+ (instancetype)initWithDictionary:(NSDictionary *)dictionary{
    return NULL;
}

+ (UIColor*)colorWithDict:(NSDictionary *) dict{
    float red = ([[dict objectForKey:@"red"] intValue] * 1.0f) / 255;
    float green = ([[dict objectForKey:@"green"] intValue] * 1.0f) / 255;
    float blue = ([[dict objectForKey:@"blue"] intValue] * 1.0f) / 255;
    return [UIColor colorWithRed:red green:green blue:blue alpha:1.0f];
}

+ (UIFont*)fontWithStyle:(ZPFontStyle)style andSize:(int)size{
    switch (style) {
        case ZPTextStyleBold:
            return [UIFont boldSystemFontOfSize:size];
            break;
        case ZPTextStyleItalic:
            return [UIFont italicSystemFontOfSize:size];
            break;
        case ZPTextStyleNormal:
            return [UIFont systemFontOfSize:size];
            break;
        default:
            break;
    }
    return NULL;
}

@end
