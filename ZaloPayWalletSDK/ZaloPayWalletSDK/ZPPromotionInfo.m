//
//  ZPPromotionInfo.m
//  ZaloPayWalletSDK
//
//  Created by Phuochh on 5/30/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import "ZPPromotionInfo.h"

@implementation ZPPromotionInfo
- (instancetype)initDataWithDict:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        self.promotionsig = [dict stringForKey:@"promotionsig"];
        self.promotiontransid = [dict stringForKey:@"promotiontransid"];
        self.discountamount = [dict doubleForKey:@"discountamount"];
    }
    return self;
    
}
@end
