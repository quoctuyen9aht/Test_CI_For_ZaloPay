//
//  ZPMiniBank.m
//  ZaloPayWalletSDK
//
//  Created by thi la on 1/17/17.
//  Copyright © 2017-present VNG. All rights reserved.
//

#import "ZPMiniBank.h"

@implementation ZPMiniBank

+ (instancetype)fromDictionary:(NSDictionary *)bankFunction {
    ZPMiniBank *miniBank = [ZPMiniBank new];
    miniBank.bankCode  = [bankFunction stringForKey:@"tpebankcode"];
    miniBank.bankFuntion = [bankFunction intForKey:@"bankfunction"];
    miniBank.minibankStatus = [bankFunction intForKey:@"status"];
    miniBank.maintenancefrom = [bankFunction uint64ForKey:@"maintenancefrom"];
    miniBank.maintenanceto = [bankFunction uint64ForKey:@"maintenanceto"];
    miniBank.maintenancemsg = [bankFunction stringForKey:@"maintenancemsg"];
    return miniBank;
}

@end
