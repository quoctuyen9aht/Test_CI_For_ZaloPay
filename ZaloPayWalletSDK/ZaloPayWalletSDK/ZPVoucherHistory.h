//
//  ZPVoucherHistory.h
//  ZaloPayWalletSDK
//
//  Created by Phuoc Huynh on 8/24/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZPVoucherPaymentConditions.h"

@interface ZPVoucherHistory : NSObject

@property (nonatomic, strong) NSString *userid;
@property (nonatomic, assign) long expireymd;
@property (nonatomic, strong) NSString *uid;
@property (nonatomic, assign) int historystatus;
@property (nonatomic, strong) NSString *givevoucherforuserid;

@property (nonatomic)         double givevoucherdate;
@property (nonatomic, strong) NSString *vouchername;
@property (nonatomic, strong) NSString *vouchercode;
@property (nonatomic, strong) NSString *usevoucherapptransid;
@property (nonatomic, strong) NSString *voucherconfigid;
@property (nonatomic)         int voucherstatus;

@property (nonatomic, assign) double discountamount;
@property (nonatomic, assign) int valuetype;
@property (nonatomic, strong) NSString *valueDisplayCollectionViewCell;
@property (nonatomic, strong) id valueDisplayTableViewCell;
@property (nonatomic, assign) long long value;
@property (nonatomic, assign) double percentValue;

@property (nonatomic, assign) long useconditonappid;
@property (nonatomic, strong) NSString *useconditionpmcid;

@property (nonatomic, assign) double useconditionminamount;
@property (nonatomic, assign) double useconditionmaxamount;
@property (nonatomic, assign) double useconditionexpireddate;
@property (nonatomic, assign) long countExpiredDate;
@property (nonatomic, strong)  NSString *countExpiredDateDisplay;

@property (nonatomic, assign) double useconditionusedcountlimit;
@property (nonatomic, assign) long cap;
@property (nonatomic, strong) NSString *voucherHistoryDescription;
@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, assign) BOOL isSelected;
@property (nonatomic, assign) BOOL isDisable;
@property (nonatomic, strong) NSString *detail_url;

//v3.0
@property (nonatomic, strong) ZPVoucherPaymentConditions *paymentConditions;

- (instancetype)initDataWithDict:(NSDictionary *)dict;
@end
