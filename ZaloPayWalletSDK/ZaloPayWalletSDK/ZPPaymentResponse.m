//
//  ZPPaymentResponse.m
//  ZPSDK
//
//  Created by phungnx on 12/22/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import "ZPDefine.h"
#import "ZPPaymentResponse.h"
#import "ZPDataManager.h"
#import "ZPPaymentManager.h"
#import "ZaloPayWalletSDKLog.h"
#import "ZPBill.h"

@implementation ZPPaymentResponse


- (instancetype)initWith:(ZPResponseObject *)responseObject {
    self = [super init];
    [self copyFrom:responseObject];
    return self;
}

- (instancetype)initWithPaymentResponse:(ZPPaymentResponse *)responseObject {
    self = [[ZPPaymentResponse alloc] initWith:responseObject];
    if ([responseObject isKindOfClass:[ZPPaymentResponse class]]) {
        self.reqdate = responseObject.reqdate;
        self.balance = responseObject.balance;
    }
    return self;
}

+ (ZPPaymentResponse *) unidentifiedStatusObject{
    ZPPaymentResponse* response = [[ZPPaymentResponse alloc] init];
    response.errorCode = kZPZaloPayCreditErrorCodeNoneError;
    response.originalCode = -1;
    response.exchangeStatus = kZPZaloPayCreditStatusCodeUnidentified;
    response.message = [[ZPDataManager sharedInstance] stringByEnum:ZPStringTypeAnnounceMessageExchangeUnidentified];
    response.errorStep = ZPErrorStepNonRetry;
    return response;
}

+ (ZPPaymentResponse *)timeOutRequestObject{
    ZPPaymentResponse* responseObject = [[ZPPaymentResponse alloc] init];
    
    int errorStep = ZPErrorStepNonRetry;
    responseObject.errorCode = kZPZaloPayCreditErrorCodeRequestTimeout;
    responseObject.exchangeStatus = kZPZaloPayCreditStatusCodeFail;
    responseObject.originalCode = -1;
    responseObject.message = [[ZPDataManager sharedInstance] stringByEnum:ZPStringTypeErrorMessageNetworkError];
    responseObject.errorStep = errorStep;
    return responseObject;
}

+ (ZPPaymentResponse *)retryRequestObject{
    ZPPaymentResponse* responseObject = [[ZPPaymentResponse alloc] init];
    
    int errorStep = ZPErrorStepAbleToRetry;
    responseObject.errorCode = kZPZaloPayCreditErrorCodeRetryOTP;
    responseObject.exchangeStatus = kZPZaloPayCreditStatusCodeFail;
    responseObject.originalCode = -1;
    responseObject.message = @"Mã xác nhận không chính xác";
    responseObject.errorStep = errorStep;
    return responseObject;
}


+ (ZPPaymentResponse *) unknownExceptionResponseObject{
    ZPPaymentResponse* responseObject = [[ZPPaymentResponse alloc] init];
    
    int errorStep = ZPErrorStepNonRetry;
    responseObject.errorCode = kZPZaloPayCreditErrorCodeUnknownException;
    responseObject.exchangeStatus = kZPZaloPayCreditStatusCodeFail;
    responseObject.originalCode = -1;
    responseObject.message = @"Giao dịch thất bại";
    responseObject.errorStep = errorStep;
    return responseObject;
}
+ (ZPPaymentResponse *) duplicateTransIDResponseObject{
    ZPPaymentResponse* responseObject = [[ZPPaymentResponse alloc] init];
    
    int errorStep = ZPErrorStepNonRetry;
    responseObject.errorCode = -68;
    responseObject.originalCode = -1;
    responseObject.exchangeStatus = kZPZaloPayCreditStatusCodeFail;
    responseObject.message = @"Giao dịch thất bại";
    responseObject.errorStep = errorStep;
    return responseObject;
}

+ (ZPPaymentResponse *)sandboxResponse:(double)chargeAmount currency:(NSString *)currency;
{
    ZPPaymentResponse* responseObject = [[ZPPaymentResponse alloc] init];
    
    responseObject.errorCode = kZPZaloPayCreditErrorCodeNoneError;
    responseObject.exchangeStatus = kZPZaloPayCreditStatusCodeSuccess;
    responseObject.message = @"Giao dịch thành công";
    return responseObject;
}

+ (instancetype)paymentResponseFrom:(NSDictionary *)dictionary
                               with:(NSString *)zpTransID
                                 at:(ZPPaymentStep)step
                           bankCode:(NSString *)bankCode
                            path:(NSString *)path
                            params:(NSDictionary *)params
{    
    int rtcode = [dictionary intForKey:@"returncode"];
    NSString *returnmessage = [dictionary stringForKey:@"returnmessage"];
    NSString *suggestMessage = [dictionary stringForKey:@"suggestmessage"];
    NSArray *suggestAction = [dictionary arrayForKey:@"suggestaction"];
    
    ZPPaymentResponse* responseObject = [[ZPPaymentResponse alloc] init];
    responseObject.errorCode = rtcode;
    responseObject.originalCode = rtcode;
    responseObject.message = returnmessage;
    responseObject.suggestMessage = suggestMessage;
    responseObject.suggestAction = suggestAction;
    responseObject.isProcessing = [[dictionary objectForKey:@"isprocessing"] boolValue];
    responseObject.data = [dictionary stringForKey:@"data"];
    responseObject.reqdate = [dictionary uint64ForKey:@"reqdate"];
    responseObject.zpTransID = zpTransID;
    
    if (rtcode == ZALOPAY_ERRORCODE_SUCCESSFUL) { // thành công
        DDLogInfo(@"PAYMENT-STATUS-SUCCESS!!! returnCode = %d", rtcode);
        responseObject.errorCode = kZPZaloPayCreditErrorCodeNoneError;
        responseObject.exchangeStatus = kZPZaloPayCreditStatusCodeSuccess;
        responseObject.message = nil;
        responseObject.errorStep = ZPErrorStepNonRetry;
        responseObject.balance = [dictionary intForKey:@"balance"];
        responseObject.shouldHandlePaymentResponse = YES;
        return responseObject;
    }
    
    if (rtcode < 1) { //lỗi
        [ZPPaymentManager sendRequestErrorWithData:zpTransID bankCode:bankCode exInfo:[NSString stringWithFormat:@"%ld",(long)rtcode] apiPath:path params:params response:dictionary appId:0];
        
        DDLogInfo(@"PAYMENT-STATUS-FAIL!!! returnCode = %d", rtcode);
        responseObject.exchangeStatus = kZPZaloPayCreditStatusCodeFail;
        responseObject.message = responseObject.message.length == 0 ? nil : responseObject.message;
        responseObject.errorStep = ZPErrorStepNonRetry;
        responseObject.shouldHandlePaymentResponse = YES;
        return responseObject;
    }
    
    if (rtcode > 1) { //giao dịch đang xủ lý
        NSDictionary *data = [responseObject.data JSONValue];
        responseObject.actionType = [data intForKey:@"actiontype"];
        
        if ([responseObject.data length] > 0 &&
            responseObject.isProcessing &&
            responseObject.actionType != 0 && step != ZPPaymentStepGetPaymentResult)
        {
            DDLogInfo(@"zpTransID: %@", responseObject.zpTransID);
            if (rtcode == ZPServerErrorCodeAtmRetryOtp) {
                responseObject.exchangeStatus = kZPZaloPayCreditStatusCodeFail;
                responseObject.errorStep = ZPErrorStepAbleToRetry;
                responseObject.shouldHandlePaymentResponse = YES;
                return responseObject;
            }
            if (step == ZPPaymentStepSubmitTrans) {
                responseObject.shouldHandlePaymentResponse = YES;
                return responseObject;
            }            
            
        }
    }
    return responseObject;
}

- (NSString *)appsFlyerValue {
    if (self.errorCode == ZALOPAY_ERRORCODE_SUCCESSFUL) {
        return @"success";
    }
    if (self.errorCode == kZPZaloPayCreditErrorCodeRequestTimeout || self.exchangeStatus == kZPZaloPayCreditStatusCodeUnidentified) {
        return @"pending";
    }
    return @"failed";
}

- (NSDictionary *)toJsonResponse:(ZPBill *)bill {
    ZPTransType transType = bill ? bill.transType : ZPTransTypeBillPay;
    NSMutableDictionary *result = [NSMutableDictionary new];
    [result addEntriesFromDictionary:@{stringKeyBalance : [NSString stringWithFormat:@"%lld", self.balance] }];
    [result addEntriesFromDictionary:@{stringKeyTranType : [NSString stringWithFormat:@"%ld", (long)transType] }];
    [result addEntriesFromDictionary:@{stringKeyErrorCode : @(self.errorCode) }];
    [result addEntriesFromDictionary:@{stringKeyMessage: self.message ?: @"" }];
    [result addEntriesFromDictionary:@{stringKeyZPTranId: self.zpTransID ?: @"" }];
    return result;
}

@end
