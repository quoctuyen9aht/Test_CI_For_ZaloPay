//
//  ZPResponseObject.m
//  ZPSDK
//
//  Created by ZaloPayTeam on 12/22/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import "ZPResponseObject.h"

@implementation ZPResponseObject
- (void)copyFrom:(ZPResponseObject *)others {
    // validate
    if (!others || ![others isKindOfClass:[ZPResponseObject class]]) {
        return;
    }
    
    self.message = others.message;
    self.errorCode = others.errorCode;
    self.originalCode = others.originalCode;
    self.errorStep = others.errorStep;
    self.captchaImage = others.captchaImage;
    self.suggestMessage = others.suggestMessage;
    self.suggestAction = others.suggestAction;
}
@end
