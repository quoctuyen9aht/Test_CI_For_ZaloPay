//
//  ZPPaymentChannel.h
//  ZPSDK
//
//  Created by phungnx on 12/22/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZPDefine.h"
@protocol MakeBillDataProtocol<NSObject>
- (NSDictionary <NSString *, id> *)billData;
@end

@interface ZPChannel : NSObject <NSCopying, NSCoding>
@property (assign, nonatomic) ZPPaymentMethodType channelType;
@property (assign, nonatomic) ZPPMCStatus status;
@property (assign, nonatomic) int channelID;
@property (assign, nonatomic) long minPPAmount;
@property (assign, nonatomic) long maxPPAmount;
@property (strong, nonatomic) NSString * channelName;
@property (assign, nonatomic) double feeRate;
@property (assign, nonatomic) double minFee;
@property (strong, nonatomic) NSString * feeCalType;
@property (assign, nonatomic) int requireOTP;
@property (assign, nonatomic) long amountRequireOTP;
@property (strong, nonatomic) NSString *minAppVersion;
@property (strong, nonatomic) NSString * bankCode;
@property (assign, nonatomic) int inamounttype;
@property (assign, nonatomic) int overamounttype;
@property (assign, nonatomic) int transtype;
@property (assign, nonatomic) int displayOrder;


- (BOOL)shouldSupportAmount:(long)amount;
- (NSString*)getErrorWithAmount:(long)amount;
@end

@interface ZPTransTypePmc : NSObject <NSCopying, NSCoding>
@property (assign, nonatomic) ZPTransType transType;
@property (strong, nonatomic) NSString *checksum;
@property (strong, nonatomic) NSArray * pmcList;
- (instancetype)initWithDictionary:(NSDictionary *)array;
@end

@interface ZPAppData : NSObject <NSCopying, NSCoding>
@property (assign, nonatomic) ZPAPPStatus status;
@property (nonatomic) NSInteger appID;
@property (strong, nonatomic) NSString* appName;
@property (strong, nonatomic) NSString* appLogo;
@property (strong, nonatomic) NSString* checkSum;
@property (strong, nonatomic) NSMutableDictionary *pmcTransTypeMap;
@property (assign, nonatomic) int viewResultType;
@property (nonatomic, assign) NSTimeInterval expiredTime;
@property (nonatomic, strong) NSString *redirect_url;
@property (strong, nonatomic) NSDictionary * pmcsDisplayOrderMap;

@end

@interface ZPSavedCard : NSObject<MakeBillDataProtocol>
@property (strong, nonatomic) NSString* cardHash;
@property (strong, nonatomic) NSString* first6CardNo;
@property (strong, nonatomic) NSString* last4CardNo;
@property (strong, nonatomic) NSString* bankCode;
@property (strong, nonatomic) NSString* cardHolderName;
+ (instancetype)fromJson:(NSDictionary*)dict;
+ (NSDictionary *)jsonFromObject:(ZPSavedCard *)card andName:(NSString *)name andFinalBankcode:(NSString *)bankCode;
- (BOOL)isCCDeBit;
- (BOOL)isCCCard;
- (NSDictionary <NSString *, id> *)billData;
@end

@interface ZPSavedBankAccount : NSObject<MakeBillDataProtocol>
@property (strong, nonatomic) NSString* bankCode;
@property (strong, nonatomic) NSString* firstAccountNo;
@property (strong, nonatomic) NSString* lastAccountNo;
+ (instancetype)fromJson:(NSDictionary *)dict;
+ (NSDictionary *)jsonFromObject:(ZPSavedBankAccount *)card andName:(NSString *)name andFinalBankcode:(NSString *)bankCode;
@end


@interface ZPUserPLP : NSObject
@property (assign, nonatomic) ZPTransType transType;
@property (assign, nonatomic) ZPPaymentMethodType pmcID;
@property (assign, nonatomic) int profileLevel;
@property (assign, nonatomic) BOOL allow;
+ (instancetype)fromJson:(NSDictionary *)dict;
@end



