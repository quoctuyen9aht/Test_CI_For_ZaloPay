//
//  ZPProgressHUD.h
//  ZPSDK
//
//  Created by ZaloPayTeam on 12/23/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//
#import <UIKit/UIKit.h>

@interface ZPProgressHUD : UIControl
+ (ZPProgressHUD*)sharedView;
+ (void)showWithStatus:(NSString *)status;

+ (void)dismiss;
+ (BOOL)isVisible;

@end



