//
// Created by Huu Hoa Nguyen on 3/23/18.
// Copyright (c) 2018 VNG Corporation. All rights reserved.
//

#import "ZPResultRowView.h"
#import "ZPDataManager.h"


@implementation ZPResultRowView
- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    self.backgroundColor = [UIColor whiteColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;

    return self;
}
@end

@implementation ZPResultRowBlankView
@end

@implementation ZPResultRowLineView
- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    [imageView setImage:[UIImage imageNamed:@"payorder_line"]];
    [self addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.left.equalTo(10);
        make.right.equalTo(self.mas_right).with.offset(-10);
        make.height.greaterThanOrEqualTo(2);
    }];

    return self;
}
@end

@interface ZPResultFooterView ()
@property(nonatomic, strong) UIButton *closeButton;
@end

@implementation ZPResultFooterView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];

    UIView *viewBottom = [UIView.alloc initWithFrame:CGRectMake(0, frame.size.height - 70, frame.size.width, 70)];
    viewBottom.backgroundColor = [UIColor whiteColor];
    self.closeButton = [UIButton.alloc initWithFrame:CGRectMake(10, 10, frame.size.width - 20, 50)];

    NSString *okButtonTitle = [[[ZPDataManager sharedInstance] getButtonTitle:@"close"] capitalizedString];
    [self.closeButton setTitle:okButtonTitle forState:UIControlStateNormal];
    self.closeButton.backgroundColor = [UIColor hex_0xe6f6ff];
    [self.closeButton.layer setBorderColor:[[UIColor hex_0x4abbff] CGColor]];
    [self.closeButton.layer setBorderWidth:1.0f];
    [self.closeButton setTitleColor:[UIColor subText] forState:UIControlStateNormal];
    [self.closeButton.titleLabel zpMainBlackRegular];
    self.closeButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.closeButton.layer.cornerRadius = 3;

    [viewBottom addSubview:self.closeButton];
    [self addSubview:viewBottom];
    self.backgroundColor = [UIColor clearColor];
    return self;
}

- (void)addTouchTarget:(nullable id)target action:(SEL)action {
    [self.closeButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
}

@end
