//
//  ZPUserControlDelegate.h
//  ZPSDK
//
//  Created by phungnx on 12/23/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZPRecordObject.h"


@protocol ZPUserControlDelegate <NSObject>
@optional
+ (float)viewHeightWithRecordObject:(ZPRecordObject*) object;
+ (float)viewWidth;
+ (UIView *)initWithRecordObject:(ZPRecordObject*) object;
- (NSDictionary *)outputParams;
- (BOOL)checkValidate;
- (NSString *)jsSubmitFormSml;
- (NSString *)jsSubmitFormBanknet;
@end
