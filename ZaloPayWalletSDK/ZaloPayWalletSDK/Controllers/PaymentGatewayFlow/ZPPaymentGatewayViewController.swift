//
//  ZPPaymentGatewayViewController.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 5/7/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import WebKit
import SnapKit
import RxSwift
import RxCocoa
import ZaloPayWalletSDKPrivate

enum ZPPaymentGatewayState {
    case sucess([String: Any]?)
    case fail(Error)
}
public enum ZPPaymentGatewayResult {
    case response(ZPPaymentResponse?)
    case fail(Error)
    case cancel
}

@objcMembers
public final class ZPPaymentGatewayViewController: UIViewController {
    private var bill: ZPBill?
    private var url: URL?
    private let disposeBag = DisposeBag()
    private var type: ZPGatewayType = .none
    let eResponse: PublishSubject<ZPPaymentGatewayResult> = PublishSubject()
    private lazy var presenter = ZPPaymentGatewayPresenter.init(with: self, self.bill)
    private lazy var webView: UIWebView = {
        let web: UIWebView = UIWebView.init(frame: .zero)
//        if #available(iOS 10, *) {
//            let configure = self.makeConfigureForWeb()
//            web = WKWebView.init(frame: .zero, configuration: configure)
//        } else {
//            web = WKWebView.init(frame: .zero)
//        }
        web.scalesPageToFit = true
        web.backgroundColor = .white
        self.view.addSubview(web)
        web.snp.makeConstraints({ (make) in
            make.edges.equalToSuperview()
        })
        return web
    }()
    private var jsHandler: ZPUIWebViewBridgeJS?
    
    fileprivate convenience init(with url: URL?, _ bill: ZPBill?, _ type: Int) {
        self.init(nibName: nil, bundle: nil)
        self.bill = bill
        self.url = url
        self.type = ZPGatewayType.init(rawValue: type) ?? .none
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        prepareLayout()
        
        self.presenter.loadSource()
    }
    
    func loadURL(from order: String?) {
        guard let url = self.url else {
            #if DEBUG
                fatalError("Check init url")
            #else
                return
            #endif
        }
        let path = url.absoluteString
        let components: String
        if let order = order {
            components = "?order=\(order)"
        }else {
            components = ""
        }
        
        let nPath = "\(path)\(components)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        print(nPath ?? "")
        
//        var component = URLComponents.init(url: url, resolvingAgainstBaseURL: false)
//        component?.queryItems = [URLQueryItem(name: "order", value: order)]
        guard let nURL = URL.init(string: nPath ?? "") else {
            #if DEBUG
                fatalError("Check init url")
            #else
                return
            #endif
        }
        
        print("url load pay: \(nURL.absoluteString)")
        self.webView.delegate = self.presenter
        jsHandler = ZPUIWebViewBridgeJS.bridge(webView, using: { [weak self](_ objectResponse: Any, _ error: Error?) in
            self?.handlerJS(objectResponse as? [String: Any])
        })
        let request = URLRequest.init(url: nURL)
        self.webView.loadRequest(request)
    }
    
    deinit {
        self.webView.delegate = nil
        self.eResponse.onCompleted()
    }
    
    private func handlerJS(_ response: [String: Any]?) {
        print("\(response ?? [:])")
        guard let response = response,
            let msgData = response["msgdata"] as? [String: Any],
            let funcName = msgData["func"] as? String else {
            return
        }
        
        switch funcName {
        case "showLoading":
             ZPProgressHUD.show(withStatus: nil)
        case "hideLoading":
            ZPProgressHUD.dismiss()
        default:
            break
        }
    }
    
    private func prepareLayout() {
        self.title = self.bill?.billDescription
        let f = CGRect(origin: .zero, size: CGSize(width: 44, height: 44))
        let barLeftButton = UIBarButtonItem(customView: UIView(frame: f))
        self.navigationItem.leftBarButtonItem = barLeftButton
        
        let backButton = UIButton(type: .custom)
        backButton.frame = f
        backButton.isMultipleTouchEnabled = false
        backButton.tintColor = .white
        backButton.setTitle(R.string_ButtonLabel_Cancel(), for: .normal)
        backButton.titleLabel?.font = UIFont.sfuiTextRegular(withSize: 15)
        backButton.contentHorizontalAlignment = .right
        
        backButton.rx.tap.bind { [weak self](_) in
            // Show alert
            self?.presenter.showAlertCancel()
        }.disposed(by: disposeBag)
        
        let barButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.rightBarButtonItem = barButton
        
    }
    
    
    // MARK: Public function
    public static func showOn(controller vc: UIViewController?, url: URL?, bill: AnyObject?, type: Int) -> Observable<ZPPaymentGatewayResult> {
        guard let controller = vc , let bill = bill as? ZPBill else {
            return Observable.empty()
        }
        return Observable.create({[weak controller] (s) -> Disposable in
            let gatewayVC = ZPPaymentGatewayViewController.init(with: url, bill, type)
            _ = gatewayVC.eResponse.subscribe(s)
            controller?.navigationController?.pushViewController(gatewayVC, animated: true)
            return Disposables.create()
        })
    }
    
    public static func showOnObjC(controller vc: UIViewController?,
                                  url: URL?,
                                  bill: AnyObject?,
                                  type: Int,
                                  handlerResponse: ((ZPPaymentResponse?) -> ())?,
                                  handlerError: ((Error) -> ())?,
                                  handlerCancel: (() -> ())?)
    {
        _ = self.showOn(controller: vc, url: url, bill: bill, type: type).subscribe(onNext: { (result) in
            switch result {
            case .cancel:
                handlerCancel?()
            case .response(let response):
                handlerResponse?(response)
            case .fail(let e):
                handlerError?(e)
            }
        })
    }
    
}

