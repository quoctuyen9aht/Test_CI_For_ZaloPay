//
//  ZPPaymentGatewayPresenter.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 5/8/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ZaloPayWalletSDKPrivate
import WebKit

class ZPPaymentGatewayPresenter: NSObject {
    weak var rootVC: ZPPaymentGatewayViewController?
    private var bill: ZPBill?
    let eResultWeb: PublishSubject<ZPPaymentGatewayState> = PublishSubject()
    private let disposeBag: DisposeBag = DisposeBag()
    private lazy var interactor = ZPPaymentGatewayInteractor.init(with: self.bill)
    private lazy var router: ZPPaymentGatewayRouter = ZPPaymentGatewayRouter.init()
    private lazy var trackActivity: ActivityIndicator = ActivityIndicator()
    override init() {
        super.init()
    }
    
    convenience init(with rootVC: ZPPaymentGatewayViewController?, _ bill: ZPBill?) {
        self.init()
        self.rootVC = rootVC
        self.bill = bill
        setupEvent()
    }
    
    func showAlertCancel() {
        let message = String(format: stringWarningCancelPayment, "thanh toán")
        ZPDialogView.showDialog(with: DialogTypeNotification,
                                message: message,
                                cancelButtonTitle: stringAlertTitleAccept,
                                otherButtonTitle: [stringAlertTitleCancel])
        { [weak self](idx, idxCancel) in
            guard idx == idxCancel else {
                return
            }
            self?.revertVoucher()
            self?.rootVC?.eResponse.onNext(.cancel)
        }
    }
    
    func loadSource() {
        let appid = self.bill?.appId.toInt()
        self.getMerchantUserInfor(from: appid).observeOn(MainScheduler.instance).subscribe(onNext: { [weak self] in
            let order = self?.bill?.encodeBase64($0)
            self?.rootVC?.loadURL(from: order)
            }, onError: { (e) in
                ZPDialogView.showDialogWithError(e, handle: nil)
        }).disposed(by: disposeBag)
    }
    
    private func getMerchantUserInfor(from appid: Int?) -> Observable<[String: Any]?>{
        return self.interactor.getMerchantUserInfor(from: appid).trackActivity(self.trackActivity)
    }
    
    private func setupEvent() {
        self.trackActivity.asDriver().drive(onNext: {
            $0 ? ZPProgressHUD.show(withStatus: nil) : ZPProgressHUD.dismiss()
        }).disposed(by: disposeBag)
        
        self.eResultWeb.bind { [weak self] (result) in
            switch result {
            case .sucess(let result):
                self?.trackStatus(with: result)
            case .fail(let e):
                self?.handler(error: e)
            }
        }.disposed(by: disposeBag)
        
        router.eResponse.bind { [weak self](response) in
            self?.rootVC?.eResponse.onNext(.response(response))
        }.disposed(by: disposeBag)
    }
    
    private func trackStatus(with json: [String: Any]?) {
        // Check if end transaction
        ZPProgressHUD.show(withStatus: nil)
        self.interactor.trackStatusResult(using: json).observeOn(MainScheduler.instance).trackActivity(self.trackActivity).subscribe(onNext: { [weak self](response) in
            self?.handler(response: response)
            }, onError: { [weak self](e) in
                self?.handler(error: e)
            }, onDisposed: {
               ZPProgressHUD.dismiss()
        }).disposed(by: disposeBag)
    }
    
    private func handler(response r: ZPPaymentResponse?) {
        let fee = Int(r?.userFeeAmount ?? 0)
        let totalCharge = self.bill?.amount ?? 0
        // Remove if it has voucher
        self.revertVoucher()
        router.showResult(from: self.rootVC, totalCharge + fee, fee, self.bill, r)
    }
    
    private func handler(error e: Error) {
        let response = ZPPaymentResponse.unidentifiedStatusObject()
        response?.errorCode = Int32((e as NSError).code)
        response?.exchangeStatus = -1
        response?.message = e.localizedDescription
        self.handler(response: response)
    }
    
    private func revertVoucher() {
        self.interactor.revertVoucher()
    }
    
    @discardableResult
    private func trackURLRespone(from path: String?) -> Bool {
//        #if DEBUG
            print("path: \(path ?? "")")
//        #endif
        guard let path = path, path.contains("result") else {
            return false
        }
        
        // Check condition
        let components = URLComponents.init(string: path)
        let queries = (components?.queryItems ?? []).reduce([String: Any](), { var result = $0 ; result[$1.name] = $1.value; return result })
        // Callback
//        #if DEBUG
            print("queries : \(queries)")
//        #endif

        defer {
            eResultWeb.onNext(.sucess(queries))
        }
        return true
    }
}

extension ZPPaymentGatewayPresenter: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return nil
    }
}

extension ZPPaymentGatewayPresenter: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        ZPProgressHUD.show(withStatus: nil)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
         ZPProgressHUD.dismiss()
        webView.evaluateJavaScript("window.location.href") { [weak self](r, e) in
            self?.trackURLRespone(from: r as? String)
        }
    }
    
    public func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        ZPProgressHUD.dismiss()
        eResultWeb.onNext(.fail(error))
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        #if DEBUG
            print("url : \(navigationAction.request.url?.absoluteString ?? "")")
        #endif
        
        let trackResult = self.trackURLRespone(from: navigationAction.request.url?.absoluteString)
        trackResult ? decisionHandler(.cancel) : decisionHandler(.allow)
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {}
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Swift.Void) {
        #if DEBUG
            print("url : \(navigationResponse.response.url?.absoluteString ?? "")")
        #endif
        decisionHandler(.allow)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        ZPProgressHUD.dismiss()
        guard (error as NSError).code != NSURLErrorNotConnectedToInternet else {
            ZPDialogView.showDialogWithError(error, handle: nil)
            return
        }
        eResultWeb.onNext(.fail(error))
    }
}

extension ZPPaymentGatewayPresenter : UIWebViewDelegate {
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        let trackResult = self.trackURLRespone(from: request.url?.absoluteString)
        return !trackResult
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {}
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        let r = webView.stringByEvaluatingJavaScript(from: "window.location.href")
        self.trackURLRespone(from: r)
        webView.stringByEvaluatingJavaScript(from: "ZaloPayJSBridge.test()")
        ZPProgressHUD.dismiss()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        guard (error as NSError).code != NSURLErrorNotConnectedToInternet else {
            ZPDialogView.showDialogWithError(error, handle: nil)
            return
        }
//        eResultWeb.onNext(.fail(error))
    }
}
