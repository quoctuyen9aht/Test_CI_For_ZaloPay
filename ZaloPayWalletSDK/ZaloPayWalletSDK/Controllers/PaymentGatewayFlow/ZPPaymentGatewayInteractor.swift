//
//  ZPPaymentGatewayInteractor.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 5/8/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ZaloPayWalletSDKPrivate

struct ZPPaymentGatewayInteractor {
    let bill: ZPBill?
    init(with bill: ZPBill?) {
        self.bill = bill
    }
    
    func trackStatusResult(using infor: [String: Any]?) -> Observable<ZPPaymentResponse?> {
        guard let infor = infor, let bill = self.bill else {
            return Observable.empty()
        }
        
        let transid = infor["zptransid"] as? String ?? ""
        return (~ZaloPayWalletSDKPayment.sharedInstance().getStatusTransId(transid,appid: bill.appId, start: Date())).map({ $0 as? ZPPaymentResponse})
    }
    
    func getMerchantUserInfor(from appid: Int?) -> Observable<[String: Any]?>{
        guard let appid = appid  else {
            return Observable.empty()
        }
        
        return (~ZaloPayWalletSDKPayment.sharedInstance().getMerchantUserInfor(from: appid)).map({ $0 as? [String: Any] })
        
    }
    
    func revertVoucher() {
        // Revert voucher
        bill?.voucherInfo = nil
        ZaloPayWalletSDKPayment.sharedInstance().zpVoucherManager.revertArrVoucherAndUse(voucher: nil)
    }
}
