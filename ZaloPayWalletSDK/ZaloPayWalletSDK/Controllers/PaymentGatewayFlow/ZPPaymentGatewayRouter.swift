//
//  ZPPaymentGatewayRouter.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 5/8/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import ZaloPayWalletSDKPrivate

class ZPPaymentGatewayRouter {
    private let resultHandler: ZPPaymentPopupResultWraper
    let eResponse: PublishSubject<ZPPaymentResponse?>
    private let disposeBag = DisposeBag()
    init() {
        resultHandler = ZPPaymentPopupResultWraper.init()
        self.eResponse = PublishSubject()
    }
    
    func showResult(from controller: UIViewController?,
                    _ totalCharge: Int,
                    _ feeCharge: Int,
                    _ bill: ZPBill?,
                    _ response: ZPPaymentResponse?)
    {
        guard let bill = bill ,
            let controller = controller,
            let response = response else {
            return
        }
        
        resultHandler.showResult(from: controller, response: response, totalCharge: totalCharge, feeCharge: feeCharge, bill: bill).subscribe(onNext: { [weak self](r) in
            self?.eResponse.onNext(r)
        }, onCompleted: { [weak self] in
            self?.eResponse.onCompleted()
        }).disposed(by: disposeBag)
    }
    
}
