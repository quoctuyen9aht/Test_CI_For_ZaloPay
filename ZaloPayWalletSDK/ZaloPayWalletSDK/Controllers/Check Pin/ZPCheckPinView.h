//
//  ZPCheckPinView.h
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 5/11/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ZPCheckPinViewDelegate<NSObject>
- (void)checkPinDidTapClose;
@end

typedef enum {
    TouchIdCasesHided,
    TouchIdCasesUnchecked,
    TouchIdCasesChecked
} TouchIdCases;

@protocol ZPCheckPinViewDataSource<NSObject>
- (NSString *)title;
- (NSInteger)lengthPassword;
- (UIColor *)colorSelected;
- (UIColor *)colorUnselected;
- (CGFloat)borderWidth;
- (CGSize)sizePassword;
@end

@interface ZPCheckPinView : UIView
@property (weak, nonatomic) id<ZPCheckPinViewDelegate> delegate;
@property (weak, nonatomic) id<ZPCheckPinViewDataSource> dataSource;
@property (strong, nonatomic) UILabel *title;
@property (assign, nonatomic) CGFloat space;
- (instancetype)initWith:(RACSignal *)signal
               withSpace:(CGFloat)space
              methodName:(NSString *)methodName
             methodImage:(UIImage *)image
          suggestTouchId:(BOOL)isSuggest;
- (BOOL)enableTouchIdChecked;
- (BOOL)isLoadingData;
- (void)reloadData;
- (void)startLoading;
- (void)stopLoading;
- (void)showErrorMessage:(NSString *)errorMessage;
@end
