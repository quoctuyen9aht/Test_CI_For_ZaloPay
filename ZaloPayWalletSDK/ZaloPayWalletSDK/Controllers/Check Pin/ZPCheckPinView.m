//
//  ZPCheckPinView.m
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 5/11/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPCheckPinView.h"
#import "DGActivityIndicatorView.h"
#import "ZaloPayWalletSDKPayment.h"
#import <ZaloPayAnalyticsSwift/ZaloPayAnalyticsSwift-Swift.h>

@interface ZPCheckPinCell: UICollectionViewCell
@property (strong, nonatomic) UIView *circleView;
@property (strong, nonatomic) UIColor *colorSelected;
@property (strong, nonatomic) UIColor *colorUnselected;
@property (assign, nonatomic) CGFloat borderWidth;
- (void)reloadView;
@end

@interface ZPCheckPinView()<UICollectionViewDataSource>
@property (strong, nonatomic) RACSignal *signal;
@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) UICollectionViewFlowLayout *flowLayout;
@property (nonatomic, strong) UILabel *labelError;
@property (nonnull, strong) UIButton *checkButton;
@property (nonatomic, strong) DGActivityIndicatorView *spinnerView;
@property (nonatomic, assign) TouchIdCases touchIdCases;
@end

@implementation ZPCheckPinView
- (instancetype)initWith:(RACSignal *)signal
               withSpace:(CGFloat)space
              methodName:(NSString *)methodName
             methodImage:(UIImage *)image
          suggestTouchId:(BOOL)isSuggest
{
    TouchIdCases touchIdCase =  [self touchIDCaseWithSuggest: isSuggest];
    float heightReduced = image == nil ? 40.0 : 0.0;
    float height = ((touchIdCase != TouchIdCasesHided) ? 211: 181) - heightReduced;
    self = [super initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, height)];
    self.space = space;
    self.signal = signal;
    [self setupView:touchIdCase image:image];
    [self addMethodInfoViewWithImage:image methodName:methodName];
    [self setupHandlerSignal];
    return self;
}

#pragma mark - Create View
- (void)setupView:(TouchIdCases)touchIdCase image: (UIImage*)image {
    self.backgroundColor = [UIColor whiteColor];
    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    closeBtn.titleLabel.font = [UIFont iconFontWithSize:16];
    [closeBtn setIconFont:@"red_delete" forState:UIControlStateNormal];
    [closeBtn setTitleColor:[UIColor subText] forState:UIControlStateNormal];
    [self addSubview:closeBtn];
    [closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.left.equalTo(0);
        make.size.equalTo(CGSizeMake(40, 40));
    }];
    
    @weakify(self);
    closeBtn.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        @strongify(self);
        return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
            [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionSdk_password_back];
            if (self.delegate && [self.delegate respondsToSelector:@selector(checkPinDidTapClose)]) {
                [self.delegate checkPinDidTapClose];
            }
            [subscriber sendCompleted];
            return nil;
        }];
    }];
    
    self.title = [[UILabel alloc] initWithFrame:CGRectZero];
    self.title.textAlignment = NSTextAlignmentCenter;
    self.title.font = [UIFont defaultSFUITextRegular];
    self.title.numberOfLines = 0;
    [self addSubview:self.title];
    
    [_title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(26.0);
        make.height.greaterThanOrEqualTo(0);
        make.left.equalTo(30.0);
        make.right.equalTo(-30.0);
    }];
    
    // Collection View
    self.flowLayout = [UICollectionViewFlowLayout new];
    self.flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:[UICollectionViewFlowLayout new]];
    
    self.flowLayout.sectionInset = UIEdgeInsetsZero;
    self.flowLayout.minimumLineSpacing = 0;
    self.flowLayout.minimumInteritemSpacing = _space;
    self.collectionView.collectionViewLayout = _flowLayout;
    
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.dataSource = self;
    
    self.collectionView.allowsMultipleSelection = YES;
    self.collectionView.userInteractionEnabled = NO;
    
    [self.collectionView registerClass:[ZPCheckPinCell class] forCellWithReuseIdentifier:@"ZPCheckPinCell"];
    self.collectionView.clipsToBounds = NO;
    [self addSubview:self.collectionView];
    float top = image == nil ? 20.0 : 55.0;
    [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.title.mas_bottom).equalTo(top);
        make.centerX.equalTo(@0);
        make.size.equalTo(CGSizeZero);
    }];
    UIView *vLine = [[UIView alloc] initWithFrame:CGRectZero];
    vLine.backgroundColor = [UIColor lineColor];
    [self addSubview:vLine];
    [vLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(@0);
        make.centerX.equalTo(@0);
        make.size.equalTo(CGSizeMake([UIScreen mainScreen].bounds.size.width, 1));
    }];
    
    [self addUsingTouchIdCheckboxWithTouchIdCase:touchIdCase image: image];
}


- (TouchIdCases)touchIDCaseWithSuggest: (BOOL)suggest {
    id <ZPWalletDependenceProtocol> helper = [ZaloPayWalletSDKPayment sharedInstance].appDependence;
    if (!suggest) {
        return TouchIdCasesHided;
    } else if (!helper.isTouchIDExist) {
        // máy không có touchID
        return TouchIdCasesHided;
    } else if (!helper.isEnableTouchID) {
        return TouchIdCasesUnchecked;
    } else if (helper.usingTouchId) {
        return TouchIdCasesHided;
    } else {
        return helper.turnOffTouchIdManual == false ? TouchIdCasesChecked : TouchIdCasesUnchecked;
    }
}


- (void)addMethodInfoViewWithImage:(UIImage *)image methodName:(NSString *)methodName {
    UIView *methodInfoView = [[UIView alloc] initWithFrame:CGRectZero];
    [self addSubview:methodInfoView];
    
    [methodInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.title.mas_bottom).equalTo(5);
        make.centerX.equalTo(0);
        make.height.greaterThanOrEqualTo(0);
    }];
    
    UILabel *labelMethodName = [[UILabel alloc] init];
    [labelMethodName zpMainBlackRegular];
    [methodInfoView addSubview:labelMethodName];
    
    CGSize sIcon = image != nil ? CGSizeMake(23, 23) : CGSizeZero;
    if (image != nil) {
        UIImageView *methodIcon = [[UIImageView alloc] initWithImage:image];
        [methodInfoView addSubview:methodIcon];
        
        [methodIcon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.equalTo(sIcon);
            make.left.equalTo(0);
            make.top.equalTo(0);
            make.bottom.equalTo(0);
        }];
    }else {
        [methodInfoView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(23);
        }];
    }
    
    [labelMethodName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(0);
        make.left.equalTo(sIcon.width + 3);
        make.height.greaterThanOrEqualTo(0);
        make.width.greaterThanOrEqualTo(0);
        make.right.equalTo(-3);
    }];
    labelMethodName.text = methodName;
}

- (UILabel *)labelError {
    if (!_labelError) {
        UILabel *label = [[UILabel alloc] init];
        label.numberOfLines = 2;
        label.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
        label.adjustsFontSizeToFitWidth = YES;
        [self addSubview:label];
        label.font = [UIFont SFUITextRegularItalicWithSize:13];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor zp_red];
        CGRect rectC = [self.collectionView convertRect:self.collectionView.bounds toView:self];
        CGFloat hM = self.bounds.size.height - (rectC.origin.y + rectC.size.height);
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(20.0);
            make.right.equalTo(-20.0);
            UIView *vBottom = [self viewWithTag:301];
            CGFloat hBottom = vBottom ? vBottom.bounds.size.height : 0;
            make.bottom.equalTo(-hBottom - 10);
            make.height.equalTo(hM - hBottom - 10);
        }];
        
       
        _labelError = label;
    }
    return _labelError;
}


- (void)addUsingTouchIdCheckboxWithTouchIdCase: (TouchIdCases)touchIdCase image: (UIImage*) image {
    if (touchIdCase == TouchIdCasesHided) {
        return;
    }
    UIControl *touchIdInfo = [[UIControl alloc] init];
    touchIdInfo.tag = 301;
    [self addSubview:touchIdInfo];
    UIButton *checkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [checkButton roundRect:3];
    checkButton.layer.borderColor = touchIdCase == TouchIdCasesChecked ? [UIColor payColor].CGColor :
                                                                         [UIColor subText].CGColor;
    checkButton.layer.borderWidth = 1.2;
    checkButton.titleLabel.font = [UIFont zaloPayWithSize:12];
    [checkButton setIconFont:@"general_check" forState:UIControlStateSelected];
    [checkButton setBackgroundColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [checkButton setBackgroundColor:[UIColor payColor] forState:UIControlStateSelected];
    [checkButton setTitleColor:[UIColor subText] forState:UIControlStateNormal];
    [checkButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    checkButton.userInteractionEnabled = NO;
    checkButton.selected = touchIdCase == TouchIdCasesChecked;
    self.checkButton = checkButton;
    id <ZPWalletDependenceProtocol> helper = [ZaloPayWalletSDKPayment sharedInstance].appDependence;
    [[touchIdInfo rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(UIControl *control) {
        NSString *message = [R string_TouchID_Enable_System_Setting];
        message = [helper checkToReplaceFaceIDMessage:message];
        if (!checkButton.selected && helper.isEnableTouchID == false) {
            [ZPDialogView showDialogWithType:DialogTypeNotification
                                     message:message
                           cancelButtonTitle:[R string_ButtonLabel_Close]
                            otherButtonTitle:nil
                              completeHandle:^(int buttonIndex, int cancelButtonIndex) {
                              }];
            return;
        }
        checkButton.selected = !checkButton.selected;
        NSInteger event = checkButton.selected ? ZPAnalyticEventActionPopupsdk_auth_confirm : ZPAnalyticEventActionPopupsdk_auth_cancel;
        [[ZPTrackingHelper shared] trackEvent:event];
        checkButton.layer.borderColor = checkButton.selected ? [UIColor payColor].CGColor : [UIColor subText].CGColor;
        if (!checkButton.isSelected && !helper.turnOffTouchIdManual) {
            helper.turnOffTouchIdManual = true;
        }
    }];
    
    UILabel *labelDescription = [[UILabel alloc] init];
    labelDescription.userInteractionEnabled = NO;
    int fontSize = [UIView isScreen2x] ? 14 : 15;
    labelDescription.font = [UIFont SFUITextRegularWithSize:fontSize];
    labelDescription.textColor = [UIColor subText];
    
    NSString *message = [helper checkToReplaceFaceIDMessage:R.string_Use_TouchId_Payment_Later];
    labelDescription.text = message;
    CGSize size = [labelDescription sizeThatFits:CGSizeMake(500, 20)];
    [touchIdInfo addSubview:checkButton];
    [touchIdInfo addSubview:labelDescription];
    
    [checkButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(0);
        make.centerY.equalTo(0);
        make.size.equalTo(CGSizeMake(20, 20));
    }];
    
    [labelDescription mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(size);
        make.centerY.equalTo(0);
        make.left.equalTo(checkButton.mas_right).offset(5);
        make.right.equalTo(0);
    }];
    
    
    float heightReduced = image == nil ? 40.0 : 0.0;
    [touchIdInfo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(0);
        make.height.equalTo(MAX(heightReduced, size.height));
        make.bottom.equalTo(-10);
    }];
}

- (DGActivityIndicatorView *)spinnerView {
    if (!_spinnerView) {
        _spinnerView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor subText]];
        _spinnerView.bounds = CGRectMake(0, 0, 36, 10);
        [self addSubview:_spinnerView];
        CGSize size = _spinnerView.frame.size;
        [_spinnerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.equalTo(size);
            make.centerX.equalTo(self.labelError.mas_centerX);
            make.centerY.equalTo(self.labelError.mas_centerY);
        }];
    }
    return _spinnerView;
}

- (void)setupHandlerSignal {
    @weakify(self);
    [[self.signal filter:^BOOL(id value) {
        return [value isKindOfClass:[NSString class]];
    }] subscribeNext:^(NSString *newText) {
        @strongify(self);
        NSInteger lenght = MIN([newText length], [self.dataSource lengthPassword]);
        NSArray<ZPCheckPinCell *> *cells = [self.collectionView visibleCells];
        [cells forEach:^(ZPCheckPinCell *cell) {
            NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
            if (indexPath) {
                [cell setSelected:indexPath.item < lenght];
            }
        }];
    }];
}

#pragma mark - Update UI

- (void)startLoading {
    [self.spinnerView startAnimating];
    self.spinnerView.hidden = NO;
    self.labelError.text = @"";
}

- (void)stopLoading {
    [self.spinnerView stopAnimating];
    self.spinnerView.hidden = YES;
}

- (void)showErrorMessage:(NSString *)errorMessage {
    [self stopLoading];
    self.labelError.text = errorMessage;
}

- (void)reloadData {
    _title.text = [self.dataSource title];
    NSInteger numberItem = [self.dataSource lengthPassword];
    CGSize sizePassword = [self.dataSource sizePassword];
    CGFloat space = self.space;
    self.flowLayout.itemSize = sizePassword;
    CGFloat widthCollectionView = numberItem * sizePassword.width + MAX(numberItem - 1, 0) * space;
    CGSize sizeCollectionView = CGSizeMake(widthCollectionView, sizePassword.height);
    [_collectionView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(sizeCollectionView);
    }];
    
    [self layoutIfNeeded];
}

- (BOOL)isLoadingData {
    return [self.spinnerView animating];
}
- (BOOL)enableTouchIdChecked {
    return self.checkButton.selected;
}
#pragma mark - CollectionView DataSource
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ZPCheckPinCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ZPCheckPinCell" forIndexPath:indexPath];
    cell.colorSelected = [self.dataSource colorSelected];
    cell.colorUnselected = [self.dataSource colorUnselected];
    cell.borderWidth = [self.dataSource borderWidth];
    [cell reloadView];
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.dataSource lengthPassword];
}

@end

#pragma mark - Cell
@implementation ZPCheckPinCell
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    [self setupView];
    return self;
}

- (void)setupView {
    self.circleView = [[UIView alloc] initWithFrame:self.frame];
    self.circleView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.circleView];
    
}

- (void)reloadView {
    self.circleView.layer.cornerRadius = self.frame.size.width / 2;
    self.circleView.clipsToBounds = YES;
    self.circleView.layer.borderWidth = self.borderWidth;
    self.circleView.layer.borderColor = self.colorSelected.CGColor;
}

- (void)setSelected:(BOOL)selected {
    super.selected = selected;
    self.circleView.backgroundColor = selected ? self.colorSelected : self.colorUnselected;
}

@end
