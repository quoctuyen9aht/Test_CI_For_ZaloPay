//
//  ZPContainerViewController+Voucher.h
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 8/2/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZPContainerViewController.h"
@class ZPVoucherInfo;
@interface ZPContainerViewController(Voucher)
- (void)deleteCurrentVoucherAndPromotionWith:(ZPBill *)bill;
- (void)updateZPOrderHeaderViewWithVoucherInfo:(ZPVoucherInfo *)result;
- (void)updateZPOrderHeaderViewWithPromotionInfo:(ZPPromotionInfo *)result;
@end
