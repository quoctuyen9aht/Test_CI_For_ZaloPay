//
//  ZPResultHandler.h
//  ZaloPayWalletSDK
//
//  Created by Thi La on 4/26/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPPaymentHandler.h"
#import "ZPPaymentResponse.h"

@protocol ZPResultHandlerDelegate <NSObject>
@required
- (void)paymentControllerDidClose:(NSString *)key data:(NSString *)data;

- (void)paymentControllerNeedShowSupportAction;
- (void)paymentControllerShouldNotifyPaymentSuccess;
- (void)paymentControllerDidFinishWithResponse:(ZPPaymentResponse *)response;
@end

@interface ZPResultHandler : NSObject <UITableViewDelegate, UITableViewDataSource>
@property(strong, nonatomic) ZPPaymentResponse *responseResult;
@property(assign, nonatomic) BOOL isPaymentSuccess;
@property(assign, nonatomic) long totalCharge;
@property(assign, nonatomic) long feeCharge;
@property(weak, nonatomic) id <ZPResultHandlerDelegate> delegate;
@property(weak, nonatomic) UITableView *mTableView;
@property(strong, nonatomic) ZPBill *bill;

- (void)processPayment;

- (void)initialization;
@end
