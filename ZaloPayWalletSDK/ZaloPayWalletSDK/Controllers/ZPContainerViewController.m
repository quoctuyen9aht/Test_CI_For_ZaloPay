//
//  ZPContainerViewController.m
//  ZaloPayWalletSDK
//
//  Created by Nguyen Xuan Phung on 4/23/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPContainerViewController.h"
#import "ZPResourceManager.h"
#import "ZPDataManager.h"
#import "ZPPaymentMethod.h"
#import "ZPPaymentChannel.h"
#import "UIColor+ZPExtension.h"
#import "ZPPaymentResponse.h"
#import "ZPCreditCardHandler.h"
#import "ZPMethodsHandler.h"
#import "ZPATMHandler.h"
#import "ZPResultHandler.h"
#import "ZPProgressHUD.h"
#import "ZaloPayWalletSDKPayment.h"
#import "ZPIBankingHandle.h"
#import "ZPBIDVHandler.h"
#import "ZPVTBHandler.h"
#import "ZPSCBHandler.h"
#import "ZPSGCBHandler.h"
#import "ZPDefaultBankHandler.h"
#import "ZPVCBHandler.h"
#import "ZPEIBHandler.h"
#import "ZPResultViewController.h"
#import "ZPTableViewDelegateProxy.h"
#import "ZaloPayWalletSDKLog.h"
#import "UIDevice+ZPExtension.h"
#import <ZaloPayWalletSDK/ZaloPayWalletSDK-Swift.h>
#import "NetworkManager+ZaloPayWalletSDK.h"
#import "ZPBank.h"
#import "ZPVoucherInfo.h"
#import "ZPVCCBBank.h"
#import <ZaloPayAnalyticsSwift/ZaloPayAnalyticsSwift-Swift.h>
#import "ZPBIDVIBankingHandler.h"
#import "ZPPromotionManager.h"
#import "ZPPromotionInfo.h"

#define heightViewPayWithVouchersListBase [UIView isIPhoneX] ? 78 : 68
#define heightViewPayWithVouchersListHaveVoucher 116
#define heightViewPayWithVouchersListExpendMax 400
#define heightViewExpendVoucherList       50

@interface ZPContainerViewController () <UITextFieldDelegate, ZPPaymentViewControllerDelegate, UIGestureRecognizerDelegate, KYCFlowDelegateProtocol>
@property(assign, nonatomic) long feeAmount;
@property(strong, nonatomic) NSMutableArray *paymentControllers;
@property(nonatomic, strong) ZPTableViewDelegateProxy *proxy;
@property(assign, nonatomic) BOOL isExpendVouchersList;
@end

@implementation ZPContainerViewController

- (instancetype)init {
    self = [super init];
    if (self) {
        _paymentControllers = [[NSMutableArray alloc] init];
        _proxy = [ZPTableViewDelegateProxy new];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.view.multipleTouchEnabled = NO;
    self.view.exclusiveTouch = YES;

    [self addTableView];
    [self addActionButton];
    [self setupHandler];
    [self sendScreenName];
    
    [[ZPTrackingHelper shared].eventTracker trackScreen:[self screenName]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionSdk_open];
    [[ZPTrackingHelper shared] trackScreenWithController:self];
}

#pragma mark - override

- (NSString *)screenName {
    if ([self.bill isKindOfClass:[ZPIBankingAccountBill class]]) {
        return @"";
    }
    if (self.bill.transType == ZPTransTypeAtmMapCard) {
        return ZPTrackerScreen.Screen_iOS_Bank_InputCardInfo;
    }
    return ZPTrackerScreen.Screen_iOS_SDKF_Main;
}

- (BOOL)isAllowSwipeBack {
    return NO;
}

- (void)sendScreenName {
    if ([self.bill isKindOfClass:[ZPIBankingAccountBill class]]) {
        return;
    }
    [[ZPTrackingHelper shared].eventTracker trackScreen:[self screenName]];
}

- (NSArray *)leftBarButtonItems {
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.multipleTouchEnabled = NO;
    backButton.exclusiveTouch = YES;
    [backButton setTitle:[R string_ButtonLabel_Cancel] forState:UIControlStateNormal];
    [backButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    backButton.titleLabel.font = [UIFont SFUITextRegularWithSize:15];
    backButton.frame = CGRectMake(0, 0, 44, 44);
    [backButton addTarget:self action:@selector(backButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [backButton addTarget:self action:@selector(trackEventBack) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];

    UIBarButtonItem *negativeSeparator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSeparator.width = -12;
    return @[negativeSeparator, backBarButtonItem];
}

- (void)addActionButton {
    if (self.bill.transType == ZPTransTypeAtmMapCard) {
        return;
    }
    self.zpVoucherViewWithButton = [[ZPVoucherViewWithButton alloc] initWithBill:self.bill fromController:self];
    [self.view addSubview:self.zpVoucherViewWithButton];
    [self.zpVoucherViewWithButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(0);
        make.left.equalTo(0);
        make.right.equalTo(0);
        make.height.equalTo(heightViewPayWithVouchersListBase);
    }];
    self.zpVoucherViewWithButton.backgroundColor = [UIColor zpLightLightLightGrayColor];
    self.actionButton = self.zpVoucherViewWithButton.actionButton;
}

- (void)addTableView {
    UITableView *tableView = [[UITableView alloc] init];
    tableView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:tableView];
    self.zpw_tableMain = tableView;
    self.zpw_tableMain.delegate = self.proxy;
    self.zpw_tableMain.dataSource = self.proxy;
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.left.equalTo(0);
        make.right.equalTo(0);
        make.bottom.equalTo(0);
    }];
}

- (void)setupHandler {
    if ([ZPProgressHUD isVisible]) {
        [ZPProgressHUD dismiss];
    }
    ZPPaymentMethod *method = [self getDefaultMethod];
    [self paymentMethodsControllerDidChooseMethod:method];
    [self updatePaymentTitle];
}


- (ZPPaymentMethod *)getDefaultMethod {
    if ([self.bill isKindOfClass:[ZPIBankingAccountBill class]]) {
        ZPPaymentMethod *method = [[ZPPaymentMethod alloc] init];
        method.methodType = ZPPaymentMethodTypeIbanking;
        return method;
    }

    if (self.bill.transType == ZPTransTypeAtmMapCard) {
        for (ZPPaymentMethod *oneMethod in self.paymentMethods) {
            if (oneMethod.defaultChannel && oneMethod.methodType == ZPPaymentMethodTypeAtm) {
                return oneMethod;
            }
        }
    }

    ZPPaymentMethod *method = [[ZPPaymentMethod alloc] init];
    method.methodType = ZPPaymentMethodTypeMethods;
    return method;
}

- (void)backButtonClicked:(id)sender {
    [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionLinkbank_add_bank_touch_close];
    [self onClickCloseButton:nil];
}

- (IBAction)onClickCloseButton:(id)sender {
    ZPPaymentHandler *controller = [self currentController];
    [self dismissSDKKeyboard];
    @weakify(self);
    NSString *title = [NSString stringWithFormat:stringWarningCancelPayment, [[[ZPDataManager sharedInstance] getTitleByTranstype:self.bill.transType] lowercaseString]];
    [ZPDialogView showDialogWithType:DialogTypeNotification
                             message:title
                   cancelButtonTitle:stringAlertTitleAccept
                    otherButtonTitle:@[stringAlertTitleCancel]
                      completeHandle:^(int buttonIndex, int cancelButtonIndex) {
                          @strongify(self);
                          [self trackEventActionCancel:buttonIndex cancelButton:cancelButtonIndex];
                          if (buttonIndex == cancelButtonIndex) {
                              [self notifyAndCloseSDK:controller andKeyNotify:nil];
                              return;
                          }

                          [self showSDKKeyboard];
                      }];
}

- (void)trackEventActionCancel:(int)buttonIndex cancelButton:(int)cancelButtonIndex {
    if (self.bill.transType == ZPTransTypeAtmMapCard) {
        if (buttonIndex == cancelButtonIndex) {
            [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionLinkbank_add_bank_close_touch_yes];
        } else {
            [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionLinkbank_add_bank_close_touch_no];
        }
        return;
    }
    [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionSdk_cancel];
}

- (void)notifyAndCloseSDK:(ZPPaymentHandler *)handler andKeyNotify:(NSString *)keyNotify {
    NSString *keyNotifyGet = keyNotify == nil ? ZP_NOTIF_PAYMENT_CLOSE : keyNotify;

    [[ZPAppFactory sharedInstance].orderTracking trackUserCancel:self.bill.appTransID];
    [ZaloPaySDKUtil writeAppTransIdLog:self.bill];
    [self cancelBill];
    [handler viewWillClose];
    [handler viewWillBack];
    [self dismissSDKKeyboard];
    if ([self.delegate respondsToSelector:@selector(paymentControllerDidClose:data:)]) {
        [self.delegate paymentControllerDidClose:keyNotifyGet data:nil];
    }
}

- (void)cancelBill {
    if (self.bill.transType == ZPTransTypeWalletTopup) {
        [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionBalance_addcash_touch_back];
        return;
    }

    if (self.bill.transType == ZPTransTypeWithDraw) {
        [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionBalance_addcash_touch_back];
        return;
    }
    if (self.bill.transType == ZPTransTypeTranfer) {
        [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionBalance_addcash_touch_back];
    }
}

- (void)calculateFeeWithMethod:(ZPPaymentMethod *)method {
    [method calculateChargeWith:self.bill.finalAmount];
    self.feeAmount = method.totalChargeFee;
}

- (void)updateVoucherView {
    if (self.zpVoucherViewWithButton) {
        [self.zpVoucherViewWithButton mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(0);
        }];
        self.zpVoucherViewWithButton.hidden = YES;
        self.actionButton.hidden = YES;
    }
}

#pragma mark - ZPPaymentViewControllerDelegate

- (void)paymentControllerDidFinishWithResponse:(ZPPaymentResponse *)response {
    DDLogInfo(@"---paymentControllerDidFinishWithResponse errorStep: %ld", (long) response.errorStep);
    [self handleReturnResponse:response];
}

- (void)paymentControllerNotifyResultWithResponse:(ZPPaymentResponse *)response {
    DDLogInfo(@"---paymentControllerNotifyResultWithResponse errorStep: %ld", (long) response.errorStep);
    DDLogInfo(@"paymentControllerNotifyResultWithResponse balance %lld", response.balance);
    [self handleReturnResponse:response];
}

- (void)paymentControllerDidClose:(NSString *)key data:(NSString *)data {
    if (self.delegate && [self.delegate respondsToSelector:@selector(paymentControllerDidClose:data:)]) {
        [self.delegate paymentControllerDidClose:key data:data];
    }
}

- (void)paymentControllerNotifyCloseWithKey:(NSString *)key data:(NSString *)data {
    if (self.delegate && [self.delegate respondsToSelector:@selector(paymentControllerNotifyCloseWithKey:data:)]) {
        [self.delegate paymentControllerNotifyCloseWithKey:key data:data];
    }
}

- (void)paymentControllerSwitchToCreditCardHandler:(NSString *)data {
    //Check prevent map cc when pay transfer,deposit,redpacket
    ZPCreditCardHandler *handler = (ZPCreditCardHandler *) [self controllerForPaymentType:ZPPaymentMethodTypeCreditCard
                                                                                 bankCode:@""];
    handler.creditCard.cardNumber = data;
    [self atmReplaceLastHandler:handler];
}

- (void)paymentControllerSwitchToAtmCardHandler:(NSString *)data {
    ZPATMHandler *handler = (ZPATMHandler *) [self controllerForPaymentType:ZPPaymentMethodTypeAtm bankCode:@""];
    DDLogInfo(@"card number: %@", data);
    handler.isBackFromCC = YES;
    handler.atmCard.cardNumber = data;
    [self atmReplaceLastHandler:handler];
}

- (void)paymentControllerSwitchToBankHandler:(ZPBank *)bank {
    ZPPaymentHandler *handler = [self controllerForPaymentBankCode:bank.bankCode number:bank.detectString];
    [self atmReplaceLastHandlerWithoutReloadData:handler bank:bank];
}

- (void)atmReplaceLastHandler:(ZPPaymentHandler *)handler {
    if (self.paymentControllers.count >= 1) {
        ZPPaymentHandler *last = [self currentController];
        [last viewWillBack];
        [self.paymentControllers addObjectNotNil:handler];
        [self showPaymentController:handler];
    }
}

- (void)atmReplaceLastHandlerWithoutReloadData:(ZPPaymentHandler *)handler bank:(ZPBank*)bank {
    if (self.paymentControllers.count >= 1) {
        [self.paymentControllers removeLastObject];
        [self.paymentControllers addObjectNotNil:handler];
        ZPPaymentViewCell *cell = [ZPPaymentViewCell castFrom:[self.zpw_tableMain cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]]];
        if (cell) {
            cell.delegate = handler;
            self.proxy.handler = handler;
            if (cell.delegate && [cell.delegate respondsToSelector:@selector(detectCell:didDetectBank:)]) {
                [cell.delegate detectCell:cell didDetectBank:bank];
            }
        }
    }
}

- (void)paymentControllerSwitchToIBankingHandler:(ZPAtmCard *)bankInfo {

    if (self.paymentControllers.count >= 1) {
        [self.paymentControllers removeAllObjects];
        self.bill.transType = ZPTransTypeAtmMapCard;
        self.bill.bankCode = bankInfo.bankCode;
        ZPPaymentHandler *handler =[self controllerForPaymentType:ZPPaymentMethodTypeIbanking bankCode:bankInfo.bankCode];
        [self.paymentControllers addObjectNotNil:handler];
        [self showPaymentController:handler];
    }
}

- (void)paymentControllerDidFinishWithResult:(BOOL)result andResponse:(ZPPaymentResponse *)response {
    OrderStepResult sdkResult = response.errorCode >= 1 ? OrderStepResult_Success : OrderStepResult_Fail;
    [[ZPAppFactory sharedInstance].orderTracking trackOrderResult:self.bill.appTransID sdkResult:sdkResult serverResult:response.originalCode];

    if (self.bill.appId == lixiAppId && result) {
        [self notifyWithoutShowResultPage:response];
        return;
    }

    if (self.bill.shouldSkipSuccessView && result) {
        [self handleFlowDirectToMapcardFromPaymentWith:response];
        return;
    }
    // Hide btn PreNext On ZPATMSaveCardCell
    [[NSNotificationCenter defaultCenter] postNotificationName:ZPUIButtonPreNextDidHideNotification object:nil];

    ZPResultHandler *resultHandler = [[ZPResultHandler alloc] init];
    resultHandler.bill = self.bill;
    resultHandler.responseResult = response;
    resultHandler.totalCharge = self.bill.amount + self.feeAmount - [self.bill finalDiscountAmount];
    resultHandler.feeCharge = self.feeAmount;
    resultHandler.isPaymentSuccess = result;
    ZPResultViewController *resultViewController = [[ZPResultViewController alloc] initWithResultHandler:resultHandler];
    resultViewController.delegate = self.delegate;
    resultViewController.responseNotifySupport = response;
    [self.navigationController pushViewController:resultViewController animated:true];

    if (self.bill.transType == ZPTransTypeBillPay) {
        //thành công -> xoá voucher khỏi cache.
        ZPVoucherManager *manager = [ZaloPayWalletSDKPayment sharedInstance].zpVoucherManager;
        if (result) {
            [manager updateCacheAfterPaymentSuccessWithVoucher:self.bill.voucherInfo.voucherUsed forBill:self.bill];
        }
        ZPVoucherInfo *voucherRemove = result == true ? self.bill.voucherInfo : nil;
        [manager clearVouchersCaches];
        [manager revertArrVoucherAndUseVoucher:voucherRemove];
        
        ZPPromotionInfo *promotionRemove = result == true ? self.bill.promotionInfo : nil;
        ZPPromotionManager *promotionManager = [ZaloPayWalletSDKPayment sharedInstance].zpPromotionManager;
        [promotionManager revertPromotionsAndUsePromotion:promotionRemove];
    }
}

- (void)handleFlowDirectToMapcardFromPaymentWith:(ZPPaymentResponse *)response {

    if ([self.bill isKindOfClass:[ZPIBankingAccountBill class]]) {
        [ZPDataManager sharedInstance].isExistNewBankAccount = YES;
        [self notifyWithoutShowResultPage:response];
        return;
    }
    if (response.zpTransID.length <= 0) {
        [self notifyWithoutShowResultPage:response];
        return;
    }
    NSDictionary *info = @{@"zptransid": response.zpTransID};
    @weakify(self);
    [[[ZPDataManager sharedInstance] addSavedCard:info] subscribeCompleted:^{
        @strongify(self);
        [self notifyWithoutShowResultPage:response];
        [self cacheCardNumber];
    }];
}

- (void)cacheCardNumber {
    id object = [ZPDataManager sharedInstance].tempSavedCard;
    NSString *cardNumber = @"";
    if ([object isKindOfClass:[ZPCreditCard class]]) {
        cardNumber = ((ZPCreditCard *) object).cardNumber;
    } else if ([object isKindOfClass:[ZPAtmCard class]]) {
        cardNumber = ((ZPAtmCard *) object).cardNumber;
    }
    [ZPDataManager sharedInstance].savedCardNumber = cardNumber;
}

- (void)notifyWithoutShowResultPage:(ZPPaymentResponse *)response {
    if ([self.delegate respondsToSelector:@selector(paymentControllerNotifyResultWithResponse:)]) {
        [self.delegate paymentControllerNotifyResultWithResponse:response];
    }
    if ([self.delegate respondsToSelector:@selector(paymentControllerDidFinishWithResponse:)]) {
        [ZaloPaySDKUtil writeAppTransIdLog:self.bill];
        [self.delegate paymentControllerDidFinishWithResponse:response];
    }
}

- (void)handleReturnResponse:(ZPPaymentResponse *)response {
    DDLogInfo(@"// final: handleReturnResponse with errorCode: %d", response.errorCode);
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *announceMessage = response.message;

        if (response.errorCode >= 1) {

            switch (response.exchangeStatus) {

                case kZPZaloPayCreditStatusCodeFail:
                    if (announceMessage == NULL) {
                        announceMessage = [[ZPDataManager sharedInstance] stringByEnum:ZPStringTypeAnnounceMessageExchangeFail];
                    }

                    if (response.errorStep == ZPErrorStepAbleToRetry) {
                        [self showAlertNotifyWithMessage:announceMessage];
                    } else {
                        [self paymentControllerDidFinishWithResult:NO andResponse:response];

                    }

                    break;
                case kZPZaloPayCreditStatusCodeSuccess:
                    if (announceMessage == NULL) {
                        announceMessage = [[ZPDataManager sharedInstance] stringByEnum:ZPStringTypeAnnounceMessageExchangeSuccess];
                    }

                    [self paymentControllerDidFinishWithResult:YES andResponse:response];

                    break;
                case kZPZaloPayCreditStatusCodeUnidentified:
                    if (announceMessage == NULL) {
                        announceMessage = [[ZPDataManager sharedInstance] stringByEnum:ZPStringTypeAnnounceMessageExchangeUnidentified];
                    }
                    [self paymentControllerDidFinishWithResult:NO andResponse:response];
                    break;
                default:
                    break;
            }
        } else {
            DDLogInfo(@"handleReturnResponse error code < 1");
            if (announceMessage == NULL) {
                announceMessage = [[ZPDataManager sharedInstance] stringByEnum:ZPStringTypeAnnounceMessageExchangeFail];
            }
            if (response.errorStep == ZPErrorStepAbleToRetry) {
                DDLogInfo(@"able retry");
                [self showAlertNotifyWithMessage:announceMessage];
            } else {
                DDLogInfo(@"non able retry");
                // sever is maintainance
                if (response.errorCode == ZPServerErrorCodeMaintainance) {
                    //[ZPProgressHUD showWarningWithStatus:announceMessage];

                    [self showAlertNotifyWithMessage:announceMessage];
                    ZPPaymentHandler *controller = [self currentController];
                    [controller viewWillClose];
                    [self dismissSDKKeyboard];
                    if (self.delegate && [self.delegate respondsToSelector:@selector(paymentControllerDidClose:data:)]) {
                        [self.delegate paymentControllerDidClose:ZP_NOTIF_PAYMENT_IS_MAINTAINANCE data:nil];
                    }
                    return;
                }
                [self paymentControllerDidFinishWithResult:NO andResponse:response];
            }
        }

    });
}

- (void)paymentMethodsControllerDidChooseMethod:(ZPPaymentMethod *)method {
    DDLogInfo(@"select payment method: %ld", (long) method.methodType);
    NSString *bankCode = method.savedCard.bankCode ?: method.savedBankAccount.bankCode;
    ZPPaymentHandler *controller;
    switch (method.methodType) {
        case ZPPaymentMethodTypeSavedCard: {
            if ([method.savedCard isCCCard]) {
                controller = [self controllerForPaymentType:ZPPaymentMethodTypeCreditCard bankCode:bankCode];
                ((ZPCreditCardHandler *) controller).first6No = method.savedCard.first6CardNo;
                controller.currentStep = ZPCcPaymentStepSavedCardConfirm;
            } else {
                controller = [self controllerForPaymentType:ZPPaymentMethodTypeAtm bankCode:bankCode];
                controller = [self controllerForPaymentBankCode:method.savedCard.bankCode number:method.savedCard.first6CardNo];
                controller.atmCard.bankCode = method.savedCard.bankCode;
                controller.currentStep = ZPAtmPaymentStepSavedCardConfirm;
            }
            controller.bill.data = [method.savedCard.billData JSONRepresentation];
            controller.paymentMethodDidChoose = method;
            break;
        }
        case ZPPaymentMethodTypeIbanking: {
            controller = [self controllerForPaymentType:method.methodType bankCode:self.bill.bankCode];
            break;
        }
        default:
            controller = [self controllerForPaymentType:method.methodType bankCode:bankCode];
            break;
    }
    [self calculateFeeWithMethod:method];
    [self.paymentControllers addObjectNotNil:controller];
    [self showPaymentController:controller];

}

- (void)paymentMethodsControllerDidChooseMethod:(ZPPaymentMethod *)method password:(NSString *)password {
    DDLogInfo(@"select payment method: %ld", (long) method.methodType);
    ZPPaymentHandler *controller;
    switch (method.methodType) {
        case ZPPaymentMethodTypeSavedCard: {
            if ([method.savedCard isCCCard]) {
                controller = [self controllerForPaymentType:ZPPaymentMethodTypeCreditCard bankCode:method.savedCard.bankCode];
                ((ZPCreditCardHandler *) controller).first6No = method.savedCard.first6CardNo;
                ((ZPCreditCardHandler *) controller).creditCard.pin = password;
                controller.currentStep = ZPCcPaymentStepSavedCardConfirm;
            } else {
                controller = [self controllerForPaymentBankCode:method.savedCard.bankCode number:method.savedCard.first6CardNo];
                controller.atmCard.pin = password;
                controller.atmCard.bankCode = method.savedCard.bankCode;
                controller.currentStep = ZPAtmPaymentStepSavedCardConfirm;

            }
            controller.bill.data = [method.savedCard.billData JSONRepresentation];
            controller.paymentMethodDidChoose = method;

            break;
        }
        case ZPPaymentMethodTypeCCDebit: {

            controller = [self controllerForPaymentType:ZPPaymentMethodTypeCreditCard bankCode:stringDebitCardBankCode];
            ((ZPCreditCardHandler *) controller).first6No = method.savedCard.first6CardNo;
            ((ZPCreditCardHandler *) controller).creditCard.pin = password;
            controller.currentStep = ZPCcPaymentStepSavedCardConfirm;
            controller.bill.data = [method.savedCard.billData JSONRepresentation];
            controller.paymentMethodDidChoose = method;

            break;
        }
        case ZPPaymentMethodTypeSavedAccount: {
            controller = [self controllerForPaymentType:ZPPaymentMethodTypeSavedAccount bankCode:method.savedBankAccount.bankCode];
            controller.atmCard.pin = password;
            controller.atmCard.bankCode = method.savedBankAccount.bankCode;
            controller.currentStep = ZPAtmPaymentStepSavedCardConfirm;
            controller.bill.data = [method.savedBankAccount.billData JSONRepresentation];
            controller.paymentMethodDidChoose = method;
            break;
        }
        default:
            controller = [self controllerForPaymentType:method.methodType bankCode:@""];
            break;
    }
    [self calculateFeeWithMethod:method];
    [self.paymentControllers addObjectNotNil:controller];
    if (method.methodType == ZPPaymentMethodTypeCCDebit ||
            method.methodType == ZPPaymentMethodTypeSavedCard ||
            method.methodType == ZPPaymentMethodTypeSavedAccount) {
        [controller processPayment];
        return;
    }
    [self showPaymentController:controller];
}

- (void)showPaymentController:(ZPPaymentHandler *)newController {
    self.proxy.handler = newController;
    [newController displayOnTableView:self.zpw_tableMain fromController:self];
}

- (IBAction)onClickOkButton:(id)sender {
    [self.view endEditing:YES];
    ZPPaymentHandler *controller = [self currentController];
    if (controller) {
        [controller processPayment];
    }
}

- (NSString *)getBillDescription {
    return self.bill.billDescription != nil ? self.bill.billDescription : @"";
}

- (IBAction)onClickInfoButton:(UIButton *)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:ZPShowMapCardIntro object:nil];
}

- (void)updatePaymentTitle {
    self.title = [[self currentController] paymentControllerTitle];
}

- (ZPPaymentHandler *)currentController {
    return [self.paymentControllers lastObject];
}

- (void)paymentControllerDidGoBack {
    [self backButtonClicked:nil];
}

- (void)paymentControllerNeedRegisterWith:(ZPPaymentResponse *)response using:(id)cardInfo {
    if ([cardInfo isKindOfClass:[ZPAtmCard class]]) {
        ZPAtmCard *card = (ZPAtmCard *) cardInfo;
        NSString *jsonStr = [self atmCardInfo:card];
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:[self paramsWithBill:self.bill]];
        [params setObjectCheckNil:jsonStr forKey:@"cardinfo"];
        NSString *message = [response message] ?: @"";
        [params addEntriesFromDictionary:@{@"message": message}];
        @weakify(self);

        id <ZPWalletDependenceProtocol> helper = [ZaloPayWalletSDKPayment sharedInstance].appDependence;

        [helper registerBankFrom:self
                            with:params
                            from:card.zpTransID
                            with:^(id result)
        {
                                @strongify(self);
                                if (result == nil) {
                                    ZPPaymentHandler *controller = [self currentController];
                                    if (controller != nil) {
                                        [self notifyAndCloseSDK:controller andKeyNotify:ZP_NOTIF_PAYMENT_CANCEL_REGISTER_BANK];
                                    }
                                    return;
                                }
                                
                                ZPPaymentHandler *handler = [self currentController];
                                [handler saveAtmCard:card];
                                response.zpTransID = [NSString castFrom:result] ?: @"";
                                response.errorCode = ZALOPAY_ERRORCODE_SUCCESSFUL;
                                response.exchangeStatus = kZPZaloPayCreditStatusCodeSuccess;
                                [self paymentControllerDidFinishWithResponse:response];
                            } error:^(NSError *error)
        {
                    @strongify(self);
                    NSDictionary *infor = [NSDictionary castFrom:[error userInfo]];
                    if (infor) {
                        NSString *nMessage = [infor stringForKey:@"returnmessage" defaultValue:@""];
                        NSString *transId = [infor stringForKey:@"zptransid" defaultValue:@""];
                        response.message = nMessage;
                        if ([transId length] > 0) {
                            response.zpTransID = transId;
                        }

                    }
                    [self paymentControllerDidFinishWithResponse:response];
                }];
    } else {
        [self paymentControllerDidFinishWithResponse:response];
    }
}

#pragma mark - Private Methods

- (ZPPaymentHandler *)controllerForPaymentType:(ZPPaymentMethodType)type bankCode:(NSString *)bankCode {
    DDLogInfo(@"payment type: %ld", (long) type);
    NSDictionary *handlerMap = @{@(ZPPaymentMethodTypeIbanking) : [self findIBankingHandlerWith:bankCode],
                                 @(ZPPaymentMethodTypeMethods) : [ZPMethodsHandler class],
                                 @(ZPPaymentMethodTypeAtm) : [ZPATMHandler class],
                                 @(ZPPaymentMethodTypeSavedCard) : [ZPCreditCardHandler class],
                                 @(ZPPaymentMethodTypeCreditCard) : [ZPCreditCardHandler class],
                                 @(ZPPaymentMethodTypeSavedAccount) : [self findPaymentHandlerWith:bankCode]
                                 };
    
    Class class = [handlerMap objectForKey:@(type)];
    ZPPaymentHandler *controller = [[class alloc]  init];
    [self configHandler:controller withType:type bankCode:bankCode];
    return controller;
}

- (Class)findIBankingHandlerWith:(NSString*)bankCode {
    ZPBankCode bankEnum = [ZPResourceManager convertBankCodeToEnum:bankCode];
    NSDictionary *handlerMap = @{@(ZPBankCodeBIDV): [ZPBIDVIBankingHandler class],
                                 @(ZPBankCodeVCB):  [ZPIBankingHandle class]
                                 };
    Class class = [handlerMap objectForKey:@(bankEnum)] ? : [ZPDefaultBankHandler class];
    return class;
}

- (Class)findPaymentHandlerWith:(NSString*)bankCode {
    ZPBankCode bankEnum = [ZPResourceManager convertBankCodeToEnum:bankCode];
    NSDictionary *handlerMap = @{@(ZPBankCodeBIDV): [ZPBIDVHandler class],
                                 @(ZPBankCodeVTB):  [ZPVTBHandler class],
                                 @(ZPBankCodeSCB):  [ZPSCBHandler class],
                                 @(ZPBankCodeSGCB): [ZPSGCBHandler class],
                                 @(ZPBankCodeVCB):  [ZPVCBHandler class],
                                 @(ZPBankCodeEIB):  [ZPEIBHandler class],
                                 @(ZPBankCodeVCCB): [ZPVCCBBank class],
                                 };
    Class class = [handlerMap objectForKey:@(bankEnum)] ? : [ZPDefaultBankHandler class];
    return class;
}

- (ZPPaymentHandler *)controllerForPaymentBankCode:(NSString *)bankCode number:(NSString *)number {
    Class class = [self findPaymentHandlerWith:bankCode];
    ZPPaymentHandler *controller = [[class alloc] init];
    controller.atmCard.cardNumber = number;
    [self configHandler:controller withType:ZPPaymentMethodTypeAtm bankCode:bankCode];
    [self updateFee:controller];
    return controller;
}

- (void)updateFee:(ZPPaymentHandler *)controller {
    ZPPaymentMethod *method = [[ZPPaymentMethod alloc] init];
    method.minFee = controller.channel.minFee;
    method.feeRate = controller.channel.feeRate;
    method.feeCalType = controller.channel.feeCalType;
    [self calculateFeeWithMethod:method];
}

- (void)configHandler:(ZPPaymentHandler *)controller withType:(ZPPaymentMethodType)type bankCode:(NSString *)bankCode {
    controller.isBackFromCC = YES;
    controller.bill = self.bill;
    controller.methodType = type;
    controller.dataManager = [ZPDataManager sharedInstance];
    controller.delegate = self;
    controller.paymentMethods = self.paymentMethods;
    controller.zpParentController = self;
    controller.mTableView = self.zpw_tableMain;

    if (bankCode.length == 0) {
        controller.channel = [self.dataManager channelWithTypeAndBankCode:type bankCode:@""];
    } else {
        controller.channel = [self.dataManager channelWithTypeAndBankCode:type bankCode:bankCode];
    }
    [controller initialization];
    self.controller = [ZPMethodsHandler castFrom:controller];
}

- (void)paymentControllerNeedUpdateTitle {
    [self updatePaymentTitle];
}

- (void)paymentControllerNeedUpdateLayouts {
    [self updateVoucherView];
    ZPPaymentHandler *newController = [self currentController];
    self.proxy.handler = newController;
    [self setContenInsetWith:newController];
    [newController displayOnTableView:self.zpw_tableMain fromController:self];
}

- (void)setContenInsetWith:(ZPPaymentHandler *)newController {
    if (newController.currentStep == ZPAtmPaymentStepVietinBankOTP
            || newController.currentStep == ZPAtmPaymentStepVietinBankCapCha
            || newController.currentStep == ZPAtmPaymentStepAtmAuthenPayer) {

        RACSignal *until = [newController rac_willDeallocSignal];
        RACSignal *keyboard = [[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillShowNotification object:nil];
        @weakify(self);
        [[[keyboard takeUntil:until] take:1] subscribeNext:^(NSNotification *notification) {
            @strongify(self);
            CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
            float bottom = keyboardSize.height + 5;
            self.zpw_tableMain.contentInset = UIEdgeInsetsMake(0, 0, bottom, 0);
            float offSetY = self.zpw_tableMain.contentSize.height - (self.zpw_tableMain.frame.size.height - bottom);
            if (offSetY > 0) {
                [self.zpw_tableMain setContentOffset:CGPointMake(0, offSetY) animated:false];
            }
        }];
        return;
    }
    self.zpw_tableMain.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
}

#pragma mark handle list method when ibanking method only VCB

- (void)showSDKKeyboard {
    [[self currentController] showKeyboard];
}

- (void)dismissSDKKeyboard {
    [[self currentController] dismissKeyboard];
}

- (void)showAlertNotifyWithMessage:(NSString *)message {
    [self dismissSDKKeyboard];
    @weakify(self);
    [ZPDialogView showDialogWithType:DialogTypeNotification
                               title:[R string_Dialog_Title_Notification]
                             message:message
                        buttonTitles:@[[R string_ButtonLabel_Close]]
                             handler:^(NSInteger index) {
                                 @strongify(self);
                                 [self showSDKKeyboard];
                             }];
}


#pragma mark - helper

- (NSDictionary *)paramsWithBill:(ZPBill *)bill {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    UIDevice *device = [UIDevice currentDevice];
    NSString *deviceId = [[ZaloPayWalletSDKPayment sharedInstance].appDependence getDeviceId];
    [params setObjectCheckNil:@(bill.appId) forKey:@"appid"];
    [params setObjectCheckNil:bill.appTransID forKey:@"apptransid"];
    [params setObjectCheckNil:bill.appUser forKey:@"appuser"];
    [params setObjectCheckNil:@(bill.time) forKey:@"apptime"];
    [params setObjectCheckNil:bill.embedData forKey:@"embeddata"];
    [params setObjectCheckNil:@"ios" forKey:@"platform"];
    [params setObjectCheckNil:bill.mac forKey:@"mac"];
    [params setObjectCheckNil:deviceId forKey:@"deviceid"];
    [params setObjectCheckNil:[device zpMobileNetworkCode] forKey:@"mno"];
    [params setObjectCheckNil:SDK_BUILD_VERSION forKey:@"sdkver"];
    [params setObjectCheckNil:[device zpOsVersion] forKey:@"osver"];
    [params setObjectCheckNil:[device zpDeviceModel] forKey:@"devicemodel"];
    [params setObjectCheckNil:[ZaloPayWalletSDKPayment sharedInstance].network.connectionType forKey:@"conntype"];
    [params setObjectCheckNil:[bill.appUserInfo stringForKey:@"zaloid"] forKey:@"zaloid"];
    return params;
}

- (NSString *)atmCardInfo:(ZPAtmCard *)atmCard {
    NSMutableDictionary *bankDict = [[NSMutableDictionary alloc] init];
    [bankDict setObjectCheckNil:atmCard.bankCode forKey:@"bankcode"];
    [bankDict setObjectCheckNil:atmCard.cardHolderName forKey:@"cardholdername"];
    [bankDict setObjectCheckNil:atmCard.cardNumber forKey:@"cardnumber"];
    [bankDict setObjectCheckNil:atmCard.type forKey:@"type"];
    [bankDict setObjectCheckNil:atmCard.otpType forKey:@"otptype"];
    [bankDict setObjectCheckNil:atmCard.validFrom forKey:@"cardvalidfrom"];
    [bankDict setObjectCheckNil:atmCard.validTo forKey:@"cardvalidto"];
    [bankDict setObjectCheckNil:atmCard.cardPassword forKey:@"pwd"];
    return [bankDict JSONRepresentation];
}

#pragma mark - kyc
- (void)KYCFlowDidCancel {
    ZPPaymentHandler * controller = [self.paymentControllers lastObject];
    [self notifyAndCloseSDK:controller andKeyNotify:nil];
}
@end
