//
//  ZPKYCGenderControl.swift
//  KYCTest
//
//  Created by Dung Vu on 3/26/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import UIKit
import SnapKit
import ZaloPayCommonSwift
class ZPKYCGenderControl: UISegmentedControl {
    var sources: [String] = []
    
    private var buttons: [UIButton] = []
    override var selectedSegmentIndex: Int {
        get {
            return super.selectedSegmentIndex
        }
        
        set {
            super.selectedSegmentIndex = newValue
            setSelect(at: newValue)
        }
    }
    
    convenience init(with titles:[String]) {
        self.init(frame: CGRect.zero)
        self.tintColor = .clear
        self.sources = titles
        self.sources.enumerated().forEach({
            self.insertSegment(withTitle: $0.element, at: $0.offset, animated: false)
        })
    }
    
    
    override func insertSegment(withTitle title: String?, at segment: Int, animated: Bool) {
        super.insertSegment(withTitle: title, at: segment, animated: animated)
        // Hack
        let button = UIButton.checkIcon(with: nil, size: 24)
        button.clipsToBounds = false
        button.contentHorizontalAlignment = .left
        button.isUserInteractionEnabled = false
        let label = UILabel(frame: .zero)
        label.textColor = UIColor.defaultText()
        label.font = UIFont.sfuiTextRegular(withSize: 16)
        label.isUserInteractionEnabled = false
        button.addSubview(label)
        label.snp.makeConstraints { (make) in
            make.left.equalTo(36)
            make.centerY.equalToSuperview()
            make.size.greaterThanOrEqualTo(CGSize.zero)
        }
        label.text = title
        self.addSubview(button)
        buttons.append(button)
    }
    
    override func sendActions(for controlEvents: UIControlEvents) {
        super.sendActions(for: controlEvents)
        guard controlEvents == .valueChanged else {
            return
        }
        let newValue = self.selectedSegmentIndex
        self.setSelect(at: newValue)
    }
    
    private func setSelect(at idx: Int) {
        self.buttons.enumerated().forEach { (e) in
            e.element.isSelected = e.offset == idx
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        guard self.buttons.count > 0 else {
            return
        }
        
        // Add
        let w = self.bounds.width
        let wItem = w / CGFloat(self.buttons.count)
        self.buttons.enumerated().forEach { (e) in
            e.element.snp.makeConstraints({ (m) in
                m.width.equalTo(wItem)
                m.left.equalTo(wItem * CGFloat(e.offset))
                m.top.equalToSuperview()
                m.height.equalToSuperview()
            })
        }
    }
}

extension UIButton {
    static func checkIcon(with title: String?,
                          iconNormal normal: String = "red_checknormal",
                          iconSelect select: String = "red_checkactive",
                          isUsingWhiteColor: Bool = false,
                          size iconSize: CGFloat = 16,
                          titleSize: CGFloat = 16) -> UIButton {
        let button = UIButton(type: .custom)
        let iconNormal = UILabel.iconCode(withName: normal) ?? ""
        let iconActive = UILabel.iconCode(withName: select) ?? ""
        let textNormal = "\(iconNormal) \(title ?? "")"
        let textActive = "\(iconActive) \(title ?? "")"
        
        // Normal
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        
        let attNormal: [NSAttributedStringKey: Any] = [.paragraphStyle: paragraphStyle,
                                                       .foregroundColor: UIColor.defaultText(),
                                                       .font: UIFont.sfuiTextRegular(withSize: titleSize)]
        
        let attIconNormal: [NSAttributedStringKey: Any] = [.paragraphStyle: paragraphStyle,
                                                           .foregroundColor: isUsingWhiteColor ?.white : UIColor.line(),
                                                     .font: UIFont.zaloPay(withSize: iconSize)]
        let attIcon: [NSAttributedStringKey: Any] = [.paragraphStyle: paragraphStyle,
                                                     .foregroundColor: isUsingWhiteColor ?.white : UIColor.zaloBase(),
                                                     .font: UIFont.zaloPay(withSize: iconSize)]
        
        var attributeNormal = NSAttributedString(string: textNormal, attributes: attNormal)
        attributeNormal += NSAttributedString.init(string: iconNormal, attributes: attIconNormal)
        
        // Active
        var attributeActive = NSAttributedString(string: textActive, attributes: attNormal)
        attributeActive += NSAttributedString.init(string: iconActive, attributes: attIcon)
        
        button.setAttributedTitle(attributeNormal, for: .normal)
        button.setAttributedTitle(attributeActive, for: .selected)
        
        if isUsingWhiteColor {
           button.setBackgroundColor(UIColor.white, for: .normal)
           button.setBackgroundColor(UIColor.zaloBase(), for: .selected)
        }
        
        return button
    }
}
