//
//  ZPKYCTextFieldCustom.swift
//  KYCTest
//
//  Created by Dung Vu on 3/26/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit

protocol ZPKYCTextFieldValidateProtocol: UITextFieldDelegate {
    func ZPKYCTextFieldValidate(_ text: String?) throws -> Bool
}

enum ZPKYCTextFieldError: Error {
    case reason(message: String)
    case none
}

class ZPKYCTextFieldCustom: UIView {
    var lblNotice: UILabel?
    var textField: UITextField!
    
    // Title
    var defaultNoticeNormalColor: UIColor = #colorLiteral(red: 0.6745098039, green: 0.7019607843, blue: 0.7294117647, alpha: 1)
    var defaultNoticeHighlightColor: UIColor = .zaloBase()
    var defaultNoticeErrorColor: UIColor = .error()
    var defaultTitle: String?
    var placeHolder: String? {
        didSet {
            self.lblPlaceHolder?.text = placeHolder
        }
    }
    
    private weak var delegate: ZPKYCTextFieldValidateProtocol?
    private var lblPlaceHolder: UILabel?
    private let disposeBag = DisposeBag()
    private lazy var isSmallView: Bool = {
        return UIScreen.main.bounds.width == 320
    }()
    
    convenience init(with placeHolder: String?,
                     defaultTitle: String? = nil,
                     delegate: ZPKYCTextFieldValidateProtocol?) {
        self.init(frame: .zero)
        self.placeHolder = placeHolder
        self.defaultTitle = defaultTitle
        self.delegate = delegate
        addLayout()
        self.textField?.borderStyle = .none
        self.lblPlaceHolder?.text = placeHolder
        self.textField?.clearButtonMode = .whileEditing
        setupEvent()
    }
    
    private func addLayout() {
        // Add label notice
        let lblNotice = UILabel(frame: .zero)
        lblNotice.font = UIFont.sfuiTextRegular(withSize: 12)
        lblNotice.textColor = defaultNoticeNormalColor
        self.addSubview(lblNotice)
        lblNotice.snp.makeConstraints { (make) in
            make.top.equalTo(9)
            make.left.equalTo(12)
            make.size.greaterThanOrEqualTo(CGSize.zero)
        }
        self.lblNotice = lblNotice
        
        // Textfield
        let textField = UITextField(frame: .zero)
        textField.font = UIFont.sfuiTextRegular(withSize: isSmallView ? 14 : 16)
        textField.textColor = UIColor.defaultText()
        textField.borderStyle = .none
        self.addSubview(textField)
        
        textField.snp.makeConstraints { (make) in
            make.top.equalTo(lblNotice.snp.bottom).priority(.high)
            make.left.equalTo(lblNotice.snp.left)
            make.right.equalTo(-2)
            make.bottom.equalTo(-9).priority(.medium)
        }
        textField.delegate = self.delegate
        self.textField = textField
        
        // Placeholder
        let lblPlacholder = UILabel(frame: .zero)
        lblPlacholder.isUserInteractionEnabled = false
        lblPlacholder.font = UIFont.sfuiTextRegular(withSize: isSmallView ? 14 : 16)
        lblPlacholder.textColor = defaultNoticeNormalColor
        self.addSubview(lblPlacholder)
        lblPlacholder.snp.makeConstraints { (make) in
            make.left.equalTo(textField.snp.left)
            make.centerY.equalTo(textField.snp.centerY).priority(.medium)
            make.right.equalTo(textField.snp.right)
            make.height.greaterThanOrEqualTo(0)
        }
        self.lblPlaceHolder = lblPlacholder
        self.layoutSubviews()
    }
    
    func setupEvent() {
        self.textField?.rx.text.asDriver().drive(onNext: { [weak self](s) in
            self?.lblPlaceHolder?.isHidden = s?.count != 0
        }).disposed(by: disposeBag)
        
        self.textField?.rx.controlEvent(.editingChanged).bind { [weak self] in
            // Validate first
            let s = self?.textField.text
            do {
                let result = (try self?.delegate?.ZPKYCTextFieldValidate(s)) ?? false
                guard result else {
                    return
                }
                self?.lblNotice?.text = self?.defaultTitle
                self?.lblNotice?.textColor = self?.defaultNoticeHighlightColor
            } catch {
//                self?.handler(with: error)
                self?.lblNotice?.text = self?.defaultTitle
                self?.lblNotice?.textColor = self?.defaultNoticeHighlightColor
            }
            }.disposed(by: disposeBag)
        
        self.textField?.rx.controlEvent(.editingDidBegin).asObservable().bind(onNext: { [weak self](_) in
            // hightlight
            guard let wSelf = self else {
                return
            }
            wSelf.lblNotice?.text = wSelf.defaultTitle
            wSelf.lblNotice?.textColor = wSelf.defaultNoticeHighlightColor
        }).disposed(by: disposeBag)
        
        self.textField?.rx.controlEvent(.editingDidEnd).asObservable().map({ [weak self](_) -> Bool in
            guard let delegate = self?.delegate else {
                return true
            }
            let text = self?.textField?.text
            return try delegate.ZPKYCTextFieldValidate(text)
        }).subscribe(onNext: { [weak self](result) in
            guard let wSelf = self else {
                return
            }
            
            guard result else {
                wSelf.lblNotice?.text = ""
                return
            }
            wSelf.lblNotice?.text = wSelf.defaultTitle
            wSelf.lblNotice?.textColor = wSelf.defaultNoticeNormalColor
            }, onError: { [weak self] (e) in
                self?.handler(with: e)
        }).disposed(by: disposeBag)
        
    }
    
    func handler(with error: Error?) {
        guard let error = error as? ZPKYCTextFieldError else {
            self.lblNotice?.text = self.defaultTitle
            self.lblNotice?.textColor = self.defaultNoticeNormalColor
            return
        }
        switch error {
        case .reason(let message):
            self.lblNotice?.text = message
            self.lblNotice?.textColor = self.defaultNoticeErrorColor
        default:
            self.lblNotice?.text = ""
        }
    }
    
    func resetValue(_ event: UIControlEvents = .valueChanged) {
        setText(with: "",event: event)
    }
    
    func setText(with s: String?, event: UIControlEvents = .valueChanged) {
        self.lblNotice?.text = ""
        self.textField.text = s
        self.textField.sendActions(for: event)
    }
    
    func text() -> String? {
        return self.textField.text
    }
}

