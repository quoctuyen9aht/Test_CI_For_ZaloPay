//
//  ZPKYCChooseValueTextField.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 3/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import ZaloPayWalletSDKPrivate
import SnapKit

class ZPKYCChooseValueTextField: UITextField, ZPKYCUpdateContentProtocol {
    override var inputView: UIView? {
        get {
            return super.inputView
        }
        
        set {
           super.inputView = newValue
           // Check
           checkInput()
        }
    }
    lazy var update: PublishSubject<UpdateType> = PublishSubject()
    private lazy var disposeBag = DisposeBag()
    private var lblPlaceHolder: UILabel?
    private var needRightView: Bool = true
    private lazy var isSmallView: Bool = {
        return UIScreen.main.bounds.width == 320
    }()
    
    override var placeholder: String? {
        get {
            return self.lblPlaceHolder?.text
        }
        
        set {
            self.lblPlaceHolder?.text = newValue
        }
        
    }
    
    convenience init(with placeHolderText: String?, needRightView: Bool = true) {
        self.init(frame: .zero)
        self.needRightView = needRightView
        self.tintColor = .clear
        self.textColor = UIColor.defaultText()
        self.font = UIFont.sfuiTextRegular(withSize: isSmallView ? 14 : 16)
        self.borderStyle = .none
        self.addLayout()
        self.placeholder = placeHolderText
        self.setupEvent()
    }
    
    private func addLayout() {
        self.leftViewMode = .always
        let lView = UIView(frame: CGRect(origin: .zero, size: CGSize(width: 12, height: 60)))
        lView.autoresizingMask = .flexibleHeight
        self.leftView = lView
        
        // attach right view
        if needRightView {
            self.rightViewMode = .always
            let rView = UILabel(frame: CGRect(origin: .zero, size: CGSize(width: 25, height: 60)))
            rView.autoresizingMask = .flexibleHeight
            rView.font = UIFont.zaloPay(withSize: 14)
            rView.text = UILabel.iconCode(withName: "gift_arrowdown")
            rView.textColor = UIColor.line()
            self.rightView = rView
        }
        
        // placeholder
        let lblPlaceholder = UILabel(frame: .zero)
        lblPlaceholder.isUserInteractionEnabled = false
        lblPlaceholder.font = UIFont.sfuiTextRegular(withSize: isSmallView ? 14 : 16)
        lblPlaceholder.textColor = UIColor.placeHolder()
        self.addSubview(lblPlaceholder)
        lblPlaceholder.snp.makeConstraints { (make) in
            make.left.equalTo(12)
            make.centerY.equalToSuperview()
            make.size.greaterThanOrEqualTo(CGSize.zero)
        }
        self.lblPlaceHolder = lblPlaceholder
        
        // add line right
        if needRightView {
            let vLine = UIView.init(frame: .zero)
            vLine.backgroundColor = UIColor.line()
            self.addSubview(vLine)
            vLine.snp.makeConstraints { (make) in
                make.top.equalToSuperview()
                make.width.equalTo(0.5)
                make.right.equalToSuperview()
                make.top.equalToSuperview()
                make.bottom.equalToSuperview()
            }
        }
    }
    
    private func setupEvent() {
        self.rx.text.asObservable().bind { [unowned self](s) in
            self.lblPlaceHolder?.isHidden = s?.count != 0
        }.disposed(by: disposeBag)
    }

    private func checkInput() {
        guard let event = self.inputView as? ZPKYCUpdateContentProtocol else {
            return
        }
        
        // Update UI
        event.update.bind { [weak self](value) in
            // Only identify and birthday
            self?.text = value.description
            self?.sendActions(for: .valueChanged)
        }.disposed(by: disposeBag)
        
        // Update to result
        event.update.bind(to: self.update).disposed(by: disposeBag)
        
    }
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
}
