//
//  ZPKYCViewController.swift
//  KYCTest
//
//  Created by Dung Vu on 3/26/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import UIKit
import SnapKit
import RxCocoa
import RxSwift
import ZaloPayWalletSDKPrivate
import ZaloPayAnalyticsSwift

enum KYCResult {
    case sucess([String: Any])
    case cancel
    case fail(Error)
}

@objcMembers
public final class ZPKYCViewController: UITableViewController {
    private lazy var presenter: ZPKYCPresenter = ZPKYCPresenter(using: self, with: cardInfor, at: currentType)
    let eResult: PublishSubject<KYCResult> = PublishSubject()
    private var cardInfor: [String: Any]?
    private let disposeBag = DisposeBag()
    private var currentType: ZPKYCType = .fullInformation
    private var isCancel: Bool = false
    
    init(using cardInfor: [String: Any]?, type: Int) {
        super.init(nibName: nil, bundle: nil)
        self.cardInfor = cardInfor
        self.currentType = ZPKYCType.init(rawValue: type) ?? .fullInformation
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func loadView() {
        let tableView = UITableView(frame: .zero, style: .grouped)
        self.view = tableView
        self.tableView = tableView
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.setupLayout()
        self.prepareData()
        self.setupEvent()
    }
    
    public override func screenName() -> String! {
        return ZPTrackerScreen.Screen_iOS_OnboardingKYC_UserInfo
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        ZPTrackingHelper.shared().trackScreen(with: self)
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    private func prepareData() {
        self.presenter.loadSources()
    }
    
    private func setupEvent() {
        self.eResult.filter { (result) -> Bool in
            switch result {
            case .cancel:
                return true
            case .fail:
                return true
            default:
                return false
            }
            }.bind { [weak self](_) in
                self?.isCancel = true
            }.disposed(by:disposeBag)
    }
    
    private func setupLayout() {
        self.tableView.backgroundColor = .white
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.separatorColor = .clear
        self.tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        
        let backButton = UIButton(type: .custom)
        backButton.isMultipleTouchEnabled = false
        backButton.tintColor = .white
        let isAllowBack: Bool = true
        switch currentType {
        case .onboarding:
            fallthrough
        case .transaction:
            backButton.setTitle(UILabel.iconCode(withName: "general_backios"), for: .normal)
            backButton.titleLabel?.font = UIFont.iconFont(withSize: 20)
        case .mapCard:
            fallthrough
        case .profile:
            backButton.setTitle(UILabel.iconCode(withName: "general_backios"), for: .normal)
            backButton.titleLabel?.font = UIFont.iconFont(withSize: 20)
        case .fullInformation:
            backButton.setTitle(R.string_ButtonLabel_Cancel(), for: .normal)
            backButton.titleLabel?.font = UIFont.sfuiTextRegular(withSize: 15)
        }
        
        if #available(iOS 11, *) {
            backButton.contentEdgeInsets = UIEdgeInsetsMake(0, -12, 0, 0)
        }
        backButton.frame = CGRect(origin: .zero, size: CGSize(width: 44, height: 44))
        if (currentType == .onboarding) {
            backButton.contentHorizontalAlignment = .left
        }
        
        if isAllowBack {
            backButton.rx.tap.bind { [weak self](_) in
                // Show alert
                self?.showAlert()
                }.disposed(by: disposeBag)
        }
        
        let barButton = UIBarButtonItem(customView: backButton)
        let negativeSeparator = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        negativeSeparator.width = -12
        self.navigationItem.leftBarButtonItems = [negativeSeparator, barButton]
    }
    
    private func showAlert() {
        let message = String(format: stringWarningCancelPayment, "nhập thông tin cá nhân")
        ZPDialogView.showDialog(with: DialogTypeNotification,
                                message: message,
                                cancelButtonTitle: stringAlertTitleAccept,
                                otherButtonTitle: [stringAlertTitleCancel])
        { [weak self](idx, idxCancel) in
            guard idx == idxCancel else {
                return
            }
            self?.eResult.onNext(KYCResult.cancel)
        }
    }
    
    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    deinit {
        guard self.isCancel else {
            self.eResult.onCompleted()
            return
        }
        self.eResult.dispose()
    }
    
}

// MARK: - Public Function
extension ZPKYCViewController {
    static func runKYCFlow(on controller: UIViewController?, title: String?, using infor:[String: Any]?, at mode: Int) -> Observable<KYCResult> {
        guard let controller = controller else {
            return Observable.empty()
        }
        return Observable.create({ (s) -> Disposable in
            let kycVC = ZPKYCViewController.init(using: infor, type: mode)
            kycVC.title = title
            _ = kycVC.eResult.subscribe(s)
            controller.navigationController?.pushViewController(kycVC, animated: true)
            return Disposables.create()
        })
    }
    
    ///ObjC func
    public static func runKYCFlowObjC(on controller: UIViewController?,
                                      title: String?,
                                      using infor:[String: Any]?,
                                      at mode: Int,
                                      handlerValue: (([String: Any]) -> ())?,
                                      cancel: (() -> ())?,
                                      error:((Error) -> ())?,
                                      complete:(() -> ())?)
    {
        
        _ = runKYCFlow(on: controller, title: title, using: infor, at: mode).subscribe(onNext: { (result) in
            switch result {
            case .sucess(let infor):
                handlerValue?(infor)
            case .cancel:
                cancel?()
            case .fail(let e):
                error?(e)
            }
        }, onCompleted: {
            complete?()
        })
    }
    
}


// MARK: - TableView DataSource
extension ZPKYCViewController {
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.presenter.cells[indexPath.section][indexPath.item]
        return cell
    }
    
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.presenter.cells[section].count
    }
    
    override public func numberOfSections(in tableView: UITableView) -> Int {
        return self.presenter.cells.count
    }
}

// MARK: - TableView Delegate
extension ZPKYCViewController {
    override public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return section == max(self.presenter.cells.count - 1, 0) ? self.presenter.headerView : nil
    }
    
    override public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == max(self.presenter.cells.count - 1, 0) ? self.presenter.heightForHeader() : 0.1
    }
    
    override public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    override public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
}
