//
//  ZPKYCPresenter.swift
//  KYCTest
//
//  Created by Dung Vu on 3/26/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import SnapKit
import ZaloPayWalletSDKPrivate

enum CellType: Int {
    case cardInfor
    case name
    case identify
    case phone
    case dayOfBirth
    case gender
    case next
    case total
}


final class ZPKYCPresenter {
    lazy var interactor: ZPKYCInteractor = ZPKYCInteractor.init(isMapBank: self.currentType == .mapCard)
    private lazy var router: ZPKYCRouter = ZPKYCRouter(using: self)
    var cells: [[ZPKYCBaseTableViewCell]] = []
    private var buttonNext: UIButton?
    weak var rootVC: ZPKYCViewController?
    weak var cardInfor: ZPKYCCardDetailCell?
    private var infor: [String: Any]?
    private var currentType: ZPKYCType
    lazy var headerView: UIView = {
        return self.createHeaderView()
    }()
    private lazy var tracking: ActivityIndicator = ActivityIndicator()
    private var currentHeightHeader: CGFloat = 0
    private let disposeBag = DisposeBag()
    init(using rootVC: ZPKYCViewController?, with infor:[String: Any]?, at mode: ZPKYCType) {
        // TODO: Need set for interactor
        self.rootVC = rootVC
        self.infor = infor
        self.currentType = mode
        self.generateCells()
        self.loadSources()
    }
    
    private func createHeaderView() -> UIView {
        
        let view = UIView(frame: .zero)
        view.backgroundColor = UIColor(hexString: "#f0f4f6")
        
        let title: String
        let message: String
        switch currentType {
        case .fullInformation:
            fallthrough
        case .mapCard:
            title = ""
            message = R.string_Onboard_KYC_Message_Mapcard()
        case .onboarding:
            title = ""
            message = R.string_Onboard_KYC_Message_Onboarding()
        case .transaction:
            title = ""
            message = R.string_Onboard_KYC_Message_Transaction()
        case .profile:
            title = ""
            message = R.string_Onboard_KYC_Message_Profile()
        }
        
        // add label
        let isHaveTitle = title.count > 0
        let delta: CGFloat = isHaveTitle ? 12 : 0
//        let top: CGFloat = isHaveTitle || currentType == .onboarding ? 30 : 12
//        let bottom: CGFloat = isHaveTitle || currentType == .onboarding ? -17 : -12
        let top: CGFloat = 30
        let bottom: CGFloat = -17
        let padding: CGFloat = 12
        
        let lblNotice = UILabel(frame: .zero)
        lblNotice.numberOfLines = 0
        lblNotice.font = UIFont.sfuiTextRegular(withSize: 13)
        lblNotice.textColor = UIColor.subText()
        view.addSubview(lblNotice)
        lblNotice.snp.makeConstraints { (make) in
            make.left.equalTo(padding)
            make.right.equalTo(-padding)
            make.top.equalTo(top)
            make.height.greaterThanOrEqualTo(0)
        }
        
        let lblDescription =  UILabel(frame: .zero)
        lblDescription.numberOfLines = 0
        lblDescription.font = UIFont.sfuiTextRegular(withSize: 14)
        
        lblDescription.textColor = UIColor.subText()
        
        view.addSubview(lblDescription)
        lblDescription.snp.makeConstraints { (make) in
            make.top.equalTo(lblNotice.snp.bottom).offset(delta)
            make.left.equalTo(lblNotice.snp.left)
            make.right.equalTo(lblNotice.snp.right)
            make.height.greaterThanOrEqualTo(20)
            make.bottom.equalTo(bottom).priority(.high)
        }
        
        lblNotice.text = title
        lblDescription.text = message
        
        let btn = UIButton(type: .custom)
        btn.tintColor = .clear
        view.addSubview(btn)
        btn.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        btn.rx.tap.bind { [weak self](_) in
            self?.rootVC?.view.endEditing(true)
        }.disposed(by: disposeBag)
        
        return view
    }
    
    func heightForHeader() -> CGFloat {
        guard currentHeightHeader == 0 else {
            return currentHeightHeader
        }
        self.headerView.layoutSubviews()
        let constraintSize = CGSize(width: UIScreen.main.bounds.width, height: CGFloat.greatestFiniteMagnitude)
        let s = self.headerView.systemLayoutSizeFitting(constraintSize, withHorizontalFittingPriority: .required, verticalFittingPriority: .fittingSizeLevel)
        // Cache
        self.currentHeightHeader = s.height
        return s.height
    }
    
    
    func loadSources() {
        guard self.currentType != .onboarding else {
            return
        }
        self.interactor.requestInforKYC().trackActivity(self.tracking).observeOn(MainScheduler.instance).subscribe(onNext: { [weak self](_) in
            self?.reloadUI()
        }, onError: { [weak self](e) in
            // show error
            self?.handler(with: e)
        }).disposed(by: disposeBag)
    }
    
    private func reloadUI() {
        let store = cells.flatMap({ $0 })
        store.forEach({
            $0.configure(with: self.interactor.entity)
        })
        self.rootVC?.tableView.reloadData()
    }
    
    private func handler(with error: Error) {
        // TODO: Check error
        ZPDialogView.showDialogWithError(error, handle: nil)
    }
    
    private func generateCells() {
        if let infor = self.infor {
            let cardInfor = ZPKYCCardDetailCell.init(style: .default, reuseIdentifier: "ZPKYCCardDetailCell")
            let icon = infor["image"] as? UIImage
            let bankName = infor["bankName"] as? String ?? ""
            let bankNumber = infor["bankNumber"] as? String ?? ""
            let lastDigit = String(bankNumber.suffix(4))
            let content = String(format: stringMethodNameFormat, bankName, lastDigit)
            cardInfor.updateInformation(icon: icon, content: content)
            cells.append([cardInfor])
        }
        
        let userInfor : [ZPKYCBaseTableViewCell]
        
        let lastCell = ZPKYCTermCell.init(style: .default, reuseIdentifier: "ZPKYCTermCell")
        lastCell.eOpenLink.bind { [weak self](_) in
            self?.router.openSupport()
        }.disposed(by: disposeBag)
        
        self.buttonNext = lastCell.btnNext
        let nameCell = ZPKYCNameCell.init(style: .default, reuseIdentifier: "ZPKYCNameCell")
        let idetifyCell = ZPKYCIdentifyCell.init(style: .default, reuseIdentifier: "ZPKYCIdentifyCell")
        let phoneCell = ZPKYCPhoneCell.init(style: .default, reuseIdentifier: "ZPKYCPhoneCell")
        let birthdayCell = ZPKYCBirthdayCell.init(style: .default, reuseIdentifier: "ZPKYCBirthdayCell")
        let genderCell = ZPKYCGenderCell.init(style: .default, reuseIdentifier: "ZPKYCGenderCell")
        let noticedCell = ZPKYCNoticedCell.init(style: .default, reuseIdentifier: "ZPKYCErrorCell")
        
        switch currentType {
        case .onboarding:
            phoneCell.setLayoutOnboarding()
            cells.append([noticedCell, phoneCell])
            userInfor = [nameCell, idetifyCell, birthdayCell, genderCell, lastCell]
        case .fullInformation:
            userInfor = [nameCell, idetifyCell, phoneCell, birthdayCell, genderCell, lastCell]
        case .transaction:
            fallthrough
        case .mapCard:
            userInfor = [nameCell, idetifyCell, birthdayCell, genderCell, lastCell]
        case .profile:
            userInfor = [nameCell, idetifyCell, birthdayCell, genderCell, lastCell]
        }
        
        cells.append(userInfor)
        self.updateUI()
        self.setupEvent()
    }
    
    /// Update data to UI
    private func updateUI() {
        self.reloadUI()
        let store = cells.flatMap({ $0 })
        // Track focus
        store.compactMap({ $0 as? ZPKYCHandlerFocusProtocol }).forEach {
            $0.delegate = self
        }
        
        Observable.merge(store.map({ $0.update })).bind { [weak self] in
            self?.interactor.updateInfor(with: $0)
        }.disposed(by: disposeBag)
        
        Observable.combineLatest(store.map({ $0.validate })).bind { [weak self](trackValues) in
            let result = trackValues.reduce(true, { $0 && $1})
            self?.buttonNext?.isEnabled = result
        }.disposed(by: disposeBag)
        
    }
    
    /// Track event
    private func setupEvent() {
        // Add call api
        self.buttonNext?.rx.tap.bind { [weak self] in
            guard let wSelf = self else {
                return
            }
            ZPTrackingHelper.shared().trackEvent(.onboardingkyc_submit_user_info)
            wSelf.router.moveToResult()
        }.disposed(by: disposeBag)
        
        self.tracking.asDriver().drive(onNext: {
            $0 ? ZPProgressHUD.show(withStatus: nil) : ZPProgressHUD.dismiss()
        }).disposed(by: disposeBag)
    }
}

// MARK: - Input Source
extension ZPKYCPresenter: ZPKYCFocusProtocol {
    func scrollViewWillFocus(at cell: UITableViewCell?) {
        guard let cell = cell,
            let idx = self.rootVC?.tableView.indexPath(for: cell) else {
            return
        }
        self.rootVC?.tableView.scrollToRow(at: idx, at: .top, animated: true)
    }
    
    
}
