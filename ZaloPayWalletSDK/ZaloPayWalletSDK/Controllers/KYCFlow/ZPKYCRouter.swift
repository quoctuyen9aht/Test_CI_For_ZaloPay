//
//  ZPKYCRouter.swift
//  KYCTest
//
//  Created by Dung Vu on 3/26/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import Foundation
import ZaloPayWalletSDKPrivate
import ZaloPayAnalyticsSwift

struct ZPKYCRouter {
    /// move to item get result
    weak var presenter: ZPKYCPresenter?
    init(using presenter: ZPKYCPresenter?) {
        self.presenter = presenter
    }
    
    func moveToResult() {
        let userInfor = self.presenter?.interactor.exportJson() ?? [:]
        let rootVC = self.presenter?.rootVC
        rootVC?.eResult.onNext(KYCResult.sucess(userInfor))
    }
    
    func openSupport() {
        ZPTrackingHelper.shared().trackEvent(.onboardingkyc_view_term_and_condition)
        guard let controller = self.presenter?.rootVC else {
            return
        }
        ZaloPayWalletSDKPayment.sharedInstance().appDependence?.showTermView(from: controller)
    }
}
