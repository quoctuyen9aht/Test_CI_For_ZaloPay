//
//  ZPKYCInteractor.swift
//  KYCTest
//
//  Created by Dung Vu on 3/26/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ZaloPayWalletSDKPrivate

final class ZPKYCInteractor {
    let entity: ZPKYCEntity
    init(isMapBank flow:Bool) {
        self.entity = ZPKYCEntity(mapBank: flow)
    }
    
    /// Using request latest KYC infor
    func requestInforKYC() -> Observable<Void>{
        let signal = ZaloPayWalletSDKPayment.sharedInstance().getUserProfile()
        return (~signal).do(onNext: { [weak self](result) in
            // Update
            guard let json = result as? [String: Any] else {
                return
            }
            self?.entity.update(with: json["kycInfo"] as? [String: Any])
        }).map { (_) in }
    }
    
    /// Export json to post
    func exportJson() -> [String: Any] {
        let result = entity.json().toJsonString() ?? ""
        return ["kyc": result]
    }
    
    /// Update information for entity
    func updateInfor(with type: UpdateType) {
        switch type {
        case .name(let content):
            self.entity.name = content
        case .identify(let type,let content):
            self.entity.typeOfIdentify = type
            self.entity.numberIdentify = content
        case .birthday(let date):
            self.entity.dateOfBirth = date
        case .gender(let type):
            self.entity.gender = type
        case .phone(let number):
            self.entity.phone = number
        default:
            break
        }
    }
}
