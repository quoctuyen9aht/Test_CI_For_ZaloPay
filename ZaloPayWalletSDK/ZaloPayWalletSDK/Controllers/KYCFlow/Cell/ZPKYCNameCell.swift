//
//  ZPKYCNameCell.swift
//  KYCTest
//
//  Created by Dung Vu on 3/26/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import UIKit
import SnapKit
import RxCocoa
import RxSwift
import ZaloPayWalletSDKPrivate

private let maxCharacters = 50
class ZPKYCNameCell: ZPKYCBaseTableViewCell, ZPKYCHandlerFocusProtocol {
    weak var delegate: ZPKYCFocusProtocol?
    private var vTextField: ZPKYCTextFieldCustom?
    override func addLayout() {
        let vTextField = ZPKYCTextFieldCustom.init(with: R.string_Onboard_KYC_Hint_Name(), defaultTitle: R.string_Onboard_KYC_Name(), delegate: self)
        self.contentView.addSubview(vTextField)
        vTextField.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalToSuperview()
            make.height.equalTo(60)
            make.bottom.equalToSuperview()
        }
        vTextField.textField.autocapitalizationType = .allCharacters
        self.vTextField = vTextField
    }
    
    override func setupEvent() {
        self.topLine.isHidden = false
        self.input = self.vTextField?.textField
        
        // Update value to entity
        self.validate.filter({ $0 }).bind { [weak self] (_) in
            let name = self?.vTextField?.text()
            self?.update.onNext(UpdateType.name(content: name))
        }.disposed(by: disposeBag)
    }
    
    override func configure(with entity: ZPKYCEntity) {
        guard let name = entity.name,
            !name.isEmpty else {
            return
        }
        self.vTextField?.setText(with: name, event: .editingDidEnd)
    }
    
    override func commonSetup() {
        super.commonSetup()
        self.updatePaddingBottomLine(left: 12)
    }
}

extension ZPKYCNameCell: ZPKYCTextFieldValidateProtocol {
    func ZPKYCTextFieldValidate(_ text: String?) throws -> Bool {
        var validate: Bool = true
        defer {
            self.validate.onNext(validate)
        }
        
        guard let text = text , !text.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty else {
            validate = false
            throw ZPKYCTextFieldError.reason(message: R.string_Onboard_KYC_Empty_Name())
        }
        
        let space = CharacterSet.whitespaces
        let newCharacter = characterSet.union(space)
        if text.rangeOfCharacter(from: newCharacter.inverted) != nil || text.containsEmoji {
            validate = false
            throw ZPKYCTextFieldError.reason(message: R.string_Onboard_KYC_Invalid_Name())
        }
        
        return validate
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.bottomLine.backgroundColor = UIColor.zaloBase()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.bottomLine.backgroundColor = UIColor.line()
        var isValidated: Bool
        do {
            isValidated = try self.ZPKYCTextFieldValidate(textField.text)
            self.vTextField?.handler(with: nil)
        } catch {
            self.vTextField?.handler(with: error)
            isValidated = false
        }
        
        if !isValidated {
            ZPTrackingHelper.shared().trackEvent(.onboardingkyc_input_real_name_invalid)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let t = textField.text else {
            return true
        }
        
        let nStr = (t as NSString).replacingCharacters(in: range, with: string)
        let next = nStr.count <= maxCharacters
        return next
    }
    
}
