//
//  ZPKYCBaseTableViewCell.swift
//  KYCTest
//
//  Created by Dung Vu on 3/26/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit

enum UpdateType {
    case none
    case name(content: String?)
    case identify(type: IdentifyType?, content: String?)
    case phone(number: String?)
    case birthday(date: Date?)
    case gender(type: Gender?)
    
    var description: String? {
        switch self {
        case .identify(let type, _):
            return type?.description
        case .birthday(let date):
            return date?.string(using: "dd/MM/yyyy")
        default:
            return nil
        }
    }
}

protocol ZPKYCValidateProtocol {
    var validate: BehaviorSubject<Bool> { get }
}

protocol ZPKYCUpdateContentProtocol {
    var update: PublishSubject<UpdateType> { get }
}

protocol ZPKYCFocusProtocol: class {
    // Using for scroll
    func scrollViewWillFocus(at cell: UITableViewCell?)
}

protocol ZPKYCHandlerFocusProtocol: class {
    var delegate: ZPKYCFocusProtocol? { get set }
}

internal class ZPKYCBaseTableViewCell: UITableViewCell, ZPKYCValidateProtocol, ZPKYCUpdateContentProtocol {
    lazy var validate: BehaviorSubject<Bool> = BehaviorSubject(value: false)
    lazy var update: PublishSubject<UpdateType> = PublishSubject()
    lazy var characterSet = CharacterSet.alphanumerics
    lazy var numberSet = CharacterSet.decimalDigits
    lazy var isSmallView: Bool = {
        return UIScreen.main.bounds.width == 320
    }()
    
    lazy var topLine: UIView = {
        let v = self.createLine(isTop: true)
        return v
    }()
    
    lazy var bottomLine: UIView = {
        let v = self.createLine(isTop: false)
        return v
    }()
    
    let disposeBag = DisposeBag()
    weak var input: UIResponder? {
        didSet {
            // set event
            self.trackInput()
        }
    }
    
    private func createLine(isTop top: Bool) -> UIView {
        let v = UIView(frame: .zero)
        v.backgroundColor = UIColor.line()
        self.contentView.addSubview(v)
        v.snp.makeConstraints { (make) in
            if top {
                make.top.equalToSuperview()
            } else {
                make.bottom.equalToSuperview().offset(0.5)
            }
            make.left.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalTo(0.5)
        }
        
        return v
    }
    
    func trackInput() {
        input?.rx.methodInvoked(#selector(becomeFirstResponder)).bind(onNext: { [weak self](_) in
            self?.handlerFocus()
        }).disposed(by: disposeBag)
    }
    
    func handlerFocus() {
        guard let handler = self as? ZPKYCHandlerFocusProtocol else {
            return
        }
        handler.delegate?.scrollViewWillFocus(at: self)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let v = super.hitTest(point, with: event)
        if let view = v , !(view is UIControl) {
            UIApplication.shared.windows.forEach({ $0.endEditing(true) })
        }
        return v
    }
    
    // Add first layout
    func commonSetup() {
        self.selectionStyle = .none
        addLayout()
        self.topLine.isHidden = true
        self.bottomLine.isHidden = false
        setupEvent()
    }
    
    func updatePaddingBottomLine(left: Int) {
        bottomLine.snp.updateConstraints {
            $0.left.equalTo(left)
        }
    }
    
    func addLayout() {
        fatalError("Implement this!!!!")
    }
    
    func setupEvent() {}
    
    /// Using for config cell
    func configure(with entity: ZPKYCEntity) {}
}
