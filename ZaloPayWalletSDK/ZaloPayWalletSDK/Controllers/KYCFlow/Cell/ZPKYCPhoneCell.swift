//
//  ZPKYCPhoneCell.swift
//  KYCTest
//
//  Created by Dung Vu on 3/26/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit
import ZaloPayWalletSDKPrivate

fileprivate let maxCharacters = 15
final class ZPKYCPhoneCell: ZPKYCBaseTableViewCell, ZPKYCHandlerFocusProtocol {
    weak var delegate: ZPKYCFocusProtocol?
    private var vTextField: ZPKYCTextFieldCustom?
    private lazy var patterns: [String] = {
        return loadPattern()
    }()
    
    private func loadPattern() -> [String] {
        let phones = ZaloPayWalletSDKPayment.sharedInstance().appDependence?.phonePattern()
        return phones ?? []
    }
    
    override func addLayout() {
        let vTextField = ZPKYCTextFieldCustom.init(with: R.string_Onboard_KYC_Hint_Phone(), defaultTitle: R.string_Onboard_KYC_Phone(), delegate: self)
        self.contentView.addSubview(vTextField)
        vTextField.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalToSuperview()
            make.height.equalTo(60)
            make.bottom.equalToSuperview().priority(.high)
        }
        vTextField.textField.keyboardType = UIKeyboardType.numberPad
        self.vTextField = vTextField
    }
    
    override func setupEvent() {
        self.input = self.vTextField?.textField
        
        // Update value to entity
        self.validate.filter({ $0 }).bind { [weak self] (_) in
            let phone = self?.vTextField?.text()
            self?.update.onNext(UpdateType.phone(number: phone))
        }.disposed(by: disposeBag)
    }
    
    override func configure(with entity: ZPKYCEntity) {
        guard let phone = entity.phone else {
            return
        }
        self.vTextField?.setText(with: phone, event: .editingDidEnd)
    }
    
    func setLayoutOnboarding() {
//        self.vTextField?.textField.clearButtonMode = .never
//        let btnRight = UIButton(type: .custom)
//        btnRight.setTitleColor(UIColor.zaloBase(), for: .normal)
//        btnRight.titleLabel?.font = UIFont.zaloPay(withSize: 24)
//        let code = UILabel.iconCode(withName: "dialoge_infor")
//        btnRight.setTitle(code, for: .normal)
//        self.contentView.addSubview(btnRight)
//        btnRight.snp.makeConstraints { (make) in
//            make.top.equalToSuperview()
//            make.bottom.equalToSuperview()
//            make.right.equalToSuperview()
//            make.width.equalTo(48)
//        }
//
//        btnRight.rx.controlEvent(.touchUpInside).bind {
//            ZPDialogView.showDialog(with: DialogTypeInfo, title: nil, message: "Số điện thoại phải trùng với số Zalo", buttonTitles:[R.string_ButtonLabel_Close()], handler: nil)
//
//        }.disposed(by: disposeBag)
        
        let lLabel = UILabel(frame: .zero)
        lLabel.font = UIFont.zaloPay(withSize: 36)
        lLabel.textAlignment = .center
        let icon = UILabel.iconCode(withName: "onboarding_phone") ?? UILabel.iconCode(withName: "onboarding_sđt")
        lLabel.text = icon
        lLabel.textColor = UIColor.zaloBase()
        self.contentView.addSubview(lLabel)
        
        lLabel.snp.makeConstraints { (make) in
            make.left.equalTo(12)
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.width.equalTo(36)
        }
        
        self.vTextField?.snp.updateConstraints({ (make) in
            make.left.equalTo(48)
            make.right.equalToSuperview()
        })
        self.vTextField?.textField.font = UIFont.sfuiTextSemibold(withSize: 18)
        self.contentView.layoutSubviews()
    }
}

extension ZPKYCPhoneCell: ZPKYCTextFieldValidateProtocol {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.bottomLine.backgroundColor = UIColor.zaloBase()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.bottomLine.backgroundColor = UIColor.line()
        var isValidate : Bool
        do {
            isValidate = try self.ZPKYCTextFieldValidate(textField.text)
            self.vTextField?.handler(with: nil)
        } catch {
            self.vTextField?.handler(with: error)
            isValidate = false
        }
        
        if !isValidate {
            ZPTrackingHelper.shared().trackEvent(.onboardingkyc_input_phone_number_invalid)
        }
    }
    
    func ZPKYCTextFieldValidate(_ text: String?) throws -> Bool {
        var validate: Bool = true
        defer {
            self.validate.onNext(validate)
        }
        guard let text = text , !text.isEmpty else {
            validate = false
            throw ZPKYCTextFieldError.reason(message: R.string_Onboard_KYC_Empty_Phone())
        }
        
        if text.count < 10 {
            validate = false
            throw ZPKYCTextFieldError.reason(message: R.string_Onboard_KYC_Invalid_Phone())
        }
        
        if text.rangeOfCharacter(from: numberSet.inverted) != nil || text.containsEmoji {
            validate = false
            throw ZPKYCTextFieldError.reason(message: R.string_Onboard_KYC_Invalid_Phone())
        }
        
        func check() -> Bool{
            guard self.patterns.count > 0 else {
                return true
            }
            
            for regex in self.patterns {
                let p = NSPredicate(format: "SELF MATCHES %@", regex)
                if p.evaluate(with: text) {
                    return true
                }
                continue
            }
            
            return false
        }
        
        if !check() {
            validate = false
            throw ZPKYCTextFieldError.reason(message: R.string_Onboard_KYC_Invalid_Phone())
        }

        return validate
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let t = textField.text else {
            return true
        }
        
        let nStr = (t as NSString).replacingCharacters(in: range, with: string)
        let next = nStr.count <= maxCharacters
        return next
    }
}
