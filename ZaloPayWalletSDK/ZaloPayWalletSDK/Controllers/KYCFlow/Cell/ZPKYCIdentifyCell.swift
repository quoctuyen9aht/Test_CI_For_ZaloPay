//
//  ZPKYCIdentifyCell.swift
//  KYCTest
//
//  Created by Dung Vu on 3/26/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit
import ZaloPayWalletSDKPrivate
import ZaloPayAnalyticsSwift

final class ZPKYCIdentifyCell: ZPKYCBaseTableViewCell, ZPKYCHandlerFocusProtocol {
    weak var delegate: ZPKYCFocusProtocol?
    private var textFieldChooseType: ZPKYCChooseValueTextField?
    private var inputPicker: ZPKYCPickerChooseTypeView?
    private var vInputValue: ZPKYCTextFieldCustom?
    private var entry: ZPKYCEntity?
    
    private var currentType: IdentifyType = .cmnd {
        didSet {
            self.vInputValue?.textField.keyboardType = currentType.keyboardType
        }
    }
    
    override func addLayout() {
        // Add select view
        let vSelect = ZPKYCChooseValueTextField.init(with: nil)
        self.contentView.addSubview(vSelect)
        vSelect.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.width.equalTo(121)
            make.height.equalTo(60)
            make.bottom.equalToSuperview().priority(.high)
        }
        
        self.textFieldChooseType = vSelect
        // Input view
        let vInputValue = ZPKYCTextFieldCustom(with: R.string_Onboard_KYC_Hint_Identify(), defaultTitle: R.string_Onboard_KYC_Identify(), delegate: self)
        self.contentView.addSubview(vInputValue)
        vInputValue.snp.makeConstraints { (make) in
            make.left.equalTo(vSelect.snp.right)
            make.top.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        self.vInputValue = vInputValue
        
        let inputPicker = ZPKYCPickerChooseTypeView(frame: CGRect(origin: .zero, size: CGSize(width: UIScreen.main.bounds.width, height: 205)))
        self.inputPicker = inputPicker
        
        // set input for type
        self.textFieldChooseType?.inputView = inputPicker
        
    }
    
    override func commonSetup() {
        super.commonSetup()
        self.updatePaddingBottomLine(left: 12)
    }
    
    override func configure(with entity: ZPKYCEntity) {
        // Get from entity
        self.entry = entity
        self.currentType = entity.typeOfIdentify ?? .cmnd
        self.textFieldChooseType?.text = self.currentType.description
        self.inputPicker?.setSelect(at: self.currentType.rawValue)
        guard let numberIdentify = entity.numberIdentify else {
            return
        }
        self.configInput(type: self.currentType, value: numberIdentify)
    }
    
    private func configInput(type: IdentifyType, value: String?) {
        self.vInputValue?.defaultTitle = String(format: R.string_Onboard_KYC_Number_Identify(), type.description)
        self.vInputValue?.placeHolder = String(format: R.string_Onboard_KYC_Number_Identify(), type.description)
        self.vInputValue?.resetValue(.editingChanged)
        self.vInputValue?.setText(with: value ?? "",event: .editingChanged)
    }
    
    override func setupEvent() {
        self.input = self.vInputValue?.textField
        // Check all case , change type same type
        self.inputPicker?.update.map({ result -> IdentifyType? in
            guard case let .identify(type, _) = result else {
                return nil
            }
            return type
        }).filter({ [weak self] in $0 != self?.currentType }).bind(onNext: { [weak self](nType) in
            guard let nType = nType else {
                return
            }
            // Update
            self?.currentType = nType
            let identifyValue = self?.entry?.cache[nType]
            self?.entry?.typeOfIdentify = nType
            self?.entry?.numberIdentify = identifyValue ?? ""
            self?.configInput(type: nType, value: identifyValue)
        }).disposed(by: disposeBag)
        
        // Update value to entity
        self.validate.filter({ $0 }).bind { [weak self] (_) in
            let content = self?.vInputValue?.text()
            let type = self?.currentType
            self?.update.onNext(UpdateType.identify(type: type, content: content))
        }.disposed(by: disposeBag)
    }
    
    override func trackInput() {
        super.trackInput()
        // Add track picker
        self.inputPicker?.rx.methodInvoked(#selector(becomeFirstResponder)).bind(onNext: { [weak self](_) in
            guard let wSelf = self else {
                return
            }
            wSelf.inputPicker?.setSelect(at: wSelf.currentType.rawValue)
            wSelf.handlerFocus()
        }).disposed(by: disposeBag)
    }

}

// MARK: - Validate
extension ZPKYCIdentifyCell: ZPKYCTextFieldValidateProtocol {
    func ZPKYCTextFieldValidate(_ text: String?) throws -> Bool {
        var validate: Bool
        defer {
            self.validate.onNext(validate)
        }
        guard let text = text , !text.isEmpty else {
            validate = false
            throw ZPKYCTextFieldError.reason(message: String(format: R.string_Onboard_KYC_Empty_Identify(), self.currentType.description))
        }
        validate = !text.containsEmoji
        if !validate {
            throw ZPKYCTextFieldError.reason(message: String(format: R.string_Onboard_KYC_Invalid_Identify(), self.currentType.description))
        }
        
        let numberCharacters = text.count
        switch self.currentType {
        case .cmnd:
            if text.rangeOfCharacter(from: numberSet.inverted) != nil {
                validate = false
                throw ZPKYCTextFieldError.reason(message: String(format: R.string_Onboard_KYC_Invalid_Identify(), self.currentType.description))
            }
            
            validate = (numberCharacters == 9 || numberCharacters == 12)
            if !validate {
                throw ZPKYCTextFieldError.reason(message: R.string_Onboard_KYC_Rule_Identify())
            }
        case .passport:
            if text.rangeOfCharacter(from: characterSet.inverted) != nil {
                validate = false
                throw ZPKYCTextFieldError.reason(message: String(format: R.string_Onboard_KYC_Invalid_Identify(), self.currentType.description))
            }
            
            let partern = "^[a-z,A-Z]\\d{7}$"
            guard let regular = try? NSRegularExpression.init(pattern: partern, options: .caseInsensitive) else {
                #if DEBUG
                    fatalError("Check regular")
                #endif
                break
            }
            let range = NSMakeRange(0, numberCharacters)
            if regular.firstMatch(in: text, options: [], range: range) == nil {
                validate = false
                throw ZPKYCTextFieldError.reason(message: String(format: R.string_Onboard_KYC_Invalid_Identify(), self.currentType.description))
            }
        case .identity:
            if (text.rangeOfCharacter(from: numberSet.inverted) != nil) || (numberCharacters < self.currentType.maxCharacter) {
                validate = false
                throw ZPKYCTextFieldError.reason(message: String(format: R.string_Onboard_KYC_Invalid_Identify(), self.currentType.description))
            }
        }
        
        return validate
    }
    
    @discardableResult
    private func validateContent(_ content: String?) -> Bool {
        do {
            let result = try self.ZPKYCTextFieldValidate(content)
            self.vInputValue?.handler(with: nil)
            return result
        } catch {
            self.vInputValue?.handler(with: error)
        }
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.bottomLine.backgroundColor = UIColor.zaloBase()
        self.vInputValue?.defaultNoticeErrorColor = UIColor.zaloBase()
//        self.validateContent(textField.text)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.bottomLine.backgroundColor = UIColor.line()
        self.vInputValue?.defaultNoticeErrorColor = UIColor.error()
        let isValidate = self.validateContent(textField.text)
        
        if !isValidate {
            var trackEventID: ZPAnalyticEventAction
            switch self.currentType {
            case .identity:
                trackEventID = ZPAnalyticEventAction.onboardingkyc_input_citizen_id_invalid
            case .passport:
                trackEventID = ZPAnalyticEventAction.onboardingkyc_input_passport_invalid
            default:
                trackEventID = ZPAnalyticEventAction.onboardingkyc_input_personal_id_invalid
            }
            ZPTrackingHelper.shared().trackEvent(trackEventID)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let t = textField.text else {
            return true
        }
        
        let nStr = (t as NSString).replacingCharacters(in: range, with: string)
        let next = nStr.count <= self.currentType.maxCharacter
        return next
    }
}

// MARK: - input view
final class ZPKYCPickerChooseTypeView: UIView, ZPKYCUpdateContentProtocol {
    lazy var update: PublishSubject<UpdateType> = PublishSubject()
    private lazy var source: [IdentifyType]  = [.cmnd, .passport, .identity]
    private var headerView: ZPKYCHeaderChooseView?
    private var pickerView: UIPickerView?
    private var currentSelect: Int = 0
    private let disposeBag = DisposeBag()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addLayout()
        setupEvent()
        self.backgroundColor = .white
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addLayout() {
        // add view cancel
        let headerView = ZPKYCHeaderChooseView(withTitle: nil)
        self.addSubview(headerView)
        headerView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(44)
        }
        self.headerView = headerView
        // add picker
        let picker = UIPickerView(frame: .zero)
        picker.dataSource = self
        picker.delegate = self
        self.addSubview(picker)
        picker.snp.makeConstraints { (make) in
            make.top.equalTo(headerView.snp.bottom)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        self.pickerView = picker
    }
    
    private func setupEvent() {
        self.headerView?.eAction.bind(onNext: { [weak self](action) in
            guard let wSelf = self else {
                return
            }
            switch action {
            case .done:
                let value = wSelf.source[wSelf.currentSelect]
                wSelf.update.onNext(UpdateType.identify(type: value, content: nil))
                fallthrough
            case .cancel:
                UIApplication.shared.windows.forEach({ $0.endEditing(true) })
            }
            
        }).disposed(by: disposeBag)
    }
    
    func setSelect(at index: Int) {
        self.currentSelect = index
        self.pickerView?.selectRow(index, inComponent: 0, animated: false)
    }
}

extension ZPKYCPickerChooseTypeView: UIPickerViewDataSource {
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return source.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
}

extension ZPKYCPickerChooseTypeView: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let text = source[row].description
        guard let label = view as? UILabel  else {
            // create
            let nLabel = UILabel(frame: .zero)
            nLabel.font = UIFont.sfuiTextRegular(withSize: 24)
            nLabel.text = text
            nLabel.sizeToFit()
            return nLabel
        }
        label.text = text
        label.sizeToFit()
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.currentSelect = row
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
}

// MARK: - View Header
enum ZPKYCHeaderAction {
    case cancel
    case done
}

final class ZPKYCHeaderChooseView: UIView {
    private var btnCancel: UIButton?
    private var btnDone: UIButton?
    private var titleView: UILabel?
    lazy var eAction: PublishSubject<ZPKYCHeaderAction> = PublishSubject()
    
    private let disposeBag = DisposeBag()
    convenience init(withTitle name: String?) {
        self.init(frame: .zero)
        addLayout()
        self.backgroundColor = UIColor.defaultBackground()
        self.titleView?.text = name
        self.btnCancel?.setTitle(R.string_ButtonLabel_Cancel(), for: .normal)
        self.btnDone?.setTitle(R.string_ButtonLabel_Done(), for: .normal)
        setupEvent()
    }
    
    private func addLayout() {
        //Cancel
        let btnCancel = UIButton(type: .custom)
        btnCancel.setTitleColor(UIColor.zaloBase(), for: .normal)
        btnCancel.titleLabel?.font = UIFont.sfuiTextRegular(withSize: 15)
        self.addSubview(btnCancel)
        
        btnCancel.snp.makeConstraints { (make) in
            make.left.equalTo(12)
            make.centerY.equalToSuperview()
        }
        
        self.btnCancel = btnCancel
    
        // Title
        let titleView = UILabel(frame: .zero)
        titleView.font = UIFont.sfuiTextMedium(withSize: 15)
        titleView.textColor = UIColor.defaultText()
        self.addSubview(titleView)
        
        titleView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
        self.titleView = titleView
        // Done
        let btnDone = UIButton(type: .custom)
        btnDone.setTitleColor(UIColor.zaloBase(), for: .normal)
        btnDone.titleLabel?.font = UIFont.sfuiTextSemibold(withSize: 15)
        self.addSubview(btnDone)
        
        btnDone.snp.makeConstraints { (make) in
            make.right.equalTo(-12)
            make.centerY.equalToSuperview()
        }
        
        self.btnDone = btnDone
    }
    
    private func setupEvent() {
        self.btnDone?.rx.tap.asObservable().bind { [unowned self] in
            self.eAction.onNext(.done)
        }.disposed(by: disposeBag)
        
        self.btnCancel?.rx.tap.asObservable().bind { [unowned self] in
            self.eAction.onNext(.cancel)
        }.disposed(by: disposeBag)
    }
    
}

