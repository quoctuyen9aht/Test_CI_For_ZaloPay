//
//  ZPKYCTermCell.swift
//  KYCTest
//
//  Created by Dung Vu on 3/26/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import ZaloPayWalletSDKPrivate
import SnapKit


class ZPKYCTermCell: ZPKYCBaseTableViewCell {
    var btnAgreeTerm: UIButton!
    var btnNext: UIButton!
    private (set) lazy var eOpenLink = PublishSubject<Void>()
    private lazy var borderColor = UIColor(hexString: "#727f8c") ?? UIColor.line()
    private lazy var isIphone4 = UIScreen.main.bounds.height < 568

    override func addLayout() {
        // button check
        let btnCheck = UIButton.checkIcon(with: nil,iconNormal: "general_check", iconSelect: "general_check", isUsingWhiteColor: true)
        btnCheck.contentEdgeInsets = UIEdgeInsetsMake(2, 0, 0, 0)
        btnCheck.layer.cornerRadius = 4
        btnCheck.layer.borderColor = self.borderColor.cgColor
        btnCheck.layer.borderWidth = 1
        btnCheck.clipsToBounds = true
        btnCheck.isSelected = false
        self.contentView.addSubview(btnCheck)
        btnCheck.snp.makeConstraints { (make) in
            make.left.equalTo(12)
            make.top.equalTo(15)
            make.size.greaterThanOrEqualTo(CGSize.zero)
        }
        self.btnAgreeTerm = btnCheck
        self.contentView.layoutSubviews()
        let wBtn = btnCheck.bounds.width + btnCheck.bounds.origin.x + 20

        let message = R.string_UpdateProfileLevel2TermOfUse()
        let lblTerm: MDHTMLLabel = MDHTMLLabel.init(frame: .zero)
        lblTerm.font = UIFont.sfuiTextRegular(withSize: 15)
        lblTerm.numberOfLines = 2
        lblTerm.htmlText = message
        lblTerm.delegate = self
        lblTerm.linkAttributes = [NSAttributedStringKey.foregroundColor.rawValue: UIColor.zaloBase()]
        self.contentView.addSubview(lblTerm)
        
        lblTerm.snp.makeConstraints { (make) in
            make.top.equalTo(btnCheck.snp.top).offset(-8)
            make.left.equalTo(wBtn)
            make.right.equalTo(-12)
            make.height.equalTo(52)
        }
        
        let vTouchExpand = UIView.init(frame: .zero)
        vTouchExpand.backgroundColor = .clear
        vTouchExpand.tag = 678
        self.contentView.addSubview(vTouchExpand)
        vTouchExpand.snp.makeConstraints { (make) in
            make.top.equalTo(btnCheck.snp.top)
            make.left.equalTo(btnCheck.snp.right).priority(.high)
            make.width.equalTo(60)
            make.height.equalTo(22)
        }
        
        // next
        let btnNext = UIButton(type: .custom)
        btnNext.titleLabel?.font = UIFont.sfuiTextRegular(withSize: 18)
        btnNext.setTitleColor(.white, for: .normal)
        btnNext.setTitleColor(.white, for: .disabled)
        btnNext.setTitle(R.string_ButtonLabel_Next(), for: .normal)
        btnNext.setTitle(R.string_ButtonLabel_Next(), for: .disabled)
        let imgNormal = UIImage.mm_image(with: UIColor.zaloBase(), size: CGSize(width: 80, height: 80))
        let imgDisable = UIImage.mm_image(with: UIColor.disableButton(), size: CGSize(width: 80, height: 80))
        btnNext.setBackgroundImage(imgNormal, for: .normal)
        btnNext.setBackgroundImage(imgDisable, for: .disabled)
        btnNext.layer.cornerRadius = 4
        btnNext.clipsToBounds = true
        self.contentView.addSubview(btnNext)
        
        var bottom: CGFloat = -100
        if #available(iOS 10.0, *) {}
        else {
            bottom = -50
        }
        
        btnNext.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.right.equalTo(-10)
            make.top.equalTo(82)
            make.height.equalTo(isIphone4 ? 44 : 54)
            make.bottom.equalTo(bottom).priority(.high)
        }
        
        self.btnNext = btnNext

    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let v = super.hitTest(point, with: event)
        return v?.tag == 678 ? self.btnAgreeTerm : v
    }
    
    override func setupEvent() {
        self.bottomLine.isHidden = true
        self.validate.onNext(false)
        let eButton = btnAgreeTerm.rx.controlEvent(.touchUpInside).scan(false) { (v, _) -> Bool in
            return !v
        }
        
        eButton.bind { [weak self](r) in
            guard let wSelf = self else {
                return
            }
            let color = r ? UIColor.zaloBase() : wSelf.borderColor
            wSelf.btnAgreeTerm.layer.borderColor = color.cgColor
        }.disposed(by: disposeBag)
        
        eButton.bind(to: btnAgreeTerm.rx.isSelected).disposed(by: disposeBag)
        eButton.bind(to: validate).disposed(by: disposeBag)
    }
}

extension ZPKYCTermCell: MDHTMLLabelDelegate {
    func htmlLabel(_ label: MDHTMLLabel!, didSelectLinkWith URL: URL!) {
        self.eOpenLink.onNext(())
    }
}

