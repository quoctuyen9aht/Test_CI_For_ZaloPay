//
//  ZPKYCBirthdayCell.swift
//  KYCTest
//
//  Created by Dung Vu on 3/26/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SnapKit
import ZaloPayWalletSDKPrivate

final class ZPKYCBirthdayCell: ZPKYCBaseTableViewCell, ZPKYCHandlerFocusProtocol {
    weak var delegate: ZPKYCFocusProtocol?
    private var textFieldBirthday: ZPKYCChooseValueTextField?
    private var currentDate: Date?
    private var birthdayPicker: ZPKYCPickerBirthDayView?
    private var lblNotice: UILabel?
    private lazy var noticeColor = #colorLiteral(red: 0.6745098039, green: 0.7019607843, blue: 0.7294117647, alpha: 1)
    override func addLayout() {
        // Add label notice
        let lblNotice = UILabel(frame: .zero)
        lblNotice.font = UIFont.sfuiTextRegular(withSize: 12)
        lblNotice.textColor = noticeColor
        self.contentView.addSubview(lblNotice)
        lblNotice.snp.makeConstraints { (make) in
            make.top.equalTo(9)
            make.left.equalTo(12)
            make.size.greaterThanOrEqualTo(CGSize.zero)
        }
        self.lblNotice = lblNotice
        
        let rView = UILabel(frame: .zero)
        rView.font = UIFont.zaloPay(withSize: 14)
        rView.text = UILabel.iconCode(withName: "gift_arrowdown")
        rView.textColor = UIColor(hexString: "#c7c7cc")
        self.contentView.addSubview(rView)
        rView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(60)
            make.bottom.equalToSuperview().priority(.high)
            make.width.equalTo(25)
        }
        
        let textFieldBirthday = ZPKYCChooseValueTextField.init(with: R.string_Onboard_KYC_Hint_DOB(), needRightView: false)
        self.contentView.addSubview(textFieldBirthday)
        textFieldBirthday.snp.makeConstraints { (make) in
            make.top.equalTo(9)
            make.left.equalToSuperview()
            make.right.equalTo(-25)
            make.bottom.equalTo(-9)
        }
        self.textFieldBirthday = textFieldBirthday
        let birthdayPicker = ZPKYCPickerBirthDayView(frame: CGRect(origin: .zero, size: CGSize(width: UIScreen.main.bounds.width, height: 260)))
        self.birthdayPicker = birthdayPicker
        self.contentView.layoutSubviews()
    }
    
    override func configure(with entity: ZPKYCEntity) {
        self.currentDate = entity.dateOfBirth
        defer {
            validateValue()
        }
        guard let date = self.currentDate else {
            return
        }
        self.textFieldBirthday?.text = date.string(using: "dd/MM/yyyy")
        self.textFieldBirthday?.sendActions(for: .valueChanged)
    }
    
    override func commonSetup() {
        super.commonSetup()
        self.updatePaddingBottomLine(left: 12)
    }
    
    private func validateValue() {
        let message: String?
        let color: UIColor
        let correct: Bool
        
        defer {
            self.lblNotice?.text = message
            self.lblNotice?.textColor = color
            self.contentView.layoutSubviews()
            let hLabel = self.lblNotice?.bounds.height ?? 0
            self.textFieldBirthday?.snp.updateConstraints({ (make) in
                make.top.equalTo(hLabel + 9)
            })
            self.validate.onNext(correct)
        }
        
        guard let currentDate = self.currentDate else {
            message = ""
            color = noticeColor
            correct = false
            return
        }
        let today = Date()
        let range = Calendar.current.dateComponents([.year], from: today, to: currentDate).year ?? 0
        correct = range <= -15
        message = correct ? self.textFieldBirthday?.placeholder : R.string_Onboard_KYC_Invalid_DOB()
        color = correct ? noticeColor : UIColor.error()
        if !correct {
            ZPTrackingHelper.shared().trackEvent(.onboardingkyc_input_dob_invalid)
        }
    }
    
    override func setupEvent() {
       self.input = self.textFieldBirthday
       self.textFieldBirthday?.inputView = self.birthdayPicker
       self.textFieldBirthday?.update.map({ type -> Date? in
        guard case let .birthday(date) = type else {
            return nil
         }
        return date
       }).bind(onNext: { [weak self] in
            self?.currentDate = $0
            self?.validateValue()
       }).disposed(by: disposeBag)
        
        // Update value to entity
        self.validate.filter({ $0 }).bind { [weak self] (_) in
            let date = self?.currentDate
            self?.update.onNext(UpdateType.birthday(date: date))
        }.disposed(by: disposeBag)
    }
    
    override func trackInput() {
        super.trackInput()
        self.textFieldBirthday?.rx.methodInvoked(#selector(becomeFirstResponder)).bind(onNext: { [weak self](_) in
            self?.birthdayPicker?.setDate(with: self?.currentDate)
        }).disposed(by: disposeBag)
    }
}

fileprivate final class ZPKYCPickerBirthDayView: UIView, ZPKYCUpdateContentProtocol {
    lazy var update: PublishSubject<UpdateType> = PublishSubject()
    private var headerView: ZPKYCHeaderChooseView?
    private var currentDate: Date?
    private var pickerDate: UIDatePicker?
    private var disposeBag = DisposeBag()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addLayout()
        self.backgroundColor = .white
        setupEvent()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addLayout() {
        // add view cancel
        let headerView = ZPKYCHeaderChooseView(withTitle: R.string_Onboard_KYC_DOB())
        self.addSubview(headerView)
        headerView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(44)
        }
        self.headerView = headerView
        // add picker
        let pickerDate = UIDatePicker(frame: .zero)
        pickerDate.datePickerMode = .date
        pickerDate.locale = Locale(identifier: "vi-VN")
        self.addSubview(pickerDate)
        pickerDate.snp.makeConstraints { (make) in
            make.top.equalTo(headerView.snp.bottom)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        let today = Date()
//        let maximumDate = Calendar.current.date(byAdding: .year, value: -15, to: today)
//        #if DEBUG
//            assert(maximumDate != nil, "Check this!!!")
//        #endif
//        pickerDate.maximumDate = today
        self.currentDate = today
        self.pickerDate = pickerDate
    }
    
    private func setupEvent() {
        self.pickerDate?.rx.controlEvent(.valueChanged).bind { [weak self] in
            self?.currentDate = self?.pickerDate?.date
        }.disposed(by: disposeBag)
        
        self.headerView?.eAction.bind(onNext: { [weak self](action) in
            guard let wSelf = self else {
                return
            }
            switch action {
            case .done:
                let date = wSelf.currentDate
                wSelf.update.onNext(UpdateType.birthday(date: date))
                fallthrough
            case .cancel:
                UIApplication.shared.windows.forEach({ $0.endEditing(true) })
            }
        }).disposed(by: disposeBag)
    }
    
    func setDate(with birthday: Date?) {
        guard let date = birthday else {
            return
        }
        self.pickerDate?.date = date
    }
    
}
