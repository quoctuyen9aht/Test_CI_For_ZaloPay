//
//  ZPKYCGenderCell.swift
//  KYCTest
//
//  Created by Dung Vu on 3/26/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit
import ZaloPayWalletSDKPrivate

class ZPKYCGenderCell: ZPKYCBaseTableViewCell {
    private var segment: ZPKYCGenderControl?
    override func addLayout() {
        let lblTitle = UILabel.init(frame: .zero)
        lblTitle.font = UIFont.sfuiTextRegular(withSize: 16)
        lblTitle.textColor = UIColor.defaultText()
        self.contentView.addSubview(lblTitle)
        
        lblTitle.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalTo(12)
            make.size.greaterThanOrEqualTo(CGSize.zero)
        }
        lblTitle.text = R.string_Onboard_KYC_Gender()
        let sources: [Gender] = [.male, .female]
        let segment = ZPKYCGenderControl.init(with: sources.map({ $0.description }))
        self.contentView.addSubview(segment)
        
        let x: CGFloat = 160
        let wSegment = UIScreen.main.bounds.width - x
        segment.snp.makeConstraints { (make) in
            make.left.equalTo(x)
            make.size.equalTo(CGSize(width: wSegment, height: 60))
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().priority(.high)
        }
        self.segment = segment
    }
    
    override func setupEvent() {
        self.segment?.rx.controlEvent(.valueChanged).bind { [weak self] in
           let idx = self?.segment?.selectedSegmentIndex ?? 0
           let gender = Gender(rawValue: idx)
           self?.update.onNext(UpdateType.gender(type: gender))
        }.disposed(by: disposeBag)
    }
    
    override func configure(with entity: ZPKYCEntity) {
        let gender = entity.gender ?? .male
        self.segment?.selectedSegmentIndex = gender.rawValue
        self.validate.onNext(true)
    }
}
