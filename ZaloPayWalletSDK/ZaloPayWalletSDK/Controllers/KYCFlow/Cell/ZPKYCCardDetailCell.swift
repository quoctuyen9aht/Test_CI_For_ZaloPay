//
//  ZPKYCCardDetailCell.swift
//  KYCTest
//
//  Created by Dung Vu on 3/26/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import UIKit
import SnapKit

final class ZPKYCCardDetailCell: ZPKYCBaseTableViewCell {
    private var iconBank: UIImageView?
    private var lblDescription: UILabel?
    override func commonSetup() {
        // Add view
        super.commonSetup()
        self.validate.onNext(true)
    }
    
    override func addLayout() {
        // image view
        let imgView = UIImageView(frame: .zero)
        imgView.contentMode = .scaleAspectFit
        self.contentView.addSubview(imgView)
        imgView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.size.equalTo(CGSize(width: 36, height: 36))
            make.left.equalTo(12)
        }
        self.iconBank = imgView
        
        let lblDescription = UILabel(frame: .zero)
        lblDescription.font = UIFont.sfuiTextRegular(withSize: 17)
        lblDescription.textColor = .black
        lblDescription.numberOfLines = 0
        lblDescription.textAlignment = .left
        self.contentView.addSubview(lblDescription)
        
        lblDescription.snp.makeConstraints { (make) in
            make.left.equalTo(63)
            make.right.equalToSuperview().offset(-10)
            make.top.equalTo(20)
            make.height.greaterThanOrEqualTo(0).priority(.required)
            make.bottom.equalTo(-20).priority(.high)
        }
        self.lblDescription = lblDescription
        
    }
    
    func updateInformation(icon: UIImage?, content: String?) {
        self.iconBank?.image = icon
        self.lblDescription?.text = content
        
    }
    
}
