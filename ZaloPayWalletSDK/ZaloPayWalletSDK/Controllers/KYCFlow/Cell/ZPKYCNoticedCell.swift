//
//  ZPKYCErrorCell.swift
//  ZaloPayWalletSDK
//
//  Created by tridm2 on 5/14/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit
import ZaloPayWalletSDKPrivate


class ZPKYCNoticedCell : ZPKYCBaseTableViewCell {
    static let CellHeight: CGFloat = 42.0

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.backgroundColor = UIColor(hexString: "#fffcbb")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func configure(with entity: ZPKYCEntity) {
        self.validate.onNext(true)
    }
    
    override func addLayout() {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        imageView.animationDuration = 1
//        imageView.animationRepeatCount = Int.max
        imageView.animationImages = [#imageLiteral(resourceName: "ic_speaker_on"), #imageLiteral(resourceName: "ic_speaker_off")]
        imageView.startAnimating()
        
        self.contentView.addSubview(imageView)
        let topConstant = (ZPKYCNoticedCell.CellHeight - 27) / 2
        imageView.snp.makeConstraints {
            $0.left.equalTo(13.9)
            $0.centerY.equalToSuperview()
            $0.width.height.equalTo(27)
            $0.top.equalTo(topConstant)
            $0.bottom.equalTo(-topConstant)
        }
        
        let label = UILabel()
        label.text = R.string_Onboard_KYC_Phone_Match_ZaloPhone_Note()
        label.font = UIFont.sfuiTextRegular(withSize: 13)
        label.textAlignment = .left
        label.textColor = UIColor(hexString: "#af8000")
        self.contentView.addSubview(label)
        label.snp.makeConstraints {
            $0.left.equalTo(imageView.snp.right).offset(9.3)
            $0.right.equalToSuperview()
            $0.centerY.equalToSuperview()
            $0.height.equalTo(18)
        }
    }
}
