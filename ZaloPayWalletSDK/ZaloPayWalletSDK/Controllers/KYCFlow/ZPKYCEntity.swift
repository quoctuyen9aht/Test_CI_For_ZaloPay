//
//  ZPKYCEntity.swift
//  KYCTest
//
//  Created by Dung Vu on 3/26/18.
//  Copyright © 2018 VNG. All rights reserved.
//

import Foundation
import UIKit
import ZaloPayWalletSDKPrivate
enum Gender: Int, CustomStringConvertible {
    case male
    case female
    
    var description: String {
        switch self {
        case .male:
            return R.string_MainProfile_Male()
        case .female:
            return R.string_MainProfile_Female()
        }
    }
    
    var value: Int {
        return self.rawValue + 1
    }
}

enum IdentifyType: Int, CustomStringConvertible {
    case cmnd
    case passport
    case identity
    
    var description: String {
        switch self {
        case .cmnd:
            return R.string_Onboard_KYC_CMND_Identify()
        case .passport:
            return R.string_Onboard_KYC_Passport_Identify()
        case .identity:
            return R.string_Onboard_KYC_CC_Identify()
        }
    }
    
    var maxCharacter: Int {
        switch self {
        case .cmnd:
            return 12
        case .passport:
            return 8
        case .identity:
            return 12
        }
    }
    
    var keyboardType: UIKeyboardType {
        switch self {
        case .cmnd:
            return .numberPad
        case .passport:
            return .default
        case .identity:
            return .numberPad
        }
    }
    
    var value: Int {
        return self.rawValue + 1
    }
}

final class ZPKYCEntity {
    var name: String?
    var typeOfIdentify: IdentifyType? = .cmnd
    var numberIdentify: String? {
        didSet {
            guard let type = typeOfIdentify, type != .identity else {
                return
            }
            ZaloPayWalletSDKPayment.sharedInstance().appDependence?.updateIdentifyNumber(numberIdentify)
        }
    }
    var phone: String?
    var dateOfBirth: Date?
    var gender: Gender? = .male
    var cache = [IdentifyType: String]()
    
    private var isMapBank: Bool
    init(mapBank flow:Bool) {
        isMapBank = flow
    }
    func update(with json:[String : Any]?) {
        guard let json = json else {
            return
        }
        let gender = (json["gender"] as? Int ?? 0) - 1
        self.gender = Gender(rawValue: gender) ?? .male
        if let value = json["dob"] as? TimeInterval, value > 0 {
            self.dateOfBirth = Date(timeIntervalSince1970: value)
        }
        
        // Map bank is not load name and identify
//        guard !isMapBank else {
//            return
//        }
        
        self.name = json["fullName"] as? String
        guard let ids = json["ids"] as? [[String : Any]],
              let lastIdentifyInfor = ids.last else {
            return
        }
        // Using cache for alltype
        for info in ids {
            guard let tIndentify = info["type"] as? Int,
                  let typeOfIdentify = IdentifyType(rawValue: tIndentify - 1),
                  let numberIdentify = info["value"] as? String else {
                    continue
            }
            cache[typeOfIdentify] = numberIdentify
        }
        
        let tIndentify = lastIdentifyInfor["type"] as? Int ?? 1
        self.typeOfIdentify = IdentifyType(rawValue: tIndentify - 1)
        self.numberIdentify = lastIdentifyInfor["value"] as? String
//        // Cache
//        if let type = self.typeOfIdentify {
//            cache[type] = self.numberIdentify
//        }
    }
    
    func json() -> [String : Any] {
//        Json Object:
//        {
//            "fullName":"",
//            "zaloPhone":"",
//            "idValue":"",
//            "dob":"1988-05-22"
//            "idType": "1" (accepted value: 1 (CMND), 2 (PASSPORT), 3 (CC))
//            "gender": "1"   (accepted value: 1 (MALE), 2 (FEMALE))
//        }
        var result: [String : Any] = [:]
        result["fullName"] = name
        result["zaloPhone"] = phone ?? ZaloPayWalletSDKPayment.sharedInstance().appDependence?.userPhoneNumber()
        result["idValue"] = numberIdentify
        result["dob"] = dateOfBirth?.timeIntervalSince1970
        result["idType"] = typeOfIdentify?.value
        result["gender"] = gender?.value
        return result
    }
}

// MARK: -- Date
extension Date {
    func string(using format: String = "yyyyMMddHHmm") -> String {
        let formater = DateFormatter()
        formater.dateFormat = format
        return formater.string(from: self)
    }
    
    static func date(from string: String?, format: String = "yyyyMMddHHmm") -> Date? {
        guard let string = string else {
            return nil
        }
        let formater = DateFormatter()
        formater.dateFormat = format
        return formater.date(from: string)
    }
}

extension String {
    var containsEmoji: Bool {
        for scalar in unicodeScalars {
            switch scalar.value {
            case 0x1F600...0x1F64F, // Emoticons
            0x1F300...0x1F5FF, // Misc Symbols and Pictographs
            0x1F680...0x1F6FF, // Transport and Map
            0x2600...0x26FF,   // Misc symbols
            0x2700...0x27BF,   // Dingbats
            0xFE00...0xFE0F,   // Variation Selectors
            0x1F900...0x1F9FF, // Supplemental Symbols and Pictographs
            0x1F1E6...0x1F1FF: // Flags
                return true
            default:
                continue
            }
        }
        return false
    }
}
