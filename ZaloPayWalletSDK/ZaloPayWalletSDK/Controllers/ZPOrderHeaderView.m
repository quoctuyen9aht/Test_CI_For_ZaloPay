//
//  ZPOrderHeaderView.m
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/14/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPOrderHeaderView.h"
#define kHeaderHeightWithOutMessage        138
#define kDescriptionHeight_3Line           [UIView isScreen2x] ? 59 : 65
#define kDescriptionToBottom               15
@interface ZPOrderHeaderView()
@property (strong, nonatomic) UIView *discountView;
@end


@implementation ZPOrderHeaderView

- (id)initWithAmount:(long)amount message:(NSString *)message {
    float h = kHeaderHeightWithOutMessage;
    if (message.length > 0) {
        h = h + [ZPOrderHeaderView calculateMessageHeight:message];
    }
    float w = [UIScreen mainScreen].applicationFrame.size.width;
    self = [super initWithFrame:CGRectMake(0, 0, w, h)];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self setupViewWithAmount:amount message:message];
    }
    return self;
}

- (id)initWithUsingInPopupSDK:(long)amount {
    self = [super initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 100)];
    self.backgroundColor = [UIColor whiteColor];
    [self setupViewWithAmount:amount message:nil];
    
    // change
    [self.labelAmount mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(30);
        make.centerY.equalTo(0);
        make.width.equalTo([UIScreen mainScreen].bounds.size.width - 40);
        make.height.greaterThanOrEqualTo(0);
    }];
    
    [self.labelMessage mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.center.equalTo(self.center);
        make.height.equalTo(0);
    }];
    // Add button
    
    return self;
}



+ (float)calculateMessageHeight:(NSString *)message {
    static UILabel *label;
    if (!label) {
        label = [[UILabel alloc] init];
        label.font = [UIFont defaultSFUITextItalic];
        label.numberOfLines = 0;
        label.textColor = [UIColor subText];
        label.textAlignment = NSTextAlignmentCenter;
    }
    label.text = message;
    CGSize size = CGSizeMake([UIScreen mainScreen].applicationFrame.size.width - 40, 200);
    float height = [label sizeThatFits:size].height;
    float maxHeight = kDescriptionHeight_3Line;
    height =  MIN(height, maxHeight);
    return height + kDescriptionToBottom;
}


- (UIView *)viewBeforeDiscount:(long)beforeAmount {
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 32)];
    // label title
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectZero];
    [lblTitle zpMainGrayRegular];
    [v addSubview:lblTitle];
    [lblTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(12);
        make.bottom.equalTo(0);
        make.size.greaterThanOrEqualTo(0);
    }];
    lblTitle.text = [R string_Voucher_before_input_title];
    
    
    // lable amount
    UILabel *lblAmount = [[UILabel alloc] initWithFrame:CGRectZero];
    [lblAmount zpMainGrayRegular];
    [v addSubview:lblAmount];
    [lblAmount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(-12);
        make.bottom.equalTo(0);
        make.size.greaterThanOrEqualTo(0);
    }];
    
    NSString *money = [@(beforeAmount) formatCurrency];
    NSRange r = [money rangeOfString:@"VND"];
    NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:money attributes:@{NSForegroundColorAttributeName: [UIColor subText]}];
    [att addAttributes:@{NSFontAttributeName : [UIFont SFUITextRegularWithSize:12]} range:r];
    
    lblAmount.attributedText = att;
    
    return v;
}



- (void) updateAmountTitle:(long)amount {
    NSString *moneyTitle = [@(amount) formatCurrency];
    self.labelAmount.attributedText = [moneyTitle formatCurrencyFont:[UIFont SFUITextMediumWithAmount:amount]
                                                               color:[UIColor defaultText]
                                                             vndFont:[UIFont SFUITextRegularWithSize:14]
                                                               vndcolor:[UIColor subText]
                                                           alignment:NSTextAlignmentCenter];
}

#pragma mark - setup view
- (void)setupViewWithAmount:(long)amount message:(NSString *)message {
    self.labelAmount = [[UILabel alloc] init];
    [self addSubview:self.labelAmount];
    self.labelAmount.textAlignment = NSTextAlignmentCenter;
    self.labelAmount.font = [UIFont SFUITextMediumWithAmount:amount];
    self.labelAmount.textColor = [UIColor defaultText];
    
    [self.labelAmount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(30);
        make.width.equalTo([UIScreen mainScreen].bounds.size.width - 30);
        make.top.equalTo(40);
        make.height.equalTo(58);
    }];
    self.labelMessage = [[UILabel alloc] init];
    [self addSubview:self.labelMessage];
    self.labelMessage.font = [UIFont defaultSFUITextItalic];
    self.labelMessage.numberOfLines = 0;
    self.labelMessage.textColor = [UIColor subText];
    self.labelMessage.textAlignment = NSTextAlignmentCenter;
    
    [self.labelMessage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(20);
        make.width.equalTo([UIScreen mainScreen].bounds.size.width - 40);
        make.top.equalTo(self.labelAmount.mas_bottom).offset(40);
        make.bottom.equalTo(-kDescriptionToBottom);
    }];
    self.labelMessage.text = message;
    [self updateAmountTitle:amount];
    self.bottomLine = [[ZPIconFontImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 1)];
    [self addSubview:self.bottomLine];
    [self.bottomLine setImage:[UIImage imageNamed:@"payorder_line"]];
    [self.bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(12);
        make.width.equalTo([UIScreen mainScreen].bounds.size.width - 24);
        make.height.equalTo(2);
        make.bottom.equalTo(0);
    }];
}

@end
