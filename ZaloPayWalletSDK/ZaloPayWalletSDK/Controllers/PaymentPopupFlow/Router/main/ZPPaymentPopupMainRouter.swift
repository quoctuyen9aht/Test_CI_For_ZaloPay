//
//  ZPPaymentPopupMainRouter.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/8/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ZaloPayWalletSDKPrivate
struct ZPPaymentPopupMainRouter {
    weak var presenter: ZPPaymentPopupMainPresenter?
    func pushTo(segue name: String, sender value:Any?) {
        presenter?.mainController?.performSegue(withIdentifier: name, sender: value)
    }
    
    func findPassword() {
        self.pushTo(segue: ZPPaymentPopupMainType.password.segueName, sender: self.presenter?.currentMethod?.method)
    }
    
    func authenByTouchId() -> Observable<String> {
        return Observable.create({ (s) -> Disposable in
            ZaloPayWalletSDKPayment.sharedInstance().appDependence.authen(fromTouchIdCancel: {
                s.onCompleted()
            }, error: {(e) in
                guard self.presenter?.isCallComing == false else {
                    s.onCompleted()
                    return
                }
                let error = NSError(domain: NSURLErrorDomain, code: NSURLErrorDataNotAllowed, userInfo: nil)
                s.onError(error)
            }, complete: {
                s.onNext($0 ?? "")
                s.onCompleted()
            })
            return Disposables.create()
        })
    }
    
    func inputOTP(from item: RetryExcutePaymentItem) {
       self.pushTo(segue: ZPPaymentPopupMainType.otp.segueName, sender: item)
    }
    
    private func validKYCInfor() -> Observable<Bool> {
        return Observable.just(true)
    }
    
    private func alertKYC() -> Observable<Bool> {
        let message = R.string_KYC_Alert()
        let buttons = [R.string_ButtonLabel_Later() ?? "", R.string_ButtonLabel_OK() ?? ""]
        return Observable.create({ (s) -> Disposable in
            ZPDialogView.showDialog(with: DialogTypeNotification, title: nil, message: message, buttonTitles: buttons) {
                s.onNext($0 != 0)
                s.onCompleted()
            }
            return Disposables.create()
        }).filter({ $0 })
    }
    
    private func excuteKYCFlow() -> Observable<Bool> {
        return alertKYC().flatMap({ _ in
            ZPKYCViewController.runKYCFlow(on: self.presenter?.mainController?.rootPresenter?.rootVC, title: R.string_UpdateProfileLevel2_Title(), using: nil, at: 1)
        }).flatMap { (result) -> Observable<[String: Any]?> in
            switch result {
            case .cancel:
                let e = NSError(domain: NSURLErrorDomain, code: NSURLErrorCancelled, userInfo: nil)
                return Observable.error(e)
            case .fail(let e):
                return Observable.error(e)
            case .sucess(let values):
                let v = values["kyc"] as? String
                let json: [String : Any]? = v?.toJson()
                var nJSON: [String : Any] = [:]
                nJSON["zalophone"] = json?["zaloPhone"] as? String
                nJSON["gender"] = json?["gender"] as? Int
                nJSON["fullname"] = json?["fullName"] as? String
                nJSON["birthday"] = json?["dob"] as? TimeInterval
                nJSON["idtype"] = json?["idType"] as? Int
                nJSON["idvalue"] = json?["idValue"] as? String
                return Observable.just(nJSON)
            }
            }.flatMap { (json) -> Observable<Bool> in
                guard let interactor = self.presenter?.mainController?.rootPresenter?.interactor else {
                    return Observable.empty()
                }
                return interactor.updateKYC(infor: json).do(onError: {
                    ZPDialogView.showDialogWithError($0, handle: nil)
                }).catchError({ _ in Observable.just(false)})
        }
    }
    
    func runKYCFlow() -> Observable<Bool>{
        guard let interactor = self.presenter?.mainController?.rootPresenter?.interactor else {
            return Observable.empty()
        }
        
        let canUseKYCLimit = ZaloPayWalletSDKPayment.sharedInstance().appDependence?.kycEnableLimitTransaction() ?? false
        let limit = ZaloPayWalletSDKPayment.sharedInstance().appDependence?.kycLimitTransaction() ?? 500000
        guard let bill = self.presenter?.interactor.getBill(), canUseKYCLimit else {
            return Observable.just(true)
        }
        
        guard bill.amount >= limit else {
            return Observable.just(true)
        }
        
        return interactor.checkExistKYC().flatMap { (next) -> Observable<Bool> in
            return next ? Observable.just(true) : self.excuteKYCFlow()
        }
    }
    
}
