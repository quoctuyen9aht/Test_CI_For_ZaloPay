//
//  ZPPaymentPopupPasswordRouter.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/27/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import UIKit

struct ZPPaymentPopupPasswordRouter {
    private weak var presenter: ZPPaymentPopupPasswordPresenter?
    init(with presenter: ZPPaymentPopupPasswordPresenter?) {
        self.presenter = presenter
    }
    
    func inputOTP(from item:RetryExcutePaymentItem) {
        self.presenter?.controller?.performSegue(withIdentifier: ZPPaymentPopupMainType.otp.segueName, sender: item)
    }
}
