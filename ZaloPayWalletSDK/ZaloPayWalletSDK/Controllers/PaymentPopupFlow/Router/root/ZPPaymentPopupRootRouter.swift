//
//  ZPPaymentPopupRootRouter.swift
//  ZaloPay
//
//  Created by Dung Vu on 12/4/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import ZaloPayWalletSDKPrivate

class ZPPaymentPopupRootRouter {
    private weak var presenter: ZPPaymentPopupPresenter?
    private let disposeBag = DisposeBag()
    init(using presenter: ZPPaymentPopupPresenter?) {
        self.presenter = presenter
    }
    
    func linkNewCard() {
        self.presenter?.rootVC?.isMappingCard = true
        let bill = self.presenter?.interactor.bill
        (~ZaloPayWalletSDKPayment.sharedInstance().appDependence.mapBank(with: self.presenter?.rootVC, for: bill)).subscribe(onNext: { [weak self](v) in
            self?.presenter?.loadMethods()
            self?.presenter?.rootVC?.popMe()
            }, onError: { [weak self](_) in
                self?.presenter?.rootVC?.popMe()
            }).disposed(by: disposeBag)
    }
    
    func openWeb(from url: String) -> Observable<Bool> {
        self.presenter?.rootVC?.performSegue(withIdentifier: "showWeb", sender: url)
        let e = self.presenter?.rootVC?.eResultWeb
        return e ?? Observable.empty()
    }
    
}

