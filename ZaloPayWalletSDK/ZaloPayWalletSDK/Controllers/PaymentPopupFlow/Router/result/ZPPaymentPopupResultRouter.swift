//
//  ZPPaymentPopupResultRouter.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/22/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayWalletSDKPrivate
import RxSwift
import RxCocoa
class ZPPaymentPopupResultRouter: NSObject {
    private weak var presenter: ZPPaymentPopupResultPresenter?
    private let disposeBag = DisposeBag()
    convenience init(with presenter:ZPPaymentPopupResultPresenter?) {
        self.init()
        self.presenter = presenter
    }
    
    func showActionSheet() {
        (~ZPCustomActionSheet.show()).map({ (v) -> ZPCustomActionSheetType? in
            let value = v?.intValue ?? -1
            return ZPCustomActionSheetType(rawValue: value)
        }).bind { [weak self] in
            self?.handlerChooseActionSheet(from: $0)
        }.disposed(by: disposeBag)
    }
    
    private func handlerChooseActionSheet(from action: ZPCustomActionSheetType?) {
        
        guard let action = action, let vc = self.presenter?.controller else {
            return
        }
        
        switch action {
        case .FAQ:
            ZaloPayWalletSDKPayment.sharedInstance().appDependence.showFAQ(vc)
        case .needSupport:
            let imgCapture = self.captureImage(from: vc)
            var params: [String: Any] = [:]
            let transType = self.presenter?.interactor.getBill().transType ?? ZPTransType.billPay
            let payType = ZPDataManager.sharedInstance().getTitleByTranstype(transType)
            let response = self.presenter?.interactor.response()
            let zptransid = response?.zpTransID
            let errorCode = response?.originalCode
            let supportMessge = response?.message
            params["paytype"] = payType
            params["errorcode"] = errorCode
            params["zptransid"] = zptransid
            params["errormessage"] = supportMessge
            params["capturescreen"] = imgCapture
           ZaloPayWalletSDKPayment.sharedInstance().appDependence.showNeedSupport(using: params, from: vc)
        case .cancel:
            break
        }
    }
    
    private func captureImage(from vc: UIViewController) -> UIImage? {
        UIGraphicsBeginImageContext(vc.view.bounds.size)
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        context.clip(to: vc.view.bounds)
        vc.view.layer.render(in: context)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result
    }
}
