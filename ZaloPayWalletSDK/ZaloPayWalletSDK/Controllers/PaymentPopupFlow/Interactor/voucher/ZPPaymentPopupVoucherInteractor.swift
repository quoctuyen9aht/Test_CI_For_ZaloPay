//
//  ZPPaymentPopupVoucherInteractor.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/14/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import UIKit
import ZaloPayWalletSDKPrivate

class ZPPaymentPopupVoucherInteractor {
    weak var presenter: ZPPaymentPopupVoucherPresenter?
    weak var rootInteractor: ZPPaymentPopupInteractor?
    private let disposeBag = DisposeBag()
    let listVoucher: Variable<[ZPVoucherHistory]> = Variable([])
    let error: Variable<Error?> = Variable(nil)
    
    init(with presenter: ZPPaymentPopupVoucherPresenter?) {
        self.presenter = presenter
        self.rootInteractor = findingRootInteractor()
    }
    
    func getBill() -> ZPBill {
        guard let bill = rootInteractor?.bill else {
            fatalError("Check this case")
        }
        return bill
    }
    
    func useVoucher(from voucherHistory: ZPVoucherHistory) -> Observable<ZPVoucherInfo> {
        guard let rootInteractor = rootInteractor else {
            return Observable.empty()
        }
        
        return rootInteractor.usingVoucher(from: voucherHistory)
    }
    
    private func findingRootInteractor() -> ZPPaymentPopupInteractor? {
        
        guard let parentVC = self.presenter?.controller?.navigationController?.parent as? ZPPaymentPopupRootController else {
            return nil
        }
        return parentVC.presenter.interactor
    }
    
    private func loadAllVouchers() -> Observable<[ZPVoucherHistory]> {
        let all = self.rootInteractor?.listVoucher()
        return Observable.just(all ?? [])
    }
    
    
    func loadSource() {
        self.loadAllVouchers().map({ v -> [ZPVoucherHistory] in
            var result = v
            let addVoucher = ZPVoucherHistory()
            addVoucher.discountamount = -1
            defer {
                result.removeAll()
            }
            result.append(addVoucher)
            return result
        }).bind(to: self.listVoucher).disposed(by: disposeBag)
    }
}
