//
//  ZPPaymentPopupPasswordInteractor.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/11/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct ZPPaymentPopupPasswordInteractor {
    weak var presenter: ZPPaymentPopupPasswordPresenter?
    init(with presenter: ZPPaymentPopupPasswordPresenter?) {
        self.presenter = presenter
    }
}
