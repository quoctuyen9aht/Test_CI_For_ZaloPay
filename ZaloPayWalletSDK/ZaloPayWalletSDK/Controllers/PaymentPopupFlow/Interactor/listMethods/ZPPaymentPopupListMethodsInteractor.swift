//
//  ZPPaymentPopupListMethodsInteractor.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/13/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ZaloPayWalletSDKPrivate
import UIKit

struct ZPPaymentPopupListMethodsInteractor {
    private weak var presenter: ZPPaymentPopupListMethodsPresenter?
    var eSource: Observable<[ZPPaymentMethodCellDisplay]> {
        return self.findSource()
    }
    init(using presenter: ZPPaymentPopupListMethodsPresenter?) {
        self.presenter = presenter
    }
    
    private func findSource() -> Observable<[ZPPaymentMethodCellDisplay]>{
        guard let parentVC = self.presenter?.controller?.navigationController?.parent as? ZPPaymentPopupRootController else {
            return Observable.just([])
        }
        let rootPresenter = parentVC.presenter
        // sort method enable
        return rootPresenter.sourceDisplay.asObservable().map({ (items) -> [ZPPaymentMethodCellDisplay] in
            var methodsEnable = items.filter({ $0.method.isEnable() })
            var methodsDisable = items.filter({ !$0.method.isEnable() })
            let result = methodsEnable + methodsDisable
            defer {
                methodsEnable.removeAll()
                methodsDisable.removeAll()
            }
            return result
        })
    }
}
