//
//  ZPPaymentPopupInputOTPInteractor.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/28/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayWalletSDKPrivate
import RxSwift
import RxCocoa

struct ZPPaymentPopupInputOTPInteractor {
    let item: RetryExcutePaymentItem
    weak var presenter: ZPPaymentPopupInputOTPPresenter?
    private var rootInteractor: ZPPaymentPopupInteractor? {
        return presenter?.rootPresenter?.interactor
    }

    func retryPayment(with otp: String) -> Observable<ZPPaymentResponse?> {
        // tracking internet
        guard ZPDataManager.sharedInstance().checkInternetConnect() else {
            let error = NSError(domain: NSURLErrorDomain, code: NSURLErrorNotConnectedToInternet, userInfo: ["returnmessage": stringNetworkError])
            return Observable.error(error)
        }
        
        guard let rInteractor = rootInteractor else {
            return Observable.empty()
        }
        // Track log
        rInteractor.trackVerifyOTP(with: item)
        let card = item.card
        if let atmCard = card as? ZPAtmCard {
            atmCard.step = ZPAtmPaymentStep.atmPaymentStepAtmAuthenPayer.rawValue
            atmCard.otpType = "otp"
            atmCard.authenValue = otp
            atmCard.zpTransID = item.response.zpTransID
        }
        return rInteractor.excutePayment(with: item.paymentMethod, from: card, methodType: item.methodType)
    }
}
