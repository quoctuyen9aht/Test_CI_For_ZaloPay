//
//  ZPPaymentPopupResultInteractor.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/22/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayWalletSDKPrivate

struct ItemResult {
    let bill: ZPBill
    let response: ZPPaymentResponse
    let method: ZPPaymentMethod?
}

class ZPPaymentPopupResultInteractor {
    private let result: ItemResult
    init(using result: ItemResult) {
        self.result = result
    }
    
    func getBill() -> ZPBill {
        return result.bill
    }
    
    func response() -> ZPPaymentResponse {
        return result.response
    }
    
    func generateData() -> [DataTypeDisplay] {
        
        let response = self.result.response
        
        let topItem = DisplayResultItem(type: .top, title: response.message, description: response.suggestMessage)
        
        var desriptionItems = [DisplayResultItem]()
        let f = ZPBillExtraInfoData(title: R.string_Pay_Bill_Merchant_Name(), value: self.result.bill.appName(), type: ZPBillExtraInfoType_Info)
        
        let totalChargeFee = self.getBill().currentPaymentMethod.totalChargeFee
        let feeText = totalChargeFee > 0 ? NSNumber(value: totalChargeFee).formatCurrency() : R.string_Pay_Bill_Fee_Free()
        let s = ZPBillExtraInfoData(title: R.string_Pay_Bill_Fee_Title(), value: feeText, type: ZPBillExtraInfoType_Fee)
        
        desriptionItems.append(DisplayResultItem(type: .description, title: f?.title, description: f?.value))
        desriptionItems.append(DisplayResultItem(type: .description, title: s?.title, description: s?.value))
        
        let itemsInfor = self.result.bill.extFromItem()?.map({
            DisplayResultItem(type: .description, title: $0.extraKey, description: $0.extraValue)
        }) ?? []
        desriptionItems += itemsInfor
        
        // add transaction time
        desriptionItems.append(self.createTransactionTime(response: response))
        
        // add transactionID
        if let transID = self.createTransactionID(response: response) {
            desriptionItems.append(transID)
        }
        
        let supporItem = DisplayResultItem(type: .support, title: nil, description: nil)
        
        let bottomItem = DisplayResultItem(type: .close, title: nil, description: nil)
        
        return [[topItem], desriptionItems, [supporItem], [bottomItem]]
    }
    
    func createTransactionTime(response:ZPPaymentResponse) -> DisplayResultItem {
        let reqdate = Float(response.reqdate)
        let date = reqdate > 0 ? Date(timeIntervalSince1970: TimeInterval(reqdate/1000.0)) : Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
        let timeString = dateFormatter.string(from: date)
        return DisplayResultItem(type: .description, title: R.string_Time_Title(), description: timeString)
    }
    func createTransactionID(response:ZPPaymentResponse) -> DisplayResultItem? {
        if response.zpTransID.count > 0 && response.zpTransID != "0" {
            let zpTransID = NSMutableString(string: response.zpTransID)
            if zpTransID.length > 6 {
                zpTransID.insert("-", at: 6)
            }
            return DisplayResultItem(type: .description, title: R.string_Feedback_TransactionId(), description: zpTransID as String)
        }
        return nil
    }
    
    
}
