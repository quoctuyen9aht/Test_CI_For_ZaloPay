//
//  ZPPaymentPopupInteractor.swift
//  ZaloPay
//
//  Created by Dung Vu on 12/4/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayWalletSDKPrivate
import RxSwift
import RxCocoa

enum Result<T> {
    case success(T)
    case fail(Error)
}

enum MethodsType {
    case wallet
    case unSupport
    case defaultMethod
    case defaultUnSupportMethod
    case all
}

// MARK: - Main
typealias MethodsData = [ZPPaymentMethod]
class ZPPaymentPopupInteractor {
    var bill: ZPBill
    var currentPassword: String?
    lazy var activityTracking: ActivityIndicator = ActivityIndicator()
    fileprivate let methods: Variable<MethodsData> = Variable([])
    fileprivate (set) lazy var keyCacheVoucher: String? = {
        let key = ZaloPayWalletSDKPayment.sharedInstance().zpVoucherManager.cacheKey(for: self.bill)
        return key
    }()
    
    private var cacheVouchers: [ZPVoucherHistory] = []
    
    var eMethods: Driver<MethodsData> {
        return methods.asDriver()
    }
    
    var isUsingWallet: Bool {
        return self.bill.currentPaymentMethod?.methodType == .zaloPayWallet
    }
    
    fileprivate let disposeBag = DisposeBag()
    init(with bill: ZPBill) {
        self.bill = bill
    }
    func loadMethods() {
        findMethods(from: self.bill).bind(to: methods).disposed(by: disposeBag)
    }
    
    func finalAmount() -> Int {
        let finalAmount = self.bill.amount
        return Int(finalAmount)
    }
}


// MARK: Check
// Step 1 : Check Platform infor -> find methods -> show pop up
extension ZPPaymentPopupInteractor {
    // Platform
    static func startInfor(with bill: ZPBill) -> Observable<ZPAppData?> {
        return Observable<[String: Any]?>.create({ (s) -> Disposable in
            ZPDataManager.sharedInstance().getPlatformInfoConfig { (v) in
                s.onNext(v as? [String: Any])
                s.onCompleted()
            }
            return Disposables.create()
        }).flatMap({ (infor) -> Observable<ZPAppData?> in
            return self.loadAppInfoAndVoucher(from: bill)
        }).do(onNext: { (app) in
            #if DEBUG
                assert(app != nil, "Check logic")
                print(app?.appID ?? -1)
            #endif
            // track start payment
            self.trackStart(with: bill)
        }, onError: { (e) in
            DispatchQueue.main.async {
                ZPProgressHUD.dismiss()
                ZPDialogView.showDialogWithError(e, handle: nil)
            }
        })
    }
    
    // load appinfor
    static func loadAppInfoAndVoucher(from bill: ZPBill) -> Observable<ZPAppData?> {
        guard let sdk = ZaloPayWalletSDKPayment.sharedInstance() else {
            return Observable.empty()
        }
        let transType = [bill.transType.rawValue]
        let signalGetAppInfor = sdk.getAppInfo(withId: bill.appId.toInt, transtypes: transType) ?? RACSignal.empty()
        if bill.transType != .billPay {
            let s = ~signalGetAppInfor
            return s.map({ $0 as? ZPAppData })
        }
        guard let voucherManager = sdk.zpVoucherManager else {
            return Observable.empty()
        }
        let vSignal = voucherManager.preloadVoucher(for: bill) ?? RACSignal.empty()
        let sConcat = (~vSignal).catchErrorJustReturn(nil).debug().concat((~signalGetAppInfor))
        return sConcat.map({ $0 as? ZPAppData })
    }
    
    func findMethods(from bill: ZPBill) -> Observable<MethodsData> {
        return allMethodsSupport(using: bill)
    }
    
   
    // Step 1: Finding
    fileprivate func allMethodsSupport(using bill: ZPBill) -> Observable<MethodsData>{
        let methods = ZaloPayWalletSDKPayment.sharedInstance().zaloPaySDK.chargebleMethods(bill, methodType: .methods) as? MethodsData ?? []
        return Observable.just(methods).flatMap({ [weak self] methods -> Observable<MethodsData> in
            guard let wSelf = self else {
                return Observable.empty()
            }
            return wSelf.sortMethods(from: methods)
        })
    }
    // Step 2: Sort
    fileprivate func sortMethods(from methods:MethodsData) -> Observable<MethodsData> {
        guard methods.count > 0 else {
            return Observable.just([])
        }
        var nMethods = methods
        // load config
        let displayConfig = ZPDataManager.sharedInstance().bankManager.bankDisplayOrderForPayment() as? [String: Any] ?? [:]
        // sort position
        nMethods.sort { (m1, m2) -> Bool in
            let bCode1 = m1.bankCode ?? ""
            let bCode2 = m2.bankCode ?? ""
            let order1 = displayConfig[bCode1] as? Int ?? -1
            let order2 = displayConfig[bCode2] as? Int ?? -1
            return order1 < order2
        }
        
        nMethods.sort {
            return $0.displayOrder < $1.displayOrder
        }
        
        var result: MethodsData = []
        var wallet: MethodsData = []
        var allCard: MethodsData = []
        var unSupport: MethodsData = []
        var defaultMethod: MethodsData = []
        var defaultUnSupportMethod: MethodsData = []
        
        for method in nMethods {
            if method.methodType == .zaloPayWallet && method.status.toInt == ZPAPPStatus.enable.rawValue {
                wallet.append(method)
                continue
            }
            
            let isUnSupport = !method.isEnable()
            if isUnSupport && !method.defaultChannel {
                unSupport.append(method)
                continue
            }
            
            if method.methodType == .savedCard || method.methodType == .savedAccount  || method.methodType == .ccDebit {
                allCard.append(method)
                continue
            }
            
            let isCC = method.isCCCard() || method.isCCDebitCard()
            if !isUnSupport {
                isCC ? defaultMethod.insert(method, at: 0) : defaultMethod.append(method)
                continue
            }
            isCC ? defaultUnSupportMethod.insert(method, at: 0) :  defaultUnSupportMethod.append(method)
        }
        defer {
            wallet.removeAll()
            allCard.removeAll()
            unSupport.removeAll()
            defaultMethod.removeAll()
            defaultUnSupportMethod.removeAll()
        }
        
        result += wallet
        result += allCard
        result += unSupport
        result += defaultMethod
        result += defaultUnSupportMethod
        
        return Observable.just(result)
    }
    
    func updateBill(from method: ZPPaymentMethod?) {
        self.bill.currentPaymentMethod = method
        self.bill.voucherInfo = nil
    }
}

// MARK: - Analytic
extension ZPPaymentPopupInteractor {
    static func trackStart(with bill: ZPBill) {
        let data = ZPAOrderData.getAppInfo(
            bill.appTransID,
            appid: "\(bill.appId)",
            transtype: Int32(bill.transType.rawValue),
            source: OrderSource(rawValue: OrderSource.RawValue(bill.ordersource)),
            result: .success,
            server_result: 1)
        ZPAppFactory.sharedInstance().orderTracking.startTrackOrder(data)
    }
    
    func trackChooseMethod(using channel: ZPChannel?, method: ZPPaymentMethod?) {
        guard let channel = channel, let method = method else {
            return
        }
        ZPAppFactory.sharedInstance().orderTracking.trackChoosePayMethod(self.bill.appTransID, pcmid: Int(channel.channelID), bankCode: method.bankCode, result: .success)
    }
    
    func trackVerifyOTP(with retryItem: RetryExcutePaymentItem) {
        ZPAppFactory.sharedInstance().orderTracking.trackVerifyOtp(self.bill.appTransID, transid: retryItem.response.zpTransID ?? "", serverResult: Int(retryItem.serverCode))
    }
    
    func trackWebLogin() {
        ZPAppFactory.sharedInstance().orderTracking.trackWebLogin(self.bill.appTransID, result: .success)
    }
    
    func trackOrderDisplay(from response: ZPPaymentResponse, resultDisplay: OrderResultDisplay) {
        ZPAppFactory.sharedInstance().orderTracking.trackOrderDisplay(self.bill.appTransID, errorCode: Int(response.errorCode), paymentStatus: resultDisplay, message: response.message ?? "")
    }
    
    func trackOrderResult(from response: ZPPaymentResponse) {
        let sdkResult:OrderStepResult = response.errorCode >= 1 ? .success : .fail;
        ZPAppFactory.sharedInstance().orderTracking.trackOrderResult(self.bill.appTransID, sdkResult: sdkResult, serverResult: Int(response.originalCode))
    }
    
    func trackUserCancel() {
        ZPAppFactory.sharedInstance().orderTracking.trackUserCancel(self.bill.appTransID)
    }
    
    func writeLog() {
        ZaloPaySDKUtil.writeAppTransIdLog(self.bill)
    }
    
}

// MARK: - KYC
extension ZPPaymentPopupInteractor {
    func checkExistKYC() -> Observable<Bool> {
        // tracking internet
        guard ZPDataManager.sharedInstance().checkInternetConnect() else {
            let error = NSError(domain: NSURLErrorDomain, code: NSURLErrorNotConnectedToInternet, userInfo: ["returnmessage": stringNetworkError])
            return Observable.error(error)
        }
        
        guard let sdk = ZaloPayWalletSDKPayment.sharedInstance() else {
            return Observable.empty()
        }
        
        return (~sdk.getUserProfile()).map ({ json -> Bool in
            guard let json = json as? [String: Any] else {
                return false
            }
            
            let kycInfo = json["kycInfo"] as? [String: Any]
            let kycInfoMissing = (kycInfo?["kycInfoMissing"] as? Bool) ?? true
            return !kycInfoMissing
        }).trackActivity(self.activityTracking).observeOn(MainScheduler.instance)
    }
    
    func updateKYC(infor: [String: Any]?) -> Observable<Bool> {
        guard let infor = infor else {
            return Observable.empty()
        }
        
        // tracking internet
        guard ZPDataManager.sharedInstance().checkInternetConnect() else {
            let error = NSError(domain: NSURLErrorDomain, code: NSURLErrorNotConnectedToInternet, userInfo: ["returnmessage": stringNetworkError])
            return Observable.error(error)
        }
        
        guard let sdk = ZaloPayWalletSDKPayment.sharedInstance() else {
            return Observable.empty()
        }
        return (~sdk.updateKYCInfor(infor)).map({ json -> Bool in
            let json = json as? [String: Any]
            let result = json?["returncode"] as? Int
            return result == 1
        }).trackActivity(self.activityTracking).observeOn(MainScheduler.instance)
    }
}

// MARK: - Voucher
extension ZPPaymentPopupInteractor {
    func listVoucher() -> [ZPVoucherHistory] {
        let voucherManger = ZaloPayWalletSDKPayment.sharedInstance().zpVoucherManager
        var list: [ZPVoucherHistory]?
        
        // Track load from cache
        func loadCacheVoucher() {
            // Check if it has result -> use
            findingVoucher: if cacheVouchers.count > 0 {
                list = cacheVouchers
            } else {
                list = voucherManger?.voucherCaches(for: self.bill) as? [ZPVoucherHistory]
                
                // check need cache for next
                guard let temp = list , temp.count > 0 else {
                    break findingVoucher
                }
                // Keep for next
                self.cacheVouchers = temp
            }
        }
        
        loadCacheVoucher()
        // filter if have method
        if self.bill.currentPaymentMethod != nil {
            list = voucherManger?.filterAvailableVouchers(whenSelectedPaymentMethod: list ?? [], for: self.bill)
        }
        return list ?? []
    }
    
    var isHaveVoucher: Bool {
        return listVoucher().count > 0
    }
    
    // Choose best voucher
    func getBestVoucher() -> ZPVoucherHistory?  {
        let list = self.listVoucher()
        guard  list.count > 0 else {
            return nil
        }
        let bestOffer = ZaloPayWalletSDKPayment.sharedInstance().zpVoucherManager.getBestVoucher(fromArrayVoucher: list, of: self.bill)
        return bestOffer
    }
    
    // Use voucher
    func usingVoucher(from voucherHistory: ZPVoucherHistory?) -> Observable<ZPVoucherInfo> {
        // tracking internet
        guard ZPDataManager.sharedInstance().checkInternetConnect() else {
            let error = NSError(domain: NSURLErrorDomain, code: NSURLErrorNotConnectedToInternet, userInfo: ["returnmessage": stringNetworkError])
            return Observable.error(error)
        }
        guard let voucherHistory = voucherHistory else {
            return Observable.empty()
        }
        let code = voucherHistory.vouchercode ?? ""
        return (~ZPPaymentManager.submitVoucher(with: self.bill, voucherHistory: voucherHistory))
            .map({
                let v = ZPVoucherInfo.fromDic(($0 as? [String: Any]) ?? [:])
                v.voucherCode = code
                v.voucherUsed = voucherHistory
                return v
            }).do(onNext: { [weak self](v) in
             // update for bill
                self?.bill.voucherInfo = v
            // Add to revert
                ZaloPayWalletSDKPayment.sharedInstance().zpVoucherManager.addRevert(voucher: v)
            }).debug("Take Voucher!!!!!!")
            .trackActivity(self.activityTracking)
    }
    
    func revertVouchers() {
        ZaloPayWalletSDKPayment.sharedInstance().zpVoucherManager.revertArrVoucherAndUse(voucher: self.bill.voucherInfo)
        ZaloPayWalletSDKPayment.sharedInstance().zpVoucherManager.clearVouchersCaches()
    }
}

// MARK: - Summit trans
extension ZPPaymentPopupInteractor {
    func submitTrans(using method: ZPPaymentMethod, encryptPassword: String) -> Observable<ZPPaymentResponse?> {
        // tracking internet
        guard ZPDataManager.sharedInstance().checkInternetConnect() else {
            let error = NSError(domain: NSURLErrorDomain, code: NSURLErrorNotConnectedToInternet, userInfo: ["returnmessage": stringNetworkError])
            return Observable.error(error)
        }
        let isWallet = method.methodType == .zaloPayWallet
        let paymentItem = PaymentItem(method, encryptPassword)
        return isWallet ? submitUsingWallet(using: paymentItem) : submitOthers(using: paymentItem)
    }
    
    func submitUsingWallet(using item:PaymentItem) -> Observable<ZPPaymentResponse?> {
        let signal = ZPWalletSubmitTransModel.submiTrans(withPassword: item.encryptPassword,
                                                         bill: self.bill,
                                                         channelId: item.method.channel.channelID)
        return (~signal).trackActivity(self.activityTracking).map({ $0 as? ZPPaymentResponse})
    }
    // Flow:
    // 1> wallet : -> summit
    // 2> card : -> create atm card -> otp (optional) -> update infor to atm -> submit -> check status
    func submitOthers(using item:PaymentItem) -> Observable<ZPPaymentResponse?> {
        guard ZPDataManager.sharedInstance().checkInternetConnect() else {
            let error = NSError(domain: NSURLErrorDomain, code: NSURLErrorNotConnectedToInternet, userInfo: ["returnmessage": stringNetworkError])
            return Observable.error(error)
        }
        func findBillDataProtocol() -> MakeBillDataProtocol? {
            switch item.method.methodType {
            case .savedCard:
                return item.method.savedCard
            case .savedAccount:
                return item.method.savedBankAccount
            case .ccDebit:
                return item.method.savedCard
            default:
                return nil
            }
        }
        
        guard let ePaymentItem = ExcutePaymentItem(with: item.method.methodType , bankCode: item.method.bankCode, amount: self.bill.amount, pin: item.encryptPassword),
            let card = ePaymentItem.card,
            let makeBill = findBillDataProtocol() else {
            return Observable.empty()
        }
        self.bill.data = makeBill.billData()?.toJsonString() ?? ""
        return excutePayment(with: item.method, from: card, methodType: card.method).flatMap({  [weak self](r) -> Observable<ZPPaymentResponse?> in
            guard let wSelf = self , let r = r else {
                return Observable.empty()
            }
            return wSelf.routerChecking(from: r, with: item.method, from: card, type: card.method)
        }).trackActivity(self.activityTracking)
    }
    
    func channelOf(method: ZPPaymentMethodType, bankCode: String, bill: ZPBill) -> ZPChannel? {
        // note: case rút tiền phải chuyển về kênh ví.
        let paymentMethod = bill.transType == .withDraw ? .zaloPayWallet : method
        return ZPDataManager.sharedInstance().channel(withTypeAndBankCode: paymentMethod, bankCode: bankCode)
    }

    func excutePayment(with method: ZPPaymentMethod, from cardInfor: ZPPaymentInfo, methodType: ZPPaymentMethodType) -> Observable<ZPPaymentResponse?> {
        guard ZPDataManager.sharedInstance().checkInternetConnect() else {
            let error = NSError(domain: NSURLErrorDomain, code: NSURLErrorNotConnectedToInternet, userInfo: ["returnmessage": stringNetworkError])
            return Observable.error(error)
        }
        let bill = self.bill
        let appId = self.bill.appId.toInt
        let channel = self.bill.currentPaymentMethod?.channel
        
        return Observable.create { (s) -> Disposable in
            ZPPaymentManager.payment(with: bill,
                                     andPaymentInfo: cardInfor, andAppId: appId, channel: channel, inMode: methodType)
            { (r) in
                s.onNext(r)
                s.onCompleted()
            }
            return Disposables.create()
        }
    }
    
    // Tracking response
    private func routerChecking(from response: ZPPaymentResponse, with method: ZPPaymentMethod, from card: ZPPaymentInfo, type: ZPPaymentMethodType) -> Observable<ZPPaymentResponse?> {
        let codeError = response.errorCode
        let isProcessing = response.isProcessing
        let data = response.data
        guard codeError > 0 else {
            response.exchangeStatus = Int32(ZPZaloPayCreditStatus.codeFail.rawValue)
            return Observable.just(response)
        }
        if let data = data , isProcessing {
            // open web
            let json = data.toJson() ?? [:]
            let path = json["redirecturl"] as? String
            // Wrap information
            let retryItem = RetryExcutePaymentItem(response: response,
                                                   paymentMethod: method,
                                                   card: card,
                                                   methodType: type,
                                                   path: path,
                                                   serverCode: codeError)
            return Observable.error(ZPPopupError.retry(item: retryItem))
        }
        return Observable.just(response)
    }
}

// MARK: - Extension
extension Int32 {
    var toInt: Int {
        return Int(self)
    }
}

extension Dictionary {
    func toJsonString(_ format: Bool = false) -> String? {
        let option: JSONSerialization.WritingOptions = format ? .prettyPrinted : []
        guard let data = try? JSONSerialization.data(withJSONObject: self, options: option) else {
            return nil
        }
        let result = String.init(data: data, encoding: .utf8)
        return result
    }
}

extension String {
    func toJson() -> [String: Any]? {
        guard let data = self.data(using: .utf8) else {
            return nil
        }
        let result = (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)) as? [String: Any]
        return result
    }
}

// MARK: -- RACSignal
fileprivate func asObservable<E>(_ signal: RACSignal<E>) -> Observable<E?> {
    return Observable.create({ (s) -> Disposable in
        let racDisposable = signal.subscribeNext({
            s.onNext($0)
        }, error: { (e) in
            s.onError(e ?? NSError())
        }, completed: {
            s.onCompleted()
        })
        return Disposables.create {
            racDisposable.dispose()
        }
    })
}

prefix operator ~
prefix func ~<E>(s: RACSignal<E>?) -> Observable<E?> {
    guard let s = s else { return Observable.empty() }
    return asObservable(s)
}

postfix operator ~
postfix func ~<E>(o:Observable<E>) -> RACSignal<E> {
    return RACSignal.createSignal({ (s) -> RACDisposable? in
        let disposeAble = o.subscribe(onNext: {
            s.sendNext($0)
        }, onError: {
            s.sendError($0)
        }, onCompleted: {
            s.sendCompleted()
        })
        return RACDisposable(block: {
            disposeAble.dispose()
        })
    })
}

private struct ActivityToken<E> : ObservableConvertibleType, Disposable {
    private let _source: Observable<E>
    private let _dispose: Cancelable
    
    init(source: Observable<E>, disposeAction: @escaping () -> ()) {
        _source = source
        _dispose = Disposables.create(with: disposeAction)
    }
    
    func dispose() {
        _dispose.dispose()
    }
    
    func asObservable() -> Observable<E> {
        return _source
    }
}

/**
 Enables monitoring of sequence computation.
 
 If there is at least one sequence computation in progress, `true` will be sent.
 When all activities complete `false` will be sent.
 */
public class ActivityIndicator : SharedSequenceConvertibleType {
    public typealias E = Bool
    public typealias SharingStrategy = DriverSharingStrategy
    
    private let _lock = NSRecursiveLock()
    private let _variable = Variable(0)
    private let _loading: SharedSequence<SharingStrategy, Bool>
    
    public init() {
        _loading = _variable.asDriver()
            .map { $0 > 0 }
            .distinctUntilChanged()
    }
    
    fileprivate func trackActivityOfObservable<O: ObservableConvertibleType>(_ source: O) -> Observable<O.E> {
        return Observable.using({ () -> ActivityToken<O.E> in
            self.increment()
            return ActivityToken(source: source.asObservable(), disposeAction: self.decrement)
        }) { t in
            return t.asObservable()
        }
    }
    
    private func increment() {
        _lock.lock()
        _variable.value = _variable.value + 1
        _lock.unlock()
    }
    
    private func decrement() {
        _lock.lock()
        _variable.value = _variable.value - 1
        _lock.unlock()
    }
    
    public func asSharedSequence() -> SharedSequence<SharingStrategy, E> {
        return _loading
    }
}

extension ObservableConvertibleType {
    public func trackActivity(_ activityIndicator: ActivityIndicator) -> Observable<E> {
        return activityIndicator.trackActivityOfObservable(self)
    }
}



