//
//  ZPPaymentPopupMainInteractor.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/20/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayWalletSDKPrivate
import RxSwift
import RxCocoa

class ZPPaymentPopupMainInteractor {
    private weak var presenter: ZPPaymentPopupMainPresenter?
    init(_ presenter: ZPPaymentPopupMainPresenter?) {
        self.presenter = presenter
    }
    
    func getBill() -> ZPBill {
        guard let rootPresenter = self.presenter?.mainController?.rootPresenter else {
            fatalError("Please check for this , wrong logic")
        }
        return rootPresenter.interactor.bill
    }
    
    var isHaveVoucher: Bool {
        return self.presenter?.mainController?.rootPresenter?.interactor.isHaveVoucher ?? false
    }
    
    func calculateFee() {
        guard let currentMethod = self.presenter?.currentMethod else {
            return
        }
        // Update with voucher
        let bill = self.getBill()
        // caculate again charge
        let amount: Int
        if let voucher = bill.voucherInfo {
            amount = bill.amount - voucher.discountAmount
        } else {
            amount = bill.amount
        }
        
        let bankCode = convertBankCode(from: currentMethod.method.methodType, bankCode: currentMethod.method.bankCode)
        let channel = self.presenter?.mainController?.rootPresenter?.interactor.channelOf(method: currentMethod.method.methodType, bankCode: bankCode, bill: bill)
        #if DEBUG
            assert(channel != nil, "Wrong Logic!!!")
        #endif
        bill.currentPaymentMethod?.channel = channel
        defer {
            self.presenter?.mainController?.rootPresenter?.interactor.trackChooseMethod(using: channel, method: currentMethod.method)
        }
        
        let v = currentMethod.method.calculateCharge(with: amount)
        let support = channel?.shouldSupportAmount(v) ?? false
        currentMethod.method.isTotalChargeGreaterThanMaxAmount = !support
        // wallet
        currentMethod.method.isTotalChargeGreaterThanBalance = false
        if currentMethod.method.methodType == .zaloPayWallet {
            let balance = ZaloPayWalletSDKPayment.sharedInstance().appDependence.currentBalance()
            let isTotalChargeGreaterThanBalance = currentMethod.method.totalChargeFee + amount > balance
            currentMethod.method.isTotalChargeGreaterThanBalance = isTotalChargeGreaterThanBalance
        }
    }
}
