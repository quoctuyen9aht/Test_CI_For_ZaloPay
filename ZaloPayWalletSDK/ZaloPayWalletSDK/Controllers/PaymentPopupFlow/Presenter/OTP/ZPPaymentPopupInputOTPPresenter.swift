//
//  ZPPaymentPopupInputOTPPresenter.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/27/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayWalletSDKPrivate
import RxSwift
import RxCocoa
import UIKit
import ZaloPayAnalyticsSwift
import ZaloPayCommonSwift
fileprivate let minCharacter = 6
class ZPPaymentPopupInputOTPPresenter: NSObject {
    weak var controller: ZPPaymentPopupInputOTPViewController?
    lazy var activityTracking: ActivityIndicator = ActivityIndicator()
    private (set) var canEditText: Bool = true
    private var currentInput: Variable<String> = Variable("")
    private let disposeBag = DisposeBag()
    private var textField: UITextField? {
        return self.controller?.textField
    }
    private var retryItem: RetryExcutePaymentItem!
    private lazy var interactor: ZPPaymentPopupInputOTPInteractor = ZPPaymentPopupInputOTPInteractor(item: retryItem, presenter: self)
    var rootPresenter: ZPPaymentPopupPresenter? {
        guard let parentVC = self.controller?.navigationController?.parent as? ZPPaymentPopupRootController else {
            return nil
        }
        return parentVC.presenter
        
    }
    private lazy var maxCharacter: Int = {
       let m = ZPDataManager.sharedInstance().double(forKey: "otp_max_length")
       return Int(m)
    }()
    
    convenience init(using controller: ZPPaymentPopupInputOTPViewController?, with item: RetryExcutePaymentItem?) {
        guard let item = item else {
            fatalError("Please check value")
        }
        
        self.init()
        self.retryItem = item
        self.controller = controller
        self.controller?.textField?.delegate = self
        self.configKeyboard()
    }
    
    private func configKeyboard() {
        let kValue = ZPResourceManager.otpKeyboardType(self.retryItem.paymentMethod.bankCode ?? "").toInt
        let type =  UIKeyboardType(rawValue: kValue) ?? .default
        self.textField?.keyboardType = type
    }
    
    func setupEvent() {
        NotificationCenter.default.rx
            .notification(Notification.Name.ZPDialogViewHide)
            .filter({[weak self] _ in self?.controller?.isAppear ?? false })
            .bind
        { [weak self](_) in
            self?.textField?.becomeFirstResponder()
        }.disposed(by: disposeBag)
        
        self.textField?.rx.text.map({ $0 ?? ""}).bind(to: currentInput).disposed(by: disposeBag)
        currentInput.asObservable().bind {
            print("\($0)")
        }.disposed(by: disposeBag)
        self.activityTracking.asDriver().drive(onNext: {[weak self] in
            self?.canEditText = !$0
            let spinnerView = self?.controller?.spinnerView
            $0 ? spinnerView?.startAnimating() : spinnerView?.stopAnimating()
        }).disposed(by: disposeBag)
        
        self.textField?.rx.controlEvent(.editingChanged).bind(onNext: { [weak self](_) in
            guard let textField = self?.textField else {
                return
            }
            let text = textField.text ?? ""
            let att = NSMutableAttributedString(string: text)
            att.addAttribute(NSAttributedStringKey.kern, value: 6.2, range: NSMakeRange(0, text.count))
            textField.attributedText += att
        }).disposed(by: disposeBag)
        currentInput.asDriver().map({ $0.count >= minCharacter }).drive(onNext: { [weak self] in
            self?.controller?.btnContinue?.isEnabled = $0
        }).disposed(by: disposeBag)
        self.controller?.btnContinue?.rx.controlEvent(.touchUpInside).filter({ [weak self]_ in self?.canEditText ?? false}).bind {[weak self] in
            guard let wSelf = self else {
                return
            }
            let otp = wSelf.currentInput.value
            wSelf.updatePayment(with: otp)
        }.disposed(by: disposeBag)
        
//        currentInput
//            .asObservable()
//            .filter({[weak self] in $0.count == 6 && self?.textField?.isFirstResponder == true })
//            .debounce(0.3, scheduler: MainScheduler.instance).subscribe(onNext: { [weak self] in
//                self?.updatePayment(with: $0)
//            }).disposed(by: disposeBag)
    }
    
    private func updatePayment(with otp:String) {
        ZPTrackingHelper.shared().trackEvent(.popupsdk_otp_resend)
        self.controller?.lblError?.isHidden = true
        self.interactor.retryPayment(with: otp).trackActivity(self.activityTracking).map { (r) -> ZPPaymentResponse in
            guard let r = r else {
                let e = NSError(domain: NSURLErrorDomain, code: NSURLErrorBadServerResponse, userInfo: ["returnmessage": "Can't find value"])
                throw e
            }
            return r
            }.observeOn(MainScheduler.instance).subscribe(onNext: { [weak self] in
                self?.handler(from: $0, method: self?.interactor.item.paymentMethod)
            }, onError: { (e) in
                ZPDialogView.showDialogWithError(e, handle: nil)
            }).disposed(by: disposeBag)
    }
    
    private func handler(from response: ZPPaymentResponse, method: ZPPaymentMethod?) {
        let eNext = rootPresenter?.rootVC?.eResponse
        
        switch response.errorCode.toInt {
        case 1:
            eNext?.onNext(response)
            eNext?.onCompleted()
        case ZPServerErrorCode.atmRetryOtp.rawValue where response.isProcessing:
            ZPTrackingHelper.shared().trackEvent(.popupsdk_otp_fail)
            self.controller?.textField?.text = ""
            self.controller?.textField?.sendActions(for: .valueChanged)
            self.controller?.lblError?.text = response.message
            self.controller?.lblError?.isHidden = false
            self.controller?.btnContinue?.isEnabled = false
        default:
            let e = ZPPopupError.result(response: response, method: method)
            eNext?.onError(e)
        }
    }
    
    func cancelPayment() {
        guard canEditText else {
            return
        }
        // Cancel
        self.rootPresenter?.cancelPayment()
    }
}

// MARK: - TextField
extension ZPPaymentPopupInputOTPPresenter: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard canEditText else { return false }
        guard let t = textField.text else {
            return true
        }
        let nStr = (t as NSString).replacingCharacters(in: range, with: string)
        let next = nStr.count <= maxCharacter
        return next
    }
}
