//
//  ZPPaymentPopupResultPresenter.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/22/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayWalletSDKPrivate

enum DisplayResultItemType: Int  {
    case none
    case top
    case description
    case support
    case close
}

struct DisplayResultItem {
    let type: DisplayResultItemType
    let title: String?
    let description: String?
}

typealias DataTypeDisplay = [DisplayResultItem]

func +=<K, V>(lhs: inout [K: V], rhs: [K: V]) {
    rhs.forEach {
        lhs[$0.key] = $0.value
    }
}

class ZPPaymentPopupResultPresenter {
    let interactor: ZPPaymentPopupResultInteractor
    var source: [Int: DataTypeDisplay] = [:]
    var cellSource: [ZPPaymentPopupResultBaseCell.Type] = []
    lazy var router = ZPPaymentPopupResultRouter(with: self)
    weak var controller: ZPPaymentPopupResultTableViewController?
    init(with displayItem: ItemResult, on vc: ZPPaymentPopupResultTableViewController) {
        self.interactor = ZPPaymentPopupResultInteractor(using: displayItem)
        self.controller = vc
    }
    
    func loadSource() {
        self.interactor.generateData().enumerated().map({
            [$0.offset: $0.element]
        }).forEach {
            source += $0
        }
        
        cellSource = [ZPPaymentPopupResultDescriptionTopCell.self,
                      ZPPaymentPopupResultDescriptionCell.self,
                      ZPPaymentPopupResultSupportCell.self,
                      ZPPaymentPopupResultBottomCell.self]
        
        controller?.tableView.reloadData()
    }
    
    func handlerSelect(at type: DisplayResultItemType) {
        switch type {
        case .support:
            print("support")
            router.showActionSheet()
        default:
            break
        }
    }
    
    
}
