//
//  ZPPaymentPopupPasswordPresenter.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/8/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ZaloPayWalletSDKPrivate
import UIKit
import ZaloPayAnalyticsSwift

class ZPPaymentPopupPasswordPresenter: NSObject {
    fileprivate var currentInput: Variable<String> = Variable("")
    lazy var activityTracking: ActivityIndicator = ActivityIndicator()
    weak var controller: ZPPaymentPopupPasswordViewController?
    private (set) var canEditText: Bool = true
    private let disposeBag = DisposeBag()
    var rootPresenter: ZPPaymentPopupPresenter? {
        guard let parentVC = self.controller?.navigationController?.parent as? ZPPaymentPopupRootController else {
            return nil
        }
        return parentVC.presenter
        
    }
    private lazy var router: ZPPaymentPopupPasswordRouter = ZPPaymentPopupPasswordRouter(with: self)
    convenience init(with controller: ZPPaymentPopupPasswordViewController?) {
        self.init()
        self.controller = controller
    }
    
    func setupEvent() {
        // text field
        NotificationCenter.default.rx
            .notification(Notification.Name.ZPDialogViewHide)
            .filter({[weak self] _ in self?.controller?.isAppear ?? false })
            .bind
        { [weak self](_) in
            self?.controller?.textField.becomeFirstResponder()
        }.disposed(by: disposeBag)
        
        
        self.controller?.textField.rx.text.map({ $0 ?? "" }).bind(to: currentInput).disposed(by: disposeBag)
        
        // loading
        activityTracking.drive(onNext: { [weak self] in
            self?.canEditText = !$0
            let spinnerView = self?.controller?.spinnerView
            $0 ? spinnerView?.startAnimating() : spinnerView?.stopAnimating()
        }).disposed(by: disposeBag)
        
        currentInput.asDriver().drive(onNext: { [weak self] in
            let total = $0.count
            (0..<6).map({ IndexPath(item: $0, section: 0) }).forEach({
                guard let cell = self?.controller?.collectionView?.cellForItem(at: $0) as? ZPPaymentPopupPasswordCollectionViewCell else {
                    return
                }
                cell.isSelected = $0.row < total
            })
        }).disposed(by: disposeBag)
        currentInput
            .asObservable().distinctUntilChanged()
            .filter({[weak self] in $0.count == 6 && self?.controller?.textField.isFirstResponder == true })
            .debounce(0.3, scheduler: MainScheduler.instance).subscribe(onNext: { [weak self] in
                self?.submitTrans(using: $0)
            }).disposed(by: disposeBag)
        self.controller?.textField.rx.controlEvent(.editingDidBegin).bind { [weak self] in
            self?.controller?.lblError?.isHidden = true
        }.disposed(by: disposeBag)
    }
    
    private func submitTrans(using pass: String) {
        // Save pass
        guard let method = self.controller?.method, let rPresenter = self.rootPresenter else {
            return
        }
        self.controller?.lblNote?.isHidden = true
        self.controller?.lblError?.isHidden = true
        let ePassword = (pass as NSString).zpSha256() ?? ""
        rPresenter.interactor.currentPassword = ePassword
        rPresenter.submitTrans(with: method, encryptPassword: ePassword).trackActivity(self.activityTracking).debug("Test 11111").subscribe(onNext: { [weak self](v) in
            self?.handlerSuccess(from: v, method: method)
        }, onError: { [weak self](e) in
            self?.handlerError(from: e)
        }).disposed(by: disposeBag)
    }
    
    private func handlerError(from e:Error) {
        // Track error url
        if let error = e as? ZPPopupError, case .retry(let response) = error{
            // handler OTP
            self.handlerRetry(from: response)
            return
        }
        let code = (e as NSError).code
        if code == NSURLErrorCancelled {
            // Cancel
            self.rootPresenter?.isProccessing = false
            self.rootPresenter?.cancelPayment()
            return
        }
        ZPDialogView.showDialogWithError(e, handle: nil)
    }
    
    private func handlerSuccess(from v: AnyObject?, method: ZPPaymentMethod) {
        let eNext = self.rootPresenter?.rootVC?.eResponse
        guard let response = v as? ZPPaymentResponse else {
            return
        }
        
        switch response.errorCode.toInt {
        case 1:
            eNext?.onNext(response)
            eNext?.onCompleted()
        case ZPZaloPayCreditError.kZPZaloPayCreditErrorCodePinRetry.rawValue:
            ZPTrackingHelper.shared().trackEvent(.popupsdk_password_fail)
            self.controller?.textField.text = ""
            self.controller?.textField.sendActions(for: .valueChanged)
            self.controller?.lblError?.text = response.message
            self.controller?.lblError?.isHidden = false
        default:
            let e = ZPPopupError.result(response: response, method: method)
            eNext?.onError(e)
        }
    }
    
    private func handlerRetry(from item: RetryExcutePaymentItem) {
        action: if item.response.actionType == ZPBankActionType.web.rawValue {
            // move to process url
            guard let path = item.path else {
                return
            }
            rootPresenter?.excutePaymentByWeb(with: path, item: item)
                .trackActivity(self.activityTracking)
                .subscribe(onNext: { [weak self](v) in
                self?.handlerSuccess(from: v, method: item.paymentMethod)
                }, onError: { [weak self](e) in
                    self?.handlerError(from: e)
            }).disposed(by: disposeBag)
            return
        }
        // move to input otp
        router.inputOTP(from: item)
    }
}

// MARK: - TextField
extension ZPPaymentPopupPasswordPresenter: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard canEditText else { return false }
        guard let t = textField.text else {
            return true
        }
        
        let nStr = (t as NSString).replacingCharacters(in: range, with: string)
        let next = nStr.count <= 6
        return next
    }
}
