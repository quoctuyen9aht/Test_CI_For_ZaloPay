//
//  ZPPaymentPopupListMethodsPresenter.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/13/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ZaloPayWalletSDKPrivate

class ZPPaymentPopupListMethodsPresenter {
    weak var controller: ZPPaymentPopupChooseMethodViewController?
    private lazy var interactor = ZPPaymentPopupListMethodsInteractor(using: self)
    private var sourceRoot: Observable<[ZPPaymentMethodCellDisplay]> {
        return self.interactor.eSource
    }
    private let dataDisplay: Variable<[DisplayType]> = Variable([])
    var displayMethods: Observable<[DisplayType]> {
        return dataDisplay.asObservable().filter({ $0.count > 0 })
    }
    private let disposeBag = DisposeBag()
    
    init(with controller: ZPPaymentPopupChooseMethodViewController?) {
        self.controller = controller
        setupEvent()
    }
    
    private func setupEvent() {
        sourceRoot.observeOn(MainScheduler.instance).bind { [weak self](displayMethods) in
            var methods = displayMethods.map({ DisplayType(type: .none, infor: $0, voucher: nil, amount: 0) })
            let isHaveMethod = displayMethods.filter({ $0.method.isEnable() }).count > 0
            // Need To check not have any method appreciate
            if !isHaveMethod {
                // Add new
                let new = DisplayType(type: .new, infor: ZPPaymentMethodCellDisplay(), voucher: nil, amount: 0)
//                methods.append(new)
                methods.insert(new, at: 0)
            }
            self?.dataDisplay.value = methods
        }.disposed(by: disposeBag)
        self.dataDisplay.asDriver().map({ $0.count > 1 }).drive(onNext: { [weak self] in
            self?.controller?.tableView?.isScrollEnabled = $0
        }).disposed(by: disposeBag)
    }
    
    func item(at idx: IndexPath) -> ZPPaymentMethodCellDisplay {
        return self.dataDisplay.value[idx.item].infor
    }
    
    func findDefaultSelect(from select: ZPPaymentMethodCellDisplay?) -> IndexPath? {
        guard let select = select else {
            return nil
        }
        
        let methods = dataDisplay.value.map({ $0.infor })
        guard let idx = methods.index(of: select) else { return nil }
        return IndexPath(item: idx, section: 0)
    }
}
