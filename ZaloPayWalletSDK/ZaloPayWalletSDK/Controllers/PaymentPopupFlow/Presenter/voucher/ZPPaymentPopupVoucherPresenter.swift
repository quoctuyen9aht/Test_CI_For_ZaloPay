//
//  ZPPaymentPopupVoucherPresenter.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/14/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift
import ZaloPayWalletSDKPrivate

class ZPPaymentPopupVoucherPresenter {
    weak var controller: ZPPaymentPopupVoucherViewController?
    private lazy var interactor: ZPPaymentPopupVoucherInteractor = ZPPaymentPopupVoucherInteractor(with: self)
    var dataSource: Observable<[ZPVoucherHistory]> {
        return interactor.listVoucher.asObservable()
    }
    
    var list: [ZPVoucherHistory] {
        return interactor.listVoucher.value
    }
    
    var bill: ZPBill {
        return self.interactor.getBill()
    }
    init(_ controller: ZPPaymentPopupVoucherViewController?) {
        self.controller = controller
    }
    func loadResource() {
        interactor.loadSource()
    }
    
    func useVoucher(from voucherHistory: ZPVoucherHistory) -> Observable<ZPVoucherInfo> {
        return interactor.useVoucher(from: voucherHistory)
    }
}
