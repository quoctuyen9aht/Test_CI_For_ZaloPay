//
//  ZPPaymentPopupPresenter.swift
//  ZaloPay
//
//  Created by Dung Vu on 12/4/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ZaloPayWalletSDKPrivate
import ZaloPayAnalytics

enum ZPPaymentPopupState: Int {
    case main = 0
    case listMethod
    case voucher
    case password
    case otp
    case result
    
    var isCanCancel: Bool {
        switch self {
        case .otp:
            return false
        case .listMethod:
            return false
        case .voucher:
            return false
        default:
            return true
        }
        
    }
    
}

protocol ZPPaymentPopupAdjustDisplayProtocol {
    var eState: Observable<ZPPaymentPopupState> { get }
    var eChangeHeight: Observable<CGFloat> { get }
}

class ZPPaymentPopupPresenter: NSObject {
    private (set) var interactor: ZPPaymentPopupInteractor!
    var statePayment: Variable<ZPPaymentPopupState> = Variable(.main)
    let sourceDisplay: Variable<[ZPPaymentMethodCellDisplay]> =  Variable([])
    weak var rootVC: ZPPaymentPopupRootController?
    var isProccessing: Bool = false
    private let disposeBag = DisposeBag()
    private lazy var router: ZPPaymentPopupRootRouter = ZPPaymentPopupRootRouter(using: self)
    private override init() {
        super.init()
    }
    
    convenience init(with bill: ZPBill) {
        self.init()
        self.interactor = ZPPaymentPopupInteractor(with: bill)
        self.setupEventSource()
    }
    
    private func setupEventSource() {
        // Transform to display
        self.interactor.eMethods.map({[weak self] in
            $0.filter({ !$0.defaultChannel }).map({ m -> ZPPaymentMethodCellDisplay in
                let amount = self?.interactor.finalAmount() ?? 0
                return ZPPaymentMethodCellDisplay(m, withAmount: amount)
            })
        }).drive(sourceDisplay).disposed(by: disposeBag)
        self.interactor.activityTracking.asDriver().drive(onNext: { [weak self] in
            self?.isProccessing = $0
            if self?.statePayment.value == .main {
                $0 ? ZPProgressHUD.show(withStatus: nil) :  ZPProgressHUD.dismiss()
            }
        }).disposed(by: disposeBag)
    }
    
    func loadMethods() {
        self.interactor.loadMethods()
    }
    
    func isUsingWallet() -> Bool {
        return self.interactor.isUsingWallet
    }
    
    func revertVouchers() {
        self.interactor.revertVouchers()
    }
    
    func writeLog() {
        self.interactor.writeLog()
    }
    
    func linkNewCard() {
        router.linkNewCard()
    }

    func updateNewMethodForBill(from displayMethod: ZPPaymentMethodCellDisplay?) {
        self.interactor.updateBill(from: displayMethod?.method)
    }
    
    func checkingPass(with response: AnyObject?) {
        var infor : [String: String] = [:]
        if let response = response as? ZPPaymentResponse {
            let zptransid = response.zpTransID
            let apptransid = self.interactor.bill.appTransID
            let result = response.errorCode
            infor["zptransid"] = "\(zptransid ?? "")"
            infor["apptransid"] = "\(apptransid ?? "")"
            infor["errorcode"] = "\(result)"
        }
        let result = NSDictionary.init(dictionary: infor)
        
        guard let pass = interactor.currentPassword, 
            !pass.isEmpty,
            ZaloPayWalletSDKPayment.sharedInstance().appDependence.isTouchIDExist(),
            ZaloPayWalletSDKPayment.sharedInstance().appDependence.isEnableTouchID() else
        {
            rootVC?.eResult.onNext(result)
            rootVC?.eResult.onCompleted()
            return
        }
        
        guard !ZaloPayWalletSDKPayment.sharedInstance().appDependence.usingTouchId() else {
            ZaloPayWalletSDKPayment.sharedInstance().appDependence.savePassword(pass)
            rootVC?.eResult.onNext(result)
            rootVC?.eResult.onCompleted()
            return
        }
        
        let message = R.string_Confirm_TouchId_Payment_Later()!
        let msg = ZaloPayWalletSDKPayment.sharedInstance().appDependence.check(toReplaceFaceIDMessage: message)
        ZPDialogView.showDialog(with: DialogTypeInfo,
                                title: nil,
                                message: msg , buttonTitles: ["Đồng ý","Để sau"])
        { [weak self](idx) in
            defer {
                self?.rootVC?.eResult.onNext(result)
                self?.rootVC?.eResult.onCompleted()
            }
            guard idx == 0 else {
                ZPTrackingHelper.shared().trackEvent(.popupsdk_auth_cancel)
                return
            }
            ZPTrackingHelper.shared().trackEvent(.popupsdk_auth_confirm)
            ZaloPayWalletSDKPayment.sharedInstance().appDependence.savePassword(pass)
            ZaloPayWalletSDKPayment.sharedInstance().appDependence.setUsingTouchId(true)
            self?.showToast()
        }
    }
    
    private func showToast() {
        let message = ZaloPayWalletSDKPayment.sharedInstance().appDependence.check(toReplaceFaceIDMessage: R.string_TouchID_Enable_Success_Message())
        let controller = self.rootVC?.navigationController?.presentingViewController
        #if DEBUG
            assert(controller != nil, "wrong!!!")
        #endif
        ZaloPayWalletSDKPayment.sharedInstance().appDependence.showToast(from: controller, message: message, delay: 0.4)
    }
    
    func cancelPayment() {
        guard !isProccessing else {
            return
        }
        self.interactor.updateBill(from: nil)
        self.interactor.trackUserCancel()
        rootVC?.eResponse.onError(ZPPopupError.cancel)
    }
    
    func excutePaymentByWeb(with path: String, item: RetryExcutePaymentItem) -> Observable<ZPPaymentResponse?> {
        return router.openWeb(from: path).map({ next -> Bool in
            guard next else {
                let error = NSError(domain: NSURLErrorDomain, code: NSURLErrorCancelled, userInfo: ["returnmessage": "Huỷ nhập OTP"])
                throw error
            }
            return next
        }).flatMap({ [weak self] (_) -> Observable<ZPPaymentResponse?> in
            guard let wSelf = self else {
                return Observable.empty()
            }
            /// Track this
            let card = item.card
            card.step = ZPAtmPaymentStep.atmPaymentStepGetTransStatus.rawValue
            // credit card using other step
            if card is ZPCreditCard {
                card.step = ZPCreditCardPaymentStep.ccPaymentStepUpdate.rawValue
            }
            card.zpTransID = item.response.zpTransID
            return wSelf.interactor.excutePayment(with: item.paymentMethod, from: card, methodType: item.methodType)
        }).trackActivity(self.interactor.activityTracking)
    }
    
    func updateHeightContainer(with h: CGFloat) {
        let hScreen = UIScreen.main.bounds.height
        UIView.animate(withDuration: 0.2) {
            self.rootVC?.hContainerView?.constant = min(hScreen, h)
            self.rootVC?.view.layoutIfNeeded()
        }
    }
    
    func submitTrans(with method: ZPPaymentMethod, encryptPassword: String) -> Observable<ZPPaymentResponse?> {
        return self.interactor.submitTrans(using:method , encryptPassword: encryptPassword).observeOn(MainScheduler.instance)
    }
    
}

extension ZPPaymentPopupPresenter: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        guard let viewAdjust = viewController as? ZPPaymentPopupAdjustDisplayProtocol else {
            return
        }
        let delta: CGFloat = UIScreen.main.bounds.height >= 812 ? 10 : 0
        viewAdjust.eChangeHeight.map({ $0 + delta })
            .observeOn(MainScheduler.instance)
            .takeUntil(viewController.rx.deallocating)
            .filter({ $0 > delta }).bind
            { [weak self](nH) in
            //Update
            self?.updateHeightContainer(with: nH)
        }.disposed(by: disposeBag)
        
        // Update state view
        viewAdjust.eState
            .takeUntil(viewController.rx.deallocating)
            .bind(to: self.statePayment).disposed(by: disposeBag)
        
        
    }
}
