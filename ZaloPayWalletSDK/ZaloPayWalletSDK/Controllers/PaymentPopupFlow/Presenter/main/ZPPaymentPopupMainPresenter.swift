//
//  ZPPaymentPopupMainPresenter.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/7/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ZaloPayWalletSDKPrivate
import SnapKit
import CoreTelephony
import AppsFlyerLib

// Prepare for select
enum ZPPaymentPopupMainType: Int {
    case none
    case new
    case method
    case voucher
    case password
    case otp
    case total
    
    var segueName: String {
        switch self {
        case .method:
            return "showListMethods"
        case .voucher:
            return "showVoucher"
        case .password:
            return "showPassword"
        case .otp:
            return "showOTP"
        default:
            return ""
        }
    }
}

class ZPPaymentPopupMainPresenter {
    let eHeight: Variable<CGFloat> = Variable(0)
    weak var mainController: ZPPaymentPopupMainViewController?
    fileprivate (set) var source: [DisplayType] = [] {
        didSet {
            mainController?.tableView.reloadData()
        }
    }
    fileprivate let kHeightItem = 80
    fileprivate (set) var isCanPayment: Bool = true
    fileprivate (set) var cellSource: [ZPPaymentPopupMainBaseCell.Type] = []
    fileprivate let disposeBag = DisposeBag()
    fileprivate var currentSelectMethod: Int = 0
    fileprivate (set) var currentMethod: ZPPaymentMethodCellDisplay? {
        didSet {
            self.mainController?.rootPresenter?.updateNewMethodForBill(from: currentMethod)
        }
    }
    fileprivate lazy var router = ZPPaymentPopupMainRouter(presenter: self)
    fileprivate lazy var header: ZPOrderHeaderView? = {
        let nHeader = ZPOrderHeaderView(usingInPopupSDK: 0)
        self.setupBackButton(from: nHeader)
        return nHeader
    }()
    fileprivate lazy var eCancel: PublishSubject<Void> = PublishSubject()
    fileprivate (set) lazy var interactor: ZPPaymentPopupMainInteractor = ZPPaymentPopupMainInteractor(self)
    
    private (set) var isOneMethod: Bool = false
    private lazy var callCenter = CTCallCenter()
    private var isPaymentByBiometric: Bool = false
    private (set) var isCallComing: Bool = false
    
    init(_ mainController: ZPPaymentPopupMainViewController?) {
        self.mainController = mainController
        setupEvent()
    }
    private func setupEvent() {
        guard let rootPresenter = self.mainController?.rootPresenter else {
            return
        }
        self.eCancel.subscribe(onNext: { [weak rootPresenter](_) in
            rootPresenter?.cancelPayment()
        }).disposed(by: disposeBag)
        
        rootPresenter.sourceDisplay.asDriver().skip(1).drive(onNext: { [weak self] in
            self?.setupSource(from: $0)
        }).disposed(by: disposeBag)
        
        rootPresenter.rootVC?.eResponse.subscribe(onNext: { [weak self](_) in
            self?.mainController?.tableView.visibleCells.compactMap({ $0 as? ZPPaymentPopupMainTotalCell}).first?.getButton()?.isHidden = true
        }).disposed(by: disposeBag)
        
        rootPresenter.rootVC?.eResponse.flatMap({ [weak self](v) -> Observable<AnyObject?> in
            guard let wSelf = self else {
                return Observable.empty()
            }
            wSelf.mainController?.popMe()
            return Observable<Int>.just(0).delay(0.5, scheduler: MainScheduler.instance).flatMap({ (_) -> Observable<AnyObject?>  in
                wSelf.showAnimationResult().map({ _ in v })
            })
        }).subscribe(onNext: { [weak rootPresenter](s) in
            // Check Pass
            rootPresenter?.rootVC?.presenter.checkingPass(with: s)
            // Update balance
            let transType = rootPresenter?.rootVC?.presenter.interactor.bill.transType
            // Track if withdraw or add cash
            let needUpdateBalance = transType == .withDraw || transType == .walletTopup
            let response = s as? ZPPaymentResponse
            
            // Add track for appsflyer
            let returnValue = response?.appsFlyerValue() ?? ""
            ZaloPayWalletSDKPayment.sharedInstance().appDependence?.trackAppsflyer("zp_payment",eventValues: ["zp_returncode": returnValue])
            
            guard needUpdateBalance || rootPresenter?.rootVC?.presenter.isUsingWallet() == true else {
                return
            }
            ZaloPayWalletSDKPayment.sharedInstance().appDependence.update(withNewBalance: response?.balance ?? 0)
            }).disposed(by: disposeBag)
        
        // Add check if it has any call
        callCenter.callEventHandler = { [weak self] call -> () in
            guard let wSelf = self else {
                return
            }
            let state = call.callState
            guard state != CTCallStateIncoming else {
                wSelf.isCallComing = true
                return
            }
            
            // check if it 's authenticate by biometric and excuting payment
            let needRunPayment = wSelf.mainController?.isAppear == true && wSelf.isPaymentByBiometric
            wSelf.isCallComing = false
            guard state == CTCallStateDisconnected && needRunPayment else {
                return
            }
            wSelf.paymentByTouchid()
            
        }
        // Use for Test
//        let bestOffer = rootPresenter.interactor.getBestVoucher()
//        rootPresenter.interactor.usingVoucher(from: bestOffer).observeOn(MainScheduler.instance).subscribe(onNext: { [weak self](v) in
//            self?.updateWithVoucher()
//        }).disposed(by: disposeBag)
        
    }
    
    func updateWithVoucher() {
        guard let currentMethod = self.currentMethod else {
            return
        }
        interactor.calculateFee()
        self.updateUI(from: currentMethod)
    }
    
    private func showAnimationResult() -> Observable<Void>{
        let viewAnimate = self.mainController?.tableView.visibleCells.compactMap({ $0 as? AnimateViewResultProtocol }).first
        return viewAnimate?.showAnimate(with: true) ?? Observable.empty()
    }
    
    private func setupBackButton(from view:UIView?) {
        let btnBack = UIButton(type: .custom)
        btnBack.frame = CGRect(origin: .zero, size: CGSize(width: 38, height: 38))
        btnBack.setIconFont("red_delete", for: .normal)
        btnBack.titleLabel?.font = UIFont.iconFont(withSize: 16)
        btnBack.setTitleColor(UIColor.subText(), for: .normal)
       
        view?.addSubview(btnBack)
        btnBack.snp.makeConstraints { (m) in
            m.top.equalToSuperview()
            m.left.equalToSuperview()
            m.size.equalTo(btnBack.frame.size)
        }
        btnBack.rx.tap.bind(to: eCancel).disposed(by: disposeBag)
    }
    
    
    private func setupSource(from cellDisplays: [ZPPaymentMethodCellDisplay]) {
        // Take first
        currentSelectMethod = 0
        self.currentMethod = nil
        self.isOneMethod = false
        guard cellDisplays.count > 0 else {
            return
        }
        
        let itemNext: ZPPaymentMethodCellDisplay
        let canPayment: Bool
        defer {
           setupDisplay(from: itemNext)
        }
        let items = cellDisplays.filter({ $0.method.isEnable() })
        self.isOneMethod = items.count == 1 && cellDisplays.count == 1
        // Check wallet zalopay, if it can use -> use first , if not use other
        guard let item = cellDisplays.first(where: { $0.method.methodType == .zaloPayWallet && $0.method.isEnable() }) ??   cellDisplays.first(where: { $0.method.isEnable() }) else {
            // No method
            itemNext = cellDisplays[0]
            return
        }
        itemNext = item
    }
    func setupDisplay(from item: ZPPaymentMethodCellDisplay) {
        if item.isNewMethod {
            addNewCard() 
            return
        }
        guard self.currentMethod != item else {
            return
        }
        isCanPayment = item.method.isEnable()
        self.mainController?.tableView.tableHeaderView = header
        self.currentMethod = item
        self.interactor.calculateFee()
        self.updateUI(from: item)
    }
    
    private func addNewCard() {
        self.mainController?.rootPresenter?.linkNewCard()
    }
    
    private func updateUI(from item: ZPPaymentMethodCellDisplay) {
        let isHaveVoucher = interactor.isHaveVoucher
        let bill = interactor.getBill()
        let voucher = bill.voucherInfo
        let amount = bill.amount
        let item1 = DisplayType(type: .method, infor: item, voucher: voucher, amount: amount)
        let item2 = DisplayType(type: .total, infor: item, voucher: voucher, amount: amount)
        let totalCharge: Int = item.method.totalChargeAmount
        if isHaveVoucher {
            cellSource = [ZPPaymentPopupMainAccountCell.self, ZPPaymentPopupMainVoucherCell.self,  ZPPaymentPopupMainTotalCell.self]
            let item3 = DisplayType(type: .voucher, infor: item, voucher: voucher, amount: amount)
            source = [item1, item3, item2]
        }else {
            self.header?.updateAmountTitle(item.method.totalChargeAmount)
            cellSource = [ZPPaymentPopupMainAccountCell.self, ZPPaymentPopupMainTotalCell.self]
            source = [item1, item2]
        }
        
        self.header?.updateAmountTitle(totalCharge)
        var newH: CGFloat = CGFloat(source.count * kHeightItem) + (header?.frame.height ?? 0)
        if #available(iOS 11.0, *) {
            let parentVC = self.mainController?.parent
            let bottomPadding = parentVC?.view.safeAreaInsets.bottom ?? 0
            newH += max(bottomPadding, 0)
        }
        eHeight.value = newH
    }
    
    func loadSource() {
        mainController?.rootPresenter?.loadMethods()
    }
    
    func handlerSelect(with type: ZPPaymentPopupMainType) {
        switch type {
        case .total:
            break
        case .method:
            // Ignore case only has 1 channel
            guard !self.isOneMethod else {
                return
            }
            router.pushTo(segue: type.segueName, sender: self.currentMethod)
        case .voucher:
            router.pushTo(segue: type.segueName, sender: nil)
        default:
            break
        }
    }
    
    // MARK: - Payment
    // Step 1: - check password
    func excutePayment() {
        var canPayment: Bool = false
        self.router.runKYCFlow().debug().filter({ $0 }).subscribe(onNext: { [weak self](_) in
            self?.mainController?.rootPresenter?.rootVC?.popMe(animated: false)
           canPayment = true
        }, onError: { [weak self](e) in
                guard (e as NSError).code == NSURLErrorCancelled else {
                    ZPDialogView.showDialogWithError(e, handle: nil)
                    return
                }
                self?.mainController?.rootPresenter?.rootVC?.popMe()
        }, onCompleted:{ [weak self] in
            guard canPayment else {
                return
            }
            let usingTouchId = ZaloPayWalletSDKPayment.sharedInstance().appDependence.phoneIsSupportTouchId()
            if usingTouchId {
                // By pass -> excute next step at here
                self?.paymentByTouchid()
            } else {
                // will move input password
                self?.router.findPassword()
            }
        }).disposed(by: disposeBag)
    }
    
    private func paymentByTouchid() {
        self.isPaymentByBiometric = true
        router.authenByTouchId().do(onCompleted: { [weak self] in
            // Check case if it doesn't have any coming call , reset flag check paymentByBiometric
            guard self?.isCallComing == false else {
                return
            }
            self?.isPaymentByBiometric = false
        }).flatMap({ [weak self](s) -> Observable<ZPPaymentResponse?> in
            guard let p = self?.mainController?.rootPresenter, let m = self?.currentMethod?.method else {
                return Observable.empty()
            }
            return p.submitTrans(with: m, encryptPassword: s)
        }).map { (r) -> ZPPaymentResponse? in
            guard let response = r else {
                let e = NSError(domain: NSURLErrorDomain, code: NSURLErrorUnknown, userInfo: [NSLocalizedDescriptionKey : "Can't find result"])
                throw e
            }
            let code = response.errorCode
            switch code {
            case 1:
                return r
            default:
                throw ZPPopupError.result(response: response, method: self.currentMethod?.method)
            }
            }.observeOn(MainScheduler.instance).subscribe(onNext: { [weak self](r) in
                self?.mainController?.rootPresenter?.rootVC?.eResponse.onNext(r)
                }, onError: { [weak self](e) in
                    // check code show pin
                    let code = (e as NSError).code
                    guard code != NSURLErrorDataNotAllowed else {
                        self?.router.findPassword()
                        return
                    }
                    
                    guard let error = e as? ZPPopupError else {
                        ZPDialogView.showDialogWithError(e, handle: nil)
                        return
                    }
                    
                    // handler OTP, captcha...
                    if case .retry(let response) = error {
                        self?.handlerRetry(from: response)
                    } else {
                        self?.mainController?.rootPresenter?.rootVC?.eResponse.onError(e)
                    }
            }).disposed(by: disposeBag)
    }
    
    private func handlerRetry(from item: RetryExcutePaymentItem) {
        action: if item.response.actionType == ZPBankActionType.web.rawValue {
            guard let path = item.path else {
                return
            }
            // move to process url
            self.mainController?.rootPresenter?.excutePaymentByWeb(with: path, item: item).map({ r -> ZPPaymentResponse in
                guard let response = r else {
                    let e = NSError(domain: NSURLErrorDomain, code: NSURLErrorUnknown, userInfo: [NSLocalizedDescriptionKey : "Can't find result"])
                    throw e
                }
                return response
            }).subscribe(onNext: { [weak self] (response) in
                let code = response.errorCode
                switch code {
                case 1:
                    self?.mainController?.rootPresenter?.rootVC?.eResponse.onNext(response)
                default:
                    self?.mainController?.rootPresenter?.rootVC?.eResponse.onError(ZPPopupError.result(response: response, method: item.paymentMethod))
                }
            }, onError: { [weak self](e) in
                let code = (e as NSError).code
                if code == NSURLErrorCancelled {
                    // Cancel
                    self?.mainController?.rootPresenter?.isProccessing = false
                    self?.mainController?.rootPresenter?.cancelPayment()
                    return
                }
                ZPDialogView.showDialogWithError(e, handle: nil)
            }).disposed(by: disposeBag)
            
            return
        }
        // move to input otp
        router.inputOTP(from: item)
    }
}
