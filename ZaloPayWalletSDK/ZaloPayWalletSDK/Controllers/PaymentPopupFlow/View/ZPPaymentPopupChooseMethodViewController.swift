//
//  ZPPaymentPopupChooseChannelViewController.swift
//  ZaloPay
//
//  Created by Dung Vu on 12/4/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import ZaloPayWalletSDKPrivate
import ZaloPayAnalyticsSwift

class ZPPaymentPopupChooseMethodViewController: ZPBaseChildPopupPaymentController, ZPPaymentPopupAdjustDisplayProtocol {
    private let _state = BehaviorSubject<ZPPaymentPopupState>(value: .listMethod)
    private let _height = BehaviorSubject<CGFloat>(value: 0)
    var eState: Observable<ZPPaymentPopupState> {
        return _state.asObservable()
    }
    var eChangeHeight: Observable<CGFloat> {
        return self._height.asObservable()
    }
    @IBOutlet weak var tableView: UITableView?
    private lazy var presenter: ZPPaymentPopupListMethodsPresenter = ZPPaymentPopupListMethodsPresenter(with: self)
    var currentSelect: ZPPaymentMethodCellDisplay?
    private var currentIndex: IndexPath?
    let eResult: PublishSubject<ZPPaymentMethodCellDisplay> = PublishSubject()
    private let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    func setupNavigationBar() {}
    override func setupEvent() {
        self.backBtn.rx.tap.asDriver().drive(onNext: { [weak self](_) in
            ZPTrackingHelper.shared().trackEvent(.popupsdk_source_back)
            self?.eResult.onCompleted()
        }).disposed(by: disposeBag)
        self.tableView?.estimatedRowHeight = 64
        self.tableView?.rowHeight = UITableViewAutomaticDimension
        self.tableView?.rx.setDelegate(self).disposed(by: disposeBag)
        self.eResult.subscribe(onCompleted: { [weak self] in
            self?.navigationController?.popViewController(animated: true)
        }).disposed(by: disposeBag)
        guard let tableView = self.tableView else {
            return
        }
        
        // Calculate height
//        self.presenter.displayMethods.map { (values) -> CGFloat in
//            min(CGFloat(values.count * 70), 256)
//        }.bind(to: _height).disposed(by: disposeBag)
        
        self.presenter.displayMethods
            .bind(to: tableView.rx.items) { [weak self](tableView, row, element) in
                if element.type == .new {
                    let nCell = ZPPaymentPopupListMethodsAddNewTableViewCell.loadCell(from: tableView)
                    return nCell
                }
                let cell = ZPPaymentPopupListMethodsTableViewCell.loadCell(from: tableView)
                cell.setup(from: element)
                cell.isSelected = self?.currentIndex?.item == row
                return cell
            }
            .disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Select first
        guard let currentSelect = currentSelect, let defaultSelect = self.presenter.findDefaultSelect(from: currentSelect) else {
            return
        }
        self.currentIndex = defaultSelect
        self.tableView?.selectRow(at: defaultSelect, animated: false, scrollPosition: .none)
        ZPTrackingHelper.shared().trackScreen(withName: ZPTrackerScreen.Screen_iOS_SDK_Source)
    }
    
}
// MARK: - UITableViewDelegate
extension ZPPaymentPopupChooseMethodViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Check select
        func setPrevious() {
            guard let defaultSelect = self.currentIndex else {
                return
            }
            self.tableView?.selectRow(at: defaultSelect, animated: false, scrollPosition: .none)
        }
        
        guard let cell = tableView.cellForRow(at: indexPath) as? ZPPaymentPopupListMethodsTableViewCell else {
            ZPTrackingHelper.shared().trackEvent(.popupsdk_add)
            // Add new
            let new = ZPPaymentMethodCellDisplay()
            new.isNewMethod = true
            self.eResult.onNext(new)
            self.eResult.onCompleted()
            return
        }
        checkSelect: if cell.isCanSelect == false {
            // select previous
            setPrevious()
        }else {
            let item = self.presenter.item(at: indexPath)
            defer {
                self.eResult.onNext(item)
                self.eResult.onCompleted()
            }
            guard self.currentIndex?.item != indexPath.item else {
                break checkSelect
            }
            self.currentIndex = indexPath
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
}

