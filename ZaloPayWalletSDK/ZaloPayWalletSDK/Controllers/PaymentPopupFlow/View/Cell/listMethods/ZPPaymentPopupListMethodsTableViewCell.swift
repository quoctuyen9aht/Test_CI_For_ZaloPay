//
//  ZPPaymentPopupListMethodsTableViewCell.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/13/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayWalletSDKPrivate
import ZaloPayCommonSwift
@IBDesignable
class ZPPaymentPopupListMethodsSelectView: UIView {
    @IBInspectable var delta: CGFloat = 8 {
        didSet{
            setNeedsDisplay()
        }
    }
    private lazy var circleShape: CAShapeLayer = CAShapeLayer()
    private var isSelect: Bool = false
    override func draw(_ rect: CGRect) {
        self.layer.sublayers?.forEach({ $0.removeFromSuperlayer() })
        self.setupView(from: rect)
    }
    
    private func setupView(from rect:CGRect) {
        let w = rect.width
        self.layer.cornerRadius = w / 2
        self.layer.borderWidth = 0.5
        let rectShape = CGRect(x: delta / 2, y: delta / 2, width: w - delta, height: w - delta)
        let benzierPath = UIBezierPath(ovalIn: rectShape)
        self.circleShape.path = benzierPath.cgPath
        self.circleShape.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        self.layer.addSublayer(self.circleShape)
        self.updateDisplay()
    }
    
    private func updateDisplay() {
        let colorBorder: UIColor = isSelect ? .zaloBase() : .subText()
        let colorFillShape: UIColor = isSelect ? .zaloBase() : .white
        self.layer.borderColor = colorBorder.cgColor
        circleShape.fillColor = colorFillShape.cgColor
    }
    
    func setState(_ isSelect: Bool) {
        self.isSelect = isSelect
        self.updateDisplay()
    }
}

// MARK: Main class select
class ZPPaymentPopupMainSelectView: UITableViewCell, CellSelectProtocol {
    @IBOutlet weak var selectView: ZPPaymentPopupListMethodsSelectView?
    var isCanSelect: Bool = true {
        didSet {
            self.contentView.alpha = isCanSelect ? 1 : 0.6
        }
    }
    override var isSelected: Bool {
        get {
            return super.isSelected
        }
        
        set {
            guard updateState(newValue) else {
                return
            }
            super.isSelected = newValue
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        guard updateState(selected) else {
            return
        }
        super.setSelected(selected, animated: animated)
    }
}

// MARK: List
class ZPPaymentPopupListMethodsTableViewCell: ZPPaymentPopupMainSelectView {

    @IBOutlet weak var imgIcon: UIImageView?
    // Description
    @IBOutlet weak var lblDescription: UILabel?
    @IBOutlet weak var lblSubDescription: UILabel?
    @IBOutlet weak var lblFee: UIButton?
    @IBOutlet weak var widthButtonConstraint: NSLayoutConstraint?
    
    // More Informations
    @IBOutlet weak var moreInformationView: UIView?
    @IBOutlet weak var lblMoreInformationPrice: UILabel?
    @IBOutlet weak var lblMoreInformationSubDescription: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.lblFee?.isUserInteractionEnabled = false
        self.lblFee?.layer.cornerRadius = 3
        self.lblFee?.layer.borderWidth = 0.5
        self.lblFee?.layer.borderColor = UIColor(hexValue: 0xFE7A00).cgColor
        
        // adjust font for small screen
        if UIScreen.main.bounds.width == 320 {
            self.lblFee?.titleLabel?.font = UIFont.sfuiTextRegular(withSize: 10)
            self.lblSubDescription?.font = UIFont.sfuiTextRegular(withSize: 10)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        moreInformationView?.isHidden = true
    }
    
    func setup(from display: DisplayType) {
        let methodInfor = display.infor
        self.isCanSelect = methodInfor.method.isEnable()
        self.imgIcon?.image = methodInfor.image
        self.lblDescription?.text = methodInfor.methodName
        let isWallet = methodInfor.method.methodType == .zaloPayWallet
        self.moreInformationView?.isHidden = !isWallet
        let message: String
        if isWallet {
            self.lblFee?.isHidden = true
            self.lblSubDescription?.text = ""
            self.lblMoreInformationSubDescription?.text = methodInfor.feeText
            let total = ZaloPayWalletSDKPayment.sharedInstance().appDependence.currentBalance()
            let textTotal = "\(NSNumber(value: total).formatCurrency() ?? "")"
            self.lblMoreInformationPrice?.text = textTotal
            if let font = self.lblMoreInformationPrice?.font,
                let nF = UIFont(name: font.fontName, size: 13)
            {
                let att = NSAttributedString(string: "VND", attributes: [NSAttributedStringKey.font : nF])
                self.lblMoreInformationPrice?.attributedText += att
            }
            message = ""
        } else {
            self.lblFee?.isHidden = false
            let total = NSNumber(value: methodInfor.method.totalChargeFee).formatCurrency() ?? ""
            message =  methodInfor.fee == 0 ? R.string_Pay_Bill_Fee_Free() : total
            self.lblSubDescription?.text = methodInfor.feeText
        }
        self.lblFee?.setTitle(message, for: .normal)
        self.calculateWidthButton()
    }
    
    private func calculateWidthButton() {
        let s: CGFloat
        let fee = self.lblFee?.titleLabel?.text?.count ?? 0
        if fee > 0 {
            s = self.lblFee?.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude)).width ?? 0
        } else {
            s = 0
        }
        self.widthButtonConstraint?.constant = s
        self.layoutIfNeeded()
    }
}
