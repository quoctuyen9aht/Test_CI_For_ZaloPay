//
//  ZPPaymentPopupListMethodsAddNewTableViewCell.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/13/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayWalletSDKPrivate
class ZPPaymentPopupListMethodsAddNewTableViewCell: UITableViewCell {
    @IBOutlet weak var lblIcon: UILabel?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.lblIcon?.font = UIFont.iconFont(withSize: 30)
        let code = UILabel.iconCode(withName: "pay_addcard")
        self.lblIcon?.text = code
        self.lblIcon?.textColor = .zaloBase()
    }
}
