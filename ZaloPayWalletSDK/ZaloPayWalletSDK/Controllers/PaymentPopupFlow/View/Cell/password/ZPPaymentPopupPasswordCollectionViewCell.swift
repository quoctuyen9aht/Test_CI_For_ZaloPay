//
//  ZPPaymentPopupPasswordCollectionViewCell.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/11/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

class ZPPaymentPopupPasswordCollectionViewCell: UICollectionViewCell {
    private lazy var vDisplay: UIView = {
        let v = UIView(frame: self.contentView.bounds)
        self.contentView.addSubview(v)
        return v
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
        vDisplay.layer.cornerRadius = vDisplay.bounds.width / 2
        vDisplay.clipsToBounds = true
        vDisplay.layer.borderWidth = 1;
        vDisplay.layer.borderColor = UIColor.zaloBase().cgColor
    }
    
    override var isSelected: Bool {
        didSet{
            updateSelect()
        }
    }
    
    func updateSelect() {
        vDisplay.backgroundColor = !isSelected ? .white : .zaloBase()
    }
}
