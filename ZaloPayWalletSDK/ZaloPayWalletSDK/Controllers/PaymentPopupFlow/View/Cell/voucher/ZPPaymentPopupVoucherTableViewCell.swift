//
//  ZPPaymentPopupVoucherTableViewCell.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/14/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayWalletSDKPrivate
import ZaloPayCommonSwift
protocol CellSelectProtocol: class {
    var isCanSelect: Bool { get }
    var selectView: ZPPaymentPopupListMethodsSelectView? { get }
    // return can update
    func updateState(_ select:Bool) -> Bool
}

extension CellSelectProtocol {
    func updateState(_ select:Bool) -> Bool {
        guard isCanSelect else {
            return false
        }
        self.selectView?.setState(select)
        return true
    }
}

class ZPPaymentPopupVoucherTableViewCell: ZPPaymentPopupMainSelectView {
    @IBOutlet weak var lblDescription: UILabel?
    @IBOutlet weak var lblSubDescription: UILabel?
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupDisplay(from voucher: ZPVoucherHistory) {
        let type = voucher.valuetype.toInt
        self.isCanSelect = !voucher.isDisable
        guard let vType = ZPVoucherValueType(rawValue: type) else {
            return
        }
        switch vType {
        case .percent:
            lblDescription?.text = "Giảm \(voucher.value)%"
        case .value:
            lblDescription?.text = "Giảm \(NSNumber(value: voucher.value).formatCurrency() ?? "" )"
            lblDescription?.attributedText += NSAttributedString(string: "VND", attributes: [NSAttributedStringKey.font : UIFont.sfuiTextRegular(withSize: 13)])
        }
        
        lblSubDescription?.text = voucher.voucherHistoryDescription
    }
}

class ZPPaymentPopupNotChooseVoucherTableViewCell: ZPPaymentPopupMainSelectView {
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
