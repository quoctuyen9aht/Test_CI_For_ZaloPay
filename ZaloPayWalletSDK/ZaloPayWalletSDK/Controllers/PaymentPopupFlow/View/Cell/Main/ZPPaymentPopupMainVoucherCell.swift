//
//  ZPPaymentPopupMainVoucherCell.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/20/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit
import ZaloPayWalletSDKPrivate

class ZPPaymentPopupMainVoucherCell: ZPPaymentPopupMainBaseCell {
    @IBOutlet weak var imgIcon: UILabel?
    @IBOutlet weak var lblDescription: UILabel?
    @IBOutlet weak var lblChoose: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // add Line
        let vLine = UIView(frame: .zero)
        vLine.backgroundColor = UIColor.line()
        self.addSubview(vLine)
        vLine.snp.makeConstraints { (m) in
            m.top.equalToSuperview()
            m.left.equalToSuperview()
            m.right.equalToSuperview()
            m.height.equalTo(1)
        }
        imgIcon?.font = UIFont.zaloPay(withSize: 34)
        imgIcon?.textColor = UIColor.zp_red()
        
    }

    override func setup(from display: DisplayType) {
        self.type = display.type
        imgIcon?.text = UILabel.iconCode(withName: "gift_box")
        let isHaveVoucher: Bool
        var message: String
        defer {
            lblChoose?.isHidden = isHaveVoucher
            self.lblDescription?.text = message
        }
        
        guard let voucher = display.voucher else {
            isHaveVoucher = false
            message = "Bạn có Quà tặng"
            return
        }
        isHaveVoucher = true
        
        let voucherUse = voucher.voucherUsed
        let discount = voucher.discountAmount
        let percent = voucherUse?.value ?? 0
        let money = NSNumber(value: discount).formatCurrency() ?? ""
        message = voucherUse?.valuetype == Int32(ZPVoucherValueType.percent.rawValue) ? "Giảm \(percent)%" : money
        
    }
    

}
