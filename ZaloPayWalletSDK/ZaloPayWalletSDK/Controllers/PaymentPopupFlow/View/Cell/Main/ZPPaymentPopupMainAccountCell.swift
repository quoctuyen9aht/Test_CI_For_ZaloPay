//
//  ZPPaymentPopupMainAccountCell.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/7/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayWalletSDKPrivate
import ZaloPayCommonSwift
protocol Cell {}
extension UITableViewCell: Cell {}
extension Cell where Self: UITableViewCell {
    static func loadCell(using identify: String? = nil, from table:UITableView) -> Self {
        let identify = identify ?? "\(self)"
        guard let cell = table.dequeueReusableCell(withIdentifier: identify) as? Self else {
            fatalError("Please check cell")
        }
        return cell
    }
}
class ZPPaymentPopupMainBaseCell: UITableViewCell {
    var type: ZPPaymentPopupMainType = .none
    func getButton() -> UIButton? { return nil }
    func setup(from display: DisplayType) {}
    func adjustIndicator(show isShow: Bool) {}
}


class ZPPaymentPopupMainAccountCell: ZPPaymentPopupMainBaseCell {
    @IBOutlet weak var imgIcon: UIImageView?
    @IBOutlet weak var lblName: UILabel?
    @IBOutlet weak var lblDescription: UILabel?
    @IBOutlet weak var lblFee: UIButton?
    
    @IBOutlet weak var moreInformationView: UIView?
    @IBOutlet weak var descriptionView: UIView?
    @IBOutlet weak var lblMoreInformationPrice: UILabel?
    @IBOutlet weak var lblMoreInformationSubDescription: UILabel?
    
    @IBOutlet weak var rightConstraintInformationView: NSLayoutConstraint?
    @IBOutlet weak var widthButtonConstraint: NSLayoutConstraint?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.lblFee?.isUserInteractionEnabled = false
        self.lblFee?.layer.cornerRadius = 3
        self.lblFee?.layer.borderWidth = 0.5
        self.lblFee?.layer.borderColor = UIColor(hexValue: 0xFE7A00).cgColor
        
        // adjust font for small screen
        if UIScreen.main.bounds.width == 320 {
            self.lblFee?.titleLabel?.font = UIFont.sfuiTextRegular(withSize: 10)
            self.lblDescription?.font = UIFont.sfuiTextRegular(withSize: 10)
        }
        
    }
    override func setup(from display: DisplayType) {
        let methodInfor = display.infor
        self.type = display.type
        imgIcon?.image = methodInfor.image
        lblName?.text = methodInfor.methodName
        let isWallet = methodInfor.method.methodType == .zaloPayWallet
        self.moreInformationView?.isHidden = !isWallet
        self.lblFee?.isHidden = isWallet
        let message: String
        if isWallet {
            self.lblMoreInformationSubDescription?.text = methodInfor.feeText
            let total = ZaloPayWalletSDKPayment.sharedInstance().appDependence.currentBalance()
            let textTotal = "\(NSNumber(value: total).formatCurrency() ?? "")"
            self.lblMoreInformationPrice?.text = textTotal
            let att = NSAttributedString(string: "VND", attributes: [NSAttributedStringKey.font : UIFont.sfuiTextRegular(withSize: 13)])
            self.lblMoreInformationPrice?.attributedText += att
            self.lblDescription?.text = ""
            message = ""
        } else {
            let total = NSNumber(value: methodInfor.method.totalChargeFee).formatCurrency() ?? ""
            message =  methodInfor.fee == 0 ? R.string_Pay_Bill_Fee_Free() : total
            self.lblDescription?.text = methodInfor.feeText
        }
        self.lblFee?.setTitle(message, for: .normal)
        self.calculateWidthButton()
    }
    
    private func calculateWidthButton() {
        let s = self.lblFee?.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: 30)).width ?? 0
        self.widthButtonConstraint?.constant = s
        self.layoutIfNeeded()
    }
    
    override func adjustIndicator(show isNotShow: Bool) {
        self.accessoryType = !isNotShow ? .disclosureIndicator : .none
        self.rightConstraintInformationView?.constant = !isNotShow ? 0 : 12
    }
}
