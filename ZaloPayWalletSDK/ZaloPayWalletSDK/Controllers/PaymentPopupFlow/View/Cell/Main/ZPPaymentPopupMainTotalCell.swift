//
//  ZPPaymentPopupMainTotalCell.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/7/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayWalletSDKPrivate
import RxCocoa
import RxSwift
import SnapKit
import ZaloPayAnalyticsSwift
import ZaloPayCommonSwift

protocol AnimateViewResultProtocol: class {
    func showAnimate(with result: Bool) -> Observable<Void>
}

class ZPPaymentPopupMainTotalCell: ZPPaymentPopupMainBaseCell, AnimateViewResultProtocol {
    @IBOutlet weak var btnPayment: UIButton?
    override func getButton() -> UIButton? {
        return btnPayment
    }
    private lazy var viewResult: UILabel = {
        let lblResult = UILabel(frame: .zero)
        lblResult.numberOfLines = 2
        lblResult.alpha = 0
        lblResult.textAlignment = .center
        lblResult.isHidden = true
        lblResult.font = UIFont.sfuiTextRegular(withSize: 14)
//        lblResult.layer.borderWidth = 1
//        lblResult.layer.borderColor = UIColor.line().cgColor
        self.contentView.addSubview(lblResult)
        lblResult.snp.makeConstraints { (m) in
            m.size.equalToSuperview()
            m.center.equalToSuperview()
        }
        lblResult.transform = CGAffineTransform(translationX: 0, y: 300)
        return lblResult
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.btnPayment?.layer.cornerRadius = 3
        self.btnPayment?.clipsToBounds = true
        self.btnPayment?.setBackgroundColor(#colorLiteral(red: 0.01568627451, green: 0.7450980392, blue: 0.01568627451, alpha: 1), for: .normal)
        self.btnPayment?.setBackgroundColor(#colorLiteral(red: 0.7803921569, green: 0.7803921569, blue: 0.8, alpha: 1), for: .disabled)
    }
    
    func showAnimate(with result: Bool) -> Observable<Void> {
        let event: ZPAnalyticEventAction = result ? .popupsdk_success : .popupsdk_fail
        ZPTrackingHelper.shared().trackEvent(event)
        let iconName = result ? "pay_success" : "pay_fail"
        let iconFont = UILabel.iconCode(withName: iconName) ?? ""
        let message = "\(iconFont) \n \(result ? "Thành công" : "Thất bại")"
        let color = result ? #colorLiteral(red: 0.01568627451, green: 0.7450980392, blue: 0.01568627451, alpha: 1) : .red
        viewResult.text = message
        viewResult.textColor = color
        viewResult.attributedText += NSAttributedString(string: iconFont, attributes: [NSAttributedStringKey.font: UIFont.zaloPay(withSize: 30)])
        viewResult.isHidden = false
        let vLine = UIView(frame: CGRect(origin: .zero, size: CGSize(width: UIScreen.main.bounds.width, height: 1)))
        vLine.backgroundColor = UIColor.line()
        viewResult.addSubview(vLine)
        return Observable.create({ [weak self](s) -> Disposable in
            UIView.animateKeyframes(withDuration: 1.0, delay: 0, options: .calculationModeLinear, animations: {
                UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.3, animations: {
                    self?.viewResult.transform = .identity
                    self?.viewResult.alpha = 1
                })
            }, completion: { (c) in
                s.onNext(())
                s.onCompleted()
            })
            return Disposables.create()
        }).subscribeOn(MainScheduler.instance)
    }
}
