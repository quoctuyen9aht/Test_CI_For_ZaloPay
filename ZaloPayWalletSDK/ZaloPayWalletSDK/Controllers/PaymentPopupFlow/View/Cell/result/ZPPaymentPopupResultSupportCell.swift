//
//  ZPPaymentPopupResultSupportCell.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/22/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import SnapKit
import ZaloPayWalletSDKPrivate
class ZPPaymentPopupResultSupportCell: ZPPaymentPopupResultBaseCell {
    @IBOutlet weak var imgSupport: UIImageView?
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tView = UIView(frame: .zero)
        tView.backgroundColor = UIColor.line()
        self.addSubview(tView)
        tView.snp.makeConstraints { (m) in
            m.top.equalToSuperview()
            m.left.equalToSuperview()
            m.right.equalToSuperview()
            m.height.equalTo(1)
        }
        
        let bView = UIView(frame: .zero)
        bView.backgroundColor = UIColor.line()
        self.addSubview(bView)
        bView.snp.makeConstraints { (m) in
            m.bottom.equalToSuperview()
            m.left.equalToSuperview()
            m.right.equalToSuperview()
            m.height.equalTo(1)
        }
        imgSupport?.image = ZPResourceManager.getImageWithName("support.png")
    }
}
