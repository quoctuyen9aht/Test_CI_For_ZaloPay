//
//  ZPPaymentPopupResultBottomCell.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/22/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit

class ZPPaymentPopupResultBottomCell: ZPPaymentPopupResultBaseCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnClose?.layer.cornerRadius = 3
        btnClose?.layer.borderColor = #colorLiteral(red: 0.2901960784, green: 0.7333333333, blue: 1, alpha: 1).cgColor
        btnClose?.layer.borderWidth = 1
        btnClose?.clipsToBounds = true
        
    }
}
