//
//  ZPPaymentPopupResultDescriptionTopCell.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/22/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayWalletSDKPrivate
import SnapKit

class ZPPaymentPopupResultBaseCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel?
    @IBOutlet weak var lblDescription: UILabel?
    @IBOutlet weak var btnClose: UIButton?
    var type: DisplayResultItemType = .none
    func setupDisplay(with item: DisplayResultItem) {
        lblTitle?.text = item.title
        lblDescription?.text = item.description
        self.type = item.type
    }
    func getButton() -> UIButton? {
        return btnClose
    }
}

class ZPPaymentPopupResultDescriptionTopCell: ZPPaymentPopupResultBaseCell {
    @IBOutlet weak var lblTop: UILabel?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.lblTop?.font = UIFont.zaloPay(withSize: 66)
        self.lblTop?.textColor = UIColor.zp_red()
        self.lblTop?.text = UILabel.iconCode(withName: "pay_fail")
        //self.bottomLine setImage:[UIImage imageNamed:@"payorder_line"]
        let bottomLine = UIImageView(image: #imageLiteral(resourceName: "payorder_line"))
        self.contentView.addSubview(bottomLine)
        bottomLine.snp.makeConstraints { (m) in
            m.bottom.equalToSuperview()
            m.left.equalTo(12)
            m.right.equalTo(12)
            m.height.equalTo(1)
        }
    }
}
