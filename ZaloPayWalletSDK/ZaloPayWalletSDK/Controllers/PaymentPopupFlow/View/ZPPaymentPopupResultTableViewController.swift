//
//  ZPPaymentPopupResultTableViewController.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/22/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import ZaloPayWalletSDKPrivate
import RxSwift
import RxCocoa
class ZPPaymentPopupResultTableViewController: UITableViewController {
    var itemResult: ItemResult?
    private lazy var presenter: ZPPaymentPopupResultPresenter = {
        guard let itemResult = self.itemResult else {
            fatalError("Please set value first!!!")
        }
        return ZPPaymentPopupResultPresenter(with: itemResult, on: self)
    }()
    let eCompleted: PublishSubject<Void> = PublishSubject()
    private let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Thanh toán thất bại"
        let v = UIView(frame: .zero)
        v.backgroundColor = UIColor.clear
        let lBar = UIBarButtonItem(customView: v)
        self.navigationItem.leftBarButtonItem = lBar
        presenter.loadSource()
    }
    
    private func setupEvent() {
        self.eCompleted.subscribe(onNext: { [weak self](_) in
            self?.navigationController?.popViewController(animated: true)
        }).disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return self.presenter.cellSource.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let items = self.presenter.source[section]
        return items?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellType = self.presenter.cellSource[indexPath.section]
        let cell = cellType.loadCell(from: tableView)
        if let displayItem = self.presenter.source[indexPath.section]?[indexPath.item] {
            cell.setupDisplay(with: displayItem)
        }
        if let button = cell.getButton() {
            button.rx.tap.takeUntil(cell.rx.methodInvoked(#selector(UITableViewCell.prepareForReuse))).bind { [weak self] in
                self?.eCompleted.onNext(())
                self?.eCompleted.onCompleted()
            }.disposed(by: disposeBag)
        }
        return cell
    }
    
    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        guard let cell = tableView.cellForRow(at: indexPath) as? ZPPaymentPopupResultBaseCell else {
            return
        }
        self.presenter.handlerSelect(at: cell.type)
    }

}
