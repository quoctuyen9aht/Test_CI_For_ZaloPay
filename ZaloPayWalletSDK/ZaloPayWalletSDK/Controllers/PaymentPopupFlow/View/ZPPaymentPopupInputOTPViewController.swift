//
//  ZPPaymentPopupInputOTPViewController.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/21/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import ZaloPayWalletSDKPrivate
import ZaloPayAnalyticsSwift

class ZPPaymentPopupInputOTPViewController: ZPBaseChildPopupPaymentController, ZPPaymentPopupAdjustDisplayProtocol {
    @IBOutlet weak var btnClose: UIButton?
    @IBOutlet weak var textField: UITextField?
    @IBOutlet weak var lblError: UILabel?
    @IBOutlet weak var indicatorView: UIView?
    @IBOutlet weak var btnContinue: UIButton?
    var isAppear: Bool = true
    fileprivate (set) lazy var spinnerView: DGActivityIndicatorView = {
        let v: DGActivityIndicatorView = DGActivityIndicatorView(type:.ballPulse, tintColor: UIColor.subText())
        self.indicatorView?.addSubview(v)
        v.snp.makeConstraints({
            $0.edges.equalToSuperview()
        })
        return v
    }()
    var retryItem: RetryExcutePaymentItem?
    
    private let _state = BehaviorSubject<ZPPaymentPopupState>(value: .otp)
    private let _height = BehaviorSubject<CGFloat>(value: 188)
    var eState: Observable<ZPPaymentPopupState> {
        return _state.asObservable()
    }
    var eChangeHeight: Observable<CGFloat> {
        return self._height.asObservable()
    }
    private let disposeBag = DisposeBag()
    private lazy var presenter: ZPPaymentPopupInputOTPPresenter = ZPPaymentPopupInputOTPPresenter(using: self, with: self.retryItem)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDisplay()
    }
    
    func setupNavigationBar() {}
    
    private func setupDisplay() {
        btnContinue?.layer.cornerRadius = (btnContinue?.bounds.height ?? 0) / 2
        btnContinue?.clipsToBounds = true
        btnContinue?.setBackgroundColor(#colorLiteral(red: 0, green: 0.5607843137, blue: 0.8980392157, alpha: 1), for: .normal)
        btnContinue?.setBackgroundColor(#colorLiteral(red: 0.7803921569, green: 0.7803921569, blue: 0.8, alpha: 1), for: .disabled)
        
        btnClose?.setIconFont("red_delete", for: .normal)
        btnClose?.titleLabel?.font = UIFont.iconFont(withSize: 16)
        btnClose?.setTitleColor(UIColor.subText(), for: .normal)
    }
    
    internal override func setupEvent() {
        self.rx.methodInvoked(#selector(viewWillAppear(_:))).take(1).bind { [weak self](_) in
            ZPTrackingHelper.shared().trackEvent(.popupsdk_otp_input)
            self?.textField?.becomeFirstResponder()
        }.disposed(by: disposeBag)
        self.presenter.setupEvent()
        
        btnClose?.rx.controlEvent(UIControlEvents.touchUpInside).bind { [weak self] in
            ZPTrackingHelper.shared().trackEvent(.popupsdk_otp_close)
            self?.presenter.cancelPayment()
        }.disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        isAppear = true
        ZPTrackingHelper.shared().trackScreen(withName:ZPTrackerScreen.Screen_iOS_SDK_OTP)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isAppear = false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard textField?.isFirstResponder == false else {
            return
        }
        textField?.becomeFirstResponder()
    }
}
