//
//  ZPPaymentPopupVoucherViewController.swift
//  ZaloPay
//
//  Created by Dung Vu on 12/4/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import ZaloPayWalletSDKPrivate
import ZaloPayAnalyticsSwift

class ZPPaymentPopupVoucherViewController: ZPBaseChildPopupPaymentController, ZPPaymentPopupAdjustDisplayProtocol {
    @IBOutlet weak var tableView: UITableView?
    fileprivate lazy var presenter: ZPPaymentPopupVoucherPresenter = ZPPaymentPopupVoucherPresenter(self)
    let eResult: PublishSubject<Bool> = PublishSubject()
    private let _state = BehaviorSubject<ZPPaymentPopupState>(value: .voucher)
    private let _height = BehaviorSubject<CGFloat>(value: 0)
    var eState: Observable<ZPPaymentPopupState> {
        return _state.asObservable()
    }
    var eChangeHeight: Observable<CGFloat> {
        return self._height.asObservable()
    }
    private var currentIndex: IndexPath?
    private let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.loadResource()
    }
    
    func setupNavigationBar() {}
    
    internal override func setupEvent() {
        self.backBtn.rx.tap.asDriver().drive(onNext: { [weak self](_) in
            ZPTrackingHelper.shared().trackEvent(.popupsdk_voucher_back)
            self?.navigationController?.popViewController(animated: true)
        }).disposed(by: disposeBag)
        
        self.tableView?.rx.setDelegate(self).disposed(by: disposeBag)
        self.eResult.subscribe(onCompleted: { [weak self] in
            self?.navigationController?.popViewController(animated: true)
        }).disposed(by: disposeBag)
        guard let tableView = tableView else {
            return
        }
        
        // Update height
//        self.presenter.dataSource.map({ ms -> CGFloat in
//            guard ms.count > 0 else {
//                return 0
//            }
//            let itemsVoucher = max(0, ms.count - 1)
//            let result: CGFloat = CGFloat(itemsVoucher * 84 + 63)
//            return min(result, 315)
//        }).bind(to: _height).disposed(by: disposeBag)
        
        self.presenter.dataSource
            .bind(to: tableView.rx.items) { (tableView, row, element) in
                if element.discountamount < 0 {
                    // add no method
                    let noVoucherCell = ZPPaymentPopupNotChooseVoucherTableViewCell.loadCell(from: tableView)
                    return noVoucherCell
                }
                
                let cell = ZPPaymentPopupVoucherTableViewCell.loadCell(from: tableView)
                cell.setupDisplay(from: element)
                return cell
            }
            .disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkCurrentSelect()
        ZPTrackingHelper.shared().trackScreen(withName:ZPTrackerScreen.Screen_iOS_SDK_Voucher)
    }
    
    private func checkCurrentSelect() {
        let count = self.presenter.list.count
        let indexPath: IndexPath
        defer {
            self.currentIndex = indexPath
            self.tableView?.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
        
        guard let voucher = self.presenter.bill.voucherInfo else {
            // Select not use voucher
            indexPath = IndexPath(item: count - 1, section: 0)
            return
        }
        let idx = self.presenter.list.index { (h) -> Bool in
           return h.uid == voucher.voucherUsed?.uid
        }
        guard let index = idx else {
            indexPath = IndexPath(item: count - 1, section: 0)
            return
        }
        indexPath = IndexPath(item: index, section: 0)
    }
    private func selectPrevious() {
        self.tableView?.selectRow(at: self.currentIndex, animated: false, scrollPosition: .none)
    }
    
}

extension ZPPaymentPopupVoucherViewController: UITableViewDelegate {
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? CellSelectProtocol,
            cell.isCanSelect else {
            // select previous
            self.selectPrevious()
            return
        }
        
        let voucherHistory = self.presenter.list[indexPath.item]
        if voucherHistory.discountamount < 0 {
            // No voucher
            presenter.bill.voucherInfo = nil
            self.eResult.onCompleted()
        } else {
            // Check if duplicate voucher
            let oldUid: String? = presenter.bill.voucherInfo?.voucherUsed?.uid
            let newUId = voucherHistory.uid
            if oldUid == newUId {
                self.eResult.onCompleted()
                return
            }
            // Other -> Use
            ZPProgressHUD.show(withStatus: nil)
            presenter.useVoucher(from: voucherHistory).observeOn(MainScheduler.instance).subscribe(onError: {[weak self](e) in
                // handler error
                voucherHistory.isDisable = true
                self?.tableView?.reloadRows(at: [indexPath], with: .none)
                ZPProgressHUD.dismiss()
                ZPDialogView.showDialogWithError(e, handle: nil)
            }, onCompleted: { [weak self] in
                ZPProgressHUD.dismiss()
                self?.eResult.onCompleted()
            }).disposed(by: disposeBag)
        }
        
    }
}
