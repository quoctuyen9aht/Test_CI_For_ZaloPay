//
//  ZPPaymentPopupResultWraper.swift
//  ZaloPayWalletSDK
//
//  Created by Bon Bon on 3/16/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ZaloPayWalletSDKPrivate

class ZPPaymentPopupResultWraper: NSObject {
    fileprivate (set) lazy var completeSignal: PublishSubject<ZPPaymentResponse?> = PublishSubject()
    weak var viewController: UIViewController?
    public func showErrorResult(from: UIViewController,
                                response: ZPPaymentResponse,
                                method: ZPPaymentMethod?,
                                bill: ZPBill) -> Observable<ZPPaymentResponse?> {
        method?.calculateCharge(with: bill.amount)
        return self.showResult(from: from,
                               response: response,
                               totalCharge: method?.totalChargeAmount ?? 0,
                               feeCharge: method?.totalChargeFee ?? 0, bill: bill)
    }
    
    public func showResult(from: UIViewController,
                                response: ZPPaymentResponse,
                                totalCharge: Int,
                                feeCharge: Int,
                                bill: ZPBill) -> Observable<ZPPaymentResponse?> {
        let handler = ZPResultHandler()
        handler.bill = bill
        handler.responseResult = response
        handler.totalCharge = totalCharge
        handler.feeCharge = feeCharge
        handler.isPaymentSuccess = response.errorCode == 1
        let resultViewController = ZPResultViewController.init(resultHandler: handler)!
        resultViewController.delegate = self
        resultViewController.responseNotifySupport = response
        from.navigationController?.pushViewController(resultViewController, animated: true)
        self.viewController = resultViewController;
        return self.completeSignal
    }
}

extension ZPPaymentPopupResultWraper: ZPPaymentViewControllerDelegate {
    func paymentControllerDidFinish(with response: ZPPaymentResponse!) {
        completeSignal.onNext(response)
        completeSignal.onCompleted()
    }
    
    func paymentControllerNotifyResult(with response: ZPPaymentResponse!) {
        
    }
    
    func paymentControllerDidChooseFAQ() {
        if let controller = self.viewController {
            ZaloPayWalletSDKPayment.sharedInstance().appDependence.showFAQ(controller)
        }
    }
    
    func paymentControllerDidChooseSupport(withData data: [AnyHashable : Any]!) {
        if let controller = self.viewController {
            ZaloPayWalletSDKPayment.sharedInstance().appDependence.showNeedSupport(using: data, from: controller)
        }
    }
    
    func paymentControllerDidClose(_ key: String!, data: String!) {
        if key == ZP_NOTIF_UPGRADE_LEVEL_3 {
            ZaloPayWalletSDKPayment.sharedInstance().appDependence.updateLevel3(from: self.viewController)
            return
        }
        completeSignal.onNext(nil)
        completeSignal.onCompleted()
    }
}


