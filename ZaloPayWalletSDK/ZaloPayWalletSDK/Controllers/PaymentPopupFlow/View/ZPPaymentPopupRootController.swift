//
//  ZPPaymentPopupRootController.swift
//  ZaloPay
//
//  Created by Dung Vu on 12/4/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import SnapKit
import ZaloPayWalletSDKPrivate
import ZaloPayAnalyticsSwift

// find method appriciate
internal protocol MakeControllerProtocol: class {}
extension UIViewController: MakeControllerProtocol {}
internal extension MakeControllerProtocol where Self:UIViewController {
    /// Create controller from storyboard
    ///
    /// - Parameters:
    ///   - name: name storyboard, default is Main
    ///   - identify: identify in storyboard if nil it'll use default controller
    ///   - bundle: bundler that include storyboard
    /// - Returns: new controller
    static func loadFromStoryBoard(with name: String = "Main",
                                   _ identify: String? = nil,
                                   at bundle: Bundle? = nil) -> Self {
        let storyBoard = UIStoryboard(name: name, bundle: bundle)
        let controller: UIViewController?
        // case identify not nil
        if let identify = identify , !identify.isEmpty {
            controller = storyBoard.instantiateViewController(withIdentifier: identify)
        } else {
            controller = storyBoard.instantiateInitialViewController()
        }
        
        guard let result = controller as? Self else {
            fatalError("Error create controller")
        }
        
        return result
    }
}
// Root will control data and response
@objcMembers
public final class ZPPaymentPopupRootController: UIViewController {
    fileprivate (set) lazy var presenter: ZPPaymentPopupPresenter = ZPPaymentPopupPresenter(with: bill)
    fileprivate var bill: ZPBill!
    fileprivate (set) lazy var eResponse: PublishSubject<AnyObject?> = PublishSubject()
    fileprivate (set) lazy var eResult: PublishSubject<AnyObject?> = PublishSubject()
    fileprivate let disposeBag = DisposeBag()
    @IBOutlet weak var containerController: UIView?
    @IBOutlet weak var hContainerView: NSLayoutConstraint?
    weak var eResultWeb: PublishSubject<Bool>?
    public var isMappingCard: Bool = false
    
    private lazy var errorHandler = ZPPaymentPopupResultWraper()
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.setupDisplay()
        self.setupEvent()
        self.presenter.rootVC = self
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isMappingCard = false
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    private func setupDisplay() {
        let effect = UIBlurEffect(style: .extraLight)
        let effectView = UIVisualEffectView(effect: effect)
        containerController?.insertSubview(effectView, at: 0)
        effectView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        containerController?.transform = CGAffineTransform(translationX: 0, y: 1000)
        self.navigationController?.defaultNavigationBarStyle()
    }
    
    private func setupEvent() {
        eResult.subscribe(onDisposed: { [weak self] in
            ZPTrackingHelper.shared().trackEvent(.popupsdk_out_main)
            self?.presenter.revertVouchers()
            self?.presenter.writeLog()
        }).disposed(by: disposeBag)
        
        // Track log sucess
        self.eResponse.observeOn(SerialDispatchQueueScheduler(qos: .background)).subscribe(onNext: { [weak self](response) in
            guard let response = response as? ZPPaymentResponse else {
                return
            }
            self?.presenter.interactor.trackOrderDisplay(from: response, resultDisplay: .success)
            self?.presenter.interactor.trackOrderResult(from: response)
        }).disposed(by: disposeBag)
        
        // Add view prevent other touch when receive result
        self.eResponse.observeOn(MainScheduler.instance).subscribe(onNext: { [weak self](_) in
            // add view
            let v = UIView(frame: .zero)
            v.backgroundColor = .clear
            self?.view.addSubview(v)
            v.snp.makeConstraints({ (m) in
                m.edges.equalToSuperview()
            })
        }).disposed(by: disposeBag)
        
        self.eResponse.observeOn(MainScheduler.instance).subscribe(onError: { [weak self](e) in
            guard let error = e as? ZPPopupError else {
                return
            }
            switch error {
            case .cancel:
                let nE = NSError(domain: NSURLErrorDomain, code: NSURLErrorCancelled, userInfo: [NSLocalizedDescriptionKey : "Cancel"])
                self?.eResult.onError(nE)
            case .result(let response, let method):
                // Move to error screen
                guard let wSelf = self else {
                    return
                }
                self?.presenter.interactor.trackOrderDisplay(from: response, resultDisplay: .fail)
                self?.presenter.interactor.trackOrderResult(from: response)
//                let itemResult = ItemResult(bill: wSelf.presenter.interactor.bill, response: response, method:method)
//                wSelf.performSegue(withIdentifier: "showResult", sender: itemResult)
                let bill = wSelf.presenter.interactor.bill
                wSelf.presenter.interactor.updateBill(from: nil)
                wSelf.errorHandler.showErrorResult(from: wSelf,
                                                   response: response,
                                                   method: method,
                                                   bill: bill).subscribe(onCompleted:
                {
                    let e = NSError(domain: NSURLErrorDomain,
                                    code: NSURLErrorBadServerResponse,
                                    userInfo: [NSLocalizedDescriptionKey: "Thất bại"])
                    self?.eResult.onError(e)
                }).disposed(by: wSelf.disposeBag)
            default:
                break
            }
        }).disposed(by: disposeBag)
        
        self.rx.methodInvoked(#selector(viewWillAppear(_:))).take(1).subscribe(onNext: { [weak self](_) in
            UIView.animate(withDuration: 0.3, animations: {
                self?.containerController?.transform = .identity
            })
        }).disposed(by: disposeBag)
        
        let eventKeyboards = [NotificationCenter.default.rx
            .notification(Notification.Name.UIKeyboardWillShow),
                              NotificationCenter.default.rx
                                .notification(Notification.Name.UIKeyboardWillHide)]
        
        Observable.merge(eventKeyboards)
            .subscribe(onNext: { [weak self](n) in
                guard let info = n.userInfo , let wSelf = self else {
                    return
                }
                let isHidden = n.name == Notification.Name.UIKeyboardWillHide
                
                let duration: TimeInterval = info[UIKeyboardAnimationDurationUserInfoKey] as? TimeInterval ?? 0
                let f = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue ?? .zero
                
                let nextHeight: CGFloat = !isHidden ? f.size.height : 0
                UIView.animate(withDuration: duration, animations: {
                    wSelf.containerController?.transform = CGAffineTransform(translationX: 0, y: -nextHeight)
                }, completion: nil)
            }).disposed(by: disposeBag)
    }
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let name = segue.identifier
        switch name {
        case let .some(f) where f == "showFlow":
            let navi = segue.destination as? UINavigationController
            navi?.view.backgroundColor = .white
            guard let mainPaymentVC = navi?.visibleViewController as? ZPPaymentPopupMainViewController else {
                return
            }
            navi?.delegate = self.presenter
            mainPaymentVC.rootPresenter = presenter
        case let .some(f) where f == "showWeb":
            self.presenter.interactor.trackWebLogin()
            let webVC = segue.destination as? ZPPaymentPopupWebViewController
            webVC?.urlString = sender as? String
            webVC?.apptransid = self.presenter.interactor.bill.appTransID
            self.eResultWeb = webVC?.eResult
          
//        case let .some(f) where f == "showResult":
//            let resultVC = segue.destination as? ZPPaymentPopupResultTableViewController
//            let item = sender as? ItemResult
//            resultVC?.itemResult = item
//            resultVC?.eCompleted.subscribe(onCompleted: { [weak self] in
//                let e = NSError(domain: NSURLErrorDomain, code: NSURLErrorBadServerResponse, userInfo: [NSLocalizedDescriptionKey: "Thất bại"])
//                self?.eResult.onError(e)
//            }).disposed(by: disposeBag)
        default:
            break
        }
    }
    
    override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first, let container = self.containerController else {
            return
        }
        let point = touch.location(in: container)
        guard !container.bounds.contains(point), self.presenter.statePayment.value.isCanCancel else {
            return
        }
        self.presenter.cancelPayment()
    }
}

// MARK: Public
public extension ZPPaymentPopupRootController {
    public static func show(on vc: UIViewController?, with bill: AnyObject?) -> Observable<AnyObject?>{
        guard let vc = vc , let bill = bill as? ZPBill else {
            return Observable.empty()
        }
        ZPProgressHUD.show(withStatus: nil)
        return ZPPaymentPopupInteractor.startInfor(with: bill)
            .observeOn(MainScheduler.asyncInstance).flatMap { (_) -> Observable<AnyObject?> in
            ZPProgressHUD.dismiss()
            return Observable.create({ (s) -> Disposable in
                let popUpVC = UINavigationController.loadFromStoryBoard(with: "PaymentPopup")
                popUpVC.view.backgroundColor = .clear
                guard let rootVC = popUpVC.visibleViewController as? ZPPaymentPopupRootController else {
                    s.onCompleted()
                    return Disposables.create()
                }
                var transitionCustom: PaymentPopUpTransitionDelegate? = PaymentPopUpTransitionDelegate()
                rootVC.bill = bill
                _ = rootVC.eResult.subscribe(s)
                popUpVC.modalPresentationStyle = .custom
                popUpVC.transitioningDelegate = transitionCustom
                vc.present(popUpVC, animated: true, completion: nil)
                return Disposables.create {
                    popUpVC.dismiss(animated: true, completion: nil)
                    transitionCustom = nil
                }
            })
        }
    }
    
    // Objc
    
    /// Show Popup Payment
    ///
    /// - Parameters:
    ///   - vc: View Controller that 'll show popup
    ///   - bill: - ZPBill : bill infor
    ///   - completion: handler result payment
    ///   - errorHandler: handler error in payment
    @objc public static func showObjC(on vc: UIViewController?,
                                with bill: AnyObject?,
                                completion:((AnyObject?) -> ())?,
                                errorHandler:((Error?) -> ())?) {
        _ = self.show(on: vc, with: bill).subscribe(onNext: {
            completion?($0)
        }, onError: { errorHandler?($0) })
    }
}
// MARK: - Transition
final class PaymentPopUpAnimate:NSObject, UIViewControllerAnimatedTransitioning {
    private lazy var overLayView: UIView = {
        let v = UIView(frame: .zero)
        v.alpha = 0
        v.isHidden = true
        v.backgroundColor = .black
        return v
    }()
    var isDismiss: Bool = false
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        containerView.backgroundColor = .clear
        let nextAlpha: CGFloat = isDismiss ? 0 : 0.6
        if !isDismiss {
            self.overLayView.isHidden = false
            containerView.addSubview(self.overLayView)
            overLayView.snp.makeConstraints({
                $0.edges.equalToSuperview()
            })
            if let vTo = transitionContext.view(forKey: .to) {
                containerView.addSubview(vTo)
            }
            containerView.layoutIfNeeded()
        }
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
            self.overLayView.alpha = nextAlpha
        }, completion: { (c) in
            defer {
                transitionContext.completeTransition(true)
            }
            if self.isDismiss {
                self.overLayView.snp.removeConstraints()
                containerView.subviews.forEach({ $0.removeFromSuperview() })
            }
        })
    }
}

final class PaymentPopUpTransitionDelegate:NSObject, UIViewControllerTransitioningDelegate {
    private let animate: PaymentPopUpAnimate = PaymentPopUpAnimate()
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return animate
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        animate.isDismiss = true
        return animate
    }
}

// MARK: - NavigationController
final class ZPNavigationPopupCustomController: UINavigationController {
    override func setNavigationBarHidden(_ hidden: Bool, animated: Bool) {
        var hidden = hidden
        defer {
            super.setNavigationBarHidden(hidden, animated: animated)
        }
        
        guard self.viewControllers.count > 1 else {
            hidden = true
            return
        }
    }
}

// MARK: - Base controller child
class ZPBaseChildPopupPaymentController: UIViewController, PaymentPopupControllerProtocol {
    lazy var backBtn: UIButton = {
        let nButton = self.backButton()
        let barLeft = UIBarButtonItem.init(customView: nButton)
        self.navigationItem.leftBarButtonItem = barLeft
        return nButton
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupEvent()
    }
    
    func setupEvent() {}
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
}

protocol PaymentPopupControllerProtocol {}
extension PaymentPopupControllerProtocol where Self: UIViewController {
    func setupNavigationBar() {
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.barStyle = .default
        // add line
        let vline = UIView.init(frame: .zero)
        vline.backgroundColor = UIColor.line()
        navigationBar?.addSubview(vline)
        vline.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.bottom.equalToSuperview()
            make.centerX.equalToSuperview()
            make.height.equalTo(0.5)
        }
    }
    
    func backButton() -> UIButton {
        let backButton = UIButton(type: .custom)
        backButton.setTitleColor(#colorLiteral(red: 0.1876136363, green: 0.2034130991, blue: 0.2227030993, alpha: 1), for: .normal)
        let titleStr =  "Quay lại"
        backButton.titleLabel?.font = UIFont.sfuiTextRegular(withSize: 16)
        backButton.setTitle(titleStr, for: .normal)
        let backImg = ZaloPayWalletSDKPayment.sharedInstance().getImageWithName("back-ar-hover.png")
        backButton.setImage(backImg?.withRenderingMode(.alwaysTemplate), for: .normal)
        backButton.tintColor = #colorLiteral(red: 0.4470588235, green: 0.4980392157, blue: 0.5490196078, alpha: 1)
        if #available(iOS 11.0, *) {} else {
            backButton.contentEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0)
        }
        backButton.titleEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 0)
        backButton.sizeToFit()
        backButton.contentVerticalAlignment = .center
        backButton.contentHorizontalAlignment = .left
        return backButton
    }
}

// MARK: Extension
extension UINavigationController {
    @objc func clearColorNavigationBarStyle() {
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = true
        view.backgroundColor = UIColor.clear
        navigationBar.backgroundColor = UIColor.clear
        isNavigationBarHidden = false
    }
    
    @objc func defaultNavigationBarStyle() {
        navigationBar.setBackgroundImage(nil, for: .default)
        navigationBar.barTintColor = UIColor.zaloBase()
        navigationBar.tintColor = UIColor.white
        navigationBar.isTranslucent = false
        navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: UIFont.sfuiTextMedium(withSize: 18)]
    }
    @objc class func defaltAppearanceStyle() {
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().barTintColor = UIColor.zaloBase()
        UINavigationBar.appearance().titleTextAttributes = [
            NSAttributedStringKey.foregroundColor : UIColor.white
        ]
        UINavigationBar.appearance().tintColor = UIColor.white
    }
    
}

extension UIViewController {
    func popMe(animated animate: Bool = true) {
        guard let navi = self.navigationController, let idx = navi.viewControllers.index(of: self) else {
            return
        }
        // Check if at last will ignore
        if navi.viewControllers.count == (idx + 1) {
            return
        }
        navi.popToViewController(self, animated: animate)
    }
}




