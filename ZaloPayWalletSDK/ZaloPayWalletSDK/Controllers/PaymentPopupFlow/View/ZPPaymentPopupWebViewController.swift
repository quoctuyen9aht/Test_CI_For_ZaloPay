//
//  ZPPaymentPopupWebViewController.swift
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 12/12/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import WebKit
import SnapKit
import ZaloPayWalletSDKPrivate

final class ZPPaymentPopupWebViewController: UIViewController {
    fileprivate lazy var webView: WKWebView = {
        let config = self.createConfig()
        let wView = WKWebView(frame: .zero, configuration: config)
        self.view.addSubview(wView)
        wView.snp.makeConstraints({
            $0.edges.equalToSuperview()
        })
        wView.navigationDelegate = self
        return wView
    }()
    let eResult: PublishSubject<Bool> = PublishSubject()
    private let disposeBag = DisposeBag()
    var urlString: String?
    var apptransid: String?
    private var logWeb: ZPWalletLogWKWebEvent<WKWebView>?
    // Scale to fit view
    private func createConfig() -> WKWebViewConfiguration {
        let jscript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
        let userScript = WKUserScript(source: jscript, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        let wkUController = WKUserContentController()
        wkUController.addUserScript(userScript)
        let wkWebConfig = WKWebViewConfiguration()
        wkWebConfig.userContentController = wkUController
        return wkWebConfig
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupDisplay()
        setupEvent()
        guard let urlString = urlString,let url = URL.init(string: urlString) else {
            #if DEBUG
                assert(false, "Check URL : \(self.urlString ?? "")")
            #endif
            return
        }
        let request = URLRequest(url: url)
        self.webView.load(request)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if (self.webView.isLoading == true) {
            self.webView.stopLoading()
        }
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    private func setupDisplay() {
        let backButton = UIButton(type: .custom)
        backButton.setIconFont("general_backios", for: .normal)
        backButton.setTitleColor(.white, for: .normal)
        backButton.titleLabel?.font = UIFont.iconFont(withSize: 20)
        if #available(iOS 11, *) {
            backButton.contentEdgeInsets = UIEdgeInsetsMake(0, -12, 0, 0)
        }
        backButton.frame = CGRect(origin: .zero, size: CGSize(width: 20, height: 20))
        backButton.rx.tap.bind { [weak self] in
            // alert
            self?.showAlert()
        }.disposed(by: disposeBag)
        let backBar = UIBarButtonItem(customView: backButton)
        let negativeSeparator = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        negativeSeparator.width = -12
        self.navigationItem.leftBarButtonItems = [negativeSeparator, backBar]
    }
    
    private func showAlert() {
        let message = String(format: stringWarningCancelPayment, "thanh toán")
        let b1 = R.string_ButtonLabel_Skip()!
        let b2 = R.string_ButtonLabel_OK()!
        ZPDialogView.showDialog(with: DialogTypeNotification, title: nil, message: message, buttonTitles: [b1, b2]) { [weak self](idx) in
            guard idx == 1 else {
                return
            }
            self?.eResult.onNext(false)
            self?.eResult.onCompleted()
        }
    }
    
    private func setupEvent() {
        self.logWeb = ZPWalletLogWKWebEvent(with: webView, transid: apptransid)
        self.eResult.subscribe(onCompleted: { [weak self] in
            self?.navigationController?.popViewController(animated: true)
        }).disposed(by: disposeBag)
        self.webView.rx.observe(String.self, "title").observeOn(MainScheduler.instance).bind { [weak self] in
            self?.title = $0
        }.disposed(by: disposeBag)
       
        self.webView.rx
            .observe(Bool.self, "isLoading")
            .observeOn(MainScheduler.instance).bind
            {
                ($0 ?? false) ? ZPProgressHUD.show(withStatus: nil) : ZPProgressHUD.dismiss()
            }.disposed(by: disposeBag)
    }
}
extension ZPPaymentPopupWebViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        let host = navigationAction.request.url?.host
        if host == "payment-complete" {
            decisionHandler(.cancel)
            eResult.onNext(true)
            eResult.onCompleted()
            return
        }
        decisionHandler(.allow)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        ZPDialogView.showDialogWithError(error, handle: nil)
    }
    
    
}
