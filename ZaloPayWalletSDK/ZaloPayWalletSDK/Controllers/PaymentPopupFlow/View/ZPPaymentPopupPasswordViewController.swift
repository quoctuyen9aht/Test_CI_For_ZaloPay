//
//  ZPPaymentPopupPasswordViewController.swift
//  ZaloPay
//
//  Created by Dung Vu on 12/4/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import ZaloPayWalletSDKPrivate
import SnapKit
import ZaloPayAnalyticsSwift

// 194
class ZPPaymentPopupPasswordViewController: ZPBaseChildPopupPaymentController, ZPPaymentPopupAdjustDisplayProtocol {
    @IBOutlet weak var collectionView: UICollectionView?
    @IBOutlet weak var lblError: UILabel?
    @IBOutlet weak var indicatorView: UIView?
    @IBOutlet weak var lblNote: UILabel?
    var method: ZPPaymentMethod?
    var isAppear: Bool = true
    private let disposeBag = DisposeBag()
    fileprivate lazy var presenter = ZPPaymentPopupPasswordPresenter(with: self)
    fileprivate (set) lazy var textField: UITextField = {
        let t = UITextField(frame: .zero)
        t.keyboardType = .numberPad
        t.delegate = self.presenter
        self.view.addSubview(t)
        return t
    }()
    
    private let _state = BehaviorSubject<ZPPaymentPopupState>(value: .password)
    private let _height = BehaviorSubject<CGFloat>(value: 194)
    var eState: Observable<ZPPaymentPopupState> {
        return _state.asObservable()
    }
    var eChangeHeight: Observable<CGFloat> {
        return _height.asObserver()
    }
    
    fileprivate (set) lazy var spinnerView: DGActivityIndicatorView = {
        let v: DGActivityIndicatorView = DGActivityIndicatorView(type:.ballPulse, tintColor: UIColor.subText())
        self.indicatorView?.addSubview(v)
        v.snp.makeConstraints({
            $0.edges.equalToSuperview()
        })
        return v
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.presenter.setupEvent()
    }
    
    func setupNavigationBar() {}
    
    internal override func setupEvent() {
        backBtn.rx.controlEvent(.touchUpInside).filter({[weak self] in self?.presenter.canEditText ?? false }).bind { [weak self] in
            ZPTrackingHelper.shared().trackEvent(.popupsdk_password_back)
            self?.navigationController?.popViewController(animated: true)
        }.disposed(by: disposeBag)
        
        self.rx.methodInvoked(#selector(viewWillAppear(_:))).take(1).delay(0.3, scheduler: MainScheduler.instance).bind { [weak self](_) in
            self?.textField.becomeFirstResponder()
            ZPTrackingHelper.shared().trackEvent(.popupsdk_password_input)
        }.disposed(by: disposeBag)
        
        guard let collectionView = self.collectionView else {
            return
        }
        Observable.just(0..<6)
            .bind(to: collectionView.rx.items(cellIdentifier: "ZPPaymentPopupPasswordCollectionViewCell", cellType: ZPPaymentPopupPasswordCollectionViewCell.self))
            {
                $2.isSelected = false
            }
            .disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isAppear = true
        ZPTrackingHelper.shared().trackScreen(withName:ZPTrackerScreen.Screen_iOS_SDK_Auth)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isAppear = false
        self.textField.resignFirstResponder()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let identify = segue.identifier ?? ""
        guard identify == ZPPaymentPopupMainType.otp.segueName, let controller = segue.destination as? ZPPaymentPopupInputOTPViewController else {
            return
        }
        controller.retryItem = sender as? RetryExcutePaymentItem
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // Catch case hide keyboard
        guard !self.textField.isFirstResponder else {
            return
        }
        self.textField.becomeFirstResponder()
    }

}

