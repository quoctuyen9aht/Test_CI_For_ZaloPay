//
//  ZPPaymentPopupMainViewController.swift
//  ZaloPay
//
//  Created by Dung Vu on 12/5/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import ZaloPayWalletSDKPrivate
import ZaloPayAnalyticsSwift

class ZPPaymentPopupMainViewController: UITableViewController, ZPPaymentPopupAdjustDisplayProtocol, PaymentPopupControllerProtocol {
    private let _state = BehaviorSubject<ZPPaymentPopupState>(value: .main)
    var eState: Observable<ZPPaymentPopupState> {
        return _state.asObservable()
    }
    var eChangeHeight: Observable<CGFloat> {
        return self.mPresenter.eHeight.asObservable()
    }
    
    var isAppear: Bool = true
    weak var rootPresenter: ZPPaymentPopupPresenter?
    fileprivate lazy var mPresenter: ZPPaymentPopupMainPresenter = ZPPaymentPopupMainPresenter(self)
    fileprivate let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        self.tableView.clipsToBounds = false
        self.tableView.estimatedRowHeight = 73
        self.tableView.rowHeight = UITableViewAutomaticDimension
        mPresenter.loadSource()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isAppear = true
        ZPTrackingHelper.shared().trackScreen(withName:ZPTrackerScreen.Screen_iOS_SDK_Main)
        ZPTrackingHelper.shared().trackEvent(.popupsdk_launched)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isAppear = false
    }
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        let identify = segue.identifier ?? ""
        switch identify {
        case ZPPaymentPopupMainType.password.segueName:
            guard let controller = segue.destination as? ZPPaymentPopupPasswordViewController else {
                return
            }
            controller.method = sender as? ZPPaymentMethod
        case ZPPaymentPopupMainType.method.segueName:
            guard let controller = segue.destination as? ZPPaymentPopupChooseMethodViewController else {
                return
            }
            ZPTrackingHelper.shared().trackEvent(.popupsdk_source)
            controller.currentSelect = sender as? ZPPaymentMethodCellDisplay
            controller.eResult.subscribe(onNext: { [weak self] in
                self?.mPresenter.setupDisplay(from: $0)
            }).disposed(by: disposeBag)
        case ZPPaymentPopupMainType.voucher.segueName:
            guard let controller = segue.destination as? ZPPaymentPopupVoucherViewController else {
                return
            }
            ZPTrackingHelper.shared().trackEvent(.popupsdk_voucher)
            controller.eResult.observeOn(MainScheduler.asyncInstance).subscribe(onCompleted: { [weak self] in
                self?.mPresenter.updateWithVoucher()
            }).disposed(by: disposeBag)
        case ZPPaymentPopupMainType.otp.segueName:
            guard let controller = segue.destination as? ZPPaymentPopupInputOTPViewController else {
                return
            }
            controller.retryItem = sender as? RetryExcutePaymentItem
        default:
            break
        }
        
    }
}

// MARK: - Table datasource
extension ZPPaymentPopupMainViewController {
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tCell = self.mPresenter.cellSource[indexPath.item]
        let display = self.mPresenter.source[indexPath.item]
        let cell = tCell.loadCell(from: tableView)
        cell.setup(from: display)
        cell.adjustIndicator(show: self.mPresenter.isOneMethod)
        if let button = cell.getButton() {
            button.isEnabled = self.mPresenter.isCanPayment
            button.rx.tap.takeUntil(cell.rx.methodInvoked(#selector(cell.prepareForReuse))).bind { [weak self] in
                ZPTrackingHelper.shared().trackEvent(.popupsdk_pay)

//                ZPAppFactory.sharedInstance().trackEventId(ZPAnalyticEventAction.popupsdk_pay)
                self?.mPresenter.excutePayment()
            }.disposed(by: disposeBag)
        }
        return cell
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mPresenter.source.count
    }
}
// MARK: - Table delegate
extension ZPPaymentPopupMainViewController {
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        guard let cell = tableView.cellForRow(at: indexPath) as? ZPPaymentPopupMainBaseCell else {
            return
        }
        self.mPresenter.handlerSelect(with: cell.type)
        
    }
}


