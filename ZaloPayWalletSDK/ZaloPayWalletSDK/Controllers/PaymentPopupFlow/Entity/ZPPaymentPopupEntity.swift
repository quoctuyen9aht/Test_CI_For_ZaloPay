//
//  ZPPaymentPopupEntity.swift
//  ZaloPay
//
//  Created by Dung Vu on 12/4/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import Foundation
import ZaloPayWalletSDKPrivate

typealias PaymentItem = (method: ZPPaymentMethod, encryptPassword: String)

struct DisplayType {
    let type: ZPPaymentPopupMainType
    let infor: ZPPaymentMethodCellDisplay
    let voucher: ZPVoucherInfo?
    let amount: Int
}

struct ExcutePaymentItem {
    var card: ZPPaymentInfo?
    var bankCode: String
    init?(with method: ZPPaymentMethodType, bankCode: String, amount: Int, pin: String) {
        guard method == .savedCard || method == .savedAccount || method == .ccDebit else {
            return nil
        }
        self.bankCode = bankCode
        switch method {
        case .savedCard:
            if ZPDataManager.sharedInstance().isCCCard(self.bankCode) {
                let creditCard = ZPCreditCard()
                creditCard.step = ZPCreditCardPaymentStep.ccPaymentStepSubmitPurchase.rawValue
                creditCard.pin = pin
                self.card = creditCard
                self.bankCode = stringCreditCardBankCode
            } else {
                let atmCard = ZPAtmCard()
                atmCard.bankCode = bankCode
                atmCard.amount = amount
                atmCard.step = ZPAtmPaymentStep.atmPaymentStepSubmitPurchase.rawValue
                atmCard.pin = pin
                self.card = atmCard
            }
        case .savedAccount:
            let ibankingCard = ZPIbankingAccount()
            ibankingCard.pin = pin
            ibankingCard.bankCode = bankCode
            ibankingCard.step = ZPAtmPaymentStep.atmPaymentStepSubmitPurchase.rawValue
            ibankingCard.amount = amount
            ibankingCard.method = ZPPaymentMethodType.atm
            self.card = ibankingCard
        case .ccDebit:
            if ZPDataManager.sharedInstance().isCCDebitCard(self.bankCode) {
                let creditCard = ZPCreditCard()
                creditCard.step = ZPCreditCardPaymentStep.ccPaymentStepSubmitPurchase.rawValue
                creditCard.pin = pin
                self.card = creditCard
                self.bankCode = stringDebitCardBankCode
            }
        default:
            break
        }
    }
}

internal func convertBankCode(from method: ZPPaymentMethodType, bankCode: String) -> String {
    var bankCode = bankCode
    switch method {
    case .savedCard:
        if ZPDataManager.sharedInstance().isCCCard(bankCode) {
            bankCode = stringCreditCardBankCode
        }
    case .savedAccount:
        break
    case .ccDebit:
        if ZPDataManager.sharedInstance().isCCDebitCard(bankCode) {
            bankCode = stringDebitCardBankCode
        }
    default:
        break
    }
    return bankCode
}

// This is use for OTP
struct RetryExcutePaymentItem {
    let response: ZPPaymentResponse
    let paymentMethod: ZPPaymentMethod
    let card: ZPPaymentInfo
    let methodType: ZPPaymentMethodType
    let path: String?
    let serverCode: Int32
}

enum ZPPopupError: Error {
    case result(response: ZPPaymentResponse, method: ZPPaymentMethod?)
    case retry(item: RetryExcutePaymentItem)
    case cancel
}
