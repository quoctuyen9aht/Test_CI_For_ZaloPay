//
//  ZPInputVoucherViewController.m
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 7/31/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPInputVoucherViewController.h"
#import "DGActivityIndicatorView.h"
#import <ZaloPayCommon/UILabel+IconFont.h>
#import "ZPPaymentManager.h"
#import "ZPVoucherInfo.h"
@interface ZPInputVoucherViewController ()<UITextFieldDelegate>
@property (strong, nonatomic) UIButton *buttonNext;
@property (strong, nonatomic) UIView *containerView;
@property (strong, nonatomic) UITextField *textField;
@property (strong, nonatomic) UIView *overLayView;
@property (strong, nonatomic) DGActivityIndicatorView *spinnerView;
@property (strong, nonatomic) UIButton *closeBtn;
@property (strong, nonatomic) UILabel *lblTitle;
@property (strong, nonatomic) UILabel *lblMessageError;
@property (strong, nonatomic) UILabel *lblValue;
@end


static NSString *const kHashtag = @"#";
static NSInteger const kMinimumInput = 2;
static NSInteger const kMaximumInput = 10;
@implementation ZPInputVoucherViewController

- (instancetype)init {
    self = [super init];
    self.voucher = [RACSubject new];
    return self;
}

- (void) setupView {
    // Create container
    
    self.containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 193)];
    self.containerView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.containerView];
    
    // Add constraint
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(0);
        make.left.equalTo(0);
        make.size.equalTo(self.containerView.frame.size);
    }];
    
    // setup close button
    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    closeBtn.titleLabel.font = [UIFont iconFontWithSize:16];
    [closeBtn setIconFont:@"red_delete" forState:UIControlStateNormal];
    [closeBtn setTitleColor:[UIColor subText] forState:UIControlStateNormal];
    [self.containerView addSubview:closeBtn];
    [closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.left.equalTo(0);
        make.size.equalTo(CGSizeMake(40, 40));
    }];
    self.closeBtn = closeBtn;
    
    // setup title
    UILabel *titlelbl = [[UILabel alloc] initWithFrame:CGRectZero];
    titlelbl.font = [UIFont SFUITextRegularWithSize:16];
    [self.containerView addSubview:titlelbl];
    
    [titlelbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(31);
        make.centerX.equalTo(0);
        make.width.greaterThanOrEqualTo(0);
        make.height.greaterThanOrEqualTo(0);
    }];
    self.lblTitle = titlelbl;
    self.lblTitle.text = [R string_Voucher_input_title];
    // Text Field
    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectZero];
    textField.delegate = self;
    [self.containerView addSubview:textField];
    
    textField.borderStyle = UITextBorderStyleNone;
    textField.font = [UIFont SFUITextRegularWithSize:36];
    textField.textAlignment = NSTextAlignmentCenter;
    textField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    [self.containerView addSubview:textField];
    
    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(80);
        make.centerX.equalTo(0).offset(8);
        make.width.equalTo(self.containerView.frame.size.width - 20);
        make.height.equalTo(43);
    }];
    self.textField = textField;
    
    // Add a subview to layout center error
    UIView *subView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.containerView addSubview:subView];
    [subView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textField.mas_bottom);
        make.width.equalTo(self.containerView.frame.size.width - 16);
        make.bottom.equalTo(0);
        make.centerX.equalTo(0);
    }];
    
    // Add buttton Next
    UIButton *btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    btnNext.titleLabel.font = [UIFont SFUITextRegularWithSize:16];
    btnNext.titleLabel.textAlignment = NSTextAlignmentCenter;
    [subView addSubview:btnNext];
    
    [btnNext mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(-20);
        make.centerX.equalTo(0);
    }];
    self.buttonNext = btnNext;
    [self setupUIButtonNext];
    
   
    // Add Spinner view
    self.spinnerView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor subText]];
    _spinnerView.frame = CGRectMake(0, 0, 36, 10);
    [subView addSubview:_spinnerView];
    [_spinnerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(btnNext.mas_centerX);
        make.centerY.equalTo(btnNext.mas_centerY);
        make.size.equalTo(_spinnerView.frame.size);
    }];
    
    // Add label error
    UILabel *lblError = [[UILabel alloc] initWithFrame:CGRectZero];
    lblError.numberOfLines = 2;
    lblError.textAlignment = NSTextAlignmentCenter;
    lblError.font = [UIFont SFUITextRegularItalicWithSize:13];
    lblError.textColor = [UIColor zp_red];
    [subView addSubview:lblError];
    
    [lblError mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(-30);
        make.centerX.equalTo(0);
        make.width.lessThanOrEqualTo([UIScreen mainScreen].bounds.size.width - 16);
        make.height.greaterThanOrEqualTo(0);
    }];
    
    self.lblMessageError = lblError;
    [self configHashtag];
}

- (void) configHashtag {
    // Add label to get value
    UILabel *lblValue = [[UILabel alloc] initWithFrame:CGRectZero];
    lblValue.font = [UIFont SFUITextRegularWithSize:36];
    lblValue.textColor = [UIColor clearColor];
    [self.containerView insertSubview:lblValue belowSubview:self.textField];
    [lblValue mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.textField.mas_centerX);
        make.centerY.equalTo(self.textField.mas_centerY);
        make.size.greaterThanOrEqualTo(0);
    }];
    self.lblValue = lblValue;
    // Add label hashtag
    UILabel *lblHastag = [[UILabel alloc] initWithFrame:CGRectZero];
    lblHastag.font = [UIFont SFUITextRegularWithSize:43];
    lblHastag.textColor = [UIColor subText];
    [self.containerView insertSubview:lblHastag belowSubview:self.textField];
    [lblHastag mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.textField.mas_centerY);
        make.right.equalTo(lblValue.mas_left);
        make.size.greaterThanOrEqualTo(0);
    }];
    lblHastag.text = kHashtag;
    
}

- (void) setupUIButtonNext {
    NSString *icon = [UILabel iconCodeWithName:@"general_arrowright"];
    NSString *title = [NSString stringWithFormat:@"%@ %@",[R string_Voucher_button_next],icon];
    
    NSMutableAttributedString *attN = [[NSMutableAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName : [UIColor zaloBaseColor]}];
    NSMutableAttributedString *attD = [[NSMutableAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName : [UIColor colorWithHexValue:0xc7cad3]}];
    NSRange r = [title rangeOfString:icon];
    [attN addAttributes:@{NSFontAttributeName : [UIFont zaloPayWithSize:15]} range:r];
    [attD addAttributes:@{NSFontAttributeName : [UIFont zaloPayWithSize:15]} range:r];
    
    [self.buttonNext setAttributedTitle:attN forState:UIControlStateNormal];
    [self.buttonNext setAttributedTitle:attD forState:UIControlStateDisabled];
    self.buttonNext.enabled = NO;
}

- (void) addOverLayView {
    self.overLayView = [[UIView alloc] initWithFrame:CGRectZero];
    self.overLayView.backgroundColor = [UIColor blackColor];
    self.view.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.overLayView];
    [self.overLayView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.left.equalTo(0);
        make.right.equalTo(0);
        make.bottom.equalTo(0);
    }];
}

- (void)setupEvent {
    @weakify(self);
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:nil action:nil];
    [self.overLayView addGestureRecognizer:tapGesture];
    RACSignal *eTap = [tapGesture rac_gestureSignal];
    RACSignal *eClose = [self.closeBtn rac_signalForControlEvents:UIControlEventTouchUpInside];
    [[[RACSignal merge:@[eTap, eClose]] filter:^BOOL(id value) {
        @strongify(self)
        return !self.spinnerView.animating;
    }] subscribeNext:^(id x) {
        @strongify(self);
        [self showAlert];
    }];
    
    RACSignal *eShowKeyBoard = [[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillShowNotification object:nil];
    RACSignal *eHidekeyBoard = [[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillHideNotification object:nil];
    
    [[RACSignal merge:@[eShowKeyBoard, eHidekeyBoard]] subscribeNext:^(NSNotification *notify) {
        if (![notify isKindOfClass:[NSNotification class]]) {
            return;
        }
        @strongify(self);
        BOOL isHidden = [notify.name isEqualToString:UIKeyboardWillHideNotification];
        NSDictionary *info = notify.userInfo;
        NSTimeInterval t = 0;
        CGFloat f = 0;
        if (info) {
            NSNumber *duration = info[UIKeyboardAnimationDurationUserInfoKey];
            t = duration ? [duration doubleValue] : 0;
            f = [info[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
        }
        
        CGFloat nextAlpha = isHidden ? 0 : 0.6;
        CGFloat nextHeight = isHidden ? -self.containerView.bounds.size.height : f;
        [UIView animateWithDuration:t animations:^{
            self.overLayView.alpha = nextAlpha;
            self.containerView.transform = CGAffineTransformMakeTranslation(0, -nextHeight);
        }];
    }];
    
    // Dismiss
    [[self rac_signalForSelector:@selector(viewWillDisappear:)] subscribeNext:^(id x) {
        @strongify(self);
        [self.textField resignFirstResponder];
    }];
    
    // Enable Button
    [[[self.textField rac_textSignal] filter:^BOOL(NSString *value) {
        return [value isKindOfClass:[NSString class]];
    }] subscribeNext:^(NSString *x) {
        @strongify(self);
        self.lblMessageError.hidden = YES;
        self.buttonNext.hidden = [x length] == 0;
        self.buttonNext.enabled = [x length] >= kMinimumInput;
    }];
    
    [[[self.buttonNext rac_signalForControlEvents:UIControlEventTouchUpInside] filter:^BOOL(id value) {
        @strongify(self)
        return !self.spinnerView.animating;
    }] subscribeNext:^(id x) {
        @strongify(self);
        [self requestVoucherInformation];
    }];
    
    // Show key board
    [[self rac_signalForSelector:@selector(viewWillAppear:)] subscribeNext:^(id x) {
        @strongify(self);
        [self.textField becomeFirstResponder];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addOverLayView];
    [self setupView];
    [self setupEvent];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - text field
- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (self.spinnerView.animating) {
        return NO;
    }
    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSInteger newLenght = [newText length];
    BOOL isChange = newLenght <= kMaximumInput;
    if (isChange) {
        // Will update hashTag
        self.lblValue.text = newText;
    }
    return isChange;
}

#pragma mark - request
- (void) requestVoucherInformation {
    @weakify(self);
    [[[[[[[RACSignal return:@"Loading"] doNext:^(id x) {
        @strongify(self);
        self.buttonNext.hidden = YES;
        [self.spinnerView startAnimating];
        self.lblMessageError.hidden = YES;
    }] flattenMap:^__kindof RACSignal * _Nullable(NSString  *_Nullable s) {
        @strongify(self);
        return [ZPPaymentManager submitVoucherWith:self.appTransId
                                       voucherCode:self.textField.text
                                             appId:self.appId
                                            amount:self.amount];
    }] deliverOn:[RACScheduler mainThreadScheduler]] doError:^(NSError *error) {
        @strongify(self);
        [self.spinnerView stopAnimating];
    }] doCompleted:^{
        @strongify(self);
        [self.spinnerView stopAnimating];
    }] subscribeNext:^(NSDictionary *result) {
        @strongify(self);
        if ([result isKindOfClass:[NSDictionary class]]) {
            ZPVoucherInfo *info = [ZPVoucherInfo fromDic:result];
            info.voucherCode = self.textField.text;
            info.appTransId = self.appTransId;
            [self.voucher sendNext:info];
        }else {
            [self.voucher sendNext:nil];
        }
        [self.voucher sendCompleted];
    } error:^(NSError *error) {
        @strongify(self);
        //  using for test
//        NSDictionary *result = @{@"vouchersig": @"jdusdjshjdhs",
//                                 @"campaignid": @(4767367),
//                                 @"discountamount": @(10000),
//                                 @"usevouchertime" : @(15783839)};
//        ZPVoucherInfo *info = [[ZPVoucherInfo alloc] initDataWithDict:result code:self.textField.text];
//        [self.voucher sendNext:info];
//        [self.voucher sendCompleted];
//        return;
        
        NSString *message = @"";
        self.buttonNext.enabled = NO;
        if (error.code == NSURLErrorNotConnectedToInternet) {
            message = [R string_Home_InternetConnectionError];
        }else {
            NSDictionary *info = [error userInfo];
            if ([info isKindOfClass:[NSDictionary class]]) {
                message = [info stringForKey:@"returnmessage" defaultValue:@""];
            }
        }
        self.lblMessageError.hidden = [message length] == 0;
        self.lblMessageError.text = message;
        self.buttonNext.hidden = YES;
    }];
}

#pragma mark - alert
- (void) showAlert {
    @weakify(self)
    [ZPDialogView showDialogWithType:DialogTypeNotification
                             message:[R string_Voucher_message_alert]
                   cancelButtonTitle:[R string_ButtonLabel_OK]
                   ortherButtonTitle:@[[R string_ButtonLabel_Cancel]]
                      completeHandle:^(int buttonIndex, int cancelButtonIndex)
    {
        @strongify(self);
        if (buttonIndex != cancelButtonIndex) {
            [self.textField becomeFirstResponder];
        } else {
            [self.voucher sendNext:nil];
            [self.voucher sendCompleted];
        }
    }];
}

#pragma mark - public function
// OUPUT -> Dictionary
+ (RACSignal *)showInputVoucherOn:(UIViewController *)controller
                            using:(NSString *)appTransId
                             with:(NSInteger)appId
                           amount:(long) amount
{
    if (![controller isKindOfClass:[UIViewController class]]) {
        return [RACSignal empty];
    }
    
    if ([appTransId length] == 0) {
        return [RACSignal return:nil];
    }
    
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        ZPInputVoucherViewController *vc = [ZPInputVoucherViewController new];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        vc.appId = appId;
        vc.appTransId = appTransId;
        vc.amount = amount;
        [vc.voucher subscribe:subscriber];
        [controller presentViewController:vc animated:YES completion:nil];
        
        return [RACDisposable disposableWithBlock:^{
            [vc dismissViewControllerAnimated:YES completion:nil];
        }];
        
    }];
}

@end
