//
//  ZPTableViewDelegateProxy.h
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 7/3/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ZPPaymentHandler.h"

@interface ZPTableViewDelegateProxy : NSObject <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, weak) ZPPaymentHandler *handler;
@end
