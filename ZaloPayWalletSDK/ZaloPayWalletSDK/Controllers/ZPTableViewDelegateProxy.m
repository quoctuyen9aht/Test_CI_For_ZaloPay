//
//  ZPTableViewDelegateProxy.m
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 7/3/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPTableViewDelegateProxy.h"

@implementation ZPTableViewDelegateProxy

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.handler numberOfSectionsInTableView:tableView];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.handler tableView:tableView numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.handler tableView:tableView cellForRowAtIndexPath:indexPath];
    if (!cell) {
        cell = [UITableViewCell defaultCellForTableView:tableView];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self.handler tableView:tableView heightForRowAtIndexPath:indexPath];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [self.handler tableView:tableView viewForFooterInSection:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return [self.handler tableView:tableView heightForFooterInSection:section];
}

- (void)tableView:(UITableView *)tableView willDisplayFooterView:(nonnull UIView *)view forSection:(NSInteger)section {
    [self.handler tableView:tableView willDisplayFooterView:view forSection:section];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.handler tableView:tableView didSelectRowAtIndexPath:indexPath];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.handler tableView:tableView willDisplayCell:cell forRowAtIndexPath:indexPath];
}
@end
