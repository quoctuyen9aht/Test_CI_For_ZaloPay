//
//  ZPContainerViewController.h
//  ZaloPayWalletSDK
//
//  Created by Nguyen Xuan Phung on 4/23/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPPaymentHandler.h"
#import "ZPVoucherViewWithButton.h"

@class ZPVoucherViewWithButton;
@class ZPMethodsHandler;

@interface ZPContainerViewController : BaseViewController
@property (strong, nonatomic) UITableView *zpw_tableMain;
@property (nonatomic, strong) UIButton *actionButton;
@property (strong, nonatomic) ZPDataManager * dataManager;
@property (strong, nonatomic) ZPBill * bill;
@property (strong, nonatomic) NSMutableArray * paymentMethods;
@property (weak, nonatomic) id<ZPPaymentViewControllerDelegate> delegate;
@property (nonatomic,strong) ZPVoucherViewWithButton *zpVoucherViewWithButton;
@property (nonatomic,strong) ZPMethodsHandler *controller;

- (void)showSDKKeyboard;

- (void)dismissSDKKeyboard;

- (void)calculateFeeWithMethod:(ZPPaymentMethod *)method;

- (void)paymentControllerNeedUpdateLayouts;

@end
