//
// Created by Huu Hoa Nguyen on 3/23/18.
// Copyright (c) 2018 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ZPResultViewModel;

@interface ZPResultRowView : UITableViewCell
- (instancetype _Nonnull)initWithReuseIdentifier:(NSString *_Nonnull)reuseIdentifier;
@property (strong, nonatomic) ZPResultViewModel* _Nullable  model;
@end

@interface ZPResultRowBlankView : ZPResultRowView
@end

@interface ZPResultRowLineView : ZPResultRowView
@end

@interface ZPResultFooterView : UIView
- (void)addTouchTarget:(nullable id)target action:(SEL _Nullable)action;
@end
