//
//  ZPOrderHeaderView.h
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/14/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class ZPIconFontImageView;
@interface ZPOrderHeaderView : UIView
@property (nonatomic, strong) UILabel *labelAmount;
@property (nonatomic, strong) UILabel *labelMessage;
@property (nonatomic, strong) ZPIconFontImageView *bottomLine;
- (id)initWithAmount:(long)amount message:(NSString *)message;
- (id)initWithUsingInPopupSDK:(long)amount;
- (void)updateAmountTitle:(long)amount;
@end
