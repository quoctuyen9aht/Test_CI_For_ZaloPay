//
//  ZPResultHandler.m
//  ZaloPayWalletSDK
//
//  Created by Thi La on 4/26/16.
//  Copyright © 2016-present VNG. All rights reserved.
//

#import "ZPResultHandler.h"
#import "ZPDataManager.h"
#import "ZPResultHeaderView.h"
#import "ZPProgressHUD.h"
#import "ZaloPayWalletSDKPayment.h"
#import "ZPResultSupportView.h"
#import "ZPResultViewModel.h"
#import "ZPResultRowTitleValueView.h"
#import "ZPResultMoneyTranferView.h"
#import "ZPBill+ExtInfo.h"
#import "ZaloPayWalletSDKLog.h"
#import <ZaloPayWalletSDK/ZaloPayWalletSDK-Swift.h>
#import <ZaloPayAnalyticsSwift/ZaloPayAnalyticsSwift-Swift.h>
#import "ZPVoucherInfo.h"
#import <AppsFlyerLib/AppsFlyerTracker.h>

@interface ZPResultHandler ()
@property (strong, nonatomic) ZPDataManager * dataManager;
@property (nonatomic, strong)  NSArray *dataSource;
@end

@implementation ZPResultHandler

- (void)dealloc {
    DDLogInfo(@"dealloc");
    _delegate = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)initialization {
    DDLogInfo(@"init ZPResultHandler");
    self.dataManager = [ZPDataManager sharedInstance];
    self.dataSource =  [self createDataSource];

    
    UINib *nibResult = [self.dataManager nibNamed:@"ZPResultSupportView"];
    UINib *nibHeader = [self.dataManager nibNamed:@"ZPResultHeaderView"];
#ifdef DEBUG
    NSAssert(nibResult != nil ,@"Fail load nibResult");
    NSAssert(nibHeader != nil, @"Fail load nibHeader");
#endif
    [self.mTableView registerNib:nibHeader
          forCellReuseIdentifier:@"ZPResultHeaderView"];
    [self.mTableView registerNib:nibResult
          forCellReuseIdentifier:@"ZPResultSupportView"];
    self.mTableView.separatorColor = [UIColor clearColor];
    self.mTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if ([ZPProgressHUD isVisible]) {
        [ZPProgressHUD dismiss];
    }
    self.mTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.mTableView.separatorColor = [UIColor hex_0xe3e6e7];
    self.mTableView.scrollEnabled = YES;
    self.mTableView.estimatedRowHeight = 80;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateStatusWithNotify:)
                                                 name:ZPInApp_Notification_Key
                                               object:nil];
    [self trackResultAction];
}

- (void)trackResultAction {
    if (self.bill.transType == ZPTransTypeWalletTopup) {
        [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionBalance_addcash_result];
        return;
    }
    if (self.bill.transType == ZPTransTypeWithDraw) {
        [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionBalance_withdraw_result];
        return;
    }
    if (self.bill.transType == ZPTransTypeTranfer) {
        [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionMoneyreceive_result];
        return;
    }
}

- (void)trackEventAppsflyer {
    NSString *eventName = (self.bill.transType == ZPTransTypeAtmMapCard) ? @"zp_mapcard" : @"zp_payment";
    NSDictionary *value = @{@"zp_returncode": self.responseResult.appsFlyerValue};
    id<ZPWalletDependenceProtocol> dependency = [ZaloPayWalletSDKPayment sharedInstance].appDependence;
    if (!dependency) {
        return;
    }
    [dependency trackAppsflyer:eventName eventValues:value];
}

- (void)trackEventAction:(BOOL)success {
    if(_isPaymentSuccess) {
        [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionSdk_success];
    } else {
        [[ZPTrackingHelper shared] trackEvent:ZPAnalyticEventActionSdk_fail];
    }
}

- (void)setIsPaymentSuccess:(BOOL)success {
    _isPaymentSuccess = success;
    [self trackEventAction:success];
    [self trackEventAppsflyer];
    if (_isPaymentSuccess) {
        ZPDataManager *manager = [ZPDataManager sharedInstance];
        if (manager.tempSavedCard) {
            [manager.addNewCardSignal sendNext:manager.tempSavedCard];
            manager.tempSavedCard = nil;
        }
    }
}

#pragma mark - DataSource

- (ZPResultRowViewModel *)merchantName {
    return  [[ZPResultRowViewModel alloc] initWithTitle:[R string_Pay_Bill_Merchant_Name]
                                                  value:[self.bill appName]];
}

- (ZPResultRowViewModel *)resultTime {
    long long reqdate = _responseResult.reqdate;
    NSDate *date = reqdate > 0 ? [NSDate dateWithTimeIntervalSince1970:reqdate/1000.0] : [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    NSString *timeString = [dateFormatter stringFromDate:date];
    return [[ZPResultRowViewModel alloc] initWithTitle:[R string_Time_Title] value:timeString];
}

- (ZPResultRowViewModel *)resultTransId {
    if (_responseResult.zpTransID.length == 0 || [_responseResult.zpTransID isEqualToString:@"0"]) {
        return nil;
    }
    NSMutableString *zpTransID = [NSMutableString stringWithString:_responseResult.zpTransID];
    if ([zpTransID length] > 6) {
        [zpTransID insertString:@"-" atIndex:6];
    }
    return [[ZPResultRowViewModel alloc] initWithTitle:[R string_Feedback_TransactionId] value:zpTransID];
}

- (ZPResultRowViewModel *)feeModel {
    NSString *feeText = self.feeCharge > 0 ? [@(self.feeCharge) formatCurrency] : [R string_Pay_Bill_Fee_Free];
    return [[ZPResultRowViewModel alloc] initWithTitle:[R string_Pay_Bill_Fee_Title] value:feeText];
}

- (void)appendBillExt:(NSMutableArray *)data {
    NSArray *infos = [self.bill extFromItem];
    if (infos.count <= 0) {
        return;
    }
    NSArray *newArray = [infos map:^ZPResultRowViewModel*(ZPBillExtraInfo* obj) {
        return [[ZPResultRowViewModel alloc] initWithTitle:obj.extraKey value:obj.extraValue];
    }];
    [data addObjectsFromArray:newArray];
}

- (NSArray *)billInfoData {
    NSMutableArray *_return = [NSMutableArray new];
    if (self.bill.transType == ZPTransTypeAtmMapCard) {
        [_return addObjectNotNil:[self resultTime]];
        [_return addObjectNotNil:[self resultTransId]];
        return _return;
    }
    
    [_return addObjectNotNil:[self merchantName]];
    [self appendBillExt:_return];
    if (_isPaymentSuccess) {
        if (self.bill.voucherInfo || self.bill.promotionInfo) {
            ZPResultRowViewModel *originalAmount = [[ZPResultRowViewModel alloc] initWithTitle: stringTitleOriginalAmount value:[@(self.bill.amount) formatCurrency]];
            ZPResultRowViewModel *discountAmount = [[ZPResultRowViewModel alloc] initWithTitle:stringTitleDiscountAmount value:[@(-[self.bill finalDiscountAmount]) formatCurrency]];
            [_return addObject:originalAmount];
            [_return addObject:discountAmount];
        }
    }
    [_return addObjectNotNil:[self feeModel]];
    [_return addObjectNotNil:[self resultTime]];
    [_return addObjectNotNil:[self resultTransId]];
    return _return;
}

- (NSArray *)createDataSource {
    NSMutableArray *dataSource = [NSMutableArray new];

    ZPResultHeaderViewModel *headerModel = [ZPResultHeaderViewModel new];
    headerModel.isPaymentSuccess = self.isPaymentSuccess;
    headerModel.paymentResponse =  _responseResult;
    headerModel.amount = self.totalCharge;
    headerModel.bill = self.bill;
    headerModel.messageCustomAfterSuccess = [[ZaloPayWalletSDKPayment sharedInstance].appDependence getMessageLinkCardResult:_responseResult.first6NumberCard];
    
    [dataSource addObject:headerModel];

    [dataSource addObject:[ZPResultLineViewModel new]];
    [dataSource addObject:[ZPResultBlankViewModel new]];
    
    if (self.isPaymentSuccess) {
        if (self.bill.transType == ZPTransTypeTranfer) {
            ZPResultTranferMoneyDetailViewModel *transferModel = [ZPResultTranferMoneyDetailViewModel new];
            transferModel.userAvatar = [self.bill.appUserInfo stringForKey:@"currentUserAvatarUrl"];
            transferModel.friendAvatar = [self.bill.appUserInfo stringForKey:@"avatar"];
            [dataSource addObject:transferModel];
            [dataSource addObject:[ZPResultBlank4pxViewModel new]];
        }
        
        [dataSource addObjectsFromArray:[self billInfoData]];
        return dataSource;
    }
    //default
    [dataSource addObjectsFromArray:[self billInfoData]];
    if (self.responseResult.suggestAction.count == 0) {
        [dataSource addObject:[ZPResultBlankViewModel new]];
        [dataSource addObject:[ZPResultSupportViewModel new]];
        return dataSource;
    }
    
    [dataSource addObject:[ZPResultBlankViewModel new]];
    NSArray *actionList = @[[ZPResultSupportViewModel new],[ZPResultUpdateProfileLevel3ViewModel new]];
    for (NSNumber *number in self.responseResult.suggestAction) {
        if (![number isKindOfClass:[NSNumber class]]) {
            continue;
        }
        NSInteger index = [number integerValue];
        if (index <= actionList.count) {
            ZPResultViewModel *model = [actionList safeObjectAtIndex:index - 1];
            if (model) {
                [dataSource addObject:model];
            }
        }
    }
    
    return dataSource;
}

#pragma mark - UITableViewDelegate vs DataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DDLogInfo(@"_isPaymentSuccess %d", _isPaymentSuccess);
    DDLogInfo(@"_responseResult %@", _responseResult.zpTransID);
    // notify result before close payment sdk
    ZPResultViewModel *model = [_dataSource safeObjectAtIndex:indexPath.row];
    
    ZPResultRowView *tableViewCell;
    @try {
        tableViewCell = [tableView dequeueReusableCellWithIdentifier:model.reuseIdentifier];
    }
    @catch (NSException *e){
        NSString *reason = [e reason];
        DDLogInfo(@"identifier: %@, reason %@",model.reuseIdentifier, reason);
#ifdef DEBUG
        NSAssert(NO, reason);
#endif
    }
    
    if (!tableViewCell) {
        // initialize new cell
        tableViewCell = [self createResultRowView:model];
    }

    [tableViewCell setModel:model];
    return tableViewCell;
}

- (ZPResultRowView *)createResultRowView:(ZPResultViewModel *)model {
    ZPResultRowView *rowView = nil;
    switch (model.cellType) {
        case ZPResultCellTypeBlank4px:
        case ZPResultCellTypeBlank:
            rowView = [[ZPResultRowBlankView alloc] initWithReuseIdentifier:model.reuseIdentifier];
            break;
        case ZPResultCellTypeResultLine:
            rowView = [[ZPResultRowLineView alloc] initWithReuseIdentifier:model.reuseIdentifier];
            break;
        case ZPResultCellTypeTransferMoneyDetail:
            rowView = [[ZPResultMoneyTranferView alloc] initWithReuseIdentifier:model.reuseIdentifier];
            break;
        case ZPResultCellTypeResultDetails:
            rowView = [[ZPResultRowTitleValueView alloc] initWithReuseIdentifier:model.reuseIdentifier];
            break;
        case ZPResultCellTypeResult:
            rowView = [[ZPResultHeaderView alloc] initWithReuseIdentifier:model.reuseIdentifier];
            break;
        default:
            NSAssert(NO, @"Check this!!!!");
    }
    return rowView;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    CGFloat viewHeight = [self getFooterViewHeight];
    ZPResultFooterView *footerView = [[ZPResultFooterView alloc] initWithFrame:CGRectMake(0, 0, self.mTableView.frame.size.width, viewHeight)];
    [footerView addTouchTarget:self action:@selector(processPayment)];
    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return [self getFooterViewHeight];
}

- (CGFloat)getFooterViewHeight {
    return _isPaymentSuccess ? 74 : 106;
}

- (void)processPayment {
    DDLogInfo(@"processPayment balance %lld", _responseResult.balance);
    if ([self.delegate respondsToSelector:@selector(paymentControllerDidFinishWithResponse:)]) {
        [self.delegate paymentControllerDidFinishWithResponse:_responseResult];
    }
}

- (void)showProgressHUD {
    [ZPProgressHUD showWithStatus:[[ZPDataManager sharedInstance] messageForKey:@"message_announce_in_processing"]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZPResultViewModel *model = [_dataSource safeObjectAtIndex:indexPath.row];
    if (model == nil) {
        return 0;
    }
    return [model height];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataSource.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ZPResultViewModel *model = [_dataSource safeObjectAtIndex:indexPath.row];
    ZPResultCellType type = model.cellType;
    
    if (type == ZPResultCellTypeUpdateLevel3 && [self.delegate respondsToSelector:@selector(paymentControllerDidClose:data:)]) {
        [self.delegate paymentControllerDidClose:ZP_NOTIF_UPGRADE_LEVEL_3 data:nil];
        return;
    }
    
    if (type == ZPResultCellTypeSupport && [self.delegate respondsToSelector:@selector(paymentControllerNeedShowSupportAction)]) {
        [self.delegate paymentControllerNeedShowSupportAction];
        return;
    }
}

- (void)updateStatusWithNotify:(NSNotification *)notification {
    if (!notification.object) {
        return;
    }

    if (_isPaymentSuccess ||  _responseResult.exchangeStatus != kZPZaloPayCreditStatusCodeUnidentified) {
        return;
    }
    
    if (![[notification.object stringForKey:@"transid"] isEqualToString:self.responseResult.zpTransID]) {
        return;
    }
    
    int type = [notification.object intForKey:@"type"];
    id<ZPWalletDependenceProtocol> helper = [ZaloPayWalletSDKPayment sharedInstance].appDependence;
    if ([helper isNotificationWithUpdateBalance:type]) {
        // notification post ở background -> nhận dc ở background.
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *billType = [[ZPDataManager sharedInstance] getTitleByTranstype:self.bill.transType];
            NSString *title =  [NSString stringWithFormat:@"%@ %@",billType,stringResultSuccess];
            self.mTableView.viewController.title = title;
            self.responseResult.errorCode = 1;
            self.responseResult.exchangeStatus = kZPZaloPayCreditStatusCodeSuccess;
            self.responseResult.message = [NSString stringWithFormat:@"%@ %@",[self.dataManager getTitleByTranstype:self.bill.transType], stringResultSuccess];
            self.isPaymentSuccess = YES;
            if ([self.delegate respondsToSelector:@selector(paymentControllerShouldNotifyPaymentSuccess)]) {
                [self.delegate paymentControllerShouldNotifyPaymentSuccess];
            }
        });
    }
}

@end
