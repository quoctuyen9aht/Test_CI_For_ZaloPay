//
//  ZPContainerViewController+Voucher.m
//  ZaloPayWalletSDK
//
//  Created by Dung Vu on 8/2/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPContainerViewController+Voucher.h"
#import "ZPOrderHeaderView.h"
#import "ZPBill.h"
#import "ZPPaymentManager.h"
#import "ZaloPaySDKUtil.h"
#import "ZPResourceManager.h"
#import "ZaloMobilePaymentSDK+ChargeableMethod.h"
#import "ZaloPayWalletSDKPayment.h"
#import "ZPMethodsHandler.h"
#import <ZaloPayWalletSDK/ZaloPayWalletSDK-Swift.h>


@implementation ZPContainerViewController(Voucher)

- (void)updateZPOrderHeaderViewWithVoucherInfo:(ZPVoucherInfo *)result {
    if (result == nil) {
        return;
    }
    self.bill.voucherInfo = result;
    [self updateHeaderAndTableView];
}
- (void)updateZPOrderHeaderViewWithPromotionInfo:(ZPPromotionInfo *)result {
    if (result == nil) {
        return;
    }
    self.bill.promotionInfo = result;
    [self updateHeaderAndTableView];
}

- (void)deleteCurrentVoucherAndPromotionWith:(ZPBill *) bill {
    bill.voucherInfo = nil;
    bill.promotionInfo = nil;
    self.bill = bill;
    [self updateHeaderAndTableView];
}

- (void)updateHeaderAndTableView {
    ZaloMobilePaymentSDK *sdk = [ZaloPayWalletSDKPayment sharedInstance].zaloPaySDK;
    NSArray *methods = [sdk chargebleMethods:self.bill methodType:ZPPaymentMethodTypeMethods];
    @synchronized (self.paymentMethods) {
        [self.paymentMethods removeAllObjects];
        [self.paymentMethods addObjectsFromArray:methods];
    }
    [self.controller reloadUI];
}

@end
