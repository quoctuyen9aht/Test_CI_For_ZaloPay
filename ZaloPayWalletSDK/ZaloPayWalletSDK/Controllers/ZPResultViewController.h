//
//  ZPResultViewController.h
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/14/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPContainerViewController.h"
@class ZPResultHandler;

@interface ZPResultViewController : BaseViewController
@property (nonatomic, strong) ZPPaymentResponse *responseNotifySupport;
@property (nonatomic, strong) ZPBill *bill;

@property (weak, nonatomic) id<ZPPaymentViewControllerDelegate> delegate;
@property (nonatomic, strong) ZPResultHandler *resultHandler;

- (id)initWithResultHandler:(ZPResultHandler *)handler;
@end
