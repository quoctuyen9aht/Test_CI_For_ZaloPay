//
//  ZPResultViewController.m
//  ZaloPayWalletSDK
//
//  Created by bonnpv on 6/14/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPResultViewController.h"
#import "ZPResultHandler.h"
#import "ZPCustomActionSheet.h"
#import "ZPDataManager.h"
#import "NSString+ZPExtension.h"
#import "ZPBill.h"
#import "ZPResourceManager.h"
#import "ZaloPaySDKUtil.h"
#import "ZaloPayWalletSDKLog.h"
#import <ZaloPayAnalyticsSwift/ZaloPayAnalyticsSwift-Swift.h>

@interface ZPResultViewController ()<ZPResultHandlerDelegate, ChooseSupportActionSheetDelegate>
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UITableView *tableView;

@end

@implementation ZPResultViewController

- (id)initWithResultHandler:(ZPResultHandler *)handler {
    self = [super init];
    if (self) {
        self.resultHandler = handler;
        self.resultHandler.delegate = self;
        self.bill = _resultHandler.bill;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = [self configTitleResult];
    self.automaticallyAdjustsScrollViewInsets =  NO;
    [self addTableView];
    self.resultHandler.mTableView = self.tableView;
    [self.resultHandler initialization];
    self.tableView.delegate = self.resultHandler;
    self.tableView.dataSource = self.resultHandler;

    [self notifyPaymentResult];
    [[ZPTrackingHelper shared].eventTracker trackScreen:[self screenName]];
}

- (NSString *)screenName {
    if (self.bill.transType == ZPTransTypeAtmMapCard) {
        return ZPTrackerScreen.Screen_iOS_Bank_Result;
    }
    return ZPTrackerScreen.Screen_iOS_SDKF_Main;
}
- (BOOL)isAllowSwipeBack {
    return NO;
}

- (NSString *)configTitleResult {
    NSString *title = [[ZPDataManager sharedInstance] getTitleByTranstype:self.bill.transType];
    if (self.bill.transType == ZPTransTypeAtmMapCard) {
        title = stringTitleMapcard;
    }
    if ([self.bill isKindOfClass:[ZPIBankingAccountBill class]]) {
        ZPIBankingAccountBill *bill = (ZPIBankingAccountBill*)self.bill;
        if (!bill.isMapAccount) {
            title = stringTitleRemoveMapcard;
        }
    }
    if ([self.resultHandler isPaymentSuccess]) {
        return [NSString stringWithFormat:@"%@ %@",title,stringResultSuccess];
    }
    if (self.resultHandler.responseResult.errorCode == kZPZaloPayCreditErrorCodeRequestTimeout) {
        return [R string_Result_Title_Network_Problem];
        
    }
    if (self.resultHandler.responseResult.exchangeStatus == kZPZaloPayCreditStatusCodeUnidentified) {
        return [R string_Result_Title_Processing];
    }
    return [NSString stringWithFormat:@"%@ %@",title,stringResultFailed];

}
- (void)setUpUiBar {
    self.navigationItem.leftBarButtonItems = nil;
    self.navigationItem.leftBarButtonItem=nil;
    self.navigationItem.hidesBackButton=YES;
    self.navigationItem.rightBarButtonItems = nil;
    self.navigationItem.rightBarButtonItem=nil;
    self.navigationController.navigationBarHidden = false;
}

- (void)addTableView {
    UITableView *tableView = [[UITableView alloc] init];
    tableView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:tableView];
    self.tableView = tableView;
    [self.view bringSubviewToFront:tableView];
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.bottom.equalTo(0);
        make.left.equalTo(0);
        make.right.equalTo(0);
    }];
}

- (IBAction)onClickOkButton:(id)sender {
    [self.resultHandler processPayment];
}

- (void)notifyPaymentResult {
    DDLogInfo(@"notifyPaymentResult balance %lld", self.resultHandler.responseResult.balance);
    if ([self.delegate respondsToSelector:@selector(paymentControllerNotifyResultWithResponse:)]) {
        [self.delegate paymentControllerNotifyResultWithResponse:self.resultHandler.responseResult];
    }
}

- (void)paymentControllerShouldNotifyPaymentSuccess {
    [self notifyPaymentResult];
}

- (void)paymentControllerDidClose:(NSString*)key data:(NSString*)data {
    if (self.delegate && [self.delegate respondsToSelector:@selector(paymentControllerDidClose:data:)]) {
        [self.delegate paymentControllerDidClose:key data:data];
    }
}
- (void)paymentControllerNeedShowSupportAction {
    
    ZPCustomActionSheet * actionSheetView = [[ZPCustomActionSheet alloc] initWithFrame:self.view.bounds];
    actionSheetView.frame = CGRectMake(0,400, self.view.bounds.size.width, self.view.bounds.size.height);
    self.bgView = [[UIView alloc] initWithFrame:self.view.bounds];
    self.bgView.backgroundColor = [UIColor colorWithWhite:0.1 alpha:0.0];
    [self.view addSubview:self.bgView];
    actionSheetView.delegate = self;
    [UIView animateWithDuration:0.3
                          delay:0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         
                         actionSheetView.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
                         self.bgView.backgroundColor = [UIColor colorWithWhite:0.1 alpha:0.8];
                         
                     }
                     completion:^(BOOL finished){
                     }];
    [self.view addSubview:actionSheetView];
    
}

- (void)paymentControllerDidFinishWithResponse:(ZPPaymentResponse *)response {
    DDLogInfo(@"---paymentControllerDidFinishWithResponse errorStep: %ld", (long)response.errorStep);
    [ZaloPaySDKUtil writeAppTransIdLog:self.bill];
    if ([self.delegate respondsToSelector:@selector(paymentControllerDidFinishWithResponse:)]) {
        [self.delegate paymentControllerDidFinishWithResponse:response];
    }
}

#pragma mark - ActionSheet

- (void)sheetDidChooseCancel:(ZPCustomActionSheet *)sheet {
    [sheet removeFromSuperview];
    [self.bgView removeFromSuperview];
    self.bgView = nil;
}

- (void)sheetDidChooseFAQ:(ZPCustomActionSheet *)sheet {
    [sheet removeFromSuperview];
    [self.bgView removeFromSuperview];
    self.bgView = nil;
    if (self.delegate && [self.delegate respondsToSelector:@selector(paymentControllerDidChooseFAQ)]) {
        [self.delegate paymentControllerDidChooseFAQ];
    }
}

- (void)sheetDidChooseNeedSupport:(ZPCustomActionSheet *)sheet {
    [sheet removeFromSuperview];
    [self.bgView removeFromSuperview];
    self.bgView = nil;
    if ([self.delegate respondsToSelector:@selector(paymentControllerDidChooseSupportWithData:)]) {
        NSString *payType = [[ZPDataManager sharedInstance] getTitleByTranstype:self.bill.transType];
        NSDictionary *data = @{@"paytype": payType ,
                               @"errorcode":@(self.responseNotifySupport.originalCode),
                               @"zptransid": [NSString zpEmptyIfNilString:self.responseNotifySupport.zpTransID],
                               @"errormessage": [NSString zpEmptyIfNilString:self.responseNotifySupport.message],
                               @"capturescreen":[self captureScreenInRect:self.view.bounds]
                               };
        [self.delegate paymentControllerDidChooseSupportWithData:data];
    }
}

-(UIImage *)captureScreenInRect:(CGRect)captureFrame {
    CALayer *layer;
    layer = self.view.layer;
    UIGraphicsBeginImageContext(self.view.bounds.size);
    CGContextClipToRect (UIGraphicsGetCurrentContext(),captureFrame);
    [layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *screenImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return screenImage;
}


@end
