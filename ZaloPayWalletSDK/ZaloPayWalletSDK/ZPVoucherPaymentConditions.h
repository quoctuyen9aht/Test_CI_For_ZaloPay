//
//  VoucherPaymentConditions.h
//  ZaloPayWalletSDK
//
//  Created by Phuoc Huynh on 1/3/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZPDefine.h"

@interface ZPVoucherPaymentConditions : NSObject
@property (nonatomic, strong) NSArray *useconditionapplist;
@property (nonatomic, strong) NSArray *useconditionpmcs;
@property (nonatomic, strong) NSArray *useconditionbankcodes;
@property (nonatomic, strong) NSArray *useconditionccbankcodes;
@property (nonatomic) int currentPMCID;
@property (nonatomic, strong) NSString *currentBankcode;
@property (nonatomic, strong) NSString *currentCCBankcode;
@property (nonatomic, assign) ZPVoucherConditionPPAmount voucherConditionPPAmount;
- (instancetype)initDataWithDict:(NSDictionary *)dict;
- (instancetype)initDataWithPromotionDict:(NSDictionary *)dict;
@end
