//
//  ZaloPayWalletSDKCallBackDelegate.h
//  ZaloPayWalletSDK
//
//  Created by thi la on 3/2/17.
//  Copyright © 2017-present VNG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//! The ZaloPayWalletSDKCallBackDelegate is used to declare methods that are independent of any specific class. SDK use to notify result with data or some specific case for app.

@protocol ZaloPayWalletSDKCallBackDelegate<NSObject>

@required
- (void)paymentDidCloseWithKey:(NSString*)key andData:(NSDictionary*)data;
- (void)paymentReponseSuccess:(NSDictionary*)data;
- (void)paymentReponseFail:(NSDictionary*)data;
- (void)paymentReponseUnidentified;
- (void)paymentTrackingEventWithData:(NSDictionary*)data;

// Handle delegate to know result before payment sdk is closed
- (void)paymentNotifyCloseWithKey:(NSString*)key andData:(NSDictionary*)data;
- (void)paymentNotifySuccess:(NSDictionary*)data;
- (void)paymentNotifyFail:(NSDictionary*)data;
- (void)paymentNotifyUnidentified;
- (void)paymentNotifyShowFAQ;
- (void)paymentNotifyShowSupportPage:(NSDictionary*)data;

@end
