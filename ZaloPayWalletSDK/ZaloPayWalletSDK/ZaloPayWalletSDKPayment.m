//
//  ZPSDK+Payment.m
//  ZPSDK
//
//  Created by ZaloPayTeam on 12/24/15.
//  Copyright © 2015-present VNG Corporation All rights reserved.
//

#import "ZaloPayWalletSDKPayment.h"
#import "ZaloMobilePaymentSDK.h"
#import "ZPBill.h"
#import "ZPDataManager.h"
#import "ZPResourceManager.h"
#import "NetworkManager+ZaloPayWalletSDK.h"
#import "ZPSDKAppInfoManager.h"
#import "ZPPaymentChannel.h"
#import <ZaloPayWalletSDK/ZaloPayWalletSDK-Swift.h>
#import "ZPPaymentResponse.h"
#import "ZPBank.h"

@interface ZaloPayWalletSDKPayment ()
@end

@implementation ZaloPayWalletSDKPayment{
    int userLevel;
    ZPBill *zpBill;
}

+ (instancetype)sharedInstance{
    static ZaloPayWalletSDKPayment *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (void)initializeSDK {
    self.zaloPaySDK = [ZaloMobilePaymentSDK new];
    [self.zaloPaySDK initPaymentSDK];
    self.zpVoucherManager = [ZPVoucherManager new];
    self.zpPromotionManager = [ZPPromotionManager new];
}

- (int)userProfileLevel{
    return userLevel;
}

- (void)setUserProfileLevel:(int)level{
    userLevel = level;
}

- (RACSignal *)getAppInfoWithId:(NSInteger)appID transtypes:(NSArray *)arrTranstype {
    return [[ZPDataManager sharedInstance].appInfoManager getAppInfoWithId:appID transtypes:arrTranstype];
}

- (void) setUserPLP:(NSArray*)array{
    NSMutableArray *userLevelPermission = [NSMutableArray array];
    for (NSDictionary *oneDic in array) {
        ZPUserPLP *info = [ZPUserPLP fromJson:oneDic];
        [userLevelPermission addObject:info];
    }
    [ZPDataManager sharedInstance].paymentUserPLP = userLevelPermission;
}
- (NSArray *)updatepaymentSavedCard:(NSArray *)paymentSavedCard{
    return [ZPDataManager sharedInstance].paymentSavedCard = paymentSavedCard;
}
- (NSArray *)paymentSavedCard{
    return [ZPDataManager sharedInstance].paymentSavedCard;
}

- (NSArray *)paymentSavedBankAccount{
    return [ZPDataManager sharedInstance].paymentSavedBankAccount;
}

- (void)getBankListEnabled:(void (^)(NSDictionary *))callback {
    [[ZPDataManager sharedInstance].bankManager getBanklistWithCallBack:callback];
}

- (RACSignal <NSDictionary *>*)getBankPaymentGateway:(NSInteger)appid {
    return [[ZPDataManager sharedInstance].bankManager getBankGatetway:appid];
}

- (BOOL)canPaymentGateway:(NSInteger)appid {
    return [[ZPDataManager sharedInstance].bankManager canPaymentGatway:appid];
}

- (UIImage *)getImageWithName:(NSString *)imageName {
    return [ZPResourceManager getImageWithName:imageName];
}

- (void)getPlatformInfoConfig:(void (^)(NSDictionary *))callback {
    [[ZPDataManager sharedInstance] getPlatformInfoConfig:callback];
}

- (void)getPaymentGatewayInfoCompleteHandle:(void (^) (int errorCode, NSString *mesage))callBack {
    [[ZPDataManager sharedInstance] getPaymentGatewayInfoCompleteHandle:callBack];
}

- (RACSignal *)getPaymentGatewayInfo {
    return [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
        [[ZPDataManager sharedInstance] getPaymentGatewayInfoCompleteHandle:^(int errorCode, NSString *mesage) {
            [subscriber sendNext:@(errorCode)];
            [subscriber sendCompleted];
        }];
        return nil;
    }];
}

- (NSString*)getCcBankCodeFromConfig:(NSString*)ccBankNumber {
    return [ZPResourceManager getCcBankCodeFromConfig:ccBankNumber];
}

- (void)closePaymentAndCleanSDK {
    [self.zaloPaySDK closePaymentAndCleanSDK];
}

- (BOOL)checkIsCurrentPaymentFinished {
    return [self.zaloPaySDK checkIsCurrentPaymentFinished];
}

- (BOOL)checkIsCurrentPaying {
    return [self.zaloPaySDK isPayingBill];
}

- (void)forceClosePaymentSDK {
    [self.zaloPaySDK closePaymentAndCleanUp];
}

- (void)dismissSDKKeyboard {
    [self.zaloPaySDK dismissSDKKeyboard];
}

- (void)showSDKKeyboard {
    [self.zaloPaySDK showSDKKeyboard];
}

- (void)updateSavedCardList:(dispatch_block_t)callback {
    [[ZPDataManager sharedInstance] requestUpdateCardList:callback];
}

- (void)updateSavedAccountList:(dispatch_block_t)callback {
    [[ZPDataManager sharedInstance] requestUpdateAccountList:callback];
}

- (RACSignal *)removeSavedCard:(ZPSavedCard *)card {
    return [self.zaloPaySDK removeSavedCard:card];
}

- (RACSignal *)removeBankAccount:(NSString*)bankCustomerId bankAccount:(ZPSavedBankAccount *)bankAccount {
    return [self.zaloPaySDK removeBankAccount:bankCustomerId bankAccount:bankAccount];
}

- (BOOL)isMasterCardEnable {
    return [[ZPDataManager sharedInstance].bankManager isMasterCardEnable];
}

- (BOOL)isVisaCardEnable {
    return [[ZPDataManager sharedInstance].bankManager isVisaCardEnable];
}

- (BOOL)isJCBCardEnable {
    return [[ZPDataManager sharedInstance].bankManager isJCBCardEnable];
}

- (BOOL)isMaxCCSavedCard {
    return [[ZPDataManager sharedInstance] isMaxCCSavedCard];
}

- (void)showAlertMaxCCSavedCard {
    [[ZPDataManager sharedInstance] showAlertMaxCCSavedCard];
}

- (NSString*)checkErrorMappCCCardWith:(ZPTransType)transType appId:(int32_t)appId {
    return [[ZPDataManager sharedInstance] checkErrorMappCCCardWith:transType appId:appId];
}

- (NSArray <ZPMiniBank *> *)getMiniBanksForMapping {
    return [[ZPDataManager sharedInstance].bankManager getMiniBanksForMapping];
}

- (BOOL)chekMinAppVersion:(NSString *)minAppVersion {
    return [[ZPDataManager sharedInstance] chekMinAppVersion:minAppVersion];
}

- (RACSignal *)requetAppInfo:(NSInteger)appId transtype:(ZPTransType)transtype {
    return [[ZPDataManager sharedInstance].appInfoManager getAppInfoWithId:appId transtypes:@[@(transtype)]];
}

- (void)startHandleBill:(ZPBill *)bill
           withDelegate:(UIViewController<ZaloPayWalletSDKCallBackDelegate> *)delegate
    navigaionController:(UINavigationController *)navi {
    
    zpBill = bill;
    self.paymentDelegate = delegate;
    self.zaloPaySDK.parentController = delegate;
    self.zaloPaySDK.navigationController = navi;
    [self processPaymentWith:bill];
}

- (void)processPaymentWith:(ZPBill *)bill {
    if ([bill isKindOfClass:[ZPIBankingAccountBill class]]) {
        [self.zaloPaySDK processBankAccountWithBill:bill];
        return;
    }
    if (bill.transType == ZPTransTypeAtmMapCard) {
        [self.zaloPaySDK linkCardWithBill:bill];
        return;
    }
    [self.zaloPaySDK payWithBill:bill];
}

- (void)logout {
    [[ZPDataManager sharedInstance] clearLocalUserCache];
}

- (ZPBill *)bill {
    return zpBill;
}

- (NSDictionary *)bankDisplayOrder {
    return [[ZPDataManager sharedInstance].bankManager bankDisplayOrder];
}

- (NSNumber *)ccDisplayOrder:(NSString *)key {
    return [[ZPDataManager sharedInstance].bankManager ccDisplayOrder:key];
}

- (void)clearPlatformInfoAndResource {
    [[ZPDataManager sharedInstance] clearPlatformInfoAndResource];
}

- (void)clearPaymentCache {
    [[ZPDataManager sharedInstance] clearPaymentCache];
}

- (RACSignal *)registerBankWith:(NSDictionary *)params using:(NSString *)zpTransID {
    return [self sdkRegisterBankWith:params using:zpTransID];
}

- (RACSignal *)authCardholderWith:(NSDictionary *)params using:(NSDictionary *)paramsNext {
    return [self sdkAuthCardholderWith:params using:paramsNext];
}

- (RACSignal*)loadCardListAndBankAccount {
    return [[ZPDataManager sharedInstance] loadCardListAndBankAccount];
}

#pragma mark - popup
- (void)showPopupPayment:(UIViewController *)controller with:(ZPBill *)bill complete:(void (^)(id))completionHandler error:(void (^)(NSError *))errorHandler {
    [ZPPaymentPopupRootController showObjCOn:controller
                                        with:bill
                                  completion:completionHandler
                                errorHandler:errorHandler];
    
}

#pragma mark - kyc
- (RACSignal *)runKYCFlow:(UIViewController *)controller
                    title:(NSString *)title
                    infor:(NSDictionary *)infor
                     type:(ZPKYCType)type {
    @weakify(controller);
    return [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
        @strongify(controller);
        [ZPKYCViewController runKYCFlowObjCOn:controller
                                        title:title
                                        using:infor
                                           at:type
                                 handlerValue:^(NSDictionary<NSString *,id> * _Nonnull result)
        {
            [subscriber sendNext:result];
        } cancel:^{
            NSError *cancel = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorCancelled userInfo:nil];
            [subscriber sendError:cancel];
        } error:^(NSError * _Nonnull e) {
            [subscriber sendError:e];
        } complete:^{
            [subscriber sendCompleted];
        }];
        
        return nil;
    }];
}

#pragma mark - gateway
- (RACSignal <NSDictionary *>*)runGateway:(ZPBill *)bill on:(UIViewController *)controller url:(NSURL *)url type:(ZPGatewayType)type {
    @weakify(controller);
    return [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
        @strongify(controller);
        [ZPPaymentGatewayViewController showOnObjCWithController:controller url:url bill:bill type:type handlerResponse:^(ZPPaymentResponse * _Nullable response) {
            NSDictionary *result = @{};
            if (response) {
                result = [response toJsonResponse:bill];
            }
            [subscriber sendNext:result];
            [subscriber sendCompleted];
        } handlerError:^(NSError * _Nonnull error) {
            [subscriber sendError:error];
        } handlerCancel:^{
            NSError *e = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorCancelled userInfo:@{}];
            [subscriber sendError:e];
        }];
        return nil;
    }];
}

- (ZPSDK_BankType)bankSupportType:(NSArray *)bankFunctions {
    return [ZPBank bankSupportType:bankFunctions];
}

@end
