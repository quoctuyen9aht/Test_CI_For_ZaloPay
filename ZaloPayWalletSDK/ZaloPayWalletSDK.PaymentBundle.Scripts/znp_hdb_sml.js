var request_captcha = function () {
    var img = document.getElementById('CaptchaImage');
    
    var otp_onload = function() {
        var c = document.createElement('canvas');
        var ctx = c.getContext('2d');
        c.width=img.width; c.height=img.height;
        ctx.drawImage(img,0,0);
        var result = {pageId:2};
        result.captcha=c.toDataURL().replace('data:image/png;base64,', '');
        window.location = 'https://captchacallback.com?param='+encodeURI(JSON.stringify(result));
        
    };
    
    var otp_error = function() {
        znp_hdb_request();
    };
    
    var znp_hdb_request  = function () {
        if(img) {
            img = new Image();
            img.onload = otp_onload;
            img.onerror = otp_error;
            img.src = document.getElementById('CaptchaImage').src;;
        }
        else {
            otp_onload();
        }
        
    };
    
    znp_hdb_request();
};

// interval
var intervalListener;
var check_error = function (goToSubmitOtp) {
    intervalListener = self.setInterval(function() {
                                        get_error_msg(goToSubmitOtp)
                                        }, 500);
};

// login-------------------------
var login = function (username, password, captcha) {
    document.getElementById('input00').value = username;
    document.getElementById('input01').value = password;
    document.getElementById('input03').value = captcha;
    document.getElementById('btnSubmit').click();
    check_error(true);
};

// submit otp
var submit_otp = function(otp){
    document.getElementById('otpinput').value = otp;
    document.getElementById('btnPayment').click();
    check_error(false);
}

// check error--------------------
var get_error_msg = function (goToSubmitOtp) {
    var errorText = document.getElementById('message');
    var otp = document.getElementById('otpinput');
    
    if (errorText && errorText.innerText != '') {
        if(intervalListener){
            window.clearInterval(intervalListener);
        }
        var msg = errorText.innerText;
        errorText.innerText = '';
        var result = {
            message: msg
        };
        window.location = 'https://hdb-error-message.com?param='+encodeURI(JSON.stringify(result));
        
    }else{
        if(otp && goToSubmitOtp){
            window.clearInterval(intervalListener);
            window.location = 'https://hdb-submit-otp.com';
        }
    }
};

