var img;
var otp_onload = function() {
  var c = document.createElement('canvas');
  var ctx = c.getContext('2d');
  c.width=img.width;
  c.height=img.height;
  ctx.drawImage(img, 0, 0);
  var result = {pageId:2};
  
  
  var ses = document.getElementsByTagName('script');
  var emsg = '';
  if(ses)
    for (var i=0; i < ses.length; i++) {
      var se = ses[i];
      if(!!se.src || !se.innerHTML) continue;
      if(se.innerHTML.trim().indexOf('alert') == 0) {
        emsg = se.innerHTML.trim().replace('alert(\"', '').replace('\");', '');
        break;
      }
    }
  
  if(emsg && emsg.trim().length > 0)  {
    result.message = emsg.trim();
  }
  
  ses = document.getElementsByTagName('td');
  if(ses)
    for (var i=0; i < ses.length; i++) {
      var se = ses[i];
      if(!se.innerHTML || se.getAttribute('style') != 'color:#FF0000') continue;
      emsg = se.innerHTML;
    }
  
  if(emsg && emsg.trim().length > 0)  {
    result.message = emsg.trim();
  }
  
  result.captcha = c.toDataURL().replace('data:image/png;base64,', '');
  window.location = 'https://captchacallback.com?param='+encodeURI(JSON.stringify(result));
};

var otp_error = function() {
  znp_dab_captcha();
};

function check_error_message() {
  var sms = document.getElementsByClassName('demo-DialogBox-message');
  if (sms.length > 0 && sms[0].innerHTML &&  sms[0].innerHTML != 'Quý khách chắc chắn đồng ý thực hiện Thanh toán?')
  {
    var buttons = document.getElementsByTagName('button');
    for (var i=0; i<buttons.length; i++) {
      if (buttons[i].firstChild.nodeValue == 'Đồng ý'){
        buttons[i].click();
        break;
      }
    }
  }
}

var znp_dab_captcha = function() {
  var counter = 0;
  var interval = setInterval(function(){
                             img = document.getElementsByClassName("gwt-Image")[0];
                             check_error_message();
                             if(img){
                             if(img.naturalWidth === 0 || img.naturalHeight === 0 || img.complete === false) {
                             img = document.createElement('img');
                             img.id = 'js_otpimg';
                             img.onload = otp_onload;
                             img.onerror = otp_error;
                             img.src = document.getElementsByClassName("gwt-Image")[0].src;
                             }
                             else {
                             otp_onload();
                             }
                             }
                             counter++;
                             if(counter === 10 || img) {
                             clearInterval(interval);
                             }
                             }, 500);
};
