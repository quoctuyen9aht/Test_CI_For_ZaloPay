var zp_ibanking_VCB_GetCaptchar = function () {
    var img = document.getElementById('captchaImage');
    if(img == null){
       var msg = "Quý khách chưa đăng ký số điện thoại nhận OTP, vui lòng đăng ký tại trang web của ngân hàng.";
         window.location = 'https://sdk-ibanking-error.com?msg=' + msg;
    }
    var otp_onload = function() {
       // alert("canvas");
        var c = document.createElement('canvas');
        var ctx = c.getContext('2d');
        c.width=img.naturalWidth; c.height=img.naturalHeight;
        ctx.drawImage(img,0,0);
        var result = {pageId:2};
        result.captcha=c.toDataURL().replace('data:image/png;base64,', '');
        window.location = 'http://captchacallback.com?param='+encodeURI(JSON.stringify(result));
    };
    var otp_error = function() {
        znp_vcb_request();
    };
    var znp_vcb_request  = function () {
        if(img.naturalWidth === 0 || img.naturalHeight === 0 || img.complete === false) {
            setTimeout(otp_onload, 1000);
        }
        else {
           // alert("otp_onload");
            otp_onload();
        }
    };
    znp_vcb_request();
};


