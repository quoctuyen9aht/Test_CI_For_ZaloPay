
var znp_bidv_check_step = function () {
    var paymentInfo = document.getElementById('payment-info');
    var otpInfo = document.getElementById('otp-info');
    var billInfo = document.getElementById('bill-info');
    if(paymentInfo == null && otpInfo == null && billInfo == null){
        msg = 'Không thể kết nối với ngân hàng, quý khách vui lòng quay lại sau.';
        window.location = 'https://sdk-atm-error.com?msg=' + msg;
    }
    if (otpInfo.style.display === 'none' && billInfo.style.display === 'none'){
        var allElement = document.getElementsByClassName('bootbox-body');
        if(allElement.length > 0){
          var  lblTimeout = document.getElementById('time-out');
        if(lblTimeout == null) window.location = 'https://bidv-captcha-step.com';
        } else window.location = 'https://bidv-captcha-step.com';
    }
    else if (paymentInfo.style.display === 'none' && billInfo.style.display === 'none') {
        window.location = 'https://bidv-otp-step.com';
    }
    else if (paymentInfo.style.display === 'none' && otpInfo.style.display === 'none') {
        window.location = 'https://bidv-bill-step.com';
    }
    else{
        msg = 'Không thể kết nối với ngân hàng, quý khách vui lòng quay lại sau.';
        window.location = 'https://sdk-atm-error.com?msg=' + msg;
    }
};
