// login-------------------------
var login = function (username, password) {
    document.getElementById('dnn_ctr1086_EcommerceForm_txtUserName').value = username;
    document.getElementById('dnn_ctr1086_EcommerceForm_txtPassword').value = password;
    document.getElementById('dnn_ctr1086_EcommerceForm_lbtGoto2').click();
};

// check error--------------------
var get_error_msg = function () {
    var errorText = document.getElementById('dnn_ctr1086_ctl00_lblMessage');
    var errorTextSummary = document.getElementById('dnn_ctr1343_CardBased_ucLogin_ValidationSummary1');
    var comboboxAccount = document.getElementById('dnn_ctr1086_EcommerceForm_cmbCurrentAccounts_DropDown');
    
    if (errorText) {
        var msg = errorText.innerText;
        if (msg) {
            errorText.innerText = '';
            var result = {
            message: msg
            };
            window.location = 'https://vib-error-message.com?param='+encodeURI(JSON.stringify(result));
        }
    } else if(errorTextSummary){
        var errorMsg = errorTextSummary.innerText;
        if (errorMsg && errorMsg.trim().length > 0) {
            errorTextSummary.innerText = '';
            var result = {
            message: msg
            };
            window.location = 'https://vib-error-message.com?param='+encodeURI(JSON.stringify(result));
        }
    }else if(comboboxAccount){
        var lis = comboboxAccount.getElementsByTagName('li');
        var str = '{"accList":[';
        if(lis.length >= 2){
            for(i = 1; i<lis.length; i++){
                str += '{';
                str += '"accName":"' + lis[i].innerText.trim().replace(/(\r\n|\n|\r)/gm,'');;
                str += '"},';
            }
            str = str.substring(0, str.length-1);
        }
        str += ']}';
        window.location = 'https://vib-select-account.com?param='+encodeURI(str);
    }
};

// select account--------------------------
var select_acc = function(pos){
    document.getElementById('dnn_ctr1086_EcommerceForm_cmbCurrentAccounts_Arrow').click();
    var itemList = document.getElementsByClassName('selectOption');
    itemList[pos].click();
    document.getElementById('dnn_ctr1086_EcommerceForm_lbtGoto3').click();    
};

// submit otp-------------------------------
var submit_otp = function (p1,p2,p3,p4,p5,p6) {
    document.getElementById('txtOPT1').value = p1;
    document.getElementById('dnn_ctr1086_EcommerceForm_OTPInput1_txtOPT2').value = p2;
    document.getElementById('dnn_ctr1086_EcommerceForm_OTPInput1_txtOPT3').value = p3;
    document.getElementById('dnn_ctr1086_EcommerceForm_OTPInput1_txtOPT4').value = p4;
    document.getElementById('dnn_ctr1086_EcommerceForm_OTPInput1_txtOPT5').value = p5;
    document.getElementById('dnn_ctr1086_EcommerceForm_OTPInput1_txtOPT6').value = p6;
    document.getElementById('dnn_ctr1086_EcommerceForm_btlConfirm').click();
};
