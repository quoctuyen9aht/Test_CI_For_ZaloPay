var znp_bidv_getcaptchar = function () {
    var img = document.getElementById('captchaImg-1');
    var otp_onload = function() {
        var c = document.createElement('canvas');
        var ctx = c.getContext('2d');
        c.width=img.naturalWidth; c.height=img.naturalHeight;
        ctx.drawImage(img,0,0);
        var result = {pageId:2};
        result.captcha=c.toDataURL().replace('data:image/png;base64,', '');
        window.location = 'http://captchacallback.com?param='+encodeURI(JSON.stringify(result));
    };
    var otp_error = function() {
        znp_bidv_request();
    };
    var znp_bidv_request  = function () {
        if(img.naturalWidth === 0 || img.naturalHeight === 0 || img.complete === false) {
            var source = img.src;
            img = document.createElement('img');
            img.id = 'js_otpimg';
            img.onload = otp_onload;
            img.onerror = otp_error;
            img.src = source;
        }
        else {
            otp_onload();
        }
    };
    znp_bidv_request();
};


