var intervalListener;

var znp_vib_login_validation = function () {
    intervalListener = self.setInterval(function() {
        checkLogin()
    }, 1000);
};

var checkError = function () {
    var errorText = document.getElementById('dnn_ctr1343_ctl00_lblMessage');
    if (errorText) {
        var msg = errorText.innerText;
        if (msg) {
            errorText.innerText = '';
            window.clearInterval(intervalListener);
            var result = {
            message: msg
            };
            window.location = 'https://vib-error-message.com?param='+encodeURI(JSON.stringify(result));
        }
    } else {
        var errorTextSummary = document.getElementById('dnn_ctr1343_CardBased_ucLogin_ValidationSummary1');
        if (errorTextSummary) {
            var errorMsg = errorTextSummary.innerText;
            if (errorMsg && errorMsg.trim().length > 0) {
                errorTextSummary.innerText = '';
                window.clearInterval(intervalListener);
                var result = {
                message: msg
                };
                window.location = 'https://vib-error-message.com?param='+encodeURI(JSON.stringify(result));
            }
        }
    }
};

var checkLogin = function () {
    if (document.getElementById('dnn_ctr1343_CardBased_divStep2')) {
        window.clearInterval(intervalListener);
        var result = {
        pageId: 123436
        };
        SendOtp($('#agetotp')[0]);
        window.location = 'https://vib-otp-step.com';
    } else if (document.getElementById('dnn_ctr1343_CardBased_ucLogin_divStep1')) {
        checkError();
    }
};


var znp_vib_login = function (username, password) {
    document.getElementById('dnn_ctr1343_CardBased_ucLogin_txtUserName').value = username;
    document.getElementById('dnn_ctr1343_CardBased_ucLogin_txtPassword').value = password;
    document.getElementById('dnn_ctr1343_CardBased_ucLogin_lbtLogin').click();
    
    znp_vib_login_validation();
};
