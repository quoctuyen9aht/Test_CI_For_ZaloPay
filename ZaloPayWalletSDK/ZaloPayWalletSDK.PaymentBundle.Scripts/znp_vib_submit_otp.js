var intervalListener;

var znp_vib_submit_otp_validation = function () {
    intervalListener = self.setInterval(function() {
        checkEnterOtp()
    }, 1000);
};

var checkError = function () {
    
    var errorText = document.getElementById('dnn_ctr1343_ctl00_lblMessage');
    if (errorText) {
        var msg = errorText.innerText;
        if (msg) {
            errorText.innerText = '';
            window.clearInterval(intervalListener);
            var result = {
            message: msg
            };
            window.location = 'https://vib-error-message.com?param='+encodeURI(JSON.stringify(result));
        }
    } else {
        var errorTextSummary = document.getElementById('dnn_ctr1343_CardBased_ucLogin_ValidationSummary1');
        if (errorTextSummary) {
            var errorMsg = errorTextSummary.innerText;
            if (errorMsg && errorMsg.trim().length > 0) {
                errorTextSummary.innerText = '';
                window.clearInterval(intervalListener);
                var result = {
                message: msg
                };
                window.location = 'https://vib-error-message.com?param='+encodeURI(JSON.stringify(result));
            }
        }
    }
};


var checkEnterOtp = function () {
    if (document.getElementById('dnn_ctr1343_CardBased_divStep2')) {
        checkError();
    }
};

var znp_vib_submit_otp = function (p1,p2,p3,p4,p5,p6) {
    document.getElementById('txtOPT1').value = p1;
    document.getElementById('dnn_ctr1343_CardBased_OTPInput1_txtOPT2').value = p2;
    document.getElementById('dnn_ctr1343_CardBased_OTPInput1_txtOPT3').value = p3;
    document.getElementById('dnn_ctr1343_CardBased_OTPInput1_txtOPT4').value = p4;
    document.getElementById('dnn_ctr1343_CardBased_OTPInput1_txtOPT5').value = p5;
    document.getElementById('dnn_ctr1343_CardBased_OTPInput1_txtOPT6').value = p6;
    document.getElementById('dnn_ctr1343_CardBased_lbtConfirm').click();
    
    znp_vib_submit_otp_validation();
};

