var result = {
shouldstop: "NO"
};

var znp_dab_confirm_payment = function () {
  document.addEventListener('DOMContentLoaded', intervalListener, false);
};
var intervalListener = self.setInterval(function() {
                                        click_confirm()
                                        }, 1000);

function submit_confirm() {
  var confirm_check = document.getElementById("gwt-uid-40");
  if (confirm_check != null) {
    var buttons = document.getElementsByTagName('button');
    for (var i=0; i<buttons.length; i++) {
      if (buttons[i].firstChild.nodeValue == 'Thanh toán'){
        buttons[i].click();
        window.clearInterval(intervalListener);
        break;
      }
    }
    setTimeout(znp_dab_get_error_message, 500);
  }
}

function click_confirm() {
  var confirm_check = document.getElementById("gwt-uid-40");
  if (confirm_check != null) {
    if (confirm_check.checked == false) {
      confirm_check.click();
    }
    setTimeout(submit_confirm, 500);
  }
}
var znp_dab_get_error_message = function () {
  var sms = document.getElementsByClassName('demo-DialogBox-message');
  if (sms.length > 0 && sms[0].innerHTML &&  sms[0].innerHTML != 'Quý khách chắc chắn đồng ý thực hiện Thanh toán?')
  {
    result.message = sms[0].innerHTML.replace('<br>', '');
    result.shouldstop = "YES";
    window.location = 'https://dab-error-message.com?param='+encodeURI(JSON.stringify(result));
  } else {
    setTimeout(znp_dab_check_step, 500);
  }
};

var znp_dab_check_step = function () {
  document.addEventListener('DOMContentLoaded', intervalListenerCheck, false);
};

var intervalListenerCheck = setInterval(function() {
                                        var buttons = document.getElementsByTagName('button');
                                        for (var i=0; i<buttons.length; i++) {
                                        if (buttons[i].firstChild.nodeValue == 'Đăng nhập'){
                                        window.location = 'https://dab-login-step.com';
                                        window.clearInterval(intervalListenerCheck);
                                        break;
                                        } else if (buttons[i].firstChild.nodeValue == 'Thanh toán'){
                                        window.location = 'https://dab-confirm-step.com';
                                        window.clearInterval(intervalListenerCheck);
                                        break;
                                        } else if (buttons[i].firstChild.nodeValue == 'Xác nhận'){
                                        window.location = 'https://dab-otp-step.com';
                                        window.clearInterval(intervalListenerCheck);
                                        break;
                                        } else {
                                        window.location = 'https://dab-exception-step.com';
                                        window.clearInterval(intervalListenerCheck);
                                        }
                                        }
                                        },500);
