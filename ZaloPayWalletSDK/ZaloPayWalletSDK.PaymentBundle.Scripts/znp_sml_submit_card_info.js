var znp_sml_submit_card_info = function (cardHolderName,cardNumber,cardPassword,cardMonth,cardYear){
    
    var form=document.getElementsByTagName('form')[0];
    
    if(form.elements['cardHolderName']  != null)   form.elements['cardHolderName'].value = cardHolderName;
    if(form.elements['cardNumber']      != null)   form.elements['cardNumber'].value = cardNumber;
    if(form.elements['cardPassword']    != null)   form.elements['cardPassword'].value = cardPassword;
    if(form.elements['cardMonth']       != null)   form.elements['cardMonth'].value = cardMonth;
    if(form.elements['cardYear']        != null)   form.elements['cardYear'].value = cardYear;
    
    form.onsubmit=null;
    form.submit();
    
};
