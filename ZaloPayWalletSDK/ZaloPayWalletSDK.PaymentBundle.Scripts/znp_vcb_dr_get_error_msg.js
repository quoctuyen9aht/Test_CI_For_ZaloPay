var znp_vcb_dr_get_error_msg = function(){
    var errMsgCaptcha = document.getElementById('ctl00_Content_Login_Form_CaptchaValidator');
    var errMsgUserPass = document.getElementById('ctl00_Content_Login_Form_LblWarning');
    var errMsgTimeout = document.getElementById('ctl00_Content_LblWarning');
    
    if(errMsgCaptcha && errMsgCaptcha.innerText != ''){
        return errMsgCaptcha.innerText;
    }else if(errMsgUserPass && errMsgUserPass.innerText != ''){
        return errMsgUserPass.innerText;
    }else{
        if(errMsgTimeout && errMsgTimeout.innerText != ''){
            var msg = errMsgTimeout.innerText;
            window.location = 'https://sdk-atm-error.com?msg=' + msg;
        }else{
            return '';
        }
    }
};
