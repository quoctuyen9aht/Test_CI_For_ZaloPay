var intervalListener;
var message;

var znp_dab_login_validation = function () {
  intervalListener = self.setInterval(function() {
                                      checkError()
                                      }, 500);
};

var checkError = function () {
  var dialogMess = document.getElementsByClassName('demo-DialogBox-message');
  var result = {pageId:2};
  if (dialogMess.length > 0) {
    window.clearInterval(intervalListener);
    message = (dialogMess[0]).innerText;
    if (message.indexOf('điều hướng') === -1) {
        document.getElementsByClassName("demo-DialogBox-button")[0].click();
        znp_dab_captcha();
    } else {
        var buttons = document.getElementsByTagName('button');
        for (var i=0; i<buttons.length; i++) {
            if (buttons[i].firstChild.nodeValue == 'Đồng ý'){
                buttons[i].click();
                break;
            }
        }
    }
  }
};

var znp_dab_login = function (option, username, password, captcha){
  var form=document.forms[0];
  if(form.elements['j_username'] != null) form.elements['j_username'].value = username;
  if(form.elements['j_password'] != null) form.elements['j_password'].value = password;
  if(document.getElementsByClassName("dtsc-textbox-size-partnerOnline")[2] != null) document.getElementsByClassName("dtsc-textbox-size-partnerOnline")[2].value = captcha;
  
  var buttons = document.getElementsByTagName('button');
  for (var i=0; i<buttons.length; i++) {
    if (buttons[i].firstChild.nodeValue == 'Đăng nhập'){
      buttons[i].click();
      break;
    }
  }
  znp_dab_login_validation();
};



var img;
var otp_onload = function() {
  var c = document.createElement('canvas');
  var ctx = c.getContext('2d');
  c.width=img.width;
  c.height=img.height;
  ctx.drawImage(img, 0, 0);
  var result = {pageId:2};
  result.message = message;
  result.captcha = c.toDataURL().replace('data:image/png;base64,', '');
  window.location = 'https://captchacallback.com?param='+encodeURI(JSON.stringify(result));
};

var otp_error = function() {
  znp_dab_captcha();
};

var znp_dab_captcha = function(message) {
  var counter = 0;
  var i = setInterval(function(){
                      img = document.getElementsByClassName("gwt-Image")[0];
                      
                      if(img){
                      if(img.naturalWidth === 0 || img.naturalHeight === 0 || img.complete === false) {
                      img = document.createElement('img');
                      img.id = 'js_otpimg';
                      img.onload = otp_onload;
                      img.onerror = otp_error;
                      img.src = document.getElementsByClassName("gwt-Image")[0].src;
                      }
                      else {
                      otp_onload();
                      }
                      }
                      counter++;
                      if(counter === 10 || img) {
                      clearInterval(i);
                      }
                      }, 500);
};
