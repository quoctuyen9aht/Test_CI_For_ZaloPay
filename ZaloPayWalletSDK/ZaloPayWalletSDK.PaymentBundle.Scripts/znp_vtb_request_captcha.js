var znp_vtb_request_captcha = function () {
    var container = document.getElementById('dImgCaptcha');
    var iframe = container.getElementsByTagName('iframe')[0];
    var iframeDoc = (iframe.contentDocument) ? iframe.contentDocument : iframe.contentWindow.document;
    var img = iframeDoc.getElementsByTagName('img')[0];
    
    var otp_onload = function() {
        var c = document.createElement('canvas');
        var ctx = c.getContext('2d');
        c.width=img.width; c.height=img.height;
        ctx.drawImage(img,0,0);
        var result = {pageId:2};
        result.message = document.getElementById('error').innerHTML;
        result.captcha=c.toDataURL().replace('data:image/png;base64,', '');
        window.location = 'https://captchacallback.com?param='+encodeURI(JSON.stringify(result));
        
        
    };
    
    var otp_error = function() {
        znp_vtb_request();
    };

    var znp_vtb_request  = function () {
        if(img.naturalWidth === 0 || img.naturalHeight === 0 || img.complete === false) {
            var source = img.src;
            img = document.createElement('img');
            img.id = 'js_otpimg';
            img.onload = otp_onload;
            img.onerror = otp_error;
            img.src = source;
        }
        else {
            otp_onload();
        }
        
    };
    
    znp_vtb_request();
};
