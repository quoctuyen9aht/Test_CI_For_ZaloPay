import os
import csv

def read_csv(file_path):
    data = []
    with open(file_path, 'rb') as csvfile:
        event_reader = csv.reader(csvfile, delimiter=',', quotechar='|')
        rownum = 0
        for row in event_reader:
            if rownum == 0:
                header = row
            else:
                obj = {
                    header[0]:row[0],
                    header[1]:row[1],
                    header[2]:row[2]
                }
                data.append(obj)
                # print(row[0], row[2], row[1])
            rownum += 1
    return data


def print_enum(data):
    lines = []
    for ev in data:
        lines.append('    EventAction_%s = %s,' % (ev['Action'], ev['EventId']))
    buffer = 'typedef NS_ENUM(NSInteger, EventAction) {\n%s\n};' % '\n'.join(lines)
    return buffer


def print_convert_action(data):
    lines = []
    for ev in data:
        lines.append('        case EventAction_%s:\n            return @"%s";' % (ev['Action'], ev['Action']))
    buffer = '''NSString* eventActionFromId(NSInteger eventId) {
    switch (eventId) {
%s
        default:
            return @"DefaultAction";
    }
};''' % '\n'.join(lines)
    return buffer


def print_convert_category(data):
    lines = []
    for ev in data:
        lines.append('        case EventAction_%s:\n            return @"%s";' % (ev['Action'], ev['Category']))
    buffer = '''NSString* eventCategoryFromId(NSInteger eventId) {
    switch (eventId) {
%s
        default:
            return @"DefaultCategory";
    }
};''' % '\n'.join(lines)
    return buffer

def print_declare(enu):
    buffer = '''//
//  TrackerEvents.h
//  ZaloPayAnalytics
//
//  Auto-generated.
//  Copyright (c) 2016-present VNG Corporation. All rights reserved.
//

#ifndef __TRACKER_EVENTS_H__
#define __TRACKER_EVENTS_H__

#import <Foundation/Foundation.h>

// helper functions
NSString* eventActionFromId(NSInteger eventId);
NSString* eventCategoryFromId(NSInteger eventId);

// define enum
%s

#endif // __TRACKER_EVENTS_H__
''' % enu
    return buffer

def print_implementation(f1, f2):
    buffer = '''//
//  EventTracker.m
//  ZaloPayAnalytics
//
//  Auto-generated.
//  Copyright (c) 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TrackerEvents.h"

%s

%s
''' % (f1, f2)
    return buffer


def write_output(file_path, buffer):
    print("Updating %s" % file_path)
    with open(file_path, 'w') as etd:
        etd.write(buffer)


data = read_csv('ZaloPay-EventAnalytics-Data.csv')
en = print_enum(data)
# print(en)

ac = print_convert_action(data)
# print(ac)

cat = print_convert_category(data)
# print(cat)

declare_file_buffer = print_declare(en)
# print(declare_file_buffer)
write_output("../ZaloPayAnalytics/ZaloPayAnalytics/TrackerEvents.h", declare_file_buffer)

implementation_file_buffer = print_implementation(ac, cat)
# print(implementation_file_buffer)
write_output("../ZaloPayAnalytics/ZaloPayAnalytics/TrackerEvents.m", implementation_file_buffer)

print("Done!")

