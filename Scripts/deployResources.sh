#!/bin/sh
set -e

mkdir -p "${TARGET_BUILD_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}"

RESOURCES_TO_COPY=${PODS_ROOT}/resources-to-copy-${TARGETNAME}.txt
> "$RESOURCES_TO_COPY"

XCASSET_FILES=()

case "${TARGETED_DEVICE_FAMILY}" in
  1,2)
    TARGET_DEVICE_ARGS="--target-device ipad --target-device iphone"
    ;;
  1)
    TARGET_DEVICE_ARGS="--target-device iphone"
    ;;
  2)
    TARGET_DEVICE_ARGS="--target-device ipad"
    ;;
  *)
    TARGET_DEVICE_ARGS="--target-device mac"
    ;;
esac

realpath() {
  DIRECTORY="$(cd "${1%/*}" && pwd)"
  FILENAME="${1##*/}"
  echo "$DIRECTORY/$FILENAME"
}

install_resource()
{
  if [[ "$1" = /* ]] ; then
    RESOURCE_PATH="$1"
  else
    RESOURCE_PATH="${PODS_ROOT}/../$1"
  fi
  case $RESOURCE_PATH in
    *)
      echo "$RESOURCE_PATH"
      echo "$RESOURCE_PATH" >> "$RESOURCES_TO_COPY"
      ;;
  esac
}

install_resource "paymentapps/internal/ios.1.zip"

if [[ "$CONFIGURATION" == "Debug" ]]; then
  install_resource "Scripts/network_mockup.json"
#  install_resource "paymentapps/sandbox/ios.17.zip"
fi
#if [[ "$CONFIGURATION" == "Sandbox" ]]; then
#  install_resource "paymentapps/sandbox/ios.11.zip"
#  install_resource "paymentapps/sandbox/ios.17.zip"
#fi
#if [[ "$CONFIGURATION" == "Production" ]]; then
#  install_resource "paymentapps/production/ios.11.zip"
#  install_resource "paymentapps/production/ios.17.zip"
#fi
#if [[ "$CONFIGURATION" == "Staging" ]]; then
#  install_resource "paymentapps/staging/ios.11.zip"
#  install_resource "paymentapps/staging/ios.17.zip"
#fi

mkdir -p "${TARGET_BUILD_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}"
rsync -avr --copy-links --no-relative --exclude '*/.svn/*' --files-from="$RESOURCES_TO_COPY" / "${TARGET_BUILD_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}"
rm -f "$RESOURCES_TO_COPY"

