import os
import csv


def read_csv(file_path):
    data = []
    with open(file_path, 'rb') as csvfile:
        event_reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        rownum = 0
        for row in event_reader:
            if len(row) > 4:
                print(row)
            if rownum == 0:
                header = row
            else:
                obj = {
                    header[0]: row[0],
                    header[1]: row[1],
                    header[2]: row[2],
                    header[3]: row[3]
                }
                data.append(obj)
                # print(row[0], row[2], row[1])
            rownum += 1
    return data


def print_declare_method(data):
    lines = []
    for ev in data:
        # if len(ev['Comment']) > 0:
        #     lines.append('/** %s */' % ev['Comment'])
        lines.append('/** %s */' % ev['Resource Value'])
        if ev['Resource Type'] == 'STRING':
            lines.append('+ (NSString *)string_%s;' % ev['Resource Name'])
    buffer = '\n'.join(lines)
    return buffer


def print_implement_method(data):
    lines = []
    for ev in data:
        if ev['Resource Type'] == 'STRING':
            # lines.append('+ (NSString *)string_%s { return @"%s"; }' % (ev['Resource Name'], ev['Resource Value']))
            lines.append('+ (NSString *)string_%s {return [textMap stringForKey:@"%s"];}' % (ev['Resource Name'], ev['Resource Name']))
    buffer = '\n'.join(lines)
    return buffer


# def print_declare_plist_file(content, ):
#     buffer = '''<?xml version="1.0" encoding="UTF-8"?>
# <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
# <plist version="1.0">
# <dict>
# %s
# </dict>
# </plist>

# ''' % (content)
#     return buffer


# def print_text_to_plist(data):
#     lines = []
#     for ev in data:
#         if ev['Resource Type'] == 'STRING':
#             str = ev['Resource Value'].replace("<", "&lt;")
#             str = str.replace(">", "&gt;")
#             str = str.replace("&", "&amp;")
#             lines.append('\t<key>string_%s</key>\n\t<string>%s</string>' % (ev['Resource Name'], str))
#     buffer = '\n'.join(lines)
#     return buffer


def print_text_to_json(data):
    lines = []
    for ev in data:
        if ev['Resource Type'] == 'STRING':
            lines.append('"%s": "%s"' % (ev['Resource Name'], ev['Resource Value']))
    buffer = ',\n'.join(lines)
    return buffer 


def print_declare(enu):
    buffer = '''//
//  R.h
//  ZaloPayResource
//
//  Auto-generated.
//  Copyright (c) 2016-2018 VNG Corporation. All rights reserved.
//

#ifndef __R_ZALOPAY_RESOURCES_H__
#define __R_ZALOPAY_RESOURCES_H__

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface R : NSObject
+ (void)loadTextFromDict:(NSDictionary *)dict;

%s

@end

#endif // __R_ZALOPAY_RESOURCES_H__
''' % enu
    return buffer


def print_implementation(f1,):
    buffer = '''//
//  R.m
//  ZaloPayResource
//
//  Auto-generated.
//  Copyright (c) 2016-2018 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "R.h"
#import "NSCollection+JSON.h"

static NSMutableDictionary *textMap;

@implementation R


+ (void)loadTextFromDict:(NSDictionary *)dict {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        textMap = [NSMutableDictionary dictionary];
    });
    @synchronized(textMap) {
        [textMap removeAllObjects];
        [textMap addEntriesFromDictionary:dict];
    }
}

%s
@end

''' % (f1)
    return buffer


def write_output(file_path, buffer):
    print("Updating %s" % file_path)
    with open(file_path, 'w') as etd:
        etd.write(buffer)


def main():
    import sys

    data = read_csv(sys.argv[1])

    method_declarations = print_declare_method(data)
    method_implementations = print_implement_method(data)
    # print(ac)

    # key_value_plist_file = print_text_to_plist(data)
    # declare_file_plist = print_declare_plist_file(key_value_plist_file)
    # write_output("../ZaloPay/resource_string.plist", declare_file_plist)

    text_json = print_text_to_json(data)
    buffer_text_json = '''{ %s }''' % text_json                 
    write_output("../ZaloPay/Resources/resource_string.json", buffer_text_json)

    declare_file_buffer = print_declare(method_declarations)

    # print(declare_file_buffer)
    write_output("../ZaloPayCommon/ZaloPayCommon/R.h", declare_file_buffer)

    implementation_file_buffer = print_implementation(method_implementations)
    # print(implementation_file_buffer)
    write_output("../ZaloPayCommon/ZaloPayCommon/R.m", implementation_file_buffer)

    print("Done!")


if __name__ == '__main__':
    main()
