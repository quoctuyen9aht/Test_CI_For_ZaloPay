#!/usr/local/bin/python
import os
import shutil
import sys

class BuildPaymentApp:
    def __init__(self, param_app_id, param_app_name, param_output_android, param_output_ios):
        self.root = "."
        self.app_id = param_app_id
        self.app_name = param_app_name
        self.output_android = param_output_android
        self.output_ios = param_output_ios

    # create folder ["production", "staging", "sandbox"]
    def prepare_output_folder(self, folders):

        for f in folders:
            path_name = os.path.join(self.root, f)
            shutil.rmtree(path_name, ignore_errors=True)
            os.makedirs(path_name)

        # call(["mkdir", "-p", "./output/internal-ios"])

    def remove_output_folder(self, folders):
        for f in folders:
            path_name = os.path.join(self.root, f)
            shutil.rmtree(path_name, ignore_errors=True)

    @staticmethod
    def add_folder_to_zip(zip_handle, path, base_path=""):
        """
        Adding directory given by \a path to opened zip file \a zipHandle

        :param base_path: path that will be removed from \a path when adding to archive
        :param zip_handle: handle to ZipFile
        :param path: path that will be removed from \a path when adding to archive

        Examples:
            # add whole "dir" to "test.zip" (when you open "test.zip" you will see only "dir")
            zipHandle = zipfile.ZipFile('test.zip', 'w')
            addDirToZip(zipHandle, 'dir')
            zipHandle.close()

            # add contents of "dir" to "test.zip" (when you open "test.zip" you will see only it's contents)
            zipHandle = zipfile.ZipFile('test.zip', 'w')
            addDirToZip(zipHandle, 'dir', 'dir')
            zipHandle.close()

            # add contents of "dir/subdir" to "test.zip" (when you open "test.zip" you will see only contents of "subdir")
            zipHandle = zipfile.ZipFile('test.zip', 'w')
            addDirToZip(zipHandle, 'dir/subdir', 'dir/subdir')
            zipHandle.close()

            # add whole "dir/subdir" to "test.zip" (when you open "test.zip" you will see only "subdir")
            zipHandle = zipfile.ZipFile('test.zip', 'w')
            addDirToZip(zipHandle, 'dir/subdir', 'dir')
            zipHandle.close()

            # add whole "dir/subdir" with full path to "test.zip" (when you open "test.zip" you will see only "dir" and inside it only "subdir")
            zipHandle = zipfile.ZipFile('test.zip', 'w')
            addDirToZip(zipHandle, 'dir/subdir')
            zipHandle.close()

            # add whole "dir" and "otherDir" (with full path) to "test.zip" (when you open "test.zip" you will see only "dir" and "otherDir")
            zipHandle = zipfile.ZipFile('test.zip', 'w')
            addDirToZip(zipHandle, 'dir')
            addDirToZip(zipHandle, 'otherDir')
            zipHandle.close()
        """
        base_path = base_path.rstrip("\\/") + ""
        base_path = base_path.rstrip("\\/")
        for root_folder, dirs, files in os.walk(path):
            # add dir itself (needed for empty dirs
            # add files
            for file in files:
                file_path = os.path.join(root_folder, file)
                in_zip_path = file_path.replace(base_path, "", 1).lstrip("\\/")

                # print filePath + " , " + inZipPath
                zip_handle.write(file_path, in_zip_path)

    def compress_js_bundle(self, zip_path_name, bundle_path):
        import zipfile

        ziph = zipfile.ZipFile(zip_path_name, 'w', zipfile.ZIP_DEFLATED)
        self.add_folder_to_zip(ziph, bundle_path, bundle_path)

    def process_folder(self, env):
        import zipfile

        # unzip file
        zip_path = os.path.join(self.root, "%s.%s.%s.zip" % (self.app_name, self.app_id, env))
        dst_path = os.path.join(self.root, env)
        ziph = zipfile.ZipFile(zip_path)
        ziph.extractall(dst_path)

        android_zip = os.path.join(self.root, env, "android.%s.zip" % self.app_id)
        android_bundle = os.path.join(self.root, env, "output", "android")
        self.compress_js_bundle(android_zip, android_bundle)

        ios_zip = os.path.join(self.root, env, "ios.%s.zip" % self.app_id)
        ios_bundle = os.path.join(self.root, env, "output", "ios")
        self.compress_js_bundle(ios_zip, ios_bundle)

        return android_zip, ios_zip

    @staticmethod
    def copy_output(params):
        import os

        zip = params["zip"]
        dest_folder = params["dest"]
        env = params["env"]

        dst_path = os.path.join(dest_folder, "paymentapps", env, os.path.basename(zip))
        shutil.copy2(zip, dst_path)

    def process_build(self, env):
        sandbox_android, sandbox_ios = self.process_folder(env)
        self.copy_output(
            {"zip": sandbox_android,
             "dest": self.output_android,
             "env": env})
        self.copy_output(
            {"zip": sandbox_ios,
             "dest": self.output_ios,
             "env": env})

if __name__ == "__main__":
    argc = len(sys.argv)
    if argc < 3:
        print("Usage: makeApp.py appId appName")
        print("Example: makeApp.py 014 zingxu")
        sys.exit(2)

    root = "."
    app_name = sys.argv[2]
    app_id = sys.argv[1]

    print("app_id: %s, app_name: %s" % (app_id, app_name))

    output_android = "/Volumes/Works/wallet/src/zalopay-android"
    output_ios = "/Volumes/Works/wallet/src/sandbox-ios"

    build = BuildPaymentApp(app_id, app_name, output_android, output_ios)

    print("Prepare temporary folders")
    build.prepare_output_folder(["production", "sandbox", "staging"])

    environment = "sandbox"
    print("Process for environment: %s" % environment)
    build.process_build(environment)

    environment = "staging"
    print("Process for environment: %s" % environment)
    build.process_build(environment)

    environment = "production"
    print("Process for environment: %s" % environment)
    build.process_build(environment)

    print("Cleanup")
    build.remove_output_folder(["production", "sandbox", "staging"])

    print("Done")
