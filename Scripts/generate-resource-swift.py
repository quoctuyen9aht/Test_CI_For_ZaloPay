import os
import csv

def read_csv(file_path):
    data = []
    with open(file_path, 'rb') as csvfile:
        event_reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        rownum = 0
        for row in event_reader:
            if len(row) > 4:
                print(row)
            if rownum == 0:
                header = row
            else:
                obj = {
                    header[0]:row[0],
                    header[1]:row[1],
                    header[2]:row[2],
                    header[3]:row[3]
                }
                data.append(obj)
                # print(row[0], row[2], row[1])
            rownum += 1
    return data

def print_implement_method(data):
    lines = []
    for ev in data:
        if ev['Resource Type'] == 'STRING':
            lines.append('public static func string_%s() -> String \n\t{ \n\t\treturn "%s" \n\t}\n' % (ev['Resource Name'], ev['Resource Value']))
    buffer = '\n\t'.join(lines)
    return buffer



def print_implementation(f1,):
    buffer = '''//
//  R.swift
//  ZaloPayResource
//
//  Auto-generated.
//  Copyright (c) 2016-2017 VNG Corporation. All rights reserved.
//

import Foundation

@objcMembers
public final class R: NSObject {
    %s
}

''' % (f1)
    return buffer


def write_output(file_path, buffer):
    print("Updating %s" % file_path)
    with open(file_path, 'w') as etd:
        etd.write(buffer)

import sys

data = read_csv(sys.argv[1])

method_implementations = print_implement_method(data)
# print(cat)


implementation_file_buffer = print_implementation(method_implementations)
# print(implementation_file_buffer)
write_output("../ZaloPayCommonSwift/ZaloPayCommonSwift/R.swift", implementation_file_buffer)

print("Done!")

