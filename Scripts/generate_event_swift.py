import os
import csv

def read_csv(file_path):
    data = []
    with open(file_path, 'rb') as csvfile:
        event_reader = csv.reader(csvfile, delimiter=',', quotechar='|')
        rownum = 0
        for row in event_reader:
            if rownum == 0:
                header = row
            else:
                obj = {
                    header[0]:row[0],
                    header[1]:row[1],
                    header[2]:row[2]
                }
                data.append(obj)
                # print(row[0], row[2], row[1])
            rownum += 1
    return data


def print_enum(data):
    lines = []
    for ev in data:
        lines.append('    case %s = %s' % (ev['Action'], ev['EventId']))
    buffer = '@objc public enum ZPAnalyticEventAction: Int {\n%s\n}' % '\n'.join(lines)
    return buffer


def print_convert_action(data):
    lines = []
    for ev in data:
        if ev['Action'] == 'linkbank_add_select_bank_code':
            lines.append('        case .%s:\n            return "linkbank_add_select_%%@"' % (ev['Action']))
        elif ev['Action'] == 'home_touch_id_app':
            lines.append('        case .%s:\n            return "home_touch_id_%%ld"' % (ev['Action']))
        else:
            lines.append('        case .%s:\n            return "%s"' % (ev['Action'], ev['Action']))
    buffer = '''public static func eventActionFromId(_ eventId: ZPAnalyticEventAction) -> String {
        switch (eventId) {
%s
        }
    }''' % '\n'.join(lines)
    return buffer


def print_convert_category(data):
    lines = []
    for ev in data:
        lines.append('        case .%s:\n            return "%s"' % (ev['Action'], ev['Category']))
    buffer = '''public static func eventCategoryFromId(_ eventId: ZPAnalyticEventAction) -> String {
        switch (eventId) {
%s
        }
    }''' % '\n'.join(lines)
    return buffer

def print_implementation(enu,action,category):
    buffer = '''//
//  ZPTrackerEvents.swift
//  ZaloPayAnalytics
//
//  Auto-generated.
//  Copyright (c) 2016-present VNG Corporation. All rights reserved.
//

import Foundation


// define enum
%s

@objcMembers
public final class ZPTrackerEvents: NSObject {
    %s

    %s
    
}''' % (enu, action, category)
    return buffer


def write_output(file_path, buffer):
    print("Updating %s" % file_path)
    with open(file_path, 'w') as etd:
        etd.write(buffer)


data = read_csv('ZaloPay-EventAnalytics-Data.csv')
en = print_enum(data)
# print(en)

ac = print_convert_action(data)
# print(ac)

cat = print_convert_category(data)
# print(cat)


implementation_file_buffer = print_implementation(en, ac, cat)
# print(implementation_file_buffer)
write_output("../ZaloPayAnalyticsSwift/ZaloPayAnalyticsSwift/ZPTrackerEvents.swift", implementation_file_buffer)

print("Done!")

