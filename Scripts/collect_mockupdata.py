import os
import sys
import re
import glob
import json


def get_session(session_string):
    session = re.sub(r'_\d+\.txt', r'', session_string)
    return session


def get_session_order(session_string):
    m = re.search(r'_(\d+)\.txt', session_string)
    if m is None:
        return session_string

    return int(m.group(1))


def get_all_session_files(session):
    file_pattern = "%s_*.txt" % session
    all_files = glob.glob(file_pattern)
    all_files = sorted(all_files, key=get_session_order)

    return all_files


def merge_session_files(all_files):
    mockup_data = {}
    for file_name in all_files:
        data = json.loads(open(file_name).read())
        path = data['path']
        if path not in mockup_data:
            mockup_data[path] = []

        mockup_data[path] = mockup_data[path] + data['mockup']

    final_result = {
      "type": "Network",
      "apis": [{"path": x, "mockup": mockup_data[x]}
               for x in mockup_data.keys()]
    }

    return final_result


def write_merged_session(session, final_result):
    output_file_name = "%s.json" % session
    with open(output_file_name, "w") as fout:
        fout.write(json.dumps(final_result, indent=2, sort_keys=False))
        fout.close()
    return output_file_name


def main():
    if len(sys.argv) < 1:
        print("Usage: %s file" % sys.argv[0])
        exit()

    session = get_session(sys.argv[1])
    all_files = get_all_session_files(session)
    final_result = merge_session_files(all_files)
    output_file_name = write_merged_session(session, final_result)

    print("Done merging %d files to %s" % (len(all_files), output_file_name))

if __name__ == '__main__':
    main()
