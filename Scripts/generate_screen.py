import os
import csv


def convert_screen_name(data):
    temp = data.replace('][', '_')
    temp = temp.replace('[', '').replace(']', '')
    return "Screen_" + temp


def read_csv(file_path):
    data = {}
    with open(file_path, 'rb') as csvfile:
        event_reader = csv.reader(csvfile, delimiter=',', quotechar='|')
        rownum = 0
        for row in event_reader:
            if rownum == 0:
                header = row
            else:
                name = convert_screen_name(row[0])
                value = "[iOS]"+row[0]
                obj = {
                    name: value
                }
                data[name] = value
            rownum += 1
    return data


def print_extern(data):
    lines = []
    for key, value in data.iteritems():
        lines.append('extern NSString *%s;' % key)
    return '''%s''' % '\n'.join(lines)


def print_extern_value(data):
    lines = []
    for key, value in data.iteritems():
        lines.append('NSString *%s = @\"%s";' % (key, value))
    return '''%s''' % '\n'.join(lines)


def print_extern_declare(data):
    enu = print_extern(data)
    buffer = '''//
//  TrackerScreen.h
//  ZaloPayAnalytics
//
//  Auto-generated.
//  Copyright (c) 2016-2017 VNG Corporation. All rights reserved.
//

#ifndef __TRACKER_SCREEN_H__
#define __TRACKER_SCREEN_H__

#import <Foundation/Foundation.h>

%s

#endif // __TRACKER_SCREEN_H__
''' % enu
    return buffer


def print_extern_implementation(data):
    enu = print_extern_value(data)
    buffer = '''//
//  TrackerScreen.m
//  ZaloPayAnalytics
//
//  Auto-generated.
//  Copyright (c) 2016-2017 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TrackerScreen.h"

%s

''' % enu
    return buffer


def write_output(file_path, buffer):
    print("Updating %s" % file_path)
    with open(file_path, 'w') as etd:
        etd.write(buffer)

data = read_csv('ScreenName.csv')
externString = print_extern_declare(data)
valueString = print_extern_implementation(data)
dest_path = "../ZaloPayAnalytics/ZaloPayAnalytics"
write_output(os.path.join(dest_path, "TrackerScreen.h"), externString)
write_output(os.path.join(dest_path, "TrackerScreen.m"), valueString)
print(externString)
print(valueString)
