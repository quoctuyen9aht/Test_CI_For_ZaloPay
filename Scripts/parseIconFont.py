
import json
import os

def write_output(file_path, data):
    print("Updating %s" % file_path)
    with open(file_path, 'w') as outfile:
        json.dump(data, outfile, indent=4)

allIcon = {}
with open("selection.json") as json_file:
    json_data = json.load(json_file)
    icons = json_data["icons"]
    
    
    for oneIcon in icons:
        name = oneIcon["properties"]["name"]
        code = oneIcon["properties"]["code"]
        colors = oneIcon["attrs"]
        
        jsonIcon = {}
        jsonIcon["rawcode"] = code
        jsonIcon["colors"] = colors
        jsonIcon["code"] = unichr(code)
        try:
            codes = oneIcon["properties"]["codes"]
            jsonIcon["codes"] = codes
        except KeyError:
            print("no codes")
        print(jsonIcon)
        allIcon[name] = jsonIcon
write_output("../ZaloPay/Resources/Fonts/zalopay.json", allIcon)
write_output("zalopay.json", allIcon)

