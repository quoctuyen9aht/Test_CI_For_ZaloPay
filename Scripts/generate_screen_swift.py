import os
import csv


def convert_screen_name(data):
    temp = data.replace('][', '_')
    temp = temp.replace('[', '').replace(']', '')
    return "Screen_" + temp


def read_csv(file_path):
    data = {}
    with open(file_path, 'rb') as csvfile:
        event_reader = csv.reader(csvfile, delimiter=',', quotechar='|')
        rownum = 0
        for row in event_reader:
            if rownum == 0:
                header = row
            else:
                name = convert_screen_name(row[0])
                value = row[0]
                obj = {
                    name: value
                }
                data[name] = value
            rownum += 1
    return data

def print_extern_value(data):
    lines = []
    for key, value in data.iteritems():
        lines.append('   public static let %s = \"%s"' % (key, value))
    return '''%s''' % '\n'.join(lines)


def print_extern_declare(data):
    enu = print_extern_value(data)
    buffer = '''//
//  ZPTrackerScreen.swift
//  ZaloPayAnalytics
//
//  Auto-generated.
//  Copyright (c) 2016-2017 VNG Corporation. All rights reserved.
//

import Foundation

@objcMembers
public final class ZPTrackerScreen: NSObject {
%s
}

''' % enu
    return buffer




def write_output(file_path, buffer):
    print("Updating %s" % file_path)
    with open(file_path, 'w') as etd:
        etd.write(buffer)

data = read_csv('ScreenName.csv')
implementation_file_buffer = print_extern_declare(data)

write_output("../ZaloPayAnalyticsSwift/ZaloPayAnalyticsSwift/ZPTrackerScreen.swift", implementation_file_buffer)

print("Done!")
