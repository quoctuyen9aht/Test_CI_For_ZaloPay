//
//  ZaloPayTests.m
//  ZaloPayTests
//
//  Created by bonnpv on 6/1/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <ZaloPayCommon/NSNumber+Decimal.h>
#import <ZaloPayNetwork/ZPReactNetwork.h>
#import <ZaloPayAppManager/PaymentReactNativeApp/ZPMerchantTable.h>
#import <ZaloPayDatabase/ZPTransactionLogTable.h>
#import <ZaloPayDatabase/ZPCacheDataTable.h>
#import <ZaloPayDatabase/PerUserDataBase.h>
#import <ZaloPayAppManager/TransactionLogsConst.h>
#import <ZaloPayWalletSDK/ZPBill+ExtInfo.h>
#import <ZaloPayDatabase/GlobalDatabase.h>
#import <ZaloPayWalletSDK/ZaloPaySDKUtil.h>

@interface ZaloPayTests : XCTestCase

@end

@implementation ZaloPayTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

//- (void)testPerformanceExample {
//    // This is an example of a performance test case.
//    [self measureBlock:^{
//        // Put the code you want to measure the time of here.
//    }];
//}


- (void)testUpdateFragment {
    PerUserDataBase *perUserDataBase = [[PerUserDataBase alloc] init];
    [perUserDataBase registerTable:[ZPTransactionLogTable self]];
    [perUserDataBase createDBWithUser:[[NSNumber numberWithFloat:random()] stringValue] oldUser:@""];
    
//    [perUserDataBase ensureDataBase];
    ZPTransactionLogTable* transactionLogTable = [[ZPTransactionLogTable alloc] initWithDB:perUserDataBase];
    [transactionLogTable cleanDB];
    [transactionLogTable updateFragmentWithKey:MainKeyTimestamp
                                       max:1000
                                       min:900
                                statusType:StatusTypeSuccess
                                     order:TransOrderOldest];
    
    
    [transactionLogTable updateFragmentWithKey:500
                                       max:500
                                       min:400
                                statusType:StatusTypeSuccess
                                     order:TransOrderOldest];
    [transactionLogTable setOutOfDataWithTimestamp:400 statusType:StatusTypeSuccess];
    [transactionLogTable updateFragmentWithKey:800
                                       max:700
                                       min:600
                                statusType:StatusTypeSuccess
                                     order:TransOrderOldest];
//    [perUserDataBase updateFragmentWithKey:800
//                                       max:700
//                                       min:600
//                                statusType:StatusTypeSuccess
//                                     order:TransOrderOldest];
    
    
    [transactionLogTable updateFragmentWithKey:MainKeyTimestamp
                                       max:1200
                                       min:1001
                                statusType:StatusTypeSuccess
                                     order:TransOrderLastest];
    [transactionLogTable updateFragmentWithKey:950
                                       max:880
                                       min:800
                                statusType:StatusTypeSuccess
                                     order:TransOrderOldest];
    
    
    NSTimeInterval max = [transactionLogTable maxTimestampFragmentWithKey:MainKeyTimestamp
                                                           statusType:StatusTypeSuccess];
    NSTimeInterval min = [transactionLogTable minTimestampFragmentWithKey:MainKeyTimestamp
                                                           statusType:StatusTypeSuccess];
    
    XCTAssertEqual(max, 1200);
    XCTAssertEqual(min, 600);
    
    XCTAssertEqual([transactionLogTable minTimestampFragmentWithKey:950
                                                     statusType:StatusTypeSuccess], 600);
    XCTAssertEqual([transactionLogTable maxTimestampFragmentWithKey:950
                                                     statusType:StatusTypeSuccess], 1200);
    
    XCTAssertEqual([transactionLogTable minTimestampFragmentWithKey:800
                                                     statusType:StatusTypeSuccess], 600);
    XCTAssertEqual([transactionLogTable outOfDataWithTimestamp:800
                                                     statusType:StatusTypeSuccess], NO);
    XCTAssertEqual([transactionLogTable outOfDataWithTimestamp:550
                                                statusType:StatusTypeSuccess], NO);
    
    XCTAssertEqual([transactionLogTable outOfDataWithTimestamp:100
                                                statusType:StatusTypeSuccess], YES);
    
}
/*
- (void)testReactNetwork {
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    ZPReactNetwork *network = [[ZPReactNetwork alloc] init];
    [[network requestWithMethod:@"GET"
                         header:[self headers]
                      urlString:@"https://www.google.com.vn"
                         params:@{@"maccesstoken": @"abc"}
                           body:nil] subscribeNext:^(NSString *resultString) {
        XCTAssert(resultString.length > 0,@"result length == 0");
        dispatch_semaphore_signal(sema);
    } error:^(NSError *error) {
        XCTFail(@"error = %@",error);
        dispatch_semaphore_signal(sema);
    } completed:^{
        
    }];
    
    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
}

- (void)testJsonString {
    ZPReactNetwork *network = [[ZPReactNetwork alloc] init];
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    [[network requestWithMethod:@"GET"
                         header:[self headers]
                      urlString:@"https://zalopay.com.vn/um/getmerchantuserinfo?param1=abc"
                         params:@{@"maccesstoken": @"abc"}
                           body:nil] subscribeNext:^(NSString *resultString) {
        XCTAssert(resultString.length > 0,@"result length == 0");
        dispatch_semaphore_signal(sema);
    } error:^(NSError *error) {
        XCTFail(@"error = %@",error);
        dispatch_semaphore_signal(sema);
    }];
    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
}
 */

- (NSDictionary *)headers {
    NSMutableDictionary *header = [NSMutableDictionary dictionary];
    [header setObject:@"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:44.0) Gecko/20100101 Firefox/44.0" forKey:@"User-Agent"];
    [header setObject:@"text/html, */*; q=0.01" forKey:@"Accept"];
    [header setObject:@"en-US,en;q=0.5" forKey:@"Accept-Language"];
    [header setObject:@"keep-alive" forKey:@"Connection"];
    [header setObject:@"application/json;charset=UTF-8" forKey:@"Content-Type"];
    return header;
}

- (void)testMerchantInDataBase {
    [[GlobalDatabase sharedInstance] registerTable:[ZPCacheDataTable self]];
    [[PerUserDataBase sharedInstance] registerTable:[ZPMerchantTable self]];
    
    [[GlobalDatabase sharedInstance] createDBWithConfigVersion];
    
    ZPCacheDataTable* cacheDataTable = [[ZPCacheDataTable alloc] initWithDB:[GlobalDatabase sharedInstance]];
    NSString *oldUid = [cacheDataTable cacheDataValueForKey:kLastLoginUser];
    [[PerUserDataBase sharedInstance] createDBWithUser:oldUid oldUser:oldUid];
    
    NSArray *listAppIds = @[@(11), @(17), @(18), @(20)];
    ZPMerchantTable* merchantTable = [[ZPMerchantTable alloc] initWithDB:[PerUserDataBase sharedInstance]];
    NSDictionary *dic = [merchantTable checkMerchantUserInfoWithAppIds:listAppIds];
    NSLog(@"appids = %@",dic);    
}

- (void)testParseBillItem {
    NSString *item = @"1";
    XCTAssertEqual( [ZaloPaySDKUtil extInfoFromString:item], nil);
    
    item = @"default text";
    XCTAssertEqual( [ZaloPaySDKUtil extInfoFromString:item], nil);

    item = @"transtype:4";
    XCTAssertEqual( [ZaloPaySDKUtil extInfoFromString:item], nil);
    
    item = @"{\"transtype\":4,\"ext\":\" \"}";
    XCTAssertEqual( [ZaloPaySDKUtil extInfoFromString:item].count, 0);
    
    item = @"{\"ext\":\"Người nhận:Anh\",\"transtype\":4}";
    NSArray<ZPBillExtraInfo*> *value = [ZaloPaySDKUtil extInfoFromString:item];
    XCTAssertEqual(value.count, 1);
    
    NSString *detail = ((ZPBillExtraInfo*)[value firstObject]).extraValue;
    XCTAssertEqual([detail isEqualToString:@"Anh"], true);
    
    item = @"{\"ext\":\"Người nhận:Anh:ABC\",\"transtype\":4}";
    value = [ZaloPaySDKUtil extInfoFromString:item];
    detail = ((ZPBillExtraInfo*)[value firstObject]).extraValue;
    XCTAssertEqual(value.count, 1);
    XCTAssertEqual([detail isEqualToString:@"Anh:ABC"], true);
    
    item = @"{\"ext\":\"Người nhận:Anh\\thello:bonpv\",\"transtype\":4}";
    value = [ZaloPaySDKUtil extInfoFromString:item];
    XCTAssertEqual(value.count, 2);
}

@end
