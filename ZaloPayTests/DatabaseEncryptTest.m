//
//  DatabaseEncryptTest.m
//  ZaloPayTests
//
//  Created by vuongvv on 12/6/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <ZaloPayDatabase/PaymentDatabase.h>
#import <ZaloPayDatabase/ZPFMDatabaseEncryptHelper.h>
#import <sqlite3.h>
@interface DatabaseEncryptTest : XCTestCase

@end

@implementation DatabaseEncryptTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}
-(void)testEncrypt{
    
    BOOL isTest = false;
    
    NSString *cachesDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *folderPath = [cachesDirectory stringByAppendingPathComponent:@"Databases"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:folderPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:folderPath
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:nil];
    }
    NSString *databasePath = [folderPath stringByAppendingString:[NSString stringWithFormat:@"/ZaloPayEncryptTest.db"]];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:databasePath]) {
        [ZPFMDatabaseEncryptHelper deleteDatabase:databasePath];
    }
    
    //Read
    sqlite3 *db;
    int ret = sqlite3_open([databasePath UTF8String], &db);
    ret = sqlite3_exec(db, (const char*) "SELECT count(*) FROM sqlite_master;", NULL, NULL, NULL);
    if (ret == SQLITE_OK) {
        [ZPFMDatabaseEncryptHelper encrypted:databasePath];
        ret = sqlite3_open([databasePath UTF8String], &db);
        ret = sqlite3_exec(db, (const char*) "SELECT count(*) FROM sqlite_master;", NULL, NULL, NULL);
        if (ret != SQLITE_OK) {
            BOOL openWithKey = [ZPFMDatabaseEncryptHelper openDB:db path:databasePath encryptedKey:[ZPFMDatabaseEncryptHelper newKey]];
            if (openWithKey) {
                isTest = true;
            }
        }
    }
    XCTAssertTrue(isTest, "fail encrypt database");
}
@end
