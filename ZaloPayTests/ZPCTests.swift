//
//  ZPCTests.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 8/8/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import XCTest
@testable import ZaloPayProfile

class ZPCTests: XCTestCase {
    


    func createTestData() -> NSMutableDictionary {
        let dict: NSMutableDictionary = ["avatar":"",
                                         "displayNameUCB":"",
                                         "displayNameZLF":"Vi",
                                         "firstName":"",
                                         "lastName":"",
                                         "phoneNumber":"",
                                         "zaloId":"3034900213345568597",
                                         "zalopayId":"",
                                         "zalopayName":""
        ]
        return dict
    }
    
    func createViewController () ->ZPContactListViewController {
        let view = ZPContactListViewController()
        let presenter: ZPContactListPresenterProtocol & ZPContactListInteractorOutputProtocol = ZPContactListPresenter()
        let interactor: ZPContactListInteractorInputProtocol = ZPContactListInteractor()
        let router: ZPContactListRouterProtocol = ZPContactListRouter()
        
        view.presenter = presenter
        view.config.sourceZPC = .fromRedPacket
        view.config.viewMode = ViewModeKeyboard_ABC
        view.config.displayMode = .normal
        
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        return view
    }

    
    func userFromZPCDatabaseDic(_ dict:NSDictionary) -> ZPContactSwift {
        let user = ZPContactSwift()
        user.zaloId = dict.object(forKey: "zaloId") as! String
        user.displayNameUCB = dict.object(forKey: "displayNameUCB") as! String
        user.displayNameZLF = dict.object(forKey: "displayNameZLF") as! String
        user.firstName = dict.object(forKey: "firstName") as! String
        user.lastName = dict.object(forKey: "lastName") as! String
        user.phoneNumber = dict.object(forKey: "phoneNumber") as! String
        user.zaloPayId = dict.object(forKey: "zalopayId") as! String
        user.zaloPayName = dict.object(forKey: "zalopayName") as! String
        user.zpContactID = "\(user.phoneNumber)\(user.zaloId)\(user.zaloPayId)" 
        var displayNameFinal = ""
        displayNameFinal = user.displayNameUCB.count > 0 ? user.displayNameUCB : user.displayNameZLF
        user.displayName = displayNameFinal
        return user
    }
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        ZPUIAppDelegate.sharedInstance.loadResourceString()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
    }
    
    func testFuzzyMatchingSearching() {
        let controller = ZPContactListInteractor()
        let input = "+(0122)4379580"
        let userCase1 = "01224379580"
        let actual1 = controller.normalizePhoneNumber(input)
        XCTAssert(userCase1 == actual1)
        
        let input2 = "090 4379540"
        let userCase2 = "0904379540"
        let actual2 = controller.normalizePhoneNumber(input2)
        XCTAssert(userCase2 == actual2)
        
        let input3 = "-0168 (4379540)"
        let userCase3 = "01684379540"
        let actual3 = controller.normalizePhoneNumber(input3)
        XCTAssert(userCase3 == actual3)
        
        let input4 = "0909932947"
        let userCase4 = "0909932947"
        let actual4 = controller.normalizePhoneNumber(input4)
        XCTAssert(userCase4 == actual4)
        
        let input5 = "Dì 5"
        let userCase5 = "Dì 5"
        let actual5 = controller.normalizePhoneNumber(input5)
        XCTAssert(actual5 == actual5)
        
        let input6 = "Nguyen Van A +(123)"
        let userCase6 = "Nguyen Van A +(123)"
        let actual6 = controller.normalizePhoneNumber(input6)
        XCTAssert(userCase6 == actual6)
    }
    
    func testGetMessageAttributedString() {

        
        let view = createViewController()
        let dict = createTestData()
        let inviteModel = ZPCInviteFriendModel()
        

        //case 1: normal
        let userNormal = self.userFromZPCDatabaseDic(dict)
        let messageAttributedStringNormal = inviteModel.getMessageAttributedStringFrom(userNormal, source: .fromTransferMoney_Contact)
        XCTAssert(messageAttributedStringNormal.length > 0)
        
        // case 2: empty display name
        dict.setValue("", forKey: "displayNameZLF")
        let userEmptyDisplayName = self.userFromZPCDatabaseDic(dict)
        let messageAttributedStringEmptyDisplayName = inviteModel.getMessageAttributedStringFrom(userNormal, source: .fromTransferMoney_Contact)
        XCTAssert(messageAttributedStringEmptyDisplayName.length > 0)
        
        // case 3: zpContactID contains 2 in 3 fields {zaloId,zalopayId,zalopayName}
        dict.setValue("123456", forKey: "zalopayId")
        let userCase3 = self.userFromZPCDatabaseDic(dict)
        let messageAttributedStringCase3 = inviteModel.getMessageAttributedStringFrom(userNormal, source: .fromTransferMoney_Contact)
        XCTAssert(messageAttributedStringCase3.length > 0)
        
        // case 4: zpContactID contains all fields {zaloId,zalopayId,zalopayName}
        dict.setValue("0907000000", forKey: "phoneNumber")
        let userCase4 = self.userFromZPCDatabaseDic(dict)
        let messageAttributedStringCase4 = inviteModel.getMessageAttributedStringFrom(userNormal, source: .fromTransferMoney_Contact)
        XCTAssert(messageAttributedStringCase4.length > 0)

    }
    
    func testNormalizePhonePastageBoard() {
        let controller = ZPContactListViewController()
        var value = "+841694404393"
        UIPasteboard.general.string = value
        XCTAssert(controller.prehandleSearchText(value as NSString) == "01694404393")
        
        value = "+84 1694 404 393"
        UIPasteboard.general.string = value
        XCTAssert(controller.prehandleSearchText(value as NSString) == "01694404393")
        
        value = "+841694404abc393"
        UIPasteboard.general.string = value
        let result = controller.prehandleSearchText(value as NSString)
        print("result = \(result)")
        XCTAssert(result == "+841694404abc393")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    
}
