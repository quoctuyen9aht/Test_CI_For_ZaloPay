//
//  OptionalTest.swift
//  ZaloPayTests
//
//  Created by Bon Bon on 4/11/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import XCTest
import ZaloPayCommonSwift

class OptionalTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    func testNil () {
        var value: Int?
        XCTAssert(value.isNil, "Option isNil fail")
        value = 1
        XCTAssert(!value.isNil, "Option isNil fail")
    }
    
    func testSome () {
        var value: Int?
        XCTAssert(!value.isSome, "Option isSome fail")
        value = 1
        XCTAssert(value.isSome, "Option isSome fail")
    }
    
    func testOr() {
        var value: Int?
        XCTAssert(value.or(10) == 10, "Option Or fail")
        value = 1
        XCTAssert(value.or(10) == 1, "Option Or fail")
    }
    
    func testElse() {
        var value: Int?
        XCTAssert(value.or(else: 10) == 10, "Option Or fail")
        XCTAssert(value.or(else: { () -> Int in
            return 10
        }) == 10, "Option Or fail")
        
        value = 1
        XCTAssert(value.or(else: 10) == 1, "Option Or fail")
    }
    
}
