//
//  ZPSettingsConfigTest.swift
//  ZaloPayTests
//
//  Created by CPU11680 on 1/16/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import XCTest

class ZPSettingsConfigTest: XCTestCase {
    private let userId = "123"
    
    override func setUp() {
        ZPSettingsConfig.loadUserSetting(userId)
        let settingConfig = ZPSettingsConfig.sharedInstance()
        settingConfig.askForPassword = true
        settingConfig.usingTouchId = true
        settingConfig.turnOffTouchIdManual = true
        settingConfig.paymentCodeShowSecurityIntro = true
        settingConfig.paymentCodeChannelSelect = 1
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAskForPassword() {
        ZPSettingsConfig.loadUserSetting(userId)
        let settingConfig = ZPSettingsConfig.sharedInstance()
        XCTAssert(settingConfig.askForPassword)
    }
    
    func testUsingTouchId() {
        ZPSettingsConfig.loadUserSetting(userId)
        let settingConfig = ZPSettingsConfig.sharedInstance()
        XCTAssert(settingConfig.usingTouchId)
    }
    
    func testTurnOffTouchIdManual() {
        ZPSettingsConfig.loadUserSetting(userId)
        let settingConfig = ZPSettingsConfig.sharedInstance()
        XCTAssert(settingConfig.turnOffTouchIdManual)
    }
    
    func testPaymentCodeShowSecurityIntro() {
        func testTurnOffTouchIdManual() {
            ZPSettingsConfig.loadUserSetting(userId)
            let settingConfig = ZPSettingsConfig.sharedInstance()
            XCTAssert(settingConfig.paymentCodeShowSecurityIntro)
        }

    }
    
    func testPaymentCodeChannelSelect() {
        ZPSettingsConfig.loadUserSetting(userId)
        let settingConfig = ZPSettingsConfig.sharedInstance()
        XCTAssert(settingConfig.paymentCodeChannelSelect == 1)
    }
    
    func testUpdateUser() {
        let currentId = self.userId;
        ZPSettingsConfig.loadUserSetting(userId)
        ZPSettingsConfig.sharedInstance().paymentCodeChannelSelect = 100
        
        ZPSettingsConfig.loadUserSetting("456")
        XCTAssert(ZPSettingsConfig.sharedInstance().paymentCodeChannelSelect != 100)
    }
    
    //    func testAskForPassword(){
    //        PerUserDataBase.sharedInstance().cacheDataUpdateValue("", forKey: kAskForPassword)
    //        let value = ZPSettingsConfig.sharedInstance.askForPassword
    //        XCTAssertTrue(value, "fail AskForPassword")
    //    }
    //    func testUsingTouchID(){
    //        PerUserDataBase.sharedInstance().cacheDataUpdateValue("", forKey: kUsingTouchID)
    //        let value = ZPSettingsConfig.sharedInstance.usingTouchId
    //        XCTAssertFalse(value, "fail UsingTouchID")
    //    }
    //    func testTurnOffTouchIdManual(){
    //        PerUserDataBase.sharedInstance().cacheDataUpdateValue("", forKey: kTurnOffTouchIdManual)
    //        let value = ZPSettingsConfig.sharedInstance.turnOffTouchIdManual
    //        XCTAssertFalse(value, "fail TurnOffTouchIdManual")
    //    }
    //    func testPaymentCodeShowSecurityIntro(){
    //        PerUserDataBase.sharedInstance().cacheDataUpdateValue("", forKey: kPaymentCodeShowSecurityIntro)
    //        let value = ZPSettingsConfig.sharedInstance.paymentCodeShowSecurityIntro
    //        XCTAssertTrue(value, "fail PaymentCodeShowSecurityIntro")
    //    }
    //    func testPaymentCodeChannelSelect(){
    //        PerUserDataBase.sharedInstance().cacheDataUpdateValue("", forKey: kPaymentCodeChannelSelect)
    //        let value = ZPSettingsConfig.sharedInstance.paymentCodeChannelSelect
    //        XCTAssertTrue(value == -1, "fail PaymentCodeChannelSelect")
    //    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
