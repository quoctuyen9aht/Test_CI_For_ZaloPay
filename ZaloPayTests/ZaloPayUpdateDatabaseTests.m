//
//  ZaloPayUpdateDatabaseTests.m
//  ZaloPay
//
//  Created by Dung Vu on 7/13/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <XCTest/XCTest.h>
//#import <ZaloPayDatabase/PerUserDataBase.h>
//#import <ZPRecentTransferMoneyTable.h>
//#import ZALOPAY_MODULE_SWIFT

//@interface ZaloPayUpdateDatabaseTests : XCTestCase
//@property (strong, nonatomic) PerUserDataBase *db;
//@end
//
//static NSString *kUserID = @"userIDTest";
//@implementation ZaloPayUpdateDatabaseTests
//
//- (void)setUp {
//    [super setUp];
//    // Put setup code here. This method is called before the invocation of each test method in the class.
//    [self setupDatabase];
//}
//
//- (void)setupDatabase {
//    self.db = [PerUserDataBase new];
//    [self registerTables];
//
//    BOOL create = [_db createDBWithUser:kUserID oldUser:nil];
//    XCTAssertTrue(create, @"Database error create!!!!!");
//    XCTAssertTrue([self.db allTables].count > 0, @"Error Create Tables!!!!!!");
//}
//
//- (void) registerTables {
//    [self.db registerTable:[ZPRecentTransferMoneyTable self]];
//    [self.db registerTable:[ZPLixiNotificationTable self]];
//}
//
//
//- (void)tearDown {
//    // Put teardown code here. This method is called after the invocation of each test method in the class.
//    [super tearDown];
//}
//
//- (void)testDeleteTableNil {
//    [self.db dropAllTableWithoutTableNames:nil];
//    NSInteger after = [self.db allTables].count;
//    XCTAssertTrue(after == 0, @"Database has to remove all table!!!!");
//}
//
//- (void)testDeleteTableNotExists {
//    [self.db dropAllTableWithoutTableNames:[NSSet setWithArray:@[@"test1",@"test2"]]];
//    NSInteger after = [self.db allTables].count;
//    XCTAssertTrue(after == 0, @"Database has to remove all table!!!!");
//}
//
//
//- (void)testDeleteTableExistOnTable {
//    NSInteger current = [self.db allTables].count;
//    NSSet *tablesWillDelete = [NSSet setWithArray:@[@"LixiNotify",@"RecentTransferFriend"]];
//
//    [self.db dropAllTableWithoutTableNames:tablesWillDelete];
//    NSMutableSet *remains = [self.db allTables];
//    NSInteger after = remains.count;
//
//    XCTAssertTrue(after < current, "Database not delete correct");
//    XCTAssertTrue(after == tablesWillDelete.count, @"Database has to have remain : %ld tables",tablesWillDelete.count);
//
//    // Check Correct Table remain
//    [remains minusSet:tablesWillDelete];
//    XCTAssertTrue(remains.count == 0, @"Database remove wrong table");
//}
//
//- (void) testDeleteTableWithExistData {
//    //Insert value
//    UserTransferData *user = [UserTransferData new];
//    user.zaloId = kUserID;
//    user.avatar = @"test";
//    user.displayName = @"name";
//    user.phone = @"phone";
//    user.transferMoney = @"1234";
//    user.message = @"test transfer";
//    user.paymentUserId = @"djhsd";
//    user.accountName = @"test1234";
//
//    ZPRecentTransferMoneyTable* recentTransferMoneyTable = [[ZPRecentTransferMoneyTable alloc] initWithDB:self.db];
//    [recentTransferMoneyTable addRecentTransferFriend:user];
//    [self testDeleteTableExistOnTable];
//
//    NSArray *transfer = [recentTransferMoneyTable recentTransferFriends];
//    XCTAssertTrue(transfer.count == 1, @"Database have to keep data!!!");
//
//    // Check correct data
//    UserTransferData *check = transfer.firstObject;
//    XCTAssert([check isKindOfClass:[UserTransferData class]], "Database reponse not correct class");
//    if (!check) {
//        XCTAssertTrue([check.zaloId isEqualToString:user.zaloId], "Database response not correct data saved");
//    }
//}

//@end
