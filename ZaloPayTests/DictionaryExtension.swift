//
//  DictionaryExtension.swift
//  ZaloPayTests
//
//  Created by bonnpv on 11/7/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import XCTest

class DictionaryExtension: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func createTestData() -> [String: Any] {
        var data = [String: Any]()
        data["stringValue1"] = "10"
        data["stringValue2"] = "10abc"
        data["intValue"] = 10
        data["floatValue"] = Float(10.5)
        data["doubleValue"] = Double(10.5)
        data["int64Value"] = Int64(10)
        data["nsNumberValue"] = NSNumber(value: 10)
        return data
    }
    
    func testParseInt() {
        let data = createTestData()
        XCTAssert(data.int(forKey: "stringValue1") == 10)
        XCTAssert(data.int(forKey: "stringValue2") == 0)
        XCTAssert(data.int(forKey: "intValue") == 10)
        XCTAssert(data.int(forKey: "floatValue") == 10)
        XCTAssert(data.int(forKey: "doubleValue") == 10)
        XCTAssert(data.int(forKey: "int64Value") == 10)
        XCTAssert(data.int(forKey: "nsNumberValue") == 10)
        XCTAssert(data.int(forKey: "nsNumberValue2") == 0)
    }
    
    func testParseString() {
        let data = createTestData()
        XCTAssert(data.string(forKey: "stringValue1") == "10")
        XCTAssert(data.string(forKey: "stringValue2") == "10abc")
        XCTAssert(data.string(forKey: "intValue") == "10")
        XCTAssert(data.string(forKey: "floatValue") == "10.5")
        XCTAssert(data.string(forKey: "doubleValue") == "10.5")
        XCTAssert(data.string(forKey: "int64Value") == "10")
        XCTAssert(data.string(forKey: "nsNumberValue") == "10")
        XCTAssert(data.string(forKey: "nsNumberValue2") == "")
    }
    
    func testDouble() {
        let data = createTestData()
        XCTAssert(data.double(forKey: "stringValue1") == 10)
        XCTAssert(data.double(forKey: "stringValue2") == 0)
        XCTAssert(data.double(forKey: "intValue") == 10)
        XCTAssert(data.double(forKey: "floatValue") == 10.5)
        XCTAssert(data.double(forKey: "doubleValue") == 10.5)
        XCTAssert(data.double(forKey: "int64Value") == 10)
        XCTAssert(data.double(forKey: "nsNumberValue") == 10)
        XCTAssert(data.double(forKey: "nsNumberValue2") == 0)
    }
    
    func testInt32() {
        let data = createTestData()
        XCTAssert(data.int32(forKey: "stringValue1") == 10)
        XCTAssert(data.int32(forKey: "stringValue2") == 0)
        XCTAssert(data.int32(forKey: "intValue") == 10)
        XCTAssert(data.int32(forKey: "floatValue") == 10)
        XCTAssert(data.int32(forKey: "doubleValue") == 10)
        XCTAssert(data.int32(forKey: "int64Value") == 10)
        XCTAssert(data.int32(forKey: "nsNumberValue") == 10)
        XCTAssert(data.int32(forKey: "nsNumberValue2") == 0)
    }
    
    func testUInt() {
        let data = createTestData()
        XCTAssert(data.uInt(forKey: "stringValue1") == 10)
        XCTAssert(data.uInt(forKey: "stringValue2") == 0)
        XCTAssert(data.uInt(forKey: "intValue") == 10)
        XCTAssert(data.uInt(forKey: "floatValue") == 10)
        XCTAssert(data.uInt(forKey: "doubleValue") == 10)
        XCTAssert(data.uInt(forKey: "int64Value") == 10)
        XCTAssert(data.uInt(forKey: "nsNumberValue") == 10)
        XCTAssert(data.uInt(forKey: "nsNumberValue2") == 0)
    }
}
