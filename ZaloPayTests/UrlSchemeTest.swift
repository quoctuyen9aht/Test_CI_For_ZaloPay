//
//  UrlSchemeTest.swift
//  ZaloPayTests
//
//  Created by bonnpv on 10/26/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import XCTest

class UrlSchemeTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
/*
     case scheme_backtoapp                = "zalopay-1://backtoapp"
     case scheme_post                     = "zalopay-1://post"
     case scheme_backtologin              = "zalopay-1://backtologin"
     case scheme_pay                      = "zalopay://pay"
     case scheme_otp                      = "zalopay://otp"
     case scheme_zalopayvn_view_transid   = "zalopay://zalopay.vn/view/transid/"
     case scheme_launch_app               = "zalopay://launch/app/"
     case scheme_launch_tab               = "zalopay://launch/tab/"
     case scheme_launch_function          = "zalopay://launch/f/"
     case scheme_tel                      = "tel://"
     case scheme_mailto                   = "mailto://"
     case scheme_zalo_transfer_money      = "zalopay-zapi-28://app/transfer"
     case scheme_zalo_mywallet            = "zalopay-zapi-29://app/mywallet"
 */
    
    func testBackToApp() {
        XCTAssert(canOpenUrl("zalopay-1://backtoapp"))
        XCTAssert(canOpenUrl("zalopay-1://back") == false)
    }
    
    func testPost()  {
        XCTAssert(canOpenUrl("zalopay-1://post"))
        XCTAssert(canOpenUrl("zalopay-1://") == false)
    }
    
    func backToLogin () {
        XCTAssert(canOpenUrl("zalopay-1://backtologin"))
    }
    
    func testPay()  {
        XCTAssert(canOpenUrl(kSchemePayment + "://pay"))
    }
    
    func testOtp() {
        XCTAssert(canOpenUrl("zalopay://otp/123456"))
    }
    
    func testTransactionDetail() {
        XCTAssert(canOpenUrl("zalopay://zalopay.vn/view/transid/123456"))
    }
    
    func testLaunchApp() {
        XCTAssert(canOpenUrl("zalopay://launch/app/"))
    }
    
    func testLaunchTab() {
        XCTAssert(canOpenUrl("zalopay://launch/tab/home"))
        XCTAssert(canOpenUrl("zalopay://launch/tab/abc"))
    }
    
    func testLaunchFunction() {
        XCTAssert(canOpenUrl("zalopay://launch/f/transfer"))
        XCTAssert(canOpenUrl("zalopay://launch/f/abc"))
    }
    
    func testTel() {
        XCTAssert(canOpenUrl("tel://0969599645"))
    }
    
    func testMail() {
        XCTAssert(canOpenUrl("mailto://anhbontb@gmail.com"))
    }
    
    func testTransferMoney() {
        XCTAssert(canOpenUrl("zalopay-zapi-28://app/transfer"))
    }
    
    func testMyWallet() {
        XCTAssert(canOpenUrl("zalopay-zapi-29://app/mywallet"))
    }
    
    func canOpenUrl(_ urlString: String) -> Bool{
        let url:URL = URL(string: urlString)!
        let urlHandle = ZPSchemeUrlHandle.sharedInstance
        let scheme = ZPScheme(schemeWith: url, sourceApplication: "")
        return urlHandle.findHander(scheme: scheme) != nil
    }
    
    func testParseScheme() {
//        let urlScheme = "zalopay-1://post?muid=8VRFzFrDo1V8qgRbMrUMFg2UbqZEGvOQbae8A2gPeew&maccesstoken=q3PSp0CNkh6ZqQ6totdGthsWO0gRttuqqsTjStOrXRc&appid=15&apptransid=1801190063473&appuser=1029265222671572992&apptime=1516331798459&item=com.vng.dmzvn.item100&description=Mua Một Túi Đá trong Game 360mobi Ngôi Sao Bộ Lạc cho Nhân vật 56766218630#58&embeddata=&amount=93500&mac=f51c643b69144811ed378d2aebfa2dec8af2d7c634afafeb0e419e97ff517f38"
        let urlScheme = "zalopay-1://post?muid=8VRFzFrDo1V8qgRbMrUMFg2UbqZEGvOQbae8A2gPeew&maccesstoken=q3PSp0CNkh6ZqQ6totdGthsWO0gRttuqqsTjStOrXRc&appid=15&apptransid=1801190063473&appuser=1029265222671572992&apptime=1516331798459&item=com.vng.dmzvn.item100&description=Mua+M%E1%BB%99t+T%C3%BAi+%C4%90%C3%A1+trong+Game+360mobi+Ng%C3%B4i+Sao+B%E1%BB%99+L%E1%BA%A1c+cho+Nh%C3%A2n+v%E1%BA%ADt+56766218630#58&embeddata=&amount=93500&mac=f51c643b69144811ed378d2aebfa2dec8af2d7c634afafeb0e419e97ff517f38"
        let url = URL(string: urlScheme)
        
        XCTAssert(url != nil)
        
        let scheme = ZPScheme.init(schemeWith: url!, sourceApplication: "")
        let params = scheme.params
        
        let amount = params.int(forKey: "amount")
        XCTAssert(amount == 93500)
        
        let des = params.string(forKey: "description")
        XCTAssert(des == "Mua Một Túi Đá trong Game 360mobi Ngôi Sao Bộ Lạc cho Nhân vật 56766218630#58")
        
        let muid = params.string(forKey: "muid")
        XCTAssert(muid == "8VRFzFrDo1V8qgRbMrUMFg2UbqZEGvOQbae8A2gPeew")
        
        let maccesstoken = params.string(forKey: "maccesstoken")
        XCTAssert(maccesstoken == "q3PSp0CNkh6ZqQ6totdGthsWO0gRttuqqsTjStOrXRc")
        
        let appId = params.int(forKey: "appid")
        XCTAssert(appId == 15)
        
        let apptransid = params.string(forKey: "apptransid")
        XCTAssert(apptransid == "1801190063473")
        
        let appuser = params.string(forKey: "appuser")
        XCTAssert(appuser == "1029265222671572992")
        
        let apptime = params.int64(forKey: "apptime")
        XCTAssert(apptime == 1516331798459)
        
        let item = params.string(forKey: "item")
        XCTAssert(item == "com.vng.dmzvn.item100")
        
        let mac = params.string(forKey: "mac")
        XCTAssert(mac == "f51c643b69144811ed378d2aebfa2dec8af2d7c634afafeb0e419e97ff517f38")
    }
}
