//
//  ZPPaymentDatabaseTest.swift
//  ZaloPayTests
//
//  Created by tridm2 on 3/28/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import XCTest

class ZPPaymentDatabaseTest: XCTestCase {
    private var perUserDataBase: PerUserDataBase!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        perUserDataBase = PerUserDataBase()
        registerTables()
        createDB()
    }
    
    func registerTables() {
        let tables: [ZPTableProtocol.Type] = [
            ZPCacheDataTable.self, ZPDataManifestTable.self, ZPTransactionLogTable.self, ZPNotifyTable.self, ZPNotifyPopupTable.self,
            ZPProfileTable.self, ZPZaloFriendTable.self, ZPRecentTransferMoneyTable.self, ZPLixiNotificationTable.self, ZPMerchantTable.self, ZPTrackingAppTransidTable.self
        ]
        perUserDataBase.registerTables(tables)
    }
    
    func createDB() {
        let isCreatedDB = perUserDataBase.createDB(withUser: Int(arc4random()).toString(), oldUser: "")
        assert(isCreatedDB, "Database error create!!!!!")
        assert(perUserDataBase.allTables().count > 0, "Error Create Tables!!!!!!")
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCreateTable() {
        let tables = ((Array(perUserDataBase.allTables()) as? [String]) ?? []).sorted(by: { $0 > $1 })
        let expectedTables = [
            "Scheme",
//            "Dictionary",
            "CacheData",
            "DataManifest",
            "TransactionLogs",
            "FragmentTransaction",
            "NotifyMessage",
            "NotifyPopupMessage",
            "ProfileInfomation",
            "ZaloFriendList",
            "UserContactBook",
            "ZPC",
            "UnSavedPhoneContact",
            "ZPCModeKeyboard",
            "RecentTransferFriend",
            "LixiNotify",
            "MerchantUserInfo",
            "TrackingAppTransid",
            "AppTransIdStep",
            "AppTransIdAPI"
        ].sorted(by: { $0 > $1 })
        
        assert(tables == expectedTables)
    }
    
    func testDropTableWhenUpgrade() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let expectedRemainTables = [
//            "Dictionary",
            "Scheme",
            "RecentTransferFriend",
            "CacheData",
            "ZPC"
        ].sorted(by: { $0 > $1 })
        
        perUserDataBase.dropAllTablesCanBeDropped()
        let remainTables = ((Array(perUserDataBase.allTables()) as? [String]) ?? []).sorted(by: { $0 > $1 })
        assert(remainTables == expectedRemainTables)
    }
    
    func testDropAllTables() {
        perUserDataBase.dropAllTables()
        let numberRemainTables = perUserDataBase.allTables().count ?? 0
        assert(numberRemainTables == 0)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
