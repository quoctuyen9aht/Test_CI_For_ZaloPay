//
//  VoucherTest.swift
//  ZaloPayTests
//
//  Created by Phuoc Huynh on 12/14/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import XCTest

class VoucherTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        
    }
    
    func testGetBestVoucher(){
        var arrVoucher:[ZPVoucherHistory]
        let bill = ZPBill()
        
        let zpVoucherManager = ZPVoucherManager()
        arrVoucher = self.createArrVoucherToTest(100,numberVoucherType1: 20,cap: 10000)
        bill.amount = 100000
        let expectBestVoucher = arrVoucher[10];
        let bestVoucher =  zpVoucherManager.getBestVoucher(fromArrayVoucher: arrVoucher, of: bill)
        XCTAssert((bestVoucher?.uid)! == (expectBestVoucher.uid)!)
        
        arrVoucher = self.createArrVoucherToTest(200,numberVoucherType1: 10,cap: 0)
        bill.amount = 200000
        let expectBestVoucher2 = arrVoucher.last;
        let bestVoucher2 =  zpVoucherManager.getBestVoucher(fromArrayVoucher: arrVoucher, of: bill)
        XCTAssert((bestVoucher2?.uid)! == (expectBestVoucher2?.uid)!)
        
        arrVoucher = self.createArrVoucherToTest(190,numberVoucherType1: 20,cap:150000)
        bill.amount = 100000
        let expectBestVoucher3 = arrVoucher[19];
        let bestVoucher3 =  zpVoucherManager.getBestVoucher(fromArrayVoucher: arrVoucher, of: bill)
        XCTAssert((bestVoucher3?.uid)! == (expectBestVoucher3.uid)!)
        
    }
    
    func createArrVoucherToTest(_ numberVouchers:Int, numberVoucherType1:Int, cap:Int) -> [ZPVoucherHistory] {
        var arrVoucher = [ZPVoucherHistory]()
        
        for i in 0...numberVouchers {
            let voucher = ZPVoucherHistory()
            voucher.value = i.toInt64(defaultValue: 0)
            voucher.uid = i.toString()
            voucher.cap = cap
            if i < numberVoucherType1 {
                voucher.valuetype = 1
            }else {
                voucher.valuetype = 2
                voucher.value *= 100
            }
            arrVoucher.append(voucher)
        }
        return arrVoucher
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
