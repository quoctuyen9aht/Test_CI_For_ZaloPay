//
//  AppleNotificationTest.swift
//  ZaloPayTests
//
//  Created by vuongvv on 10/31/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import XCTest

class AppleNotificationTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    func testOpenApp(){
        
        let userInfo: [AnyHashable: Any] = ["embebdata": ["data":"{\"action\":\"openapp\",\"data\":61,\"notificationType\":120}"]]
        let model = ZPAppleNotificationModel(userInfo)
        let handler = ZPAppleNotificationServices.sharedInstance.getHandler(model: model)
        XCTAssert(handler != nil)
    }
    func testOpenWeb(){
        
        let userInfo: [AnyHashable: Any] = ["embebdata": ["data":"{\"action\":\"openweb\",\"data\":\"https://sbpromotion.zalopay.vn/promotion/evoucher\",\"notificationType\":120}"]]
        let model = ZPAppleNotificationModel(userInfo)
        let handler = ZPAppleNotificationServices.sharedInstance.getHandler(model: model)
        XCTAssert(handler != nil)
    }
}
