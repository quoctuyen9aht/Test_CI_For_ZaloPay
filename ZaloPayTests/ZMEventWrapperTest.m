//
//  ZMEventWrapperTest.m
//  ZaloPayTests
//
//  Created by Dung Vu on 11/29/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface ZMEventWrapperTest : XCTestCase
@end

// cheat to see class
@interface ZMEventWrapper: NSObject
@property (nonatomic, weak, nullable) id object;
@property (nonatomic, copy, nullable) ZMEventHandler block;
- (instancetype _Nonnull)initWith:(id _Nullable)object using:(ZMEventHandler _Nullable)block;
- (void)excuteWith:(ZMEvent *_Nullable)e;
- (RACSignal *_Nonnull)eDealloc;

@end

@implementation ZMEventWrapperTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

#pragma mark - test capture object
- (void)testCaptureObject {
    NSObject *object = [NSObject new];
    //wrapper
    ZMEventWrapper *wrapper = [[ZMEventWrapper alloc] initWith:object using:^(ZMEvent *event) {
        DDLogInfo(@"Test");
        XCTAssert(NO, "It's still keep reference");
    }];
    
    [self operation:^(ZMEvent *event) {
        [wrapper excuteWith:event];
    }];
    
    // set object nil
    [[wrapper eDealloc] subscribeCompleted:^{
        XCTAssertTrue(YES,@"Correct");
    }];
    
    object = nil;
}

- (void)operation:(ZMEventHandler _Nullable)block {
    //using 20s to keep object
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(20 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        block(nil);
    });
}



@end
