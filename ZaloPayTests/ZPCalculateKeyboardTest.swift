//
//  ZPCTests.swift
//  ZaloPay
//
//  Created by Phuoc Huynh on 8/8/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

import XCTest

class ZPCalculateKeyboardTest: XCTestCase {
    
    func testCalculorInputView(){
        let inputTextView = ZPCalFloatTextInputView()
        let numperPad = ZPCalculateNumberPad()
        
        inputTextView.textField.text = "1.000 + 20.000.000"
        let outputCase1 = 20001000
        let output1 = numperPad.calc(inputTextView)

        XCTAssert(outputCase1 == output1)
        
        inputTextView.textField.text = "1.000 + 20.000.000 ÷ 0"
        let outputCase2 = Int.min
        let output2 = numperPad.calc(inputTextView)
        XCTAssert(outputCase2 == output2)
        
        inputTextView.textField.text = "1.000 + 20.000.000 × "
        let outputCase3 = Int.min
        let output3 = numperPad.calc(inputTextView)
        XCTAssert(outputCase3 == output3)
        
        inputTextView.textField.text = "1.000 + 20.000.000 - 1.000"
        let outputCase4 = 20000000
        let output4 = numperPad.calc(inputTextView)
        XCTAssert(outputCase4 == output4)
        
        inputTextView.textField.text = "20.000.000 - 10.000 × 100"
        let outputCase5 = 19000000
        let output5 = numperPad.calc(inputTextView)
        XCTAssert(outputCase5 == output5)
    }
    
}
