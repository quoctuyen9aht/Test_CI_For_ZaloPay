//
//  ZPTestCharacterName.swift
//  ZaloPayTests
//
//  Created by Dung Vu on 1/23/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import XCTest

class ZPTestCharacterName: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    func testNormal() {
        let example = "A B Cs Da"
        let result = example.getFirstCharacters(numWords: 3)
        XCTAssert(result == "ABC", "get wrong result")
    }
    
    func testOverLenght() {
        let example = "we go to school"
        let result = example.getFirstCharacters(numWords: 100)
        XCTAssert(result == "wgts", "get wrong result")
    }
    
    func testNegative() {
        let example = "one two three"
        let result = example.getFirstCharacters(numWords: -5)
        XCTAssert(result == "", "get wrong result")
    }
    
    func testUnicode() {
        let example = "i am so ☹test"
        let result = example.getFirstCharacters(numWords: 4)
        XCTAssert(result == "ias☹", "get wrong result")
    }
    
    func testPerformanceFindString() {
        // This is an example of a performance test case.
        let example = "ABCDEFGHABCDEFGHABCDEFGHABC DEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGH ABCDEFGHABCDEFGHABCDEFGHABCDEFGHAB CDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHA BCDEFGHABCDEFGHABCDEF GHABCDEFGHABCDEFGHABC DEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGH ABCDEFGHABCDEFGHABCDEFGHAB CDEFGH"
        self.measure {
            // Put the code you want to measure the time of here.
            _ = example.getFirstCharacters(numWords: 3)
        }
    }
}
