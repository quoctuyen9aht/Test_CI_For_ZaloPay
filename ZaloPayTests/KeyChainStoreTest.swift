//
//  KeyChainStoreTest.swift
//  ZaloPayTests
//
//  Created by Dung Vu on 11/15/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import XCTest

class KeyChainStoreTest: XCTestCase {
    let s = "Test.KeyChainStore"
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        KeyChainStore.removeAllItems(service: s)
    }
    
    
    // MARK : Test String
    struct TestString {
        static let test = "ABCDEFGH"
        static let key = "TestString"
    }
    func testSetString() {
        
        // Set
        let result = KeyChainStore.set(string: TestString.test, forKey: TestString.key, service: s)
        XCTAssert(result, "Can't set string, please check")
    }
    
    func testGetSetString() {
        testSetString()
        let result = KeyChainStore.string(forKey: TestString.key, service: s)
        XCTAssert(result != nil , "Wrong set!!")
        XCTAssert(result == TestString.test, "Wrong Value!!")
    }
    
    func testUpdateValue() {
        let newValue = "hello world"
        testSetString()
        // update
        let result = KeyChainStore.set(string: newValue, forKey: TestString.key, service: s)
        XCTAssert(result, "Can't Update")
        let value = KeyChainStore.string(forKey: TestString.key, service: s)
        XCTAssert(value != nil , "Wrong set!!")
        XCTAssert(value == newValue, "Wrong Value!!")
    }
    
    struct TestString2 {
        let test: String
        let key: String
    }
    
    // Remove all items
    func testRemoveAll() {
        // check value remain
        let allItems = KeyChainStore.items(from: s)
        let total = allItems?.count ?? 0
        guard total > 0 else {
            return
        }
        let result = KeyChainStore.removeAllItems(service: s)
        XCTAssert(result , "Can't remove")
        // Check again
        let allItemsAfter = KeyChainStore.items(from: s) ?? []
        XCTAssert(allItemsAfter.count == 0 , "Wrong Remove")
    }
    
    func testSetMoreString() {
        // Remove First
        testRemoveAll()
        let v1 = TestString2(test: "hello 123", key: "v1")
        let v2 = TestString2(test: "hello 1234", key: "v2")
        let v3 = TestString2(test: "hello 12345", key: "v3")
        let v4 = TestString2(test: "hello 123456", key: "v4")
        
        self.measure {
            [v1,v2,v3,v4].forEach({
                KeyChainStore.set(string: $0.test, forKey: $0.key, service: s)
            })
        }
        // Get all
        let result = KeyChainStore.items(from: s)
        XCTAssert(result != nil , "Please check because it doesn't set anything")
        XCTAssert(result?.count == 4, "Miss some value")
    }
    
    func testSetAndRemoveItem() {
        testSetString()
        let result = KeyChainStore.remove(forKey: TestString.key, service: s)
        XCTAssert(result, "Can't Remove")
        // Get agaib
        let value = KeyChainStore.string(forKey: TestString.key, service: s)
        XCTAssert(value == nil, "Wrong Remove")
    }
    
    func testPerformanceSetString() {
        // This is an example of a performance test case.
        let example = "ABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGHABCDEFGH"
        self.measure {
            // Put the code you want to measure the time of here.
            KeyChainStore.set(string: example, forKey: "example", service: s)
        }
    }
    
    // MARK: - Custom Object
    struct CustomObject: Codable, CustomStringConvertible {
        let str1: String
        let number: Int
        
        var description: String {
            return "\(str1) \(number)"
        }
    }
    
    func testGetSetCustomObject() {
        let object = CustomObject(str1: "Custom Object", number: 5)
        let keyCustomObject = "keyCustomObject"
        // Set
        let resultSet = KeyChainStore.set(object: object, forKey: keyCustomObject)
        XCTAssert(resultSet, "Can't set")
        let getValue = KeyChainStore.get(withType: CustomObject.self, forKey: keyCustomObject)
        XCTAssert(getValue?.description == object.description, "Decode wrong value")
    }
    
}
