//
//  ZPApplicationSettingTest.swift
//  ZaloPayTests
//
//  Created by Bon Bon on 2/27/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import XCTest
import ZaloPayConfig
import ZaloPayCommonSwift
import RxSwift

// Use for test
extension ApplicationState {
    func getConfig(_ key: String) -> [String: Any] {
        return (self.zalopayConfig() ?? [:]).value(forKey: key, defaultValue: [String: Any]())
    }
    
    func loadConfig(){
        let manager = ReactNativeAppManager.sharedInstance()
        let configWithdrawApp  = manager.getApp(ZPConstants.withdrawAppId)
        let intenalPath = manager.getRunFolder(configWithdrawApp) ?? ""
        
        _ = ApplicationState.sharedInstance.loadConfig(usingLocal:usingLocalConfig.boolValue,path: intenalPath)
    }
}

class ZPApplicationConfigTest: XCTestCase{

    override func setUp() {
        super.setUp()
        ApplicationState.sharedInstance.loadConfig()
    }
    
    func testGerneralConfig() {
        let rawConfig = ApplicationState.sharedInstance.getConfig("general")
        guard let config = ZPApplicationConfig.getGeneralConfig() else {
            XCTAssert(rawConfig.count == 0)
            return
        }
        
        XCTAssert(config.getMaxCCLinks() == rawConfig.int(forKey: "max_cc_links"))
        
        guard let phoneFormat = config.getPhoneFormat(),
            let rawPhoneConfig = rawConfig["phone_format"] as? JSON else {
            return
        }
        
        let patterns = phoneFormat.getPatterns()
        let rawPatterns = rawPhoneConfig.value(forKey: "patterns", defaultValue: [String]())
        XCTAssert(rawPatterns == patterns)
        XCTAssert(phoneFormat.getMinLength() == rawPhoneConfig.int(forKey: "min_length"))
        XCTAssert(phoneFormat.getMaxLength() == rawPhoneConfig.int(forKey: "max_length"))
        XCTAssert(phoneFormat.getInvalidLengthMessage() == rawPhoneConfig.string(forKey: "invalid_length_message"))
        XCTAssert(phoneFormat.getGeneralMessage() == rawPhoneConfig.string(forKey: "general_message"))
    }
    
    func testZPCAppConfig() {
        let rawConfig = ApplicationState.sharedInstance.getConfig("zpc")
        guard let config = ZPApplicationConfig.getZPCAppConfig() else {
            XCTAssert(rawConfig.count == 0)
            return
        }
        
        XCTAssert(config.getEnableMergeContactName() == rawConfig.int(forKey: "enable_merge_contact_name"))
        XCTAssert(config.getFriendFavorite() == rawConfig.int(forKey: "friend_favorite"))
    }
    
    func testSearchConfig() {
        let rawConfig = ApplicationState.sharedInstance.getConfig("search")
        guard let config = ZPApplicationConfig.getSearchConfig() else {
            XCTAssert(rawConfig.count == 0)
            return
        }
        
        let insideApps : [ZPInternalAppSearchConfigProtocol] = config.getInsideApp()
        let rawInsideApps = rawConfig.value(forKey: "inside_app", defaultValue: [JSON]())
        XCTAssert(insideApps.count == rawInsideApps.count)
        for idx in 0..<insideApps.count {
            let app = insideApps[idx]
            let rawApp = rawInsideApps[idx]
            XCTAssert(app.getInsideAppId() == rawApp.int(forKey: "inside_app_id"))
            XCTAssert(app.getAppType() == rawApp.int(forKey: "app_type"))
            XCTAssert(app.getAppName() == rawApp.string(forKey: "app_name"))
            XCTAssert(app.getIconName() == rawApp.string(forKey: "icon_name"))
            XCTAssert(app.getIconColor() == rawApp.string(forKey: "icon_color"))
        }
        
        let ignoreApps = config.getIgnoreApps()
        let rawIgnoreApps = rawConfig.value(forKey: "ignore_apps", defaultValue: [Int]())
        XCTAssert(ignoreApps == rawIgnoreApps)
        XCTAssert(config.getNumberSearchApp() == rawConfig.int(forKey: "number_search_app"))
    }
    
    func testApiConfig() {
        let rawConfig = ApplicationState.sharedInstance.getConfig("api")
        guard let config = ZPApplicationConfig.getApiConfig() else {
            XCTAssert(rawConfig.count == 0)
            return
        }
        
        let apiNames = config.getApiNames()
        let rawApiNames = rawConfig.value(forKey: "api_names", defaultValue: [String]())
        XCTAssert(apiNames == rawApiNames)
        XCTAssert(config.getApiRoute() == rawConfig.string(forKey: "api_route"))
        
        let rawApiAsync = rawConfig.value(forKey: "call_api_async", defaultValue: JSON())
        guard let apiAsync = config.getCallApiAsync() else {
            XCTAssert(rawApiAsync.count == 0)
            return
        }
        XCTAssert(apiAsync.getCanGetListUserByPhone() == rawApiAsync.bool(forKey: "get_list_user_by_phone"))
    }
    
    func testWithdrawConfig() {
        let rawConfig = ApplicationState.sharedInstance.getConfig("withdraw")
        guard let config = ZPApplicationConfig.getWithdrawConfig() else {
            XCTAssert(rawConfig.count == 0)
            return
        }
        
        let withdrawMoney = config.getWithdrawMoney()
        let rawWithdrawMoney = rawConfig.value(forKey: "withdraw_money", defaultValue: [Int]())
        XCTAssert(withdrawMoney == rawWithdrawMoney)
        XCTAssert(config.getMinWithdrawMoney() == rawConfig.int(forKey: "min_withdraw_money"))
        XCTAssert(config.getMultipleWithdrawMoney() == rawConfig.int(forKey: "multiple_withdraw_money"))
    }
    
    func testWebAppConfig() {
        let rawConfig = ApplicationState.sharedInstance.getConfig("webapp")
        guard let config = ZPApplicationConfig.getWebAppConfig() else {
            XCTAssert(rawConfig.count == 0)
            return
        }
        
        let allowUrls = config.getAllowUrls()
        let rawAllowUrls = rawConfig.value(forKey: "allow_urls", defaultValue: [String]())
        XCTAssert(allowUrls == rawAllowUrls)
    }
    
    func testTabMeConfig() {
        let rawConfig = ApplicationState.sharedInstance.getConfig("tab_me")
        guard let config = ZPApplicationConfig.getTabMeConfig() else {
            XCTAssert(rawConfig.count == 0)
            return
        }
        
        XCTAssert(config.getQuickCommentUrl() == rawConfig.string(forKey: "quick_comment_url"))
        XCTAssert(config.getEnableRegisterProfileZalopayId() == rawConfig.int(forKey: "enable_register_profile_zalopayid"))
    }
    
    func testNotificationConfig() {
        let rawConfig = ApplicationState.sharedInstance.getConfig("notification")
        guard let config = ZPApplicationConfig.getNotificationConfig() else {
            XCTAssert(rawConfig.count == 0)
            return
        }
        
        let vibrate = config.getVibrate()
        let rawVibrate = rawConfig.value(forKey: "vibrate", defaultValue: [Int]())
        XCTAssert(vibrate == rawVibrate)
    }
    
    func testTabHomeConfig() {
        let rawConfig = ApplicationState.sharedInstance.getConfig("tab_home")
        guard let config = ZPApplicationConfig.getTabHomeConfig() else {
            XCTAssert(rawConfig.count == 0)
            return
        }
        
        let internalApps = config.getInternalApps()
        let rawInternalApps = rawConfig.value(forKey: "internal_apps", defaultValue: [JSON]())
        XCTAssert(internalApps.count == rawInternalApps.count)
        for idx in 0..<internalApps.count {
            let app = internalApps[idx]
            let rawApp = rawInternalApps[idx]
            XCTAssert(app.getAppId() == rawApp.int(forKey: "appId"))
            XCTAssert(app.getOrder() == rawApp.int(forKey: "order"))
            XCTAssert(app.getDisplayName() == rawApp.string(forKey: "display_name"))
            XCTAssert(app.getIconName() == rawApp.string(forKey: "icon_name"))
            XCTAssert(app.getIconColor() == rawApp.string(forKey: "icon_color"))
        }
    }
    
    func testPromotionConfig() {
        let rawConfig = ApplicationState.sharedInstance.getConfig("promotion")
        guard let config = ZPApplicationConfig.getPromotionConfig() else {
            XCTAssert(rawConfig.count == 0)
            return
        }
        
        let rawVoucher = rawConfig.value(forKey: "voucher", defaultValue: JSON())
        guard let voucher = config.getVoucher() else {
            XCTAssert(rawVoucher.count == 0)
            return
        }
        XCTAssert(voucher.getAllowPaymentUseVoucher() == rawVoucher.int(forKey: "allow_payment_use_voucher"))
    }
    
    func testBankIntegrationConfig() {
        let rawConfig = ApplicationState.sharedInstance.getConfig("bankIntegration")
        guard let config = ZPApplicationConfig.getBankIntegrationConfig() else {
            XCTAssert(rawConfig.count == 0)
            return
        }
        
        let enableOB = config.getEnableOB()
        let rawEnableOB = rawConfig.value(forKey: "enableOB", defaultValue: [JSON]())
        XCTAssert(enableOB.count == rawEnableOB.count)
        for idx in 0..<enableOB.count {
            let item = enableOB[idx]
            let rawItem = rawEnableOB[idx]
            XCTAssert((item.getBankCode()) == rawItem.string(forKey: "bankCode"))
            XCTAssert((item.getBankName()) == rawItem.string(forKey: "bankName"))
            XCTAssert((item.getNumberLastDigit()) == rawItem.int(forKey: "numberLastDigit", defaultValue: 1))
            XCTAssert((item.getPhone()) == rawItem.string(forKey: "phone"))
            XCTAssert((item.getSMS()) == rawItem.string(forKey: "sms"))
            XCTAssert((item.getPrice()) == rawItem.string(forKey: "price"))
            XCTAssert((item.getSupportUrl()) == rawItem.string(forKey: "supportUrl"))
            XCTAssert((item.getRegisterUrl()) == rawItem.string(forKey: "registerUrl"))
            XCTAssert((item.getMaxRetry()) == rawItem.int(forKey: "max_retry"))
            XCTAssert((item.getPhoneSupport()) == rawItem.string(forKey: "phoneSupport"))
            XCTAssert((item.getUsingSMS()) == rawItem.int(forKey: "usingSMS"))
        }
    }
    
    func testUpdateProfileLevelConfig() {
        let rawConfig = ApplicationState.sharedInstance.getConfig("updateprofilelevel")
        guard let config = ZPApplicationConfig.getUpdateProfileLevelConfig() else {
            XCTAssert(rawConfig.count == 0)
            return
        }
        
        XCTAssert(config.getImagesize() == rawConfig.int(forKey: "imagesize"))
    }
    
    func testDisclaimerConfig() {
        let rawConfig = ApplicationState.sharedInstance.getConfig("disclaimer")
        guard let config = ZPApplicationConfig.getDisclaimerConfig() else {
            XCTAssert(rawConfig.count == 0)
            return
        }
        
        let listIdInternal = config.getListInternalId()
        let rawListIdInternal = rawConfig.value(forKey: "list_id_internal", defaultValue: [Int]())
        XCTAssert(listIdInternal == rawListIdInternal)
        XCTAssert(config.getContent() == rawConfig.string(forKey: "content"))
        
        let externalApps = config.getExternalApps()
        let rawExternalApps = rawConfig.value(forKey: "external_apps", defaultValue: [JSON]())
        XCTAssert(externalApps.count == rawExternalApps.count)
        for idx in 0..<externalApps.count {
            let app = externalApps[idx]
            let rawApp = rawExternalApps[idx]
            XCTAssert(app.getAppId() == rawApp.int(forKey: "app_id"))
            XCTAssert(app.getMerchantUrl() == rawApp.string(forKey: "merchant_url"))
            XCTAssert(app.getMerchantName() == rawApp.string(forKey: "merchant_name"))
            XCTAssert((app.getUserInfo()) == rawApp.value(forKey: "user_info", defaultValue: [String]()))
        }
    }
    
    func testProfileConfig() {
        let rawConfig = ApplicationState.sharedInstance.getConfig("profile")
        guard let config = ZPApplicationConfig.getProfileConfig() else {
            XCTAssert(rawConfig.count == 0)
            return
        }
        
        XCTAssert(config.getManageLoginDevices() == rawConfig.bool(forKey: "manageLoginDevices"))
    }
    
    func testInviteZPConfig() {
        let rawConfig = ApplicationState.sharedInstance.getConfig("inviteZP")
        guard let config = ZPApplicationConfig.getInviteZPConfig() else {
            XCTAssert(rawConfig.count == 0)
            return
        }
        
        XCTAssert(config.getEnable() == rawConfig.int(forKey: "enable"))
        XCTAssert(config.getDeeplinkShare() == rawConfig.string(forKey: "deeplink_share"))
    }
    
    func testQuickPayConfig() {
        let rawConfig = ApplicationState.sharedInstance.getConfig("quickpay")
        guard let config = ZPApplicationConfig.getQuickPayConfig() else {
            XCTAssert(rawConfig.count == 0)
            return
        }
        
        XCTAssert(config.getEnable() == rawConfig.int(forKey: "enable"))
        XCTAssert(config.getUserManualUrl() == rawConfig.string(forKey: "user_manual_url"))
        XCTAssert(config.getTimeRefresh() == rawConfig.int(forKey: "time_refresh"))
        XCTAssert(config.getMaxPaymentCode() == rawConfig.int(forKey: "max_payment_code"))
        XCTAssert(config.getPaymentLimit() == rawConfig.int(forKey: "payment_limit"))
        XCTAssert(config.getTransactionStatusTimeout() == rawConfig.int(forKey: "transaction_status_timeout"))
        
        let paymentMethod = config.getPaymentMethod()
        let rawPaymentMethod = rawConfig.value(forKey: "payment_method", defaultValue: [JSON]())
        XCTAssert(paymentMethod.count == rawPaymentMethod.count)
        for idx in 0..<paymentMethod.count {
            let method = paymentMethod[idx]
            let rawMethod = rawPaymentMethod[idx]
            
            XCTAssert(method.getPmcid() == rawMethod.int(forKey: "pmcid"))
            XCTAssert(method.getName() == rawMethod.string(forKey: "name"))
            XCTAssert(method.getLogo() == rawMethod.string(forKey: "logo"))
        }
    }
    
    func testKeypadConfig() {
        let rawConfig = ApplicationState.sharedInstance.getConfig("configKeyPad")
        guard let config = ZPApplicationConfig.getKeypadConfig() else {
            XCTAssert(rawConfig.count == 0)
            return
        }
    
        XCTAssert(config.getTransfer() == rawConfig.int(forKey: "transfer"))
        XCTAssert(config.getReceive() == rawConfig.int(forKey: "receive"))
        XCTAssert(config.getRecharge() == rawConfig.int(forKey: "recharge"))
    }
    
    func testTransferConfig() {
        let rawConfig = ApplicationState.sharedInstance.getConfig("transfer")
        guard let config = ZPApplicationConfig.getTransferConfig() else {
            XCTAssert(rawConfig.count == 0)
            return
        }
        
        XCTAssert(config.getMaxLengthMessage() == rawConfig.int(forKey: "max_length_message"))
    }
    
    func testZaloConfig() {
        let rawConfig = ApplicationState.sharedInstance.getConfig("zalo")
        guard let config = ZPApplicationConfig.getZaloConfig() else {
            XCTAssert(rawConfig.count == 0)
            return
        }
        
        XCTAssert(config.getTransferMode() == rawConfig.string(forKey: "transfer_mode"))
    }
    
    func testServerLogConfig() {
        let rawConfig = ApplicationState.sharedInstance.getConfig("serverLog")
        guard let config = ZPApplicationConfig.getServerLogConfig() else {
            XCTAssert(rawConfig.count == 0)
            return
        }
        
        XCTAssert(config.getTimeSendLog() == rawConfig.double(forKey: "timeSendLog"))
    }
    
    func testSDKConfig() {
        let rawConfig = ApplicationState.sharedInstance.getConfig("sdk")
        guard let config = ZPApplicationConfig.getSDKConfig() else {
            XCTAssert(rawConfig.count == 0)
            return
        }
        
        let popupModeAppList = config.getPopupModeAppList()
        let rawPopupModeAppList = rawConfig.value(forKey: "popup_mode_app_list", defaultValue: [Int]())
        XCTAssert(popupModeAppList == rawPopupModeAppList)
        XCTAssert(config.getSuccessScreenDelay() == rawConfig.int(forKey: "success_screen_delay"))
    }
    
    func testSharingConfig() {
        let rawConfig = ApplicationState.sharedInstance.getConfig("sharing")
        guard let config = ZPApplicationConfig.getSharingConfig() else {
            XCTAssert(rawConfig.count == 0)
            return
        }
        
        XCTAssert(config.getShareViaSocial() == rawConfig.string(forKey: "shareViaSocial"))
        XCTAssert(config.getFacebookFanpage() == rawConfig.string(forKey: "facebook_fanpage"))
    }
    
}
