//
//  ZPReactNativeAppRouter.swift
//  ZaloPayTests
//
//  Created by CPU11680 on 3/2/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

import XCTest

class ZPReactNativeAppRouterTest: XCTestCase {
    
    lazy var reactAppRouter: ZPReactNativeAppRouter = ZPReactNativeAppRouter()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testOpenApp(){
        let controller = UIApplication.shared.keyWindow?.rootViewController
        let appId = 1 //
        let app = ReactNativeAppManager.sharedInstance().getApp(appId)
        let moduleName: String = "PaymentMain" //ReactNativeAppManager.sharedInstance().defaultModuleName(appId)
        let properties = ReactNativeAppManager.sharedInstance().reactProperties(Int(appId))
        let confirmOpenApp = self.reactAppRouter.openApp(app, from: controller!, moduleName: moduleName, withProps: properties!)
        XCTAssertNotNil(confirmOpenApp)
    }
    
    func testTransactionHistory(){
        
        let transId: UInt64 = 0
        let zaloPayId: String = "180104000002002" // NetworkManager.sharedInstance().paymentUserId
        
        let properties = NSMutableDictionary()
        properties["zalopay_userid"] = zaloPayId
        properties["view"] = "history"
        properties["transid"] = transId
        
        XCTAssertNotNil(self.reactAppRouter.internalModuleView(ReactModule_TransactionLogs, properties: properties))
        
    }
    
    func testClearingAccount(){
        let properties = NSMutableDictionary()
        properties["remain_time"] =  1 //ZPProfileManager.shareInstance().userLoginData?.hourSessionTime
        XCTAssertNotNil(self.reactAppRouter.internalModuleView("AccountClearing", properties: properties))
    }
    
    func testShowNotification(){
        XCTAssertNotNil(self.reactAppRouter.notificationViewController())
    }
    
//    func testCanPresentLixiPopup(){
//        let controller = ZPAppFactory.sharedInstance().rootNavigation().topViewController
//        let test = self.reactAppRouter.canPresentLixiPopup(controller)
//        XCTAssertFalse(test)
//    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    
    
}
