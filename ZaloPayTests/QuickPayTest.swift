//
//  QuickPayTest.swift
//  ZaloPayTests
//
//  Created by vuongvv on 12/12/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

import XCTest
import ZaloPayConfig

class QuickPayTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        ApplicationState.sharedInstance.loadConfig()
        ZPSettingsConfig.loadUserSetting("123")
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    //Test : add,delete
    func testDBCache(){
        
        //Remove all
        ZPPaymentCodeModel.removeAllPaymentCodes()
        guard var arrCache = ZPPaymentCodeModel.getPaymentCodes() as? [ZPPaymentCodeModel] else {
            XCTFail("No Array Cache")
            return
        }
        
        XCTAssert(arrCache.count == 0)
        
        //Add with max to 5 (from config)
        var arrCodes = [ZPPaymentCodeModel]()
        arrCodes.append(ZPPaymentCodeModel(259444734600000001, timetolive: 360000, expire: 1513048181))
        arrCodes.append(ZPPaymentCodeModel(259444734600000002, timetolive: 360000, expire: 1513048181))
        arrCodes.append(ZPPaymentCodeModel(259444734600000003, timetolive: 360000, expire: 1513048181))
        arrCodes.append(ZPPaymentCodeModel(259444734600000004, timetolive: 360000, expire: 1513048181))
        arrCodes.append(ZPPaymentCodeModel(259444734600000005, timetolive: 360000, expire: 1513048181))
        arrCodes.append(ZPPaymentCodeModel(259444734600000006, timetolive: 360000, expire: 1513048181))
        arrCodes.append(ZPPaymentCodeModel(259444734600000007, timetolive: 360000, expire: 1513048181))
        ZPPaymentCodeModel.addPaymentCodes(arrCodes)
        
        arrCache = ZPPaymentCodeModel.getPaymentCodes() as? [ZPPaymentCodeModel] ?? []
        XCTAssert(arrCache.count == 5)
        
        //Remove
        ZPPaymentCodeModel.removePaymentCode(arrCodes.last)
        arrCodes.removeLast()
        ZPPaymentCodeModel.removePaymentCode(arrCodes.last)
        
        arrCache = ZPPaymentCodeModel.getPaymentCodes() as? [ZPPaymentCodeModel] ?? []
        XCTAssert(arrCache.count == 3)

    }
    func testCheckRetainCodeForGetNewCode(){
        ZPPaymentCodeModel.removeAllPaymentCodes()
        
        let expired = UInt64(NSDate().timeIntervalSince1970) - 10
        let notexpired = UInt64(NSDate().timeIntervalSince1970) + 100
        
        var arrCodes = [ZPPaymentCodeModel]()
        arrCodes.append(ZPPaymentCodeModel(259444734600000001, timetolive: 360000, expire: expired))
        arrCodes.append(ZPPaymentCodeModel(259444734600000002, timetolive: 360000, expire: notexpired))
        arrCodes.append(ZPPaymentCodeModel(259444734600000003, timetolive: 360000, expire: expired))
        arrCodes.append(ZPPaymentCodeModel(259444734600000004, timetolive: 360000, expire: expired))
        arrCodes.append(ZPPaymentCodeModel(259444734600000005, timetolive: 360000, expire: notexpired))
        ZPPaymentCodeModel.addPaymentCodes(arrCodes)
        
        let paymentCodeInteractor = ZPPaymentCodeInteractor()
        paymentCodeInteractor.arrPaymentCode = ZPPaymentCodeModel.getPaymentCodes() as! [ZPPaymentCodeModel]
        let (arrRetain,_) = paymentCodeInteractor.getInfoPaymentCanUse()
        
        XCTAssert(arrRetain.count == 2)
    }
}
