//
//  NSMutableDictionary+JSON.h
//  
//
//  Created by Nguyễn Hữu Hoà on 7/10/13.
//  Copyright (c) 2013 VNG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (JSON)
- (int)intForKey:(id)key defaultValue:(int)defaultValue;
- (int)intForKey:(id)key;

- (uint32_t)uint32ForKey:(id)key defaultValue:(uint32_t)defaultValue;
- (uint32_t)uint32ForKey:(id)key;

- (uint64_t)uint64ForKey:(id)key defaultValue:(uint64_t)defaultValue;
- (uint64_t)uint64ForKey:(id)key;

- (NSArray *)arrayForKey:(id)key defaultValue:(NSArray *)defaultValue;
- (NSArray *)arrayForKey:(id)key;

- (NSDictionary *)dictionaryForKey:(id)key defaultValue:(NSDictionary *)defaultValue;
- (NSDictionary *)dictionaryForKey:(id)key;

- (NSString *)stringForKey:(id)key defaultValue:(NSString *)defaultValue;
- (NSString *)stringForKey:(id)key;

- (NSDate *)dateForKey:(id)key defaultValue:(NSDate *)defaultValue;
- (NSDate *)dateForKey:(id)key;

- (NSNumber *)numericForKey:(id)key defaultValue:(NSNumber *)defaultValue;
- (NSNumber *)numericForKey:(id)key;

- (BOOL)boolForKey:(id)key defaultValue:(BOOL)defaultValue;
- (BOOL)boolForKey:(id)key;

- (double)doubleForKey:(id)key defaultValue:(double)defaultValue;
- (double)doubleForKey:(id)key;

// deprecated

// Method is deprecated. Use intForKey:: method instead.
- (int)intValueForKey:(id)key defaultValue:(int)defaultValue __attribute((deprecated("use -intForKey:defaultValue:] instead")));
- (uint32_t)uint32ValueForKey:(id)key defaultValue:(uint32_t)defaultValue __attribute((deprecated("use -uint32ForKey:defaultValue:] instead")));
- (uint64_t)uint64ValueForKey:(id)key defaultValue:(uint64_t)defaultValue __attribute((deprecated("use -uint64ForKey:defaultValue:] instead")));
- (NSArray *)arrayValueForKey:(id)key defaultValue:(NSArray *)defaultValue __attribute((deprecated("use -arrayForKey:defaultValue:] instead")));
- (NSDictionary *)dictionaryValueForKey:(id)key defaultValue:(NSDictionary *)defaultValue __attribute((deprecated("use -dictionaryForKey:defaultValue:] instead")));
- (NSString *)stringValueForKey:(id)key defaultValue:(NSString *)defaultValue __attribute((deprecated("use -stringForKey:defaultValue:] instead")));
- (NSDate *)dateValueForKey:(id)key defaultValue:(NSDate *)defaultValue __attribute((deprecated("use -dateForKey:defaultValue:] instead")));
- (NSNumber *)numericValueForKey:(id)key defaultValue:(NSNumber *)defaultValue __attribute((deprecated("use -numericForKey:defaultValue:] instead")));
- (BOOL)boolValueForKey:(id)key defaultValue:(BOOL)defaultValue __attribute((deprecated("use -boolForKey:defaultValue:] instead")));
- (double)doubleValueForKey:(id)key defaultValue:(double)defaultValue __attribute((deprecated("use -doubleForKey:defaultValue:] instead")));

- (int)intValueForKey:(id)key __attribute((deprecated("use -intForKey:] instead")));
- (uint32_t)uint32ValueForKey:(id)key __attribute((deprecated("use -uint32ForKey:] instead")));
- (uint64_t)uint64ValueForKey:(id)key __attribute((deprecated("use -uint64ForKey:] instead")));
- (NSArray *)arrayValueForKey:(id)key __attribute((deprecated("use -arrayForKey:] instead")));
- (NSDictionary *)dictionaryValueForKey:(id)key __attribute((deprecated("use -dictionaryForKey:] instead")));
- (NSString *)stringValueForKey:(id)key __attribute((deprecated("use -stringForKey:] instead")));
- (NSDate *)dateValueForKey:(id)key __attribute((deprecated("use -dateForKey:] instead")));
- (NSNumber *)numericValueForKey:(id)key __attribute((deprecated("use -numericForKey:] instead")));
- (BOOL)boolValueForKey:(id)key __attribute((deprecated("use -boolForKey:] instead")));
- (double)doubleValueForKey:(id)key __attribute((deprecated("use -doubleForKey:] instead")));
@end

@interface NSMutableDictionary (NewSetObjectForKey)
- (void)setObjectCheckNil:(id)anObject forKey:(id <NSCopying>)aKey;
@end

@interface NSMutableArray (NewAddObject)
- (void)addObjectNotNil:(id)anObject;
- (void)insertObjectNotNil:(id)obj atIndex:(int)index;
- (void)addObjectsFromArrayNotNil:(NSArray *)otherArray;
- (void)addObjectNotExist:(id)anObject;
@end

@interface NSMutableSet (NewAddObject)
- (void)addObjectNotNil:(id)anObject;
@end

@interface NSArray (FeedJSON)
- (NSArray *)toJSON;
@end

@interface NSString (Hyperlink)
- (NSString *)autoEnableHyperlink;

- (NSString *)autoEnableHyperlink2;
@end

@interface NSString (JSON)
- (id)JSONValue;
- (NSString *)encodeBase64UrlSafe;
- (NSString *)decodeBase64UrlSafe;
@end

@interface NSObject (JSON)
- (NSString *)JSONRepresentation;
- (NSString *)JSONRepresentation:(BOOL)beautifulFormat;
@end

@interface NSData (JSON)
- (id)JSONValue;
@end

