//
//  Collection+P2PNotification.h
//  ZaloPayCommon
//
//  Created by phucpv on 7/4/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface NSString (P2PNotification)
+ (NSString *)encodeP2PMessage:(NSString *)string;
+ (NSString *)decodeP2PMessage:(NSString *)string;
@end

@interface NSString(ThankMessage)
+ (NSString *)embededDataThankMessageWithTransId:(NSString *)transId
                                     displayName:(NSString *)displayName
                                         message:(NSString *)message
                                       zalopayId:(NSString *)zalopayId
                                       timestamp:(NSString *)timestamp;
@end

@interface NSString (QRTransfer)
+ (NSString *)embededDataQRTransferWithDisplayName:(NSString *)displayName
                                            avatar:(NSString *)avatar
                                          progress:(int)progress
                                            amount:(int64_t)amount
                                           transId:(NSString *)transId;
@end
