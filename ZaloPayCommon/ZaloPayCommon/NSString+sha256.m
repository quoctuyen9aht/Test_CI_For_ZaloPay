//
//  NSString+sha256.m
//  Passcode
//
//  Created by Matt on 1/12/13.
//  Copyright (c) 2013 Matt Zanchelli. All rights reserved.
//

#import "NSString+sha256.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (sha256)

///	Get an @c NSData byte array directly from sha256.
///	@return An @c NSData byte array.
- (NSData *)sha256Data
{
    const char *str = [self UTF8String];
    unsigned char result[CC_SHA256_DIGEST_LENGTH];
    CC_SHA256(str, (CC_LONG) strlen(str), result);
	return [NSData dataWithBytes:(const void *)result
						  length:sizeof(unsigned char)*CC_SHA256_DIGEST_LENGTH];
}

///	An @c NSString with hex data from sha256.
///	@return An @c NSString with hex data from sha256.
- (NSString *)sha256
{
    const char *str = [self UTF8String];
    unsigned char result[CC_SHA256_DIGEST_LENGTH];
    CC_SHA256(str, (CC_LONG) strlen(str), result);
	
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH*2];
    for ( int i=0; i<CC_SHA256_DIGEST_LENGTH; i++ ) {
        [ret appendFormat:@"%02x",result[i]];
    }
	
	return ret;
}

@end
