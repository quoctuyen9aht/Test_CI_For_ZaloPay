//
//  ZaloPayCommon.h
//  ZaloPayCommon
//
//  Created by Phuoc Huynh on 3/20/18.
//  Copyright © 2018 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ZaloPayCommon.
FOUNDATION_EXPORT double ZaloPayCommonVersionNumber;

//! Project version string for ZaloPayCommon.
FOUNDATION_EXPORT const unsigned char ZaloPayCommonVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ZaloPayCommon/PublicHeader.h>

#import <ZaloPayCommon/CodableObject.h>
