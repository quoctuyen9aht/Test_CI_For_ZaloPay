//
//  ZPCache.h
//  ZaloPayCommon
//
//  Created by bonnpv on 1/14/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZPCache : NSObject
+ (id)objectForKey:(NSString *)key;
+ (void)setObject:(id <NSCoding>)object forKey:(NSString *)key;
+ (void)removeObjectForKey:(NSString *)key;
@end
