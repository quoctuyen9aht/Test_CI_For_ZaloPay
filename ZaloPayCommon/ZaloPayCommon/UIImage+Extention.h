//
//  UIImage+Extention.h
//  MarQet
//
//  Created by Bon on 2/13/15.
//  Copyright (c) 2015 Bon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry/NSArray+MASAdditions.h>

@interface UIImage (Extention)
+ (UIImage *)defaultAvatar;
+ (UIImage *)imageFromColor:(UIColor *)color;
- (UIImage*)scaledToSize:(CGSize)newSize;
- (UIImage*)resizeImageWithScale:(CGFloat)scale;
- (void)saveImageToDoucument:(NSString *)name;
+ (UIImage *)filledImageFrom:(UIImage *)source withColor:(UIColor *)color;
+ (UIImage*) imageWithImage:(UIImage*) source
                      color:(UIColor *)color
                      alpha:(CGFloat) alpha;
+ (UIImage *)imageWithGradient:(UIImage *)img
                    startColor:(UIColor *)color1
                      endColor:(UIColor *)color2
                          axis:(MASAxisType)axis;
- (UIImage *)scaledAspectToSize:(CGSize)newSize;
+ (UIImage *)createQRImageWith:(CGSize)outputSize from:(NSData *)qrCodeData;
+ (UIImage *)createBarCodeImageWith:(CGSize)outputSize from:(NSData *)qrCodeData;

@end
