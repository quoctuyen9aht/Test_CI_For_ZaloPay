#import "ZPConstants.h"

const int ZaloPayAppStoreId         = 1112407590;
NSString *const ZaloPayAppStoreUrl  = @"https://itunes.apple.com/us/app/zalo-pay-thanh-toan-trong/id1112407590?mt=8";

NSInteger kZaloPayClientAppId       = 1;
NSInteger reactInternalAppId        = 1;
NSInteger withdrawAppId             = 2;
NSInteger showshowAppId             = 22;
NSInteger lixiAppId                 = 6;
NSInteger topupPhoneAppId           = 11;
NSInteger mobileCardAppId           = 12;
NSInteger topupPhoneV2AppId         = 61;
NSInteger appDichVuId               = 15;
NSInteger appVoucherId              = 5;

const int NUMBER_OF_DIGIT = 6;
const int NUMBER_OF_OTP = 6;
const int TRANSFER_MESSAGE_LENGTH = 50;
const int minAccountNameLength    = 4;
const int maxAccountNameLength    = 24;
const int EMAIL_LENGTH  = 45;
const int IDENTYFIER_NUMBER_LENGTH = 45;
const int FEEDBACK_MESSAGE_LENGTH  = 400;
const float timeoutIntervalForRequest    = 10.0;
const float timeoutIntervalForConnectorRequest = 5.0;
const double delayUpTransacLogServer   = 10.0;
const int WithDrawNumberOther = -1111;
const int WithDrawMinDefault = 20000;
const int WithDrawMultipleDefault = 10000;

const int minRechargeValue                = 20000;
const int maxRechargeValue                = 100000000;
const int defaultMinTransfer              = 10000;
const int defaultMaxTransfer              = 5000000;
const int defaultMaxWithdraw              = 100000000;
const int maxCountNotifications = 99;

const NSString* itunesAppLink = @"itms-apps://itunes.apple.com/app/";

#pragma mark - Key

NSString *const ZPConfigCacheKey = @"ZPConfigCacheKey";
NSString *const ZPBannerKey      = @"ZPBannerKey";
NSString *const ZPBankSupportKey = @"ZPBankSupportKey";

NSString *const ZPProfileLevel3_Email                   = @"ZPProfileLevel3_Email";
NSString *const ZPProfileLevel3_Identifier              = @"ZPProfileLevel3_Identifier";
NSString *const ZPProfileLevel3_FrontIdentifyCard       = @"ZPProfileLevel3_FrontIdentifyCard";
NSString *const ZPProfileLevel3_BackSideIdentifyCard    = @"ZPProfileLevel3_BackSideIdentifyCard";
NSString *const ZPProfileLevel3_Avatar                  = @"ZPProfileLevel3_Avatar";

NSString *const Force_Update_Appversion_Key             = @"Force_Update_Appversion_Key";
NSString *const ZPEnableRechargeKey                     = @"ZPEnableRechargeKey";
NSString *const ZPInApp_Notification_Key                = @"ZPInApp_Notification_Key";
NSString *const Reload_AccountList_Notification         = @"Reload_AccountList_Notification";
NSString *const Reload_CardList_Notification            = @"Reload_CardList_Notification";
NSString *const Reload_Profile_Notification             = @"Reload_Profile_Notification";
NSString *const GMSService_API                          = @"AIzaSyBBynYIi93D36Jbq9LsfLou_enqA3dbRFc";

#pragma mark - Param key
NSString *const ZPCreateWalletOrderTranstypeKey         = @"transtype";
NSString *const ZPCreateWalletOrderExtKey               = @"ext";
NSString *const ZPCreateWalletOrderSenderKey           = @"sender";
NSString *const ZPCreateWalletOrderSenderPhoneNumberKey               = @"phonenumber";
NSString *const Key_Store_Database_Promotion            = @"Key_Store_Database_Promotion";
NSString *const ZPNumberAppSearched                     = @"number_search_app";
NSString *const Quick_Comment_URL                       = @"https://goo.gl/forms/FCO642guFSbpYwlt2";

NSString *const ViewModeKeyboard_ABC                    = @"keyboardABC";
NSString *const ViewModeKeyboard_Phone                  = @"keyboardPhone";

NSString *const ReactModule_TransactionLogs             = @"TransactionLogs";
NSString *const ReactModule_PaymentMain                 = @"PaymentMain";
NSString *const ReactModule_RedPacket                   = @"RedPacket";
NSString *const Merchant_Disclaimer_Key                 = @"Merchant_Disclaimer_Key";

