//
//  FileUtils.h
//  PaymentReactNativeApp
//
//  Created by Nguyễn Hữu Hoà on 5/25/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileUtils : NSObject
+ (void)createFolderAtPath:(NSString *)pathName;
+ (void)removeFolderAtPath:(NSString *)pathName;
+ (void)copyFolderAtPath:(NSString *)srcPath toPath:(NSString *)dstPath overrideOldFile:(BOOL)overrideOldFile;
+ (BOOL)unzipFileAtPath:(NSString *)srcPath
           toFolderPath:(NSString *)dstPath
          removeZipFile:(BOOL)removeZipFile;
@end
