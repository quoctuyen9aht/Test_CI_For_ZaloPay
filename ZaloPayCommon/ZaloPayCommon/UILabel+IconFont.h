//
//  UILabel+IconFont.h
//  ZaloPay
//
//  Created by bonnpv on 12/23/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (IconFont)
+ (void)loadIconNameFromDic:(NSDictionary *)dic;
+ (NSDictionary *)iconInfoWithName:(NSString *)name;
+ (NSString *)iconCodeWithName:(NSString *)iconName;
- (void)setIconfont:(NSString *)iconName;
- (void)setIconfont:(NSString *)iconName defaultIcon:(NSString *)defaultIcon;
@end
