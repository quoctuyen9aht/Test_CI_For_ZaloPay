//
//  UILabel+IconFont.m
//  ZaloPay
//
//  Created by bonnpv on 12/23/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "UILabel+IconFont.h"
#import "NSCollection+JSON.h"

#define kIconCode   @"code"


static NSDictionary *iconMaper;

@implementation UILabel (IconFont)

+ (void)loadIconNameFromDic:(NSDictionary *)dic {
    iconMaper = dic;
}

+ (NSDictionary *)iconInfoWithName:(NSString *)name {
    return [iconMaper dictionaryForKey:name defaultValue:@{}];
}

+ (NSString *)iconCodeWithName:(NSString *)iconName {
    NSDictionary *info = [UILabel iconInfoWithName:iconName];
    return [info stringForKey:kIconCode defaultValue:@""];
}

- (void)setIconfont:(NSString *)iconName {
    [self setIconfont:iconName defaultIcon:@""];
}

- (void)setIconfont:(NSString *)iconName defaultIcon:(NSString *)defaultIcon {
    NSDictionary *info = [UILabel iconInfoWithName:iconName];
    if (info.count == 0 && defaultIcon.length > 0) {
        info = [UILabel iconInfoWithName:defaultIcon];
    }
    NSString *code = [info stringForKey:kIconCode defaultValue:@""];
    self.text = code;
}

@end
