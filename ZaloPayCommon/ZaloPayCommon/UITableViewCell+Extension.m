//
//
//  Created by Bon on 10/25/15.
//  Copyright © 2015 Bon. All rights reserved.
//

#import "UITableViewCell+Extension.h"
#import "UIView+Extention.h"

@implementation UITableViewCell (Extension)


+ (NSString *)identify {
    return NSStringFromClass(self);
}

+ (instancetype)defaultCellForTableView:(UITableView *)tableView {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[self identify]];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[self identify]];
    }
    return cell;
}

+ (instancetype)cellFromSameNibForTableView:(UITableView *)tableView {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[self identify]];
    if (!cell) {
        cell = [self viewFromSameNib];
    }
    return cell;
}

+ (instancetype)cellForTableView:(UITableView *)tableView {
    return [self defaultCellForTableView:tableView];
}
+ (float)height {
    return 44.0;
}

+ (UITableViewCell *)makeATemplateCell {
    UITableViewCell * cell = [[UITableViewCell alloc] init];
    [cell setBackgroundColor :[UIColor clearColor]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
@end
