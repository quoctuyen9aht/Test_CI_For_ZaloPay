//
//  UIView+Config.m
//  ZaloPayCommon
//
//  Created by bonnpv on 4/21/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "UIView+Config.h"

@implementation UIView (Config)
+ (void)configSreen2x:(ViewConfigBlock)config2x scree3x:(ViewConfigBlock)config3x {
    BOOL isScreen3x= ![self isScreen2x];
    
    if (isScreen3x && config3x ) {
        config3x();
        return;
    }
    if (!isScreen3x && config2x) {
        config2x();
    }
}

+ (BOOL)isScreen2x {
    static BOOL isScreen2x;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        isScreen2x  =[UIScreen mainScreen].scale == 2.0;
    });
    return isScreen2x;
}

+ (BOOL)isIPhone6Plus {
    if (CGRectGetHeight([[UIScreen mainScreen] bounds]) == 736) {
        return YES;
    }
    return NO;
}

+ (BOOL)isIPhone4 {
    if (CGRectGetHeight([[UIScreen mainScreen] bounds]) == 480) {
        return YES;
    }
    return NO;
}

+ (BOOL)isIPhone5 {
    if (CGRectGetHeight([[UIScreen mainScreen] bounds]) == 568) {
        return YES;
    }
    return NO;
}

+ (BOOL)isIPhone6 {
    if (CGRectGetHeight([[UIScreen mainScreen] bounds]) == 667) {
        return YES;
    }
    return NO;
}

+ (BOOL)isIPhoneX {
    if (CGRectGetHeight([[UIScreen mainScreen] bounds]) == 812) {
        return YES;
    }
    return NO;
}
+ (BOOL)isHeightIP6PlusOrLarger {
    return CGRectGetHeight([[UIScreen mainScreen] bounds]) >= 736 ;
}
@end
