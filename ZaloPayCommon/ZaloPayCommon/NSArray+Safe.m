//
//  NSArray+Safe.m
//  MarQet
//
//  Created by Bon on 2/14/15.
//  Copyright (c) 2015 Bon. All rights reserved.
//

#import "NSArray+Safe.h"
#import <UIKit/UIKit.h>


@implementation NSArray (Safe)
- (id)safeObjectAtIndex:(NSInteger)index {
    if (index < self.count) {
        return [self objectAtIndex:index];
    }
    return nil;
}

- (id)itemAtIndexPath:(NSIndexPath *)indexpath {
    NSArray *section = [self safeObjectAtIndex:indexpath.section];
    if ([section isKindOfClass:[NSArray class]]) {
        return [section safeObjectAtIndex:indexpath.row];
    }
    return [self safeObjectAtIndex:indexpath.row];
}

- (NSArray *)sectionAtIndexPath:(NSIndexPath *)indexPath {
    return [self sectionAtIndex:indexPath.section];
}
- (NSArray *)sectionAtIndex:(NSInteger)index {
    NSArray *secitons = [self safeObjectAtIndex:index];
    if ([secitons isKindOfClass:[NSArray class]]) {
        return secitons;
    }
    return nil;
}

- (NSArray *)subArrayAtIndex:(NSInteger)index withTotalItem:(NSInteger)total {
    NSInteger startIndex = index *total;
    return [self itemsAtIndex:startIndex withTotalItem:total];
}

- (NSArray *)itemsAtIndex:(NSInteger)startIndex withTotalItem:(NSInteger)total {
    NSInteger length = (self.count - startIndex) > total ? total : self.count - startIndex;
    if (startIndex  + length > self.count) {
      //  DDLogInfo(@"startIndex :%ld",(long)startIndex);
      //  DDLogInfo(@"length :%ld",(long)length);
      //  DDLogInfo(@"count :%ld",(long)self.count);
        return nil;
    }
    NSRange range = NSMakeRange(startIndex, length);
    return [self subarrayWithRange:range];


}
- (void)forEach:(void (^)(id obj))handler {
    for (id obj in self) {
        handler(obj);
    }
}

- (NSArray *)map:(id(^)(id obj))transform {
    NSMutableArray *results = [NSMutableArray new];
    [self forEach:^(id obj) {
        id result = transform(obj);
        if (result) {
            if ([result isKindOfClass:[NSArray class]]){
                [results addObjectsFromArray:result];
            }else {
                [results addObject:result];
            }
        }
    }];
    return results;
}

- (NSInteger)subArrayCount:(NSInteger)total {
    return ceil((float)self.count/total);
}
@end

@implementation NSMutableArray (SafeRemove)
- (void)safeRemoveObjectAtIndex:(NSInteger)index {
    if (index < self.count) {
        [self removeObjectAtIndex:index];
    }
}
@end

@implementation NSArray(First)
- (id)firstObjectUsing:(BOOL(^)(id))blockCondition {
    for (id value in self) {
        if (blockCondition(value)) {
            return value;
        }
    }
    return nil;
}
@end
