//
//  Collection+P2PNotification.m
//  ZaloPayCommon
//
//  Created by phucpv on 7/4/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "Collection+P2PNotification.h"
#import "MF_Base64Additions.h"
#import "NSCollection+JSON.h"
#import "NSString+Asscci.h"

const int NotificationP2PTransferType = 1;
const int NotificationP2PThankMessageType = 2;
typedef enum {
    P2PNotfQRTransferType = 1,
    P2PNotfThankMessageType = 2,
} P2PNotificationType;


@implementation NSString (P2PNotification)
+ (NSString *)encodeP2PMessage:(NSString *)string {
    NSData *data = [string dataUsingEncoding:NSUTF16StringEncoding];
    return [data base64String];
}

+ (NSString *)decodeP2PMessage:(NSString *)string {
    return [[NSString alloc] initWithData:[NSData dataWithBase64String:string]
                                 encoding:NSUTF16StringEncoding];
}

@end

@implementation NSString(ThankMessage)

+ (NSString *)embededDataThankMessageWithTransId:(NSString *)transId
                                     displayName:(NSString *)displayName
                                         message:(NSString *)message
                                       zalopayId:(NSString *)zalopayId
                                       timestamp:(NSString *)timestamp {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@(P2PNotfThankMessageType) forKey:@"type"];
    [params setObjectCheckNil:transId forKey:@"transid"];
    [params setObjectCheckNil:zalopayId forKey:@"zalopayid"];
    [params setObjectCheckNil:timestamp forKey:@"timestamp"];
    
    [params setObjectCheckNil:[NSString encodeP2PMessage:message] forKey:@"message"];
    [params setObjectCheckNil:[NSString encodeP2PMessage:displayName] forKey:@"displayname"];
    
    NSString *data = [params JSONRepresentation];
    return [data base64String];
}

@end

@implementation NSString(QRTransfer)

+ (NSString *)embededDataQRTransferWithDisplayName:(NSString *)displayName
                                            avatar:(NSString *)avatar
                                          progress:(int)progress
                                            amount:(int64_t)amount
                                           transId:(NSString *)transId {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@(P2PNotfQRTransferType) forKey:@"type"];
    [params setObjectCheckNil:avatar forKey:@"avatar"];
    [params setObjectCheckNil:transId forKey:@"transid"];
    [params setObject:@(progress) forKey:@"mt_progress"];
    [params setObject:@(amount) forKey:@"amount"];
    
    [params setObjectCheckNil:[NSString encodeP2PMessage:displayName] forKey:@"displaynamev2"];
    [params setObjectCheckNil:[displayName removeSpecialCharacters] forKey:@"displayname"];
    
    NSString *data = [params JSONRepresentation];
    return [data base64String];
}

@end


