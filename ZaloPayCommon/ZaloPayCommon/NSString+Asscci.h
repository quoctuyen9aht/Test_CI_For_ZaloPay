//
//  NSString+Asscci.h
//  
//
//  Created by Bon on 9/28/15.
//  Copyright (c) 2015 Bon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Ascci)
- (NSString *)ascciString;
- (NSString *)generateMD5:(NSString *)string;
- (NSString *)removeSpecialCharacters;

@end
