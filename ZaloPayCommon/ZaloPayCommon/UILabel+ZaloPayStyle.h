//
//  UILabel+ZaloPayStyle.h
//  ZaloPayCommon
//
//  Created by bonnpv on 6/27/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>


//https://gitlab.zalopay.vn/zalopay-apps/task/issues/640#note_4559

@interface UILabel (ZaloPayStyle)

// text chính với size 16 cho 3x, 15 cho 2x
- (void)zpMainBlackRegularHomeCell;

- (void)zpMainBlackRegular;

- (void)zpPlaceHolderRegular;

- (void)zpMainGrayRegular;

- (void)zpMainBlackItalic;

- (void)zpMainGrayItalic;

// text section size 13  cho 2x và 3x

- (void)zpBoxTitleGayRegular;
- (void)zpBoxSubTitleGrayRegular;

// text chú thích với size 13 cho 3x, 11 cho 2x

- (void)zpDetailGrayStyle;

// text phụ size 14 cho 3x, 13 cho 2x

- (void)zpSubTextGrayRegular;

// text medium với size 16 cho 3x, 15 cho 2x

- (void)zpMediumTitleStyle;

- (void)zpDefaultBtnTitleStyle;

+ (CGFloat)navigationSize;
+ (CGFloat)mainTextSize;
+ (CGFloat)smallMainTextSize;
+ (CGFloat)boxTitleSize;
+ (CGFloat)zpDetailGraySize;
+ (CGFloat)subTitleSize;

@end
