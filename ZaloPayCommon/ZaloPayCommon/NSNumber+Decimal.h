//
//  NSNumber+Decimal.h
//  ZaloPay
//
//  Created by Nguyễn Hữu Hoà on 1/20/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (Decimal)
+ (NSNumberFormatter*) currencyNumberFormatter;
- (NSString *)formatDecimal;
- (NSString *)formatCurrency;
- (NSString *)formatMoneyValue;

- (NSNumber *)encrypt;
- (NSNumber *)decrypt;
@end
