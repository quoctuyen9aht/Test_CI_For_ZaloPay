//
//  NSRegularExpression+Valid.m
//  ZaloPayCommon
//
//  Created by Dung Vu on 5/3/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "NSRegularExpression+Valid.h"
#import "NSObject+Validate.h"
@implementation NSRegularExpression(Valid)
+ (BOOL)checkRegularExpression:(NSString *)exp url:(NSString *)url {
    if (!validObject(url, [NSString class])) {
        return NO;
    }
    
    NSRegularExpression *regExOtp = [[NSRegularExpression alloc]
                                     initWithPattern:exp
                                     options:NSRegularExpressionCaseInsensitive error:NULL];
    
    NSArray *matchArray = [regExOtp matchesInString:url options:0 range: NSMakeRange(0, url.length)];
    return (matchArray != NULL && matchArray.count > 0);
}
@end
BOOL isValidURL(NSString *url,ZPRegularExpressionType type) {
    return NO;
}
