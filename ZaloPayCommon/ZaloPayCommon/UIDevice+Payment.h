//
//  UIDevice+Payment.h
//  ZaloPayCommon
//
//  Created by bonnpv on 4/29/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (Payment)
- (NSString *) screenTypeString;
@end
