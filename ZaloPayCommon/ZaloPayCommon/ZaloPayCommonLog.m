//
//  ZaloPayCommonLog.m
//  ZaloPayCommon
//
//  Created by bonnpv on 6/8/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZaloPayCommonLog.h"

const DDLogLevel ZaloPayCommonLogLevel = DDLogLevelInfo;
