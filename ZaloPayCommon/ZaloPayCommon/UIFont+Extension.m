//
//  UIFont+SFUIDisplay.m
//  
//
//  Created by PhucPv on 10/14/15.
//  Copyright (c) 2015-present VNG Corporation All rights reserved.
//

#import "UIFont+Extension.h"
#import "ZaloPayCommonLog.h"
#import "UIView+Config.h"
#import <CoreText/CoreText.h>
#import <ReactiveObjC/ReactiveObjC.h>

@implementation UIFont(Extension)
+ (UIFont *)robotoBoldWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"Roboto-Bold" size:size];
}
+ (UIFont *)robotoRegularWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"Roboto-Regular" size:size];
}
+ (UIFont *)robotoMediumWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"Roboto-Medium" size:size];
}
+ (UIFont *)robotoMediumItalicWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"Roboto-MediumItalic" size:size];
}
+ (UIFont *)helveticaNeueRegularWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"HelveticaNeue" size:size];
}
+ (UIFont *)helveticaNeueBoldWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"HelveticaNeue-Bold" size:size];
}
+ (UIFont *)helveticaNeueMediumWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"HelveticaNeue-Medium" size:size];
}

+ (UIFont *)SFUITextRegularWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"SFUIText-Regular" size:size];
}

+ (UIFont *)SFUITextMediumWithSize:(CGFloat)size {
  //  [self dumpAllFonts];
    return [UIFont fontWithName:@"SFUIText-Medium" size:size];
}

+ (UIFont *)SFUITextSemiboldWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"SFUIText-Semibold" size:size];
}

+ (UIFont *)SFUITextRegularItalicWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"SFUIText-RegularItalic" size:size];
}

+ (UIFont *)SFUITextSemiboldItalicWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"SFUIText-SemiboldItalic" size:size];
}

+ (UIFont *)zaloPayWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"zalopay" size:size];
}

+ (UIFont *)ocraRegular:(CGFloat)size {
    return [self fontWithName:@"OCRATributeW01-RegularMono" size:size];
}

+ (UIFont *)SFUITextRegularSmall {
    return [UIFont SFUITextRegularWithSize:14];
}

+ (UIFont *)SFUITextRegularBalanceValue {
    return [UIFont SFUITextRegularWithSize:48];
}

+ (UIFont *)SFUITextRegularMedium {
    return [UIFont SFUITextRegularWithSize:16];
}

+ (UIFont *)defaultSFUITextRegular {
    if ([UIView isScreen2x]) {
        return [self SFUITextRegularWithSize:15];
    }
    return [self SFUITextRegularWithSize:17];
}

+ (UIFont *)defaultSFUITextItalic {
    if ([UIView isScreen2x]) {
        return [self SFUITextRegularItalicWithSize:15];
    }
    return [self SFUITextRegularItalicWithSize:17];

}

+ (UIFont *)SFUITextMediumWithAmount:(long)amount {
    if (amount < 100000) {
        return [self SFUITextMediumWithSize:48];
    }
    
    if (amount < 1000000) {
        return [self SFUITextMediumWithSize:42];
    }
    return [self SFUITextMediumWithSize:38];
}

/*
 2016-04-20 09:29:21.355 ZaloPay[764:17193] SFUIText-Regular
 2016-04-20 09:29:21.355 ZaloPay[764:17193] SFUIText-Semibold
 2016-04-20 09:29:21.355 ZaloPay[764:17193] SFUIText-RegularItalic
 2016-04-20 09:29:21.356 ZaloPay[764:17193] SFUIText-SemiboldItalic
 */


+ (void)dumpAllFonts{
    for (NSString *familyName in [UIFont familyNames]) {
        for (NSString *fontName in [UIFont fontNamesForFamilyName:familyName]) {
            DDLogInfo(@"%@", fontName);
        }
    }
}

+ (void)registerFontWithData:(NSData *)inData {
    [self registerFontWithData:inData subscriber:nil];
}

+ (void)registerFontWithData:(NSData *)inData
                  subscriber:(id<RACSubscriber>) signal {
    
    if(inData) {
        CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)inData);
        CGFontRef font = CGFontCreateWithDataProvider(provider);
        if (font != NULL)
        {
            
            CFErrorRef error;
            CTFontManagerUnregisterGraphicsFont(font, &error);
            BOOL isRegistered = CTFontManagerRegisterGraphicsFont(font, &error);
            if (signal) {
                if (isRegistered) {
                    [signal sendCompleted];
                    //font can be used
#if DEBUG
                    //                [UIFont dumpAllFonts];
#endif
                } else {
                    [signal sendError:(__bridge NSError *)(error)];
                }
            }
            CFRelease(font);
        }
        CFRelease(provider);
    }
}


@end
