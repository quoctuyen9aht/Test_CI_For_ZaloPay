//
//  FileUtils.m
//  PaymentReactNativeApp
//
//  Created by Nguyễn Hữu Hoà on 5/25/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "FileUtils.h"
#import "ZaloPayCommonLog.h"
#import <Objective-Zip/Objective-Zip.h>

@implementation FileUtils
+ (void)createFolderAtPath:(NSString *)pathName {
    [[NSFileManager defaultManager ] createDirectoryAtPath:pathName
                               withIntermediateDirectories:YES
                                                attributes:nil
                                                     error:nil];
}

+ (void)removeFolderAtPath:(NSString *)pathName {
    [[NSFileManager defaultManager] removeItemAtPath:pathName error:nil];
}

+ (void)copyFolderAtPath:(NSString *)srcPath toPath:(NSString *)dstPath overrideOldFile:(BOOL)overrideOldFile {
    if (overrideOldFile) {
        [self removeFolderAtPath:dstPath];
    }
    NSError * error = nil;
    if (![[NSFileManager defaultManager] copyItemAtPath:srcPath toPath:dstPath error:&error]) {
        DDLogInfo(@"coppy folder error :%@", error);
        DDLogInfo(@"srcPath :%@", srcPath);
        DDLogInfo(@"toPath :%@", dstPath);
    }
}

+ (BOOL)unzipFileAtPath:(NSString *)srcPath
           toFolderPath:(NSString *)dstPath
          removeZipFile:(BOOL)removeZipFile
{
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:srcPath]) {
        return FALSE;
    }
    BOOL success = TRUE;
    OZZipFile *unzipFile = nil;
    @try {
        DDLogInfo(@"dstPath : %@",dstPath);
        [FileUtils createFolderAtPath:dstPath];
        
        unzipFile = [[OZZipFile alloc] initWithFileName:srcPath
                                                   mode:OZZipFileModeUnzip];
        NSArray *infos= [unzipFile listFileInZipInfos];
        for (OZFileInZipInfo *info in infos) {
            [unzipFile locateFileInZip:info.name];
            NSString *filePath = [dstPath stringByAppendingPathComponent:info.name];
            OZZipReadStream *read = [unzipFile readCurrentFileInZip];
            NSMutableData *data = [[NSMutableData alloc] initWithLength:(NSUInteger)info.length];
            long bytesRead= [read readDataWithBuffer:data];
            if (bytesRead <= 0) {
                [FileUtils createFolderAtPath:filePath];
                continue;
            }
            
            if ([data writeToFile:filePath atomically:NO]) {
                [read finishedReading];
                data = nil;
                continue;
            }
            
            // case : info.name = /config/zalopay_config.json
            NSArray *folders = [info.name componentsSeparatedByString:@"/"];
            if (folders.count > 1) {
                NSString *root = [dstPath copy];
                for (int i = 0; i < folders.count - 1; i++) {
                    root = [dstPath stringByAppendingPathComponent:folders[i]];
                    if (![[NSFileManager defaultManager] fileExistsAtPath:root]) {
                        [self createFolderAtPath:root];
                    }
                }
            }
            [data writeToFile:filePath atomically:NO];
            [read finishedReading];
            data = nil; // release
        }
        [unzipFile close];
    }
    @catch (NSException *exception) {
        DDLogError(@"exception = %@", exception);
        success = FALSE;
    }
    @finally {
        if (removeZipFile) {
            [self removeFolderAtPath:srcPath];
        }
    }
    return success;
}

//+ (BOOL)unzipFileAtPath:(NSString *)srcPath
//           toFolderPath:(NSString *)dstPath
//          removeZipFile:(BOOL)removeZipFile
//{
//    BOOL success = TRUE;
//    OZZipFile *unzipFile = nil;
//    @try {
//        DDLogInfo(@"dstPath : %@",dstPath);
//        [FileUtils createFolderAtPath:dstPath];
//        
//        unzipFile = [[OZZipFile alloc] initWithFileName:srcPath
//                                                   mode:OZZipFileModeUnzip];
//        NSArray *infos= [unzipFile listFileInZipInfos];
//        for (OZFileInZipInfo *info in infos) {
//            [unzipFile locateFileInZip:info.name];
//            NSString *filePath = [dstPath stringByAppendingPathComponent:info.name];
//            OZZipReadStream *read = [unzipFile readCurrentFileInZip];
//            NSMutableData *data = [[NSMutableData alloc] initWithLength:(NSUInteger)info.length];
//            long bytesRead= [read readDataWithBuffer:data];
//            if (bytesRead > 0) {
//                [data writeToFile:filePath atomically:NO];
//                [read finishedReading];
//                
//                data = nil; // release
//                continue;
//            }
//            [FileUtils createFolderAtPath:filePath];
//        }
//        [unzipFile close];
//    }
//    @catch (NSException *exception) {
//        DDLogError(@"exception = %@", exception);
//        success = FALSE;
//    }
//    @finally {
//        if (removeZipFile) {
//            [self removeFolderAtPath:srcPath];
//        }
//    }
//    return success;
//}

@end
