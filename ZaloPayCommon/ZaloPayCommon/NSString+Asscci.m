//
//  NSString+Asscci.m
//  
//
//  Created by Bon on 9/28/15.
//  Copyright (c) 2015 Bon. All rights reserved.
//

#import "NSString+Asscci.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (Ascci)
- (NSString *)ascciString {
    return [self filterString:self];
}

- (NSString *)removeSpecialCharacters {
//    NSString unfilteredString = @"!@#$%^&()_+|abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    
    NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ "] invertedSet];
    return [[[self ascciString] componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
    
}

- (NSString *)filterString:(NSString *)string {
    NSString *trimmedString = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([trimmedString length] == 0) {
        return @"";
    }
    return [self stringWithoutAccentsFromString:trimmedString];
}

- (NSString*)stringWithoutAccentsFromString:(NSString*)s {
    if (!s) {
        return @"";
    }
    
    NSMutableString *result = [s mutableCopy];
    // __bridge only required if you compile with ARC:
    CFStringTransform((__bridge CFMutableStringRef)result, NULL, kCFStringTransformStripCombiningMarks, NO);
    
    [result replaceOccurrencesOfString:@"đ" withString:@"d" options:0 range:NSMakeRange(0, [result length])];
    [result replaceOccurrencesOfString:@"Đ" withString:@"D" options:0 range:NSMakeRange(0, [result length])];
    
    return result;
}
- (NSString *)generateMD5:(NSString *)string{
    const char *cStr = [string UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    
    CC_MD5( cStr, (CC_LONG)strlen(cStr), result );
    
    return [NSString
            stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1],
            result[2], result[3],
            result[4], result[5],
            result[6], result[7],
            result[8], result[9],
            result[10], result[11],
            result[12], result[13],
            result[14], result[15]
            ];
}
@end
