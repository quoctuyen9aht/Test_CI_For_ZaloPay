//
//  ZPTextInputView.h
//  ZaloPayCommom
//
//  Created by nhatnt on 27/11/2017.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "JVFloatLabeledTextField.h"
#import "ZPFloatTextInputView.h"

#define MAX_CHARACTERS_INPUT 30

extern uint64_t kMaxInputValue;

@interface ZPCalFloatTextInputView : ZPFloatTextInputView
- (void)updateTextField:(UITextField*)textField updateText:(NSString*)text;
- (void)showErrorAuto:(NSString *)errorText;
- (BOOL)isExpression;
- (BOOL)isMaxCharactor:(NSString *)newString;
- (BOOL)isGreaterThanMaxInputValue:(NSString *)text;
@end
