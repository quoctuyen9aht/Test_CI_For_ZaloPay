//
//  UIDevice+Name.h
//  ZaloPayCommon
//
//  Created by bonnpv on 12/28/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (Name)
- (NSString *)deviceName;
@end
