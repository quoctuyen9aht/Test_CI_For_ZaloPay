//
//  UIImage+IconFont.m
//  ZaloPayCommon
//
//  Created by bonnpv on 2/7/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "UIImage+IconFont.h"
#import "UIFont+IconFont.h"
#import "UILabel+IconFont.h"

@implementation UIImage (IconFont)

+ (UIImage*)imageWithIcon:(NSString*)identifier
          backgroundColor:(UIColor*)bgColor
                iconColor:(UIColor*)iconColor
                  andSize:(CGSize)size {
    
    if (!bgColor) {
        bgColor = [UIColor clearColor];
    }
    if (!iconColor) {
        iconColor = [UIColor whiteColor];
    }
    
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0f);
    NSString* textContent = [UILabel iconCodeWithName:identifier];
    CGRect textRect = CGRectZero;
    textRect.size = size;
    
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:textRect];
    [bgColor setFill];
    [path fill];
    int fontSize = MIN(size.height, size.width);
    UIFont *font = [UIFont iconFontWithSize:fontSize];
    [iconColor setFill];
    
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    style.alignment = NSTextAlignmentCenter;
    [textContent drawInRect:textRect withAttributes:@{NSFontAttributeName : font,
                                                      NSForegroundColorAttributeName : iconColor,
                                                      NSBackgroundColorAttributeName : bgColor,
                                                      NSParagraphStyleAttributeName: style,
                                                      }];
    
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
@end
