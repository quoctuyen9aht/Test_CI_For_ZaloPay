//
//  NSObject+Validate.m
//  ZaloPayCommon
//
//  Created by Dung Vu on 4/19/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "NSObject+Validate.h"
BOOL validObject(id _Nullable obj,Class _Nonnull cls) {
    return [obj isKindOfClass:cls];    
}

void excuteInSafeThread(BlockExcution _Nonnull excution) {
    dispatch_async(dispatch_get_main_queue(), ^{
        excution();
    });
}

@implementation NSObject(Validate)
+ (instancetype _Nullable)castFrom:(id _Nullable)obj {
    if (validObject(obj, self)) {
        return obj;
    }
    return nil;
}
@end
