//
//  NSArray+ConstrainsLayout.h
//  ZaloPay
//
//  Created by PhucPv on 1/17/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MasonryConfig.h"

@interface NSArray(ConstrainsLayout)
- (void)viewsAlongAxis:(MASAxisType)axisType
      withFixedSpacing:(CGFloat)fixedSpacing
           leadSpacing:(CGFloat)leadSpacing;
@end
