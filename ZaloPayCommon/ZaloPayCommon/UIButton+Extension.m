//
//  UIButton+Extension.m
//  ZaloPay
//
//  Created by PhucPv on 1/4/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "UIButton+Extension.h"
#import "UIImage+Extention.h"
#import "UIColor+Extension.h"
#import "UIFont+Extension.h"
#import "UIView+Extention.h"

CGFloat const kZaloButtonHeight = 48;
float const kZaloButtonToScrollOffset = 45;
float const kMinZaloButtonToScrollOffset = 10;

@implementation UIButton(Extension)

- (void)setBackgroundColor:(UIColor *)backgroundColor forState:(UIControlState)state {
    [self setBackgroundImage:[UIImage imageFromColor:backgroundColor]
                    forState:state];
}

- (void)setupZaloPayButton {
    [self setBackgroundColor:[UIColor disableButtonColor] forState:UIControlStateDisabled];
    [self setBackgroundColor:[UIColor zaloBaseColor] forState:UIControlStateNormal];
    [self setBackgroundColor:[UIColor buttonHighlightColor] forState:UIControlStateHighlighted];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.titleLabel setFont:[UIFont SFUITextRegularWithSize:17]];
    [self roundRect:3];

}
@end
