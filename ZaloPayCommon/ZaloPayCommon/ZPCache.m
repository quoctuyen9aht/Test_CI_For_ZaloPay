//
//  ZPCache.m
//  ZaloPayCommon
//
//  Created by bonnpv on 1/14/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "ZPCache.h"
#import "ZaloPayCommonLog.h"
#import <PINCache/PINCache.h>

@implementation ZPCache

+ (void)setObject:(id <NSCoding>)object forKey:(NSString *)key {
    if (key.length == 0) {
        return;
    }
    if (object == nil) {
        [[PINCache sharedCache] removeObjectForKey:key];
        return;
    }
    [[PINCache sharedCache] setObject:object forKey:key];
}

+ (id)objectForKey:(NSString *)key {
    id _return = nil;
    @try {
        _return = [[PINCache sharedCache] objectForKey:key];
    } @catch (NSException *exception) {
        DDLogInfo(@"key = %@ exception: %@", key, exception);
        [[PINCache sharedCache] removeObjectForKey:key];
    } @finally {
        
    }
    return _return;
}

+ (void)removeObjectForKey:(NSString *)key {
    if (key) {
        [[PINCache sharedCache] removeObjectForKey:key];
    }
}
@end
