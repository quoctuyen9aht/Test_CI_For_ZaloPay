//
//  NSString+Validation.h
//  ZaloPay
//
//  Created by PhucPv on 5/22/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(Validation)
- (BOOL)isValidEmail;
- (BOOL)isValidPhoneNumber;
- (BOOL)isNumber;

- (NSString *)formatPhoneNumber;
- (NSString *)format:(NSInteger)groupLenght
                with:(NSString *)separator
           direction:(BOOL)isLeftToRight;
- (NSString *)hiddenWith:(NSInteger)startLenght
                     end:(NSInteger)endLenght
           withCharacter:(NSString *)character;
- (NSString*) stringWithHexBytes;
@end
