//
//  UIImage+Extention.m
//  MarQet
//
//  Created by Bon on 2/13/15.
//  Copyright (c) 2015 Bon. All rights reserved.
//

#import "UIImage+Extention.h"

@implementation UIImage (Extention)

+ (UIImage *)defaultAvatar {
    return [UIImage imageNamed:@"profile_avatar"];
}

+ (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (UIImage*)scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContextWithOptions( newSize, NO, [UIScreen mainScreen].scale );
    [self drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (UIImage *)scaledAspectToSize:(CGSize)newSize {
    CGRect scaledImageRect = CGRectZero;
    
    CGFloat aspectWidth = newSize.width / self.size.width;
    CGFloat aspectHeight = newSize.height / self.size.height;
    CGFloat aspectRatio = MIN ( aspectWidth, aspectHeight );
    
    scaledImageRect.size.width = self.size.width * aspectRatio;
    scaledImageRect.size.height = self.size.height * aspectRatio;
    scaledImageRect.origin.x = (newSize.width - scaledImageRect.size.width) / 2.0f;
    scaledImageRect.origin.y = (newSize.height - scaledImageRect.size.height) / 2.0f;
    
    UIGraphicsBeginImageContextWithOptions( newSize, NO, 0 );
    [self drawInRect:scaledImageRect];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return scaledImage;

}

- (UIImage*)resizeImageWithScale:(CGFloat)scale {
    //    return self;
    CGSize newSize = CGSizeMake(self.size.width*scale, self.size.height*scale);
    return [self scaledToSize:newSize];
}

- (void)saveImageToDoucument:(NSString *)name {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",name]];
//    DDLogInfo(@"%@",savedImagePath);
    NSData *imageData = UIImagePNGRepresentation(self);
    [imageData writeToFile:savedImagePath atomically:YES];
}

+ (UIImage *)filledImageFrom:(UIImage *)source withColor:(UIColor *)color{
    
    // begin a new image context, to draw our colored image onto with the right scale
    UIGraphicsBeginImageContextWithOptions(source.size, NO, [UIScreen mainScreen].scale);
    
    // get a reference to that context we created
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // set the fill color
    [color setFill];
    
    // translate/flip the graphics context (for transforming from CG* coords to UI* coords
    CGContextTranslateCTM(context, 0, source.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    CGContextSetBlendMode(context, kCGBlendModeColorBurn);
    CGRect rect = CGRectMake(0, 0, source.size.width, source.size.height);
    CGContextDrawImage(context, rect, source.CGImage);
    
    CGContextSetBlendMode(context, kCGBlendModeSourceIn);
    CGContextAddRect(context, rect);
    CGContextDrawPath(context,kCGPathFill);
    
    // generate a new UIImage from the graphics context we drew onto
    UIImage *coloredImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //return the color-burned image
    return coloredImg;
}

+ (UIImage*) imageWithImage:(UIImage*) source
                      color:(UIColor *)color
                      alpha:(CGFloat) alpha
// Note: the hue input ranges from 0.0 to 1.0, both red.  Values outside this range will be clamped to 0.0 or 1.0.
{
    // Find the image dimensions.
    CGRect rect = CGRectMake(0, 0, source.size.width, source.size.height);
//
    CGSize imageSize = [source size];
//    CGRect imageExtent = CGRectMake(0,0,imageSize.width,imageSize.height);
//    
//    // Create a context containing the image.
    UIGraphicsBeginImageContext(imageSize);
    CGContextRef context = UIGraphicsGetCurrentContext();
//    [source drawAtPoint:CGPointMake(0,0)];
//    
//    // Draw the hue on top of the image.
//    CGContextSetBlendMode(context, kCGBlendModeHue);
//    [color set];
//    // mask by alpha values of original image
////    CGContextSetBlendMode(context, kCGBlendModeDestinationIn);
////    CGContextDrawImage(context, rect, source.CGImage);
//    
//    UIBezierPath *imagePath = [UIBezierPath bezierPathWithRect:imageExtent];
//    [imagePath fill];
//    
//    // Retrieve the new image.
    // draw black background to preserve color of transparent pixels
//    CGContextSetBlendMode(context, kCGBlendModeNormal);
//    [[UIColor blackColor] setFill];
//    CGContextFillRect(context, rect);
    
    // draw original image
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGContextDrawImage(context, rect, source.CGImage);
    
    // tint image (loosing alpha) - the luminosity of the original image is preserved
    CGContextSetBlendMode(context, kCGBlendModeColor);
    [color setFill];
    CGContextFillRect(context, rect);
    
    // mask by alpha values of original image
    CGContextSetBlendMode(context, kCGBlendModeDestinationIn);
    CGContextDrawImage(context, rect, source.CGImage);
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    CGImageRef cgRef = result.CGImage;
    return [[UIImage alloc] initWithCGImage:cgRef scale:source.scale orientation:UIImageOrientationDown];
    
    return result;
}

- (UIImage*) imageWithImage:(UIImage*) source rotatedByHue:(CGFloat) deltaHueRadians;
{
    // Create a Core Image version of the image.
    CIImage *sourceCore = [CIImage imageWithCGImage:[source CGImage]];
    
    // Apply a CIHueAdjust filter
    CIFilter *hueAdjust = [CIFilter filterWithName:@"CIHueAdjust"];
    [hueAdjust setDefaults];
    [hueAdjust setValue: sourceCore forKey: @"inputImage"];
    [hueAdjust setValue: [NSNumber numberWithFloat: deltaHueRadians] forKey: @"inputAngle"];
    CIImage *resultCore = [hueAdjust valueForKey: @"outputImage"];
    
    // Convert the filter output back into a UIImage.
    // This section from http://stackoverflow.com/a/7797578/1318452
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef resultRef = [context createCGImage:resultCore fromRect:[resultCore extent]];
    UIImage *result = [UIImage imageWithCGImage:resultRef];
    CGImageRelease(resultRef);
    
    return result;
    
}

+ (UIImage *)imageWithGradient:(UIImage *)img
                    startColor:(UIColor *)color1
                      endColor:(UIColor *)color2
                          axis:(MASAxisType)axis{
    UIGraphicsBeginImageContextWithOptions(img.size, NO, img.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0, img.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGRect rect = CGRectMake(0, 0, img.size.width, img.size.height);
    
    // Create gradient
    NSArray *colors = [NSArray arrayWithObjects:(id)color1.CGColor, (id)color2.CGColor, nil];
    CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient = CGGradientCreateWithColors(space, (__bridge CFArrayRef)colors, NULL);
    
    // Apply gradient
    CGContextClipToMask(context, rect, img.CGImage);
    if (axis == MASAxisTypeHorizontal) {
        CGContextDrawLinearGradient(context, gradient, CGPointMake(0,0),
                                    CGPointMake(0, img.size.height), 0);
    } else {
        CGContextDrawLinearGradient(context, gradient, CGPointMake(0,0),
                                    CGPointMake(img.size.width, 0), 0);
    }
    
    UIImage *gradientImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    CGGradientRelease(gradient);
    CGColorSpaceRelease(space);
    
    return gradientImage;
}

#pragma mark - QRImage /BarCode
+ (UIImage *)createQRImageWith:(CGSize)outputSize from:(NSData *)qrCodeData {
    if (!qrCodeData) { return nil;}
    CIFilter *qrCodeFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [qrCodeFilter setValue:qrCodeData forKey:@"inputMessage"];
    [qrCodeFilter setValue:@"M" forKey:@"inputCorrectionLevel"]; //default of L,M,Q & H modes
    CIImage *qrCodeImage = qrCodeFilter.outputImage;
    return [UIImage CIImageToUIImage:qrCodeImage andSize:outputSize];
}
+ (UIImage *)createBarCodeImageWith:(CGSize)outputSize from:(NSData *)qrCodeData {
    if (!qrCodeData) { return nil;}
    CIFilter *qrCodeFilter = [CIFilter filterWithName:@"CICode128BarcodeGenerator"];
    [qrCodeFilter setValue:qrCodeData forKey:@"inputMessage"];
    [qrCodeFilter setValue:[NSNumber numberWithFloat:0] forKey:@"inputQuietSpace"];
    CIImage *qrCodeImage = qrCodeFilter.outputImage;
    return [UIImage CIImageToUIImage:qrCodeImage andSize:outputSize];
}
+(UIImage*)CIImageToUIImage:(CIImage *)qrCodeImage andSize:(CGSize)outputSize{
    CGRect imageSize = CGRectIntegral(qrCodeImage.extent); // generated image size
    CIImage *imageByTransform = [qrCodeImage imageByApplyingTransform:CGAffineTransformMakeScale(outputSize.width/CGRectGetWidth(imageSize), outputSize.height/CGRectGetHeight(imageSize))];
    
    UIImage *qrCodeImageByTransform = [UIImage imageWithCIImage:imageByTransform];
    return qrCodeImageByTransform;
}

@end
