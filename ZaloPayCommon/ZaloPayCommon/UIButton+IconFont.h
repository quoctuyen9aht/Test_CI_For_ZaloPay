//
//  UIButton+IconFont.h
//  ZaloPay
//
//  Created by bonnpv on 12/25/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (IconFont)
- (void)setIconFont:(NSString *)iconName forState:(UIControlState)state;
@end
