//
//  UIView+Extention.h
//  MarQet
//
//  Created by Bon on 2/16/15.
//  Copyright (c) 2015 Bon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Extention)
+ (instancetype)viewFromSameNib;
+ (instancetype)viewFromSameNib:(NSBundle *)bundle;
+ (instancetype)viewFromNibName:(NSString *)nibName bundle:(NSBundle *)bundle;
- (UIViewController*)viewController;
- (void)roundRect:(float)radius;
- (void)showShadow;
- (void)showShadow:(float)offSet color:(UIColor *)color opacity:(float)opacity;
- (void)showShadowWithOutBezierPath:(float)offSet color:(UIColor *)color opacity:(float)opacity;
- (void)roundRect:(float)radius
      borderColor:(UIColor *)color
      borderWidth:(float)borderWidth;
- (UITableView *)superTableView;

- (void)debugLayout;
+ (UIEdgeInsets)safeAreaInsets;

@end

