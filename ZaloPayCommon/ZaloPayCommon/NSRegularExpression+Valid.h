//
//  NSRegularExpression+Valid.h
//  ZaloPayCommon
//
//  Created by Dung Vu on 5/3/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM(NSInteger,ZPRegularExpressionType){
    //Smart Link
    ZPRegularExpressionTypeSmartLinkSkippable = 0,
    ZPRegularExpressionTypeSmartLinkMessageError,
    ZPRegularExpressionTypeSmartLinkOTP,
    ZPRegularExpressionTypeBankNetOTP,
    //Vietcombank
    ZPRegularExpressionTypeVCBDirect,
    ZPRegularExpressionTypeVCBDirectErrorURL,
    ZPRegularExpressionTypeVCBDirectCaptchaURL,
    ZPRegularExpressionTypeVCBDirectOtpURL,
    ZPRegularExpressionTypeVCBOtpURL,
    ZPRegularExpressionTypeVCBLoginURL,
    ZPRegularExpressionTypeVCBErrorURL,
    //VTB
    ZPRegularExpressionTypeVTBURL,
    ZPRegularExpressionTypeVTBCaptchaURL,
    ZPRegularExpressionTypeVTBOtpURL,
    //VIB
    ZPRegularExpressionTypeVIBLoginURL,
    ZPRegularExpressionTypeVIBSubmitOtpURL,
    ZPRegularExpressionTypeVIBErrorMessageURL,
    ZPRegularExpressionTypeVIB1PaySelectAccountURL,
    //DABank
    ZPRegularExpressionTypeDABankURL,
    ZPRegularExpressionTypeDABankLoginStepURL,
    ZPRegularExpressionTypeDABankConfirmStepURL,
    ZPRegularExpressionTypeDABankOTPStepURL,
    ZPRegularExpressionTypeDABankErrorMessageURL,
    ZPRegularExpressionTypeDABankTimeOutCallback,
    ZPRegularExpressionTypeDABankExceptionStepURL,
    ZPRegularExpressionTypeDABankOnePayURL,
    //HDBank
    ZPRegularExpressionTypeHDBSMLLoginURL,
    ZPRegularExpressionTypeHDBErrorMessageURL,
    ZPRegularExpressionTypeHDBSMLSubmitOTPURL,
    //BIDV
    ZPRegularExpressionTypeBIDVURL,
    ZPRegularExpressionTypeBIDVOtpURL,
    ZPRegularExpressionTypeBIDVCaptchaURL,
    //iBanking
    ZPRegularExpressionTypeiBankingError,
    //One Pay
    ZPRegularExpressionTypeOnepayURL,
    ZPRegularExpressionTypeOnepayFailLoadURL,
    //ATM
    ZPRegularExpressionTypeATMCallback,
    ZPRegularExpressionTypeATMError,
    //OTP
    ZPRegularExpressionTypeOTPCallback,
    //Captcha
    ZPRegularExpressionTypeCaptchaCallback
};
@interface NSRegularExpression(Valid)
+ (BOOL)checkRegularExpression:(NSString *)exp url:(NSString *)url;
@end
BOOL isValidURL(NSString *url,ZPRegularExpressionType type);
