//
//  UILabel+ZaloPayStyle.m
//  ZaloPayCommon
//
//  Created by bonnpv on 6/27/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import "UILabel+ZaloPayStyle.h"
#import "UIFont+Extension.m"
#import "UIColor+Extension.h"

@implementation UILabel (ZaloPayStyle)

+ (CGFloat)navigationSize {
    if ([UIView isScreen2x]) {
         return 16;
    }
    return 18;
}

+ (CGFloat)mainTextSize {
    if ([UIView isScreen2x]) {
        return 15;
    }
    return 16;
}

+ (CGFloat)smallMainTextSize {
    if ([UIView isScreen2x]) {
        return 10;
    }
    return 11;
}

+ (CGFloat)boxTitleSize {
    return 13;
}

+ (CGFloat)zpDetailGraySize {
    if ([UIView isScreen2x]) {
        return 12;
    }
    return 13;
}

+ (CGFloat)subTitleSize {
    if ([UIView isScreen2x]) {
        return 13;
    }
    return 14;
}

// text chính với size 16 cho 3x, 15 cho 2x
- (void)zpMainBlackRegularHomeCell {
    self.textColor = [UIColor defaultText];
    self.font = [UIFont SFUITextRegularWithSize:15];
}
- (void)zpMainBlackRegular {
    self.textColor = [UIColor defaultText];
    self.font = [UIFont SFUITextRegularWithSize:[UILabel mainTextSize]];
}

- (void)zpPlaceHolderRegular {
    self.textColor = [UIColor placeHolderColor];
    self.font = [UIFont SFUITextRegularWithSize:[UILabel mainTextSize]];
}

- (void)zpMainGrayRegular {
    self.textColor = [UIColor subText];
    self.font = [UIFont SFUITextRegularWithSize:[UILabel mainTextSize]];
}

- (void)zpMainBlackItalic {
    self.textColor = [UIColor defaultText];
    self.font = [UIFont SFUITextRegularItalicWithSize:[UILabel mainTextSize]];
}

- (void)zpMainGrayItalic {
    self.textColor = [UIColor subText];
    self.font = [UIFont SFUITextRegularItalicWithSize:[UILabel mainTextSize]];
}

// text section size 13  cho 2x và 3x

- (void)zpBoxTitleGayRegular {
    self.textColor = [UIColor subText];
    self.font = [UIFont SFUITextRegularWithSize:[UILabel boxTitleSize]];
}

- (void)zpBoxSubTitleGrayRegular {
    self.textColor = [UIColor placeHolderColor];
    self.font = [UIFont SFUITextRegularWithSize:[UILabel subTitleSize]];
}

// text chú thích với size 13 cho 3x, 11 cho 2x

- (void)zpDetailGrayStyle {
    self.textColor = [UIColor subText];
    self.font = [UIFont SFUITextRegularWithSize:[UILabel zpDetailGraySize]];
}

// text phụ size 14 cho 3x, 13 cho 2x

- (void)zpSubTextGrayRegular {
    self.textColor = [UIColor subText];
    self.font = [UIFont SFUITextRegularWithSize:[UILabel zpDetailGraySize]];
}

- (void)zpMediumTitleStyle {
    self.font = [UIFont SFUITextMediumWithSize:[UILabel mainTextSize]];
}
- (void)zpDefaultBtnTitleStyle {
    self.font = [UIFont SFUITextMediumWithSize:[UILabel navigationSize]];
}

@end
