//
//  R.h
//  ZaloPayResource
//
//  Auto-generated.
//  Copyright (c) 2016-2018 VNG Corporation. All rights reserved.
//

#ifndef __R_ZALOPAY_RESOURCES_H__
#define __R_ZALOPAY_RESOURCES_H__

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface R : NSObject
+ (void)loadTextFromDict:(NSDictionary *)dict;

/** Đồng ý */
+ (NSString *)string_ButtonLabel_OK;
/** Đóng */
+ (NSString *)string_ButtonLabel_Close;
/** Hủy */
+ (NSString *)string_ButtonLabel_Cancel;
/** Cập Nhật */
+ (NSString *)string_ButtonLabel_Upgrade;
/** Tiếp Tục */
+ (NSString *)string_ButtonLabel_Next;
/** Bỏ Qua */
+ (NSString *)string_ButtonLabel_Skip;
/** Để sau */
+ (NSString *)string_ButtonLabel_Later;
/** Bắt Đầu */
+ (NSString *)string_ButtonLabel_Begin;
/** Chi Tiết */
+ (NSString *)string_ButtonLabel_Detail;
/** Thử lại */
+ (NSString *)string_ButtonLabel_Retry;
/** Xoá thẻ */
+ (NSString *)string_ButtonLabel_RemoveCard;
/** Xong */
+ (NSString *)string_ButtonLabel_Done;
/** Gửi */
+ (NSString *)string_ButtonLabel_Send;
/** Xoá */
+ (NSString *)string_ButtonLabel_Remove;
/** Giữ lại */
+ (NSString *)string_ButtonLabel_Keep;
/** Xác Nhận Thanh Toán */
+ (NSString *)string_ButtonLabel_Confirm_To_Pay;
/** Chi Tiết Giao Dịch */
+ (NSString *)string_ButtonLabel_Detail_Exchange;
/** Cập nhật */
+ (NSString *)string_ButtonLabel_UpgradeDialog;
/** Đăng nhập */
+ (NSString *)string_ButtonLabel_Login;
/** Quay lại */
+ (NSString *)string_ButtonLabel_Back;
/** Tiếp tục chờ */
+ (NSString *)string_ButtonLabel_ContinueWait;
/** Liên kết */
+ (NSString *)string_ButtonLabel_LinkCard;
/** Bỏ cập nhật */
+ (NSString *)string_ButtonLabel_SkipUpgrade;
/** Ở lại */
+ (NSString *)string_ButtonLabel_Stay;
/** Xác nhận */
+ (NSString *)string_ButtonLabel_Confirm;
/** Thay đổi */
+ (NSString *)string_ButtonLabel_Change;
/** Nhập mật khẩu để truy cập */
+ (NSString *)string_Message_Input_PIN_ManageCard;
/** Nhập mật khẩu để truy cập */
+ (NSString *)string_Message_Input_PIN_Profile;
/** Nhập mật khẩu thanh toán để truy cập */
+ (NSString *)string_Message_Input_PIN_Transactions;
/** Bạn muốn đăng xuất tài khoản này? */
+ (NSString *)string_Message_Signout_Confirmation;
/** Phiên làm việc đã hết hạn.\nVui lòng đăng nhập lại để tiếp tục. */
+ (NSString *)string_Message_Session_Expire;
/** Hệ thống đang bảo trì.\nVui lòng thử lại sau. */
+ (NSString *)string_Message_Server_Maintain;
/** Tài khoản hiện đang bị khoá.\nVui lòng liên hệ 1900 54 54 36 để được trợ giúp */
+ (NSString *)string_Message_Lock_Account;
/** Thông báo */
+ (NSString *)string_Dialog_Title_Notification;
/** Thu hồi tiền */
+ (NSString *)string_Dialog_Title_RevertMoney;
/** Hãy cập nhật thông tin để sử dụng tính năng này */
+ (NSString *)string_Message_Update_Profile;
/** Rút tiền */
+ (NSString *)string_Withdraw_Title;
/** Rút tiền về thẻ/tài khoản đã liên kết */
+ (NSString *)string_Withdraw_Description_Message;
/** Bạn đã Liên kết thẻ ngân hàng thành công. Bạn có muốn tiếp tục rút tiền ? */
+ (NSString *)string_Withdraw_Description_Link_Success_Alert;
/** Liên kết thẻ ngân hàng */
+ (NSString *)string_Withdraw_Description_Link_Bold_Title;
/** Nạp tiền */
+ (NSString *)string_AddCash_Title;
/** Nạp tiền vào tài khoản ZaloPay */
+ (NSString *)string_AddCash_Description_Message;
/** Số tiền (VND) */
+ (NSString *)string_AddCash_TextPlaceHolder;
/** Bạn chưa nhập ZaloPay ID */
+ (NSString *)string_Account_Name_NonEmpty_Message;
/** ZaloPay ID phải từ 4-24 ký tự */
+ (NSString *)string_Account_Name_Length_Message;
/** ZaloPay ID không có dấu cách */
+ (NSString *)string_Account_Name_Space_Message;
/** ZaloPay ID không có kí tự đặc biệt */
+ (NSString *)string_Account_Name_Special_Char_Message;
/** Nhập ZaloPay ID */
+ (NSString *)string_Transfer_InputAccontName;
/** ZaloPay ID được tạo một lần duy nhất và không được thay đổi. Bạn có chắc muốn dùng ID này không? */
+ (NSString *)string_Account_Name_Warning_Message;
/** Bạn nhập sai cú pháp */
+ (NSString *)string_InputMoney_Syntax_Error_Message;
/** Không thể chia cho 0 */
+ (NSString *)string_InputMoney_Cant_Divide_By_Zero_Error_Message;
/** Không được vượt quá 30 kí tự */
+ (NSString *)string_InputMoney_Max_Character_Error_Message;
/** Số tiền tối thiểu %@ */
+ (NSString *)string_InputMoney_SmallAmount_Error_Message;
/** Số tiền không được vượt quá %@ */
+ (NSString *)string_InputMoney_BigAmount_Error_Message;
/** Lời nhắn không được vượt quá %i kí tự */
+ (NSString *)string_InputMessage_Error_Message;
/** Ghi chú không được vượt quá %i kí tự */
+ (NSString *)string_InputNode_Error_Message;
/** Nhập mật khẩu để thanh toán bằng */
+ (NSString *)string_Title_PIN;
/** Số điện thoại */
+ (NSString *)string_Title_Phone_Number;
/** Mật khẩu thanh toán gồm %i chữ số */
+ (NSString *)string_MessageInput_PIN_Require;
/** Số điện thoại không hợp lệ */
+ (NSString *)string_MessageInput_Phone_Invalid;
/** Bạn chưa nhập số điện thoại */
+ (NSString *)string_MessageInput_Phone_Empty;
/** Thiết lập bảo vệ tài khoản */
+ (NSString *)string_Settings_Protect_Account;
/** Tạo lại mật khẩu thanh toán */
+ (NSString *)string_ChangePIN_Title;
/** Đổi mật khẩu thanh toán thành công */
+ (NSString *)string_ChangePIN_Message_Success;
/** Nhập mật khẩu thanh toán cũ */
+ (NSString *)string_ChangePIN_Title_OldPIN;
/** Nhập mật khẩu thanh toán mới */
+ (NSString *)string_ChangePIN_Title_NewPIN;
/** Mật khẩu thanh toán mới và cũ phải khác nhau */
+ (NSString *)string_ChangePIN_Message_Duplicate;
/** <p>Quên mật khẩu thanh toán vui lòng liên hệ <a href='zp-app://support'>1900 54 54 36</a></p> */
+ (NSString *)string_ChangePIN_Note;
/** Thông tin của bạn đã được cập nhật thành công */
+ (NSString *)string_UpdateProfile2_SuccessMessage;
/** Tạo mật khẩu để thanh toán bằng số dư hoặc thẻ đã liên kết */
+ (NSString *)string_UpdateProfile2_Information;
/** Phiên bản ZaloPay của bạn không còn được hỗ trợ. Vui lòng cập nhật phiên bản mới nhất để tiếp tục sử dụng. */
+ (NSString *)string_Force_Update_App_Message;
/** Ứng dụng ZaloPay đã có phiên bản mới. Vui lòng cập nhật để trải nghiệm những tính năng mới nhất! */
+ (NSString *)string_Notice_Update_App_Message;
/** Thông Báo */
+ (NSString *)string_AutoMapCardNotify_Title;
/** Thẻ vừa thanh toán đã được liên kết vào tài khoản ZaloPay của bạn. */
+ (NSString *)string_AutoMapCardNotify_MapCardTitle;
/** Bạn có thể sử dụng thẻ này vào các lần thanh toán sau mà không cần nhập lại số thẻ. */
+ (NSString *)string_AutoMapCardNotify_UseCardTitle;
/** ZaloPay thực hiện theo Tiêu Chuẩn Bảo Mật PCI-DSS và không lưu thông tin thẻ của bạn. */
+ (NSString *)string_AutoMapCardNotify_SercurityTitle;
/** Xem Chi Tiết */
+ (NSString *)string_AutoMapCardNotify_ManageCard;
/** Thiết bị đang sử dụng đã bị Jailbreak. Sử dụng ZaloPay trên thiết bị này sẽ không đảm bảo an toàn và rất có thể dẫn đến việc lộ thông tin tài khoản. ZaloPay khuyến cáo bạn không nên tiếp tục sử dụng ứng dụng ZaloPay trên thiết bị này. Nhấn nút \"Tiếp Tục\" để tiếp tục sử dụng ZaloPay. */
+ (NSString *)string_NotifyJailbreak_Message;
/** Có lỗi xảy ra khi lấy thông tin của ứng dụng. Vui lòng thử lại sau. */
+ (NSString *)string_PayBillSetAppId_ErrorMessage;
/** Miễn phí giao dịch */
+ (NSString *)string_AutoMapCard_FeeTitle;
/** ZaloPay ID */
+ (NSString *)string_Transfer_ZaloPayId;
/** Số ĐT */
+ (NSString *)string_Transfer_PhoneNumber;
/** Danh bạ */
+ (NSString *)string_Transfer_ZaloPayContact;
/** THANH TOÁN */
+ (NSString *)string_Home_PayBill;
/** SỐ DƯ */
+ (NSString *)string_Home_Balance;
/** NGÂN HÀNG */
+ (NSString *)string_Home_LinkCard;
/** Chuyển Tiền */
+ (NSString *)string_Home_TransferMoney;
/** Nhận Tiền */
+ (NSString *)string_Home_ReceiveMoney;
/** Lì Xì */
+ (NSString *)string_Home_Lixi;
/** Đang tải dữ liệu. Vui lòng đợi trong ít phút. */
+ (NSString *)string_HomeReactNative_WaitForDownloading;
/** Tải dữ liệu bị lỗi. Bạn có muốn tải lại không ? */
+ (NSString *)string_HomeReactNative_DownloadAgainMessage;
/** Tải lại */
+ (NSString *)string_HomeReactNative_DownloadAgain;
/** Tải dữ liệu thành công */
+ (NSString *)string_HomeReactNative_DownloadSuccess;
/** Tải dữ liệu thất bại. Bạn có muốn thử tải lại không? */
+ (NSString *)string_HomeReactNative_DownloadError;
/** Không có kết nối mạng */
+ (NSString *)string_Home_InternetConnectionError;
/** Kiểm tra kết nối */
+ (NSString *)string_Home_CheckInternetConnection;
/** Trang Chủ */
+ (NSString *)string_LeftMenu_Home;
/** Thông Báo */
+ (NSString *)string_LeftMenu_Notification;
/** GIAO DỊCH */
+ (NSString *)string_LeftMenu_Transaction_Title;
/** Thanh Toán */
+ (NSString *)string_LeftMenu_PayBill;
/** Nạp Tiền */
+ (NSString *)string_LeftMenu_Recharge;
/** Chuyển Tiền */
+ (NSString *)string_LeftMenu_Transfer;
/** Ngân Hàng */
+ (NSString *)string_LeftMenu_LinkCard;
/** Lịch Sử Thanh Toán */
+ (NSString *)string_LeftMenu_Transaction_History;
/** HỖ TRỢ */
+ (NSString *)string_LeftMenu_Application_Title;
/** FAQ */
+ (NSString *)string_LeftMenu_FAQ;
/** Trung Tâm Hỗ Trợ */
+ (NSString *)string_LeftMenu_TermOfUse;
/** Góp Ý Nhanh */
+ (NSString *)string_LeftMenu_Quick_Comment;
/** Liên Hệ Hỗ Trợ */
+ (NSString *)string_LeftMenu_Support;
/** Danh Sách Quà Tặng */
+ (NSString *)string_LeftMenu_VoucherList;
/** Thông Tin Ứng Dụng */
+ (NSString *)string_LeftMenu_About;
/** Đăng Xuất */
+ (NSString *)string_LeftMenu_SigOut;
/** Chưa cập nhật */
+ (NSString *)string_LeftMenu_ZaloPayIdEmpty;
/** ZaloPay ID: %@ */
+ (NSString *)string_LeftMenu_ZaloPayId;
/** LIÊN KẾT THẺ */
+ (NSString *)string_LinkCard_Title;
/** LIÊN KẾT TÀI KHOẢN */
+ (NSString *)string_LinkBank_Title;
/** <font color='#727f8c'>Tôi đã đọc và đồng ý với <a href='termsOfUse'>thoả thuận sử dụng</a> của ZaloPay.</font> */
+ (NSString *)string_UpdateProfileLevel2TermOfUse;
/** Nhập mật khẩu thanh toán */
+ (NSString *)string_UpdateProfileLevel2_InputMK;
/** Hiển thị mật khẩu thanh toán */
+ (NSString *)string_UpdateProfileLevel2_ShowMK;
/** Nhập số điện thoại */
+ (NSString *)string_UpdateProfileLevel2_InputPhone;
/** ZaloPay ID bao gồm: */
+ (NSString *)string_UpdateProfileLevel2_ZaloPayIdContain;
/** Độ dài từ 4-24 ký tự (gồm chữ và số) */
+ (NSString *)string_UpdateProfileLevel2_ZaloPayIdLength;
/** Không phân biệt chữ hoa, thường */
+ (NSString *)string_UpdateProfileLevel2_Character;
/** Chỉ được đăng kí một lần duy nhất */
+ (NSString *)string_UpdateProfileLevel2_Rule;
/** Thông tin cá nhân */
+ (NSString *)string_UpdateProfileLevel2_Title;
/** Email */
+ (NSString *)string_UpdateProfileLevel3_Email;
/** CMND/Hộ chiếu */
+ (NSString *)string_UpdateProfileLevel3_CMND;
/** <font color='#727f8c'>Tôi đã đọc và đồng ý với <a href='termsOfUse'>thoả thuận sử dụng</a> của ZaloPay</font> */
+ (NSString *)string_UpdateProfileLevel3_TermOfUse;
/** Cập nhật */
+ (NSString *)string_UpdateProfileLevel3_Update_Title;
/** Email không hợp lệ. */
+ (NSString *)string_UpdateProfileLevel3_Email_Invalid;
/** Vui lòng cập nhật ảnh mặt trước CMND/Hộ chiếu. */
+ (NSString *)string_UpdateProfileLevel3_Invalid_Front_IdentifierPhoto;
/** Vui lòng cập nhật ảnh mặt sau CMND/Hộ chiếu. */
+ (NSString *)string_UpdateProfileLevel3_Invalid_BackSize_IdentifierPhoto;
/** Vui lòng cập nhật hình đại diện. */
+ (NSString *)string_UpdateProfileLevel3_Invalid_Avatar;
/** Thông tin của bạn đã được gửi đi và đang chờ duyệt. */
+ (NSString *)string_UpdateProfileLevel3_UploadSuccess_Message;
/** Email không được vượt quá %d kí tự */
+ (NSString *)string_UpdateProfileLevel3_Invalid_EmailLength;
/** CMND/Hộ chiếu không được vượt quá %d kí tự */
+ (NSString *)string_UpdateProfileLevel3_Invalid_IdentifierLength;
/** Cập nhật số điện thoại thành công. Hãy cập nhật Email và CMND để có thể rút tiền. */
+ (NSString *)string_UpdateProfileLevel2_SuccessMessage;
/** Hình ảnh */
+ (NSString *)string_UpdateProfileLevel3_UploadPhoto_Title;
/** Số dư không đủ */
+ (NSString *)string_Withdraw_NotEnoughMoney;
/** Để sử dụng tính năng Rút tiền, bạn cần liên kết đến một trong các ngân hàng sau */
+ (NSString *)string_WithdrawCondition_Title_Requiment;
/** Liên Kết Ngay */
+ (NSString *)string_WithdrawCondition_Connect_Now;
/** Điều kiện rút tiền */
+ (NSString *)string_WithdrawCondition_Title;
/** THÔNG TIN CÁ NHÂN */
+ (NSString *)string_WithdrawCondition_Info;
/** Cập nhật */
+ (NSString *)string_WithdrawCondition_Update;
/** Số điện thoại */
+ (NSString *)string_WithdrawCondition_Phone;
/** Mật khẩu thanh toán */
+ (NSString *)string_WithdrawCondition_Password;
/** Cần cập nhật đầy đủ thông tin cá nhân sau: */
+ (NSString *)string_WithdrawCondition_UpdateInfo;
/** LIÊN KẾT THẺ */
+ (NSString *)string_WithdrawCondition_LinkCard;
/** Thêm */
+ (NSString *)string_WithdrawCondition_AddCard;
/** Cần liên kết một trong các thẻ ATM nội địa: */
+ (NSString *)string_WithdrawCondition_LinkCardTitle;
/** Bạn chưa đủ điều kiện để rút tiền */
+ (NSString *)string_WithdrawCondition_NeedMoreCondition;
/** Để sử dụng được tính năng rút tiền, bạn cần phải cập nhật các thông tin sau: */
+ (NSString *)string_WithdrawCondition_NeedUpdateMoreInfo;
/** Chưa cập nhật */
+ (NSString *)string_ZaloPayId_NotAvalaibale;
/** Số dư TK ZaloPay */
+ (NSString *)string_WalletAction_Title;
/**  Tìm hiểu thêm về ZaloPay */
+ (NSString *)string_WalletAction_MoreAbout;
/** Nạp Tiền */
+ (NSString *)string_WalletAction_Rechager;
/** Từ ngân hàng liên kết vào TK ZaloPay */
+ (NSString *)string_WalletAction_Rechager_Description;
/** Từ TK ZaloPay về ngân hàng liên kết */
+ (NSString *)string_WalletAction_Withdraw_Description;
/** Rút Tiền */
+ (NSString *)string_WalletAction_Withdraw;
/** ZaloPay ID: Chưa cập nhật */
+ (NSString *)string_WalletAction_Withdraw_Not_Update_ZaloPayID;
/** Số dư của tôi (VND) */
+ (NSString *)string_WalletAction_Ballance;
/** ZaloPay ID */
+ (NSString *)string_WalletAction_ZaloPayId;
/** Thanh toán trong 2 giây */
+ (NSString *)string_Login_Slogan;
/** Đăng nhập với Zalo */
+ (NSString *)string_Login_Button_Title;
/** Chấp nhận thẻ */
+ (NSString *)string_Login_AcceptCard;
/** Có lỗi xảy ra trong quá trình đăng nhập qua Zalo.\n Vui lòng thử lại sau. */
+ (NSString *)string_Login_Zalo_Error;
/** Bạn chưa có tài khoản?<a href='https://zalopay.vn/'> <u>Đăng ký</u></a> */
+ (NSString *)string_Login_Description;
/** Tên truy cập hoặc mật khẩu không chính xác.\nLưu ý: Dịch vụ sẽ bị tạm khóa nếu nhập sai mật khẩu quá 5 lần. */
+ (NSString *)string_Login_VCB_Wrong_Input;
/** Nạp Tiền */
+ (NSString *)string_Recharge_Button_Title;
/** Tối thiểu %@ */
+ (NSString *)string_Recharge_Min_Value;
/** Tối thiểu từ %@ và là bội số của %@ */
+ (NSString *)string_Recharge_Min_Multiple_Value;
/** Số tiền phải là bội số của 10.000 VND */
+ (NSString *)string_Recharge_Condition;
/** Xác thực liên kết thẻ */
+ (NSString *)string_MapCard_Message;
/** Huỷ thẻ thành công */
+ (NSString *)string_LinkCard_Remove_Success;
/** Huỷ thẻ thất bại */
+ (NSString *)string_LinkCard_Remove_Error;
/** Bạn có chắc chắn muốn huỷ thẻ không? */
+ (NSString *)string_LinkCard_Confirm_Remove;
/** Ngân hàng */
+ (NSString *)string_LinkCard_ManagaCard;
/** Để liên kết thành công, số điện thoại đăng ký TK %@ phải là %@ */
+ (NSString *)string_LinkCard_Phone_Notice;
/** Ngân hàng đang bảo trì. Vui lòng quay lại sau ít phút */
+ (NSString *)string_LinkCard_Maintain_Message;
/** Đang bảo trì */
+ (NSString *)string_LinkCard_Maintain_Status;
/** Không hỗ trợ */
+ (NSString *)string_LinkCard_NotSupport_Status;
/** Chỉ hỗ trợ thẻ Debit */
+ (NSString *)string_LinkCard_NotSupport_CreditCard;
/** Vui lòng nhập số điện thoại trước khi cập nhật email và CMND/Hộ chiếu. */
+ (NSString *)string_MainProfile_UpdateProfileLevel2Message;
/** Thanh toán đơn hàng */
+ (NSString *)string_PayMerchantBill_Title;
/** Thanh toán */
+ (NSString *)string_PayMerchantBill_Pay_Button;
/** ZaloPay ID để nhận/chuyển khoản cho bạn bè nhanh nhất */
+ (NSString *)string_UpdateAccountName_Description;
/** ZaloPay ID */
+ (NSString *)string_UpdateAccountName_ZaloPayId;
/** Cập nhật ZaloPay ID */
+ (NSString *)string_UpdateAccountName_Title;
/** Cập nhật ZaloPay ID thành công */
+ (NSString *)string_UpdateAccountName_Success_Message;
/** Kiểm Tra */
+ (NSString *)string_UpdateAccountName_Check;
/** Đăng Ký */
+ (NSString *)string_UpdateAccountName_Register;
/** Danh bạ Zalo */
+ (NSString *)string_TransferMoney_Title;
/** GIAO DỊCH GẦN ĐÂY */
+ (NSString *)string_TransferMoney_RecentTransaction;
/** BẠN BÈ SỬ DỤNG ZALOPAY */
+ (NSString *)string_TransferMoney_ZaloPayFriend;
/** BẠN BÈ CHƯA SỬ DỤNG ZALOPAY */
+ (NSString *)string_TransferMoney_Friend;
/** %@ chưa sử dụng ZaloPay. Mời %@ dùng ngay để chuyển tiền miễn phí. */
+ (NSString *)string_TransferMoney_Warning;
/** Thông báo */
+ (NSString *)string_TransferMoney_WarningTitle;
/** ZaloPay ID */
+ (NSString *)string_TransferToAccount_TextPlaceHolder;
/** Không thể chuyển tiền đến tài khoản của chính bạn. */
+ (NSString *)string_TransferToAccount_CurrentUserAccountMessage;
/** Chưa cập nhật */
+ (NSString *)string_TransferHome_EmptyZaloPayId;
/** ZaloPay ID */
+ (NSString *)string_TransferHome_ToZaloPayID;
/** Danh bạ */
+ (NSString *)string_TransferHome_ToZaloPayContact;
/** Số điện thoại */
+ (NSString *)string_TransferHome_ToPhoneNumber;
/** Chuyển tiền qua số điện thoại */
+ (NSString *)string_TransferHome_DescriptionToPhoneNumber;
/** Chuyển tiền cho bạn bè trong danh bạ */
+ (NSString *)string_TransferHome_DescriptionToZaloPayContact;
/** Chuyển tiền qua ZaloPay ID */
+ (NSString *)string_TransferHome_DescriptionToZaloPayID;
/** Chuyển tiền */
+ (NSString *)string_TransferHome_Title;
/** Bạn có thể dùng tính năng Chuyển tiền để trả tiền hoặc gửi tiền cho bạn bè */
+ (NSString *)string_TransferHome_BottomTitle;
/** Số tiền (VND) */
+ (NSString *)string_TransferReceive_AmountPlaceHolder;
/** Lời nhắn */
+ (NSString *)string_TransferReceive_MessagePlaceHolder;
/** Không lấy được thông tin của người này. Vui lòng thử lại sau. */
+ (NSString *)string_TransferReceive_GetInforErrorMessage;
/** Chưa cập nhật */
+ (NSString *)string_TransferReceive_EmptyZaloPayId;
/** ZaloPay ID: %@ */
+ (NSString *)string_TransferReceive_ZaloPayId;
/** Cảnh báo */
+ (NSString *)string_Jailbreak_Warning_Title;
/** Thiết bị đang sử dụng đã bị Jailbreak */
+ (NSString *)string_JailBreak_JailbreakDevice;
/** Sử dụng ZaloPay trên thiết bị này sẽ không đảm bảo an toàn và có thể dẫn đến việc lộ thông tin tài khoản. */
+ (NSString *)string_JailBreak_WarningMessage1;
/** ZaloPay khuyến cáo không nên tiếp tục sử dụng ZaloPay trên thiết bị này. Nhấn nút \"Tiếp Tục\" để sử dụng ZaloPay? */
+ (NSString *)string_JailBreak_WarningMessage2;
/** Không hỏi lại cho những lần sau */
+ (NSString *)string_JailBreak_DoNotAskAgain;
/** ĐĂNG NHẬP VỚI ZALO */
+ (NSString *)string_Intro_Title_1;
/** Không cần đăng ký mới\n Đăng nhập dễ dàng với tài khoản Zalo.\n */
+ (NSString *)string_Intro_Description_1;
/** TỰ ĐẶT ZALOPAY ID */
+ (NSString *)string_Intro_Title_2;
/** ZaloPay ID dùng để chuyển khoản hay nhận tiền giống như tài khoản ngân hàng. */
+ (NSString *)string_Intro_Description_2;
/** NHIỀU DỊCH VỤ TIỆN ÍCH */
+ (NSString *)string_Intro_Title_3;
/** Hỗ trợ thanh toán nhiều dịch vụ thiết yếu như tiền điện, tiền nước, Internet, nạp tiền điện thoại, ... */
+ (NSString *)string_Intro_Description_3;
/** LÌ XÌ */
+ (NSString *)string_Intro_Title_4;
/** Lì xì cho cả nhóm hoặc cá nhân\n Tăng niềm vui bằng cách lì xì số tiền ngẫu nhiên hay bằng nhau. */
+ (NSString *)string_Intro_Description_4;
/** THANH TOÁN TRONG 2 GIÂY */
+ (NSString *)string_Intro_Title_5;
/** Giao dịch siêu nhanh, ổn định, thành công cao. */
+ (NSString *)string_Intro_Description_5;
/** Xóa */
+ (NSString *)string_TransferQRCode_Delete;
/** Số tiền */
+ (NSString *)string_TransferQRCode_Money;
/** Có lỗi khi tạo mã QR */
+ (NSString *)string_TransferQRCode_ErrorMessage;
/** Thiết lập số tiền */
+ (NSString *)string_TransferQRCode_Title;
/** Số tiền (VND) */
+ (NSString *)string_TransferQRCode_AmountPlaceHolder;
/** Lời nhắn */
+ (NSString *)string_TransferQRCode_MessagePlaceHolder;
/** Nhận tiền */
+ (NSString *)string_TransferQRCode_ReceiveMoneTitle;
/** Đang chuyển tiền... */
+ (NSString *)string_TransferQRCode_TransferLoading;
/** Chuyển thành công */
+ (NSString *)string_TransferQRCode_TransferSuccess;
/** Chuyển thất bại */
+ (NSString *)string_TransferQRCode_TransferFail;
/** Hủy chuyển tiền */
+ (NSString *)string_TransferQRCode_TransferCancel;
/** Tổng cộng */
+ (NSString *)string_TransferQRCode_Total;
/** NGÂN HÀNG HỖ TRỢ */
+ (NSString *)string_MapCard_BankSupport;
/** Thêm Thẻ */
+ (NSString *)string_MapCard_AddCard;
/** Thanh toán */
+ (NSString *)string_QRPay_Title;
/** Để sử dụng tính năng này, vui lòng cho phép ZaloPay truy cập camera của bạn. */
+ (NSString *)string_QRPay_AskPermissionMessage;
/** Cho phép */
+ (NSString *)string_QRPay_AskPermissionEnable;
/** Mã QR không hợp lệ */
+ (NSString *)string_QRPay_ErrorTitle;
/** Mã QR không hợp lệ. Vui lòng cung cấp lại mã QR. */
+ (NSString *)string_QRPay_ErrorMessage;
/** Quét mã QR khác */
+ (NSString *)string_QRPay_ScanOrther;
/** Quét mã QR */
+ (NSString *)string_QRPay_ScanToPay;
/** Hướng camera vào mã QR để quét thanh toán */
+ (NSString *)string_QRPay_ScanToPayGuide;
/** Mã thanh toán */
+ (NSString *)string_QRPay_CodePay;
/** Hướng dẫn kết nối mạng */
+ (NSString *)string_InternetConnection_Title;
/** <font size=\"16\" color=\"#24272b\"><b>Bật Wi-Fi</b>: chọn <b>\"Settings\"</b> (cài đặt) - <b>\"Wi-Fi\"</b> và chọn mạng Wi-Fi có sẵn.</font> */
+ (NSString *)string_InternetConnection_Wifi;
/** <font size=\"16\" color=\"#24272b\"><b>Bật 3G</b>: chọn <b>\"Settings\"</b> (cài đặt) - <b>\"Cellular\"</b> (di động) và bật <b>\"Cellular Data\"</b> (dữ liệu di động).</font> */
+ (NSString *)string_InternetConnection_3G;
/** Không có kết nối mạng.\nHãy bật Wi-Fi hoặc 3G. */
+ (NSString *)string_InternetConnection_NoInternetTitle;
/** VUI LÒNG LÀM THEO HƯỚNG DẪN: */
+ (NSString *)string_InternetConnection_Guide;
/** Bạn chưa nhập mã */
+ (NSString *)string_InvitationCode_NoCode;
/** Nhập mã kích hoạt ứng dụng.\nĐể có mã kích hoạt, liên hệ hotro@zalopay.vn */
+ (NSString *)string_InvitationCode_Support;
/** Bạn đã nhập sai mã quá 5 lần. Vui lòng đợi 1 phút để nhập lại mã. */
+ (NSString *)string_InvitationCode_Error;
/** Vui lòng kiểm tra kết nối mạng. */
+ (NSString *)string_NetworkError_NoConnectionMessage;
/** Thời gian kết nối đến máy chủ đã hết. Vui lòng thử lại sau. */
+ (NSString *)string_NetworkError_ConnectionTimeOut;
/** Có lỗi xảy ra. Vui lòng thử lại sau. */
+ (NSString *)string_NetworkError_TryAgainMessage;
/** Mạng kết nối không ổn định. Vui lòng kiểm tra kết nối và thử lại. */
+ (NSString *)string_NetworkError_NoConnectionAndRetry;
/** Máy chủ đang bị lỗi. Vui lòng thử lại sau. */
+ (NSString *)string_ServerError;
/** Thành công */
+ (NSString *)string_Dialog_SuccessTitle;
/** Thất bại */
+ (NSString *)string_Dialog_FailTitle;
/** Phiên bản mới */
+ (NSString *)string_Dialog_NewVersion;
/** Cảnh báo */
+ (NSString *)string_Dialog_Warning;
/** Thông báo */
+ (NSString *)string_Dialog_Notification;
/** Xác nhận */
+ (NSString *)string_Dialog_Confirm;
/** Xong */
+ (NSString *)string_KeyboardManage_Done;
/** Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau. */
+ (NSString *)string_ReactNative_ExceptionMessage;
/** %@ đã quét mã QR */
+ (NSString *)string_QRReceive_BeginScan;
/** %@ gửi tiền thành công */
+ (NSString *)string_QRReceive_SendSuccess;
/** %@ gửi tiền thất bại */
+ (NSString *)string_QRReceive_SendFail;
/** %@ hủy gửi tiền */
+ (NSString *)string_QRReceive_Cancel;
/** Xác Nhận */
+ (NSString *)string_ReceiveInput_Confirm;
/** Email */
+ (NSString *)string_MainProfile_Email;
/** Đang chờ duyệt */
+ (NSString *)string_MainProfile_Waiting;
/** CMND/Hộ chiếu */
+ (NSString *)string_MainProfile_CMND;
/** Số điện thoại */
+ (NSString *)string_MainProfile_Phone;
/** ZaloPay ID */
+ (NSString *)string_MainProfile_ZaloPayId;
/** Ngày sinh */
+ (NSString *)string_MainProfile_BirthDay;
/** Giới tính */
+ (NSString *)string_MainProfile_Gender;
/** Nam */
+ (NSString *)string_MainProfile_Male;
/** Tên thật */
+ (NSString *)string_MainProfile_RealName;
/** Loại tài khoản */
+ (NSString *)string_MainProfile_AccountType;
/** Chưa định danh */
+ (NSString *)string_MainProfile_AccountType_NotVerified;
/** Đã định danh */
+ (NSString *)string_MainProfile_AccountType_Verified;
/** Đang chờ duyệt */
+ (NSString *)string_MainProfile_AccountType_Wating;
/** Thông tin tài khoản */
+ (NSString *)string_MainProfile_AccountInfoTitle;
/** Nữ */
+ (NSString *)string_MainProfile_Female;
/** Mã định danh */
+ (NSString *)string_MainProfile_Id;
/** Chỉnh sửa Ngày sinh và Giới tính tại ứng dụng Zalo */
+ (NSString *)string_MainProfile_Content_BirthDay_Gender;
/** Cập nhật Số điện thoại và ZaloPay ID để thực hiện các giao dịch chuyển tiền, rút tiền và lì xì cho bạn bè */
+ (NSString *)string_MainProfile_Content_Phone_ID;
/** Cập nhật Email và CMND/Hộ chiếu để tăng hạn mức tối đa các giao dịch */
+ (NSString *)string_MainProfile_Content_Email_CMND;
/** Nhập thông tin bên dưới để giao dịch và thanh toán được an toàn */
+ (NSString *)string_MainProfile_Update_Address_Note;
/** Tôi đã đọc và đồng ý với %@ của ZaloPay */
+ (NSString *)string_MainProfile_Update_Address_Description;
/** thoả thuận sử dụng */
+ (NSString *)string_MainProfile_Update_Address_Term;
/** Địa chỉ */
+ (NSString *)string_MainProfile_Update_Address_Title;
/** Địa chỉ không được vượt quá 160 ký tự. */
+ (NSString *)string_MainProfile_Update_Address_Error_Maximum;
/** Xác thực bằng vân tay thay vì sử dụng mật khẩu thanh toán.\nLưu ý: Bất cứ vân tay nào đã đăng ký trong hệ thống đều xác thực được khi thanh toán. */
+ (NSString *)string_Setting_TouchId_Description;
/** Sử dụng TouchID */
+ (NSString *)string_Setting_TouchId_Title;
/** Xác thực bằng Face ID thay vì sử dụng mật khẩu thanh toán. Lưu ý: Bất cứ ai có khuôn mặt giống bạn đều có nguy cơ xác thực được khi thanh toán. */
+ (NSString *)string_Setting_FaceId_Description;
/** Sử dụng Face ID */
+ (NSString *)string_Setting_FaceId_Title;
/** Hỏi mật khẩu hoặc xác thực vân tay mỗi khi truy xuất danh sách liên kết Ngân Hàng */
+ (NSString *)string_Setting_Ask_Password_Description;
/** Bảo vệ thông tin cá nhân */
+ (NSString *)string_Setting_Ask_Password_Title;
/** Mật khẩu dùng để xác thực mỗi khi thực hiện thanh toán */
+ (NSString *)string_Setting_Update_Password_Description;
/** Đổi mật khẩu */
+ (NSString *)string_Setting_Update_Password_Title;
/** Thiết bị của bạn không hỗ trợ TouchID */
+ (NSString *)string_Setting_TouchID_NotAvailable;
/** Liệt kê các thiết bị hiện tại đang thực hiện đăng nhập bằng tài khoản ZaloPay này */
+ (NSString *)string_Setting_Manage_Login_Device_Description;
/** Danh sách thiết bị đăng nhập */
+ (NSString *)string_Setting_Manage_Login_Device_Title;
/** Yêu cầu xác thực khi mở ứng dụng */
+ (NSString *)string_Setting_Authentication_Open_Title;
/** Yêu cầu xác thực bằng mật khẩu hoặc vân tay đẻ mở ứng dụng. Khuyến cáo bật tùy chọn này để tăng tính bảo mật. */
+ (NSString *)string_Setting_Authentication_Open_Description;
/** NHẬP MẬT KHẨU XÁC THỰC */
+ (NSString *)string_Setting_Authentication_Enter_Password;
/** Lưu ý: Mật khẩu này không phải mã PIN thẻ/tài khoản ngân hàng. */
+ (NSString *)string_Setting_Authentication_Enter_Password_Description;
/** Quên mật khẩu? */
+ (NSString *)string_Setting_Authentication_Forget_Password;
/** Để đặt lại mật khẩu, vui lòng liên hệ Hotline 1900 54 54 36. Bạn có muốn tiếp tục không? */
+ (NSString *)string_Setting_Authentication_Forget_Password_Content;
/** Chạm phím Home để truy cập */
+ (NSString *)string_Setting_Authentication_Login_By_TouchID;
/** Lớp phủ màn hình đa nhiệm */
+ (NSString *)string_Setting_Splash_Screen_Title;
/** Khi tùy chọn này được bật, người khác sẽ không thể thấy được nội dung hiển thị trên màn hình của ứng dụng của ZaloPay. Khuyến cáo bật tùy chọn này để tăng tính bảo mật. */
+ (NSString *)string_Setting_Splash_Screen_Description;
/** Tùy chỉnh thời gian tự động khóa */
+ (NSString *)string_Setting_Auto_Lock_Title;
/** Ngay sau khi ẩn ứng dụng */
+ (NSString *)string_Setting_Auto_Lock_Description;
/** Thời gian */
+ (NSString *)string_Setting_Auto_Lock_Menu_Title;
/** Đóng */
+ (NSString *)string_Setting_Auto_Lock_Cancel_Title;
/** Ngay lập tức */
+ (NSString *)string_Setting_Auto_Lock_Right_Now_Title;
/** %@ giây */
+ (NSString *)string_Setting_Auto_Lock_After_Second_Title;
/** %@ phút */
+ (NSString *)string_Setting_Auto_Lock_After_Minute_Title;
/** Chạm nút Home để xác thực */
+ (NSString *)string_TouchID_Message;
/** Nhập mật khẩu để xác thực */
+ (NSString *)string_TouchID_Password_Message;
/** Nhập mật khẩu */
+ (NSString *)string_TouchID_Input_Password;
/** Nhập mật khẩu thanh toán để xác nhận */
+ (NSString *)string_TouchID_Authen_Message;
/** Chạm phím Home để xác nhận thanh toán */
+ (NSString *)string_TouchID_Confirm_Pay_Message;
/** Nhập mật khẩu để xác nhận */
+ (NSString *)string_TouchID_Confirm_Pay_Password_Message;
/** Hỏi mật khẩu mỗi khi truy xuất danh sách liên kết Ngân Hàng */
+ (NSString *)string_Setting_Ask_Password_Description_WithOutTouchID;
/** Hỏi mật khẩu hoặc xác thực bằng Face ID mỗi khi truy xuất danh sách liên kết Ngân Hàng */
+ (NSString *)string_Setting_Ask_Password_FaceID_Description;
/** Bạn có muốn tắt thiết lập bảo vệ bằng vân tay */
+ (NSString *)string_Setting_TouchID_RemoveTouchID;
/** Vui lòng nhập mật khẩu thanh toán để thiết lập */
+ (NSString *)string_Setting_Remove_Ask_Password;
/** Bạn chưa có bạn bè trên Zalo. */
+ (NSString *)string_ZaloFriend_Empty_Title;
/** Phân loại */
+ (NSString *)string_Feedback_Categorys;
/** Rút Tiền */
+ (NSString *)string_Feedback_Withdraw;
/** Thông tin cần hỗ trợ (bắt buộc) */
+ (NSString *)string_Feedback_Description;
/** Mô tả (bắt buộc) */
+ (NSString *)string_Feedback_Error_Description_Require;
/** Mô tả */
+ (NSString *)string_Feedback_Error_Description;
/** Email */
+ (NSString *)string_Feedback_Email;
/** Email (bắt buộc) */
+ (NSString *)string_Feedback_Email_Require;
/** Gửi kèm thông tin người dùng */
+ (NSString *)string_Feedback_UserInfo;
/** Gửi kèm thông tin thiết bị */
+ (NSString *)string_Feedback_DeviceInfo;
/** Gửi kèm thông tin ứng dụng */
+ (NSString *)string_Feedback_AppInfo;
/** Vui lòng nhập thông tin để gửi đến bộ phận CSKH */
+ (NSString *)string_Feedback_Contact_CSKH;
/** Đính kèm màn hình (%d/4) */
+ (NSString *)string_Feedback_ScreenShoot;
/** Yêu cầu hỗ trợ */
+ (NSString *)string_Feedback_Title;
/** CHỌN LOẠI BẠN CẦN HỖ TRỢ */
+ (NSString *)string_Feedback_Select_Category;
/** Bạn chưa nhập thông tin cần hỗ trợ. */
+ (NSString *)string_Feedback_Description_Empty;
/** Thiết bị của bạn không hỗ trợ gửi email. */
+ (NSString *)string_Feedback_Email_NotAvailable;
/** Phản hồi/Góp ý ZaloPay */
+ (NSString *)string_Feedback_Email_Title;
/** Mã giao dịch */
+ (NSString *)string_Feedback_TransactionId;
/** hotro@zalopay.vn */
+ (NSString *)string_Feedback_Email_Receiver;
/** Mô tả không được vượt quá %d kí tự */
+ (NSString *)string_Feedback_Message_InvalidLength;
/** Gửi báo lỗi */
+ (NSString *)string_Feedback_Error_Title;
/** Tính năng Rút Tiền đang được bảo trì. Thời gian bảo trì từ %@ ngày %@ đến %@ ngày %@. */
+ (NSString *)string_Withdraw_Maintain_Message;
/** Bạn chưa liên kết thẻ */
+ (NSString *)string_LinkCard_NoCard_Title;
/** Liên kết thẻ để thanh toán các dịch vụ được dễ dàng và nhanh chóng. */
+ (NSString *)string_LinkCard_NoCard_Message;
/**    Liên Kết Thẻ */
+ (NSString *)string_LinkCard_NoCard_AddCard;
/** Bạn chưa liên kết tài khoản */
+ (NSString *)string_LinkAccount_NoAccount_Title;
/** Liên kết tài khoản ngân hàng để thanh toán các dịch vụ được dễ dàng và nhanh chóng. */
+ (NSString *)string_LinkAccount_NoAccount_Message;
/**    Liên Kết Tài Khoản */
+ (NSString *)string_LinkAccount_NoAccount_AddAcount;
/** Chọn ngân hàng để liên kết tài khoản: */
+ (NSString *)string_LinkAccount_Select_Bank_To_Link_Account;
/** Điện thoại liên kết */
+ (NSString *)string_LinkAccount_Phone;
/** Để thực hiện %@ với %@, vui lòng cập nhật phiên bản ZaloPay mới nhất. */
+ (NSString *)string_LinkAccount_MinVerison;
/** Cần liên kết một trong các tài khoản: */
+ (NSString *)string_WithdrawCondition_LinkAccountTitle;
/** Bạn đang đăng nhập tài khoản %@. Bạn có muốn đăng nhập lại ngay không? */
+ (NSString *)string_Zalo_Scheme_SwitchAccountMessage;
/** Bạn này chưa sử dụng ZaloPay. Hãy mời bạn sử dụng ZaloPay để tiếp tục chuyển tiền. */
+ (NSString *)string_Zalo_Scheme_FriendNotUsingZaloPay;
/** %@ chưa đăng ký ZaloPay nên không thể nhận lì xì, vui lòng báo %@ đăng ký ZaloPay và thử lại sau. */
+ (NSString *)string_Zalo_Scheme_Lixi_FriendNotUsingZaloPay;
/** Người nhận */
+ (NSString *)string_Zalo_Scheme_Lixi_Default_DisplayName;
/** Đăng nhập */
+ (NSString *)string_Zalo_Scheme_SwitchAccount;
/** Không hỏi lại gợi ý này */
+ (NSString *)string_Suggest_TouchID_DoNotAskAgain;
/** Gợi ý sử dụng TouchID */
+ (NSString *)string_Suggest_TouchID_Title;
/** Bạn có muốn sử dụng TouchID thay vì nhập mật khẩu thanh toán */
+ (NSString *)string_Suggest_TouchID_Message;
/** Bạn có muốn sử dụng xác thực bằng vân tay cho những lần thanh toán sau không? */
+ (NSString *)string_Confirm_TouchId_Payment_Later;
/** Bạn có muốn sử dụng xác thực bằng khuôn mặt cho những lần thanh toán sau không? */
+ (NSString *)string_Confirm_FaceId_Payment_Later;
/** Sử dụng vân tay cho lần thanh toán sau */
+ (NSString *)string_Use_TouchId_Payment_Later;
/** Bạn đang có một giao dịch đang trong quá trình thanh toán. Vui lòng kết thúc thanh toán cho giao dịch đó và thử lại */
+ (NSString *)string_Zalo_Scheme_PayingBill;
/** Tài khoản (%@) đang bị khóa. Vui lòng liên hệ hỗ trợ khách hàng 1900 54 54 36 để biết thêm chi tiết */
+ (NSString *)string_Message_Lock_Account_Format;
/** NGÂN HÀNG HỖ TRỢ */
+ (NSString *)string_LinkBank_BankSupport;
/** Các thẻ ngân hàng được ZaloPay hỗ trợ */
+ (NSString *)string_BankCard_Support_ByZaloPay;
/**  Ngân hàng hỗ trợ liên kết thẻ */
+ (NSString *)string_Bank_Support_LinkCard;
/** Được cung cấp bởi %@ */
+ (NSString *)string_WebApp_BottomSheet_SupportFor;
/** Chia sẻ với Zalo */
+ (NSString *)string_WebApp_BottomSheet_ShareTo_Zalo;
/** Sao chép URL */
+ (NSString *)string_WebApp_BottomSheet_Copy_URL;
/** Làm mới */
+ (NSString *)string_WebApp_BottomSheet_Refresh;
/** Mở trong trình duyệt */
+ (NSString *)string_WebApp_BottomSheet_Open_Browser;
/** Quý khách đã liên kết với ngân hàng %@ */
+ (NSString *)string_LinkBank_BankExist;
/** Không thể liên kết */
+ (NSString *)string_LinkBank_BankExist_Title;
/** và số dư tối thiểu trong TK %@ là %@ VND */
+ (NSString *)string_LinkBank_Minimum_Require;
/** Nhập mật khẩu để xác nhận hủy liên kết */
+ (NSString *)string_LinkBank_Message_Input_Password;
/** <font color='#24272b'>Để liên kết thành công, bạn cần đăng ký sử dụng dịch vụ BIDV Online / BIDV Smartbanking. \n\nNếu chưa đăng ký dịch vụ, vui lòng đăng ký tại quầy giao dịch BIDV hoặc <a href='https://ebank.bidv.com.vn/DKNHDT/dk_bidvonline.htm' style='text-decoration:underline;'>tại đây</a>.</font> */
+ (NSString *)string_LinkBank_BIDV_Notify_Message;
/** Tôi đã đăng ký */
+ (NSString *)string_LinkBank_BIDV_Accept_Title_Button;
/** Ngân hàng không tồn tại */
+ (NSString *)string_BankName_Not_Exist;
/** Nhập từ khoá để tìm kiếm ứng dụng */
+ (NSString *)string_Search_Suggest_Find_Keyword;
/** ỨNG DỤNG TIÊU BIỂU */
+ (NSString *)string_Search_Suggest_App_Title;
/** ỨNG DỤNG */
+ (NSString *)string_Search_Result_App_Tille;
/** Vui lòng kiểm tra lại chính tả hoặc sử dụng các từ khoá tổng quát hơn */
+ (NSString *)string_Search_No_Result_Description;
/** Không tìm thấy kết quả cho từ khoá \"%@\" */
+ (NSString *)string_Search_No_Result;
/** Xem tất cả */
+ (NSString *)string_Search_Read_All_Result;
/** Tìm kiếm */
+ (NSString *)string_Search_Field_PlaceHolder;
/** Trang Chủ */
+ (NSString *)string_Tabbar_Home;
/** Quanh Đây */
+ (NSString *)string_Tabbar_ShowShow;
/** Giao Dịch */
+ (NSString *)string_Tabbar_Transaction;
/** Cá Nhân */
+ (NSString *)string_Tabbar_Profile;
/** Ưu Đãi */
+ (NSString *)string_Tabbar_Promotion;
/** Vui lòng cập nhật phiên bản ZaloPay mới nhất để quét QR. */
+ (NSString *)string_QRPay_ErrorUnsupport;
/** TK ZaloPay */
+ (NSString *)string_Profile_Balance;
/** Ngân Hàng */
+ (NSString *)string_Profile_Bank;
/** Hiện tại ZaloPay chỉ hỗ trợ liên kết tài khoản Vietcombank. Liên kết tài khoản các ngân hàng khác sẽ được hỗ trợ trong thời gian tới. */
+ (NSString *)string_LinkBank_Only_Support_Vietcombank;
/** Bạn cần phải đăng nhập tài khoản %@ để huỷ liên kết */
+ (NSString *)string_LinkBank_Delete_Notification_Message;
/** Phiên bản đang chạy chưa được hỗ trợ. Vui lòng liên hệ bộ phận CSKH. */
+ (NSString *)string_Http_Error_404;
/** Không tải được nội dung */
+ (NSString *)string_FetchTransactionLogsFail;
/** Bạn cần bật cài đặt vân tay và mật khẩu cho máy */
+ (NSString *)string_TouchID_Enable_System_Setting;
/** Cài Đặt */
+ (NSString *)string_ButtonLabel_Setting;
/** Hiện */
+ (NSString *)string_ButtonLabel_Show;
/** Ẩn */
+ (NSString *)string_ButtonLabel_Hide;
/** Dịch vụ */
+ (NSString *)string_Pay_Bill_Merchant_Name;
/** Phí giao dịch */
+ (NSString *)string_Pay_Bill_Fee_Title;
/** Người nhận */
+ (NSString *)string_Pay_Bill_Receiver;
/** Miễn phí */
+ (NSString *)string_Pay_Bill_Fee_Free;
/** Kích hoạt vân tay thành công */
+ (NSString *)string_TouchID_Enable_Success_Message;
/** PHƯƠNG THỨC THANH TOÁN KHÁC */
+ (NSString *)string_Payment_Method_Orther_Method;
/** TẠO MẬT KHẨU THANH TOÁN */
+ (NSString *)string_Onboard_Create_Password_Title;
/** Số điện thoại */
+ (NSString *)string_Onboard_KYC_Phone;
/** Số điện thoại */
+ (NSString *)string_Onboard_KYC_Hint_Phone;
/** Bạn chưa nhập Số điện thoại */
+ (NSString *)string_Onboard_KYC_Empty_Phone;
/** Số điện thoại không hợp lệ */
+ (NSString *)string_Onboard_KYC_Invalid_Phone;
/** Họ và tên */
+ (NSString *)string_Onboard_KYC_Name;
/** Họ tên thật, có dấu, viết cách */
+ (NSString *)string_Onboard_KYC_Hint_Name;
/** Bạn chưa nhập Họ tên */
+ (NSString *)string_Onboard_KYC_Empty_Name;
/** Họ tên không hợp lệ */
+ (NSString *)string_Onboard_KYC_Invalid_Name;
/** Ngày sinh */
+ (NSString *)string_Onboard_KYC_DOB;
/** Ngày sinh */
+ (NSString *)string_Onboard_KYC_Hint_DOB;
/** Bạn cần trên 15 tuổi */
+ (NSString *)string_Onboard_KYC_Invalid_DOB;
/** Giới tính */
+ (NSString *)string_Onboard_KYC_Gender;
/** Số CMND */
+ (NSString *)string_Onboard_KYC_Identify;
/** Số CMND */
+ (NSString *)string_Onboard_KYC_Hint_Identify;
/** Số %@ */
+ (NSString *)string_Onboard_KYC_Number_Identify;
/** Bạn chưa nhập %@ */
+ (NSString *)string_Onboard_KYC_Empty_Identify;
/** %@ không hợp lệ */
+ (NSString *)string_Onboard_KYC_Invalid_Identify;
/** CMND có 9 hoặc 12 ký tự */
+ (NSString *)string_Onboard_KYC_Rule_Identify;
/** CMND */
+ (NSString *)string_Onboard_KYC_CMND_Identify;
/** Passport */
+ (NSString *)string_Onboard_KYC_Passport_Identify;
/** Căn cước */
+ (NSString *)string_Onboard_KYC_CC_Identify;
/** Số điện thoại đăng ký phải trùng với số Zalo */
+ (NSString *)string_Onboard_KYC_Phone_Match_ZaloPhone_Note;
/** Thông tin cá nhân đăng ký với ngân hàng */
+ (NSString *)string_Onboard_KYC_Title_Mapcard;
/** Nhập đúng thông tin bạn đã đăng ký với ngân hàng để liên kết thẻ thành công. */
+ (NSString *)string_Onboard_KYC_Message_Mapcard;
/** Để bảo mật tài khoản, vui lòng cung cấp thông tin cá nhân của chính bạn. */
+ (NSString *)string_Onboard_KYC_Message_Onboarding;
/** Nhập đúng thông tin thật của bạn để tiếp tục thực hiện giao dịch */
+ (NSString *)string_Onboard_KYC_Message_Transaction;
/** Cung cấp thông tin thật của bạn để định danh tài khoản */
+ (NSString *)string_Onboard_KYC_Message_Profile;
/** Mật khẩu để bảo mật tài khoản và xác nhận giao dịch khi thanh toán */
+ (NSString *)string_Onboard_KYC_Create_Password_Note;
/** Lưu ý: Mật khẩu này không phải mã PIN thẻ/tài khoản ngân hàng */
+ (NSString *)string_Onboard_KYC_Create_Password_Footer;
/** Mã xác thực đã gửi đến %@ */
+ (NSString *)string_Onboard_KYC_OTP_Send_To_Phone;
/** XÁC NHẬN MẬT KHẨU */
+ (NSString *)string_Onboard_Confirm_Password_Title;
/** Nhập lại mật khẩu thanh toán */
+ (NSString *)string_Onboard_KYC_Confirm_Password_Note;
/** Để xác nhận giao dịch của bạn khi thanh toán */
+ (NSString *)string_Onboard_Create_Password_Note;
/** Vui lòng không chia sẻ mật khẩu với bất kì ai */
+ (NSString *)string_Onboard_Confirm_Password_Note;
/** Mật khẩu thanh toán không trùng khớp */
+ (NSString *)string_Onboard_Confirm_Password_Error;
/** SỐ ĐIỆN THOẠI DI ĐỘNG */
+ (NSString *)string_Onboard_Input_Phone_Title;
/** Đăng ký một lần và không thay đổi */
+ (NSString *)string_Onboard_Input_Phone_Note;
/** Số điện thoại không hợp lệ */
+ (NSString *)string_Onboard_Input_Phone_Error;
/** Số điện thoại hợp lệ */
+ (NSString *)string_Onboard_Input_Phone_Correct;
/** XÁC THỰC SỐ ĐIỆN THOẠI */
+ (NSString *)string_Onboard_Input_OTP_Title;
/** Mã xác thực đã gửi đến %@ */
+ (NSString *)string_Onboard_Input_OTP_Note;
/** Xác nhận */
+ (NSString *)string_Onboard_Input_OTP_Next_Title;
/** %@ Mã xác thực khác */
+ (NSString *)string_Onboard_Input_OTP_Refresh;
/** Gửi lại mã trong %02ld:%02ld */
+ (NSString *)string_Onboard_Input_OTP_Refresh_Counting;
/** Bạn đã nhập sai mã xác thực quá số lần quy định. Vui lòng đăng nhập lại để tiếp tục. */
+ (NSString *)string_Onboard_Input_OTP_Over_Limit;
/** Mã xác thực không hợp lệ */
+ (NSString *)string_Onboard_Input_OTP_Error;
/** Mã xác thực đang được gửi đến %@. Bạn có muốn tiếp tục chờ không? */
+ (NSString *)string_Onboard_Input_OTP_Dialog_Message;
/** Đã gửi lại mã xác thực */
+ (NSString *)string_Onboard_Input_OTP_Resend_Success;
/** Tiếp tục */
+ (NSString *)string_Onboard_Input_Phone_Next;
/** Xác Nhận */
+ (NSString *)string_Onboard_Input_OTP_Confirm;
/** Để tiếp tục thực hiện giao dịch, hãy cung cấp thông tin của chính bạn. */
+ (NSString *)string_KYC_Alert;
/** VUI LÒNG CHỌN SỐ TIỀN CẦN RÚT */
+ (NSString *)string_Withdraw_MoneyNumber_Title;
/** Miễn phí rút tiền */
+ (NSString *)string_Withdraw_Free_Charge_Title;
/** Số dư TK ZaloPay */
+ (NSString *)string_Withdraw_Balance;
/** Cá nhân */
+ (NSString *)string_Profile_Title;
/** Người nhận */
+ (NSString *)string_Receiver;
/** Số dư không đủ */
+ (NSString *)string_Withdraw_Not_Enough_Money_Info;
/** Không có kênh nào hỗ trợ mệnh giá này */
+ (NSString *)string_Payment_Method_No_Method_Support;
/** Thời gian */
+ (NSString *)string_Time_Title;
/** Đang được xử lý */
+ (NSString *)string_Result_Title_Processing;
/** Mạng không ổn định */
+ (NSString *)string_Result_Title_Network_Problem;
/** Ngân hàng %@ chỉ hỗ trợ thanh toán số tiền từ %@ đến %@. Vui lòng thử lại. */
+ (NSString *)string_Atm_Map_Card_Min_Max_Message;
/** Nhập mật khẩu để chuyển tiền bằng */
+ (NSString *)string_TransferMoney_Password_Title;
/** Nhập mật khẩu để nạp tiền bằng */
+ (NSString *)string_Recharge_Password_Title;
/** Nhập mật khẩu để rút tiền bằng */
+ (NSString *)string_Withdraw_Password_Title;
/** Thanh toán đơn hàng */
+ (NSString *)string_App_DichVu;
/** Đổi mật khẩu thành công */
+ (NSString *)string_Change_Password_Successful;
/** Nhập mật khẩu hiện tại */
+ (NSString *)string_Change_Password_Input_Current;
/** Nhập mật khẩu mới */
+ (NSString *)string_Change_Password_Input_New;
/** Xác nhận mật khẩu mới */
+ (NSString *)string_Change_Password_Confirm;
/** Nhập mã OTP */
+ (NSString *)string_Change_Password_Input_OTP;
/** Mật khẩu không trùng khớp */
+ (NSString *)string_Change_Password_Input_Wrong;
/** Quên mật khẩu vui lòng gọi  */
+ (NSString *)string_Change_Password_Call_Support;
/** 1900 54 54 36 */
+ (NSString *)string_Change_Password_Phone_Support;
/** Bạn có muốn tiếp tục đổi mật khẩu không? */
+ (NSString *)string_Change_Password_Cancel_Message;
/** Số khác */
+ (NSString *)string_Withdraw_Other_Number;
/** Rút Tiền */
+ (NSString *)string_Withdraw_Button_Title;
/** <font color='#000000'>Số dư không đủ, bạn cần <a href='deposit'>Nạp thêm</a></font> */
+ (NSString *)string_Method_Handler_AllMethodUnSupportCurrentBill;
/** Thêm liên kết */
+ (NSString *)string_Method_AddNewCardButton;
/** YÊU THÍCH */
+ (NSString *)string_ZPC_favorite_friend;
/** Chọn vào ngôi sao tại tên bạn bè để thêm người đó vào danh sách Yêu thích */
+ (NSString *)string_ZPC_guide_add_favorite;
/** Chưa có bạn bè yêu thích */
+ (NSString *)string_ZPC_empty_favorite;
/** Danh sách bạn bè */
+ (NSString *)string_ZPC_zalo_contact;
/** Sửa */
+ (NSString *)string_ZPC_edit_text;
/** Nhập tên hoặc số điện thoại */
+ (NSString *)string_ZPC_guide_search;
/** Ưa thích */
+ (NSString *)string_ZPC_favorite_text;
/** Cập nhật danh bạ */
+ (NSString *)string_ZPC_update_contact_text;
/** Bạn bè từ danh bạ điện thoại và danh bạ Zalo sẽ tự động thêm vào danh bạ ZaloPay */
+ (NSString *)string_ZPC_update_contact_description;
/** Lần cập nhật cuối */
+ (NSString *)string_ZPC_last_update_text;
/** Bỏ cập nhật danh bạ ? */
+ (NSString *)string_ZPC_inorge_update_contact_warning;
/** Bỏ cập nhật, danh bạ điện thoại sẽ không cập nhật vào danh bạ ZaloPay. Bạn muốn tiếp tục cập nhật không? */
+ (NSString *)string_ZPC_inorge_update_contact_description;
/** Bạn bè từ danh bạ điện thoại */
+ (NSString *)string_ZPC_friend_from_contact;
/** Bạn bè từ danh bạ zalo */
+ (NSString *)string_ZPC_friend_from_zalo;
/** Chưa cập nhật danh bạ */
+ (NSString *)string_ZPC_update_contact_miss;
/** đã bị xoá khỏi danh sách Yêu thích */
+ (NSString *)string_ZPC_remove_friend_from_favorite;
/** đã được thêm vào danh sách Yêu thích */
+ (NSString *)string_ZPC_add_friend_into_favorite;
/** Danh sách yêu thích đã đầy */
+ (NSString *)string_ZPC_max_favorite_text;
/** Tối đa chỉ chọn được %d người */
+ (NSString *)string_ZPC_max_red_packet_text;
/** Bạn có muốn xóa người này khỏi danh sách người nhận lì xì không? */
+ (NSString *)string_ZPC_confirm_remove_red_packet;
/** Không tìm thấy kết quả phù hợp\nVui lòng nhập tên khác hoặc số điện thoại hợp lệ */
+ (NSString *)string_ZPC_Search_Empty;
/** Đã cập nhật danh bạ */
+ (NSString *)string_ZPC_update_contact_done;
/** Số của tôi */
+ (NSString *)string_ZPC_my_phone_number;
/** bạn */
+ (NSString *)string_ZPC_number_contacts;
/** Bạn muốn bỏ sử dụng khuyến mãi? */
+ (NSString *)string_Voucher_message_alert;
/** Sử dụng mã KM */
+ (NSString *)string_Voucher_button_next;
/** Nhập mã khuyến mãi */
+ (NSString *)string_Voucher_input_title;
/** Mã khuyến mãi */
+ (NSString *)string_Voucher_title_view;
/** Nhập */
+ (NSString *)string_Voucher_button_input;
/** Giá ban đầu */
+ (NSString *)string_Voucher_before_input_title;
/** Bạn muốn ngưng sử dụng mã khuyến mãi? */
+ (NSString *)string_Voucher_delete_message;
/** Còn %ld ngày */
+ (NSString *)string_Voucher_expiredate;
/** giảm %ld */
+ (NSString *)string_Voucher_discount_amount;
/** xem tất cả */
+ (NSString *)string_Voucher_view_all;
/** * Chỉ hiển thị Quà tặng phù hợp, xem tất cả */
+ (NSString *)string_Voucher_view_more;
/** Bạn có Ưu đãi */
+ (NSString *)string_Voucher_have_gift_text;
/** Chọn */
+ (NSString *)string_Voucher_select_text;
/** Chọn Ưu đãi */
+ (NSString *)string_Voucher_select_gift_text;
/** Ưu đãi */
+ (NSString *)string_Voucher_gift_text;
/** Vui lòng kiểm tra số dư theo cú pháp *101# */
+ (NSString *)string_Topup_phonev2_message;
/** ZaloPay ID */
+ (NSString *)string_TransferMoney_Ext_ZaloPayId;
/** Tài khoản %@ đang bị khoá */
+ (NSString *)string_TransferMoney_Account_Locked;
/** Số ĐT */
+ (NSString *)string_TransferMoney_Phone_Number;
/** chưa cập nhật */
+ (NSString *)string_TransferMoney_Phone_None;
/** %@ chưa có số điện thoại. Vui lòng chọn bạn khác để tiếp tục. */
+ (NSString *)string_Topup_No_Phone;
/** *** %@ */
+ (NSString *)string_TransferMoney_ZaloAccount_PhoneNumber;
/** Đăng ký thanh toán trực tuyến */
+ (NSString *)string_ViettinBank_Register_Online_Title;
/** Số điện thoại hoặc CMND chưa trùng khớp */
+ (NSString *)string_ViettinBank_Register_Online_Error_Message;
/** Số CMND/Hộ chiếu */
+ (NSString *)string_ViettinBank_Register_Online_Title_TextField;
/** Lưu ý: Số điện thoại %@ & số CMND/Hộ chiếu phải trùng với thông tin đã đăng ký với ngân hàng. */
+ (NSString *)string_ViettinBank_Register_Online_Notice;
/** Bạn có chắc muốn thoát Đăng ký thanh toán trực tuyến? */
+ (NSString *)string_ViettinBank_Register_Online_Alert_Message;
/** Nhập sai OTP 3 lần */
+ (NSString *)string_ViettinBank_Register_Online_Wrong_OTP;
/** tel://1900558868 */
+ (NSString *)string_ViettinBank_Register_Online_Phone;
/** Bạn có muốn huỷ nhập OTP không? */
+ (NSString *)string_ViettinBank_Register_Online_Cancel_Input_OTP;
/** Bấm nút ĐĂNG KÝ để sao chép cú pháp %@ */
+ (NSString *)string_Sacombank_Register_Online_Step1;
/** Gửi SMS đến %@ (phí %@VND/sms) */
+ (NSString *)string_Sacombank_Register_Online_Step2;
/** Bạn đang có 1 giao dịch đang thực hiện. Vui lòng hãy thực hiện xong giao dịch hiện tại. */
+ (NSString *)string_AppToApp_Popup_When_IsBilling;
/** Nội dung tiếp theo là do %@ cung cấp. Nếu bạn thấy nội dung vi phạm hay không phù hợp, hãy liên lạc với bộ phận chăm sóc khách hàng của ZaloPay để thông báo. Bạn có muốn tiếp tục không? */
+ (NSString *)string_Dialog_Disclaimer_Merchant_App;
/** Số chưa lưu */
+ (NSString *)string_ZPC_Unsave_Phone_Number;
/** Hoặc liên hệ bộ phận CSKH để được hỗ trợ\n1900 54 54 36  */
+ (NSString *)string_Clearing_call_CS_text;
/** Sau khi đăng nhập, vui lòng thanh lý toàn bộ số dư và huỷ liên kết ngân hàng. */
+ (NSString *)string_Clearing_login_text;
/** Tài khoản đang bị khóa, vui lòng liên hệ bộ phận CSKH để được hỗ trợ 1900545436. */
+ (NSString *)string_Clearing_account_locked;
/** Hết thời gian thanh lý, vui lòng liên hệ bộ phận CSKH để được hỗ trợ 1900545436. */
+ (NSString *)string_Clearing_overtime;
/** Có lỗi trong quá trình đăng nhập, vui lòng thử lại sau. */
+ (NSString *)string_Clearing_login_undefine_error;
/** NHẬP MẬT KHẨU THANH TOÁN */
+ (NSString *)string_Clearing_input_pin;
/** Sai mật khẩu thanh toán quá số lần quy định, vui lòng thử lại sau 15 phút. */
+ (NSString *)string_Clearing_over_wrong_pin;
/** Sai mã xác thực quá số lần quy định, vui lòng thử lại sau 15 phút. */
+ (NSString *)string_Clearing_over_wrong_otp;
/** Đổi mã xác thực quá số lần quy định, vui lòng thử lại sau 15 phút. */
+ (NSString *)string_Clearing_over_change_otp;
/** Số dư sau khi rút cần >  */
+ (NSString *)string_Clearing_withdraw_faild_min;
/** Số dư sau khi chuyển cần >  */
+ (NSString *)string_Clearing_transfer_faild_min;
/** NHẬP MÃ XÁC THỰC TÀI KHOẢN */
+ (NSString *)string_Clearing_verify_OTP;
/** Chưa huỷ liên kết ngân hàng. */
+ (NSString *)string_Clearing_reject_remove_card;
/** Người dùng huỷ bỏ giao dịch. */
+ (NSString *)string_Clearing_reject_transaction;
/** <font color='#000000'>Số dư không đủ, vui lòng <a href='transfer'>Chuyển Tiền</a></font> */
+ (NSString *)string_Clearing_not_enough_money_withdraw;
/** Tôi đã hiểu */
+ (NSString *)string_Disclaimer_Accept_Button_Title;
/** Dịch vụ đang được bảo trì. Quý khách vui lòng quay lại sau. */
+ (NSString *)string_Voucher_server_maintenance;
/** Bạn chưa có quà tặng. */
+ (NSString *)string_Voucher_empty;
/** Gửi lời mời */
+ (NSString *)string_InvitationZP_send;
/** Tài khoản %@ đang không khả dụng. Xin vui lòng quay lại sau. */
+ (NSString *)string_Account_Unavailable;
/** %@ chưa sử dụng ZaloPay.\nBạn hãy mời %@ sử dụng ZaloPay để chuyển tiền nhanh chóng và dễ dàng. */
+ (NSString *)string_TransferMoney_Warning_Not_Invite;
/** Có lỗi trong quá trình tải dữ liệu,\n Vui lòng quay trở lại sau */
+ (NSString *)string_Download_Resources_Error;
/** Thanh toán */
+ (NSString *)string_QuickPay_Title;
/** Thanh toán thành công */
+ (NSString *)string_QuickPay_Result_Success;
/** Thanh toán thất bại */
+ (NSString *)string_QuickPay_Result_Fail;
/** Thông tin cài đặt */
+ (NSString *)string_QuickPay_Setting_Title;
/** Thông tin cài đặt */
+ (NSString *)string_QuickPay_Setting;
/** Đổi mã khác */
+ (NSString *)string_QuickPay_ChangePaymentCode;
/** Hướng dẫn sử dụng */
+ (NSString *)string_QuickPay_Support;
/** Mất kết nối mạng! \n Bạn vẫn có thể thanh toán. */
+ (NSString *)string_QuickPay_Disconnect_TopView;
/** Nhập mật khẩu để kích hoạt tính năng */
+ (NSString *)string_QuickPay_Input_Password_Function;
/** Đổi mã khác thành công. */
+ (NSString *)string_QuickPay_ChangePaymentCode_Alert;
/** Lỗi khi nhập mã pin. */
+ (NSString *)string_QuickPay_Error_Input_Password;
/** Mã Thanh Toán \n Mã dùng để thanh toán cho người bán\n Vui lòng không chia sẻ với người khác */
+ (NSString *)string_QuickPay_Secuirty_Warning;
/** Lưu ý: Tuyệt đối không gửi hoặc chia sẻ mã cho người khác */
+ (NSString *)string_QuickPay_Secuirty_Warning_DontShare;
/** Thanh toán tối đa %@ VND/giao dịch */
+ (NSString *)string_QuickPay_Secuirty_Warning_Max;
/** Không hiển thị lần sau */
+ (NSString *)string_QuickPay_Secuirty_Warning_DontShow;
/** Giá ban đầu */
+ (NSString *)string_QuickPay_Result_Original_Amount;
/** Khi không có kết nối mạng */
+ (NSString *)string_QuickPay_Setting_WhenDisconnect;
/** HẠN MỨC KHÔNG CẦN NHẬP MKTT */
+ (NSString *)string_QuickPay_Setting_MaxAmount_NotIputPassword;
/** THANH TOÁN TỐI ĐA */
+ (NSString *)string_QuickPay_Setting_MaxAmount;
/** NGUỒN THANH TOÁN */
+ (NSString *)string_QuickPay_Setting_ListChannel;
/** KHOÁ MÀN HÌNH ỨNG DỤNG */
+ (NSString *)string_QuickPay_Setting_LockScreen;
/** Vui lòng kết nối mạng để tiếp tục\n sử dụng tính năng này */
+ (NSString *)string_QuickPay_Disconnect;
/** Đồng Ý */
+ (NSString *)string_QuickPay_Button_OK;
/** Đang tải dữ liệu\n Vui lòng chờ trong ít phút! */
+ (NSString *)string_Download_Resources_Loading;
/** Là hình thức thanh toán ở các cửa hàng tiện lợi, quán cafe, nhà hàng, quán ăn... */
+ (NSString *)string_QuickPay_Slide_Intro_SubText_1;
/** Người dùng đưa mã thanh toán cho bên cung cấp dịch vụ để quét mã */
+ (NSString *)string_QuickPay_Slide_Intro_SubText_2;
/** Tiện lợi-An toàn-Siêu nhanh-Miễn phí */
+ (NSString *)string_QuickPay_Slide_Intro_SubText_3;
/** Trả tiền */
+ (NSString *)string_QuickPay_Slide_Intro_Title_1;
/** Hướng dẫn */
+ (NSString *)string_QuickPay_Slide_Intro_Title_2;
/** Ưu điểm */
+ (NSString *)string_QuickPay_Slide_Intro_Title_3;
/** Giao dịch đang xử lý, vui lòng kiểm tra kết quả giao dịch trong phần Lịch sử giao dịch sau 20 phút */
+ (NSString *)string_QuickPay_GetStatus_Timeout;
/** Có lỗi xảy ra khi xác nhận mã thanh toán. */
+ (NSString *)string_QuickPay_Error_VerifyPin;
/** Có lỗi xảy ra khi lấy kết quả giao dịch. */
+ (NSString *)string_QuickPay_Error_GetStatus;
/** Đã cập nhật mã thanh toán */
+ (NSString *)string_QuickPay_UpdatePin_Success;
/** Bấm vào mã vạch để lấy mã thanh toán */
+ (NSString *)string_QuickPay_Detail_BarCode;
/** Để liên kết thẻ thành công, số điện thoại đăng ký thẻ Vietinbank phải là \n %@ */
+ (NSString *)string_LinkBank_Notice_VTB_Message;
/** \nLưu ý: Hiện tại chỉ hỗ trợ mời bạn bè trên Zalo */
+ (NSString *)string_TransferMoney_Warning_Note;
/** %@ chưa sử dụng ZaloPay. Bạn hãy mời %@ sử dụng ZaloPay để nhận lì xì nhé. */
+ (NSString *)string_Send_RedPacket_Warning;
/** Giá trị nhập vào cần nhỏ hơn %@ */
+ (NSString *)string_TransferMoney_Max_Input_Value;
/** DANH SÁCH ĐÃ CHỌN */
+ (NSString *)string_RedPacket_Title_View_Selected;
/** Từ nay bạn có thể gửi %@ cho cả những người chưa có ZaloPay. Hãy nhắc bạn bè của bạn đăng ký ZaloPay để nhận %@ nhé. */
+ (NSString *)string_RedPacket_Guide_Text;
/** Đã áp dụng Ưu đãi mới cho kênh %@ */
+ (NSString *)string_Voucher_Use_For_New_PaymentMethod;
/** Ưu đãi không áp dụng cho kênh %@ */
+ (NSString *)string_Voucher_Not_Use_For_New_PaymentMethod;
/** Không thể gửi tin nhắn do thiết bị của bạn không hỗ trợ */
+ (NSString *)string_Sharing_Cannot_Send_Message;
/** Chia sẻ trên Facebook */
+ (NSString *)string_Sharing_Facebook_Title;
/** Chia sẻ trên Zalo */
+ (NSString *)string_Sharing_Zalo_Title;
/** Gửi tin nhắn trên Messenger */
+ (NSString *)string_Sharing_Messenger_Title;
/** Gửi tin nhắn */
+ (NSString *)string_Sharing_IMessage_Title;
/** Khác */
+ (NSString *)string_Sharing_Other_Title;
/** Số tiền thanh toán phải lớn hơn hạn mức giao dịch tối thiểu của %@ là %ld VND */
+ (NSString *)string_Voucher_Condition_LessThanMinPPAmount;
/** Số tiền thanh toán phải nhỏ hơn hạn mức giao dịch tối đa của %@ là %ld VND */
+ (NSString *)string_Voucher_Condition_GreaterThanMaxPPAmount;
/** Bạn có muốn tiếp tục thanh toán mà không sử dụng Ưu đãi đang có? */
+ (NSString *)string_Endow_Warning_NotUse;
/** Đang có khuyến mãi */
+ (NSString *)string_Endow_Having_Text;
/** Chỉ hiển thị Ưu đãi phù hợp */
+ (NSString *)string_Endow_Situation_Text;
/** Hoàn tiền */
+ (NSString *)string_Giftcode_Cashback_Title;
/** Voucher */
+ (NSString *)string_Giftcode_Voucher_Title;
/** <font color='#24272b'>Để liên kết thành công, bạn cần đăng ký sử dụng Dịch vụ Ngân hàng điện tử của Ngân hàng Bản Việt. \n\nNếu chưa đăng ký, vui lòng đăng ký tại quầy giao dịch của ngân hàng.</font> */
+ (NSString *)string_LinkBank_VCCB_Notify_Message;
/** Tôi đã đăng ký */
+ (NSString *)string_LinkBank_VCCB_Accept_Title_Button;
/** không tồn tại */
+ (NSString *)string_ZPC_not_exist_message;
/** Tìm kiếm theo ZaloPay ID cho */
+ (NSString *)string_ZPC_search_with_ZaloPayId;
/** NGÂN HÀNG LIÊN KẾT */
+ (NSString *)string_Gateway_Header_LinkBank;
/** NGÂN HÀNG KHÁC */
+ (NSString *)string_Gateway_Header_Payment;
/** Nguồn tiền khác */
+ (NSString *)string_Gateway_Title_Source;
/** Giao dịch tối thiểu từ %@ */
+ (NSString *)string_Gateway_Notice_LessMinAmount;
/** Giao dịch tối đa %@ */
+ (NSString *)string_Gateway_Notice_GreaterMaxAmount;
/** Nhập tên hoặc số điện thoại */
+ (NSString *)string_ZPC_guide_search_name_or_phone;
/** Giao dịch đang xử lý, vui lòng chờ trong giây lát */
+ (NSString *)string_QRScan_ExchangeProcessingMessage;
/** Mất kết nối mạng, vui lòng kiểm tra lại */
+ (NSString *)string_QRScan_NoConnectionMessage;

@end

#endif // __R_ZALOPAY_RESOURCES_H__
