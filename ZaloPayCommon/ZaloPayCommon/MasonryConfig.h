//
//  MasonryConfig.h
//  ZaloPayCommon
//
//  Created by Bon Bon on 3/23/16.
//  Copyright © 2016 Bon Bon. All rights reserved.
//

#ifndef MasonryConfig_h
#define MasonryConfig_h

//#define MAS_SHORTHAND
#define MAS_SHORTHAND_GLOBALS
#import <Masonry/Masonry.h>

#endif /* MasonryConfig_h */
