//
//  NSArray+ConstrainsLayout.m
//  ZaloPay
//
//  Created by PhucPv on 1/17/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "NSArray+ConstrainsLayout.h"

@implementation NSArray(ConstrainsLayout)
- (void)viewsAlongAxis:(MASAxisType)axisType
      withFixedSpacing:(CGFloat)fixedSpacing
           leadSpacing:(CGFloat)leadSpacing
                {
    UIView *lastView;
    for (int i = 0; i < self.count; i++) {
        UIView *view = [self objectAtIndex:i];
        if (axisType == MASAxisTypeVertical) {
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                if (lastView) {
                    make.top.equalTo(lastView.mas_bottom).with.offset(fixedSpacing);
                } else {
                    make.top.equalTo(@(leadSpacing)).with.priorityLow();
                }
            }];
        } else {
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                if (lastView) {
                    make.left.equalTo(lastView.mas_right).with.offset(fixedSpacing);
                } else {
                    make.left.equalTo(@(leadSpacing)).with.priorityLow();
                }
            }];
        }
        lastView = view;
    }
}

@end
