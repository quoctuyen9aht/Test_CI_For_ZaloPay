//
//  NSObject+Validate.h
//  ZaloPayCommon
//
//  Created by Dung Vu on 4/19/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void(^BlockExcution)(void);
@interface NSObject(Validate)

/**
 Cast obj for Objective C class

 @param obj anyObject
 @return  object if it is right class , other, nil object
 */
+ (instancetype _Nullable) castFrom:(id _Nullable)obj;
@end

/**
 Check object is right class

 @param obj any object , can nil
 @param cls Class type
 @return Boolean
 */
BOOL validObject(id _Nullable obj,Class _Nonnull cls);

/**
 Move code dispatch async in main thread to this

 @param excution action need to excute in main
 */
void excuteInSafeThread(BlockExcution _Nonnull excution);
