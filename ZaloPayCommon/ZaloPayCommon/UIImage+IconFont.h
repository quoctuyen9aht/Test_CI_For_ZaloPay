//
//  UIImage+IconFont.h
//  ZaloPayCommon
//
//  Created by bonnpv on 2/7/17.
//  Copyright © 2017-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (IconFont)
+ (UIImage*)imageWithIcon:(NSString*)identifier
          backgroundColor:(UIColor*)bgColor
                iconColor:(UIColor*)iconColor
                  andSize:(CGSize)size;
@end
