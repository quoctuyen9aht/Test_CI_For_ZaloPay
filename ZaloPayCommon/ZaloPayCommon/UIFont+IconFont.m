//
//  UIFont+IcontFont.m
//  ZaloPay
//
//  Created by bonnpv on 12/23/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "UIFont+IconFont.h"

@implementation UIFont (IconFont)
+ (UIFont *)iconFontWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"zalopay" size:size];
}
@end
