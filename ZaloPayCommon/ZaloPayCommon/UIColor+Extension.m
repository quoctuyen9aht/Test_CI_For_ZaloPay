//
//  UIColor+Extension.m
//  NewsReader
//
//  Created by Phuc  Pham Van on 3/10/14.
//  Copyright (c) 2014 NP. All rights reserved.
//
#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

//RGB color macro with alpha
#define UIColorFromRGBWithAlpha(rgbValue,a) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]

#import "UIColor+Extension.h"
#import "ZaloPayCommonLog.h"

@implementation UIColor (Extension)

+ (UIColor *)colorWithHexValue:(uint32_t)value {
    return UIColorFromRGB(value);
}

+ (UIColor *)colorWithHexValue:(uint32_t)value alpha:(float)alpha {
    return UIColorFromRGBWithAlpha(value, alpha);
}

+ (UIColor *)randomColor {
    CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
    return [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
}

+ (UIColor *)colorFromHexString:(NSString *)hexString {
    if (hexString.length != 7) {
        DDLogError(@"invalid color string :%@", hexString);
        return nil;
    }
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

@end

@implementation UIColor (ZaloPay)

+ (UIColor *)subText {
    return UIColorFromRGB(0x727f8c);
}

+ (UIColor *)defaultText {
    return UIColorFromRGB(0x24272b);
}

+ (UIColor *)midNightBlueColor {
    return UIColorFromRGB(0x2c3e50);
}

+ (UIColor *)yellowOchreColor {
    return UIColorFromRGB(0xd49c05);
}

+ (UIColor *)deepOrangeColor {
    return UIColorFromRGB(0xdb5800);
}

+ (UIColor *)zaloBaseColor {
    return [UIColor colorWithHexValue:0x008fe5];
}

+ (UIColor *)lineColor {
    return [UIColor colorWithHexValue:0xe3e6e7];
}

+ (UIColor *)errorColor {
    return [UIColor colorWithHexValue:0xfe0000];
}

+ (UIColor*)defaultBackground {
    return UIColorFromRGB(0xF0F4F7);
}

+ (UIColor *)placeHolderColor {
    return [UIColor colorWithHexValue:0xacb3ba];
}

+ (UIColor *)disableButtonColor {
    return [UIColor colorWithHexValue:0xc7cad3];
}
+ (UIColor *)highlightButtonColor {
    return [UIColor colorWithHexValue:0x0174af];
}
+ (UIColor *)payColor {
    return [UIColor colorWithHexValue:0x04be04];
}
+ (UIColor *)payHighlightColor {
    return [UIColor colorWithHexValue:0x21d921];
}
+ (UIColor *)selectedColor {
    return [UIColor colorWithHexValue:0xe7e9eb];
}
+ (UIColor *)buttonHighlightColor {
    return [UIColor colorWithHexValue:0x49bbff];
}
+ (UIColor *_Nonnull)buttonRedPacketHighlightColor {
    return [UIColor colorWithHexValue:0xc13a2f];
}

+ (UIColor *)zp_blue {
    return [self colorWithHexValue:0x4387f6];
}

+ (UIColor *)zp_green {
    return [self colorWithHexValue:0x129d5a];
}

+ (UIColor *)zp_red {
    return [self colorWithHexValue:0xde4438];
}

+ (UIColor *)zp_yellow {
    return [self colorWithHexValue:0xf6b501];
}

+ (UIColor *)zp_warning_yellow {
    return [self colorWithHexValue:0xffeb00];
}

+ (UIColor *)zp_gray {
    return [self colorWithHexValue:0x53718b];
}
+ (UIColor *)defaultAppIconColor {
    return [self colorWithHexValue:0xbbc1c8];
}

+ (UIColor *)hex_0x333333 {
    return [self colorWithHexValue:0x333333];
}

+ (UIColor *)hex_0x2b2b2b {
    return [self colorWithHexValue:0x2b2b2b];
}

+ (UIColor *)hex_0xacb3ba {
    return [self colorWithHexValue:0xacb3ba];
}

+ (UIColor *)hex_0x686871 {
    return [self colorWithHexValue:0x686871];
}

+ (UIColor *)rechargeColor {
    return [self colorWithHexValue:0x139b5b];
}

+ (UIColor *)withdrawColor {
    return [self colorWithHexValue:0xf6b501];
}
+ (UIColor *)hex_0xff5239 {
    return [self colorWithHexValue:0xff5239];
}

+ (UIColor *)hex_0x3c4854 {
    return [self colorWithHexValue:0x3c4854];
}

+ (UIColor *)hex_0xffffc1 {
    return [self colorWithHexValue:0xffffc1];
}

+ (UIColor *)hex_0xc7c7cc {
    return [self colorWithHexValue:0xc7c7cc];
}

+ (UIColor *)hex_0x4abbff {
    return [self colorWithHexValue:0x4abbff];
}
+ (UIColor *)hex_0xe6f6ff {
    return [self colorWithHexValue:0xe6f6ff];
}
+ (UIColor *)hex_0xe3e6e7 {
    return [self colorWithHexValue:0xe3e6e7];
}

+ (UIColor *)hex_0xEAF7FF {
    return [self colorWithHexValue:0xEAF7FF];
}

+ (UIColor *)hex_0x727f8c {
    return [self colorWithHexValue:0x727f8c];
}

+ (UIColor *)hex_0xB3E0FB {
    return [self colorWithHexValue:0xB3E0FB];
}

+ (UIColor *)hex_0X10A5FF {
    return [self colorWithHexValue:0X10A5FF];
}

+ (UIColor *)hex_0xff7c00 {
    return [self colorWithHexValue:0xff7c00];
}

+ (UIColor *)hex_0xff3824 {
    return [self colorWithHexValue:0xff3824];
}

+ (UIColor *)hex_0x585858 {
    return [self colorWithHexValue:0x585858];
}

+ (UIColor *)hex_0xcd7a04 {
    return [self colorWithHexValue:0xcd7a04];
}

@end
