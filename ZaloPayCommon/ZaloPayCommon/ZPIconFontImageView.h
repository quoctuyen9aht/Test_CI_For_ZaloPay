//
//  ZPIconFontImageView.h
//  ZaloPay
//
//  Created by bonnpv on 12/23/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZPIconFontImageView : UIImageView
@property (nonatomic, strong) UILabel *defaultView;
- (void)setIconFont:(NSString *)iconName;
- (void)setIconfont:(NSString *)iconName defaultIcon:(NSString *)defaultIcon;
- (void)setIconColor:(UIColor *)color;
- (void)iconFontSizeToFit;
@end
