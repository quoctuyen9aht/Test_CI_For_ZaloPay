//
//  ZPTextInputView.m
//  ZaloPayCommom
//
//  Created by nhatnt on 27/11/2017.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import "ZPCalFloatTextInputView.h"
#import "NSString+Decimal.h"

uint64_t kMaxInputValue = 1000000000000000;

@implementation ZPCalFloatTextInputView

- (BOOL)isExpression {
    NSArray *allOperator = @[@"+",@"-",@"×",@"÷"];
    NSString *text = self.textField.text;
    for (NSString *oneOperator in allOperator) {
        if ([text containsString:oneOperator]) {
            return TRUE;
        }
    }
    return FALSE;
}

- (BOOL)isMaxCharactor:(NSString *)newString {
    NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@" ."];
    NSString *trimmedString = [[newString componentsSeparatedByCharactersInSet: characterSet] componentsJoinedByString:@""];
    if (trimmedString.length > MAX_CHARACTERS_INPUT) {
        return TRUE;
    }
    return FALSE;
}

- (BOOL)isGreaterThanMaxInputValue:(NSString *)text {
    NSString *last = [text componentsSeparatedByString:@" "].lastObject;
    uint64_t value = [[last onlyDigit] longLongValue];
    return value > kMaxInputValue;
}


- (BOOL)isOperator: (NSString*)character {
    NSCharacterSet *s = [NSCharacterSet characterSetWithCharactersInString:@"+-×÷"];
    NSRange r = [character rangeOfCharacterFromSet:s];
    if (r.location != NSNotFound) {
        return TRUE;
    }
    return FALSE;
}

- (void)updateTextField:(UITextField*)textField updateText:(NSString*)text{
    if ([text length] == 0) {
        return;
    }
    NSString* resultText = @"";
    NSMutableArray* elements = [NSMutableArray arrayWithArray: [text componentsSeparatedByString:@" "]];
    //Normalized elements when the last is " "
    if(elements.lastObject != nil) {
        if ([elements.lastObject isEqual: @""]) {
            [elements removeLastObject];
        } else {
            if ([self isOperator:elements.lastObject]){
                [elements removeLastObject];
            }
        }
    }
    
    //Normalized elements wrong format like +4 4-,...
    for (int index = 0; index < elements.count; index++) {
        if ([self isOperator:elements[index]] && [elements[index] length] > 1) {
            [elements removeObjectAtIndex:index];
            break;
        }
    }
    
    for (NSString*element in elements){
        if ([self isOperator:element]) {
            resultText = [NSMutableString stringWithFormat:@"%@ %@ ", resultText, element];
        } else {
            resultText = [NSMutableString stringWithFormat:@"%@%@", resultText, [[element onlyDigit] formatMoneyValue]];
        }
    }
    textField.text = resultText;
}

- (void)showErrorAuto:(NSString *)errorText {
    [super showErrorWithText:errorText];
}

@end
