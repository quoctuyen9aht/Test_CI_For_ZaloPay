//
//  ZPIconFontImageView.m
//  ZaloPay
//
//  Created by bonnpv on 12/23/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "ZPIconFontImageView.h"
#import "MasonryConfig.h"
#import "UIFont+Extension.h"
#import "UIFont+IconFont.h"
#import "UIColor+Extension.h"
#import "UILabel+IconFont.h"

@interface ZPIconFontImageView ()
@end

@implementation ZPIconFontImageView

- (void)setImage:(UIImage*)image {
    [super setImage:image];
    self.defaultView.hidden = (nil != image);
}

- (UILabel*)defaultView {
    if (nil != _defaultView) {
        return _defaultView;
    }
    
    _defaultView = [[UILabel alloc] initWithFrame:self.bounds];
    _defaultView.font = [UIFont iconFontWithSize:self.bounds.size.height];
    _defaultView.textAlignment = NSTextAlignmentCenter;
    _defaultView.adjustsFontSizeToFitWidth = YES;
    _defaultView.backgroundColor = [UIColor clearColor];
    _defaultView.textColor = [UIColor zp_red];
    [self addSubview:_defaultView];
//    [_defaultView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(@0);
//        make.bottom.equalTo(@0);
//        make.left.equalTo(@0);
//        make.right.equalTo(@0);
//    }];
    [_defaultView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    return _defaultView;
}

#pragma mark - Properties

- (void)setIconFont:(NSString *)iconName {
    [self setIconfont:iconName defaultIcon:@""];
}

- (void)setIconfont:(NSString *)iconName defaultIcon:(NSString *)defaultIcon {
    [self.defaultView setIconfont:iconName defaultIcon:defaultIcon];
    [self setImage:nil];    
//    _defaultView.hidden = false;
}

- (void)setIconColor:(UIColor *)color {
    [self.defaultView setTextColor:color];
}

- (void)iconFontSizeToFit {
    _defaultView.font = [UIFont iconFontWithSize:self.bounds.size.height];
}

@end
