//
//  UIFont+IcontFont.h
//  ZaloPay
//
//  Created by bonnpv on 12/23/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (IconFont)
+ (UIFont *)iconFontWithSize:(CGFloat)size;
@end
