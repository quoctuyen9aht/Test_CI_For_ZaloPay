//
//  
//
//  Created by Bon on 10/25/15.
//  Copyright © 2015 Bon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (Extension)
+ (NSString *)identify;
+ (instancetype)defaultCellForTableView:(UITableView *)tableView;
+ (instancetype)cellFromSameNibForTableView:(UITableView *)tableView;
+ (instancetype)cellForTableView:(UITableView *)tableView;
+ (float)height;
+ (UITableViewCell *)makeATemplateCell;
@end
