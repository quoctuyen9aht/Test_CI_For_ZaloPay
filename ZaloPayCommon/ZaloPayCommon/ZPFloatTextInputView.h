//
//  ZPTextInputView.h
//  ZaloPayCommon
//
//  Created by PhucPv on 9/10/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextField.h"
@class RACSubject;
@interface ZPFloatTextInputView : UIView
@property (nonatomic, strong) JVFloatLabeledTextField *textField;

@property (nonatomic, strong) UIColor *normalColor;
@property (nonatomic, strong) UIColor *highlightColor;
@property (nonatomic, strong) UIColor *errorColor;
@property (nonatomic, strong) RACSubject *eError;
//@property (nonatomic, assign) UIEdgeInsets lineInsets;
//@property (nonatomic, assign) UIEdgeInsets textInsets;
//@property (nonatomic, assign) UIEdgeInsets textFieldInsets;

- (void)showErrorAuto:(NSString *)errorText;
- (void)showErrorWithText:(NSString *)errorText;
- (void)clearErrorText;
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, assign) UIEdgeInsets lineInsets;
@property (nonatomic, assign) UIEdgeInsets textFieldInsets;
@end
