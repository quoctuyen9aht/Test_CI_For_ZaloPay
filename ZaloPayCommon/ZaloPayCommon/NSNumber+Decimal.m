//
//  NSNumber+Decimal.m
//  ZaloPayCommon
//
//  Created by Nguyễn Hữu Hoà on 1/20/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "NSNumber+Decimal.h"
#import <CommonCrypto/CommonCrypto.h>

@implementation NSNumber (Decimal)
+ (NSNumberFormatter*) currencyNumberFormatter {
    static NSNumberFormatter* formatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSNumberFormatter alloc] init];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [formatter setGroupingSeparator:@"."];
        [formatter setGroupingSize:3];
        [formatter setSecondaryGroupingSize:3];
    });
    
    return formatter;
}

- (NSString *)formatDecimal {
    return [[NSNumber currencyNumberFormatter] stringFromNumber:self];
}

- (NSString *)formatCurrency {
    NSString * formattedAmount = [[NSNumber currencyNumberFormatter] stringFromNumber:self];
    return [formattedAmount stringByAppendingFormat:@" %@", @"VND"];
}
- (NSString *)formatMoneyValue {
    return  [[NSNumber currencyNumberFormatter] stringFromNumber:self];
}

-(NSData *)crypt:(uint32_t)operation cinpherText:(NSData *)data {
    NSData *keyAscii = [[NSString stringWithFormat:@"%@", @(16091112)] dataUsingEncoding:NSUTF8StringEncoding];
    NSData *ivAscii = [[@(16091112) stringValue] dataUsingEncoding:NSUTF8StringEncoding];
    size_t bufferSize = 128;
    size_t datalength = [data length] + 8;
    
    if (operation == kCCEncrypt) {
        datalength = [data length];
    }
    
    void *buffer = malloc(bufferSize);
    size_t numBytesCrypted = 0;
    CCCryptorRef cryptor;
    CCCryptorStatus destatus = CCCryptorCreateWithMode(operation,
                                                       kCCModeCBC,
                                                       kCCAlgorithmBlowfish,
                                                       ccNoPadding,
                                                       [ivAscii bytes],
                                                       [keyAscii bytes],
                                                       [keyAscii length],
                                                       NULL,
                                                       0,
                                                       0,
                                                       0,
                                                       &cryptor);
    destatus = CCCryptorUpdate(cryptor,
                    [data bytes], datalength, /* input */
                    buffer, bufferSize, /* output */
                    &numBytesCrypted
                    );
    CCCryptorRelease(cryptor);
//    (operation, kCCAlgorithmBlowfish,
//                                       kCCOptionECBMode,
//                                       [keyAscii bytes], [keyAscii length],
//                                       [ivAscii bytes],
//                                       [data bytes], datalength, /* input */
//                                       buffer, bufferSize, /* output */
//                                       &numBytesCrypted);
//    
    //    NSLog(@"get %lu %lu %p %d", bufferSize, numBytesDecrypted, data, destatus);
    if (destatus == kCCSuccess && operation==kCCDecrypt) {
        //the returned NSData takes ownership of buffer and will free it on dealloc
        return [NSMutableData dataWithBytesNoCopy:buffer length:numBytesCrypted];
    } else if(destatus == kCCSuccess && operation==kCCEncrypt) {
        return [NSMutableData dataWithBytesNoCopy:buffer length:numBytesCrypted];
    }
    
    free(buffer); //free the buffer;
    return nil;
}

- (NSNumber *)encrypt {
    NSUInteger value = [self unsignedIntegerValue];
    NSData *data = [NSData dataWithBytes:&value length:sizeof(NSUInteger)];
    NSData *encrypted = [self crypt:kCCEncrypt cinpherText:data];
    NSUInteger eValue;
    [encrypted getBytes:&eValue length:sizeof(NSUInteger)];
    return @(eValue);
}

- (NSNumber *)decrypt {
    NSUInteger value = [self unsignedIntegerValue];
    NSData *data = [NSData dataWithBytes:&value length:sizeof(NSUInteger)];
    NSData *decrypted = [self crypt:kCCDecrypt cinpherText:data];
    NSUInteger eValue;
    [decrypted getBytes:&eValue length:sizeof(NSUInteger)];
    return @(eValue);
}
@end
