//
//  UIDevice+Payment.m
//  ZaloPayCommon
//
//  Created by bonnpv on 4/29/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "UIDevice+Payment.h"
#import "Macro.h"


typedef NS_ENUM(NSInteger, ZPDeviceType){
    ZPDeviceTypeiPhone1x,
    ZPDeviceTypeiPhone2x,
    ZPDeviceTypeiPhone3x,
    ZPDeviceTypeiPad1x,
    ZPDeviceTypeiPad2x,
    ZPDeviceTypeUnknown
};

#define IS_1X ( [[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 1 )
#define IS_2X ( [[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2 )
#define IS_3X ( [[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 3 )

@implementation UIDevice (Payment)

- (ZPDeviceType)deviceType {
    if (IS_IPHONE && IS_1X){
        return ZPDeviceTypeiPhone1x;
    } else if (IS_IPHONE && IS_2X) {
        return ZPDeviceTypeiPhone2x;
    } else if (IS_IPHONE && IS_3X) {
        return ZPDeviceTypeiPhone3x;
    } else if (IS_IPAD() && IS_1X) {
        return ZPDeviceTypeiPad1x;
    } else if (IS_IPAD() && IS_2X) {
        return ZPDeviceTypeiPad2x;
    } else {
        return ZPDeviceTypeUnknown;
    }
}

- (NSString *)screenTypeString {
    switch ([self deviceType]) {
        case ZPDeviceTypeiPhone1x: return @"iphone1x";
        case ZPDeviceTypeiPhone2x: return @"iphone2x";
        case ZPDeviceTypeiPhone3x: return @"iphone3x";
        case ZPDeviceTypeiPad1x:   return @"ipad1x";
        case ZPDeviceTypeiPad2x:   return @"ipad2x";
        default:                    return @"iphone2x";
    }
}
@end
