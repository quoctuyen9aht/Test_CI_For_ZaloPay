//
//  UITextField+Extension.m
//  ZaloPay
//
//  Created by PhucPv on 1/17/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "UITextField+Extension.h"
#import "NSNumber+Decimal.h"
#import "NSString+Decimal.h"
#import "UIView+Extention.h"
#import "UIColor+Extension.h"

@implementation UITextField(Extension)

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
+ (BOOL)converToDecimalWithTextField:(UITextField *)textField
       shouldChangeCharactersInRange:(NSRange)range
                   replacementString:(NSString *)string
                           maxValue:(u_int64_t)maxValue
                         errorBlock:(void (^)(void))errorBlock
{
    NSNumberFormatter *formatter = [NSNumber currencyNumberFormatter];
    NSString *decimalSeparator = [formatter decimalSeparator];
    NSString *groupingSeparator = [formatter groupingSeparator];
    
    if (([string isEqualToString:@"0"] || [string isEqualToString:@""]) && [textField.text rangeOfString:decimalSeparator].location < range.location) {
        return YES;
    }
    bool isNumeric = [string isEqualToString:[string onlyDigit]];
    if (isNumeric) {
        NSString *combinedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        if ([[combinedText onlyDigit] longLongValue] <= maxValue) {
            textField.text = [combinedText formatNumber:groupingSeparator];
        } else {
            if (errorBlock) {
                errorBlock();
            }
        }
    }
//    if (isNumeric ||
//        [string isEqualToString:@""] ||
//        ([string isEqualToString:decimalSeparator] &&
//         [textField.text rangeOfString:decimalSeparator].location == NSNotFound)) {
//            // Combine the new text with the old; then remove any
//            // commas from the textField before formatting
//            NSString *combinedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
//            NSString *numberWithoutCommas = [combinedText stringByReplacingOccurrencesOfString:groupingSeparator withString:@""];
//            NSNumber *number = [formatter numberFromString:numberWithoutCommas];
//            if ([number longLongValue] <= maxValue) {
//                NSString *formattedString = [formatter stringFromNumber:number];
//                
//                // If the last entry was a decimal or a zero after a decimal,
//                // re-add it here because the formatter will naturally remove
//                // it.
//                if ([string isEqualToString:decimalSeparator] &&
//                    range.location == textField.text.length) {
//                    formattedString = [formattedString stringByAppendingString:decimalSeparator];
//                }
//                
//                textField.text = formattedString;
//            } else {
//            }
//        }
    
    // Return no, because either the replacement string is not
    // valid or it is and the textfield has already been updated
    // accordingly
    return NO;
}

- (void)setupUIBase {
    self.borderStyle = UITextBorderStyleNone;
    self.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);
    [self roundRect:5
        borderColor:[UIColor midNightBlueColor]
        borderWidth:1.0];
    
}
@end
@implementation UITextView(Extension)
- (void)setupUIBase {
    [self roundRect:5
        borderColor:[UIColor midNightBlueColor]
        borderWidth:1.0];
}
@end
