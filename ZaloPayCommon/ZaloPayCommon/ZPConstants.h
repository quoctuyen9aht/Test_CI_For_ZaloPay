
#import <UIKit/UIKit.h>


#ifndef __ZALOPAYCOMMON_CONSTANTS_H__
#define __ZALOPAYCOMMON_CONSTANTS_H__

extern NSInteger kZaloPayClientAppId;
extern NSInteger reactInternalAppId;
extern NSInteger withdrawAppId;
extern NSInteger showshowAppId;
extern const int ZaloPayAppStoreId;
extern NSInteger lixiAppId;
extern NSInteger topupPhoneAppId;
extern NSInteger mobileCardAppId;
extern NSInteger topupPhoneV2AppId;
extern NSInteger appDichVuId;
extern NSInteger appVoucherId;
extern NSString *const ZaloPayAppStoreUrl;

extern const int NUMBER_OF_DIGIT;
extern const int NUMBER_OF_OTP;
extern const int TRANSFER_MESSAGE_LENGTH;
extern const int minAccountNameLength;
extern const int maxAccountNameLength;
extern const int EMAIL_LENGTH;
extern const int IDENTYFIER_NUMBER_LENGTH;
extern const int FEEDBACK_MESSAGE_LENGTH;
extern const float timeoutIntervalForRequest;
extern const float timeoutIntervalForConnectorRequest;
extern const int WithDrawNumberOther;
extern const int WithDrawMinDefault;
extern const int WithDrawMultipleDefault;

extern const int minRechargeValue;
extern const int maxRechargeValue;
extern const int defaultMinTransfer;
extern const int defaultMaxTransfer;
extern const int defaultMaxWithdraw;
extern const int maxCountNotifications;

extern const NSString* itunesAppLink;


#pragma mark - Key
extern NSString *const ZPEnableRechargeKey;
extern NSString *const Force_Update_Appversion_Key;
extern NSString *const ZPConfigCacheKey;
extern NSString *const ZPBannerKey;
extern NSString *const ZPBankSupportKey;

extern NSString *const ZPProfileLevel3_Email;
extern NSString *const ZPProfileLevel3_Identifier;
extern NSString *const ZPProfileLevel3_FrontIdentifyCard;
extern NSString *const ZPProfileLevel3_BackSideIdentifyCard;
extern NSString *const ZPProfileLevel3_Avatar;
extern NSString *const ZPInApp_Notification_Key;


extern NSString *const Reload_AccountList_Notification;
extern NSString *const Reload_CardList_Notification;
extern NSString *const Reload_Profile_Notification;
extern NSString *const ZPCreateWalletOrderTranstypeKey;
extern NSString *const ZPCreateWalletOrderExtKey;
extern NSString *const ZPCreateWalletOrderSenderKey;
extern NSString *const ZPCreateWalletOrderSenderPhoneNumberKey;
extern NSString *const Key_Store_Database_Promotion;
extern NSString *const ZPNumberAppSearched;
extern NSString *const Quick_Comment_URL;
extern NSString *kUrlPromotion;

extern NSString *const ViewModeKeyboard_ABC;
extern NSString *const ViewModeKeyboard_Phone;

extern NSString *const ReactModule_TransactionLogs;
extern NSString *const ReactModule_PaymentMain;
extern NSString *const ReactModule_RedPacket;
extern NSString *const Merchant_Disclaimer_Key;

#endif // __ZALOPAYCOMMON_CONSTANTS_H__
