//
//  UIButton+IconFont.m
//  ZaloPay
//
//  Created by bonnpv on 12/25/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import "UIButton+IconFont.h"
#import "UILabel+IconFont.h"
@implementation UIButton (IconFont)

- (void)setIconFont:(NSString *)iconName forState:(UIControlState)state {
    NSString *code = [UILabel iconCodeWithName:iconName];
    [self setTitle:code forState:state];
}
@end
