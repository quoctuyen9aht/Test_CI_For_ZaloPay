//
//  UIColor+Extension.h
//  NewsReader
//
//  Created by Phuc  Pham Van on 3/10/14.
//  Copyright (c) 2014 NP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Extension)
+ (UIColor *_Nonnull)colorWithHexValue:(uint32_t)value;
+ (UIColor *_Nonnull)colorWithHexValue:(uint32_t)value alpha:(float)alpha;
+ (UIColor *_Nonnull)randomColor;
+ (UIColor *_Nullable)colorFromHexString:(NSString *_Nullable)hexString;
@end

@interface UIColor (ZaloPay)
+ (UIColor *_Nonnull)midNightBlueColor;
+ (UIColor *_Nonnull)zaloBaseColor;
+ (UIColor *_Nonnull)lineColor;
+ (UIColor *_Nonnull)errorColor;
+ (UIColor *_Nonnull)defaultBackground;
+ (UIColor *_Nonnull)subText;
+ (UIColor *_Nonnull)defaultText;
+ (UIColor *_Nonnull)placeHolderColor;
+ (UIColor *_Nonnull)disableButtonColor;
+ (UIColor *_Nonnull)highlightButtonColor;
+ (UIColor *_Nonnull)payColor;
+ (UIColor *_Nonnull)payHighlightColor;
+ (UIColor *_Nonnull)selectedColor;
+ (UIColor *_Nonnull)buttonHighlightColor;
+ (UIColor *_Nonnull)buttonRedPacketHighlightColor;

// KYC - verification status colors
+ (UIColor *_Nonnull)yellowOchreColor;
+ (UIColor *_Nonnull)deepOrangeColor;

+ (UIColor *_Nonnull)zp_blue;
+ (UIColor *_Nonnull)zp_green;
+ (UIColor *_Nonnull)zp_red;
+ (UIColor *_Nonnull)zp_yellow;
+ (UIColor *_Nonnull)zp_warning_yellow;
+ (UIColor *_Nonnull)zp_gray;
+ (UIColor *_Nonnull)defaultAppIconColor;

+ (UIColor *_Nonnull)hex_0x333333;
+ (UIColor *_Nonnull)hex_0x2b2b2b;
+ (UIColor *_Nonnull)hex_0xacb3ba;
+ (UIColor *_Nonnull)hex_0x686871;

+ (UIColor *_Nonnull)rechargeColor;
+ (UIColor *_Nonnull)withdrawColor;
+ (UIColor *_Nonnull)hex_0xff5239;
+ (UIColor *_Nonnull)hex_0x3c4854;
+ (UIColor *_Nonnull)hex_0xffffc1;
+ (UIColor *_Nonnull)hex_0xc7c7cc;
+ (UIColor *_Nonnull)hex_0x4abbff;
+ (UIColor *_Nonnull)hex_0xe6f6ff;
+ (UIColor *_Nonnull)hex_0xe3e6e7;
+ (UIColor *_Nonnull)hex_0xEAF7FF;
+ (UIColor *_Nonnull)hex_0x727f8c;
+ (UIColor *_Nonnull)hex_0xB3E0FB;
+ (UIColor *_Nonnull)hex_0X10A5FF;
+ (UIColor *_Nonnull)hex_0xff7c00;
+ (UIColor *_Nonnull)hex_0xff3824;
+ (UIColor *_Nonnull)hex_0x585858;
+ (UIColor *_Nonnull)hex_0xcd7a04;

@end
