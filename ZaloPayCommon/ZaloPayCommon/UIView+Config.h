//
//  UIView+Config.h
//  ZaloPayCommon
//
//  Created by bonnpv on 4/21/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef  void (^ViewConfigBlock)(void);

@interface UIView (Config)
+ (void)configSreen2x:(ViewConfigBlock)config2x scree3x:(ViewConfigBlock)config3x;
+ (BOOL)isScreen2x;
+ (BOOL)isIPhone6Plus;
+ (BOOL)isIPhone4;
+ (BOOL)isIPhone5;
+ (BOOL)isIPhone6;
+ (BOOL)isIPhoneX;
+ (BOOL)isHeightIP6PlusOrLarger;
@end
