//
//  UITextField+Extension.h
//  ZaloPay
//
//  Created by PhucPv on 1/17/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField(Extension)
+ (BOOL)converToDecimalWithTextField:(UITextField *)textField
       shouldChangeCharactersInRange:(NSRange)range
                   replacementString:(NSString *)string
                            maxValue:(u_int64_t)maxValue
                          errorBlock:(void (^)(void))errorBlock;

- (void)setupUIBase;
@end

@interface UITextView(Extension)
- (void)setupUIBase;
@end
