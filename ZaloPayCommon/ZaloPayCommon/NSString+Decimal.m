//
//  NSString+Decimal.m
//  ZaloPay
//
//  Created by PhucPv on 12/30/15.
//  Copyright © 2015-present VNG Corporation. All rights reserved.
//

#import "NSString+Decimal.h"
#import "NSNumber+Decimal.h"
#import "ZaloPayCommonLog.h"
#import "UIColor+Extension.h"

@implementation NSString(Decimal)
+ (NSString *)stringDecimalFromString:(NSString *)original {
    NSNumber * number = [NSNumber numberWithInt:[original intValue]];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    NSString *convertNumber = [formatter stringFromNumber:number];
    return convertNumber;
}
+ (NSString *)stringDecimalFromLongLong:(u_int64_t)value {
    NSNumber * number = [NSNumber numberWithLongLong:value];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    NSString *convertNumber = [formatter stringFromNumber:number];
    return convertNumber;
}

+ (NSNumber *)numberFromStringDecimal:(NSString *)string {
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter.numberStyle = NSNumberFormatterCurrencyStyle;
    NSNumber *value = [numberFormatter numberFromString:string];
    return value;
}

+ (NSNumber *)numberFromStringCurrency:(NSString *)string {
    NSNumberFormatter* NumberFormatter = [[NSNumberFormatter alloc] init];
    [NumberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [NumberFormatter setUsesSignificantDigits:YES];
    [NumberFormatter setMinimumSignificantDigits:0];
    [NumberFormatter setGroupingSeparator:@"."];
    [NumberFormatter setDecimalSeparator:@","];
    return [NumberFormatter numberFromString:string];
}

- (NSString *)formatCurrency {
    return [@([self floatValue]) formatCurrency];
}

- (NSString *)formatMoneyValue {
    if (self.length == 0) {
        return @"";
    }
    
    return [@([self longLongValue]) formatMoneyValue];
}

- (NSString *)onlyDigit {
    if (self.length == 0) {
        return @"";
    }
    
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    NSString *toPhone = [[self componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return toPhone;
}
- (NSString *)removePhoneCountryCode {
    if ([self hasPrefix:@"84"]) {
        return [self stringByReplacingCharactersInRange:NSMakeRange(0, 2) withString:@"0"];
    }
    return [self stringByReplacingOccurrencesOfString:@"+84" withString:@"0"];
}

- (NSString *)normalizePhoneNumber {
    if (self.length >= 10) {
        return [self removePhoneCountryCode];
    }
    return @"";
}

- (NSString *)formatNumber:(NSString *)character {
    if (self.length < 3) {
        return self;
    }
    NSString *newString = [self stringByReplacingOccurrencesOfString:character withString:@""];
    newString = [self removeFirtZeroCharacter:newString];
    NSMutableString *origin = [NSMutableString stringWithString:newString];
    
    return [self originString:origin insertString:character index:origin.length - 3 nextIndex:3];
}

- (NSString *)removeFirtZeroCharacter:(NSString *)originString {
    if ([originString hasPrefix:@"0"]) {
        originString = [originString substringFromIndex:1];
        return [self removeFirtZeroCharacter:originString];
    }
    return originString;
}

- (NSString *)originString:(NSMutableString *)origin
              insertString:(NSString *)string
                     index:(NSUInteger)index
                 nextIndex:(int)next {
    if (index <= 0 || index >= origin.length) {
        return origin;
    }
    [origin insertString:string atIndex:index];
    return [self originString:origin insertString:string index:index - next nextIndex:next];
}

- (NSString *)formatCard {
    if (self.length <= 4) {
        return self;
    }
    NSMutableString *card = [NSMutableString stringWithString:[self stringByReplacingOccurrencesOfString:@" " withString:@""]];
    return [self reverserOriginString:card insertString:@" " index:4 nextIndex:5];
}

- (NSString *)reverserOriginString:(NSMutableString *)origin
              insertString:(NSString *)string
                     index:(NSUInteger)index
                 nextIndex:(int)next{
    if (index <= 0 || index >= origin.length) {
        return origin;
    }
    [origin insertString:string atIndex:index];
    return [self reverserOriginString:origin insertString:string index:index + next nextIndex:next];
}

- (BOOL)isPhoneNumber {
    if ([self length] == 0) {
        return NO;
    }
    
    NSString* phone = [self stringByReplacingOccurrencesOfString:@"+84" withString:@"0"];

    NSRegularExpression *regex = [[NSRegularExpression alloc] initWithPattern:@"(09\\d{8})|(01\\d{9})" options:0 error:nil];
    NSRange range = [regex rangeOfFirstMatchInString:phone options:0 range:NSMakeRange(0, phone.length)];
    if (range.location != NSNotFound) {
        DDLogInfo(@"%@ is phone number", phone);
        return YES;
    }
    return NO;
}
- (NSString*)stringWithDashSeparated{
    NSMutableString *string = [NSMutableString stringWithString:self];
    NSUInteger index = 4;
    while (index < [string length]) {
        [string insertString:@"-" atIndex:index];
        index += 5;
    }
    return string;
}

- (NSString *)removePrefix:(NSString *)prefix {
    if ([self hasPrefix:prefix]) {
        return [self substringFromIndex:prefix.length];
    }
    return self;
}

- (NSString *)removeSuffix:(NSString *)suffix {
    if ([self hasSuffix:suffix]) {
        return [self substringToIndex:self.length - suffix.length - 1];
    }
    return self;
}

- (NSMutableAttributedString *)formatCurrencyFont:(UIFont *)font
                                            color:(UIColor *)color
                                          vndFont:(UIFont*)vndFont
                                         vndcolor:(UIColor *)vndColor
                                        alignment:(NSTextAlignment)alignment{
    
    NSMutableParagraphStyle *style  = [[NSMutableParagraphStyle alloc] init];
    style.lineSpacing = 1.f;
    style.alignment = alignment;
    
    NSDictionary *attributtes = @{NSParagraphStyleAttributeName : style,
                                  NSFontAttributeName           : font,
                                  NSForegroundColorAttributeName: color};
    
    NSMutableAttributedString *_return = [[NSMutableAttributedString alloc] initWithString:self
                                                                                attributes:attributtes];
    
    if (self.length > 4) {
        NSRange range = NSMakeRange(self.length - 4, 4);
        [_return addAttribute:NSFontAttributeName
                        value:vndFont
                        range:range];
        [_return addAttribute:NSForegroundColorAttributeName
                        value:vndColor
                        range:range];
    }
    return _return;
}
+ (NSMutableString*)timeIntervalToStringExpire:(double)time{
    
    NSMutableString *timeLeft = [[NSMutableString alloc]init];
    
    NSDate* expireddate = [NSDate dateWithTimeIntervalSince1970:time/1000 ];
    NSTimeInterval seconds = [expireddate timeIntervalSinceDate:[NSDate date]];
    
    if (seconds <= 0) {
        [timeLeft appendString:[NSString stringWithFormat: @"Hết hạn"]];
        return timeLeft;
    }
    NSInteger days = (int) (floor(seconds / (3600 * 24)));
    if(days) seconds -= days * 3600 * 24;
    
    NSInteger hours = (int) (floor(seconds / 3600));
    if(hours) seconds -= hours * 3600;
    
    NSInteger minutes = (int) (floor(seconds / 60));
    if(minutes) seconds -= minutes * 60;
    
    if(days) {
        [timeLeft appendString:[NSString stringWithFormat:@"Còn %ld ngày", (long)days]];
    }
    else if(hours) {
        [timeLeft appendString:[NSString stringWithFormat: @"Còn %ld giờ", (long)hours]];
    }
    else if(minutes) {
        [timeLeft appendString: [NSString stringWithFormat: @"Còn %ld phút",(long)minutes]];
    }
    else {
        [timeLeft appendString:[NSString stringWithFormat: @"Còn 1 phút"]];
    }
    
    return timeLeft;
    
}
@end
