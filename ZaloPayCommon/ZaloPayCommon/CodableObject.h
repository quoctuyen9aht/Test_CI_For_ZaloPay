//
//  CodableObject.h
//  ZaloPay
//
//  Created by Nguyễn Hữu Hoà on 1/30/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CodableObject : NSObject<NSCoding>

@end
