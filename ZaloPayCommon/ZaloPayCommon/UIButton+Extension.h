//
//  UIButton+Extension.h
//  ZaloPay
//
//  Created by PhucPv on 1/4/16.
//  Copyright © 2016-present VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
extern CGFloat const kZaloButtonHeight;
extern float const kZaloButtonToScrollOffset;
extern float const kMinZaloButtonToScrollOffset;

@interface UIButton(Extension)
- (void)setBackgroundColor:(UIColor *)backgroundColor forState:(UIControlState)state;
- (void)setupZaloPayButton;
@end
