//
//  NSString+Decimal.h
//  ZaloPay
//
//  Created by PhucPv on 12/30/15.
//  Copyright © 2015-present VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString(Decimal)
+ (NSString *)stringDecimalFromString:(NSString *)original;
+ (NSString *)stringDecimalFromLongLong:(u_int64_t)value;
+ (NSNumber *)numberFromStringDecimal:(NSString *)string;
+ (NSNumber *)numberFromStringCurrency:(NSString *)string;
- (NSString *)formatCurrency;
- (NSString *)formatMoneyValue;
- (NSString *)onlyDigit;
- (NSString *)removePhoneCountryCode;
// Return string for phone number without contry code
- (NSString *)normalizePhoneNumber;
- (NSString *)formatNumber:(NSString *)character;
- (NSString *)formatCard;
- (BOOL)isPhoneNumber;
- (NSString*)stringWithDashSeparated;
- (NSString *)removePrefix:(NSString *)prefix;
- (NSString *)removeSuffix:(NSString *)suffix;
- (NSMutableAttributedString *)formatCurrencyFont:(UIFont *)font
                                            color:(UIColor *)color
                                          vndFont:(UIFont*)vndFont
                                          vndcolor:(UIColor *)vndColor
                                        alignment:(NSTextAlignment)alignment;
+ (NSMutableString*)timeIntervalToStringExpire:(double)time;
@end
