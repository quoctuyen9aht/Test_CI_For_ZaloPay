//
//  NSArray+Safe.h
//  MarQet
//
//  Created by Bon on 2/14/15.
//  Copyright (c) 2015 Bon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Safe)
- (id)safeObjectAtIndex:(NSInteger)index;
- (id)itemAtIndexPath:(NSIndexPath *)indexpath;
- (NSArray *)sectionAtIndexPath:(NSIndexPath *)indexPath;
- (NSArray *)sectionAtIndex:(NSInteger)index;
- (NSArray *)subArrayAtIndex:(NSInteger)index withTotalItem:(NSInteger)total;
- (NSInteger)subArrayCount:(NSInteger)total;
- (NSArray *)itemsAtIndex:(NSInteger)index withTotalItem:(NSInteger)total;
- (void)forEach:(void (^)(id obj))handler;
- (NSArray *)map:(id(^)(id obj))transform;
@end

@interface NSMutableArray (SafeRemove)
- (void)safeRemoveObjectAtIndex:(NSInteger)index;
@end

@interface NSArray<ObjectType> (First)
- (ObjectType)firstObjectUsing:(BOOL(^)(ObjectType))blockCondition;
@end
