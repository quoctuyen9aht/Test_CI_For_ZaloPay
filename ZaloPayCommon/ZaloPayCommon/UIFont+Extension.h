//
//  UIFont+SFUIDisplay.h
//  
//
//  Created by PhucPv on 10/14/15.
//  Copyright (c) 2015-present VNG Corporation All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol RACSubscriber;
#define AspectRatioCalculator(size) \
CGRectGetWidth([UIScreen mainScreen].bounds)/414.0 *size
//1
@interface UIFont(Extension)
+ (UIFont *)robotoBoldWithSize:(CGFloat)size;
+ (UIFont *)robotoRegularWithSize:(CGFloat)size;
+ (UIFont *)robotoMediumWithSize:(CGFloat)size;
+ (UIFont *)robotoMediumItalicWithSize:(CGFloat)size;
+ (UIFont *)helveticaNeueRegularWithSize:(CGFloat)size;
+ (UIFont *)helveticaNeueBoldWithSize:(CGFloat)size;
+ (UIFont *)helveticaNeueMediumWithSize:(CGFloat)size;

+ (UIFont *)SFUITextRegularWithSize:(CGFloat)size;
+ (UIFont *)SFUITextMediumWithSize:(CGFloat)size;
+ (UIFont *)SFUITextSemiboldWithSize:(CGFloat)size;
+ (UIFont *)SFUITextRegularItalicWithSize:(CGFloat)size;
+ (UIFont *)SFUITextSemiboldItalicWithSize:(CGFloat)size;
+ (UIFont *)zaloPayWithSize:(CGFloat)size;
+ (UIFont *)ocraRegular:(CGFloat)size;
+ (UIFont *)SFUITextRegularSmall;
+ (UIFont *)SFUITextRegularBalanceValue;
+ (UIFont *)SFUITextRegularMedium;
+ (UIFont *)defaultSFUITextRegular;
+ (UIFont *)defaultSFUITextItalic;
+ (UIFont *)SFUITextMediumWithAmount:(long)amount;

+ (void)dumpAllFonts;
+ (void)registerFontWithData:(NSData *)inData;
+ (void)registerFontWithData:(NSData *)inData
                  subscriber:(id<RACSubscriber>) signal;

@end
